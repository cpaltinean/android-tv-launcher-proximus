/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import tv.threess.threeready.data.proximus.log.utils.EdidInfo;
import tv.threess.threeready.data.proximus.log.utils.ExtendedEdidInfo;

/**
 * Test unit for utility class.
 *
 * @author Karetka Mezei ZOltan
 * @since 2017.02.14
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class HdmiEdidParser {

    @Test
    public void testParser() {
        final byte[] bytes = {
                (byte) 0x00, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0x00,
                (byte) 0x41, (byte) 0x0c, (byte) 0xe5, (byte) 0xc0, (byte) 0xce, (byte) 0x06, (byte) 0x00, (byte) 0x00,
                (byte) 0x1e, (byte) 0x1a, (byte) 0x01, (byte) 0x03, (byte) 0x80, (byte) 0x30, (byte) 0x1b, (byte) 0x78,
                (byte) 0x2a, (byte) 0xe7, (byte) 0xe5, (byte) 0xa5, (byte) 0x56, (byte) 0x4d, (byte) 0xa1, (byte) 0x25,
                (byte) 0x0f, (byte) 0x50, (byte) 0x54, (byte) 0xbd, (byte) 0x4b, (byte) 0x00, (byte) 0xd1, (byte) 0xc0,
                (byte) 0x95, (byte) 0x00, (byte) 0x95, (byte) 0x0f, (byte) 0xb3, (byte) 0x00, (byte) 0x81, (byte) 0xc0,
                (byte) 0x81, (byte) 0x80, (byte) 0x01, (byte) 0x01, (byte) 0x01, (byte) 0x01, (byte) 0x02, (byte) 0x3a,
                (byte) 0x80, (byte) 0x18, (byte) 0x71, (byte) 0x38, (byte) 0x2d, (byte) 0x40, (byte) 0x58, (byte) 0x2c,
                (byte) 0x45, (byte) 0x00, (byte) 0xdd, (byte) 0x0c, (byte) 0x11, (byte) 0x00, (byte) 0x00, (byte) 0x1e,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xff, (byte) 0x00, (byte) 0x5a, (byte) 0x51, (byte) 0x30,
                (byte) 0x31, (byte) 0x36, (byte) 0x33, (byte) 0x30, (byte) 0x30, (byte) 0x30, (byte) 0x31, (byte) 0x37,
                (byte) 0x34, (byte) 0x32, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xfc, (byte) 0x00, (byte) 0x50,
                (byte) 0x48, (byte) 0x4c, (byte) 0x20, (byte) 0x32, (byte) 0x32, (byte) 0x37, (byte) 0x45, (byte) 0x36,
                (byte) 0x0a, (byte) 0x20, (byte) 0x20, (byte) 0x20, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xfd,
                (byte) 0x00, (byte) 0x38, (byte) 0x4c, (byte) 0x1e, (byte) 0x53, (byte) 0x11, (byte) 0x00, (byte) 0x0a,
                (byte) 0x20, (byte) 0x20, (byte) 0x20, (byte) 0x20, (byte) 0x20, (byte) 0x20, (byte) 0x01, (byte) 0x54
        };

        EdidInfo info = new EdidInfo(bytes);
        Assert.assertEquals("00 FF FF FF FF FF FF 00", info.getHeader());
        Assert.assertEquals("CE 06 00 00", info.getSerialNumber());
        Assert.assertEquals("PHL", info.getManufacturerId());
        Assert.assertEquals("E5 C0", info.getProductCode());
        Assert.assertEquals(2016, info.getManufacturedYear());
        Assert.assertEquals(30, info.getManufacturedWeek());
        Assert.assertEquals("01", info.getVersion());
        Assert.assertEquals("03", info.getRevision());
        System.out.println(info);
    }

    @Test
    public void testExtendedParser() {
        final byte[] bytes = {
                (byte) 0x02, (byte) 0x03, (byte) 0x22, (byte) 0xf1, (byte) 0x4f, (byte) 0x01, (byte) 0x02, (byte) 0x03,
                (byte) 0x05, (byte) 0x06, (byte) 0x07, (byte) 0x10, (byte) 0x11, (byte) 0x12, (byte) 0x13, (byte) 0x14,
                (byte) 0x15, (byte) 0x16, (byte) 0x1f, (byte) 0x04, (byte) 0x23, (byte) 0x09, (byte) 0x17, (byte) 0x07,
                (byte) 0x83, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x65, (byte) 0x03, (byte) 0x0c, (byte) 0x00,
                (byte) 0x10, (byte) 0x00, (byte) 0x02, (byte) 0x3a, (byte) 0x80, (byte) 0x18, (byte) 0x71, (byte) 0x38,
                (byte) 0x2d, (byte) 0x40, (byte) 0x58, (byte) 0x2c, (byte) 0x45, (byte) 0x00, (byte) 0xdd, (byte) 0x0c,
                (byte) 0x11, (byte) 0x00, (byte) 0x00, (byte) 0x1e, (byte) 0x8c, (byte) 0x0a, (byte) 0xd0, (byte) 0x8a,
                (byte) 0x20, (byte) 0xe0, (byte) 0x2d, (byte) 0x10, (byte) 0x10, (byte) 0x3e, (byte) 0x96, (byte) 0x00,
                (byte) 0xdd, (byte) 0x0c, (byte) 0x11, (byte) 0x00, (byte) 0x00, (byte) 0x18, (byte) 0x01, (byte) 0x1d,
                (byte) 0x00, (byte) 0x72, (byte) 0x51, (byte) 0xd0, (byte) 0x1e, (byte) 0x20, (byte) 0x6e, (byte) 0x28,
                (byte) 0x55, (byte) 0x00, (byte) 0xdd, (byte) 0x0c, (byte) 0x11, (byte) 0x00, (byte) 0x00, (byte) 0x1e,
                (byte) 0x8c, (byte) 0x0a, (byte) 0xd0, (byte) 0x90, (byte) 0x20, (byte) 0x40, (byte) 0x31, (byte) 0x20,
                (byte) 0x0c, (byte) 0x40, (byte) 0x55, (byte) 0x00, (byte) 0xdd, (byte) 0x0c, (byte) 0x11, (byte) 0x00,
                (byte) 0x00, (byte) 0x18, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x4d
        };

        ExtendedEdidInfo info = new ExtendedEdidInfo(bytes);
        Assert.assertEquals("02", info.getExtensionTag());
        Assert.assertEquals("03", info.getRevisionNr());
        Assert.assertTrue(info.isSupportUnderscan());
        Assert.assertTrue(info.isSupportBasicAudio());
        Assert.assertTrue(info.isSupportYCvCr444());
        Assert.assertTrue(info.isSupportYCvCr422());
        Assert.assertEquals(1, info.getTotalNrOfNativeFormats());
        Assert.assertEquals(0, info.getResIndexValue());
        System.out.println(info);
    }
}