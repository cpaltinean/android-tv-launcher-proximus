package tv.threess.threeready.data.proximus.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.util.ArrayList;

import tv.threess.threeready.data.mw.BaseMwProxy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(BlockJUnit4ClassRunner.class)
public class UsageTest {

    private final String [] packageNames = {"px.be.stbtvclient", "com.ninja.netflix", "com.radioline"};
    private final int [] eventTimes = {10, 20, 60, 75};

    public static class Event {
        String packageName;
        long timestamp;
        boolean isActive;

        public Event (String packageName, long timestamp, boolean isActive){
            this.packageName = packageName;
            this.timestamp = timestamp;
            this.isActive = isActive;
        }
    }

    public static class TestEvents implements BaseMwProxy.ApplicationEvents{
        private final Event [] events;
        private int i = -1;

        public TestEvents(Event... events) {
            this.events = events;
        }

        @Override
        public boolean isForeground() {
            return events[i].isActive;
        }

        @Override
        public boolean isBackground() {
            return !events[i].isActive;
        }

        @Override
        public long getTimeStamp() {
            return events[i].timestamp;
        }

        @Override
        public String getPackageName() {
            return events[i].packageName;
        }

        @Override
        public boolean hasNext() {
            return i<events.length-1;
        }

        @Override
        public BaseMwProxy.ApplicationEvents next() {
            i++;
            return this;
        }
    }


    // Gives our app start to reportUsages function
    // Has to gives back no events
    @Test
    public void ownApplicationStart(){
        EventLogger result = new EventLogger();

        Event ownAppStartEvent = new Event(packageNames[0], eventTimes[0], true);
        TestEvents events = new TestEvents(ownAppStartEvent);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);
        assertTrue(result.isEmpty());
    }

    // Gives our app stop to reportUsages function
    // Has to gives back no events
    @Test
    public void ownApplicationStop(){
        Event ownAppStartEvent = new Event(packageNames[0], eventTimes[0], false);

        EventLogger result = new EventLogger();
        TestEvents events = new TestEvents(ownAppStartEvent);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);
        assertTrue(result.isEmpty());
    }

    // one stop event same application
    // should be gives back a stop event
    @Test
    public void sameApplicationStopEventInList(){
        Event stopEvent = new Event(packageNames[1], eventTimes[0], false);
        TestEvents events = new TestEvents(stopEvent);

        EventLogger result = new EventLogger();
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);
        assertEquals(1, result.size());
        assertFalse(result.get(0).isActive);
        assertEquals(stopEvent.packageName, result.get(0).packageName);
    }

    // start event same application
    // should gives back one start event
    @Test
    public void applicationStartEventInList(){
        Event startEvent = new Event(packageNames[1], eventTimes[0], true);
        TestEvents events = new TestEvents(startEvent);
        EventLogger result = new EventLogger();
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);

        assertEquals(1, result.size());
        assertTrue(result.get(0).isActive);
        assertEquals(startEvent.packageName, result.get(0).packageName);
    }

    // same application one start and stop event
    // should be gives back these two events
    @Test
    public void sameApplicationStartStopEvent(){
        Event [] eventsSameStartStop = {
                new Event(packageNames[1], eventTimes[0], true),
                new Event(packageNames[1], eventTimes[1], false)
        };

        EventLogger result = new EventLogger();
        TestEvents events = new TestEvents(eventsSameStartStop);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);

        assertEquals(2, result.size());
        assertEquals(eventsSameStartStop[0].isActive, result.get(0).isActive);
        assertEquals(eventsSameStartStop[0].packageName, result.get(0).packageName);
        assertEquals(eventsSameStartStop[0].timestamp, result.get(0).timestamp);
        assertEquals(eventsSameStartStop[1].isActive, result.get(1).isActive);
        assertEquals(eventsSameStartStop[1].packageName, result.get(1).packageName);
        assertEquals(eventsSameStartStop[1].timestamp, result.get(1).timestamp);
    }

    // same application stop and start event
    // should gives back these two event
    @Test
    public void sameApplicationStopStartEvent(){
        Event [] eventsSameStopStart = {
                new Event(packageNames[1], eventTimes[0], false),
                new Event(packageNames[1], eventTimes[1], true)
        };

        EventLogger result = new EventLogger();
        TestEvents events = new TestEvents(eventsSameStopStart);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);

        assertEquals(eventsSameStopStart.length, result.size());
        assertEquals(eventsSameStopStart[0].isActive, result.get(0).isActive);
        assertEquals(eventsSameStopStart[0].packageName, result.get(0).packageName);
        assertEquals(eventsSameStopStart[0].timestamp, result.get(0).timestamp);
        assertEquals(eventsSameStopStart[1].isActive, result.get(1).isActive);
        assertEquals(eventsSameStopStart[1].packageName, result.get(1).packageName);
        assertEquals(eventsSameStopStart[1].timestamp, result.get(1).timestamp);
    }

    // same application start->stop->start->stop
    // should gives back the first start and the last stop event (merge)
    @Test
    public void sameApplicationDoubleNormalOrderStartStop(){
        Event [] doubleNormalOrderStartStop = {
                new Event(packageNames[1], eventTimes[0], true),
                new Event(packageNames[1], eventTimes[1], false),
                new Event(packageNames[1], eventTimes[2], true),
                new Event(packageNames[1], eventTimes[3], false)
        };

        EventLogger result = new EventLogger();
        TestEvents events = new TestEvents(doubleNormalOrderStartStop);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);

        assertEquals(2, result.size());
        assertEquals(doubleNormalOrderStartStop[0].isActive, result.get(0).isActive);
        assertEquals(doubleNormalOrderStartStop[0].packageName, result.get(0).packageName);
        assertEquals(doubleNormalOrderStartStop[0].timestamp, result.get(0).timestamp);
        assertEquals(doubleNormalOrderStartStop[3].isActive, result.get(1).isActive);
        assertEquals(doubleNormalOrderStartStop[3].packageName, result.get(1).packageName);
        assertEquals(doubleNormalOrderStartStop[3].timestamp, result.get(1).timestamp);
    }

    // same application stop->start->stop->start
    // should gives back a stop event and a merged start->stop
    // the last start has no pair so left untouched because on BE don't need to know
    // when started secondly the same app, just the first start and last stop,
    // in this case have no last stop.
    @Test
    public void sameApplicationDoubleStopStartEvent(){
        Event [] doubleNormalOrderStopStart = {
                new Event(packageNames[1], eventTimes[0], false),
                new Event(packageNames[1], eventTimes[1], true),
                new Event(packageNames[1], eventTimes[2], false),
                new Event(packageNames[1], eventTimes[3], true)
        };

        EventLogger result = new EventLogger();
        TestEvents events = new TestEvents(doubleNormalOrderStopStart);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);

        assertEquals(3, result.size());
        assertEquals(doubleNormalOrderStopStart[0].isActive, result.get(0).isActive);
        assertEquals(doubleNormalOrderStopStart[0].packageName, result.get(0).packageName);
        assertEquals(doubleNormalOrderStopStart[0].timestamp, result.get(0).timestamp);
        assertEquals(doubleNormalOrderStopStart[1].isActive, result.get(1).isActive);
        assertEquals(doubleNormalOrderStopStart[1].packageName, result.get(1).packageName);
        assertEquals(doubleNormalOrderStopStart[1].timestamp, result.get(1).timestamp);
        assertEquals(doubleNormalOrderStopStart[2].isActive, result.get(2).isActive);
        assertEquals(doubleNormalOrderStopStart[2].packageName, result.get(2).packageName);
        assertEquals(doubleNormalOrderStopStart[2].timestamp, result.get(2).timestamp);
    }

    // same application stop->start->start->start
    // When comes after a start again a start with same package name then skip reporting all next
    // start events that have same package name.
    // It should be gives back 2 events (stop, first start)
    @Test
    public void sameApplicationStopMultiStart(){
        Event [] eventsStopMultiStartSameApplication = {
                new Event(packageNames[1], eventTimes[0], false),
                new Event(packageNames[1], eventTimes[1], true),
                new Event(packageNames[1], eventTimes[2], true),
                new Event(packageNames[1], eventTimes[3], true),
        };

        EventLogger result = new EventLogger();
        TestEvents events = new TestEvents(eventsStopMultiStartSameApplication);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);

        assertEquals(2, result.size());
        assertEquals(eventsStopMultiStartSameApplication[0].isActive, result.get(0).isActive);
        assertEquals(eventsStopMultiStartSameApplication[0].packageName, result.get(0).packageName);
        assertEquals(eventsStopMultiStartSameApplication[0].timestamp, result.get(0).timestamp);
        assertEquals(eventsStopMultiStartSameApplication[1].isActive, result.get(1).isActive);
        assertEquals(eventsStopMultiStartSameApplication[1].packageName, result.get(1).packageName);
        assertEquals(eventsStopMultiStartSameApplication[1].timestamp, result.get(1).timestamp);
    }


    // same application start->stop->stop->stop
    // When comes after a stop again a stop with same package name then skip reporting all next
    // stop events that have same package name and report the last stop event.
    // It should be gives back 2 events (start, last stop)
    @Test
    public void sameApplicationStartMultiStop(){
        Event [] eventsStartMultiStopSameApplication = {
                new Event(packageNames[1], eventTimes[0], true),
                new Event(packageNames[1], eventTimes[1], false),
                new Event(packageNames[1], eventTimes[2], false),
                new Event(packageNames[1], eventTimes[3], false),
        };

        EventLogger result = new EventLogger();
        TestEvents events = new TestEvents(eventsStartMultiStopSameApplication);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);

        assertEquals(2, result.size());
        assertEquals(eventsStartMultiStopSameApplication[0].isActive, result.get(0).isActive);
        assertEquals(eventsStartMultiStopSameApplication[0].packageName, result.get(0).packageName);
        assertEquals(eventsStartMultiStopSameApplication[0].timestamp, result.get(0).timestamp);
        assertEquals(eventsStartMultiStopSameApplication[3].isActive, result.get(1).isActive);
        assertEquals(eventsStartMultiStopSameApplication[3].packageName, result.get(1).packageName);
        assertEquals(eventsStartMultiStopSameApplication[3].timestamp, result.get(1).timestamp);
    }

    // different application stop->start
    // In package name change case we have to log the previous event (in this case the stop event)
    // and after that log the start event too.
    // It should gives back two events (stop(packageNameId1), start(packageNameId2))
    @Test
    public void notSameApplicationStopStart(){
        Event [] eventsStopStartNotSameApplication = {
                new Event(packageNames[2], eventTimes[0], false),
                new Event(packageNames[1], eventTimes[1], true)
        };

        EventLogger result = new EventLogger();
        TestEvents events = new TestEvents(eventsStopStartNotSameApplication);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);

        assertEquals(2, result.size());
        assertEquals(eventsStopStartNotSameApplication[0].isActive, result.get(0).isActive);
        assertEquals(eventsStopStartNotSameApplication[0].packageName, result.get(0).packageName);
        assertEquals(eventsStopStartNotSameApplication[0].timestamp, result.get(0).timestamp);
        assertEquals(eventsStopStartNotSameApplication[1].isActive, result.get(1).isActive);
        assertEquals(eventsStopStartNotSameApplication[1].packageName, result.get(1).packageName);
        assertEquals(eventsStopStartNotSameApplication[1].timestamp, result.get(1).timestamp);
    }

    // different application start->stop
    // In package name change case we have to log the previous event (in this case the start event)
    // and after that log the stop event too.
    // It should gives back two events (start(packageNameId1), stop(packageNameId2))
    @Test
    public void notSameApplicationStartStop(){
        Event [] eventsStartStopNotSameApplication = {
                new Event(packageNames[2], eventTimes[0], true),
                new Event(packageNames[1], eventTimes[1], false)
        };

        EventLogger result = new EventLogger();
        TestEvents events = new TestEvents(eventsStartStopNotSameApplication);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);

        assertEquals(2, result.size());
        assertEquals(eventsStartStopNotSameApplication[0].isActive, result.get(0).isActive);
        assertEquals(eventsStartStopNotSameApplication[0].packageName, result.get(0).packageName);
        assertEquals(eventsStartStopNotSameApplication[0].timestamp, result.get(0).timestamp);
        assertEquals(eventsStartStopNotSameApplication[1].isActive, result.get(1).isActive);
        assertEquals(eventsStartStopNotSameApplication[1].packageName, result.get(1).packageName);
        assertEquals(eventsStartStopNotSameApplication[1].timestamp, result.get(1).timestamp);
    }

    // different application start->start
    // In package name change case we have to log the previous event (in this case the start event)
    // and after that log the second start (another app start) event too.
    // It should gives back two events (start(packageNameId1), start(packageNameId2))
    @Test
    public void notSameApplicationStartStart(){
        Event [] eventsStartStartNotSame = {
                new Event(packageNames[2], eventTimes[0], true),
                new Event(packageNames[1], eventTimes[1], true)
        };

        EventLogger result = new EventLogger();
        TestEvents events = new TestEvents(eventsStartStartNotSame);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);

        assertEquals(2, result.size());
        assertEquals(eventsStartStartNotSame[0].isActive, result.get(0).isActive);
        assertEquals(eventsStartStartNotSame[0].packageName, result.get(0).packageName);
        assertEquals(eventsStartStartNotSame[0].timestamp, result.get(0).timestamp);
        assertEquals(eventsStartStartNotSame[1].isActive, result.get(1).isActive);
        assertEquals(eventsStartStartNotSame[1].packageName, result.get(1).packageName);
        assertEquals(eventsStartStartNotSame[1].timestamp, result.get(1).timestamp);
    }

    // different application stop->stop
    // In package name change case we have to log the previous event (in this case the stop event)
    // and after that log the second stop (another app stop) event too.
    // It should gives back two events (stop(packageNameId1), stop(packageNameId2))
    @Test
    public void notSameApplicationStopStop(){
        Event [] eventsStopStopNotSame = {
                new Event(packageNames[2], eventTimes[0], false),
                new Event(packageNames[1], eventTimes[1], false)
        };

        EventLogger result = new EventLogger();
        TestEvents events = new TestEvents(eventsStopStopNotSame);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);

        assertEquals(2, result.size());
        assertEquals(eventsStopStopNotSame[0].isActive, result.get(0).isActive);
        assertEquals(eventsStopStopNotSame[0].packageName, result.get(0).packageName);
        assertEquals(eventsStopStopNotSame[0].timestamp, result.get(0).timestamp);
        assertEquals(eventsStopStopNotSame[1].isActive, result.get(1).isActive);
        assertEquals(eventsStopStopNotSame[1].packageName, result.get(1).packageName);
        assertEquals(eventsStopStopNotSame[1].timestamp, result.get(1).timestamp);
    }

    // normal order (ex. same app start->stop and another app start->stop)
    // In this case we have to log all events as pair.
    // It should gives back four event (two with same package name,
    // another two with another package name)
    @Test
    public void notSameAppsNormalOrder(){
        Event [] eventNormalOrder = {
                new Event(packageNames[1], eventTimes[0], true),
                new Event(packageNames[1], eventTimes[1], false),
                new Event(packageNames[2], eventTimes[2], true),
                new Event(packageNames[2], eventTimes[3], false)
        };

        EventLogger result = new EventLogger();
        TestEvents events = new TestEvents(eventNormalOrder);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);

        assertEquals(4, result.size());
        assertEquals(eventNormalOrder[0].isActive, result.get(0).isActive);
        assertEquals(eventNormalOrder[0].packageName, result.get(0).packageName);
        assertEquals(eventNormalOrder[0].timestamp, result.get(0).timestamp);
        assertEquals(eventNormalOrder[1].isActive, result.get(1).isActive);
        assertEquals(eventNormalOrder[1].packageName, result.get(1).packageName);
        assertEquals(eventNormalOrder[1].timestamp, result.get(1).timestamp);
        assertEquals(eventNormalOrder[2].isActive, result.get(2).isActive);
        assertEquals(eventNormalOrder[2].packageName, result.get(2).packageName);
        assertEquals(eventNormalOrder[2].timestamp, result.get(2).timestamp);
        assertEquals(eventNormalOrder[3].isActive, result.get(3).isActive);
        assertEquals(eventNormalOrder[3].packageName, result.get(3).packageName);
        assertEquals(eventNormalOrder[3].timestamp, result.get(3).timestamp);
    }

    // normal order means the events comes randomly
    // (ex. start(packageId1)->start(packageId2)->stop(packageId2)->start(packageId1))
    // In this case we have to log all events as pair.
    // It should gives back four event
    @Test
    public void notSameAppsAbnormalOrder(){
        Event [] eventsAbnormalOrder = {
                new Event(packageNames[1], eventTimes[0], true),
                new Event(packageNames[2], eventTimes[1], true),
                new Event(packageNames[2], eventTimes[2], false),
                new Event(packageNames[1], eventTimes[3], false)
        };

        EventLogger result = new EventLogger();
        TestEvents events = new TestEvents(eventsAbnormalOrder);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);

        assertEquals(4, result.size());
        assertEquals(eventsAbnormalOrder[0].isActive, result.get(0).isActive);
        assertEquals(eventsAbnormalOrder[0].packageName, result.get(0).packageName);
        assertEquals(eventsAbnormalOrder[0].timestamp, result.get(0).timestamp);
        assertEquals(eventsAbnormalOrder[1].isActive, result.get(1).isActive);
        assertEquals(eventsAbnormalOrder[1].packageName, result.get(1).packageName);
        assertEquals(eventsAbnormalOrder[1].timestamp, result.get(1).timestamp);
        assertEquals(eventsAbnormalOrder[2].isActive, result.get(2).isActive);
        assertEquals(eventsAbnormalOrder[2].packageName, result.get(2).packageName);
        assertEquals(eventsAbnormalOrder[2].timestamp, result.get(2).timestamp);
        assertEquals(eventsAbnormalOrder[3].isActive, result.get(3).isActive);
        assertEquals(eventsAbnormalOrder[3].packageName, result.get(3).packageName);
        assertEquals(eventsAbnormalOrder[3].timestamp, result.get(3).timestamp);
    }

    // different application stop and another application multiple start
    // At package name change has to log the previous event and log the first start event of
    // the another app, because we need the first occurrence of the start.
    // It should gives back two events (stop(packageName1)->first start(packageName2))
    @Test
    public void notSameAppStopMultiStart(){
        Event [] eventStopMultiStartNotSame = {
                new Event(packageNames[1], eventTimes[0], false),
                new Event(packageNames[2], eventTimes[1], true),
                new Event(packageNames[2], eventTimes[2], true),
                new Event(packageNames[2], eventTimes[3], true)
        };

        EventLogger result = new EventLogger();
        TestEvents events = new TestEvents(eventStopMultiStartNotSame);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);

        assertEquals(2, result.size());
        assertEquals(eventStopMultiStartNotSame[0].isActive, result.get(0).isActive);
        assertEquals(eventStopMultiStartNotSame[0].packageName, result.get(0).packageName);
        assertEquals(eventStopMultiStartNotSame[0].timestamp, result.get(0).timestamp);
        assertEquals(eventStopMultiStartNotSame[1].isActive, result.get(1).isActive);
        assertEquals(eventStopMultiStartNotSame[1].packageName, result.get(1).packageName);
        assertEquals(eventStopMultiStartNotSame[1].timestamp, result.get(1).timestamp);
    }


    // different application start and another application multiple stop
    // At package name change has to log the previous start event and log the last stop event of
    // the another app, because we need the last occurrence of the stop.
    // It should gives back two events (start(packageName1)->last stop(packageName2))
    @Test
    public void notSameAppStartMultiStop(){
        Event [] eventStartMultiStopNotSame = {
                new Event(packageNames[1], eventTimes[0], true),
                new Event(packageNames[2], eventTimes[1], false),
                new Event(packageNames[2], eventTimes[2], false),
                new Event(packageNames[2], eventTimes[3], false)
        };

        EventLogger result = new EventLogger();
        TestEvents events = new TestEvents(eventStartMultiStopNotSame);
        BaseMwProxy.reportUsageEvents(events, packageNames[0], result);

        assertEquals(2, result.size());
        assertEquals(eventStartMultiStopNotSame[0].isActive, result.get(0).isActive);
        assertEquals(eventStartMultiStopNotSame[0].packageName, result.get(0).packageName);
        assertEquals(eventStartMultiStopNotSame[0].timestamp, result.get(0).timestamp);
        assertEquals(eventStartMultiStopNotSame[3].isActive, result.get(1).isActive);
        assertEquals(eventStartMultiStopNotSame[3].packageName, result.get(1).packageName);
        assertEquals(eventStartMultiStopNotSame[3].timestamp, result.get(1).timestamp);
    }


    private static class EventLogger extends ArrayList<Event> implements BaseMwProxy.ApplicationUsageLogger{
        /**
         * It add events to list in different way.
         * If the start and end timestamp is equal than it means it got an event without pair.
         * (Ex. start without stop or inversely). If the event isInActiveState is true than it adds
         * to list as a start event. Else adds as a stop event.
         *
         * Normally the start and end timestamp is different that means it adds start and the stop event
         * to list as a pair because these two events has the same package name.
         *
         * @param start the event start timestamp
         * @param end the event end timestamp
         * @param packageName the application package name the will be reported
         * @param isInActiveState the type of the event (background == false or foreground == true)
         */
        @Override
        public void onUsageLog(long start, long end, String packageName, boolean isInActiveState) {
            if (start == end) {
                if (isInActiveState) {
                    this.add(new Event(packageName, start, true));
                    return;
                }

                this.add(new Event(packageName, end, false));
                return;
            }

            this.add(new Event(packageName, start, true));
            this.add(new Event(packageName, end, false));
        }
    }
}
