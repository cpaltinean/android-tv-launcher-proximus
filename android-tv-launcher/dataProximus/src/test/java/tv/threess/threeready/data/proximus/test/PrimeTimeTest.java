/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.data.config.model.PlaybackSettings3Ready;

/**
 * Test unit for utility class.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.02.14
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class PrimeTimeTest {
    @Test
    public void testPrimeTimes() {
        long timeWindow = Long.MAX_VALUE;
        PlaybackSettings3Ready.PrimeTime pt = new PlaybackSettings3Ready.PrimeTime(20, 0);

        showPrimeTimes(true, 3, pt);
        showPrimeTimes(false, 3, pt);

        // reference time: 2019.09.14-17:25, next primeTime: 2019.09.15-20:00
        Assert.assertEquals(1568480400000L, TimeUtils.nextPrimeTime(1568471100000L, timeWindow, pt));

        // reference time: 2019.09.15-20:00, next prime time: 2019.09.16-20:00
        Assert.assertEquals(1568566800000L, TimeUtils.nextPrimeTime(1568480400000L, timeWindow, pt));

        // reference time: 2019.09.14-17:25, past prime time: 2019.09.13-20:00
        Assert.assertEquals(1568394000000L, TimeUtils.prevPrimeTime(1568471100000L, timeWindow, pt));

        // reference time: 2019.09.14-20:00, past prime time: 2019.09.13-20:00
        Assert.assertEquals(1568394000000L, TimeUtils.prevPrimeTime(1568471100000L, timeWindow, pt));
    }

    private static void showPrimeTimes(boolean future, int days, PlaybackSettings3Ready.PrimeTime pt) {
        System.out.println((future ? "future" : "past") + " prime times: " + days);
        long referenceTime = TimeUtils.LIVE_PROGRAM_TIMESTAMP;
        long timeWindow = TimeUnit.DAYS.toMillis(365);
        for (int i = 0; i < days; ++i) {
            long primeTime = future ?
                    TimeUtils.nextPrimeTime(referenceTime, timeWindow, pt) :
                    TimeUtils.prevPrimeTime(referenceTime, timeWindow, pt);
            System.out.println("reference time: " + format(referenceTime) + ", prime time: " + format(primeTime));
            referenceTime = primeTime;
        }
        System.out.println();
        referenceTime -= TimeUnit.DAYS.toMillis((future ? 2 : -2) * days);
        for (int i = 0; i < days * 2; ++i) {
            long primeTime = future ?
                    TimeUtils.nextPrimeTime(referenceTime, timeWindow, pt) :
                    TimeUtils.prevPrimeTime(referenceTime, timeWindow, pt);
            System.out.println("reference time: " + format(referenceTime) + ", prime time: " + format(primeTime));
            referenceTime = primeTime;
        }
        System.out.println();
    }

    private static String format(long timestamp) {
        return TimeBuilder.local(timestamp).toString(TimeUnit.MINUTES);
    }
}
