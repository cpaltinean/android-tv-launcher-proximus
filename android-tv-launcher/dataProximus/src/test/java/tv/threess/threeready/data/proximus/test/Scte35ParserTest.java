/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import tv.threess.threeready.api.middleware.model.Scte35Info;
import tv.threess.threeready.api.middleware.model.BitStream;

/**
 * Test unit for parsing scte-35 messages.
 *
 * @author Karetka Mezei Zoltan
 * @since 2021.08.13
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class Scte35ParserTest {

    @Test
    public void testParser_xFFF0() {
        final BitStream bytes = new BitStream(new byte[] {
                (byte) 0xfc, (byte) 0x30, (byte) 0xed, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0xff, (byte) 0xf0, (byte) 0x14, (byte) 0x05, (byte) 0x00, (byte) 0x00,
                (byte) 0x14, (byte) 0x03, (byte) 0x7f, (byte) 0xef, (byte) 0xfe, (byte) 0xca, (byte) 0xe1, (byte) 0x82,
                (byte) 0xa0, (byte) 0xfe, (byte) 0x00, (byte) 0x29, (byte) 0x5d, (byte) 0x10, (byte) 0x00, (byte) 0x04,
                (byte) 0x01, (byte) 0x01, (byte) 0x00, (byte) 0xc8, (byte) 0x02, (byte) 0xc6, (byte) 0x43, (byte) 0x55,
                (byte) 0x45, (byte) 0x49, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x7f, (byte) 0xff,
                (byte) 0x00, (byte) 0x00, (byte) 0x29, (byte) 0x32, (byte) 0xe0, (byte) 0x0c, (byte) 0xb2, (byte) 0x50,
                (byte) 0x58, (byte) 0x53, (byte) 0x41, (byte) 0x01, (byte) 0xff, (byte) 0xf0, (byte) 0x5f, (byte) 0x5f,
                (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f,
                (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f,
                (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f,
                (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x53, (byte) 0x45, (byte) 0x47, (byte) 0x4d,
                (byte) 0x30, (byte) 0x34, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f,
                (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f,
                (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x50, (byte) 0x52, (byte) 0x4f, (byte) 0x47, (byte) 0x30,
                (byte) 0x34, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f,
                (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f,
                (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f,
                (byte) 0x5f, (byte) 0x5f, (byte) 0x49, (byte) 0x4e, (byte) 0x47, (byte) 0x20, (byte) 0x42, (byte) 0x41,
                (byte) 0x4e, (byte) 0x51, (byte) 0x55, (byte) 0x45, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f,
                (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f, (byte) 0x5f,
                (byte) 0x5f, (byte) 0x5f, (byte) 0x50, (byte) 0x34, (byte) 0x1e, (byte) 0x00, (byte) 0x0a, (byte) 0x00,
                (byte) 0x14, (byte) 0x00, (byte) 0x1e, (byte) 0x00, (byte) 0x28, (byte) 0x00, (byte) 0x32, (byte) 0x00,
                (byte) 0x3c, (byte) 0x00, (byte) 0x46, (byte) 0x00, (byte) 0x50, (byte) 0x00, (byte) 0x5a, (byte) 0x00,
                (byte) 0x64, (byte) 0x00, (byte) 0x96, (byte) 0x00, (byte) 0xc8, (byte) 0x00, (byte) 0xfa, (byte) 0x01,
                (byte) 0x2c, (byte) 0x01, (byte) 0x5e, (byte) 0x01, (byte) 0x90, (byte) 0x01, (byte) 0xc2, (byte) 0x01,
                (byte) 0xf4, (byte) 0x02, (byte) 0x26, (byte) 0x02, (byte) 0x58, (byte) 0x02, (byte) 0x8a, (byte) 0x02,
                (byte) 0xbc, (byte) 0x02, (byte) 0xee, (byte) 0x03, (byte) 0x20, (byte) 0x03, (byte) 0x2a, (byte) 0x03,
                (byte) 0x52, (byte) 0x03, (byte) 0x84, (byte) 0x03, (byte) 0xb6, (byte) 0x03, (byte) 0xc0, (byte) 0x03,
                (byte) 0xde, (byte) 0x30, (byte) 0x01, (byte) 0x01, (byte) 0x49, (byte) 0x41, (byte) 0xdb, (byte) 0x50
        });

        // base64 encoded splice descriptor
        String spliceDescriptor = "AsZDVUVJAAAAAH//AAApMuAMslBYU0EB//BfX19fX19fX19fX19fX19fX19fX19f" +
                "X19fX19fX19TRUdNMDRfX19fX19fX19fX19fX19fX1BST0cwNAAAAF9fX19fX19f" +
                "X19fX19fX19fX19fX19JTkcgQkFOUVVFX19fX19fX19fX19fX19QNB4ACgAUAB4A" +
                "KAAyADwARgBQAFoAZACWAMgA+gEsAV4BkAHCAfQCJgJYAooCvALuAyADKgNSA4QD" +
                "tgPAA94wAQE=";

        Scte35Info info = new Scte35Info(bytes.reset());
        Assert.assertEquals(252, info.get(Scte35Info.Field.table_id, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.section_syntax_indicator, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.private_indicator, 0));
        Assert.assertEquals(237, info.get(Scte35Info.Field.section_length, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.protocol_version, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.encrypted_packet, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.encryption_algorithm, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.pts_adjustment, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.cw_index, 0));
        Assert.assertEquals(4095, info.get(Scte35Info.Field.tier, 0));
        Assert.assertEquals(20, info.get(Scte35Info.Field.splice_command_length, 0));
        Assert.assertEquals(5, info.get(Scte35Info.Field.splice_command_type, 0));
        Assert.assertEquals(200, info.get(Scte35Info.Field.descriptor_loop_length, 0));
        Assert.assertEquals(3403776672L, info.get(Scte35Info.Field.pts_time, 0));
        Assert.assertEquals(5123, info.get(Scte35Info.Field.splice_event_id, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.splice_event_cancel_indicator, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.out_of_network_indicator, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.program_splice_flag, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.duration_flag, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.splice_immediate_flag, 0));
        Assert.assertEquals(4, info.get(Scte35Info.Field.unique_program_id, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.avail_num, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.avails_expected, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.auto_return, 0));
        Assert.assertEquals(2710800, info.get(Scte35Info.Field.duration, 0));
        Assert.assertEquals(2, info.get(Scte35Info.Field.splice_descriptor_tag, 0));
        Assert.assertEquals(198, info.get(Scte35Info.Field.splice_descriptor_length, 0));
        Assert.assertEquals("CUEI", info.get(Scte35Info.Field.splice_identifier));
        Assert.assertEquals(spliceDescriptor, info.get(Scte35Info.Field.splice_descriptor));
        Assert.assertEquals(0, info.get(Scte35Info.Field.segmentation_event_id, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.segmentation_event_cancel_indicator, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.delivery_not_restricted_flag, 0));
        Assert.assertEquals(2700000, info.get(Scte35Info.Field.segmentation_duration, 0));
        Assert.assertEquals(12, info.get(Scte35Info.Field.segmentation_upid_type, 0));
        Assert.assertEquals(178, info.get(Scte35Info.Field.segmentation_upid_length, 0));
        Assert.assertEquals(48, info.get(Scte35Info.Field.segmentation_type_id, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.segment_num, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.segments_expected, 0));
        Assert.assertEquals("PXSA", info.get(Scte35Info.Field.format_identifier));
        Assert.assertEquals(1, info.get(Scte35Info.Field.private_upid_version, 0));
        Assert.assertEquals("______________________________SEGM04", info.get(Scte35Info.Field.private_segment_id));
        Assert.assertEquals("_________________PROG04", info.get(Scte35Info.Field.private_program_id));
        Assert.assertEquals("0000", info.get(Scte35Info.Field.private_cni));
        Assert.assertEquals(0, info.get(Scte35Info.Field.private_ad_count_position, 0));
        Assert.assertEquals("______________________ING BANQUE", info.get(Scte35Info.Field.private_brand));
        Assert.assertEquals("______________P4", info.get(Scte35Info.Field.private_pod_id));
        Assert.assertEquals(30, info.get(Scte35Info.Field.private_sectors_count, 0));
        Assert.assertEquals(30, info.count(Scte35Info.Field.private_sectorial));
        Assert.assertEquals("FFF0", info.get(Scte35Info.Field.type_of_replacement));
        Assert.assertEquals(1, info.get(Scte35Info.Field.transmission_inside_break, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.transmission_is_live, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.perform_replacement_in_cpvr, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.perform_replacement_in_npvr, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.perform_replacement_in_replay, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.perform_replacement_in_live, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.replacement_is_multiple, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.replacement_is_regional, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.replacement_is_brand4brand, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.replacement_language_fr_nl, 0));
        Assert.assertEquals(1229052752L, info.get(Scte35Info.Field.crc_32, 0));
        Assert.assertEquals(7, info.count(Scte35Info.Field.reserved));
    }

    @Test
    public void testParser_xFFE8() {
        BitStream bytes = new BitStream("FC30B900000000000000FFF01405000012B57FEFFF785D0F70FE000DBBA00006" +
                "04040094029243554549000000017FFF00000000000C7E5058534101FFE85F5F" +
                "5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5345474D" +
                "30325F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F50524F4730320000005F5F5F5F" +
                "5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F52454E41554C545F5F5F5F" +
                "5F5F5F5F5F5F5F5F5F5F5032040270010C02260127300101F772D552");

        // base64 encoded splice descriptor
        String spliceDescriptor = "ApJDVUVJAAAAAX//AAAAAAAMflBYU0EB/+hfX19fX19fX19fX19fX19fX19fX19f" +
                "X19fX19fX19TRUdNMDJfX19fX19fX19fX19fX19fX1BST0cwMgAAAF9fX19fX19f" +
                "X19fX19fX19fX19fX19fX19SRU5BVUxUX19fX19fX19fX19fX19QMgQCcAEMAiYB" +
                "JzABAQ==";

        Scte35Info info = new Scte35Info(bytes.reset());
        Assert.assertEquals(252, info.get(Scte35Info.Field.table_id, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.section_syntax_indicator, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.private_indicator, 0));
        Assert.assertEquals(185, info.get(Scte35Info.Field.section_length, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.protocol_version, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.encrypted_packet, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.encryption_algorithm, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.pts_adjustment, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.cw_index, 0));
        Assert.assertEquals(4095, info.get(Scte35Info.Field.tier, 0));
        Assert.assertEquals(20, info.get(Scte35Info.Field.splice_command_length, 0));
        Assert.assertEquals(5, info.get(Scte35Info.Field.splice_command_type, 0));
        Assert.assertEquals(148, info.get(Scte35Info.Field.descriptor_loop_length, 0));
        Assert.assertEquals(6314332016L, info.get(Scte35Info.Field.pts_time, 0));
        Assert.assertEquals(4789, info.get(Scte35Info.Field.splice_event_id, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.splice_event_cancel_indicator, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.out_of_network_indicator, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.program_splice_flag, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.duration_flag, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.splice_immediate_flag, 0));
        Assert.assertEquals(6, info.get(Scte35Info.Field.unique_program_id, 0));
        Assert.assertEquals(4, info.get(Scte35Info.Field.avail_num, 0));
        Assert.assertEquals(4, info.get(Scte35Info.Field.avails_expected, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.auto_return, 0));
        Assert.assertEquals(900000, info.get(Scte35Info.Field.duration, 0));
        Assert.assertEquals(2, info.get(Scte35Info.Field.splice_descriptor_tag, 0));
        Assert.assertEquals(146, info.get(Scte35Info.Field.splice_descriptor_length, 0));
        Assert.assertEquals("CUEI", info.get(Scte35Info.Field.splice_identifier));
        Assert.assertEquals(spliceDescriptor, info.get(Scte35Info.Field.splice_descriptor));
        Assert.assertEquals(1, info.get(Scte35Info.Field.segmentation_event_id, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.segmentation_event_cancel_indicator, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.delivery_not_restricted_flag, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.segmentation_duration, 0));
        Assert.assertEquals(12, info.get(Scte35Info.Field.segmentation_upid_type, 0));
        Assert.assertEquals(126, info.get(Scte35Info.Field.segmentation_upid_length, 0));
        Assert.assertEquals(48, info.get(Scte35Info.Field.segmentation_type_id, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.segment_num, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.segments_expected, 0));
        Assert.assertEquals("PXSA", info.get(Scte35Info.Field.format_identifier));
        Assert.assertEquals(1, info.get(Scte35Info.Field.private_upid_version, 0));
        Assert.assertEquals("______________________________SEGM02", info.get(Scte35Info.Field.private_segment_id));
        Assert.assertEquals("_________________PROG02", info.get(Scte35Info.Field.private_program_id));
        Assert.assertEquals("0000", info.get(Scte35Info.Field.private_cni));
        Assert.assertEquals(0, info.get(Scte35Info.Field.private_ad_count_position, 0));
        Assert.assertEquals("_________________________RENAULT", info.get(Scte35Info.Field.private_brand));
        Assert.assertEquals("______________P2", info.get(Scte35Info.Field.private_pod_id));
        Assert.assertEquals(4, info.get(Scte35Info.Field.private_sectors_count, 0));
        Assert.assertEquals(4, info.count(Scte35Info.Field.private_sectorial));
        Assert.assertEquals("FFE8", info.get(Scte35Info.Field.type_of_replacement));
        Assert.assertEquals(1, info.get(Scte35Info.Field.transmission_inside_break, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.transmission_is_live, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.perform_replacement_in_cpvr, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.perform_replacement_in_npvr, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.perform_replacement_in_replay, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.perform_replacement_in_live, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.replacement_is_multiple, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.replacement_is_regional, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.replacement_is_brand4brand, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.replacement_language_fr_nl, 0));
        Assert.assertEquals(-143469230L, info.get(Scte35Info.Field.crc_32, 0));
        Assert.assertEquals(7, info.count(Scte35Info.Field.reserved));
    }

    @Test
    public void testParser_xFF10() {
        BitStream bytes = new BitStream("FC30B300000000000000FFF01405000012B77FEFFF7901DAF0FE0014C3A00008" +
                "0101008E028C43554549000000007FFF00001499700C785058534101FF105F5F" +
                "5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5345474D" +
                "30325F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F50524F4730320000005F5F5F5F" +
                "5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F52454E41554C545F5F5F5F" +
                "5F5F5F5F5F5F5F5F5F5F50320102703001015197957B");

        // base64 encoded splice descriptor
        String spliceDescriptor = "AoxDVUVJAAAAAH//AAAUmXAMeFBYU0EB/xBfX19fX19fX19fX19fX19fX19fX19f" +
                "X19fX19fX19TRUdNMDJfX19fX19fX19fX19fX19fX1BST0cwMgAAAF9fX19fX19f" +
                "X19fX19fX19fX19fX19fX19SRU5BVUxUX19fX19fX19fX19fX19QMgECcDABAQ==";

        Scte35Info info = new Scte35Info(bytes.reset());
        Assert.assertEquals(252, info.get(Scte35Info.Field.table_id, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.section_syntax_indicator, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.private_indicator, 0));
        Assert.assertEquals(179, info.get(Scte35Info.Field.section_length, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.protocol_version, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.encrypted_packet, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.encryption_algorithm, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.pts_adjustment, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.cw_index, 0));
        Assert.assertEquals(4095, info.get(Scte35Info.Field.tier, 0));
        Assert.assertEquals(20, info.get(Scte35Info.Field.splice_command_length, 0));
        Assert.assertEquals(5, info.get(Scte35Info.Field.splice_command_type, 0));
        Assert.assertEquals(142, info.get(Scte35Info.Field.descriptor_loop_length, 0));
        Assert.assertEquals(6325132016L, info.get(Scte35Info.Field.pts_time, 0));
        Assert.assertEquals(4791, info.get(Scte35Info.Field.splice_event_id, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.splice_event_cancel_indicator, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.out_of_network_indicator, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.program_splice_flag, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.duration_flag, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.splice_immediate_flag, 0));
        Assert.assertEquals(8, info.get(Scte35Info.Field.unique_program_id, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.avail_num, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.avails_expected, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.auto_return, 0));
        Assert.assertEquals(1360800, info.get(Scte35Info.Field.duration, 0));
        Assert.assertEquals(2, info.get(Scte35Info.Field.splice_descriptor_tag, 0));
        Assert.assertEquals(140, info.get(Scte35Info.Field.splice_descriptor_length, 0));
        Assert.assertEquals("CUEI", info.get(Scte35Info.Field.splice_identifier));
        Assert.assertEquals(spliceDescriptor, info.get(Scte35Info.Field.splice_descriptor));
        Assert.assertEquals(0, info.get(Scte35Info.Field.segmentation_event_id, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.segmentation_event_cancel_indicator, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.delivery_not_restricted_flag, 0));
        Assert.assertEquals(1350000, info.get(Scte35Info.Field.segmentation_duration, 0));
        Assert.assertEquals(12, info.get(Scte35Info.Field.segmentation_upid_type, 0));
        Assert.assertEquals(120, info.get(Scte35Info.Field.segmentation_upid_length, 0));
        Assert.assertEquals(48, info.get(Scte35Info.Field.segmentation_type_id, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.segment_num, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.segments_expected, 0));
        Assert.assertEquals("PXSA", info.get(Scte35Info.Field.format_identifier));
        Assert.assertEquals(1, info.get(Scte35Info.Field.private_upid_version, 0));
        Assert.assertEquals("______________________________SEGM02", info.get(Scte35Info.Field.private_segment_id));
        Assert.assertEquals("_________________PROG02", info.get(Scte35Info.Field.private_program_id));
        Assert.assertEquals("0000", info.get(Scte35Info.Field.private_cni));
        Assert.assertEquals(0, info.get(Scte35Info.Field.private_ad_count_position, 0));
        Assert.assertEquals("_________________________RENAULT", info.get(Scte35Info.Field.private_brand));
        Assert.assertEquals("______________P2", info.get(Scte35Info.Field.private_pod_id));
        Assert.assertEquals(1, info.get(Scte35Info.Field.private_sectors_count, 0));
        Assert.assertEquals(1, info.count(Scte35Info.Field.private_sectorial));
        Assert.assertEquals("FF10", info.get(Scte35Info.Field.type_of_replacement));
        Assert.assertEquals(1, info.get(Scte35Info.Field.transmission_inside_break, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.transmission_is_live, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.perform_replacement_in_cpvr, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.perform_replacement_in_npvr, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.perform_replacement_in_replay, 0));
        Assert.assertEquals(1, info.get(Scte35Info.Field.perform_replacement_in_live, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.replacement_is_multiple, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.replacement_is_regional, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.replacement_is_brand4brand, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.replacement_language_fr_nl, 0));
        Assert.assertEquals(1368888699L, info.get(Scte35Info.Field.crc_32, 0));
        Assert.assertEquals(7, info.count(Scte35Info.Field.reserved));
    }

    @Test
    public void testParser_empty() {
        BitStream bytes = new BitStream("FC301100000000000000FFF0000000007A4FBFFF");
        Scte35Info info = new Scte35Info(bytes.reset());
        Assert.assertEquals(252, info.get(Scte35Info.Field.table_id, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.section_syntax_indicator, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.private_indicator, 0));
        Assert.assertEquals(17, info.get(Scte35Info.Field.section_length, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.protocol_version, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.encrypted_packet, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.encryption_algorithm, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.pts_adjustment, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.cw_index, 0));
        Assert.assertEquals(4095, info.get(Scte35Info.Field.tier, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.splice_command_length, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.splice_command_type, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.descriptor_loop_length, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.pts_time, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.splice_event_id, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.splice_event_cancel_indicator, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.out_of_network_indicator, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.program_splice_flag, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.duration_flag, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.splice_immediate_flag, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.unique_program_id, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.avail_num, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.avails_expected, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.auto_return, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.duration, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.splice_descriptor_tag, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.splice_descriptor_length, 0));
        Assert.assertNull(info.get(Scte35Info.Field.splice_identifier));
        Assert.assertEquals(0, info.get(Scte35Info.Field.segmentation_event_id, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.segmentation_event_cancel_indicator, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.delivery_not_restricted_flag, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.segmentation_duration, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.segmentation_upid_type, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.segmentation_upid_length, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.segmentation_type_id, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.segment_num, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.segments_expected, 0));
        Assert.assertNull(info.get(Scte35Info.Field.format_identifier));
        Assert.assertEquals(0, info.get(Scte35Info.Field.private_upid_version, 0));
        Assert.assertNull(info.get(Scte35Info.Field.private_segment_id));
        Assert.assertNull(info.get(Scte35Info.Field.private_program_id));
        Assert.assertNull(info.get(Scte35Info.Field.private_cni));
        Assert.assertEquals(0, info.get(Scte35Info.Field.private_ad_count_position, 0));
        Assert.assertNull(info.get(Scte35Info.Field.private_brand));
        Assert.assertNull(info.get(Scte35Info.Field.private_pod_id));
        Assert.assertEquals(0, info.get(Scte35Info.Field.private_sectors_count, 0));
        Assert.assertEquals(0, info.count(Scte35Info.Field.private_sectorial));
        Assert.assertNull(info.get(Scte35Info.Field.type_of_replacement));
        Assert.assertEquals(0, info.get(Scte35Info.Field.transmission_inside_break, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.transmission_is_live, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.perform_replacement_in_cpvr, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.perform_replacement_in_npvr, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.perform_replacement_in_replay, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.perform_replacement_in_live, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.replacement_is_multiple, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.replacement_is_regional, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.replacement_is_brand4brand, 0));
        Assert.assertEquals(0, info.get(Scte35Info.Field.replacement_language_fr_nl, 0));
        Assert.assertEquals(2052046847L, info.get(Scte35Info.Field.crc_32, 0));
        Assert.assertEquals(1, info.count(Scte35Info.Field.reserved));
    }
}
