package tv.threess.threeready.data.proximus.tv.model.advertisement;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

/**
 * A <Linear> element has two required child elements, the <Duration> and the <MediaFiles> element.
 * Additionally, four optional child elements are offered: <VideoClicks>, <AdParameters>, <TrackingEvents> and <Icons>.
 */
public class VastLinear {
    @XmlElement(name = "Duration")
    private String mDuration;

    @XmlElementWrapper(name = "MediaFiles")
    @XmlElement(name = "MediaFile")
    private List<VastMediaFile> mMediaFiles;

    @XmlElementWrapper(name = "TrackingEvents")
    @XmlElement(name = "Tracking")
    private List<VastTracking> mTrackingEvents;

    @XmlElementWrapper(name = "VideoClicks")
    @XmlElement(name = "ClickTracking")
    private List<VastIdCdata> mVideoClicks;

    public String getDuration() {
        return mDuration;
    }

    public List<VastMediaFile> getMediaFiles() {
        return mMediaFiles;
    }

    public List<VastTracking> getTrackingEvents() {
        return mTrackingEvents;
    }

    public List<VastIdCdata> getVideoClicks() {
        return mVideoClicks;
    }
}
