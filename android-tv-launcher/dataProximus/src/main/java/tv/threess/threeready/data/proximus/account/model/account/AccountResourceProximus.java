package tv.threess.threeready.data.proximus.account.model.account;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.data.proximus.account.model.account.adapter.AccountResourceKeyAdapterProximus;

/**
 * Represent a resource linked to an account.
 * E.g. the maximum bandwidth available on the account.
 *
 * @author Barabas Attila
 * @since 2020.04.06
 */
@XmlRootElement(name = "Resource")
public class AccountResourceProximus implements Serializable {

    @XmlJavaTypeAdapter(AccountResourceKeyAdapterProximus.class)
    @XmlAttribute(name = "name")
    private Key mKey;

    @XmlAttribute(name = "amount")
    private int mAmount;

    /**
     * @return The unique name of the resource.
     */
    public Key getKey() {
        return mKey;
    }

    /**
     * @return Numeric value of the resouce.
     */
    public int getAmount() {
        return mAmount;
    }


    /**
     * Known keys by the application.
     */
    public enum Key {
        NetworkBandwidth,
        AvailableOverheadBandwidth
    }
}
