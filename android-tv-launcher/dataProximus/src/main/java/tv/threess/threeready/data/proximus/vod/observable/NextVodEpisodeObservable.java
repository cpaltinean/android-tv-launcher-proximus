package tv.threess.threeready.data.proximus.vod.observable;

import android.content.Context;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.BingeWatchingInfo;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IBaseVodSeries;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.model.VodSeriesDetailModel;
import tv.threess.threeready.data.vod.VodCategoryCache;
import tv.threess.threeready.data.vod.VodContract;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Rx observable to return the next vod episode from a series.
 *
 * @author Karetka Mezei Zoltan
 * @since 2020.05.18
 */
public class NextVodEpisodeObservable extends BaseVodObservable<BingeWatchingInfo<IVodSeries, IVodItem>> {
    private static final String TAG = Log.tag(NextVodEpisodeObservable.class);

    private final VodCategoryCache mVodCategoryCache = Components.get(VodCategoryCache.class);
    private final IVodItem mVod;
    private IBaseVodSeries mVodSeries;

    private BingeWatchingInfo<IVodSeries, IVodItem> mBingeWatchingInfo;
    private CopyOnWriteArrayList<IBaseVodItem> mEpisodes = new CopyOnWriteArrayList<>();

    public NextVodEpisodeObservable(Context context, IVodItem vod, IVodSeries vodSeries) {
        super(context);
        mVod = vod;
        mVodSeries = vodSeries;
    }

    @Override
    public void subscribe(ObservableEmitter<BingeWatchingInfo<IVodSeries, IVodItem>> e) throws Exception {
        super.subscribe(e);

        if (mVodSeries == null) {
            mVodSeries = mVodCategoryCache.getVodCategorySeries(mVod.getSeriesId());
        }

        publishNextEpisode();
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<BingeWatchingInfo<IVodSeries, IVodItem>> emitter) throws Exception {
        if (uri.getEncodedPath() != null && VodContract.RentalExpiration.CONTENT_URI.getEncodedPath() != null
                && uri.getEncodedPath().startsWith(VodContract.RentalExpiration.CONTENT_URI.getEncodedPath())) {

            String id = uri.getLastPathSegment();
            IBaseVodItem episode = getEpisodeByVariantId(id);
            if (episode == null) {
                // Not in the list.
                return;
            }

            if (mBingeWatchingInfo.getNextEpisode() != null && !Objects.equals(mBingeWatchingInfo.getNextEpisode().getId(), episode.getId())) {
                Log.d(TAG, "The changed vod episode is not the next one.");
                return;
            }

            Log.d(TAG, "The next vod is changed: " + episode.getTitle());

            mBingeWatchingInfo = new BingeWatchingInfo<>(mBingeWatchingInfo.getSeries(), getCompleteVod(episode));
            emitter.onNext(mBingeWatchingInfo);
        }
    }

    private IBaseVodItem getEpisodeByVariantId(String variantId) {
        for (IBaseVodItem episode : ArrayUtils.notNull(mEpisodes)) {
            if (episode.isVariantIncluded(variantId)) {
                return episode;
            }
        }
        return null;
    }

    private void publishNextEpisode() throws Exception {
        if (RxUtils.isDisposed(mEmitter)) {
            // Already canceled.
            return;
        }

        if (mVod == null) {
            throw new Exception("No episode available.");
        }

        if (mVodSeries == null) {
            throw new Exception("No series available with id: " + mVod.getSeriesId());
        }

        List<IBaseVodItem> episodes = new ArrayList<>();
        for (String seasonId : mVodSeries.getSeasonIds()) {
            episodes.addAll(mVodProxy.getEpisodes(mVod.getSeriesId(), seasonId, mVodCache.getRentalExpiration()));
        }

        IVodSeries series = new VodSeriesDetailModel(mVodSeries, episodes, true, null);
        if (series.getNumberOfEpisodes() < 2) {
            Log.d(TAG, "Series should have at least 2 episodes.");
            mEmitter.onComplete();
            return;
        }

        List<IBaseVodItem> episodeList = series.getEpisodesSorted();

        // Subscribe for rental changes in episodes.
        for (IBaseVodItem vod : episodeList) {
            registerObserver(VodContract.buildPurchaseUrisForVod(vod));
        }

        IBaseVodItem nextEpisode = null;
        for (int i = 0; i < episodeList.size() - 1; ++i) {
            if (mVod.getId().equals(episodeList.get(i).getId())) {
                nextEpisode = episodeList.get(i + 1);
                break;
            }
        }

        if (nextEpisode == null) {
            Log.d(TAG, "There is no next episode after episode with the following id: " + mVod.getId());
            mEmitter.onComplete();
            return;
        }

        mBingeWatchingInfo = new BingeWatchingInfo<>(series, getCompleteVod(nextEpisode));

        mEpisodes = new CopyOnWriteArrayList<>(episodeList);
        mEmitter.onNext(mBingeWatchingInfo);
    }
}
