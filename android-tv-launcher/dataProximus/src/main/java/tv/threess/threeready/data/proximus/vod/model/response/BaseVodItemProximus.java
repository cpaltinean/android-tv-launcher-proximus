package tv.threess.threeready.data.proximus.vod.model.response;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.vod.model.CurrencyType;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.data.vod.model.BaseVodItem;

/**
 * Proximus' implementation of the category item.
 * Used to store info for vod assets.
 *
 * @author Andor Lukacs
 * @since 10/2/18
 */
public class BaseVodItemProximus extends BaseVodItem<VodVariantProximus> {

    protected final List<String> mVariantIds;
    protected final Map<String, InvoicePrice> mVariantPrices;

    private IVodPrice mCheapestPrice;

    public BaseVodItemProximus(VodVariantProximus mainVariant, List<String> variantIds, @NonNull Map<String, InvoicePrice> priceList) {
        super(mainVariant);
        mVariantIds = variantIds;
        mVariantPrices = priceList;
    }

    @Override
    public String getDescription() {
        return mMainVariant.getDescription();
    }

    @Override
    public Integer getSeasonNumber() {
        return mMainVariant.getSeasonNumber();
    }

    @Override
    public Integer getEpisodeNumber() {
        return mMainVariant.getEpisodeNumber();
    }

    @Override
    public String getEpisodeTitle() {
        return mMainVariant.getEpisodeTitle();
    }

    @Override
    public String getReleaseYear() {
        return mMainVariant.getReleaseYear();
    }

    @Override
    public long getDuration() {
        return mMainVariant.getDuration();
    }

    @Override
    public String getSeriesId() {
        return mMainVariant.getSeriesId();
    }

    @Override
    public List<String> getActors() {
        return mMainVariant.getActors();
    }

    @Override
    public List<String> getDirectors() {
        return mMainVariant.getDirectors();
    }

    @Override
    public boolean isHD() {
        return mMainVariant.isHD();
    }

    @Override
    public IVodPrice getCheapestPrice() {
        return mCheapestPrice;
    }

    @Override
    public boolean isLastChance() {
        return mMainVariant.getLicensePeriodEnd() <= (System.currentTimeMillis() + TimeUnit.DAYS.toMillis(LAST_CHANCE_DAYS));
    }

    @Override
    public boolean isNew() {
        return mMainVariant.isNew();
    }

    @Override
    public List<String> getGenres() {
        return mMainVariant.getGenres();
    }

    @Override
    public String getId() {
        return TextUtils.join(",", getVariantIds());
    }

    @Override
    public String getTitle() {
        return mMainVariant.getTitle();
    }

    @Override
    public ParentalRating getParentalRating() {
        return mMainVariant.getParentalRating();
    }

    @Override
    public List<IImageSource> getImageSources() {
        return mMainVariant.getImageSources();
    }

    @Override
    public boolean isTVod() {
        return mMainVariant.isTVod();
    }

    @Override
    public boolean isRentable() {
        return mMainVariant.isRentable();
    }

    @Override
    public long getRentalPeriodEnd() {
        return mRentExpiration;
    }

    @Override
    public String getItemTitle() {
        return mMainVariant.getItemTitle();
    }

    @Override
    public String getGroupTitle() {
        return mMainVariant.getGroupTitle();
    }

    public String getGroupId() {
        return mMainVariant.getGroupId();
    }

    @Override
    public String getSeriesTitle() {
        return mMainVariant.getSeriesTitle();
    }

    @Override
    public boolean isSVod() {
        return mMainVariant.isSubscribed();
    }

    @Override
    public boolean isSubscribed() {
        return mMainVariant.isSubscribed();
    }

    @Override
    public boolean isRented() {
        return mRentExpiration > System.currentTimeMillis();
    }

    @Override
    public boolean isFree() {
        return mMainVariant.isFree();
    }

    @Override
    public List<String> getVariantIds() {
        return mVariantIds;
    }

    @Override
    public String getSuperGenre() {
        return mMainVariant.getSuperGenre();
    }

    @Override
    public int getInternalId() {
        return mMainVariant.getInternalId();
    }

    @Override
    public IVodVariant getMainVariant() {
        return mMainVariant;
    }

    @Override
    public boolean hasDescriptiveAudio() {
        return mMainVariant.hasDescriptiveAudio();
    }

    @Override
    public boolean hasDescriptiveSubtitle() {
        return mMainVariant.hasDescriptiveSubtitle();
    }

    public void updateCheapestPrice(double creditPriceLimit, int freeRentals) {
        RentOfferProximus rentOffer = mMainVariant.getRentOffer();
        InvoicePrice price = mVariantPrices.get(mMainVariant.getId());
        if (rentOffer != null) {
            rentOffer.updatePriceOptions(price, rentOffer.getRentalCost() < creditPriceLimit && freeRentals > 0);

            if (rentOffer.canPurchaseForCredit(freeRentals)) {
                mCheapestPrice = rentOffer.getPriceForCurrency(CurrencyType.Credit);
                return;
            }
        }

        mCheapestPrice = new VodPriceProximus(getCheapestPriceForEuro(), CurrencyType.Euro, price != null ? price.getPriceDetermination() : null);
    }

    private double getCheapestPriceForEuro() {
        double cheapestPrice = mMainVariant.getRentOffer() == null ? Float.MAX_VALUE :
                mMainVariant.getRentOffer().getPriceForCurrency(CurrencyType.Euro).getCurrencyAmount();
        for (InvoicePrice price: mVariantPrices.values()) {
            if (price != null && price.getPrice() != null && price.getPrice() < cheapestPrice) {
                cheapestPrice = price.getPrice();
            }
        }
        return cheapestPrice;
    }

    public void setSeriesId(String seriesId) {
        mMainVariant.setSeriesId(seriesId);
    }

    /**
     * Generate VOD id from the variants.
     */
    public static String getVodId(List<String> variantIds) {
        if (variantIds == null) {
            return "";
        }
        return TextUtils.join(",", variantIds);
    }

    /**
     * Generate variant ids from the vod id.
     */
    public static List<String> getVariantIds(String vodId) {
        return Arrays.asList(TextUtils.split(vodId, ","));
    }

    @Override
    public String toString() {
        return "VodProximus{" +
                "Id=" + getVodId(mVariantIds) +
                ", MainVariant=" + mMainVariant +
                '}';
    }
}
