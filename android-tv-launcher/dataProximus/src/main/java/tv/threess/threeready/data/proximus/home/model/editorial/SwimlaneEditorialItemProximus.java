package tv.threess.threeready.data.proximus.home.model.editorial;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemStyle;
import tv.threess.threeready.data.proximus.home.model.DisplayNamesProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Class represents and editorial item in a swimlane.
 *
 * @author Barabas Attila
 * @since 5/24/21
 */
public abstract class SwimlaneEditorialItemProximus implements EditorialItem {
    private final SwimLaneProximus mSwimlane;
    private final SwimLaneProximus.SwimlaneTile mSwimlaneTile;
    private final Integer mPosition;

    private final EditorialItemStyle mStyle = new EditorialItemStyle() {
        private final Size mWidth = new VariantSize(VariantSize.Variant.ViewAll);
        private final Size mHeight = new AdaptiveSize();

        @Nullable
        @Override
        public IImageSource getBackgroundImage() {
            return mSwimlaneTile.getBackgroundImage();
        }

        @Nullable
        @Override
        public Size getWidth() {
            return mWidth;
        }

        @Nullable
        @Override
        public Size getHeight() {
            return mHeight;
        }
    };

    @Override
    public String getId() {
        return mSwimlane.getId() + "." + mSwimlaneTile.getAction();
    }


    public SwimlaneEditorialItemProximus(SwimLaneProximus swimLane,
                                         SwimLaneProximus.SwimlaneTile swimlaneTile, Integer position) {
        mSwimlane = swimLane;
        mSwimlaneTile = swimlaneTile;
        mPosition = position;
    }

    @Nullable
    @Override
    public Integer getPosition() {
        return mPosition;
    }

    @Override
    public Integer getMinItemCount() {
        return mSwimlaneTile.getMinCoreTile();
    }

    @Override
    public String getTitle() {
        DisplayNamesProximus displayNames = mSwimlaneTile.getDisplayName();
        String title = displayNames.getLocalizedNameOrFirst();
        if (TextUtils.isEmpty(title)) {
            return TranslationKey.SEE_ALL;
        }
        return title;
    }

    @Nullable
    @Override
    public EditorialItemStyle getStyle() {
        return mStyle;
    }

    @Nullable
    @Override
    public abstract EditorialItemAction getAction();
}
