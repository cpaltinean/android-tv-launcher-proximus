package tv.threess.threeready.data.proximus.log.model;

import android.text.TextUtils;

import java.util.LinkedHashMap;

/**
 * Wrapper class used for sending events to the data collection API.
 *
 * @author Barabas Attila
 * @since 2020.03.17
 */
public class LogDetailsProximus extends LinkedHashMap<String, Object> {
    public void add(String key, String value) {
        if (!TextUtils.isEmpty(value))
            put(key, value);
    }

    public void add(String key, long value) {
        put(key, value);
    }

    public void add(String key, double value) {
        put(key, value);
    }

    public void add(String key, boolean value) {
        put(key, value);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("LogDetailsProximus {");
        for (String key : keySet()) {
            result.append("\n\t").append(key).append(": ").append(get(key));
        }
        result.append("\n}");

        return result.toString();
    }
}
