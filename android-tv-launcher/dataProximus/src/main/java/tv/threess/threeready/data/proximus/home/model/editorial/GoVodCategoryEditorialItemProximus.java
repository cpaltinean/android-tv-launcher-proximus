package tv.threess.threeready.data.proximus.home.model.editorial;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Editorial tile which opens VOD category browsing for a given category.
 *
 * @author Barabas Attila
 * @since 5/24/21
 */
public class GoVodCategoryEditorialItemProximus extends SwimlaneEditorialItemProximus {

    private final String mCategoryId;

    public GoVodCategoryEditorialItemProximus(SwimLaneProximus swimLane,
                                              SwimLaneProximus.SwimlaneTile swimlaneTile, Integer position) {
        super(swimLane, swimlaneTile, position);
        mCategoryId = swimlaneTile.getCategoryId();
    }

    @Nullable
    @Override
    public EditorialItemAction getAction() {
        return mAction;
    }

    private final EditorialItemAction mAction = new EditorialItemAction.CategoryEditorialItemAction() {

        @Nullable
        @Override
        public String getCategoryId() {
            return mCategoryId;
        }
    };
}
