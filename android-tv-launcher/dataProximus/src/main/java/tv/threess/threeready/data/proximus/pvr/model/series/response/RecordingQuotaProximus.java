package tv.threess.threeready.data.proximus.pvr.model.series.response;

import java.util.concurrent.TimeUnit;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import tv.threess.threeready.api.pvr.model.IRecordingQuota;
import tv.threess.threeready.data.proximus.pvr.model.series.adapter.RetentionTypeAdapterProximus;

/**
 * Proximus Recording Quota entity used for {@link RecordingQuotaResponseProximus}.
 *
 * Created by Daniela Toma on 04/22/2020.
 */
@XmlRootElement(name = "RecordingQuota")
public class RecordingQuotaProximus implements IRecordingQuota {

    @XmlJavaTypeAdapter(RetentionTypeAdapterProximus.class)
    @XmlElement(name = "RetentionType")
    private RetentionTypesProximus mRetentionType;

    @XmlElement(name = "QuotaHours")
    private int mQuotaHours;

    @XmlElement(name = "StrictlyEnforced")
    private boolean mStrictlyEnforced;

    @XmlElement(name = "AlarmingActive")
    private boolean mAlarmingActive;

    @XmlElement(name = "RecordedMinutes")
    private long mRecordedMinutes;

    @XmlElement(name = "UsedQuotaPercentage")
    private int mUsedQuotaPercentage;

    @XmlElement(name = "CleanupStartDate")
    private long mCleanupStartDate;

    @XmlElement(name = "WarningThreshold")
    private int mWarningThreshold;

    public RetentionTypesProximus getRetentionType() {
        return mRetentionType;
    }

    public int getQuotaHours() {
        return  mQuotaHours;
    }

    public boolean isStrictlyEnforced() {
        return mStrictlyEnforced;
    }

    public boolean isAlarmingActive() {
        return mAlarmingActive;
    }

    public long getRecordedMinutes() {
        return mRecordedMinutes;
    }

    public int getUsedQuotaPercentage() {
        return mUsedQuotaPercentage;
    }

    public long getCleanupStartDate() {
        return mCleanupStartDate;
    }

    public int getWarningThreshold() {
        return mWarningThreshold;
    }

    @Override
    public long getQuota() {
        return TimeUnit.HOURS.toMillis(mQuotaHours);
    }

    @Override
    public long getUsedQuota() {
        return TimeUnit.HOURS.toMillis(mQuotaHours) * mUsedQuotaPercentage / 100;
    }

    @Override
    public long getCleanupDate() {
        return mCleanupStartDate;
    }
}
