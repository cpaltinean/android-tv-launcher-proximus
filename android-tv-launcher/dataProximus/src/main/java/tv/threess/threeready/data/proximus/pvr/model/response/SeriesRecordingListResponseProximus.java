package tv.threess.threeready.data.proximus.pvr.model.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * Proximus BTA response entity for Series Recording List.
 *
 * Created by Daniela Toma on 04/16/2020.
 */
@XmlRootElement(name = "BTAResponse")
public class SeriesRecordingListResponseProximus extends BTAResponseProximus{

    @XmlElementWrapper(name = "SeriesList")
    @XmlElement(name = "Series")
    private List<SeriesRecordingSchedule> mSeriesList;

    public List<SeriesRecordingSchedule> getSeriesList() {
        return mSeriesList;
    }
}
