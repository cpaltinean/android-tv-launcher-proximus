package tv.threess.threeready.data.proximus.account.model.boot;

import javax.xml.bind.annotation.XmlElement;

/**
 * POJO containing the IGMP rejoin parameters.
 *
 * @author Andor Lukacs
 * @since 2020-07-24
 */
public class IGMPRejoinProximus {

    @XmlElement(name = "lowBandwidth")
    private int lowBandwidth;

    @XmlElement(name = "thresholdTime")
    private int thresholdTime;

    public int getLowBandwidth() {
        return lowBandwidth;
    }

    public int getThresholdTime() {
        return thresholdTime;
    }
}
