package tv.threess.threeready.data.proximus.playback.adapter;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import tv.threess.threeready.api.log.Log;

import java.io.IOException;

/**
 * XMl type adapter to read the bucket time as UTC timestamp, without failing when parsing invalid numbers.
 * E.g. 020-07-14T12:30:00Z
 *
 * @author Karetka Mezei Zoltan
 * @since 2021.05.27
 */
public class BucketTimeSafeTypeAdapter extends BucketTimeTypeAdapter {
    private static final String TAG = Log.tag(BucketTimeSafeTypeAdapter.class);

    @Override
    public void write(JsonWriter out, Long value) {
        throw new IllegalStateException("Not implemented.");
    }

    @Override
    public Long read(JsonReader in) throws IOException {
        try {
            return super.read(in);
        } catch (NumberFormatException e) {
            Log.w(TAG, "Failed to parse timestamp", e);
            return INVALID_TIMESTAMP;
        }
    }
}
