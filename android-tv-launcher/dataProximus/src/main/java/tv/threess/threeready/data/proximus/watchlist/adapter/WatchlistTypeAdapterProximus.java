package tv.threess.threeready.data.proximus.watchlist.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import tv.threess.threeready.api.generic.model.WatchlistType;

/**
 * Json type adapter to read value as watchlist type enum.
 *
 * @author Barabas Attila
 * @since 5/16/21
 */
public class WatchlistTypeAdapterProximus extends TypeAdapter<WatchlistType> {
    private static final Map<WatchlistType, String> sTypeNameMap = new HashMap<WatchlistType, String>() {
        {
            put(WatchlistType.Broadcast, "PROGRAM");
            put(WatchlistType.Vod, "VOD");
        }
    };
    private static final Map<String, WatchlistType> sNameTypeMap = sTypeNameMap
            .entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));

    @Override
    public void write(JsonWriter out, WatchlistType value) throws IOException {
        out.value(sTypeNameMap.get(value));
    }

    @Override
    public WatchlistType read(JsonReader in) throws IOException {
        WatchlistType type = sNameTypeMap.get(in.nextString());
        if (type == null) {
            return WatchlistType.Broadcast;
        }
        return type;
    }
}
