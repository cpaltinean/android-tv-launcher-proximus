package tv.threess.threeready.data.proximus.pvr.model.response;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;
import tv.threess.threeready.data.proximus.pvr.model.RecordingProximus;
import tv.threess.threeready.data.proximus.tv.adapter.EPGKeyIndexMapAdapter;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.MetaInfoProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.ProgramMetaInfoKeyProximus;

/**
 * Retrofit response class for schedule series recordings.
 *
 * Created by Noemi Dali on 21.04.2020.
 */
@XmlRootElement(name = "BTAResponse")
public class RecordingScheduleSeriesResponse extends BTAResponseProximus {

    @XmlElement(name = "Series")
    private Series mSeries;

    @XmlElement(name = "Meta")
    private Meta mMeta;

    private boolean mMetaInfoGenerated;

    public String getSeriesRecordingId() {
        return mSeries.mSeriesId;
    }

    public List<RecordingProximus> getRecordings() throws IOException {
        if (!mMetaInfoGenerated) {
            for (RecordingProximus recordingProximus: mSeries.mRecordingEpisodes) {
                BufferedReader programMetaInfoReader = new BufferedReader(
                        new StringReader(recordingProximus.getProgramMetaInfo()));

                String metaInfoString;
                while ((metaInfoString = programMetaInfoReader.readLine()) != null) {
                    String[] programMetaInfoArray = metaInfoString.split(MetaInfoProximus.META_INFO_SEPARATOR);
                    MetaInfoProximus<ProgramMetaInfoKeyProximus> programMetaInfo = new MetaInfoProximus<>(
                            mMeta.mProgram, programMetaInfoArray);
                    recordingProximus.setMetaInfo(programMetaInfo);
                }
            }
            mMetaInfoGenerated = true;
        }

        return mSeries.mRecordingEpisodes;
    }

    @XmlRootElement(name = "Meta")
    private static class Meta {
        @XmlJavaTypeAdapter(EPGKeyIndexMapAdapter.class)
        @XmlElement(name = "Program")
        private Map<String, Integer> mProgram;
    }

    @XmlRootElement(name = "Series")
    private static class Series {
        @XmlElement(name = "userSeriesId")
        private String mSeriesId;

        @XmlElementWrapper(name = "Episodes")
        @XmlElement(name = "Recording")
        private List<RecordingProximus> mRecordingEpisodes;
    }
}
