package tv.threess.threeready.data.proximus.vod.model.response;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.generic.model.VideoQuality;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.data.proximus.generic.adapter.ParentalRatingAdapterProximus;
import tv.threess.threeready.data.proximus.generic.model.ImageSourceProximus;
import tv.threess.threeready.data.vod.model.BaseVodVariantModel;

/**
 * Proximus' implementation of the Vod variant item.
 *
 * @author Andor Lukacs
 * @since 11/8/18
 */
@XmlRootElement(name = "Movie")
public class VodVariantProximus extends BaseVodVariantModel {

    private final char POINT_CHARACTER = '.';

    @XmlElement(name = "ExternalID")
    private String mExternalId;

    @XmlElement(name = "ID")
    private int mId;

    @XmlElement(name = "Title")
    private String mItemTitle;

    @XmlElement(name = "GroupTitle")
    private String mGroupTitle;

    @XmlElement(name = "longDescription")
    private String mDescription;

    @XmlElement(name = "ReleaseYear")
    private String mReleaseYear;

    @XmlElement(name = "Runtime")
    private int mRuntime;

    @XmlElement(name = "MovieBG")
    private String mBackground;

    @XmlElement(name = "JPEG")
    private String mPoster;

    @XmlElement(name = "RentalItems")
    private RentalItems mRentalItem;

    @XmlElement(name = "HDContent")
    private boolean mIsHd;

    @XmlElement(name = "AudioLanguage")
    private List<String> mAudioLanguages;

    @XmlElement(name = "SubTitleLanguage")
    private List<String> mSubtitleLanguages;

    @XmlElement(name = "GroupID")
    private String mGroupId;

    @XmlElement(name = "Genre")
    private String mGenre;

    @XmlElement(name = "Supergenre")
    private String mSuperGenre;

    @XmlElement(name = "SeriesTitle")
    private String mSeriesTitle;

    @XmlElement(name = "RatingCode")
    @XmlJavaTypeAdapter(ParentalRatingAdapterProximus.class)
    private ParentalRating mParentalRating;

    @XmlElement(name = "SeasonNumber")
    private Integer mSeasonNumber;

    @XmlElement(name = "EpisodeNumber")
    private Integer mEpisodeNumber;

    @XmlElementWrapper(name = "Actors")
    @XmlElement(name = "Actor")
    private List<String> mActors;

    @XmlElementWrapper(name = "Directors")
    @XmlElement(name = "Director")
    private List<String> mDirectors;

    @XmlElement(name = "LicenseEndDate")
    private long mLicencePeriodEnd;

    @XmlElement(name = "isChargeable")
    private boolean mChargable;

    @XmlElement(name = "PreviewURL")
    private String mPreviewUrl;

    @XmlElement(name = "isNewRelease")
    private boolean mIsNew;

    @XmlElement(name = "SupportForVisuallyImpaired")
    private boolean mHasSupportForVisuallyImpaired;

    @XmlElement(name= "SupportForHearingImpaired")
    private boolean mHasSupportForHearingImpaired;

    private String mSeriesId;

    private List<IImageSource> mPosterImageSources;

    @Override
    public String getId() {
        return mExternalId;
    }

    @Override
    public String getTitle() {
        String title = getEpisodeTitle();
        if (!TextUtils.isEmpty(title)) {
            return title;
        }

        return mSeriesTitle;
    }

    @Override
    public String getItemTitle() {
        if (!TextUtils.isEmpty(mItemTitle) && POINT_CHARACTER == mItemTitle.charAt(0)) {
            return mItemTitle.substring(1);
        }

        return mItemTitle;
    }

    @Override
    public String getGroupTitle() {
        if (!TextUtils.isEmpty(mGroupTitle) && POINT_CHARACTER == mGroupTitle.charAt(0)) {
            return mGroupTitle.substring(1);
        }

        return mGroupTitle;
    }

    @Override
    public String getGroupId() {
        return mGroupId;
    }

    @Override
    public String getDescription() {
        return mDescription;
    }

    public String getSeriesTitle() {
        return mSeriesTitle;
    }

    @Override
    public Integer getSeasonNumber() {
        return mSeasonNumber;
    }

    @Override
    public Integer getEpisodeNumber() {
        return mEpisodeNumber;
    }

    @Override
    public String getEpisodeTitle() {
        if (TextUtils.isEmpty(mGroupTitle)) {
            return getItemTitle();
        }
        return getGroupTitle();
    }

    @Override
    public String getSeriesId() {
        return mSeriesId;
    }


    @Override
    public List<String> getGenres() {
        return mGenre == null ? Collections.emptyList() : Collections.singletonList(mGenre);
    }

    @Override
    public String getReleaseYear() {
        return mReleaseYear;
    }

    @Override
    public long getDuration() {
        return TimeUnit.MINUTES.toMillis(mRuntime);
    }

    @Override
    public ParentalRating getParentalRating() {
        return mParentalRating;
    }

    @Override
    public boolean isHD() {
        return mIsHd;
    }

    @Override
    public boolean isNew() {
        return mIsNew;
    }

    @Override
    public boolean hasDescriptiveAudio() {
        return mHasSupportForVisuallyImpaired;
    }

    @Override
    public boolean hasDescriptiveSubtitle() {
        return mHasSupportForHearingImpaired;
    }

    @Override
    public List<String> getAudioLanguages() {
        return mAudioLanguages == null ? Collections.emptyList() : mAudioLanguages;
    }

    @Override
    public List<String> getSubtitleLanguages() {
        return mSubtitleLanguages == null ? Collections.emptyList() : mSubtitleLanguages;
    }

    @Override
    public List<String> getDirectors() {
        return mDirectors == null ? Collections.emptyList() : mDirectors;
    }

    @Override
    public List<String> getActors() {
        return mActors == null ? Collections.emptyList() : mActors;
    }

    @Override
    public boolean hasTrailer() {
        return !TextUtils.isEmpty(mPreviewUrl);
    }

    @Override
    public boolean isTVod() {
        return !isSubscribed();
    }

    @Override
    public boolean isSVod() {
        return isSubscribed();
    }

    @Override
    public boolean isRentable() {
        if (isFree() || isRented()) {
            return false;
        }
        RentOfferProximus offer = mRentalItem.mRentOffer;
        return offer != null && offer.getOfferStart() < System.currentTimeMillis()
                && offer.getOfferEnd() > System.currentTimeMillis();
    }

    @Override
    public boolean isSubscribed() {
        IRentOffer rentOffer = getRentOffer();
        return !mChargable || (rentOffer != null && rentOffer.isFree());
    }

    @Override
    public boolean isFree() {
        IRentOffer rentOffer = getRentOffer();
        return rentOffer != null && rentOffer.isFree();
    }

    @Override
    public VideoQuality getHighestQuality() {
        return mIsHd ? VideoQuality.HD : VideoQuality.SD;
    }

    @Override
    public long getLicensePeriodEnd() {
        return mLicencePeriodEnd;
    }

    @Override
    public String getSuperGenre() {
        return mSuperGenre;
    }

    @Override
    public int getInternalId() {
        return mId;
    }

    @Override
    public List<IImageSource> getImageSources() {
        if (mPosterImageSources == null) {
            mPosterImageSources = new ArrayList<>();
            mPosterImageSources.add(new ImageSourceProximus(mPoster, IImageSource.Type.COVER));
            mPosterImageSources.add(new ImageSourceProximus(mBackground, IImageSource.Type.BACKGROUND));
        }

        return mPosterImageSources;
    }

    @Nullable
    public RentOfferProximus getRentOffer() {
        return mRentalItem != null ? mRentalItem.mRentOffer : null;
    }

    public void setSeriesId(String seriesId) {
        mSeriesId = seriesId;
    }

    @XmlRootElement(name = "RentalItems" )
    private static class RentalItems implements Serializable {

        @XmlElement(name = "RentalItem")
        private RentOfferProximus mRentOffer;
    }

    @Override
    public String toString() {
        return "VodVariantProximus{" +
                "Id=" + mExternalId +
                ", Title='" + mItemTitle + '\'' +
                ", SeriesId='" + mSeriesId + '\'' +
                ", SeriesTitle='" + mSeriesTitle + '\'' +
                ", SeasonNumber=" + mSeasonNumber +
                ", EpisodeNumber=" + mEpisodeNumber +
                ", ReleaseYear='" + mReleaseYear + '\'' +
                '}';
    }
}
