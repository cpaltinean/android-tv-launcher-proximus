package tv.threess.threeready.data.proximus.account.model.account;

import androidx.annotation.Nullable;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * Proximus Account info response entity.
 *
 * Created by Noemi Dali on 26.03.2020.
 */
@XmlRootElement(name = "BTAResponse")
public class AccountInfoResponseProximus extends BTAResponseProximus {

    @XmlElement(name = "AccountInfo")
    private AccountInfoProximus mAccountInfo;

    @XmlElementWrapper(name = "DeviceResources")
    @XmlElement(name = "DeviceResource")
    private List<DeviceResourceProximus> mDeviceResources;

    public AccountInfoProximus getAccountInfo() {
        return mAccountInfo;
    }

    @Nullable
    public DeviceResourceProximus getDeviceResource(String macAddress) {
        for (DeviceResourceProximus deviceResource : mDeviceResources) {
            if (macAddress.equalsIgnoreCase(deviceResource.getMacAddress())) {
                return deviceResource;
            }
        }
        return null;
    }
}
