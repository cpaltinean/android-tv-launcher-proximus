package tv.threess.threeready.data.proximus.home.model.selector;

import androidx.annotation.Nullable;

import java.util.List;

import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneQueryParamProximus;

/**
 * Wrap the swimlane static tile and data source sort/filter selector.
 *
 * @author Barabas Attila
 * @since 9/4/21
 */
public class SwimlaneTileSelector implements SwimlaneSelector {

    private final SwimLaneProximus mSwimlane;
    private final SwimLaneProximus.StaticTile mTile;

    public SwimlaneTileSelector(SwimLaneProximus swimlane, SwimLaneProximus.StaticTile tile) {
        mSwimlane = swimlane;
        mTile = tile;
    }

    @Override
    public String getName() {
        return mTile.getDisplayNames().getLocalizedNameOrFirst();
    }

    @Override
    public String getReportName() {
        return getName();
    }

    @Override
    public String getReportReference() {
        return mSwimlane.getItemQueryName();
    }

    @Override
    public boolean isDefault() {
        return mTile.isDefault();
    }

    @Nullable
    @Override
    public String getTokenName() {
        return mSwimlane.getItemQueryName();
    }

    @Nullable
    @Override
    public String getTokenValue() {
        return mTile.getId();
    }

    @Override
    public List<SwimLaneQueryParamProximus> getParameters() {
        return mTile.getParameters();
    }
}
