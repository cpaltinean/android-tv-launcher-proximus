package tv.threess.threeready.data.proximus.generic.model;

import tv.threess.threeready.data.generic.model.BaseResponse;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Generic BTA backend response.
 * Holds the results and the backend errors.
 *
 * @author Barabas Attila
 * @since 2020.03.23
 */
@XmlRootElement(name = "BTAResponse")
public class BTAResponseProximus implements BaseResponse {

    @XmlElement(name = "ERROR")
    BTAResponseErrorProximus mError;

    public BTAResponseErrorProximus getError() {
        return mError;
    }
}
