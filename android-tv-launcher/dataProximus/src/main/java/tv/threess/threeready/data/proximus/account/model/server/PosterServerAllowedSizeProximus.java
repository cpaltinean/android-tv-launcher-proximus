package tv.threess.threeready.data.proximus.account.model.server;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.account.model.server.IPosterServerInfoField;

/**
 * Proximus implementation for IPosterServerAllowedSize interface which holds the information related to allowed images.
 *
 * Created by Noemi Dali on 01.04.2020.
 */
@XmlRootElement(name = "AllowedSize")
public class PosterServerAllowedSizeProximus implements IPosterServerInfoField {

    @XmlElement(name = "Width")
    private Float mWidth;

    @XmlElement(name = "Height")
    private Float mHeight;

    @XmlElement(name = "URI")
    private String mUri;

    @Override
    public Float getWidth() {
        return mWidth;
    }

    @Override
    public Float getHeight() {
        return mHeight;
    }

    @Override
    public String getUri() {
        return mUri;
    }

    @Override
    public boolean isResizable() {
        return false;
    }
}
