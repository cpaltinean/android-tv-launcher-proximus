package tv.threess.threeready.data.proximus.vod.observable;

import android.content.Context;

import java.util.List;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.data.account.AccountLocalCache;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.generic.observable.BaseBookmarkObservable;

/**
 * RX observable that reacts when the {@link ConfigContract.Bookmarks} content's uri is notified
 * in order to query for a VOD's bookmark that holds its duration & lastViewedPosition
 *
 * @author Denisa Trif
 * @since 01/16/2019
 */
public class VodBookmarkObservable extends BaseBookmarkObservable<IBaseVodItem> {

    private final AccountLocalCache mAccountCache = Components.get(AccountLocalCache.class);

    public VodBookmarkObservable(Context context, IBaseVodItem vod) {
        super(context, vod);
    }

    @Override
    public void subscribe(ObservableEmitter<IBookmark> e) throws Exception {
        super.subscribe(e);
        // register observers for each VodVariant of Vod
        List<String> variantIds = mContentItem.getVariantIds();
        if (variantIds != null) {
            for (String id : variantIds) {
                registerObserver(ConfigContract.buildUriForBookmark(id));
            }
        }
    }

    @Override
    protected IBookmark getBookmark() {
        return mAccountCache.getVodBookmark(mContentItem);
    }
}
