package tv.threess.threeready.data.proximus.generic.interceptor;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Invocation;
import tv.threess.threeready.api.generic.helper.FileUtils;

/**
 * OkHttp interceptor to retry a marked call after 500 error receiver from server.
 * The retrofit method needs to be marked with {@link RetryOnError} to enable the retry.
 *
 * @author Barabas Attila
 * @since 1/12/21
 */
public class RetryOnErrorOkHttpInterceptor implements Interceptor {

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request request = chain.request();

        // Calculate max retry count.
        int maxCallCount = 1;
        Invocation invocation = chain.request().tag(Invocation.class);
        if (invocation != null) {
            RetryOnError retryOnError = invocation.method().getAnnotation(RetryOnError.class);
            if (retryOnError != null) {
                maxCallCount += retryOnError.value();
            }
        }

        // Try until success or max retry.
        Response response = null;
        for (int i = 0; i < maxCallCount && retryNeeded(response); ++i) {
            FileUtils.closeSafe(response);
            response = chain.proceed(request);
        }

        return response;
    }

    /**
     * @return True in case the response is considered as error and retry needed.
     */
    private boolean retryNeeded(Response response) {
        return response == null || response.code() == 500;
    }

    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface RetryOnError {
        // Number of retries.
        int value() default 1;
    }
}
