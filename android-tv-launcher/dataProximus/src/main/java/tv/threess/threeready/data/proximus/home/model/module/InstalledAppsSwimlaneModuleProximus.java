package tv.threess.threeready.data.proximus.home.model.module;

import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.List;

import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceParams;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceService;
import tv.threess.threeready.api.home.model.module.ModuleDataType;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.api.home.model.module.ModuleType;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Module which displays all the installed applications.
 *
 * @author Barabas Attila
 * @since 6/21/21
 */
public class InstalledAppsSwimlaneModuleProximus extends SwimlaneModuleConfigProximus {
    private final ModuleDataSourceParams mParams;

    public InstalledAppsSwimlaneModuleProximus(SwimLaneProximus swimlane) {
        super(swimlane);
        mParams = new InstalledAppModuleParams(swimlane);
    }

    @Nullable
    @Override
    public String getTitle() {
        return TranslationKey.MODULE_ANDROID_APPS;
    }

    @Override
    public ModuleType getType() {
        return ModuleType.STRIPE;
    }

    @Override
    public ModuleDataType getDataType() {
        return ModuleDataType.APPS;
    }

    @Override
    public ModuleCardVariant getCardVariant() {
        return ModuleCardVariant.B1;
    }

    @Nullable
    @Override
    public ModuleDataSource getDataSource() {
        return mDataSource;
    }

    private final ModuleDataSource mDataSource = new ModuleDataSource() {

        @Override
        public ModuleDataSourceService getService() {
            return ModuleDataSourceService.GMS;
        }

        @Override
        public ModuleDataSourceMethod getMethod() {
            return ModuleDataSourceMethod.ANDROID_TV_APPS;
        }

        @Nullable
        @Override
        public ModuleDataSourceParams getParams() {
            return mParams;
        }
    };

    private static class InstalledAppModuleParams extends SwimlaneModuleDataSourceParams {
        public InstalledAppModuleParams(SwimLaneProximus swimlane) {
            super(swimlane);
        }

        @Nullable
        @Override
        public List<String> getFilterList(String key) {
            switch (key) {
                default:
                    return null;
                case ModuleFilterOption.App.Type.NAME:
                    return Collections.singletonList(ModuleFilterOption.App.Type.Value.INSTALLED);
            }
        }
    }
}
