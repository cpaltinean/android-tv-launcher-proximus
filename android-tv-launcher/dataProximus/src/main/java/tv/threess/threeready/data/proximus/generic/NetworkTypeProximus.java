package tv.threess.threeready.data.proximus.generic;

/**
 * The type of the CC client profile configuration.
 *
 * @author Barabas Attila
 * @since 2019.03.06
 */
public enum NetworkTypeProximus {
    // Login via line number over the managed network.
    ManagedNetwork,
    // Login via username and password over the internet.
    Internet
}
