package tv.threess.threeready.data.proximus.mw;

import tv.threess.threeready.api.generic.receivers.WaitTask;

public class EmptyScanTask extends WaitTask<Boolean> {

    @Override
    public boolean isFinished() {
        return true;
    }

    @Override
    protected Boolean result() {
        return Boolean.FALSE;
    }

    @Override
    public Boolean waitForResult(long timeoutMillis) {
        return Boolean.FALSE;
    }
}
