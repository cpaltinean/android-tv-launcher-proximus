package tv.threess.threeready.data.proximus.home.model.editorial;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Class represents an editorial item in a swimlane which will open a replay channel content wall.
 *
 * @author David Bondor
 * @since 4.28.2022
 */
public class GoPageEditorialItemProximus extends SwimlaneEditorialItemProximus {

    private final PageConfig mPageConfig;
    private final EditorialItemAction mAction = new EditorialItemAction.PageEditorialItemAction() {

        @Override
        public PageConfig getPage() {
            return mPageConfig;
        }
    };

    public GoPageEditorialItemProximus(PageConfig pageConfig, SwimLaneProximus swimLane, SwimLaneProximus.SwimlaneTile swimlaneTile, Integer position) {
        super(swimLane, swimlaneTile, position);
        mPageConfig = pageConfig;
    }

    @Nullable
    @Override
    public EditorialItemAction getAction() {
        return mAction;
    }
}