/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */


package tv.threess.threeready.data.proximus.generic;

import android.content.Context;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeoutException;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.generic.BackendIsolationException;
import tv.threess.threeready.data.generic.helper.URIBasedInternetChecker;
import tv.threess.threeready.data.proximus.account.AccountRepositoryProximus;
import tv.threess.threeready.data.proximus.generic.model.BTAErrorCodesProximus;

/**
 * Class for checking the internet in the application for Proximus.
 * This is to be used in any location where we need internet status.
 *
 * @author Andor Lukacs
 * @since 6/18/18
 */
public class InternetCheckerProximus extends URIBasedInternetChecker {
    private static final String TAG = Log.tag(InternetCheckerProximus.class);

    private static final String PING_IP = "8.8.8.8";

    public InternetCheckerProximus(Context context) {
        super(context);
    }

    @Override
    public boolean checkInternetAvailability() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Log.d(TAG, "Start ping " + PING_IP);
            Process pingProcess = runtime.exec("/system/bin/ping -c 1 -W 2 " + PING_IP);
            int status = pingProcess.waitFor();
            Log.d(TAG, "Ping " + PING_IP + " status " + status);
            return status == 0;
        } catch (InterruptedException | IOException pingException) {
            Log.d(TAG, "Unable to ping server: " +  PING_IP, pingException);
        }
        return false;
    }

    @Override
    @Nullable
    public Exception checkBackendAvailability() {
        AccountRepositoryProximus configHandler = Components.get(AccountRepositoryProximus.class);
        try {
            /* First check the operator override flag to determine if operator decided for STB to boot in “isolated mode”
               If the flag is True, the operator has decided to override, so the STB must boot in “Isolated Mode”. */
            if (configHandler.isOperatorOverriding()) {
                return new BackendIsolationException();
            }
        } catch (Exception e) {
            Log.i(TAG, "Could not check backend availability", e);
        }

        /* Secondly, perform an isAlive check to ascertain if BEP is UP or DOWN. */
        Exception lastException = null;
        for (int i = 0; i < 3; ++i) {
            try {
                return configHandler.isAlive() ? null : new BackendIsolationException();
            } catch (Exception e) {
                lastException = e;
            }
        }

        // Boot in Isolated mode if IsAlive call failed 3 times
        return lastException;
    }

    @Override
    protected boolean shouldTriggerInternetCheck(Exception exception) {
        if (exception instanceof UnknownHostException) {
            return true;
        }

        if (exception instanceof SocketTimeoutException) {
            return true;
        }

        if (exception instanceof TimeoutException) {
            return true;
        }

        if (exception instanceof BackendException) {
            switch (((BackendException) exception).getErrorCode()) {
                case BTAErrorCodesProximus.HTTP_FORBIDDEN:
                case BTAErrorCodesProximus.HTTP_NOT_FOUND:
                case BTAErrorCodesProximus.HTTP_REQUEST_TIMEOUT:
                case BTAErrorCodesProximus.HTTP_INTERNAL_SERVER_ERROR:
                case BTAErrorCodesProximus.SYSTEM_ERROR:
                case BTAErrorCodesProximus.COMMUNICATION_ERROR_IN_INTERNAL_DEPENDENT_SYSTEMS:
                case BTAErrorCodesProximus.REMOTE_COMMUNICATION_ERROR:
                case BTAErrorCodesProximus.UNDEFINED_ERROR:
                case BTAErrorCodesProximus.UUSD_NOT_REACHABLE:
                case BTAErrorCodesProximus.TM_JMS_QUEUE_PROBLEM:
                case BTAErrorCodesProximus.HTTP_INVALID_RESPONSE:
                case BTAErrorCodesProximus.SYSTEM_ERROR_18:
                case BTAErrorCodesProximus.SYSTEM_ERROR_19:
                    return true;
            }
        }
        return false;
    }
}
