package tv.threess.threeready.data.proximus.home.model.menu;

import tv.threess.threeready.api.home.model.menu.MenuItem;
import tv.threess.threeready.data.proximus.home.model.AccessPointProximus;

/**
 * Model represents an access point as menu item on the UI.
 *
 * @author Barabas Attila
 * @since 5/19/21
 */
public class AccessPointMenuItemProximus implements MenuItem {

    private final AccessPointProximus mAccessPoint;

    public AccessPointMenuItemProximus(AccessPointProximus accessPoint) {
        mAccessPoint = accessPoint;
    }

    @Override
    public String getReportName() {
        return mAccessPoint.getName();
    }

    @Override
    public String getReportReference() {
        return mAccessPoint.getId();
    }

    @Override
    public String getTitle() {
        return mAccessPoint.getDisplayNames().getLocalizedNameOrFirst();
    }

    @Override
    public String getId() {
        return mAccessPoint.getId();
    }
}
