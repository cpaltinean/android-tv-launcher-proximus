package tv.threess.threeready.data.proximus.account.model.account;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Body used for updating STB properties
 *
 * @author Solyom Zsolt
 * @since 2020.07.08
 */
@XmlRootElement(name = "BTA")
public class STBPropertiesBodyProximus implements Serializable {

    @XmlElement(name = "SetSTBPropertiesDoc")
    private final STBPropertiesDoc mStbPropertiesDoc = new STBPropertiesDoc();

    public STBPropertiesBodyProximus(STBProperties propertyName, String propertyValue) {
        addProperty(new STBPropertiesDoc.STBProperty(propertyName, propertyValue));
    }

    @XmlRootElement(name = "SetSTBPropertiesDoc")
    public static class STBPropertiesDoc implements Serializable {

        @XmlElement(name = "Property")
        @XmlElementWrapper(name = "Properties")
        private final List<STBProperty> mSTBProperties = new ArrayList<>();

        @XmlRootElement(name = "Property")
        public static class STBProperty implements Serializable {

            @XmlElement(name = "Name")
            private final String mName;

            @XmlElement(name = "Value")
            private final String mValue;

            public STBProperty(STBProperties property, String value) {
                mName = property.getPropertyName();
                mValue = value;
            }
        }
    }

    private void addProperty(STBPropertiesDoc.STBProperty property) {
        mStbPropertiesDoc.mSTBProperties.add(property);
    }

}