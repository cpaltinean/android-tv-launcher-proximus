/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.playback;

import java.io.IOException;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.playback.StreamingSessionRepository;
import tv.threess.threeready.api.playback.model.IOttStreamingSession;
import tv.threess.threeready.api.playback.model.IRecordingOttStreamingSession;
import tv.threess.threeready.api.playback.model.IReplayOttStreamingSession;
import tv.threess.threeready.api.playback.model.IVodOttStreamingSession;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;

/**
 * Proximus Extension methods that handles the session specific actions.
 *
 * @author Barabas Attila
 * @since 2018.06.22
 */
public class StreamingSessionRepositoryProximus implements StreamingSessionRepository {

    private final StreamingSessionProxyProximus mStreamingSessionProxy = Components.get(StreamingSessionProxyProximus.class);

    @Override
    public IOttStreamingSession openChannelStreamingSession(String channelId) throws IOException {
        return mStreamingSessionProxy.openChannelStreamingSession(channelId);
    }

    @Override
    public IReplayOttStreamingSession openReplayStreamingSession(IBroadcast broadcast) throws IOException {
        return mStreamingSessionProxy.openReplayStreamingSession(broadcast);
    }

    @Override
    public IReplayOttStreamingSession openPreviousReplayStreamingSession(IReplayOttStreamingSession currentSession) throws IOException {
        return mStreamingSessionProxy.openPreviousReplayStreamingSession(currentSession);
    }

    @Override
    public IReplayOttStreamingSession openNextReplayStreamingSession(IReplayOttStreamingSession currentSession) throws IOException {
        return mStreamingSessionProxy.openNextReplayStreamingSession(currentSession);
    }

    @Override
    public IRecordingOttStreamingSession openRecordingStreamingSession(IRecording recording) throws IOException {
        return mStreamingSessionProxy.openRecordingStreamingSession(recording);
    }

    @Override
    public IVodOttStreamingSession openVodStreamingSession(IVodItem vod, IVodVariant vodVariant) throws IOException {
        return mStreamingSessionProxy.openVodStreamingSession(vod, vodVariant);
    }

    @Override
    public IOttStreamingSession openTrailerStreamingSession(IVodItem vod, IVodVariant vodVariant) throws IOException {
        return mStreamingSessionProxy.openTrailerStreamingSession(vod, vodVariant);
    }

    @Override
    public void closeStreamingSession(IOttStreamingSession streamingSession) {
        mStreamingSessionProxy.closeStreamingSession(streamingSession);
    }
}
