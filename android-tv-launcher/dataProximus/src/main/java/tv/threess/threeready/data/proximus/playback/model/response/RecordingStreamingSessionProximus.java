package tv.threess.threeready.data.proximus.playback.model.response;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import tv.threess.threeready.api.playback.model.IRecordingOttStreamingSession;
import tv.threess.threeready.api.pvr.model.IRecording;

/**
 * OTT streaming session for Recording content.
 * Contains the necessary data to start the playback and the recording.
 *
 * @author Barabas Attila
 * @since 2020.05.13
 */
public class RecordingStreamingSessionProximus extends BTAStreamingSessionProximus implements IRecordingOttStreamingSession {

    @JacksonXmlProperty(localName = "userRecordingStartDeltaMs")
    private long userStartDelta;

    @JacksonXmlProperty(localName = "userRecordingStopDeltaMs")
    private long userStopDelta;

    @JacksonXmlProperty(localName = "assetStartDeltaMs")
    private long assetStartDelta;

    private IRecording mRecording;

    public void setRecording(IRecording recording) {
        mRecording = recording;
    }

    @Override
    public long getStartTime() {
        return mRecording.getRecordingStart() - assetStartDelta - userStartDelta;
    }

    @Override
    public long getEndTime() {
        return mRecording.getRecordingEnd();
    }
}
