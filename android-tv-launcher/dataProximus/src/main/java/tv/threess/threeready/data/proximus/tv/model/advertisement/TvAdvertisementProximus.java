package tv.threess.threeready.data.proximus.tv.model.advertisement;

import java.util.concurrent.TimeUnit;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.data.tv.model.TvAdvertisement;
import tv.threess.threeready.data.proximus.tv.adapter.AdDurationTypeAdapter;
import tv.threess.threeready.data.proximus.tv.adapter.AdTimestampTypeAdapter;

public class TvAdvertisementProximus implements TvAdvertisement {
    @XmlElement(name = "creativeId")
    private String mCreativeId;

    @XmlElement(name = "name")
    private String mName;

    @XmlElement(name = "broadcaster")
    private String mBroadcaster;

    @XmlElement(name = "filename")
    private String mFilename;

    @XmlElement(name = "size")
    private long mSize;

    @XmlJavaTypeAdapter(AdDurationTypeAdapter.class)
    @XmlElement(name = "duration")
    private Long mDuration;

    @XmlJavaTypeAdapter(AdTimestampTypeAdapter.class)
    @XmlElement(name = "expires")
    private Long mExpires;

    @Override
    public String getCreativeId() {
        return mCreativeId;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public String getBroadcaster() {
        return mBroadcaster;
    }

    @Override
    public String getFilename() {
        return mFilename;
    }

    @Override
    public long getSize() {
        return mSize;
    }

    @Override
    public long getDuration(TimeUnit unit) {
        return unit.convert(mDuration, TimeUnit.MILLISECONDS);
    }

    @Override
    public int getAttemptCount() {
        return 0;
    }

    @Override
    public boolean isOnMarker() {
        return true;
    }

    @Override
    public long getExpires() {
        return mExpires;
    }

    @Override
    public String toString() {
        return "Advertisement{" +
                "mCreativeId='" + mCreativeId + '\'' +
                ", mName='" + mName + '\'' +
                ", mBroadcaster='" + mBroadcaster + '\'' +
                ", mFilename='" + mFilename + '\'' +
                ", mSize='" + mSize + '\'' +
                ", mDuration='" + mDuration + '\'' +
                ", mExpires='" + mExpires + '\'' +
                '}';
    }
}
