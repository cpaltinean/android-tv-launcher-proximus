package tv.threess.threeready.data.proximus.playback.model.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Proximus backend model to bucket based replay streamign session.
 *
 * @author Barabas Attila
 * @since 2020.07.06
 */
public class ReplayStreamingBucketRequestProximus implements Serializable {

    @SerializedName("bucketSpec")
    private final BucketSpec mBucketSpec;

    @SerializedName("videoQuality")
    private final VideoQuality mVideoQuality;

    @SerializedName("playout")
    private final Playout mPlayout;

    @SerializedName("mimeTypes")
    private final List<String> mimeTypes = Collections.singletonList("application/dash+xml");

    public ReplayStreamingBucketRequestProximus(String airingId,
                                                VideoQuality videoQuality, String deviceId) {
        mBucketSpec = new BucketSpec();
        mBucketSpec.mAiringId = airingId;
        mVideoQuality = videoQuality;
        mPlayout = new Playout(deviceId);
    }

    private static class BucketSpec {
        @SerializedName("airingId")
        private String mAiringId;
    }

    private static class Playout {
        @SerializedName("deviceId")
        private final String mDeviceId;

        public Playout(String deviceId) {
            mDeviceId = deviceId;
        }
    }

    public enum VideoQuality {
        @SerializedName("SD")
        SD,
        @SerializedName("HD")
        HD
    }
}
