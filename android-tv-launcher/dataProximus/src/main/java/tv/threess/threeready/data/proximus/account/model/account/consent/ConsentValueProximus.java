package tv.threess.threeready.data.proximus.account.model.account.consent;

/**
 * Enum with the possible values of a consent.
 *
 * @author Zsolt Bokor_
 * @since 2020.10.22
 */
public enum ConsentValueProximus {
    OPTIN,
    OPTOUT
}
