package tv.threess.threeready.data.proximus.watchlist.request;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import tv.threess.threeready.api.generic.model.WatchlistType;
import tv.threess.threeready.data.proximus.watchlist.adapter.WatchlistTypeAdapterProximus;

/**
 * Request POJO for add to watchlist api int he watchlist microservice
 *
 * @author Andor Lukacs
 * @since 21/10/2020
 */
public class AddToWatchlistRequestProximus {

    @SerializedName("item")
    WatchlistItem item;

    @SerializedName("channel_id")
    String channelId;

    public AddToWatchlistRequestProximus(WatchlistType type, String contentId) {
        item = new WatchlistItem(type, contentId);
    }

    public AddToWatchlistRequestProximus(WatchlistType type, String broadcastId, String channelId) {
        item = new WatchlistItem(type, broadcastId);
        this.channelId = channelId;
    }

    private static class WatchlistItem {

        @JsonAdapter(WatchlistTypeAdapterProximus.class)
        @SerializedName("item_type")
        WatchlistType type;

        @SerializedName("item_id")
        String contentId;

        public WatchlistItem(WatchlistType type, String contentId) {
            this.type = type;
            this.contentId = contentId;
        }
    }
}
