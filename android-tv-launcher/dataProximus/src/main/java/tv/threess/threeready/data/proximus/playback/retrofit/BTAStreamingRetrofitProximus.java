package tv.threess.threeready.data.proximus.playback.retrofit;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import tv.threess.threeready.data.proximus.generic.interceptor.RetryOnErrorOkHttpInterceptor.RetryOnError;
import tv.threess.threeready.data.proximus.playback.model.request.ChannelStreamingSessionRequestProximus;
import tv.threess.threeready.data.proximus.playback.model.request.RecordingStreamingSessionRequestProximus;
import tv.threess.threeready.data.proximus.playback.model.request.VodStreamingSessionRequestProximus;
import tv.threess.threeready.data.proximus.playback.model.response.BTAStreamingSessionProximus;
import tv.threess.threeready.data.proximus.playback.model.response.RecordingStreamingSessionProximus;
import tv.threess.threeready.data.proximus.playback.model.response.VodStreamingSessionProximus;

/**
 * Retrofit service to access proximus playback related data from backend.
 *
 * @author Barabas Attila
 * @since 2020.04.08
 */
public interface BTAStreamingRetrofitProximus {

    @RetryOnError
    @POST("/broker/bta/getURL")
    Call<BTAStreamingSessionProximus> getURL(@Body ChannelStreamingSessionRequestProximus request);

    @RetryOnError
    @POST("/broker/bta/getURL")
    Call<RecordingStreamingSessionProximus> getURL(@Body RecordingStreamingSessionRequestProximus request);

    @RetryOnError
    @POST("/broker/bta/getURL")
    Call<VodStreamingSessionProximus> getURL(@Body VodStreamingSessionRequestProximus request);

}
