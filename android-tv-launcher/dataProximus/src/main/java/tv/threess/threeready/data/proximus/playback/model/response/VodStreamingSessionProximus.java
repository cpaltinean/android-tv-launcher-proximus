package tv.threess.threeready.data.proximus.playback.model.response;

import tv.threess.threeready.api.playback.model.IVodOttStreamingSession;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;

/**
 * OTT streaming session for VOD content.
 * Contains the necessary data to start the data and the VOD.
 *
 * @author Barabas Attila
 * @since 2020.05.15
 */
public class VodStreamingSessionProximus extends BTAStreamingSessionProximus implements IVodOttStreamingSession {

    private IVodItem mVod;
    private IVodVariant mVariant;

    public void setVod(IVodItem vod) {
        mVod = vod;
    }

    public void setVariant(IVodVariant variant) {
        mVariant = variant;
    }

    @Override
    public long getStartTime() {
        return 0;
    }

    @Override
    public long getEndTime() {
        return mVariant.getDuration();
    }
}
