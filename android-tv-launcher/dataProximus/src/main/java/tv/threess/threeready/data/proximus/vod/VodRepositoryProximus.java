/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.proximus.vod;

import android.app.Application;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.BingeWatchingInfo;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.model.CurrencyType;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodRentalTransaction;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.api.vod.model.VodSeriesDetailModel;
import tv.threess.threeready.api.vod.wrapper.LanguageRentOffer;
import tv.threess.threeready.api.vod.wrapper.VodRentOffers;
import tv.threess.threeready.data.generic.observable.WaitForUpdateObservable;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodItemProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodRentalHistoryResponseProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodRentalResponse;
import tv.threess.threeready.data.proximus.vod.observable.LastPlayedVodObservable;
import tv.threess.threeready.data.proximus.vod.observable.NextVodEpisodeObservable;
import tv.threess.threeready.data.proximus.vod.observable.UserRentalsObservable;
import tv.threess.threeready.data.proximus.vod.observable.VodBookmarkObservable;
import tv.threess.threeready.data.proximus.vod.observable.VodLanguageRentOfferObservable;
import tv.threess.threeready.data.proximus.vod.observable.VodObservable;
import tv.threess.threeready.data.proximus.vod.observable.VodSeriesObservable;
import tv.threess.threeready.data.proximus.vod.observable.VodVariantRentOfferObservable;
import tv.threess.threeready.data.vod.BaseVodRepository;
import tv.threess.threeready.data.vod.VodCache;
import tv.threess.threeready.data.vod.observable.VodVariantBookmarkObservable;

/**
 * Implementation of the Proximus specific VOD access data methods.
 *
 * @author Barabas Attila
 * @since 2018.06.06
 */
public class VodRepositoryProximus extends BaseVodRepository implements VodRepository, Component {

    private final VodProxyProximus mVodProxy = Components.get(VodProxyProximus.class);
    private final VodCache mVodCache = Components.get(VodCache.class);

    public VodRepositoryProximus(Application application) {
        super(application);
    }

    @Override
    public void rentVodItem(IVodItem vod, IVodVariant vodVariant, IVodPrice price) throws IOException {
        rentVodItem(vod, vodVariant, price, null);
    }

    @Override
    public void rentVodItem(IVodItem vod, IVodVariant vodVariant, IVodPrice price, @Nullable String pin) throws IOException {
        boolean isFreeRental = false;
        float formattedPrice = 0f;
        if (price.getCurrencyType().equals(CurrencyType.Credit)) {
            isFreeRental = true;
        } else {
            formattedPrice = (float) price.getCurrencyAmount();
        }

        VodRentalResponse response = mVodProxy.rentVodItem(vodVariant.getId(), formattedPrice, pin, isFreeRental, price.getPriceDetermination());

        if (response.getExpirationTime() > System.currentTimeMillis()) {

            mVodCache.rentVodItem(vodVariant.getId(), response.getExpirationTime(), response.getRentalTime());

            if (response.isFreeRental()) {
                SettingsProximus.freeRentals.edit().put(response.getFreeRentalCount());
            }

            // Send a VodRental event
            UILog.logVodRental(vodVariant, price);
        }

        // Update rent information
        if (vod instanceof VodItemProximus) {
            ((VodItemProximus) vod).setRentalExpirationInfo(mVodCache.getRentalExpiration());
        }

    }

    @Override
    public Observable<IVodItem> getVod(String id) {
        return Observable.create(new VodObservable(mApp, id));
    }

    @Override
    public Observable<IVodItem> getVod(IBaseVodItem vod) {
        return Observable.create(new VodObservable(mApp, vod));
    }

    @Override
    public Observable<VodSeriesDetailModel> getVodSeries(String seriesId) {
        return Observable.create(new VodSeriesObservable(mApp, seriesId));
    }

    @Override
    public Observable<VodRentOffers> getVariantRentOffers(IBaseVodItem baseVod) {
        return Observable.create(new VodVariantRentOfferObservable(mApp, baseVod));
    }

    @Override
    public Observable<IBookmark> getBookmarkForVod(IBaseVodItem vod) {
        return Observable.create(new VodBookmarkObservable(mApp, vod));
    }

    @Override
    public Observable<IBookmark> getBookmarkForVod(IVodVariant vodVariant) {
        return Observable.create(new VodVariantBookmarkObservable(mApp, vodVariant));
    }

    @Override
    public Observable<VodRentOffers> getLastPlayedVodInSeries(IVodSeries series) {
        return Observable.create(new LastPlayedVodObservable(mApp, series));
    }

    @Override
    public Observable<BingeWatchingInfo<IVodSeries, IVodItem>> getNextEpisode(IVodItem series, IVodSeries vodSeries) {
        return Observable.create(new NextVodEpisodeObservable(mApp, series, vodSeries));
    }

    @Override
    public Observable<ModuleData<IBaseContentItem>> getUserRentals(ModuleConfig moduleConfig, int start, int count) {
        return Observable.create(new UserRentalsObservable(mApp, moduleConfig, start, count));
    }

    @Override
    public Observable<LanguageRentOffer> getLanguageRentOffers(IBaseVodItem baseVod) {
        return Observable.create(new VodLanguageRentOfferObservable(mApp, baseVod));
    }

    @Override
    public Single<List<IVodRentalTransaction>> getVodRentalTransactions() {
        return Single.create(emitter -> {
            VodRentalHistoryResponseProximus vodRentalHistoryResponseProximus = mVodProxy.getVodRentalHistory();
            emitter.onSuccess(new ArrayList<>(vodRentalHistoryResponseProximus.getRentalTransactionList()));
        });
    }

    @Override
    public Single<IVodItem> getCompleteVod(IBaseVodItem vod) {
        return Single.fromCallable(() -> mVodProxy.getCompleteVod(vod.getId(), vod.getSeriesId(), mVodCache.getRentalExpiration()));
    }

    @Override
    public Completable waitForVodSync() {
        return Completable.fromObservable(
                Observable.create(new WaitForUpdateObservable(mApp,
                        rentalUpdater, categoryUpdater)));
    }
}
