package tv.threess.threeready.data.proximus.search.retrofit.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.search.model.SearchSuggestion;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * Proximus retrofit response for search suggestions.
 *
 * Created by Noemi Dali on 06.05.2020.
 */
@XmlRootElement(name = "BTAResponse")
public class SearchSuggestionsResponse extends BTAResponseProximus {

    @XmlElement(name = "Suggestions")
    private Suggestions suggestionList;

    /**
     * @return a list of suggestions.
     */
    public List<SearchSuggestion> getSuggestionList() {
        List<SearchSuggestion> searchSuggestionList = new ArrayList<>();
        for (String suggestion : ArrayUtils.notNull(suggestionList.suggestion)) {
            searchSuggestionList.add(new SearchSuggestion(suggestion));
        }
        return searchSuggestionList;
    }

    @XmlRootElement(name = "Suggestions")
    private static class Suggestions {

        @XmlElement(name = "Suggestion")
        List<String> suggestion;
    }
}
