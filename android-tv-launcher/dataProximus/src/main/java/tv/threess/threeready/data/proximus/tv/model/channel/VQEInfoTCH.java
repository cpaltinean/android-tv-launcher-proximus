package tv.threess.threeready.data.proximus.tv.model.channel;

import java.util.Objects;

/**
 * Model containing all the required fields for the VQE client setup.
 */
public class VQEInfoTCH {

    private final String mChannelConfig;

    private final String mNetworkConfig;

    private final String mBurstConfig;

    public VQEInfoTCH(String channelConfig, String networkConfig, String burstConfig) {
        mChannelConfig = channelConfig;
        mNetworkConfig = networkConfig;
        mBurstConfig = burstConfig;
    }

    public String getNetworkConfig() {
        return mNetworkConfig;
    }

    public String getChannelConfig() {
        return mChannelConfig;
    }

    public String getBurstConfig() {
        return mBurstConfig;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mChannelConfig, mNetworkConfig, mBurstConfig);
    }
}
