package tv.threess.threeready.data.proximus.account.model.account;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class represent a resources assigned to a given device.
 *
 * @author Barabas Attila
 * @since 2020.04.06
 */
@XmlRootElement(name = "DeviceResource")
public class DeviceResourceProximus implements Serializable {

    @XmlElement(name = "DeviceRef")
    private DeviceReferenceProximus mDeviceReference;

    @XmlElement(name = "STBProfile")
    private STBProfile mSTBProfile;

    /**
     * @return registered MAC address of the device.
     */
    public String getMacAddress() {
        return mDeviceReference != null ? mDeviceReference.getRef() : "";
    }

    /**
     * @return True if the device can play HD content.
     */
    public boolean hasHdProfile() {
        return mSTBProfile != null && mSTBProfile.mHdReception;
    }

    public String getProfileName(){ return mSTBProfile.mProfileName; }

    @XmlRootElement(name = "STBProfile")
    private static class STBProfile {
        @XmlElement(name = "HDReception")
        private boolean mHdReception;

        @XmlElement(name = "DisplayName")
        private String mProfileName;
    }
}
