package tv.threess.threeready.data.proximus.watchlist.response;

import com.google.gson.annotations.SerializedName;
import tv.threess.threeready.data.generic.model.BaseResponse;

import java.util.List;

/**
 * Watchlist item list response proximus
 *
 * @author Andor Lukacs
 * @since 21/10/2020
 */
public class WatchlistItemsProximus implements BaseResponse {

    @SerializedName("watchlist_items")
    private List<GetWatchlistItemResponseProximus> watchlistItems;


    public List<GetWatchlistItemResponseProximus> getWatchlistItems() {
        return watchlistItems;
    }
}
