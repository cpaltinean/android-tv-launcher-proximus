package tv.threess.threeready.data.proximus.vod.model.response;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * Base BTA call for VOD related backend responses.
 *
 * @author Barabas Attila
 * @since 1/14/21
 */
public abstract class VodBaseResponseProximus extends BTAResponseProximus {

    @XmlElementWrapper(name = "Movies")
    @XmlElement(name = "Movie")
    protected ArrayList<VodVariantProximus> mVariants;

    @XmlElementWrapper(name = "Pricing")
    @XmlElement(name = "Price")
    protected List<Price> mPrices;

    protected List<VodItemProximus> getGroupedVodItems(List<String> vodIds, Map<String, InvoicePrice> variantPriceMap) {
        Map<String, VodVariantProximus> variantIdMap = getVariantMap();
        List<VodItemProximus> vodItems = new ArrayList<>();
        for (String id : vodIds) {
            List<String> variantIds = VodItemProximus.getVariantIds(id);
            List<VodVariantProximus> vodItemVariants = findVariants(
                    variantIds, variantIdMap);
            Map<String, InvoicePrice> prices = findVariantPrices(variantIds, variantPriceMap);
            if (!vodItemVariants.isEmpty()) {
                vodItems.add(new VodItemProximus(id, vodItemVariants, prices));
            }
        }
        return vodItems;
    }

    protected List<BaseVodItemProximus> getGroupedBaseVodItems(List<String> vodIds,
                                                               Map<String, InvoicePrice> variantPriceMap) {
        Map<String, VodVariantProximus> variantIdMap = getVariantMap();
        List<BaseVodItemProximus> vodItems = new ArrayList<>();
        for (String id : vodIds) {
            List<String> variantIds = VodItemProximus.getVariantIds(id);
            List<VodVariantProximus> vodItemVariants = findVariants(variantIds, variantIdMap);
            if (!vodItemVariants.isEmpty()) {
                VodVariantProximus mainVariant = vodItemVariants.get(0);
                Map<String, InvoicePrice> prices = findVariantPrices(variantIds, variantPriceMap);
                vodItems.add(new BaseVodItemProximus(mainVariant, variantIds, prices));
            }
        }
        return vodItems;
    }

    protected Map<String, InvoicePrice> getPriceMap() {
        Map<String, InvoicePrice> priceMap = new HashMap<>();
        if (mPrices != null && !mPrices.isEmpty()) {
            for (Price price: mPrices) {
                priceMap.put(price.movieRef.getReference(), price.invoicePrice);
            }
        }

        return priceMap;
    }

    protected Map<String, VodVariantProximus> getVariantMap() {
        Map<String, VodVariantProximus> variantIdMap = new HashMap<>();
        for (VodVariantProximus variant : mVariants) {
            variantIdMap.put(variant.getId(), variant);
        }
        return variantIdMap;
    }

    @NonNull
    private Map<String, InvoicePrice> findVariantPrices(List<String> variantIds, Map<String, InvoicePrice> priceMap) {
        Map<String, InvoicePrice> prices = new HashMap<>();
        for (String variantId : variantIds) {
            if (priceMap.containsKey(variantId)) {
                InvoicePrice price = priceMap.get(variantId);
                if (price != null) {
                    prices.put(variantId, price);
                }
            }
        }
        return prices;
    }

    private List<VodVariantProximus> findVariants(List<String> variantIds, Map<String, VodVariantProximus> variantIdMap) {
        List<VodVariantProximus> vodVariants = new ArrayList<>();
        for (String variantId : variantIds) {
            VodVariantProximus variant = variantIdMap.get(variantId);
            if (variant != null) {
                vodVariants.add(variant);
            }
        }
        return vodVariants;
    }

    @XmlRootElement(name = "MovieRef")
    protected static class MovieRef {

        @XmlAttribute(name = "ref")
        private String mRef;

        @XmlAttribute(name = "extref")
        private String mExtRef;

        public String getReference(){
            return mRef;
        }

        public String getExternalReference(){
            return mExtRef;
        }
    }

    @XmlRootElement(name = "Price")
    protected static class Price {

        @XmlElement(name = "MovieRef")
        protected VodCategoryContentResponseProximus.MovieRef movieRef;

        @XmlElement(name = "InvoicePrice")
        protected InvoicePrice invoicePrice;
    }

}
