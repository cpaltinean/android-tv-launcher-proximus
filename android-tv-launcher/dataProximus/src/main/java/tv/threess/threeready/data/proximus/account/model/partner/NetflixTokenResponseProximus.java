package tv.threess.threeready.data.proximus.account.model.partner;

import javax.xml.bind.annotation.XmlElement;

import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * Netflix authentication token response.
 *
 * @author Barabas Attila
 * @since 2020.05.11
 */
public class NetflixTokenResponseProximus extends BTAResponseProximus {

    @XmlElement(name = "Token")
    private String mToken;


    public String getToken() {
        return mToken;
    }
}
