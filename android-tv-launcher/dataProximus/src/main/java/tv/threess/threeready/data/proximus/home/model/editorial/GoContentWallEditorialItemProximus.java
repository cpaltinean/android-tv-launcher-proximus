package tv.threess.threeready.data.proximus.home.model.editorial;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.api.home.model.page.ModulePageConfig;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.data.proximus.home.model.module.ContentWallSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Class represents and editorial item in a swimlane which will open a content wall.
 *
 * @author Barabas Attila
 * @since 5/24/21
 */
public class GoContentWallEditorialItemProximus extends SwimlaneEditorialItemProximus {
    private final PageConfig mPageConfig;
    private final EditorialItemAction mAction = new EditorialItemAction.PageEditorialItemAction() {

        @Override
        public PageConfig getPage() {
            return mPageConfig;
        }
    };

    public GoContentWallEditorialItemProximus(SwimLaneProximus swimLane,
                                              SwimLaneProximus.SwimlaneTile swimlaneTile, Integer position) {
        super(swimLane, swimlaneTile, position);
        mPageConfig = new ModulePageConfig(new ContentWallSwimlaneModuleProximus(swimLane), UILog.Page.ContentWallFullContentPage);
    }

    @Nullable
    @Override
    public EditorialItemAction getAction() {
        return mAction;
    }
}
