package tv.threess.threeready.data.proximus.home.model.module;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceService;
import tv.threess.threeready.api.home.model.module.ModuleDataType;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.proximus.home.model.AccessPointProximus;
import tv.threess.threeready.data.proximus.home.model.editorial.GoPageEditorialItemProximus;
import tv.threess.threeready.data.proximus.home.model.page.AccessPointPageConfigProximus;
import tv.threess.threeready.data.proximus.home.model.page.ReplayChannelPageConfig;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneHubResponseProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Module config representing a replay channel swimlane.
 *
 * @author David Bondor
 * @since 4.28.2022
 */
public class ReplayChannelSwimlaneModuleProximus extends SwimlaneModuleConfigProximus implements ReplayChannelPageModuleProximus {

    private final SwimLaneHubResponseProximus mHub;
    private final AccessPointProximus mAccessPoint;

    public ReplayChannelSwimlaneModuleProximus(SwimLaneProximus swimLane, SwimLaneHubResponseProximus hub) {
        super(swimLane);
        mHub = hub;

        List<String> accessPointIds = mSwimlane.getAccessPointIds();
        mAccessPoint = ArrayUtils.isEmpty(accessPointIds) ? null : mHub.getAccessPoint(accessPointIds.get(0));
    }

    @Override
    public ModuleDataType getDataType() {
        return ModuleDataType.REPLAY_CHANNEL;
    }

    @Nullable
    @Override
    public List<EditorialItem> getEditorialItems() {
        if (mEditorialItems == null) {
            mEditorialItems = new ArrayList<>();

            // Add start tile if defined.
            if (mSwimlane.getStartTile() != null) {
                SwimLaneProximus.SwimlaneTile startTile = mSwimlane.getStartTile();
                EditorialItem startItem;

                if (startTile.getAction() == SwimLaneProximus.SwimLaneTileAction.GoPage) {
                    startItem = getReplayChannelPageEditorialItem(startTile, 0);
                } else {
                    startItem = getTile(mSwimlane, mSwimlane.getStartTile(), 0, false);
                }

                if (startItem != null) {
                    mEditorialItems.add(startItem);
                }
            }

            // Add end tile if defined.
            if (mSwimlane.getEndTile() != null) {
                SwimLaneProximus.SwimlaneTile endTile = mSwimlane.getEndTile();
                EditorialItem endItem;

                if (endTile.getAction() == SwimLaneProximus.SwimLaneTileAction.GoPage) {
                    endItem = getReplayChannelPageEditorialItem(endTile, null);
                } else {
                    endItem = getTile(mSwimlane, mSwimlane.getStartTile(), 0, false);
                }

                if (endItem != null) {
                    mEditorialItems.add(endItem);
                }
            }
        }

        return mEditorialItems;
    }

    @Nullable
    @Override
    public ModuleDataSource getDataSource() {
        return mDataSource;
    }

    @Nullable
    @Override
    public PageConfig getPageConfigForChannel(TvChannel channel) {
        return new ReplayChannelPageConfig(mAccessPoint, mHub, channel.getId(), channel.getName());
    }

    private EditorialItem getReplayChannelPageEditorialItem(SwimLaneProximus.SwimlaneTile swimlaneTile, Integer position) {
        String pageId = swimlaneTile.getPageId();
        if (pageId != null) {
            AccessPointProximus accessPoint = mHub.getAccessPoint(pageId);
            if (accessPoint != null) {
                return new GoPageEditorialItemProximus(
                        new AccessPointPageConfigProximus(accessPoint, mHub, false),
                        mSwimlane, swimlaneTile, position
                );
            }
        }
        return null;
    }

    private final ModuleDataSource mDataSource = new ModuleDataSource() {

        @Override
        public ModuleDataSourceService getService() {
            return ModuleDataSourceService.TV;
        }

        @Override
        public ModuleDataSourceMethod getMethod() {
            return ModuleDataSourceMethod.REPLAY_CHANNEL_PAGE_ITEMS;
        }
    };
}