package tv.threess.threeready.data.proximus.log.utils;

/**
 * Parse and the process status from technicolor content provider.
 * extra mapping is needed.
 *
 * @author Karetka Mezei Zoltan
 * @since 2019.06.07
 */
public class ProcStatusTch extends ProcStatus {
    public ProcStatusTch() {
        super();
    }

    protected void put(Field field, String value) {
        switch (field) {
            // convert kilo-bytes to bytes
            case VmPeak:
            case VmSize:
            case VmRSS:
                long bytes = Long.parseLong(value);
                if (bytes > 0) {
                    bytes <<= 10;
                }
                value = String.valueOf(bytes);
                break;
        }
        super.put(field, value);
    }

    @Override
    protected Field lookupKey(String key) {
        switch (key) {
            case "pid":
                return Field.Pid;

            case "vss_peak_size":
                return Field.VmPeak;

            case "vss_size":
                return Field.VmSize;

            case "rss_size":
                return Field.VmRSS;

            case "thread_count":
                return Field.Threads;
        }
        return null;
    }
}
