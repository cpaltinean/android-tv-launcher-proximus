package tv.threess.threeready.data.proximus.home.model;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.generic.helper.LocaleUtils;

/**
 * Localized title of swimlanes and tiles.
 *
 * @author Barabas Attila
 * @since 5/5/21
 */
public class DisplayNamesProximus extends ArrayList<DisplayNamesProximus.Name> {

    /**
     * @return Display name in the given language if is in the list, otherwise:
     * Empty string if the list is null or empty.
     * First item from the list.
     */
    public String getLocalizedNameOrFirst(String language) {
        String displayNameForLanguage = getDisplayNameForLanguage(language);
        if (displayNameForLanguage != null) {
            return displayNameForLanguage;
        }
        return size() > 0 ? get(0).getName() : "";
    }

    public String getLocalizedNameOrFirst() {
        return getLocalizedNameOrFirst(LocaleUtils.getApplicationLanguage());
    }

    /**
     * @return Display name in the given language if is in the list, otherwise the default value.
     */
    public String getLocalizedNameOrDefault(String language, String defaultValue) {
        String displayNameForLanguage = getDisplayNameForLanguage(language);
        return displayNameForLanguage != null ? displayNameForLanguage : defaultValue;
    }

    public  String getLocalizedNameOrDefault(String defaultValue) {
        return getLocalizedNameOrDefault(LocaleUtils.getApplicationLanguage(), defaultValue);
    }

    @Nullable
    private String getDisplayNameForLanguage(String language) {
        for (Name name : this) {
            if (language.equalsIgnoreCase(name.getLanguage())) {
                return name.getName();
            }
        }
        return null;
    }

    @XmlRootElement(name = "DisplayName")
    static class Name implements Serializable {
        @XmlElement(name = "Name")
        private String mName;

        @XmlElement(name = "Language")
        private String mLanguage;

        public String getName() {
            return mName;
        }

        public String getLanguage() {
            return mLanguage;
        }
    }

    public static final DisplayNamesProximus EMPTY_NAMES = new DisplayNamesProximus();
}
