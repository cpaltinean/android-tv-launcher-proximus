/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.vod;

import android.app.Application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.StandByStateChangeReceiver;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.vod.BaseVodRepository;
import tv.threess.threeready.data.vod.VodServiceRepository;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.proximus.vod.model.response.RentalExpirationProximus;
import tv.threess.threeready.data.proximus.vod.model.response.RentalExpirationResponseProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategoryProximus;
import tv.threess.threeready.data.vod.VodCache;
import tv.threess.threeready.data.vod.VodService;

/**
 * Implementation of the Proximus specific vod service handler methods.
 *
 * @author Barabas Attila
 * @since 2018.06.06
 */
public class VodServiceRepositoryProximus extends BaseVodRepository implements VodServiceRepository {
    private static final String TAG = Log.tag(VodServiceRepositoryProximus.class);

    private final VodProxyProximus mVodProxy = Components.get(VodProxyProximus.class);
    private final VodCache mVodCache = Components.get(VodCache.class);

    public VodServiceRepositoryProximus(Application app) {
        super(app);

        // Detect standby state changes.
        Components.get(StandByStateChangeReceiver.class)
                .addStateChangedListener(mStandByStateChangeListener);
    }

    @Override
    public void updatePurchases() throws Exception {
        rentalUpdater.updateIfExpired(this::updateRentedVods);
    }

    /**
     * @return Calculate a hash based on the user profile
     * parameters which can alter the category content.
     */
    protected static long getCategoryContentRequestHash() {
        String hdProfile = SettingsProximus.hdProfile.get("");
        String shopLanguage = SettingsProximus.shopLanguage.get("");
        return Objects.hash(hdProfile, shopLanguage);
    }

    @Override
    public void updateVodCategories() throws Exception {
        categoryUpdater.updateIfExpired(() -> {
            List<VodCategoryProximus> leafCategories = mVodProxy.getLeafCategories();

            List<String> nonEmptyLeafIds = new ArrayList<>();
            for (VodCategoryProximus category : leafCategories) {
                nonEmptyLeafIds.add(category.getId());
            }

            Components.get(VodCategoryCacheProximus.class).updateCategoryContent(leafCategories);
            SettingsProximus.batchEdit()
                    .put(SettingsProximus.leafCategoryIds, nonEmptyLeafIds)
                    .put(SettingsProximus.leafCategoryRequestHash, getCategoryContentRequestHash())
                    .persistNow();
        });
    }

    @Override
    public boolean isDeviceProfileValid() {
        long currentHash = getCategoryContentRequestHash();
        long cacheHash = SettingsProximus.leafCategoryRequestHash.get(0L);
        return currentHash == cacheHash;
    }

    /**
     * Updates the vod database with rented vods.
     *
     * @throws IOException In case of errors.
     */
    private void updateRentedVods() throws IOException {
        long now = System.currentTimeMillis();

        RentalExpirationResponseProximus rentalExpirationResponseProximus = mVodProxy.getRentedVodList();

        List<RentalExpirationProximus> backendRentals = rentalExpirationResponseProximus.getRentalInfos();
        mVodCache.updateRentedVods(backendRentals, now);

        SettingsProximus.freeRentals.edit().put(rentalExpirationResponseProximus.getFreeRentalCount());

        Log.d(TAG, "Rental info updated.");
    }

    // Get notification about the standby state changes.
    private final StandByStateChangeReceiver.Listener mStandByStateChangeListener = screenOn -> {
        // Cancel the periodic purchase update during standby. Will be loaded during the standby flow.
        if (!screenOn) {
            BaseWorkManager.cancelAlarm(mApp, VodService.buildUpdateVodCategoriesIntent());
            BaseWorkManager.cancelAlarm(mApp, VodService.buildUpdatePurchasesIntent());
        }
    };
}
