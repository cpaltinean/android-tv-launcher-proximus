package tv.threess.threeready.data.proximus.generic.interceptor;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Invocation;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.log.Log;

/**
 * Ok http interceptor to override the backend cache control header.
 *
 * @author Barabas Attila
 * @since 2020.04.29
 */
public class CacheControlOverrideOkHttpInterceptor implements Interceptor {
    private static final String TAG = Log.tag(CacheControlOverrideOkHttpInterceptor.class);
    private static final String CACHE_CONTROL_HEADER = "Cache-Control";

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);

        Invocation invocation = request.tag(Invocation.class);
        if (invocation == null) {
            return response;
        }

        // Check if cache control is enabled.
        CacheControlOverride override = invocation.method()
                .getAnnotation(CacheControlOverride.class);
        if (override == null) {
            return response;
        }

        // Get cache override option.
        CacheControlOverrideOption overrideOption = request.tag(CacheControlOverrideOption.class);
        if (overrideOption == null) {
            // Default cache control override.
            AppConfig appConfig = Components.get(AppConfig.class);
            overrideOption = new CacheControlOverrideOption(
                    (int) appConfig.getCacheSettings().getVodCacheTime(TimeUnit.MILLISECONDS));
        }

        // Override cache control headers.
        CacheControl cacheControl = new CacheControl.Builder()
                .maxAge(overrideOption.getMaxAgeMillis(), TimeUnit.MILLISECONDS)
                .maxStale(overrideOption.getMaxStateMillis(), TimeUnit.MILLISECONDS)
                .build();

        Log.d(TAG, "URL : " + request.url() + ", Cache control : " + cacheControl);
        response = response.newBuilder()
                .removeHeader(CACHE_CONTROL_HEADER)
                .header(CACHE_CONTROL_HEADER, cacheControl.toString())
                .build();

        return response;
    }


    @Target(METHOD)
    @Retention(RUNTIME)
    public @interface CacheControlOverride {
    }

    public static class CacheControlOverrideOption {
        private final int mMaxAgeMillis;
        private final int mMaxStaleMillis;

        public CacheControlOverrideOption(int maxAgeMillis) {
            mMaxAgeMillis = maxAgeMillis;
            mMaxStaleMillis = maxAgeMillis;
        }

        public CacheControlOverrideOption(int maxAgeMillis, int maxStaleMillis) {
            mMaxAgeMillis = maxAgeMillis;
            mMaxStaleMillis = maxStaleMillis;
        }

        /**
         * The maximum amount of time a resource is considered fresh.
         */
        public int getMaxAgeMillis() {
            return mMaxAgeMillis;
        }

        /**
         * @return Indicates the client will accept a stale response.
         * An optional value in seconds indicates the upper limit of staleness the client will accept.
         */
        public int getMaxStateMillis() {
            return mMaxStaleMillis;
        }
    }
}
