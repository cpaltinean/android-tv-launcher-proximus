package tv.threess.threeready.data.proximus.account.model.server;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Model representing the configuration options for a backend request.
 *
 * @author Barabas Attila
 * @since 3/10/21
 */
@XmlRootElement(name = "Request")
public class RequestPropertyProximus {

    @XmlElement(name = "name")
    private String mName;

    @XmlElement(name = "mustAuthenticate")
    private boolean mMustAuthenticate;

    public String getName() {
        return mName;
    }

    /**
     * @return True if the request most pass through the TIAMS authentications.
     */
    public boolean mustAuthenticate() {
        return mMustAuthenticate;
    }
}
