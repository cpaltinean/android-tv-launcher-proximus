package tv.threess.threeready.data.proximus.home.adapter;

import android.text.TextUtils;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus.SwimLaneTileAction;

public class SwimLaneTileTypeAdapter extends XmlAdapter<String, SwimLaneTileAction> {
    @Override
    public SwimLaneTileAction unmarshal(String v) {
        if (TextUtils.isEmpty(v)) {
            return SwimLaneTileAction.NoAction;
        }

        for (SwimLaneTileAction action : SwimLaneTileAction.values()) {
            if (v.equals(action.getAction())) {
                return action;
            }
        }
        return SwimLaneTileAction.NoAction;
    }

    @Override
    public String marshal(SwimLaneTileAction v) {
        throw new IllegalStateException("Not implemented.");
    }
}
