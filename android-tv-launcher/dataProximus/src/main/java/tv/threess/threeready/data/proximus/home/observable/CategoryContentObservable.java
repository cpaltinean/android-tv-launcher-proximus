/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.home.observable;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.api.home.model.module.ModuleSortOption;
import tv.threess.threeready.api.home.model.module.ModuleType;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IRentalExpiration;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.home.observable.BaseCategoryContentObservable;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.proximus.home.HomeProxyProximus;
import tv.threess.threeready.data.proximus.home.model.editorial.BannerEditorialProximus;
import tv.threess.threeready.data.proximus.home.model.editorial.VodCategoryEditorialItemProximus;
import tv.threess.threeready.data.proximus.home.model.module.ContentWallSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.module.SwimlaneModuleConfigProximus;
import tv.threess.threeready.data.proximus.home.model.module.VodCategoryDetailModuleConfigProximus;
import tv.threess.threeready.data.proximus.home.model.module.VodCategorySwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.BannerProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneContentResponseProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;
import tv.threess.threeready.data.proximus.vod.VodCategoryCacheProximus;
import tv.threess.threeready.data.proximus.vod.VodProxyProximus;
import tv.threess.threeready.data.proximus.vod.model.response.BaseVodItemProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategoryContentResponseProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategoryProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategorySeriesProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategorySortOptionProximus;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.vod.VodCache;
import tv.threess.threeready.data.vod.VodContract;

/**
 * Rx observable class to return content from a category based on the module config.
 * Automatically updates the purchase info and emits a new data when a purchase completed or expired.
 *
 * @author Barabas Attila
 * @since 2017.09.15
 */
public class CategoryContentObservable extends BaseCategoryContentObservable<IBaseContentItem> {
    private static final String TAG = Log.tag(CategoryContentObservable.class);

    private final HomeProxyProximus mHomeProxy = Components.get(HomeProxyProximus.class);
    private final VodProxyProximus mVodProxy = Components.get(VodProxyProximus.class);

    private final VodCategoryCacheProximus mCategoryCache = Components.get(VodCategoryCacheProximus.class);
    private final TvCache mTvCache = Components.get(TvCache.class);
    private final VodCache mVodCache = Components.get(VodCache.class);

    private final List<DataSourceSelector> mDataSourceSelectors;

    public CategoryContentObservable(Context context, ModuleConfig moduleConfig, int start, int count,
                                     List<DataSourceSelector> dataSourceSelectors) {
        super(context, moduleConfig, start, count);
        mDataSourceSelectors = dataSourceSelectors;
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<ModuleData<IBaseContentItem>> observableEmitter) throws IOException {
        Log.d(TAG, "onChange -> Uri : " + uri);

        if (mItemList == null) {
            // Nothing to update. Category content is not loaded yet.
            return;
        }

        if (uri.toString().contains(
                ConfigContract.CONTENT_URI_WITH_MODULE_ID.toString())) {
            loadItemList(observableEmitter);
            return;
        }

        String variantId = VodContract.getVariantIdFromPurchaseUri(uri);
        if (TextUtils.isEmpty(variantId)) {
            Log.w(TAG, "Not valid uri to update vod. Uri : " + uri);
            return;
        }

        List<IBaseContentItem> categoryItems = new ArrayList<>(mItemList);
        for (int i = 0; i < categoryItems.size(); ++i) {
            IBaseContentItem item = categoryItems.get(i);
            if (item instanceof IBaseVodItem) {
                IBaseVodItem vodItem = (IBaseVodItem) item;
                if (vodItem.isVariantIncluded(variantId)) {
                    try {
                        Log.d(TAG, "Update VOD :" + vodItem.getId());

                        IBaseVodItem completeVod = mVodProxy.getVod(vodItem.getId());
                        updateRentalExpiration(Collections.singletonList(completeVod));
                        updatePriceInfo(Collections.singletonList(completeVod));

                        categoryItems.set(i, completeVod);
                        mCategoryData = new ModuleData<>(mModuleConfig, categoryItems, mCategoryData);
                        onItemListLoaded(observableEmitter);
                        break;
                    } catch (Exception e) {
                        Log.e(TAG, "Couldn't update vod. Id : " + item.getId(), e);
                    }
                }
            }
        }
    }

    @Override
    public void subscribe(ObservableEmitter<ModuleData<IBaseContentItem>> emitter) throws Exception {
        super.subscribe(emitter);

        if (!TextUtils.isEmpty(mModuleConfig.getId())) {
            registerObserver(ConfigContract.buildUriForSubscriptionModuleId(mModuleConfig.getId()));
        }
    }

    @Override
    protected void loadItemList(ObservableEmitter<ModuleData<IBaseContentItem>> emitter) throws IOException {
        mCategoryData = getContent(mModuleConfig, mStart, mCount, mDataSourceSelectors);
        onItemListLoaded(emitter);
    }

    /**
     * Load data for the given module.
     *
     * @param moduleConfig        The module to load content for.
     * @param start               The pagination start index.
     * @param count               The pagination count.
     * @param dataSourceSelectors Sorting and filtering options for the content.
     *                            If null the default will be applied.
     * @return The data source which holds the data and title of the content.
     * @throws IOException In case of errors.
     */
    public ModuleData<IBaseContentItem> getContent(ModuleConfig moduleConfig, int start, int count,
                                                   @Nullable List<DataSourceSelector> dataSourceSelectors) throws IOException {
        VodCategoryProximus category = getVodCategory(moduleConfig);
        SwimLaneProximus swimLane = getSwimlane(moduleConfig);

        if (category == null && swimLane == null) {
            throw new IllegalStateException("No VOD category or swimlane defined.");
        }

        // Get filtering and sorting options for module.
        if (dataSourceSelectors == null) {
            dataSourceSelectors = getSelectors(moduleConfig, category);
        }

        // Load swimlane
        ModuleData<IBaseContentItem> moduleData;
        if (category == null) {
            Log.d(TAG, "Load swimlane details. Swimlane : " + swimLane.getId());
            moduleData = getSwimlaneModuleDataSource(moduleConfig, swimLane, start, count, dataSourceSelectors);

        // Load VOD category
        } else {
            Log.d(TAG, "Load category details. Category id : " + category.getId());
            DataSourceSelector categorySelector = dataSourceSelectors == null || dataSourceSelectors.isEmpty()
                    ? null : dataSourceSelectors.get(0); // Only sorting supported for VOD
            SwimLaneProximus.ClientUiClass uiClass = swimLane != null ? swimLane.getClientUiClass() : SwimLaneProximus.ClientUiClass.Undefined;
            moduleData = getVodCategoryModuleDataSource(category, uiClass, start, count, categorySelector);
        }

        List<IBaseContentItem> dataList = moduleData.getItems();
        updateRentalExpiration(dataList);
        updatePriceInfo(dataList);
        return moduleData;
    }

    /**
     * Load the swim lane content from backend as module data source.
     *
     * @return The module data source of the swim lane.
     * @throws IOException In case of errors.
     */
    private ModuleData<IBaseContentItem> getSwimlaneModuleDataSource(ModuleConfig moduleConfig,
                                                                     SwimLaneProximus swimlane, int start, int count,
                                                                     List<DataSourceSelector> selectors) throws IOException {
        count = moduleConfig.getSize(count);

        // TODO : temporal solution until we have the lane control only solution.
        // Apply the limit from swimlane for stripe.
        if (moduleConfig.getType() == ModuleType.STRIPE) {
            count = Math.min(swimlane.getItemDisplayLimit(), count);
        }

        String title = swimlane.getDisplayNames().getLocalizedNameOrFirst();
        if (swimlane.isClientSwimlane()) {
            return new ModuleData<>(mModuleConfig, title, Collections.emptyList(), selectors, count, start, true);
        }

        // Get swimlane content.
        SwimLaneContentResponseProximus response = mHomeProxy.geSwimlaneContentItems(swimlane, start, count, selectors);

        // Movies and programs
        List<IBaseContentItem> items = new ArrayList<>(response.getContentItems());

        // VOD categories
        for (String categoryId : ArrayUtils.notNull(response.getCategoryIds())) {
            VodCategoryProximus category = mCategoryCache.getCategory(categoryId);
            if (category == null || !mCategoryCache.hasContent(categoryId)) {
                continue;
            }

            items.add(applySeriesGrouping(category, swimlane.getClientUiClass()));
        }

        // Channels
        items.addAll(mTvCache.getChannels(response.getChannelIds()));

        // Add banners
        for (BannerProximus banner : ArrayUtils.notNull(response.getBanners())) {
            items.add(new BannerEditorialProximus(banner));
        }

        return new ModuleData<>(mModuleConfig, title, items, selectors, count, start, !response.getContentItems().isEmpty());
    }

    private ModuleData<IBaseContentItem> getVodCategoryModuleDataSource(VodCategoryProximus category,
                                                                        SwimLaneProximus.ClientUiClass uiClass, int start, int count,
                                                                        DataSourceSelector sorting) throws IOException {
        boolean hasMoreItems;
        List<IBaseContentItem> contentItems = new ArrayList<>();

        // Load category content.
        if (category.isLeaf()) {
            VodCategoryContentResponseProximus categoryContent = mVodProxy.getVodCategoryContent(
                    category.getId(), start, count, (VodCategorySortOptionProximus) sorting.getSelectedOption());
            contentItems.addAll(categoryContent.getVODs());
            hasMoreItems = start + count < categoryContent.getTotalCount();
        } else {
            // Load child categories.
            List<VodCategoryProximus> subCategories = mCategoryCache.getSubCategories(category.getId());
            for (VodCategoryProximus subCategory : subCategories) {
                contentItems.add(applySeriesGrouping(subCategory, uiClass));
            }
            // No paginated loading for VOD categories.
            count = subCategories.size();
            hasMoreItems = false;
        }

        return new ModuleData<>(mModuleConfig, category.getTitle(), contentItems,
                Collections.singletonList(sorting), count, start, hasMoreItems);
    }

    /**
     * @return The given category as a series or an editorial tile.
     */
    private IBaseContentItem applySeriesGrouping(VodCategoryProximus category, SwimLaneProximus.ClientUiClass uiClass) {
        // Check if the category is marked as series.
        if (category.hasSeriesLayout()) {
            boolean hasSeasons = true;
            List<VodCategoryProximus> seasons = mCategoryCache.getSubCategories(category.getId());

            // Check if has seasons and not other branch categories.
            for (VodCategoryProximus season : seasons) {
                if (!season.isLeaf()) {
                    hasSeasons = false;
                    break;
                }
            }

            if (hasSeasons) {
                return new VodCategorySeriesProximus(category, seasons, uiClass);
            }
        }

        return new VodCategoryEditorialItemProximus(category, uiClass);
    }

    /**
     * @param moduleConfig The module config to get the category for.
     * @return The VOD category for the module orn ull if there is no such.
     */
    @Nullable
    private VodCategoryProximus getVodCategory(ModuleConfig moduleConfig) {
        // Category already available.
        if (moduleConfig instanceof VodCategoryDetailModuleConfigProximus) {
            return ((VodCategoryDetailModuleConfigProximus) moduleConfig).getCategory();
        }

        // Get category by id.
        return mCategoryCache.getCategory(getCategoryId(moduleConfig));
    }

    @Nullable
    private String getCategoryId(ModuleConfig moduleConfig) {
        // Get category by id.
        if (moduleConfig instanceof VodCategoryDetailModuleConfigProximus) {
            return ((VodCategoryDetailModuleConfigProximus) moduleConfig).getCategoryId();
        }

        // Check if category swimlane.
        if (moduleConfig instanceof VodCategorySwimlaneModuleProximus) {
            return ((VodCategorySwimlaneModuleProximus) moduleConfig).getCategoryId();
        }

        if (moduleConfig.getDataSource() != null && moduleConfig.getDataSource().getParams() != null) {
            return moduleConfig.getDataSource().getParams()
                    .getFilter(ModuleFilterOption.Mixed.CategoryId.NAME);
        }

        return null;
    }

    /**
     * @param moduleConfig The module config to get the swimlane for.
     * @return The swimlane for the module or null if there is no such.
     * @throws IOException In case of errors.
     */
    @Nullable
    private SwimLaneProximus getSwimlane(ModuleConfig moduleConfig) throws IOException {
        // Swimlane already available.
        if (moduleConfig instanceof SwimlaneModuleConfigProximus) {
            return ((SwimlaneModuleConfigProximus) moduleConfig).getSwimlane();
        }

        // Swimlane by id.
        String swimlaneId = null;
        if (moduleConfig.getDataSource() != null && moduleConfig.getDataSource().getParams() != null) {
            swimlaneId = moduleConfig.getDataSource().getParams()
                    .getFilter(ModuleFilterOption.Mixed.SwimLaneId.NAME);
        }

        if (TextUtils.isEmpty(swimlaneId)) {
            return null;
        }

        return mHomeProxy.getSwimLaneHub().getSwimlane(swimlaneId);
    }


    /**
     * @return Sorting and filtering options for the module config.
     */
    private List<DataSourceSelector> getSelectors(ModuleConfig moduleConfig, VodCategoryProximus category) {
        // Content wall sort option.
        if (moduleConfig instanceof ContentWallSwimlaneModuleProximus) {
            return ((ContentWallSwimlaneModuleProximus) moduleConfig).getSelectors();
        }

        // VOD category collection sort options
        if (category != null) {
            // TODO : once only lane control supported move this to the module config.
            String sortValue = moduleConfig.getDataSource() != null && moduleConfig.getDataSource().getParams() != null
                    ? moduleConfig.getDataSource().getParams().getSort(ModuleSortOption.Mixed.SortBy.NAME) : null;
            return Collections.singletonList(
                    new DataSourceSelector((List) category.getSortOptions(), category.getSorting(sortValue)));
        }

        return Collections.emptyList();
    }

    /**
     * Updates the rental expiration for a vod items in the list.
     *
     * @param contentItems The list of content item which needs to be updated.
     */
    private void updateRentalExpiration(List<IBaseContentItem> contentItems) {
        Map<String, IRentalExpiration> rentalExpirationMap = mVodCache.getRentalExpiration();
        for (IBaseContentItem item : contentItems) {
            if (item instanceof BaseVodItemProximus) {
                ((BaseVodItemProximus) item).setRentalExpirationInfo(rentalExpirationMap);
            }
        }
    }

    /**
     * Update the price options based on the credit limit.
     *
     * @param contentItems The list of content items where the price option needs to be updated.
     */
    private void updatePriceInfo(List<IBaseContentItem> contentItems) {
        double creditPriceLimit = SettingsProximus.freeRentalLimit.get(0d);
        int freeRentals = SettingsProximus.freeRentals.get(0);

        for (IBaseContentItem item : contentItems) {
            if (item instanceof BaseVodItemProximus) {
                ((BaseVodItemProximus) item).updateCheapestPrice(creditPriceLimit, freeRentals);
            }
        }
    }
}
