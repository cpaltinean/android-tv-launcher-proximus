package tv.threess.threeready.data.proximus.account.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import tv.threess.threeready.data.proximus.account.model.account.consent.ConsentProximus;
import tv.threess.threeready.data.proximus.account.model.account.consent.UpdateConsentBodyProximus;

/**
 * Retrofit interface to manage user consents.
 *
 * @author Zsolt Bokor_
 * @since 2020.10.22
 */
public interface ConsentRetrofitProximus {

    @GET("/api/userconsent/owner-type/{owner_type}/owner-id/{owner_id}/consents")
    Call<List<ConsentProximus>> getConsents(@Path("owner_type") String ownerType,
                                     @Path("owner_id") String ownerId,
                                     @Query("consent_language") String consentLanguage,
                                     @Query("request_scenario") String requestScenario);

    @GET("/api/userconsent/owner-type/{owner_type}/owner-id/{owner_id}/consents/{consent_id}")
    Call<ConsentProximus> getConsent(@Path("owner_type") String ownerType,
                                     @Path("owner_id") String ownerId,
                                     @Path("consent_id") String consentId,
                                     @Query("consent_language") String consentLanguage);

    @PUT("/api/userconsent/owner-type/{owner_type}/owner-id/{owner_id}/consents/{consent_id}")
    Call<Void> setConsent(@Path("owner_type") String ownerType,
                             @Path("owner_id") String ownerId,
                             @Path("consent_id") String consentId,
                             @Body UpdateConsentBodyProximus updateConsentBody);
}
