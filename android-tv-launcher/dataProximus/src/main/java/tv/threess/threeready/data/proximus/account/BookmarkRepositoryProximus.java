package tv.threess.threeready.data.proximus.account;

import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.BookmarkRepository;
import tv.threess.threeready.api.generic.model.BookmarkType;
import tv.threess.threeready.api.generic.model.ContinueWatchingConfig;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.data.account.AccountLocalCache;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.vod.VodCache;

/**
 * Implementation of the Proximus specific bookmark handler methods.
 *
 * @author Bartalus Csaba - Zsolt
 * @since 2020.12.04
 */

public class BookmarkRepositoryProximus implements BookmarkRepository, Component {

    private final TvCache mTvCache = Components.get(TvCache.class);
    private final VodCache mVodCache = Components.get(VodCache.class);
    private final AccountLocalCache mAccountCache = Components.get(AccountLocalCache.class);

    @Override
    public IBookmark getBookmark(IContentItem contentItem, BookmarkType bookmarkType) {
        return mAccountCache.getBookmark(contentItem.getId(), bookmarkType);
    }

    @Override
    public Completable addReplayBookmark(IBroadcast broadcast, long position, long duration) {
        return Completable.fromAction(() -> {
            // Empty bookmark. Nothing to do.
            if (position == 0 || duration == 0) {
                return;
            }

            // Delete all old bookmarks.
            cleanupBookmarks();

            ContinueWatchingConfig cwConfig = Components.get(ContinueWatchingConfig.class);
            long replayWatchedMinTime = cwConfig.getReplayWatchedMinTime(TimeUnit.MILLISECONDS);

            if (isInBookmarkRange(position, replayWatchedMinTime)) {
                mAccountCache.addBookmark(broadcast.getId(), BookmarkType.Replay, position, duration);
                mTvCache.addReplayContinueWatching(broadcast);
            } else {
                mAccountCache.deleteBookmarks(BookmarkType.Replay, broadcast.getId());
                mTvCache.deleteReplayContinueWatching(broadcast);
            }
        });
    }

    @Override
    public Completable addRecordingBookmark(IRecording recording, long position, long duration) {
        return Completable.fromAction(() -> {
            // Empty bookmark. Nothing to do.
            if (position == 0 || duration == 0) {
                return;
            }

            // Delete all old bookmarks.
            cleanupBookmarks();

            ContinueWatchingConfig cwConfig = Components.get(ContinueWatchingConfig.class);
            long recordingWatchedMinTime = cwConfig.getRecordingWatchedMinTime(TimeUnit.MILLISECONDS);

            if (isInBookmarkRange(position, recordingWatchedMinTime)) {
                mAccountCache.addBookmark(recording.getId(), BookmarkType.Recording, position, duration);
            } else {
                mAccountCache.deleteBookmarks(BookmarkType.Recording, recording.getId());
            }
        });
    }

    @Override
    public Completable addVodBookmark(IVodItem vodItem, IVodVariant variant, long position, long duration) {
        return Completable.fromAction(() -> {
            // Empty bookmark. Nothing to do.
            if (position == 0 || duration == 0) {
                return;
            }

            // Delete all old bookmarks.
            cleanupBookmarks();

            ContinueWatchingConfig cwConfig = Components.get(ContinueWatchingConfig.class);
            long vodWatchedMinTime = cwConfig.getVodWatchedMinTime(TimeUnit.MILLISECONDS);

            if (isInBookmarkRange(position, vodWatchedMinTime)) {
                // Save bookmark for both. Vod item and variant.
                mAccountCache.addBookmark(variant.getId(), BookmarkType.VodVariant, position, duration);
                mVodCache.addVodContinueWatching(vodItem, variant);
            } else {
                mAccountCache.deleteBookmarks(BookmarkType.VodVariant, variant.getId());
                mVodCache.deleteVodContinueWatching(vodItem, variant);
            }
        });
    }

    /**
     * Deletes the bookmarks and continue watching items older than the configured value (e.g 10 days)
     */
    private void cleanupBookmarks() {
        ContinueWatchingConfig cwConfig = Components.get(ContinueWatchingConfig.class);
        long olderThan = System.currentTimeMillis() - cwConfig.geAgeLimit(TimeUnit.MILLISECONDS);


        // Delete bookmarks.
        mAccountCache.deleteOlderBookmarks(olderThan);

        // Delete continue watching replay.
        mTvCache.deleteOlderContinueWatchingReplay(olderThan);

        // Delete continue watching VOD.
        mVodCache.deleteOlderContinueWatchingVod(olderThan);

        // Delete continue watching VOD variant.
        mVodCache.deleteOlderContinueWatchingVodVariant(olderThan);
    }

    /**
     * @return True if the position is inside of the bookmark range.
     * Bookmarks are not saved at the very beginning.
     */
    private static boolean isInBookmarkRange(long position,
                                             long watchedTimeMin) {
        return position > watchedTimeMin;
    }
}
