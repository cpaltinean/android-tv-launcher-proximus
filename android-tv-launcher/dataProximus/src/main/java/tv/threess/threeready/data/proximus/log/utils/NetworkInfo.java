package tv.threess.threeready.data.proximus.log.utils;

import java.io.File;

import tv.threess.threeready.api.middleware.model.BaseInfo;

/**
 * Parse and the `/proc/net/dev` file.
 *
 * @author Karetka Mezei Zoltan
 * @since 2019.08.26
 */
public class NetworkInfo extends BaseInfo<NetworkInfo.Field> {

    private NetworkInfo() {
        super(Field.class);
    }

    /**
     * Read information from `/proc/net/dev` file and return it.
     */
    public static NetworkInfo read() {
        NetworkInfo result = new NetworkInfo();
        result.readFrom(new File("/proc/net/dev"));
        return result;
    }

    /**
     * The content of the file is similar to:
     * Inter-|   Receive                                                |  Transmit
     *  face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed
     *   sit0:       0       0    0    0    0     0          0         0        0       0    0    0    0     0       0          0
     * ip6_vti0:       0       0    0    0    0     0          0         0        0       0    0    0    0     0       0          0
     * ip_vti0:       0       0    0    0    0     0          0         0        0       0    0    0    0     0       0          0
     *     lo:  119235     432    0    0    0     0          0         0   119235     432    0    0    0     0       0          0
     *   eth0: 3081317601 5791614    0    0    0     0          0     19717 271115939 1348603    0    0    0     0       0          0
     * ip6tnl0:       0       0    0    0    0     0          0         0        0       0    0    0    0     0       0          0
     */
    @Override
    protected void processLine(String line) {
        int separator = line.indexOf(':');
        if (separator < 0) {
            // not a valid `key: value` pair
            return;
        }

        String interfaceName = line.substring(0, separator).trim();
        if (!NetworkType.ethernet.device().equals(interfaceName)) {
            return;
        }

        String[] values = line.substring(separator + 1).trim().split("\\s+");
        if (values.length != 16) {
            return;
        }

        this.put(Field.RxBytes, values[0]);
        this.put(Field.RxErrs, values[2]);
        this.put(Field.RxDrop, values[3]);
        this.put(Field.TxBytes, values[8]);
        this.put(Field.TxErrs, values[10]);
        this.put(Field.TxDrop, values[11]);
    }

    public enum Field implements BaseInfo.Field {
        RxBytes,
        RxErrs,
        RxDrop,
        TxBytes,
        TxErrs,
        TxDrop;

        Field(String key) {
            mKey = key;
        }
        Field() {
            mKey = name();
        }
        final String mKey;

        @Override
        public String getKey() {
            return mKey;
        }
    }

    private enum NetworkType {
        wifi("wlan0"),
        ethernet("eth0"),
        noNetwork("lo");

        private final String mDevice;

        NetworkType( String device) {
            mDevice = device;
        }

        public String device() {
            return mDevice;
        }
    }
}
