package tv.threess.threeready.data.proximus.home.model.module;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceParams;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceService;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
* Module config which shows the watchlist items

* @author Barabas Attila
* @since 6/27/21
*/

public class WishlistSwimlaneModuleProximus extends SwimlaneModuleConfigProximus {

    public WishlistSwimlaneModuleProximus(SwimLaneProximus swimlane) {
        super(swimlane);
    }

    @Nullable
    @Override
    public ModuleDataSource getDataSource() {
        return mDataSource;
    }

    private final ModuleDataSource mDataSource = new ModuleDataSource() {

        @Override
        public ModuleDataSourceService getService() {
            return ModuleDataSourceService.TV;
        }

        @Override
        public ModuleDataSourceMethod getMethod() {
            return ModuleDataSourceMethod.WATCHLIST;
        }

        @Nullable
        @Override
        public ModuleDataSourceParams getParams() {
            return mParams;
        }
    };

}
