package tv.threess.threeready.data.proximus.generic.interceptor;

import android.app.Application;

import java.io.IOException;

import okhttp3.Request;
import tv.threess.threeready.data.proximus.iam.IAMOkHttpInterceptor;
import tv.threess.threeready.data.proximus.iam.IAMRetrofitProximus;
import tv.threess.threeready.data.proximus.utils.URLParser;

/**
 * Replace the request base url with the value from boot config
 * and add generic micro services request parameters.
 *
 * @author Barabas Attila
 * @since 2020.06.24
 */
public class MSOkHttpInterceptor extends IAMOkHttpInterceptor {
    private static final String PXTV_VERSION_HEADER = "X-PXTV-VERSION";
    private static final String ACCEPT_HEADER = "Accept";
    private static final String APPLICATION_JSON = "application/json";

    public MSOkHttpInterceptor(Application app, IAMRetrofitProximus iamRetrofit) {
        super(app, iamRetrofit);
    }

    @Override
    protected boolean mustAuthenticate(Request request) {
        // No unsecured request for micro services.
        return true;
    }

    @Override
    protected void overrideRequest(Request request, Request.Builder requestBuilder, String overrideBaseUrl) throws IOException {
        super.overrideRequest(request, requestBuilder, overrideBaseUrl);

        // Extra headers for Consent API
        requestBuilder.addHeader(PXTV_VERSION_HEADER, URLParser.getBTAHeader());
        requestBuilder.addHeader(ACCEPT_HEADER, APPLICATION_JSON);
    }
}
