package tv.threess.threeready.data.proximus.log.utils;

import tv.threess.threeready.api.middleware.model.BaseInfo;

/**
 * Parse the output of command `df -k`.
 *
 * @author Karetka Mezei Zoltan
 * @since 2019.08.28
 */
public class StorageInfo extends BaseInfo<StorageInfo.Field> {

    private static final String INT_FILESYSTEM = "/dev/root";
    private static final String EXT_FILESYSTEM = "/dev/fuse";
    private static final String EXT_MOUNTPOINT = "/storage/emulated";
    private static final String USB_MOUNTPOINT = "/storage/";

    private StorageInfo() {
        super(Field.class);
    }

    /**
     * Read storage status executing process: `df -k`.
     */
    public static StorageInfo read() {
        StorageInfo result = new StorageInfo();
        result.readFrom("df", "-k");
        return result;
    }

    protected void put(Field field, String value) {
        switch (field) {
            // convert kilo bytes to bytes
            case IntUsed:
            case ExtUsed:
            case UsbUsed:
            case IntAvailable:
            case ExtAvailable:
            case UsbAvailable:
                long bytes = Long.parseLong(value);
                if (bytes > 0) {
                    bytes <<= 10;
                }
                value = String.valueOf(bytes);
                break;
        }
        super.put(field, value);
    }

    /**
     * Content of the file is similar to:
     * Filesystem            1K-blocks    Used Available Use% Mounted on
     * /dev/root               2048044 1350068    681592  67% /
     * tmpfs                    880392     876    879516   1% /dev
     * tmpfs                    880392       0    880392   0% /mnt
     * /dev/block/dm-1          243772   80068    158504  34% /vendor
     * tmpfs                    880392       0    880392   0% /apex
     * /dev/block/mmcblk1p20   9893888 1467060   8426828  15% /data
     * /dev/block/mmcblk1p5      12016     192     11500   2% /dev/hwcfg
     * /dev/block/mmcblk1p9      11760     888     10388   8% /cache
     * /dev/block/mmcblk1p21   1131344      24   1103112   1% /tad
     * /dev/fuse               9893888 1467060   8426828  15% /storage/emulated
     * /dev/fuse               7542368 5747388   1794980  77% /storage/4C63-7380
     */
    @Override
    protected void processLine(String line) {
        String[] values = line.trim().split("\\s+");
        if (values.length != 6) {
            return;
        }
        String valFilesystem = values[0];
        String valBlocks1K = values[1];
        String valUsed = values[2];
        String valAvailable = values[3];
        String valUsePercent = values[4];
        String valMountedOn = values[5];

        if (valFilesystem.equals(INT_FILESYSTEM)) {
            put(Field.IntFilesystem, valFilesystem);
            put(Field.IntBlocks1K, valBlocks1K);
            put(Field.IntUsed, valUsed);
            put(Field.IntAvailable, valAvailable);
            put(Field.IntUsePercent, valUsePercent);
            put(Field.IntMountedOn, valMountedOn);
        }
        else if (valFilesystem.equals(EXT_FILESYSTEM) && valMountedOn.equals(EXT_MOUNTPOINT)) {
            put(Field.ExtFilesystem, valFilesystem);
            put(Field.ExtBlocks1K, valBlocks1K);
            put(Field.ExtUsed, valUsed);
            put(Field.ExtAvailable, valAvailable);
            put(Field.ExtUsePercent, valUsePercent);
            put(Field.ExtMountedOn, valMountedOn);
        }
        else if (valFilesystem.startsWith(EXT_FILESYSTEM) && valMountedOn.startsWith(USB_MOUNTPOINT)) {
            // use the last usb details
            put(Field.UsbFilesystem, valFilesystem);
            put(Field.UsbBlocks1K, valBlocks1K);
            put(Field.UsbUsed, valUsed);
            put(Field.UsbAvailable, valAvailable);
            put(Field.UsbUsePercent, valUsePercent);
            put(Field.UsbMountedOn, valMountedOn);
        }
    }

    public enum Field implements BaseInfo.Field {
        // internal storage
        IntFilesystem,
        IntBlocks1K,
        IntUsed,
        IntAvailable,
        IntUsePercent,
        IntMountedOn,

        // external storage
        ExtFilesystem,
        ExtBlocks1K,
        ExtUsed,
        ExtAvailable,
        ExtUsePercent,
        ExtMountedOn,

        // removable storage
        UsbFilesystem,
        UsbBlocks1K,
        UsbUsed,
        UsbAvailable,
        UsbUsePercent,
        UsbMountedOn;

        @Override
        public String getKey() {
            return name();
        }
    }
}
