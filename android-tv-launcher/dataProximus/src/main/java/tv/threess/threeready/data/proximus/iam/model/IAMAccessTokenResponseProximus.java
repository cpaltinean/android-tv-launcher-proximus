package tv.threess.threeready.data.proximus.iam.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Proximus backend response for IAM access token method.
 *
 * @author Barabas Attila
 * @since 2020.03.20
 */
public class IAMAccessTokenResponseProximus implements Serializable {

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("token_type")
    private String tokeType;


    public String getTokeType() {
        return tokeType;
    }

    public String getAccessToken() {
        return accessToken;
    }
}
