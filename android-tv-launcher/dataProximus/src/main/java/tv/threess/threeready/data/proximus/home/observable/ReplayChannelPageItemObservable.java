package tv.threess.threeready.data.proximus.home.observable;

import android.content.Context;
import android.net.Uri;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.TvChannelCacheType;
import tv.threess.threeready.api.tv.model.ReplayChannelPageItem;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.proximus.home.model.module.ReplayChannelPageModuleProximus;
import tv.threess.threeready.data.proximus.home.model.module.ReplayChannelWallSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.selector.ChannelMenuSelector;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.tv.TvContract;

/**
 * RX observable which returns a list of {@link ReplayChannelPageItem}
 * Registers content observables and emits the new items if the channel list is changed.
 *
 * @author David Bondor
 * @since 4.28.2022
 */
public class ReplayChannelPageItemObservable extends BaseContentObservable<ModuleData<ReplayChannelPageItem>> {

    private final static String TAG = Log.tag(ReplayChannelPageItemObservable.class);

    private final TvCache mTvCache = Components.get(TvCache.class);

    private final ModuleConfig mModuleConfig;
    private final int mStart;
    private final int mCount;
    private List<DataSourceSelector> mDataSourceSelectors;

    public ReplayChannelPageItemObservable(Context context, ModuleConfig moduleConfig, int start, int count,
                                           List<DataSourceSelector> dataSourceSelectors) {
        super(context);
        mModuleConfig = moduleConfig;
        mStart = start;
        mCount = count;
        mDataSourceSelectors = dataSourceSelectors;
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<ModuleData<ReplayChannelPageItem>> emitter) throws Exception {
        List<ReplayChannelPageItem> channels = getReplayChannelPageItems();
        emitter.onNext(new ModuleData<>(mModuleConfig, mModuleConfig.getTitle(), channels, mDataSourceSelectors, mCount, mStart, mCount == channels.size()));
    }

    @Override
    public void subscribe(ObservableEmitter<ModuleData<ReplayChannelPageItem>> emitter) throws Exception {
        super.subscribe(emitter);

        registerObserver(TvContract.Channel.CONTENT_URI);
        registerObserver(TvContract.Favorites.CONTENT_URI);

        // Get filtering for module.
        if (mDataSourceSelectors == null) {
            mDataSourceSelectors = getSelectors(mModuleConfig);
        }

        List<ReplayChannelPageItem> channels = getReplayChannelPageItems();

        Log.d(TAG, "Emit replay channel page item list with size: " + channels.size());

        emitter.onNext(new ModuleData<>(mModuleConfig, mModuleConfig.getTitle(), channels, mDataSourceSelectors, mCount, mStart, mCount == channels.size()));
    }

    private List<ReplayChannelPageItem> getReplayChannelPageItems() {
        TvChannelCacheType channelType = TvChannelCacheType.ALL;
        for (DataSourceSelector selector : ArrayUtils.notNull(mDataSourceSelectors)) {
            SelectorOption selectorOption = selector.getSelectedOption();
            if ((selectorOption instanceof ChannelMenuSelector)) {
                channelType = ((ChannelMenuSelector) selectorOption).getChannelType();
                break;
            }
        }
        List<ReplayChannelPageItem> replayChannelPages = new ArrayList<>();
        for (TvChannel channel : mTvCache.getReplayChannels(channelType, mStart, mCount)) {
            PageConfig pageConfig = ((ReplayChannelPageModuleProximus) mModuleConfig).getPageConfigForChannel(channel);
            if (pageConfig != null) {
                replayChannelPages.add(new ReplayChannelPageItem(channel, pageConfig));
            }
        }
        return replayChannelPages;
    }


    /**
     * @return Filtering options for the module config.
     */
    private List<DataSourceSelector> getSelectors(ModuleConfig moduleConfig) {
        // Content wall sort option.
        if (moduleConfig instanceof ReplayChannelWallSwimlaneModuleProximus) {
            return ((ReplayChannelWallSwimlaneModuleProximus) moduleConfig).getSelectors();
        }

        return Collections.emptyList();
    }
}