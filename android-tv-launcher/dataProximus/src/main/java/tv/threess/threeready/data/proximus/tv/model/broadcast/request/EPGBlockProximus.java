package tv.threess.threeready.data.proximus.tv.model.broadcast.request;

/**
 * Enum representing 4 hour block of EPG data on the backend.
 *
 * @author Barabas Attila
 * @since 2020.03.30
 */
public enum EPGBlockProximus {
    B_0_4(0, 4),
    B_4_8(4, 8),
    B_8_12(8, 12),
    B_12_16(12, 16),
    B_16_20(16, 20),
    B_20_24(20, 24);

    private final int mStartHour;
    private final int mEndHour;

    EPGBlockProximus(int startHour, int endHour) {
        mStartHour = startHour;
        mEndHour = endHour;
    }

    public String getName() {
        return String.format("%02d", mStartHour) + "-" + String.format("%02d", mEndHour);
    }

    public int getStartHour() {
        return mStartHour;
    }

    public int getEndHour() {
        return mEndHour;
    }
}