package tv.threess.threeready.data.proximus.home.model.module;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceParams;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceService;
import tv.threess.threeready.api.home.model.module.ModuleDataType;
import tv.threess.threeready.api.home.model.module.ModuleSortOption;
import tv.threess.threeready.api.home.model.module.ModuleType;
import tv.threess.threeready.api.home.model.page.ModulePageConfig;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.data.proximus.home.model.editorial.GoAppsEditorialItemsProximus;
import tv.threess.threeready.data.proximus.home.model.editorial.GoPageByIdEditorialItemProximus;
import tv.threess.threeready.data.proximus.home.model.editorial.GoGridEditorialItemProximus;
import tv.threess.threeready.data.proximus.home.model.editorial.GoNetflixEditorialItemProximus;
import tv.threess.threeready.data.proximus.home.model.editorial.GoPageEditorialItemProximus;
import tv.threess.threeready.data.proximus.home.model.editorial.GoRecordingsEditorialItemProximus;
import tv.threess.threeready.data.proximus.home.model.editorial.GoRentalsEditorialItemProximus;
import tv.threess.threeready.data.proximus.home.model.editorial.GoVodCategoryEditorialItemProximus;
import tv.threess.threeready.data.proximus.home.model.editorial.GoWishlistEditorialItemProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus.SwimLaneTileAction;

/**
 * Module config representing a proximus swimlane.
 *
 * @author Barabas Attila
 * @since 5/19/21
 */
public class SwimlaneModuleConfigProximus implements ModuleConfig {

    protected final SwimLaneProximus mSwimlane;
    protected final ModuleDataSourceParams mParams;

    protected List<EditorialItem> mEditorialItems;

    public SwimlaneModuleConfigProximus(SwimLaneProximus swimlane) {
        mSwimlane = swimlane;
        mParams = new SwimlaneModuleDataSourceParams(swimlane, getType() != ModuleType.COLLECTION);
    }

    @Override
    public String getId() {
        return mSwimlane.getId();
    }

    @Nullable
    protected static EditorialItem getTile(SwimLaneProximus swimLane,
                                  SwimLaneProximus.SwimlaneTile swimlaneTile, Integer position, boolean onlyContentWall) {
        if (swimlaneTile == null
                || swimlaneTile.getAction() == null) {
            return null;
        }

        // Only allow content wall type tiles.
        if (onlyContentWall && swimlaneTile.getAction() != SwimLaneTileAction.GoContentWall) {
            return null;
        }

        switch (swimlaneTile.getAction()) {
            default:
                return null;
            case GoContentWall:
                return new GoPageEditorialItemProximus(new ModulePageConfig(
                        new ContentWallSwimlaneModuleProximus(swimLane), UILog.Page.ContentWallFullContentPage
                ), swimLane, swimlaneTile, position);
            case GoVodCategory:
                return new GoVodCategoryEditorialItemProximus(swimLane, swimlaneTile, position);
            case GoPage:
                return new GoPageByIdEditorialItemProximus(swimLane, swimlaneTile, position);
            case GoNetflix:
                return new GoNetflixEditorialItemProximus(swimLane, swimlaneTile, position);
            case GoRecordings:
                return new GoRecordingsEditorialItemProximus(swimLane, swimlaneTile, position);
            case GoGrid:
                return new GoGridEditorialItemProximus(swimLane, swimlaneTile, position);
            case GoRentals:
                return new GoRentalsEditorialItemProximus(swimLane, swimlaneTile, position);
            case GoWishlist:
                return new GoWishlistEditorialItemProximus(swimLane, swimlaneTile, position);
            case GoAppList:
                return new GoAppsEditorialItemsProximus(swimLane, swimlaneTile, position);
        }
    }

    public SwimLaneProximus getSwimlane() {
        return mSwimlane;
    }

    /**
     * Generate a list of editorial tiles based on the swimlane start/end tile config.
     * @param swimlane The swimlane which holds the tile config.
     * @param onlyContentWall True if only content wall tiles should be allowed.
     * @return A list with the start and end tile editorial items.
     */
    public static List<EditorialItem> generateEditorialTiles(SwimLaneProximus swimlane, boolean onlyContentWall) {
        List<EditorialItem> editorialItems = new ArrayList<>();

        // Add start tile if defined.
        EditorialItem startTile = getTile(swimlane, swimlane.getStartTile(), 0, onlyContentWall);
        if (startTile != null) {
            editorialItems.add(startTile);
        }

        // Add end tile if defined.
        EditorialItem endTile = getTile(swimlane, swimlane.getEndTile(), null, onlyContentWall);
        if (endTile != null) {
            editorialItems.add(endTile);
        }

        return editorialItems;
    }

    @Nullable
    @Override
    public List<EditorialItem> getEditorialItems() {
        if (mEditorialItems == null) {
            mEditorialItems = generateEditorialTiles(mSwimlane, false);
        }

        return mEditorialItems;
    }

    @Override
    public String getReportName() {
        return mSwimlane.getName();
    }

    @Override
    public String getReportReference() {
        return mSwimlane.getId();
    }

    @Nullable
    @Override
    public String getTitle() {
        return mSwimlane.getDisplayNames().getLocalizedNameOrFirst();
    }

    @Override
    public ModuleType getType() {
        return ModuleType.STRIPE;
    }

    @Override
    public String toString() {
        return "SwimlaneModuleConfigProximus{" +
                "mSwimlane=" + mSwimlane.getId() +
                '}';
    }

    @Override
    public ModuleDataType getDataType() {
        return mSwimlane.handleAsHeroBanner()
                ? ModuleDataType.GALLERY : ModuleDataType.MIXED;
    }

    @Override
    public ModuleCardVariant getCardVariant() {
        return ModuleCardVariant.A1;
    }

    @Nullable
    @Override
    public ModuleDataSource getDataSource() {
        return mDataSource;
    }

    private final ModuleDataSource mDataSource = new ModuleDataSource() {
        @Override
        public ModuleDataSourceService getService() {
            return ModuleDataSourceService.TV;
        }

        @Override
        public ModuleDataSourceMethod getMethod() {
            return ModuleDataSourceMethod.TV_PROGRAM_INDEX;
        }

        @Nullable
        @Override
        public ModuleDataSourceParams getParams() {
            return mParams;
        }
    };

    protected static class SwimlaneModuleDataSourceParams implements ModuleDataSourceParams {
        private final SwimLaneProximus mSwimLane;
        private final int[] mSize;

        public SwimlaneModuleDataSourceParams(SwimLaneProximus swimLane) {
            this(swimLane, true);
        }

        public SwimlaneModuleDataSourceParams(SwimLaneProximus swimlane, boolean applySizeLimit) {
            mSwimLane = swimlane;
            mSize = applySizeLimit ? new int[]{swimlane.getItemDisplayLimit()} : null;
        }

        @Nullable
        @Override
        public String getSort(String key) {
            switch (key) {
                default:
                    return null;
                case ModuleSortOption.Mixed.SortBy.NAME:
                    return mSwimLane.getCategoryItemSorting();
            }
        }

        @Override
        public int[] getSize() {
            return mSize;
        }
    }
}
