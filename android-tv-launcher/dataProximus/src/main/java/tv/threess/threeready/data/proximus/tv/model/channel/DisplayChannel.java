package tv.threess.threeready.data.proximus.tv.model.channel;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Model class for display channel item used for getting correct channel number info.
 *
 * Created by Noemi Dali on 03.06.2020.
 */
@XmlRootElement(name = "Channel")
public class DisplayChannel implements Serializable {

    @XmlElement(name = "ref")
    private String channelId;

    @XmlElement(name = "nr")
    private int channelNumber;

    public String getChannelId() {
        return channelId;
    }

    public int getChannelNumber() {
        return channelNumber;
    }
}
