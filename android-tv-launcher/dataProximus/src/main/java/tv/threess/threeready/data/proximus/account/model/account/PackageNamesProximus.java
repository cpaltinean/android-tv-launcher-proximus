package tv.threess.threeready.data.proximus.account.model.account;

import java.io.Serializable;

/**
 * Proximus package external id types.
 *
 * Created by Daniela Toma on 04/21/2020.
 */
public interface PackageNamesProximus extends Serializable {
    String REPLAY_EXTERNAL_ID = "TVREPLAY";
    String REPLAY_PLUS_EXTERNAL_ID = "TVREPLAYPL";
    String PLTV_EXTERNAL_ID = "PLTV";
}
