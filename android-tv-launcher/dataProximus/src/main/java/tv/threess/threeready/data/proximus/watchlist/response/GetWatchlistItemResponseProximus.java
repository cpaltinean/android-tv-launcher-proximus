package tv.threess.threeready.data.proximus.watchlist.response;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import tv.threess.threeready.api.account.watchlist.IWatchListResponseItem;
import tv.threess.threeready.api.generic.model.WatchlistType;
import tv.threess.threeready.data.generic.model.BaseResponse;
import tv.threess.threeready.data.proximus.watchlist.adapter.WatchlistTypeAdapterProximus;

/**
 * POJO for watchlist item response from the watchlist microservice
 *
 * @author Andor Lukacs
 * @since 21/10/2020
 */
public class GetWatchlistItemResponseProximus implements BaseResponse, IWatchListResponseItem {

    @SerializedName("watchlist_item")
    public WatchlistItem watchlistItem;

    public static class WatchlistItem implements BaseResponse {
        @SerializedName("id")
        private String id;

        @SerializedName("added_at")
        private String timestamp;

        @SerializedName("item")
        private Watchlist watchlist;

        @SerializedName("channel_id")
        private String channelId;

        @Override
        public String toString() {
            return "WatchlistItem{" +
                    "id='" + id + '\'' +
                    ", timestamp='" + timestamp + '\'' +
                    ", watchlist=" + watchlist +
                    '}';
        }

        public String getId() {
            return id;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public WatchlistType getType() {
            return watchlist.type;
        }

        public String getItemId() {
            return watchlist.itemId;
        }

        public String getChannelId() {
            return channelId;
        }
    }

    public static class Watchlist {

        @JsonAdapter(WatchlistTypeAdapterProximus.class)
        @SerializedName("item_type")
        private WatchlistType type;

        @SerializedName("item_id")
        private String itemId;

        @Override
        public String toString() {
            return "Watchlist{" +
                    "type=" + type +
                    ", itemId='" + itemId + '\'' +
                    '}';
        }
    }

    @Override
    public String getId() {
        return watchlistItem.getId();
    }

    @Override
    public String getTimestamp() {
        return watchlistItem.getTimestamp();
    }

    @Override
    public WatchlistType getType() {
        return watchlistItem.getType();
    }

    @Override
    public String getItemId() {
        return watchlistItem.getItemId();
    }

    @Override
    public String getChannelId() {
        return watchlistItem.getChannelId();
    }

    @Override
    public String toString() {
        return "WatchlistItemProximus{" +
                "watchlistItem=" + watchlistItem +
                '}';
    }
}
