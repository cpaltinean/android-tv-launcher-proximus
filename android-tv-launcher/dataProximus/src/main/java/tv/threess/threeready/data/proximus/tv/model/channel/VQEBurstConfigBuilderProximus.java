package tv.threess.threeready.data.proximus.tv.model.channel;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Builder class to calculate a construct the VQE override config file.
 *
 * @author Barabas Attila
 * @since 2020.04.07
 */
public class VQEBurstConfigBuilderProximus {
    Map<String, String> mValues = new LinkedHashMap<>();


    /**
     * @param sdBandwidth The bandwidth available for SD profile STB.
     * @param hdBandwidth The bandwidth available for HD profile STB.
     * @param overheadBandwidth The extra bandwidth available for error rapid and rapid channel change.
     * @param hdInterest True if the calculation should be done on a HD profile STB.
     */
    public VQEBurstConfigBuilderProximus(long sdBandwidth, long hdBandwidth, long overheadBandwidth, boolean hdInterest) {
        long maxBWSD, maxBWHD, maxBWSDRCC, maxBWHDRCC;
        if (hdInterest) {
            maxBWSDRCC = overheadBandwidth + hdBandwidth - sdBandwidth;
            maxBWHDRCC = overheadBandwidth + hdBandwidth;
            maxBWSD = (overheadBandwidth + hdBandwidth - 2 * sdBandwidth) / 2 + sdBandwidth;
            maxBWHD = overheadBandwidth + hdBandwidth;
        } else {
            maxBWSDRCC = overheadBandwidth + sdBandwidth;
            maxBWHDRCC = 0;
            maxBWSD = overheadBandwidth + sdBandwidth;
            maxBWHD = 0;
        }

        mValues.put("max_receive_bandwidth_sd_rcc", String.valueOf(maxBWSDRCC * 1000));
        mValues.put("max_receive_bandwidth_hd_rcc", String.valueOf(maxBWHDRCC * 1000));
        mValues.put("max_receive_bandwidth_sd", String.valueOf(maxBWSD * 1000));
        mValues.put("max_receive_bandwidth_hd", String.valueOf(maxBWHD * 1000));
    }

    /**
     * Enable or disable the VQE error repairing.
     * @param enable True if enable, false otherwise.
     * @return The builder to build upon.
     */
    public VQEBurstConfigBuilderProximus enableRET(boolean enable) {
        mValues.put("error_repair_enable", String.valueOf(enable));
        return this;
    }

    /**
     * Enable or disable the VQE rapid channel change repairing.
     * @param enable True if enable, false otherwise.
     * @return The builder to build upon.
     */
    public VQEBurstConfigBuilderProximus enableRCC(boolean enable) {
        mValues.put("rcc_enable", String.valueOf(enable));
        return this;
    }

    /**
     * The time until the server keeps the data for error repair re-transmission.
     * @param networkBuffer The time in milliseconds.
     * @return The builder to build upon.
     */
    public VQEBurstConfigBuilderProximus networkBuffer(int networkBuffer) {
        mValues.put("jitter_buff_size", String.valueOf(networkBuffer));
        return this;
    }

    /**
     * @return Build the config override;
     */
    public String build() {
        StringBuilder sb = new StringBuilder();
        for (String key : mValues.keySet()) {
            sb.append(key);
            sb.append(" = ");
            sb.append(mValues.get(key));
            sb.append(";\n");
        }

        // Remove the last new line.
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }

        return sb.toString();
    }


}
