package tv.threess.threeready.data.proximus.home.model.selector;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.api.tv.TvChannelCacheType;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Data source selector for replay channel wall.
 * For now we have only two menu selectors, like "All" and "Favorites"
 *
 * @author David Bondor
 * @since 4.28.2022
 */
public class ReplayChannelDataSourceSelector extends DataSourceSelector {

    public ReplayChannelDataSourceSelector() {
        super(generateSelectorOptions());
    }

    private static List<SelectorOption> generateSelectorOptions() {
        List<SelectorOption> selectorOptions = new ArrayList<>();
        selectorOptions.add(new ChannelMenuSelector(TvChannelCacheType.ALL, TranslationKey.CONTENT_WALL_CHANNELS_ALL, true));
        selectorOptions.add(new ChannelMenuSelector(TvChannelCacheType.FAVORITE, TranslationKey.CONTENT_WALL_CHANNELS_FAVORITES, TranslationKey.EMPTY_CONTENT_PAGES_FAVORITES, false));
        return selectorOptions;
    }

    @Override
    public DisplayType getDisplayType() {
        return DisplayType.Menu;
    }
}