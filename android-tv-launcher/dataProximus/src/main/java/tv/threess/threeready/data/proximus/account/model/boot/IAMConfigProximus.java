package tv.threess.threeready.data.proximus.account.model.boot;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Proximus boot config options for TIAMS.
 *
 * @author Barabas Attila
 * @since 2020.03.17
 */
public class IAMConfigProximus implements Serializable {

    @SerializedName("audience")
    private String mAudience;

    @SerializedName("redirectUri")
    private String mRedirectUrl;

    @SerializedName("scope")
    private List<String> mScopes;

    @SerializedName("clientRegistrationUri")
    private String mClientRegistrationUrl;

    @SerializedName("clientCredentialGrantUri")
    private String mAccessTokeUri;

    @SerializedName("keyId_JWT")
    private String mJWTKeyId;

    @SerializedName("keyId_SSA")
    private String mSSAKeyId;

    public String getAudience() {
        return mAudience;
    }

    public String getRedirectUrl() {
        return mRedirectUrl;
    }

    public List<String> getScopes() {
        return mScopes;
    }

    public String getRegisterUrl() {
        return mClientRegistrationUrl;
    }

    public String getAccessTokenUrl() {
        return mAccessTokeUri;
    }

    public String getJWTKeyId() {
        return mJWTKeyId;
    }

    public String getSSAKeyId() {
        return mSSAKeyId;
    }

}
