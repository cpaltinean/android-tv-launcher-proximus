package tv.threess.threeready.data.proximus.pvr;


import android.app.Application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.FeatureControl;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.BingeWatchingInfo;
import tv.threess.threeready.api.generic.model.BookmarkType;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.generic.model.ISeason;
import tv.threess.threeready.api.generic.model.WatchlistType;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.pvr.exception.PvrException;
import tv.threess.threeready.api.pvr.model.IBaseRecordingSeries;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingQuota;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.pvr.model.SeriesRecordingDetailModel;
import tv.threess.threeready.api.pvr.model.SeriesRecordingStatus;
import tv.threess.threeready.api.pvr.model.SingleRecordingStatus;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.account.AccountLocalCache;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.generic.observable.WaitForUpdateObservable;
import tv.threess.threeready.data.proximus.generic.model.BTAErrorCodesProximus;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingAddResponseProximus;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingDeleteResponse;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingDeleteSeriesResponse;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingScheduleSeriesResponse;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingStopResponse;
import tv.threess.threeready.data.proximus.pvr.observable.NextRecordingEpisodeObservable;
import tv.threess.threeready.data.pvr.BasePvrRepository;
import tv.threess.threeready.data.pvr.PvrCache;
import tv.threess.threeready.data.pvr.observable.RecordedCategoryContentObservable;
import tv.threess.threeready.data.pvr.observable.RecordingStatusObservable;
import tv.threess.threeready.data.pvr.observable.ScheduledCategoryContentObservable;
import tv.threess.threeready.data.pvr.observable.SeriesDetailModelObservable;
import tv.threess.threeready.data.pvr.observable.SeriesRecordingObservable;
import tv.threess.threeready.data.pvr.observable.SeriesRecordingStatusObservable;
import tv.threess.threeready.data.tv.TvService;

/**
 * Implementation of the Proximus specific pvr handler methods.
 *
 * @author Denisa Trif
 * @since 2018.09.14
 */
public class PvrRepositoryProximus extends BasePvrRepository implements PvrRepository, Component {

    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);
    private final PvrProxyProximus mPvrProxy = Components.get(PvrProxyProximus.class);
    private final PvrCache mPvrCache = Components.get(PvrCache.class);
    private final AccountLocalCache mAccountCache = Components.get(AccountLocalCache.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);

    public PvrRepositoryProximus(Application application) {
        super(application);
    }

    @Override
    public boolean isPVREnabled(IBroadcast broadcast) {
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        return Components.get(FeatureControl.class).isPVREnabled()
                && (channel != null && channel.isNPVREnabled()) && broadcast.isNPVREnabled();
    }

    @Override
    public Completable waitRecordingSync() {
        return Completable.fromObservable(
                Observable.create(new WaitForUpdateObservable(mApp, pvrUpdater)));
    }

    @Override
    public Completable scheduleRecording(IBroadcast broadcast) {
        return Completable.fromAction(() -> {
            if (!isPVREnabled(broadcast)) {
                throw new PvrException("PVR not enabled for broadcast " + broadcast.getId());
            }

            RecordingStatus recordingStatus = getStatus(broadcast);

            boolean isPartiallyRecorded =
                    broadcast.isLive() && recordingStatus.getRecording() != null && recordingStatus.getSingleRecordingStatus() == SingleRecordingStatus.COMPLETED;

            if (isPartiallyRecorded) {
                mPvrProxy.deleteRecordings(recordingStatus.getRecording().getRecordingId());
                mPvrCache.deleteRecordingsByIds(recordingStatus.getRecording().getRecordingId());
            }

            try {
                RecordingAddResponseProximus response = mPvrProxy.scheduleRecording(broadcast.getId(), broadcast.getChannelId());
                mPvrCache.scheduleRecording(broadcast, response.getRecording());
            } catch (BackendException ex) {
                if (BTAErrorCodesProximus.EPG_OUT_OF_DATE.equalsIgnoreCase(ex.getErrorCode())) {
                    forceUpdateBroadcasts();
                }
                throw ex;
            }
        });
    }

    @Override
    public Completable scheduleSeriesRecording(IBroadcast broadcast) {
        return Completable.fromAction(() -> {
            IRecordingSeries series = mPvrCache.getSeriesRecording(broadcast.getSeriesId());

            if (series != null) {
                deleteOngoingRecordings(series);
            }

            if (!isPVREnabled(broadcast)) {
                throw new PvrException("PVR not enabled for broadcast " + broadcast.getId());
            }

            if (!broadcast.isEpisode()) {
                throw new PvrException("Couldn't schedule series recording. Not an episode.");
            }

            try {
                RecordingScheduleSeriesResponse response = mPvrProxy.scheduleSeriesRecording(broadcast.getId(), broadcast.getChannelId());
                mPvrCache.scheduleSeriesRecording(broadcast, response.getSeriesRecordingId(), response.getRecordings());
            } catch (BackendException ex) {
                if (BTAErrorCodesProximus.EPG_OUT_OF_DATE.equalsIgnoreCase(ex.getErrorCode())) {
                    forceUpdateBroadcasts();
                }
                throw ex;
            }
        });
    }

    @Override
    public Completable scheduleSeriesRecording(IRecordingSeries series) {
        return Single.fromCallable(() -> {
            IBroadcast firstEpisode = series.getFirstOngoingOrFutureEpisode();
            if (firstEpisode == null) {
                throw new PvrException("Couldn't schedule series recording.");
            }

            deleteOngoingRecordings(series);
            return firstEpisode;
        }).flatMapCompletable((Function<IBroadcast, Completable>) this::scheduleSeriesRecording);
    }

    @Override
    public Completable deleteSeriesRecording(IRecordingSeries series) {
        return Completable.fromAction(() -> {
            List<String> recordingIds = new ArrayList<>();
            Set<String> broadcastIds = new HashSet<>();

            for (ISeason<IRecording> season : series.getSeasons()) {
                for (IRecording recording : season.getEpisodes()) {
                    if (recording.isRecordingFinished()) {
                        recordingIds.add(recording.getRecordingId());
                        broadcastIds.add(recording.getId());
                    }
                }
            }

            String[] recordingIdArray = recordingIds.toArray(new String[0]);

            mPvrProxy.deleteRecordings(recordingIdArray);
            mPvrCache.deleteRecordings(series.getId(), recordingIdArray);
            mAccountCache.deleteBookmarks(BookmarkType.Recording, broadcastIds.toArray(new String[0]));
        });
    }

    @Override
    public Completable cancelRecording(IBroadcast broadcast) {
        return Completable.fromAction(() -> {
            List<IRecording> recordings = mPvrCache.getRecordings(broadcast.getId());

            if (recordings == null) {
                throw new PvrException("Recording doesn't exist");
            }
            for (IRecording recording : recordings) {
                if (recording.isLiveRecording()) {
                    long now = System.currentTimeMillis();
                    mPvrProxy.stopRecording(recording.getRecordingId());
                    mPvrCache.updateRecordingEnd(recording, now);
                } else {
                    RecordingDeleteResponse response = mPvrProxy.cancelRecording(recording.getRecordingId());
                    mPvrCache.deleteRecordings(broadcast, response.getRecordingIDs());
                }
            }
        });
    }

    @Override
    public Completable cancelSeriesRecording(IBroadcast broadcast) {
        return Completable.fromAction(() -> cancelSeriesRecording(broadcast, true));
    }

    @Override
    public Completable cancelSeriesRecording(IRecordingSeries series) {
        return Single.fromCallable(() -> {
            IBroadcast firstEpisode = series.getFirstEpisode();
            if (firstEpisode == null) {
                throw new PvrException("Couldn't cancel series recording.");
            }
            return firstEpisode;
        }).flatMapCompletable(this::cancelSeriesRecording);
    }

    @Override
    public Completable deleteAndCancelRecording(IRecordingSeries series) {
        return Completable.fromAction(() -> {
            // First, cancel all future episodes
            IBroadcast firstEpisode = series.getFirstOngoingOrFutureEpisode();
            if (firstEpisode != null && mPvrCache.getSeriesStatusById(series.getId()) == SeriesRecordingStatus.SCHEDULED) {
                cancelSeriesRecording(firstEpisode, false);
            }

            List<String> airingsIds = new ArrayList<>();

            //Second is to get all recordings, partial included with the same broadcastId
            //And stop any ongoing episode
            List<IRecording> recordings = mPvrCache.getRecordingsBySeriesId(series.getId());
            for (IRecording rec : ArrayUtils.notNull(recordings)) {
                if (rec != null) {
                    if (rec.isLiveRecording()) {
                        RecordingStopResponse stopEpisodeResponse = mPvrProxy.stopRecording(rec.getRecordingId());
                        mPvrCache.updateRecordingEnd(rec, stopEpisodeResponse.getStopTimeMarker());
                    }
                    airingsIds.add(rec.getRecordingId());
                }
            }

            // Third is to delete all remaining recorded episodes, ongoing episodes included
            RecordingDeleteResponse recordingDeleteResponse = mPvrProxy.deleteRecordings(airingsIds.toArray(new String[0]));
            mPvrCache.deleteRecordings(series.getId(), recordingDeleteResponse.getRecordingIDs());
        });
    }

    @Override
    public Completable deleteRecording(IRecording recording) {
        return Completable.fromAction(() -> deleteRecording(recording, true))
                .toSingle(() -> mAccountCache.isInWatchlist(recording.getId()))
                .flatMapCompletable(isInWatchlist -> {
                    if (isInWatchlist) {
                        return mAccountRepository.removeFromWatchlist(recording.getId(), WatchlistType.Broadcast);
                    }
                    return Completable.complete();
                });
    }

    @Override
    public Observable<RecordingStatus> getRecordingStatus(IBroadcast broadcast) {
        return Observable.create(new RecordingStatusObservable(mApp, broadcast));
    }

    @Override
    public Observable<SeriesRecordingStatus> getSeriesRecordingStatus(IBaseRecordingSeries series) {
        return Observable.create(new SeriesRecordingStatusObservable(mApp, series));
    }

    @Override
    public Observable<IRecordingSeries> getSeriesRecording(String seriesId) {
        return Observable.create(new SeriesRecordingObservable(mApp, seriesId));
    }

    @Override
    public Observable<SeriesRecordingDetailModel> getSeriesRecordingDetailModel(String seriesId) {
        return Observable.create(new SeriesDetailModelObservable(mApp, seriesId));
    }

    @Override
    public Observable<BingeWatchingInfo<IRecordingSeries, IRecording>> getNextEpisode(IRecording episode, IRecordingSeries series) {
        return Observable.create(new NextRecordingEpisodeObservable(mApp, episode, series));
    }

    @Override
    public Observable<ModuleData<IBaseContentItem>> getRecordedCategoryContent(final ModuleConfig moduleConfig, final int start, final int count) {
        return Observable.create(new RecordedCategoryContentObservable(mApp, moduleConfig, start, count));
    }

    @Override
    public Observable<ModuleData<IBaseContentItem>> getScheduledCategoryContent(final ModuleConfig moduleConfig, final int start, final int count) {
        return Observable.create(new ScheduledCategoryContentObservable(mApp, moduleConfig, start, count));
    }

    @Override
    public Observable<IRecordingQuota> getSTCRecordingQuota() {
        return Observable.create(emitter -> {
            emitter.onNext(mPvrProxy.getSTCRecordingQuota());
            emitter.onComplete();
        });
    }

    /**
     * Cancel the series recording of the broadcast.
     * No notification is triggered while cancelling. See {@link PvrRepositoryProximus#cancelSeriesRecording(IBroadcast)} for option with notification.
     *
     * @param broadcast the broadcast for which
     *                  the series recording should be canceled recordings should be canceled.
     * @throws PvrException in case of errors.
     */
    private void cancelSeriesRecording(IBroadcast broadcast, boolean notifyChange) throws PvrException, IOException {
        if (!broadcast.isEpisode()) {
            throw new PvrException("Couldn't cancel series recording. Not an episode.");
        }

        String userSeriesId = mPvrCache.getUserSeriesIdBySeriesId(broadcast.getSeriesId());
        if (userSeriesId == null) {
            throw new PvrException("Series is not scheduled, cannot cancel.");
        }

        RecordingDeleteSeriesResponse response = mPvrProxy.deleteSeriesRecording(userSeriesId);

        //Delete future recording items
        mPvrCache.deleteRecordingsFromDbBySystemId(response.getDeletedEpisodes().toArray(new String[0]));

        //Delete series from db
        mPvrCache.deleteSeriesFromDb(broadcast.getSeriesId(), notifyChange);

        // Check if status updated.
        RecordingStatus status = getStatus(broadcast);

        if (status.getSeriesRecordingStatus() != SeriesRecordingStatus.NONE) {
            throw new PvrException("Couldn't cancel series recording.");
        }
    }

    /**
     * Delete an already existing recording for the given broadcast.
     *
     * @param recording item what should be deleted.
     * @throws PvrException in case of errors.
     */
    private void deleteRecording(IRecording recording, boolean notifyChanges) throws PvrException, IOException {
        if (recording == null) {
            throw new PvrException("Recording doesn't exist, cannot delete");
        }

        List<String> recordingIds = mPvrCache.getRecordingsOnSameBroadcast(recording);

        if (recordingIds == null || recordingIds.isEmpty()) {
            return;
        }

        String[] recordingIdsArray = recordingIds.toArray(new String[0]);

        mPvrProxy.deleteRecordings(recordingIdsArray);

        if (notifyChanges) {
            mPvrCache.deleteRecordings(recording, recordingIdsArray);
        } else {
            mPvrCache.deleteRecordingsByIds(recordingIdsArray);
        }
        mAccountCache.deleteBookmarks(BookmarkType.Recording, recording.getId());
    }

    /**
     * Delete all ongoing partial recordings
     *
     * @param series The series from which the partial recordings should be deleted.
     * @throws IOException in case of errors.
     */
    private void deleteOngoingRecordings(IRecordingSeries series) throws IOException {
        List<String> recordingIds = new ArrayList<>();

        for (ISeason<IRecording> season : series.getSeasons()) {
            for (IRecording recording : season.getEpisodes()) {
                if (recording.isLive() && recording.isRecordingFinished()) {
                    recordingIds.add(recording.getRecordingId());
                }
            }
        }

        if (!recordingIds.isEmpty()) {
            String[] recordingIdArray = recordingIds.toArray(new String[0]);
            mPvrProxy.deleteRecordings(recordingIdArray);
            mPvrCache.deleteRecordingsByIds(recordingIdArray);
        }
    }

    /**
     * @return The cached recording status.
     */
    private RecordingStatus getStatus(IBroadcast broadcast) {
        SingleRecordingStatus singleRecordingStatus = SingleRecordingStatus.NONE;

        // Single recording status.
        IRecording recordingItem = mPvrCache.getRecording(broadcast.getId());
        if (recordingItem != null) {
            singleRecordingStatus = recordingItem.getRecordedStatus();
        }

        // Series recording status.
        SeriesRecordingStatus seriesRecordingStatus = SeriesRecordingStatus.NONE;
        if (broadcast.isEpisode()) {
            seriesRecordingStatus = mPvrCache.getSeriesStatusById(broadcast.getSeriesId());
        }

        return new RecordingStatus(recordingItem, singleRecordingStatus, seriesRecordingStatus);
    }

    /**
     * Force update broadcasts if we get an epg out of date exception from backend.
     * {@link BTAErrorCodesProximus}
     */
    private void forceUpdateBroadcasts() {
        Settings.batchEdit()
                .remove(Settings.lastBroadcastSyncTime)
                .remove(Settings.lastBaseBroadcastSyncTime)
                .persistNow();
        BaseWorkManager.start(mApp, TvService.buildBroadcastUpdateIntent());
    }
}
