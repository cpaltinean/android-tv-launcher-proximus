package tv.threess.threeready.data.proximus.tv.model.broadcast.response;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.data.proximus.tv.model.BaseBroadcastProximus;

/**
 * Proximus' implementation of the broadcast item.
 * Used to store info for linear assets.
 *
 * @author Andor Lukacs
 * @since 10/2/18
 */
public class TvBroadcastProximus extends BaseBroadcastProximus implements IBroadcast {
    private static final String NO_INFORMATION_BROADCAST_PROGRAM_ID = "-1";

    private final MetaInfoProximus<BroadcastMetaInfoKeyProximus> mBroadcastMetaInfo;

    public TvBroadcastProximus(MetaInfoProximus<ProgramMetaInfoKeyProximus> programMetaInfo,
                               MetaInfoProximus<BroadcastMetaInfoKeyProximus> broadcastMetaInfo) {
        super(programMetaInfo);
        mBroadcastMetaInfo = broadcastMetaInfo;
    }

    @Override
    public String getId() {
        //in case we are not getting data from the backend, generate an event id based on channel id and start time (otherwise only the last will survive)
        if (Objects.equals(getProgramId(), NO_INFORMATION_BROADCAST_PROGRAM_ID)) {
            return getChannelId() + "-" + getStart();
        }
        //use the default
        return buildBroadcastId(getChannelId(), mBroadcastMetaInfo.getString(BroadcastMetaInfoKeyProximus.ScheduleTrailID));
    }

    @Override
    public String getChannelId() {
        return mBroadcastMetaInfo.getString(BroadcastMetaInfoKeyProximus.ChannelExtID);
    }

    @Override
    public long getStart() {
        return mBroadcastMetaInfo.getLong(BroadcastMetaInfoKeyProximus.ProgramScheduleStart);
    }

    @Override
    public long getEnd() {
        return getStart() + TimeUnit.MINUTES.toMillis(
                mBroadcastMetaInfo.getLong(BroadcastMetaInfoKeyProximus.Duration));
    }

    @Override
    public boolean isBlackListed() {
        if (Objects.equals(getProgramId(), NO_INFORMATION_BROADCAST_PROGRAM_ID)) {
            return true;
        }
        return super.isBlackListed();
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof TvBroadcastProximus) {
            return Objects.equals(getId(), ((TvBroadcastProximus)obj).getId());
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getId(),
                getChannelId(), isNPVREnabled(), getStart(), getEnd());
    }

    @Override
    public boolean isNextToLive() {
        return false;
    }

    public String toString() {
        return "TvBroadcastProximus{Id='" + getId() + '\'' +
                ", Start='" + Log.timestamp(getStart()) + '\'' +
                ", End='" + Log.timestamp(getEnd()) + '\'' +
                ", Title='" + getTitle() + "'}";
    }
}
