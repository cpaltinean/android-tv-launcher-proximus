package tv.threess.threeready.data.proximus.log.reporter;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemClock;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tv.threess.threeready.api.account.setting.MassStorage;
import tv.threess.threeready.api.account.setting.MemoryCache;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.generic.receivers.BaseBroadcastReceiver;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.ReporterInfo;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.log.model.VodItemType;
import tv.threess.threeready.api.vod.model.CurrencyType;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.proximus.log.model.LogDetailsProximus;

/**
 * Event reporter to create, format and send UI related events to proximus data collection.
 *
 * @author Barabas Attila
 * @since 10/19/21
 */
public class UIEventReporterProximus extends EventReporterProximus<UILogEvent, UILogEvent.Detail> {
    private static final String TAG = Log.tag(UIEventReporterProximus.class);

    private static final long POWER_CYCLE_TYPE_SUSPICIOUS_THRESHOLD = TimeUnit.SECONDS.toMillis(600);

    private static final String KEY_CURRENCY_UNIT = "currency_unit";
    private static final String KEY_APP_IDENTIFIER = "app_identifier";
    private static final String KEY_APP_DURATION = "app_duration";
    private static final String KEY_SEARCH_STRING = "search_string";

    private static final String KEY_BOOT_REASON = "boot_reason";
    private static final String KEY_PREVIOUS_STATE_UPTIME = "previous_state_uptime";
    private static final String KEY_PRE_CYCLE_STATE = "pre_cycle_state";
    private static final String KEY_STARTUP_TIME = "startup_time";
    private static final String KEY_POST_CYCLE_STATE = "post_cycle_state";
    private static final String KEY_POWER_CYCLE_TYPE = "power_cycle_type";
    private static final String KEY_PERIOD_OF_INACTIVITY = "period_of_inactivity";
    private static final String KEY_LAST_KNOWN_HEART_BEAT = "last_known_heart_beat";
    private static final String KEY_FIRST_SIGN_OF_LIFE = "first_sign_of_life";
    private static final String KEY_FOCUS_PAGE_NAME = "focus_page_name";
    private static final String KEY_FOCUS_KEY_POINT = "focus_key_point";
    private static final String KEY_FOCUS_KEY_REFERENCE = "focus_key_reference";
    private static final String KEY_FOCUS_SUB_POINT = "focus_sub_point";
    private static final String KEY_FOCUS_SUB_REFERENCE = "focus_sub_reference";
    private static final String KEY_SELECTION_PAGE_NAME = "selection_page_name";
    private static final String KEY_SELECTION_KEY_POINT = "selection_key_point";
    private static final String KEY_SELECTION_KEY_REFERENCE = "selection_key_reference";
    private static final String KEY_SELECTION_SUB_POINT = "selection_sub_point";
    private static final String KEY_SELECTION_SUB_REFERENCE = "selection_sub_reference";


    private static final String VALUE_HOTKEY = "HOTKEY";
    private static final String VALUE_TV_GUIDE = "TV-GUIDE";
    private static final String VALUE_VOICECONTROL = "VOICECONTROL";
    private static final String VALUE_MENU = "MENU";
    private static final String VALUE_MEDIA = "Media";

    private static final String VALUE_PVR = "PVR";
    private static final String VALUE_NPVR_USER = "NPVR-USER";
    private static final String VALUE_RECORDING = "RECORDING";
    private static final String VALUE_DELETED = "DELETED";
    private static final String VALUE_CANCELLED = "CANCELLED";
    private static final String VALUE_USERSTARTED = "USERSTARTED";
    private static final String VALUE_USERSTOPPED = "USERSTOPPED";
    private static final String VALUE_SCHEDULED = "SCHEDULED";

    private static final String VALUE_UNINSTALLED = "UNINSTALLED";
    private static final String VALUE_INSTALLED = "INSTALLED";
    private static final String VALUE_UPDATED = "UPDATED";
    private static final String VALUE_LAUNCHED = "LAUNCHED";
    private static final String VALUE_EXITED = "EXITED";

    private static final String VALUE_VOD_RENTAL = "VOD-RENTAL";
    private static final String VALUE_EUR = "EUR";
    private static final String VALUE_CREDIT = "CREDIT";

    private static final Pattern STANDBY_STATE_PATTERN = Pattern.compile("([^;]*);([0-9]*)");
    private static final String STANDBY_REASON_PROPERTY = "sys.standby.reason";
    private static final String STANDBY_STATE_POWER_BUTTON = "POWER_BUTTON"; // POWER_BUTTON;1559287880705
    private static final String STANDBY_STATE_SLEEP_TIMEOUT = "TIMEOUT"; // TIMEOUT;1559244374115
    private static final String VALUE_ACT = "ACT";
    private static final String VALUE_FORCED = "FORCED";
    private static final String VALUE_NORMAL = "NORMAL";
    private static final String VALUE_SUSPICIOUS = "SUSPICIOUS";

    private static final String VALUE_HOME = "HOME";
    private static final String VALUE_RADIO = "RADIO";
    private static final String VALUE_OK = "OK";
    private static final String VALUE_BACK = "BACK";
    private static final String VALUE_TIMEOUT = "TIMEOUT";

    private Event<UILogEvent, UILogEvent.Detail> mLastPageEvent;
    private UILog.OpenedBy mOpenedBy = UILog.OpenedBy.Restart;

    public UIEventReporterProximus(Context context, ReporterInfo reporterInfo) {
        super(context, reporterInfo);
        onTimeSet.registerReceiver(context);
    }

    @Override
    public boolean canSend(Event<UILogEvent, UILogEvent.Detail> event) {
        return UILogEvent.DOMAIN.equals(event.getDomain());
    }

    @Override
    protected LogDetailsProximus formatPayload(Event<UILogEvent, UILogEvent.Detail> event) {
        if (!UILogEvent.DOMAIN.equals(event.getDomain())) {
            throw new IllegalArgumentException("Invalid UI domain: " + event.getDomain());
        }

        LogDetailsProximus payload = new LogDetailsProximus();
        UILogEvent eventKind = event.getName(UILogEvent.Unknown);

        switch (eventKind) {
            default:
                return null;

            case PageSelection:
                // Update last page selection info.
                if (mLastPageEvent != null) {
                    UILog.Page page = event.getDetail(UILogEvent.Detail.Page, UILog.Page.Unknown);
                    if (page != UILog.Page.Unknown) {
                        mLastPageEvent.putDetail(UILogEvent.Detail.Page, page);
                    }

                    String pageName = event.getDetail(UILogEvent.Detail.PageName);
                    if (pageName != null) {
                        mLastPageEvent.putDetail(UILogEvent.Detail.PageName, pageName);
                        mLastPageEvent.remove(UILogEvent.Detail.PageId.name());
                        mLastPageEvent.remove(UILogEvent.Detail.FocusName.name());
                        mLastPageEvent.remove(UILogEvent.Detail.FocusId.name());
                        mLastPageEvent.remove(UILogEvent.Detail.NeedReset.name());
                    }

                    String pageId = event.getDetail(UILogEvent.Detail.PageId);
                    if (pageId != null) {
                        mLastPageEvent.putDetail(UILogEvent.Detail.PageId, pageId);
                    }

                    String focusName = event.getDetail(UILogEvent.Detail.FocusName);
                    if (focusName != null) {
                        mLastPageEvent.putDetail(UILogEvent.Detail.FocusName, focusName);
                    }

                    String focusId = event.getDetail(UILogEvent.Detail.FocusId);
                    if (focusId != null) {
                        mLastPageEvent.putDetail(UILogEvent.Detail.FocusId, focusId);
                    }
                } else {
                    mLastPageEvent = event;
                }

                mOpenedBy = event.getDetail(UILogEvent.Detail.OpenedBy, UILog.OpenedBy.Ok);

                // Not reported directly.
                return null;

            case PageOpen:
                if (mLastPageEvent != null && event.hasDetail(UILogEvent.Detail.NeedReset)) {
                    boolean isResetNeed = event.getDetail(UILogEvent.Detail.NeedReset, false);
                    mLastPageEvent.putDetail(UILogEvent.Detail.NeedReset, isResetNeed);
                }

                if (isSamePage(event, mLastPageEvent)) {
                    // Don't report the same event twice.
                    return null;
                }

                if (isNavigationSessionResetNeeded()) {
                    // Reset navigation session
                    if (mLastPageEvent != null) {
                        UILog.Page page = mLastPageEvent.getDetail(UILogEvent.Detail.Page, UILog.Page.Unknown);
                        if (page != UILog.Page.StandBy && page != UILog.Page.Restart) {
                            mLastPageEvent = null;
                        }
                    }
                    MemoryCache.NavigationSessionStart.edit().put(System.currentTimeMillis());
                    MemoryCache.NavigationSessionCounter.edit().put(0);
                } else {
                    int navCounter = MemoryCache.NavigationSessionCounter.get(0);
                    MemoryCache.NavigationSessionCounter.edit().put(navCounter + 1);
                }

                payload.add(KEY_TYPE, VALUE_INTERACTION);
                payload.add(KEY_ACTION, getPageOpenAction(mOpenedBy));
                payload.add(KEY_SUB_TYPE, VALUE_MENU);
                payload.add(KEY_ACTION_TYPE, VALUE_TRANSIENT);

                // Focus page part
                UILog.Page currentPage = event.getDetail(UILogEvent.Detail.Page, UILog.Page.Unknown);
                payload.add(KEY_FOCUS_PAGE_NAME, getPageName(currentPage));
                payload.add(KEY_FOCUS_KEY_POINT, event.getDetail(UILogEvent.Detail.PageName, NONE));
                payload.add(KEY_FOCUS_KEY_REFERENCE, event.getDetail(UILogEvent.Detail.PageId, NONE));
                payload.add(KEY_FOCUS_SUB_POINT, event.getDetail(UILogEvent.Detail.FocusName, NONE));
                payload.add(KEY_FOCUS_SUB_REFERENCE, event.getDetail(UILogEvent.Detail.FocusId, NONE));

                // Selection part
                if (mLastPageEvent != null) {
                    payload.add(KEY_SELECTION_PAGE_NAME, getPageName(mLastPageEvent.getDetail(
                            UILogEvent.Detail.Page, UILog.Page.Unknown)));
                    payload.add(KEY_SELECTION_KEY_POINT, mLastPageEvent.getDetail(UILogEvent.Detail.PageName, NONE));
                    payload.add(KEY_SELECTION_KEY_REFERENCE, mLastPageEvent.getDetail(UILogEvent.Detail.PageId, NONE));
                    payload.add(KEY_SELECTION_SUB_POINT, mLastPageEvent.getDetail(UILogEvent.Detail.FocusName, NONE));
                    payload.add(KEY_SELECTION_SUB_REFERENCE, mLastPageEvent.getDetail(UILogEvent.Detail.FocusId, NONE));
                }

                saveLastPageInCache(currentPage);
                addNavigationSessionPart(payload);
                mLastPageEvent = event;
                mOpenedBy = UILog.OpenedBy.Ok;
                break;

            case Started:
                if (!addPowerCycleParts(payload)) {
                    // the application was restarted, but not the os
                    return null;
                }
                payload.add(KEY_REMOTE_CONNECTED, event.getDetail(UILogEvent.Detail.RemoteConnected, false));
                payload.add(KEY_ACTION, VALUE_HARD_PWR_ON);
                addPowerCycleStartData(payload, event);
                break;

            case Stopped:
                if (!addPowerCycleParts(payload)) {
                    // the application was restarted, but not the os
                    return null;
                }
                payload.add(KEY_REMOTE_CONNECTED, event.getDetail(UILogEvent.Detail.RemoteConnected, false));
                payload.add(KEY_ACTION, VALUE_HARD_PWR_OFF);
                addPowerCycleStopData(payload, event);
                break;

            case Startup:
                payload.add(KEY_CATEGORY, VALUE_EVENT);
                payload.add(KEY_TYPE, VALUE_TECHNICAL);
                payload.add(KEY_SUB_TYPE, VALUE_STARTUP);

                if (isPowerCycle()) {
                    payload.add(KEY_ACTION, VALUE_POWER_CYCLE);
                } else if (mOpenedBy == UILog.OpenedBy.Restart) {
                    payload.add(KEY_ACTION, VALUE_APP_RESTART);
                } else {
                    payload.add(KEY_ACTION, VALUE_STANDBY);
                }

                payload.add(KEY_ACTION_TYPE, VALUE_START);
                payload.add(KEY_STARTUP_TIME, event.getDetail(UILogEvent.Detail.StartupTime, 0));

                break;

            case Standby:
                boolean isRemoteConnected = event.getDetail(UILogEvent.Detail.RemoteConnected, false);
                MassStorage.batchEdit()
                        .put(MassStorage.lastKnownScreenOn, event.getDetail(UILogEvent.Detail.ActiveState, false))
                        .put(MassStorage.lastKnownRemoteConnection, isRemoteConnected)
                        .persistNow();

                payload.add(KEY_REMOTE_CONNECTED, isRemoteConnected);
                payload.add(KEY_CATEGORY, VALUE_EVENT);
                addStandbyPart(payload, event);
                break;

            case Search:
                if (event.getDetail(UILogEvent.Detail.SearchType, UILog.SearchType.class) != UILog.SearchType.GA) {
                    // exclude local search
                    return null;
                }
                if (event.getDetail(UILogEvent.Detail.SearchTermType, UILog.SearchTermType.class) != UILog.SearchTermType.MOVIE) {
                    // send only one search.
                    return null;
                }
                payload.add(KEY_CATEGORY, VALUE_EVENT);
                payload.add(KEY_TYPE, VALUE_VOICECONTROL);
                payload.add(KEY_EVENT_TYPE_OLD, VALUE_VOICECONTROL);
                payload.add(KEY_EVENT_CAT_OLD, VALUE_MEDIA);
                payload.add(KEY_SUB_TYPE, VALUE_MEDIA);
                payload.add(KEY_ACTION_TYPE, VALUE_TRANSIENT);
                payload.add(KEY_ACTION, VALUE_SEARCH);
                payload.add(KEY_SEARCH_STRING, event.getDetail(UILogEvent.Detail.SearchTerm, NONE));
                break;

            case ButtonPress:
                UILog.ButtonType button = event.getDetail(UILogEvent.Detail.ButtonName, UILog.ButtonType.class);
                if (button == null) {
                    android.util.Log.d(TAG, "Skipping ButtonPress event without ButtonName");
                    return null;
                }
                switch (button) {
                    default:
                        // do not log unknown buttons
                        return null;

                    case HOME:
                    case NETFLIX:
                    case RADIO:
                    case SUB:
                    case FASTFORWARD:
                    case REWIND:
                    case PLAYPAUSE:
                    case STOP:
                    case RECORD:
                    case GUIDE:
                        addHotkeyEventPart(payload, button);
                        break;
                }
                break;

            case AppStarted:
                payload.add(KEY_CATEGORY, VALUE_EVENT);
                payload.add(KEY_REMOTE_CONNECTED, event.getDetail(UILogEvent.Detail.RemoteConnected, false));
                switch (event.getDetail(UILogEvent.Detail.StartAction, StartAction.Undefined)) {
                    default:
                        payload.add(KEY_ACTION_TYPE, VALUE_TRANSIENT);
                        break;

                    case Startup:
                    case UsageStats:
                        payload.add(KEY_ACTION_TYPE, VALUE_START);
                        break;
                }
                addAppLaunchPart(payload, event);
                addAppDetails(payload, event);
                break;

            case AppStopped:
                payload.add(KEY_CATEGORY, VALUE_EVENT);
                payload.add(KEY_REMOTE_CONNECTED, event.getDetail(UILogEvent.Detail.RemoteConnected, false));
                addAppExitPart(payload, event);
                addAppDetails(payload, event);
                break;

            case AppUpdated:
                payload.add(KEY_ACTION_TYPE, VALUE_TRANSIENT);
                payload.add(KEY_ACTION, VALUE_UPDATED);
                payload.add(KEY_APP_IDENTIFIER, getIdentifier(event.getTimestamp()));
                addAppDetails(payload, event);
                break;

            case AppInstalled:
                payload.add(KEY_ACTION_TYPE, VALUE_TRANSIENT);
                payload.add(KEY_ACTION, VALUE_INSTALLED);
                payload.add(KEY_APP_IDENTIFIER, getIdentifier(event.getTimestamp()));
                addAppDetails(payload, event);
                break;

            case AppUninstalled:
                payload.add(KEY_ACTION_TYPE, VALUE_TRANSIENT);
                payload.add(KEY_ACTION, VALUE_UNINSTALLED);
                payload.add(KEY_APP_IDENTIFIER, getIdentifier(event.getTimestamp()));
                payload.add(KEY_REMOTE_CONNECTED, event.getDetail(UILogEvent.Detail.RemoteConnected, false));
                addAppDetails(payload, event);
                break;

            case RecordingStarted:
                payload.add(KEY_CATEGORY, VALUE_EVENT);
                payload.add(KEY_ACTION, VALUE_USERSTARTED);
                payload.add(KEY_ACTION_TYPE, VALUE_START);
                addRecordingEventsPart(payload, event, false);
                break;

            case RecordingStopped:
                payload.add(KEY_CATEGORY, VALUE_EVENT);
                payload.add(KEY_ACTION, VALUE_USERSTOPPED);
                payload.add(KEY_ACTION_TYPE, VALUE_STOP);
                addRecordingEventsPart(payload, event, false);
                break;

            case RecordingScheduled:
            case SeriesRecScheduled:
                payload.add(KEY_CATEGORY, VALUE_EVENT);
                payload.add(KEY_ACTION, VALUE_SCHEDULED);
                payload.add(KEY_ACTION_TYPE, VALUE_TRANSIENT);
                addRecordingEventsPart(payload, event, eventKind == UILogEvent.SeriesRecScheduled);
                break;

            case RecordingCanceled:
            case SeriesRecCanceled:
                payload.add(KEY_CATEGORY, VALUE_EVENT);
                payload.add(KEY_ACTION, VALUE_CANCELLED);
                payload.add(KEY_ACTION_TYPE, VALUE_TRANSIENT);
                addRecordingEventsPart(payload, event, eventKind == UILogEvent.SeriesRecCanceled);
                break;

            case RecordingDeleted:
            case SeriesRecDeleted:
                payload.add(KEY_CATEGORY, VALUE_EVENT);
                payload.add(KEY_ACTION, VALUE_DELETED);
                payload.add(KEY_ACTION_TYPE, VALUE_TRANSIENT);
                addRecordingEventsPart(payload, event, eventKind == UILogEvent.SeriesRecDeleted);
                break;

            case VodRental:
                payload.add(KEY_CATEGORY, VALUE_EVENT);
                payload.add(KEY_ACTION_TYPE, VALUE_TRANSIENT);
                payload.add(KEY_TYPE, VALUE_INTERACTION);
                payload.add(KEY_EVENT_TYPE_OLD, VALUE_INTERACTION);
                payload.add(KEY_SUB_TYPE, VALUE_VOD_RENTAL);
                payload.add(KEY_EVENT_CAT_OLD, VALUE_VOD_RENTAL);

                addVodRentalContentSpecificPart(payload, event);
                break;

            case AdDownload:
                payload.add(KEY_TYPE, VALUE_TECHNICAL);
                payload.add(KEY_SUB_TYPE, VALUE_AD_DOWNLOAD);
                payload.add(KEY_ADVERTISEMENT_REFERENCE, event.getDetail(UILogEvent.Detail.AssetId));
                payload.add(KEY_ASSET_HANDLING, BaseReporterProximus.VALUE_UNICAST);
                payload.add(KEY_ASSET_HANDLING_RESULT, event.getDetail(UILogEvent.Detail.AssetHandlingResult));
                payload.add(KEY_ASSET_ATTEMPT_COUNTER, event.getDetail(UILogEvent.Detail.AssetAttemptCounter));
                payload.add(KEY_ASSET_DOWNLOAD_ERROR, event.getDetail(UILogEvent.Detail.DownloadErrorCode));
                payload.add(KEY_ASSET_HANDLING_DURATION, event.getDetail(UILogEvent.Detail.AssetHandlingDuration));
                payload.add(KEY_ASSET_RANDOM_TIMER, event.getDetail(UILogEvent.Detail.AssetRandomTimer));
                payload.add(KEY_ASSET_HANDLING_FAILURE_CAUSE, event.getDetail(UILogEvent.Detail.AssetHandlingFailureCause));
                break;
        }

        addAgentSpecificParts(payload);
        super.addEventCounter(payload);
        payload.add(KEY_EVENT_TIME, event.getTimestamp());

        return payload;
    }

    private void addPowerCycleStartData(LogDetailsProximus payload, Event<UILogEvent, UILogEvent.Detail> event) {
        long firstSignOfLife = event.getTimestamp();
        long lastKnownHeartBeat = event.getDetail(UILogEvent.Detail.LastKnownHeartBeat, firstSignOfLife);

        long startupTime = SystemClock.elapsedRealtime() - (System.currentTimeMillis() - firstSignOfLife);
        long inactiveTime = firstSignOfLife - lastKnownHeartBeat;

        String bootReason = mMwProxy.getBootReason();

        payload.add(KEY_ACTION_TYPE, VALUE_START);
        payload.add(KEY_HDMI_CONN_OLD, mHdmiReceiver.isConnected());
        payload.add(KEY_HDMI_CONN, mHdmiReceiver.isConnected());
        payload.add(KEY_STARTUP_TIME, startupTime);
        payload.add(KEY_LAST_KNOWN_HEART_BEAT, lastKnownHeartBeat);
        payload.add(KEY_PREVIOUS_STATE_UPTIME, MassStorage.lastKnownHeartBeatUptime.get(event.getTimestamp()));
        payload.add(KEY_PERIOD_OF_INACTIVITY, inactiveTime);
        payload.add(KEY_FIRST_SIGN_OF_LIFE, firstSignOfLife);
        payload.add(KEY_POWER_CYCLE_TYPE, getPowerCycleType(firstSignOfLife, lastKnownHeartBeat, bootReason));
        payload.add(KEY_PRE_CYCLE_STATE, getPreCycleState());
        payload.add(KEY_POST_CYCLE_STATE, VALUE_ACT);
        payload.add(KEY_BOOT_REASON, bootReason);
    }

    private void addPowerCycleStopData(LogDetailsProximus payload, Event<UILogEvent, UILogEvent.Detail> event) {
        long firstSignOfLife = event.getCreatedTimestamp();
        long lastKnownHeartBeat = event.getDetail(UILogEvent.Detail.LastKnownHeartBeat, firstSignOfLife);

        String bootReason = mMwProxy.getBootReason();

        payload.add(KEY_ACTION_TYPE, VALUE_STOP);
        payload.add(KEY_HDMI_CONN_OLD, MassStorage.lastKnownHDMIConnection.get(false));
        payload.add(KEY_HDMI_CONN, MassStorage.lastKnownHDMIConnection.get(false));
        payload.add(KEY_STARTUP_TIME, 0);
        payload.add(KEY_LAST_KNOWN_HEART_BEAT, lastKnownHeartBeat);
        payload.add(KEY_PREVIOUS_STATE_UPTIME, 0);
        payload.add(KEY_PERIOD_OF_INACTIVITY, 0);
        payload.add(KEY_FIRST_SIGN_OF_LIFE, 0);
        payload.add(KEY_POWER_CYCLE_TYPE, getPowerCycleType(firstSignOfLife, lastKnownHeartBeat, bootReason));
        payload.add(KEY_PRE_CYCLE_STATE, getPreCycleState());
        payload.add(KEY_POST_CYCLE_STATE, VALUE_ACT);
        payload.add(KEY_BOOT_REASON, bootReason);
    }

    /**
     * add the power cycle specific parameters to the json object
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Power+Cycle+events
     */
    private boolean addPowerCycleParts(LogDetailsProximus payload) {
        if (!isPowerCycle()) {
            return false;
        }
        payload.add(KEY_CATEGORY, VALUE_EVENT);
        payload.add(KEY_TYPE, VALUE_INTERACTION);
        payload.add(KEY_SUB_TYPE, VALUE_POWER_CYCLE);
        payload.add(KEY_EVENT_TYPE_OLD, VALUE_POWER_CYCLE);
        return true;
    }

    /**
     * add standby event parameters to the json object
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Stand+by+events
     */
    private void addStandbyPart(LogDetailsProximus payload, Event<UILogEvent, UILogEvent.Detail> event) {
        if (event.getDetail(UILogEvent.Detail.ActiveState, false)) {
            payload.add(KEY_ACTION_TYPE, VALUE_START);
            payload.add(KEY_ACTION, VALUE_STANDBY_2_ACTIVE);
        } else {
            payload.add(KEY_ACTION_TYPE, VALUE_STOP);
            payload.add(KEY_ACTION, VALUE_ACTIVE_2_STANDBY);
        }

        switch (getStandbyStateTrigger()) {
            case STANDBY_STATE_SLEEP_TIMEOUT:
                payload.add(KEY_EVENT_TYPE_OLD, VALUE_TECHNICAL);
                payload.add(KEY_TYPE, VALUE_TECHNICAL);
                break;

            default:
            case STANDBY_STATE_POWER_BUTTON:
                payload.add(KEY_EVENT_TYPE_OLD, VALUE_INTERACTION);
                payload.add(KEY_TYPE, VALUE_INTERACTION);
                break;
        }

        payload.add(KEY_EVENT_CAT_OLD, VALUE_STANDBY);
        payload.add(KEY_SUB_TYPE, VALUE_STANDBY);

        payload.add(KEY_HDMI_CONN_OLD, mHdmiReceiver.isConnected());
        payload.add(KEY_HDMI_CONN, mHdmiReceiver.isConnected());
    }


    /**
     * add the name and the time stamp of the hotkey
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Hotkeys
     */
    private void addHotkeyEventPart(LogDetailsProximus payload, UILog.ButtonType button) {
        String buttonName = getButtonName(button);
        payload.add(KEY_CATEGORY, VALUE_EVENT);
        payload.add(KEY_TYPE, VALUE_INTERACTION);
        payload.add(KEY_EVENT_TYPE_OLD, VALUE_HOTKEY);
        payload.add(KEY_SUB_TYPE, VALUE_HOTKEY);
        payload.add(KEY_EVENT_CAT_OLD, buttonName);
        payload.add(KEY_ACTION, buttonName);
        payload.add(KEY_ACTION_TYPE, NONE);
    }


    /**
     * add the application launch parameters to the json object
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/App+related+events
     */
    private void addAppLaunchPart(LogDetailsProximus payload, Event<UILogEvent, UILogEvent.Detail> event) {
        long eventTime = event.getTimestamp();
        payload.add(KEY_ACTION, VALUE_LAUNCHED);
        payload.add(KEY_APP_DURATION, 0);
        payload.add(KEY_APP_IDENTIFIER, getIdentifier(eventTime));
    }

    /**
     * add the application exit parameters to the json object
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/App+related+events
     */
    private void addAppExitPart(LogDetailsProximus payload, Event<UILogEvent, UILogEvent.Detail> event) {
        long duration = event.getDetail(UILogEvent.Detail.Duration, 0);
        long startTime = event.getTimestamp() - duration;
        payload.add(KEY_ACTION_TYPE, VALUE_STOP);
        payload.add(KEY_ACTION, VALUE_EXITED);
        payload.add(KEY_APP_DURATION, duration);
        payload.add(KEY_APP_IDENTIFIER, getIdentifier(startTime));
    }


    /**
     * add the application related details to the json object
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/App+related+events
     */
    private void addAppDetails(LogDetailsProximus payload, Event<UILogEvent, UILogEvent.Detail> event) {
        String packageName = event.getDetail(UILogEvent.Detail.PackageName);
        String appName = PackageUtils.getAppNameByPackageName(mContext, packageName);
        payload.add(KEY_EVENT_TYPE_OLD, VALUE_APP);
        payload.add(KEY_TYPE, VALUE_APP);
        payload.add(KEY_EVENT_CAT_OLD, appName);
        payload.add(KEY_SUB_TYPE, appName);
        payload.add(KEY_ITEM_ID, packageName);
    }

    /**
     * add recording event parameters to the json object
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Recording+Events
     */
    private void addRecordingEventsPart(LogDetailsProximus payload, Event<UILogEvent, UILogEvent.Detail> event,
                                        boolean series) {
        payload.add(KEY_EVENT_TYPE_OLD, VALUE_RECORDING);
        payload.add(KEY_TYPE, VALUE_RECORDING);
        payload.add(KEY_SUB_TYPE, VALUE_NPVR_USER);
        payload.add(KEY_EVENT_CAT_OLD, VALUE_NPVR);
        payload.add(KEY_CALL_LETTER, event.getDetail(UILogEvent.Detail.ChannelCallLetter, NONE));
        payload.add(KEY_CHANNEL_EXTERNAL_IDENTIFIER_OLD, event.getDetail(UILogEvent.Detail.ChannelId, NONE));
        payload.add(KEY_CHANNEL_ID, event.getDetail(UILogEvent.Detail.ChannelId, NONE));
        payload.add(KEY_PROGRAM_TITLE_OLD, event.getDetail(UILogEvent.Detail.ItemTitle, NONE));
        payload.add(KEY_ITEM_TITLE, event.getDetail(UILogEvent.Detail.ItemTitle, NONE));
        payload.add(KEY_PROGRAM_REFERENCE_NUMBER_OLD, event.getDetail(UILogEvent.Detail.ProgramId, NONE));
        payload.add(KEY_ITEM_ID, event.getDetail(UILogEvent.Detail.ProgramId, NONE));
        payload.add(KEY_SCHEDULED_TRAIL_IDENTIFIER, getScheduledTrailIdentifier(event.getDetail(UILogEvent.Detail.ProgramId, NONE)));
        payload.add(KEY_RESOLUTION, event.getDetail(UILogEvent.Detail.Resolution, NONE));
        payload.add(KEY_ITEM_TYPE, series ? VALUE_SERIES : VALUE_SINGLE);
        payload.add(KEY_SERIES_NR, event.getDetail(UILogEvent.Detail.SeasonNr));
        payload.add(KEY_EPISODE_NR, event.getDetail(UILogEvent.Detail.EpisodeNr));
        payload.add(KEY_EPISODE_TITLE, event.getDetail(UILogEvent.Detail.EpisodeTitle, NONE));
        payload.add(KEY_START_TIME, event.getDetail(UILogEvent.Detail.Start));
        payload.add(KEY_PROGRAM_DURATION, event.getDetail(UILogEvent.Detail.Duration));
        payload.add(KEY_PROGRAM_CATEGORY, event.getDetail(UILogEvent.Detail.ProgramCategory, NONE));
        payload.add(KEY_PROGRAM_SUB_CATEGORY, event.getDetail(UILogEvent.Detail.ProgramSubCategory, NONE));
    }

    /**
     * Add the vod rental content specific parameters
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/VOD+Rental+Events
     */
    private void addVodRentalContentSpecificPart(LogDetailsProximus payload, Event<UILogEvent, UILogEvent.Detail> event) {
        payload.add(KEY_VOD_TITLE_OLD, event.getDetail(UILogEvent.Detail.VodTitle, NONE));
        payload.add(KEY_GROUP_TITLE, event.getDetail(UILogEvent.Detail.GroupTitle, NONE));
        payload.add(KEY_VOD_ITEM_IDENTIFIER_OLD, event.getDetail(UILogEvent.Detail.VodItemIdentifier));
        payload.add(KEY_VOD_EXTERNAL_IDENTIFIER_OLD, event.getDetail(UILogEvent.Detail.VodExternalIdentifier, NONE));
        payload.add(KEY_GENRE, event.getDetail(UILogEvent.Detail.Genre, NONE));
        payload.add(KEY_SUPER_GENRE, event.getDetail(UILogEvent.Detail.SuperGenre, NONE));
        payload.add(KEY_SERIES_TITLE, event.getDetail(UILogEvent.Detail.SeriesTitle, NONE));
        payload.add(KEY_SERIES_NR, event.getDetail(UILogEvent.Detail.SeasonNr));
        payload.add(KEY_EPISODE_NR, event.getDetail(UILogEvent.Detail.EpisodeNr));
        payload.add(KEY_VOD_TYPE, getVodType(event.getDetail(UILogEvent.Detail.VodType, VodItemType.UNDEFINED)));
        payload.add(KEY_RESOLUTION, event.getDetail(UILogEvent.Detail.Resolution, NONE));
        payload.add(KEY_CURRENCY_UNIT, getCurrencyUnit(event));
        payload.add(KEY_RENTAL_COST, event.getDetail(UILogEvent.Detail.RentalCost));
    }


    private String getStandbyStateTrigger() {
        String standbyReason = mMwProxy.getSystemProperties(STANDBY_REASON_PROPERTY);
        if (standbyReason == null) {
            return NONE;
        }

        Matcher matcher = STANDBY_STATE_PATTERN.matcher(standbyReason);
        if (!matcher.matches()) {
            return NONE;
        }

        if (STANDBY_STATE_POWER_BUTTON.equals(matcher.group(1))) {
            return STANDBY_STATE_POWER_BUTTON;
        }

        if (STANDBY_STATE_SLEEP_TIMEOUT.equals(matcher.group(1))) {
            return STANDBY_STATE_SLEEP_TIMEOUT;
        }

        return NONE;
    }

    /**
     * @return Currency unit for the VOD rental UI event.
     */
    private String getCurrencyUnit(Event<UILogEvent, UILogEvent.Detail> event) {
        switch (event.getDetail(UILogEvent.Detail.CurrencyUnit, CurrencyType.Unknown)) {
            case Euro:
                return VALUE_EUR;
            case Credit:
                return VALUE_CREDIT;
            default:
                return null;
        }
    }

    private String getButtonName(UILog.ButtonType button) {
        switch (button) {
            case GUIDE:
                return VALUE_TV_GUIDE;
            case RECORD:
                return VALUE_PVR;
            default:
                return button.name();
        }
    }

    private String getPageOpenAction(UILog.OpenedBy openedBy) {
        if (openedBy == null) {
            return VALUE_OK;
        }

        switch (openedBy) {
            default:
            case Ok:
                return VALUE_OK;
            case Back:
                return VALUE_BACK;
            case Restart: {
                if (isPowerCycle()) {
                    return VALUE_POWER_CYCLE;
                }

                return VALUE_APP_RESTART;
            }
            case Home:
                return VALUE_HOME;
            case Radio:
                return VALUE_RADIO;
            case Standby:
                return VALUE_STANDBY;
            case Timeout:
                return VALUE_TIMEOUT;
            case TvGuide:
                return VALUE_TV_GUIDE;
        }
    }

    private String getPowerCycleType(long firstSignOfLife, long lastKnownHeartBeat, String bootReason) {
        String[] bootReasons = ArrayUtils.split(bootReason, ",");
        if (!ArrayUtils.isEmpty(bootReasons)) {
            for (String reason : bootReasons) {
                if (MwProxy.REBOOT_REASON_QUIESCENT.equals(reason)) {
                    return VALUE_FORCED;
                }
            }
        }
        if (firstSignOfLife - lastKnownHeartBeat < POWER_CYCLE_TYPE_SUSPICIOUS_THRESHOLD) {
            return VALUE_SUSPICIOUS;
        }
        if (!MassStorage.lastKnownScreenOn.get(false)) {
            return VALUE_SLEEP;
        }
        return VALUE_NORMAL;
    }

    private String getPreCycleState() {
        if (MassStorage.lastKnownScreenOn.get(false)) {
            return VALUE_ACT;
        }
        return VALUE_PWR_OFF;
    }

    /**
     * Every single page open event this saves in cache the current page opened except if this is a TVPage.
     * @param currentPage page which is opened just now.
     */
    private void saveLastPageInCache(UILog.Page currentPage) {
        if (UILog.Page.TvPlayer == currentPage || UILog.Page.MediaPlayerPage == currentPage) {
            return;
        }

        MemoryCache.LastOpenedPage.edit().put(currentPage);
    }

    /**
     * @return True if the navigation session needs to be reset by a new event.
     */
    private boolean isNavigationSessionResetNeeded() {
        // First event. Reset session.
        if (mLastPageEvent == null) {
            return true;
        }

        // Reset navigation on player.
        UILog.Page lastPage = mLastPageEvent.getDetail(UILogEvent.Detail.Page, UILog.Page.Unknown);
        boolean isResetNeeded = mLastPageEvent.getDetail(UILogEvent.Detail.NeedReset, false);
        return (lastPage == UILog.Page.TvPlayer || lastPage == UILog.Page.MediaPlayerPage) && isResetNeeded;
    }

    /**
     * @return True if the events are the same page opening events
     */
    private boolean isSamePage(Event<UILogEvent, UILogEvent.Detail> pageEvent,
                               Event<UILogEvent, UILogEvent.Detail> lastPageEvent) {
        return pageEvent != null && lastPageEvent != null
                && Objects.equals(pageEvent.getDetail(UILogEvent.Detail.Page, UILog.Page.Unknown),
                lastPageEvent.getDetail(UILogEvent.Detail.Page, UILog.Page.Unknown))
                && Objects.equals(pageEvent.getDetail(UILogEvent.Detail.PageName),
                lastPageEvent.getDetail(UILogEvent.Detail.PageName))
                && Objects.equals(pageEvent.getDetail(UILogEvent.Detail.PageId),
                lastPageEvent.getDetail(UILogEvent.Detail.PageId))
                && Objects.equals(pageEvent.getDetail(UILogEvent.Detail.FocusName),
                lastPageEvent.getDetail(UILogEvent.Detail.FocusName));
    }

    private final BaseBroadcastReceiver onTimeSet = new BaseBroadcastReceiver() {
        @Override
        public IntentFilter getIntentFilter() {
            IntentFilter result = new IntentFilter();
            result.addAction(Intent.ACTION_TIME_CHANGED);
            result.addAction(Intent.ACTION_DATE_CHANGED);
            return result;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            // send a transient app start event when the time change
            Log.event(new Event<>(UILogEvent.AppStarted)
                    .addDetail(UILogEvent.Detail.PackageName, context.getPackageName())
                    .addDetail(UILogEvent.Detail.StartAction, StartAction.Undefined)
            );
        }
    };
}
