package tv.threess.threeready.data.proximus.account.model.account;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * Proximus BTA response entity.
 *
 * Created by Daniela Toma on 03/31/2020.
 */
@XmlRootElement(name = "BTAResponse")
public class UpdateResponseProximus extends BTAResponseProximus {

    @XmlElement(name="Status")
    private Status mStatus;

    public boolean isSuccess() {
        return mStatus == Status.Success;
    }

    private enum Status {
        Success
    }
}
