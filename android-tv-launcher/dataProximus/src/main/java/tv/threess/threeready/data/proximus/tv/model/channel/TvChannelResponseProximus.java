/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.proximus.tv.model.channel;


import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.api.tv.model.ITvChannelResponse;
import tv.threess.threeready.api.tv.model.StreamType;
import tv.threess.threeready.api.tv.model.TifScanParameters;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;
import tv.threess.threeready.data.proximus.tv.adapter.DisplayChannelMapAdapter;

/**
 * Proximus channel response entity.
 *
 * @author Barabas Attila
 * @since 2018.06.07
 */
@XmlRootElement(name = "BTAResponse")
public class TvChannelResponseProximus extends BTAResponseProximus
        implements ITvChannelResponse<TvChannelProximus> {

    @XmlElementWrapper(name = "Channels")
    @XmlElement(name = "Channel")
    private ArrayList<TvChannelProximus> mChannels;

    @XmlJavaTypeAdapter(DisplayChannelMapAdapter.class)
    @XmlElementWrapper(name = "ChannelDisplayList")
    private Map<String, Integer> mDisplayChannels;

    @XmlElementWrapper(name = "MTPList")
    @XmlElement(name = "MTP")
    private ArrayList<MtpInfoProximus> mMtp;

    private VQEInfoTCH mVQEInfo;
    private VCASInfoTCH mVCASInfo;

    public void setVQEInfo(VQEInfoTCH vqeInfo) {
        mVQEInfo = vqeInfo;
    }

    public void setVCASInfo(VCASInfoTCH VCASInfoTCH) {
        mVCASInfo = VCASInfoTCH;
    }

    @Override
    public List<TvChannelProximus> getChannels() {
        for (TvChannelProximus tvChannelProximus: mChannels) {
            Integer number = mDisplayChannels.get(tvChannelProximus.getId());
            tvChannelProximus.setNumber(number == null ? 0 : number);
        }
        return mChannels;
    }

    @Override
    public Iterable<Option> getOptions(TvChannelProximus channel) {
        ArrayList<Option> result = new ArrayList<>();

        // TODO : find a better solution to decide between iptv and ott channel lists.
        Option iptv = getIptvOption(channel);
        if (iptv != null) {
            result.add(iptv);
        }

        // Add optional app with lowest priority.
        Option app = getAppOption(channel);
        if (app != null) {
            result.add(app);
        }

        return result;
    }

    @Override
    public TifScanParameters getScanParameters() {
        TreeSet<String> uniqueAddresses = new TreeSet<>();
        ArrayList<IpTvParametersTch.IpTvService> services = new ArrayList<>();
        for (TvChannelProximus channel : mChannels) {
            Iptv option = getIptvOption(channel);
            if (option == null) {
                continue;
            }
            if (uniqueAddresses.contains(option.mAddress)) {
                continue;
            }
            uniqueAddresses.add(option.mAddress);
            services.add(new IpTvParametersTch.IpTvService(option.mAddress, channel.getName(), channel.getNumber()));
        }
        return new IpTvParametersTch(IpTvParametersTch.TYPE_IP_VQE, IpTvParametersTch.COUNTRY, mVQEInfo, mVCASInfo, uniqueAddresses, services);
    }

    private Iptv getIptvOption(TvChannelProximus channel) {
        TvChannelProximus.ChannelStreamProximus stream = channel.getMulticastStream();
        if (stream == null) {
            return null;
        }

        String address = stream.getMulticastAddress();
        if (TextUtils.isEmpty(address)) {
            return null;
        }

        String mtpId = stream.getMtpId();
        return new Iptv(address, mMtp.stream()
                .filter(m -> Objects.equals(mtpId, m.getId()))
                .findFirst().orElse(null)
        );
    }

    private App getAppOption(TvChannelProximus forChannel) {
        if (!TextUtils.isEmpty(forChannel.getPackageName())) {
            return new App(forChannel.getPackageName());
        }
        return null;
    }

    /**
     * The playback option with the ott type model
     *
     * @author Paul
     * @since 2017.05.15
     */
    public static class Ott implements Option {

        private final String mPlaybackURI;

        public Ott(String url) {
            mPlaybackURI = url;
        }

        public Ott() {
            this(null);
        }

        @Override
        public StreamType getPlaybackType() {
            return StreamType.Ott;
        }

        @Override
        public String getPlaybackData() {
            return mPlaybackURI;
        }

        @Override
        public String getPlaybackExtra() {
            return null;
        }
    }

    /**
     * The playback option with the iptv type model
     *
     * @author Paul
     * @since 2017.05.15
     */
    public static class Iptv implements Option {

        private final String mAddress;
        private final MtpInfoProximus mMtp;

        public Iptv(String address, MtpInfoProximus mtp) {
            mAddress = address;
            mMtp = mtp;
        }

        @Override
        public StreamType getPlaybackType() {
            return StreamType.IpTv;
        }

        @Override
        public String getPlaybackData() {
            return mAddress;
        }

        @Override
        public String getPlaybackExtra() {
            return mMtp.getBitrate();
        }
    }
}
