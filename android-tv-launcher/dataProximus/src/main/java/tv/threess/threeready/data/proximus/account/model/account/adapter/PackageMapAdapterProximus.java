package tv.threess.threeready.data.proximus.account.model.account.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.data.proximus.account.model.account.PackageInfoProximus;

/**
 * Type adapter to read the user subscribed package info into a map.
 *
 * @author Barabas Attila
 * @since 2020.05.28
 */
public class PackageMapAdapterProximus extends XmlAdapter<List<PackageInfoProximus>, Map<String, PackageInfoProximus>> {

    @Override
    public Map<String, PackageInfoProximus> unmarshal(List<PackageInfoProximus> packages) {
        Map<String, PackageInfoProximus> packageMap = new HashMap<>();
        for (PackageInfoProximus packageInfo : packages) {
            packageMap.put(packageInfo.getExternalId(), packageInfo);
        }
        return packageMap;
    }

    @Override
    public List<PackageInfoProximus> marshal(Map<String, PackageInfoProximus> packageMap) {
        return new ArrayList<>(packageMap.values());
    }
}
