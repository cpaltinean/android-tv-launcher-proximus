package tv.threess.threeready.data.proximus.iam.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Proximus IAM client registration request JWT body.
 *
 * @author Barabas Attila
 * @since 2020.03.19
 */
public class IAMRegisterPayloadProximus implements Serializable {
    @SerializedName("jti")
    private String mJTI;

    @SerializedName("aud")
    private String mAud;

    @SerializedName("exp")
    private int mExp;

    @SerializedName("iat")
    private int mIat;

    @SerializedName("iss")
    private String mIss;

    @SerializedName("redirect_uris")
    private List<String> mRedirectUris;

    @SerializedName("response_types")
    private List<String> mResponseTypes;

    @SerializedName("scope")
    private String mScope;

    @SerializedName("grant_types")
    private List<String> mGrandTypes;

    @SerializedName("token_endpoint_auth_method")
    private String mTokenEndpointAuthMethod;

    @SerializedName("application_type")
    private String mApplicationType;

    @SerializedName("client_name")
    private String mClientName;

    @SerializedName("software_statement")
    private String mSoftwareStatement;

    @SerializedName("hardware_information")
    private HardwareInformation mHardwareInformation;


    private static class HardwareInformation {
        @SerializedName("hardware_serial_number")
        private String mSerialNumber;

        @SerializedName("hardware_mac_addresses")
        private List<String> mMacAddress;

        @SerializedName("hardware_os_version")
        private String mOsVersion;

        @SerializedName("hardware_model_number")
        private String mHardwareModelNumber;
    }

    public static class Builder {

        private final IAMRegisterPayloadProximus mPayload;

        public Builder() {
            mPayload = new IAMRegisterPayloadProximus();
            mPayload.mJTI = UUID.randomUUID().toString();

            mPayload.mResponseTypes = new ArrayList<>();
            mPayload.mResponseTypes.add("token");

            mPayload.mGrandTypes = new ArrayList<>();
            mPayload.mGrandTypes.add("client_credentials");

            mPayload.mTokenEndpointAuthMethod = "client_secret_basic";
            mPayload.mApplicationType = "native";
            mPayload.mClientName = "Android";
            mPayload.mIss = "ES_ProximusTV_JWT";

            mPayload.mHardwareInformation = new HardwareInformation();
            mPayload.mHardwareInformation.mMacAddress = new ArrayList<>();
            mPayload.mHardwareInformation.mHardwareModelNumber = "1.0";
            mPayload.mHardwareInformation.mOsVersion = "1.0";
        }

        public Builder audience(String audience) {
            mPayload.mAud = audience;
            return this;
        }

        public Builder expiration(long timeMillis) {
            mPayload.mExp = (int) TimeUnit.MILLISECONDS.toSeconds(timeMillis);
            return this;
        }

        public Builder issued(long timeMillis) {
            mPayload.mIat = (int) TimeUnit.MILLISECONDS.toSeconds(timeMillis);
            return this;
        }

        public Builder redirectUrl(String redirectUrl) {
            mPayload.mRedirectUris = new ArrayList<>();
            mPayload.mRedirectUris.add(redirectUrl);
            return this;
        }

        public Builder scope(String scope) {
            mPayload.mScope = scope;
            return this;
        }

        public Builder hardwareSerial(String hardwareSerial) {
            mPayload.mHardwareInformation.mSerialNumber = hardwareSerial;
            return this;
        }

        public Builder hardwareModelNumber(String hardwareModelNumber) {
            mPayload.mHardwareInformation.mHardwareModelNumber = hardwareModelNumber;
            return this;
        }

        public Builder hardwareOsVersion(String osVersion) {
            mPayload.mHardwareInformation.mOsVersion = osVersion;
            return this;
        }

        public Builder macAddress(String macAddress) {
            mPayload.mHardwareInformation.mMacAddress.add(macAddress);
            return this;
        }

        public Builder softwareStatement(String softwareStatement) {
            mPayload.mSoftwareStatement = softwareStatement;
            return this;
        }

        public IAMRegisterPayloadProximus build() {
            return mPayload;
        }
    }
}
