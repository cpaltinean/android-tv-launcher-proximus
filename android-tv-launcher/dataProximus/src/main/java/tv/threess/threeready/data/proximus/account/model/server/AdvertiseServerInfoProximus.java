package tv.threess.threeready.data.proximus.account.model.server;

import java.io.Serializable;

/**
 * Class for proximus advertisement related server information.
 */
public class AdvertiseServerInfoProximus extends ServerInfoProximus implements Serializable {
    private static final String PUBLISHING_PATH = "Publishingpath";
    private static final String VAST_REQUEST_RANDOMIZATION = "VASTRequestRandomization";
    private static final String AD_RETRIEVAL_INTERVAL = "ADRetrievalInterval";
    private static final String REQUEST_RANDOM_INTERVAL = "RequestRandomInterval";
    private static final String AD_DURATION_TOLERANCE = "AdDurationTolerance";

    public AdvertiseServerInfoProximus(ServerInfoProximus serverInfoProximus) {
        super(serverInfoProximus);
    }

    public String getPublishingPath() {
        return super.getAttributeValueForName(PUBLISHING_PATH);
    }

    public long getVASTRequestRandomization() {
        return super.getAttributeValueForName(VAST_REQUEST_RANDOMIZATION, 0);
    }

    public long getADRetrievalInterval() {
        return super.getAttributeValueForName(AD_RETRIEVAL_INTERVAL, 0);
    }

    public long getRequestRandomInterval() {
        return super.getAttributeValueForName(REQUEST_RANDOM_INTERVAL, 0);
    }

    public long getAdDurationTolerance() {
        return super.getAttributeValueForName(AD_DURATION_TOLERANCE, 0);
    }
}
