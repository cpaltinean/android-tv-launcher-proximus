package tv.threess.threeready.data.proximus.mw;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import tv.threess.threeready.api.generic.receivers.BaseBroadcastReceiver;
import tv.threess.threeready.api.generic.receivers.WaitTask;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

/**
 * Task to check and wait for the DTV service initialization
 *
 * @author Barabas Attila
 * @since 1/15/21
 */
public class WaitDtvSetupTaskProximus extends WaitTask<Void> {
    private static final String ACTION_DTV_READY = "com.technicolor.android.dtvinput.setup.READY";
    private static final String ACTION_DTV_SETUP_CHECK = "com.technicolor.android.dtvscan.action.CHECK_SETUP_STATE";

    private final Context mContext;

    public WaitDtvSetupTaskProximus(Context context) {
        mContext = context;
    }

    @Override
    public Void waitForResult(long timeoutMillis) throws ExecutionException, InterruptedException, TimeoutException {
        try {
            mBroadcastReceiver.registerReceiver(mContext);
            Intent intent = new Intent(ACTION_DTV_SETUP_CHECK);
            intent.setComponent(new ComponentName(
                    MwProxyProximus.PACKAGE_DTVINPUT, MwProxyProximus.INSTALLER_SERVICE));
            mContext.startForegroundService(intent);
            return super.waitForResult(timeoutMillis);
        } finally {
            mBroadcastReceiver.unregisterReceiver(mContext);
        }
    }

    @Override
    public Void result() {
        return null;
    }

    private final BaseBroadcastReceiver mBroadcastReceiver = new BaseBroadcastReceiver() {
        @Override
        public IntentFilter getIntentFilter() {
            return new IntentFilter(ACTION_DTV_READY);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };
}
