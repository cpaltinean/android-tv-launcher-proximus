package tv.threess.threeready.data.proximus.account;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.ReadableSetting;
import tv.threess.threeready.api.account.setting.SettingProperty;
import tv.threess.threeready.data.account.SettingsAccessProxy;
import tv.threess.threeready.data.proximus.mw.MwProxyProximus;

/**
 * Enumeration of known technicolor cas related settings.
 *
 * @author Boldijar Paul
 * @since 2017.07.25
 */
public enum TechnicolorCasSettings implements SettingProperty, ReadableSetting {
    // IPTV VCAS verimatrix information
    BackendAuthenticator("cas.verimatrix.backend_authenticator"),
    System("cas.verimatrix.vcas.system"),
    ChipsetId("cas.chipset.id"),
    PluginVersion("cas.verimatrix.plugin.version"),
    ClientVersion("cas.verimatrix.viewrightclient.version"),
    SecurityMode("cas.verimatrix.securitymode"),
    OfflineStatus("cas.verimatrix.init.offline.status"),
    OnlineStatus("cas.verimatrix.init.online.status"),
    IpAddress("cas.verimatrix.vcas.ipaddresss");

    private static final SettingsAccessProxy sSettingsAccess = Components.get(SettingsAccessProxy.class);

    private final String name;

    TechnicolorCasSettings(String value) {
        this.name = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return sSettingsAccess.getTechnicolorSettings(MwProxyProximus.CAS_URI, this);
    }

    public int put(String value) {
        return sSettingsAccess.putTechnicolorSettings(MwProxyProximus.CAS_URI, this, value);
    }

    public int put(boolean value) {
        return put(String.valueOf(value));
    }
}
