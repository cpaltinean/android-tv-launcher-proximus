package tv.threess.threeready.data.proximus.tv.model.broadcast.response;


/**
 * Proximus broadcast meta info keys.
 *
 * @author Barabas Attila
 * @since 2019.10.30
 */
enum BroadcastMetaInfoKeyProximus implements MetaInfoProximus.IKey {
    ProgramReferenceNo("programReferenceNo"),
    ScheduleTrailID("scheduleTrailID"),
    ProgramScheduleStart("programScheduleStart"),
    ChannelExtID("ChannelExtID"),
    Duration("duration");

    private final String mKey;

    BroadcastMetaInfoKeyProximus(String key) {
        mKey = key;
    }

    @Override
    public String getName() {
        return mKey;
    }
}
