package tv.threess.threeready.data.proximus.account.model.account;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Body used for deleting STB properties
 *
 * @author Solyom Zsolt
 * @since 2020.07.10
 */
@XmlRootElement(name = "BTA")
public class DeleteSTBPropertiesBodyProximus implements Serializable {

    @XmlElement(name = "DeleteSTBPropertiesDoc")
    private final DeleteSTBPropertiesDoc mDeleteDoc = new DeleteSTBPropertiesDoc();

    public DeleteSTBPropertiesBodyProximus(STBProperties... propertyNames) {
        Arrays.asList(propertyNames).forEach(it -> addProperty(new DeleteSTBPropertiesDoc.STBProperty(it)));
    }

    @XmlRootElement(name = "DeleteSTBPropertiesDoc")
    public static class DeleteSTBPropertiesDoc implements Serializable {

        @XmlElement(name = "Property")
        @XmlElementWrapper(name = "Properties")
        private final List<STBProperty> mSTBProperties = new ArrayList<>();

        @XmlRootElement(name = "Property")
        public static class STBProperty implements Serializable {

            @XmlElement(name = "Name")
            private final String mName;

            public STBProperty(STBProperties property) {
                mName = property.getPropertyName();
            }
        }
    }

    private void addProperty(DeleteSTBPropertiesDoc.STBProperty property) {
        mDeleteDoc.mSTBProperties.add(property);
    }

}