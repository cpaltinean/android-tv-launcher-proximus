package tv.threess.threeready.data.proximus.log.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
/**
 * Wrapper class used for sending events.
 * Contains a json property with name: events.
 *
 * @author Zsolt Bokor_
 * @since 2020.06.17
 */

public class SendEventsRequest {
    @SerializedName("events")
    private final List<LogDetailsProximus> mEvents = new ArrayList<>();

    public SendEventsRequest(LogDetailsProximus event) {
        mEvents.add(event);
    }

    public SendEventsRequest(List<LogDetailsProximus> events) {
        mEvents.addAll(events);
    }
}
