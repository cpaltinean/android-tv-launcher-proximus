package tv.threess.threeready.data.proximus.vod.model.response;

import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * BTA backend response of a VOD subcategory request.
 *
 * @author Barabas Attila
 * @since 2020.04.28
 */
public class VodCategoriesResponseProximus extends BTAResponseProximus {

    @XmlElement(name = "Category")
    private Category mCategory;

    /**
     * @return The list of available sub-categories.
     */
    public List<VodCategoryProximus> getCategories() {
        return mCategory == null ? Collections.emptyList() : (List) mCategory.mVodCategories;
    }

    @XmlRootElement(name = "Category")
    private static class Category {

        @XmlElement(name = "VOD")
        private List<VodCategoryDetailProximus> mVodCategories;

        @XmlAttribute(name = "total")
        private int mTotalCount;

        @XmlAttribute(name = "count")
        private int mCount;
    }

}
