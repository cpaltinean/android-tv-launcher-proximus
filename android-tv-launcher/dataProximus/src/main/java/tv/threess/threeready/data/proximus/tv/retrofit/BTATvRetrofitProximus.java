package tv.threess.threeready.data.proximus.tv.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import tv.threess.threeready.data.proximus.generic.interceptor.CacheControlOverrideOkHttpInterceptor.CacheControlOverride;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.BTAAiringsProximus;
import tv.threess.threeready.data.proximus.tv.model.channel.TvChannelResponseProximus;

/**
 * Retrofit service the access tv related information from the BTA backend.
 *
 * @author Barabas Attila
 * @since 2020.03.23
 */
public interface BTATvRetrofitProximus {

    @GET("/broker/bta/getChannels?fc_UnlockedPackagesOnly=true")
    Call<TvChannelResponseProximus> getChannels();

    @CacheControlOverride
    @GET("/broker/bta/getDetails?fc_UnlockedPackagesOnly=true&c_ContentTypes=EPG&fc_withEPGMetadata=true&fc_DetailLevelEPGp=1&fc_DetailLevelEPGs=1")
    Call<BTAAiringsProximus> getBroadcast(@Query("fc_SchTrailIDs") String broadcastId,
                                          @Query("fc_ExternalIDs") String channelId,
                                          @Query("fc_StartDate") long startDate,
                                          @Query("fc_EndDate") long endDate);
}
