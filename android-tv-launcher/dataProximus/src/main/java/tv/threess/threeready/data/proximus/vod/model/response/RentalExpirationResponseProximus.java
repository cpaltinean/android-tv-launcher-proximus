package tv.threess.threeready.data.proximus.vod.model.response;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.vod.model.IRentalExpirationResponse;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * Retrofit response for rented vods call.
 *
 * Created by Noemi Dali on 24.04.2020.
 */
@XmlRootElement(name = "BTAResponse")
public class RentalExpirationResponseProximus extends BTAResponseProximus implements IRentalExpirationResponse<RentalExpirationProximus> {

    @XmlElement(name = "Rented")
    private Rented mRented;

    @XmlElement(name = "Movies")
    private Movies mMovies;

    public List<RentalExpirationProximus> getRentalInfos() {
        if (mRented == null || mRented.rentedItems == null) {
            return Collections.emptyList();
        }
        return mRented.rentedItems;
    }

    public List<BaseVodItemProximus> getVods() {
        if(mMovies == null || mMovies.vodItems == null) {
            return Collections.emptyList();
        }

        List<BaseVodItemProximus> vodList = new ArrayList<>(mMovies.mTotalMovies);
        for (VodVariantProximus vodVariantItem : mMovies.vodItems) {
                vodList.add(new BaseVodItemProximus(vodVariantItem, Arrays.asList(vodVariantItem.getId()), Collections.emptyMap()));
         }

        return vodList;
    }

    public int getFreeRentalCount() {
        if (mRented == null) {
            return 0;
        }
        return mRented.freeRentalCount;
    }

    @XmlRootElement(name = "Rented")
    private static class Rented {

        @XmlElementWrapper(name = "RentedItems")
        @XmlElement(name = "RentedItem")
        List<RentalExpirationProximus> rentedItems;

        @XmlElement(name = "FreeRentals")
        private int freeRentalCount;
    }

    @XmlRootElement(name = "Movies")
    private static class Movies {

        @XmlAttribute(name = "totalMovies")
        private int mTotalMovies;

        @XmlElement(name = "Movie")
        private List<VodVariantProximus> vodItems;
    }
}
