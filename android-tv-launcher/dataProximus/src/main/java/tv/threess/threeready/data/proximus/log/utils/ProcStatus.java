package tv.threess.threeready.data.proximus.log.utils;

import tv.threess.threeready.api.middleware.model.BaseInfo;

import java.io.File;

/**
 * Parse and the `/proc/<*>/status` file and return it.
 *
 * @author Karetka Mezei Zoltan
 * @since 2019.06.07
 */
public class ProcStatus extends BaseInfo<ProcStatus.Field> {
    protected ProcStatus() {
        super(Field.class);
    }

    /**
     * Read memory status from /proc/memory and return it.
     */
    public static ProcStatus read(int pid) {
        ProcStatus result = new ProcStatus();
        result.readFrom(new File("/proc/" + pid + "/status"));
        return result;
    }

    protected void put(Field field, String value) {
        switch (field) {
            // convert kilo-bytes to bytes
            case VmPeak:
            case VmSize:
            case VmRSS:
                super.put(field, kibiBytes(value));
                return;
        }
        super.put(field, value);
    }

    public enum Field implements BaseInfo.Field {
        Pid,
        VmPeak,
        VmSize,
        VmRSS,
        Threads;

        Field(String key) {
            mKey = key;
        }
        Field() {
            mKey = name();
        }
        final String mKey;

        @Override
        public String getKey() {
            return mKey;
        }
    }
}
