package tv.threess.threeready.data.proximus.vod.model.response;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.vod.model.CurrencyType;
import tv.threess.threeready.api.vod.model.IVodPrice;

/**
 * Proximus implementation of the VOD price item.
 *
 * @author Eugen Guzyk
 * @since 2018.11.14
 */
public class VodPriceProximus implements IVodPrice {

    private final double mAmount;
    private final CurrencyType mCurrencyType;
    private final String mPriceDetermination;

    VodPriceProximus(double amount, CurrencyType currencyType, String priceDetermination) {
        mAmount = amount;
        mCurrencyType = currencyType;
        mPriceDetermination = priceDetermination;
    }

    @Override
    public double getCurrencyAmount() {
        return mAmount;
    }

    @Override
    public CurrencyType getCurrencyType() {
        return mCurrencyType;
    }

    @Override
    @Nullable
    public String getPriceDetermination() {
        return mPriceDetermination;
    }
}
