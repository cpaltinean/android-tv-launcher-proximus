package tv.threess.threeready.data.proximus.home.model.module;

import java.util.List;

import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Module config for content wall 2.0 with multiple filtering and sorting options.
 *
 * @author Barabas Attila
 * @since 9/4/21
 */
public class ContentWall2SwimlaneModuleProximus extends ContentWallSwimlaneModuleProximus {

    protected final List<DataSourceSelector> mSelectors;

    public ContentWall2SwimlaneModuleProximus(SwimLaneProximus swimlane,
                                              List<DataSourceSelector> selectors) {
        super(swimlane);
        mSelectors = selectors;
    }

    @Override
    public List<DataSourceSelector> getSelectors() {
        return mSelectors;
    }

    @Override
    public int getMinElements() {
        // Don't hide the module when it's empty. Selectors are needed to be displayed
        return mSelectors.isEmpty() ? super.getMinElements() : 0;
    }
}
