package tv.threess.threeready.data.proximus.pvr.model.series.response;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * Proximus BTA response entity used for Recording Quota List.
 *
 * Created by Daniela Toma on 04/22/2020.
 */
@XmlRootElement(name = "BTAResponse")
public class RecordingQuotaResponseProximus extends BTAResponseProximus {

    @XmlElement(name = "RecordingQuota")
    private List<RecordingQuotaProximus> mRecordingQuotaList;

    public List<RecordingQuotaProximus> getRecordingQuotaList(){
        return mRecordingQuotaList;
    }
}
