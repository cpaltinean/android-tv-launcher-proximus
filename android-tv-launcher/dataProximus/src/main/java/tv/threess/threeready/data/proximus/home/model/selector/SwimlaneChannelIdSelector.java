package tv.threess.threeready.data.proximus.home.model.selector;

import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneParamResponsibleProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneQueryParamProximus;

/**
 * This selection option is used to insert the selected channel ID into the content wall query process.
 *
 * @author David Bondor
 * @since 4.28.2022
 */
public class SwimlaneChannelIdSelector implements SwimlaneSelector {

    private static final Pattern VALUE_TOKEN_PATTERN = Pattern.compile("\\%\\(((.*?))\\)[s,i,b]");

    private final SwimLaneProximus mSwimlane;
    private final String mChannelId;
    private final String mTokenName;

    public SwimlaneChannelIdSelector(SwimLaneProximus swimlane, String channelId) {
        mSwimlane = swimlane;
        mChannelId = channelId;
        mTokenName = prepareTokenName();
    }

    @Override
    public String getReportName() {
        return mChannelId;
    }

    @Override
    public String getReportReference() {
        return mSwimlane.getItemQueryName();
    }

    @Override
    public String getName() {
        return mChannelId;
    }

    @Override
    public boolean isDefault() {
        return false;
    }

    @Nullable
    @Override
    public String getTokenName() {
        return mTokenName;
    }

    @Nullable
    @Override
    public String getTokenValue() {
        return mChannelId;
    }

    @Override
    public List<SwimLaneQueryParamProximus> getParameters() {
        return Collections.emptyList();
    }

    private String prepareTokenName() {
        for (SwimLaneQueryParamProximus param : mSwimlane.getParameters()) {
            if (param.getResponsible() == SwimLaneParamResponsibleProximus.PageVariable) {
                Matcher matcher = VALUE_TOKEN_PATTERN.matcher(param.getValueToken());
                if (matcher.find() && matcher.groupCount() > 1) {
                    return matcher.group(1);
                }
            }
        }

        return null;
    }
}