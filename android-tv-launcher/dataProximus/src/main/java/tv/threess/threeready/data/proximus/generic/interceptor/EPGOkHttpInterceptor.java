package tv.threess.threeready.data.proximus.generic.interceptor;

import android.app.Application;
import android.text.TextUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import tv.threess.threeready.data.proximus.account.SessionProximus;
import tv.threess.threeready.data.proximus.account.retrofit.BootConfigRetrofitProximus;

/**
 * Replace the request base url with the value from boot config
 * and add generic request parameters.
 *
 * @author Barabas Attila
 * @since 2020.03.30
 */
public class EPGOkHttpInterceptor implements Interceptor {

    private final Application mApp;

    public EPGOkHttpInterceptor(Application app) {
        mApp = app;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder requestBuilder = chain.request().newBuilder();

        String requestUrl = chain.request().url().toString();
        String baseUrl = BootConfigRetrofitProximus.BASE_URI + "/";

        String overrideBaseUrl = SessionProximus.epgBaseUrl.get("") + "/";

        // Override request base url with the value from boot config.
        if (!TextUtils.isEmpty(overrideBaseUrl)
                && requestUrl.startsWith(baseUrl)) {
            requestUrl = requestUrl.replace(baseUrl, overrideBaseUrl);
        }

        requestBuilder.url(requestUrl);

        return chain.proceed(requestBuilder.build());
    }
}
