/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.account;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.view.KeyEvent;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;

import java.io.IOException;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.account.model.account.IConsent;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.generic.exception.InvalidPinException;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.generic.model.IParentalControlSettings;
import tv.threess.threeready.api.generic.model.WatchlistType;
import tv.threess.threeready.api.generic.receivers.BaseLocalBroadcastReceiver;
import tv.threess.threeready.api.home.model.local.FontConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.Error;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.playback.model.TrackInfo;
import tv.threess.threeready.api.tv.epg.EpgLayoutType;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.account.AccountLocalCache;
import tv.threess.threeready.data.account.AccountService;
import tv.threess.threeready.data.account.BaseAccountRepository;
import tv.threess.threeready.data.account.observable.CachingObservable;
import tv.threess.threeready.data.account.observable.FontConfigObservable;
import tv.threess.threeready.data.account.observable.InWatchlistObservable;
import tv.threess.threeready.data.account.observable.UpdateBootConfigObservable;
import tv.threess.threeready.data.config.observable.ParentalControlSettingsObservable;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.mw.MwService;
import tv.threess.threeready.data.netflix.receiver.NetflixDETResponseReceiver;
import tv.threess.threeready.data.proximus.account.model.account.PackageNamesProximus;
import tv.threess.threeready.data.proximus.account.model.registration.request.STBRegistrationUpdateRequestProximus;
import tv.threess.threeready.data.proximus.account.model.registration.response.STBRegistrationInfoProximus;
import tv.threess.threeready.data.proximus.generic.model.BTAErrorCodesProximus;
import tv.threess.threeready.data.proximus.tv.model.BaseBroadcastProximus;
import tv.threess.threeready.data.proximus.utils.URLParser;
import tv.threess.threeready.data.proximus.watchlist.response.GetWatchlistItemResponseProximus;
import tv.threess.threeready.data.pvr.PvrService;
import tv.threess.threeready.data.tv.TvService;
import tv.threess.threeready.data.tv.TvTadService;
import tv.threess.threeready.data.utils.GsonUtils;
import tv.threess.threeready.data.vod.VodService;

/**
 * Implementation of the Proximus specific config handler methods.
 *
 * @author Barabas Attila
 * @since 2018.06.05
 */
public class AccountRepositoryProximus extends BaseAccountRepository implements AccountRepository, Component {
    private static final String TAG = Log.tag(AccountRepositoryProximus.class);

    private final AccountProxyProximus mAccountProxy = Components.get(AccountProxyProximus.class);
    private final AccountLocalCache mAccountCache = Components.get(AccountLocalCache.class);
    private final MwProxy mMwProxy = Components.get(MwProxy.class);

    private final NetflixDETResponseReceiver mDETReceiver = Components.get(NetflixDETResponseReceiver.class);

    private final PublishSubject<Error> mPublishSubject = PublishSubject.create();

    public AccountRepositoryProximus(Application application) {
        super(application);
        initLoggerBroadcast();
    }

    @Override
    public String getEnvironment() {
        return SessionProximus.environment.get("");
    }

    @Override
    public Single<Boolean> registerDevice(String lineNumber, String pin) {
        return Single.create(emitter -> {
            try {
                mAccountProxy.registerDevice(lineNumber,
                        mMwProxy.getIpAddress(),
                        mMwProxy.getHardwareSerialNumber(),
                        pin,
                        mMwProxy.getHardwareVersion(),
                        mMwProxy.getMWVersion(),
                        mMwProxy.getAndroidSerialNumber(),
                        mMwProxy.getEthernetMacAddress());
                Settings.authenticated.edit().put(true);
                setEpgLayoutSync(EpgLayoutType.ClassicEpg);
                emitter.onSuccess(true);
            } catch (InvalidPinException e) {
                Log.e(TAG, "Device registration failed.", e);
                emitter.onSuccess(false);
            }
        });
    }

    @Override
    public void sendImpression(String deepLink) {
        mDETReceiver.sendImpression(mApp, deepLink);
    }

    @Override
    @WorkerThread
    public boolean hasReplayPlusSubscription() {
        return isSubscribed(PackageNamesProximus.REPLAY_PLUS_EXTERNAL_ID);
    }

    @Override
    @WorkerThread
    public boolean hasReplaySubscription() {
        return isSubscribed(PackageNamesProximus.REPLAY_EXTERNAL_ID);
    }

    @Override
    @WorkerThread
    public boolean isSubscribed(String toPackage) {
        return mAccountCache.isPackageSubscribed(toPackage);
    }

    @Override
    public void updateFavoriteChannels(String favoriteChannels) throws IOException {
        mAccountProxy.updateFavoriteChannels(favoriteChannels);
    }

    @Override
    public void startPerformCaching() {
        BaseWorkManager.start(mApp, AccountService.buildPerformCachingIntent());
    }

    @Override
    public void startUpdateDeviceRegistration() {
        BaseWorkManager.start(mApp, AccountService.buildUpdateDeviceRegistration());
    }

    @Override
    public Completable clearData() {
        return Completable.fromAction(() -> {
            BaseWorkManager.cancelAlarm(mApp, AccountService.buildUpdateBootConfigIntent());
            BaseWorkManager.cancelAlarm(mApp, AccountService.buildUpdateGlobalInstallIntent());
            BaseWorkManager.cancelAlarm(mApp, AccountService.buildUpdateAccountInfoIntent());
            BaseWorkManager.cancelAlarm(mApp, MwService.buildRusWindowIntent());
            BaseWorkManager.cancelAlarm(mApp, TvService.buildChannelUpdateIntent());
            BaseWorkManager.cancelAlarm(mApp, TvTadService.buildAdvertisementDownloadIntent());
            BaseWorkManager.cancelAlarm(mApp, TvService.buildBroadcastUpdateIntent());
            BaseWorkManager.cancelAlarm(mApp, VodService.buildUpdatePurchasesIntent());
            BaseWorkManager.cancelAlarm(mApp, PvrService.buildUpdateRecordingIntent());

            for (String db : mApp.databaseList()) {
                FileUtils.delete(mApp.getDatabasePath(db), false);
            }
            FileUtils.delete(mApp.getExternalCacheDir(), true);
            FileUtils.delete(mApp.getCacheDir(), true);
            FileUtils.delete(mApp.getFilesDir(), true);
        });
    }

    @Override
    protected String getSavedInstance() {
        return SessionProximus.appInstanceId.get("");
    }

    @Override
    public void registerInstance(String instanceId) throws IOException {
        mAccountProxy.registerInstance(instanceId);
        SessionProximus.appInstanceId.edit().put(instanceId);
    }

    private void initLoggerBroadcast() {
        BaseLocalBroadcastReceiver baseBroadcastReceiver = new BaseLocalBroadcastReceiver() {
            @Override
            public IntentFilter getIntentFilter() {
                return new IntentFilter(Log.LOG_ACTION);
            }

            @Override
            public void onReceive(Context context, Intent intent) {
                Error error = (Error) intent.getSerializableExtra(Log.ARG_ERROR);
                mPublishSubject.onNext(error);
            }
        };
        baseBroadcastReceiver.registerReceiver(mApp);
    }

    @Override
    public Disposable subscribeToErrors(Consumer<Error> consumer) {
        return mPublishSubject
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer);
    }

    @Override
    public Observable<FontConfig[]> getFontConfigs() {
        return Observable.create(new FontConfigObservable(mApp));
    }

    @Override
    public Single<String> getNetflixAuthenticationToken() {
        return Single.create(emitter -> {
            String token = mAccountProxy.getNetflixAuthenticationToken(mMwProxy.getHardwareSerialNumber());
            if (!TextUtils.isEmpty(token)) {
                emitter.onSuccess(token);
            } else {
                emitter.onError(new NullPointerException("Netflix authentication token is null or empty!"));
            }
        });
    }

    @Override
    public Completable updateBootConfig() {
        return Completable.fromObservable(Observable.create(new UpdateBootConfigObservable(mApp)));
    }

    @Override
    public Completable caching() {
        return Completable.fromObservable(Observable.create(new CachingObservable(mApp)));
    }

    @Override
    public Completable setHintViewed(final String hintId) {
        return Completable.fromAction(() -> mAccountProxy.setViewedHints(hintId));
    }

    @Override
    public Completable deleteViewedHints() {
        return Completable.fromAction(mAccountProxy::deleteViewedHints);
    }

    @Override
    public Completable enableParentalControl(boolean enable) {
        return Completable.fromAction(() -> {
            mAccountProxy.enableParentalControl(enable);
            Settings.enabledParentalControl.edit().put(enable);
        });
    }

    @Override
    public Completable setPowerConsumptionStatus(boolean status) {
        return Completable.fromAction(() -> {
            mAccountProxy.setPowerConsumptionStatus(status);
            Settings.powerConsumptionStepDisplayed.edit().put(status);
        });
    }

    @Override
    public Single<Boolean> isDeviceRegistrationRequired() {
        return Single.fromObservable(Observable.create(emitter -> {
            try {
                STBRegistrationInfoProximus registrationInfo = mAccountProxy.getSTBRegistrationInfo();
                Settings.batchEdit()
                        .put(Settings.accountNumber, registrationInfo.getAccountNumber())
                        .put(Settings.deviceName, registrationInfo.getSTBName())
                        .put(Settings.authenticated, true)
                        .persistNow();

                String ipAddress = Settings.deviceIp.get("");

                STBRegistrationUpdateRequestProximus request = new STBRegistrationUpdateRequestProximus(
                        ipAddress,
                        registrationInfo.getSoftwareVersion(),
                        registrationInfo.getCASDeviceID(),
                        registrationInfo.getSTBName(),
                        registrationInfo.getESN(),
                        registrationInfo.getSTBUiVersion()
                );

                SettingsProximus.deviceRegistrationInfoHash.edit().put(request.getRegistrationInfoHash());

                emitter.onNext(false);
            } catch (BackendException e) {
                //in case of invalid MAC or the STB was not assigned yet
                if (BTAErrorCodesProximus.INVALID_MAC.equalsIgnoreCase(e.getErrorCode())
                        || BTAErrorCodesProximus.STB_NOT_ASSIGNED.equalsIgnoreCase(e.getErrorCode())) {
                    Settings.authenticated.edit().put(false);
                    emitter.onNext(true);
                } else {
                    throw e;
                }
            }
            emitter.onComplete();
        }));
    }

    @Override
    public Observable<String> buildURL(String url) {
        return Observable.create(emitter -> {
            emitter.onNext(URLParser.getURLWithReplacedTemplateParameters(
                    url,
                    Settings.accountNumber.get(""),
                    SettingsProximus.companyLocationName.get(""),
                    SettingsProximus.companySite.get("")));
            emitter.onComplete();
        });
    }

    @Override
    public Observable<String> buildURL(String url, TvChannel channel) {
        return Observable.create(emitter -> {
            emitter.onNext(
                    URLParser.getURLWithReplacedChannelInfo(
                            url,
                            Settings.accountNumber.get(""),
                            SettingsProximus.companyLocationName.get(""),
                            SettingsProximus.companySite.get(""),
                            String.valueOf(KeyEvent.KEYCODE_DPAD_CENTER),
                            channel.getCallLetter(),
                            SettingsProximus.hdProfile.get(false)
                    ));
            emitter.onComplete();
        });
    }

    @Override
    public Single<String> getShopLanguage() {
        return Single.fromCallable(() -> LocaleUtils.getShopLanguage(SettingsProximus.shopLanguage.getList()));
    }

    @Override
    public Completable setShopLanguages(String languages) {
        return Completable.fromAction(() -> {
            mAccountProxy.setShopLanguage(languages);
            SettingsProximus.shopLanguage.edit().put(TextUtils.split(languages, ","));
        });
    }

    @Override
    public Completable setPreferredSubtitleLanguages(TrackInfo language) {
        return Completable.fromAction(() -> {
            TechnicolorSettings.MediaSubtitlesEnabled.put(!TextUtils.isEmpty(language.getLanguage()));
            TechnicolorSettings.MediaSubtitlesPreferredType.put(language.isDescriptive() ? TechnicolorSettings.SUBTITLE_TYPE_HARD_OF_HEARING : TechnicolorSettings.SUBTITLE_TYPE_NORMAL);
            TechnicolorSettings.MediaSubtitlesPreferredLanguages.put(language.getLanguage());

            String json = GsonUtils.getGson().toJson(language);
            mAccountProxy.setSubtitleLanguage(json);
            Settings.subtitlesLanguage.edit().put(json);
        });
    }

    @Override
    public Completable setPreferredAudioLanguage(TrackInfo language) {
        return Completable.fromAction(() -> {
            // FIXME: Workaround for TCH no audio issue.
            //  The default selection is done in tv.threess.threeready.player.control.base.BaseTvViewControl.onTracksChanged
//        TechnicolorSettings.MediaAudioPreferredType.put(TechnicolorSettings.AUDIO_TYPE_NORMAL);
//        TechnicolorSettings.MediaAudioPreferredLanguages.put(language.getLanguage());

            String json = GsonUtils.getGson().toJson(language);
            mAccountProxy.setAudioLanguage(json);
            Settings.audioLanguage.edit().put(json);
        });
    }


    @Override
    public Completable setEpgLayout(EpgLayoutType epgLayout) {
        return Completable.fromAction(() -> setEpgLayoutSync(epgLayout));
    }

    @Override
    public Single<List<? extends IConsent>> getConsents(String requestScenario) {
        return Single.create(e -> e.onSuccess(mAccountProxy.getConsents(requestScenario)));
    }

    @Override
    public Single<IConsent> getConsent(String consentId) {
        return Single.create(e -> e.onSuccess(mAccountProxy.getConsent(consentId)));
    }

    @Override
    public Completable setConsent(String consentId, boolean isAccepted) {
        return Completable.fromAction(() -> mAccountProxy.setConsent(consentId, isAccepted));
    }

    @Override
    public Single<IImageSource> getConsentQrCode() {
        return Single.fromCallable(mAccountProxy::getConsentQrCode);
    }

    @Override
    public Single<Boolean> changeParentalControlPIN(String pin) {
        return Single.create(e -> {
            boolean isChanged = false;
            if (mAccountProxy.changeParentalControlPIN(pin)) {
                Settings.systemPIN.edit().put(pin);
                isChanged = true;
            }
            e.onSuccess(isChanged);
        });
    }

    @Override
    public Single<Boolean> changePurchasePIN(String purchasePIN) {
        return Single.create(e -> {
            boolean isChanged = false;
            if (mAccountProxy.changePurchasePIN(purchasePIN)) {
                Settings.purchasePIN.edit().put(purchasePIN);
                isChanged = true;
            }
            e.onSuccess(isChanged);
        });
    }

    @Override
    public Completable setParentalRating(@NonNull ParentalRating parentalRating) {
        return Completable.fromAction(() -> {
            mAccountProxy.setParentalRating(parentalRating);
            Settings.minAgeRating.edit().put(parentalRating.getMinimumAge());
        });
    }

    @Override
    public Observable<IParentalControlSettings> getParentalControlSettings() {
        return Observable.create(new ParentalControlSettingsObservable(mApp));
    }

    @Override
    public Observable<Boolean> isInWatchlist(String contentId) {
        return Observable.create(new InWatchlistObservable(mApp, contentId));
    }

    @Override
    public Completable addToWatchlist(WatchlistType type, String contentId) {
        return Completable.fromAction(() -> {
            Log.d(TAG, "addToWatchlist() called with: type = [" + type + "], contentId = [" + contentId + "]");
            GetWatchlistItemResponseProximus.WatchlistItem response;
            if (type == WatchlistType.Broadcast) {
                response = mAccountProxy.addItemToWatchlist(type, BaseBroadcastProximus.buildTrailId(contentId), BaseBroadcastProximus.buildChannelId(contentId));
            } else {
                response = mAccountProxy.addItemToWatchlist(type, contentId);
            }

            if (response != null) {
                mAccountCache.addToWatchlist(type, response.getId(), contentId, response.getTimestamp());
            }
        });
    }

    @Override
    public Completable removeFromWatchlist(String contentId, WatchlistType type) {
        return Completable.fromAction(() -> {
            Log.d(TAG, "removeFromWatchlist() called with: contentId = [" + contentId + "]");

            String id = getWatchlistId(contentId);

            if (TextUtils.isEmpty(id)) {
                return;
            }

            boolean response = mAccountProxy.removeFromWatchlist(id);

            Log.d(TAG, "removeFromWatchlist() called with: response = [" + response + "]");
            if (response) {
                mAccountCache.removeFromWatchlist(type, id, contentId);
            }
        });
    }

    private void setEpgLayoutSync(EpgLayoutType epgLayout) throws IOException {
        mAccountProxy.setEpgLayout(epgLayout);
        Settings.epgLayoutType.edit().put(epgLayout);
    }

    /**
     * Get the wathlist ID based on a content ID.
     */
    private String getWatchlistId(String contentId) {
        Log.d(TAG, "getWatchlistId() called with contentId: [" + contentId + "]");
        return mAccountCache.getWatchlistId(contentId);
    }

    /**
     * Returns the result of the Operator Isolation Override varnish call.
     *
     * @return true if box needs to boot in boot in “isolated mode”
     */
    public boolean isOperatorOverriding() throws IOException {
        return mAccountProxy.isOperatorOverriding();
    }

    /**
     * Returns the result of the BTA isAlive service call.
     *
     * @return false if box needs to boot in boot in “isolated mode”
     */
    public boolean isAlive() throws IOException {
        return mAccountProxy.isAlive();
    }
}
