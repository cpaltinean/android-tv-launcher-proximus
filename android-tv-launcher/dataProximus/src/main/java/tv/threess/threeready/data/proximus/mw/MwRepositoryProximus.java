/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.mw;

import android.bluetooth.BluetoothDevice;
import android.content.Context;

import java.util.Collection;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.receivers.WaitTask;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.BluetoothUtils;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.api.middleware.model.HdmiInfo;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.log.ReportService;
import tv.threess.threeready.data.mw.MwService;

/**
 * Proximus specific MW handler methods.
 *
 * @author Barabas Attila
 * @since 2018.04.25
 */
public class MwRepositoryProximus implements MwRepository, Component {
    private static final String TAG = Log.tag(MwRepositoryProximus.class);

    private final MwProxyProximus mMWProxy = Components.get(MwProxyProximus.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    private final Context mContext;
    private volatile Boolean mTalkBackCache = null;

    public MwRepositoryProximus(Context context) {
        mContext = context;
    }

    @Override
    public String getMWVersion() {
        return mMWProxy.getMWVersion();
    }

    @Override
    public String getEthernetMacAddress() {
        return mMWProxy.getEthernetMacAddress();
    }

    @Override
    public String getAppVersion() {
        return mMWProxy.getAppVersion();
    }

    @Override
    public boolean isScreenOn() {
        return mMWProxy.isScreenOn();
    }

    @Override
    public boolean isTalkBackOn() {
        if (mTalkBackCache == null) {
            mTalkBackCache = mMWProxy.isTalkBackOn();
        }

        return mTalkBackCache;
    }

    @Override
    public void clearTalkBackCache() {
        mTalkBackCache = null;
    }

    @Override
    public void sendUsageStats() {
        BaseWorkManager.start(mContext, ReportService.buildSendUsageStats());
    }

    @Override
    public void startCheckSystemUpdate() {
        BaseWorkManager.start(mContext, MwService.buildCheckSystemUpdateIntent());
    }

    @Override
    public WaitTask<Void> waitForDtvSetup() {
        return mMWProxy.waitForDtvSetup();
    }

    @Override
    public boolean isDefaultLauncher() {
        return mMWProxy.isDefaultLauncher();
    }

    @Override
    public int getWindowHeight() {
        return mMWProxy.getWindowHeight();
    }

    @Override
    public int getWindowWidth() {
        return mMWProxy.getWindowWidth();
    }

    @Override
    public boolean isInQuiescentReboot() {
        long lastQuiescentRebootTime = Settings.lastQuiescentRebootTime.get(0L);
        long outOfStandbyTime = Settings.outOfStandbyTime.get(0L);
        long intoStandbyTime = Settings.intoStandbyTime.get(0L);

        return outOfStandbyTime < intoStandbyTime && lastQuiescentRebootTime > intoStandbyTime;
    }

    @Override
    public HdmiInfo getHdmiInfo() {
        return mMWProxy.getHdmiInfo();
    }

    @Override
    public String getBootId() {
        return mMWProxy.getBootId();
    }

    @Override
    public Single<String> getAndroidSerialNumber() {
        return Single.fromCallable(mMWProxy::getAndroidSerialNumber);
    }

    @Override
    public Single<String> getRemoteControlVersion() {
        return Single.fromCallable(() -> {
            Collection<String> remoteNames = mAppConfig.getRemoteNames();
            BluetoothDevice device = BluetoothUtils.getConnectedDevice(mContext, remoteNames);
            if (device == null) {
                throw new RuntimeException("Remote control is not connected");
            }
            String deviceFirmwareVersion = BluetoothUtils.readFirmwareVersion(mContext, device);
            String deviceSoftwareVersion = BluetoothUtils.readSoftwareVersion(mContext, device);
            return deviceFirmwareVersion + "-" + deviceSoftwareVersion;
        });
    }

    @Override
    public Single<String> getVCASClientVersion() {
        return Single.fromCallable(mMWProxy::getVCASClientVersion);
    }

    @Override
    public Single<String> getVCASPluginVersion() {
        return Single.fromCallable(mMWProxy::getVCASPluginVersion);
    }

    @Override
    public Completable checkSystemUpdates(boolean reset, boolean rollOutBypass, boolean manualCheck) {
        return Completable.fromAction(() -> mMWProxy.checkSystemUpdates(reset, rollOutBypass, manualCheck));
    }

    @Override
    public Completable applySystemUpdate() {
        return Completable.fromAction(() -> {
            if (!mMWProxy.isScreenOn()) {
                Settings.lastQuiescentRebootTime.edit()
                        .put(System.currentTimeMillis());
            }

            mMWProxy.applySystemUpdate();
        });
    }

    @Override
    public Completable bootCompletedAction() {
        return Completable.fromAction(mMWProxy::bootCompletedAction);
    }

    @Override
    public Completable waitForHDCPActivation() {
        return Completable.fromObservable(Observable.create(new HDCPActivationObservable(mContext)));
    }
}
