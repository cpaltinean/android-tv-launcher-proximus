package tv.threess.threeready.data.proximus.home.model.page;

import java.util.Collections;
import java.util.List;

import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.data.proximus.home.model.AccessPointProximus;
import tv.threess.threeready.data.proximus.home.model.module.ContentWall2SwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.selector.SwimlaneChannelIdSelector;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneHubResponseProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Replay Channel page config of an access point defined by the hub.
 *
 * @author David Bondor
 * @since 4.28.2022
 */
public class ReplayChannelPageConfig extends AccessPointPageConfigProximus {

    private final String mChannelName;
    private final String mChannelId;

    public ReplayChannelPageConfig(AccessPointProximus accessPoint, SwimLaneHubResponseProximus hub, String channelId, String channelName) {
        super(accessPoint, hub, false);
        mChannelName = channelName;
        mChannelId = channelId;
    }

    @Override
    protected ModuleConfig createSwimlaneModule(SwimLaneProximus swimlane, SwimLaneHubResponseProximus hub, List<DataSourceSelector> selectors) {
        if (swimlane.getDataSourceType() == SwimLaneProximus.DataSourceType.Remote) {
            if (isContentWall2(swimlane, selectors)) {
                selectors.add(new DataSourceSelector(Collections.singletonList(new SwimlaneChannelIdSelector(swimlane, mChannelId))));
                return new ContentWall2SwimlaneModuleProximus(swimlane, selectors);
            }
        }

        return super.createSwimlaneModule(swimlane, hub, selectors);
    }

    @Override
    public String getTitle() {
        return mChannelName;
    }

    @Override
    public UILog.Page getPageName() {
        return UILog.Page.ContentWallFullContentFilterPage;
    }
}