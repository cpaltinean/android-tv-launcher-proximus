package tv.threess.threeready.data.proximus.mw;

import android.app.Application;
import android.content.Context;
import android.os.PowerManager;
import android.os.SystemClock;

import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.MwServiceRepository;
import tv.threess.threeready.api.middleware.UpdateUrgency;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.mw.SystemUpdateTask;

/**
 * Proximus specific implementation of the MW service handler.
 *
 * @author Barabas Attila
 * @since 9/20/20
 */
public class MwServiceRepositoryProximus implements MwServiceRepository {
    private static final String TAG = Log.tag(MwServiceRepositoryProximus.class);

    private final AppConfig mAppConfig = Components.get(AppConfig.class);
    private final MwProxyProximus mMwProxy = Components.get(MwProxyProximus.class);

    private final PowerManager.WakeLock mWakeLock;

    public MwServiceRepositoryProximus(Application app) {
        // Create wake lock for system update checks.
        PowerManager powerManager = (PowerManager) app.getSystemService(Context.POWER_SERVICE);
        if (powerManager != null) {
            mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        } else {
            Log.w(TAG, "Couldn't create wakelock.");
            mWakeLock = null;
        }
    }

    /**
     * Check for system update and wait for download if needed.
     * The system update time is only triggered if the current check in time exceeds
     * the next check in time coming from server.
     *
     * @param checkInTime     The current check in time.
     * @param updateFirstTime True if the update check needs to be triggered
     *                        even when the next check in time is not defined yet.
     *                        Usually this happens at FTI or clear data.
     * @return True if an update was downloaded.
     */
    @Override
    public boolean checkSystemUpdate(long checkInTime, boolean updateFirstTime) {
        long nextCheckInTime = Settings.systemUpdateNextCheckInTime.get(0L);
        if (nextCheckInTime == 0 && !updateFirstTime) {
            Log.d(TAG, "Skip first time system update check.");
            return false;
        }

        if (checkInTime < nextCheckInTime) {
            Log.d(TAG, "Skip system update check. Recently checked."
                    + " CheckInTime : " + Log.timestamp(checkInTime)
                    + ", nextCheckInTime : " + Log.timestamp(nextCheckInTime));
            return false;
        }

        Log.d(TAG, "Check for system update."
                + " CheckInTime : " + Log.timestamp(checkInTime)
                + ", nextCheckInTime : " + Log.timestamp(nextCheckInTime));

        SystemUpdateTask systemUpdateTask = new SystemUpdateTask();
        systemUpdateTask.start();

        // Wakelock during the download.
        long updateDownloadTimeout = mAppConfig.getSystemUpdateTimeout(TimeUnit.MILLISECONDS);

        // Check and download system update.
        UpdateUrgency urgency = null;
        try {
            urgency = systemUpdateTask.waitResult(updateDownloadTimeout);
        } catch (Exception e) {
            Log.e(TAG, "Failed to check system update.", e);
        }

        if (urgency == null) {
            Log.d(TAG, "System update check finished. No update available.");
        } else {
            Log.d(TAG, "System update check finished. Urgency : " + urgency);
        }

        return urgency != null;
    }

    /**
     * Reboot if the device in standby if the configured uptime is reached.
     */
    @Override
    public void quiescentReboot() {
        if (mMwProxy.isScreenOn()) {
            Log.d(TAG, "Skip quiescent reboot: screen is on!");
            return;
        }

        long realUptime = SystemClock.elapsedRealtime();
        long minimumUptime = Settings.quiescentRebootUptime.get(0);
        if (minimumUptime <= 0 || realUptime < minimumUptime) {
            Log.d(TAG, "Skip quiescent reboot: uptime is too short or disabled: "
                    + Log.formatDuration(realUptime) + ", configured: " + Log.formatDuration(minimumUptime));
            return;
        }

        Settings.lastQuiescentRebootTime.edit().put(System.currentTimeMillis());

        mMwProxy.reboot(MwProxy.REBOOT_REASON_QUIESCENT);
    }

    @Override
    public PowerManager.WakeLock getWakeLock() {
        return mWakeLock;
    }
}
