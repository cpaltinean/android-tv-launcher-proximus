/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.proximus.home.model.swimalane;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.data.proximus.home.adapter.SwimLaneTileTypeAdapter;
import tv.threess.threeready.data.proximus.home.adapter.SwimlaneClientUIClassTypeAdapter;
import tv.threess.threeready.data.proximus.home.model.DisplayNamesProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.MetaInfoProximus;

/**
 * BTA response representing the definition a swim lane.
 *
 * @author Barabas Attila
 * @since 2019.10.28
 */
@XmlRootElement(name = "Swimlane")
public class SwimLaneProximus implements Serializable {
    public static final String KEY_SWIMLANE_ID = "SwLaneID";
    public static final String KEY_ITEM_QUERY_TYPE = "ItemQueryType";
    public static final String KEY_ITEM_QUERY_NAME = "ItemQueryName";

    private static final String KEY_MIN_CORE_TILES = "minCoreTiles";
    private static final String KEY_VOD_CATEGORY_ID = "CatExtId";
    private static final String KEY_NETFLIX_CATEGORY = "Category";
    private static final String KEY_NETFLIX_INTERACTION_ID = "InterActionId";
    private static final String KEY_AP_ID = "apExtID";
    private static final String KEY_EPG_GENRE = "EPGsuperGenre";
    private static final String KEY_APP_SELECT = "AppSelect";
    private static final String KEY_CAT_ITEM_SORT = "CatItemSort";

    private static final String CLIENT_QUERY_TYPE = "Client";
    private static final String SELECTOR_QUERY_TYPE = "ClientSelector";

    private static final String CONTINUE_WATCHING_QUERY_NAME = "UC01CW";
    private static final String RECORDINGS_QUERY_NAME = "UC02PVRL";
    private static final String NOW_ON_TV_QUERY_NAME = "UC03FXCH";
    private static final String RENTALS_QUERY_NAME = "UC04RNTL";
    private static final String VOD_CATEGORY_QUERY_NAME = "UC05VodCat";
    private static final String WISHLIST_QUERY_NAME = "UC06WishList";
    private static final String APP_LIST_QUERY_NAME = "UC07AppList";
    private static final String APP_LIST_EDITORIAL_QUERY_NAME = "UC12AppListEdito";
    private static final String PAGE_LINK_QUERY_NAME = "UC08PageLink";
    private static final String REPlAY_CHANNEL_PAGE_LINK_QUERY_NAME = "UC11ReplayChannelPageLink";
    private static final String NOW_ON_TV_GENRE_QUERY_NAME = "UC09GenreFXCH";
    private static final String SYSTEM_NOTIFICATION_QUERY_NAME = "UC10SysNotif";
    private static final String NETFLIX_QUERY_NAME = "UC13NetflixSL";

    private static final String RECOMMENDED_APP_SELECT_VALUE = "Recommended";

    private static final int DEFAULT_MIN_CORE_TILES_VALUE = 1;

    @XmlElement(name = "Name")
    private String mName;

    @XmlElement(name = "ExternalID")
    private String mId;

    @XmlElementWrapper(name = "ItemQueryParameters")
    private ArrayList<SwimLaneQueryParamProximus> mParameters;

    @XmlElementWrapper(name = "DisplayNames")
    private DisplayNamesProximus mDisplayNames;

    @XmlElement(name = "ItemQueryType")
    private String mItemQueryType;

    @XmlElement(name = "ItemQueryName")
    private String mItemQueryName;

    @XmlElementWrapper(name = "ContentWall")
    private ArrayList<SwimLaneSortOptionProximus> mContentWall;

    // Stat tile
    @XmlJavaTypeAdapter(SwimLaneTileTypeAdapter.class)
    @XmlElement(name = "StartTileName")
    private SwimLaneTileAction mStartTileName;

    @XmlElement(name = "StartTileImage")
    private String mStartTileImage;

    @XmlElementWrapper(name = "StartTileParameters")
    private ArrayList<SwimLaneQueryParamProximus> mStartTileParameters;

    @XmlElementWrapper(name = "StartTileDisplayNames")
    private DisplayNamesProximus mStartTileDisplayNames;

    // End tile
    @XmlJavaTypeAdapter(SwimLaneTileTypeAdapter.class)
    @XmlElement(name = "EndTileName")
    private SwimLaneTileAction mEndTileName;

    @XmlElement(name = "EndTileImage")
    private String mEndTileImage;

    @XmlElementWrapper(name = "EndTileParameters")
    private ArrayList<SwimLaneQueryParamProximus> mEndTileParameters;

    @XmlElementWrapper(name = "EndTileDisplayNames")
    private DisplayNamesProximus mEndTileDisplayNames;

    @XmlElementWrapper(name = "StaticTileList")
    private ArrayList<StaticTile> mStaticTiles;

    @XmlElement(name = "ItemDisplayLimit")
    private int mItemDisplayLimit;

    @XmlElement(name = "HandleAsHerobanner")
    private boolean mHandleAsHeroBanner;

    @XmlElement(name = "minClientCacheMinutes")
    private int mMinClientCacheMinutes;

    @XmlElement(name = "maxClientCacheMinutes")
    private int maxClientCacheMinutes;

    @XmlElement(name = "UI_language")
    private String mUiLanguage;

    @XmlJavaTypeAdapter(SwimlaneClientUIClassTypeAdapter.class)
    @XmlElement(name = "ClientUIClass")
    private final ClientUiClass mClientUiClass = ClientUiClass.Undefined;

    public String getName() {
        return mName;
    }

    private final SwimlaneTile mStartTile = new SwimlaneTile() {
        private final IImageSource mImage = TextUtils.isEmpty(mStartTileImage) ? null : new IImageSource() {
            @Override
            public String getUrl() {
                return mStartTileImage;
            }

            @Override
            public Type getType() {
                return Type.DEFAULT;
            }
        };

        @Override
        public IImageSource getBackgroundImage() {
            return mImage;
        }

        @Override
        public SwimLaneTileAction getAction() {
            return mStartTileName;
        }

        @Override
        public DisplayNamesProximus getDisplayName() {
            return mStartTileDisplayNames == null ?
                    DisplayNamesProximus.EMPTY_NAMES : mStartTileDisplayNames;
        }

        @Override
        public List<SwimLaneQueryParamProximus> getQueryParams() {
            return mStartTileParameters;
        }
    };

    private final SwimlaneTile mEndTile = new SwimlaneTile() {
        private final IImageSource mImage = TextUtils.isEmpty(mEndTileImage) ? null : new IImageSource() {
            @Override
            public String getUrl() {
                return mEndTileImage;
            }

            @Override
            public Type getType() {
                return Type.DEFAULT;
            }
        };

        @Override
        public IImageSource getBackgroundImage() {
            return mImage;
        }

        @Override
        public SwimLaneTileAction getAction() {
            return mEndTileName;
        }

        @Override
        public DisplayNamesProximus getDisplayName() {
            return mEndTileDisplayNames == null ?
                    DisplayNamesProximus.EMPTY_NAMES : mEndTileDisplayNames;
        }

        @Override
        public List<SwimLaneQueryParamProximus> getQueryParams() {
            return mEndTileParameters;
        }
    };

    /**
     * @return Unique identifier of the swimlane.
     */
    public String getId() {
        return mId;
    }

    /**
     * @return Query parameters of th swimlane.
     */
    public List<SwimLaneQueryParamProximus> getParameters() {
        return mParameters == null ? Collections.emptyList() : mParameters;
    }

    /**
     * @return The title of the swimlane localized.
     */
    public DisplayNamesProximus getDisplayNames() {
        return mDisplayNames == null ? DisplayNamesProximus.EMPTY_NAMES : mDisplayNames;
    }

    /**
     * @return Static tiles to display in the swimlane.
     */
    public ArrayList<StaticTile> getStaticTiles() {
        return mStaticTiles;
    }

    /**
     * @return The type of the query which needs to executed to request items for the swimlane.
     * E.g For 'Client' the items are resolved by the client application.
     */
    public String getItemQueryType() {
        return mItemQueryType;
    }

    /**
     * @return The name of the method to request items from.
     */
    public String getItemQueryName() {
        return mItemQueryName;
    }

    /**
     * @return True if the items in the swimlane should be prepared by the client.
     */
    public boolean isClientSwimlane() {
        return CLIENT_QUERY_TYPE.equals(getItemQueryType());
    }

    /**
     * @return True if the swimlane contains filter and sorting options for other siwmlanes on the page.
     */
    public boolean isSelectorSwimlane() {
        return SELECTOR_QUERY_TYPE.equals(getItemQueryType());
    }

    /**
     * @return True if the content of the swimlane needs to be displayed as gallery.
     */
    public boolean handleAsHeroBanner() {
        return mHandleAsHeroBanner;
    }

    /**
     * @return Enum which represents from where the data for swimlane coming from.
     */
    public DataSourceType getDataSourceType() {
        if (!isClientSwimlane()) {
            return DataSourceType.Remote;
        }

        switch (getItemQueryName()) {
            default:
                return DataSourceType.Remote;
            case CONTINUE_WATCHING_QUERY_NAME:
                return DataSourceType.ContinueWatching;
            case RECORDINGS_QUERY_NAME:
                return DataSourceType.Recordings;
            case VOD_CATEGORY_QUERY_NAME:
                return DataSourceType.VodCategorySwimlane;
            case PAGE_LINK_QUERY_NAME:
                return DataSourceType.AccessPointSwimlane;
            case REPlAY_CHANNEL_PAGE_LINK_QUERY_NAME:
                return DataSourceType.ReplayChannelSwimlane;
            case NETFLIX_QUERY_NAME:
                return DataSourceType.Netflix;
            case APP_LIST_QUERY_NAME:
                SwimLaneQueryParamProximus appSelectParam = getSwimLaneQueryParam(mParameters, KEY_APP_SELECT);
                if (appSelectParam != null && RECOMMENDED_APP_SELECT_VALUE.equals(appSelectParam.getValueToken())) {
                    return DataSourceType.RecommendedApps;
                }
                return DataSourceType.InstalledApps;
            case APP_LIST_EDITORIAL_QUERY_NAME:
                return DataSourceType.EditorialApps;
            case NOW_ON_TV_GENRE_QUERY_NAME:
            case NOW_ON_TV_QUERY_NAME:
                return DataSourceType.NowOnTv;
            case WISHLIST_QUERY_NAME:
                return DataSourceType.Wishlist;
            case RENTALS_QUERY_NAME:
                return DataSourceType.Rentals;
            case SYSTEM_NOTIFICATION_QUERY_NAME:
                return DataSourceType.SystemNotification;
        }
    }

    /**
     * @return Get a single VOD category id from the params.
     */
    public String getVodCategoryId() {
        SwimLaneQueryParamProximus param = getSwimLaneQueryParam(mParameters, KEY_VOD_CATEGORY_ID);
        if (param == null) {
            return "";
        }
        return param.getValueToken();
    }

    public String getNetflixCategory() {
        SwimLaneQueryParamProximus param = getSwimLaneQueryParam(mParameters, KEY_NETFLIX_CATEGORY);
        if (param == null) {
            return "";
        }
        return param.getValueToken();
    }

    public String getNetflixInteractionId() {
        SwimLaneQueryParamProximus param = getSwimLaneQueryParam(mParameters, KEY_NETFLIX_INTERACTION_ID);
        if (param == null) {
            return "";
        }
        return param.getValueToken();
    }

    /**
     * @return A list of access point ids from the params.
     */
    public List<String> getAccessPointIds() {
        return getSwimLaneQueryParamList(mParameters, KEY_AP_ID).stream()
                .map(SwimLaneQueryParamProximus::getValueToken).collect(Collectors.toList());
    }

    /**
     * @return A list of genre from the params
     */
    public List<String> getGenreNames() {
        return getSwimLaneQueryParamList(mParameters, KEY_EPG_GENRE).stream()
                .map(p -> p.getValueToken().replaceFirst(MetaInfoProximus.CATEGORY_PREFIX, ""))
                .collect(Collectors.toList());
    }


    /**
     * @return The selected VOD category item sorting option name.
     */
    @Nullable
    public String getCategoryItemSorting() {
        SwimLaneQueryParamProximus param = getSwimLaneQueryParam(
                getParameters(), KEY_CAT_ITEM_SORT);
        if (param == null) {
            return null;
        }
        return param.getValueToken();
    }

    /**
     * @return The possible sort options for the swimlane items.
     */
    public List<SwimLaneSortOptionProximus> getSortOptions() {
        return mContentWall;
    }

    /**
     * @return The maximum number of items can be displayed in the swimlane.
     * The start and end tiles are excluded.
     */
    public int getItemDisplayLimit() {
        return mItemDisplayLimit;
    }

    /**
     * @return The tile which needs to be displayed at the start of the swimlane items..
     */
    public SwimlaneTile getStartTile() {
        return mStartTile;
    }

    /**
     * @return The tile which needs to be displayed in the end of the swimlane items
     */
    public SwimlaneTile getEndTile() {
        return mEndTile;
    }

    /**
     * @param timeUnit The time unit to return the result in.
     * @return The minimum time to cache the swimlane response for.
     */
    public int getMinClientCacheTime(TimeUnit timeUnit) {
        return (int) timeUnit.convert(mMinClientCacheMinutes, TimeUnit.MINUTES);
    }

    /**
     * @param timeUnit The time unit to return the result in.
     * @return The maximum time until the swimlane response can be returned from the cache.
     */
    public int getMaxClientCacheTime(TimeUnit timeUnit) {
        return (int) timeUnit.convert(maxClientCacheMinutes, TimeUnit.MINUTES);
    }

    /**
     * @param params The swimlane parameter list to search in.
     * @param key    The key of the parameter to search for.
     * @return The swimlane parameter with the given name or null.
     */
    @Nullable
    public static SwimLaneQueryParamProximus getSwimLaneQueryParam(List<SwimLaneQueryParamProximus> params, String key) {
        for (SwimLaneQueryParamProximus param : ArrayUtils.notNull(params)) {
            if (key.equals(param.getName())) {
                return param;
            }
        }
        return null;
    }

    /**
     * @param params The swimlane parameter list to search in.
     * @param key    The key of the parameter to search for.
     * @return A list of swimlane parameter with the given name or null.
     */
    public static List<SwimLaneQueryParamProximus> getSwimLaneQueryParamList(
            List<SwimLaneQueryParamProximus> params, String key) {
        List<SwimLaneQueryParamProximus> retList = new ArrayList<>();
        params = params == null ? Collections.emptyList() : params;
        params.sort((p1, p2) -> Integer.compare(p1.getListSort(), p2.getListSort()));
        for (SwimLaneQueryParamProximus param : params) {
            if (Objects.equals(key, param.getName())) {
                retList.add(param);
            }
        }
        return retList;
    }

    /**
     * @return The language of UI specific for this SL
     */
    @Nullable
    public String getUiLanguage() {
        return mUiLanguage;
    }

    /**
     * @return The mode how the swimlane needs to be displayed on the UI.
     */
    public ClientUiClass getClientUiClass() {
        return mClientUiClass;
    }

    /**
     * Class represents a tile in the swimlane start or end.
     */
    public static abstract class SwimlaneTile implements Serializable {
        /**
         * @return The minimum number of items in the swimlane for the tile to be displayed.
         */
        public int getMinCoreTile() {
            int minCoreTiles = 1;
            SwimLaneQueryParamProximus param = getSwimLaneQueryParam(
                    getQueryParams(), KEY_MIN_CORE_TILES);
            if (param != null) {
                minCoreTiles = param.getValue(DEFAULT_MIN_CORE_TILES_VALUE);
            }
            return minCoreTiles;
        }

        /**
         * @return The id of the category for which the tile opens.
         */
        @Nullable
        public String getCategoryId() {
            SwimLaneQueryParamProximus param = getSwimLaneQueryParam(
                    getQueryParams(), KEY_VOD_CATEGORY_ID);
            if (param == null) {
                return null;
            }
            return param.getValueToken();
        }

        /**
         * @return The id of an access point page.
         */
        @Nullable
        public String getPageId() {
            SwimLaneQueryParamProximus param = getSwimLaneQueryParam(
                    getQueryParams(), KEY_AP_ID);
            if (param == null) {
                return null;
            }
            return param.getValueToken();
        }

        public abstract IImageSource getBackgroundImage();

        public abstract SwimLaneTileAction getAction();

        public abstract DisplayNamesProximus getDisplayName();

        public abstract List<SwimLaneQueryParamProximus> getQueryParams();
    }

    /**
     * Class represents a tile in the swimlane.
     */
    @XmlRootElement(name = "StaticTile")
    public static class StaticTile implements Serializable {

        @XmlElement(name = "ExternalID")
        private String mExternalId;

        @XmlElementWrapper(name = "DisplayNames")
        private DisplayNamesProximus mDisplayNames;

        @XmlElementWrapper(name = "ItemQueryParameters")
        private final ArrayList<SwimLaneQueryParamProximus> mParameters = new ArrayList<>();

        @XmlElement(name = "ListSort")
        private int mListSort;

        @XmlElement(name = "bgPosterFilename")
        private String mBgFileName;

        @XmlElement(name = "IsDefault")
        private boolean mIsDefault;

        public String getId() {
            return mExternalId;
        }

        public DisplayNamesProximus getDisplayNames() {
            return mDisplayNames == null ? DisplayNamesProximus.EMPTY_NAMES : mDisplayNames;
        }

        public List<SwimLaneQueryParamProximus> getParameters() {
            return mParameters;
        }

        /**
         * @return The order to display the tile inside of the swimlane.
         */
        public int getSortOrder() {
            return mListSort;
        }

        /**
         * @return A list of access point ids from the params.
         */
        public List<String> getAccessPointIds() {
            return getSwimLaneQueryParamList(mParameters, KEY_AP_ID).stream()
                    .map(SwimLaneQueryParamProximus::getValueToken).collect(Collectors.toList());
        }

        /**
         * @return Background image of the static tile.
         */
        @Nullable
        public IImageSource getBackgroundImage() {
            return TextUtils.isEmpty(mBgFileName) ? null : new IImageSource() {
                @Override
                public String getUrl() {
                    return mBgFileName;
                }

                @Override
                public Type getType() {
                    return Type.DEFAULT;
                }
            };
        }

        /**
         * @return True if the tile needs to be selected by default.
         * Only used in selector type swimlanes.
         */
        public boolean isDefault() {
            return mIsDefault;
        }
    }

    public enum DataSourceType {
        ContinueWatching,
        Recordings,
        Netflix,
        RecommendedApps,
        InstalledApps,
        EditorialApps,
        AccessPointSwimlane,
        ReplayChannelSwimlane,
        VodCategorySwimlane,
        NowOnTv,
        SystemNotification,
        Wishlist,
        Rentals,
        Remote
    }

    public enum SwimLaneTileAction {
        GoContentWall("goContentWall"),
        GoVodCategory("goVodCategory"),
        GoPage("goPage"),
        GoNetflix("goNetflix"),
        GoRecordings("goRecordings"),
        GoGrid("goGrid"),
        GoRentals("goRentals"),
        GoWishlist("goWishlist"),
        GoAppList("goAppList"),
        NoAction("");

        private final String action;

        SwimLaneTileAction(String action) {
            this.action = action;
        }

        public String getAction() {
            return action;
        }
    }

    /**
     * The mode how the display the swimlane on the UI.
     */
    public enum ClientUiClass {
        // The static items from the swimlane will be displayed in the dropdown list.
        PullDownMenu,
        EPG_Wall,
        VodCat_Small,
        Channel_Wall,
        Undefined
    }
}
