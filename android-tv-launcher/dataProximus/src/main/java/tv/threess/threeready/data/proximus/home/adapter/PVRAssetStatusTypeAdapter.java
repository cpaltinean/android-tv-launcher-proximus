package tv.threess.threeready.data.proximus.home.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.data.proximus.pvr.model.RecordingProximus;

/**
 * Type adapter to read the XML value as pvr asset status type enum.
 *
 * @author David Bondor
 * @since 21.09.2022
 */
public class PVRAssetStatusTypeAdapter  extends XmlAdapter<String, RecordingProximus.PVRAssetStatus> {

    @Override
    public RecordingProximus.PVRAssetStatus unmarshal(String value) {
        switch (value) {
            default:
                return RecordingProximus.PVRAssetStatus.Unknown;
            case "RecordSuccess" :
                return RecordingProximus.PVRAssetStatus.RecordSuccess;
            case "RecordFailed" :
                return RecordingProximus.PVRAssetStatus.RecordFailed;
        }
    }

    @Override
    public String marshal(RecordingProximus.PVRAssetStatus v) {
        throw new IllegalStateException("Not implemented.");
    }
}
