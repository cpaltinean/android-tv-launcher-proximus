package tv.threess.threeready.data.proximus.account.model.account;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Proximus item subtypes used for {@link PackageInfoProximus}.
 *
 * Created by Daniela Toma on 04/21/2020.
 */
@XmlRootElement(name = "ItemSubtypes")
public class ItemSubtypesProximus implements Serializable {

    @XmlElement(name = "ItemSubtype")
    private String mItemSubtype;

    @XmlElement(name = "BufferSizeMin")
    private int mBufferSizeMin;

    public int getBufferSizeMin() {
        return mBufferSizeMin;
    }

    public String getItemSubtype() {
        return mItemSubtype;
    }
}
