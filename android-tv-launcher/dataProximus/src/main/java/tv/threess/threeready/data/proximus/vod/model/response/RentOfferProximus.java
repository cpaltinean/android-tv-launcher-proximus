package tv.threess.threeready.data.proximus.vod.model.response;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.vod.model.CurrencyType;
import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IVodPrice;

/**
 * Proximus implementation of the rent offer item.
 *
 * @author Eugen Guzyk
 * @since 2018.11.13
 */
@XmlRootElement(name = "RentalItem")
public class RentOfferProximus implements IRentOffer {

    @XmlElement(name = "RentalPeriodStartDate")
    private long mOfferStart;

    @XmlElement(name = "RentalPeriodEndDate")
    private long mOfferEnd;

    @XmlElement(name = "RentalCost")
    private float mRentalCost;

    @XmlElement(name = "RentalPeriodDuration")
    private long mRentalPeriodDurationMin;

    private List<VodPriceProximus> mPrices = new ArrayList<>();

    @Override
    public long getOfferStart() {
        return mOfferStart;
    }

    @Override
    public long getOfferEnd() {
        return mOfferEnd;
    }

    @Override
    public long getRentalTime() {
        return TimeUnit.MINUTES.toMillis(mRentalPeriodDurationMin);
    }

    @Override
    public boolean isFree() {
        return mRentalCost == 0f;
    }

    public float getRentalCost() {
        return mRentalCost;
    }

    public void updatePriceOptions(InvoicePrice price, boolean addCreditPrice) {
        String priceDetermination = null;

        if (price != null) {
            mRentalCost = price.getPrice().floatValue();
            priceDetermination = price.getPriceDetermination();
        }

        mPrices = new ArrayList<>();
        mPrices.add(new VodPriceProximus(mRentalCost, CurrencyType.Euro, priceDetermination));

        if (addCreditPrice && !isFree()) {
            mPrices.add(new VodPriceProximus(1d, CurrencyType.Credit, priceDetermination));
        }
    }

    @Override
    public List<? extends IVodPrice> getPriceOptions() {
        return mPrices;
    }
}
