package tv.threess.threeready.data.proximus.account.model.account.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.data.proximus.account.model.account.AccountResourceProximus;

/**
 * Type adapter to create a lookup map for account resources.
 *
 * @author Barabas Attila
 * @since 2020.04.09
 */
public class AccountResourcesMapAdapter extends XmlAdapter<List<AccountResourceProximus>,
                Map<AccountResourceProximus.Key, AccountResourceProximus>> {

    @Override
    public Map<AccountResourceProximus.Key, AccountResourceProximus> unmarshal(List<AccountResourceProximus> accountResources) {
        Map<AccountResourceProximus.Key, AccountResourceProximus> resourcesMap = new HashMap<>();

        for (AccountResourceProximus resource : accountResources) {
            resourcesMap.put(resource.getKey(), resource);
        }

        return resourcesMap;
    }

    @Override
    public List<AccountResourceProximus> marshal(Map<AccountResourceProximus.Key, AccountResourceProximus> v) {
        throw new IllegalStateException("Not implemented.");
    }
}
