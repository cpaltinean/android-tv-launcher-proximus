/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.proximus.home.model.swimalane;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.api.generic.helper.NumberUtils;
import tv.threess.threeready.data.proximus.home.adapter.SwimLaneParamResponsibleAdapterProximus;

/**
 * Extra parameter for a swim lane which needs to be filled and send to the backend to get the content..
 *
 * @author Barabas Attila
 * @since 2019.10.28
 */
@XmlRootElement(name = "Param")
public class SwimLaneQueryParamProximus implements Serializable {

    @XmlElement(name = "Name")
    private String mName;

    @XmlElement(name = "Responsible")
    @XmlJavaTypeAdapter(SwimLaneParamResponsibleAdapterProximus.class)
    private SwimLaneParamResponsibleProximus mResponsible;

    @XmlElement(name = "ValueToken")
    private String mValueToken;

    @XmlElement(name = "ListSort")
    private int mListSort;

    public String getName() {
        return mName;
    }

    public SwimLaneParamResponsibleProximus getResponsible() {
        return mResponsible;
    }

    public String getValueToken() {
        return mValueToken;
    }

    public int getValue(int defaultValue) {
        return NumberUtils.parseInt(mValueToken, defaultValue);
    }

    public int getListSort() {
        return mListSort;
    }
}
