package tv.threess.threeready.data.proximus.home.model.editorial;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.AppEditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.CategoryEditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.VodDetailItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.VodPlaybackItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.WebPageEditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemStyle;
import tv.threess.threeready.api.home.model.module.EditorialItemStyle.VariantSize.Variant;
import tv.threess.threeready.data.proximus.home.model.swimalane.BannerProximus;

/**
 * Editorial tile for a proximus banner.
 *
 * @author Barabas Attila
 * @since 7/4/21
 */
public class BannerEditorialProximus implements EditorialItem {
    private final BannerProximus mBanner;
    private final EditorialItemStyle.VariantSize mSize;
    private final EditorialItemAction mAction;

    public BannerEditorialProximus(BannerProximus banner) {
        mBanner = banner;
        mSize = new EditorialItemStyle.VariantSize(
                mBanner.getType() == BannerProximus.Type.Wide
                        ? Variant.WideBanner : Variant.SmallBanner);

        // Create editorial action for action link.
        BannerProximus.BannerActionLink actionLink = mBanner.getBannerActionLink();
        if (!TextUtils.isEmpty(actionLink.getHtmlPage())) {
            mAction = (WebPageEditorialItemAction) actionLink::getHtmlPage;

        } else if (actionLink.getVodItem() != null) {
            mAction = (VodDetailItemAction) () -> actionLink.getVodItem().getVodId();

        } else if (actionLink.getLinkVod() != null) {
            mAction = (VodPlaybackItemAction) () -> actionLink.getLinkVod().getVodId();

        } else if (actionLink.getVodCategory() != null) {
            mAction = (CategoryEditorialItemAction) actionLink::getVodCategory;

        } else if (actionLink.getAppItem() != null) {
            mAction = (AppEditorialItemAction) () -> actionLink.getAppItem().getPackageId();

        } else {
            mAction = null;
        }
    }

    @Override
    public String getId() {
        return String.valueOf(mBanner.hashCode());
    }

    @Nullable
    @Override
    public String getFallbackTitle() {
        return mBanner.getText();
    }

    @Nullable
    @Override
    public EditorialItemStyle getStyle() {
        return mStyle;
    }

    @Override
    public EditorialItemAction getAction() {
        return mAction;
    }

    @Override
    public boolean hasViewedFeedback() {
        // View feedback code only for wide banners.
        return mBanner.getType() == BannerProximus.Type.Wide
                && !TextUtils.isEmpty(mBanner.getViewedFeedbackCode());
    }

    @Override
    public boolean hasClickedFeedback() {
        return !TextUtils.isEmpty(mBanner.getClickedFeedbackCode());
    }

    public BannerProximus getBanner() {
        return mBanner;
    }

    private final EditorialItemStyle mStyle = new EditorialItemStyle() {
        @Nullable
        @Override
        public IImageSource getBackgroundImage() {
            return mBanner.getImage();
        }

        @Nullable
        @Override
        public Size getWidth() {
            return mSize;
        }

        @Nullable
        @Override
        public Size getHeight() {
            return mSize;
        }

        @Nullable
        @Override
        public Size getFocusScaleSize() {
            return mSize;
        }
    };
}
