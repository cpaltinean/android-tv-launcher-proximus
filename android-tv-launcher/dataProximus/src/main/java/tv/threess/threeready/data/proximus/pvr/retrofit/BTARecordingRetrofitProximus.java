package tv.threess.threeready.data.proximus.pvr.retrofit;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import tv.threess.threeready.data.proximus.pvr.model.request.RecordingAddRequestProximus;
import tv.threess.threeready.data.proximus.pvr.model.request.RecordingDeleteRequest;
import tv.threess.threeready.data.proximus.pvr.model.request.RecordingDeleteSeriesRequest;
import tv.threess.threeready.data.proximus.pvr.model.request.RecordingScheduleSeriesRequest;
import tv.threess.threeready.data.proximus.pvr.model.request.RecordingStopRequest;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingAddResponseProximus;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingDeleteResponse;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingDeleteSeriesResponse;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingListResponseProximus;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingScheduleSeriesResponse;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingStopResponse;
import tv.threess.threeready.data.proximus.pvr.model.response.SeriesRecordingListResponseProximus;
import tv.threess.threeready.data.proximus.pvr.model.series.response.RecordingQuotaResponseProximus;

/**
 * Retrofit service to access recording related information from the BTA backend.
 *
 * Created by Noemi Dali on 15.04.2020.
 */
public interface BTARecordingRetrofitProximus {

    @GET("/broker/bta/getRecordingList?returnAllAssetStatusForMDP=IPTV")
    Call<RecordingListResponseProximus> getRecordingsList();

    @GET("/broker/bta/getSeriesRecordingList")
    Call<SeriesRecordingListResponseProximus> getSeriesRecordingList();

    @GET("/broker/bta/getRecordingQuota")
    Call<RecordingQuotaResponseProximus> getRecordingQuota();

    @POST("/broker/bta/addRecording")
    Call<RecordingAddResponseProximus> scheduleRecording(@Body RecordingAddRequestProximus request);

    @POST("/broker/bta/addSeriesRecording")
    Call<RecordingScheduleSeriesResponse> scheduleSeries(@Body RecordingScheduleSeriesRequest request);

    @POST("/broker/bta/deleteRecordings")
    Call<RecordingDeleteResponse> deleteRecordings(@Body RecordingDeleteRequest request);

    @POST("/broker/bta/deleteSeriesRecording")
    Call<RecordingDeleteSeriesResponse> deleteSeriesRecording(@Body RecordingDeleteSeriesRequest request);

    @POST("/broker/bta/stopRecording")
    Call<RecordingStopResponse> stopRecording(@Body RecordingStopRequest request);
}
