package tv.threess.threeready.data.proximus.vod.model.response;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.data.proximus.home.model.swimalane.BannerProximus;

/**
 * BTA backend response for the content of a VOD category.
 *
 * @author Barabas Attila
 * @since 2019.10.26
 */
public class VodCategoryContentResponseProximus extends VodBaseResponseProximus {

    @XmlElementWrapper(name = "DisplayList")
    @XmlElement(name = "DisplayItem")
    protected DisplayItemList mDisplayItems;

    public List<BaseVodItemProximus> getVODs() {
        if (mDisplayItems == null || mDisplayItems.getItems() == null || mVariants == null) {
            return Collections.emptyList();
        }

        Map<String, InvoicePrice> priceMap = getPriceMap();
        List<String> vodIds = getVodIds();
        return getGroupedBaseVodItems(vodIds, priceMap);
    }

    private List<String> getVodIds() {
        List<String> vodIds = new ArrayList<>();
        for (DisplayItem displayItem : mDisplayItems.getItems()) {
            List<String> variantIds = new ArrayList<>();

            // Build variant id list.
            if (displayItem.mMovieRefrences != null && !displayItem.mMovieRefrences.isEmpty()) {
                for (MovieRef ref : displayItem.mMovieRefrences) {
                    if (TextUtils.isEmpty(ref.getReference())) {
                        variantIds.add(ref.getExternalReference());
                    } else {
                        variantIds.add(ref.getReference());
                    }
                }
            }

            if (!variantIds.isEmpty()) {
                vodIds.add(BaseVodItemProximus.getVodId(variantIds));
            }
        }

        return vodIds;
    }

    /**
     * @return The number of items available in the category on the backend.
     */
    public int getTotalCount() {
        return mDisplayItems == null ? 0 : mDisplayItems.mTotalCount;
    }

    @XmlRootElement(name = "DisplayList")
    protected static class DisplayItemList {

        @XmlAttribute(name = "totalDisplayItems")
        private int mTotalCount;

        @XmlAttribute(name = "count")
        private int mCount;

        @XmlElement(name = "DisplayItem")
        protected ArrayList<DisplayItem> mItems;

        public ArrayList<DisplayItem> getItems() {
            return mItems;
        }
    }

    @XmlRootElement(name = "DisplayItem")
    protected static class DisplayItem {

        @XmlElement(name = "MovieRef")
        private List<MovieRef> mMovieRefrences;

        @XmlElement(name = "ProgramRef")
        private List<ProgramRef> mProgramReferences;

        @XmlElement(name = "ContentCategoryRef")
        private List<CategoryRef> mCategoryRef;

        @XmlElement(name = "ChannelRef")
        private List<ChannelRef> mChannelRef;

        @XmlElement(name = "Banner")
        private List<BannerProximus> mBanners;

        @NonNull
        public List<MovieRef> getMovieReferences(){
            if(mMovieRefrences == null){
                return Collections.emptyList();
            }
            return mMovieRefrences;
        }

        @NonNull
        public List<ProgramRef> getProgramReferences(){
            if (mProgramReferences == null) {
                return Collections.emptyList();
            }
            return mProgramReferences;
        }

        @NonNull
        public List<CategoryRef> getCategoryReferences(){
            if (mCategoryRef == null) {
                return Collections.emptyList();
            }
            return mCategoryRef;
        }

        @NonNull
        public List<ChannelRef> getChannelReferences(){
            if (mChannelRef == null) {
                return Collections.emptyList();
            }
            return mChannelRef;
        }

        public List<BannerProximus> getBanners() {
            if (mBanners == null) {
                return Collections.emptyList();
            }
            return mBanners;
        }
    }

    @XmlRootElement(name = "ProgramRef")
    protected static class ProgramRef{

        @XmlAttribute(name = "ref")
        private String mRef;

        public String getReference(){
            return mRef;
        }
    }

    @XmlRootElement(name = "ContentCategoryRef")
    protected static class CategoryRef {

        @XmlElement(name = "extref")
        private String mRef;

        public String getRef() {
            return mRef;
        }
    }

    @XmlRootElement(name = "ChannelRef")
    protected static class ChannelRef {

        @XmlElement(name = "extref")
        private String mRef;

        public String getRef() {
            return mRef;
        }
    }
}