package tv.threess.threeready.data.proximus.playback.model.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import tv.threess.threeready.api.playback.model.IReplayOttStreamingSession;
import tv.threess.threeready.data.proximus.playback.model.request.ReplayStreamingBucketRequestProximus.VideoQuality;

/**
 * Proximus backend model to request the next or previous streaming bucket.
 *
 * @author Barabas Attila
 * @since 2020.07.09
 */
public class AdjectedReplayStreamingBucketRequestProximus implements Serializable {

    @SerializedName("bucketSpec")
    private final BucketSpec mBucketSpec;

    @SerializedName("videoQuality")
    private final VideoQuality mVideoQuality;

    @SerializedName("playout")
    private final Playout mPlayout;

    @SerializedName("mimeTypes")
    private final List<String> mimeTypes = Collections.singletonList("application/dash+xml");

    public AdjectedReplayStreamingBucketRequestProximus(IReplayOttStreamingSession currentSession,
                                                        VideoQuality quality, String deviceId, NextPrev nextPrev) {
        mVideoQuality = quality;

        // Bucket spec.
        mBucketSpec = new BucketSpec();
        mBucketSpec.mBucketId = currentSession.getId();
        mBucketSpec.mNextPrev = nextPrev;

        // Device spec.
        mPlayout = new Playout();
        mPlayout.mDeviceId = deviceId;
    }

    private static class BucketSpec {

        @SerializedName("bucketId")
        private String mBucketId;

        @SerializedName("nextPrev")
        private NextPrev mNextPrev;

    }

    private static class Playout {
        @SerializedName("deviceId")
        private String mDeviceId;
    }


    public enum NextPrev {
        @SerializedName("nextBucket")
        Next,

        @SerializedName("prevBucket")
        Previous
    }
}
