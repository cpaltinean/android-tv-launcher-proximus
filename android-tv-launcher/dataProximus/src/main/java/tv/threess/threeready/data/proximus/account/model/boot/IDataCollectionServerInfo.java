package tv.threess.threeready.data.proximus.account.model.boot;

/**
 * Interface for data collection server information.
 * This contains all the necessary fields for sending events and metrics to their respective servers.
 *
 * This information can come from:
 * - Server list information
 * - Boot config
 *
 * @author Andor Lukacs
 * @since 2020-06-16
 */
public interface IDataCollectionServerInfo {

    DataCollectionServerType getCollectionType();

    String getEndpoint();

    String getHeaders();

    long getMaxKeepPeriod();

    int getMaxKeepSize();

    int getMaxBatchSize();

    long getRandomPeriod();

    long getTimeoutPeriod();

    enum DataCollectionServerType {
        TYPE_EVENT,
        TYPE_METRIC,
        TYPE_UNKNOWN
    }
}
