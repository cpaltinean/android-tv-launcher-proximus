package tv.threess.threeready.data.proximus.account.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import tv.threess.threeready.data.proximus.account.model.account.AccountInfoResponseProximus;
import tv.threess.threeready.data.proximus.account.model.account.DeleteSTBPropertiesBodyProximus;
import tv.threess.threeready.data.proximus.account.model.account.STBPropertiesBodyProximus;
import tv.threess.threeready.data.proximus.account.model.account.UpdateAccountInfoBodyProximus;
import tv.threess.threeready.data.proximus.account.model.account.UpdateResponseProximus;
import tv.threess.threeready.data.proximus.account.model.boot.TMServerInfoResponseProximus;
import tv.threess.threeready.data.proximus.account.model.partner.NetflixTokenRequestProximus;
import tv.threess.threeready.data.proximus.account.model.partner.NetflixTokenResponseProximus;
import tv.threess.threeready.data.proximus.account.model.registration.request.DRMClientRegistrationRequestProximus;
import tv.threess.threeready.data.proximus.account.model.registration.request.STBRegistrationRequestProximus;
import tv.threess.threeready.data.proximus.account.model.registration.request.STBRegistrationUpdateRequestProximus;
import tv.threess.threeready.data.proximus.account.model.registration.response.DRMRegisteredClientsResponseProximus;
import tv.threess.threeready.data.proximus.account.model.registration.response.STBRegistrationInfoResponseProximus;
import tv.threess.threeready.data.proximus.account.model.server.InterfaceResponseProximus;
import tv.threess.threeready.data.proximus.account.model.settings.STBSettingsResponseProximus;
import tv.threess.threeready.data.proximus.generic.interceptor.BackendIsolationInterceptor;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;
import tv.threess.threeready.data.proximus.iam.IAMOkHttpInterceptor;

/**
 * Retrofit interface to access account info related data from backend.
 *
 * @author Barabas Attila
 * @since 2020.03.19
 */
public interface BTAAccountRetrofitProximus {

    @GET("/broker/bta/getAccountInfo")
    Call<AccountInfoResponseProximus> getAccountInfo();

    @POST("/broker/bta/updateAccountInfo")
    Call<UpdateResponseProximus> updateAccountInfo(@Body UpdateAccountInfoBodyProximus body,
                                                   @Query("auth_APIN") String pinAccount);

    @GET("/broker/bta/getTMInfo")
    @BackendIsolationInterceptor.Allow
    Call<TMServerInfoResponseProximus> getTMInfo();

    @GET("/broker/bta/getInterface")
    @BackendIsolationInterceptor.Allow
    @IAMOkHttpInterceptor.MustAuthenticate
    Call<InterfaceResponseProximus> getInterface();

    @GET("/broker/bta/getDRMClientRegistrations")
    Call<DRMRegisteredClientsResponseProximus> getDRMClientRegistration();

    @POST("/broker/bta/registerDRMClient")
    Call<BTAResponseProximus> registerDRMClient(
            @Body DRMClientRegistrationRequestProximus request);

    @POST("/broker/bta/setSTBProperties")
    Call<UpdateResponseProximus> setSTBProperties(@Body STBPropertiesBodyProximus body);

    @GET("/broker/bta/getSTBProperties")
    Call<STBSettingsResponseProximus> getSTBProperties();

    @POST("/broker/bta/deleteSTBProperties")
    Call<UpdateResponseProximus> deleteSTBProperties(@Body DeleteSTBPropertiesBodyProximus body);

    @GET("/broker/bta/getSTBRegistrationInfo")
    Call<STBRegistrationInfoResponseProximus> getSTBRegistrationInfo();

    @POST("/broker/bta/autoConfig")
    Call<BTAResponseProximus> autoConfig(@Query("SubscriberAccountNum") String accountNumber,
                                         @Body STBRegistrationRequestProximus request);

    @POST("/broker/bta/updateSTBRegistrationInfo")
    Call<BTAResponseProximus> updateSTBRegistrationInfo(@Body STBRegistrationUpdateRequestProximus request);

    @POST("/broker/bta/getToken")
    Call<NetflixTokenResponseProximus> getNetflixToken(@Query("fc_UILanguage") String language, @Body NetflixTokenRequestProximus request);

    @POST("/broker/bta/registerAppInstance")
    Call<BTAResponseProximus> registerAppInstance(@Query("PNSInstanceID") String instanceId);

    @GET("/broker/bta/isAlive")
    @BackendIsolationInterceptor.Allow
    Call<ResponseBody> isAlive();
}
