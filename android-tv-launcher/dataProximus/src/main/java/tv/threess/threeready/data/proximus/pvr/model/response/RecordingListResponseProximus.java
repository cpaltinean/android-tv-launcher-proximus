package tv.threess.threeready.data.proximus.pvr.model.response;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.api.tv.model.IRecordingListResponse;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;
import tv.threess.threeready.data.proximus.pvr.model.RecordingProximus;
import tv.threess.threeready.data.proximus.tv.adapter.EPGKeyIndexMapAdapter;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.MetaInfoProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.ProgramMetaInfoKeyProximus;

/**
 * Proximus specific implementation for recording list retrofit response.
 *
 * Created by Noemi Dali on 14.04.2020.
 */
@XmlRootElement(name = "BTAResponse")
public class RecordingListResponseProximus extends BTAResponseProximus implements IRecordingListResponse<RecordingProximus> {

    @XmlElement(name = "Meta")
    private Meta mMeta;

    @XmlElementWrapper(name = "RecordingHistory")
    @XmlElement(name = "Recording")
    private List<RecordingProximus> mRecordingList;

    public List<RecordingProximus> getRecordingList() throws IOException {
        for (RecordingProximus recordingProximus: mRecordingList) {
            BufferedReader programMetaInfoReader = new BufferedReader(
                    new StringReader(recordingProximus.getProgramMetaInfo()));

            String metaInfoString;
            while ((metaInfoString = programMetaInfoReader.readLine()) != null) {
                String[] programMetaInfoArray = metaInfoString.split(MetaInfoProximus.META_INFO_SEPARATOR);
                MetaInfoProximus<ProgramMetaInfoKeyProximus> programMetaInfo = new MetaInfoProximus<>(
                        mMeta.mProgram, programMetaInfoArray);
                recordingProximus.setMetaInfo(programMetaInfo);

            }
        }

        return mRecordingList;
    }

    @XmlRootElement(name = "Meta")
    private static class Meta {

        @XmlJavaTypeAdapter(EPGKeyIndexMapAdapter.class)
        @XmlElement(name = "Program")
        private Map<String, Integer> mProgram;
    }
}
