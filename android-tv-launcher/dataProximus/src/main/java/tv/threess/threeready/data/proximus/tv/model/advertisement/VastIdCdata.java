package tv.threess.threeready.data.proximus.tv.model.advertisement;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * Utility class of a wrapper tag of a cdata value containing an id attribute:
 *
 * <Tag id="id">
 *   <![CDATA[ value ]]>
 * </Tag>
 */
public class VastIdCdata {
    @XmlAttribute(name = "id")
    private String mId;

    @XmlValue
    @JacksonXmlCData
    private String mValue;

    public String getId() {
        return mId;
    }

    public String getValue() {
        return mValue;
    }

    @Override
    public String toString() {
        if (mValue == null || mValue.trim().isEmpty()) {
            return mId + "()";
        }
        return mId + "(" + mValue.trim() + ")";
    }
}
