/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.tv.model.channel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.middleware.model.TifChannel;

/**
 * Channel information from the tif database to be merged with the application channels.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.06.07
 */
public class TifChannelTch implements TifChannel {

    private final long mChannelId;
    private final String mInputId;
    private final String mDisplayNumber;
    private final String mData;

    public TifChannelTch(long channelId, String displayNumber, String inputId, String data) {
        mChannelId = channelId;
        mDisplayNumber = displayNumber;
        mInputId = inputId;
        mData = data;
    }

    @Override
    public long getChannelId() {
        return mChannelId;
    }

    @Override
    public String getInputId() {
        return mInputId;
    }

    @Override
    public String getData() {
        return mData;
    }

    public static class InternalProviderData {

        @SerializedName("transport")
        private Transport mTransport;

        @SerializedName("path")
        private String mPath;

        @SerializedName("setup")
        private String mSetup;

        @SerializedName("uri")
        private String mUri;

        @SerializedName("name")
        private String mName;

        @SerializedName("type")
        private String mType;

        @SerializedName("scrambled")
        private Boolean mScrambled;

        @SerializedName("running-status")
        private Integer mRunningStatus;

        @SerializedName("has-eitpf")
        private Boolean mHasEitpf;

        @SerializedName("has-eitschedule")
        private Boolean mHasEitschedule;

        @SerializedName("entry")
        private String mEntry;

        @SerializedName("public")
        private ProviderPublicData mPublic;

        @SerializedName("linkage-id")
        private String mLinkageId;

        @SerializedName("visible")
        private Boolean mVisible;

        @SerializedName("raw-descriptors")
        private final List<Object> mRawDescriptors = new ArrayList<Object>();

        public ProviderPublicData getPublic() {
            return mPublic;
        }

        public String getUri() {
            return mUri;
        }
    }

    public static class Transport {
        @SerializedName("type")
        private int type;

        @SerializedName("typeStr")
        private String typeStr;

        public int getType() {
            return type;
        }

        public String getTypeStr() {
            return typeStr;
        }
    }
    public static class ProviderPublicData {

        @SerializedName("scrambled")
        private Boolean scrambled;

        @SerializedName("running-status")
        private Integer runningStatus;

        @SerializedName("linkage-id")
        private String linkageId;

        @SerializedName("visible")
        private Boolean visible;

        @SerializedName("raw-descriptors")
        private final List<Object> rawDescriptors = new ArrayList<Object>();

        public Boolean getScrambled() {
            return scrambled;
        }

        public Integer getRunningStatus() {
            return runningStatus;
        }

        public String getLinkageId() {
            return linkageId;
        }

        public String getMappingId() {
            return linkageId;
        }

        public Boolean getVisible() {
            return visible;
        }

        public List<Object> getRawDescriptors() {
            return rawDescriptors;
        }
    }
}
