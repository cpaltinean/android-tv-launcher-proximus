package tv.threess.threeready.data.proximus.home.model.module;

import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.List;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceService;
import tv.threess.threeready.api.home.model.module.ModuleDataType;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.tv.TvChannelCacheType;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.proximus.home.model.AccessPointProximus;
import tv.threess.threeready.data.proximus.home.model.page.ReplayChannelPageConfig;
import tv.threess.threeready.data.proximus.home.model.selector.ChannelMenuSelector;
import tv.threess.threeready.data.proximus.home.model.selector.ReplayChannelDataSourceSelector;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneHubResponseProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * ModuleConfig used for replay channel content wall.
 *
 * @author David Bondor
 * @since 4.28.2022
 */
public class ReplayChannelWallSwimlaneModuleProximus extends ContentWallSwimlaneModuleProximus implements ReplayChannelPageModuleProximus {

    private final static int DEFAULT_PAGE_SIZE = 30;

    private final SwimLaneHubResponseProximus mHub;
    private final AccessPointProximus mAccessPoint;

    private ReplayChannelDataSourceSelector mSelector;

    public ReplayChannelWallSwimlaneModuleProximus(SwimLaneProximus swimlane, SwimLaneHubResponseProximus hub) {
        super(swimlane);
        mHub = hub;
        List<String> accessPointIds = mSwimlane.getAccessPointIds();
        mAccessPoint = ArrayUtils.isEmpty(accessPointIds) ? null : mHub.getAccessPoint(accessPointIds.get(0));
    }

    @Nullable
    @Override
    public ModuleDataSource getDataSource() {
        return mDataSource;
    }

    @Override
    public ModuleCardVariant getCardVariant() {
        return ModuleCardVariant.C1;
    }

    @Override
    public ModuleDataType getDataType() {
        return ModuleDataType.REPLAY_CHANNEL;
    }

    @Override
    protected int getPageDefaultPageSize(SwimLaneProximus swimLane) {
        return DEFAULT_PAGE_SIZE;
    }

    @Override
    public List<DataSourceSelector> getSelectors() {
        if (mSelector == null) {
            mSelector = new ReplayChannelDataSourceSelector();
        }
        return Collections.singletonList(mSelector);
    }

    @Override
    public int getMinElements() {
        return 0;
    }

    @Nullable
    @Override
    public PageConfig getPageConfigForChannel(TvChannel channel) {
        return new ReplayChannelPageConfig(mAccessPoint, mHub, channel.getId(), channel.getName());
    }

    private final ModuleDataSource mDataSource = new ModuleDataSource() {

        @Override
        public ModuleDataSourceService getService() {
            return ModuleDataSourceService.TV;
        }

        @Override
        public ModuleDataSourceMethod getMethod() {
            return ModuleDataSourceMethod.REPLAY_CHANNEL_PAGE_ITEMS;
        }
    };
}
