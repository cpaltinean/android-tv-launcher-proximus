/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.tv.observable;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.data.proximus.tv.TvProxyProximus;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Rx observable class to return details information about a tv event.
 *
 * @author Eugen Guzyk
 * @since 2018.07.10
 */
public class BroadcastDetailObservable implements ObservableOnSubscribe<IBroadcast> {

    private static final String TAG = BroadcastDetailObservable.class.getName();
    private final TvProxyProximus mTvProxy = Components.get(TvProxyProximus.class);

    private final Context mContext;

    private String mBroadcastId;
    private IBroadcast mBroadcast;

    private Disposable mDelayDisposable;

    public BroadcastDetailObservable(Context context, String broadcastId) {
        mContext = context;
        mBroadcastId = broadcastId;
    }

    public BroadcastDetailObservable(Context context, IBroadcast broadcast) {
        mContext = context;
        mBroadcast = broadcast;
    }

    @Override
    public void subscribe(@NonNull ObservableEmitter<IBroadcast> emitter) throws Exception {
        //check if both of specific variable is null
        if (mBroadcast == null && mBroadcastId == null) {
            return;
        }

        //if broadcast null need to get broadcast from TvHandler
        if (mBroadcast == null) {
            List<? extends IBroadcast> broadcastList = mTvProxy.getBroadcasts(
                    Collections.singletonList(mBroadcastId),
                    TimeUtils.getEPGWindowBorder(-Settings.epgPastWindowLength.get(TimeUnit.DAYS.toMillis(7))),
                    TimeUtils.getEPGWindowBorder(Settings.epgFutureWindowLength.get(TimeUnit.DAYS.toMillis(7)))
            );
            if (ArrayUtils.isEmpty(broadcastList)) {
                throw new IllegalStateException("No broadcast with id : " + mBroadcastId);
            }

            mBroadcast = broadcastList.get(0);
        }

        //cancel inner observable when needed
        emitter.setCancellable(this::disposeDisposable);

        //create inner observable and calculate the delay when needs to update
        // the detail page of a broadcast
        createDisposable(emitter);

        emitter.onNext(mBroadcast);
    }

    /**
     * Create an observable when need to trigger the detail page update after the delay.
     */
    private void createDisposable(@NonNull ObservableEmitter<IBroadcast> emitter) {
        RxUtils.disposeSilently(mDelayDisposable);

        long delay = calculateDelay();

        if (delay != 0) {
            mDelayDisposable = Completable.timer(delay, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe(() -> {
                        Log.d(TAG, "Delay ( " + TimeUnit.MILLISECONDS.toSeconds(delay)
                                + " seconds) is passed. Trigger update.");
                        emitter.onNext(mBroadcast);
                        createDisposable(emitter);
                    });
        }
    }

    /**
     * Calculate the delay from the current timestamp to the broadcast start/end.
     *
     * @return the calculated delay in milliseconds.
     */
    private long calculateDelay() {
        long delay = mBroadcast.getStart() - System.currentTimeMillis();

        if (delay > 0) {
            return delay;
        }

        delay = mBroadcast.getEnd() - System.currentTimeMillis();

        if (delay > 0) {
            return delay;
        }

        return 0;
    }

    /**
     * Disposes the inner observable.
     */
    public void disposeDisposable() {
        RxUtils.disposeSilently(mDelayDisposable);
    }
}