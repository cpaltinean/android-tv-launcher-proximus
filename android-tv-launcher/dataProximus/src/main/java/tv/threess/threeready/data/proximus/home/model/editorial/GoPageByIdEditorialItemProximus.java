package tv.threess.threeready.data.proximus.home.model.editorial;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Editorial tile of a swimlane which opens an page.
 *
 * @author Barabas Attila
 * @since 5/27/21
 */
public class GoPageByIdEditorialItemProximus extends SwimlaneEditorialItemProximus {
    private final String mPageId;

    public GoPageByIdEditorialItemProximus(SwimLaneProximus swimLane,
                                           SwimLaneProximus.SwimlaneTile swimlaneTile, Integer position) {
        super(swimLane, swimlaneTile, position);
        mPageId = swimlaneTile.getPageId();
    }

    @Nullable
    @Override
    public EditorialItemAction getAction() {
        return mItemAction;
    }

    private final EditorialItemAction mItemAction = new EditorialItemAction.PageByIdEditorialItemAction() {

        @Nullable
        @Override
        public String getPageId() {
            return mPageId;
        }
    };
}
