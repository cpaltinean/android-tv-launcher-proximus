package tv.threess.threeready.data.proximus.account.model.server;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.api.log.Log;

/**
 * Type adapter class for server types.
 *
 * Created by Noemi Dali on 09.04.2020.
 */
public class ServerTypeAdapter extends XmlAdapter<String, ServerType> {
    private static final String TAG = Log.tag(ServerTypeAdapter.class);

    @Override
    public ServerType unmarshal(String v) throws Exception {
        for (ServerType type : ServerType.values()) {
            if (type.getType().equals(v)) {
                return type;
            }
        }
        Log.w(TAG, "Unknown server type: " + v);
        return null;
    }

    @Override
    public String marshal(ServerType v) throws Exception {
        throw new IllegalStateException("Not implemented");
    }
}
