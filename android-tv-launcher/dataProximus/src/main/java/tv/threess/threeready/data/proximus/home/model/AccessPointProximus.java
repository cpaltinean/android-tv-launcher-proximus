package tv.threess.threeready.data.proximus.home.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.data.proximus.home.adapter.AccessPointTypeAdapter;


/**
 * Model describes a page and its swimlanes.
 *
 * @author Barabas Attila
 * @since 5/5/21
 */
@XmlRootElement(name = "Accesspoint")
public class AccessPointProximus implements Serializable {

    @XmlElement(name = "Name")
    private String mName;

    @XmlElement(name = "ExternalID")
    private String mId;

    @XmlElement(name = "apGroupID")
    private String mGroupId;

    @XmlElementWrapper(name = "DisplayNames")
    @XmlElement(name = "DisplayName")
    private DisplayNamesProximus mDisplayNames;

    @XmlJavaTypeAdapter(AccessPointTypeAdapter.class)
    @XmlElementWrapper(name = "AccesspointType")
    private Type mType;

    @XmlElementWrapper(name = "apSwimLanes")
    @XmlElement(name = "apSwimlane")
    private List<ApSwimlane> mApSwimlanes;

    @XmlElementWrapper(name = "apPosters")
    @XmlElement(name = "poster")
    private List<Poster> mPosters;

    @XmlElement(name = "ListSort")
    private int mListSort;

    public String getName() {
        return mName;
    }

    public String getId() {
        return mId;
    }

    @Nullable
    public String getGroupId() {
        return mGroupId;
    }

    public Type getType() {
        return mType;
    }

    public int getListSort() {
        return mListSort;
    }

    @NonNull
    public DisplayNamesProximus getDisplayNames() {
        return mDisplayNames == null ? DisplayNamesProximus.EMPTY_NAMES : mDisplayNames;
    }

    /**
     * @return The image source for the UI language or null if there is no such.
     */
    @Nullable
    public IImageSource getLocalizedImage() {
        String currentLanguage = LocaleUtils.getApplicationLanguage();
        for (Poster poster : ArrayUtils.notNull(mPosters)) {
            if (Objects.equals(poster.mLanguage, currentLanguage)) {
                return poster;
            }
        }
        return null;
    }

    public List<ApSwimlane> getSwimlaneReferences() {
        return mApSwimlanes;
    }

    public static class ApSwimlane implements Serializable {
        @XmlAttribute(name = "ListSort")
        private int mSortOder;

        @XmlAttribute(name = "ref")
        private String mSwimlaneId;

        public int getSortOrder() {
            return mSortOder;
        }

        public String getSwimlaneId() {
            return mSwimlaneId;
        }
    }

    private static class Poster implements IImageSource {
        @XmlElement(name = "language")
        private String mLanguage;

        @XmlElement(name = "name")
        private String mName;

        @Override
        public String getUrl() {
            return mName;
        }

        @Override
        public Type getType() {
            return Type.DEFAULT;
        }
    }

    public enum Type {
        Page,
        PageGroup,
        SubPage,
    }
}
