package tv.threess.threeready.data.proximus.home.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneParamResponsibleProximus;

/**
 * Java XML adapter to read backend response field as {@link SwimLaneParamResponsibleProximus}.
 *
 * @author Barabas Attila
 * @since 2020.04.30
 */
public class SwimLaneParamResponsibleAdapterProximus extends XmlAdapter<String, SwimLaneParamResponsibleProximus> {

    @Override
    public SwimLaneParamResponsibleProximus unmarshal(String value) {
        switch (value) {
            default:
                return SwimLaneParamResponsibleProximus.Undefined;
            case "BepToken" :
                return SwimLaneParamResponsibleProximus.BepToken;
            case "clientToken" :
                return SwimLaneParamResponsibleProximus.ClientToken;
            case "pageVariable" :
                return SwimLaneParamResponsibleProximus.PageVariable;
            case "BepAdmin" :
                return SwimLaneParamResponsibleProximus.BepAdmin;
        }
    }

    @Override
    public String marshal(SwimLaneParamResponsibleProximus value) {
       throw new IllegalStateException("Not implemented.");
    }
}
