package tv.threess.threeready.data.proximus.watchlist.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import tv.threess.threeready.api.generic.model.WatchlistType;
import tv.threess.threeready.data.proximus.watchlist.request.AddToWatchlistRequestProximus;
import tv.threess.threeready.data.proximus.watchlist.response.GetWatchlistItemResponseProximus;
import tv.threess.threeready.data.proximus.watchlist.response.WatchlistItemsProximus;

/**
 * Retrofit service for watchlist microservice
 *
 * @author Andor Lukacs
 * @since 21/10/2020
 */
public interface WatchlistRetrofitProximus {

    @GET("/api/watchlist/owner/{account_type}/{account_id}")
    Call<WatchlistItemsProximus> getWatchlistItems(@Path("account_type") String accountType,
                                                   @Path("account_id") String accountId);

    @GET("/api/watchlist/watchlist-items/{watchlist_item_id}")
    Call<GetWatchlistItemResponseProximus> getWatchlistItemById(@Path("watchlist_item_id") String watchlistItemId);

    @GET("/api/watchlist/owner/{account_type}/{account_id}/item/{item_type}/{item_id}")
    Call<GetWatchlistItemResponseProximus> getWatchlistItemByItemId(@Path("account_type") String accountType,
                                                                    @Path("account_id") String accountId,
                                                                    @Path("item_type") WatchlistType type,
                                                                    @Path("item_id") String contentId);

    @POST("/api/watchlist/owner/{account_type}/{account_id}/watchlist-items")
    Call<GetWatchlistItemResponseProximus.WatchlistItem> addToWatchlist(@Path("account_type") String accountType,
                                                          @Path("account_id") String accountId,
                                                          @Body AddToWatchlistRequestProximus request);

    @DELETE("/api/watchlist/watchlist-items/{watchlist_item_id}")
    Call<ResponseBody> removeFromWatchlist(@Path("watchlist_item_id") String itemId);

}
