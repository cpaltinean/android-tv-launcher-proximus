package tv.threess.threeready.data.proximus.iam.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * IAM client registration server response.
 *
 * @author Barabas Attila
 * @since 2020.03.19
 */
public class IAMRegisterResponseProximus implements Serializable {

    @SerializedName("client_id")
    private String mClientId;

    @SerializedName("client_secret")
    private String mClientSecret;

    @SerializedName("scope")
    private String mScope;

    @SerializedName("grant_types")
    private List<String> mGrantType;


    public String getClientId() {
        return mClientId;
    }

    public String getClientSecret() {
        return mClientSecret;
    }

    public String getScope() {
        return mScope;
    }

    public String getGrantType() {
        if (mGrantType == null) {
            return "";
        }

        return TextUtils.join(" ", mGrantType);
    }
}
