package tv.threess.threeready.data.proximus.tv.adapter;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * TODO: Class description
 *
 * @author Barabas Attila
 * @since 2020.03.27
 */
public class EPGKeyIndexMapAdapter extends XmlAdapter<String, Map<String, Integer>> {

    private static final String DELIMITER_CHAR = "|";

    @Override
    public Map<String, Integer> unmarshal(String v) {
        Map<String, Integer> programMetaInfoIndexMap = new HashMap<>();
        StringTokenizer programMetaInfoKeyTokenizer = new StringTokenizer(v, DELIMITER_CHAR);
        for (int i = 0; programMetaInfoKeyTokenizer.hasMoreTokens(); ++i) {
            programMetaInfoIndexMap.put(programMetaInfoKeyTokenizer.nextToken(), i);
        }

        return programMetaInfoIndexMap;
    }

    @Override
    public String marshal(Map<String, Integer> v){
        throw new IllegalStateException("Not implemented.");
    }
}