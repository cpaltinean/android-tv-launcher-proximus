package tv.threess.threeready.data.proximus.home.model.selector;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus.ClientUiClass;

/**
 * Data source selector from a swimlane.
 *
 * @author Barabas Attila
 * @since 9/20/21
 */
public class SwimlaneDataSourceSelector extends DataSourceSelector {

    private final SwimLaneProximus mSwimLane;

    public SwimlaneDataSourceSelector(SwimLaneProximus swimlane) {
        super(generateSelectorOptions(swimlane));
        mSwimLane = swimlane;
    }

    private static List<SelectorOption> generateSelectorOptions(SwimLaneProximus swimLane) {
        List<SelectorOption> selectorOptions = new ArrayList<>();
        List<SwimLaneProximus.StaticTile> staticTiles = swimLane.getStaticTiles();
        staticTiles.sort((st1, st2) -> Integer.compare(st1.getSortOrder(), st2.getSortOrder()));
        for (SwimLaneProximus.StaticTile tile : staticTiles) {
            selectorOptions.add(new SwimlaneTileSelector(swimLane, tile));
        }
        return selectorOptions;
    }

    @Override
    public DisplayType getDisplayType() {
        return mSwimLane.getClientUiClass() == ClientUiClass.PullDownMenu ? DisplayType.DropDown : DisplayType.Menu;
    }

    @Override
    public String getName() {
        return mSwimLane.getDisplayNames().getLocalizedNameOrFirst();
    }
}
