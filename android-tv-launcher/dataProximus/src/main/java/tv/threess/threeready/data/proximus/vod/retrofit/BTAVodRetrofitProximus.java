package tv.threess.threeready.data.proximus.vod.retrofit;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import tv.threess.threeready.data.proximus.generic.interceptor.CacheControlOverrideOkHttpInterceptor.CacheControlOverride;
import tv.threess.threeready.data.proximus.vod.model.request.VodRentalRequest;
import tv.threess.threeready.data.proximus.vod.model.response.RentalExpirationResponseProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategoriesResponseProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategoryContentResponseProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodItemDetailResponseProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodRentalHistoryResponseProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodRentalResponse;
import tv.threess.threeready.data.proximus.vod.model.response.VodShopLanguageProximus;

/**
 * Retrofit interface for VOD related backend calls.
 *
 * @author Barabas Attila
 * @since 2020.04.23
 */
public interface BTAVodRetrofitProximus {

    @CacheControlOverride
    @GET("/broker/bta/getCategories?fc_UnlockedPackagesOnly=true&fc_Exclude_ExtRatingCodes=ND&fc_Selection=Leaf")
    Call<VodCategoriesResponseProximus> getLeafCategories(@Query("fc_HDCapable") boolean hdCapable,
                                                          @Query("fc_Language") VodShopLanguageProximus language);

    @CacheControlOverride
    @GET("/broker/bta/getDetails?fc_UnlockedPackagesOnly=true&fc_Exclude_ExtRatingCodes=ND&ac_InvoicePrices=true")
    Call<VodCategoryContentResponseProximus> getRootCategoryContent(@Query("fc_CategoryID") String rootCategoryId,
                                                                    @Query("fc_StartIndex") int startIndex,
                                                                    @Query("fc_HDCapable") boolean hdCapable,
                                                                    @Query("fc_Language") VodShopLanguageProximus language);

    @CacheControlOverride
    @GET("/broker/bta/getDetails?fc_UnlockedPackagesOnly=true&fc_Exclude_ExtRatingCodes=ND&ac_InvoicePrices=true")
    Call<VodCategoryContentResponseProximus> getCategoryContent(@Query("fc_CategoryID") String categoryId,
                                                                @Query("fc_StartIndex") int startIndex,
                                                                @Query("fc_HowMany") int count,
                                                                @Query("fc_HDCapable") boolean hdCapable,
                                                                @Query("fc_Language") VodShopLanguageProximus language);

    @CacheControlOverride
    @GET("/broker/bta/getDetails?fc_UnlockedPackagesOnly=true&fc_Exclude_ExtRatingCodes=ND&ac_InvoicePrices=true")
    Call<VodCategoryContentResponseProximus> getCategoryContent(@Query("fc_CategoryID") String categoryId,
                                                                @Query("fc_StartIndex") int startIndex,
                                                                @Query("fc_HowMany") int count,
                                                                @Query("fc_HDCapable") boolean hdCapable,
                                                                @Query("fc_Language") VodShopLanguageProximus language,
                                                                @Query("fc_Sort") String sortOption);

    @CacheControlOverride
    @GET("/broker/bta/getDetails?fc_UnlockedPackagesOnly=true&fc_Exclude_ExtRatingCodes=ND&ac_InvoicePrices=true")
    Call<VodItemDetailResponseProximus> getVodById(@Query("fc_ExternalIDs") String variantIds,
                                                   @Query("fc_HDCapable") boolean hdCapable);

    @GET("/broker/bta/getRentedItems")
    Call<RentalExpirationResponseProximus> getRentedItems();

    @POST("/broker/bta/itemRental")
    Call<VodRentalResponse> rentVod(@Query("auth_TPIN") String pin, @Body VodRentalRequest rentalRequest);

    @GET("/broker/bta/getRentalHistory")
    Call<VodRentalHistoryResponseProximus> getRentalHistory();
}
