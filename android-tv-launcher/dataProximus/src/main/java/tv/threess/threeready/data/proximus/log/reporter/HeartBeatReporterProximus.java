package tv.threess.threeready.data.proximus.log.reporter;

import android.content.Context;

import androidx.annotation.Nullable;

import java.util.List;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.Reporter;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.ReporterInfo;
import tv.threess.threeready.data.mw.MwProxy;

/**
 * Reporter for saving heart beat data to the db.
 *
 * @author Zsolt Bokor_
 * @since 2020.05.13
 */

public class HeartBeatReporterProximus extends Reporter {
    private static final String TAG = Log.tag(HeartBeatReporterProximus.class);

    private static final long PERSIST_HEART_BEAT_RATE = TimeUnit.SECONDS.toMillis(10);

    private final MwProxy mMwProxy = Components.get(MwProxy.class);

    public HeartBeatReporterProximus(Context context, ReporterInfo reporterInfo) {
        super(context, reporterInfo);
    }

    @Override
    public boolean canSend(Event event) {
        return false;
    }

    @Override
    public long getMaxKeepPeriod() {
        return 0;
    }

    @Override
    public int getMaxKeepSize() {
        return 0;
    }

    @Override
    public int getMaxBatchSize() {
        return 0;
    }

    @Override
    public int getBatchSize() {
        return 0;
    }

    @Override
    public long getBatchPeriod() {
        return PERSIST_HEART_BEAT_RATE;
    }

    @Override
    public void sendEvent(Event event) throws Exception {
    }

    @Override
    public void sendEvents(List list) throws Exception {
        mMwProxy.saveHeartBeat();
        Log.d(TAG, "Heart beat saved successfully.");
    }

    @Nullable
    @Override
    public String serializeEvent(Event event) {
        return null;
    }
}
