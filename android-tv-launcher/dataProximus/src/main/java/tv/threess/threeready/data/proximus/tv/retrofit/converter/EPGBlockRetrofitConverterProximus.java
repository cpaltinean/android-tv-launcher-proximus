package tv.threess.threeready.data.proximus.tv.retrofit.converter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import retrofit2.Converter;
import retrofit2.Retrofit;
import tv.threess.threeready.data.proximus.tv.model.broadcast.request.EPGBlockProximus;

/**
 * Convert block to string for EPG requests.
 *
 * @author Barabas Attila
 * @since 2020.03.30
 */
public class EPGBlockRetrofitConverterProximus extends Converter.Factory {
    @Override
    public Converter<?, String> stringConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        Converter<?, String> converter = null;
        if (type instanceof Class && ((Class<?>) type).isAssignableFrom(EPGBlockProximus.class)) {
            converter = value -> ((EPGBlockProximus) value).getName();
        }
        return converter;
    }
}