package tv.threess.threeready.data.proximus.account.model.account.adapter;


import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.data.proximus.account.model.account.AccountResourceProximus;

/**
 * Java type adapter to read string field as {@link AccountResourceProximus.Key}
 *
 * @author Barabas Attila
 * @since 2020.04.06
 */
public class AccountResourceKeyAdapterProximus extends XmlAdapter<AccountResourceProximus.Key, String> {

    @Override
    public AccountResourceProximus.Key marshal(String v) {
        switch (v) {
            default:
                return null;
            case "NetworkBandwidth":
                return AccountResourceProximus.Key.NetworkBandwidth;
            case "AvailableOverheadBandwidth":
                return AccountResourceProximus.Key.AvailableOverheadBandwidth;
        }
    }

    @Override
    public String unmarshal(AccountResourceProximus.Key v) {
        throw new IllegalStateException("Not implemented.");
    }
}
