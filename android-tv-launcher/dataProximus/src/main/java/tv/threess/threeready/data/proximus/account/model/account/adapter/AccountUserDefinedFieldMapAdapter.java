package tv.threess.threeready.data.proximus.account.model.account.adapter;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.data.proximus.account.model.account.AccountUserDefinedFieldProximus;

/**
 * Type adapter to create lookup map for account user defined fields.
 *
 * @author Barabas Attila
 * @since 2020.04.09
 */
public class AccountUserDefinedFieldMapAdapter extends XmlAdapter<List<AccountUserDefinedFieldProximus>,
        Map<AccountUserDefinedFieldProximus.Key, AccountUserDefinedFieldProximus>> {

    @Override
    public Map<AccountUserDefinedFieldProximus.Key, AccountUserDefinedFieldProximus> unmarshal(
            List<AccountUserDefinedFieldProximus> fields) {
        Map<AccountUserDefinedFieldProximus.Key, AccountUserDefinedFieldProximus> fieldsMap =
                new EnumMap<>(AccountUserDefinedFieldProximus.Key.class);
        for (AccountUserDefinedFieldProximus field : fields) {
            AccountUserDefinedFieldProximus.Key key = field.getKey();
            if (key != null) {
                fieldsMap.put(field.getKey(), field);
            }
        }

        return fieldsMap;
    }

    @Override
    public List<AccountUserDefinedFieldProximus> marshal(
            Map<AccountUserDefinedFieldProximus.Key, AccountUserDefinedFieldProximus> v) {
        throw new IllegalStateException("Not implemented.");
    }
}
