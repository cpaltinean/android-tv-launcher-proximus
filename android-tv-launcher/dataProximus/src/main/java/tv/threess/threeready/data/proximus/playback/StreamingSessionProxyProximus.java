/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.proximus.playback;

import java.io.IOException;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.playback.StreamingSessionProxy;
import tv.threess.threeready.api.playback.exception.SessionOpenException;
import tv.threess.threeready.api.playback.model.IOttStreamingSession;
import tv.threess.threeready.api.playback.model.IReplayOttStreamingSession;
import tv.threess.threeready.api.playback.model.IVodOttStreamingSession;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.proximus.generic.RetrofitServicesProximus;
import tv.threess.threeready.data.proximus.playback.model.request.AdjectedReplayStreamingBucketRequestProximus;
import tv.threess.threeready.data.proximus.playback.model.request.AdjectedReplayStreamingBucketRequestProximus.NextPrev;
import tv.threess.threeready.data.proximus.playback.model.request.ChannelStreamingSessionRequestProximus;
import tv.threess.threeready.data.proximus.playback.model.request.RecordingStreamingSessionRequestProximus;
import tv.threess.threeready.data.proximus.playback.model.request.ReplayStreamingBucketRequestProximus;
import tv.threess.threeready.data.proximus.playback.model.request.ReplayStreamingBucketRequestProximus.VideoQuality;
import tv.threess.threeready.data.proximus.playback.model.request.VodStreamingSessionRequestProximus;
import tv.threess.threeready.data.proximus.playback.model.response.BTAStreamingSessionProximus;
import tv.threess.threeready.data.proximus.playback.model.response.RecordingStreamingSessionProximus;
import tv.threess.threeready.data.proximus.playback.model.response.ReplayStreamingSessionProximus;
import tv.threess.threeready.data.proximus.playback.model.response.VodStreamingSessionProximus;
import tv.threess.threeready.data.proximus.playback.retrofit.BTAStreamingRetrofitProximus;
import tv.threess.threeready.data.proximus.playback.retrofit.MSStreamingRetrofitProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.TvBroadcastProximus;

/**
 * Proximus implementation of streaming-related operations
 *
 * @author Barabas Attila
 * @since 2018.06.22
 */
public class StreamingSessionProxyProximus implements StreamingSessionProxy, Component {

    private final RetrofitServicesProximus mRetrofit = Components.get(RetrofitServicesProximus.class);
    private final BTAStreamingRetrofitProximus mBTAStreamingRetrofit = mRetrofit.getBTAStreamingRetrofit();
    private final MSStreamingRetrofitProximus mMSStreamingRetrofit = mRetrofit.getMSStreamingRetrofit();

    /**
     * Open a live streaming session for a channel.
     *
     * @param channelId The id of the channel for which the streaming session needs to be opened.
     */
    @Override
    public BTAStreamingSessionProximus openChannelStreamingSession(String channelId) throws IOException {
        try {
            return RetrofitServicesProximus.execute(mBTAStreamingRetrofit.getURL(
                    new ChannelStreamingSessionRequestProximus(channelId)
            ));
        } catch (SessionOpenException e) {
            throw e;
        } catch (Exception e) {
            throw new SessionOpenException(e, false);
        }
    }

    /**
     * Open a replay streaming session for a broadcast.
     *
     * @param broadcast The broadcast for which the streaming session should be opened.
     */
    @Override
    public ReplayStreamingSessionProximus openReplayStreamingSession(IBroadcast broadcast) throws IOException {
        try {
            boolean hdInterest = SettingsProximus.hdProfile.get(false);
            return RetrofitServicesProximus.execute(mMSStreamingRetrofit.getReplayBucket(
                    new ReplayStreamingBucketRequestProximus(
                            TvBroadcastProximus.buildTrailId(broadcast.getId()),
                            hdInterest ? VideoQuality.HD : VideoQuality.SD,
                            Components.get(MwProxy.class).getEthernetMacAddress()
                    )
            ));
        } catch (SessionOpenException e) {
            throw e;
        } catch (Exception e) {
            throw new SessionOpenException(e, true);
        }
    }

    /**
     * Open streaming session for replay playback previous to the currently opened session.
     *
     * @param currentSession The currently opened session.
     * @return The continuous/gap-free/overlap-free session previous to the currently opened.
     */
    @Override
    public ReplayStreamingSessionProximus openPreviousReplayStreamingSession(IReplayOttStreamingSession currentSession) throws IOException {
        try {
            boolean hdInterest = SettingsProximus.hdProfile.get(false);
            return RetrofitServicesProximus.execute(mMSStreamingRetrofit.getNextReplayBucket(
                    new AdjectedReplayStreamingBucketRequestProximus(
                            currentSession,
                            hdInterest ? VideoQuality.HD : VideoQuality.SD,
                            Components.get(MwProxy.class).getEthernetMacAddress(), NextPrev.Previous
                    )
            ));
        } catch (SessionOpenException e) {
            throw e;
        } catch (Exception e) {
            throw new SessionOpenException(e, true);
        }
    }

    /**
     * Open streaming session for replay playback next to the currently opened session.
     *
     * @param currentSession The currently opened session.
     * @return The continuous/gap-free/overlap-free session next to the currently opened.
     */
    @Override
    public ReplayStreamingSessionProximus openNextReplayStreamingSession(IReplayOttStreamingSession currentSession) throws IOException {
        try {
            boolean hdInterest = SettingsProximus.hdProfile.get(false);
            return RetrofitServicesProximus.execute(mMSStreamingRetrofit.getNextReplayBucket(
                    new AdjectedReplayStreamingBucketRequestProximus(
                            currentSession,
                            hdInterest ? VideoQuality.HD : VideoQuality.SD,
                            Components.get(MwProxy.class).getEthernetMacAddress(), NextPrev.Next
                    )
            ));
        } catch (SessionOpenException e) {
            throw e;
        } catch (Exception e) {
            throw new SessionOpenException(e, true);
        }
    }

    /**
     * Open streaming session for VOD playback.
     *
     * @param vodVariant The VOD variant for which the streaming session should be opened.
     * @param vod        The parent VOD of the variant.
     * @return The streaming session which holds the URL and DRM information.
     */
    @Override
    public VodStreamingSessionProximus openVodStreamingSession(IVodItem vod, IVodVariant vodVariant) throws IOException {
        try {
            VodStreamingSessionProximus session = RetrofitServicesProximus.execute(mBTAStreamingRetrofit.getURL(
                    new VodStreamingSessionRequestProximus(vodVariant, false)
            ));

            // Set vod.
            session.setVod(vod);
            session.setVariant(vodVariant);
            return session;
        } catch (SessionOpenException e) {
            throw e;
        } catch (Exception e) {
            throw new SessionOpenException(e, false);
        }
    }

    /**
     * Open streaming session for VOD trailer playback.
     *
     * @param vodVariant The VOD variant for which the streaming session should be opened.
     * @param vod        The parent VOD of the variant.
     * @return The streaming session which holds the URL and DRM information.
     */
    @Override
    public IVodOttStreamingSession openTrailerStreamingSession(IVodItem vod, IVodVariant vodVariant) throws IOException {
        try {
            return RetrofitServicesProximus.execute(mBTAStreamingRetrofit.getURL(
                    new VodStreamingSessionRequestProximus(vodVariant, true)
            ));
        } catch (SessionOpenException e) {
            throw e;
        } catch (Exception e) {
            throw new SessionOpenException(e, false);
        }
    }

    /**
     * Open streaming session for recording playback
     *
     * @param recording The recording for which the session should be opened.
     * @return The streaming session which holds the URL and DRM information.
     */
    @Override
    public RecordingStreamingSessionProximus openRecordingStreamingSession(IRecording recording) throws IOException {
        try {
            RecordingStreamingSessionProximus session = RetrofitServicesProximus.execute(mBTAStreamingRetrofit.getURL(
                    new RecordingStreamingSessionRequestProximus(recording)
            ));

            // Set broadcast.
            session.setRecording(recording);
            return session;
        } catch (SessionOpenException e) {
            throw e;
        } catch (Exception e) {
            throw new SessionOpenException(e, false);
        }
    }

    /**
     * Close a previously opened streaming session.
     *
     * @param streamingSession The session which should be closed.
     */
    @Override
    public void closeStreamingSession(IOttStreamingSession streamingSession) {
        // Nothing to do on proximus.
    }
}
