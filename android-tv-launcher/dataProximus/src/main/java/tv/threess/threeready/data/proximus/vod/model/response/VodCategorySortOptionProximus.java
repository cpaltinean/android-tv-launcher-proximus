package tv.threess.threeready.data.proximus.vod.model.response;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.home.SelectorOption;

/**
 * Proximus sort option fora VOD category.
 *
 * @author Barabas Attila
 * @since 2020.08.24
 */
@XmlRootElement(name = "SortingOption")
public class VodCategorySortOptionProximus implements SelectorOption {
    @XmlAttribute(name = "value")
    private String mValue;

    @XmlAttribute(name = "default")
    private boolean mDefault;

    @XmlAttribute(name = "unfeaturedSort")
    private String mUnFeaturedSortValue;

    public String getValue() {
        return mValue;
    }

    @Override
    public String getName() {
        // Map secondary value to translation key.
        if (SORT_VALUE_NAME_MAP.containsKey(mUnFeaturedSortValue)) {
            return SORT_VALUE_NAME_MAP.get(mUnFeaturedSortValue);
        }

        // Map value to translation key.
        if (SORT_VALUE_NAME_MAP.containsKey(mValue)) {
            return SORT_VALUE_NAME_MAP.get(mValue);
        }

        // Cannot map it. Use the value directly.
        return mValue;
    }

    @Override
    public String getReportName() {
        return getName();
    }

    @Override
    public String getReportReference() {
        return getReportName();
    }

    @Override
    public boolean isDefault() {
        return mDefault;
    }

    // Map sort option values to translation keys.
    private static final Map<String, String> SORT_VALUE_NAME_MAP = new HashMap<String, String>() {
        {
            put("sortName", "SORT_BY_NAME_ASCENDING");
            put("DateTime", "SORT_BY_DATE_ASCENDING");
            put("DateTimeReverse", "SORT_BY_DATE_DESCENDING");
            put("pureSortName", "SORT_BY_NAME_ASCENDING");
            put("pureDateTimeRev", "SORT_BY_DATE_DESCENDING");
            put("pureEndDateTime", "SORT_BY_END_TIME");
            put("pureReleaseDateRev", "SORT_BY_RELEASE_YEAR_DESCENDING");
            put("mostRented", "SORT_BY_MOST_RENTED");
        }
    };

}