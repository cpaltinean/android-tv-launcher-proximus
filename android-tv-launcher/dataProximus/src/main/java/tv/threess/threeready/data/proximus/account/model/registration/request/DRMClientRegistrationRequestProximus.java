package tv.threess.threeready.data.proximus.account.model.registration.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * BTA backend request doc to register DRM clients.
 *
 * @author Barabas Attila
 * @since 2020.04.17
 */
@XmlRootElement(name = "BTA")
public class DRMClientRegistrationRequestProximus implements Serializable {

    @XmlElement(name = "RegisterDRMClientDoc")
    RegisterDRMClientDoc mDoc;

    public DRMClientRegistrationRequestProximus(DRMClientProximus drmClient) {
        mDoc = new RegisterDRMClientDoc();
        mDoc.mDrmClient = drmClient;
    }


    @XmlRootElement(name = "RegisterDRMClientDoc")
    private static class RegisterDRMClientDoc {
        @XmlElement(name = "DRMClientCapability")
        DRMClientProximus mDrmClient;
    }
}
