package tv.threess.threeready.data.proximus.tv.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.data.proximus.tv.model.channel.DisplayChannel;

/**
 * Type adapter for DisplayChannel items.
 *
 * Created by Noemi Dali on 03.06.2020.
 */
public class DisplayChannelMapAdapter extends XmlAdapter<List<DisplayChannel>, Map<String, Integer>> {

    @Override
    public Map<String, Integer> unmarshal(List<DisplayChannel> displayChannelList) throws Exception {
        Map<String, Integer> displayChannelMap = new HashMap<>();
        for (DisplayChannel channel: displayChannelList) {
            displayChannelMap.put(channel.getChannelId(), channel.getChannelNumber());
        }
        return displayChannelMap;
    }

    @Override
    public List<DisplayChannel> marshal(Map<String, Integer> v) throws Exception {
        throw new IllegalStateException("Not implemented.");
    }
}
