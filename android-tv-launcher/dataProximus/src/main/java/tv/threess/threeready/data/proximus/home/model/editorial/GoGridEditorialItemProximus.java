package tv.threess.threeready.data.proximus.home.model.editorial;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Swimlane tile which opens the EPG grid.
 *
 * @author Barabas Attila
 * @since 6/18/21
 */
public class GoGridEditorialItemProximus extends SwimlaneEditorialItemProximus {

    public GoGridEditorialItemProximus(SwimLaneProximus swimLane,
                                       SwimLaneProximus.SwimlaneTile swimlaneTile, Integer position) {
        super(swimLane, swimlaneTile, position);
    }

    @Nullable
    @Override
    public EditorialItemAction getAction() {
        return mItemAction;
    }

    private final EditorialItemAction mItemAction = new EditorialItemAction.EpgEditorialItemAction() {};
}
