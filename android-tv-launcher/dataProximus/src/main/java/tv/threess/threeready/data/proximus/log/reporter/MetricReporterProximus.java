/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.log.reporter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import androidx.annotation.Nullable;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import retrofit2.Response;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.MemoryCache;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.generic.model.InputServiceInfo;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.DataCollectionEvent;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.PlaybackEventDetails;
import tv.threess.threeready.api.log.model.PlaybackEventValues;
import tv.threess.threeready.api.log.model.PlaybackLogEvent;
import tv.threess.threeready.api.log.model.ReporterInfo;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.middleware.BluetoothUtils;
import tv.threess.threeready.api.middleware.model.HdmiInfo;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.data.proximus.log.model.LogDetailsProximus;
import tv.threess.threeready.data.proximus.log.model.SendMetricsRequest;
import tv.threess.threeready.data.proximus.log.utils.EdidInfo;
import tv.threess.threeready.data.proximus.log.utils.ExtendedEdidInfo;
import tv.threess.threeready.data.proximus.log.utils.LoadInfo;
import tv.threess.threeready.data.proximus.log.utils.MemInfo;
import tv.threess.threeready.data.proximus.log.utils.NetworkInfo;
import tv.threess.threeready.data.proximus.log.utils.ProcStatus;
import tv.threess.threeready.data.proximus.log.utils.ProcStatusTch;
import tv.threess.threeready.data.proximus.log.utils.StorageInfo;
import tv.threess.threeready.data.proximus.log.utils.VQEClientInfo;
import tv.threess.threeready.data.proximus.log.utils.VQEJitterInfo;
import tv.threess.threeready.data.proximus.mw.MwProxyProximus;

/**
 * Specific playback metric handler for Proximus
 *
 * @author Karetka Mezei Zoltan
 * @since 2019.02.12
 */
public class MetricReporterProximus extends BaseReporterProximus<PlaybackLogEvent, PlaybackEventDetails> {
    private static final String TAG = Log.tag(MetricReporterProximus.class);

    private static final String WORKER_THREAD_NAME = "Metrics";
    private static final TimeUnit SEND_SCHEDULE_UNIT = TimeUnit.MINUTES;
    private static final int SEND_SCHEDULE_RATE = 5;

    private static final String KEY_CLIENT_STATUS = "client_status";

    private static final String KEY_EPOCH_TIME = "epoch_time";

    private static final String KEY_SYS_METRIC_UPTIME = "sys_uptime";
    private static final String KEY_SYS_METRIC_LOAD_AVG_5 = "sys_load_avg_5";
    private static final String KEY_SYS_METRIC_LOAD_AVG_15 = "sys_load_avg_15";

    private static final String KEY_SYS_METRIC_MEM_TOTAL = "sys_mem_total";
    private static final String KEY_SYS_METRIC_MEM_USED = "sys_mem_used";
    private static final String KEY_SYS_METRIC_MEM_FREE = "sys_mem_free";

    private static final String KEY_SYS_METRIC_MEM_SHARED = "sys_mem_shared";
    private static final String KEY_SYS_METRIC_MEM_BUFFERS = "sys_mem_buffers";
    private static final String KEY_SYS_METRIC_MEM_CACHED = "sys_mem_cached";
    private static final String KEY_SYS_METRIC_MEM_INACTIVE = "sys_mem_inactive";
    private static final String KEY_SYS_METRIC_MEM_ACTIVE = "sys_mem_active";

    private static final String KEY_SYS_METRIC_SWAP_TOTAL = "swap_mem_total";
    private static final String KEY_SYS_METRIC_SWAP_USED = "swap_mem_used";
    private static final String KEY_SYS_METRIC_SWAP_FREE = "swap_mem_free";

    private static final String KEY_PROC_TV_CLIENT_PID = "process_stbtvclient_pid";
    private static final String KEY_PROC_TV_CLIENT_VM_PEAK = "process_stbtvclient_vm_peak";
    private static final String KEY_PROC_TV_CLIENT_VM_SIZE = "process_stbtvclient_vm_size";
    private static final String KEY_PROC_TV_CLIENT_VM_RSS = "process_stbtvclient_vm_rss";
    private static final String KEY_PROC_TV_CLIENT_VM_THREADS = "process_stbtvclient_vm_threads";

    private static final String KEY_PROC_TV_PLAYER_PID = "process_stbtvclientplayer_pid";
    private static final String KEY_PROC_TV_PLAYER_VM_PEAK = "process_stbtvclientplayer_vm_peak";
    private static final String KEY_PROC_TV_PLAYER_VM_SIZE = "process_stbtvclientplayer_vm_size";
    private static final String KEY_PROC_TV_PLAYER_VM_RSS = "process_stbtvclientplayer_vm_rss";
    private static final String KEY_PROC_TV_PLAYER_VM_THREADS = "process_stbtvclientplayer_vm_threads";

    private static final String KEY_PROC_YOUTUBE_PID = "process_youtube_pid";
    private static final String KEY_PROC_YOUTUBE_VM_PEAK = "process_youtube_vm_peak";
    private static final String KEY_PROC_YOUTUBE_VM_SIZE = "process_youtube_vm_size";
    private static final String KEY_PROC_YOUTUBE_VM_RSS = "process_youtube_vm_rss";
    private static final String KEY_PROC_YOUTUBE_VM_THREADS = "process_youtube_vm_threads";

    private static final String KEY_PROC_NETFLIX_PID = "process_netflix_pid";
    private static final String KEY_PROC_NETFLIX_VM_PEAK = "process_netflix_vm_peak";
    private static final String KEY_PROC_NETFLIX_VM_SIZE = "process_netflix_vm_size";
    private static final String KEY_PROC_NETFLIX_VM_RSS = "process_netflix_vm_rss";
    private static final String KEY_PROC_NETFLIX_VM_THREADS = "process_netflix_vm_threads";

    private static final String KEY_PROC_TCH_IPTV_PID = "process_tch_iptv_pid";
    private static final String KEY_PROC_TCH_IPTV_VM_PEAK = "process_tch_iptv_vm_peak";
    private static final String KEY_PROC_TCH_IPTV_VM_SIZE = "process_tch_iptv_vm_size";
    private static final String KEY_PROC_TCH_IPTV_VM_RSS = "process_tch_iptv_vm_rss";
    private static final String KEY_PROC_TCH_IPTV_VM_THREADS = "process_tch_iptv_vm_threads";

    private static final String KEY_HDMI_SINK_EDID_MANUFACTURE_ID = "hdmi_sink_edid_manufacturer_id";
    private static final String KEY_HDMI_SINK_EDID_PRODUCT_CODE = "hdmi_sink_edid_product_code";
    private static final String KEY_HDMI_SINK_EDID_SERIAL_NR = "hdmi_sink_edid_serial_nr";
    private static final String KEY_HDMI_SINK_EDID_YEAR_OF_MANUFACTURER = "hdmi_sink_edid_year_of_manufacturer";
    private static final String KEY_HDMI_SINK_NATIVE_RESOLUTION = "hdmi_sink_native_resolution";
    private static final String KEY_HDMI_SINK_APPLIED_RESOLUTION = "hdmi_sink_applied_resolution";
    private static final String KEY_HDMI_SINK_EDID_VERSION = "hdmi_sink_edid_version";
    private static final String KEY_HDMI_SOURCE_HPD_STATE = "hdmi_source_hpd_state";

    private static final String KEY_NET_RX_BYTES = "net_rx_bytes";
    private static final String KEY_NET_RX_ERRS = "net_rx_errs";
    private static final String KEY_NET_RX_DROP = "net_rx_drop";
    private static final String KEY_NET_TX_BYTES = "net_tx_bytes";
    private static final String KEY_NET_TX_ERRS = "net_tx_errs";
    private static final String KEY_NET_TX_DROP = "net_tx_drop";

    private static final String KEY_STREAM_PURPOSE = "stream_1_purpose";
    private static final String KEY_STREAM_PROGRAM_TITLE = "stream_1_program_title";
    private static final String KEY_STREAM_PROGRAM_REFERENCE_NUMBER = "stream_1_program_reference_number";
    private static final String KEY_STREAM_SCHEDULED_TRAIL_IDENTIFIER = "stream_1_scheduled_trail_identifier";
    private static final String KEY_STREAM_CALL_LETTER = "stream_1_call_letter";
    private static final String KEY_STREAM_CHANNEL_IDENTIFIER = "stream_1_channel_identifier";
    private static final String KEY_STREAM_CHANNEL_EXTERNAL_IDENTIFIER = "stream_1_channel_external_identifier";
    private static final String KEY_STREAM_VOD_TITLE = "stream_1_vod_title";
    private static final String KEY_STREAM_VOD_ITEM_IDENTIFIER = "stream_1_vod_item_identifier";
    private static final String KEY_STREAM_VOD_EXTERNAL_IDENTIFIER = "stream_1_vod_external_identifier";

    // Player Specific Parameters
    private static final String KEY_PLAYER_NAME = "player_name";
    private static final String KEY_PLAYER_VERSION = "player_version";

    // Stream properties
    private static final String KEY_STREAM_COUNT = "stream_count";
    private static final String KEY_STREAM_CONTENT_URL = "stream_1_content_url";
    private static final String KEY_STREAM_CONTROL_PROTOCOL = "stream_1_control_protocol";
    private static final String KEY_STREAM_STREAM_MODE = "stream_1_stream_mode";
    private static final String KEY_STREAM_ADAPTIVITY_INDICATOR = "stream_1_adaptivity_indicator";
    private static final String KEY_STREAM_CONSECUTIVE_PLAYOUT_INDICATOR = "stream_1_consecutive_playout_indicator";
    private static final String KEY_STREAM_PLAY_TIME = "stream_1_play_time";
    private static final String KEY_STREAM_STARTUP_TIME = "stream_1_startup_time";
    private static final String KEY_STREAM_BIT_RATE = "stream_1_bit_rate";

    // Channel Change properties
    private static final String KEY_STREAM_CHANNEL_CHANGE_CUMUL = "stream_1_channel_change_cumul";
    private static final String KEY_STREAM_CHANNEL_CHANGE_EVENT = "stream_1_channel_change_event";

    // UABR Specific parameters
    private static final String KEY_STREAM_EFFECTIVE_PLAY_TIME = "stream_1_effective_play_time";
    private static final String KEY_STREAM_AVERAGE_BIT_RATE = "stream_1_average_bit_rate";
    private static final String KEY_STREAM_AVERAGE_DOWNLOAD_RATE = "stream_1_average_download_rate";
    private static final String KEY_STREAM_PROFILE_SWITCHING_COUNT = "stream_1_profile_switching_count";
    private static final String KEY_STREAM_BUFFER_COUNT = "stream_1_buffer_count";
    private static final String KEY_STREAM_BUFFER_RATIO = "stream_1_buffer_ratio";

    private static final String KEY_STORAGE_INT_RW_STATUS = "storage_int_rw_status";
    private static final String KEY_STORAGE_INT_TOTAL_SIZE = "storage_int_total_size";
    private static final String KEY_STORAGE_INT_FREE_SIZE = "storage_int_free_size";
    private static final String KEY_STORAGE_INT_WRITE_CYCLES = "storage_int_write_cycles";

    private static final String KEY_STORAGE_EXT_RW_STATUS = "storage_ext_rw_status";
    private static final String KEY_STORAGE_EXT_TOTAL_SIZE = "storage_ext_total_size";
    private static final String KEY_STORAGE_EXT_FREE_SIZE = "storage_ext_free_size";
    private static final String KEY_STORAGE_EXT_WRITE_CYCLES = "storage_ext_write_cycles";

    private static final String KEY_STORAGE_USB_RW_STATUS_ = "storage_usb_rw_status_";
    private static final String KEY_STORAGE_USB_TOTAL_SIZE_ = "storage_usb_total_size_";
    private static final String KEY_STORAGE_USB_FREE_SIZE_ = "storage_usb_free_size_";
    private static final String KEY_STORAGE_USB_BLOCK_DEVICE_ = "storage_usb_block_device_";
    private static final String KEY_STORAGE_USB_MOUNT_POINT_ = "storage_usb_mount_point_";

    private static final String KEY_BT_DEVICE_COUNT = "bt_device_count";
    private static final String KEY_BT_DEVICE_CONN_ = "bt_device_connected_";
    private static final String KEY_BT_DEVICE_NAME_ = "bt_device_name_";
    private static final String KEY_BT_DEVICE_MAC_ADDRESS_ = "bt_device_mac_address_";
    private static final String KEY_BT_DEVICE_REMOTE_ = "bt_device_remote_";
    private static final String KEY_BT_REMOTE_BATTERY_LEVEL_ = "bt_device_remote_battery_level_";
    private static final String KEY_BT_REMOTE_FIRMWARE_VERSION_ = "bt_device_remote_firmware_version_";
    private static final String KEY_BT_REMOTE_SOFTWARE_VERSION_ = "bt_device_remote_software_version_";

    private static final String KEY_PRIMARY_ECAP = "primary_ecap";
    private static final String KEY_INIT_SVC_CHECK_AGP26 = "init.svc.checkagp26";
    private static final String KEY_VENDOR_AGP26_PFAULT_PINSTATE = "vendor.agp26_pfault_pinstate";

    private static final String KEY_VQE_CC_REQUESTS = "vqe_cc_requests";
    private static final String KEY_VQE_DELTA_CC_REQUESTS = "vqe_delta_cc_requests";
    private static final String KEY_VQE_DELTA_POST_REPAIR_LOSSES = "vqe_delta_post_repair_losses";
    private static final String KEY_VQE_DELTA_POST_REPAIR_LOSSES_RCC = "vqe_delta_post_repair_losses_rcc";
    private static final String KEY_VQE_DELTA_POST_REPAIR_OUTPUTS = "vqe_delta_post_repair_outputs";
    private static final String KEY_VQE_DELTA_PRE_REPAIR_LOSSES = "vqe_delta_pre_repair_losses";
    private static final String KEY_VQE_DELTA_PRIMARY_RTCP_INPUTS = "vqe_delta_primary_rtcp_inputs";
    private static final String KEY_VQE_DELTA_PRIMARY_RTCP_OUTPUTS = "vqe_delta_primary_rtcp_outputs";
    private static final String KEY_VQE_DELTA_PRIMARY_RTP_DROPS = "vqe_delta_primary_rtp_drops";
    private static final String KEY_VQE_DELTA_PRIMARY_RTP_DROPS_LATE = "vqe_delta_primary_rtp_drops_late";
    private static final String KEY_VQE_DELTA_PRIMARY_RTP_INPUTS = "vqe_delta_primary_rtp_inputs";
    private static final String KEY_VQE_DELTA_RCC_ABORTS_TOTAL = "vqe_delta_rcc_aborts_total";
    private static final String KEY_VQE_DELTA_RCC_REQUESTS = "vqe_delta_rcc_requests";
    private static final String KEY_VQE_DELTA_RCC_WITH_LOSS = "vqe_delta_rcc_with_loss";
    private static final String KEY_VQE_DELTA_REPAIRS_POLICED = "vqe_delta_repairs_policed";
    private static final String KEY_VQE_DELTA_REPAIRS_REQUESTED = "vqe_delta_repairs_requested";
    private static final String KEY_VQE_DELTA_REPAIR_RTCP_INPUTS = "vqe_delta_repair_rtcp_inputs";
    private static final String KEY_VQE_DELTA_REPAIR_RTCP_STUN_INPUTS = "vqe_delta_repair_rtcp_stun_inputs";
    private static final String KEY_VQE_DELTA_REPAIR_RTCP_STUN_OUTPUTS = "vqe_delta_repair_rtcp_stun_outputs";
    private static final String KEY_VQE_DELTA_REPAIR_RTP_DROPS = "vqe_delta_repair_rtp_drops";
    private static final String KEY_VQE_DELTA_REPAIR_RTP_DROPS_LATE = "vqe_delta_repair_rtp_drops_late";
    private static final String KEY_VQE_DELTA_REPAIR_RTP_INPUTS = "vqe_delta_repair_rtp_inputs";
    private static final String KEY_VQE_DELTA_REPAIR_RTP_STUN_INPUTS = "vqe_delta_repair_rtp_stun_inputs";
    private static final String KEY_VQE_DELTA_REPAIR_RTP_STUN_OUTPUTS = "vqe_delta_repair_rtp_stun_outputs";
    private static final String KEY_VQE_DELTA_STREAMUNDERRUNS_CUMUL = "vqe_delta_streamunderruns_cumul";
    private static final String KEY_VQE_DELTA_TUNER_QUEUE_DROPS = "vqe_delta_tuner_queue_drops";
    private static final String KEY_VQE_POST_REPAIR_LOSSES = "vqe_post_repair_losses";
    private static final String KEY_VQE_POST_REPAIR_LOSSES_RCC = "vqe_post_repair_losses_rcc";
    private static final String KEY_VQE_POST_REPAIR_OUTPUTS = "vqe_post_repair_outputs";
    private static final String KEY_VQE_PRE_REPAIR_LOSSES = "vqe_pre_repair_losses";
    private static final String KEY_VQE_PRIMARY_RTCP_INPUTS = "vqe_primary_rtcp_inputs";
    private static final String KEY_VQE_PRIMARY_RTCP_OUTPUTS = "vqe_primary_rtcp_outputs";
    private static final String KEY_VQE_PRIMARY_RTP_DROPS = "vqe_primary_rtp_drops";
    private static final String KEY_VQE_PRIMARY_RTP_DROPS_LATE = "vqe_primary_rtp_drops_late";
    private static final String KEY_VQE_PRIMARY_RTP_INPUTS = "vqe_primary_rtp_inputs";
    private static final String KEY_VQE_RCC_ABORTS_TOTAL = "vqe_rcc_aborts_total";
    private static final String KEY_VQE_RCC_REQUESTS = "vqe_rcc_requests";
    private static final String KEY_VQE_RCC_WITH_LOSS = "vqe_rcc_with_loss";
    private static final String KEY_VQE_REPAIRS_POLICED = "vqe_repairs_policed";
    private static final String KEY_VQE_REPAIRS_REQUESTED = "vqe_repairs_requested";
    private static final String KEY_VQE_REPAIR_RTCP_INPUTS = "vqe_repair_rtcp_inputs";
    private static final String KEY_VQE_REPAIR_RTCP_STUN_INPUTS = "vqe_repair_rtcp_stun_inputs";
    private static final String KEY_VQE_REPAIR_RTCP_STUN_OUTPUTS = "vqe_repair_rtcp_stun_outputs";
    private static final String KEY_VQE_REPAIR_RTP_DROPS = "vqe_repair_rtp_drops";
    private static final String KEY_VQE_REPAIR_RTP_DROPS_LATE = "vqe_repair_rtp_drops_late";
    private static final String KEY_VQE_REPAIR_RTP_INPUTS = "vqe_repair_rtp_inputs";
    private static final String KEY_VQE_REPAIR_RTP_STUN_INPUTS = "vqe_repair_rtp_stun_inputs";
    private static final String KEY_VQE_REPAIR_RTP_STUN_OUTPUTS = "vqe_repair_rtp_stun_outputs";
    private static final String KEY_VQE_STREAMUNDERRUNS_CUMUL = "vqe_streamunderruns_cumul";
    private static final String KEY_VQE_TUNER_QUEUE_DROPS = "vqe_tuner_queue_drops";

    private static final String KEY_VQE_PRIMARY_RTP_JITTER_MIN = "stream_1_vqe_primary_rtp_jitter_min";
    private static final String KEY_VQE_PRIMARY_RTP_JITTER_MAX = "stream_1_vqe_primary_rtp_jitter_max";
    private static final String KEY_VQE_PRIMARY_RTP_JITTER_AVG = "stream_1_vqe_primary_rtp_jitter_avg";
    private static final String KEY_VQE_PRIMARY_RTP_JITTER_STD = "stream_1_vqe_primary_rtp_jitter_std";

    private static final String VALUE_METRIC = "METRIC";

    private static final String VALUE_ACT = "ACT";
    private static final String VALUE_CCDN = "CCDN";
    private static final String VALUE_CCEPG = "CCEPG";
    private static final String VALUE_CCNUM = "CCNUM";
    private static final String VALUE_CCUP = "CCUP";
    private static final String VALUE_DISC = "DISC";
    private static final String VALUE_HIGH = "HIGH";
    private static final String VALUE_LOW = "LOW";
    private static final String VALUE_RO = "RO";
    private static final String VALUE_RW = "RW";
    private static final String VALUE_STBY = "STBY";
    private static final String VALUE_UNA = "UNA";
    private static final String VALUE_PLAYER_VQE = "VQE-Technicolor";
    private static final String VALUE_PLAYER_ABR = "ABR-castLabs";
    private static final String VALUE_CONSECUTIVE = "CONSECUTIVE";

    private boolean mIsPlaybackStarted = false;
    private long mPlayerInitialised = 0;
    private long mPlaybackStarted = 0;
    private long mChannelChangeCount = 0;
    private Event<PlaybackLogEvent, PlaybackEventDetails> mLastStartEvent = null;

    private String mPreviousState = null;
    private long mPreviousStateChange = 0;

    private long mBufferTime = 0;
    private long mBufferingCount = 0;

    private long mPausedStart = 0;
    private long mPausedTotal = 0;

    private long mCurrentBitrate = 0;
    private long mBitrateTotal = 0;
    private long mBitrateChanges = 0;

    private long mSegmentSizeTotal = 0;
    private long mSegmentTimeTotal = 0;

    private final Handler mHandler;

    private final Runnable mSendMetricRunnable;

    private final InputServiceInfo mInputServiceInfo = Components.get(InputServiceInfo.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    private VQEClientInfo mLastVQEClientInfo = null;

    public MetricReporterProximus(Context context, ReporterInfo reporterInfo) {
        super(context, reporterInfo);

        mHandler = createWorkerThread();
        mSendMetricRunnable = new Runnable() {
            @Override
            public void run() {
                mHandler.removeCallbacks(this);
                mHandler.postDelayed(this, getNextMetricDelay());
                sendMetrics();
            }
        };
        mHandler.postDelayed(mSendMetricRunnable, getNextMetricDelay());
    }

    private static Handler createWorkerThread() {
        HandlerThread handlerThread = new HandlerThread(WORKER_THREAD_NAME);
        handlerThread.start();
        return new Handler(handlerThread.getLooper());
    }

    @Override
    public boolean canSend(Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        long now = System.currentTimeMillis();

        switch (event.getName(PlaybackLogEvent.Unknown)) {
            case PlaybackDisconnected:
            case PlaybackUnavailable:
            case PlaybackStopped:
            case PlaybackFailed:
            case PlaybackEnded:
            case StartLoadingSpinner:
                mIsPlaybackStarted = false;
                break;

            case PlaybackBuffered:
            case PlaybackStarted:
            case StopLoadingSpinner:
                mIsPlaybackStarted = true;
                break;
        }

        switch (event.getName(PlaybackLogEvent.Unknown)) {
            case StartInitiated:
                if (isLive(event)) {
                    mChannelChangeCount += 1;
                }
                break;

            case StartResolved:
                resetValues();
                // this is right before the player is initialized, but contains the release of the old player
                mPlayerInitialised = event.getDetail(PlaybackEventDetails.PLAYBACK_INITIATED_TIMESTAMP, now);
                // use the initial bitrate from the metadata, in case the player will not update it
                mCurrentBitrate = event.getDetail(PlaybackEventDetails.BITRATE, 0L);
                break;

            case PlaybackStarted:
                if (event.getDetail(PlaybackEventDetails.ACTION, StartAction.Undefined) == StartAction.ProgramChanged){
                    return false;
                }

                mLastStartEvent = event;
                if (mPlaybackStarted < 0) {
                    mPlaybackStarted = event.getDetail(PlaybackEventDetails.STATE_CHANGED_TIMESTAMP, now);
                    resetCounters();
                }
                break;

            case PlaybackStopped:
                if (event.getDetail(PlaybackEventDetails.ACTION, StartAction.Undefined) == StartAction.ProgramChanged){
                    return false;
                }
                resetValues();
                break;

            case PlaybackFailed:
            case PlaybackUnavailable:
                resetValues();
                break;

            case StateChanged:
                String state = event.getDetail(PlaybackEventDetails.PLAYER_STATE);

                if (state == null) {
                    // playback manager state changed, not exo-player's state!
                    break;
                }

                // Total no. of times SingleControllerPlaylist.State changed to "Buffering"
                if (PlaybackEventValues.STATE_BUFFERING.equals(state)) {
                    mBufferingCount += 1;
                }

                // start_time is the clock time at which player state(SingleControllerPlaylist.State) changed to "Playing"
                if (PlaybackEventValues.STATE_PLAYING.equals(state)) {
                    if (mPlaybackStarted < 0) {
                        mPlaybackStarted = event.getDetail(PlaybackEventDetails.STATE_CHANGED_TIMESTAMP, now);
                        resetCounters();
                    }
                }

                // BufferTime is the time between player state changed from Buffering to Playing.
                if (PlaybackEventValues.STATE_BUFFERING.equals(mPreviousState) && PlaybackEventValues.STATE_PLAYING.equals(state)) {
                    if (mPlaybackStarted > 0) {
                        mBufferTime += event.getDetail(PlaybackEventDetails.STATE_CHANGED_TIMESTAMP, now) - Math.max(mPreviousStateChange, mPlaybackStarted);
                    }
                }

                mPreviousStateChange = event.getDetail(PlaybackEventDetails.STATE_CHANGED_TIMESTAMP, now);
                mPreviousState = state;
                break;

            case BitrateChanged:
                long bitrate = event.getDetail(PlaybackEventDetails.BITRATE, mCurrentBitrate);
                if (mCurrentBitrate != bitrate) {
                    mCurrentBitrate = bitrate;
                    mBitrateTotal += bitrate;
                    mBitrateChanges += 1;
                }
                break;

            case SegmentDownloaded:
                mSegmentSizeTotal += event.getDetail(PlaybackEventDetails.SEGMENT_SIZE, 0);
                mSegmentTimeTotal += event.getDetail(PlaybackEventDetails.SEGMENT_TIME, 0);
                break;

            case PlaybackPaused:
                mPausedStart = now;
                break;

            case PlaybackResumed:
                if (mPausedStart > 0) {
                    mPausedTotal += now - mPausedStart;
                }
                mPausedStart = 0;
                break;
        }

        return event.getName(DataCollectionEvent.Unknown) == DataCollectionEvent.sendMetrics;
    }

    @Override
    public long getBatchPeriod() {
        return 0;
    }

    @Override
    public void sendEvent(Event<PlaybackLogEvent, PlaybackEventDetails> event) throws Exception {
        LogDetailsProximus payload = formatPayload(event);
        if (payload == null) {
            android.util.Log.d(TAG, "Could not send event, no payload: " + event);
            return;
        }

        sendMetrics(getEndpoint(), getHeaders(), getTimeoutPeriod(), new SendMetricsRequest(payload));
    }

    @Override
    public void sendEvents(List<Event<PlaybackLogEvent, PlaybackEventDetails>> events) throws Exception {
        List<LogDetailsProximus> payload = formatPayloadList(events);
        if (payload == null || payload.isEmpty()) {
            android.util.Log.d(TAG, "Could not send events. Payload list is empty.");
            return;
        }

        sendMetrics(getEndpoint(), getHeaders(), getTimeoutPeriod(), new SendMetricsRequest(payload));
    }

    /**
     * Send metrics to data collection API.
     *
     * @param url                Url of the collector server where the data will be sent.
     * @param headers            Custom headers for metrics from backend which need to be included.
     * @param sendMetricsRequest Retrofit request class for sending metrics.
     */
    private void sendMetrics(String url, String headers, long timeout,
                             SendMetricsRequest sendMetricsRequest) throws Exception {
        if (TextUtils.isEmpty(url)) {
            throw new IllegalArgumentException("Could not send metrics. Url is null or empty.");
        }

        Response<String> response = mDataCollectionRetrofit.sendMetrics(url, getHeadersMap(headers),
                timeout, sendMetricsRequest).execute();

        // HTTP Error
        if (!response.isSuccessful() || !Objects.equals(response.body(), OK)) {
            throw new BackendException(response.code(), response.message());
        }
    }


    /**
     * Reset the internal state after a fail, playback stop, and before a new start
     */
    private void resetValues() {
        mPlayerInitialised = 0;
        mPlaybackStarted = -1;
        mPreviousState = null;
        mLastStartEvent = null;
        mPreviousStateChange = 0;
        mCurrentBitrate = 0;
        resetCounters();
    }

    /**
     * Clear the internal state between consecutive metric sending
     */
    protected void resetCounters() {
        mBufferTime = 0;
        mBufferingCount = 0;
        mBitrateTotal = 0;
        mBitrateChanges = 0;
        mSegmentSizeTotal = 0;
        mSegmentTimeTotal = 0;
        mPausedTotal = 0;
        mPausedStart = 0;
    }

    private long getNextMetricDelay() {
        long now = System.currentTimeMillis();
        long nextEvent = TimeBuilder.utc(now)
                .ceil(SEND_SCHEDULE_UNIT, SEND_SCHEDULE_RATE)
                .get();
        return nextEvent - now;
    }

    @Override
    @Nullable
    protected LogDetailsProximus formatPayload(Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        LogDetailsProximus payload = new LogDetailsProximus();

        payload.add(KEY_CATEGORY, VALUE_METRIC);

        super.addAgentSpecificParts(payload);
        payload.add(KEY_EPOCH_TIME, System.currentTimeMillis());
        addOperatingSystemParameters(payload);
        addProcessMetricsParameters(payload);
        addStreamingProperties(payload);
        addHDMISinkMetricsParameters(payload);
        addNetworkMetricsParameters(payload);
        addStorageMetricsParameters(payload);
        addBluetoothDeviceInfo(payload);
        addPSUMetrics(payload);
        resetCounters();
        return payload;
    }

    private void sendMetrics() {
        // Create a new event for sending metrics
        Log.event(new Event<>(DataCollectionEvent.sendMetrics));
    }


    /**
     * Send tv client specific parts described by the following documentation.
     * https://confluenceprd-proximuscorp.msappproxy.net/pages/viewpage.action?spaceKey=PRXMS&title=Metrics+%3A+TV+Client+APP+metrics
     */
    private void addClientSpecificParts(LogDetailsProximus payload, long startupTime) {
        payload.add(KEY_CLIENT_STATUS, mMwProxy.isScreenOn() ? VALUE_ACT : VALUE_STBY);
        payload.add(KEY_STREAM_STARTUP_TIME, startupTime);
        addChannelChangeProperties(payload);
    }

    private void addOperatingSystemParameters(LogDetailsProximus payload) {
        MemInfo memInfo = MemInfo.read();
        LoadInfo loadInfo = new LoadInfo(mMwProxy.readFileContent(MwProxyProximus.LOADAVG_URI));
        payload.add(KEY_SYS_METRIC_UPTIME, SystemClock.elapsedRealtime());
        payload.add(KEY_SYS_METRIC_LOAD_AVG_5, loadInfo.get(LoadInfo.Field.Average5Min, 0.));
        payload.add(KEY_SYS_METRIC_LOAD_AVG_15, loadInfo.get(LoadInfo.Field.Average15Min, 0.));
        payload.add(KEY_SYS_METRIC_MEM_TOTAL, memInfo.get(MemInfo.Field.MemTotal, 0L));
        payload.add(KEY_SYS_METRIC_MEM_USED, memInfo.get(MemInfo.Field.MemTotal, 0L) - memInfo.get(MemInfo.Field.MemFree, 0L));
        payload.add(KEY_SYS_METRIC_MEM_FREE, memInfo.get(MemInfo.Field.MemFree, 0L));

        payload.add(KEY_SYS_METRIC_MEM_SHARED, memInfo.get(MemInfo.Field.Shmem, 0L));
        payload.add(KEY_SYS_METRIC_MEM_BUFFERS, memInfo.get(MemInfo.Field.Buffers, 0L));
        payload.add(KEY_SYS_METRIC_MEM_CACHED, memInfo.get(MemInfo.Field.Cached, 0L));
        payload.add(KEY_SYS_METRIC_MEM_INACTIVE, memInfo.get(MemInfo.Field.Inactive, 0L));
        payload.add(KEY_SYS_METRIC_MEM_ACTIVE, memInfo.get(MemInfo.Field.Active, 0L));

        payload.add(KEY_SYS_METRIC_SWAP_TOTAL, memInfo.get(MemInfo.Field.SwapTotal, 0L));
        payload.add(KEY_SYS_METRIC_SWAP_USED, memInfo.get(MemInfo.Field.SwapTotal, 0L) - memInfo.get(MemInfo.Field.SwapFree, 0L));
        payload.add(KEY_SYS_METRIC_SWAP_FREE, memInfo.get(MemInfo.Field.SwapFree, 0L));
    }

    private void addProcessMetricsParameters(LogDetailsProximus payload) {
        ProcStatus info = ProcStatus.read(android.os.Process.myPid());
        payload.add(KEY_PROC_TV_CLIENT_PID, info.get(ProcStatus.Field.Pid, -1L));
        payload.add(KEY_PROC_TV_CLIENT_VM_PEAK, info.get(ProcStatus.Field.VmPeak, -1L));
        payload.add(KEY_PROC_TV_CLIENT_VM_SIZE, info.get(ProcStatus.Field.VmSize, -1L));
        payload.add(KEY_PROC_TV_CLIENT_VM_RSS, info.get(ProcStatus.Field.VmRSS, -1L));
        payload.add(KEY_PROC_TV_CLIENT_VM_THREADS, info.get(ProcStatus.Field.Threads, -1));

        String playerProcessName = mMwProxy.getProcessName(mInputServiceInfo.getOttInputComponentName());
        info = mMwProxy.readProcessInfo(playerProcessName, new ProcStatusTch());
        payload.add(KEY_PROC_TV_PLAYER_PID, info.get(ProcStatus.Field.Pid, -1L));
        payload.add(KEY_PROC_TV_PLAYER_VM_PEAK, info.get(ProcStatus.Field.VmPeak, -1L));
        payload.add(KEY_PROC_TV_PLAYER_VM_SIZE, info.get(ProcStatus.Field.VmSize, -1L));
        payload.add(KEY_PROC_TV_PLAYER_VM_RSS, info.get(ProcStatus.Field.VmRSS, -1L));
        payload.add(KEY_PROC_TV_PLAYER_VM_THREADS, info.get(ProcStatus.Field.Threads, -1));

        info = mMwProxy.readProcessInfo(PackageUtils.PACKAGE_NAME_NETFLIX, new ProcStatusTch());
        payload.add(KEY_PROC_NETFLIX_PID, info.get(ProcStatus.Field.Pid, -1L));
        payload.add(KEY_PROC_NETFLIX_VM_PEAK, info.get(ProcStatus.Field.VmPeak, -1L));
        payload.add(KEY_PROC_NETFLIX_VM_SIZE, info.get(ProcStatus.Field.VmSize, -1L));
        payload.add(KEY_PROC_NETFLIX_VM_RSS, info.get(ProcStatus.Field.VmRSS, -1L));
        payload.add(KEY_PROC_NETFLIX_VM_THREADS, info.get(ProcStatus.Field.Threads, -1));

        info = mMwProxy.readProcessInfo(PackageUtils.PACKAGE_NAME_YOUTUBE, new ProcStatusTch());
        payload.add(KEY_PROC_YOUTUBE_PID, info.get(ProcStatus.Field.Pid, -1L));
        payload.add(KEY_PROC_YOUTUBE_VM_PEAK, info.get(ProcStatus.Field.VmPeak, -1L));
        payload.add(KEY_PROC_YOUTUBE_VM_SIZE, info.get(ProcStatus.Field.VmSize, -1L));
        payload.add(KEY_PROC_YOUTUBE_VM_RSS, info.get(ProcStatus.Field.VmRSS, -1L));
        payload.add(KEY_PROC_YOUTUBE_VM_THREADS, info.get(ProcStatus.Field.Threads, -1));

        info = mMwProxy.readProcessInfo(PackageUtils.PACKAGE_NAME_TCH_IPTV, new ProcStatusTch());
        payload.add(KEY_PROC_TCH_IPTV_PID, info.get(ProcStatus.Field.Pid, -1L));
        payload.add(KEY_PROC_TCH_IPTV_VM_PEAK, info.get(ProcStatus.Field.VmPeak, -1L));
        payload.add(KEY_PROC_TCH_IPTV_VM_SIZE, info.get(ProcStatus.Field.VmSize, -1L));
        payload.add(KEY_PROC_TCH_IPTV_VM_RSS, info.get(ProcStatus.Field.VmRSS, -1L));
        payload.add(KEY_PROC_TCH_IPTV_VM_THREADS, info.get(ProcStatus.Field.Threads, -1));
    }

    private void addVQEClientMetrics(LogDetailsProximus payload) {
        try {
            VQEClientInfo info = VQEClientInfo.read(mContext);

            if (mLastVQEClientInfo == null) {
                mLastVQEClientInfo = info;
            }

            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.PrimaryRtpInputs, KEY_VQE_PRIMARY_RTP_INPUTS, KEY_VQE_DELTA_PRIMARY_RTP_INPUTS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.PrimaryRtpDrops, KEY_VQE_PRIMARY_RTP_DROPS, KEY_VQE_DELTA_PRIMARY_RTP_DROPS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.PrimaryRtpDropsLate, KEY_VQE_PRIMARY_RTP_DROPS_LATE, KEY_VQE_DELTA_PRIMARY_RTP_DROPS_LATE);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.PrimaryRtcpInputs, KEY_VQE_PRIMARY_RTCP_INPUTS, KEY_VQE_DELTA_PRIMARY_RTCP_INPUTS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.PrimaryRtcpOutputs, KEY_VQE_PRIMARY_RTCP_OUTPUTS, KEY_VQE_DELTA_PRIMARY_RTCP_OUTPUTS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.RepairRtpInputs, KEY_VQE_REPAIR_RTP_INPUTS, KEY_VQE_DELTA_REPAIR_RTP_INPUTS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.RepairRtpDrops, KEY_VQE_REPAIR_RTP_DROPS, KEY_VQE_DELTA_REPAIR_RTP_DROPS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.RepairRtpDropsLate, KEY_VQE_REPAIR_RTP_DROPS_LATE, KEY_VQE_DELTA_REPAIR_RTP_DROPS_LATE);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.RepairRtcpInputs, KEY_VQE_REPAIR_RTCP_INPUTS, KEY_VQE_DELTA_REPAIR_RTCP_INPUTS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.RepairRtpStunInputs, KEY_VQE_REPAIR_RTP_STUN_INPUTS, KEY_VQE_DELTA_REPAIR_RTP_STUN_INPUTS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.RepairRtpStunOutputs, KEY_VQE_REPAIR_RTP_STUN_OUTPUTS, KEY_VQE_DELTA_REPAIR_RTP_STUN_OUTPUTS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.RepairRtcpStunInputs, KEY_VQE_REPAIR_RTCP_STUN_INPUTS, KEY_VQE_DELTA_REPAIR_RTCP_STUN_INPUTS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.RepairRtcpStunOutputs, KEY_VQE_REPAIR_RTCP_STUN_OUTPUTS, KEY_VQE_DELTA_REPAIR_RTCP_STUN_OUTPUTS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.RepairsRequested, KEY_VQE_REPAIRS_REQUESTED, KEY_VQE_DELTA_REPAIRS_REQUESTED);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.RepairsPoliced, KEY_VQE_REPAIRS_POLICED, KEY_VQE_DELTA_REPAIRS_POLICED);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.PreRepairLosses, KEY_VQE_PRE_REPAIR_LOSSES, KEY_VQE_DELTA_PRE_REPAIR_LOSSES);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.PostRepairOutputs, KEY_VQE_POST_REPAIR_OUTPUTS, KEY_VQE_DELTA_POST_REPAIR_OUTPUTS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.PostRepairLosses, KEY_VQE_POST_REPAIR_LOSSES, KEY_VQE_DELTA_POST_REPAIR_LOSSES);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.PostRepairLossesRcc, KEY_VQE_POST_REPAIR_LOSSES_RCC, KEY_VQE_DELTA_POST_REPAIR_LOSSES_RCC);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.TunerQueueDrops, KEY_VQE_TUNER_QUEUE_DROPS, KEY_VQE_DELTA_TUNER_QUEUE_DROPS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.ChannelChangeRequests, KEY_VQE_CC_REQUESTS, KEY_VQE_DELTA_CC_REQUESTS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.RccRequests, KEY_VQE_RCC_REQUESTS, KEY_VQE_DELTA_RCC_REQUESTS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.RccWithLoss, KEY_VQE_RCC_WITH_LOSS, KEY_VQE_DELTA_RCC_WITH_LOSS);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.RccAbortsTotal, KEY_VQE_RCC_ABORTS_TOTAL, KEY_VQE_DELTA_RCC_ABORTS_TOTAL);
            calculateAndAddVQEValues(payload, info, VQEClientInfo.Field.UnderRuns, KEY_VQE_STREAMUNDERRUNS_CUMUL, KEY_VQE_DELTA_STREAMUNDERRUNS_CUMUL);

            mLastVQEClientInfo = info;
        } catch (Exception e) {
            Log.e(TAG, "Failed to read VQE Client Metrics.", e);
        }
    }


    /**
     *
     * Adds field to payload. Calculate and adds the delta value between the last {@link VQEClientInfo} and the new {@link VQEClientInfo}.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/SH/Delta+values+for+VQE+metrics
     */
    private void calculateAndAddVQEValues(LogDetailsProximus payload, VQEClientInfo info, VQEClientInfo.Field field, String key, String deltaKey) {
        long lastValue = mLastVQEClientInfo.get(field, -1L);
        long newValue = info.get(field, -1L);
        long delta = newValue - lastValue;
        payload.add(key, newValue);
        payload.add(deltaKey, Math.max(0, delta));
    }

    private void addVQEJitterMetrics(LogDetailsProximus payload) {
        try {
            VQEJitterInfo info = VQEJitterInfo.read(mContext);
            payload.add(KEY_VQE_PRIMARY_RTP_JITTER_AVG, info.get(VQEJitterInfo.Field.AvgJitter, -1L));
            payload.add(KEY_VQE_PRIMARY_RTP_JITTER_MIN, info.get(VQEJitterInfo.Field.MinJitter, -1L));
            payload.add(KEY_VQE_PRIMARY_RTP_JITTER_MAX, info.get(VQEJitterInfo.Field.MaxJitter, -1L));
            payload.add(KEY_VQE_PRIMARY_RTP_JITTER_STD, info.get(VQEJitterInfo.Field.StdJitter, -1L));
        } catch (Exception e) {
            Log.e(TAG, "Failed to read VQE Jitter Metrics.", e);
        }
    }

    private void addHDMISinkMetricsParameters(LogDetailsProximus payload) {
        try {
            boolean isScreenOn = mMwProxy.isScreenOn();
            HdmiInfo hdmiInfo = mMwProxy.getHdmiInfo();

            if (isScreenOn && hdmiInfo != null && hdmiInfo.isConnected()) {
                payload.add(KEY_HDMI_SOURCE_HPD_STATE, VALUE_ACT);
            } else if (isScreenOn) {
                payload.add(KEY_HDMI_SOURCE_HPD_STATE, VALUE_DISC);
            } else {
                payload.add(KEY_HDMI_SOURCE_HPD_STATE, VALUE_STBY);
            }

            EdidInfo edidInfo = new EdidInfo(hdmiInfo.getRawEdid());
            payload.add(KEY_HDMI_SINK_EDID_MANUFACTURE_ID, edidInfo.getManufacturerId());
            payload.add(KEY_HDMI_SINK_EDID_PRODUCT_CODE, edidInfo.getProductCode());
            payload.add(KEY_HDMI_SINK_EDID_SERIAL_NR, edidInfo.getSerialNumber());
            payload.add(KEY_HDMI_SINK_EDID_YEAR_OF_MANUFACTURER, edidInfo.getManufacturedYear());
            payload.add(KEY_HDMI_SINK_EDID_VERSION, edidInfo.getVersion());

            // get applied display resolution:
            DisplayMetrics displayMetrics = new DisplayMetrics();
            WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            wm.getDefaultDisplay().getMetrics(displayMetrics);
            int screenWidth = displayMetrics.widthPixels;
            int screenHeight = displayMetrics.heightPixels;
            float refreshRating = wm.getDefaultDisplay().getRefreshRate();

            payload.add(KEY_HDMI_SINK_APPLIED_RESOLUTION, screenWidth + "x" + screenHeight + "@" + refreshRating + "Hz");

            // get native display resolution:
            ExtendedEdidInfo extendedEdidInfo = new ExtendedEdidInfo(hdmiInfo.getExtendedEdid());
            String nRes = extendedEdidInfo.getNativeResolution();
            payload.add(KEY_HDMI_SINK_NATIVE_RESOLUTION, nRes != null ? nRes : NONE);

        } catch (Exception e) {
            android.util.Log.e(TAG, "Failed to read the display resolutions.", e);
        }
    }

    private void addNetworkMetricsParameters(LogDetailsProximus payload) {
        NetworkInfo info = NetworkInfo.read();
        payload.add(KEY_NET_RX_BYTES, info.get(NetworkInfo.Field.RxBytes, 0L));
        payload.add(KEY_NET_RX_ERRS, info.get(NetworkInfo.Field.RxErrs, 0L));
        payload.add(KEY_NET_RX_DROP, info.get(NetworkInfo.Field.RxDrop, 0L));
        payload.add(KEY_NET_TX_BYTES, info.get(NetworkInfo.Field.TxBytes, 0L));
        payload.add(KEY_NET_TX_ERRS, info.get(NetworkInfo.Field.TxErrs, 0L));
        payload.add(KEY_NET_TX_DROP, info.get(NetworkInfo.Field.TxDrop, 0L));
    }

    /**
     * Stream Metrics aligned
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Metrics+%3A+Stream+Metrics+aligned
     */
    private void addStreamingProperties(LogDetailsProximus payload) {
        long playTime = playTime();
        Event<PlaybackLogEvent, PlaybackEventDetails> startedEvent = mLastStartEvent;

        long startupTime = mPlaybackStarted - mPlayerInitialised;

        if (playTime == 0) {
            startedEvent = null;
            startupTime = 0;
        }

        // Content Specific Parameters
        payload.add(KEY_STREAM_PURPOSE, getPlaybackCategory(startedEvent, VALUE_UNA));
        if (startedEvent != null) {
            switch (startedEvent.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None)) {
                case App:
                case None:
                case TrickPlay:
                    break;

                case LiveTvDvbC:
                case LiveTvDvbT:
                case LiveTvIpTv:
                case LiveTvOtt:
                case RadioIpTv:
                case RadioOtt:
                case Replay:
                case Recording:
                    payload.add(KEY_STREAM_PROGRAM_TITLE, startedEvent.getDetail(PlaybackEventDetails.ITEM_TITLE, NONE));
                    payload.add(KEY_STREAM_PROGRAM_REFERENCE_NUMBER, startedEvent.getDetail(PlaybackEventDetails.PROGRAM_ID, NONE));
                    payload.add(KEY_STREAM_SCHEDULED_TRAIL_IDENTIFIER, getScheduledTrailIdentifier(startedEvent.getDetail(PlaybackEventDetails.PROGRAM_ITEM_ID)));
                    payload.add(KEY_STREAM_CALL_LETTER, startedEvent.getDetail(PlaybackEventDetails.CHANNEL_CALL_LETTER, NONE));
                    payload.add(KEY_STREAM_CHANNEL_IDENTIFIER, startedEvent.getDetail(PlaybackEventDetails.CHANNEL_INTERNAL_ID, NONE));
                    payload.add(KEY_STREAM_CHANNEL_EXTERNAL_IDENTIFIER, startedEvent.getDetail(PlaybackEventDetails.CHANNEL_ID, NONE));
                    break;

                case Vod:
                case Trailer:
                    payload.add(KEY_STREAM_VOD_TITLE, startedEvent.getDetail(PlaybackEventDetails.ITEM_TITLE, NONE));
                    payload.add(KEY_STREAM_VOD_ITEM_IDENTIFIER, startedEvent.getDetail(PlaybackEventDetails.VOD_ITEM_IDENTIFIER, NONE));
                    payload.add(KEY_STREAM_VOD_EXTERNAL_IDENTIFIER, startedEvent.getDetail(PlaybackEventDetails.ITEM_EXTERNAL_ID, NONE));
                    break;
            }
        }

        // Player Specific Parameters
        if (startedEvent != null) {
            switch (startedEvent.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None)) {
                case RadioIpTv:
                case LiveTvIpTv:
                case LiveTvDvbC:
                case LiveTvDvbT:
                    payload.add(KEY_PLAYER_NAME, VALUE_PLAYER_VQE);
                    payload.add(KEY_PLAYER_VERSION, mMwProxy.getMWVersion());
                    break;

                case RadioOtt:
                case LiveTvOtt:
                case Replay:
                case Recording:
                case Vod:
                case Trailer:
                case TrickPlay:
                    payload.add(KEY_PLAYER_NAME, VALUE_PLAYER_ABR);
                    payload.add(KEY_PLAYER_VERSION, Settings.playerVersion.get(NONE));
                    break;

                case App:
                case None:
                    payload.add(KEY_PLAYER_NAME, NONE);
                    payload.add(KEY_PLAYER_VERSION, NONE);
                    break;
            }
        }

        // Stream properties
        payload.add(KEY_STREAM_COUNT, startedEvent != null ? 1 : 0);
        payload.add(KEY_STREAM_CONTENT_URL, streamUrl(startedEvent));
        payload.add(KEY_STREAM_CONTROL_PROTOCOL, streamProtocol(startedEvent));
        payload.add(KEY_STREAM_STREAM_MODE, streamCast(startedEvent));
        payload.add(KEY_STREAM_ADAPTIVITY_INDICATOR, streamRate(startedEvent));
        if (startedEvent == null) {
            payload.add(KEY_STREAM_CONSECUTIVE_PLAYOUT_INDICATOR, NONE);
        }
        else if (startedEvent.getDetail(PlaybackEventDetails.ACTION, StartAction.Undefined) == StartAction.ContinuousPlay) {
            payload.add(KEY_STREAM_CONSECUTIVE_PLAYOUT_INDICATOR, VALUE_CONSECUTIVE);
        }
        else {
            payload.add(KEY_STREAM_CONSECUTIVE_PLAYOUT_INDICATOR, VALUE_START);
        }
        payload.add(KEY_STREAM_PLAY_TIME, playTime);
        payload.add(KEY_STREAM_BIT_RATE, mCurrentBitrate / 1000.);

        // Add client specific parameters
        addClientSpecificParts(payload, startupTime);

        // Stream Properties: depending on stream_1_control_protocol value
        if (startedEvent != null) {
            switch (startedEvent.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None)) {
                case App:
                case None:
                    break;

                case RadioIpTv:
                case LiveTvIpTv:
                case LiveTvDvbC:
                case LiveTvDvbT:
                    addVQEClientMetrics(payload);
                    addVQEJitterMetrics(payload);
                    break;

                case RadioOtt:
                case LiveTvOtt:
                case Replay:
                case Recording:
                case Vod:
                case Trailer:
                case TrickPlay:
                    addOttProperties(payload);
                    break;
            }
        }
    }

    private void addOttProperties(LogDetailsProximus payload) {
        payload.add(KEY_STREAM_EFFECTIVE_PLAY_TIME, effectivePlayTime());
        // avg_bit_rate and avg_download_rate should be expressed in kbit/s
        payload.add(KEY_STREAM_AVERAGE_BIT_RATE, avgBitrate() / 1000.);
        payload.add(KEY_STREAM_AVERAGE_DOWNLOAD_RATE, avgDownloadRate() / 1000.);
        payload.add(KEY_STREAM_PROFILE_SWITCHING_COUNT, mBitrateChanges);
        payload.add(KEY_STREAM_BUFFER_COUNT, mBufferingCount);
        payload.add(KEY_STREAM_BUFFER_RATIO, bufferRatio());
    }

    private void addChannelChangeProperties(LogDetailsProximus payload) {
        payload.add(KEY_STREAM_CHANNEL_CHANGE_CUMUL, mChannelChangeCount);
        switch (getLastStartAction()) {
            default:
                payload.add(KEY_STREAM_CHANNEL_CHANGE_EVENT, NONE);
                break;

            case BtnUp:
                payload.add(KEY_STREAM_CHANNEL_CHANGE_EVENT, VALUE_CCUP);
                break;

            case BtnDown:
                payload.add(KEY_STREAM_CHANNEL_CHANGE_EVENT, VALUE_CCDN);
                break;

            case BtnNumber:
                payload.add(KEY_STREAM_CHANNEL_CHANGE_EVENT, VALUE_CCNUM);
                break;

            case Ui:
                UILog.Page lastPage = MemoryCache.LastOpenedPage.getEnum(UILog.Page.Unknown);
                if (UILog.Page.ZapListPage == lastPage || UILog.Page.MiniEPGPage == lastPage) {
                    payload.add(KEY_STREAM_CHANNEL_CHANGE_EVENT, VALUE_CCEPG);
                }
                break;
        }
    }

    private void addStorageMetricsParameters(LogDetailsProximus payload) {
        StorageInfo info = StorageInfo.read();
        File intStorage = new File(info.get(StorageInfo.Field.IntMountedOn, "/"));
        payload.add(KEY_STORAGE_INT_RW_STATUS, intStorage.canWrite() ? VALUE_RW : VALUE_RO);
        payload.add(KEY_STORAGE_INT_TOTAL_SIZE, info.get(StorageInfo.Field.IntUsed, 0L) +
                info.get(StorageInfo.Field.IntAvailable, 0L));
        payload.add(KEY_STORAGE_INT_FREE_SIZE, info.get(StorageInfo.Field.IntAvailable, 0L));
        payload.add(KEY_STORAGE_INT_WRITE_CYCLES, NONE); // TODO: extra MW call is required

        File extStorage = new File(info.get(StorageInfo.Field.ExtMountedOn, "/"));
        payload.add(KEY_STORAGE_EXT_RW_STATUS, extStorage.canWrite() ? VALUE_RW : VALUE_RO);
        payload.add(KEY_STORAGE_EXT_TOTAL_SIZE, info.get(StorageInfo.Field.ExtUsed, 0L) +
                info.get(StorageInfo.Field.ExtAvailable, 0L));
        payload.add(KEY_STORAGE_EXT_FREE_SIZE, info.get(StorageInfo.Field.ExtAvailable, 0L));
        payload.add(KEY_STORAGE_EXT_WRITE_CYCLES, NONE); // TODO: extra MW call is required

        for (int i = 0; i < info.count(StorageInfo.Field.UsbFilesystem); ++i) {
            int j = i + 1;
            File usbStorage = new File(info.get(StorageInfo.Field.UsbMountedOn, "/", i));
            payload.add(KEY_STORAGE_USB_RW_STATUS_ + j, usbStorage.canWrite() ? VALUE_RW : VALUE_RO);
            payload.add(KEY_STORAGE_USB_TOTAL_SIZE_ + j, info.get(StorageInfo.Field.UsbUsed, 0L, i) +
                    info.get(StorageInfo.Field.UsbAvailable, 0L, i));
            payload.add(KEY_STORAGE_USB_FREE_SIZE_ + j, info.get(StorageInfo.Field.UsbAvailable, 0L, i));
            payload.add(KEY_STORAGE_USB_BLOCK_DEVICE_ + j, info.get(StorageInfo.Field.UsbFilesystem, NONE, i));
            payload.add(KEY_STORAGE_USB_MOUNT_POINT_ + j, info.get(StorageInfo.Field.UsbMountedOn, NONE, i));
        }
    }

    private void addBluetoothDeviceInfo(LogDetailsProximus payload) {
        Set<String> remoteNames = new HashSet<>(mAppConfig.getRemoteNames());
        Set<BluetoothDevice> pairedDevices = BluetoothUtils.getPairedDevices(mContext);
        Set<BluetoothDevice> connectedDevices = BluetoothUtils.getConnectedDevices(mContext);
        payload.add(KEY_BT_DEVICE_COUNT, pairedDevices.size());
        int i = 1;
        for (BluetoothDevice device : pairedDevices) {
            boolean connected = connectedDevices.contains(device);
            payload.add(KEY_BT_DEVICE_MAC_ADDRESS_ + i, device.getAddress().replace(":", ""));
            payload.add(KEY_BT_DEVICE_NAME_ + i, device.getName());
            payload.add(KEY_BT_DEVICE_CONN_ + i, connected);
            payload.add(KEY_BT_DEVICE_REMOTE_ + i, remoteNames.contains(device.getName()));

            if (connected && remoteNames.contains(device.getName())) {
                try {
                    int batteryLevel = BluetoothUtils.readBatteryLevel(mContext, device);
                    String remoteFirmwareVersion = BluetoothUtils.readFirmwareVersion(mContext, device);
                    String remoteSoftwareVersion = BluetoothUtils.readSoftwareVersion(mContext, device);
                    payload.add(KEY_BT_REMOTE_BATTERY_LEVEL_ + i, batteryLevel);
                    payload.add(KEY_BT_REMOTE_FIRMWARE_VERSION_ + i, remoteFirmwareVersion);
                    payload.add(KEY_BT_REMOTE_SOFTWARE_VERSION_ + i, remoteSoftwareVersion);
                } catch (Exception e) {
                    Log.e(TAG, "Failed to read remote info", e);
                    payload.add(KEY_BT_REMOTE_BATTERY_LEVEL_ + i, NONE);
                    payload.add(KEY_BT_REMOTE_FIRMWARE_VERSION_ + i, NONE);
                    payload.add(KEY_BT_REMOTE_SOFTWARE_VERSION_ + i, NONE);
                }
            }
            i++;
        }
    }

    private void addPSUMetrics(LogDetailsProximus payload) {
        // Primary_ecap is reachable via the following properties
        String init_svc_checkagp = mMwProxy.getSystemProperties(KEY_INIT_SVC_CHECK_AGP26);
        String vendor_agp_pfault_pinstate = mMwProxy.getSystemProperties(KEY_VENDOR_AGP26_PFAULT_PINSTATE);

        payload.add(KEY_INIT_SVC_CHECK_AGP26, init_svc_checkagp);
        payload.add(KEY_VENDOR_AGP26_PFAULT_PINSTATE, vendor_agp_pfault_pinstate);

        if (vendor_agp_pfault_pinstate != null) {
            // value of vendor.agp26_pfault_pinstate can be 0 - LOW or 1 - HIGH
            payload.add(KEY_PRIMARY_ECAP, Integer.parseInt(vendor_agp_pfault_pinstate) > 0 ? VALUE_HIGH : VALUE_LOW);
        }
    }

    /**
     * playback time expressed in milliseconds
     */
    private long playTime() {
        if (mIsPlaybackStarted && mPlaybackStarted > mPlayerInitialised) {
            return System.currentTimeMillis() - mPlaybackStarted;
        }
        return 0;
    }

    /**
     * Effective playtime is the average view time for each session and is computed without taking into account:
     *  - join time         -- not included in {@link #playTime()}
     *  - buffer time       -- excluded using {@link #mBufferTime}
     *  - ads time          -- TODO: add it when ads are implemented
     *  - seek time         -- excluded using {@link #mBufferTime} for castLabs player
     *  - pause time        -- excluded using {@link #mPausedTotal}
     * It shows the actual time that users have been engaged with a given video.
     */
    private long effectivePlayTime() {
        long playTime = playTime() - mBufferTime - mPausedTotal;
        if (mPausedStart > 0) {
            // still paused when requesting the value,
            // add the time span since playback was paused.
            playTime -= System.currentTimeMillis() - mPausedStart;
        }
        return playTime < 0 ? 0 : playTime;
    }

    /**
     * averaged bit rate
     */
    private long avgBitrate() {
        if (mBitrateChanges == 0) {
            return mCurrentBitrate;
        }
        return mBitrateTotal / mBitrateChanges;
    }

    /**
     * average download rate in bits per second
      */
    private long avgDownloadRate() {
        if (mSegmentTimeTotal == 0) {
            return 0;
        }
        return mSegmentSizeTotal * 8000 / mSegmentTimeTotal;
    }

    /**
     * buffer ratio expressed in percent
     */
    private double bufferRatio() {
        long playTime = playTime();
        if (playTime == 0) {
            return 0;
        }
        return 100. * mBufferTime / playTime;
    }

    private StartAction getLastStartAction() {
        Event<PlaybackLogEvent, PlaybackEventDetails> event = mLastStartEvent;
        if (event == null) {
            return StartAction.Undefined;
        }
        return event.getDetail(PlaybackEventDetails.ACTION, StartAction.Undefined);
    }
}
