package tv.threess.threeready.data.proximus.search.model;

/**
 * Enum for allowed local search types.
 *
 * Created by Noemi Dali on 11.05.2020.
 */
public enum SearchType {

    DirectSearchProgramsNl("directsearchprogramsnl"),
    DirectSearchProgramsFr("directsearchprogramsfr"),
    DirectSearchProgramsNotNl("directsearchprogramsnotnl"),
    DirectSearchProgramsNotFr("directsearchprogramsnotfr"),
    DirectSearchVod("directsearchvod"),

    SuggestSearchProgramsFr("suggestsearchprogramsfr"),
    SuggestSearchProgramsNl("suggestsearchprogramsnl"),
    SuggestSearchProgramsNotNl("suggestsearchprogramsnotnl"),
    SuggestSearchProgramsNotFr("suggestsearchprogramsnotfr"),
    SuggestSearchVod("suggestsearchvod"),

    VoiceSearchEpg("voicesearchepg"),
    VoiceSearchVod("voicesearchvod"),
    VoicePlayItem("voiceplayitem");

    private final String mValue;

    SearchType(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }
}
