package tv.threess.threeready.data.proximus.home.model.module;

import androidx.annotation.Nullable;

import java.util.List;

import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceParams;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceService;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
* Module config which shown currently running programs.

* @author Barabas Attila
* @since 6/27/21
*/

public class NowOnTvSwimlaneModuleProximus extends SwimlaneModuleConfigProximus {

    private final ModuleDataSourceParams mParams;

    public NowOnTvSwimlaneModuleProximus(SwimLaneProximus swimlane) {
        super(swimlane);
        mParams = new NowOnTvDataSourceParams(swimlane);
    }

    @Nullable
    @Override
    public ModuleDataSource getDataSource() {
        return mDataSource;
    }

    private final ModuleDataSource mDataSource = new ModuleDataSource() {

        @Override
        public ModuleDataSourceService getService() {
            return ModuleDataSourceService.TV;
        }

        @Override
        public ModuleDataSourceMethod getMethod() {
            return ModuleDataSourceMethod.TV_PROGRAM_NOW;
        }

        @Nullable
        @Override
        public ModuleDataSourceParams getParams() {
            return mParams;
        }
    };

    private static class NowOnTvDataSourceParams extends SwimlaneModuleDataSourceParams {
        private final List<String> mGenreFilter;

        public NowOnTvDataSourceParams(SwimLaneProximus swimlane) {
            super(swimlane);
            mGenreFilter = swimlane.getGenreNames();
        }

        @Nullable
        @Override
        public List<String> getFilterList(String key) {
            switch (key) {
                default:
                    return null;
                case ModuleFilterOption.Mixed.Genre.NAME :
                    return mGenreFilter;
            }
        }
    }
}
