package tv.threess.threeready.data.proximus.tv.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.ChannelType;

/**
 * XML type adapter to parse channel type.
 *
 * @author Barabas Attila
 * @since 2020.03.25
 */
public class ChannelTypeAdapter extends XmlAdapter<String, ChannelType> {
    private static final String TAG = Log.tag(ChannelTypeAdapter.class);

    @Override
    public ChannelType unmarshal(String v) {
        switch (v) {
            default:
                Log.w(TAG, "Unknown channel type. " + v);
            case "DTV":
                return ChannelType.TV;
            case "MUS":
                return ChannelType.RADIO;
        }
    }

    @Override
    public String marshal(ChannelType v) {
        throw new IllegalStateException("Not implemented");
    }
}
