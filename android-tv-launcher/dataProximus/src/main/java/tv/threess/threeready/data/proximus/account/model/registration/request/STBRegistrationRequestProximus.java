package tv.threess.threeready.data.proximus.account.model.registration.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.data.proximus.generic.adapter.PinAdapterProximus;

/**
 * BTA request to register the STB for an account.
 *
 * @author Barabas Attila
 * @since 2020.04.14
 */
@XmlRootElement(name = "BTA")
public class STBRegistrationRequestProximus implements Serializable {

    @XmlElement(name = "AutoConfigDoc")
    AutoConfigDocProximus mAutoConfigDoc = new AutoConfigDocProximus();

    public STBRegistrationRequestProximus(String ipAddress, String serialNum, String accountPIN,
                                          String hardwareVersion, String softwareVersion,
                                          String androidSerialNumber, String CASDeviceID) {
        mAutoConfigDoc.mIPAddress = ipAddress;
        mAutoConfigDoc.mSerialNum = serialNum;
        mAutoConfigDoc.mAccountPIN = accountPIN;
        mAutoConfigDoc.mHardwareVersion = hardwareVersion;
        mAutoConfigDoc.mSoftwareVersion = softwareVersion;
        mAutoConfigDoc.mAndroidSerialNumber = androidSerialNumber;
        mAutoConfigDoc.mCASDeviceID = CASDeviceID;

        mAutoConfigDoc.mName = ""; // Will be generated om BE.

        // TODO : make this configurable for OTT devices.
        mAutoConfigDoc.mDeviceMDP = "IPTV";
        mAutoConfigDoc.mBEPDeviceType = "STB_IPTV";
    }


    @XmlRootElement(name = "AutoConfigDoc")
    private static class AutoConfigDocProximus {

        @XmlElement(name = "IPAddress")
        private String mIPAddress;

        @XmlElement(name = "STBSerialNum")
        private String mSerialNum;

        @XmlJavaTypeAdapter(PinAdapterProximus.class)
        @XmlElement(name = "AccountPIN")
        private String mAccountPIN;

        @XmlElement(name = "STBHardwareVersion")
        private String mHardwareVersion;

        @XmlElement(name = "STBSoftwareVersion")
        private String mSoftwareVersion;

        @XmlElement(name = "BEPDeviceTypeExternalID")
        private String mBEPDeviceType;

        @XmlElement(name = "DeviceMDP")
        private String mDeviceMDP;

        @XmlElement(name = "STBSerialNumAndroid")
        private String mAndroidSerialNumber;

        @XmlElement(name = "CASDeviceID")
        private String mCASDeviceID;

        @XmlElement(name = "STBName")
        private String mName;
    }
}
