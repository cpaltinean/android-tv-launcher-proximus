package tv.threess.threeready.data.proximus.home.observable;

import android.content.Context;

import java.io.IOException;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.data.proximus.search.SearchProxyProximus;

/**
 * Rx observable class for movies and series search results.
 *
 * @author Barabas Attila
 * @since 2017.12.19
 */

public class ProgramSearchResultObservable extends CategoryContentObservable {

    private final SearchProxyProximus mSearchProxy = Components.get(SearchProxyProximus.class);

    public ProgramSearchResultObservable(Context context, ModuleConfig moduleConfig, int start, int count) {
        super(context, moduleConfig, start, count, null);
    }

    @Override
    protected void loadItemList(ObservableEmitter<ModuleData<IBaseContentItem>> emitter) throws IOException {
        mCategoryData = mSearchProxy.contentSearch(mModuleConfig, mStart, mCount);
        onItemListLoaded(emitter);
    }
}
