/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.generic.model;

import java.io.Serializable;

import tv.threess.threeready.api.generic.model.IImageSource;

/**
 * Generic container to hold and image url and the dimensions of the image.
 *
 * @author Barabas Attila
 * @since 2018.11.21
 */
public class ImageSourceProximus implements Serializable, IImageSource {
    private final String mUrl;
    private int mWidth;
    private int mHeight;
    private final Type mType;

    public ImageSourceProximus(String url) {
        this(url, Type.COVER);
    }

    public ImageSourceProximus(String url, Type type) {
        mUrl = url;
        mType = type;
    }

    @Override
    public String getUrl() {
        return mUrl;
    }

    @Override
    public int getHeight() {
        return mHeight;
    }

    @Override
    public int getWidth() {
        return mWidth;
    }

    @Override
    public Type getType() {
        return mType;
    }
}
