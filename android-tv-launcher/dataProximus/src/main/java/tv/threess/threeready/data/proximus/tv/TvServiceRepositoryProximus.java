/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.tv;

import android.app.Application;
import android.text.TextUtils;
import android.util.Range;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountCache;
import tv.threess.threeready.api.account.model.CacheSettings;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.home.model.generic.ChannelApp;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.model.ITvChannelResponse;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.proximus.account.SessionProximus;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.proximus.account.model.account.PackageNamesProximus;
import tv.threess.threeready.data.proximus.generic.RetrofitServicesProximus;
import tv.threess.threeready.data.proximus.mw.MwProxyProximus;
import tv.threess.threeready.data.proximus.tv.model.advertisement.TvAdvertisementProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.TvBroadcastProximus;
import tv.threess.threeready.data.proximus.tv.model.channel.TvChannelProximus;
import tv.threess.threeready.data.proximus.tv.model.channel.TvChannelResponseProximus;
import tv.threess.threeready.data.proximus.tv.model.channel.VQEInfoTCH;
import tv.threess.threeready.data.tv.BaseTvServiceRepository;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.data.tv.TvProxy;
import tv.threess.threeready.data.tv.model.TvAdvertisement;

/**
 * Implementation of the Proximus specific tv service handler methods.
 *
 * @author Barabas Attila
 * @since 2018.06.05
 */
public class TvServiceRepositoryProximus extends BaseTvServiceRepository implements Component {
    private static final String TAG = Log.tag(TvServiceRepositoryProximus.class);

    private final RetrofitServicesProximus mRetrofitServices = Components.get(RetrofitServicesProximus.class);
    private final AccountCache.Packages mPackageCache = Components.get(AccountCache.Packages.class);
    private final TvProxyProximus mTvProxy = Components.get(TvProxyProximus.class);

    public TvServiceRepositoryProximus(Application app) {
        super(app);
    }

    @Override
    public void updateChannels() throws Exception {
        channelUpdater.updateIfExpired(() -> updateChannels(getChannels()));
    }

    @Override
    public void updatePlaybackConfig() throws Exception {
        // Get VQE config files.
        Log.d(TAG, "Updating VQE config files");
        mMwProxy.updateVqeConfig(mTvProxy.getVQEInfoTCH());
    }


    /**
     * Periodic EPG cache window update.
     * It will cache the broadcast inside the cache window and remove the broadcast outside of the window.
     *
     * @see CacheSettings#getEpgCacheWindowLength(java.util.concurrent.TimeUnit)
     */
    @Override
    public void updateBroadcasts() throws Exception {
        broadcastUpdater.updateIfExpired(() -> {
            boolean purge = checkForVersionEPGUpdate();
            if (purge) {
                Settings.lastBaseBroadcastSyncTime.edit().remove();
            }
            baseBroadcastUpdater.updateIfExpired(() -> cacheBroadcasts(purge));
        });
    }

    /**
     * @return True if the cached EPG window is outside of the current cache window.
     */
    private boolean isBaseCacheExpired() {
        // Calculate if the whole cache needs to be purged or can be updated with delta.
        long cacheLength = mAppConfig.getCacheSettings().getEpgCacheWindowLength(TimeUnit.MILLISECONDS);
        long oldCacheWindowEnd = Settings.lastBaseBroadcastSyncTime.get(0L) + cacheLength / 2;
        long newCacheWindowStart = System.currentTimeMillis() - cacheLength / 2;
        return oldCacheWindowEnd < newCacheWindowStart;
    }

    /**
     * @return True if a new EPG version is available on the server.
     */
    private boolean checkForVersionEPGUpdate() throws IOException {
        String availableUpdatesPath = mTvProxy.getAvailableUpdates().getPath();
        String epgSplicedPath = SessionProximus.epgSplicePath.get("");
        if (!Objects.equals(epgSplicedPath, availableUpdatesPath)) {
            Log.d(TAG, "Spliced EPG path changed. Old path : "
                    + epgSplicedPath + ", new path : " + availableUpdatesPath);
            SessionProximus.epgSplicePath.edit().put(availableUpdatesPath);
            return true;
        }
        return false;
    }

    /**
     * Update the broadcast in the cache.
     * It will cache the broadcast inside the cache window and remove the broadcast outside of the window.
     *
     * @param purge True if the update should delete and re-sync the broadcast even if those are inside the cache window.
     * @see CacheSettings#getEpgCacheWindowLength(java.util.concurrent.TimeUnit)
     */
    private void cacheBroadcasts(boolean purge) throws Exception {
        // Calculate the EPG cache window.
        long cacheLength = mAppConfig.getCacheSettings().getEpgCacheWindowLength(TimeUnit.MILLISECONDS);
        long currentTime = System.currentTimeMillis();
        long newCacheWindowStart = currentTime - cacheLength / 2;
        long newCacheWindowEnd = currentTime + cacheLength / 2;

        List<TvBroadcastProximus> broadcasts;
        // Purge everything and sync from server.
        if (purge || isBaseCacheExpired()) {
            // Update current broadcast first.
            Log.d(TAG, "Update current broadcasts on all channels.");
            broadcasts = mTvProxy.getCurrentEvents().getBroadcasts();
            mTvCache.updateBroadcastsTotally(broadcasts, System.currentTimeMillis());

            // Consider the cache valid for 10 minutes after current programs update.
            long temporalBroadcastSyncTime = newCacheWindowStart + TimeUnit.MINUTES.toMillis(10);
            Settings.lastBaseBroadcastSyncTime.edit().put(temporalBroadcastSyncTime);
            Log.d(TAG, "Current programs update. Sync time : " + Log.timestamp(temporalBroadcastSyncTime));

            // Fully update the cache window.
            Log.d(TAG, "Update cached broadcast on all channels."
                    + ", from: " + Log.timestamp(newCacheWindowStart)
                    + ", to: " + Log.timestamp(newCacheWindowEnd));
            broadcasts = mTvProxy.getBroadcasts(newCacheWindowStart, newCacheWindowEnd).getBroadcasts();

            mTvCache.updateBroadcastsTotally(broadcasts, currentTime);

            // Calculate the missing data and update.
        } else {
            Range<Long> requestRange = calculateMissingBroadcastsInterval(newCacheWindowStart, newCacheWindowEnd);

            if (requestRange == null) {
                Log.d(TAG, "Skipping broadcast update. Cache already up too date.");
                return;
            }

            Log.d(TAG, "Update broadcasts on all channels."
                    + ", from: " + Log.timestamp(requestRange.getLower())
                    + ", to: " + Log.timestamp(requestRange.getUpper()));
            broadcasts = mTvProxy.getBroadcasts(requestRange.getLower(), requestRange.getUpper()).getBroadcasts();

            // Update cache and clear everything outside of the EPG cache window.
            mTvCache.updateBroadcasts(broadcasts, newCacheWindowStart, newCacheWindowEnd);
        }

        Settings.lastBaseBroadcastSyncTime.edit().put(currentTime);
        Log.d(TAG, "Broadcast sync time update. " + Log.timestamp(currentTime));
    }

    public void updateAdvertisements() throws IOException {
        Collection<TvAdvertisementProximus> ads = ArrayUtils.notNull(mTvProxy.getAdvertisements());
        mTvCache.updateAdvertisements(ads);
    }

    @Override
    public void downloadAdvertisements(long trigger, String filename) throws IOException {
        Log.d(TAG, "downloadAdvertisements(trigger: " + Log.timestamp(trigger) + ", filename: " + filename + ")");
        enforceTargetAdvertisementEnabled();
        long now = System.currentTimeMillis();

        // If the intent is empty or does not have a filename then we need to trigger pre-download
        long minUpdateTime = TimeBuilder.utc(now)
                .add(TimeUnit.HOURS, -2 * Settings.tasADRetrievalInterval.get(24))
                .get();

        boolean forceUpdate = TextUtils.isEmpty(filename);
        if (!LocaleUtils.getApplicationLanguage().equals(Settings.tadDownloadLanguage.get(null))) {
            Log.d(TAG, "Forcing advertisement update: language changed");
            forceUpdate = true;
        }
        if (mTvCache.getLastUpdateTime(minUpdateTime) <= minUpdateTime) {
            Log.d(TAG, "Forcing advertisement update: cache is out of date");
            forceUpdate = true;
        }

        if (forceUpdate) {
            updateAdvertisements();
            List<TvAdvertisement> ads = mTvCache.getAdvertisements(false);
            downloadAds(ads, false, now - trigger);
            Settings.tadDownloadLanguage.edit().put(LocaleUtils.getApplicationLanguage());
        }
        if (TextUtils.isEmpty(filename)) {
            return;
        }

        List<TvAdvertisementProximus> ads = mTvProxy.getAdvertisement(filename);
        downloadAds(ads, true, now - trigger);
        mTvCache.insertAdvertisements(ads);
    }

    private void downloadAds(List<? extends TvAdvertisement> ads, boolean onMarker, long triggerDelay) {
        long now = System.currentTimeMillis();
        // 1. create the download directory for ad files
        File path = new File(TvProxy.TAD_DOWNLOAD_DIR);
        if (!path.exists()) {
            if (!path.mkdirs()) {
                Log.d(TAG, "failed to create ad download dir: " + path);
                return;
            }
            if (!setPermissions(path)) {
                Log.d(TAG, "failed to set permissions ad download dir: " + path);
                return;
            }
        }

        // 2. get the list of ads from database (known ads)
        Map<String, TvAdvertisement> knownAds = new HashMap<>();
        for (TvAdvertisement ad : mTvCache.getAllAdvertisements()) {
            knownAds.put(ad.getFilename(), ad);
        }

        class DeletableAdFile {
            public final boolean isMissing;
            public final boolean onMarker;
            public final long expiration;
            public final long fileTime;
            public final File file;

            public DeletableAdFile(TvAdvertisement ad, File file) {
                this.isMissing = ad == null;
                this.onMarker = ad == null || ad.isOnMarker();
                this.expiration = ad == null ? 0 : ad.getExpires();
                this.fileTime = FileUtils.lastAccessed(file);
                this.file = file;
            }

            @Override
            public String toString() {
                return file.getPath();
            }
        }

        // 3. make a sorted list of deletable files (non pre-downloaded asd)
        ArrayList<DeletableAdFile> deletableAds = new ArrayList<>();
        for (File file : FileUtils.list(path)) {
            TvAdvertisement ad = knownAds.get(file.getName());
            if (ad == null) {
                // file can be deleted due no metadata in database
                deletableAds.add(new DeletableAdFile(null, file));
                continue;
            }

            if (ad.isExpired(now)) {
                // expired ads can be deleted
                deletableAds.add(new DeletableAdFile(ad, file));
                continue;
            }

            if (onMarker && ad.isOnMarker()) {
                // only delete on-marker ads when downloading on-marker ads
                deletableAds.add(new DeletableAdFile(ad, file));
            }
        }
        deletableAds.sort((a, b) -> {
            if (a.isMissing != b.isMissing) {
                // missing metadata ad files on top
                return -Boolean.compare(a.isMissing, b.isMissing);
            }

            if (a.onMarker != b.onMarker) {
                // on marker downloaded ads on top
                return -Boolean.compare(a.onMarker, b.onMarker);
            }

            if (a.expiration != b.expiration) {
                // expired ads on top
                return Long.compare(a.expiration, b.expiration);
            }

            // compare by last accessed time
            return Long.compare(a.fileTime, b.fileTime);
        });

        // 4. remove expired and existing ads from the download list
        ArrayList<TvAdvertisement> download = new ArrayList<>(ads);
        download.removeIf(ad -> {
            if (ad.isExpired(now)) {
                // do not download expired ads
                return true;
            }

            File adFile = new File(path, ad.getFilename());
            if (ad.getSize() == adFile.length()) {
                // file size matches to the previous downloaded size
                return adFile.exists();
            }

            return false;
        });

        // 5. calculate space needed for download
        long downloadSize = 0;
        for (TvAdvertisement ad : download) {
            downloadSize += ad.getSize();
        }

        // 6. free up disk space for the new ads by deleting old files
        Log.d(TAG, "starting cleaning up ads: " + deletableAds.size() +
                ", download size: " + downloadSize +
                ", free space: " + FileUtils.freeSpace(path)
        );
        for (DeletableAdFile ad : deletableAds) {
            long freeSpace = FileUtils.freeSpace(path);
            if (freeSpace >= downloadSize) {
                // there is enough space to download the ads
                break;
            }
            Log.d(TAG, "cleanup ad, free space: " + freeSpace + ", ad: " + ad);
            if (!ad.file.delete()) {
                Log.d(TAG, "failed to delete ad: " + ad.file);
            }
        }

        // 7. download new ad video files
        Log.d(TAG, "starting downloading ad files: " + download.size());
        String serverUrl = Settings.taCdnPublishUrl.get(null);
        for (TvAdvertisement ad : download) {
            HttpUrl url = HttpUrl.parse(serverUrl).newBuilder().addPathSegment(ad.getFilename()).build();
            File tmpFile = new File(path, ad.getFilename() + ".download");
            File adFile = new File(path, ad.getFilename());
            Exception downloadException = null;
            long downloadDuration = 0;

            try (OutputStream out = new FileOutputStream(tmpFile)) {
                Request request = new Request.Builder().url(url).build();
                mApp.getContentResolver().call(TvContract.Advertisement.CONTENT_URI, TvContract.Advertisement.CALL_INCREMENT_ATTEMPTS, ad.getCreativeId(), null);

                downloadDuration = -System.currentTimeMillis();
                Response response = mRetrofitServices.getCacheLessOkHttpClient().newCall(request).execute();
                if (!response.isSuccessful() || response.body() == null) {
                    throw new BackendException(response.code(), response.message());
                }

                try (InputStream tad = response.body().byteStream()) {
                    FileUtils.copy(out, tad);
                    if (!tmpFile.renameTo(adFile)) {
                        Log.d(TAG, "could not rename ad file: " + ad.getFilename());
                        continue;
                    }

                    boolean permissionSet = setPermissions(adFile);
                    Log.d(TAG, "successfully downloaded ad file: " + ad.getFilename() +
                            ", permission set: " + permissionSet);
                    downloadDuration += System.currentTimeMillis();
                }
            } catch (Exception e) {
                downloadException = e;
                Log.e(TAG, "failed to download ad file: " + adFile, e);
            } finally {
                UILog.logAdDownload(adFile, downloadException, downloadDuration, triggerDelay, ad.getAttemptCount());
            }
        }
    }

    private ITvChannelResponse<TvChannelProximus> getChannels() throws IOException {
        TvChannelResponseProximus channelResponse = mTvProxy.getChannels();

        boolean hasReplaySubscription = mPackageCache.isPackageSubscribed(PackageNamesProximus.REPLAY_EXTERNAL_ID)
                || mPackageCache.isPackageSubscribed(PackageNamesProximus.REPLAY_PLUS_EXTERNAL_ID);

        Set<String> babyChannelIds = SettingsProximus.babyChannelIds.getSet();
        Set<String> regionalChannelIds = SettingsProximus.regionalChannelIds.getSet();

        Map<String, ChannelApp> channelApps = Components.get(AppConfig.class).getChannelApps();

        for (TvChannelProximus channel : channelResponse.getChannels()) {
            // Replay subscription.
            channel.setHasReplaySubscription(hasReplaySubscription);

            // Mark baby channels.
            channel.setBabyContentWarningRequired(babyChannelIds.contains(channel.getId()));

            // Check if the channel is app channel
            if (channelApps.containsKey(channel.getCallLetter())) {
                ChannelApp channelApp = channelApps.get(channel.getCallLetter());
                if (channelApp != null && !TextUtils.isEmpty(channelApp.getPackageName())) {
                    channel.setPackageName(channelApp.getPackageName());
                    channel.setChannelType(ChannelType.APP);
                }
            } else if (regionalChannelIds.contains(channel.getId())) {
                channel.setChannelType(ChannelType.REGIONAL);
            }
        }

        // Filter HD channel on SD profile.
        boolean hdInterest = SettingsProximus.hdProfile.get(false);
        if (!hdInterest) {
            channelResponse.getChannels().removeIf(TvChannelProximus::isHDEnabled);
        }

        // Sort channels based on channel number.
        channelResponse.getChannels().sort(Comparator.comparingInt(TvChannelProximus::getNumber));


        // Get VQE config files.
        String channelConfig = null;
        try {
            channelConfig = mTvProxy.getVQEChannelConfig();
        } catch (Exception e) {
            try {
                channelConfig = FileUtils.readFromFile(MwProxyProximus.channelConfigFile(mApp));
                Log.e(TAG, "Couldn't load VQEChannel config, using local file", e);
            } catch (Exception ex) {
                Log.e(TAG, "Couldn't load VQEChannel config file.", ex);
            }
        }

        String networkConfig = null;
        try {
            networkConfig = mTvProxy.getVQENetworkConfig();
        } catch (Exception e) {
            Log.e(TAG, "Couldn't load VQENetwork config.", e);
        }

        VQEInfoTCH vqeInfo = new VQEInfoTCH(channelConfig, networkConfig, mTvProxy.getVQEBurstConfig());
        channelResponse.setVQEInfo(vqeInfo);

        // Get VCAS info.
        channelResponse.setVCASInfo(mTvProxy.getVCASInformation());

        return channelResponse;
    }

    /**
     * The STB will analyse whether target ad features are applicable to him based on:
     * - HW capabilities
     * - Line profile capabilities
     * - Target ad server information available in the GlobalInstall
     * - Availability of a subscription to at least one channel that is target ad enabled
     * - Opt-in for target advertisement is set
     * <p>
     * If for any of the above reasons the STB is not enabled for target advertisement,
     * it should not execute Target ad download / replacement
     */
    private void enforceTargetAdvertisementEnabled() throws IOException {
        if (!Settings.tadHardwareEnabled.get(false)) {
            throw new IOException("No AD feature: HW capabilities");
        }

        if (Settings.tadDownloadBandwidth.get(0) <= 0) {
            throw new IOException("No AD feature: Line profile capabilities");
        }

        if (Settings.taCdnPublishUrl.get(null) == null) {
            throw new IOException("No AD feature: Target ad server information available in the GlobalInstall");
        }

        if (!Settings.targetedAdvertisingFlag.get(false)) {
            throw new IOException("No AD feature: Opt-in for target advertisement is set");
        }

        if (!mTvCache.isChannelWithEnabledTAD()) {
            throw new IOException("No AD feature: User has no TAD enabled channel");
        }
    }

    private boolean setPermissions(File file) {
        try {
            String mode = file.isDirectory() ? "777" : "666";
            MwProxy.exec("chmod", mode, file.getAbsolutePath());
            return true;
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return false;
    }
}
