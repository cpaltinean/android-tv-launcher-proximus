package tv.threess.threeready.data.proximus.home.model.selector;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.api.tv.TvChannelCacheType;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Menu selector used in the Replay channel wall page.
 * For now, this can be "All" and "Favorites".
 *
 * @author David Bondor
 * @since 4.28.2022
 */
public class ChannelMenuSelector implements SelectorOption {

    private final Translator mTranslator = Components.get(Translator.class);

    private final TvChannelCacheType mChannelType;
    private final String mName;
    private final boolean mIsDefault;
    private final String mEmptyPageMessage;

    public ChannelMenuSelector(TvChannelCacheType channelType, String name, boolean isDefault) {
        this(channelType, name, null, isDefault);
    }

    public ChannelMenuSelector(TvChannelCacheType channelType, String name, String emptyPageMessage, boolean isDefault) {
        mChannelType = channelType;
        mName = name;
        mEmptyPageMessage = emptyPageMessage;
        mIsDefault = isDefault;
    }

    @Override
    public String getEmptyPageMessage() {
        return mEmptyPageMessage;
    }

    @Override
    public String getReportName() {
        return mName;
    }

    @Override
    public String getReportReference() {
        return getReportName();
    }

    @Override
    public String getName() {
        return mTranslator.get(mName);
    }

    @Override
    public boolean isDefault() {
        return mIsDefault;
    }

    public TvChannelCacheType getChannelType() {
        return mChannelType;
    }
}