package tv.threess.threeready.data.proximus.iam;

import android.app.Application;
import android.net.Uri;
import android.text.TextUtils;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.net.HttpURLConnection;
import java.security.PrivateKey;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Invocation;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.generic.exception.MissingBaseUrlException;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.proximus.account.SessionProximus;
import tv.threess.threeready.data.proximus.account.retrofit.BootConfigRetrofitProximus;
import tv.threess.threeready.data.proximus.iam.model.IAMAccessTokenResponseProximus;
import tv.threess.threeready.data.proximus.iam.model.IAMRegisterPayloadProximus;
import tv.threess.threeready.data.proximus.iam.model.IAMRegisterResponseProximus;
import tv.threess.threeready.data.proximus.iam.model.IAMSoftwareStatementProximus;
import tv.threess.threeready.data.utils.GsonUtils;

/**
 * OkHttp interceptor which handles the IAM authentication.
 *
 * @author Barabas Attila
 * @since 2020.03.19
 */
public abstract class IAMOkHttpInterceptor implements Interceptor {
    private static final String TAG = Log.tag(IAMOkHttpInterceptor.class);

    private static final String PAYLOAD_HEADER_TYPE_KEY = "typ";
    private static final String PAYLOAD_HEADER_ALGORITHM_KEY = "alg";
    private static final String PAYLOAD_HEADER_KEY_ID = "kid";

    private static final String PAYLOAD_HEADER_TYPE_VALUE = "JWT";
    private static final String PAYLOAD_HEADER_ALGORITHM_VALUE = "ES256";

    private static final String AUTHORIZATION = "Authorization";

    private final IAMRetrofitProximus mIAMRetrofit;

    protected final Application mApp;
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    private final MwProxy mMwProxy = Components.get(MwProxy.class);

    public IAMOkHttpInterceptor(Application application,
                                IAMRetrofitProximus iamRetrofit) {
        mApp = application;
        mIAMRetrofit = iamRetrofit;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        if (mustAuthenticate(request)) {
            return securedIntercept(chain, true);
        }

        return unsecuredIntercept(chain);
    }

    /**
     * Intercept and proceed with the request without TIAMS authentication.
     * @param chain The backend request chain to dispatch it to other interceptors.
     * @return The backend response.
     * @throws IOException In case of errors.
     */
    private Response unsecuredIntercept(Chain chain) throws IOException{
        Request request = chain.request();
        Request.Builder requestBuilder = chain.request().newBuilder();
        overrideRequest(request, requestBuilder, getBaseUrl(false));
        return chain.proceed(requestBuilder.build());
    }

    /**
     * Intercept the backend requests and do the IAM authentication if needed.
     * @param chain The backend request chain to dispatch it to other interceptors.
     * @param retryOnError True if should refresh the token and retry in case of IAM error.
     * @return The backend response.
     * @throws IOException In case of errors.
     */
    private Response securedIntercept(Chain chain, boolean retryOnError) throws IOException {
        Response response;
        String accessToken;
        try {
            lock.readLock().lock(); // Lock calls during toke update.
            Request request = chain.request();
            Request.Builder requestBuilder = chain.request().newBuilder();

            // Override request parameters
            overrideRequest(request, requestBuilder, getBaseUrl(true));

            // Add IAM access token if available.
            accessToken = SessionProximus.iamAccessToken.get("");
            if (!TextUtils.isEmpty(accessToken)) {
                requestBuilder.addHeader(AUTHORIZATION, accessToken);
            }

            response = chain.proceed(requestBuilder.build());
        } finally {
            lock.readLock().unlock();
        }

        // Trigger IAM authentication and retry afterwards.
        if (response.code() == HttpURLConnection.HTTP_UNAUTHORIZED && retryOnError) {
            try {
                // Lock multiple threads to update the token at the same time.
                lock.writeLock().lock();

                String currentToken = SessionProximus.iamAccessToken.get("");
                // Token not refreshed yet. Try to refresh.
                if (Objects.equals(accessToken, currentToken)) {
                    refreshAccessToken(true);
                }
            } finally {
                lock.writeLock().unlock();
            }

            // Token refreshed. Try again.
            return securedIntercept(chain, false);
        }

        return response;
    }

    /**
     * @param secured True a TIAMS url needs to be returned.
     * @return The base url which needs to replace the placeholder.
     */
    protected String getBaseUrl(boolean secured) {
        if (secured) {
            return SessionProximus.securedBtaBaseUrl.get("");
        }
        return SessionProximus.unsecuredBtaBaseUrl.get("");
    }

    /**
     * Override the original request url.
     * Add extra generic query parameters to it.
     * @param uriBuilder The builder to override the url.
     */
    protected void overrideRequestUri(Uri.Builder uriBuilder) {
    }

    /**
     * Override the request. Replace the placeholder URL with the real one.
     * Add extra generic header and url parameters.
     * @param request The original request which needs to be modified.
     * @param requestBuilder The builder for the new request.
     * @param overrideBaseUrl The base url which will replace the placeholder.
     * @throws IOException In case of errors.
     */
    protected void overrideRequest(Request request, Request.Builder requestBuilder, String overrideBaseUrl) throws IOException {
        String requestUrl = request.url().toString();

        if (TextUtils.isEmpty(overrideBaseUrl)) {
            throw new MissingBaseUrlException(
                    "Missing base URL for " + request.url().encodedPath());
        }

        // Remove extra '/' from URL end.
        if (overrideBaseUrl.endsWith("/")) {
            overrideBaseUrl = overrideBaseUrl
                    .substring(0, overrideBaseUrl.lastIndexOf("/"));
        }

        // Override request base url with the value from boot config.
        if (requestUrl.startsWith(BootConfigRetrofitProximus.BASE_URI)) {
            requestUrl = requestUrl.replace(BootConfigRetrofitProximus.BASE_URI, overrideBaseUrl);
        }

        // Override request URI.
        Uri.Builder uriBuilder = Uri.parse(requestUrl).buildUpon();
        overrideRequestUri(uriBuilder);
        requestBuilder.url(uriBuilder.build().toString());
    }

    /**
     * @return True if the call on the given url needs to be done through TIAMS.
     */
    protected boolean mustAuthenticate(Request request) {
        // Bypass not allowed.
        if (!SessionProximus.iamBypassAllowed.get(false)) {
            return true;
        }

        // No unsecured url, most authenticate.
        if(TextUtils.isEmpty(SessionProximus.unsecuredBtaBaseUrl.get(null))) {
            return true;
        }

        // Check annotation on method.
        Invocation invocation = request.tag(Invocation.class);
        if (invocation != null && invocation.method().getAnnotation(MustAuthenticate.class) != null) {
            return true;
        }

        // Check if secured path.
        List<String> securedPaths = SessionProximus.securedPaths.getList();
        return securedPaths == null || securedPaths.contains(request.url().encodedPath());
    }

    /**
     * Register the IAM client if it's not registered yet and request new access token.
     * @param retryError True if the refresh should try again in case of errors.
     *             E.g. retry the client registration in case of token access errors.
     * @throws IOException in case of errors.
     */
    private void refreshAccessToken(boolean retryError) throws IOException {
        // Get client registration info from cache.
        String clientId = SessionProximus.iamClientId.get("");

        Log.d(TAG, "Refresh IAM access token. Client id : " + clientId);

        if (TextUtils.isEmpty(clientId)) { // Client not registered yet.
            // Register the client and refresh token after.
            registerClient();
            refreshAccessToken(false);
            return;
        }

        // Try the get now access token.
        try {
          accessToken(clientId);
        } catch (Exception e) {
            Log.e(TAG, "Failed to get new access token.", e);
           if (retryError) {
               // Clear the client registration info.
               SessionProximus.batchEdit()
                       .remove(SessionProximus.iamGrantType)
                       .remove(SessionProximus.iamClientId)
                       .remove( SessionProximus.iamClientSecret)
                       .persistNow();

               // Retry the client registration and token access
               refreshAccessToken(false);
           } else {
               throw e;
           }
        }
    }

    /**
     * Get a new access token from server and cache it for latter use.
     * @param clientId The registered client id.
     * @throws IOException In case of errors.
     */
    private void accessToken(String clientId) throws IOException {
        Log.d(TAG, "Get IAM access token. Client id : " + clientId);
        String clientSecret = SessionProximus.iamClientSecret.get("");
        String accessUrl = SessionProximus.iamAccessTokenUrl.get("");
        String scope = SessionProximus.iamScopes.get("");
        String grantType = SessionProximus.iamGrantType.get("");
        String auth = "Basic " + Base64.getEncoder()
                .encodeToString((clientId + ":" + clientSecret).getBytes());

        IAMAccessTokenResponseProximus accessTokenResponse =
                mIAMRetrofit.accessToken(accessUrl, auth, grantType, scope).execute().body();

        if (accessTokenResponse == null) {
            throw new BackendException("Failed register JWT.");
        }

        String accessToken = accessTokenResponse.getTokeType() + " " +accessTokenResponse.getAccessToken();
        SessionProximus.iamAccessToken.edit().put(accessToken);
        Log.d(TAG, "IAM access token refreshed. Token : " + accessToken);
    }

    /**
     * Register as a new IAM client to the server.
     * The register response will contain the client id and secret needed for token access.
     * @throws IOException in case of errors.
     */
    private void registerClient() throws IOException {
        String jwtPayload = createRegisterJWT();
        Log.d(TAG, "Register JWT : " + jwtPayload);

        // Register client on backend
        String registerUrl = SessionProximus.iamRegisterUrl.get("");
        IAMRegisterResponseProximus response = mIAMRetrofit.register(registerUrl,
                RequestBody.create(MediaType.parse("text/plain"), jwtPayload)).execute().body();
        if (response == null) {
            throw new BackendException("Failed register JWT.");
        }

        // Save grant type, client id  and secret for toke access.
        SessionProximus.batchEdit()
                .put(SessionProximus.iamClientId, response.getClientId())
                .put(SessionProximus.iamClientSecret, response.getClientSecret())
                .put(SessionProximus.iamGrantType, response.getGrantType())
                .persistNow();

        Log.d(TAG, "IAM client registered. Client id : " + response.getClientId());
    }


    /**
     * @return Create a signed JWT token for device registration.
     */
    private String createRegisterJWT() {
        // Make the JWT valid for 5 minutes.
        long now = System.currentTimeMillis();
        long expiration = now + TimeUnit.MINUTES.toMillis(5);

        // Create SSA payload
        IAMSoftwareStatementProximus softwareStatement = new IAMSoftwareStatementProximus.Builder()
                .issued(now)
                .expiration(expiration)
                .softwareVersion(mMwProxy.getAppVersion())
                .redirectUrl(SessionProximus.iamRedirectUrl.get("") + "/")
                .scope(SessionProximus.iamScopes.get(""))
                .build();

        // Create software statement token.
        String ssaKeyId = SessionProximus.iamSSAKeyId.get("");
        PrivateKey ssaPrivateKey = IAMSigningKeyProvider.getPrivateKey(ssaKeyId);
        String stToken =  Jwts.builder()
                .setHeaderParam(PAYLOAD_HEADER_TYPE_KEY, PAYLOAD_HEADER_TYPE_VALUE)
                .setHeaderParam(PAYLOAD_HEADER_ALGORITHM_KEY, PAYLOAD_HEADER_ALGORITHM_VALUE)
                .setHeaderParam(PAYLOAD_HEADER_KEY_ID, ssaKeyId)
                .setPayload(GsonUtils.getGson().toJson(softwareStatement))
                .signWith(ssaPrivateKey, SignatureAlgorithm.ES256)
                .compact();

        // Get the signing key id from the boot config.
        String jwtKeyId = SessionProximus.iamJWTKeyId.get("");
        PrivateKey jwtPrivateKey = IAMSigningKeyProvider.getPrivateKey(jwtKeyId);

        // Create JWT payload.
        IAMRegisterPayloadProximus payload = new IAMRegisterPayloadProximus.Builder()
                .audience(SessionProximus.iamAudience.get(""))
                .expiration(expiration)
                .issued(now)
                .hardwareSerial(mMwProxy.getHardwareSerialNumber())
                .macAddress(mMwProxy.getEthernetMacAddress())
                .redirectUrl(SessionProximus.iamRedirectUrl.get("") + "/")
                .scope(SessionProximus.iamScopes.get(""))
                .hardwareOsVersion(mMwProxy.getMWVersion())
                .hardwareModelNumber(mMwProxy.getHardwareVersion())
                .softwareStatement(stToken)
                .build();

        // Create a signed JWT with the payload.
        return Jwts.builder()
                .setHeaderParam(PAYLOAD_HEADER_TYPE_KEY, PAYLOAD_HEADER_TYPE_VALUE)
                .setHeaderParam(PAYLOAD_HEADER_ALGORITHM_KEY, PAYLOAD_HEADER_ALGORITHM_VALUE)
                .setHeaderParam(PAYLOAD_HEADER_KEY_ID, jwtKeyId)
                .setPayload(GsonUtils.getGson().toJson(payload))
                .signWith(jwtPrivateKey, SignatureAlgorithm.ES256)
                .compact();
    }


    /**
     * Interface to force authentication on certain calls.
     */
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface MustAuthenticate {
    }
}
