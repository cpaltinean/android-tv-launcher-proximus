package tv.threess.threeready.data.proximus.log.utils;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.model.BaseInfo;

import java.io.File;

/**
 * Parse and the `/proc/memory` file and return it.
 *
 * @author Karetka Mezei Zoltan
 * @since 2019.02.22
 */
public class MemInfo extends BaseInfo<MemInfo.Field> {

    private MemInfo() {
        super(Field.class);
    }

    /**
     * Insert a field and it's value in bytes into the map.
     *
     * based on https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s2-proc-meminfo
     * the suffix is always kB and the multiplier is 1024.
     */
    @Override
    protected void put(Field field, String value) {
        try {
            super.put(field, kibiBytes(value));
        } catch (Exception e) {
            Log.e(TAG, "error converting value: " + value, e);
        }
    }

    /**
     * Read memory status from /proc/memory and return it.
     */
    public static MemInfo read() {
        MemInfo result = new MemInfo();
        result.readFrom(new File("/proc/meminfo"));
        return result;
    }

    public enum Field implements BaseInfo.Field {
        MemTotal,
        MemFree,
        Buffers,
        Cached,
        Active,
        Inactive,
        SwapTotal,
        SwapFree,
        Shmem;

        Field(String key) {
            mKey = key;
        }
        Field() {
            mKey = name();
        }
        final String mKey;

        @Override
        public String getKey() {
            return mKey;
        }
    }
}
