package tv.threess.threeready.data.proximus.tv.model.broadcast.response;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.api.generic.helper.TimerLog;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;
import tv.threess.threeready.data.proximus.tv.adapter.EPGKeyIndexMapAdapter;

/**
 * Proximus EPG backend response.
 * The response contains the program and airing schedule info.
 *
 * The program- and schedule- information is provided in two elements respectively ProgramData and ScheduleData, each of
 * which contains a list of “lines”, i.e. character strings ending with a pipe symbol plus a new line. Each of these
 * strings is a sequential list of attributes separated by pipe symbols.
 *
 * @author Barabas Attila
 * @since 2019.10.26
 */
public class BTAAiringsProximus extends BTAResponseProximus {
    private static final String TAG = Log.tag(BTAAiringsProximus.class);

    @XmlElement(name = "AiringList2")
    private AiringList mAiringList;

    public List<TvBroadcastProximus> getBroadcasts() throws IOException {
        return getBroadcasts(0, Long.MAX_VALUE);
    }

    /**
     * Parse the BTA CDATA field as broadcasts list
     * and filter them based on the request time range.
     *
     * @param from The time stamp from which the
     * @param to The time from where the broadcast should be returned.
     * @return The time until when the broadcast should bre returned.
     * @throws IOException In case of errors.
     */
    public List<TvBroadcastProximus> getBroadcasts(long from, long to) throws IOException {
        TimerLog timerLog = new TimerLog(TAG);

        List<TvBroadcastProximus> broadcasts = new ArrayList<>();

        Map<String, MetaInfoProximus<ProgramMetaInfoKeyProximus>> programMetaInfoMap = new HashMap<>();

        // Parse program info list
        BufferedReader programMetaInfoReader = new BufferedReader(
                new StringReader(mAiringList.mSchedule.mProgramData));

        String metaInfoString;
        while ((metaInfoString = programMetaInfoReader.readLine()) != null) {
            String[] programMetaInfoArray = metaInfoString.split(MetaInfoProximus.META_INFO_SEPARATOR);
            MetaInfoProximus<ProgramMetaInfoKeyProximus> programMetaInfo = new MetaInfoProximus<>(
                    mAiringList.mMeta.mProgram, programMetaInfoArray);

            // Map programs for lookup.
            programMetaInfoMap.put(programMetaInfo.getString(
                    ProgramMetaInfoKeyProximus.ProgramReferenceNo), programMetaInfo);
        }

        // Parse broadcast list.
        BufferedReader broadcastMetaInfoReader = new BufferedReader(
                new StringReader(mAiringList.mSchedule.mScheduleData));

        String broadcastMetaInfoString;
        while ((broadcastMetaInfoString = broadcastMetaInfoReader.readLine()) != null) {

            // Parse broadcast schedule info.
            String[] broadcastInfoArray = broadcastMetaInfoString.split(MetaInfoProximus.META_INFO_SEPARATOR);
            MetaInfoProximus<BroadcastMetaInfoKeyProximus> broadcastMetaInfo = new MetaInfoProximus<>(
                    mAiringList.mMeta.mSchedule, broadcastInfoArray);

            // Get program for broadcast.
            MetaInfoProximus<ProgramMetaInfoKeyProximus> programMetaInfo = programMetaInfoMap.get(
                    broadcastMetaInfo.getString(BroadcastMetaInfoKeyProximus.ProgramReferenceNo));
            programMetaInfo = programMetaInfo == null ? new MetaInfoProximus<>() : programMetaInfo;

            // Filter broadcast broadcast outside of the requested range.
            long broadcastStart = broadcastMetaInfo.getLong(BroadcastMetaInfoKeyProximus.ProgramScheduleStart);
            long broadcastEnd =  broadcastStart + TimeUnit.MINUTES.toMillis(
                    broadcastMetaInfo.getInt(BroadcastMetaInfoKeyProximus.Duration));
            if (broadcastEnd > from && broadcastStart < to) {
                broadcasts.add(new TvBroadcastProximus(programMetaInfo, broadcastMetaInfo));
            }
        }

        timerLog.d("BTA airings converted. Doc. id : " + getId()
                + ", Programs : " + programMetaInfoMap.size()
                + ", broadcast : " + broadcasts.size());

        return broadcasts;
    }

    public long getId() {
        return mAiringList.mParentDocumentUpdate.mVersion;
    }


    @XmlRootElement(name = "AiringList2")
    private static class AiringList {

        @XmlElement(name = "ParentDocumentUpdate")
        private ParentDocumentUpdate mParentDocumentUpdate;

        @XmlElement(name = "Meta")
        private Meta mMeta;

        @XmlElement(name = "ProgramScheduleData")
        private ProgramScheduleData mSchedule;
    }

    @XmlRootElement(name = "Meta")
    private static class Meta {

        @XmlJavaTypeAdapter(EPGKeyIndexMapAdapter.class)
        @XmlElement(name = "Program")
        private Map<String, Integer> mProgram;

        @XmlJavaTypeAdapter(EPGKeyIndexMapAdapter.class)
        @XmlElement(name = "Schedule")
        private Map<String, Integer> mSchedule;
    }

    @XmlRootElement(name = "ProgramScheduleData")
    private static class ProgramScheduleData {

        @JacksonXmlCData()
        @XmlElement(name = "ProgramData")
        private String mProgramData;

        @JacksonXmlCData()
        @XmlElement(name = "ScheduleData")
        private String mScheduleData;
    }

    @XmlRootElement(name = "ParentDocumentUpdate")
    private static class ParentDocumentUpdate {

        @XmlAttribute(name = "Version")
        private long mVersion;

    }
}
