package tv.threess.threeready.data.proximus.account.model.server;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import okhttp3.HttpUrl;

/**
 * Base class for proximus server related information.
 *
 * @author Zsolt Bokor_
 * @since 2020.05.27
 */
@XmlRootElement(name = "Server")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServerInfoProximus implements Serializable {
    private static final String PROTOCOL = "Protocol";
    private static final String PORT = "Port";
    private static final String PATH = "Path";
    private static final String HTTP = "http";

    @XmlElement(name = "Name")
    private String mName;

    @XmlElement(name = "Type")
    @XmlJavaTypeAdapter(ServerTypeAdapter.class)
    private ServerType mType;

    @XmlElement(name = "Address")
    private String mAddress;

    @XmlElement(name = "Attribute")
    protected List<ServerAttributeProximus> mAttributes;

    @XmlElement(name = "AllowedSize")
    protected List<PosterServerAllowedSizeProximus> mAllowedSizes;

    public ServerInfoProximus() {
    }

    public ServerInfoProximus(ServerInfoProximus serverInfoProximus) {
        mName = serverInfoProximus.mName;
        mType = serverInfoProximus.mType;
        mAddress = serverInfoProximus.mAddress;
        mAttributes = serverInfoProximus.mAttributes;
        mAllowedSizes = serverInfoProximus.mAllowedSizes;
    }

    /**
     * @return Server name.
     */
    public String getName() {
        return mName;
    }

    /**
     * @return Server type.
     */
    public ServerType getType() {
        return mType;
    }

    /**
     * @return Server`s address.
     */
    public String getAddress() {
        return mAddress;
    }

    /**
     * @return Server protocol from attributes.
     */
    public String getProtocol() {
        return getAttributeValueForName(PROTOCOL, HTTP);
    }

    /**
     * @return Server`s port get from attributes.
     */
    public int getServerPort() throws NumberFormatException {
        return Integer.parseInt(getAttributeValueForName(PORT));
    }

    /**
     * @return Server`s path get from attributes.
     */
    public String getServerPath() {
        return getAttributeValueForName(PATH);
    }

    /**
     * @return The server`s default url built from server address, port and path.
     */
    public String getDefaultUrl() throws NumberFormatException {
        // Avoid duplicated segment separator.
        String path = getServerPath();
        if (path.startsWith("/")) {
            path = path.replaceFirst("/", "");
        }

        return new HttpUrl.Builder()
                .scheme(getProtocol())
                .host(getAddress())
                .port(getServerPort())
                .addEncodedPathSegments(path)
                .toString();
    }

    /**
     * @return Attribute with the given name.
     */
    protected String getAttributeValueForName(String name, String defaultValue) {
        for (ServerAttributeProximus attributeProximus : mAttributes) {
            if (attributeProximus.getName().contains(name)) {
                return attributeProximus.getValue();
            }
        }
        return defaultValue;
    }

    protected String getAttributeValueForName(String name) {
        return getAttributeValueForName(name, "");
    }

    protected long getAttributeValueForName(String name, long defaultValue) throws NumberFormatException {
        String value = getAttributeValueForName(name);
        if (TextUtils.isEmpty(value)) {
            return defaultValue;
        }
        return Long.parseLong(value);
    }
}