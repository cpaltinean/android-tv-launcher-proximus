package tv.threess.threeready.data.proximus.account.model.account;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Body used for updating Account Information.
 *
 * Created by Daniela Toma on 7.04.2020.
 */
@XmlRootElement(name = "BTA")
public class UpdateAccountInfoBodyProximus implements Serializable {

    @XmlElement(name = "UpdateAccountInfoDoc")
    private final AccountInfoUpdateRequestProximus mAccountInfo;

    public UpdateAccountInfoBodyProximus(AccountInfoUpdateRequestProximus accountInfoUpdateBody) {
        this.mAccountInfo = accountInfoUpdateBody;
    }
}
