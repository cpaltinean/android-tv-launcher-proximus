package tv.threess.threeready.data.proximus.account.model.boot;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class which holds a TM server config attributes.
 *
 * @author Barabas Attila
 * @since 2020.04.07
 */
@XmlRootElement(name = "Attribute")
public class TMServerAttributeProximus implements Serializable {

    @XmlElement(name = "Name")
    private String mName;

    @XmlElement(name = "Value")
    private String mValue;

    /**
     * @return The name of the attribute.
     */
    public String getName() {
        return mName;
    }

    /**
     * @return The value of the attribute.
     */
    public String getValue() {
        return mValue;
    }

    /**
     * The known attributes by the application.
     */
    public enum Key {
        BrokerCDNPort("BrokerCDNPort"),
        BrokerPublishingPath("BrokerPublishingPath"),
        BrokerDownloadProtocol("BrokerDownloadProtocol"),
        ServerProtocol("ServerProtocol"),
        ServerPort("ServerPortNumber"),
        ServerSecuredPort("ServerSecurePortNumber"),
        ServerPath("ServerPath");

        private final String mName;

        Key(String name) {
            mName = name;
        }

        public String getName() {
            return mName;
        }
    }
}