/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.proximus.tv.model.broadcast.response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import tv.threess.threeready.api.tv.model.ITvBroadcastResponse;

/**
 * Entity class representing a Tv broadcast for Proximus.
 *
 * @author Barabas Attila
 * @since 2018.06.07
 */
public class TvBroadcastResponseProximus implements ITvBroadcastResponse<TvBroadcastProximus> {
    private final List<TvBroadcastProximus> mBroadcasts;

    public TvBroadcastResponseProximus(List<BTAAiringsProximus> airingsList, long from, long to) throws IOException {
        // Tree set to sort by start time and avoid duplicates.
        HashMap<String, TvBroadcastProximus> broadcastMap = new HashMap<>();
        for (BTAAiringsProximus airings : airingsList) {
            List<TvBroadcastProximus> broadcasts = airings.getBroadcasts(from, to);
            for (TvBroadcastProximus broadcast : broadcasts) {
                broadcastMap.put(broadcast.getId(), broadcast);
            }
        }

        mBroadcasts = new ArrayList<>(broadcastMap.values());
        mBroadcasts.sort(Comparator.comparingLong(TvBroadcastProximus::getStart));
    }

    @Override
    public List<TvBroadcastProximus> getBroadcasts() {
        return mBroadcasts;
    }
}
