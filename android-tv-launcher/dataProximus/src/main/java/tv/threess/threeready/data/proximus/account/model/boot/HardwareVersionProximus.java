package tv.threess.threeready.data.proximus.account.model.boot;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

public class HardwareVersionProximus implements Serializable {
    @XmlElement(name = "Version")
    private String mVersion;

    @XmlElement(name = "CPVRCapable")
    private boolean mCPVRCapable;

    @XmlElement(name = "ClientAuthenticationModel")
    private String mClientAuthenticationModel;

    @XmlElement(name = "ClientProtocol")
    private String mClientProtocol;

    @XmlElement(name = "STBSharedSecretConf")
    private STBSharedSecretConf mSTBSharedSecretConf;

    @XmlElement(name = "TSTVBufferSize")
    private int mTSTVBufferSize;

    @XmlElement(name = "HDCapable")
    private boolean mHDCapable;

    @XmlElementWrapper(name = "ResourceList")
    @XmlElement(name = "Resource")
    private List<Resource> mResources;

    @XmlElement(name = "TADTVEnabled")
    private boolean mTADTVEnabled;

    @XmlElement(name = "TAStorageSize")
    private int mTAStorageSize;

    @XmlElement(name = "TAPreDownloadPercentage")
    private int mTAPreDownloadPercentage;

    public String getVersion() {
        return mVersion;
    }

    public boolean isCPVRCapable() {
        return mCPVRCapable;
    }

    public String getClientAuthenticationModel() {
        return mClientAuthenticationModel;
    }

    public String getClientProtocol() {
        return mClientProtocol;
    }

    public int getSTBSharedSecretExpiration() {
        return mSTBSharedSecretConf.mSTBSharedSecretExpiration;
    }

    public int getSTBSharedSecretRefresh() {
        return mSTBSharedSecretConf.mSTBSharedSecretRefresh;
    }

    public int getTSTVBufferSize() {
        return mTSTVBufferSize;
    }

    public boolean isHDCapable() {
        return mHDCapable;
    }

    public List<Resource> getResources() {
        return mResources;
    }

    public boolean isTADTVEnabled() {
        return mTADTVEnabled;
    }

    public int getTAStorageSize() {
        return mTAStorageSize;
    }

    public int getTAPreDownloadPercentage() {
        return mTAPreDownloadPercentage;
    }

    @XmlRootElement(name = "Resource")
    public static class Resource implements Serializable {
        @XmlAttribute(name = "name")
        String mName;

        @XmlAttribute(name = "amount")
        String mAmount;

        @XmlAttribute(name = "unit")
        String mUnit;
    }

    @XmlRootElement(name = "STBSharedSecretConf")
    public static class STBSharedSecretConf implements Serializable {
        @XmlElement(name = "STBSharedSecretExpiration")
        private int mSTBSharedSecretExpiration;

        @XmlElement(name = "STBSharedSecretRefresh")
        private int mSTBSharedSecretRefresh;
    }
}
