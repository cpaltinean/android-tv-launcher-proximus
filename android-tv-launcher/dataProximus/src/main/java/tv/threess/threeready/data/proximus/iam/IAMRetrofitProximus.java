package tv.threess.threeready.data.proximus.iam;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;
import tv.threess.threeready.data.proximus.generic.interceptor.BackendIsolationInterceptor;
import tv.threess.threeready.data.proximus.iam.model.IAMAccessTokenResponseProximus;
import tv.threess.threeready.data.proximus.iam.model.IAMRegisterResponseProximus;

/**
 * Retrofit interface for client registration and toke access.
 *
 * @author Barabas Attila
 * @since 2020.03.19
 */
public interface IAMRetrofitProximus {

    @POST
    @BackendIsolationInterceptor.Allow
    @Headers("Content-Type: application/jwt")
    Call<IAMRegisterResponseProximus> register(@Url String url,
                                               @Body RequestBody jwtToken);

    @POST
    @BackendIsolationInterceptor.Allow
    @FormUrlEncoded
    @Headers("Content-Type: application/x-www-form-urlencoded")
    Call<IAMAccessTokenResponseProximus> accessToken(@Url String url,
                                                     @Header("Authorization") String auth,
                                                     @Field("grant_type") String grantType,
                                                     @Field("scope") String scope);
}
