package tv.threess.threeready.data.proximus.home.model.editorial;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.data.proximus.home.model.module.RecordingCollectionModuleProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Swimlane editorial tile which opens a collection with the recordings.
 *
 * @author Barabas Attila
 * @since 6/18/21
 */
public class GoRecordingsEditorialItemProximus extends SwimlaneEditorialItemProximus {
    private final ModuleConfig mModuleConfig = new RecordingCollectionModuleProximus();

    public GoRecordingsEditorialItemProximus(SwimLaneProximus swimLane,
                                             SwimLaneProximus.SwimlaneTile swimlaneTile, Integer position) {
        super(swimLane, swimlaneTile, position);
    }

    @Nullable
    @Override
    public EditorialItemAction getAction() {
        return mItemAction;
    }

    private final EditorialItemAction mItemAction =
            (EditorialItemAction.ModuleEditorialItemAction) () -> mModuleConfig;
}
