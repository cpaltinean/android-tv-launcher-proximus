/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.proximus.home.model.swimalane;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.data.proximus.tv.adapter.EPGKeyIndexMapAdapter;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.MetaInfoProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.ProgramMetaInfoKeyProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategoryContentResponseProximus;
import tv.threess.threeready.data.proximus.vod.model.response.BaseVodItemProximus;

/**
 * BTA response for a swim lane content request. It contains the broadcasts and VODs of the a swim lane.
 *
 * @author Barabas Attila
 * @since 2019.10.28
 */
public class SwimLaneContentResponseProximus extends VodCategoryContentResponseProximus {

    @XmlElementWrapper(name = "ProgramList")
    @XmlElement(name = "Program")
    private ArrayList<SwimLaneProgramProximus> mPrograms;

    @JacksonXmlProperty(localName = "Meta")
    private Meta mMeta;

    private List<IBaseContentItem> mContentItems;

    /**
     * @return A mixed list with all the VODs and broadcasts from the swim lane.
     * @throws IOException In case of errors.
     */
    public List<IBaseContentItem> getContentItems() throws IOException {
        if (mContentItems == null) {
            mContentItems = new ArrayList<>();

            if (mDisplayItems == null || mDisplayItems.getItems() == null) {
                return Collections.emptyList();
            }

            //get the vods
            List<BaseVodItemProximus> vods = getVODs();
            //get the broadcasts
            List<SwimLaneBroadcastProximus> broadcastProximusList = getBroadcasts();

            //Make map<String, ContentItem> for data
            Map<String, BaseVodItemProximus> vodMap = new HashMap<>();
            Map<String, SwimLaneBroadcastProximus> broadcastMap = new HashMap<>();

            //Put vodId and vod into a map to avoid multiple for loop at read
            for (BaseVodItemProximus vod : vods) {
                for (String variantId : vod.getVariantIds()) {
                    vodMap.put(variantId, vod);
                }
            }

            //Put programId and program into a map to avoid multiple for loop at read
            for (SwimLaneBroadcastProximus program : broadcastProximusList) {
                broadcastMap.put(program.getProgramId(), program);
            }

            //Add items(ex. broadcast, VOD) in one list by reference of the elements of the DisplayItems
            for (DisplayItem item : mDisplayItems.getItems()) {
                //Check movie references
                for (MovieRef ref : item.getMovieReferences()) {
                    BaseVodItemProximus vod = null;
                    //check the movie has reference or external reference id
                    if (ref.getReference() != null) {
                        vod = vodMap.get(ref.getReference());
                    } else if (ref.getExternalReference() != null) {
                        vod = vodMap.get(ref.getExternalReference());
                    }

                    if (vod != null) {
                        mContentItems.add(vod);
                        break;
                    }
                }

                //Check program references
                for (ProgramRef ref : item.getProgramReferences()) {
                    SwimLaneBroadcastProximus program = null;
                    //Check if the reference in the ProgramRef object is not null
                    if (ref.getReference() != null) {
                        program = broadcastMap.get(ref.getReference());
                    }

                    if (program != null) {
                        mContentItems.add(program);
                        break;
                    }
                }
            }
        }
        return mContentItems;
    }

    /**
     * @return A list with all the broadcasts from the swim lane.
     * @throws IOException In case of errors.
     */
    public List<SwimLaneBroadcastProximus> getBroadcasts() throws IOException {
        if (mPrograms == null) {
            return Collections.emptyList();
        }

        List<SwimLaneBroadcastProximus> broadcasts = new ArrayList<>();

        for (SwimLaneProgramProximus program : mPrograms) {
            // Read program meta info.
            BufferedReader programMetaInfoReader = new BufferedReader(
                    new StringReader(program.getProgramMetaInfo()));

            String metaInfoString;
            while ((metaInfoString = programMetaInfoReader.readLine()) != null) {
                // Prepare program meta info.
                String[] programMetaInfoArray = metaInfoString.split(MetaInfoProximus.META_INFO_SEPARATOR);
                MetaInfoProximus<ProgramMetaInfoKeyProximus> programMetaInfo = new MetaInfoProximus<>(
                        mMeta.mProgram, programMetaInfoArray);

                // Append broadcast with meta info
                SwimLaneBroadcastProximus broadcast = program.getBroadcast();
                if (broadcast != null) {
                    broadcast.setMetaInfo(programMetaInfo);
                    broadcasts.add(broadcast);
                }
            }
        }

        return broadcasts;
    }

    public List<String> getCategoryIds() {
        List<String> categoryIds = new ArrayList<>();
        for (DisplayItem displayItem : ArrayUtils.notNull(mDisplayItems.getItems())) {
            for (CategoryRef categoryRef : ArrayUtils.notNull(displayItem.getCategoryReferences())) {
                categoryIds.add(categoryRef.getRef());
            }
        }
        return categoryIds;
    }

    public List<String> getChannelIds() {
        List<String> categoryIds = new ArrayList<>();
        for (DisplayItem displayItem : ArrayUtils.notNull(mDisplayItems.getItems())) {
            for (ChannelRef channelRef : ArrayUtils.notNull(displayItem.getChannelReferences())) {
                categoryIds.add(channelRef.getRef());
            }
        }
        return categoryIds;
    }

    public List<BannerProximus> getBanners() {
        List<BannerProximus> banners = new ArrayList<>();
        for (DisplayItem displayItem : ArrayUtils.notNull(mDisplayItems.getItems())) {
            banners.addAll(displayItem.getBanners());
        }
        return banners;
    }

    @XmlRootElement(name = "Meta")
    private static class Meta {

        @XmlJavaTypeAdapter(EPGKeyIndexMapAdapter.class)
        @XmlElement(name = "Program")
        private Map<String, Integer> mProgram;
    }
}
