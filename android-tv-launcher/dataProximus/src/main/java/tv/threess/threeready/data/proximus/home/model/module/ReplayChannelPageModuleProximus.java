package tv.threess.threeready.data.proximus.home.model.module;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.tv.model.TvChannel;

/**
 * Interface used for the common methods used in replay channel stripe and collection.
 *
 * @author David Bondor
 * @since 4.28.2022
 */
public interface ReplayChannelPageModuleProximus {

    /**
     * @return The page configuration which will include the channel`s name and id.
     */
    @Nullable
    PageConfig getPageConfigForChannel(TvChannel channel);
}