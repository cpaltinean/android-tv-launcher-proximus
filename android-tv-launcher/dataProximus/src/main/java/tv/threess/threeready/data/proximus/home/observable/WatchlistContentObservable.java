package tv.threess.threeready.data.proximus.home.observable;

import android.content.Context;
import android.net.Uri;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.model.IWatchlistItem;
import tv.threess.threeready.api.generic.model.WatchlistType;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IRentalExpiration;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.data.account.AccountLocalCache;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.home.observable.BaseCategoryContentObservable;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.proximus.tv.TvProxyProximus;
import tv.threess.threeready.data.proximus.vod.VodProxyProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodItemProximus;
import tv.threess.threeready.data.pvr.PvrCache;
import tv.threess.threeready.data.vod.VodCache;

/**
 * Content observable for watchlist swimlane
 *
 * @author Andor Lukacs
 * @since 04/11/2020
 */
public class WatchlistContentObservable extends BaseCategoryContentObservable<IBaseContentItem> {
    private static final String TAG = Log.tag(WatchlistContentObservable.class);

    private final VodProxyProximus mVodProxy = Components.get(VodProxyProximus.class);
    private final TvProxyProximus mTvProxy = Components.get(TvProxyProximus.class);

    private final VodCache mVodCache = Components.get(VodCache.class);
    private final PvrCache mPvrCache = Components.get(PvrCache.class);
    private final AccountLocalCache mAccountCache = Components.get(AccountLocalCache.class);

    public WatchlistContentObservable(Context context, ModuleConfig moduleConfig, int start, int count) {
        super(context, moduleConfig, start, count);
    }

    @Override
    public void subscribe(ObservableEmitter<ModuleData<IBaseContentItem>> emitter) throws Exception {
        super.subscribe(emitter);
        registerObserver(ConfigContract.Watchlist.CONTENT_URI);
    }

    @Override
    protected void loadItemList(ObservableEmitter<ModuleData<IBaseContentItem>> emitter) throws Exception {
        List<IWatchlistItem> watchlistItems = mAccountCache.getWatchlistItems(mModuleConfig.getDataSource(), mStart, mCount);

        ModuleData<IBaseContentItem> contentItem = new ModuleData<>(mModuleConfig, new ArrayList<>());

        Map<String, IVodItem> vodMap = getVodIdMap(getVod(getVodIds(watchlistItems)));
        Map<String, IBroadcast> broadcastMap = getBroadcastMap(
                mTvProxy.getBroadcasts(
                        getBroadcastIds(watchlistItems),
                        TimeUtils.getEPGWindowBorder(-Settings.epgPastWindowLength.get(TimeUnit.DAYS.toMillis(7))),
                        TimeUtils.getEPGWindowBorder(Settings.epgFutureWindowLength.get(TimeUnit.DAYS.toMillis(7)))
                ));

        for (IWatchlistItem watchlistItem : watchlistItems) {
            switch (watchlistItem.getType()) {
                case Vod:
                    IBaseVodItem vodItem = vodMap.get(watchlistItem.getContentId());
                    Log.d(TAG, "loadItemList() called with: vodItem = [" + vodItem + "]");
                    if (vodItem != null) {
                        contentItem.getItems().add(vodItem);
                    }
                    break;
                case Broadcast:
                    IContentItem item = broadcastMap.get(watchlistItem.getContentId());
                    if (item == null) {
                        item = mPvrCache.getRecording(watchlistItem.getContentId());
                    }
                    Log.d(TAG, "loadItemList() called with: item = [" + item + "]");
                    if (item != null) {
                        contentItem.getItems().add(item);
                    }
                    break;
                default:
                    break;
            }
        }

        mCategoryData = contentItem;

        onItemListLoaded(emitter);
    }

    /**
     * Request multiple VODs in a batch.
     *
     * @param ids The id of the VODs which needs to be returned.
     * @return A list with the requested VODs
     */
    private List<IVodItem> getVod(List<String> ids) throws IOException {
        Map<String, IRentalExpiration> rentalExpirationMap = mVodCache.getRentalExpiration();
        double priceLimit = SettingsProximus.freeRentalLimit.get(0d);
        int freeRentals = SettingsProximus.freeRentals.get(0);

        List<IVodItem> vodList = new ArrayList<>();
        for (VodItemProximus item : mVodProxy.getVod(ids)) {
            item.setRentalExpirationInfo(rentalExpirationMap);
            item.updateCheapestPrice(priceLimit, freeRentals);
            vodList.add(item);
        }

        return vodList;
    }

    private List<String> getVodIds(List<IWatchlistItem> watchlistItems) {
        List<String> vodIDs = new ArrayList<>();
        for (IWatchlistItem watchlistItem : watchlistItems) {
            if (watchlistItem.getType() == WatchlistType.Vod) {
                vodIDs.add(watchlistItem.getContentId());
            }
        }
        return vodIDs;
    }

    private Map<String, IVodItem> getVodIdMap(List<IVodItem> vodItems) {
        Map<String, IVodItem> vodMap = new HashMap<>();
        for (IVodItem vodItem : vodItems) {
            vodMap.put(vodItem.getId(), vodItem);
        }
        return vodMap;
    }

    private List<String> getBroadcastIds(List<IWatchlistItem> watchlistItems) {
        List<String> programIds = new ArrayList<>();
        for (IWatchlistItem watchlistItem : watchlistItems) {
            if (watchlistItem.getType() == WatchlistType.Broadcast) {
                programIds.add(watchlistItem.getContentId());
            }
        }
        return programIds;
    }

    private Map<String, IBroadcast> getBroadcastMap(List<? extends IBroadcast> broadcasts) {
        Map<String, IBroadcast> broadcastMap = new HashMap<>();
        for (IBroadcast broadcast : broadcasts) {
            broadcastMap.put(broadcast.getId(), broadcast);
        }
        return broadcastMap;
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<ModuleData<IBaseContentItem>> observableEmitter) throws Exception {
        loadItemList(observableEmitter);
    }
}
