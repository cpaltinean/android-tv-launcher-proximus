package tv.threess.threeready.data.proximus.account.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Retrofit interface to get the 3Ready config from google cloud.
 * Calls trough this interface are ignored by Managed network checker.
 *
 * @author Barabas Attila
 * @since 2019.05.21
 */
public interface ConfigJsonRetrofitProximus {

    String BASE_URI = "https://cc7.streaming.proximustv.be";

    /**
     * Download the 3Ready config from the server.
     * @param language The language for which the config should be returned
     * @param appVersion The version name of the application for which the config should be returned.
     * @return The server response and body.
     */
    @GET("{language}/{appVersion}/config.json")
    Call<ResponseBody> getConfig(@Path("language") String language, @Path("appVersion") String appVersion);

    /**
     * Download the translations from the server.
     * @param language The language for which the file should be returned
     * @param appVersion The version name of the application for which the file should be returned.
     * @return The server response and body.
     */
    @GET("{language}/{appVersion}/strings.json")
    Call<ResponseBody> getTranslations(@Path("language") String language, @Path("appVersion") String appVersion);
}
