/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.log.reporter;

import android.content.Context;

import androidx.annotation.Nullable;

import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.Reporter;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.PlaybackEventDetails;
import tv.threess.threeready.api.log.model.PlaybackLogEvent;
import tv.threess.threeready.api.log.model.ReporterInfo;
import tv.threess.threeready.api.playback.model.TrackInfo;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.proximus.account.SessionProximus;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.proximus.generic.RetrofitServicesProximus;
import tv.threess.threeready.data.proximus.log.model.LogDetailsProximus;
import tv.threess.threeready.data.proximus.log.retrofit.DataCollectionRetrofitProximus;
import tv.threess.threeready.data.proximus.tv.model.BaseBroadcastProximus;
import tv.threess.threeready.data.utils.GsonUtils;

/**
 * Specific playback event batch handler for Proximus
 *
 * @author Arnold Szabo
 * @since 2017.07.26
 */
public abstract class BaseReporterProximus<E extends Enum<E> & Event.Kind, D extends Enum<D> & Event.Detail<E>>
        extends Reporter<E, D> {
    private static final String TAG = Log.tag(BaseReporterProximus.class);

    private static final Type TYPE_MAP_STRING_STRING = new TypeToken<Map<String, String>>() {}.getType();

    protected static final String OK = "ok";

    protected static final long EVENT_COUNTER_RESET = 1000000;
    protected static final String NONE = "NONE";

    // AGENT SPECIFIC PART
    protected static final String KEY_CATEGORY = "category";
    private static final String KEY_MAC_OLD = "mac";
    private static final String KEY_MAC = "mac_address";
    private static final String KEY_HW_TYPE = "hw_type";
    private static final String KEY_OPERATION_SYSTEM_VERSION = "operation_system_version";
    private static final String KEY_CLIENT_SOFTWARE_VERSION_MW = "client_software_version_MW";
    private static final String KEY_CLIENT_SOFTWARE_VERSION_APP = "client_software_version_APP";
    private static final String KEY_HOUSEHOLD_IDENTIFIER = "household_identifier";
    private static final String KEY_COMPANY_LOCATION_NAME = "company_location_name";
    private static final String KEY_COMPANY_SITE = "company_site";
    private static final String KEY_IP_ADDRESS = "ip_address";
    private static final String KEY_DEVICE_NAME = "device_name";
    private static final String KEY_OAUTH2_CLIENT_ID = "oauth2_client_id";
    private static final String KEY_LANGUAGE_AUDIO = "language_audio";
    private static final String KEY_LANGUAGE_SUBTITLES = "language_subtitles";
    private static final String KEY_LANGUAGE_INTERFACE = "language_interface";
    private static final String KEY_LANGUAGE_SHOP = "language_shop";
    private static final String KEY_RECOMMENDATION_FLAG = "recommendations_flag";
    private static final String KEY_TARGETED_ADVERTISING_FLAG = "targeted_advertising_flag";
    private static final String KEY_STB_PROFILE = "STB_profile";
    private static final String KEY_SUBSCRIBER_TYPE = "subscriber_type";

    private static final String KEY_EVENT_COUNTER_OLD = "eventcounter";
    private static final String KEY_EVENT_COUNTER = "event_counter";

    protected static final String KEY_REMOTE_CONNECTED = "remote_connected";

    private static final String LANGUAGE_CODE_ENGLISH = "EN";
    private static final String LANGUAGE_CODE_DUTCH = "NL";
    private static final String LANGUAGE_CODE_FRENCH = "FR";
    private static final String LANGUAGE_CODE_GERMAN = "GR";
    private static final String LANGUAGE_CODE_DUTCH_AND_FRENCH = "NL_FR";
    private static final String SUBTITLE_OFF_CODE = "OFF";

    protected static final String VALUE_NO_ID = "-1";
    protected static final String VALUE_START = "START";
    protected static final String VALUE_STOP = "STOP";
    protected static final String VALUE_TRUE = "TRUE";
    protected static final String VALUE_FALSE = "FALSE";

    protected static final String VALUE_IGMP = "IGMP";
    protected static final String VALUE_HTTP = "HTTP";
    protected static final String VALUE_FIXED = "FIXED";
    protected static final String VALUE_ADAPTIVE = "ADAPTIVE";
    protected static final String VALUE_UNICAST = "UNICAST";
    protected static final String VALUE_MULTICAST = "MULTICAST";

    protected static final String VALUE_LINEAR = "LINEAR";
    protected static final String VALUE_RADIO = "RADIO";
    protected static final String VALUE_NPVR = "NPVR";
    protected static final String VALUE_CUTV = "CUTV";
    protected static final String VALUE_VOD = "VOD";

    protected final MwProxy mMwProxy = Components.get(MwProxy.class);

    protected final DataCollectionRetrofitProximus mDataCollectionRetrofit =
            Components.get(RetrofitServicesProximus.class).getDataCollectionRetrofit();

    public BaseReporterProximus(Context context, ReporterInfo reporterInfo) {
        super(context, reporterInfo);
    }

    @Override
    public boolean canSend(Event<E, D> event) {
        return true;
    }

    @Nullable
    @Override
    public String serializeEvent(Event<E, D> event) {
        LogDetailsProximus payload = formatPayload(event);
        if (payload == null) {
            android.util.Log.d(TAG, "Could not send event, no payload: " + event);
            return null;
        }

        return GsonUtils.getGson().toJson(payload);
    }

    /**
     * Returns a payload with all event details. We send this payload to the backend.
     *
     * @param event Payload is built on this event.
     * @return Payload which will be sent to the backend.
     */
    protected abstract LogDetailsProximus formatPayload(Event<E, D> event);

    /**
     * Returns a list with payloads. We send this list directly to the backend.
     *
     * @param events Payloads are built on these events.
     * @return A list of payloads which will be sent to the backend.
     */
    protected List<LogDetailsProximus> formatPayloadList(List<Event<E, D>> events) throws JsonSyntaxException {
        List<LogDetailsProximus> payloads = new ArrayList<>();
        for (Event<E, D> event : events) {
            if (event.getValue() == null) {
                continue;
            }
            LogDetailsProximus payload = GsonUtils.getGson().fromJson(event.getValue(), LogDetailsProximus.class);
            if (payload != null) {
                payloads.add(payload);
            }
        }
        return payloads;
    }

    /**
     * add the agent specific parameters to the json object
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Agent+Specific+Parts
     */
    protected void addAgentSpecificParts(LogDetailsProximus payload) {
        payload.add(KEY_MAC_OLD, mMwProxy.getEthernetMacAddress());
        payload.add(KEY_MAC, mMwProxy.getEthernetMacAddress());
        payload.add(KEY_HW_TYPE, mMwProxy.getHardwareType());
        payload.add(KEY_OPERATION_SYSTEM_VERSION, mMwProxy.getOperationSystemVersion());
        payload.add(KEY_CLIENT_SOFTWARE_VERSION_MW, mMwProxy.getMWVersion());
        payload.add(KEY_CLIENT_SOFTWARE_VERSION_APP, mMwProxy.getAppVersion());
        payload.add(KEY_HOUSEHOLD_IDENTIFIER, SettingsProximus.householdId.get(NONE));
        payload.add(KEY_COMPANY_LOCATION_NAME, SettingsProximus.companyLocationName.get(NONE));
        payload.add(KEY_COMPANY_SITE, SettingsProximus.companySite.get(NONE));
        payload.add(KEY_IP_ADDRESS, mMwProxy.getIpAddress());
        payload.add(KEY_DEVICE_NAME, Settings.deviceName.get(NONE));
        // We use custom oauth2 implementation, pass iam client id instead.
        payload.add(KEY_OAUTH2_CLIENT_ID, SessionProximus.iamClientId.get(NONE));

        TrackInfo audioLanguage = Settings.audioLanguage.getObject(TrackInfo.class, TrackInfo::fromLanguage);
        payload.add(KEY_LANGUAGE_AUDIO, formatLanguageCode(
                audioLanguage != null ? audioLanguage.getLanguage() : LocaleUtils.getApplicationLanguage()
        ));
        TrackInfo subtitlesLanguage = Settings.subtitlesLanguage.getObject(TrackInfo.class, TrackInfo::fromLanguage);
        payload.add(KEY_LANGUAGE_SUBTITLES, formatLanguageCode(
                subtitlesLanguage != null ? subtitlesLanguage.getLanguage() : SUBTITLE_OFF_CODE
        ));

        payload.add(KEY_LANGUAGE_INTERFACE, formatLanguageCode(LocaleUtils.getApplicationLanguage()));
        payload.add(KEY_LANGUAGE_SHOP, formatLanguageCode(LocaleUtils.getShopLanguage(SettingsProximus.shopLanguage.getList())));
        payload.add(KEY_STB_PROFILE, SettingsProximus.STBProfile.get(NONE));
        payload.add(KEY_SUBSCRIBER_TYPE, SettingsProximus.subscriberType.get(NONE));
        payload.add(KEY_RECOMMENDATION_FLAG, SettingsProximus.recommendationFlag.get(NONE));
        payload.add(KEY_TARGETED_ADVERTISING_FLAG, Settings.targetedAdvertisingFlag.get(NONE));
    }

    protected void addEventCounter(LogDetailsProximus payload) {
        long eventCounter = SettingsProximus.eventCounter.increment() % EVENT_COUNTER_RESET;
        payload.add(KEY_EVENT_COUNTER_OLD, eventCounter);
        payload.add(KEY_EVENT_COUNTER, eventCounter);
    }

    /**
     * Format the UI, subtitle, audio and shop language code in a format expected by the data collection service.
     *
     * @param languageCode The original language code which needs to be formatted.
     * @return The formatted language code which can be sent to the server.
     */
    private static String formatLanguageCode(String languageCode) {
        switch (languageCode) {
            case LocaleUtils.ENGLISH_LANGUAGE_CODE:
                return LANGUAGE_CODE_ENGLISH;
            case LocaleUtils.DUTCH_LANGUAGE_CODE:
                return LANGUAGE_CODE_DUTCH;
            case LocaleUtils.FRENCH_LANGUAGE_CODE:
                return LANGUAGE_CODE_FRENCH;
            case LocaleUtils.GERMAN_LANGUAGE_CODE:
                return LANGUAGE_CODE_GERMAN;
            case LocaleUtils.FRENCH_AND_DUTCH_SHOP_LANGUAGE:
                return LANGUAGE_CODE_DUTCH_AND_FRENCH;
            default:
                return languageCode;
        }
    }

    /**
     * Converts a string with headers to a map.
     */
    protected Map<String, String> getHeadersMap(String headers) {
        return GsonUtils.getGson().fromJson(headers, TYPE_MAP_STRING_STRING);
    }

    protected static boolean isLive(Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        if (event == null) {
            return false;
        }
        switch (event.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None)) {
            case RadioOtt:
            case RadioIpTv:
            case LiveTvOtt:
            case LiveTvIpTv:
            case LiveTvDvbC:
            case LiveTvDvbT:
                return true;
        }
        return false;
    }

    protected static String streamProtocol(Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        if (event == null) {
            return NONE;
        }
        switch (event.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None)) {
            case App:
            case None:
            case LiveTvDvbC:
            case LiveTvDvbT:
                break;

            case RadioIpTv:
            case LiveTvIpTv:
                return VALUE_IGMP;

            case RadioOtt:
            case LiveTvOtt:
            case Replay:
            case Recording:
            case Vod:
            case Trailer:
            case TrickPlay:
                return VALUE_HTTP;
        }
        return NONE;
    }

    protected static String streamCast(Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        if (event == null) {
            return NONE;
        }
        switch (event.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None)) {
            case App:
            case None:
                break;

            case RadioIpTv:
            case LiveTvIpTv:
            case LiveTvDvbC:
            case LiveTvDvbT:
                return VALUE_MULTICAST;

            case RadioOtt:
            case LiveTvOtt:
            case Replay:
            case Recording:
            case Vod:
            case Trailer:
            case TrickPlay:
                return VALUE_UNICAST;
        }
        return NONE;
    }

    protected static String streamRate(Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        if (event == null) {
            return NONE;
        }
        switch (event.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None)) {
            case App:
            case None:
                break;

            case RadioIpTv:
            case LiveTvIpTv:
            case LiveTvDvbC:
            case LiveTvDvbT:
                return VALUE_FIXED;

            case RadioOtt:
            case LiveTvOtt:
            case Replay:
            case Recording:
            case Vod:
            case Trailer:
            case TrickPlay:
                return VALUE_ADAPTIVE;
        }
        return NONE;
    }

    protected static String streamUrl(Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        if (event == null) {
            return NONE;
        }
        return event.getDetail(PlaybackEventDetails.PLAYBACK_URL, NONE);
    }

    protected String getPlaybackCategory(Event<PlaybackLogEvent, PlaybackEventDetails> event, String none) {
        if (event == null) {
            return none;
        }
        switch (event.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None)) {
            case LiveTvOtt:
            case LiveTvDvbC:
            case LiveTvDvbT:
            case LiveTvIpTv:
                return VALUE_LINEAR;

            case RadioIpTv:
            case RadioOtt:
                return VALUE_RADIO;

            case Replay:
                return VALUE_CUTV;

            case Recording:
                return VALUE_NPVR;

            case Vod:
            case Trailer:
                return VALUE_VOD;

            case App:
            case None:
            case TrickPlay:
                break;
        }
        return none;
    }

    protected String getScheduledTrailIdentifier(String broadcastId) {
        return broadcastId != null ? BaseBroadcastProximus.buildTrailId(broadcastId) : VALUE_NO_ID;
    }
}
