/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.account;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import okhttp3.ResponseBody;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.generic.exception.DeviceLimitReachedException;
import tv.threess.threeready.api.generic.exception.InvalidAccountException;
import tv.threess.threeready.api.generic.exception.InvalidPinException;
import tv.threess.threeready.api.generic.exception.MissingBaseUrlException;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.generic.model.WatchlistType;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.tv.epg.EpgLayoutType;
import tv.threess.threeready.data.account.AccountProxy;
import tv.threess.threeready.data.proximus.BuildConfig;
import tv.threess.threeready.data.proximus.account.model.account.AccountInfoResponseProximus;
import tv.threess.threeready.data.proximus.account.model.account.AccountInfoUpdateRequestProximus;
import tv.threess.threeready.data.proximus.account.model.account.STBProperties;
import tv.threess.threeready.data.proximus.account.model.account.STBPropertiesBodyProximus;
import tv.threess.threeready.data.proximus.account.model.account.UpdateAccountInfoBodyProximus;
import tv.threess.threeready.data.proximus.account.model.account.UpdateResponseProximus;
import tv.threess.threeready.data.proximus.account.model.account.consent.ConsentProximus;
import tv.threess.threeready.data.proximus.account.model.account.consent.UpdateConsentBodyProximus;
import tv.threess.threeready.data.proximus.account.model.boot.AvailableVersionResponseProximus;
import tv.threess.threeready.data.proximus.account.model.boot.BootConfigProximus;
import tv.threess.threeready.data.proximus.account.model.boot.BootFileNameProximus;
import tv.threess.threeready.data.proximus.account.model.boot.GlobalDataProximus;
import tv.threess.threeready.data.proximus.account.model.boot.OperatorIsolationOverride;
import tv.threess.threeready.data.proximus.account.model.boot.TMServerInfoResponseProximus;
import tv.threess.threeready.data.proximus.account.model.boot.WebAccessProximus;
import tv.threess.threeready.data.proximus.account.model.partner.NetflixTokenRequestProximus;
import tv.threess.threeready.data.proximus.account.model.partner.NetflixTokenResponseProximus;
import tv.threess.threeready.data.proximus.account.model.registration.request.DRMClientProximus;
import tv.threess.threeready.data.proximus.account.model.registration.request.DRMClientRegistrationRequestProximus;
import tv.threess.threeready.data.proximus.account.model.registration.request.STBRegistrationRequestProximus;
import tv.threess.threeready.data.proximus.account.model.registration.request.STBRegistrationUpdateRequestProximus;
import tv.threess.threeready.data.proximus.account.model.registration.response.DRMRegisteredClientsResponseProximus;
import tv.threess.threeready.data.proximus.account.model.registration.response.STBRegistrationInfoProximus;
import tv.threess.threeready.data.proximus.account.model.registration.response.STBRegistrationInfoResponseProximus;
import tv.threess.threeready.data.proximus.account.model.server.InterfaceResponseProximus;
import tv.threess.threeready.data.proximus.account.model.settings.STBSettingsResponseProximus;
import tv.threess.threeready.data.proximus.account.retrofit.BTAAccountRetrofitProximus;
import tv.threess.threeready.data.proximus.account.retrofit.BootConfigRetrofitProximus;
import tv.threess.threeready.data.proximus.account.retrofit.ConfigXmlRetrofitProximus;
import tv.threess.threeready.data.proximus.account.retrofit.ConsentRetrofitProximus;
import tv.threess.threeready.data.proximus.generic.RetrofitServicesProximus;
import tv.threess.threeready.data.proximus.generic.model.BTAErrorCodesProximus;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;
import tv.threess.threeready.data.proximus.watchlist.request.AddToWatchlistRequestProximus;
import tv.threess.threeready.data.proximus.watchlist.response.GetWatchlistItemResponseProximus;
import tv.threess.threeready.data.proximus.watchlist.response.WatchlistItemsProximus;
import tv.threess.threeready.data.proximus.watchlist.retrofit.WatchlistRetrofitProximus;

/**
 * Proxy (pass-through) interface to access proximus backend data.
 *
 * @author Barabas Attila
 * @since 2018.06.05
 */
public class AccountProxyProximus implements AccountProxy, Component {
    private static final String TAG = Log.tag(AccountProxyProximus.class);

    private static final String OWNER_TYPE = "HOUSEHOLD";
    private static final String GLOBAL_INSTALL_FILE = "globalInstall.xml";

    private final Application mApplication;

    private final RetrofitServicesProximus mRetrofitServices = Components.get(RetrofitServicesProximus.class);
    private final BootConfigRetrofitProximus mBootConfigRetrofit = mRetrofitServices.getBootConfigRetrofit();

    private final ConfigXmlRetrofitProximus mXmlConfigRetrofit = mRetrofitServices.getXmlConfigRetrofit();
    private final BTAAccountRetrofitProximus mAccountRetrofit = mRetrofitServices.getAccountRetrofit();
    private final WatchlistRetrofitProximus mWatchlistRetrofitProximus = mRetrofitServices.getWatchlistRetrofit();
    private final ConsentRetrofitProximus mConsentRetrofit = mRetrofitServices.getConsentRetrofit();
    private final XmlMapper mXmlMapper = mRetrofitServices.getXmlMapper();

    // Global data memory cache.
    private GlobalDataProximus mGlobalData;

    public AccountProxyProximus(Application application) {
        mApplication = application;
    }

    /**
     * @param signal DHCP option 67 signal base on which the environment can be decided.
     * @return The boot file which contains the base url and environment.
     */
    BootFileNameProximus getBootFileName(BootConfigRetrofitProximus.SignalType type, String signal) throws IOException {
        List<BootFileNameProximus> bootFileNameList = RetrofitServicesProximus
                .executeGen(mBootConfigRetrofit.getBootFileName(type.getType(), signal));
        return bootFileNameList.get(0); // There should be always only one.
    }

    /**
     * @param environment The environment on which the device is.
     * @return The boot config file which holds the backend endpoints
     * and client configuration options.
     */
    BootConfigProximus getBootConfig(String environment) throws IOException {
        return RetrofitServicesProximus.executeGen(mBootConfigRetrofit.getBootConfig(environment));
    }

    /**
     * @return The backend interface configurations.
     */
    InterfaceResponseProximus getInterfaces() throws IOException {
        return RetrofitServicesProximus.execute(mAccountRetrofit.getInterface());
    }

    /**
     * Get all watchlist items from the backend.
     */
    protected WatchlistItemsProximus getAllWatchlistItems() throws IOException {
        String accountType = "HOUSEHOLD";
        String accountId = SettingsProximus.householdId.get("");
        return RetrofitServicesProximus.execute(mWatchlistRetrofitProximus.getWatchlistItems(accountType, accountId));
    }

    /**
     * Add VOD item to watchlsit on the backend.
     *
     * @param type      VOD
     * @param contentId VOD ID
     * @return Watchlsit item
     * @throws IOException Backend exception
     */
    protected GetWatchlistItemResponseProximus.WatchlistItem addItemToWatchlist(WatchlistType type, String contentId) throws IOException {
        String accountType = "HOUSEHOLD";
        String accountId = SettingsProximus.householdId.get("");
        AddToWatchlistRequestProximus request = new AddToWatchlistRequestProximus(type, contentId);
        return RetrofitServicesProximus.execute(mWatchlistRetrofitProximus.addToWatchlist(accountType, accountId, request));
    }

    /**
     * Add a PROGRAM item to the watchlist.
     *
     * @param type      Watchlsit type PROGRAM
     * @param contentId Program trail ID and channel ID
     * @param channelId Channel ID (needed by the backend)
     * @return Watchlist item
     * @throws IOException Backend exception
     */
    protected GetWatchlistItemResponseProximus.WatchlistItem addItemToWatchlist(WatchlistType type, String contentId, String channelId) throws IOException {
        String accountType = "HOUSEHOLD";
        String accountId = SettingsProximus.householdId.get("");
        AddToWatchlistRequestProximus request = new AddToWatchlistRequestProximus(type, contentId, channelId);
        return RetrofitServicesProximus.execute(mWatchlistRetrofitProximus.addToWatchlist(accountType, accountId, request));
    }

    /**
     * Remove watchlist item by watchlist item ID.
     *
     * @param watchlistId ID of watchlist item (NOT content ID)
     * @return True if success, False if failure
     * @throws IOException Backend exception
     */
    protected boolean removeFromWatchlist(String watchlistId) throws IOException {
        ResponseBody result = RetrofitServicesProximus.executeGen(mWatchlistRetrofitProximus.removeFromWatchlist(watchlistId));
        Log.d(TAG, "Remove from watchlist response: " + result);
        return true;
    }

    /**
     * @return All the available broker documents and version on the server.
     */
    public AvailableVersionResponseProximus getAvailableVersions() throws IOException {
        String versionUrl = SessionProximus.availableVersionUrl.get("");
        return RetrofitServicesProximus.executeGen(mXmlConfigRetrofit.getAvailableVersions(versionUrl));
    }

    /**
     * @return The global backend configuration file.
     * @throws IOException In case of errors.
     */
    public InputStream getGlobalDataAsStream() throws IOException {
        String globalDataUrl = SessionProximus.globalInstallUrl.get("");
        return RetrofitServicesProximus.executeGen(
                mXmlConfigRetrofit.getGlobalData(globalDataUrl)).byteStream();
    }


    /**
     *
     * @return Global install broker document form the server or cache if it's still valid.
     * @throws IOException In case of errors.
     */
    public synchronized GlobalDataProximus getGlobalData() throws IOException {
        String localVersion = SessionProximus.globalInstallVersion.get(null);
        File localFile = new File(mApplication.getFilesDir(), GLOBAL_INSTALL_FILE);

        // Check if update needed.
        if (localVersion != null && localFile.exists()) {
            try {
                String remoteVersion = getAvailableVersions().getGlobalInstallVersion();
                Log.d(TAG, "Remote version : " + remoteVersion);
                if (Objects.equals(localVersion, remoteVersion)) {
                    Log.d(TAG, "Load global install from cache. Version : " + localVersion);
                    if (mGlobalData == null) {
                        mGlobalData = mXmlMapper.readValue(localFile, GlobalDataProximus.class);
                    }

                    return mGlobalData;
                }
            } catch (Exception e) {
                Log.d(TAG, "Failed to check globalInstall version. Try to update", e);
            }
        }

        // Update cached global install.
        try (InputStream inputStream = getGlobalDataAsStream()) {
            FileUtils.writeToFile(inputStream, localFile);
        } catch (Exception e) {
            Log.e(TAG, "Failed to update globalInstall", e);
        }

        mGlobalData = mXmlMapper.readValue(localFile, GlobalDataProximus.class);

        Log.d(TAG, "Global install updated. Old version : " + localVersion
                + ", new version : " + mGlobalData.getVersion());
        SessionProximus.globalInstallVersion.edit().put(mGlobalData.getVersion());
        return mGlobalData;
    }

    /**
     * Returns the result of the Operator Isolation Override varnish call.
     *
     * @return true if box needs to boot in boot in “isolated mode”
     */
    public boolean isOperatorOverriding() throws IOException {
        String url = SessionProximus.operatorIsolationOverrideUrl.get(null);
        if (TextUtils.isEmpty(url)) {
            throw new MissingBaseUrlException("Missing Operator Isolation Override url");
        }

        OperatorIsolationOverride response = RetrofitServicesProximus.execute(
                mXmlConfigRetrofit.getIsolationOverride(url));

        Log.d(TAG, "isOperationOverriding : " + response.isOperatorOverriding());
        return response.isOperatorOverriding();
    }

    /**
     * Returns the result of the BTA isAlive service call.
     *
     * @return false if box needs to boot in boot in “isolated mode”
     */
    public boolean isAlive() throws IOException {
        ResponseBody response = RetrofitServicesProximus.executeGen(mAccountRetrofit.isAlive());

        String isAliveResponse = response.string();
        Log.d(TAG, "isAlive : " + isAliveResponse);

        return Boolean.parseBoolean(isAliveResponse);
    }

    /**
     * @return A total managed server address and attributes.
     * @throws IOException In case of errors.
     */
    TMServerInfoResponseProximus getTMServerInfo() throws IOException {
        return RetrofitServicesProximus.execute(mAccountRetrofit.getTMInfo());
    }

    /**
     * @return The available DRM clients registered for the device.
     */
    public DRMRegisteredClientsResponseProximus getDRMClientRegistration() throws IOException {
        return RetrofitServicesProximus.execute(mAccountRetrofit.getDRMClientRegistration());
    }

    /**
     * Register a DRM client to the backend.
     *
     * @param drmClient The DRM client which needs to be registered.
     * @return The backend response.
     * @throws IOException In case of errors.
     */
    public BTAResponseProximus registerDRMClient(DRMClientProximus drmClient) throws IOException {
        DRMClientRegistrationRequestProximus request = new DRMClientRegistrationRequestProximus(drmClient);
        return RetrofitServicesProximus.execute(mAccountRetrofit.registerDRMClient(request));
    }

    /**
     * @return The authenticated user account info and subscriptions.
     * @throws IOException In case of errors.
     */
    AccountInfoResponseProximus getAccountInfoResponse() throws IOException {
        return RetrofitServicesProximus.execute(mAccountRetrofit.getAccountInfo());
    }

    /**
     * Changes System's Pin with the specified value.
     *
     * @param pin The value that is set as System's Pin.
     */
    @Override
    public boolean changeParentalControlPIN(String pin) throws IOException {
        UpdateResponseProximus result = RetrofitServicesProximus.execute(mAccountRetrofit.updateAccountInfo(
                new UpdateAccountInfoBodyProximus(
                        new AccountInfoUpdateRequestProximus(
                                pin,
                                null)
                ),
                Settings.systemPIN.get(""))
        );

        if (result.isSuccess()) {
            Log.d(TAG, "Parental Control PIN was changed");
            return true;
        }

        return false;
    }

    /**
     * Set a new purchase PIN which will be used for VOD renting.
     *
     * @param purchasePIN The new purchase Pin.
     */
    @Override
    public boolean changePurchasePIN(String purchasePIN) throws IOException {
        String systemPIN = Settings.systemPIN.get("");

        UpdateResponseProximus result = RetrofitServicesProximus.execute(mAccountRetrofit.updateAccountInfo(
                new UpdateAccountInfoBodyProximus(
                        new AccountInfoUpdateRequestProximus(
                                null,
                                purchasePIN)
                ), systemPIN)
        );

        if (result.isSuccess()) {
            Log.d(TAG, "Purchase PIN was changed");
            return true;
        }

        return false;
    }

    /**
     * Sets parental age rating.
     *
     * @param parentalRating The value that will be set as parental age rating.
     */
    @Override
    public void setParentalRating(@NonNull ParentalRating parentalRating) throws IOException {
        String systemPin = Settings.systemPIN.get("");

        UpdateResponseProximus result = RetrofitServicesProximus.execute(mAccountRetrofit.updateAccountInfo(
                new UpdateAccountInfoBodyProximus(new AccountInfoUpdateRequestProximus(parentalRating)), systemPin)
        );

        if (result.isSuccess()) {
            Log.d(TAG, "Parental Rating was changed to: " + parentalRating);
        }
    }

    @Override
    public void setPowerConsumptionStatus(boolean status) throws IOException {
        updateSTBSettings(STBProperties.POWER_CONSUMPTION, String.valueOf(status));
    }

    @Override
    public void updateFavoriteChannels(String favoriteChannels) throws IOException {
        updateSTBSettings(STBProperties.FAVORITE_CHANNELS, favoriteChannels);
    }

    @Override
    public void setShopLanguage(String shopLanguage) throws IOException {
        updateSTBSettings(STBProperties.SHOP_LANGUAGE, shopLanguage);
    }

    @Override
    public void setAudioLanguage(String audioLanguage) throws IOException {
        updateSTBSettings(STBProperties.STREAM_AUDIO_LANGUAGE, audioLanguage);
    }

    @Override
    public void setSubtitleLanguage(String subtitleLanguages) throws IOException {
        updateSTBSettings(STBProperties.STREAM_SUBTITLE_LANGUAGE, subtitleLanguages);
    }

    @Override
    public void setEpgLayout(EpgLayoutType epgLayout) throws IOException {
        updateSTBSettings(STBProperties.EPG_LAYOUT, epgLayout.name());
    }

    @Override
    public void setViewedHints(String hintId) throws IOException {
        Set<String> hintHashSet = Settings.viewedHints.getSet();
        hintHashSet.add(hintId);
        Settings.viewedHints.edit().put(new ArrayList<>(hintHashSet));
        updateSTBSettings(STBProperties.VIEWED_HINTS, Settings.viewedHints.get(""));
    }

    @Override
    public void deleteViewedHints() throws IOException {
        Settings.viewedHints.edit().put(new ArrayList<>(new HashSet<>()));
        updateSTBSettings(STBProperties.VIEWED_HINTS, Settings.viewedHints.get(""));
    }

    public Map<STBProperties, String> getSTBProperties() throws IOException {
        STBSettingsResponseProximus response = RetrofitServicesProximus.execute(mAccountRetrofit.getSTBProperties());
        return response.getSTBProperties();
    }

    public void updateSTBSettings(STBProperties property, String propertyValue) throws IOException {
        UpdateResponseProximus updateResponse = RetrofitServicesProximus.execute(mAccountRetrofit.setSTBProperties(
                new STBPropertiesBodyProximus(property, propertyValue)));

        if (updateResponse.isSuccess()) {
            Log.d(TAG, "STB settings was updated");
        }
    }

    /**
     * @return Info about the registered current device.
     * @throws IOException In case of errors.
     */
    public STBRegistrationInfoProximus getSTBRegistrationInfo() throws IOException {
        STBRegistrationInfoResponseProximus result = RetrofitServicesProximus.execute(mAccountRetrofit.getSTBRegistrationInfo());
        return result.getSTBRegistrationInfo();
    }

    @Override
    public void registerDevice(String lineNumber, String ipAddress,
                               String serialNum, String accountPIN,
                               String hardwareVersion, String softwareVersion,
                               String androidSerialNumber, String ethernetMACAddress) throws IOException {
        STBRegistrationRequestProximus request = new STBRegistrationRequestProximus(
                ipAddress,
                serialNum,
                accountPIN,
                hardwareVersion,
                softwareVersion,
                androidSerialNumber,
                ethernetMACAddress
        );

        try {
            RetrofitServicesProximus.execute(mAccountRetrofit.autoConfig(lineNumber, request));
        } catch (BackendException e) {
            switch (e.getErrorCode()) {
                case BTAErrorCodesProximus.INVALID_ACCOUNT_NUMBER:
                    throw new InvalidAccountException(e);

                case BTAErrorCodesProximus.INVALID_PIN:
                    throw new InvalidPinException(e);

                case BTAErrorCodesProximus.DEVICE_LIMIT_REACHED:
                    throw new DeviceLimitReachedException(e);
            }
            throw e;
        }
    }

    /**
     * Update the registered device information on the backend.
     *
     * @param request The request which holds the new device registration info.
     * @throws IOException In case of errors.
     */
    public void updateDeviceRegistration(STBRegistrationUpdateRequestProximus request) throws IOException {
        RetrofitServicesProximus.execute(mAccountRetrofit.updateSTBRegistrationInfo(request));
    }

    /**
     * @param requestScenario Starting point of the request. E.g. boot screen / setting screen...
     * @return All consents from the microservice for the given scenario.
     * @throws IOException In case of errors.
     */
    @Override
    public List<ConsentProximus> getConsents(String requestScenario) throws IOException {
        return RetrofitServicesProximus.executeGen(mConsentRetrofit.getConsents(OWNER_TYPE,
                SettingsProximus.householdId.get(""),
                LocaleUtils.getApplicationLanguage(),
                requestScenario
        ));
    }

    /**
     * @param consentId Id of the consent to be returned.
     * @return Consent with the given ID.
     * @throws IOException In case of errors.
     */
    @Override
    public ConsentProximus getConsent(String consentId) throws IOException {
        return RetrofitServicesProximus.executeGen(mConsentRetrofit.getConsent(OWNER_TYPE,
                SettingsProximus.householdId.get(""),
                consentId, LocaleUtils.getApplicationLanguage()
        ));
    }

    /**
     * @param consentId  Id of the consent to be updated.
     * @param isAccepted New state of the consent which will be sent to backend.
     * @throws IOException In case of errors.
     */
    @Override
    public void setConsent(String consentId, boolean isAccepted) throws IOException {
        RetrofitServicesProximus.executeGen(mConsentRetrofit.setConsent(OWNER_TYPE,
                SettingsProximus.householdId.get(""),
                consentId, new UpdateConsentBodyProximus(isAccepted)
        ));

        if (ConsentProximus.CONSENT_TARGETED_ADVERTISING_IDENTIFIER.equals(consentId)) {
            Settings.targetedAdvertisingFlag.edit().put(isAccepted);
        }

        if (ConsentProximus.CONSENT_RECOMMENDATION_IDENTIFIER.equals(consentId)) {
            SettingsProximus.recommendationFlag.edit().put(isAccepted);
        }


        Log.d(TAG, "Consent with id: " + consentId + " is set to: " + isAccepted);
    }

    /**
    * @return linkn of the GDPR QR code from WEBACCESS file.
    */
    public IImageSource getConsentQrCode() throws IOException {
        String webAccessUrl = SessionProximus.webAccessInstallUrl.get("");
        WebAccessProximus webAccessProximus = RetrofitServicesProximus.executeGen(mXmlConfigRetrofit.getWebAccessData(webAccessUrl));
        return webAccessProximus.getConsentQrCode();
    }

    /**
     * Enables/disables the current parental control settings.
     */
    @Override
    public void enableParentalControl(boolean enable) throws IOException {
        String systemPIN = Settings.systemPIN.get("");
        UpdateResponseProximus response = RetrofitServicesProximus.execute(mAccountRetrofit.updateAccountInfo(
                new UpdateAccountInfoBodyProximus(
                        new AccountInfoUpdateRequestProximus(enable)
                ),
                systemPIN
        ));

        if (response.isSuccess()) {
            Log.d(TAG, "Parental Control was changed to enable: " + enable);
        }
    }

    @Override
    public String getNetflixAuthenticationToken(String hardwareSerialNumber) throws IOException {
        NetflixTokenRequestProximus request = new NetflixTokenRequestProximus(
                SettingsProximus.netflixPartnerId.get(""), hardwareSerialNumber);

        NetflixTokenResponseProximus response = RetrofitServicesProximus.execute(
                mAccountRetrofit.getNetflixToken(LocaleUtils.getApplicationLanguage(), request)
        );
        return response.getToken();
    }

    public void registerInstance(String instanceId) throws IOException {
        RetrofitServicesProximus.execute(mAccountRetrofit.registerAppInstance(instanceId));
    }
}
