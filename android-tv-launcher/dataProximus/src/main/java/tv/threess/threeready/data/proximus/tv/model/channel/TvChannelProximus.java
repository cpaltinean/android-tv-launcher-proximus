/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.proximus.tv.model.channel;

import android.text.TextUtils;
import androidx.annotation.Nullable;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.proximus.tv.adapter.ChannelTypeAdapter;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Channel response entity from backend.
 *
 * @author Barabas Attila
 * @since 2018.06.07
 */
@XmlRootElement(name = "Channel")
public class TvChannelProximus extends TvChannel {

    @XmlElement(name = "ExternalID")
    private String mId;

    @XmlElement(name = "ChannelId")
    private String mInternalId;

    @XmlElement(name = "DisplayChannelName")
    private String mName;

    @XmlElement(name = "CallLetters")
    private String mCallLetter;

    @XmlElement(name = "Language")
    private String mLanguage;

    @XmlElement(name = "HDContents")
    private boolean mIsHDEnabled;

    @XmlElement(name = "IPTVCUWindowMaxHours")
    private int mReplayWindowHours;

    @XmlElement(name = "EnableNPVR")
    private boolean mIsNPVREnabled;

    @XmlElement(name = "ChannelType")
    @XmlJavaTypeAdapter(ChannelTypeAdapter.class)
    private ChannelType mChannelType;

    @XmlElement(name = "ChannelLogo")
    private String mChannelIconUrl;

    @XmlElementWrapper(name = "ChannelStreams")
    @XmlElement(name = "ChannelStream")
    private List<ChannelStreamProximus> mStreams;

    @XmlElement(name = "EnableTALTV")
    private boolean mLiveTadEnabled;

    @XmlElement(name = "EnableTAPLTV")
    private boolean mTimeShiftTadEnabled;

    @XmlElement(name = "TargetAdInactivity")
    private long mTargetAdInactivity;

    private int mNumber;

    private boolean mHasReplaySubscription;

    // Only subscribed channels are displayed.
    private final boolean mIsUserTVSubscribedChannel = true;

    // Will be set based on boot config.
    private boolean mIsBabyContentWarningRequired;

    // Will be set based on boot config.
    private String mPackageName;

    /**
     * @return Unique identifier of the channel.
     */
    @Override
    public String getId() {
        return mId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getInternalId() {
        return mInternalId;
    }

    /**
     * @return Call letter of the broadcaster channel.
     */
    @Override
    public String getCallLetter() {
        return mCallLetter;
    }

    /**
     * @return The display number of the channel.
     */
    @Override
    public int getNumber() {
        return mNumber;
    }

    /**
     * @return The display name of the channel.
     */
    @Override
    public String getName() {
        return mName;
    }

    /**
     * @return The main language of the channel.
     */
    @Override
    public String getLanguage() {
        return mLanguage;
    }

    @Override
    public String getChannelLogoUrl() {
        return mChannelIconUrl;
    }

    /**
     * @return The type of the channel.
     * @see ChannelType
     */
    @Override
    public ChannelType getType() {
        return mChannelType;
    }

    /**
     * @return True if the channel has HD streams.
     */
    @Override
    public boolean isHDEnabled() {
        return mIsHDEnabled;
    }

    /**
     * @return true, if the user is entitled to the channel
     */
    @Override
    public boolean isUserTvSubscribedChannel() {
        return mIsUserTVSubscribedChannel;
    }

    /**
     * @return true if the channel is recording capable.
     */
    @Override
    public boolean isNPVREnabled() {
        return mIsNPVREnabled;
    }

    /**
     * @return The maximum time (milliseconds) in the past until the user can watch replay.
     */
    @Override
    public long getReplayWindow() {
        return TimeUnit.HOURS.toMillis(mReplayWindowHours);
    }

    /**
     * Gets whether a warning is required regarding baby content.
     */
    @Override
    public boolean isBabyContentWarningRequired() {
        return mIsBabyContentWarningRequired;
    }

    /**
     * @return True if the channel is capable of replay playback.
     */
    @Override
    public boolean isReplayEnabled() {
        return mReplayWindowHours > 0 && mHasReplaySubscription;
    }

    public String getPackageName() {
        return mPackageName;
    }


    public void setBabyContentWarningRequired(boolean babyContentWarningRequired) {
        mIsBabyContentWarningRequired = babyContentWarningRequired;
    }

    public void setChannelType(ChannelType channelType) {
        mChannelType = channelType;
    }

    public void setPackageName(String packageName) {
        mPackageName = packageName;
    }

    public void setHasReplaySubscription(boolean hasReplaySubscription) {
        mHasReplaySubscription = hasReplaySubscription;
    }

    public void setNumber(int number) {
        mNumber = number;
    }

    /**
     * @return The multicast address of the stream if available
     * or null otherwise.
     */
    @Nullable
    public ChannelStreamProximus getMulticastStream() {
        if (mStreams != null) {
            for (ChannelStreamProximus channelStream : mStreams) {
                if (channelStream != null && !TextUtils.isEmpty(channelStream.getMulticastAddress())) {
                    return channelStream;
                }
            }
        }
        return null;
    }

    @Override
    public boolean isLiveTadEnabled() {
        return mLiveTadEnabled;
    }

    @Override
    public boolean isTimeShiftTadEnabled() {
        return mTimeShiftTadEnabled;
    }

    public long getTargetAdInactivity(TimeUnit unit) {
        return unit.convert(mTargetAdInactivity, TimeUnit.MINUTES);
    }

    @XmlRootElement(name = "ChannelStream")
    protected static class ChannelStreamProximus {

        @XmlAttribute(name = "MTPID")
        private String mMtpId;

        @XmlElement(name = "ChannelURL")
        private ChannelURLProximus mUrl;

        public String getMtpId() {
            return mMtpId;
        }

        public String getMulticastAddress() {
            return mUrl != null ? mUrl.getMulticastAddress() : null;
        }
    }

    private static class ChannelURLProximus {
        @XmlElement(name = "MulticastIPAddress")
        String mIP;

        @XmlElement(name = "Port")
        String mPort;

        public String getMulticastAddress() {
            if (TextUtils.isEmpty(mIP)) {
                return null;
            }

            StringBuilder sb = new StringBuilder();
            sb.append("rtp://");
            sb.append(mIP);
            if (!TextUtils.isEmpty(mPort)) {
                sb.append(":");
                sb.append(mPort);
            }

            return sb.toString();
        }

    }

}
