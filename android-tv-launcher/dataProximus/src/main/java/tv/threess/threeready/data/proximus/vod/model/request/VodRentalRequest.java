package tv.threess.threeready.data.proximus.vod.model.request;

import android.text.TextUtils;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.account.helper.StringUtils;

/**
 * Proximus retrofit class for vo renting request.
 *
 * Created by Noemi Dali on 30.04.2020.
 */
@XmlRootElement(name = "BTA")
public class VodRentalRequest implements Serializable {

    @XmlElement(name = "ItemRental")
    private final ItemRental mItemRental;

    public VodRentalRequest(String movieId, float price, boolean isFreeRental, String priceDetermination) {
        mItemRental = new ItemRental();
        mItemRental.movieId = movieId;
        mItemRental.price = isFreeRental ? null : StringUtils.formatPrice(price);
        mItemRental.freeRental = isFreeRental;
        mItemRental.priceDetermination = TextUtils.isEmpty(priceDetermination) ? "OriginalAssetPriceValid" : priceDetermination;
    }

    @XmlRootElement(name = "ItemRental")
    private static class ItemRental implements Serializable {

        @XmlElement(name = "VoDExtID")
        private String movieId;

        @XmlElement(name = "RentCost")
        private String price;

        @XmlElement(name = "FreeRental")
        private boolean freeRental;

        @XmlElement(name = "PriceDeterminationResult")
        private String priceDetermination;
    }
}
