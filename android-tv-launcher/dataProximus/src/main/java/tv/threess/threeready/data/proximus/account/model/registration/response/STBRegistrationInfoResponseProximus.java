package tv.threess.threeready.data.proximus.account.model.registration.response;

import javax.xml.bind.annotation.XmlElement;

import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * BTA response the a device registration info request.
 *
 * @author Barabas Attila
 * @since 2020.04.14
 */
public class STBRegistrationInfoResponseProximus extends BTAResponseProximus {

    @XmlElement(name = "STBRegistrationInfo")
    private STBRegistrationInfoProximus mSTBRegistrationInfo;

    public STBRegistrationInfoProximus getSTBRegistrationInfo() {
        return mSTBRegistrationInfo;
    }
}
