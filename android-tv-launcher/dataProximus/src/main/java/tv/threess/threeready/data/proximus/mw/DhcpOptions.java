package tv.threess.threeready.data.proximus.mw;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import tv.threess.threeready.api.middleware.model.BaseInfo;

/**
 * Read DHCP option values from bootstrap protocol.
 * https://www.iana.org/assignments/bootp-dhcp-parameters/bootp-dhcp-parameters.xhtml
 *
 * @author Barabas Attila
 * @since 3/2/21
 */
public class DhcpOptions extends BaseInfo<DhcpOptions.Field> {

    private static final Uri CONTENT_URI = Uri.parse("content://technicolor/dhcp_options");
    private static final String COLUMN_KEY = "name";
    private static final String COLUMN_VALUE = "value";

    private DhcpOptions() {
        super(Field.class);
    }

    public static DhcpOptions read(Context context) {
        DhcpOptions dhcpOptions = new DhcpOptions();
        try (Cursor cursor = context.getContentResolver().query(CONTENT_URI, null, null, null)) {
            int key = cursor.getColumnIndex(COLUMN_KEY);
            int value = cursor.getColumnIndex(COLUMN_VALUE);
            dhcpOptions.readFrom(cursor, key, value);
            return dhcpOptions;
        }
    }

    public enum Field implements BaseInfo.Field {
        Option43("option_43"),
        Option67("option_67");

        private final String mKey;

        Field(String key) {
            mKey = key;
        }

        @Override
        public String getKey() {
            return mKey;
        }
    }
}
