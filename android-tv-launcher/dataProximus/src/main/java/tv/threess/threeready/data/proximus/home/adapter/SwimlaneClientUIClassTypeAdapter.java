package tv.threess.threeready.data.proximus.home.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Xml type adapter to read the client UI class as enum.
 *
 * @author Barabas Attila
 * @since 9/20/21
 */
public class SwimlaneClientUIClassTypeAdapter extends XmlAdapter<String, SwimLaneProximus.ClientUiClass> {

    @Override
    public SwimLaneProximus.ClientUiClass unmarshal(String value) {
        switch (value) {
            default:
                return SwimLaneProximus.ClientUiClass.Undefined;
            case "VodCat_Small":
                return SwimLaneProximus.ClientUiClass.VodCat_Small;
            case "pullDownMenu" :
                return SwimLaneProximus.ClientUiClass.PullDownMenu;
            case "EPG_Wall" :
                return SwimLaneProximus.ClientUiClass.EPG_Wall;
            case "Channel_Wall" :
                return SwimLaneProximus.ClientUiClass.Channel_Wall;
        }
    }

    @Override
    public String marshal(SwimLaneProximus.ClientUiClass value) {
        throw new IllegalStateException("Not implemented.");
    }
}
