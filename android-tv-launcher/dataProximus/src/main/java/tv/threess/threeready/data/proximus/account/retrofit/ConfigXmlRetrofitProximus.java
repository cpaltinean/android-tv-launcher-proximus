package tv.threess.threeready.data.proximus.account.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;
import tv.threess.threeready.data.proximus.account.model.boot.AvailableVersionResponseProximus;
import tv.threess.threeready.data.proximus.account.model.boot.OperatorIsolationOverride;
import tv.threess.threeready.data.proximus.account.model.boot.WebAccessProximus;
import tv.threess.threeready.data.proximus.generic.interceptor.BackendIsolationInterceptor;
import tv.threess.threeready.data.proximus.tv.model.advertisement.TvAdvertisementProximus;
import tv.threess.threeready.data.proximus.tv.model.advertisement.VastAdsResponse;

/**
 * Retrofit interface to get the config file from backend.
 * Calls through this interface are ignored by Managed network checker.
 *
 * @author Karetka Mezei Zoltan
 * @since 2021.01.26
 */
public interface ConfigXmlRetrofitProximus {

    String BASE_URI = "https://cc7.streaming.proximustv.be";

    /**
     * Download the Xml config from the server.
     *
     * @return The server response and body.
     */
    @GET
    @BackendIsolationInterceptor.Allow
    Call<OperatorIsolationOverride> getIsolationOverride(@Url String url);

    @GET
    Call<AvailableVersionResponseProximus> getAvailableVersions(@Url String url);

    @GET
    Call<ResponseBody> getGlobalData(@Url String url);

    @GET
    Call<TvAdvertisementProximus> getAdvertisement(@Url String url, @Query("filename") String filename);


    @GET
    Call<VastAdsResponse> getAdvertisements(@Url String url,
                                            @Query("NA") String lineNumber,
                                            @Query("availSpace") long availSpace,
                                            @Query("uiLanguage") String uiLanguage
    );

    @GET
    Call<ResponseBody> getAdReplacement(@Url String url,
                                        @Query("VAST") String vastVersion,
                                        @Query("Version") String version,
                                        @Query("AccountNA") String lineNumber,
                                        @Query("MAC") String macAddress,
                                        @Query("RequestID") String requestId,
                                        @Query("ChannelExternalID") String channelId,
                                        @Query("Vdur") double videoDurationSeconds,
                                        @Query("SegmDesc") String segmentDescriptor
    );

    @GET
    Call<WebAccessProximus> getWebAccessData(@Url String url);
}
