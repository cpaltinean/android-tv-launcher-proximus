package tv.threess.threeready.data.proximus.generic.adapter;

import java.util.Locale;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.api.log.Log;

/**
 * Xml type adapter to read the pin add pad it with 0 prefix to reach 4 character if needed.
 *
 * @author Barabas Attila
 * @since 2020.09.03
 */
public class PinAdapterProximus extends XmlAdapter<String, String> {
    private static final String TAG = Log.tag(PinAdapterProximus.class);

    @Override
    public String unmarshal(String v) {
        return addPinPadding(v);
    }

    @Override
    public String marshal(String v) {
        return addPinPadding(v);
    }

    /**
     * Pad the pin with `0` prefix in case it has less than 4 digit.
     * @param pin The pin which needs to be padded.
     * @return The new pin with padding for 4 character.
     */
    private String addPinPadding(String pin) {
        if (pin != null && pin.length() < 4) {
            try {
                return String.format(Locale.getDefault(), "%04d", Integer.parseInt(pin));
            } catch (Exception e) {
                Log.e(TAG, "Could not add digit padding. Pin : " + pin, e);
            }
        }
        return pin;
    }
}
