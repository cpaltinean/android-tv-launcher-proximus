package tv.threess.threeready.data.proximus.home.model.swimalane;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * BTA program container for a swim lane item which holds the broadcast and the meta info.
 *
 * @author Barabas Attila
 * @since 2019.10.30
 */
@XmlRootElement(name = "Program")
public class SwimLaneProgramProximus implements Serializable {

    @XmlElement(name = "ProgRefNo")
    private String mProgramId;

    @XmlElement(name = "ProgMetainfo")
    private String mProgramMetaInfo;

    @XmlElementWrapper(name = "Airings")
    @XmlElement(name = "Airing")
    private List<SwimLaneBroadcastProximus> mBrodcasts;

    public String getProgramMetaInfo() {
        return mProgramMetaInfo;
    }

    @Nullable
    public SwimLaneBroadcastProximus getBroadcast() {
        return mBrodcasts == null || mBrodcasts.isEmpty() ? null : mBrodcasts.get(0);
    }


}
