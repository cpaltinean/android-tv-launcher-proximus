package tv.threess.threeready.data.proximus.home.model.editorial;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.PageByIdEditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemStyle;
import tv.threess.threeready.data.proximus.home.model.AccessPointProximus;

/**
 * Editorial item which open an access point page.
 *
 * @author Barabas Attila
 * @since 5/26/21
 */
public class AccessPointEditorialItemProximus implements EditorialItem {
    private final AccessPointProximus mAccessPoint;

    public AccessPointEditorialItemProximus(AccessPointProximus accessPoint) {
        mAccessPoint = accessPoint;
    }

    @Override
    public String getId() {
        return mAccessPoint.getId();
    }

    public String getTitle() {
        return mAccessPoint.getDisplayNames().getLocalizedNameOrFirst();
    }

    @Nullable
    @Override
    public EditorialItemStyle getStyle() {
        return mItemStyle;
    }

    @Nullable
    @Override
    public EditorialItemAction getAction() {
        return mItemAction;
    }

    private final PageByIdEditorialItemAction mItemAction = new PageByIdEditorialItemAction() {
        @Override
        public String getPageId() {
            return mAccessPoint.getId();
        }
    };

    protected final EditorialItemStyle mItemStyle = new EditorialItemStyle() {
        private final Size mSize = new VariantSize(VariantSize.Variant.Small);

        @Nullable
        @Override
        public IImageSource getBackgroundImage() {
            return mAccessPoint.getLocalizedImage();
        }

        @Nullable
        @Override
        public Size getHeight() {
            return mSize;
        }

        @Nullable
        @Override
        public Size getWidth() {
            return mSize;
        }
    };
}
