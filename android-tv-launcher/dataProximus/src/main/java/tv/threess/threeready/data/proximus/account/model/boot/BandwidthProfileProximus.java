package tv.threess.threeready.data.proximus.account.model.boot;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class BandwidthProfileProximus implements Serializable {
    @XmlAttribute(name = "BWPName")
    private String mBWPName;

    @XmlElement(name = "bandwidth")
    private int mBandwidth;

    @XmlElement(name = "TargetAdenabled")
    private boolean mTargetAdEnabled;

    @XmlElement(name = "TargetAdDownLoadBandwidth")
    private int mTargetAdDownLoadBandwidth;

    public String getBWPName() {
        return mBWPName;
    }

    public int getBandwidth() {
        return mBandwidth;
    }

    public boolean isTargetAdEnabled() {
        return mTargetAdEnabled;
    }

    public int getTargetAdDownLoadBandwidth() {
        return mTargetAdDownLoadBandwidth;
    }
}
