package tv.threess.threeready.data.proximus.log.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import tv.threess.threeready.api.middleware.model.BaseInfo;

/**
 * Read and store the VQE statistics.
 *
 * @author Karetka Mezei Zoltan
 * @since 2020.07.13
 */
public class VQEJitterInfo extends BaseInfo<VQEJitterInfo.Field> {

    private static final Uri CONTENT_URI = Uri.parse("content://technicolor/vqe_jitter");
    private static final String COLUMN_KEY = "name";
    private static final String COLUMN_VALUE = "value";

    private VQEJitterInfo() {
        super(Field.class);
    }

    public static VQEJitterInfo read(Context context) {
        VQEJitterInfo info = new VQEJitterInfo();

        try (Cursor cursor = context.getContentResolver().query(CONTENT_URI, null, null, null)) {
            int key = cursor.getColumnIndex(COLUMN_KEY);
            int value = cursor.getColumnIndex(COLUMN_VALUE);
            info.readFrom(cursor, key, value);
            return info;
        }
    }

    public enum Field implements BaseInfo.Field {
        MinJitter("min_jitter"),
        MaxJitter("max_jitter"),
        AvgJitter("mean_jitter"),
        StdJitter("std_dev_jitter");

        private final String mKey;

        Field(String key) {
            mKey = key;
        }

        @Override
        public String getKey() {
            return mKey;
        }
    }
}
