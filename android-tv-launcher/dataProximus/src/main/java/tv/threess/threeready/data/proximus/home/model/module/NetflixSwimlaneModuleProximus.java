package tv.threess.threeready.data.proximus.home.model.module;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceParams;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceService;
import tv.threess.threeready.api.home.model.module.ModuleDataType;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Module config to display Netflix stripes.
 *
 * @author David
 * @since 28.02.2022
 */
public class NetflixSwimlaneModuleProximus extends SwimlaneModuleConfigProximus {

    public NetflixSwimlaneModuleProximus(SwimLaneProximus swimlane) {
        super(swimlane);
    }

    public String getNetflixCategory() {
        return getSwimlane().getNetflixCategory();
    }

    public String getNetflixInteractionId() {
        return getSwimlane().getNetflixInteractionId();
    }

    @Override
    public ModuleDataType getDataType() {
        return ModuleDataType.NETFLIX;
    }

    @Nullable
    @Override
    public ModuleDataSource getDataSource() {
        return mDataSource;
    }

    private final ModuleDataSource mDataSource = new ModuleDataSource() {

        @Override
        public ModuleDataSourceService getService() {
            return ModuleDataSourceService.NETFLIX;
        }

        @Override
        public ModuleDataSourceMethod getMethod() {
            return ModuleDataSourceMethod.NETFLIX;
        }

        @Nullable
        @Override
        public ModuleDataSourceParams getParams() {
            return mParams;
        }
    };
}