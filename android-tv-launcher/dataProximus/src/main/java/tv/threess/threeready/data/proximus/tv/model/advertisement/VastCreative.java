package tv.threess.threeready.data.proximus.tv.model.advertisement;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * A creative in VAST is a file that is part of a VAST ad.
 * Multiple creative may be provided in the form of: Linear, NonLinear, Companions.
 * Each nested <Creative> element contains one of: <Linear>, <NonLinear>, <CompanionAds>.
 */
public class VastCreative {
    @XmlAttribute(name = "id")
    private String mId;

    @XmlAttribute(name = "AdID")
    private String mAdId;

    @XmlElement(name = "Linear")
    private VastLinear mSequence;

    public String getId() {
        return mId;
    }

    public String getAdId() {
        return mAdId;
    }

    public VastLinear getSequence() {
        return mSequence;
    }
}
