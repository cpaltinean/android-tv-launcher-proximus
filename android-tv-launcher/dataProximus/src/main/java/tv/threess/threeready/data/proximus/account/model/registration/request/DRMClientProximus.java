package tv.threess.threeready.data.proximus.account.model.registration.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * BTA model represents a registered DRM client.
 *
 * @author Barabas Attila
 * @since 2020.04.17
 */
@XmlRootElement(name = "DRMClientCapability")
public class DRMClientProximus implements Serializable {

    public static DRMClientProximus buildIPTVDRMClient(String clientDeviceId) {
        DRMClientProximus drmClient = new DRMClientProximus();
        drmClient.mDRMClientInstanceId = ClientInstanceId.IPTV;
        drmClient.mDRMSystemExternalId = ExternalId.IPTV;
        drmClient.mDRMClientDeviceId = clientDeviceId;
        drmClient.mimeTypes = MimeType.IPTV;
        return drmClient;
    }

    public static DRMClientProximus buildOTTDRMClient(String clientDeviceId) {
        DRMClientProximus drmClient = new DRMClientProximus();
        drmClient.mDRMClientInstanceId = ClientInstanceId.OTT;
        drmClient.mDRMSystemExternalId = ExternalId.OTT;
        drmClient.mDRMClientDeviceId = clientDeviceId;
        drmClient.mimeTypes = MimeType.OTT;
        return drmClient;
    }

    @XmlElement(name = "DRMClientInstanceID")
    private String mDRMClientInstanceId;

    @XmlElement(name = "DRMSystemExternalID")
    private String mDRMSystemExternalId;

    @XmlElement(name = "DRMClientDeviceID")
    private String mDRMClientDeviceId;

    @XmlElement(name = "MIMETypes")
    private String mimeTypes;


    public String getDRMClientInstanceId() {
        return mDRMClientInstanceId;
    }

    public String getDRMSystemExternalId() {
        return mDRMSystemExternalId;
    }

    public String getDRMClientDeviceId() {
        return mDRMClientDeviceId;
    }


    public interface ClientInstanceId {
        String IPTV = "D_STBV7_VMX_IPTV";
        String OTT = "D_STBV7_VMX_MULTIRIGHTS";
    }

    public interface ExternalId {
        String IPTV = "VMX_IPTV";
        String OTT = "VMX_MULTIRIGHTS";
    }

    public interface MimeType {
        String IPTV = "video/IPTV";
        String OTT = "application/dash+xml";
    }

}
