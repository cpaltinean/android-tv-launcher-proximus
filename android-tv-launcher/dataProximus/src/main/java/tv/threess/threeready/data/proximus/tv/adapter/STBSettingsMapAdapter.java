package tv.threess.threeready.data.proximus.tv.adapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.data.proximus.account.model.account.STBProperties;
import tv.threess.threeready.data.proximus.account.model.settings.STBPropertyProximus;

/**
 * Type adapter for STB settings
 *
 * @author Solyom Zsolt
 * @since 2020.07.10
 */
public class STBSettingsMapAdapter extends XmlAdapter<List<STBPropertyProximus>, Map<STBProperties, String>> {

    @Override
    public Map<STBProperties, String> unmarshal(List<STBPropertyProximus> propertyListProximus) throws Exception {
        Map<STBProperties, String> propertyMap = new HashMap<>();
        propertyListProximus.forEach(it -> addItemIfNotNull(propertyMap, it));
        return propertyMap;
    }

    @Override
    public List<STBPropertyProximus> marshal(Map<STBProperties, String> map) throws Exception {
        List<STBPropertyProximus> propertyList = new ArrayList<>();
        Arrays.asList(STBProperties.values()).forEach(it -> addItemToList(it, map, propertyList));
        return propertyList;
    }

    private void addItemIfNotNull(Map<STBProperties, String> propertyMap, STBPropertyProximus item) {
        STBProperties key = STBProperties.fromString(item.getName());
        if (key != null) {
            propertyMap.put(key, item.getValue());
        }
    }

    private void addItemToList(STBProperties property, Map<STBProperties, String> map, List<STBPropertyProximus> propertyList) {
        String value = map.getOrDefault(property, null);
        if (value != null) {
            propertyList.add(new STBPropertyProximus(property.getPropertyName(), value));
        }
    }
}
