package tv.threess.threeready.data.proximus.log.utils;

import androidx.annotation.NonNull;

import tv.threess.threeready.api.middleware.model.BitStream;

public class EdidInfo {
    private static final String FIXED_HEADER_PATTERN = "00 FF FF FF FF FF FF 00";

    private final String header;            // EDID header "00 FF FF FF FF FF FF 00"
    private final String manufacturerId;      // EISA 3-character ID
    private final String productCode;       // Vendor assigned code
    private final String serialNumber;      // Serial number
    private final int manufacturedWeek;  // Week number
    private final int manufacturedYear;  // Year number + 1990
    private final String version;           // EDID version
    private final String revision;          // EDID revision

    public EdidInfo(byte[] bytes) {
        if (bytes.length != 128) {
            throw new IllegalArgumentException("EDID must be a 128 byte long record");
        }

        BitStream stream = new BitStream(bytes);
        this.header = stream.readHex(8, " ");

        if (!header.equals(FIXED_HEADER_PATTERN)) {
            throw new IllegalArgumentException("First 8 byte should be the fixed header pattern: " + FIXED_HEADER_PATTERN);
        }

        if (stream.readBit() != 0) {
            throw new IllegalArgumentException("First bit in Manufacturer ID must be 0");
        }
        char[] manufacturer = new char[3];
        manufacturer[0] = (char) (stream.readU32(5) + '@');
        manufacturer[1] = (char) (stream.readU32(5) + '@');
        manufacturer[2] = (char) (stream.readU32(5) + '@');
        this.manufacturerId = new String(manufacturer);

        this.productCode = stream.readHex(2, " ");
        this.serialNumber = stream.readHex(4, " ");
        this.manufacturedWeek = stream.readU32(8);
        this.manufacturedYear = 1990 + stream.readU32(8);
        this.version = stream.readHex(1);
        this.revision = stream.readHex(1);
    }

    public String getHeader() {
        return header;
    }

    public String getManufacturerId() {
        return manufacturerId;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public int getManufacturedYear() {
        return manufacturedYear;
    }

    public int getManufacturedWeek() {
        return manufacturedWeek;
    }

    public String getVersion() {
        return version;
    }

    public String getRevision() {
        return revision;
    }

    @NonNull
    @Override
    public String toString() {
        return "EdidInfo{" +
                "header='" + header + '\'' +
                ", manufacturerId='" + manufacturerId + '\'' +
                ", productCode='" + productCode + '\'' +
                ", serialNumber='" + serialNumber + '\'' +
                ", manufacturedWeek='" + manufacturedWeek + '\'' +
                ", manufacturedYear='" + manufacturedYear + '\'' +
                ", version='" + version + '\'' +
                ", revision='" + revision + '\'' +
                '}';
    }
}