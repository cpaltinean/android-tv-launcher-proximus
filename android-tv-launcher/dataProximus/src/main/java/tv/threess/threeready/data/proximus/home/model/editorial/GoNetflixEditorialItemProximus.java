package tv.threess.threeready.data.proximus.home.model.editorial;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Swimlane tile which open netflix application.
 *
 * @author Barabas Attila
 * @since 6/18/21
 */
public class GoNetflixEditorialItemProximus extends SwimlaneEditorialItemProximus {

    public GoNetflixEditorialItemProximus(SwimLaneProximus swimLane,
                                          SwimLaneProximus.SwimlaneTile swimlaneTile, Integer position) {
        super(swimLane, swimlaneTile, position);
    }

    @Nullable
    @Override
    public EditorialItemAction getAction() {
        return mItemAction;
    }

    private final EditorialItemAction mItemAction =
            (EditorialItemAction.AppEditorialItemAction) () -> PackageUtils.PACKAGE_NAME_NETFLIX;
}
