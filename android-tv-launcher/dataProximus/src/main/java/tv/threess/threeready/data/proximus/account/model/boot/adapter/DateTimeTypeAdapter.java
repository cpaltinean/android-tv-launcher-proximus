package tv.threess.threeready.data.proximus.account.model.boot.adapter;

import java.util.concurrent.TimeUnit;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Java type adapter to read String field (hours:minutes) as Long.
 *
 * @author Zsolt Bokor_
 * @since 2020.06.10
 */
public class DateTimeTypeAdapter extends XmlAdapter<String, Long> {
    private static final int HOUR_START_POSITION = 0;
    private static final String COLON_SEPARATOR = ":";

    @Override
    public Long unmarshal(String time) throws Exception {
        int position = time.indexOf(COLON_SEPARATOR);
        if (position == -1) {
            return null;
        }
        String hours = time.substring(HOUR_START_POSITION, position);
        String minutes = time.substring(position + 1);
        return TimeUnit.HOURS.toMillis(Long.parseLong(hours)) +
                TimeUnit.MINUTES.toMillis(Long.parseLong(minutes));
    }

    @Override
    public String marshal(Long v) throws Exception {
        throw new IllegalStateException("Not implemented.");
    }
}
