package tv.threess.threeready.data.proximus.generic.interceptor;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import tv.threess.threeready.data.proximus.utils.URLParser;

/**
 * Interceptor the X-PXTV-VERSION header to backend calls.
 *
 * @author Barabas Attila
 * @since 3/2/21
 */
public class PxtvHeaderOverrideInterceptor implements Interceptor {

    private static final String PXTV_VERSION = "X-PXTV-VERSION";

    private final String mPxtvHeader;

    public PxtvHeaderOverrideInterceptor() {
        mPxtvHeader = URLParser.getBTAHeader();
    }

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request.Builder requestBuilder = chain.request().newBuilder();
        requestBuilder.addHeader(PXTV_VERSION, mPxtvHeader);
        return chain.proceed(requestBuilder.build());
    }
}
