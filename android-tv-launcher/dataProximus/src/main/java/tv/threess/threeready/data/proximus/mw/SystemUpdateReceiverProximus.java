/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.mw;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.HandlerThread;

import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.BuildConfig;
import tv.threess.threeready.api.account.setting.BatchSettingEditor;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.receivers.BaseBroadcastReceiver;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.SystemUpdateReceiver;
import tv.threess.threeready.api.middleware.UpdateUrgency;

/**
 * Proximus specific implementation of the system update receiver.
 *
 * @author Barabas Attila
 * @since 19.09.2020
 */
public class SystemUpdateReceiverProximus extends SystemUpdateReceiver {
    private static final String TAG = Log.tag(SystemUpdateReceiverProximus.class);

    private static final Handler sWorkerHandler = new Handler(new HandlerThread(":UpdateReceiver") {{
        this.start();
    }}.getLooper());

    private static final String INTENT_ACTION_SYSTEM_UPDATE = "com.technicolor.ota.firmware.SYSTEM_UPDATE";
    private static final String INTENT_ACTION_SYSTEM_UPDATE_TEST = "com.technicolor.ota.firmware.SYSTEM_UPDATE_TEST";

    private static final String EXTRA_URGENCY = "urgency";
    private static final String EXTRA_STATUS_VALUE = "status";
    private static final String EXTRA_PROGRESS_VALUE = "progress";
    private static final String EXTRA_NEXT_CHECK_IN_TIME = "next_checkin_time";

    // system update status
    private static final int STATUS_UPDATE_NOT_AVAILABLE = 0;
    private static final int STATUS_DOWNLOADING_UPDATE = 2;
    private static final int STATUS_DOWNLOAD_FAILED = 6;
    private static final int STATUS_DOWNLOAD_FAILED_NO_SPACE = 9;
    private static final int STATUS_UPDATE_AVAILABLE = 101;
    private static final int STATUS_CHECK_IN_FAILED = 102;
    private static final int STATUS_PAYLOAD_DOWNLOADING = 111;
    private static final int STATUS_PAYLOAD_VERIFYING = 112;
    private static final int STATUS_PAYLOAD_VERIFIED = 113;
    private static final int STATUS_PAYLOAD_UPDATED_NEED_REBOOT = 115;
    private static final int STATUS_PAYLOAD_DOWNLOAD_FAILED = 116;
    private static final int STATUS_PAYLOAD_VERIFY_FAILED = 117;
    private static final int STATUS_PAYLOAD_APPLY_FAILED = 118;
    private static final int STATUS_UPDATE_FAILED = 999;

    private final Context mContext;
    private boolean mManualCheck;

    public SystemUpdateReceiverProximus(Context context) {
        mContext = context;
    }

    @Override
    public void notifyUpdateCheckTriggered(boolean manualCheck) {
        super.notifyUpdateCheckTriggered(manualCheck);
        mManualCheck = manualCheck;
    }

    @Override
    public void registerReceiver() {
        super.registerReceiver();
        mBroadcastReceiver.registerReceiver(mContext, sWorkerHandler);
    }

    @Override
    public void unRegisterReceiver() {
        super.unRegisterReceiver();
        mBroadcastReceiver.unregisterReceiver(mContext);
    }

    private final BaseBroadcastReceiver mBroadcastReceiver = new BaseBroadcastReceiver() {

        @Override
        public IntentFilter getIntentFilter() {
            IntentFilter intentFilter = new IntentFilter(INTENT_ACTION_SYSTEM_UPDATE_TEST);
            intentFilter.addAction(INTENT_ACTION_SYSTEM_UPDATE);
            if (BuildConfig.DEBUG) {
                intentFilter.addAction(INTENT_ACTION_SYSTEM_UPDATE_TEST);
            }
            return intentFilter;
        }

        @Override
        public void onReceive(final Context context, Intent intent) {
            int status = intent.getIntExtra(EXTRA_STATUS_VALUE, -1);
            int progress = intent.getIntExtra(EXTRA_PROGRESS_VALUE, 0);
            long nextCheckInTime = TimeUnit.SECONDS.toMillis(
                    intent.getLongExtra(EXTRA_NEXT_CHECK_IN_TIME, 0));
            String urgencyString = intent.getStringExtra(EXTRA_URGENCY);

            BatchSettingEditor<Settings> settingsEditor = Settings.batchEdit();

            // Save next check-in time
            if (nextCheckInTime > 0) {
                settingsEditor.put(Settings.systemUpdateNextCheckInTime, nextCheckInTime);
            }

            // Save urgency.
            if (urgencyString == null) {
                urgencyString = Settings.systemUpdateUrgency.get("");
            } else {
                settingsEditor.put(Settings.systemUpdateUrgency, urgencyString);
            }

            settingsEditor.persistNow();

            UpdateUrgency urgency = UpdateUrgency.findByName(urgencyString, UpdateUrgency.Optional);
            Log.d(TAG, "Received system update status [" + status
                    + "] , urgency [" + urgencyString + "], nextCheckInTime [" +  nextCheckInTime + "]");
            switch (status) {
                case STATUS_UPDATE_NOT_AVAILABLE:
                    notifyNoUpdateAvailable(mManualCheck);
                    break;
                case STATUS_UPDATE_AVAILABLE:
                    notifyUpdateAvailable(urgency, mManualCheck);
                    break;
                case STATUS_CHECK_IN_FAILED:
                    notifyUpdateCheckFailed(mManualCheck);
                    break;
                case STATUS_DOWNLOADING_UPDATE:
                case STATUS_PAYLOAD_DOWNLOADING:
                case STATUS_PAYLOAD_VERIFYING:
                case STATUS_PAYLOAD_VERIFIED:
                    Log.d(TAG, "System update status [" + status
                            + "] with progress [" + progress + "] handled in background.");
                    break;
                case STATUS_DOWNLOAD_FAILED_NO_SPACE:
                case STATUS_DOWNLOAD_FAILED:
                case STATUS_PAYLOAD_DOWNLOAD_FAILED:
                case STATUS_PAYLOAD_VERIFY_FAILED:
                case STATUS_PAYLOAD_APPLY_FAILED:
                case STATUS_UPDATE_FAILED:
                    notifyUpdateDownloadFailed(urgency, mManualCheck);
                    break;
                case STATUS_PAYLOAD_UPDATED_NEED_REBOOT:
                    notifyUpdateDownload(urgency, mManualCheck);
                    break;
                default:
                    Log.d(TAG, "Invalid system update status: " + status);
            }
        }
    };
}