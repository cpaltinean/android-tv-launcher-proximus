package tv.threess.threeready.data.proximus.vod.observable;

import android.content.Context;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.ObservableEmitter;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.api.vod.wrapper.VodRentOffers;

/**
 * This base observable which fetch the {@link VodRentOffers} object for .
 *
 * @param <TModel> is model object.
 */
public abstract class BaseVodRentOfferObservable<TModel> extends BaseVodObservable<TModel> {

    protected final IBaseVodItem mVod;

    public BaseVodRentOfferObservable(Context context, IBaseVodItem vod) {
        super(context);
        mVod = vod;
    }

    @Override
    public void subscribe(ObservableEmitter<TModel> emitter) throws Exception {
        super.subscribe(emitter);
        publishData(emitter, getVodVariantRentOffers());
    }

    protected abstract void publishData(ObservableEmitter<TModel> emitter, VodRentOffers rentOffers) throws IOException;

    protected VodRentOffers getVodVariantRentOffers() throws IOException {
        if (mVod == null) {
            return null;
        }

        IVodItem vod;
        if (!(mVod instanceof IVodItem)) {
            vod = getCompleteVod(mVod);
        } else {
            vod = (IVodItem) mVod;
        }

        int creditCount = mVodProxy.getAvailableCreditCount();

        Map<String, IRentOffer> variantOfferMap = new HashMap<>();
        for (IVodVariant variant : vod.getVariantList()) {
            if (variant.isRentable() || variant.isSubscribed()) {
                IRentOffer offer = mVodProxy.getRentOffer(variant);
                // Check if currently available.
                if (offer != null) {
                    variantOfferMap.put(variant.getId(), offer);
                }
            }
        }

        return new VodRentOffers(vod, creditCount, variantOfferMap);
    }
}
