package tv.threess.threeready.data.proximus.home.model.module;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.gms.model.EditorialAppInfo;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleDataType;
import tv.threess.threeready.data.proximus.home.model.editorial.FeaturedAppEditorialItemProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Module config for a stripe which shows featured applications.
 *
 * @author Barabas Attila
 * @since 6/25/21
 */
public class FeaturedAppsSwimlaneModuleProximus extends SwimlaneModuleConfigProximus {
    private final List<EditorialItem> mEditorialItemList = new ArrayList<>();

    public FeaturedAppsSwimlaneModuleProximus(SwimLaneProximus swimlane) {
        super(swimlane);
    }

    @Override
    public ModuleDataType getDataType() {
        return ModuleDataType.EDITORIAL;
    }

    @Nullable
    @Override
    public ModuleDataSource getDataSource() {
        return null;
    }

    @Override
    public List<EditorialItem> getEditorialItems() {
        if (mEditorialItemList.isEmpty()) {
            Collection<EditorialAppInfo> featuredApps = ArrayUtils.notNull(
                    Components.get(AppConfig.class).getPlayApps().getFeatured());

            for (EditorialAppInfo appInfo : featuredApps) {
                mEditorialItemList.add(new FeaturedAppEditorialItemProximus(appInfo));
            }

            // Add editorials from swimlane.
            List<EditorialItem> swimlaneEditorials = super.getEditorialItems();
            if (swimlaneEditorials != null) {
                mEditorialItemList.addAll(swimlaneEditorials);
            }
        }
        return mEditorialItemList;
    }
}
