package tv.threess.threeready.data.proximus.account.model.account.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.data.proximus.account.model.account.AccountUserDefinedFieldProximus;

/**
 * Java type adapter to read string as {@link AccountUserDefinedFieldProximus.Key}
 *
 * @author Barabas Attila
 * @since 2020.04.06
 */
public class AccountUserDefinedFieldAdapterProximus extends XmlAdapter<String, AccountUserDefinedFieldProximus.Key> {

    @Override
    public AccountUserDefinedFieldProximus.Key unmarshal(String v) {
        for (AccountUserDefinedFieldProximus.Key key : AccountUserDefinedFieldProximus.Key.values()) {
            if (key.fieldName().equals(v)) {
                return key;
            }
        }
        return null;
    }

    @Override
    public String marshal(AccountUserDefinedFieldProximus.Key v) {
        throw new IllegalStateException("Not implemented.");
    }
}
