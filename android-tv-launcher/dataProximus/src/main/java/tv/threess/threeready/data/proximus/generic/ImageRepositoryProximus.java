package tv.threess.threeready.data.proximus.generic;

import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.ImageRepository;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pvr.model.IBaseRecordingSeries;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.ImageSource;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IBaseVodSeries;
import tv.threess.threeready.data.account.AccountLocalCache;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * The implementation of Proximus specific image handler methods.
 * {@see ImageHandler}
 *
 * @author Barabas Attila
 * @since 2020.03.09
 */
public class ImageRepositoryProximus implements ImageRepository {
    private static final String TAG = Log.tag(ImageRepositoryProximus.class);

    private static final String VOD_POSTER_SERVER = "Poster-Movie-HighQ";
    private static final String VOD_BACKGROUND_SERVER = "Poster-MovieBG-HighQ";

    private static final String EPG_SERVER = "Poster-EPG-HighQ";
    private static final String DEFAULT_SERVER = "Poster-Default-HighQ";
    private static final String APP_SERVER = "Poster-APP-HighQ";
    private static final String BANNER_SERVER = "Poster-Banner-HighQ";

    private final AccountLocalCache mAccountCache = Components.get(AccountLocalCache.class);

    @Override
    public String getLogoUrl(TvChannel channel, int width, int height) {
        return getBestPossibleUrl(channel.getChannelLogoUrl(), width, height, DEFAULT_SERVER);
    }

    @Override
    public String getCoverUrl(IBaseContentItem contentItem, int width, int height) {
        List<IImageSource> imageSources = contentItem.getImageSources();

        IImageSource.Type imageType = resolveImageType(contentItem, width, height);
        String serverName = resolveServerName(contentItem, imageType);

        return getMatchedUrl(imageSources, width, height, imageType, serverName);
    }

    @Override
    public String getCoverUrlForVoiceSearch(String imageName, int width, int height, boolean isVod) {
        if (imageName.isEmpty()) {
            //No images available
            return null;
        }

        return getBestPossibleUrl(imageName, width, height, (isVod ? VOD_POSTER_SERVER : EPG_SERVER));
    }

    @Override
    public String getImageSourceUrl(IImageSource imageSource, int width, int height) {
        if (imageSource == null) {
            return null;
        }

        Log.d(TAG, "getImageSourceUrl.  Url : "
                + imageSource.getUrl() + ", type : " + imageSource.getType());
        switch (imageSource.getType()) {
            default:
                return Components.get(Translator.class).get(imageSource.getUrl());
            case APP:
                return getBestPossibleUrl(imageSource.getUrl(), width, height, APP_SERVER);
            case BANNER:
                return getBestPossibleUrl(imageSource.getUrl(), width, height, BANNER_SERVER);
            case DEFAULT:
                return getBestPossibleUrl(imageSource.getUrl(), width, height, DEFAULT_SERVER);
        }
    }

    /**
     * @param filteredImageUrl Image source with required type.
     * @param width            Required width.
     * @param height           Required height.
     * @return Best possible url built from server url + image url.
     */
    private String getBestPossibleUrl(String filteredImageUrl, int width, int height, String serverName) {
        return mAccountCache.getBestPossibleUrl(filteredImageUrl, width, height, serverName);
    }

    /**
     * Resolve the type of the image of which needs to be loaded. E.g poster, background etc.
     *
     * @param contentItem The content item for which the image needs to be loaded.
     * @param width       The expected width of the image in pixel.
     * @param height      The expected height of the image in pixel.
     * @return The type of the image which needs to be loaded.
     */
    private IImageSource.Type resolveImageType(IBaseContentItem contentItem, int width, int height) {
        if (contentItem instanceof IBaseVodItem) {
            return width > height ? IImageSource.Type.BACKGROUND : IImageSource.Type.COVER;
        }

        if (contentItem instanceof IBaseVodSeries) {
            return IImageSource.Type.DEFAULT;
        }

        return IImageSource.Type.COVER;
    }

    /**
     * Resolve the name of the image resize server for a given content and image type.
     *
     * @param contentItem The data for which the image needs to be loaded.
     * @param type        The type of the image which needs to be loaded.
     * @return The name of the server to lookup the parameters and url.
     */
    private String resolveServerName(IBaseContentItem contentItem, IImageSource.Type type) {
        if (contentItem instanceof IBroadcast
                || contentItem instanceof IBaseRecordingSeries) {
            return EPG_SERVER;
        }

        if (contentItem instanceof IBaseVodItem) {
            return type == IImageSource.Type.BACKGROUND
                    ? VOD_BACKGROUND_SERVER : VOD_POSTER_SERVER;
        }

        return DEFAULT_SERVER;
    }

    /**
     * Select the best image sources which matches the given width, height and the expected image type.
     *
     * @param images The image sources to choose from.
     * @param width  The expected width or 0 if not defined.
     * @param height The expected height or 0 if not defined.
     * @param type   The type of the image. E.g cover or background.
     * @return The url which is closes to the regiments.
     */
    private String getMatchedUrl(List<IImageSource> images, int width, int height,
                                 IImageSource.Type type, String serverName) {
        if (images == null || images.isEmpty()) {
            //No images available
            return null;
        }

        IImageSource filteredSource = images.stream()
                .filter(source -> source.getType() == type)
                .findFirst()
                .orElse(new ImageSource());

        return getBestPossibleUrl(filteredSource.getUrl(), width, height, serverName);
    }
}
