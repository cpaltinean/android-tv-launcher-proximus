package tv.threess.threeready.data.proximus.playback.model.response;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;

import tv.threess.threeready.api.playback.model.IReplayOttStreamingSession;
import tv.threess.threeready.data.generic.model.BaseResponse;
import tv.threess.threeready.data.proximus.playback.adapter.BucketTimeSafeTypeAdapter;
import tv.threess.threeready.data.proximus.playback.adapter.BucketTimeTypeAdapter;

/**
 * Proximus implementation of the replay playback session.
 *
 * @author Barabas Attila
 * @since 2020.05.13
 */
public class ReplayStreamingSessionProximus implements IReplayOttStreamingSession, BaseResponse {
    @JsonAdapter(BucketTimeTypeAdapter.class)
    @SerializedName("airingStart")
    private long mAiringStart;

    @SerializedName("url")
    private String mUrl;

    @SerializedName("mimeType")
    private String mimeType;

    @SerializedName("bucket")
    private Bucket mBucket;

    @SerializedName("resInfo")
    private ResInfo mResInfo;

    @SerializedName("drm")
    private Drm mDrm;

    @Override
    public String getId() {
        return mBucket.mId;
    }

    @Override
    public String getUrl() {
        return mUrl;
    }

    @Override
    public String getMimeType() {
        return mimeType;
    }

    @Override
    public String getLicenseUrl() {
        for (Drm.Licence licence : mDrm.mLicence) {
            if (licence.mDRMProtocol == Drm.DRMProtocol.Widevine) {
                return licence.mLicenceUrl;
            }
        }
        return "";
    }

    public long getStartTime() {
        return mBucket.mTimeSlot.mStart;
    }

    public long getEndTime() {
        return mBucket.mTimeSlot.mEnd;
    }

    @Override
    public boolean isTimeIncluded(long time) {
        return getStartTime() < time && getEndTime() >= time;
    }

    @Override
    public List<? extends IBlacklistSlot> getBlacklistedSlots() {
        if (mResInfo == null || mResInfo.mSlots == null) {
            return Collections.emptyList();
        }
        return mResInfo.mSlots;
    }

    private static class Bucket {
        @SerializedName("id")
        private String mId;

        @SerializedName("timeslot")
        private TimeSlot mTimeSlot;

        private static class TimeSlot {

            @JsonAdapter(BucketTimeTypeAdapter.class)
            @SerializedName("start")
            private long mStart;

            @JsonAdapter(BucketTimeTypeAdapter.class)
            @SerializedName("end")
            private long mEnd;
        }
    }

    private static class ResInfo {
        @SerializedName("slots")
        private List<Slot> mSlots;

        private static class Slot implements IBlacklistSlot {
            @JsonAdapter(BucketTimeSafeTypeAdapter.class)
            @SerializedName("start")
            private long mStart;

            @JsonAdapter(BucketTimeSafeTypeAdapter.class)
            @SerializedName("end")
            private long mEnd;

            @JsonAdapter(BucketTimeSafeTypeAdapter.class)
            @SerializedName("noEntryAfter")
            private long noEntryAfter;

            @SerializedName("reason")
            private String mReason;

            @Override
            public boolean isActive(long playbackStartTime) {
                if (!BucketTimeTypeAdapter.isValid(noEntryAfter, mStart, mEnd)) {
                    // not parsed, or parsing failed
                    return false;
                }
                return playbackStartTime > noEntryAfter;
            }

            @Override
            public long getStart() {
                return mStart;
            }

            @Override
            public long getEnd() {
                return mEnd;
            }

            @Override
            public String getReason() {
                return mReason;
            }
        }
    }

    private static class Drm {
        @SerializedName("licenseAcqUrls")
        private List<Licence> mLicence;

        private static class Licence {
            @SerializedName("protocol")
            private DRMProtocol mDRMProtocol;

            @SerializedName("url")
            private String mLicenceUrl;
        }

        private enum DRMProtocol {
            @SerializedName("PlayReady")
            PlayReady,

            @SerializedName("Widevine")
            Widevine
        }
    }

    @Override
    public long getAccurateStartTime() {
        return mAiringStart;
    }
}
