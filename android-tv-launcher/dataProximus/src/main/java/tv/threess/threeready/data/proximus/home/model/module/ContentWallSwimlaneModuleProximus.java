package tv.threess.threeready.data.proximus.home.model.module;

import java.util.Collections;
import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.ApiConfig;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleDataSourcePaginationParams;
import tv.threess.threeready.api.home.model.module.ModuleType;
import tv.threess.threeready.data.home.model.module.ModuleDataSourcePaginationParams3Ready;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneSortOptionProximus;

/**
 * ModuleConfig used for content wall, to display the stripe also as a collection.
 */
public class ContentWallSwimlaneModuleProximus extends SwimlaneModuleConfigProximus {

    private final ModuleDataSourcePaginationParams mPagination;
    private final DataSourceSelector mSortOptionSelector;

    public ContentWallSwimlaneModuleProximus(SwimLaneProximus swimlane) {
        super(swimlane);
        int pageSize = getPageDefaultPageSize(swimlane);
        mPagination = new ModuleDataSourcePaginationParams3Ready(pageSize, pageSize / 2);
        mSortOptionSelector = new DataSourceSelector((List) swimlane.getSortOptions());
    }

    @Override
    public ModuleType getType() {
        return ModuleType.COLLECTION;
    }

    @Override
    public ModuleDataSourcePaginationParams getPagination() {
        return mPagination;
    }

    protected int getPageDefaultPageSize(SwimLaneProximus swimLane) {
        for (SwimLaneSortOptionProximus sortOption : ArrayUtils.notNull(swimLane.getSortOptions())) {
            if (sortOption.isDefault()) {
                return sortOption.getPageSize();
            }
        }

        return Components.get(ApiConfig.class).getDefaultPageSize();
    }


    @Override
    public List<EditorialItem> getEditorialItems() {
        // No editorial tiles in collections.
        return Collections.emptyList();
    }

    /**
     * @return A list with all the filtering and sorting options of the content wall.
     */
    public List<DataSourceSelector> getSelectors() {
        return Collections.singletonList(mSortOptionSelector);
    }
}
