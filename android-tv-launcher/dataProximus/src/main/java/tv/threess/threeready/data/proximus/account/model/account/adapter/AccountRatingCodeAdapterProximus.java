package tv.threess.threeready.data.proximus.account.model.account.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.api.pc.model.ParentalRating;

/**
 * Java type adapter to read string field as {@link tv.threess.threeready.data.proximus.account.model.account.RatingProximus}
 *
 * @author Daniela Toma
 * @since 2020.04.15
 */
public class AccountRatingCodeAdapterProximus extends XmlAdapter<String, Integer> {

    @Override
    public String marshal(Integer v) {
        switch (v) {
            case 0:
            case 10:
                return "G";
            case 12:
                return "10";
            case 14:
            case 16:
                return "12";
            case 18:
                return "16";
        }
        return Integer.toString(v);
    }

    @Override
    public Integer unmarshal(String v) {
        switch (v) {
            case "G":
                return ParentalRating.Rated10.getMinimumAge();
            case "10":
                return ParentalRating.Rated12.getMinimumAge();
            case "12":
            case "14":
                return ParentalRating.Rated16.getMinimumAge();
            case "16":
                return ParentalRating.Rated18.getMinimumAge();
            case "0":
                return ParentalRating.RatedAll.getMinimumAge();
            default:
                return ParentalRating.Undefined.getMinimumAge();
        }
    }
}
