package tv.threess.threeready.data.proximus.generic.interceptor;

import android.app.Application;
import android.net.Uri;

import tv.threess.lib.di.Components;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.proximus.iam.IAMOkHttpInterceptor;
import tv.threess.threeready.data.proximus.iam.IAMRetrofitProximus;

/**
 * Replace the request base url with the value from boot config
 * and add generic BTA request parameters.
 *
 * @author Barabas Attila
 * @since 2020.03.23
 */
public class BTAOkHttpInterceptor extends IAMOkHttpInterceptor {
    private static final String PARAM_KEY_MAC = "MAC";
    private static final String PARAM_KEY_VERSION = "InterfaceVersion";
    private static final String PARAM_VALUE_VERSION = "5.7.2"; // BTA version.

    private final String mMacAddress;

    public BTAOkHttpInterceptor(Application app, IAMRetrofitProximus iamRetrofit) {
        super(app, iamRetrofit);
        mMacAddress = Components.get(MwProxy.class).getEthernetMacAddress();
    }

    @Override
    protected void overrideRequestUri(Uri.Builder uriBuilder) {
        uriBuilder
                .appendQueryParameter(PARAM_KEY_MAC, mMacAddress)
                .appendQueryParameter(PARAM_KEY_VERSION, PARAM_VALUE_VERSION);
    }
}
