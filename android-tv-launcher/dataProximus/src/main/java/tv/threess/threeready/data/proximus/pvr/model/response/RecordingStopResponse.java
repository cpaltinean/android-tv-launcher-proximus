package tv.threess.threeready.data.proximus.pvr.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * Retrofit response class for stopping an ongoing recording.
 *
 * Created by Noemi Dali on 23.04.2020.
 */
@XmlRootElement(name = "BTAResponse")
public class RecordingStopResponse extends BTAResponseProximus {

    @XmlElement(name = "UserRecordId")
    private String mUserRecordId;

    @XmlElement(name = "UserStopTimeMarker")
    private long mStopTimeMarker;

    public String getUserRecordId() {
        return mUserRecordId;
    }

    public long getStopTimeMarker() {
        return mStopTimeMarker;
    }
}
