package tv.threess.threeready.data.proximus.log;

import tv.threess.threeready.api.log.Reporter;
import tv.threess.threeready.api.log.ReporterProvider;
import tv.threess.threeready.data.proximus.log.reporter.HeartBeatReporterProximus;
import tv.threess.threeready.data.proximus.log.reporter.MetricReporterProximus;
import tv.threess.threeready.data.proximus.log.reporter.PlaybackEventReporterProximus;
import tv.threess.threeready.data.proximus.log.reporter.UIEventReporterProximus;

/**
 * Class which provides the proximus specific reporters.
 *
 * @author Zsolt Bokor_
 * @since 2020.05.29
 */
public class ReporterProviderProximus implements ReporterProvider {

    private final Class<? extends Reporter>[] mReporters = new Class[] {
            UIEventReporterProximus.class,
            PlaybackEventReporterProximus.class,
            MetricReporterProximus.class,
            HeartBeatReporterProximus.class
    };

    @Override
    public Class<? extends Reporter>[] getReporters() {
        return mReporters;
    }
}
