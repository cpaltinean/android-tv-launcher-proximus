package tv.threess.threeready.data.proximus.playback.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;

import tv.threess.threeready.api.playback.model.IOttStreamingSession;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * Base implementation for all proximus related OTT streaming session.
 *
 * @author Barabas Attila
 * @since 2018.10.22
 */
public abstract class BTAStreamingSessionProximus extends BTAResponseProximus implements IOttStreamingSession {
    private final String mId;

    @XmlElement(name = "Http")
    private Http mHttp;

    @XmlElement(name = "MatchedMTP")
    private MatchedMTP matchedMTP;

    public BTAStreamingSessionProximus() {
        mId = UUID.randomUUID().toString();
    }

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public String getUrl() {
        if (mHttp == null) {
            return "";
        }

        return mHttp.mUrl;
    }

    @Override
    public String getMimeType() {
        if (matchedMTP == null) {
            return "";
        }
        return matchedMTP.mimeType;
    }

    @Override
    public String getLicenseUrl() {
        if (mHttp == null || mHttp.mLicenseAcquisitionInfos == null) {
            return "";
        }

        for (LicenseAcquisitionInfo la : mHttp.mLicenseAcquisitionInfos) {
            if (la.mDRMProtocol == DRMProtocol.Widevine) {
                return la.mLicenceUrl;
            }
        }
        return "";
    }

    @Override
    public boolean isTimeIncluded(long time) {
        return true;
    }

    @JacksonXmlRootElement(localName = "Http")
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Http {

        @JacksonXmlProperty(localName = "URL")
        private String mUrl;

        @JacksonXmlElementWrapper(localName = "LicenseAcquisitionInfo")
        @JacksonXmlProperty(localName = "LA")
        private ArrayList<LicenseAcquisitionInfo> mLicenseAcquisitionInfos;
    }


    private static class LicenseAcquisitionInfo {
        @JacksonXmlProperty(localName = "LAURL")
        private String mLicenceUrl;

        @JacksonXmlProperty(localName = "DRMProtocol")
        private DRMProtocol mDRMProtocol;
    }

    private enum DRMProtocol {
        @JacksonXmlProperty(localName = "PlayReady")
        PlayReady,

        @JacksonXmlProperty(localName = "Widevine")
        Widevine
    }

    @JacksonXmlRootElement(localName = "MatchedMTP")
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class MatchedMTP {

        @JacksonXmlProperty(localName = "MTPexternalID")
        private String mtpExternalId;

        @JacksonXmlProperty(localName = "MIMEType")
        private String mimeType;

        @JacksonXmlProperty(localName = "DRMSystemExternalIDs")
        private String drmExternalIds;

        @JacksonXmlProperty(localName = "IsEncrypted")
        private boolean isEncrypted;
    }
}
