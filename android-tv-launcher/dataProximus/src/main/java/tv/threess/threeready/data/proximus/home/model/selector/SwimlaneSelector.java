package tv.threess.threeready.data.proximus.home.model.selector;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneQueryParamProximus;

/**
 * Selection option used for the swimlanes.
 *
 * @author David Bondor
 * @since 4.28.2022
 */
public interface SwimlaneSelector extends SelectorOption, Serializable {

    /**
     * @return The key in the content swimlane query name to replace.
     */
    @Nullable
    String getTokenName();

    /**
     * @return The value to replace in the content swimlane query name.
     */
    @Nullable
    String getTokenValue();

    /**
     * @return The extra parameters for the selector option to add to the content swimlane params.
     */
    List<SwimLaneQueryParamProximus> getParameters();
}
