package tv.threess.threeready.data.proximus.search.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneContentResponseProximus;
import tv.threess.threeready.data.proximus.search.retrofit.response.SearchSuggestionsResponse;
import tv.threess.threeready.data.proximus.vod.model.response.VodShopLanguageProximus;

/**
 * Retrofit service for search.
 *
 * Created by Noemi Dali on 06.05.2020.
 */
public interface BTASearchRetrofitProximus {

    @GET("/broker/bta/suggestions")
    Call<SearchSuggestionsResponse> getSearchSuggestions(@Query("term") String searchTerm,
                                                         @Query("fc_ClientType") int client,
                                                         @Query("fc_Language") VodShopLanguageProximus language);

    @GET("/broker/bta/search")
    Call<SwimLaneContentResponseProximus> doSearch(@Query("term") String searchTerm,
                                                   @Query("fc_Language") VodShopLanguageProximus language,
                                                   @Query("fc_ClientType") int client,
                                                   @Query("selector") String selector);

    @GET("/broker/bta/search")
    Call<SwimLaneContentResponseProximus> doSearch(@Query("term") String searchTerm,
                                                   @Query("fc_ClientType") int client,
                                                   @Query("selector") String selector);
}
