package tv.threess.threeready.data.proximus.account.model.boot;

import androidx.annotation.Nullable;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Backend response which contains all the available broken documents and versions for them.
 *
 * @author Barabas Attila
 * @since 6/7/21
 */
@XmlRootElement(name = "DocumentUpdate")
public class AvailableVersionResponseProximus {
    private static final String GLOBAL_INSTALL_NAME = "GLOBALINSTALL";

    @XmlElement(name = "BrokerResponse")
    private BrokenResponse mBrokenResponse;

    /**
     * @return The version of the global install document or null if not available.
     */
    @Nullable
    public String getGlobalInstallVersion() {
        if (mBrokenResponse == null
                || mBrokenResponse.mVersionContentList == null) {
            return null;
        }

        for (VersionedContent content : mBrokenResponse.mVersionContentList) {
            if (GLOBAL_INSTALL_NAME.equals(content.mName)) {
                return content.mVersion;
            }
        }

        return null;
    }

    @XmlRootElement(name = "BrokerResponse")
    private static class BrokenResponse {

        @XmlElementWrapper(name = "Versions")
        @XmlElement(name = "VersionedContent")
        private List<VersionedContent> mVersionContentList;
    }

    @XmlRootElement(name = "VersionedContent")
    private static class VersionedContent {
        @XmlElement(name = "Name")
        private String mName;

        @XmlElement(name = "Version")
        private String mVersion;
    }
}
