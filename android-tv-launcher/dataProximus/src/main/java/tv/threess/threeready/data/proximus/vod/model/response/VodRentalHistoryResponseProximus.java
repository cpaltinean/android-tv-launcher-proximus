package tv.threess.threeready.data.proximus.vod.model.response;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.vod.model.IVodRentalTransaction;
import tv.threess.threeready.data.proximus.account.model.account.RatingTypesProximus;
import tv.threess.threeready.data.proximus.generic.adapter.ParentalRatingAdapterProximus;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * BTA backend response of VOD rental history response.
 *
 * @author Daniela Toma
 * @since 2022.02.08
 */
@XmlRootElement(name = "BTAResponse")
public class VodRentalHistoryResponseProximus extends BTAResponseProximus {

    @XmlElement(name = "SubscriberBillingInfo")
    private SubscribeBillingInfoProximus mSubscribeBillingInfo;

    public List<RentalTransactionProximus> getRentalTransactionList() {
        return mSubscribeBillingInfo.getRentalTransactionList();
    }

    @XmlRootElement(name = "SubscriberBillingInfo")
    public static class SubscribeBillingInfoProximus implements Serializable {

        @XmlElement(name = "SubscriberAccountNumber")
        private String mSubscriberAccountNumber;

        @XmlElement(name = "Name")
        private String mName;

        @XmlElement(name = "AccountHistoryDays")
        private String mAccountHistoryDays;

        @XmlElementWrapper(name = "RentalTransactionList")
        @XmlElement(name = "RentalTransaction")
        private List<RentalTransactionProximus> mRentalTransactionList;

        public List<RentalTransactionProximus> getRentalTransactionList() {
            if (mRentalTransactionList == null) {
                return Collections.emptyList();
            }
            return mRentalTransactionList;
        }
    }

    @XmlRootElement(name = "RentalTransaction")
    public static class RentalTransactionProximus implements IVodRentalTransaction {

        @XmlElement(name = "DateTime")
        private long mDateTime;

        @XmlElement(name = "ExpDateTime")
        private long mExpirationDateTime;

        @XmlElement(name = "ContentItemName")
        private String mTitle;

        @XmlElement(name = "ChargeAmount")
        private float mAmount;

        @XmlElement(name = "ContentRatingType")
        private RatingTypesProximus mRatingType;

        @XmlJavaTypeAdapter(ParentalRatingAdapterProximus.class)
        @XmlElement(name = "ContentRating")
        private ParentalRating mRatingCode;

        public RentalTransactionProximus() {
            super();
        }

        @Override
        public long getPurchaseDate() {
            return mDateTime;
        }

        public long getExpirationDateTime() {
            return mExpirationDateTime;
        }

        @Override
        public String getTitle() {
            return mTitle;
        }

        @Override
        public float getAmount() {
            return mAmount;
        }

        @Override
        public ParentalRating getRatingCode() {
            return mRatingCode == null ? ParentalRating.Undefined : mRatingCode;
        }

        public RatingTypesProximus getRatingType() {
            return mRatingType;
        }
    }
}

