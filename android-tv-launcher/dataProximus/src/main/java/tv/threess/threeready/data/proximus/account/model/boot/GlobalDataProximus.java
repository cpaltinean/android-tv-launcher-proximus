package tv.threess.threeready.data.proximus.account.model.boot;

import androidx.annotation.Nullable;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.proximus.account.model.boot.adapter.DateTimeTypeAdapter;
import tv.threess.threeready.data.proximus.account.model.server.AdvertiseServerInfoProximus;
import tv.threess.threeready.data.proximus.account.model.server.DataCollectionServerInfoProximus;
import tv.threess.threeready.data.proximus.account.model.server.PosterServerInfoProximus;
import tv.threess.threeready.data.proximus.account.model.server.ServerInfoProximus;
import tv.threess.threeready.data.proximus.account.model.server.ServerType;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodGlobalCategoryProximus;

/**
 * BTA global data backend response.
 *
 * @author Barabas Attila
 * @since 2020.03.30
 */
@XmlRootElement(name = "DocumentUpdate")
public class GlobalDataProximus extends BTAResponseProximus implements Serializable {

    @XmlElement(name = "GLOBAL_INSTALL")
    private GlobalInstall mGlobalInstall;

    @XmlElement(name = "version")
    private String mVersion;

    /**
     * @return The version of the document.
     */
    public String getVersion() {
        return mVersion;
    }

    /**
     * @return The number of days available in the EPG for past.
     */
    public int getPastEPGDays() {
        return mGlobalInstall.mPastEPGDays;
    }

    /**
     * @return The number of days available in the EPG for future.
     */
    public int getFutureEPGDays() {
        return mGlobalInstall.mFutureEPGDays;
    }

    /**
     * @return The maximum price of content which can be purchase with a credit.
     */
    public double getCreditPriceLimit() {
        return mGlobalInstall.creditPriceLimit;
    }

    /**
     * @return Remote Update Poll Start Time in Millis. (Hours + Minutes)
     */
    public Long getRemoteUpdatePollStartTime() {
        return mGlobalInstall.mRemoteUpdatePollStartTime;
    }

    /**
     * @return Remote Update Poll End Time in Millis. (Hours + Minutes)
     */
    public Long getRemoteUpdatePollEndTime() {
        return mGlobalInstall.mRemoteUpdatePollEndTime;
    }

    /**
     * @return A list with proximus advertise servers.
     */
    public List<AdvertiseServerInfoProximus> getAdvertiseServerList(ServerType filter) {
        return mGlobalInstall.getServerList(filter, AdvertiseServerInfoProximus::new);
    }

    /**
     * @return A list with proximus poster servers.
     */
    public List<PosterServerInfoProximus> getPosterServerList() {
        return mGlobalInstall.getServerList(ServerType.POSTER_SERVER, PosterServerInfoProximus::new);
    }

    /**
     * @return A list with proximus data collection servers.
     */
    public List<IDataCollectionServerInfo> getDataCollectionServerList() {
        return mGlobalInstall.getServerList(ServerType.UDC, DataCollectionServerInfoProximus::new);
    }

    /**
     * @return The definition of the banner feedback server.
     */
    @Nullable
    public ServerInfoProximus getBannerFeedbackServer() {
        List<ServerInfoProximus> serverInfoList =
                mGlobalInstall.getServerList(ServerType.BANNER_FEEDBACK, ServerInfoProximus::new);
        if (serverInfoList.isEmpty()) {
            return null;
        }

        return serverInfoList.get(0);
    }

    /**
     * return the BandwidthProfile matched by profileName
     */
    @NotNull
    public Collection<BandwidthProfileProximus> getBandwidthProfiles() {
        return ArrayUtils.notNull(mGlobalInstall.mBandwidthProfiles);
    }

    /**
     * return the HardwareVersion matching the current hardware
     */
    public HardwareVersionProximus getHardwareVersion() {
        if (mGlobalInstall.mSTBVersions == null) {
            return null;
        }

        String versionName = Components.get(MwProxy.class).getHardwareVersion();
        for (HardwareVersionProximus version : ArrayUtils.notNull(mGlobalInstall.mSTBVersions.mHWVersions)) {
            if (versionName.equals(version.getVersion())) {
                return version;
            }
        }
        return null;
    }

    /**
     * @return A list with all the defined VOD categories on the backend.
     */
    public List<VodGlobalCategoryProximus> getCategories() {
        return mGlobalInstall.mCategories;
    }

    @XmlRootElement(name = "GLOBAL_INSTALL")
    private static class GlobalInstall {
        @XmlElement(name = "OldIPGDisplayABR")
        private int mPastEPGDays;

        @XmlElement(name = "IPGDisplay")
        private int mFutureEPGDays;

        @XmlElement(name = "FreeRentalCreditLimit")
        private float creditPriceLimit;

        @XmlJavaTypeAdapter(DateTimeTypeAdapter.class)
        @XmlElement(name = "RemoteUpdatePollStartTime")
        private Long mRemoteUpdatePollStartTime;

        @XmlJavaTypeAdapter(DateTimeTypeAdapter.class)
        @XmlElement(name = "RemoteUpdatePollEndTime")
        private Long mRemoteUpdatePollEndTime;

        @XmlElementWrapper(name = "ServerList")
        @XmlElement(name = "Server")
        private List<ServerInfoProximus> mServerList;

        @XmlElement(name = "STBVersions")
        private StbVersionsProximus mSTBVersions;

        @XmlElementWrapper(name = "BandwidthProfiles")
        @XmlElement(name = "BandwidthProfile")
        private List<BandwidthProfileProximus> mBandwidthProfiles;

        @XmlElementWrapper(name = "CategoryList")
        @XmlElement(name = "Category")
        private List<VodGlobalCategoryProximus> mCategories;

        protected<T> List<T> getServerList(ServerType filter, Function<ServerInfoProximus, ? extends T> mapper) {
            List<T> result = new ArrayList<>();
            for (ServerInfoProximus serverInfoProximus : ArrayUtils.notNull(mServerList)) {
                if (serverInfoProximus.getType() == filter) {
                    result.add(mapper.apply(serverInfoProximus));
                }
            }
            return result;
        }
    }
}
