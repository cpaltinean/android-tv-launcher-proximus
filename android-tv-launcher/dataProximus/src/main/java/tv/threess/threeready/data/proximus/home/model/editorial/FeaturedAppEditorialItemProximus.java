package tv.threess.threeready.data.proximus.home.model.editorial;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.gms.model.EditorialAppInfo;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemStyle;

/**
 * Editorial tile for a 3rd party application.
 *
 * @author Barabas Attila
 * @since 6/25/21
 */
public class FeaturedAppEditorialItemProximus implements EditorialItem {
    private final EditorialAppInfo mAppInfo;

    public FeaturedAppEditorialItemProximus(EditorialAppInfo appInfo) {
        mAppInfo = appInfo;
    }

    @Override
    public EditorialItemAction getAction() {
        return mItemAction;
    }

    @Nullable
    @Override
    public String getFallbackTitle() {
        return mAppInfo.getName();
    }

    @Override
    public String getId() {
        return mAppInfo.getPackageName();
    }

    @Nullable
    @Override
    public EditorialItemStyle getStyle() {
        return mItemStyle;
    }

    private final EditorialItemStyle mItemStyle = new EditorialItemStyle() {
        private final Size mSize = new VariantSize(VariantSize.Variant.App);
        @Nullable
        @Override
        public IImageSource getBackgroundImage() {
            return mAppInfo.getBanner();
        }

        @Nullable
        @Override
        public Size getHeight() {
            return mSize;
        }

        @Nullable
        @Override
        public Size getWidth() {
            return mSize;
        }
    };

    private final EditorialItemAction mItemAction = new EditorialItemAction.AppEditorialItemAction() {

        @Override
        public String getPackageName() {
            return mAppInfo.getPackageName();
        }
    };
}
