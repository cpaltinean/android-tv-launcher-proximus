package tv.threess.threeready.data.proximus.account.model.server;

import java.io.Serializable;
import java.util.List;

import tv.threess.threeready.api.account.model.server.IPosterServerInfo;
import tv.threess.threeready.api.account.model.server.IPosterServerInfoField;

/**
 * Proximus class which holds the proximus poster server related informations.
 *
 * Created by Noemi Dali on 01.04.2020.
 */
public class PosterServerInfoProximus extends ServerInfoProximus implements IPosterServerInfo, Serializable {
    private static final String IMAGE_SIZING = "ImageSizing";

    public PosterServerInfoProximus(ServerInfoProximus serverInfoProximus) {
        super(serverInfoProximus);
    }

    /**
     * @return a list with allowed image sizes.
     */
    @Override
    public List<? extends IPosterServerInfoField> getAllowedSizes() {
        return mAllowedSizes;
    }

    /**
     * @return True if server allows image sizing.
     */
    @Override
    public boolean isImageResizable() {
        return Boolean.parseBoolean(getAttributeValueForName(IMAGE_SIZING));
    }
}
