/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.account;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountServiceRepository;
import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.account.model.account.IConsent;
import tv.threess.threeready.api.account.model.account.IPackage;
import tv.threess.threeready.api.account.model.service.ConsentRequestScenario;
import tv.threess.threeready.api.account.setting.BatchSettingEditor;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.generic.helper.TimerLog;
import tv.threess.threeready.api.generic.receivers.BaseBroadcastReceiver;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.generic.receivers.StandByStateChangeReceiver;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.Reporter;
import tv.threess.threeready.api.log.model.ReporterInfo;
import tv.threess.threeready.data.account.AccountLocalCache;
import tv.threess.threeready.data.account.AccountService;
import tv.threess.threeready.data.account.BaseAccountRepository;
import tv.threess.threeready.data.generic.BaseProvider;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.home.HomeService;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.proximus.account.model.account.AccountInfoProximus;
import tv.threess.threeready.data.proximus.account.model.account.AccountInfoResponseProximus;
import tv.threess.threeready.data.proximus.account.model.account.DeviceResourceProximus;
import tv.threess.threeready.data.proximus.account.model.account.PackageInfoProximus;
import tv.threess.threeready.data.proximus.account.model.account.PackageNamesProximus;
import tv.threess.threeready.data.proximus.account.model.account.STBProperties;
import tv.threess.threeready.data.proximus.account.model.account.consent.ConsentProximus;
import tv.threess.threeready.data.proximus.account.model.boot.BandwidthProfileProximus;
import tv.threess.threeready.data.proximus.account.model.boot.BootConfigProximus;
import tv.threess.threeready.data.proximus.account.model.boot.BootFileNameProximus;
import tv.threess.threeready.data.proximus.account.model.boot.GlobalDataProximus;
import tv.threess.threeready.data.proximus.account.model.boot.HardwareVersionProximus;
import tv.threess.threeready.data.proximus.account.model.boot.IAMConfigProximus;
import tv.threess.threeready.data.proximus.account.model.boot.IDataCollectionServerInfo;
import tv.threess.threeready.data.proximus.account.model.boot.IGMPRejoinProximus;
import tv.threess.threeready.data.proximus.account.model.boot.TMServerInfoResponseProximus;
import tv.threess.threeready.data.proximus.account.model.boot.VQEConfigProximus;
import tv.threess.threeready.data.proximus.account.model.registration.request.DRMClientProximus;
import tv.threess.threeready.data.proximus.account.model.registration.request.STBRegistrationUpdateRequestProximus;
import tv.threess.threeready.data.proximus.account.model.registration.response.DRMRegisteredClientsResponseProximus;
import tv.threess.threeready.data.proximus.account.model.server.AdvertiseServerInfoProximus;
import tv.threess.threeready.data.proximus.account.model.server.InterfaceResponseProximus;
import tv.threess.threeready.data.proximus.account.model.server.RequestPropertyProximus;
import tv.threess.threeready.data.proximus.account.model.server.ServerInfoProximus;
import tv.threess.threeready.data.proximus.account.model.server.ServerType;
import tv.threess.threeready.data.proximus.account.retrofit.BootConfigRetrofitProximus;
import tv.threess.threeready.data.proximus.log.reporter.MetricReporterProximus;
import tv.threess.threeready.data.proximus.log.reporter.PlaybackEventReporterProximus;
import tv.threess.threeready.data.proximus.log.reporter.UIEventReporterProximus;
import tv.threess.threeready.data.proximus.mw.DhcpOptions;
import tv.threess.threeready.data.proximus.mw.MwProxyProximus;
import tv.threess.threeready.data.proximus.vod.VodCategoryCacheProximus;
import tv.threess.threeready.data.pvr.PvrContract;
import tv.threess.threeready.data.pvr.PvrService;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.data.tv.TvService;
import tv.threess.threeready.data.utils.CacheUtils;
import tv.threess.threeready.data.vod.VodContract;
import tv.threess.threeready.data.vod.VodService;

/**
 * Implementation of the Proximus specific config service handler methods.
 *
 * @author Barabas Attila
 * @since 2018.06.05
 */
public class AccountServiceRepositoryProximus extends BaseAccountRepository implements AccountServiceRepository, Component {
    private static final String TAG = Log.tag(AccountServiceRepositoryProximus.class);

    private static final int IMAGE_CACHE_VERSION = 1;

    private final AccountProxyProximus mAccountProxy = Components.get(AccountProxyProximus.class);
    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);

    private final VodCategoryCacheProximus mVodCategoryCache = Components.get(VodCategoryCacheProximus.class);

    private final TvCache mTvCache = Components.get(TvCache.class);
    private final AccountLocalCache mAccountCache = Components.get(AccountLocalCache.class);
    private  final MwProxy mMwProxy = Components.get(MwProxy.class);

    public AccountServiceRepositoryProximus(Application app) {
        super(app);

        // Detect standby state changes.
        Components.get(StandByStateChangeReceiver.class)
                .addStateChangedListener(mStandByStateChangeListener);
    }

    @Override
    public void performCaching() throws Exception {
        // Check and upgrade databases if needed.
        mApp.getContentResolver().call(TvContract.Channel.CONTENT_URI, BaseProvider.DATABASE_VERSION_CHECK, "", Bundle.EMPTY);
        mApp.getContentResolver().call(PvrContract.Recording.CONTENT_URI, BaseProvider.DATABASE_VERSION_CHECK, "", Bundle.EMPTY);
        mApp.getContentResolver().call(VodContract.Vod.CONTENT_URI, BaseProvider.DATABASE_VERSION_CHECK, "", Bundle.EMPTY);


        // Update boot config.
        bootUpdater.batchUpdate()
                .runIfExpired(this::cacheBootConfig)
                .runIfExpired(this::cacheInterfaceConfig)
                .run(() -> {
                    BaseWorkManager.start(mApp, PvrService.buildUpdateRecordingIntent());
                    BaseWorkManager.start(mApp, VodService.buildUpdatePurchasesIntent());
                    BaseWorkManager.start(mApp, AccountService.buildUpdateWatchlist());
                    BaseWorkManager.start(mApp, HomeService.buildUpdateConfigIntent());
                })
                .runIfExpired(this::cacheTMServerInfo)
                .done();

        // Update account info.
        accountUpdater.batchUpdate()
                .runIfExpired(this::cacheAccountInfo)
                .run(() -> {
                    BaseWorkManager.start(mApp, TvService.buildChannelUpdateIntent());
                    BaseWorkManager.start(mApp, TvService.buildBroadcastUpdateIntent());
                })
                .runIfExpired(this::cacheConsents)
                .done();

        // update STB properties.
        updateSTBProperties();

        // Update categories and content.
        BaseWorkManager.start(mApp, VodService.buildUpdateVodCategoriesIntent());
        BaseWorkManager.start(mApp, AccountService.buildUpdateGlobalInstallIntent());

        // Handle image cache upgrade.
        long cacheVersion = Settings.imageCacheVersion.get(0L);
        if (cacheVersion != IMAGE_CACHE_VERSION) {
            CacheUtils.clearImageCache(mApp);
            Settings.imageCacheVersion.edit().put(IMAGE_CACHE_VERSION);
        }

        updateFirebaseRegistration();

        drmRegistrationUpdater.updateIfExpired(this::cacheDRMClientRegistration);
    }

    /**
     * Check if the cache for the boot config related info is still valid
     * and synchronize it with backend if needed.
     *
     * @throws IOException In case of errors.
     */
    @Override
    public void updateBootConfig() throws Exception {
        bootUpdater.batchUpdate()
                .runIfExpired(this::cacheBootConfig)
                .runIfExpired(this::cacheInterfaceConfig)
                .runIfExpired(this::cacheTMServerInfo)
                .done();
    }

    @Override
    public void updateGlobalInstall() throws Exception {
        globalInstallUpdater.updateIfExpired(this::cacheGlobalData);
    }

    /**
     * Cache endpoints and other backend related config options.
     *
     * @throws IOException In case of errors.
     */
    private void cacheBootConfig() throws Exception {
        TimerLog timerLog = new TimerLog(TAG);

        BatchSettingEditor<SettingsProximus> settingsProximusEditor = SettingsProximus.batchEdit();
        BatchSettingEditor<SessionProximus> sessionEditor = SessionProximus.batchEdit();

        BootConfigRetrofitProximus.SignalType signalType = BootConfigRetrofitProximus.SignalType.DHCP;
        DhcpOptions dhcpOptions = DhcpOptions.read(mApp);
        String signal = dhcpOptions.get(DhcpOptions.Field.Option67, "");
        timerLog.d("DHCP option 67 : " + signal);
        if (signal.isEmpty()) {
            signalType = BootConfigRetrofitProximus.SignalType.SERIAL;
            signal = mMwProxy.getHardwareSerialNumber();
            timerLog.d("Missing DHCP option 67, serial loaded : " + signal);
        }

        String prevEnvironment = SessionProximus.environment.get("");
        BootFileNameProximus bootFileName = mAccountProxy.getBootFileName(signalType, signal);
        sessionEditor.put(SessionProximus.environment, bootFileName.getEnvironment());
        timerLog.d("Boot environment : " + bootFileName.getEnvironment());

        // Detect environment change.
        if (!TextUtils.isEmpty(prevEnvironment)
                && !Objects.equals(prevEnvironment, bootFileName.getEnvironment())) {
            Log.w(TAG, "Environment changed. Previous : " + prevEnvironment
                    + ", current : " + bootFileName.getEnvironment() + ". Clear data and restart.");
            SessionProximus.environment.edit().put(bootFileName.getEnvironment());
            System.exit(0);
            return;
        }

        BootConfigProximus bootConfig = mAccountProxy.getBootConfig(bootFileName.getEnvironment());
        timerLog.d("Boot config loaded. ");

        // Generic boot config
        sessionEditor
                .put(SessionProximus.iamBypassAllowed, bootConfig.isTIAMSBypassAllowed())
                .put(SessionProximus.securedBtaBaseUrl, bootFileName.getBaseUrl())
                .put(SessionProximus.epgBaseUrl, bootConfig.getEPGBaseUrl());

        // Baby channels.
        settingsProximusEditor
                .put(SettingsProximus.babyChannelIds, bootConfig.getBabyChannelIds());

        // Adult channels.
        settingsProximusEditor
                .put(SettingsProximus.adultChannelIds, bootConfig.getAdultChannelIds());

        // Regional Channels
        settingsProximusEditor
                .put(SettingsProximus.regionalChannelIds, bootConfig.getRegionalChannelIds());

        // Robust of GetRecordingList
        settingsProximusEditor
                .put(SettingsProximus.robustGetRecordingListAllowed, bootConfig.isRobustGetRecordingListAllowed());

        // 3rd party apps
        if (bootConfig.getPartnerConfigs() != null
                && bootConfig.getPartnerConfigs().getNetflixPartnerConfig() != null) {
            settingsProximusEditor.put(SettingsProximus.netflixPartnerId,
                    bootConfig.getPartnerConfigs().getNetflixPartnerConfig().getPartnerId());
        }

        // TIAMS config
        IAMConfigProximus iamConfig = bootConfig.getIAMConfig();

        sessionEditor
                .put(SessionProximus.iamAudience, iamConfig != null ? iamConfig.getAudience() : "")
                .put(SessionProximus.iamRedirectUrl, iamConfig != null ? iamConfig.getRedirectUrl() : "")
                .put(SessionProximus.iamRegisterUrl, iamConfig != null ? iamConfig.getRegisterUrl() : "")
                .put(SessionProximus.iamAccessTokenUrl, iamConfig != null ? iamConfig.getAccessTokenUrl() : "")
                .put(SessionProximus.iamScopes, iamConfig != null ? TextUtils.join(" ", iamConfig.getScopes()) : "")
                .put(SessionProximus.iamJWTKeyId, iamConfig != null ? iamConfig.getJWTKeyId() : "")
                .put(SessionProximus.iamSSAKeyId, iamConfig != null ? iamConfig.getSSAKeyId() : "");

        // VQE config.
        VQEConfigProximus vqeConfig = bootConfig.getVQEConfig();
        if (vqeConfig != null) {
            settingsProximusEditor
                    .put(SettingsProximus.hdBandwidth, vqeConfig.getHDBandwidth())
                    .put(SettingsProximus.sdBandwidth, vqeConfig.getSDBandwidth());
        }

        // IGMP Rejoin
        IGMPRejoinProximus igmpRejoinProximus = bootConfig.getIGMPRejoin();
        if (igmpRejoinProximus != null) {
            // Saving values in the database for potential data collection
            settingsProximusEditor
                    .put(SettingsProximus.igmpLowBandwidth, igmpRejoinProximus.getLowBandwidth())
                    .put(SettingsProximus.igmpThresholdTime, igmpRejoinProximus.getThresholdTime());

            JSONObject jsonConfig = new JSONObject();
            jsonConfig.put(MwProxyProximus.FIELD_IGMP_THRESHOLD_TIME, igmpRejoinProximus.getThresholdTime());
            jsonConfig.put(MwProxyProximus.FIELD_IGMP_LOW_BANDWIDTH, igmpRejoinProximus.getLowBandwidth());
            String rejoinConfig = jsonConfig.toString();

            updateRejoinConfig(rejoinConfig);
        }

        settingsProximusEditor.persistNow();
        sessionEditor.persistNow();

        timerLog.d("Boot config cache updated.");
    }

    /**
     * Cache BE interface configurations.
     */
    private void cacheInterfaceConfig() {
        try {
            InterfaceResponseProximus interfaceConfig = mAccountProxy.getInterfaces();
            if (interfaceConfig == null) {
                return;
            }

            List<RequestPropertyProximus> requestProperties = interfaceConfig.getBTARequestProperties();
            if (requestProperties == null) {
                return;
            }

            List<String> securedPaths = new ArrayList<>();
            for (RequestPropertyProximus requestProperty : requestProperties) {
                if (requestProperty.mustAuthenticate()) {
                    securedPaths.add(requestProperty.getName());
                }
            }
            SessionProximus.securedPaths.edit().put(securedPaths);
        } catch (Exception e) {
            Log.e(TAG, "Failed to get interfaces ", e);
        }
    }

    /**
     * Update the device current status and registration information.
     *
     * @throws IOException In case of errors.
     */
    @Override
    public void updateDeviceRegistration() throws IOException {
        String netflixESN = Settings.netflixESN.get(null);
        if (netflixESN == null) {
            // Netflix ESN needs to be requested.
            mNetflixESNReceiver.requestESN(mApp);
            return;
        }

        updateDeviceRegistration(netflixESN);
    }

    /**
     * Update the device current status and registration information including netflix.
     *
     * @param netflixESN The netflix serial number of the device.
     * @throws IOException In case of errors.
     */
    @Override
    public void updateDeviceRegistration(String netflixESN) throws IOException {
        String ipAddress = mMwProxy.getIpAddress();

        STBRegistrationUpdateRequestProximus request = new STBRegistrationUpdateRequestProximus(
                ipAddress,
                mMwProxy.getMWVersion(),
                mMwProxy.getEthernetMacAddress(),
                Settings.deviceName.get(""),
                netflixESN,
                mMwProxy.getAppVersion()
        );

        // Check if the info changed.
        long newHash = request.getRegistrationInfoHash();
        long oldHash = SettingsProximus.deviceRegistrationInfoHash.get(0L);
        if (oldHash != 0 && oldHash == newHash) {
            Log.d(TAG, "Skipp device registration info update. Still valid.");
            return;
        }

        // Send new device registration info.
        mAccountProxy.updateDeviceRegistration(request);
        SettingsProximus.deviceRegistrationInfoHash.edit().put(newHash);
        Settings.deviceIp.edit().put(ipAddress);
        Log.d(TAG, "Device registration info updated. New hash : " + newHash);
    }

    private void updateRejoinConfig(String rejoinConfig) {
        long oldConfigHash = SettingsProximus.igmpConfigHash.get(-1L);
        long newConfigHash = rejoinConfig.hashCode();

        if (oldConfigHash == newConfigHash) {
            Log.d(TAG, "Ignore IGMP Rejoin config update. Already up to date.");
            return;
        }

        mMwProxy.updateRejoinConfig(rejoinConfig);
        SettingsProximus.igmpConfigHash.edit().put(newConfigHash);
    }

    @Override
    public void updateWatchlist() throws Exception {
        watchlistUpdater.updateIfExpired(() ->
                mAccountCache.updateWatchlist(
                        mAccountProxy.getAllWatchlistItems().getWatchlistItems()));
    }

    /**
     * Check if the account info cache is valid and synchronize it with the backend if needed.
     *
     * @throws Exception In case of errors.
     */
    @Override
    public void updateAccountInfo() throws Exception {
        accountUpdater.batchUpdate()
                .runIfExpired(this::cacheAccountInfo)
                .runIfExpired(() -> {
                    cacheConsents();
                    BaseWorkManager.start(mApp, TvService.buildPlaybackConfigUpdateIntent());
                })
                .done();
    }

    @Override
    public void updateAccountPackagesInfo() throws Exception {
        //get the current packages
        Set<String> currentDTVPackages = getDTVPackageTypes(
                mAccountCache.getSubscriberPackages(null));

        //update the account info
        Settings.lastAccountInfoSyncTime.edit().remove();
        updateAccountInfo();

        //get the updated packages
        Set<String> newPackages = getDTVPackageTypes(mAccountCache.getSubscriberPackages(null));

        //check if a channel refresh is also needed (DTV package changes)
        if (!currentDTVPackages.equals(newPackages)) {
            Log.i(TAG, "Package change was detected, refreshing the channel list!");
            Settings.lastChannelsSyncTime.edit().remove();
            BaseWorkManager.start(mApp, TvService.buildChannelUpdateIntent());
        }
    }

    /**
     * Returns the external ids for the DTV package types (not locked).
     *
     * @param packages the list of packages to be checked
     * @return the unique external ids
     */
    private Set<String> getDTVPackageTypes(List<IPackage> packages) {
        Set<String> DTVPackages = new HashSet<>();
        for (IPackage iPackage : packages) {
            if (iPackage.isDTVPackage() && iPackage.isSubscribed()) { //DTV and the package is available
                DTVPackages.add(iPackage.getExternalId());
            }
        }
        return DTVPackages;
    }

    @Override
    public void updateSTBProperties() throws Exception {
        stbPropertyUpdater.updateIfExpired(this::cacheSTBProperties);
    }

    /**
     * Cache the account related information.
     *
     * @throws IOException In case of errors
     */
    private void cacheAccountInfo() throws IOException {
        TimerLog timerLog = new TimerLog(TAG);

        long now = System.currentTimeMillis();
        AccountInfoResponseProximus response = mAccountProxy.getAccountInfoResponse();
        AccountInfoProximus accountInfo = response.getAccountInfo();

        BatchSettingEditor<Settings> settingsEditor = Settings.batchEdit();
        BatchSettingEditor<SettingsProximus> settingsProximusEditor = SettingsProximus.batchEdit();

        // Save PIN information.
        settingsEditor
                .put(Settings.systemPIN, accountInfo.getAccountPIN())
                .put(Settings.purchasePIN, accountInfo.getPurchasePIN());

        // Save targeted ad related information
        settingsEditor
                .put(Settings.bandwidth, accountInfo.getBandwidth())
                .put(Settings.tadAccountEnabled, accountInfo.isTargetAdvEnabled());

        // Save subscriberType
        settingsProximusEditor
                .put(SettingsProximus.subscriberType, accountInfo.getSubscriberType());

        // Save parental control info.
        settingsEditor
                .put(Settings.enabledParentalControl, accountInfo.isParentalControlEnabled());

        if (accountInfo.getRatingList() != null && !accountInfo.getRatingList().isEmpty()) {
            settingsEditor
                    .put(Settings.minAgeRating, accountInfo.getRatingList().get(0).getRatingCode());
        }

        // Read the subscribed packages.
        Collection<PackageInfoProximus> packageList = accountInfo.getPackages();

        // Pause live tv subscription options.
        PackageInfoProximus pltvPackage = accountInfo.getPackage(PackageNamesProximus.PLTV_EXTERNAL_ID);
        if (pltvPackage == null) {
            settingsEditor
                    .remove(Settings.isPLTVEnabled)
                    .remove(Settings.maxTimeShiftBufferDuration);
        } else {
            settingsEditor
                    .put(Settings.isPLTVEnabled, pltvPackage.isSubscribed())
                    .put(Settings.maxTimeShiftBufferDuration, pltvPackage.getItemSubtypes() == null
                            ? 0 : TimeUnit.MINUTES.toMillis(pltvPackage.getItemSubtypes().getBufferSizeMin()));
        }

        // Insert packages.
        mAccountCache.updatePackages(packageList, now);

        settingsProximusEditor
                .put(SettingsProximus.companySite, accountInfo.getSite())
                .put(SettingsProximus.companyLocationName, accountInfo.getLocationName())
                .put(SettingsProximus.subscriberIDHash, accountInfo.getSubscriberIDHash())
                .put(SettingsProximus.householdId, accountInfo.getHouseHoldId());

        // HD interest device

        DeviceResourceProximus deviceResource = response.getDeviceResource(mMwProxy.getEthernetMacAddress());
        if (deviceResource != null) {
            settingsProximusEditor.put(SettingsProximus.hdProfile, deviceResource.hasHdProfile());
            settingsProximusEditor.put(SettingsProximus.STBProfile, deviceResource.getProfileName());
        }

        // VQE control.
        settingsProximusEditor
                .put(SettingsProximus.vqeControlEnabled, accountInfo.isVQEControlEnabled())
                .put(SettingsProximus.vqeRCCEnabled, accountInfo.isVQERCCEnabled())
                .put(SettingsProximus.vqeRETEnabled, accountInfo.isVQERetEnabled())
                .put(SettingsProximus.vqeNetworkBuffer, accountInfo.getNetworkBufferSize())
                .put(SettingsProximus.availableOverheadBandwidth, accountInfo.getAvailableOverheadBandwidth());

        settingsEditor.put(Settings.quiescentRebootUptime,
                accountInfo.getQuiescentRebootUptime(TimeUnit.MILLISECONDS));

        // Rentals.
        settingsProximusEditor
                .put(SettingsProximus.freeRentals, accountInfo.getFreeRentals())
                .put(SettingsProximus.serviceStartDate, accountInfo.getServiceStart())
                .put(SettingsProximus.serviceEndDate, accountInfo.getServiceEnd());

        settingsEditor.persistNow();
        settingsProximusEditor.persistNow();

        timerLog.d("Account info updated." + accountInfo.getAccountNumber());
    }

    /**
     * Cache consents related info.
     *
     * @throws IOException in case of errors.
     */
    private void cacheConsents() throws IOException {
        List<? extends IConsent> consents = mAccountProxy.getConsents(ConsentRequestScenario.SETTING_SCREEN.name());

        for (IConsent consent : consents) {
            switch (consent.getIdentifier()) {
                case ConsentProximus.CONSENT_RECOMMENDATION_IDENTIFIER:
                    SettingsProximus.recommendationFlag.edit().put(consent.isAccepted());
                case ConsentProximus.CONSENT_TARGETED_ADVERTISING_IDENTIFIER:
                    Settings.targetedAdvertisingFlag.edit().put(consent.isAccepted());
            }
        }
    }

    private void cacheSTBProperties() throws IOException {
        TimerLog timerLog = new TimerLog(TAG);

        Map<STBProperties, String> stbProperties = mAccountProxy.getSTBProperties();

        if (stbProperties == null || stbProperties.isEmpty()) {
            timerLog.d("STB Property map is Null or Empty!");
            return;
        }

        BatchSettingEditor<Settings> settingsEditor = Settings.batchEdit();
        BatchSettingEditor<SettingsProximus> settingsProximusEditor = SettingsProximus.batchEdit();

        // Power consumption
        String consumptionString = stbProperties.getOrDefault(STBProperties.POWER_CONSUMPTION, null);
        if (consumptionString != null && !consumptionString.isEmpty()) {
            boolean consumption = Boolean.parseBoolean(consumptionString);
            settingsEditor.put(Settings.powerConsumptionStepDisplayed, consumption);
        }

        // Shop language
        String shopLanguage = stbProperties.getOrDefault(STBProperties.SHOP_LANGUAGE, null);
        if (shopLanguage != null && !shopLanguage.isEmpty()) {
            settingsProximusEditor.put(SettingsProximus.shopLanguage, shopLanguage);
        }

        // Audio language
        String audioLanguage = stbProperties.getOrDefault(STBProperties.STREAM_AUDIO_LANGUAGE, null);
        if (audioLanguage != null && !audioLanguage.isEmpty()) {
            settingsEditor.put(Settings.audioLanguage, audioLanguage);
        }

        // Subtitle language
        String subtitle = stbProperties.getOrDefault(STBProperties.STREAM_SUBTITLE_LANGUAGE, null);
        if (subtitle != null && !subtitle.isEmpty()) {
            settingsEditor.put(Settings.subtitlesLanguage, subtitle);
        }

        // Favorite channels
        String channels = stbProperties.getOrDefault(STBProperties.FAVORITE_CHANNELS, null);
        if (channels != null) {
            mTvCache.updateFavoriteChannels(Arrays.asList(TextUtils.split(channels, ",")));
        }

        //Epg Layout
        String epgLayout = stbProperties.getOrDefault(STBProperties.EPG_LAYOUT, null);
        if(epgLayout != null && !epgLayout.isEmpty()) {
            settingsEditor.put(Settings.epgLayoutType, epgLayout);
        }

        //Viewed Hints.
        String hintIds = stbProperties.getOrDefault(STBProperties.VIEWED_HINTS, null);
        if(hintIds != null) {
            settingsEditor.put(Settings.viewedHints, hintIds);
        }

        settingsEditor.persistNow();
        settingsProximusEditor.persistNow();

        timerLog.d("STB properties updated.");
    }

    private void updateAdvertServers(GlobalDataProximus response) {
        BatchSettingEditor<Settings> editor = Settings.batchEdit();

        List<AdvertiseServerInfoProximus> cdn = response.getAdvertiseServerList(ServerType.TACDN);
        if (cdn.size() == 1) {
            AdvertiseServerInfoProximus info = cdn.get(0);
            editor.put(Settings.taCdnServerUrl, info.getDefaultUrl());
            editor.put(Settings.taCdnPublishUrl, info.getPublishingPath());
        } else {
            Log.w(TAG, "Single TACDN server required, found: " + cdn.size());
        }

        List<AdvertiseServerInfoProximus> tap = response.getAdvertiseServerList(ServerType.TAP);
        if (tap.size() == 1) {
            AdvertiseServerInfoProximus info = tap.get(0);
            editor.put(Settings.tapServerUrl, info.getDefaultUrl());
            editor.put(Settings.tapVASTRequestRandomization, info.getVASTRequestRandomization());
            editor.put(Settings.tapADRetrievalInterval, info.getADRetrievalInterval());
            editor.put(Settings.tapRequestRandomInterval, info.getRequestRandomInterval());
            editor.put(Settings.tapAdDurationTolerance, info.getAdDurationTolerance());
        } else {
            Log.w(TAG, "Single TAP server required, found: " + cdn.size());
        }

        List<AdvertiseServerInfoProximus> tas = response.getAdvertiseServerList(ServerType.TAS);
        if (tas.size() == 1) {
            AdvertiseServerInfoProximus info = tas.get(0);
            editor.put(Settings.tasServerUrl, info.getDefaultUrl());
            editor.put(Settings.tasADRetrievalInterval, info.getADRetrievalInterval());
            editor.put(Settings.tasRequestRandomInterval, info.getRequestRandomInterval());
        } else {
            Log.w(TAG, "Single TAS server required, found: " + tas.size());
        }

        HardwareVersionProximus version = response.getHardwareVersion();
        if (version != null) {
            editor.put(Settings.tadHardwareEnabled, version.isTADTVEnabled());
            editor.put(Settings.tadDownloadPercentage, version.getTAPreDownloadPercentage());
        }

        BandwidthProfileProximus profile = null;
        int bandwidth = Settings.bandwidth.get(0);
        for (BandwidthProfileProximus item : response.getBandwidthProfiles()) {
            if (bandwidth != item.getBandwidth()) {
                // bandwidth from account info must match profile bandwidth
                continue;
            }
            if (!item.isTargetAdEnabled()) {
                // targeted ad flag must be enabled to use the profile
                continue;
            }
            profile = item;
        }
        if (profile != null) {
            editor.put(Settings.tadDownloadBandwidth, profile.getTargetAdDownLoadBandwidth());
        } else {
            editor.remove(Settings.tadDownloadBandwidth);
        }

        editor.persistNow();
    }

    /**
     * Update the data collection server endpoints based on the configuration coming from the server list or boot config.
     */
    private void updateDataCollectionServers(List<IDataCollectionServerInfo> dataCollectionServers) {
        Log.d(TAG, "Updating Data Collection servers.");
        for (IDataCollectionServerInfo serverInfo : dataCollectionServers) {
            switch (serverInfo.getCollectionType()) {
                case TYPE_EVENT:
                    updateReporterInfo(UIEventReporterProximus.class, serverInfo);
                    updateReporterInfo(PlaybackEventReporterProximus.class, serverInfo);
                    break;
                case TYPE_METRIC:
                    updateReporterInfo(MetricReporterProximus.class, serverInfo);
                    break;
            }
        }
    }

    /**
     * Update the reporter configuration with the new server info.
     * @param reporter The reporter which needs to be updated.
     * @param serverInfo The data collection server info.
     */
    private void updateReporterInfo(Class<? extends Reporter<?, ?>> reporter,
                                    IDataCollectionServerInfo serverInfo) {
        ReporterInfo reporterInfo = new ReporterInfo.Builder()
                .setReporter(reporter)
                .setEndpoint(serverInfo.getEndpoint())
                .setHeaders(serverInfo.getHeaders())
                .setMaxKeepPeriod(serverInfo.getMaxKeepPeriod())
                .setMaxKeepSize(serverInfo.getMaxKeepSize())
                .setMaxBatchSize(serverInfo.getMaxBatchSize())
                .setRandomPeriod(serverInfo.getRandomPeriod())
                .setTimeoutPeriod(serverInfo.getTimeoutPeriod())
                .build();
        Log.update(reporter, reporterInfo);
    }

    /**
     * Get the global data from backend and store it in our local cache.
     *
     * @throws IOException In case of errors.
     */
    private void cacheGlobalData() throws IOException {
        TimerLog timerLog = new TimerLog(TAG);

        GlobalDataProximus globalData = mAccountProxy.getGlobalData();
        mVodCategoryCache.updateCategoryTree(globalData.getCategories());
        mAccountCache.updatePosterServers(globalData.getPosterServerList());

        updateAdvertServers(globalData);
        updateDataCollectionServers(globalData.getDataCollectionServerList());

        ServerInfoProximus bannerFeedbackServer = globalData.getBannerFeedbackServer();
        if (bannerFeedbackServer != null) {
            SessionProximus.bannerFeedbackUrl.edit().put(bannerFeedbackServer.getDefaultUrl());
        }

        // EPG window config.
        Settings.batchEdit()
                .put(Settings.epgPastWindowLength, TimeUnit.DAYS.toMillis(globalData.getPastEPGDays()))
                .put(Settings.epgFutureWindowLength, TimeUnit.DAYS.toMillis(globalData.getFutureEPGDays()))
                .put(Settings.remoteUpdatePollStartTime, globalData.getRemoteUpdatePollStartTime())
                .put(Settings.remoteUpdatePollEndTime, globalData.getRemoteUpdatePollEndTime())
                .persistNow();

        SettingsProximus.freeRentalLimit.edit().put(globalData.getCreditPriceLimit());
        timerLog.d("Global data updated. Past EPG days : " + globalData.getPastEPGDays()
                + ", future EPG days : " + globalData.getFutureEPGDays());
    }

    /**
     * Check if the DRM client is still needed and synchronize it with the backend if needed.
     *
     * @throws IOException In case of errors.
     */
    private void cacheDRMClientRegistration() throws IOException {
        DRMRegisteredClientsResponseProximus drmClientRegistration = mAccountProxy.getDRMClientRegistration();

        // OTT DRM client id
        String ottDRMClientId = drmClientRegistration.getOTTDrmClientId();
        if (TextUtils.isEmpty(ottDRMClientId)) {
            ottDRMClientId = generateOTTDRMClientId();
            Log.d(TAG, "Register OTT DRM client id. Id : " + ottDRMClientId);
            mAccountProxy.registerDRMClient(DRMClientProximus.buildOTTDRMClient(ottDRMClientId));
        } else {
            Log.d(TAG, "OTT DRM client already registered.");
        }

        // IPTV client id.
        String iptvDRMClientId = drmClientRegistration.getIPTVDrmClientId();
        if (TextUtils.isEmpty(iptvDRMClientId)) {
            iptvDRMClientId = generateIPTVDRMClientId();
            Log.d(TAG, "Register IPTV DRM client id. Id : " + iptvDRMClientId);
            mAccountProxy.registerDRMClient(DRMClientProximus.buildIPTVDRMClient(iptvDRMClientId));
        } else {
            Log.d(TAG, "IPTV DRM client already registered.");
        }

        Settings.batchEdit()
                .put(Settings.iptvDRMClientId, iptvDRMClientId)
                .put(Settings.ottDRMClientId, ottDRMClientId)
                .persistNow();
    }

    /**
     * @return The generated OTT drm client id.
     * The device id is generated from the MAC address and SOC id..
     */
    private String generateOTTDRMClientId() {
        String macAddress = mMwProxy.getEthernetMacAddress();
        String socId = mMwProxy.getSocId();
        return macAddress + "_" + StringUtils.sha256(macAddress + socId);
    }

    /**
     * @return The generated IPTV drm client id. Soc id in case of V7.
     */
    private String generateIPTVDRMClientId() {
        return mMwProxy.getSocId();
    }

    /**
     * Get and cache the total manage server information.
     *
     * @throws IOException in case of errors.
     */
    private void cacheTMServerInfo() throws IOException {
        TimerLog timerLog = new TimerLog(TAG);

        TMServerInfoResponseProximus serverInfoResponse = mAccountProxy.getTMServerInfo();

        SessionProximus.batchEdit()
                .put(SessionProximus.unsecuredBtaBaseUrl, serverInfoResponse.getUnsecuredBtaUrl())
                .put(SessionProximus.vqeChannelConfigUrl, serverInfoResponse.getVQEChannelConfigUrl())
                .put(SessionProximus.vqeNetworkConfigUrl, serverInfoResponse.getVQENetworkConfigUrl())
                .put(SessionProximus.vcasServerAddress, serverInfoResponse.getVCASServerAddress())
                .put(SessionProximus.globalInstallUrl, serverInfoResponse.getGlobalInstallConfigUrl())
                .put(SessionProximus.webAccessInstallUrl, serverInfoResponse.getWebAccessFileUrl())
                .put(SessionProximus.availableVersionUrl, serverInfoResponse.getAvailableVersionsUrl())
                .put(SessionProximus.operatorIsolationOverrideUrl, serverInfoResponse.getOperatorIsolationOverrideUrl())
                .persistNow();

        mInternetChecker.triggerInternetCheck();

        timerLog.d("TM server info updated."
                + " VQEChannel config url : " + serverInfoResponse.getVQEChannelConfigUrl()
                + " VQENetwork config url : " + serverInfoResponse.getVQENetworkConfigUrl());
    }

    @SuppressLint("CheckResult")
    private void updateFirebaseRegistration() {
        FirebaseMessaging.getInstance().getToken().addOnSuccessListener(instanceId -> {
            Log.d(TAG, "Register new firebase token: " + instanceId);

            Completable.fromAction(() -> registerAppInstance(instanceId))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new CompletableObserver() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {

                        }

                        @Override
                        public void onComplete() {
                            Log.d(TAG, "Register new firebase token was successful");
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            Log.e(TAG, "Registration failed: " + e);
                        }
                    });
        });
    }

    @Override
    public String getSavedInstance() {
        return SessionProximus.appInstanceId.get("");
    }

    @Override
    public void registerInstance(String instanceId) throws IOException {
        mAccountProxy.registerInstance(instanceId);
        SessionProximus.appInstanceId.edit().put(instanceId);
    }

    private final NetflixESNReceiver mNetflixESNReceiver = new NetflixESNReceiver();

    /**
     * Broadcast receiver to get the Netflix ESN and trigger the device registration update.
     */
    private static class NetflixESNReceiver extends BaseBroadcastReceiver {

        /**
         * Send out this Broadcast intent to request the ESN.
         */
        private static final String NETFLIX_ESN_REQUEST_INTENT_NAME = "com.netflix.ninja.intent.action.ESN";

        /**
         * Listen to this broadcast to get the response of reading ESN.
         */
        private static final String NETFLIX_ESN_RESPONSE_BROADCAST_NAME = "com.netflix.ninja.intent.action.ESN_RESPONSE";

        /**
         * When receive com.netflix.ninja.intent.action.ESN_RESPONSE,
         * the string value passed with this key is the ESN value
         */
        private static final String NETFLIX_ESN_RESPONSE_BROADCAST_EXTRA_NAME = "ESNValue";


        @Override
        public void onReceive(Context context, Intent intent) {
            String esn = intent.getStringExtra(NETFLIX_ESN_RESPONSE_BROADCAST_EXTRA_NAME);
            Log.d(TAG, "Netflix ESN received. " + esn);
            unregisterReceiver(context);

            if (esn != null) {
                BaseWorkManager.start(context, AccountService.buildUpdateDeviceRegistration(esn));
            }
        }

        @Override
        public IntentFilter getIntentFilter() {
            return new IntentFilter(NETFLIX_ESN_RESPONSE_BROADCAST_NAME);
        }

        public void requestESN(Context context) {
            registerReceiver(context);

            // Send ESN request intent.
            Log.d(TAG, "Request ESN.");
            Intent esnQueryIntent = new Intent(NETFLIX_ESN_REQUEST_INTENT_NAME);
            esnQueryIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            esnQueryIntent.setPackage(PackageUtils.PACKAGE_NAME_NETFLIX);
            context.sendBroadcast(esnQueryIntent);
        }
    }

    // Get notification about the standby state changes.
    private final StandByStateChangeReceiver.Listener mStandByStateChangeListener = screenOn -> {
        // Cancel periodic account info and boot config updates.
        if (!screenOn) {
            BaseWorkManager.cancelAlarm(mApp, AccountService.buildUpdateAccountInfoIntent());
            BaseWorkManager.cancelAlarm(mApp, AccountService.buildUpdateBootConfigIntent());
            BaseWorkManager.cancelAlarm(mApp, AccountService.buildUpdateGlobalInstallIntent());
            BaseWorkManager.cancelAlarm(mApp, AccountService.buildUpdateWatchlist());
        }
    };
}
