package tv.threess.threeready.data.proximus.pvr.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;
import tv.threess.threeready.data.proximus.pvr.model.RecordingProximus;

/**
 * Retrofit response class for scheduling single recording.
 *
 * Created by Noemi Dali on 20.04.2020.
 */
@XmlRootElement(name = "BTAResponse")
public class RecordingAddResponseProximus extends BTAResponseProximus {

    @XmlElement(name = "Recording")
    private RecordingProximus mRecording;

    public RecordingProximus getRecording() {
        return mRecording;
    }
}
