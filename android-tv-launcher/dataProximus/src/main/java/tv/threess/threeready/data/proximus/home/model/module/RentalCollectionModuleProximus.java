package tv.threess.threeready.data.proximus.home.model.module;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceService;
import tv.threess.threeready.api.home.model.module.ModuleDataType;
import tv.threess.threeready.api.home.model.module.ModuleType;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Module config which displays all the user rentals in a collection.
 *
 * @author Barabas Attila
 * @since 6/18/21
 */
public class RentalCollectionModuleProximus implements ModuleConfig {

    @Override
    public String getReportName() {
        return getTitle();
    }

    @Override
    public String getReportReference() {
        return getDataType().name();
    }

    @Nullable
    @Override
    public String getTitle() {
        return TranslationKey.STRIPE_MY_RENTALS;
    }

    @Override
    public ModuleType getType() {
        return ModuleType.COLLECTION;
    }

    @Override
    public ModuleDataType getDataType() {
        return ModuleDataType.MIXED;
    }

    @Override
    public ModuleCardVariant getCardVariant() {
        return ModuleCardVariant.A2;
    }

    @Nullable
    @Override
    public ModuleDataSource getDataSource() {
        return mDataSource;
    }

    private final ModuleDataSource mDataSource = new ModuleDataSource() {
        @Override
        public ModuleDataSourceService getService() {
            return ModuleDataSourceService.TV;
        }

        @Override
        public ModuleDataSourceMethod getMethod() {
            return ModuleDataSourceMethod.USER_RENTALS;
        }
    };
}
