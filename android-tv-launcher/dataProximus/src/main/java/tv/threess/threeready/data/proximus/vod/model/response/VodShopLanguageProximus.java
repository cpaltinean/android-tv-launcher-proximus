package tv.threess.threeready.data.proximus.vod.model.response;

import java.util.List;

import tv.threess.threeready.api.generic.helper.LocaleUtils;

/**
 * BTA VOD request parameter to send to selected VOD shop language.
 *
 * @author Barabas Attila
 * @since 2020.05.04
 */
public class VodShopLanguageProximus {
    private final String mLanguage;

    public VodShopLanguageProximus(List<String> languageCodes) {
        // Nothing selected. Check UI language.
        if (languageCodes == null || languageCodes.isEmpty()) {
            if (LocaleUtils.getApplicationLanguage().equalsIgnoreCase(LocaleUtils.FRENCH_LANGUAGE_CODE)) {
                mLanguage = LocaleUtils.FRENCH_SHOP_LANGUAGE;
            } else {
                mLanguage = LocaleUtils.DUTCH_LANGUAGE_CODE;
            }

        // Only one language selected.
        } else if (languageCodes.size() == 1) {
            mLanguage = languageCodes.get(0);

        // Multiple language selected.
        } else {
            mLanguage = "";
        }
    }

    @Override
    public String toString() {
        return mLanguage;
    }
}
