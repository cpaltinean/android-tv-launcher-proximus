package tv.threess.threeready.data.proximus.tv.model.advertisement;

import org.jetbrains.annotations.NotNull;
import tv.threess.threeready.api.tv.model.TvAdReplacement;
import tv.threess.threeready.data.tv.model.TvAdvertisement;
import tv.threess.threeready.data.proximus.tv.TvProxyProximus;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Vast response contains the information which pre-downloaded ad file should be used as a replacement.
 */
public class VastAdProximus implements TvAdReplacement {

    private final VastResponse mVastResponse;
    private final TvAdvertisement mTvAdvertisement;
    private final long mLastModified;

    public VastAdProximus(VastResponse vastResponse) {
        mVastResponse = vastResponse;
        mTvAdvertisement = null;
        mLastModified = 0;
    }

    public VastAdProximus(VastResponse vastResponse, TvAdvertisement tvAdvertisement) {
        mVastResponse = vastResponse;
        mTvAdvertisement = tvAdvertisement;
        File file = getFile();
        if (file != null && file.exists()) {
            mLastModified = getFile().lastModified();
        } else {
            mLastModified = 0;
        }
    }

    @Override
    public File getFile() {
        VastMediaFile mediaFile = mVastResponse.getMediaFile();
        if (mediaFile == null) {
            return null;
        }

        return new File(TvProxyProximus.TAD_DOWNLOAD_DIR, mediaFile.getValue());
    }

    @Override
    public String getFileName() {
        VastMediaFile mediaFile = mVastResponse.getMediaFile();
        if (mediaFile == null) {
            return null;
        }
        return mediaFile.getValue();
    }

    public long getLastModified() {
        return mLastModified;
    }

    @Override
    public long getDuration(TimeUnit unit) {
        if (mTvAdvertisement == null) {
            return 0;
        }
        return mTvAdvertisement.getDuration(unit);
    }

    @Override
    public boolean isExpired() {
        if (mTvAdvertisement == null) {
            // expired because not found in the database
            return true;
        }
        return mTvAdvertisement.isExpired(System.currentTimeMillis());
    }

    @Override
    public boolean isOnMarker() {
        if (mTvAdvertisement == null) {
            return false;
        }
        return mTvAdvertisement.isOnMarker();
    }

    @NotNull
    public List<String> getErrorUrls() {
        VastInLine inline = mVastResponse.getInLine();
        if (inline == null || inline.getError() == null) {
            return Collections.emptyList();
        }

        return Collections.singletonList(inline.getError());
    }

    @NotNull
    public List<VastIdCdata> getImpressions() {
        VastInLine inline = mVastResponse.getInLine();
        if (inline == null || inline.getImpressions() == null) {
            return Collections.emptyList();
        }

        return inline.getImpressions();
    }

    @NotNull
    public List<VastTracking> getTrackingEvents() {
        VastLinear sequence = mVastResponse.getSequence();
        if (sequence == null || sequence.getTrackingEvents() == null) {
            return Collections.emptyList();
        }

        return sequence.getTrackingEvents();
    }
}
