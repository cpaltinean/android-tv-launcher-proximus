package tv.threess.threeready.data.proximus.account;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.ReadableSetting;
import tv.threess.threeready.api.account.setting.SettingProperty;
import tv.threess.threeready.data.account.SettingsAccessProxy;
import tv.threess.threeready.data.proximus.mw.MwProxyProximus;

/**
 * Enumeration of known technicolor related settings.
 *
 * @author Boldijar Paul
 * @since 2017.07.25
 */
public enum TechnicolorSettings implements SettingProperty, ReadableSetting {
    // Generic settings
    ProviderName("provider.name"),
    DefaultSettingsWereSet("default.settings.were.set"),

    // Language settings for audio and subtitles IPTV
    MediaAudioPreferredType("media.audio.preferredtype"),
    MediaAudioPreferredLanguages("media.audio.preferredlanguages"),
    MediaSubtitlesPreferredType("media.subtitles.preferredtype"),
    MediaSubtitlesEnabled("media.subtitles.enabled"),
    MediaSubtitlesPreferredLanguages("media.subtitles.preferredlanguages");

    private static final SettingsAccessProxy sSettingsAccess = Components.get(SettingsAccessProxy.class);

    // values for some of this settings
    public static final String AUDIO_TYPE_NORMAL = "normal";
    public static final String AUDIO_TYPE_SUPPLEMENTARY = "supplementary";
    public static final String SUBTITLE_TYPE_NORMAL = "normal";
    public static final String SUBTITLE_TYPE_HARD_OF_HEARING = "hard_of_hearing";


    private final String name;

    TechnicolorSettings(String value) {
        this.name = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return sSettingsAccess.getTechnicolorSettings(MwProxyProximus.USER_URI, this);
    }

    public int put(String value) {
        return sSettingsAccess.putTechnicolorSettings(MwProxyProximus.USER_URI, this, value);
    }

    public int put(boolean value) {
        return put(String.valueOf(value));
    }
}
