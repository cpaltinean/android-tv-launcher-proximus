package tv.threess.threeready.data.proximus.home.model.swimalane;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.data.proximus.home.model.DisplayNamesProximus;

@XmlRootElement(name = "Option")
public class SwimLaneSortOptionProximus implements SelectorOption {

    @XmlElement(name = "isDefault")
    private boolean mIsDefault;

    @XmlElementWrapper(name = "DisplayNames")
    @XmlElement(name = "DisplayName")
    private DisplayNamesProximus mDisplayNames;

    @XmlElement(name = "ItemQueryType")
    private String mItemQueryType;

    @XmlElement(name = "ItemQueryName")
    private String mItemQueryName;

    @XmlElement(name = "UseSwimlaneQueryParameters")
    private boolean mUseSwimLaneQueryParameters;

    @XmlElementWrapper(name = "ItemQueryParameters")
    @XmlElement(name = "Param")
    private ArrayList<SwimLaneQueryParamProximus> mParameters;

    @XmlElement(name = "PageSize")
    private int mPageSize;

    @Override
    public boolean isDefault() {
        return mIsDefault;
    }

    @Override
    public String getName() {
        if (mDisplayNames == null) {
            return "";
        }
        return mDisplayNames.getLocalizedNameOrFirst();
    }

    @Override
    public String getReportName() {
        return getName();
    }

    @Override
    public String getReportReference() {
        return mItemQueryName;
    }

    public String getItemQueryType() {
        return mItemQueryType;
    }

    public String getItemQueryName() {
        return mItemQueryName;
    }

    public boolean useSwimLaneQueryParameters() {
        return mUseSwimLaneQueryParameters;
    }

    public List<SwimLaneQueryParamProximus> getParameters() {
        return mParameters;
    }

    public int getPageSize() {
        return mPageSize;
    }
}
