package tv.threess.threeready.data.proximus.home.model.module;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleDataType;
import tv.threess.threeready.data.proximus.home.model.editorial.EditorialAppItemProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Module presenter for a stripe which shows editorial apps.
 *
 * @author Daniela Toma
 * @since 2022.02.17
 */
public class EditorialAppSwimlaneModulePresenter extends SwimlaneModuleConfigProximus {

    public EditorialAppSwimlaneModulePresenter(SwimLaneProximus swimlane) {
        super(swimlane);
    }

    @Override
    public ModuleDataType getDataType() {
        return ModuleDataType.EDITORIAL;
    }

    @Nullable
    @Override
    public ModuleDataSource getDataSource() {
        return null;
    }

    @Override
    public List<EditorialItem> getEditorialItems() {
        List<EditorialItem> mEditorialItemList = new ArrayList<>();
        List<SwimLaneProximus.StaticTile> staticTileList = mSwimlane.getStaticTiles();
        if (staticTileList != null) {
            Collections.sort(staticTileList, (Comparator.comparingInt(SwimLaneProximus.StaticTile::getSortOrder)));
            for (SwimLaneProximus.StaticTile staticTile : staticTileList) {
                mEditorialItemList.add(new EditorialAppItemProximus(staticTile));
            }
        }

        return mEditorialItemList;
    }
}
