package tv.threess.threeready.data.proximus.generic.firebase;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.receivers.StandByStateChangeReceiver;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.account.AccountService;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.proximus.account.AccountRepositoryProximus;
import tv.threess.threeready.data.pvr.PvrService;
import tv.threess.threeready.data.tv.TvService;
import tv.threess.threeready.data.vod.VodService;

/**
 * Service for triggering FCM messages.
 *
 * Created by Noemi Dali on 06.07.2020.
 */
public class FirebaseTriggeringService extends FirebaseMessagingService {
    private static final String TAG = Log.tag(FirebaseTriggeringService.class);

    private static final String TRIGGER_NUMBER_KEY = "TriggerNumber";
    private static final String ORIGINATING_MAC = "OriginatingMac";

    private static final int SUBSCRIBER_INFO_NR = 0;
    private static final int RENTED_ITEMS_NR = 2;
    private static final int RECORDING_LIST_NR = 5;
    private static final int SD_HD_SWITCH_NR = 9;
    private static final int ACCOUNT_INFO_NR = 20;
    private static final int CHANNELS_NR = 26;
    private static final int CREDITS_NR = 27;

    private final AccountRepositoryProximus mConfigHandlerProximus = Components.get(AccountRepositoryProximus.class);
    private final StandByStateChangeReceiver mStandByStateChangeReceiver = Components.get(StandByStateChangeReceiver.class);

    @SuppressLint("CheckResult")
    @Override
    public void onNewToken(@NonNull String s) {
        Log.d(TAG, "New token received, register app instance with token: " + s);
        mConfigHandlerProximus.registerAppInstance(s);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        Log.d(TAG, "Message received from: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            String triggerNumber = remoteMessage.getData().get(TRIGGER_NUMBER_KEY);
            Log.d(TAG, "New trigger number received: " + triggerNumber);

            String originatingMac = remoteMessage.getData().get(ORIGINATING_MAC);
            if (TextUtils.equals(Components.get(MwProxy.class).getEthernetMacAddress(), originatingMac)) {
                Log.d(TAG, "Ignore self sent message. Trigger number : " + triggerNumber);
                return;
            }

            if (triggerNumber != null) {
                int triggerNr = Integer.parseInt(triggerNumber);
                try {
                    handleNotification(triggerNr);
                } catch (Exception e) {
                    Log.e(TAG, "Handling of triggered event failed: " + e);
                }
            }
        } else {
            Log.e(TAG, "Remote message data is empty");
        }
    }

    public void handleNotification(int triggerNumber) {
        switch (triggerNumber) {
            case RECORDING_LIST_NR:
                Settings.lastRecordingSyncTime.edit().remove();

                if (mStandByStateChangeReceiver.isScreenOn()) {
                    Log.d(TAG, "Recording list changed. Update immediately.");
                    BaseWorkManager.start(getApplication(), PvrService.buildUpdateRecordingIntent());
                } else {
                    Log.d(TAG,  "Recording list changed. Update at next caching step.");
                }
                break;

            case SD_HD_SWITCH_NR:
                Settings.batchEdit()
                        .remove(Settings.lastAccountInfoSyncTime)
                        .remove(Settings.lastChannelsSyncTime)
                        .persistNow();

                if (mStandByStateChangeReceiver.isScreenOn()) {
                    Log.d(TAG, "HD switch changed. Update immediately.");
                    BaseWorkManager.start(getApplication(), AccountService.buildPerformCachingIntent());
                } else {
                    Log.d(TAG,  "HD switch changed. Update at next caching step.");
                }
                break;

            case SUBSCRIBER_INFO_NR:
                Settings.batchEdit()
                        .remove(Settings.lastAccountInfoSyncTime)
                        .remove(Settings.lastChannelsSyncTime)
                        .persistNow();

                if (mStandByStateChangeReceiver.isScreenOn()) {
                    Log.d(TAG, "Subscription info changed. Update immediately.");
                    BaseWorkManager.start(getApplication(), AccountService.buildUpdateAccountPackagesInfo());
                } else {
                    Log.d(TAG, "Subscription info changed. Update at next caching step.");
                }
                break;

            case ACCOUNT_INFO_NR:
                Settings.lastAccountInfoSyncTime.edit().remove();

                if (mStandByStateChangeReceiver.isScreenOn()) {
                    Log.d(TAG, "Account info changed. Update immediately.");
                    BaseWorkManager.start(getApplication(), AccountService.buildUpdateAccountInfoIntent());
                } else {
                    Log.d(TAG, "Account info changed. Update at next caching step.");
                }
                break;

            case CHANNELS_NR:
                Settings.lastChannelsSyncTime.edit().remove();
                if (mStandByStateChangeReceiver.isScreenOn()) {
                    Log.d(TAG, "Channel list changed. Update immediately");
                    BaseWorkManager.start(getApplication(), TvService.buildChannelUpdateIntent());
                } else {
                    Log.d(TAG, "Channel list changed. Update at next caching step.");
                }
                break;

            case CREDITS_NR:
            case RENTED_ITEMS_NR:
                Settings.lastRentedVodSyncTime.edit().remove();
                if (mStandByStateChangeReceiver.isScreenOn()) {
                  Log.d(TAG, "Rental info changed. Update immediately.");
                    BaseWorkManager.start(getApplication(), VodService.buildUpdatePurchasesIntent());
                } else {
                  Log.d(TAG, "Rental info changed. Update at next caching step.");
                }
                break;
        }
    }
}
