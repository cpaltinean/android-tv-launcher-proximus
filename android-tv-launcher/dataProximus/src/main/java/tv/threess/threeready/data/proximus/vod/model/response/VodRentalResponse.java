package tv.threess.threeready.data.proximus.vod.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * Proximus retrofit response class for vod renting response.
 *
 * Created by Noemi Dali on 30.04.2020.
 */
@XmlRootElement(name = "BTAResponse")
public class VodRentalResponse extends BTAResponseProximus {

    @XmlElement(name = "Rental")
    private Rental rentalInfo;

    public long getExpirationTime() {
        return rentalInfo.expirationTime;
    }

    public long getRentalTime() {
        return rentalInfo.rentalTime;
    }

    public boolean isFreeRental() {
        return rentalInfo.isFreeRental;
    }

    public int getFreeRentalCount() {
        return rentalInfo.freeRentalCount;
    }

    @XmlRootElement(name = "Rental")
    private static class Rental {
        @XmlElement(name = "ExpireTime")
        private long expirationTime;

        @XmlElement(name = "StartTime")
        private long rentalTime;

        @XmlElement(name = "FreeRentalCreditApplied")
        private boolean isFreeRental;

        @XmlElement(name = "FreeRentals")
        private int freeRentalCount;
    }
}
