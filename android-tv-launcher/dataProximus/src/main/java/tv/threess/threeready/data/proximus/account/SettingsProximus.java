package tv.threess.threeready.data.proximus.account;

import androidx.annotation.WorkerThread;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.BatchSettingEditor;
import tv.threess.threeready.api.account.setting.ReadableSetting;
import tv.threess.threeready.api.account.setting.SettingEditor;
import tv.threess.threeready.api.account.setting.SettingProperty;
import tv.threess.threeready.data.account.SettingsAccessProxy;

/**
 * Proximus settings enum.
 *
 * @author Eugen Guzyk
 * @since 2019.01.15
 */
public enum SettingsProximus implements SettingProperty, ReadableSetting {
    householdId,
    companyLocationName,
    companySite,
    netflixPartnerId,

    eventCounter,

    babyChannelIds,
    adultChannelIds,
    regionalChannelIds,

    robustGetRecordingListAllowed,

    // VOD
    freeRentals,
    freeRentalLimit,
    serviceStartDate,
    serviceEndDate,
    leafCategoryIds,
    leafCategoryRequestHash,

    // Account
    subscriberIDHash,
    subscriberType,

    // Device
    hdProfile,
    shopLanguage,
    deviceRegistrationInfoHash,
    STBProfile,

    // VQE override config
    sdBandwidth,
    hdBandwidth,
    availableOverheadBandwidth,
    vqeNetworkBuffer,
    vqeControlEnabled,
    vqeRCCEnabled,
    vqeRETEnabled,
    vqeConfigHash,

    // IGMP Rejoin parameters
    igmpLowBandwidth,
    igmpThresholdTime,
    igmpConfigHash,

    //Consents
    recommendationFlag;

    private static final SettingsAccessProxy sSettingsAccess = Components.get(SettingsAccessProxy.class);

    @Override
    public String getName() {
        return name();
    }

    @Override
    public String getValue() {
        return sSettingsAccess.getSetting(this);
    }

    /**
     * @return Editor to update the value of the key in the cache.
     */
    @WorkerThread
    public SettingEditor<SettingsProximus> edit() {
        return new SettingEditor<>(this, sSettingsAccess::putSettings);
    }

    /**
     * @return Editor to updated multiple values in a batch.
     */
    @WorkerThread
    public static BatchSettingEditor<SettingsProximus> batchEdit() {
        return new BatchSettingEditor<>(sSettingsAccess::putSettings);
    }

    public long increment() {
        return sSettingsAccess.incSetting(this);
    }
}
