package tv.threess.threeready.data.proximus.vod.model.response;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.generic.helper.LocaleUtils;

/**
 * Proximus VOD categories coming from BTA.
 *
 * @author Barabas Attila
 * @since 6/8/21
 */
@XmlRootElement(name = "Category")
public class VodCategoryDetailProximus extends VodCategoryProximus {
    @XmlElement(name = "ExternalCategoryId")
    private String mCategoryId;

    @XmlElement(name = "CategoryDetail")
    @XmlElementWrapper(name = "CategoryDetails")
    private List<CategoryDetail> mCategoryDetails;

    @XmlElement(name = "IsLeaf")
    private boolean mIsLeaf;

    @XmlElement(name = "CategoryName")
    private String mCategoryName;


    @Override
    public String getName() {
        return mCategoryName;
    }

    /**
     * @return Unique identifier of the category.
     */
    @Override
    public String getId() {
        return mCategoryId;
    }

    /**
     * @return True if the category doesn't contain other subcategories.
     */
    public boolean isLeaf() {
        return mIsLeaf;
    }

    /**
     * @param language The UI language.
     * @return The title of the category to be displayed on the UI.
     */
    public String getDisplayName(String language) {
        if (mCategoryDetails == null) {
            return "";
        }

        // Search for display name for the given language.
        for (CategoryDetail categoryDetail : mCategoryDetails) {
            if (language.equalsIgnoreCase(categoryDetail.mLanguage)) {
                return categoryDetail.mDisplayName;
            }
        }

        // Use the first as default.
        if (!mCategoryDetails.isEmpty()) {
            return mCategoryDetails.get(0).mDisplayName;
        }

        return "";
    }

    @Override
    public String getTitle() {
        String displayName = getDisplayName(LocaleUtils.getApplicationLanguage());
        return !TextUtils.isEmpty(displayName) ? displayName : mCategoryName;
    }

    @XmlRootElement(name = "CategoryDetail")
    private static class CategoryDetail implements Serializable {

        @XmlElement(name = "DisplayName")
        private String mDisplayName;

        @XmlElement(name = "LanguageType")
        private String mLanguage;
    }


}
