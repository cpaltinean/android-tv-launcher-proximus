package tv.threess.threeready.data.proximus.mw;

import android.content.Context;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.HdmiConnectivityReceiver;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;

/**
 * RX observable that waits for the HDCP status
 *
 * @author David
 * @since 2022.04.11
 */
public class HDCPActivationObservable extends BaseContentObservable<Void> {

    private final HdmiConnectivityReceiver mHdmiReceiver = Components.get(HdmiConnectivityReceiver.class);

    public HDCPActivationObservable(Context context) {
        super(context);
    }

    @Override
    public void subscribe(ObservableEmitter<Void> emitter) throws Exception {
        super.subscribe(emitter);

        if (mHdmiReceiver.isHdcpActive()) {
            emitter.onComplete();
            return;
        }

        mHdmiReceiver.addStateChangedListener(mChangeListener);
    }

    @Override
    protected void unregisterObserver() {
        super.unregisterObserver();
        mHdmiReceiver.removeStateChangedListener(mChangeListener);
    }

    private final HdmiConnectivityReceiver.Listener mChangeListener = new HdmiConnectivityReceiver.Listener() {
        @Override
        public void onHdcpStateChanged(boolean active) {
            if (mHdmiReceiver.isHdcpActive()) {
                mEmitter.onComplete();
            }
        }
    };
}
