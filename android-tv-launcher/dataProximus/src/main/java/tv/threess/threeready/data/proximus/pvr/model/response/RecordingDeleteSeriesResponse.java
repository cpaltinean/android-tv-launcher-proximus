package tv.threess.threeready.data.proximus.pvr.model.response;

import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * Retrofit response class for canceling series recording.
 *
 * Created by Noemi Dali on 21.04.2020.
 */
@XmlRootElement(name = "BTAResponse")
public class RecordingDeleteSeriesResponse extends BTAResponseProximus {

    @XmlElement(name = "Series")
    private Series mSeries;

    public String getUserSeriesId() {
        return mSeries.mSeriesRecordingId;
    }

    public List<String> getDeletedEpisodes() {
        if (mSeries != null && mSeries.mDeletedRecordingIds != null) {
            return mSeries.mDeletedRecordingIds;
        }
        return Collections.emptyList();
    }

    @XmlRootElement(name = "Series")
    private static class Series {
        @XmlElement(name = "userSeriesId")
        private String mSeriesRecordingId;

        @XmlElementWrapper(name = "Episodes")
        @XmlElement(name = "RecordID")
        private List<String> mDeletedRecordingIds;
    }
}
