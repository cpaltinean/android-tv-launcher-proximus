package tv.threess.threeready.data.proximus.vod.model.response;

import androidx.annotation.NonNull;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import tv.threess.threeready.api.vod.model.CurrencyType;
import tv.threess.threeready.api.vod.model.IRentalExpiration;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodVariant;


/**
 * Proximus implementation of the category item.
 * Used to store info for vod assets.
 *
 * @author Andor Lukacs
 * @since 10/2/18
 */
public class VodItemProximus extends BaseVodItemProximus implements IVodItem {

    private final String mId;
    private final List<VodVariantProximus> mVariantList;
    private IVodPrice mCheapestPrice;

    public VodItemProximus(String id, List<VodVariantProximus> variantList, @NonNull Map<String, InvoicePrice> prices) {
        super(variantList.get(0), variantList.stream()
                .map(VodVariantProximus::getId).collect(Collectors.toList()), prices);
        mId = id;
        mVariantList = variantList;
    }

    @Override
    public String getId() {
        return mId;
    }

    /**
     * @return A list of all available variants.
     */
    @Override
    public List<VodVariantProximus> getVariantList() {
        return mVariantList;
    }

    /**
     * @return True if any of the VOD variants are rented.
     */
    @Override
    public boolean isRented() {
        for (IVodVariant vodVariant : getVariantList()) {
            if (vodVariant.isRented()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public long getRentalPeriodEnd() {
        List<IVodVariant> rentedVariants = getRentedVariants();
        if (!rentedVariants.isEmpty()) {
            rentedVariants.sort((o1, o2) -> Comparator.nullsLast(Long::compareTo)
                    .compare(o1.getRentalPeriodEnd(), o2.getRentalPeriodEnd()));
            return rentedVariants.get(0).getRentalPeriodEnd();
        }
        return 0;
    }

    @Override
    public void setSeriesId(String seriesId) {
        super.setSeriesId(seriesId);
        for (VodVariantProximus variant : getVariantList()) {
            variant.setSeriesId(seriesId);
        }
    }

    @Override
    public boolean isHD() {
        for (VodVariantProximus variant : getVariantList()) {
            if (variant.isHD()) {
                return true;
            }
        }
        return super.isHD();
    }

    @Override
    public IVodPrice getCheapestPrice() {
        return mCheapestPrice;
    }

    @Override
    public boolean isLastChance() {
        long longestAvailability = mMainVariant.getLicensePeriodEnd();
        for (VodVariantProximus variant: mVariantList) {
            if (variant.getLicensePeriodEnd() > longestAvailability) {
                longestAvailability = variant.getLicensePeriodEnd();
            }
        }
        return longestAvailability <= (System.currentTimeMillis() + TimeUnit.DAYS.toMillis(LAST_CHANCE_DAYS));
    }

    @Override
    public boolean isNew() {
        for (VodVariantProximus variant: mVariantList) {
            if (variant.isNew()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void setRentalExpirationInfo(Map<String, IRentalExpiration> rentalExpirationMap) {
        super.setRentalExpirationInfo(rentalExpirationMap);

        for (VodVariantProximus variant: mVariantList) {
            IRentalExpiration rentalExpiration = rentalExpirationMap.get(variant.getId());
            if (rentalExpiration != null) {
                variant.setRentalExpirationInfo(rentalExpiration);
            }
        }
    }

    @Override
    public void updateCheapestPrice(double creditPriceLimit, int freeRentals) {
        mCheapestPrice = null;

        // Update rent info with credits.
        mVariantList.forEach(variant -> {
            RentOfferProximus rentOffer = variant.getRentOffer();
            if (rentOffer != null) {
                rentOffer.updatePriceOptions(mVariantPrices.get(variant.getId()), rentOffer.getRentalCost() < creditPriceLimit && freeRentals > 0);
            }
        });

        // Calculate cheapest price.
        for (VodVariantProximus variantProximus : mVariantList) {
            RentOfferProximus rentOffer = variantProximus.getRentOffer();
            if (rentOffer != null) {
                if (rentOffer.canPurchaseForCredit(freeRentals)) {
                    mCheapestPrice = rentOffer.getPriceForCurrency(CurrencyType.Credit);
                    break;
                }

                IVodPrice vodPrice = rentOffer.getPriceForCurrency(CurrencyType.Euro);
                if (mCheapestPrice == null ||
                        vodPrice != null && vodPrice.getCurrencyAmount() < mCheapestPrice.getCurrencyAmount()) {
                    mCheapestPrice = rentOffer.getPriceForCurrency(CurrencyType.Euro);
                }
            }
        }
    }
}
