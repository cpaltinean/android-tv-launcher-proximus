package tv.threess.threeready.data.proximus.vod.model.response;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.data.proximus.vod.model.adapter.YesAsBooleanTypeAdapter;

/**
 * VOD categories coming from the global install.
 *
 * @author Barabas Attila
 * @since 2020.04.28
 */
@XmlRootElement(name = "Category")
public class VodGlobalCategoryProximus extends VodCategoryProximus {

    @XmlAttribute(name = "ID")
    private String mInternalId;

    @XmlElement(name = "ExternalID")
    private String mCategoryId;

    @XmlAttribute(name = "ParentID")
    private String mParentId;

    @XmlElement(name = "DisplayName")
    private List<CategoryDetail> mCategoryDetails;

    @XmlElement(name = "Name")
    private String mCategoryName;

    @XmlJavaTypeAdapter(YesAsBooleanTypeAdapter.class)
    @XmlAttribute(name = "IsLeaf")
    private Boolean mIsLeaf;

    @Override
    public String getName() {
        return mCategoryName;
    }

    /**
     * @return Unique identifier of the category.
     */
    @Override
    public String getId() {
        return mCategoryId;
    }

    /**
     * @return The internal identifier of the category.
     * Should be used only the build the category tree.
     */
    public String getInternalId() {
        return mInternalId;
    }

    /**
     * @return The internal identifier of the parent category.
     */
    public String getParentId() {
        return mParentId;
    }

    /**
     * @return True if the category doesn't contain other subcategories.
     */
    @Override
    public boolean isLeaf() {
        return mIsLeaf;
    }

    /**
     * @param language The UI language.
     * @return The title of the category to be displayed on the UI.
     */
    public String getDisplayName(String language) {
        if (mCategoryDetails == null) {
            return "";
        }

        // Filter details with empty names.
        List<CategoryDetail> categoryDetails = mCategoryDetails.stream()
                .filter((c) -> !TextUtils.isEmpty(c.mDisplayName.trim()))
                .collect(Collectors.toList());

        // Search for display name for the given language.
        for (CategoryDetail categoryDetail : categoryDetails) {
            if (language.equalsIgnoreCase(categoryDetail.mLanguage)) {
                return categoryDetail.mDisplayName;
            }
        }

        // Use the first as default.
        if (!categoryDetails.isEmpty()) {
            return categoryDetails.get(0).mDisplayName;
        }

        return "";
    }

    @Override
    public String getTitle() {
        String displayName = getDisplayName(LocaleUtils.getApplicationLanguage());
        return !TextUtils.isEmpty(displayName) ? displayName : mCategoryName;
    }

    @Override
    public List<IImageSource> getImageSources() {
        return Collections.singletonList(getLocalizedImageOrFirst());
    }

    @Nullable
    public IImageSource getLocalizedImageOrFirst() {
        return getLocalizedImageOrFirst(LocaleUtils.getApplicationLanguage());
    }

    @XmlRootElement(name = "DisplayName")
    private static class CategoryDetail implements Serializable {

        @XmlAttribute(name = "Name")
        private String mDisplayName;

        @XmlAttribute(name = "LanguageType")
        private String mLanguage;
    }
}
