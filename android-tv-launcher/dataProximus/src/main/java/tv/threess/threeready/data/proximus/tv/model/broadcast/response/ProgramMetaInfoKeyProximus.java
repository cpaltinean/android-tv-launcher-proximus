package tv.threess.threeready.data.proximus.tv.model.broadcast.response;

/**
 * Proximus program meta info keys.
 *
 * @author Barabas Attila
 * @since 2019.10.30
 */
public enum ProgramMetaInfoKeyProximus implements MetaInfoProximus.IKey {
    ProgramReferenceNo("programReferenceNo"),
    Title("title"),
    SeasonNumber("seasonNumber"),
    EpisodeNumber("episodeNumber"),
    Category("category"),
    Subcategory("subCategory"),
    ReleaseYear("releaseYear"),
    Description("description"),
    EpisodeTitle("episodeTitle"),
    Actor1("actor1"),
    Actor2("actor2"),
    Actor3("actor3"),
    Actor4("actor4"),
    Actor5("actor5"),
    Actor6("actor6"),
    Director("director"),
    ParentalRating("VCHIP"),
    PosterFileName("posterFileName"),
    Blacklist("blacklist"),
    DisableNPVR("disableNPVR"),
    SeriesId("seriesID"),
    HasDescriptiveAudio("SupportForVisuallyImpaired"),
    HasDescriptiveSubtitle("SupportForHearingImpaired"),
    AssetIdentifier("AssetIdentifier");

    private final String mKey;

    ProgramMetaInfoKeyProximus(String key) {
        mKey = key;
    }

    @Override
    public String getName() {
        return mKey;
    }
}
