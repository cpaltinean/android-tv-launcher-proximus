/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.home;

import android.app.Application;
import android.text.TextUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Response;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.proximus.account.SessionProximus;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.proximus.account.retrofit.BootConfigRetrofitProximus;
import tv.threess.threeready.data.proximus.account.retrofit.ConfigJsonRetrofitProximus;
import tv.threess.threeready.data.proximus.generic.RetrofitServicesProximus;
import tv.threess.threeready.data.proximus.generic.interceptor.CacheControlOverrideOkHttpInterceptor.CacheControlOverrideOption;
import tv.threess.threeready.data.proximus.home.model.editorial.BannerFeedbackRequest;
import tv.threess.threeready.data.proximus.home.model.editorial.BannerFeedbackRequest.FeedbackType;
import tv.threess.threeready.data.proximus.home.model.selector.SwimlaneSelector;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneContentResponseProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneHubResponseProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneParamResponsibleProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneQueryParamProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneSortOptionProximus;
import tv.threess.threeready.data.proximus.home.retrofit.BTASwimLaneRetrofitProximus;
import tv.threess.threeready.data.proximus.log.retrofit.DataCollectionRetrofitProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodShopLanguageProximus;

/**
 * HomeProxy class implementation for the proximus backend.
 *
 * @author Barabas Attila
 * @since 2018.10.11
 */
public class HomeProxyProximus implements Component {
    private static final String TAG = Log.tag(HomeProxyProximus.class);

    // Example : EPG_M_%(mvGnr)s_%(ecSrt)s -> mvGnr, ecSrt
    private static final Pattern VALUE_TOKEN_PATTERN = Pattern.compile("\\%\\(((.*?))\\)[s,i,b]");

    protected Application mApp;

    private final RetrofitServicesProximus mRetrofit = Components.get(RetrofitServicesProximus.class);
    private final BTASwimLaneRetrofitProximus mSwimLaneRetrofit = mRetrofit.getSwimLaneRetrofit();
    private final DataCollectionRetrofitProximus mReportingRetrofit = mRetrofit.getDataCollectionRetrofit();
    private final ConfigJsonRetrofitProximus mJsonConfigRetrofit = mRetrofit.getJsonConfigRetrofit();

    public HomeProxyProximus(Application application) {
        mApp = application;
    }

    /**
     * Load the configuration file with the given id from the backend.
     *
     * @param language   The language for which the config should bre returned
     * @param appVersion The version name of the application for which the config should be returned.
     */
    InputStream getRemoteConfig(String language, String appVersion) throws IOException {
        // do not force internet check in this case
        Response<ResponseBody> response = mJsonConfigRetrofit.getConfig(language, appVersion).execute();
        if (!response.isSuccessful() || response.body() == null) {
            throw new BackendException(response.code(), response.message());
        }
        return response.body().byteStream();
    }

    /**
     * Load the translation file  with the given language and app version from the backend.
     *
     * @param language   The language for which the file should be returned.
     * @param appVersion The app version for which the file should e returned.
     */
    InputStream getRemoteTranslations(String language, String appVersion) throws IOException {
        // do not force internet check in this case
        Response<ResponseBody> response = mJsonConfigRetrofit.getTranslations(language, appVersion).execute();
        if (!response.isSuccessful() || response.body() == null) {
            throw new BackendException(response.code(), response.message());
        }
        return response.body().byteStream();
    }

    public SwimLaneContentResponseProximus geSwimlaneContentItems(SwimLaneProximus swimLane, int start, int count,
                                                                  List<DataSourceSelector> selectors) throws IOException {
        CacheControlOverrideOption cacheOverride = new CacheControlOverrideOption(
                swimLane.getMinClientCacheTime(TimeUnit.MILLISECONDS),
                swimLane.getMaxClientCacheTime(TimeUnit.MILLISECONDS));

        List<SwimLaneQueryParamProximus> params = new ArrayList<>(swimLane.getParameters());
        Map<String, String> queryTokenValueMap = new HashMap<>();

        // Process static tile selectors.
        for (DataSourceSelector selector : selectors) {
            if (selector.getSelectedOption() instanceof SwimlaneSelector) {
                SwimlaneSelector selectedOption =
                        (SwimlaneSelector) selector.getSelectedOption();
                if (selectedOption == null || selectedOption.getTokenName() == null || selectedOption.getTokenValue() == null) {
                    continue;
                }
                queryTokenValueMap.put(selectedOption.getTokenName(), selectedOption.getTokenValue());
                params.addAll(selectedOption.getParameters());
            }
        }

        // Add swimlane specific part
        String swimlaneId = swimLane.getId();
        String queryType = swimLane.getItemQueryType();
        String queryName = swimLane.getItemQueryName();

        // Process sort options
        for (DataSourceSelector selector : selectors) {
            if (selector.getSelectedOption() instanceof SwimLaneSortOptionProximus) {
                SwimLaneSortOptionProximus sortOption =
                        (SwimLaneSortOptionProximus) selector.getSelectedOption();
                if (sortOption == null) {
                    continue;
                }
                queryType = sortOption.getItemQueryType();
                queryName = sortOption.getItemQueryName();
                params.addAll(sortOption.getParameters());
                if (!sortOption.useSwimLaneQueryParameters()) {
                    params.removeAll(swimLane.getParameters());
                }
            }
        }

        // Replace token in query name.
        Matcher queryNameMatcher = VALUE_TOKEN_PATTERN.matcher(queryName);
        while (queryNameMatcher.find()) {
            if (queryNameMatcher.groupCount() > 1) {
                String value = queryTokenValueMap.get(queryNameMatcher.group(1));
                if (value != null) {
                    queryName = queryName.replace(queryNameMatcher.group(0), value);
                }
            }
        }

        VodShopLanguageProximus shopLanguage = new VodShopLanguageProximus(
                SettingsProximus.shopLanguage.getList());

        params.sort(Comparator.comparingInt(SwimLaneQueryParamProximus::getListSort));

        List<String> queryParams = new ArrayList<>();
        queryParams.add(createQueryParam(SwimLaneProximus.KEY_SWIMLANE_ID, swimlaneId));
        queryParams.add(createQueryParam(SwimLaneProximus.KEY_ITEM_QUERY_TYPE, queryType));
        queryParams.add(createQueryParam(SwimLaneProximus.KEY_ITEM_QUERY_NAME, queryName));

        for (SwimLaneQueryParamProximus param : ArrayUtils.notNull(params)) {
            String value = param.getValueToken();

            if (param.getResponsible() == SwimLaneParamResponsibleProximus.PageVariable) {
                // Extend with the selected channel id/name.
                String valueToken = param.getValueToken();
                Matcher paramMatcher = VALUE_TOKEN_PATTERN.matcher(valueToken);
                if (paramMatcher.find() && paramMatcher.groupCount() > 1) {
                    String tokenValue = queryTokenValueMap.get(paramMatcher.group(1));
                    if (tokenValue != null) {
                        value = valueToken.replace(paramMatcher.group(0), tokenValue);
                    }
                }
            // Extend the request with the client tokens.
            } else if (param.getResponsible() == SwimLaneParamResponsibleProximus.ClientToken) {
                switch (param.getValueToken()) {
                    default:
                        value = null;
                        Log.w(TAG, "Unknown client token. Skipp parameter. " + param.getValueToken());
                        break;

                    // Extend with the selected shop language.
                    case "%(catLang)s":
                        value = shopLanguage.toString();
                        break;

                    // Extend with device type. Always STB in our case.
                    case "%(clientType)s":
                        value = "7";
                        break;

                    // Extend with the selected UI language.
                    case "%(uiLang)s":
                        value = LocaleUtils.getApplicationLanguage();
                        break;

                    // Extend with the page size.
                    case "%(pageSize)i":
                        value = String.valueOf(count);
                        break;

                    // Extend with the page offset.
                    case "%(listOffset)i":
                        value = String.valueOf(start);
                        break;
                }
            }

            if (value != null) {
                queryParams.add(createQueryParam(param.getName(), value));
            }
        }

        String combinerParams = TextUtils.join("&", queryParams);

        String swimlaneUrl = BootConfigRetrofitProximus.BASE_URI
                + BTASwimLaneRetrofitProximus.SWIMLANE_ITEM_PATH + combinerParams;

        return RetrofitServicesProximus.execute(mSwimLaneRetrofit.getSwimLaneItems(swimlaneUrl, cacheOverride));
    }

    /**
     * Method used for clear the Http cache of the given swimlane.
     * @param moduleId  For identifying the correct swimlane.
     */
    public void clearCacheByModuleConfigId(String moduleId) throws IOException {
        mRetrofit.clearCacheByModuleConfigId(moduleId);
    }

    /**
     * @return The hub definition from backend with all the available swim lanes.
     * @throws IOException In case of errors.
     */
    public synchronized SwimLaneHubResponseProximus getSwimLaneHub() throws IOException {
        return RetrofitServicesProximus.execute(mSwimLaneRetrofit.getSwimLaneHub());
    }

    /**
     * @return Swimlane access point and hub definition as stream.
     * @throws IOException
     */
    public InputStream getSwimlaneHubAsStream() throws IOException {
        return RetrofitServicesProximus.executeGen(
                mSwimLaneRetrofit.getSwimLaneHubResponse()).byteStream();
    }

    /**
     * Send feedback to the sever for banner click or view.
     *
     * @param feedbackType The type of the action which triggered the feedback (e.g click, view)
     * @param feedbackCode The identifier of the feedback which was triggered.
     * @throws IOException In case of network or backend errors.
     */
    public void bannerFeedback(FeedbackType feedbackType, String feedbackCode) throws IOException {
        String feedbackUrl = SessionProximus.bannerFeedbackUrl.get("");
        String accountNumber = Settings.accountNumber.get("");
        BannerFeedbackRequest request = new BannerFeedbackRequest(feedbackType, accountNumber, feedbackCode);
        RetrofitServicesProximus.executeGen(mReportingRetrofit.sendBannerFeedback(feedbackUrl, request));
    }

    private static String createQueryParam(String key, String value) throws UnsupportedEncodingException {
        return URLEncoder.encode(key, StandardCharsets.UTF_8.toString())
                + "=" + URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
    }
}
