package tv.threess.threeready.data.proximus.pvr.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.pvr.model.ISeriesRecordingSchedule;

/**
 * Proximus Series entity used for Series Recording List.
 *
 * Created by Daniela Toma on 04/16/2020.
 */
@XmlRootElement(name = "Series")
public class SeriesRecordingSchedule implements ISeriesRecordingSchedule {

    @XmlElement(name = "SeriesRefNo")
    private String mSeriesRefNo;

    @XmlElement(name = "userSeriesId")
    private String mUserSeriesId;

    @XmlElement(name = "Title")
    private String mTitle;

    @XmlElement(name = "ChannelExtID")
    private String mChannelId;

    @Override
    public String getSeriesRefNo() {
        return mSeriesRefNo;
    }

    @Override
    public String getUserSeriesId() {
        return mUserSeriesId;
    }

    @Override
    public String getChannelId() {
        return mChannelId;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }
}
