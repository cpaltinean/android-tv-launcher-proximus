package tv.threess.threeready.data.proximus.pvr.model.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Retrofit request class for schedule series recording.
 *
 * Created by Noemi Dali on 21.04.2020.
 */
@XmlRootElement(name = "BTA")
public class RecordingScheduleSeriesRequest implements Serializable {

    @XmlElement(name = "AddSeriesRecordingDoc")
    private final AddSeriesRecordingDoc mRecordingDoc;

    public RecordingScheduleSeriesRequest(String trailId, String channelId) {
        mRecordingDoc = new AddSeriesRecordingDoc();
        mRecordingDoc.mSchTrailID = trailId;
        mRecordingDoc.mChannelExtId = channelId;
    }

    @XmlRootElement(name = "AddSeriesRecordingDoc")
    private static class AddSeriesRecordingDoc implements Serializable {

        @XmlElement(name = "ContentType")
        private final String mContentType = "NPVR";

        @XmlElement(name = "SchTrailID")
        private String mSchTrailID;

        @XmlElement(name = "ChannelExtID")
        private String mChannelExtId;

        @XmlElement(name = "DeleteWhenSpaceNeeded")
        private final boolean mDeleteWhenSpaceNeeded = true;

        @XmlElement(name = "ChannelBound")
        private final boolean mChannelBound = true;

        @XmlElement(name = "EpisodeScope")
        private final String mEpisodeScope = "ALL";

        @XmlElement(name = "NrEpisodesToKeep")
        private final int mNrEpisodesToKepp =  -1;

        @XmlElement(name = "Private")
        private final boolean mPrivate = false;
    }
}
