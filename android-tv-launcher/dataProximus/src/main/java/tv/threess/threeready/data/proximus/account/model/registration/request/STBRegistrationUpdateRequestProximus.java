package tv.threess.threeready.data.proximus.account.model.registration.request;

import java.io.Serializable;
import java.util.Objects;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * BTA request to update the registration info a device.
 *
 * @author Barabas Attila
 * @since 2020.04.15
 */
@XmlRootElement(name = "BTA")
public class STBRegistrationUpdateRequestProximus implements Serializable {

    @XmlElement(name = "UpdateSTBRegistrationInfoDoc")
    private final UpdateSTBRegistrationInfoDocProximus mDoc = new UpdateSTBRegistrationInfoDocProximus();

    public STBRegistrationUpdateRequestProximus (String ipAddress, String softwareVersion,
                                                 String deviceId, String stbName, String esn, String uiVersion) {
        mDoc.mIPAddress = ipAddress;
        mDoc.mSoftwareVersion = softwareVersion;
        mDoc.mCASDeviceID = deviceId;
        mDoc.mName = stbName;
        mDoc.mESN = esn;
        mDoc.mUIVersion = uiVersion;
    }

    /**
     * @return Hash code from all the registration info parameters which can change.
     */
    public int getRegistrationInfoHash() {
        return Objects.hash(mDoc.mIPAddress, mDoc.mSoftwareVersion,
                mDoc.mCASDeviceID, mDoc.mName, mDoc.mESN, mDoc.mUIVersion);
    }

    @XmlRootElement(name = "UpdateSTBRegistrationInfoDoc")
    private static class UpdateSTBRegistrationInfoDocProximus {

        @XmlElement(name = "IPAddress")
        private String mIPAddress;

        @XmlElement(name = "STBSoftwareVersion")
        private String mSoftwareVersion;

        @XmlElement(name = "CASDeviceID")
        private String mCASDeviceID;

        @XmlElement(name = "STBName")
        private String mName;

        @XmlElement(name = "ESN")
        private String mESN;

        @XmlElement(name = "STBUIVersion")
        private String mUIVersion;
    }
}
