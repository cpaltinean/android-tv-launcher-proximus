package tv.threess.threeready.data.proximus.playback.model.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.pvr.model.IRecording;

/**
 * Request playback and DRM url from backend for a recording playback.
 *
 * @author Barabas Attila
 * @since 2020.04.08
 */
@XmlRootElement(name = "BTA")
public class RecordingStreamingSessionRequestProximus implements Serializable {

    @XmlElement(name = "URLDoc")
    private final URLDoc mURLDoc;

    public RecordingStreamingSessionRequestProximus(IRecording recording) {
        mURLDoc = new URLDoc();
        mURLDoc.mRecording = new Recording();
        mURLDoc.mRecording.mRecordingId = recording.getRecordingId();
    }

    @XmlRootElement(name = "URLDoc")
    private static class URLDoc implements Serializable {

        @XmlElement(name = "Recording")
        private Recording mRecording;

        @XmlElement(name = "AcceptedMIMETypes")
        private final String mimeTypes = "application/dash+xml";
    }

    @XmlRootElement(name = "Recording")
    private static class Recording implements Serializable {
        @XmlElement(name = "UserRecordID")
        private String mRecordingId;
    }
}