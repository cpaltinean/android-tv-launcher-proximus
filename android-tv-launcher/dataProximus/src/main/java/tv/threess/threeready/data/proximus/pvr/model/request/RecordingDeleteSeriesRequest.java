package tv.threess.threeready.data.proximus.pvr.model.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Retrofit request class for canceling series recording.
 *
 * Created by Noemi Dali on 21.04.2020.
 */
@XmlRootElement(name = "BTA")
public class RecordingDeleteSeriesRequest implements Serializable {

    @XmlElement(name = "DeleteSeriesRecording")
    private final DeleteSeriesRecording mDeleteSeriesRecording;

    public RecordingDeleteSeriesRequest(String seriesId, boolean keepEpisodes) {
        mDeleteSeriesRecording = new DeleteSeriesRecording();
        mDeleteSeriesRecording.mSeriesRecordingId = seriesId;
        mDeleteSeriesRecording.mKeepRecordings = keepEpisodes;
    }

    @XmlRootElement(name = "DeleteSeriesRecording")
    private static class DeleteSeriesRecording implements Serializable {

        @XmlAttribute(name = "userSeriesID")
        private String mSeriesRecordingId;

        @XmlElement(name = "KeepRecordings")
        private boolean mKeepRecordings;
    }
}
