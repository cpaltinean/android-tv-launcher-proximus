package tv.threess.threeready.data.proximus.playback.model.request;

/**
 * TODO: Class description
 *
 * @author Barabas Attila
 * @since 2020.04.09
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;

/**
 * Request playback and DRM url from backend for live ott playback on a channel..
 *
 * @author Barabas Attila
 * @since 2020.04.08
 */
@JacksonXmlRootElement(localName = "BTA")
@JsonIgnoreProperties(ignoreUnknown =  true)
public class ChannelStreamingSessionRequestProximus implements Serializable {

    @JacksonXmlProperty(localName = "URLDoc")
    private final URLDoc mUrlDoc;

    public ChannelStreamingSessionRequestProximus(String channelId) {
        mUrlDoc = new URLDoc(channelId);
    }

    @JacksonXmlRootElement(localName = "URLDoc")
    @JsonIgnoreProperties(ignoreUnknown =  true)
    private static class URLDoc implements Serializable {

        private URLDoc(String channelId) {
            mChannelId = channelId;
        }

        @JacksonXmlProperty(localName = "ChannelExtID")
        private final String mChannelId;

        @JacksonXmlProperty(localName = "AcceptedMIMETypes")
        private final String mimeTypes = "application/dash+xml";
    }
}

