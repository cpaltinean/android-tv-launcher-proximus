/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.proximus.search;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.search.SearchRepository;
import tv.threess.threeready.api.search.model.SearchSuggestion;
import tv.threess.threeready.api.search.model.SearchTerm;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.tv.TvCache;

/**
 * Implementation of the Proximus specific search handler methods.
 *
 * @author Barabas Attila
 * @since 2018.04.25
 */
public class SearchRepositoryProximus implements SearchRepository {
    private static final String TAG = Log.tag(SearchRepositoryProximus.class);

    private final SearchProxyProximus mSearchProxy = Components.get(SearchProxyProximus.class);
    private final TvCache mTvCache = Components.get(TvCache.class);

    @Override
    public Observable<ModuleData<TvChannel>> channelSearch(ModuleConfig config, int start, int limit) {
        return Observable.fromCallable(() -> {
            ModuleDataSource dataSource = config.getDataSource();

            if (dataSource == null || dataSource.getParams() == null) {
                return new ModuleData<>(config, Collections.emptyList());
            }

            Log.d(TAG, "channelSearch() called with: moduleDataSource = ["
                    + dataSource + "], start = [" + start + "], count = [" + limit + "]");

            SearchTerm searchTerm = dataSource.getParams()
                    .getFilter(ModuleFilterOption.Mixed.SearchTerm.NAME, SearchTerm.class);

            if (searchTerm == null) {
                return new ModuleData<>(config, Collections.emptyList());
            }

            return new ModuleData<>(config, mTvCache.getChannelForSearch(searchTerm, start, limit));
        });
    }

    @Override
    public List<TvChannel> channelVoiceSearch(SearchTerm searchTerm, int start, int limit) throws IOException {
        return mTvCache.getChannelForSearch(searchTerm, start, limit);
    }

    @Override
    public List<IContentItem> broadcastVoiceSearch(SearchTerm searchTerm, int start, int limit) throws IOException {
        return mSearchProxy.broadcastVoiceSearch(searchTerm, start, limit);
    }

    @Override
    public List<IContentItem> vodVoiceSearch(SearchTerm searchTerm, int start, int limit) throws IOException {
        return mSearchProxy.vodVoiceSearch(searchTerm, start, limit);
    }

    @Override
    public TvChannel channelVoicePlaySearch(SearchTerm searchTerm) {
        return mTvCache.getChannelForVoicePlaySearch(searchTerm);
    }

    @Override
    public IContentItem contentVoicePlaySearch(SearchTerm searchTerm) throws IOException {
        return mSearchProxy.contentVoicePlaySearch(searchTerm);
    }

    @Override
    public Observable<List<SearchSuggestion>> getSuggestions(final String text) {
        return Observable.create(emitter -> {
            List<SearchSuggestion> suggestions = mSearchProxy.getSuggestions(text);
            emitter.onNext(suggestions);
            emitter.onComplete();
        });
    }
}
