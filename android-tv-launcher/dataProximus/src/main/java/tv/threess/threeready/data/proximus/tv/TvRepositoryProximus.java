/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.tv;

import android.app.Application;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.text.TextUtils;

import androidx.annotation.WorkerThread;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.account.BookmarkRepository;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.EpgFetchObservable;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.tv.TvChannelCacheType;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.PlaybackOption;
import tv.threess.threeready.api.tv.model.TvAdReplacement;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.generic.observable.WaitForUpdateObservable;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.proximus.tv.observable.BroadcastDetailObservable;
import tv.threess.threeready.data.proximus.tv.observable.EpgGridBroadcastObservable;
import tv.threess.threeready.data.pvr.observable.RecordingBookmarkObservable;
import tv.threess.threeready.data.tv.BaseTvRepository;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.tv.TvService;
import tv.threess.threeready.data.tv.observable.BabyChannelObservable;
import tv.threess.threeready.data.tv.observable.BroadcastBookmarkObservable;
import tv.threess.threeready.data.tv.observable.ChannelByNumberObservable;
import tv.threess.threeready.data.tv.observable.ChannelScanObservable;
import tv.threess.threeready.data.tv.observable.EpgBroadcastsObservable;
import tv.threess.threeready.data.tv.observable.EpgGridChannelsObservable;
import tv.threess.threeready.data.tv.observable.FirstFavoriteObservable;
import tv.threess.threeready.data.tv.observable.LastPlayedChannelIdChangeObservable;
import tv.threess.threeready.data.tv.observable.NextBroadcastObservable;
import tv.threess.threeready.data.tv.observable.PlaybackOptionsObservable;
import tv.threess.threeready.data.tv.observable.PreviousBroadcastObservable;
import tv.threess.threeready.data.tv.observable.ReplayBookmarkObservable;
import tv.threess.threeready.data.tv.observable.TvEPGChannelsObservable;
import tv.threess.threeready.data.tv.observable.UpcomingBroadcastsObservable;

/**
 * Implementation of the Proximus specific tv handler methods.
 *
 * @author Barabas Attila
 * @since 2018.06.05
 */
public class TvRepositoryProximus extends BaseTvRepository implements TvRepository, Component {
    private static final String TAG = Log.tag(TvRepositoryProximus.class);

    private final TvProxyProximus mTvProxy = Components.get(TvProxyProximus.class);

    private final BookmarkRepository mBookmarkRepository = Components.get(BookmarkRepository.class);
    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);
    private final TvCache mTvCache = Components.get(TvCache.class);

    public TvRepositoryProximus(Application application) {
        super(application);
    }

    @Override
    public boolean isAdultChannel(String channelId) {
        List<String> adultChannelIds = SettingsProximus.adultChannelIds.getList();
        return adultChannelIds != null && adultChannelIds.contains(channelId);
    }

    @Override
    public void trackAdReplacement(TvAdReplacement mAdReplacement, TvAdReplacement.Event event) {
        mTvProxy.trackAdReplacement(mAdReplacement, event);
    }

    @Override
    public void trackAdReplacement(TvAdReplacement replacement, TvAdReplacement.Error error) {
        mTvProxy.trackAdReplacement(replacement, error);
    }

    @Override
    public void startBroadcastUpdate() {
        BaseWorkManager.start(mApp, TvService.buildBroadcastUpdateIntent());
    }

    @Override
    public TvAdReplacement getAdReplacement(String channelId, double duration, String segmentDesc) throws IOException {
        return mTvProxy.getAdReplacement(channelId, duration, segmentDesc);
    }

    @Override
    public Observable<Map<TvChannelCacheType, List<TvChannel>>> getEpgChannels() {
        return Observable.create(new TvEPGChannelsObservable(mApp));
    }

    @Override
    public EpgFetchObservable<List<IBroadcast>> getEpgGridBroadcasts(TvChannel channel, List<IBroadcast> programs, boolean isAdultChannel, long windowStart, long windowEnd) {
        return new EpgGridBroadcastObservable(mApp, channel, programs, isAdultChannel, windowStart, windowEnd);
    }

    @Override
    public Observable<Map<TvChannelCacheType, List<TvChannel>>> getEpgGridChannels() {
        return Observable.create(new EpgGridChannelsObservable(mApp));
    }

    @Override
    public EpgFetchObservable<List<IBroadcast>> getEpgBroadcastsBetweenTimestamps(TvChannel channel,
                                                                          long from,
                                                                          long to,
                                                                          long windowStartLength,
                                                                          long windowEndLength,
                                                                          String dummyLoadingProgramTitle,
                                                                          String dummyNoInfoProgramTitle) {
        return new EpgBroadcastsObservable(mApp,
                channel, from, to, isAdultChannel(channel.getId()), windowStartLength, windowEndLength, dummyLoadingProgramTitle, dummyNoInfoProgramTitle);
    }

    @Override
    public EpgFetchObservable<List<IBroadcast>> getEpgBroadcastsBetweenTimestamps(TvChannel channel,
                                                                          long from,
                                                                          long to,
                                                                          String dummyLoadingProgramTitle,
                                                                          String dummyNoInfoProgramTitle) {
        return getEpgBroadcastsBetweenTimestamps(channel, from, to, 0, 0, dummyLoadingProgramTitle, dummyNoInfoProgramTitle);
    }

    @Override
    public Observable<ModuleData<IBroadcast>> getUpcomingBroadcastsBetween(String channelId, long from, long to, boolean playable, ModuleConfig config) {
        long windowStart = TimeBuilder.utc()
                .set(System.currentTimeMillis() - Settings.epgPastWindowLength.get(TimeUnit.DAYS.toMillis(7)))
                .ceil(TimeUnit.HOURS).get();
        if (from < windowStart) {
            from = windowStart;
        }

        long windowEnd = TimeBuilder.utc()
                .set(System.currentTimeMillis() + Settings.epgFutureWindowLength.get(TimeUnit.DAYS.toMillis(7)))
                .ceil(TimeUnit.HOURS).get();
        if (to > windowEnd) {
            to = windowEnd;
        }

        return Observable.create(new UpcomingBroadcastsObservable(mApp, channelId, from, to, isAdultChannel(channelId), playable, config));
    }

    @Override
    public Observable<IBroadcast> getBroadcast(String broadcastId) {
        return Observable.create(new BroadcastDetailObservable(mApp, broadcastId));
    }

    @Override
    public Observable<IBroadcast> getBroadcast(IBroadcast broadcast) {
        return Observable.create(new BroadcastDetailObservable(mApp, broadcast));
    }

    @Override
    public Observable<IBroadcast> getNextBroadcast(IBroadcast broadcast) {
        return Observable.create(new NextBroadcastObservable(mApp, broadcast, isAdultChannel(broadcast.getChannelId())));
    }

    @Override
    public Observable<IBroadcast> getPreviousBroadcast(IBroadcast broadcast) {
        return Observable.create(new PreviousBroadcastObservable(mApp, broadcast, isAdultChannel(broadcast.getChannelId())));
    }

    @Override
    public Completable waitForChannelUpdate() {
        return Completable.fromObservable(
                Observable.create(new WaitForUpdateObservable(mApp, channelUpdater)));
    }

    @Override
    public Completable waitForBroadcastUpdate() {
        return Completable.fromObservable(
                Observable.create(new WaitForUpdateObservable(mApp, baseBroadcastUpdater)));
    }

    @Override
    public Single<Boolean> scanChannels() {
        return Single.fromObservable(Observable.create(new ChannelScanObservable(mApp)));
    }

    @Override
    public Observable<String> getAppChannelPackageId(final String channelId) {
        return Observable.create(emitter -> {
            emitter.onNext(mTvCache.getAppChannelPackageId(channelId));
            emitter.onComplete();
        });
    }

    @Override
    public Observable<List<PlaybackOption>> getPlaybackOptions(final String channelId,
                                                               boolean onlyAvailable) {
        return Observable.create(new PlaybackOptionsObservable(mApp, channelId, onlyAvailable));
    }

    @Override
    public Observable<TvChannel> getChannelByNumber(int number) {
        return Observable.create(new ChannelByNumberObservable(number));
    }

    @Override
    public Observable<TvChannel> getFirstFavorite() {
        return Observable.create(new FirstFavoriteObservable(mApp));
    }

    @Override
    @WorkerThread
    public void synchronizeFavoriteChannels(Set<String> addChannel, Set<String> removeList) throws OperationApplicationException, RemoteException, IOException {
        mTvCache.synchronizeFavoriteChannels(addChannel, removeList);

        // upload to BEP
        List<String> channelsList = mTvCache.getFavoriteChannelIds();
        mAccountRepository.updateFavoriteChannels(TextUtils.join(",", channelsList));
    }

    @Override
    public Observable<List<TvChannel>> getBabyChannels() {
        return Observable.create(new BabyChannelObservable(mApp));
    }

    @Override
    public Observable<IBookmark> getBookmarkForBroadcast(IBroadcast broadcast, boolean continueWatch) {
        return Observable.create(new BroadcastBookmarkObservable(mApp, broadcast, continueWatch));
    }

    @Override
    public Observable<IBookmark> getRecordingBookmarkForBroadcast(IBroadcast broadcast) {
        return Observable.create(new RecordingBookmarkObservable(mApp, broadcast));
    }

    @Override
    public Observable<IBookmark> getReplayBookmarkForBroadcast(IBroadcast broadcast) {
        return Observable.create(new ReplayBookmarkObservable(mApp, broadcast));
    }

    @Override
    public Completable addReplayBookmark(IBroadcast broadcast, long position, long duration) {
        return mBookmarkRepository.addReplayBookmark(broadcast, position, duration);
    }

    @Override
    public Completable addRecordingBookmark(IRecording recording, long position, long duration) {
        return mBookmarkRepository.addRecordingBookmark(recording, position, duration);
    }

    @Override
    public Observable<String> getLastPlayedChannelIdChange() {
        return Observable.create(new LastPlayedChannelIdChangeObservable(mApp));
    }
}
