package tv.threess.threeready.data.proximus.account.model.server;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.data.proximus.account.model.boot.IDataCollectionServerInfo;

/**
 * Class for proximus data collection related server informations.
 *
 * @author Zsolt Bokor_
 * @since 2020.06.02
 */
public class DataCollectionServerInfoProximus extends ServerInfoProximus implements Serializable, IDataCollectionServerInfo {

    public static final String EVENT_COLLECTOR = "event collector";
    public static final String METRIC_COLLECTOR = "metric collector";

    private static final String USAGE_TYPE = "UsageType";
    private static final String HEADERS = "Headers";
    private static final String MAX_AGE = "MaxAge";
    private static final String MAX_NUM = "MaxNum";
    private static final String RANDOM_PERIOD = "RandomPeriod";
    private static final String TIMEOUT_PERIOD = "TimeoutPeriod";
    private static final String ENCAPSULATION = "Encapsulation";
    private static final String MAX_MESSAGE_LENGTH = "MaxMessageLength";

    public DataCollectionServerInfoProximus(ServerInfoProximus serverInfoProximus) {
        super(serverInfoProximus);
    }

    /**
     * @return Usage type for server. E.g. event/metric.
     */
    public String getUsageType() {
        return getAttributeValueForName(USAGE_TYPE);
    }

    @Override
    public DataCollectionServerType getCollectionType() {
        switch (getName().toLowerCase()) {
            case DataCollectionServerInfoProximus.EVENT_COLLECTOR:
                return DataCollectionServerType.TYPE_EVENT;
            case DataCollectionServerInfoProximus.METRIC_COLLECTOR:
                return DataCollectionServerType.TYPE_METRIC;
        }
        return DataCollectionServerType.TYPE_UNKNOWN;
    }

    @Override
    public String getEndpoint() {
        return getDefaultUrl();
    }

    /**
     * @return Headers which need to be included in the HTTP request.
     */
    public String getHeaders() {
        return getAttributeValueForName(HEADERS);
    }

    @Override
    public long getMaxKeepPeriod() {
        return getMaxAge(TimeUnit.MILLISECONDS);
    }

    @Override
    public int getMaxKeepSize() {
        return getMaxMessages();
    }

    @Override
    public int getMaxBatchSize() {
        return getMaxMessagesPerRequest();
    }

    @Override
    public long getRandomPeriod() {
        return getRandomPeriod(TimeUnit.MILLISECONDS);
    }

    @Override
    public long getTimeoutPeriod() {
        return getTimeOut(TimeUnit.MILLISECONDS);
    }

    /**
     * @return Maximum time in seconds a message remains stored locally before being permanently removed.
     * @param unit Time unit in which the time will be returned.
     */
    public long getMaxAge(TimeUnit unit) {
        return unit.convert(getAttributeValueForName(MAX_AGE, 0), TimeUnit.MINUTES);
    }

    /**
     * @return Maximum number of messages that are stored locally before the oldest is removed permanently.
     */
    public int getMaxMessages() {
       return (int) getAttributeValueForName(MAX_NUM, 0);
    }

    /**
     * @return Random period in seconds which is used to calculate the delay for sending.
     * @param unit Time unit in which the time will be returned.
     */
    public long getRandomPeriod(TimeUnit unit) {
        return unit.convert(getAttributeValueForName(RANDOM_PERIOD, 0), TimeUnit.SECONDS);
    }

    /**
     * @return Timeout for HTTP requests.
     * @param unit Time unit in which the timeout will be returned.
     */
    public long getTimeOut(TimeUnit unit) {
        return unit.convert(getAttributeValueForName(TIMEOUT_PERIOD, 0), TimeUnit.SECONDS);
    }

    /**
     * @return Encapsulation type in which the data needs to be sent. E.g. JSON, XML, etc...
     */
    public String getEncapsulation() {
        return getAttributeValueForName(ENCAPSULATION);
    }

    /**
     * @return Maximum number of events/metrics that can be sent to the server in a
     * single HTTP request.
     */
    public int getMaxMessagesPerRequest() {
        return (int) getAttributeValueForName(MAX_MESSAGE_LENGTH, 0);
    }
}
