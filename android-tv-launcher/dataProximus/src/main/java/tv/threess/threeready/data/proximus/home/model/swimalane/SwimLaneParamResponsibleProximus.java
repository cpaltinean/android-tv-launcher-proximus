package tv.threess.threeready.data.proximus.home.model.swimalane;

/**
 * Enum represents to component which is responsible to provide the value for the swim lane request parameter.
 *
 * @author Barabas Attila
 * @since 2020.04.30
 */
public enum SwimLaneParamResponsibleProximus {
    // Will be proved by the backend.
    BepToken,
    BepAdmin,

    // Needs to be provided by the app
    ClientToken,
    PageVariable,

    // Unknown, can be skipped.
    Undefined
}
