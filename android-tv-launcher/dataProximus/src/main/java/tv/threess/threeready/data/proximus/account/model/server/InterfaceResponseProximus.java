package tv.threess.threeready.data.proximus.account.model.server;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * Backend response which contains the configuration for the requests.
 *
 * @author Barabas Attila
 * @since 3/10/21
 */
public class InterfaceResponseProximus extends BTAResponseProximus {

    @XmlElementWrapper(name = "BTARequestProperties")
    @XmlElement(name = "Request")
    private List<RequestPropertyProximus> mBTARequestProperties;

    /**
     * @return BTA call related configurations.
     */
    public List<RequestPropertyProximus> getBTARequestProperties() {
        return mBTARequestProperties;
    }
}
