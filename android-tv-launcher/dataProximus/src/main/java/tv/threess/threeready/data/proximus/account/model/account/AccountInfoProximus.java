package tv.threess.threeready.data.proximus.account.model.account;


import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.data.proximus.account.model.account.adapter.AccountResourcesMapAdapter;
import tv.threess.threeready.data.proximus.account.model.account.adapter.AccountUserDefinedFieldMapAdapter;
import tv.threess.threeready.data.proximus.account.model.account.adapter.PackageMapAdapterProximus;
import tv.threess.threeready.data.proximus.generic.adapter.PinAdapterProximus;

/**
 * Proximus account information file which holds the account related info, like packages.
 *
 * Created by Noemi Dali on 26.03.2020.
 */
@XmlRootElement(name = "AccountInfo")
public class AccountInfoProximus implements Serializable {

    @XmlJavaTypeAdapter(PackageMapAdapterProximus.class)
    @XmlElementWrapper(name = "ItemPackageTypeList")
    @XmlElement(name = "ItemPackageType")
    private Map<String, PackageInfoProximus> mPackageMap;

    @XmlElement(name = "SubscriberIDHash")
    private String mSubscriberIDHash;

    @XmlElement(name = "Site")
    private String mSite;

    @XmlElement(name = "SubscriberAccountNumber")
    private String mAccountNumber;

    @XmlJavaTypeAdapter(AccountUserDefinedFieldMapAdapter.class)
    @XmlElementWrapper(name = "userdefinedfields")
    @XmlElement(name = "field")
    private Map<AccountUserDefinedFieldProximus.Key, AccountUserDefinedFieldProximus> mUserDefinedFields;

    @XmlJavaTypeAdapter(AccountResourcesMapAdapter.class)
    @XmlElementWrapper(name = "ResourceList")
    @XmlElement(name = "Resource")
    private Map<AccountResourceProximus.Key, AccountResourceProximus> mResourceMap;

    @XmlElement(name = "ApplyParentalControls")
    private boolean mApplyParentalControl;

    @XmlJavaTypeAdapter(PinAdapterProximus.class)
    @XmlElement(name = "AccountPIN")
    private String mAccountPIN;

    @XmlElement(name = "ShowAllTitles")
    private boolean mShowAllTitles;

    @XmlElementWrapper(name = "RatingsList")
    @XmlElement(name = "Rating")
    private List<RatingProximus> mRatingList;

    @XmlJavaTypeAdapter(PinAdapterProximus.class)
    @XmlElement(name = "PurchasePIN")
    private String mPurchasePIN;

    @XmlElement(name = "FreeRentals")
    private int mFreeRentals;

    @XmlElement(name = "ServiceStartDate")
    private long mServiceStart;

    @XmlElement(name = "ServiceEndDate")
    private long mServiceEnd;

    @XmlElement(name = "LocationName")
    private String mLocaltionName;

    @XmlElement(name = "HouseholdID")
    private String mHouseholdId;

    @XmlElement(name = "SubscriberType")
    private String mSubscriberType;

    @XmlElement(name = "TargetAdvEnabled")
    private boolean mTargetAdvEnabled;

    @XmlElement(name = "Bandwidth")
    private int mBandwidth;

    /**
     * @return Unique identifier of the account.
     */
    public String getAccountNumber() {
        return mAccountNumber;
    }

    /**
     * @return Hash of the account identifier.
     */
    public String getSubscriberIDHash() {
        return mSubscriberIDHash;
    }

    /**
     * @return type of subscriber
     */
    public String getSubscriberType(){
        return mSubscriberType;
    }

    /**
     * @return The region where the account is registered. E.g Brussels.
     */
    public String getSite() {
        return mSite;
    }

    /**
     * @return The collection of package available for on the account.
     */
    public Collection<PackageInfoProximus> getPackages() {
        return mPackageMap.values();
    }

    /**
     * @return True if the parental rating is enabled on the account, false otherwise.
     */
    public boolean isParentalControlEnabled() {
        return mApplyParentalControl;
    }

    /**
     * @return The system PIN of the account used for device registration and parental control.
     */
    public String getAccountPIN() {
        return mAccountPIN;
    }

    /**
     * @return The purchase PIN of the account used to confirm renting.
     */
    public String getPurchasePIN() {
        return mPurchasePIN;
    }

    /**
     * @return The current parental ratings set on the account.
     */
    public List<RatingProximus> getRatingList() {
        return mRatingList;
    }

    /**
     * @return The name of the location where the account is registered.
     */
    public String getLocationName() {
        return mLocaltionName;
    }

    /**
     * @return True if the VQE config override is enabled on the STB.
     */
    public boolean isVQEControlEnabled() {
        AccountUserDefinedFieldProximus field = mUserDefinedFields.get(
                AccountUserDefinedFieldProximus.Key.QoeControlEnable);
        return field != null && field.getBooleanValue() != null
                && field.getBooleanValue();
    }

    /**
     * @return True if the VQE selective retransmission error repair is enabled on the STB.
     * Or null of the value doesn't exists.
     */
    @Nullable
    public Boolean isVQERetEnabled() {
        AccountUserDefinedFieldProximus field = mUserDefinedFields.get(
                AccountUserDefinedFieldProximus.Key.RetEnable);
        return field != null ? field.getBooleanValue() : null;
    }

    /**
     * @return True if the VQE rapid channel change is enabled on the STB.
     * Or null if the value doesn't exists.
     */
    @Nullable
    public Boolean isVQERCCEnabled() {
        AccountUserDefinedFieldProximus field = mUserDefinedFields.get(
                AccountUserDefinedFieldProximus.Key.RccEnable);
        return field != null ? field.getBooleanValue() : null;
    }

    /**
     * @return The package with the given name or null if it's not available.
     */
    @Nullable
    public PackageInfoProximus getPackage(String packageName) {
        return mPackageMap.get(packageName);
    }

    /**
     * @return The buffer size in milliseconds until the server holds the data for error repair.
     */
    @Nullable
    public Integer getNetworkBufferSize() {
        AccountUserDefinedFieldProximus field = mUserDefinedFields.get(
                AccountUserDefinedFieldProximus.Key.NetworkBufferSize);
        return field != null ? field.getIntValue() : null;
    }

    /**
     * @return The amount of uptime the device must be running before a reboot can be performed.
     * a value less or equal to 0 indicates that no reboot is required for the user.
     */
    public long getQuiescentRebootUptime(TimeUnit unit) {
        AccountUserDefinedFieldProximus field = mUserDefinedFields.get(
                AccountUserDefinedFieldProximus.Key.QREnabled);
        if (field == null) {
            return 0;
        }
        Integer hours = field.getIntValue();
        if (hours == null) {
             return 0;
        }
        return unit.convert(hours, TimeUnit.HOURS);
    }

    /**
     * @return Available bandwidth on the account for error repair and rapid channel change.
     */
    public int getAvailableOverheadBandwidth() {
        AccountResourceProximus resource = mResourceMap.get(
                AccountResourceProximus.Key.AvailableOverheadBandwidth);
        return resource != null ? resource.getAmount() : 0;
    }

    /**
     * @return The number of FCC credit available on the account.
     */
    public int getFreeRentals() {
        return mFreeRentals;
    }

    public long getServiceStart() {
        return mServiceStart;
    }

    public long getServiceEnd() {
        return mServiceEnd;
    }

    /**
     * @return The identifier of the house hold to which the account belongs.
     */
    public String getHouseHoldId() {
        return mHouseholdId;
    }

    public boolean isTargetAdvEnabled() {
        return mTargetAdvEnabled;
    }

    public int getBandwidth() {
        return mBandwidth;
    }
}
