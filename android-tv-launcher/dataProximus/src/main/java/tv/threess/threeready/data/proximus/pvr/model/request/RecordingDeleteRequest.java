package tv.threess.threeready.data.proximus.pvr.model.request;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Retrofit request class for deleting one or more recording item.
 *
 * Created by Noemi Dali on 21.04.2020.
 */
@XmlRootElement(name = "BTA")
public class RecordingDeleteRequest implements Serializable {

    @XmlElementWrapper(name = "DeleteRecordingsDoc")
    @XmlElement(name = "Recording")
    private final List<Recording> mDeleteRecordings;

    public RecordingDeleteRequest(String... recordingId) {
        mDeleteRecordings = Stream.of(recordingId)
                .map(Recording::new)
                .collect(Collectors.toList());
    }

    @XmlRootElement(name = "Recording")
    private static class Recording implements Serializable {

        @XmlElement(name = "UserRecordID")
        private final String mRecordingId;

        Recording(String recordingId) {
            mRecordingId = recordingId;
        }
    }
}
