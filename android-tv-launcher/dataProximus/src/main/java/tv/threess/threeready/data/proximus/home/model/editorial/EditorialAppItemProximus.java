package tv.threess.threeready.data.proximus.home.model.editorial;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemStyle;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Editorial tile for editorial apps.
 *
 * @author Daniela Toma
 * @since 2022.02.17
 */
public class EditorialAppItemProximus implements EditorialItem {
    private final SwimLaneProximus.StaticTile mStaticTile;

    public EditorialAppItemProximus(SwimLaneProximus.StaticTile staticTile) {
        mStaticTile = staticTile;
    }

    @Nullable
    @Override
    public String getFallbackTitle() {
        return mStaticTile.getDisplayNames().getLocalizedNameOrFirst();
    }

    public Integer getSortOrder() {
        return mStaticTile.getSortOrder();
    }

    @Override
    public EditorialItemAction getAction() {
        return mItemAction;
    }

    @Override
    public String getId() {
        return mStaticTile.getId();
    }

    @Override
    public String getContentDescription() {
        return mStaticTile.getDisplayNames().getLocalizedNameOrFirst();
    }

    @Nullable
    @Override
    public EditorialItemStyle getStyle() {
        return mItemStyle;
    }

    protected final EditorialItemStyle mItemStyle = new EditorialItemStyle() {
        private final Size mSize = new VariantSize(VariantSize.Variant.App);

        @Nullable
        @Override
        public IImageSource getBackgroundImage() {
            return mImageSource;
        }

        @Nullable
        @Override
        public Size getHeight() {
            return mSize;
        }

        @Nullable
        @Override
        public Size getWidth() {
            return mSize;
        }
    };

    private final IImageSource mImageSource = new IImageSource() {
        @Override
        public String getUrl() {
            if(mStaticTile.getBackgroundImage() == null) {
               return "";
            }
            return mStaticTile.getBackgroundImage().getUrl();
        }

        @Override
        public Type getType() {
            return Type.APP;
        }
    };

    private final EditorialItemAction mItemAction = new EditorialItemAction.AppEditorialItemAction() {

        @Override
        public String getPackageName() {
            return mStaticTile.getId();
        }
    };
}
