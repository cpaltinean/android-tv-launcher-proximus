package tv.threess.threeready.data.proximus.generic;

import android.app.Application;
import android.net.Uri;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;

import java.io.File;
import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.Iterator;

import okhttp3.Cache;
import okhttp3.ConnectionPool;
import okhttp3.CookieJar;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.generic.model.BaseResponse;
import tv.threess.threeready.data.proximus.BuildConfig;
import tv.threess.threeready.data.proximus.account.retrofit.BTAAccountRetrofitProximus;
import tv.threess.threeready.data.proximus.account.retrofit.BootConfigRetrofitProximus;
import tv.threess.threeready.data.proximus.account.retrofit.ConfigJsonRetrofitProximus;
import tv.threess.threeready.data.proximus.account.retrofit.ConfigXmlRetrofitProximus;
import tv.threess.threeready.data.proximus.account.retrofit.ConsentRetrofitProximus;
import tv.threess.threeready.data.proximus.generic.interceptor.BTAOkHttpInterceptor;
import tv.threess.threeready.data.proximus.generic.interceptor.BackendIsolationInterceptor;
import tv.threess.threeready.data.proximus.generic.interceptor.CacheControlOverrideOkHttpInterceptor;
import tv.threess.threeready.data.proximus.generic.interceptor.DataCollectionOkHttpInterceptor;
import tv.threess.threeready.data.proximus.generic.interceptor.EPGOkHttpInterceptor;
import tv.threess.threeready.data.proximus.generic.interceptor.MSOkHttpInterceptor;
import tv.threess.threeready.data.proximus.generic.interceptor.PxtvHeaderOverrideInterceptor;
import tv.threess.threeready.data.proximus.generic.interceptor.RetryOnErrorOkHttpInterceptor;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;
import tv.threess.threeready.data.proximus.home.retrofit.BTASwimLaneRetrofitProximus;
import tv.threess.threeready.data.proximus.iam.IAMRetrofitProximus;
import tv.threess.threeready.data.proximus.log.retrofit.DataCollectionRetrofitProximus;
import tv.threess.threeready.data.proximus.playback.retrofit.BTAStreamingRetrofitProximus;
import tv.threess.threeready.data.proximus.playback.retrofit.MSStreamingRetrofitProximus;
import tv.threess.threeready.data.proximus.pvr.retrofit.BTARecordingRetrofitProximus;
import tv.threess.threeready.data.proximus.search.retrofit.BTASearchRetrofitProximus;
import tv.threess.threeready.data.proximus.tv.retrofit.BTATvRetrofitProximus;
import tv.threess.threeready.data.proximus.tv.retrofit.EPGRetrofitProximus;
import tv.threess.threeready.data.proximus.tv.retrofit.converter.EPGBlockRetrofitConverterProximus;
import tv.threess.threeready.data.proximus.tv.retrofit.converter.EPGDateRetrofitConverterProximus;
import tv.threess.threeready.data.proximus.vod.retrofit.BTAVodRetrofitProximus;
import tv.threess.threeready.data.proximus.watchlist.retrofit.WatchlistRetrofitProximus;
import tv.threess.threeready.data.utils.GsonUtils;
import tv.threess.threeready.data.utils.OkHttpBuilder.OkHttpBuilder;

/**
 * TODO: Class description
 *
 * @author Barabas Attila
 * @since 2020.09.09
 */
public class RetrofitServicesProximus implements Component {
    private static final String TAG = Log.tag(RetrofitServicesProximus.class);

    private static final String HTTP_CACHE_FOlDER = "HTTP_CACHE";
    private static final String HTTP_EPG_CACHE_FOlDER = "EPG_CACHE";

    private final OkHttpClient mDefaultOkHttpClient;
    private final OkHttpClient mGlideOkHttpClient;
    private final OkHttpClient mCacheLessOkHttpClient;
    private final OkHttpClient mBootConfigOkHttpClient;
    private final OkHttpClient mBTAOkHttpClient;
    private final OkHttpClient mEPGOkHttpClient;
    private final OkHttpClient mDataCollectionOkHttpClient;
    private final OkHttpClient mMSOkHttpClient;

    private final BootConfigRetrofitProximus mBootConfigRetrofit;
    private final ConfigJsonRetrofitProximus mJsonConfigRetrofit;
    private final ConfigXmlRetrofitProximus mXmlConfigRetrofit;
    private final IAMRetrofitProximus mIAMRetrofit;
    private final BTAAccountRetrofitProximus mBTAAccountRetrofit;
    private final BTATvRetrofitProximus mBTATvRetrofit;
    private final EPGRetrofitProximus mEPGRetrofit;
    private final BTAStreamingRetrofitProximus mBTAStreamingRetrofit;
    private final BTARecordingRetrofitProximus mBTARecordingRetrofit;
    private final DataCollectionRetrofitProximus mDataCollectionRetrofit;
    private final BTAVodRetrofitProximus mBTAVodRetrofit;
    private final BTASearchRetrofitProximus mBTASearchRetrofit;
    private final BTASwimLaneRetrofitProximus mBTASwimLaneRetrofit;
    private final MSStreamingRetrofitProximus mMSStreamingRetrofit;
    private final WatchlistRetrofitProximus mWatchlistRetrofitProximus;
    private final ConsentRetrofitProximus mConsentRetrofitProximus;

    private final Cache mFileCache;
    private final Cache mEPGFileCache;

    private final XmlMapper mXmlMapper;

    public RetrofitServicesProximus(Application application) {
        long cacheSize = 500 * 1024 * 1024; // Maximum 500 MB.

        File cacheDir = new File(application.getCacheDir(), HTTP_CACHE_FOlDER);
        mFileCache = new Cache(cacheDir, cacheSize);

        File epgCacheDir = new File(mFileCache.directory(), HTTP_EPG_CACHE_FOlDER);
        mEPGFileCache = new Cache(epgCacheDir, cacheSize);

        ConnectionPool connectionPool = new ConnectionPool();

        mXmlMapper = new XmlMapper();
        mXmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
        mXmlMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        mXmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mXmlMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        mXmlMapper.configure(MapperFeature.AUTO_DETECT_GETTERS, false);
        mXmlMapper.configure(MapperFeature.AUTO_DETECT_SETTERS, false);
        mXmlMapper.setDefaultUseWrapper(false);

        // Configure JAXB annotations.
        JaxbAnnotationModule module = new JaxbAnnotationModule();
        mXmlMapper.registerModule(module);

        // Logging interceptor
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(s -> Log.d(TAG, s));
        loggingInterceptor.setLevel(BuildConfig.HTTP_LOG_LEVEL);

        // No backend interceptor
        BackendIsolationInterceptor isolationInterceptor = new BackendIsolationInterceptor();

        // Cookie handler.
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieJar cookieJar = new JavaNetCookieJar(cookieManager);

        // OkHttp client for http communication where no authorization needed.
        OkHttpClient.Builder defaultOkHttpBuilder = OkHttpBuilder.createClient(OkHttpBuilder.Dscp.CS1)
                .cache(mFileCache)
                .cookieJar(cookieJar)
                .followRedirects(true)
                .followSslRedirects(true)
                .connectionPool(connectionPool)
                .addInterceptor(isolationInterceptor)
                .addNetworkInterceptor(loggingInterceptor);

        mDefaultOkHttpClient = defaultOkHttpBuilder
                .addNetworkInterceptor(new CacheControlOverrideOkHttpInterceptor())
                .build();

        mGlideOkHttpClient = OkHttpBuilder.createClient(OkHttpBuilder.Dscp.AF22).build();

        mCacheLessOkHttpClient = defaultOkHttpBuilder
                .cache(null)
                .build();

        mBootConfigOkHttpClient = defaultOkHttpBuilder
                .addInterceptor(new PxtvHeaderOverrideInterceptor())
                .build();

        // Boot config retrofit service.
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(GsonUtils.getGson()))
                .baseUrl(BootConfigRetrofitProximus.BASE_URI)
                .client(mBootConfigOkHttpClient)
                .build();
        mBootConfigRetrofit = retrofit.create(BootConfigRetrofitProximus.class);

        // 3Ready config retrofit service.
        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(GsonUtils.getGson()))
                .baseUrl(ConfigJsonRetrofitProximus.BASE_URI)
                .client(mDefaultOkHttpClient)
                .build();
        mJsonConfigRetrofit = retrofit.create(ConfigJsonRetrofitProximus.class);

        // Xml config parser retrofit service.
        retrofit = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create(mXmlMapper))
                .baseUrl(ConfigXmlRetrofitProximus.BASE_URI)
                .client(mDefaultOkHttpClient)
                .build();
        mXmlConfigRetrofit = retrofit.create(ConfigXmlRetrofitProximus.class);

        // IAM retrofit service.
        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(GsonUtils.getGson()))
                .baseUrl(ConfigXmlRetrofitProximus.BASE_URI)
                .client(mBootConfigOkHttpClient)
                .build();
        mIAMRetrofit = retrofit.create(IAMRetrofitProximus.class);

        // OkHttp for BTA communication. Handles IAM authentication.
        OkHttpClient.Builder btaOkHttpBuilder = OkHttpBuilder.createClient(OkHttpBuilder.Dscp.AF22)
                .cache(mFileCache)
                .cookieJar(cookieJar)
                .connectionPool(connectionPool)
                .addInterceptor(isolationInterceptor)
                .addInterceptor(new BTAOkHttpInterceptor(application, mIAMRetrofit))
                .addInterceptor(new PxtvHeaderOverrideInterceptor())
                .addInterceptor(new RetryOnErrorOkHttpInterceptor())
                .addNetworkInterceptor(new CacheControlOverrideOkHttpInterceptor())
                .addNetworkInterceptor(loggingInterceptor);

        mBTAOkHttpClient = btaOkHttpBuilder.build();

        // Account info related retrofit service.
        retrofit = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create(mXmlMapper))
                .baseUrl(BootConfigRetrofitProximus.BASE_URI)
                .client(mBTAOkHttpClient)
                .build();
        mBTAAccountRetrofit = retrofit.create(BTAAccountRetrofitProximus.class);


        // TV related retrofit service.
        retrofit = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create(mXmlMapper))
                .baseUrl(BootConfigRetrofitProximus.BASE_URI)
                .client(mBTAOkHttpClient)
                .build();
        mBTATvRetrofit = retrofit.create(BTATvRetrofitProximus.class);

        // Streaming related retrofit service.
        retrofit = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create(mXmlMapper))
                .baseUrl(BootConfigRetrofitProximus.BASE_URI)
                .client(mBTAOkHttpClient)
                .build();
        mBTAStreamingRetrofit = retrofit.create(BTAStreamingRetrofitProximus.class);

        // Ok http client for EPG requests.
        OkHttpClient.Builder epgOkHttpBuilder = OkHttpBuilder.createClient(OkHttpBuilder.Dscp.CS1)
                .cache(mEPGFileCache)
                .cookieJar(cookieJar)
                .connectionPool(connectionPool)
                .addInterceptor(isolationInterceptor)
                .addInterceptor(new EPGOkHttpInterceptor(application))
                .addNetworkInterceptor(loggingInterceptor);

        mEPGOkHttpClient = epgOkHttpBuilder.build();

        // EPG related retrofit service.
        retrofit = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create(mXmlMapper))
                .addConverterFactory(new EPGDateRetrofitConverterProximus())
                .addConverterFactory(new EPGBlockRetrofitConverterProximus())
                .baseUrl(BootConfigRetrofitProximus.BASE_URI)
                .client(mEPGOkHttpClient)
                .build();
        mEPGRetrofit = retrofit.create(EPGRetrofitProximus.class);

        //Recording related retrofit service.
        retrofit = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create(mXmlMapper))
                .baseUrl(BootConfigRetrofitProximus.BASE_URI)
                .client(mBTAOkHttpClient)
                .build();
        mBTARecordingRetrofit = retrofit.create(BTARecordingRetrofitProximus.class);

        //Vod related retrofit service.
        retrofit = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create(mXmlMapper))
                .baseUrl(BootConfigRetrofitProximus.BASE_URI)
                .client(mBTAOkHttpClient)
                .build();
        mBTAVodRetrofit = retrofit.create(BTAVodRetrofitProximus.class);

        //Swim lane related retrofit service.
        retrofit = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create(mXmlMapper))
                .baseUrl(BootConfigRetrofitProximus.BASE_URI)
                .client(mBTAOkHttpClient)
                .build();
        mBTASwimLaneRetrofit = retrofit.create(BTASwimLaneRetrofitProximus.class);

        //Search related retrofit service.
        retrofit = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create(mXmlMapper))
                .baseUrl(BootConfigRetrofitProximus.BASE_URI)
                .client(mBTAOkHttpClient)
                .build();
        mBTASearchRetrofit = retrofit.create(BTASearchRetrofitProximus.class);

        // OkHttp client for data collection API
        OkHttpClient.Builder dataCollectionOkHttpClient = OkHttpBuilder.createClient(OkHttpBuilder.Dscp.CS0)
                .cache(mFileCache)
                .cookieJar(cookieJar)
                .connectionPool(connectionPool)
                .addInterceptor(new DataCollectionOkHttpInterceptor())
                .addNetworkInterceptor(loggingInterceptor);

        mDataCollectionOkHttpClient = dataCollectionOkHttpClient.build();

        // Data collection related retrofit service.
        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(GsonUtils.getGson()))
                .baseUrl(DataCollectionRetrofitProximus.BASE_URL)
                .client(mDataCollectionOkHttpClient)
                .build();
        mDataCollectionRetrofit = retrofit.create(DataCollectionRetrofitProximus.class);

        // Micro service ok http client.
        OkHttpClient.Builder msOkHttpClient = OkHttpBuilder.createClient(OkHttpBuilder.Dscp.AF22)
                .cache(mFileCache)
                .cookieJar(cookieJar)
                .connectionPool(connectionPool)
                .addInterceptor(isolationInterceptor)
                .addInterceptor(new MSOkHttpInterceptor(application, mIAMRetrofit))
                .addInterceptor(new PxtvHeaderOverrideInterceptor())
                .addInterceptor(new RetryOnErrorOkHttpInterceptor())
                .addNetworkInterceptor(loggingInterceptor);

        mMSOkHttpClient = msOkHttpClient.build();

        // Streaming micro service retrofit.
        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(GsonUtils.getGson()))
                .baseUrl(BootConfigRetrofitProximus.BASE_URI)
                .client(mMSOkHttpClient)
                .build();
        mMSStreamingRetrofit = retrofit.create(MSStreamingRetrofitProximus.class);

        // Watchlist micro service retrofit.
        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(GsonUtils.getGson()))
                .baseUrl(BootConfigRetrofitProximus.BASE_URI)
                .client(mMSOkHttpClient)
                .build();

        mWatchlistRetrofitProximus = retrofit.create(WatchlistRetrofitProximus.class);

        // Consent micro service retrofit
        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(GsonUtils.getGson()))
                .baseUrl(BootConfigRetrofitProximus.BASE_URI)
                .client(mMSOkHttpClient)
                .build();
        mConsentRetrofitProximus = retrofit.create(ConsentRetrofitProximus.class);
    }

    public BootConfigRetrofitProximus getBootConfigRetrofit() {
        return mBootConfigRetrofit;
    }

    public ConfigJsonRetrofitProximus getJsonConfigRetrofit() {
        return mJsonConfigRetrofit;
    }

    public ConfigXmlRetrofitProximus getXmlConfigRetrofit() {
        return mXmlConfigRetrofit;
    }

    public BTAAccountRetrofitProximus getAccountRetrofit() {
        return mBTAAccountRetrofit;
    }

    public WatchlistRetrofitProximus getWatchlistRetrofit() {
        return mWatchlistRetrofitProximus;
    }

    public BTATvRetrofitProximus getTvRetrofit() {
        return mBTATvRetrofit;
    }

    public EPGRetrofitProximus getEPGRetrofit() {
        return mEPGRetrofit;
    }

    public BTAStreamingRetrofitProximus getBTAStreamingRetrofit() {
        return mBTAStreamingRetrofit;
    }

    public BTARecordingRetrofitProximus getRecordingRetrofit() {
        return mBTARecordingRetrofit;
    }

    public BTAVodRetrofitProximus getVodRetrofit() {
        return mBTAVodRetrofit;
    }

    public BTASwimLaneRetrofitProximus getSwimLaneRetrofit() {
        return mBTASwimLaneRetrofit;
    }

    public OkHttpClient getDefaultOkHttpClient() {
        return mDefaultOkHttpClient;
    }

    public OkHttpClient getGlideOkHttpClient() {
        return mGlideOkHttpClient;
    }

    public OkHttpClient getCacheLessOkHttpClient() {
        return mCacheLessOkHttpClient;
    }

    public BTASearchRetrofitProximus getSearchRetrofit() {
        return mBTASearchRetrofit;
    }

    public DataCollectionRetrofitProximus getDataCollectionRetrofit() {
        return mDataCollectionRetrofit;
    }

    public ConsentRetrofitProximus getConsentRetrofit() {
        return mConsentRetrofitProximus;
    }

    public XmlMapper getXmlMapper() {
        return mXmlMapper;
    }

    public MSStreamingRetrofitProximus getMSStreamingRetrofit() {
        return mMSStreamingRetrofit;
    }

    /**
     * Method used for clear the Http cache for the given swimlane.
     *
     * @param swimLaneId for detecting the correct url what we want to remove from the cache.
     */
    public void clearCacheByModuleConfigId(String swimLaneId) throws IOException {
        Iterator<String> urls = mFileCache.urls();
        while (urls.hasNext()) {
            if (swimLaneId.equals(Uri.parse(urls.next()).getQueryParameter(SwimLaneProximus.KEY_SWIMLANE_ID))) {
                urls.remove();
            }
        }
    }

    public static <T extends BaseResponse> T execute(Call<T> call) throws IOException {
        try {
            Response<T> response = call.execute();
            if (!response.isSuccessful()) {
                // HTTP error.
                throw new BackendException(response.code(), response.message(), call.request().url().toString());
            }

            T result = response.body();
            if (result == null) {
                // Parse error.
                throw new BackendException(response.code(), response.message(), call.request().url().toString());
            }

            BaseResponse.Error error = result.getError();
            if (error != null) {
                // Bta error.
                throw new BackendException(error.getErrorCode(),
                        error.getErrorDescription(), call.request().url().toString());
            }

            Components.get(InternetChecker.class).onRequestSuccessful();
            return result;
        } catch (BackendException e) {
            Components.get(InternetChecker.class).onRequestFailed(e);
            throw e;
        }
    }

    public static <T> T executeGen(Call<T> call) throws IOException {
        try {
            Response<T> response = call.execute();
            if (!response.isSuccessful()) {
                // HTTP error.
                throw new BackendException(response.code(),
                        response.message(), call.request().url().toString());
            }

            Components.get(InternetChecker.class).onRequestSuccessful();
            return response.body();
        } catch (BackendException e) {
            Components.get(InternetChecker.class).onRequestFailed(e);
            throw e;
        }
    }
}
