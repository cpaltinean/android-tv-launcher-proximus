/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.tv.model.channel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Collection;
import java.util.TreeSet;
import java.util.zip.CRC32;

import tv.threess.threeready.api.tv.model.TifScanParameters;

/**
 * Scan parameters for IPTV scan.
 *
 * @author Lukacs Andor
 * @since 2017.06.06
 */
public class IpTvParametersTch implements TifScanParameters {

    public static final String COUNTRY = "be";
    public static final String TYPE_IP_VQE = "json";

    @Expose(serialize = false, deserialize = false)
    private final transient VQEInfoTCH mVQEInfo;

    @Expose(serialize = false, deserialize = false)
    private final transient VCASInfoTCH mVCASInfo;

    @Expose(serialize = false, deserialize = false)
    private final transient TreeSet<String> mUniqueAddresses;

    @SerializedName("type")
    private final String mType;

    @SerializedName("country")
    private final String mCountry;

    @SerializedName("services")
    private final IpTvService[] mServices;

    @Override
    public String getType() {
        return mType;
    }

    @Override
    public String getCountry() {
        return mCountry;
    }

    @Override
    public Object getParams() {
        return this;
    }

    @Override
    public long getParamsHash() {
        CRC32 result = new CRC32();
        for (String address: mUniqueAddresses) {
            result.update(address.getBytes());
        }
        String channelConfig = mVQEInfo.getChannelConfig();
        if (channelConfig != null) {
            result.update(channelConfig.getBytes());
        }
        return result.getValue();
    }

    public VQEInfoTCH getVQEInfo() {
        return mVQEInfo;
    }

    public VCASInfoTCH getVCASInfo() {
        return mVCASInfo;
    }

    public IpTvService[] getServices() {
        return mServices;
    }

    private IpTvParametersTch(String type, String country, VQEInfoTCH vqeInfo,
                             VCASInfoTCH vcasInfo, TreeSet<String> uniqueAddresses, IpTvService... services) {
        mType = type;
        mCountry = country;
        mVQEInfo = vqeInfo;
        mVCASInfo = vcasInfo;
        mServices = services;
        mUniqueAddresses = uniqueAddresses;
    }

    public IpTvParametersTch(String type, String country, VQEInfoTCH vqeInfo,
                             VCASInfoTCH vcasInfo, TreeSet<String> uniqueAddresses, Collection<IpTvService> services) {
        this(type, country, vqeInfo, vcasInfo, uniqueAddresses, services.toArray(new IpTvService[0]));
    }

    @Override
    public String toString() {
        String services = mServices == null ? "null" : String.valueOf(mServices.length);
        return "IpTvParameters(type: " + mType + ", country: " + mCountry + ", services: " + services + ")";
    }

    public static class IpTvService {
        /* mandatory : uri is necessary to connect with the broadcasted stream */
        @SerializedName("uri")
        private final String uri;

        @SerializedName("sdp")
        private final String sdp;

        /* mandatory : name of the service */
        @SerializedName("name")
        private final String name;

        /* undocumented : channel number */
        @SerializedName("number")
        private final int number;

        public IpTvService(String uri, String sdp, String name, int number) {
            this.uri = uri;
            this.sdp = sdp;
            this.name = name;
            this.number = number;
        }

        public IpTvService(String uri, String name, int number) {
            // HACK: spd must be not null, using an empty string
            this(uri, "", name, number);
        }
    }
}
