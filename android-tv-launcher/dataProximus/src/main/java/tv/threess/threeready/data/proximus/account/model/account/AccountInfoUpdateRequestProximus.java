package tv.threess.threeready.data.proximus.account.model.account;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.data.proximus.generic.adapter.PinAdapterProximus;

/**
 * Proximus Account Information used as body for Update calls.
 *
 * Created by Daniela Toma on 7.04.2020.
 */
@XmlRootElement(name = "UpdateAccountInfoDoc")
public class AccountInfoUpdateRequestProximus implements Serializable {

    @XmlJavaTypeAdapter(PinAdapterProximus.class)
    @XmlElement(name = "AccountPIN")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String mAccountPIN;

    @XmlJavaTypeAdapter(PinAdapterProximus.class)
    @XmlElement(name = "PurchasePIN")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String mPurchasePIN;

    @XmlElement(name = "ApplyParentalControls")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean mApplyParentalControls;

    @XmlElement(name = "OptIn")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String mOptIn;

    @XmlElement(name = "ShowAllTitles")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String mShowAllTitles;

    @XmlElementWrapper(name = "RatingsList")
    @XmlElement(name = "Rating")
    private List<RatingProximus> mRatingList;

    @XmlElement(name = "CatalogLanguageType")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String mCatalogLanguageType;

    public AccountInfoUpdateRequestProximus(boolean applyParentalControls) {
        mApplyParentalControls = applyParentalControls;
    }

    public AccountInfoUpdateRequestProximus(ParentalRating age) {
        mRatingList = getRatingList(age);
    }

    public AccountInfoUpdateRequestProximus(String accountPIN, String purchasePIN) {
        mAccountPIN = accountPIN;
        mPurchasePIN = purchasePIN;
    }

    /**
     * Creates a Rating List using the specified age.
     */
    private List<RatingProximus> getRatingList(ParentalRating age) {
        List<RatingProximus> ratingProximusList = new ArrayList<>();

        ratingProximusList.add(new RatingProximus(age.getMinimumAge(), RatingTypesProximus.MPAA));
        ratingProximusList.add(new RatingProximus(age.getMinimumAge(), RatingTypesProximus.VCHIP));

        return ratingProximusList;
    }
}
