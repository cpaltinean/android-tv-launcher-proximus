package tv.threess.threeready.data.proximus.account.model.account.consent;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import tv.threess.threeready.api.account.model.account.IConsent;

/**
 * POJO class which holds all the information about a Proximus specific consent.
 *
 * @author Zsolt Bokor_
 * @since 2020.10.22
 */
public class ConsentProximus implements IConsent {

    public static final String CONSENT_TARGETED_ADVERTISING_IDENTIFIER = "PPEPP0008";
    public static final String CONSENT_RECOMMENDATION_IDENTIFIER = "PPEPP0011";

    @SerializedName("identifier")
    private String mIdentifier;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("tooltip")
    private String mTooltip;

    @SerializedName("introduction")
    private String mIntroduction;

    @SerializedName("value")
    @JsonAdapter(ConsentValueAdapter.class)
    private boolean mIsAccepted;

    @SerializedName("feedback")
    private boolean mHasFeedback;

    @SerializedName("help_video_content_id")
    private String mVideoUrl;

    @Override
    public String getIdentifier() {
        return mIdentifier;
    }

    @Override
    public String getTitle() {
        return mDescription;
    }

    @Override
    public String getDescription() {
        return mIntroduction;
    }

    @Override
    public String getToolTip() {
        return mTooltip;
    }

    @Override
    public boolean isAccepted() {
        return mIsAccepted;
    }

    @Override
    public boolean hasFeedback() {
        return mHasFeedback;
    }

    @Override
    public String getTutorialVideoUrl() {
        return mVideoUrl;
    }

    @Override
    public String toString() {
        return "ConsentProximus{" +
                "mIdentifier='" + mIdentifier + '\'' +
                ", mDescription='" + mIntroduction + '\'' +
                ", mTooltip='" + mTooltip + '\'' +
                ", mIsAccepted=" + mIsAccepted +
                ", mHasFeedback=" + mHasFeedback +
                ", mVideoUrl='" + mVideoUrl + '\'' +
                '}';
    }
}
