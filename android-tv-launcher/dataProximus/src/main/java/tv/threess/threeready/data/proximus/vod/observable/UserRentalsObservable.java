package tv.threess.threeready.data.proximus.vod.observable;

import android.content.Context;
import android.net.Uri;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.ContinueWatchingConfig;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IRentalExpiration;
import tv.threess.threeready.data.account.AccountLocalCache;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.home.observable.BaseCategoryContentObservable;
import tv.threess.threeready.data.proximus.vod.VodProxyProximus;
import tv.threess.threeready.data.proximus.vod.model.response.BaseVodItemProximus;
import tv.threess.threeready.data.proximus.vod.model.response.RentalExpirationResponseProximus;
import tv.threess.threeready.data.vod.VodCache;
import tv.threess.threeready.data.vod.VodContract;

/**
 * Content observable for My rentals swimlane.
 *
 * @author Daniela Toma
 * @since 06/01/2021
 */
public class UserRentalsObservable extends BaseCategoryContentObservable<IBaseContentItem> {
    public static final String TAG = Log.tag(UserRentalsObservable.class);

    private final VodProxyProximus mVodProxy = Components.get(VodProxyProximus.class);
    private final VodCache mVodCache = Components.get(VodCache.class);
    private final AccountLocalCache mAccountCache = Components.get(AccountLocalCache.class);

    private final ContinueWatchingConfig mContinueWatchingConfig = Components.get(ContinueWatchingConfig.class);

    CopyOnWriteArrayList<IBaseVodItem> vodItemList = new CopyOnWriteArrayList<>();

    public UserRentalsObservable(Context context, ModuleConfig moduleConfig, int start, int count) {
        super(context, moduleConfig, start, count);
    }

    @Override
    public void subscribe(ObservableEmitter<ModuleData<IBaseContentItem>> emitter) throws Exception {
        super.subscribe(emitter);
        registerObserver(ConfigContract.Bookmarks.CONTENT_URI);
        registerObserver(VodContract.RentalExpiration.CONTENT_URI);
    }

    @Override
    protected void loadItemList(ObservableEmitter<ModuleData<IBaseContentItem>> emitter) throws Exception {
        //Get the list of Vods rented by the user.
        vodItemList = new CopyOnWriteArrayList<>(getRentedVods());
        List<IBaseContentItem> userRentalsList = getUserRentalsList();

        ModuleData<IBaseContentItem> contentItemDataSource = new ModuleData<>(mModuleConfig, userRentalsList, hasMoreItems(userRentalsList));
        emitter.onNext(contentItemDataSource);

        mCategoryData = contentItemDataSource;

        onItemListLoaded(emitter);
    }

    private List<BaseVodItemProximus> getRentedVods() throws IOException {
        RentalExpirationResponseProximus rentalExpirationResponseProximus = mVodProxy.getRentedVodList();
        List<BaseVodItemProximus> vodList = rentalExpirationResponseProximus.getVods();
        Map<String, IRentalExpiration> rentalExpirationMap = mVodCache.getRentalExpiration();

        for (BaseVodItemProximus vodItem : vodList) {
            vodItem.setRentalExpirationInfo(rentalExpirationMap);
        }
        return vodList;
    }

    /**
     * Gets the items for My rentals with vods that are not in Continue watching and with rating age different from ParentalRating.Rated18.
     */
    private List<IBaseContentItem> getUserRentalsList() {
        List<IBaseContentItem> userRentalList = new ArrayList<>();
        if (vodItemList != null && !vodItemList.isEmpty()) {
            double maxWatchPercent = mContinueWatchingConfig.getVodWatchedMaxPercent();
            for (IBaseVodItem vodItem : vodItemList) {
                if (vodItem.getParentalRating() == ParentalRating.Rated18) {
                    continue;
                }
                IBookmark bookmark = mAccountCache.getVodBookmark(vodItem);
                if (bookmark != null && bookmark.canContinueWatching(maxWatchPercent)) {
                    Log.d(TAG, "item is in continue watching list: " + vodItem);
                    continue;
                }
                userRentalList.add(vodItem);
            }
            //Sort the descendant after the end rental period.
            Collections.sort(userRentalList, (o1, o2) -> {
                long firstRentalPeriodStart = ((IBaseVodItem) o1).getMainVariant().getRentalPeriodStart();
                long secondRentalPeriodStart = ((IBaseVodItem) o2).getMainVariant().getRentalPeriodStart();
                return Long.compare(secondRentalPeriodStart, firstRentalPeriodStart);
            });
        }

        return userRentalList;
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<ModuleData<IBaseContentItem>> observableEmitter) throws Exception {
        if (uri.getPath().contains(ConfigContract.Bookmarks.CONTENT_URI.getPath())) {
            updateItemList(uri, observableEmitter);
        } else {
            loadItemList(observableEmitter);
        }
    }

    /**
     * Updates the list of vod when a bookmark has changed.
     */
    private void updateItemList(Uri uri, ObservableEmitter<ModuleData<IBaseContentItem>> emitter) {
        List<IBaseContentItem> userRentalList = getUserRentalsList();
        mCategoryData = new ModuleData<>(mModuleConfig, userRentalList, hasMoreItems(userRentalList));
        onItemListLoaded(emitter);
    }

    /**
     * Flag used to know if View All item on My Rentals stripe should be shown.
     */
    private boolean hasMoreItems(List<IBaseContentItem> userRentalsList) {
        return !userRentalsList.isEmpty() && userRentalsList.size() > mCount;
    }
}
