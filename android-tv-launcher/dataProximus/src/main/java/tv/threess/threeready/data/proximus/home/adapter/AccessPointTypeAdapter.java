package tv.threess.threeready.data.proximus.home.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.data.proximus.home.model.AccessPointProximus;

/**
 * Type adapter to read the XML value as access point type enum.
 *
 * @author Barabas Attila
 * @since 5/26/21
 */
public class AccessPointTypeAdapter extends XmlAdapter<String, AccessPointProximus.Type> {

    @Override
    public AccessPointProximus.Type unmarshal(String value) {
        switch (value) {
            default:
                return AccessPointProximus.Type.Page;
            case "SubPage" :
                return AccessPointProximus.Type.SubPage;
            case "PageGroup" :
                return AccessPointProximus.Type.PageGroup;
        }
    }

    @Override
    public String marshal(AccessPointProximus.Type v) {
        throw new IllegalStateException("Not implemented.");
    }
}
