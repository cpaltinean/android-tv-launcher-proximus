package tv.threess.threeready.data.proximus.pvr.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * Retrofit response class to delete one or more recording item.
 *
 * Created by Noemi Dali on 21.04.2020.
 */
@XmlRootElement(name = "BTAResponse")
@JsonIgnoreProperties(ignoreUnknown = true)
public class RecordingDeleteResponse extends BTAResponseProximus {

    @XmlElement(name = "Recordings")
    private List<Recordings> mRecordings;

    public String[] getRecordingIDs() {
        return mRecordings.stream()
                .map(recordings -> recordings.recordingId)
                .toArray(String[]::new);
    }

    @XmlRootElement(name = "Recordings")
    private static class Recordings {

        @XmlElement(name = "UserRecordID")
        private String recordingId;
    }

}
