package tv.threess.threeready.data.proximus.log.utils;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.model.BaseInfo;

/** Process the '/proc/loadavg' file content
 *
 * example: 3.36 2.54 1.11 8/1574 14005
 * The first three columns measure CPU and I/O utilization of the last one, five, and 15 minute periods.
 * The fourth column shows the number of currently running processes and the total number of processes.
 * The last column displays the last process ID used.
 * https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/4/html/Reference_Guide/s2-proc-loadavg.html
 *
*/
public class LoadInfo extends BaseInfo<LoadInfo.Field> {

    public LoadInfo(String fileContent) {
        super(Field.class);
        if (fileContent == null) {
            return;
        }
        String[] values = fileContent.split(" ");
        if (values.length < 3) {
            Log.w(TAG, "can not parse content: " + fileContent);
            return;
        }
        super.put(Field.Average1Min, values[0]);
        super.put(Field.Average5Min, values[1]);
        super.put(Field.Average15Min, values[2]);
    }

    public enum Field implements BaseInfo.Field {
        Average1Min,
        Average5Min,
        Average15Min,
        ;

        Field(String key) {
            mKey = key;
        }
        Field() {
            mKey = name();
        }
        final String mKey;

        @Override
        public String getKey() {
            return mKey;
        }
    }
}
