package tv.threess.threeready.data.proximus.home.model.editorial;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.home.model.module.EditorialItemStyle;
import tv.threess.threeready.data.proximus.home.model.AccessPointProximus;
import tv.threess.threeready.data.proximus.home.model.DisplayNamesProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Editorial item for a swimlane static tile to open an access point.
 *
 * @author Barabas Attila
 * @since 9/2/21
 */
public class StaticTileAccessPointEditorialItemProximus extends AccessPointEditorialItemProximus {

    private final SwimLaneProximus.StaticTile mStaticTile;

    public StaticTileAccessPointEditorialItemProximus(SwimLaneProximus.StaticTile staticTile,
                                                      AccessPointProximus accessPoint) {
        super(accessPoint);
        mStaticTile = staticTile;
    }

    @Override
    public String getTitle() {
        if (mStaticTile.getDisplayNames() == DisplayNamesProximus.EMPTY_NAMES) {
            return super.getTitle();
        }
        return mStaticTile.getDisplayNames().getLocalizedNameOrFirst();
    }

    @Nullable
    @Override
    public EditorialItemStyle getStyle() {
        if (mStaticTile.getBackgroundImage() == null) {
            return super.getStyle();
        }
        return mItemStyle;
    }

    protected final EditorialItemStyle mItemStyle = new EditorialItemStyle() {
        @Nullable
        @Override
        public IImageSource getBackgroundImage() {
            return mStaticTile.getBackgroundImage();
        }
    };
}
