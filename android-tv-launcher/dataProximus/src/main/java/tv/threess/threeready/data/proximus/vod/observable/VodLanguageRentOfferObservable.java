package tv.threess.threeready.data.proximus.vod.observable;

import android.content.Context;

import io.reactivex.ObservableEmitter;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.wrapper.LanguageRentOffer;
import tv.threess.threeready.api.vod.wrapper.VodRentOffers;
import tv.threess.threeready.data.proximus.account.SettingsProximus;

/**
 * This observable will called from any vod detail page on rent button clicked. Create the {@link LanguageRentOffer}
 * model object which holds the ordered data and emits it for the ui.
 *
 * Created by Bartalus Csaba - Zsolt
 * since 2022.04.07
 */
public class VodLanguageRentOfferObservable extends BaseVodRentOfferObservable<LanguageRentOffer> {

    public VodLanguageRentOfferObservable(Context context, IBaseVodItem vod) {
        super(context, vod);
    }

    @Override
    protected void publishData(ObservableEmitter<LanguageRentOffer> emitter, VodRentOffers rentOffers) {
        emitter.onNext(new LanguageRentOffer(LocaleUtils.getShopLanguage(SettingsProximus.shopLanguage.getList()), rentOffers.getVod(), rentOffers.getCredit(), rentOffers.getVariantOfferMap()));
        emitter.onComplete();
    }
}
