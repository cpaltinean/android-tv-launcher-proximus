package tv.threess.threeready.data.proximus.account.model.registration.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.data.proximus.account.model.registration.request.DRMClientProximus;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * The available DRM clients registered for the device.
 *
 * @author Barabas Attila
 * @since 2020.04.09
 */
@XmlRootElement(name = "")
public class DRMRegisteredClientsResponseProximus extends BTAResponseProximus {

    @XmlElementWrapper(name = "DRMClients")
    @XmlElement(name = "DRMClientCapability")
    private List<DRMClientProximus> mCapabilities;

    /**
     * @return DRM client id for OTT playback.
     */
    public String getOTTDrmClientId() {
        if (mCapabilities == null) {
            return "";
        }

        for (DRMClientProximus capability : mCapabilities) {
            if (DRMClientProximus.ExternalId.OTT
                    .equalsIgnoreCase(capability.getDRMSystemExternalId())) {
                return capability.getDRMClientDeviceId();
            }
        }
        return "";
    }

    /**
     * @return DRM client id for IPTV playback.
     */
    public String getIPTVDrmClientId() {
        if (mCapabilities == null) {
            return "";
        }

        for (DRMClientProximus capability : mCapabilities) {
            if (DRMClientProximus.ExternalId.IPTV
                    .equalsIgnoreCase(capability.getDRMSystemExternalId())) {
                return capability.getDRMClientDeviceId();
            }
        }
        return "";
    }

}
