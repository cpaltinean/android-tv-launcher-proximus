package tv.threess.threeready.data.proximus.tv.retrofit.converter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 * Convert dates to string in EPG requests.
 *
 * @author Barabas Attila
 * @since 2020.03.30
 */
public class EPGDateRetrofitConverterProximus extends Converter.Factory {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    static {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Override
    public Converter<?, String> stringConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        Converter<?, String> converter = null;
        if (type instanceof Class && ((Class<?>) type).isAssignableFrom(Date.class)) {
            converter = value -> DATE_FORMAT.format((Date) value);
        }
        return converter;
    }
}