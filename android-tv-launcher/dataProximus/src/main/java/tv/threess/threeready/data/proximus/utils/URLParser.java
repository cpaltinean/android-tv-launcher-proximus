package tv.threess.threeready.data.proximus.utils;

import android.net.Uri;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.data.mw.MwProxy;

/**
 * Utility class that formats URL strings
 *
 * @author Denisa Trif
 * @since 29.03.2019
 */
public class URLParser {
    private static final String SW_VERSION = "@SWVersion";
    private static final String IP = "@IP";
    private static final String SERIAL_NUMBER = "@SerialNumber";
    private static final String STB_MODEL = "@STBModel";
    private static final String ACCTNUM = "@ACCTNUM";
    private static final String LOCATION = "@Location";
    private static final String MAC = "@MAC";
    private static final String TM = "@TM";
    private static final String LANGUAGE = "@Language";
    private static final String KEYCODE = "@KeyCode";
    private static final String TVCHANNEL = "@TVChannel";
    private static final String TRUE_HD_MODE = "@TrueHDMode";

    private static final String CLIENT_PRODUCT = "STB";
    private static final String CLIENT_OS = "ANDTV";
    private static final String DEVICE_TYPE = "V7";

    public static String getURLWithReplacedTemplateParameters(String originalURI, String accountNumber, String location, String site) {
        MwProxy mwProxy = Components.get(MwProxy.class);
        return originalURI
                .replace(SW_VERSION, mwProxy.getAppVersion())
                .replace(IP,  mwProxy.getIpAddress() + "")
                .replace(SERIAL_NUMBER,  mwProxy.getHardwareSerialNumber())
                .replace(STB_MODEL,  mwProxy.getHardwareVersion())
                .replace(ACCTNUM, accountNumber)
                .replace(LOCATION, location)
                .replace(MAC, mwProxy.getEthernetMacAddress() + "")
                .replace(TM, site)
                .replace(LANGUAGE, LocaleUtils.getApplicationLanguage());
    }

    public static String getURLWithReplacedChannelInfo(String originalURI, String accountNumber,
                                                       String location, String site,
                                                       String keyCode, String channelName, boolean trueHd) {
        return getURLWithReplacedTemplateParameters(originalURI, accountNumber, location, site)
                .replace(KEYCODE, keyCode)
                .replace(TVCHANNEL, channelName)
                .replace(TRUE_HD_MODE, String.valueOf(trueHd));
    }

    public static String getBTAHeader() {
        MwProxy mwProxy = Components.get(MwProxy.class);
        Uri.Builder uriBuilder = new Uri.Builder();
        uriBuilder.path(CLIENT_PRODUCT)
                .appendPath(mwProxy.getAppVersion())
                .appendPath(CLIENT_OS)
                .appendPath(DEVICE_TYPE)
                .appendPath(mwProxy.getMWVersion())
                .build();
        return uriBuilder.toString();
    }
}
