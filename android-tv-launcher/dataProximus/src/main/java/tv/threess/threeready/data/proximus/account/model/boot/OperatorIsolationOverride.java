package tv.threess.threeready.data.proximus.account.model.boot;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import tv.threess.threeready.data.generic.model.BaseResponse;

@XmlRootElement(name = "OperatorIsolationOverride")
public class OperatorIsolationOverride implements BaseResponse {

    @XmlValue
    private boolean mValue;

    public boolean isOperatorOverriding() {
        return mValue;
    }
}
