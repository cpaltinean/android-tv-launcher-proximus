package tv.threess.threeready.data.proximus.account.model.boot;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import okhttp3.HttpUrl;
import tv.threess.threeready.data.proximus.account.model.boot.adapter.TMServerInfoTypeAdapter;

/**
 * Class which holds the address and attributes about a TM server.
 *
 * @author Barabas Attila
 * @since 2020.04.02
 */
@XmlRootElement(name = "Server")
public class TMServerInfoProximus implements Serializable {

    @XmlAttribute(name = "Address")
    private String mAddress;

    @XmlJavaTypeAdapter(TMServerInfoTypeAdapter.class)
    @XmlAttribute(name = "Type")
    private Type mType;

    @XmlElement(name = "Attribute")
    private List<TMServerAttributeProximus> mAttributes;

    /**
     * @return The address of the TM server.
     */
    public String getAddress() {
        return mAddress;
    }

    /**
     * @return The type of the TM server. E.g VCAS.
     */
    public Type getType() {
        return mType;
    }

    /**
     * @param key The name of the attribute.
     * @return The string value of the attribute or null if not exists.
     */
    @Nullable
    public String getAttributeValue(TMServerAttributeProximus.Key key) {
        if (mAttributes == null) {
            return null;
        }

        for (TMServerAttributeProximus attribute : mAttributes) {
            if (key.getName().equals(attribute.getName())) {
                return attribute.getValue();
            }
        }
        return null;
    }

    public String getUrl() {
        return getUrl(false);
    }

    /**
     * @param onlyHttps If only https url should be returned.
     * @return Get the server info as url.
     */
    public String getUrl(boolean onlyHttps) {
        HttpUrl.Builder url = new HttpUrl.Builder().host(getAddress());


        String securedPort = getAttributeValue(TMServerAttributeProximus.Key.ServerSecuredPort);
        // HTTPS port
        if (securedPort != null) {
            url.port(Integer.parseInt(securedPort));
            url.scheme("https");

        // HTTP port
        } if (!onlyHttps) {
            String port = getAttributeValue(TMServerAttributeProximus.Key.ServerPort);
            if (port != null) {
                url.port(Integer.parseInt(port));
                url.scheme("http");
            }
        }

        String protocol = getAttributeValue(TMServerAttributeProximus.Key.ServerProtocol);
        if (protocol != null) {
            url.scheme(protocol);
        }

        String path = getAttributeValue(TMServerAttributeProximus.Key.ServerPath);
        if (path != null) {
            url.encodedPath(path);
        }

        return url.toString();
    }

    /**
     * The known TM server types.
     */
    public enum Type {
        UBCDN("UBCDN"),
        VCAS("VES"),
        OIO("OIO"),
        MS("MS"),
        TM("TM");

        private final String mKey;

        Type(String key) {
            mKey = key;
        }

        public String getKey() {
            return mKey;
        }
    }
}
