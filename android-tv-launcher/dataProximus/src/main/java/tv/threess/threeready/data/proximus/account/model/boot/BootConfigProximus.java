package tv.threess.threeready.data.proximus.account.model.boot;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Proximus boot config file which holds the backend endpoints
 * and client configuration and caching options.
 *
 * @author Barabas Attila
 * @since 2020.03.17
 */
public class BootConfigProximus implements Serializable {

    @SerializedName("POA")
    private boolean mTIAMSBypassAllowed;

    @SerializedName("epgBaseUrl")
    private String mEPGBaseUrl;

    @SerializedName("babyContentWarningChannels")
    private List<String> mBabyChannelIds;

    @SerializedName("vqe")
    private VQEConfigProximus mVQEConfig;

    @SerializedName("igmpRejoin")
    private IGMPRejoinProximus mIGMPRejoin;

    @SerializedName("partnersConfig")
    private PartnerConfigContainerProximus mPartnerConfigContainer;

    @SerializedName("IAM")
    private IAMConfigProximus mIAMConfig;

    @SerializedName("adultChannels")
    private HashSet<String> mAdultChannels;

    @SerializedName("regionalChannels")
    private HashSet<String> mRegionalChannels;

    @SerializedName("robust_getRecordingList")
    private boolean mRobuestGetRecordingListAllowed;

    /**
     * @return True if TIAMS authentication bypass is allowed.
     */
    public boolean isTIAMSBypassAllowed() {
        return mTIAMSBypassAllowed;
    }

    /**
     * @return True if robust of GetRecordingList is allowed.
     */
    public boolean isRobustGetRecordingListAllowed() {
        return mRobuestGetRecordingListAllowed;
    }

    /**
     * @return Base url for EPG backend calls.
     */
    public String getEPGBaseUrl() {
        return mEPGBaseUrl;
    }

    /**
     * @return A list of channel ids which are marked as baby channel.
     * A warning dialog should be displayed on these channels before the playback.
     */
    @NonNull
    public List<String> getBabyChannelIds() {
        if (mBabyChannelIds == null) {
            return Collections.emptyList();
        }
        return mBabyChannelIds;
    }

    /**
     * @return A set of channel id which should be considered as adult
     * and the maximum age rating applied even when there is no EPG data.
     */
    public Set<String> getAdultChannelIds() {
       if (mAdultChannels == null) {
           return Collections.emptySet();
       }
       return mAdultChannels;
    }

    /**
     * @return A set of channel id of the placeholder regional channels.
     */
    public Set<String> getRegionalChannelIds() {
        if (mRegionalChannels == null) {
            return Collections.emptySet();
        }
        return mRegionalChannels;
    }

    /**
     * @return VQE related boot config options.
     */
    public VQEConfigProximus getVQEConfig() {
        return mVQEConfig;
    }

    /**
     * @return IGMP rejoin parameters.
     */
    public IGMPRejoinProximus getIGMPRejoin() {
        return mIGMPRejoin;
    }

    /**
     * @return A container which holds the 3rd party app configs
     */
    public PartnerConfigContainerProximus getPartnerConfigs() {
        return mPartnerConfigContainer;
    }

    /**
     * @return IAM related config options.
     */
    @Nullable
    public IAMConfigProximus getIAMConfig() {
        return mIAMConfig;
    }
}
