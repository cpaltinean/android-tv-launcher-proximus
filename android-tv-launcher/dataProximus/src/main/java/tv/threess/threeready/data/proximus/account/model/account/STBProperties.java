package tv.threess.threeready.data.proximus.account.model.account;

/**
 * A simple Enum class with the possible STB property names
 *
 * @author Solyom Zsolt
 * @since 2020.07.14
 */
public enum STBProperties {
    SHOP_LANGUAGE("ShopLanguage"),
    FAVORITE_CHANNELS("FavoriteChannels"),
    POWER_CONSUMPTION("PowerConsumption"),
    STREAM_AUDIO_LANGUAGE("AudioLanguage"),
    STREAM_SUBTITLE_LANGUAGE("SubtitleLanguage"),
    EPG_LAYOUT("EpgLayout"),
    VIEWED_HINTS("ViewedHints");

    private final String mProperty;

    STBProperties(String property) {
        mProperty = property;
    }

    public String getPropertyName() {
        return mProperty;
    }

    public static STBProperties fromString(String property) {
        for (STBProperties it : STBProperties.values()) {
            if (it.mProperty.equalsIgnoreCase(property)) {
                return it;
            }
        }
        return null;
    }

}