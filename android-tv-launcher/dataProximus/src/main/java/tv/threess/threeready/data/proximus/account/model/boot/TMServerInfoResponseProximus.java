package tv.threess.threeready.data.proximus.account.model.boot;

import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.proximus.account.model.boot.TMServerInfoProximus.Type;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;

/**
 * Information about the total managed servers.
 *
 * @author Barabas Attila
 * @since 2020.04.02
 */
public class TMServerInfoResponseProximus extends BTAResponseProximus {
    private static final String TAG = Log.tag(TMServerInfoResponseProximus.class);

    private static final String OPERATOR_ISOLATION_OVERRIDE_FILE = "status.xml";
    private static final String VQE_CHANNEL_CONFIG_FILE = "VQECHANNELCONFIG";
    private static final String VQE_NETWORK_CONFIG_FILE = "VQENETWORKCONFIG";
    private static final String GLOBAL_INSTALL_FILE = "GLOBALINSTALL";
    private static final String WEB_ACCESS_FILE = "WEBACCESS";
    private static final String AVAILABLE_VERSIONS_FILE = "availableBrokerVersions";


    @XmlElementWrapper(name = "ServerList")
    @XmlElement(name = "Server")
    private List<TMServerInfoProximus> mTMServerInfoList;

    /**
     * @param type The type of the TM server.
     * @return The TM server information for the given type.
     */
    @Nullable
    private TMServerInfoProximus getServerConfig(Type type) {
        for (TMServerInfoProximus serverInfo : mTMServerInfoList) {
            if (serverInfo.getType() == type) {
                return serverInfo;
            }
        }

        return null;
    }

    /**
     * @return The base URL for the VQE network and channel config files.
     */
    private String getBrokerBasUrl() {
        TMServerInfoProximus serverInfo = getServerConfig(Type.UBCDN);
        if (serverInfo == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();

        // HTTP or HTTPS protocol
        String protocol = serverInfo.getAttributeValue(
                TMServerAttributeProximus.Key.BrokerDownloadProtocol);
        protocol = TextUtils.isEmpty(protocol) ? "http" : protocol;
        sb.append(protocol).append("://");

        // Server address
        String address = serverInfo.getAddress();
        sb.append(address);

        // Server port
        String port = serverInfo.getAttributeValue(
                TMServerAttributeProximus.Key.BrokerCDNPort);
        if (!TextUtils.isEmpty(port)) {
            sb.append(":").append(port);
        }

        // Path to broker documents.
        String path = serverInfo.getAttributeValue(
                TMServerAttributeProximus.Key.BrokerPublishingPath);
        if (!TextUtils.isEmpty(path)) {
            sb.append(path);
        }

        sb.append("/");

        return sb.toString();
    }

    /**
     * @return The URL for the VQE channel config file.
     */
    public String getVQEChannelConfigUrl() {
        return getBrokerBasUrl() + VQE_CHANNEL_CONFIG_FILE;
    }

    /**
     * @return The URL for the VQE network config file.
     */
    public String getVQENetworkConfigUrl() {
        return getBrokerBasUrl() + VQE_NETWORK_CONFIG_FILE;
    }

    /**
     * @return The address of the VCAS server.
     */
    public String getVCASServerAddress() {
        TMServerInfoProximus serverInfo = getServerConfig(Type.VCAS);
        if (serverInfo == null) {
            return "";
        }
        return serverInfo.getAddress();
    }

    /**
     * @return Base url of the BTA server bypassing the TIAMS authentication.
     */
    public String getUnsecuredBtaUrl() {
        TMServerInfoProximus serverInfo = getServerConfig(Type.TM);
        if (serverInfo == null) {
            return "";
        }
        return serverInfo.getUrl(true);
    }

    /**
     * @return Url from where to get the global install broker doc.
     */
    public String getGlobalInstallConfigUrl() {
        return getBrokerBasUrl() + GLOBAL_INSTALL_FILE;
    }

    public String getWebAccessFileUrl() {
        return getBrokerBasUrl() + WEB_ACCESS_FILE;
    }

    /**
     * @return Url from where to get the broker document versions.
     */
    public String getAvailableVersionsUrl() {
        return getBrokerBasUrl() + AVAILABLE_VERSIONS_FILE;
    }

    public String getOperatorIsolationOverrideUrl() {
        TMServerInfoProximus serverInfo = getServerConfig(Type.OIO);
        if (serverInfo == null) {
            return null;
        }

        try {
            return Uri.parse(serverInfo.getUrl()).buildUpon()
                    .appendEncodedPath(OPERATOR_ISOLATION_OVERRIDE_FILE)
                    .build().toString();
        } catch (Exception e) {
            Log.e(TAG, "Failed to build Operator isolation override url", e);
        }

        return null;
    }
}
