package tv.threess.threeready.data.proximus.pvr.model;

import androidx.annotation.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.proximus.home.adapter.PVRAssetStatusTypeAdapter;
import tv.threess.threeready.data.proximus.home.adapter.SupportedMTPTypeAdapter;
import tv.threess.threeready.data.proximus.tv.model.BaseBroadcastProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.MetaInfoProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.ProgramMetaInfoKeyProximus;

/**
 * Proximus implementation of the recording broadcast item.
 * Used to store info for linear recording assets.
 *
 * @author Barabas Attila
 * @since 2018.10.23
 */
@XmlRootElement(name = "Recording")
public class RecordingProximus extends BaseBroadcastProximus implements IRecording {

    private static final int REC_AVAILABILITY_DAYS = 60;

    @XmlElement(name = "userRecordId")
    private String mRecordingId;

    @XmlElement(name = "SchTrailID")
    private String mTrailId;

    @XmlElement(name = "SystemRecordID")
    private String mSystemRecordId;

    @XmlElement(name = "RecordingStartTime")
    private long mRecordingStart;

    @XmlElement(name = "RecordingDuration")
    private int mRecordingDuration;

    @XmlElement(name = "ProgramDuration")
    private int mProgramDuration;

    @XmlElement(name = "ChannelExtID")
    private String mChannelId;

    @XmlElement(name = "ProgStartTime")
    private long mProgramStartTime;

    @XmlElement(name = "ProgMetainfo")
    private String programMetaInfo;

    @XmlElement(name = "UserStopTimeMarker")
    private long mUserEndMarker;

    @XmlElement(name = "UserStartTimeMarker")
    private long mUserStartMarker;

    @XmlElement(name = "AssetStartDelta")
    private long mAssetStartDelta;

    @XmlElement(name = "Failed")
    private boolean mIsFailedRecording;

    @XmlElementWrapper(name = "Assets")
    @XmlElement(name = "PVRAsset")
    private List<PVRAsset> mPVRAssetList;

    public RecordingProximus() {
        super(EMPTY_PROGRAM_META_INFO);
    }

    @Override
    public String getRecordingId() {
        return mRecordingId;
    }

    @Override
    public String getSystemRecordId() {
        return mSystemRecordId;
    }

    @Override
    public String getId() {
        return buildBroadcastId(mChannelId, mTrailId);
    }

    @Override
    public String getChannelId() {
        return mChannelId;
    }

    @Override
    public long getRecordingStart() {
        return mRecordingStart + mUserStartMarker + mAssetStartDelta;
    }

    @Override
    public long getStart() {
        return mProgramStartTime;
    }

    @Override
    public long getRecordingEnd() {
        return mRecordingStart + mUserEndMarker;
    }

    @Override
    public long getEnd() {
        return mProgramStartTime + TimeUnit.MINUTES.toMillis(mProgramDuration);
    }

    @Override
    public boolean isNextToLive() {
        return false;
    }

    @Override
    public boolean isRecordingFinished() {
        return getRecordingEnd() < System.currentTimeMillis();
    }

    @Override
    public long getAvailabilityTime() {
        return TimeUnit.DAYS.toMillis(REC_AVAILABILITY_DAYS) + getEnd();
    }

    @Override
    public boolean isFailedRecording() {
        if (!SettingsProximus.robustGetRecordingListAllowed.get(false)) {
            return mIsFailedRecording;
        }

        for (PVRAsset pvrAsset : ArrayUtils.notNull(mPVRAssetList)) {
            if (pvrAsset.isMTPIDSupported() && !pvrAsset.isFailed()) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof RecordingProximus) {
            return Objects.equals(getRecordingId(), ((RecordingProximus) obj).getRecordingId());
        }

        return false;
    }

    public String getProgramMetaInfo() {
        return programMetaInfo;
    }

    public void setMetaInfo(MetaInfoProximus<ProgramMetaInfoKeyProximus> programMetaInfo) {
        mProgramMetaInfo = programMetaInfo;
    }

    @XmlRootElement(name = "PVRAsset")
    public static class PVRAsset {

        @XmlElement(name = "MTP")
        private MTP mMTP;

        @XmlJavaTypeAdapter(PVRAssetStatusTypeAdapter.class)
        @XmlElement(name = "Status")
        private PVRAssetStatus mStatus;

        private boolean isMTPIDSupported() {
            return mMTP.getID() != MTPID.UNSUPPORTED;
        }

        private boolean isFailed() {
            return mStatus == PVRAssetStatus.RecordFailed;
        }
    }

    private static class MTP {
        @XmlJavaTypeAdapter(SupportedMTPTypeAdapter.class)
        @XmlAttribute(name = "MTPID")
        private MTPID mID;

        public MTPID getID() {
            return mID;
        }
    }

    public enum PVRAssetStatus {
        RecordSuccess,
        RecordFailed,
        Unknown
    }

    public enum MTPID {
        DASH_IPTV_HD_ENCRYPTED,
        DASH_IPTV_SD_ENCRYPTED,
        DASH_IPTV_HD,
        DASH_IPTV_SD,
        UNSUPPORTED
    }
}
