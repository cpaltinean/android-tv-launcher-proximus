package tv.threess.threeready.data.proximus.tv.model.advertisement;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Each <Ad> contains a single <InLine> element or <Wrapper> element (but never both).
 */
public class VastAd {
    @XmlAttribute(name = "id")
    private String mId;

    @XmlAttribute(name = "sequence")
    private int mSequence;

    @XmlElement(name = "InLine")
    private VastInLine mInLine;

    public String getId() {
        return mId;
    }

    public int getSequence() {
        return mSequence;
    }

    public VastInLine getInLine() {
        return mInLine;
    }
}
