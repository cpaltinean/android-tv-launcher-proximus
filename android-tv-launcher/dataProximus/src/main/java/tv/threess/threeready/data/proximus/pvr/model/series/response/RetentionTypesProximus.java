package tv.threess.threeready.data.proximus.pvr.model.series.response;

/**
 * Proximus Retentaion Types enumeration used for {@link RecordingQuotaProximus}.
 *
 * Created by Daniela Toma on 04/22/2020.
 */
public enum RetentionTypesProximus {
    LongTermConservation,
    ShortTermConservation
}
