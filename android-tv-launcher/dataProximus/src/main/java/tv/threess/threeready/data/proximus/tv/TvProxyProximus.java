/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.tv;

import android.app.Application;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.model.TvAdReplacement;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.proximus.account.SessionProximus;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.proximus.account.retrofit.ConfigXmlRetrofitProximus;
import tv.threess.threeready.data.proximus.generic.RetrofitServicesProximus;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseErrorProximus;
import tv.threess.threeready.data.proximus.mw.MwProxyProximus;
import tv.threess.threeready.data.proximus.tv.model.advertisement.TvAdvertisementProximus;
import tv.threess.threeready.data.proximus.tv.model.advertisement.VastAdProximus;
import tv.threess.threeready.data.proximus.tv.model.advertisement.VastIdCdata;
import tv.threess.threeready.data.proximus.tv.model.advertisement.VastResponse;
import tv.threess.threeready.data.proximus.tv.model.advertisement.VastTracking;
import tv.threess.threeready.data.proximus.tv.model.broadcast.request.EPGBlockProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.request.EPGRequestDateProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.AvailableUpdatesResponseProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.BTAAiringsProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.TvBroadcastProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.TvBroadcastResponseProximus;
import tv.threess.threeready.data.proximus.tv.model.channel.TvChannelResponseProximus;
import tv.threess.threeready.data.proximus.tv.model.channel.VCASInfoTCH;
import tv.threess.threeready.data.proximus.tv.model.channel.VQEBurstConfigBuilderProximus;
import tv.threess.threeready.data.proximus.tv.model.channel.VQEInfoTCH;
import tv.threess.threeready.data.proximus.tv.retrofit.BTATvRetrofitProximus;
import tv.threess.threeready.data.proximus.tv.retrofit.EPGRetrofitProximus;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.tv.TvProxy;
import tv.threess.threeready.data.tv.model.TvAdvertisement;

/**
 * TvProxy class implementation for the proximus backend.
 *
 * @author Barabas Attila
 * @since 2018.06.05
 */
public class TvProxyProximus implements TvProxy, Component {
    private static final String TAG = Log.tag(TvProxyProximus.class);

    private static final String AD_TAS_PATH_SEGMENT = "ad";
    private static final String ADS_TAS_PATH_SEGMENT = "ads";
    private static final String ADS_TAP_PATH_SEGMENT = "getAd";
    private static final String ADS_TAP_VERSION = "1.1";
    private static final String ADS_TAP_VAST_VERSION = "vast3.0";

    private static final String VAST_MACRO_CACHE = "[CACHE]";
    private static final String VAST_MACRO_MODIFIED = "[MODIFIED]";
    private static final String VAST_MACRO_ERROR_CODE = "[ERRORCODE]";
    private static final String VAST_VALUE_PRE_DOWNLOAD = "PRE-DOWNLOAD";
    private static final String VAST_VALUE_ON_MARKER = "ON-MARKER";

    private final Application mApplication;

    // The maximum number of characters in batch airing and channel id request.
    private static final int MAX_BATCH_AIRING_CHANNEL_QUERY_CHAR_LIMIT = 1500;

    private final RetrofitServicesProximus mRetrofit = Components.get(RetrofitServicesProximus.class);
    private final ConfigXmlRetrofitProximus mXmlConfigRetrofit = mRetrofit.getXmlConfigRetrofit();
    private final BTATvRetrofitProximus mTvRetrofit = mRetrofit.getTvRetrofit();
    private final EPGRetrofitProximus mEPGRetrofit = mRetrofit.getEPGRetrofit();
    private final OkHttpClient mOkHttpClient = mRetrofit.getDefaultOkHttpClient();

    private final TvCache mTvCache = Components.get(TvCache.class);

    public TvProxyProximus(Application application) {
        mApplication = application;
    }

    /**
     * @return The backend channel response, which holds the channels list and scan parameters.
     * @throws IOException in case of errors.
     */
    @Override
    public TvChannelResponseProximus getChannels() throws IOException {
        return RetrofitServicesProximus.execute(mTvRetrofit.getChannels());
    }

    /**
     * @return The generic VQE config for network
     * @throws IOException In case of errors.
     */
    protected String getVQEChannelConfig() throws IOException {
        String channelConfigUrl = SessionProximus.vqeChannelConfigUrl.get("");

        Request request = new Request.Builder().url(channelConfigUrl).build();
        Response response = mOkHttpClient.newCall(request).execute();

        // HTTP error.
        if (!response.isSuccessful() || response.body() == null) {
            throw new BackendException(response.code(), response.message());
        }

        return response.body().string();
    }

    /**
     * @return The generic VQE config for each channel.
     * @throws IOException In case of errors.
     */
    protected String getVQENetworkConfig() throws IOException {
        String networkConfigUrl = SessionProximus.vqeNetworkConfigUrl.get("");

        Request request = new Request.Builder().url(networkConfigUrl).build();
        okhttp3.Response response = mOkHttpClient.newCall(request).execute();

        // HTTP error.
        if (!response.isSuccessful() || response.body() == null) {
            throw new BackendException(response.code(), response.message());
        }

        return response.body().string();
    }

    /**
     * Calculate the VQE override config for the device.
     *
     * @return The VQE config which will override the generic channel and network config.
     */
    protected String getVQEBurstConfig() {
        if (!SettingsProximus.vqeControlEnabled.get(false)) {
            // VQE override disabled.
            return "";
        }

        long sdBandwidth = SettingsProximus.sdBandwidth.get(0L);
        long hdBandwidth = SettingsProximus.hdBandwidth.get(0L);
        long overheadBandwidth = SettingsProximus.availableOverheadBandwidth.get(0L);
        boolean hdInterest = SettingsProximus.hdProfile.get(false);

        VQEBurstConfigBuilderProximus builder = new VQEBurstConfigBuilderProximus(
                sdBandwidth, hdBandwidth, overheadBandwidth, hdInterest);

        // VQE error repair.
        String retEnabled = SettingsProximus.vqeRETEnabled.get(null);
        if (retEnabled != null) {
            builder.enableRET(Boolean.parseBoolean(retEnabled));
        }

        // VQE rapid channel change.
        String rccEnabled = SettingsProximus.vqeRCCEnabled.get(null);
        if (rccEnabled != null) {
            builder.enableRCC(Boolean.parseBoolean(rccEnabled));
        }

        // Network re-transition buffer
        String networkBuffer = SettingsProximus.vqeNetworkBuffer.get(null);
        if (networkBuffer != null) {
            builder.networkBuffer(Integer.parseInt(networkBuffer));
        }

        return builder.build();
    }

    public VQEInfoTCH getVQEInfoTCH() {
        // Get VQE config files.
        String channelConfig = null;
        try {
            channelConfig = getVQEChannelConfig();
        } catch (Exception e) {
            try {
                channelConfig = FileUtils.readFromFile(MwProxyProximus.channelConfigFile(mApplication));
                Log.e(TAG, "Couldn't load VQEChannel config, using local file", e);
            } catch (Exception ex) {
                Log.e(TAG, "Couldn't load VQEChannel config file.", ex);
            }
        }

        String networkConfig = null;
        try {
            networkConfig = getVQENetworkConfig();
        } catch (Exception e) {
            Log.e(TAG, "Couldn't load VQENetwork config.", e);
        }

        return new VQEInfoTCH(channelConfig, networkConfig, getVQEBurstConfig());
    }

    /**
     * @return The verimatrix server information and attributes.
     */
    public VCASInfoTCH getVCASInformation() {
        String vcasSeverAddress = SessionProximus.vcasServerAddress.get("");
        String subscriberIDHash = SettingsProximus.subscriberIDHash.get("");
        return new VCASInfoTCH(vcasSeverAddress, subscriberIDHash);
    }

    /**
     * @return Information about the currently available EPG version.
     * @throws IOException In case of errors.
     */
    public AvailableUpdatesResponseProximus getAvailableUpdates() throws IOException {
        return RetrofitServicesProximus.executeGen(
                mEPGRetrofit.getAvailableUpdates(
                        SettingsProximus.companySite.get("")));
    }

    /**
     * @return The currently running events on all the available channels.
     * @throws IOException in case of errors.
     */
    @Override
    public TvBroadcastResponseProximus getCurrentEvents() throws IOException {
        return getBroadcasts(System.currentTimeMillis(), System.currentTimeMillis());
    }

    /**
     * Get the events for the given time period on all the available channels.
     *
     * @param from The start of the time period in millis.
     * @param to   The end of the time period in millis.
     * @return The response which contains the requested events.
     * @throws IOException In case of errors.
     */
    @Override
    public TvBroadcastResponseProximus getBroadcasts(long from, long to) throws IOException {
        List<BTAAiringsProximus> responseList = new ArrayList<>();

        List<EPGRequestDateProximus> requests = calculateEPGRequestRange(from, to);

        String splicePath = SessionProximus.epgSplicePath.get("");

        for (EPGRequestDateProximus request : requests) {
            // Call for the whole day.
            if (request.getEPGBlocks().size() == 0) {
                responseList.add(RetrofitServicesProximus.execute(
                        mEPGRetrofit.getEPGData(splicePath, request.getDate())));
                continue;
            }

            // Call for EPG blocks.
            for (EPGBlockProximus epgBlock : request.getEPGBlocks()) {
                responseList.add(RetrofitServicesProximus.execute(
                        mEPGRetrofit.getEPGData(splicePath, request.getDate(), epgBlock)));
            }
        }

        return new TvBroadcastResponseProximus(responseList, from, to);
    }

    /**
     * Get the events for the given time period on a given channel.
     *
     * @param from      The start of the time period in millis.
     * @param to        The end of the time period in millis.
     * @param channelId The unique id of the channel to request events from.
     * @return The response which contains the requested events.
     * @throws IOException In case of errors.
     */
    @Override
    public TvBroadcastResponseProximus getBroadcasts(String channelId, long from, long to) throws IOException {
        List<BTAAiringsProximus> responseList = new ArrayList<>();
        List<Date> dates = calculateEPGDayRange(from, to);

        BackendException lastError = null;

        String splicePath = SessionProximus.epgSplicePath.get("");

        for (Date date : dates) {
            // TODO: request only available data on backend,
            //  change the error handling to trigger internet check in case of an error
            retrofit2.Response<BTAAiringsProximus> response =
                    mEPGRetrofit.getEPGData(splicePath, date, channelId).execute();

            // HTTP error.
            if (!response.isSuccessful() || response.body() == null) {
                // Not in th EPG window. Skipp request.
                if (response.code() == HttpURLConnection.HTTP_NOT_FOUND) {
                    lastError = new BackendException(response.code(), response.message());
                    continue;
                }

                throw new BackendException(response.code(), response.message());
            }

            // BTA error.
            if (response.body().getError() != null) {
                BTAResponseErrorProximus error = response.body().getError();
                throw new BackendException(error.getErrorCode(), error.getErrorDescription());
            }

            responseList.add(response.body());
        }

        // Return the last error if no EPG data could be loaded.
        if (lastError != null && responseList.isEmpty()) {
            throw lastError;
        }

        return new TvBroadcastResponseProximus(responseList, from, to);
    }

    public List<TvBroadcastProximus> getBroadcasts(List<String> broadcastIds, long epgStartDate, long epgEndDate) throws IOException {
        if (broadcastIds.isEmpty()) {
            return Collections.emptyList();
        }

        int i;
        // Join channel and airing ids.
        StringBuilder trailIdBuilder = new StringBuilder();
        StringBuilder channelIdBuilder = new StringBuilder();
        for (i = 0; i < broadcastIds.size(); ++i) {
            String broadcastId = broadcastIds.get(i);
            String trailId = TvBroadcastProximus.buildTrailId(broadcastId);
            String channelId = TvBroadcastProximus.buildChannelId(broadcastId);

            // Batch airing and channel ids limit reached.
            if (trailIdBuilder.length() + trailId.length()
                    + channelIdBuilder.length() + channelId.length() >= MAX_BATCH_AIRING_CHANNEL_QUERY_CHAR_LIMIT) {
                break;
            }

            StringUtils.join(trailIdBuilder, trailId, ",");
            StringUtils.join(channelIdBuilder, channelId, ",");
        }

        // Get broadcasts in batch.
        BTAAiringsProximus response = RetrofitServicesProximus.execute(mTvRetrofit.getBroadcast(
                trailIdBuilder.toString(), channelIdBuilder.toString(), epgStartDate, epgEndDate));
        List<TvBroadcastProximus> broadcastList = new ArrayList<>(response.getBroadcasts());

        // Load next batch if remaining.
        if (i != broadcastIds.size()) {
            broadcastList.addAll(
                    getBroadcasts(broadcastIds.subList(i, broadcastIds.size()), epgStartDate, epgEndDate));
        }

        return broadcastList;
    }

    protected long getPreDownloadSpace(boolean onMarker) {
        long percent = Settings.tadDownloadPercentage.get(80L);
        if (onMarker) {
            percent = 100 - percent;
        }
        return FileUtils.totalMemory(TAD_MOUNT_DIR) * percent / 100;
    }

    public List<TvAdvertisementProximus> getAdvertisements() throws IOException {
        HttpUrl url = HttpUrl.parse(Settings.tasServerUrl.get(null))
                .newBuilder().addPathSegment(ADS_TAS_PATH_SEGMENT).build();

        // convert bytes to kilobytes
        long preDownloadSpaceKb = getPreDownloadSpace(false) / 1024;

        return RetrofitServicesProximus.executeGen(
                mXmlConfigRetrofit.getAdvertisements(url.toString(),
                        Settings.accountNumber.get(null),
                        preDownloadSpaceKb,
                        LocaleUtils.getApplicationLanguage()
                )
        ).getResponse();
    }

    public List<TvAdvertisementProximus> getAdvertisement(String filename) throws IOException {
        HttpUrl url = HttpUrl.parse(Settings.tasServerUrl.get(null)).newBuilder()
                .addPathSegment(AD_TAS_PATH_SEGMENT)
                .addPathSegment(filename)
                .build();

        ArrayList<TvAdvertisementProximus> result = new ArrayList<>(1);
        result.add(RetrofitServicesProximus.executeGen(
                mXmlConfigRetrofit.getAdvertisement(url.toString(), filename)
        ));
        return result;
    }

    @Override
    public TvAdReplacement getAdReplacement(String channelId, double seconds, String segmentDesc) throws IOException {
        HttpUrl url = HttpUrl.parse(Settings.tapServerUrl.get(null))
                .newBuilder().addPathSegments(ADS_TAP_PATH_SEGMENT).build();

        InputStream response = null;
        try {
            response = RetrofitServicesProximus.executeGen(
                    mXmlConfigRetrofit.getAdReplacement(url.toString(),
                            ADS_TAP_VAST_VERSION,
                            ADS_TAP_VERSION,
                            Settings.accountNumber.get(null),
                            Components.get(MwProxy.class).getEthernetMacAddress(),
                            UUID.randomUUID().toString(),
                            channelId, seconds, segmentDesc
                    )
            ).byteStream();

            // we need a new XmlMapper to parse and merge all the impressions
            // in a vast response using VastImpressionListTypeAdapter
            XmlMapper xmlMapper = new XmlMapper();
            xmlMapper.setDefaultUseWrapper(false);
            xmlMapper.registerModule(new JaxbAnnotationModule());
            xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            VastResponse vast = xmlMapper.readValue(response, VastResponse.class);
            if (vast.getId() == null) {
                return new VastAdProximus(vast);
            }

            TvAdvertisement meta = mTvCache.getAdvertisement(vast.getId());
            return new VastAdProximus(vast, meta);
        } finally {
            FileUtils.closeSafe(response);
        }
    }

    @Override
    public void trackAdReplacement(TvAdReplacement replacement, TvAdReplacement.Event event) {
        if (!(replacement instanceof VastAdProximus)) {
            // if null or not a vast response, do nothing
            return;
        }
        VastAdProximus vast = (VastAdProximus) replacement;

        if (event == TvAdReplacement.Event.creativeView || event == TvAdReplacement.Event.start) {
            // because we have a single add with a single creative with a single media file, we can
            for (VastIdCdata impression : vast.getImpressions()) {
                String url = replaceMacros(vast, impression.getValue());
                Request request = new Request.Builder().url(url).build();
                mOkHttpClient.newCall(request).enqueue(sVastCallback);
            }
        }


        for (VastTracking tracking : vast.getTrackingEvents()) {
            try {
                if (event != tracking.getEvent()) {
                    continue;
                }
                String url = replaceMacros(vast, tracking.getUrl());
                Request request = new Request.Builder().url(url).build();
                mOkHttpClient.newCall(request).enqueue(sVastCallback);
            } catch (Exception e) {
                Log.e(TAG, "Failed to start vast callback: " + tracking);
            }
        }
    }

    @Override
    public void trackAdReplacement(TvAdReplacement replacement, TvAdReplacement.Error error) {
        if (!(replacement instanceof VastAdProximus)) {
            // if null or not a vast response, do nothing
            return;
        }
        VastAdProximus vast = (VastAdProximus) replacement;
        for (String errUrl : vast.getErrorUrls()) {
            try {
                String url = errUrl.replace(VAST_MACRO_ERROR_CODE, String.valueOf(error.getCode()));
                Request request = new Request.Builder().url(url).build();
                mOkHttpClient.newCall(request).enqueue(sVastCallback);
            } catch (Exception e) {
                Log.e(TAG, "Failed to start vast callback: " + errUrl);
            }
        }
    }

    /**
     * Calculate the days to request EPG from the have the EPG data in the given time period.
     *
     * @param from The start of the EPG request range.
     * @param to   The end of the EPG request range.
     * @return The calculated dates to request EPG from.
     */
    private List<Date> calculateEPGDayRange(long from, long to) {
        // Clear the from
        Calendar fromDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        fromDate.setTimeInMillis(from);
        resetToDay(fromDate);

        // Clear the to
        Calendar toDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        toDate.setTimeInMillis(to);
        resetToDay(toDate);

        if (fromDate.before(toDate)) {
            return getEpgDays(fromDate, toDate);
        } else {
            return getEpgDays(toDate, fromDate);
        }
    }

    private List<Date> getEpgDays(Calendar from, Calendar to) {
        List<Date> dates = new ArrayList<>();
        while (from.equals(to) || to.after(from)) {
            dates.add(from.getTime());
            from.add(Calendar.DAY_OF_YEAR, 1);
        }
        return dates;
    }

    private void resetToDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    /**
     * Request a list of dates and blocs which needs to be requested
     * from the backend to have EPG data in the given range.
     *
     * @param from The start of the EPG request range.
     * @param to   The end of the EPG request range.
     * @return The calculated dates and blocs for request.
     */
    private static List<EPGRequestDateProximus> calculateEPGRequestRange(long from, long to) {
        List<EPGRequestDateProximus> dates = new ArrayList<>();
        List<EPGBlockProximus> epgBlocs = new ArrayList<>();

        //floor the 'from' part to the closest hour
        from = TimeBuilder.utc(from).floor(TimeUnit.HOURS).get();

        //ceil the 'to' part to the closest hour
        to = TimeBuilder.utc(to).ceil(TimeUnit.HOURS).get();

        //Real timeline with 4hrs blocks (initialized with from)
        Calendar realTimeline = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        realTimeline.setTimeInMillis(from);

        //End of the last block time - hours only (initialized with from).
        Calendar blockTimeline = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        blockTimeline.setTimeInMillis(from);

        Date lastRequestDate = null;
        //until we need to add a new block
        while (to > blockTimeline.getTimeInMillis()) {
            // Find block for time.
            int hourInDay = realTimeline.get(Calendar.HOUR_OF_DAY);
            for (EPGBlockProximus epgBlock : EPGBlockProximus.values()) {
                if (epgBlock.getStartHour() <= hourInDay
                        && epgBlock.getEndHour() > hourInDay) {
                    epgBlocs.add(epgBlock);
                    blockTimeline.set(Calendar.HOUR_OF_DAY, epgBlock.getEndHour());
                    break;
                }
            }

            // Request time.
            int day = realTimeline.get(Calendar.DAY_OF_YEAR);
            lastRequestDate = realTimeline.getTime();

            realTimeline.add(Calendar.HOUR, 4);

            // Day changed calculate request time.
            if (day != realTimeline.get(Calendar.DAY_OF_YEAR)) {
                dates.add(new EPGRequestDateProximus(lastRequestDate, epgBlocs));
                epgBlocs = new ArrayList<>();
            }
        }

        if (epgBlocs.size() > 0) {
            dates.add(new EPGRequestDateProximus(lastRequestDate, epgBlocs));
        }

        return dates;
    }

    /**
     * replace url params as specified in: https://confluenceprd-proximuscorp.msappproxy.net/pages/viewpage.action?spaceKey=BEST&title=V7+TAD+on+Live+Requirements
     * @param vast the vast response needed for to get the values of the macros to be replaced
     * @param url the url containing the macros to be replaced
     * @return a new string with the macros replaced
     */
    private static String replaceMacros(VastAdProximus vast, String url) {
        try {
            if (url.contains(VAST_MACRO_CACHE)) {
                boolean onMarker = vast.isOnMarker();
                url = url.replace(VAST_MACRO_CACHE, onMarker ? VAST_VALUE_ON_MARKER : VAST_VALUE_PRE_DOWNLOAD);
            }
            if (url.contains(VAST_MACRO_MODIFIED)) {
                SimpleDateFormat format = sVastDateFormatProvider.get();
                url = url.replace(VAST_MACRO_MODIFIED, format.format(vast.getLastModified()));
            }

        } catch (Exception e) {
            Log.e(TAG, "failed to replace macros in vast callback", e);
        }
        return url;
    }

    private static final ThreadLocal<SimpleDateFormat> sVastDateFormatProvider = new ThreadLocal<SimpleDateFormat>() {

        private static final String VAST_FORMAT_MODIFIED = "yyyyMMdd-HH:mm";

        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat(VAST_FORMAT_MODIFIED, Locale.ROOT);
        }
    };

    private static final Callback sVastCallback = new Callback() {
        @Override
        public void onFailure(@NotNull Call call, @NotNull IOException e) {
            Log.e(TAG, "Vast tracking failed", e);
        }

        @Override
        public void onResponse(@NotNull Call call, @NotNull Response response) {
            Log.d(TAG, "Vast tracking success");
            FileUtils.closeSafe(response);
        }
    };
}
