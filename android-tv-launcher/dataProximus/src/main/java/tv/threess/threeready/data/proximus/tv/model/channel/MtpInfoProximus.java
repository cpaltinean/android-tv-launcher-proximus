package tv.threess.threeready.data.proximus.tv.model.channel;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Model class representing stream related information of a channel.
 *
 * @author Karetka Mezei Zoltan
 * @since 2020.07.29
 */
@XmlRootElement(name = "MTP")
public class MtpInfoProximus {
    @XmlAttribute(name = "id")
    private String mId;

    @XmlElement(name = "bitrate")
    private String mBitrate;

    @XmlElement(name = "audioCodec")
    private String mAudioCodec;

    @XmlElement(name = "videoCodec")
    private String mVideoCodec;

    @XmlElement(name = "resolution")
    private String mResolution;

    @XmlElement(name = "frameRate")
    private String mFrameRate;

    @XmlElement(name = "container")
    private String mContainer;

    @XmlElement(name = "transport")
    private String mTransport;

    @XmlElement(name = "encryption")
    private String mEncryption;

    @XmlElement(name = "active")
    private String mActive;

    @XmlElement(name = "hdProfile")
    private String mHdProfile;

    @XmlElement(name = "Is3D")
    private String mIs3D;

    @XmlElement(name = "MIMEType")
    private String mMIMEType;

    @XmlElement(name = "BEPDeviceTypeExternalIDs")
    private String mBEPDeviceTypeExternalIDs;

    @XmlElement(name = "priority")
    private String mPriority;

    public String getId() {
        return mId;
    }

    public String getBitrate() {
        return mBitrate;
    }

    public String getAudioCodec() {
        return mAudioCodec;
    }

    public String getVideoCodec() {
        return mVideoCodec;
    }

    public String getResolution() {
        return mResolution;
    }

    public String getFrameRate() {
        return mFrameRate;
    }

    public String getContainer() {
        return mContainer;
    }

    public String getTransport() {
        return mTransport;
    }

    public String getEncryption() {
        return mEncryption;
    }

    public String getActive() {
        return mActive;
    }

    public String getHdProfile() {
        return mHdProfile;
    }

    public String getIs3D() {
        return mIs3D;
    }

    public String getMIMEType() {
        return mMIMEType;
    }

    public String getBEPDeviceTypeExternalIDs() {
        return mBEPDeviceTypeExternalIDs;
    }

    public String getPriority() {
        return mPriority;
    }
}
