package tv.threess.threeready.data.proximus.account.model.partner;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * BTA model to request Netflix authentication token.
 *
 * @author Barabas Attila
 * @since 2020.05.11
 */
@XmlRootElement(name = "BTA")
public class NetflixTokenRequestProximus {

    private static final String SERIAL_NUMBER_KEY = "SerialNumber";

    @XmlElement(name = "GetTokenDoc")
    private final GetTokenDoc mDoc;

    public NetflixTokenRequestProximus(String partnerId, String serial) {
        mDoc = new GetTokenDoc();
        mDoc.mPartnerId = partnerId;
        mDoc.mValues = new ArrayList<>();
        mDoc.mValues.add(new KeyValuePair(SERIAL_NUMBER_KEY, serial));

    }

    @XmlRootElement(name = "GetTokenDoc")
    private static class GetTokenDoc {
        @XmlElement(name = "BEPPartnerID")
        private String mPartnerId;

        @XmlElement(name = "charKeyVal")
        private List<KeyValuePair> mValues;
    }

    @XmlRootElement(name = "charKeyVal")
    private static class KeyValuePair {
        @XmlElement(name = "key")
        private final String mKey;

        @XmlElement(name = "value")
        private final String mValue;

        public KeyValuePair(String key, String value) {
            mKey = key;
            mValue = value;
        }
    }

}
