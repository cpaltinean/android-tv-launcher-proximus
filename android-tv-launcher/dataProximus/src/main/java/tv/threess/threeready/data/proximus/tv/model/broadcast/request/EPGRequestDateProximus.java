package tv.threess.threeready.data.proximus.tv.model.broadcast.request;

import java.util.Date;
import java.util.List;

/**
 * Holds the date and blocks to request EPG from.
 *
 * @author Barabas Attila
 * @since 2020.03.30
 */
public class EPGRequestDateProximus {
    private final Date mDate;
    private final List<EPGBlockProximus> mEPGBlocks;

    public EPGRequestDateProximus(Date date, List<EPGBlockProximus> EPGBlocks) {
        mDate = date;
        mEPGBlocks = EPGBlocks;
    }

    /**
     * @return The date to request EPG from.
     */
    public Date getDate() {
        return mDate;
    }

    /**
     * @return The blocks on the date to request EPG from.
     * If empty the whole day should be requested.
     */
    public List<EPGBlockProximus> getEPGBlocks() {
        return mEPGBlocks;
    }
}
