package tv.threess.threeready.data.proximus.log.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import tv.threess.threeready.api.middleware.model.BaseInfo;

/**
 * Read and store the VQE statistics.
 *
 * @author Barabas Attila
 * @since 2019.12.17
 */
public class VQEClientInfo extends BaseInfo<VQEClientInfo.Field> {

    private static final Uri CONTENT_URI = Uri.parse("content://technicolor/vqe");

    public VQEClientInfo() {
        super(Field.class);
    }

    public static VQEClientInfo read(Context context) {
        VQEClientInfo info = new VQEClientInfo();

        try (Cursor cursor = context.getContentResolver().query(CONTENT_URI, null, null, null)) {
            info.readFrom(cursor, 0, 1);
            return info;
        }
    }

    public enum Field implements BaseInfo.Field {
        PrimaryUdbInputs("primary_udp_inputs"),
        PrimaryUdbDrops("primary_udp_drops"),
        PrimaryRtpInputs("primary_rtp_inputs"),
        PrimaryRtpDrops("primary_rtp_drops"),
        PrimaryRtpDropsLate("primary_rtp_drops_late"),
        PrimaryRtcpInputs("primary_rtcp_inputs"),
        PrimaryRtcpOutputs("primary_rtcp_outputs"),
        RepairRtpInputs("repair_rtp_inputs"),
        RepairRtpDrops("repair_rtp_drops"),
        RepairRtpDropsLate("repair_rtp_drops_late"),
        RepairRtcpInputs("repair_rtcp_inputs"),
        FecInputs("fec_inputs"),
        FecDrops("fec_drops"),
        FecDropsLate("fec_drops_late"),
        RepairRtpStunInputs("repair_rtp_stun_inputs"),
        RepairRtpStunOutputs("repair_rtp_stun_outputs"),
        RepairRtcpStunInputs("repair_rtcp_stun_inputs"),
        RepairRtcpStunOutputs("repair_rtcp_stun_outputs"),
        PostRepairOutputs("post_repair_outputs"),
        TunerQueueDrops("tuner_queue_drops"),
        UnderRuns("underruns"),
        PreRepairLosses("pre_repair_losses"),
        PostRepairLosses("post_repair_losses"),
        PostRepairLossesRcc("post_repair_losses_rcc"),
        RepairsRequested("repairs_requested"),
        RepairsPoliced("repairs_policed"),
        SmartRequestPoliced("smart_request_policed"),
        RepeatRequestSent("repeat_request_sent"),
        RepeatRequestPoliced("repeat_request_policed"),
        FecRecoveredPaks("fec_recovered_paks"),
        ChannelChangeRequests("channel_change_requests"),
        RccRequests("rcc_requests"),
        ConcurentRccsLimited("concurrent_rccs_limited"),
        RccWithLoss("rcc_with_loss"),
        RccAbortsTotal("rcc_aborts_total");

        private final String mKey;

        Field(String key) {
            mKey = key;
        }

        @Override
        public String getKey() {
            return mKey;
        }
    }
}
