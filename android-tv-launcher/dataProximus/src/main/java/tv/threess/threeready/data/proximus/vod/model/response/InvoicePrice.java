package tv.threess.threeready.data.proximus.vod.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Invoice Price used for using the VOD price and price determination result from the API response.
 *
 * @author David
 * @since 04.02.2022
 */
@XmlRootElement(name = "InvoicePrice")
class InvoicePrice implements Serializable {

    @XmlElement(name = "ActualPrice")
    protected Double price;

    @XmlElement(name = "PriceDeterminationResult")
    protected String priceDetermination;

    public Double getPrice() {
        return price;
    }

    public String getPriceDetermination() {
        return priceDetermination;
    }
}
