package tv.threess.threeready.data.proximus.account.model.boot;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * VQE related boot config options
 *
 * @author Barabas Attila
 * @since 2020.04.06
 */
public class VQEConfigProximus implements Serializable {

    @SerializedName("bandwidthSD")
    private int mSDBandwidth;

    @SerializedName("bandwidthHD")
    private int mHDBandwidth;

    /**
     * @return The maximum available bandwidth on SD devices.
     */
    public int getSDBandwidth() {
        return mSDBandwidth;
    }

    /**
     * @return The maximum available bandwidth on HD devices.
     */
    public int getHDBandwidth() {
        return mHDBandwidth;
    }
}
