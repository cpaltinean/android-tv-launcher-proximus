package tv.threess.threeready.data.proximus.account.model.server;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Proximus class which holds the server related attributes.
 *
 * Created by Noemi Dali on 08.04.2020.
 */
@XmlRootElement(name = "Attribute")
public class ServerAttributeProximus implements Serializable {

    @XmlElement(name = "Name")
    private String mName;

    @XmlElement(name = "Value")
    private String mValue;

    /**
     * @return Attribute`s name.
     */
    public String getName() {
        return mName;
    }

    /**
     * @return Attribute`s value.
     */
    public String getValue() {
        return mValue;
    }
}
