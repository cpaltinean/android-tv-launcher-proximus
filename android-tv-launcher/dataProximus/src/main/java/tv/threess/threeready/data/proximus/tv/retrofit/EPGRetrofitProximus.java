package tv.threess.threeready.data.proximus.tv.retrofit;

import java.util.Date;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import tv.threess.threeready.data.proximus.tv.model.broadcast.request.EPGBlockProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.AvailableUpdatesResponseProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.BTAAiringsProximus;

/**
 * Retrofit interface for EPG related backend calls.
 *
 * @author Barabas Attila
 * @since 2019.10.23
 */
public interface EPGRetrofitProximus {
    String PIPE_SCHEDULE_VERSION = "PIPEDSCHEDULE_v.4.2";
    String PIPE_SCHEDULE_DETAIL_LEVEL = "detailLevelEPG-1";

    @GET("{SITE}/AvailableUpdates.xml")
    Call<AvailableUpdatesResponseProximus> getAvailableUpdates(@Path(value = "SITE", encoded = true) String site);

    @GET("{SPLICE_PATH}/" + PIPE_SCHEDULE_VERSION + "_" + PIPE_SCHEDULE_DETAIL_LEVEL + "_{DATE}.xml")
    Call<BTAAiringsProximus> getEPGData(@Path(value = "SPLICE_PATH", encoded = true) String splicePath, @Path("DATE") Date date);

    @GET("{SPLICE_PATH}/" + PIPE_SCHEDULE_VERSION + "_" + PIPE_SCHEDULE_DETAIL_LEVEL + "_{DATE}_{BLOCK}.xml")
    Call<BTAAiringsProximus> getEPGData(@Path(value = "SPLICE_PATH", encoded = true) String splicePath,
                                        @Path("DATE") Date date, @Path("BLOCK") EPGBlockProximus block);

    @GET("{SPLICE_PATH}/" + PIPE_SCHEDULE_VERSION + "_" + PIPE_SCHEDULE_DETAIL_LEVEL + "_{DATE}_{CHANNEL_ID}.xml")
    Call<BTAAiringsProximus> getEPGData(@Path(value = "SPLICE_PATH", encoded = true) String splicePath,
                                        @Path("DATE") Date date, @Path("CHANNEL_ID") String channelId);

}
