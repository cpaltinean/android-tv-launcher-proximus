package tv.threess.threeready.data.proximus.generic.interceptor;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * OkHttpInterceptor to set the timeout for data collection requests.
 *
 * @author Zsolt Bokor_
 * @since 2020.06.09
 */

public class DataCollectionOkHttpInterceptor implements Interceptor {

    private static final long MAX_TIMEOUT_MS = TimeUnit.MINUTES.toMillis(10);

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request request = chain.request();
        Long timeOut = request.tag(Long.class);
        if (timeOut != null) {
            int timeOutMs = (int) Math.min(timeOut, MAX_TIMEOUT_MS);
            return chain
                    .withConnectTimeout(timeOutMs, TimeUnit.MILLISECONDS)
                    .withReadTimeout(timeOutMs, TimeUnit.MILLISECONDS)
                    .withWriteTimeout(timeOutMs, TimeUnit.MILLISECONDS)
                    .proceed(request);
        }
        return chain.proceed(request);
    }
}
