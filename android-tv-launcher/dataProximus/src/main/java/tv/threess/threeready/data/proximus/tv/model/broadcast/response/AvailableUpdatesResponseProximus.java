package tv.threess.threeready.data.proximus.tv.model.broadcast.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *  Backend response which contains information about the currently available EPG version.
 *
 * @author Barabas Attila
 * @since 3/30/21
 */
@XmlRootElement(name = "BrokerResponse")
public class AvailableUpdatesResponseProximus {

    @XmlElement(name = "Path")
    private String mPath;

    /**
     * @return Path of the current EPG splices.
     */
    public String getPath() {
        return mPath;
    }
}
