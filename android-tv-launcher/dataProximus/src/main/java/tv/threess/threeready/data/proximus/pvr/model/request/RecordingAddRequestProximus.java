package tv.threess.threeready.data.proximus.pvr.model.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Retrofit request for scheduling single recording.
 *
 * Created by Noemi Dali on 20.04.2020.
 */
@XmlRootElement(name = "BTA")
public class RecordingAddRequestProximus implements Serializable {

    @XmlElement(name = "AddRecordingDoc")
    private final AddRecordingDoc mAddRecordingDoc;

    public RecordingAddRequestProximus(String trailId, String channelId) {
        mAddRecordingDoc = new AddRecordingDoc();
        mAddRecordingDoc.mSchTrailID = trailId;
        mAddRecordingDoc.mChannelExtId = channelId;
    }

    @XmlRootElement(name = "AddRecordingDoc")
    private static class AddRecordingDoc implements Serializable {

        @XmlElement(name = "ContentType")
        private final String mContentType = "NPVR";

        @XmlElement(name = "SchTrailID")
        private String mSchTrailID;

        @XmlElement(name = "ChannelExtID")
        private String mChannelExtId;

        @XmlElement(name = "DeleteWhenSpaceNeeded")
        private final boolean mDeleteWhenSpaceNeeded = false;

        @XmlElement(name = "Private")
        private final boolean mPrivate = false;
    }

}
