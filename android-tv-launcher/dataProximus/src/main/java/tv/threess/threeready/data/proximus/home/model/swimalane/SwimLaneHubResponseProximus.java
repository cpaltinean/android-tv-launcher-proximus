/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.proximus.home.model.swimalane;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;
import tv.threess.threeready.data.proximus.home.model.AccessPointProximus;

/**
 * BTA response which holds all the available swim lanes for the STB.
 *
 * @author Barabas Attila
 * @since 2019.10.28
 */
public class SwimLaneHubResponseProximus extends BTAResponseProximus {

    @XmlElementWrapper(name = "Swimlanes")
    @XmlElement(name = "Swimlane")
    private ArrayList<SwimLaneProximus> mSwimlanes;

    @XmlElementWrapper(name = "Accesspoints")
    @XmlElement(name = "Accesspoint")
    private ArrayList<AccessPointProximus> mAccessPoints;

    private Map<String, List<AccessPointProximus>> mGroupAccessPointMap;
    private Map<String, AccessPointProximus> mAccessPointIdMap;
    private Map<String, SwimLaneProximus> mSwimlaneIdMap;

    /**
     * @return A list with all the defined swimlanes.
     */
    public List<SwimLaneProximus> getSwimLanes() {
        return mSwimlanes;
    }

    /**
     * @param swimlaneId The id of the swimlane to search for.
     * @return The swimlane with the given id or null if there is no such defined.
     */
    @Nullable
    public SwimLaneProximus getSwimlane(String swimlaneId) {
        if (mSwimlaneIdMap == null) {
            mSwimlaneIdMap = buildSwimlaneMap();
        }
        return mSwimlaneIdMap.get(swimlaneId);
    }

    private Map<String, SwimLaneProximus> buildSwimlaneMap() {
        Map<String, SwimLaneProximus> swimLaneMap = new HashMap<>();
        for (SwimLaneProximus swimlane : mSwimlanes) {
            swimLaneMap.put(swimlane.getId(), swimlane);
        }
        return swimLaneMap;
    }

    /**
     * @return A list with all the defined access points.
     */
    public List<AccessPointProximus> getAccessPoints() {
        return mAccessPoints;
    }

    /**
     * @param id The id of the access point to search for.
     * @return The access point with the given id.
     */
    @Nullable
    public AccessPointProximus getAccessPoint(String id) {
        if (mAccessPointIdMap == null) {
            mAccessPointIdMap = buildAccessPointIdMap();
        }
        return mAccessPointIdMap.get(id);
    }

    private Map<String, AccessPointProximus> buildAccessPointIdMap() {
        Map<String, AccessPointProximus> accessPointMap = new HashMap<>();
        for (AccessPointProximus accessPoint : mAccessPoints) {
            accessPointMap.put(accessPoint.getId(), accessPoint);
        }
        return accessPointMap;
    }

    /**
     * @param groupId The id of the group to search access points for.
     * @return A list of access point with the same group id.
     */
    private List<AccessPointProximus> getGroupAccessPoints(String groupId) {
        if (mGroupAccessPointMap == null) {
            mGroupAccessPointMap = buildGroupAccessPointMap();
        }
        List<AccessPointProximus> accessPoints = buildGroupAccessPointMap().get(groupId);
        return accessPoints == null ? Collections.emptyList() : accessPoints;
    }

    /**
     * @param childAccessPoint The access point to return the parent for.
     * @return The parent of the given access point or null if there is no such.
     */
    @Nullable
    public AccessPointProximus getParent(AccessPointProximus childAccessPoint) {
        if (TextUtils.isEmpty(childAccessPoint.getGroupId())
                || childAccessPoint.getType() != AccessPointProximus.Type.SubPage) {
            return null;
        }

        List<AccessPointProximus> accessPoints = getGroupAccessPoints(childAccessPoint.getGroupId());
        for (AccessPointProximus accessPoint : accessPoints) {
            if (accessPoint.getType() == AccessPointProximus.Type.PageGroup) {
                return accessPoint;
            }
        }
        return null;
    }

    /**
     * @param pageGroupAccessPoint The page group to return subpages for.
     * @return A list with all the subpages for the given page group.
     */
    public List<AccessPointProximus> getSubPages(AccessPointProximus pageGroupAccessPoint) {
        if (TextUtils.isEmpty(pageGroupAccessPoint.getGroupId())
                || pageGroupAccessPoint.getType() != AccessPointProximus.Type.PageGroup) {
            return Collections.emptyList();
        }

        List<AccessPointProximus> subpages = new ArrayList<>();
        List<AccessPointProximus> groupAccessPoints = getGroupAccessPoints(pageGroupAccessPoint.getGroupId());
        for (AccessPointProximus accessPoint : groupAccessPoints) {
            if (accessPoint.getType() == AccessPointProximus.Type.SubPage) {
                subpages.add(accessPoint);
            }
        }

        return subpages;
    }

    private Map<String, List<AccessPointProximus>> buildGroupAccessPointMap() {
        LinkedHashMap<String, List<AccessPointProximus>> groupAccessPointsMap = new LinkedHashMap<>();
        for (AccessPointProximus accessPoint : mAccessPoints) {
            String groupId = accessPoint.getGroupId();
            List<AccessPointProximus> groupedAccessPoints = groupAccessPointsMap.get(groupId);
            if (groupedAccessPoints == null) {
                groupedAccessPoints = new ArrayList<>();
                groupAccessPointsMap.put(groupId, groupedAccessPoints);
            }
            groupedAccessPoints.add(accessPoint);
        }
        return groupAccessPointsMap;
    }
}
