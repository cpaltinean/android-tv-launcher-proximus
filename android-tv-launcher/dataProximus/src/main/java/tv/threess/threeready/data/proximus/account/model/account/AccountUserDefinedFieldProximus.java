package tv.threess.threeready.data.proximus.account.model.account;

import androidx.annotation.Nullable;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.proximus.account.model.account.adapter.AccountUserDefinedFieldAdapterProximus;

/**
 * Represent a parameter defined by the user or 3rd party tools.
 *
 * @author Barabas Attila
 * @since 2020.04.06
 */
@XmlRootElement(name = "field")
public class AccountUserDefinedFieldProximus implements Serializable {
    private static final String TAG = Log.tag(AccountUserDefinedFieldProximus.class);

    @XmlJavaTypeAdapter(AccountUserDefinedFieldAdapterProximus.class)
    @XmlElement(name = "name")
    private Key mKey;

    @XmlElement(name = "value")
    private String mValue;

    /**
     * @return The name of the user defined field.
     */
    public Key getKey() {
        return mKey;
    }

    /**
     * @return The parameter value as Integer or null if it's not defined.
     */
    @Nullable
    public Integer getIntValue() {
        try {
            return Integer.valueOf(mValue);
        } catch (Exception e) {
            Log.e(TAG, "Failed to get long value.", e);
        }
        return null;
    }

    /**
     * @return The parameter value as Boolean or null if it's not defined.
     */
    @Nullable
    public Boolean getBooleanValue() {
        try {
            return Boolean.valueOf(mValue);
        } catch (Exception e) {
            Log.e(TAG, "Couldn't get boolean value.", e);
        }
        return null;
    }

    /**
     * Known user defined field names by the application.
     */
    public enum Key {
        RetEnable("RetEnable"),
        RccEnable("RccEnable"),
        QREnabled("QREnabled"),
        NetworkBufferSize("NetworkBufferSize"),
        QoeControlEnable("QoeControlEnable");

        private final String mFieldName;

        Key(String fieldName) {
            mFieldName = fieldName;
        }

        public String fieldName() {
            return mFieldName;
        }
    }
}
