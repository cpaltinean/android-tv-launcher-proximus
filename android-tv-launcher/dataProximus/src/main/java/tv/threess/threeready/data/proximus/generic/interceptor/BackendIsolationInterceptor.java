package tv.threess.threeready.data.proximus.generic.interceptor;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import io.reactivex.annotations.NonNull;
import okhttp3.Interceptor;
import okhttp3.Response;
import retrofit2.Invocation;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.data.generic.BackendIsolationException;

/**
 * Interceptor to disable backend calls when the box enters isolation mode.
 */
public class BackendIsolationInterceptor implements Interceptor {

    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);

    public BackendIsolationInterceptor() {
    }

    @NonNull
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        if (mInternetChecker.isBackendIsolated()) {
            Invocation invocation = chain.request().tag(Invocation.class);
            if (invocation != null && invocation.method().getAnnotation(Allow.class) == null) {
                throw new BackendIsolationException();
            }
        }
        return chain.proceed(chain.request());
    }

    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Allow {
    }
}
