package tv.threess.threeready.data.proximus.account.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import tv.threess.threeready.data.proximus.account.model.boot.BootConfigProximus;
import tv.threess.threeready.data.proximus.account.model.boot.BootFileNameProximus;
import tv.threess.threeready.data.proximus.generic.interceptor.BackendIsolationInterceptor;

/**
 * Retrofit interface to access proximus boot config file.
 *
 * @author Barabas Attila
 * @since 2020.03.17
 */
public interface BootConfigRetrofitProximus {

    String BASE_URI = "https://tv-lighthousedir.glbmwr.proximustv.be";

    @GET("api/public/lighthouse/environments/signaltypes/{signalType}")
    @BackendIsolationInterceptor.Allow
    Call<List<BootFileNameProximus>> getBootFileName(@Path("signalType") String signalType, @Query("signal") String signal);

    @GET("api/public/lighthouse/environment/{environment}/bootconfig-extends/BCSTBV7")
    @BackendIsolationInterceptor.Allow
    Call<BootConfigProximus> getBootConfig(@Path("environment") String environment);

    /**
     * Signal type for lighthouse calls.
     * In case the DHCP bits are missing, we need to send the serial number to get the registered environment.
     */
    enum SignalType {
        DHCP("BOOTFILENAME"),
        SERIAL("SERIALNUMBER");

        private final String mType;
        SignalType(String type) {
            mType = type;
        }

        public String getType() {
            return mType;
        }
    }
}
