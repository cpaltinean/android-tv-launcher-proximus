package tv.threess.threeready.data.proximus.home.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Tag;
import retrofit2.http.Url;
import tv.threess.threeready.data.proximus.generic.interceptor.CacheControlOverrideOkHttpInterceptor.CacheControlOverride;
import tv.threess.threeready.data.proximus.generic.interceptor.CacheControlOverrideOkHttpInterceptor.CacheControlOverrideOption;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneContentResponseProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneHubResponseProximus;

/**
 * Retrofit interface for swim lane related backend calls.
 *
 * @author Barabas Attila
 * @since 2020.04.30
 */
public interface BTASwimLaneRetrofitProximus {
    String SWIMLANE_ITEM_PATH = "/broker/bta/getNte2Swimlane?";

    @CacheControlOverride
    @GET("/broker/bta/getNte2Hub?&screen=stbv7")
    Call<SwimLaneHubResponseProximus> getSwimLaneHub();

    @CacheControlOverride
    @GET("/broker/bta/getNte2Hub?&screen=stbv7")
    Call<ResponseBody> getSwimLaneHubResponse();

    @CacheControlOverride
    @GET
    Call<SwimLaneContentResponseProximus> getSwimLaneItems(@Url String swimLaneUrl,
                                                           @Tag CacheControlOverrideOption cacheOption);
}
