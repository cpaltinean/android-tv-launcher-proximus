package tv.threess.threeready.data.proximus.account.model.settings;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * BTA doc which holds the information about the device settings
 *
 * @author Solyom Zsolt
 * @since 2020.07.09
 */

@XmlRootElement(name = "Property")
public class STBPropertyProximus implements Serializable {

    @XmlElement(name = "Name")
    private String mName;

    @XmlElement(name = "Value")
    private String mValue;

    public STBPropertyProximus() {
    }

    public STBPropertyProximus(String propertyName, String value) {
        mName = propertyName;
        mValue = value;
    }

    public String getName() {
        return mName;
    }

    public String getValue() {
        return mValue;
    }
}
