package tv.threess.threeready.data.proximus.playback.retrofit;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import tv.threess.threeready.data.proximus.generic.interceptor.RetryOnErrorOkHttpInterceptor.RetryOnError;
import tv.threess.threeready.data.proximus.playback.model.request.AdjectedReplayStreamingBucketRequestProximus;
import tv.threess.threeready.data.proximus.playback.model.request.ReplayStreamingBucketRequestProximus;
import tv.threess.threeready.data.proximus.playback.model.response.ReplayStreamingSessionProximus;

/**
 * Retrofit interface for proximus micro service based playback API.
 *
 * @author Barabas Attila
 * @since 2020.06.24
 */
public interface MSStreamingRetrofitProximus {

    @RetryOnError
    @POST("/api/replay/url")
    Call<ReplayStreamingSessionProximus> getReplayBucket(@Body ReplayStreamingBucketRequestProximus request);

    @RetryOnError
    @POST("/api/replay/url")
    Call<ReplayStreamingSessionProximus> getNextReplayBucket(@Body AdjectedReplayStreamingBucketRequestProximus request);
}
