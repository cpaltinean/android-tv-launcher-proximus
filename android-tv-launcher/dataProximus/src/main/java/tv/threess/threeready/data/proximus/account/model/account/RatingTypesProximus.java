package tv.threess.threeready.data.proximus.account.model.account;

/**
 * Enumeration used for Rating types. It is used in the response of getting Account Information and for updating(in the body).
 *
 * Created by Daniela Toma on 7.04.2020.
 */
public enum RatingTypesProximus {
    // Ratings given to TV-programs.
    VCHIP,
    //Ratings given to TV-programs that happen to be movies.
    MPAA
}