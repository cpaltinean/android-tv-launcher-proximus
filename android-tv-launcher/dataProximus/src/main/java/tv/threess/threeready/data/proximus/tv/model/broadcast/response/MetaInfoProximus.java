package tv.threess.threeready.data.proximus.tv.model.broadcast.response;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.generic.helper.ArrayUtils;

/**
 * Container to access broadcast and program meta info by a key.
 *
 * @author Barabas Attila
 * @since 2019.10.30
 */
public class MetaInfoProximus<TKey extends MetaInfoProximus.IKey> implements Serializable {
    public static final String CATEGORY_PREFIX = "C.";
    private final Map<String, Integer> mMetaInfoIndexMap;
    private final String[] mMetaInfo;

    public MetaInfoProximus() {
        mMetaInfoIndexMap = new HashMap<>();
        mMetaInfo = ArrayUtils.EMPTY_STRING;
    }

    public MetaInfoProximus(Map<String, Integer> metaInfoIndexMap, String[] metaInfo) {
        mMetaInfoIndexMap = metaInfoIndexMap;
        mMetaInfo = metaInfo;
    }

    /**
     * Get meta info for key as long.
     * @param metaInfoKey The key of the meta info.
     * @return The long value for the key.
     */
    long getLong(TKey metaInfoKey) {
        return getLong(metaInfoKey, 0L);
    }

    /**
     * Get meta info for key as long.
     * @param metaInfoKey The key of the meta info.
     * @param defaultValue To be returned if there is no such value.
     * @return The long value for the key.
     */
    long getLong(TKey metaInfoKey, long defaultValue) {
        try {
            return Long.valueOf(getString(metaInfoKey));
        } catch (Exception ignored) {
        }

        return defaultValue;
    }

    /**
     * Get meta info for key as integer.
     * @param metaInfoKey The key of the meta info.
     * @return The integer value for the key.
     */
    public int getInt(TKey metaInfoKey) {
        return getInt(metaInfoKey, 0);
    }

    /**
     * Get meta info for key as integer.
     * @param metaInfoKey The key of the meta info.
     * @param defaultValue To be returned if there is no such value.
     * @return The integer value for the key.
     */
    int getInt(TKey metaInfoKey, int defaultValue) {
        try {
            return Integer.valueOf(getString(metaInfoKey));
        } catch (Exception ignored) {
        }

        return defaultValue;
    }


    /**
     * Get meta info for key as boolean.
     * @param metaInfoKey The key of the meta info.
     * @return The boolean value for the key.
     */
    public boolean getBoolean(TKey metaInfoKey) {
        return getBoolean(metaInfoKey, false);
    }

    /**
     * Get meta info for key as boolean.
     * @param metaInfoKey The key of the meta info.
     * @param defaultValue To be returned if there is no such value.
     * @return The boolean value for the key.
     */
    public boolean getBoolean(TKey metaInfoKey, boolean defaultValue) {
        try {
            return Boolean.valueOf(getString(metaInfoKey));
        } catch (Exception ignored) {
        }

        return defaultValue;
    }

    /**
     * Get meta info for key as string.
     * @param metaInfoKey The key of the meta info.
     * @return The string value for the key.
     */
    public String getString(TKey metaInfoKey) {
        return getString(metaInfoKey, "");
    }

    /**
     * Get meta info for key as string.
     * @param metaInfoKey The key of the meta info.
     * @param defaultValue To be returned if there is no such value.
     * @return The string value for the key.
     */
    public String getString(TKey metaInfoKey, String defaultValue) {
        Integer index = mMetaInfoIndexMap.get(metaInfoKey.getName());
        if (index != null && index > -1 && index < mMetaInfo.length) {
            return mMetaInfo[index];
        }

        return defaultValue;
    }

    /**
     * Get meta info for key as category name.
     * The prefix from the category name will be removed.
     * @param metaInfoKey The key of the meta info.
     * @return The string value for the key.
     */
    @Nullable
    public String getCategory(TKey metaInfoKey) {
        String category = getString(metaInfoKey, null);
        if (category != null && category.startsWith(CATEGORY_PREFIX)) {
            return category.replaceFirst(CATEGORY_PREFIX, "");
        }
        return category;
    }


    /**
     * The key of the meta info to request by.
     */
    public interface IKey {

        /**
         * @return The name of the key as it's defined in the backend response.
         */
        String getName();
    }

    public static final String META_INFO_SEPARATOR = "\\|";
}