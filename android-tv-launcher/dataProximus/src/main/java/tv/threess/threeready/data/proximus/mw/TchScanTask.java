package tv.threess.threeready.data.proximus.mw;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.receivers.BaseBroadcastReceiver;
import tv.threess.threeready.api.generic.receivers.WaitTask;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.proximus.tv.model.channel.IpTvParametersTch;


/**
 * Receiver class used for dvb-c, dvb-t and ip-tv scanning process.
 * the current progress received via broadcast is reported via {@code onProgress}
 * the {@code onComplete} method will report the end of the scan what may be caused by success fail or timeout.
 */
class TchScanTask extends WaitTask<Boolean> {
    protected static final String TAG = Log.tag(TchScanTask.class);

    protected static final String SCAN_STATUS = "com.technicolor.android.dtvscan.SCAN_STATUS";
    protected static final String STATUS_IN_PROGRESS = "in-progress";
    protected static final String STATUS_COMPLETE = "complete";
    protected static final String STATUS_FAILED = "failed";

    protected static final String EXTRA_NB_CHANNELS_VALUE = "nb_channels";
    protected static final String EXTRA_PROGRESS_VALUE = "progress";
    protected static final String EXTRA_STATUS_VALUE = "status";

    private static final long mProgressTimeout = TimeUnit.SECONDS.toMillis(20);

    private final MwProxyProximus mwHandler = Components.get(MwProxyProximus.class);
    private final IpTvParametersTch mScanParams;
    private final VQEConfigBuilder mVqeConfig;
    private final Handler mHandler;
    private final Context mContext;

    private volatile boolean mVQeSetupSuccess = false;
    private volatile boolean mChannelScanSuccess = false;
    private volatile int mScannedChannels = 0;
    private volatile int mProgress = 0;

    private List<ScanFinishedListener> mListenerList;

    protected TchScanTask(Context context, Handler handler, IpTvParametersTch scanParams, VQEConfigBuilder vqeConfig) {
        mContext = context;
        mHandler = handler;
        mScanParams = scanParams;
        mVqeConfig = vqeConfig;
        mListenerList = new ArrayList<>();
    }

    public TchScanTask start() throws Exception {
        Intent tifScanIntent = mVqeConfig.buildTifScanIntent(mScanParams);
        mBroadcastReceiver.registerReceiver(mContext);
        mwHandler.configureVqeParameters(mVqeConfig);
        mVQeSetupSuccess = true;

        postponeTimeout();
        mContext.sendBroadcast(tifScanIntent, MwProxyProximus.PACKAGE_DTVINPUT);
        return this;
    }

    public void addListener(ScanFinishedListener listener) {
        mListenerList.add(listener);
    }

    protected void postponeTimeout() {
        if (mHandler == null) {
            return;
        }
        mHandler.removeCallbacks(mFutureTask);
        mHandler.postDelayed(mFutureTask, mProgressTimeout);
    }

    @Override
    public Boolean result() {
        return mVQeSetupSuccess && mChannelScanSuccess;
    }

    @Override
    public void cancel(boolean mayInterruptIfRunning) {
        if (mHandler != null) {
            mHandler.removeCallbacks(mFutureTask);
        }
        mChannelScanSuccess = false;
        mBroadcastReceiver.unregisterReceiver(mContext);
        notifyListeners(mListenerList, false);
        mListenerList = null;
        super.cancel(mayInterruptIfRunning);
    }

    protected void finished(Intent intent, boolean success) {
        Log.d(TAG, "finished(success: " + success + "): " + intent.getAction());
        if (mHandler != null) {
            mHandler.removeCallbacks(mFutureTask);
        }

        if (SCAN_STATUS.equals(intent.getAction())) {
            notifyListeners(mListenerList, success);
            mChannelScanSuccess = success;
        }

        mBroadcastReceiver.unregisterReceiver(mContext);
        mListenerList = null;
        super.finish();
    }

    private void notifyListeners(List<ScanFinishedListener> listeners, boolean success) {
        mHandler.post(() -> {
            for (ScanFinishedListener listener : ArrayUtils.notNull(listeners)) {
                listener.onScanFinished(success);
            }
        });
    }

    interface ScanFinishedListener {
        void onScanFinished(boolean success);
    }

    private final BaseBroadcastReceiver mBroadcastReceiver = new BaseBroadcastReceiver() {
        @Override
        public IntentFilter getIntentFilter() {
            return new IntentFilter(SCAN_STATUS);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive", intent);
            String status = STATUS_FAILED;
            if (intent.getExtras() == null) {
                Log.d(TAG, "Intent extras were null, nothing to do here.");
                return;
            }

            if (intent.getExtras().containsKey(EXTRA_STATUS_VALUE)) {
                status = intent.getStringExtra(EXTRA_STATUS_VALUE);
            }

            if (intent.getExtras().containsKey(EXTRA_PROGRESS_VALUE)) {
                mProgress = intent.getIntExtra(EXTRA_PROGRESS_VALUE, 0);
            }

            if (intent.getExtras().containsKey(EXTRA_NB_CHANNELS_VALUE)) {
                mScannedChannels = intent.getIntExtra(EXTRA_NB_CHANNELS_VALUE, 0);
            }

            switch (status) {
                case STATUS_IN_PROGRESS:
                    postponeTimeout();
                    break;

                case STATUS_COMPLETE:
                    finished(intent, true);
                    break;

                case STATUS_FAILED:
                    finished(intent, false);
                    break;

                default:
                    Log.d(TAG, "unrecognized status: " + status);
                    break;
            }
        }
    };
}
