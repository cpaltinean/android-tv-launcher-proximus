package tv.threess.threeready.data.proximus.generic.model;


import tv.threess.threeready.data.generic.model.BaseResponse;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Generic model of the backend errors.
 * E.g invalid mac error.
 *
 * @author Barabas Attila
 * @since 2020.03.24
 */
@XmlRootElement(name = "ERROR")
public class BTAResponseErrorProximus implements BaseResponse.Error {

    @XmlElement(name = "ErrorCode")
    String mErrorCode;

    @XmlElement(name = "ErrorDescription")
    String mErrorDescription;

    public String getErrorCode() {
        return mErrorCode;
    }

    public String getErrorDescription() {
        return mErrorDescription;
    }
}
