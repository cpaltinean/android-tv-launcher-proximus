package tv.threess.threeready.data.proximus.account.model.account;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class represents the unique identifier of the device.
 *
 * @author Barabas Attila
 * @since 2020.04.06
 */
@XmlRootElement(name = "DeviceRef")
public class DeviceReferenceProximus implements Serializable {

    @XmlAttribute(name = "ref")
    private String mRef;

    /**
     * @return registered MAC address of the device.
     */
    public String getRef() {
        return mRef;
    }
}
