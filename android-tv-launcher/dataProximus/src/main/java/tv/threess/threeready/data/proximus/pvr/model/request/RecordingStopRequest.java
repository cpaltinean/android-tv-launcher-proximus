package tv.threess.threeready.data.proximus.pvr.model.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Retrofit request class for stopping an ongoing recording.
 *
 * Created by Noemi Dali on 23.04.2020.
 */
@XmlRootElement(name = "BTA")
public class RecordingStopRequest implements Serializable {

    @XmlElement(name = "StopRecordingDoc")
    private final StopRecordingDoc stopRecordingDoc;

    public RecordingStopRequest(String recordId) {
        stopRecordingDoc = new StopRecordingDoc();
        stopRecordingDoc.mUserRecordId = recordId;
    }

    @XmlRootElement(name = "StopRecordingDoc")
    private static class StopRecordingDoc implements Serializable {
        @XmlAttribute(name = "userRecordID")
        private String mUserRecordId;

        @XmlElement(name = "UserStopTimeMarker")
        private long stopTime;


    }
}
