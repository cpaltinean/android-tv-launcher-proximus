package tv.threess.threeready.data.proximus.vod.observable;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.api.vod.wrapper.VodRentOffers;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.vod.VodContract;
import tv.threess.threeready.data.vod.VodProxy;

/**
 * Cold observable that emits the last played VOD episode in a VOD series
 *
 * @author Denisa Trif
 * @since 20/02/2019
 */
public class LastPlayedVodObservable extends BaseVodObservable<VodRentOffers> {
    private static final String TAG = Log.tag(LastPlayedVodObservable.class);
    private final IVodSeries mSeries;

    private String mPreviousEmittedItemId;

    public LastPlayedVodObservable(Context context, IVodSeries series) {
        super(context);
        mSeries = series;
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<VodRentOffers> emitter) throws Exception {
        publishLastPlayedVod(emitter);
    }

    @Override
    public void subscribe(@NonNull ObservableEmitter<VodRentOffers> emitter) throws Exception {
        super.subscribe(emitter);
        registerObserver(ConfigContract.Bookmarks.CONTENT_URI);
        registerObserver(VodContract.RentalExpiration.CONTENT_URI);
        publishLastPlayedVod(emitter);
    }

    private void publishLastPlayedVod(ObservableEmitter<VodRentOffers> emitter) throws IOException {
        IBaseVodItem vod = getLastPlayedVodInSeries(mSeries);
        Log.d(TAG, "Last played VOD episode : " + vod);

        if (isNoNeedEmitUpdate(vod)) {
            return;
        }

        VodRentOffers offers;

        if (vod == null) {
            // offer for the all of the episode
            // chose the first which is subscribed, rented or free
            offers = chooseFirstEpisodeFromSeries();
        } else {
            // offer for the bookmark vod
            offers = getOffersForVod(vod);
        }
        if (offers != null) {
            mPreviousEmittedItemId = offers.getVod().getId();
            emitter.onNext(offers);
        }
    }

    /**
     * Gets the offer for the bookmark's vod.
     *
     * @param vodItem The vod item to get offers for.
     * @return A Pair with the bookmark's vod and his offer.
     */
    private VodRentOffers getOffersForVod(IBaseVodItem vodItem) throws IOException {
        IVodItem vod = getCompleteVod(vodItem);
        Map<String, IRentOffer> offerMap = new HashMap<>();
        int creditCount = mVodProxy.getAvailableCreditCount();

        for (IVodVariant variant : vod.getVariantList()) {
            if (variant.isRentable() || variant.isSubscribed()) {
                IRentOffer offer = mVodProxy.getRentOffer(variant);
                // Check if currently available.
                if (offer != null) {
                    offerMap.put(variant.getId(), offer);
                }
            }
        }

        return new VodRentOffers(vod, creditCount, offerMap);
    }

    /**
     * Gets the first episode which is subscribed/rented/free with the offers.
     */
    private VodRentOffers chooseFirstEpisodeFromSeries() throws IOException {
        if (mSeries.getFirstEpisode() != null) {
            return getOffersForVod(mSeries.getFirstEpisode());
        }
        return null;
    }

    private boolean isNoNeedEmitUpdate(IBaseVodItem vod) {
        return vod != null && Objects.equals(vod.getId(), mPreviousEmittedItemId);
    }
}
