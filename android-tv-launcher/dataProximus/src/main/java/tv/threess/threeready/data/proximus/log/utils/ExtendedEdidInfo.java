package tv.threess.threeready.data.proximus.log.utils;

import androidx.annotation.NonNull;
import tv.threess.threeready.api.middleware.model.BitStream;

public class ExtendedEdidInfo {

    private final String extensionTag;          // Extension tag
    private final String revisionNr;            // Revision number
    private final boolean supportUnderscan;     // 1 if display supports underscan, 0 if not
    private final boolean supportBasicAudio;    // 1 if display supports basic audio, 0 if not
    private final boolean supportYCvCr444;      // 1 if display supports YCbCr 4:4:4, 0 if not
    private final boolean supportYCvCr422;      // 1 if display supports YCbCr 4:2:2, 0 if not
    private final int totalNrOfNativeFormats;   // total number of native formats in the DTDs included in this block
    private int resIndexValue;                  // index value to a table of standard resolutions/timings from CEA/EIA-861

    /**
     * Table of standard resolutions/timings from CEA/EIA-861E from 1 to 127, then from 193 to 219
     * note: indexes between 128 - 192 are reserved.
     *
     * table from: https://en.wikipedia.org/wiki/Extended_Display_Identification_Data
     */
    private final String[] detailedTimingInfoTable = {
            null,
            "640x480@60Hz",
            "720x480@60Hz",
            "720x480@60Hz",
            "1280x720@60Hz",
            "1920x540@60Hz",
            "1440x240@60Hz",
            "1440x240@60Hz",
            "1440x240@60Hz",
            "1440x240@60Hz",
            "2880x240@60Hz",
            "2880x240@60Hz",
            "2880x240@60Hz",
            "2880x240@60Hz",
            "1440x480@60Hz",
            "1440x480@60Hz",
            "1920x1080@60Hz",
            "720x576@50Hz",
            "720x576@50Hz",
            "1280x720@50Hz",
            "1920x540@50Hz",
            "1440x288@50Hz",
            "1440x288@50Hz",
            "1440x288@50Hz",
            "1440x288@50Hz",
            "2880x288@50Hz",
            "2880x288@50Hz",
            "2880x288@50Hz",
            "2880x288@50Hz",
            "1440x576@50Hz",
            "1440x576@50Hz",
            "1920x1080@50Hz",
            "1920x1080@Low",
            "1920x1080@Low",
            "1920x1080@Low",
            "2880x240@60Hz",
            "2880x240@60Hz",
            "2880x576@50Hz",
            "2880x576@50Hz",
            "1920x540@50Hz",
            "1920x540@100Hz",
            "1280x720@100Hz",
            "720x576@100Hz",
            "720x576@100Hz",
            "1440x576@100Hz",
            "1440x576@100Hz",
            "1920x540@120Hz",
            "1280x720@120Hz",
            "720x576@120Hz",
            "720x576@120Hz",
            "1440x576@120Hz",
            "1440x576@120Hz",
            "720x576@200Hz",
            "720x576@200Hz",
            "1440x288@200Hz",
            "1440x288@200Hz",
            "720x480@240Hz",
            "720x480@240Hz",
            "1440x240@240Hz",
            "1440x240@240Hz",
            "1280x720@Low",
            "1280x720@Low",
            "1280x720@Low",
            "1920x1080@120Hz",
            "1920x1080@100Hz",
            "1280x720@Low",
            "1280x720@Low",
            "1280x720@Low",
            "1280x720@50Hz",
            "1650x750@60Hz",
            "1280x720@100Hz",
            "1280x720@120Hz",
            "1920x1080@Low",
            "1920x1080@Low",
            "1920x1080@Low",
            "1920x1080@50Hz",
            "1920x1080@60Hz",
            "1920x1080@100Hz",
            "1920x1080@120Hz",
            "1680x720@Low",
            "1680x720@Low",
            "1680x720@Low",
            "1680x720@50Hz",
            "1680x720@60Hz",
            "1680x720@100Hz",
            "1680x720@120Hz",
            "2560x1080@Low",
            "2560x1080@Low",
            "2560x1080@Low",
            "2560x1080@50Hz",
            "2560x1080@60Hz",
            "2560x1080@100Hz",
            "2560x1080@120Hz",
            "3840x2160@Low",
            "3840x2160@Low",
            "3840x2160@Low",
            "3840x2160@50Hz",
            "3840x2160@60Hz",
            "4096x2160@Low",
            "4096x2160@Low",
            "4096x2160@Low",
            "4096x2160@50Hz",
            "4096x2160@60Hz",
            "3840x2160@Low",
            "3840x2160@Low",
            "3840x2160@Low",
            "3840x2160@50Hz",
            "3840x2160@60Hz",
            "1280x720@Low",
            "1280x720@Low",
            "1680x720@Low",
            "1920x1080@Low",
            "1920x1080@Low",
            "2560x1080@Low",
            "3840x2160@Low",
            "4096x2160@Low",
            "3840x2160@Low",
            "3840x2160@100Hz",
            "3840x2160@120Hz",
            "3840x2160@100Hz",
            "3840x2160@120Hz",
            "5120x2160@Low",
            "5120x2160@Low",
            "5120x2160@Low",
            "5120x2160@Low",
            "5120x2160@50Hz",
            "5120x2160@60Hz",
            "5120x2160@100Hz",
            null, // these indexes are reserved
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            "5120x2160p@120Hz",
            "7680x4320p@Low",
            "7680x4320p@Low",
            "7680x4320p@Low",
            "7680x4320p@Low",
            "7680x4320p@50Hz",
            "7680x4320p@60Hz",
            "7680x4320p@100Hz",
            "7680x4320p@120Hz",
            "7680x4320p@Low",
            "7680x4320p@Low",
            "7680x4320p@Low",
            "7680x4320p@Low",
            "7680x4320p@50Hz",
            "7680x4320p@60Hz",
            "7680x4320p@100Hz",
            "7680x4320p@120Hz",
            "10240x4320p@Low",
            "10240x4320p@Low",
            "10240x4320p@Low",
            "10240x4320p@Low",
            "10240x4320p@50Hz",
            "10240x4320p@60Hz",
            "10240x4320p@100Hz",
            "10240x4320p@120Hz",
            "4096x2160p@100Hz",
            "4096x2160p@120Hz",
    };

    public ExtendedEdidInfo (byte[] bytes) {
        if (bytes.length != 128) {
            throw new IllegalArgumentException("EDID must be a 128 byte long record");
        }

        BitStream stream = new BitStream(bytes);
        this.extensionTag = stream.readHex(1);
        this.revisionNr = stream.readHex(1);

        int dtdBitOffset = stream.readI32(8) * 8; // if this = 00 -> no DTDs

        this.supportUnderscan = stream.readU32(1) != 0;
        this.supportBasicAudio = stream.readU32(1) != 0;
        this.supportYCvCr444 = stream.readU32(1) != 0;
        this.supportYCvCr422 = stream.readU32(1) != 0;
        this.totalNrOfNativeFormats = stream.readU32(4);

        while (stream.offset() < dtdBitOffset) {
            int dtdType = stream.readU32(3);
            int dtdLength = stream.readU32(5);
            if (dtdType != 2) {
                stream.skipBits(dtdLength * 8);
                continue;
            }

            for (int i = 0; i < dtdLength; ++i) {
                int nativeRes = stream.readU32(1);
                int resIndex = stream.readU32(7);

                if (nativeRes == 1) {
                    resIndexValue = resIndex;
                }
            }
        }
    }

    public String getExtensionTag() {
        return extensionTag;
    }

    public String getRevisionNr() {
        return revisionNr;
    }

    public boolean isSupportUnderscan() {
        return supportUnderscan;
    }

    public boolean isSupportBasicAudio() {
        return supportBasicAudio;
    }

    public boolean isSupportYCvCr444() {
        return supportYCvCr444;
    }

    public boolean isSupportYCvCr422() {
        return supportYCvCr422;
    }

    public int getTotalNrOfNativeFormats() {
        return totalNrOfNativeFormats;
    }

    public int getResIndexValue() {
        return resIndexValue;
    }

    public String getNativeResolution() {
        if (resIndexValue < detailedTimingInfoTable.length) {
            return detailedTimingInfoTable[resIndexValue];
        }

        return null;
    }

    @NonNull
    @Override
    public String toString() {
        return "ExtendedEdidInfo{" +
                "extensionTag='" + extensionTag + '\'' +
                ", revisionNr='" + revisionNr + '\'' +
                ", supportUnderscan=" + supportUnderscan +
                ", supportBasicAudio=" + supportBasicAudio +
                ", supportYCvCr444=" + supportYCvCr444 +
                ", supportYCvCr422=" + supportYCvCr422 +
                ", totalNrOfNativeFormats=" + totalNrOfNativeFormats +
                ", resIndexValue=" + resIndexValue +
                '}';
    }
}