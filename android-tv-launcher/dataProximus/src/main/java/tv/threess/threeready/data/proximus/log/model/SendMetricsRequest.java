package tv.threess.threeready.data.proximus.log.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Wrapper class used for sending metrics.
 * Contains a json property with name: metrics.
 *
 * @author Zsolt Bokor_
 * @since 2020.06.17
 */
public class SendMetricsRequest {
    @SerializedName("metrics")
    private final List<LogDetailsProximus> mMetrics = new ArrayList<>();

    public SendMetricsRequest(LogDetailsProximus metric) {
        mMetrics.add(metric);
    }

    public SendMetricsRequest(List<LogDetailsProximus> metrics) {
        mMetrics.addAll(metrics);
    }
}
