/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.home;

import android.app.Application;
import android.util.Pair;

import androidx.annotation.NonNull;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.home.HomeRepository;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.netflix.INetflixGroup;
import tv.threess.threeready.api.netflix.model.NetflixDETCategory;
import tv.threess.threeready.api.search.SearchRepository;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.ReplayChannelPageItem;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.generic.observable.WaitForUpdateObservable;
import tv.threess.threeready.data.home.observable.ContinueWatchingCategoryContentObservable;
import tv.threess.threeready.data.home.observable.NowNextObservable;
import tv.threess.threeready.data.home.observable.NowOnTvObservable;
import tv.threess.threeready.data.home.observable.TvChannelObservable;
import tv.threess.threeready.data.netflix.observable.NetflixDETContentObservable;
import tv.threess.threeready.data.proximus.home.model.editorial.BannerEditorialProximus;
import tv.threess.threeready.data.proximus.home.model.editorial.BannerFeedbackRequest.FeedbackType;
import tv.threess.threeready.data.proximus.home.model.module.NetflixSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.page.CategoryPageConfig;
import tv.threess.threeready.data.proximus.home.model.swimalane.BannerProximus;
import tv.threess.threeready.data.proximus.home.observable.CategoryContentObservable;
import tv.threess.threeready.data.proximus.home.observable.ProgramSearchResultObservable;
import tv.threess.threeready.data.proximus.home.observable.ReplayChannelPageItemObservable;
import tv.threess.threeready.data.proximus.home.observable.WatchlistContentObservable;
import tv.threess.threeready.data.proximus.vod.VodCategoryCacheProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategoryProximus;

/**
 * Implementation of the Proximus specific home handler methods.
 *
 * @author Barabas Attila
 * @see HomeRepository
 * @since 2018.10.11
 */
public class HomeRepositoryProximus implements HomeRepository, Component {
    private static final String TAG = Log.tag(HomeRepositoryProximus.class);

    private final HomeProxyProximus mHomeProxy = Components.get(HomeProxyProximus.class);

    private final VodCategoryCacheProximus mCategoryCache = Components.get(VodCategoryCacheProximus.class);
    private final SearchRepository mSearchRepository = Components.get(SearchRepository.class);

    private final Application mApp;

    public HomeRepositoryProximus(Application application) {
        mApp = application;
    }

    @Override
    public Observable<ModuleData<IBaseContentItem>> getCategoryContent(final ModuleConfig moduleConfig, final int start, final int count,
                                                                       List<DataSourceSelector> dataSourceSelectors) {
        return Observable.create(new CategoryContentObservable(mApp, moduleConfig, start, count, dataSourceSelectors));
    }

    @Override
    public Observable<ModuleData<IBaseContentItem>> getContinueWatching(ModuleConfig moduleConfig, int start, int count) {
        return Observable.create(new ContinueWatchingCategoryContentObservable(mApp, moduleConfig, start, count));
    }

    @Override
    public Observable<ModuleData<IBaseContentItem>> getWatchlist(ModuleConfig moduleConfig, int start, int count) {
        return Observable.create(new WatchlistContentObservable(mApp, moduleConfig, start, count));
    }

    public Observable<ModuleData<INetflixGroup>> getNetflixGroups(ModuleConfig moduleConfig, int count) {
        return Observable.create(new NetflixDETContentObservable(mApp, moduleConfig,
                getNetflixCategory(moduleConfig), getNetflixInteractionId(moduleConfig), count));
    }

    @Override
    public Observable<ModuleData<TvChannel>> getChannels(ModuleConfig moduleConfig, int count) {
        return Observable.create(new TvChannelObservable(mApp, moduleConfig, count));
    }

    @Override
    public Observable<ModuleData<ReplayChannelPageItem>> getReplayChannelPageItems(ModuleConfig moduleConfig, int start, int count,
                                                                                   List<DataSourceSelector> dataSourceSelectors) {
        return Observable.create(new ReplayChannelPageItemObservable(mApp, moduleConfig, start, count, dataSourceSelectors));
    }

    @Override
    public Observable<ModuleData<Pair<IBroadcast, IBroadcast>>> getNowNext(final ModuleConfig moduleConfig, final int count, List<DataSourceSelector> dataSourceSelectors) {
        return Observable.create(new NowNextObservable(mApp, moduleConfig, count, dataSourceSelectors));
    }

    @Override
    public Observable<ModuleData<IBroadcast>> getNowOnTV(final ModuleConfig moduleConfig, final int count) {
        return Observable.create(new NowOnTvObservable(mApp, moduleConfig, count));
    }

    @Override
    public Observable<ModuleData<TvChannel>> getChannelSearchResults(ModuleConfig moduleConfig, int start, int count) {
        return mSearchRepository.channelSearch(moduleConfig, start, count);
    }

    @Override
    public Observable<ModuleData<IBaseContentItem>> getProgramSearchResults(ModuleConfig moduleConfig, int start, int count) {
        return Observable.create(new ProgramSearchResultObservable(mApp, moduleConfig, start, count));
    }

    @Override
    public Single<PageConfig> getSubCategoryPage(String subcategoryId) {
        return Single.create(emitter -> {
            VodCategoryProximus category = mCategoryCache.getCategory(subcategoryId);
            if (category == null) {
                throw new IllegalStateException("No category with id :  " + subcategoryId);
            }
            emitter.onSuccess(new CategoryPageConfig(category));
        });
    }

    @Override
    public Completable editorialViewedFeedback(EditorialItem editorialItem) {
        return Completable.fromAction(
                () -> {
                    if (!editorialItem.hasViewedFeedback()) {
                        // No viewing feedback needed.
                        return;
                    }

                    if (editorialItem instanceof BannerEditorialProximus) {
                        BannerProximus banner = ((BannerEditorialProximus) editorialItem).getBanner();
                        mHomeProxy.bannerFeedback(FeedbackType.Viewed, banner.getViewedFeedbackCode());
                    }
                });
    }

    @Override
    public Completable editorialClickedFeedback(EditorialItem editorialItem) {
        return Completable.fromAction(
                () -> {
                    if (!editorialItem.hasClickedFeedback()) {
                        // No clicked feedback needed.
                        return;
                    }

                    if (editorialItem instanceof BannerEditorialProximus) {
                        BannerProximus banner = ((BannerEditorialProximus) editorialItem).getBanner();
                        mHomeProxy.bannerFeedback(FeedbackType.Clicked, banner.getClickedFeedbackCode());
                    }
                });
    }

    @Override
    public Completable clearCacheByModuleConfigId(String moduleId) {
        return Completable.fromAction(() -> mHomeProxy.clearCacheByModuleConfigId(moduleId))
                .doOnComplete(() -> mApp.getContentResolver().notifyChange(ConfigContract.buildUriForSubscriptionModuleId(moduleId), null))
                .doOnError(throwable -> Log.e(TAG, "Failed to delete http cache for the given config model with id: " + moduleId, throwable));
    }

    @NonNull
    private NetflixDETCategory getNetflixCategory(ModuleConfig moduleConfig) {
        if (moduleConfig instanceof NetflixSwimlaneModuleProximus) {
            return NetflixDETCategory.findByName(((NetflixSwimlaneModuleProximus) moduleConfig).getNetflixCategory());
        }

        if (moduleConfig.getDataSource() != null && moduleConfig.getDataSource().getParams() != null) {
            return NetflixDETCategory.findByName(moduleConfig.getDataSource().getParams()
                    .getFilter(ModuleFilterOption.Netflix.NetflixCategory.NAME));
        }

        return NetflixDETCategory.DEFAULT;
    }

    @NonNull
    private String getNetflixInteractionId(ModuleConfig moduleConfig) {
        if (moduleConfig instanceof NetflixSwimlaneModuleProximus) {
            return ((NetflixSwimlaneModuleProximus) moduleConfig).getNetflixInteractionId();
        }

        if (moduleConfig.getDataSource() != null && moduleConfig.getDataSource().getParams() != null) {
            String iid = moduleConfig.getDataSource().getParams()
                    .getFilter(ModuleFilterOption.Netflix.NetflixInteractionId.NAME);
            return StringUtils.notNull(iid);
        }

        return "";
    }

    public Completable waitForConfig() {
        return Completable.fromObservable(Observable.create(
                new WaitForUpdateObservable(mApp, HomeServiceRepositoryProximus.configUpdater)));
    }

    public Completable waitForTranslations() {
        return Completable.fromObservable(Observable.create(
                new WaitForUpdateObservable(mApp, HomeServiceRepositoryProximus.translationUpdater)));
    }
}
