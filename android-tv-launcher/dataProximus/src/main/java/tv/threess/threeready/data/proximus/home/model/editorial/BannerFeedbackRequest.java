package tv.threess.threeready.data.proximus.home.model.editorial;

import com.google.gson.annotations.SerializedName;

/**
 * Banner viewed and clicked feedback payload.
 *
 * @author Barabas Attila
 * @since 11/19/21
 */
public class BannerFeedbackRequest {

    @SerializedName("FeedbackType")
    private final String mFeedbackType;

    @SerializedName("AccountNumber")
    private final String mAccountNumber;

    @SerializedName("BannerFeedbackCode")
    private final String mBannerFeedbackCode;

    public BannerFeedbackRequest(FeedbackType feedbackType,
                                 String accountNumber, String bannerFeedbackCode) {
        mFeedbackType = feedbackType.mValue;
        mAccountNumber = accountNumber;
        mBannerFeedbackCode = bannerFeedbackCode;
    }

    public enum FeedbackType {
        Viewed("viewed"),
        Clicked("clicked");

        private final String mValue;

        FeedbackType(String value) {
            mValue = value;
        }
    }
}
