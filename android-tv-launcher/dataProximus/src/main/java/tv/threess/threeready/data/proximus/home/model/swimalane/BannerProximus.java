package tv.threess.threeready.data.proximus.home.model.swimalane;

import android.text.TextUtils;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.log.Log;

/**
 * Marketing banner of a swimlane.
 *
 * @author Barabas Attila
 * @since 7/1/21
 */
@XmlRootElement(name = "Banner")
public class BannerProximus {
    private static final String TAG = Log.tag(BannerProximus.class);

    private static final int SMALL_BANNER_WIDTH = 392;

    @XmlElement(name = "BanneradActionLink")
    private BannerActionLink mBannerActionLink;

    @XmlElement(name = "Text")
    private String mText;

    @XmlElement(name = "ImageURL")
    private String mImage;

    @XmlElement(name = "ImageSize")
    private String mImageSize;

    @XmlElement(name = "BannerViewedFeedbackCode")
    private String mViewedFeedbackCode;

    @XmlElement(name = "BannerClickedFeedbackCode")
    private String mClickedFeedbackCode;

    private Integer mWidth;

    private final IImageSource mImageSource = new IImageSource() {
        @Override
        public String getUrl() {
            return mImage;
        }

        @Override
        public Type getType() {
            return Type.BANNER;
        }
    };

    public String getText() {
        return mText;
    }

    public IImageSource getImage() {
        return mImageSource;
    }

    public BannerActionLink getBannerActionLink() {
        return mBannerActionLink;
    }

    public String getViewedFeedbackCode() {
        return mViewedFeedbackCode;
    }

    public String getClickedFeedbackCode() {
        return mClickedFeedbackCode;
    }

    private Integer getWidth() {
        if (mWidth == null
                && !TextUtils.isEmpty(mImageSize)) {
            String[] sizes = mImageSize.split("x");
            if (sizes.length > 0) {
                mWidth = parseDimenSafe(sizes[0]);
            }
        }
        return mWidth;
    }

    private Integer parseDimenSafe(String dimen) {
        try {
            return Integer.valueOf(dimen);
        } catch (Exception e) {
            Log.e(TAG ,"Couldn't parse dimension.", e);
        }
        return null;
    }

    public Type getType() {
        Integer width = getWidth();
        if (width != null && width > SMALL_BANNER_WIDTH) {
            return Type.Wide;
        }
        return Type.Small;
    }

    @XmlRootElement(name = "BanneradActionLink")
    public static class BannerActionLink {
        @XmlElement(name = "HTMLPage")
        private String mHtmlPage;

        @XmlElement(name = "LinkVoD")
        private LinkVod mLinkVod;

        @XmlElement(name = "VodItem")
        private VodItem mVodItem;

        @XmlElement(name = "VodCategory")
        private String mVodCategory;

        @XmlElement(name = "AppItem")
        private AppItem mAppItem;

        public String getHtmlPage() {
            return mHtmlPage;
        }

        public LinkVod getLinkVod() {
            return mLinkVod;
        }

        public VodItem getVodItem() {
            return mVodItem;
        }

        public String getVodCategory() {
            return mVodCategory;
        }

        public AppItem getAppItem() {
            return mAppItem;
        }
    }

    @XmlRootElement(name = "LinkVoD")
    public static class LinkVod {

        @XmlAttribute(name = "ref")
        private String mVodId;

        public String getVodId() {
            return mVodId;
        }
    }

    @XmlRootElement(name = "VodItem")
    public static class VodItem {

        @XmlAttribute(name = "ref")
        private String mVodId;

        public String getVodId() {
            return mVodId;
        }
    }

    @XmlRootElement(name = "AppItem")
    public static class AppItem  {
        @XmlAttribute(name = "ref")
        private String mPackageId;

        public String getPackageId() {
            return mPackageId;
        }
    }

    public enum Type {
        Small,
        Wide
    }
}
