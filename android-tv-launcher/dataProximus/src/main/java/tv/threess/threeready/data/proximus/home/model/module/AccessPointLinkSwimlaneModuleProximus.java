package tv.threess.threeready.data.proximus.home.model.module;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleDataType;
import tv.threess.threeready.api.home.model.module.ModuleType;
import tv.threess.threeready.data.proximus.home.model.AccessPointProximus;
import tv.threess.threeready.data.proximus.home.model.editorial.AccessPointEditorialItemProximus;
import tv.threess.threeready.data.proximus.home.model.editorial.StaticTileAccessPointEditorialItemProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneHubResponseProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Config for a module which contains access point page links.
 *
 * @author Barabas Attila
 * @since 5/26/21
 */
public class AccessPointLinkSwimlaneModuleProximus implements ModuleConfig {

    private final SwimLaneProximus mSwimLane;
    private final List<EditorialItem> mEditorialItems = new ArrayList<>();

    public AccessPointLinkSwimlaneModuleProximus(SwimLaneProximus swimLane, SwimLaneHubResponseProximus hub) {
        mSwimLane = swimLane;

        // Add access points from swimlane params.
        List<String> accessPointIds = swimLane.getAccessPointIds();
        for (String accessPointId : accessPointIds) {
            AccessPointProximus accessPoint = hub.getAccessPoint(accessPointId);
            if (accessPoint != null) {
                mEditorialItems.add(new AccessPointEditorialItemProximus(accessPoint));
            }
        }

        // Add static tiles.
        List<SwimLaneProximus.StaticTile> staticTiles = swimLane.getStaticTiles();
        if (staticTiles == null) {
            return;
        }

        staticTiles.sort(Comparator.comparingInt(SwimLaneProximus.StaticTile::getSortOrder));
        for (SwimLaneProximus.StaticTile staticTile : staticTiles) {
            accessPointIds = staticTile.getAccessPointIds();
            for (String accessPointId : accessPointIds) {
                AccessPointProximus accessPoint = hub.getAccessPoint(accessPointId);
                if (accessPoint != null) {
                    mEditorialItems.add(new StaticTileAccessPointEditorialItemProximus(staticTile, accessPoint));
                }
            }
        }
    }

    @Override
    public String getId() {
        return mSwimLane.getId();
    }

    @Override
    public String getReportName() {
        return mSwimLane.getName();
    }

    @Override
    public String getReportReference() {
        return mSwimLane.getId();
    }

    @Nullable
    @Override
    public String getTitle() {
        return mSwimLane.getDisplayNames().getLocalizedNameOrFirst();
    }

    @Override
    public ModuleType getType() {
        return ModuleType.STRIPE;
    }

    @Override
    public ModuleDataType getDataType() {
        return ModuleDataType.EDITORIAL;
    }

    @Override
    public ModuleCardVariant getCardVariant() {
        return ModuleCardVariant.A1;
    }

    @Override
    public List<EditorialItem> getEditorialItems() {
        return mEditorialItems;
    }
}
