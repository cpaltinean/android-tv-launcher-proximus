package tv.threess.threeready.data.proximus.account.model.server;

/**
 * Enum class for possible server types in the application.
 *
 * Created by Noemi Dali on 09.04.2020.
 */
public enum ServerType {
    POSTER_SERVER("PS"),
    BANNER_AD("BNRAD"),
    BANNER_FEEDBACK("TVIR"),
    UDC,
    TAP,
    TAS,
    TACDN;

    private final String mType;

    ServerType() {
        mType = name();
    }

    ServerType(String type) {
        mType = type;
    }

    public String getType() {
        return mType;
    }
}
