package tv.threess.threeready.data.proximus.home;

import android.app.Application;
import android.text.TextUtils;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.model.CacheSettings;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.Config;
import tv.threess.threeready.api.generic.ComponentsInitializer;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.receivers.StandByStateChangeReceiver;
import tv.threess.threeready.api.home.model.generic.ContentConfig;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceParams;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.home.BaseHomeServiceRepository;
import tv.threess.threeready.data.home.HomeService;
import tv.threess.threeready.data.proximus.generic.RetrofitServicesProximus;
import tv.threess.threeready.data.proximus.home.model.AccessPointProximus;
import tv.threess.threeready.data.proximus.home.model.module.SwimlaneModuleConfigProximus;
import tv.threess.threeready.data.proximus.home.model.page.AccessPointPageConfigProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneHubResponseProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;
import tv.threess.threeready.data.utils.CacheUpdater;
import tv.threess.threeready.data.utils.PeriodicCacheUpdater;

/**
 * Proximus implementation of the home service repository.
 *
 * @author Barabas Attila
 * @since 4/20/22
 */
public class HomeServiceRepositoryProximus extends BaseHomeServiceRepository {

    private static final String TAG = Log.tag(HomeServiceRepositoryProximus.class);

    private final ComponentsInitializer mComponentsInitializer;

    private static final String HUB_DEFINITION_FILE = "getNte2Hub.xml";

    private final XmlMapper mXmlMapper = Components.get(RetrofitServicesProximus.class).getXmlMapper();
    private final HomeProxyProximus mHomeProxy = Components.get(HomeProxyProximus.class);

    public HomeServiceRepositoryProximus(Application application, ComponentsInitializer componentsInitializer) {
        super(application);
        mComponentsInitializer = componentsInitializer;


        // Detect standby state changes.
        Components.get(StandByStateChangeReceiver.class)
                .addStateChangedListener(mStandByStateChangeListener);
    }

    @Override
    public void updateCachedTranslations() throws Exception {
        translationUpdater.updateIfExpired(() -> {
            try (InputStream steam = mHomeProxy.getRemoteTranslations(
                    getTranslationsLanguage(), mMwProxy.getAppVersion())) {
                updateCachedTranslations(steam);
            } catch (Exception e) {
               Log.e(TAG, "Failed to update translations.", e);
            }
        });
    }

    @Override
    public void updateClientConfig() throws Exception {
        configUpdater.updateIfExpired(() -> {
            updateRemote3ReadyConfig();
            updateHubDefinition();

            Config config = loadConfig(mApp,
                    Settings.authenticated.get(false),
                    Settings.forceLocalConfig.get(false));

            try {
                // Load Hub debilitation
                SwimLaneHubResponseProximus hub = mXmlMapper.readValue(
                        new File(mApp.getFilesDir(), HUB_DEFINITION_FILE), SwimLaneHubResponseProximus.class);

                boolean extendedPageConfig = Settings.extendedPageConfig.get(
                        config.getFeatureControl().extendedPageConfig());

                if (extendedPageConfig) {
                    extendPageConfiguration(config.getContentConfig(), hub);
                } else {
                    extendSwimlaneConfig(config.getContentConfig(), hub);
                }
            } catch (Exception e) {
                Log.e(TAG, "Failed to extend client page config with hub definition.", e);
            }

            mComponentsInitializer.initConfigComponents(config);
        });
    }

    /**
     * Extend the configured swimlane modules with action tiles.
     *
     * @param contentConfig The original local config.
     * @param hub           The hub definition which contains the swimlanes.
     */
    private void extendSwimlaneConfig(ContentConfig contentConfig, SwimLaneHubResponseProximus hub) {
        for (PageConfig pageConfig : contentConfig.getPageConfigs()) {
            List<ModuleConfig> moduleConfigs = pageConfig.getModules();
            if (moduleConfigs == null) {
                continue;
            }

            for (ModuleConfig moduleConfig : moduleConfigs) {
                ModuleDataSource dataSource = moduleConfig.getDataSource();
                if (dataSource == null || dataSource.getParams() == null) {
                    continue;
                }

                ModuleDataSourceParams params = dataSource.getParams();
                String swimlaneId = params.getFilter(ModuleFilterOption.Mixed.SwimLaneId.NAME);
                if (TextUtils.isEmpty(swimlaneId)) {
                    continue;
                }

                SwimLaneProximus swimlane = hub.getSwimlane(swimlaneId);
                if (swimlane == null) {
                    continue;
                }

                moduleConfig.getEditorialItems().addAll(
                        SwimlaneModuleConfigProximus.generateEditorialTiles(swimlane, true));
            }
        }
    }

    /**
     * Extend the 3Ready page config with pages from the proximus hub definition.
     *
     * @param contentConfig The page content config which needs to be extended.
     */
    private void extendPageConfiguration(ContentConfig contentConfig, SwimLaneHubResponseProximus hub) {
        HashMap<String, PageConfig> pageConfigs = new HashMap<>();

        // Add 3Ready client page configs.
        for (PageConfig pageConfig : contentConfig.getPageConfigs()) {
            pageConfigs.put(pageConfig.getId(), pageConfig);
        }

        // Generate page configurations.
        for (AccessPointProximus accessPoint : hub.getAccessPoints()) {
            PageConfig pageConfig = new AccessPointPageConfigProximus(accessPoint, hub, false);
            pageConfigs.put(pageConfig.getId(), pageConfig);
            // Duplicate the page config with group id for lookup.
            if (!TextUtils.isEmpty(accessPoint.getGroupId())
                    && accessPoint.getType() != AccessPointProximus.Type.SubPage) {
                pageConfig = new AccessPointPageConfigProximus(accessPoint, hub, true);
                pageConfigs.put(pageConfig.getId(), pageConfig);
            }
        }

        // Extended 3ready page config.
        contentConfig.getPageConfigs().clear();
        contentConfig.getPageConfigs().addAll(pageConfigs.values());
    }

    /**
     * Download and save the swimlane hub definition.
     */
    private void updateHubDefinition() {
        try (InputStream inputStream = mHomeProxy.getSwimlaneHubAsStream()) {
            FileUtils.writeToFile(inputStream,
                    new File(mApp.getFilesDir(), HUB_DEFINITION_FILE));
        } catch (Exception e) {
            Log.e(TAG, "Failed to update hub definition.", e);
        }
    }

    /**
     * Download and save the 3Ready page config.
     */
    private void updateRemote3ReadyConfig() {
        try (InputStream inputStream = mHomeProxy.getRemoteConfig(
                getConfigLanguage(), mMwProxy.getAppVersion())) {
            updateCachedConfig(inputStream);
        } catch (Exception e) {
            Log.e(TAG, "Failed to update client config.", e);
        }
    }

    // Get notification about the standby state changes.
    private final StandByStateChangeReceiver.Listener mStandByStateChangeListener = screenOn -> {
        // Cancel periodic client config and translation updates..
        if (!screenOn) {
            BaseWorkManager.cancelAlarm(mApp, HomeService.buildUpdateConfigIntent());
        }
    };

    // Cache updaters.
    protected static final CacheUpdater translationUpdater = new PeriodicCacheUpdater(Settings.lastTranslationSyncTime,
            Components.get(CacheSettings.class)::geTranslationUpdatePeriod) {

        private String mAppLanguage = LocaleUtils.getApplicationLanguage();

        @Override
        public boolean isCacheValid() {
            // Check if cached for the current language.
            return super.isCacheValid()
                    && Objects.equals(mAppLanguage, LocaleUtils.getApplicationLanguage());
        }

        @Override
        public void validateCache() {
            mAppLanguage = LocaleUtils.getApplicationLanguage();
            super.validateCache();
        }
    };

    protected static final CacheUpdater configUpdater = new PeriodicCacheUpdater(Settings.lastConfigSyncTime,
            Components.get(CacheSettings.class)::getConfigUpdatePeriod) {
        private boolean mValidated = false;
        private String mAppLanguage = LocaleUtils.getApplicationLanguage();
        private boolean mAuthenticated;

        @Override
        public boolean isCacheValid() {
            // Validate it once after each reboot.
            return mValidated && mAuthenticated == Settings.authenticated.get(false)
                    && Objects.equals(mAppLanguage, LocaleUtils.getApplicationLanguage())
                    && super.isCacheValid();
        }

        @Override
        public void validateCache() {
            mValidated = true;
            mAppLanguage = LocaleUtils.getApplicationLanguage();
            mAuthenticated = Settings.authenticated.get(false);
            super.validateCache();
        }
    };


}
