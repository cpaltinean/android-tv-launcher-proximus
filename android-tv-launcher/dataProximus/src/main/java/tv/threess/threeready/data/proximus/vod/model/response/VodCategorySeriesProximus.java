package tv.threess.threeready.data.proximus.vod.model.response;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.vod.model.IBaseVodSeries;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Class which represents a VOD category as base VOD series.
 *
 * @author Barabas Attila
 * @since 11/24/21
 */
public class VodCategorySeriesProximus implements IBaseVodSeries {
    private final VodCategoryProximus mSeriesCategory;
    private final List<String> mSeasonIds;
    private final boolean mSmallCardType;

    public VodCategorySeriesProximus(VodCategoryProximus seriesCategory, List<VodCategoryProximus> seasons, SwimLaneProximus.ClientUiClass uiClass) {
        mSeriesCategory = seriesCategory;
        mSeasonIds = new ArrayList<>();
        if (seriesCategory.isLeaf()) {
            mSeasonIds.add(seriesCategory.getId());
        } else {
            for (VodCategoryProximus season : ArrayUtils.notNull(seasons)) {
                mSeasonIds.add(season.getId());
            }
        }
        mSmallCardType = uiClass == SwimLaneProximus.ClientUiClass.VodCat_Small;
    }

    public VodCategorySeriesProximus(VodCategoryProximus seriesCategory, List<VodCategoryProximus> seasons) {
        this(seriesCategory, seasons, SwimLaneProximus.ClientUiClass.Undefined);
    }

    @Override
    public String getId() {
        return mSeriesCategory.getId();
    }

    @Override
    public String getTitle() {
        return mSeriesCategory.getTitle();
    }

    @Override
    public List<String> getSeasonIds() {
        return mSeasonIds;
    }

    @Override
    public ParentalRating getParentalRating() {
        return mSeriesCategory.getParentalRating();
    }

    @Override
    public List<IImageSource> getImageSources() {
        return mSeriesCategory.getImageSources();
    }

    @Override
    public boolean isSmallCardType() {
        return mSmallCardType;
    }
}
