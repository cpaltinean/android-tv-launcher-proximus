package tv.threess.threeready.data.proximus.vod.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.vod.model.IRentalExpiration;

/**
 * Retrofit request class for rented vods.
 *
 * Created by Noemi Dali on 24.04.2020.
 */
@XmlRootElement(name = "RentedItem")
public class RentalExpirationProximus implements IRentalExpiration {

    @XmlElement(name = "ExpireTime")
    private long mExpirationTime;

    @XmlElement(name = "MovieRef")
    private MovieReference mMovieReference;

    @XmlElement(name = "StartTime")
    private long mRentalTime;

    @Override
    public long getExpireTime() {
        return mExpirationTime;
    }

    @Override
    public long getRentalTime() {
        return mRentalTime;
    }

    @Override
    public String getVariantId() {
        return mMovieReference.mReference;
    }

    @XmlRootElement(name = "MovieRef")
    private static class MovieReference {
        @XmlElement(name = "ref")
        private String mReference;
    }
}
