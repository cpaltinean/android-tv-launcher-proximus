package tv.threess.threeready.data.proximus.vod.observable;

import android.content.Context;
import android.net.Uri;

import java.io.IOException;

import io.reactivex.ObservableEmitter;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.vod.VodContract;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Rx observable to subscribe to the purchase changes to the given VOD.
 *
 * @author Barabas Attila
 * @since 2019.02.06
 */
public class VodObservable extends BaseVodObservable<IVodItem> {
    private static final String TAG = Log.tag(VodObservable.class);

    private final String mVodId;
    private IBaseVodItem mVod;

    public VodObservable(Context context, IBaseVodItem vod) {
        this(context, vod, vod.getId());
    }

    public VodObservable(Context context, String vodId) {
        this(context, null, vodId);
    }

    private VodObservable(Context context, IBaseVodItem vod, String vodId) {
        super(context);
        mVod = vod;
        mVodId = vodId;
    }

    @Override
    public void subscribe(ObservableEmitter<IVodItem> emitter) throws Exception {
        super.subscribe(emitter);

        publishVod();

        if (mVod != null) {
            registerObserver(VodContract.buildPurchaseUrisForVod(mVod));
        }
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<IVodItem> emitter) throws Exception {
        publishVod();
    }

    private void publishVod() throws IOException {
        if (RxUtils.isDisposed(mEmitter)) {
            Log.w(TAG, "Emitter already disposed.");
            return;
        }

        IVodItem vod;
        if (mVod != null) {
            vod = getCompleteVod(mVod);
        } else {
            vod = mVodProxy.getVod(mVodId);
            registerObserver(VodContract.buildPurchaseUrisForVod(vod));
        }

        mVod = vod;
        Log.d(TAG, "Publish vod : " + mVod.getId());
        mEmitter.onNext(vod);
    }
}
