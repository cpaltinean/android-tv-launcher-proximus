package tv.threess.threeready.data.proximus.account.model.account;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.account.model.account.IPackage;

/**
 * Proximus account packages file, which holds the package related information.
 *
 * Created by Noemi Dali on 26.03.2020.
 */
@XmlRootElement(name = "ItemPackageType")
public class PackageInfoProximus implements IPackage {

    @XmlElement(name = "PackageID")
    private String mPackageId;

    @XmlElement(name = "ExternalID")
    private String mExternalId;

    @XmlElement(name = "PackageStartDate")
    private long mPackageStartDate;

    @XmlElement(name = "PackageEndDate")
    private long mPackageEndDate;

    @XmlElement(name = "isLocked")
    private boolean mIsLocked;

    @XmlElement(name = "ItemSubtypes")
    private ItemSubtypesProximus mItemSubtypes;

    @XmlElementWrapper(name = "ItemTypes")
    @XmlElement(name = "ItemType")
    private List<String> mPackageTypes;

    public String getPackageId() {
        return mPackageId;
    }

    @Override
    public String getExternalId() {
        return mExternalId;
    }

    /**
     * @return The time when the subscription on the package started.
     */
    public long getPackageStartDate() {
        return mPackageStartDate;
    }

    /**
     * @return The time when the subscription on the package ends.
     */
    public long getPackageEndDate() {
        return mPackageEndDate;
    }

    /**
     * @return True if the package is temporally locked.
     */
    public boolean isLocked() {
        return mIsLocked;
    }

    @Override
    public List<String> getPackageTypes() {
        return mPackageTypes;
    }

    public ItemSubtypesProximus getItemSubtypes() {
        return mItemSubtypes;
    }
}
