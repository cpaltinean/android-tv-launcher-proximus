/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.proximus.tv.observable;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.ObservableEmitter;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.tv.TvChannelCacheType;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.api.tv.model.ZappingInfo;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.data.tv.model.NoInfoBroadcast;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Rx observable class to return the current program list.
 * Automatically refresh and emits a new program list when a program expires.
 * <p>
 * Created by Szilard on 1/18/2018.
 */

public class ZappingInfoObservable extends BaseContentObservable<List<ZappingInfo>> {
    private static final String TAG = Log.tag(ZappingInfoObservable.class);

    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);
    private final Translator mTranslator = Components.get(Translator.class);

    private final TvCache mTvCache = Components.get(TvCache.class);

    private volatile Disposable mRescheduleDisposable;
    private volatile Disposable mReloadDisposable;

    private List<ZappingInfo> mZappingInfoList;
    private int mFavoriteCount;

    public ZappingInfoObservable(Context context) {
        super(context);
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<List<ZappingInfo>> emitter) {
        Log.d(TAG, "Uri change reload zapping list. " + uri);

        // Update the current item on channel.
        if (uri.toString().contains(
                TvContract.Broadcast.CONTENT_URI_WITH_CHANNEL.toString())) {
            String channelId = uri.getLastPathSegment();
            updateZappingInfoForChannel(channelId);

            // Update the whole list.
        } else {
            updateZappingInfoList();
        }
    }

    @Override
    public void subscribe(ObservableEmitter<List<ZappingInfo>> emitter) throws Exception {
        super.subscribe(emitter);

        // Register database observable.
        registerObserver(TvContract.Favorites.CONTENT_URI);
        registerObserver(TvContract.Channel.CONTENT_URI);
        registerObserver(TvContract.Broadcast.CONTENT_URI);
        registerObserver(TvContract.Broadcast.CONTENT_URI_WITH_CHANNEL);

        // Add internet and cable connectivity listeners.
        mInternetChecker.addStateChangedListener(mOnInternetStateChangedListener);

        updateZappingInfoList();
    }

    /**
     * Update the zapping info list and emmit the updated list.
     *
     * @param channelId The channel id on which the current program needs to be updated.
     */
    private void updateZappingInfoForChannel(String channelId) {
        Log.d(TAG, "Update zapping info on channel. " + channelId);

        if (mZappingInfoList == null) {
            // The zapping info list is not loaded yet. There is nothing to update.
            return;
        }

        IBroadcast broadcast = mTvCache.getNowBroadcastByChannelId(channelId);

        // Update the broadcast in the list.
        for (ZappingInfo zappingInfo : mZappingInfoList) {
            TvChannel channel = zappingInfo.getChannel();
            IBroadcast prevBroadcast = zappingInfo.getBroadcast();
            if (channelId.equals(channel.getId())) {
                if (broadcast != null && prevBroadcast.getId().equals(broadcast.getId())) {
                    // Broadcast not updated. Nothing to do.
                    Log.d(TAG, "Zapping info is already update to date. Channel id : " + channelId);
                    return;
                }

                if (broadcast == null || !broadcast.isLive()) {
                    // Check and create dummy program if it's needed.
                    broadcast = createNoInfoCurrentlyRunningProgram(channel, broadcast);
                }

                zappingInfo.setBroadcast(broadcast);
            }
        }

        emmitZappingInfoList(mFavoriteCount, mZappingInfoList);
    }

    /**
     * Update the zapping info list and emmit to the subscribed observers.
     */
    private void updateZappingInfoList() {
        // Build broadcast map.
        List<IBroadcast> broadcastList = mTvCache.getAllNowBroadcasts();
        Map<String, IBroadcast> channelBroadcastMap = new HashMap<>();
        for (IBroadcast broadcast : broadcastList) {
            channelBroadcastMap.put(broadcast.getChannelId(), broadcast);
        }

        List<ZappingInfo> zappingInfoList = new ArrayList<>();

        // Add favorite channels to list.
        List<TvChannel> favoriteChannelList = mTvCache.getChannels(TvChannelCacheType.FAVORITE, false);
        for (TvChannel channel : favoriteChannelList) {
            zappingInfoList.add(createZappingInfo(channel,
                    channelBroadcastMap.get(channel.getId()), true));
        }

        int favoriteCount = zappingInfoList.size();

        // Add all channels to list.
        List<TvChannel> allChannelList = mTvCache.getChannels(TvChannelCacheType.ALL, false);
        for (TvChannel channel : allChannelList) {
            zappingInfoList.add(createZappingInfo(channel,
                    channelBroadcastMap.get(channel.getId()), false));
        }

        emmitZappingInfoList(favoriteCount, zappingInfoList);
    }

    /**
     * Create zapping info which holds the channel and the currently running program on it.
     *
     * @param channel    The channel of the zapping info.
     * @param broadcast  The currently running program on the channel.
     *                   If not available a dummy "No info" program will be generated.
     * @param isFavorite True if the channel is from the favorite section.
     * @return The created zapping info.
     */
    private ZappingInfo createZappingInfo(TvChannel channel, @Nullable IBroadcast broadcast, boolean isFavorite) {
        if (broadcast == null || !broadcast.isLive()) {
            // Program not available. Create dummy "No info" program.
            broadcast = createNoInfoCurrentlyRunningProgram(channel, broadcast);
        }

        return new ZappingInfo(channel, broadcast, isFavorite);
    }

    /**
     * Create dummy "No info" program if the currently running program is null or not live.
     *
     * @param channel   The channel on which the pgoram should be created.
     * @param broadcast Null or the first available program on the channel.
     * @return The dummy "No info" program.
     */
    private IBroadcast createNoInfoCurrentlyRunningProgram(TvChannel channel, @Nullable IBroadcast broadcast) {
        // Program not available. Create dummy "No info" program.
        long now = System.currentTimeMillis();
        long endTime = broadcast != null && broadcast.isFuture()
                ? broadcast.getStart() : now + TimeUnit.HOURS.toMillis(1);

        return new NoInfoBroadcast(channel.getId(), mTranslator.get(TranslationKey.EPG_NO_INFO),
                isAdultChannel(channel.getId()) ? ParentalRating.Rated18 : ParentalRating.Undefined,
                now, endTime);
    }

    /**
     * @return True if the channel of the given id is considered adult.
     */
    private boolean isAdultChannel(String channelId) {
        List<String> adultChannelIds = SettingsProximus.adultChannelIds.getList();
        return adultChannelIds != null && adultChannelIds.contains(channelId);
    }

    /**
     * Emmit zapping info list for the subscriber and schedule the update when the fist program expires.
     *
     * @param favoriteCount   The number favorite channels in the list.
     * @param zappingInfoList The zapping info list which needs to be emmited.
     */
    private void emmitZappingInfoList(int favoriteCount, List<ZappingInfo> zappingInfoList) {
        // Emit zapping info list.
        Log.d(TAG, "Zapping info list loaded. " + zappingInfoList.size());
        if (mEmitter != null) {
            mEmitter.onNext(zappingInfoList);
        }

        mFavoriteCount = favoriteCount;
        mZappingInfoList = zappingInfoList;

        // Calculate the remaining time until a show expires.
        long minEndDate = Long.MAX_VALUE;
        for (ZappingInfo zappingInfo : zappingInfoList) {
            if (zappingInfo != null && zappingInfo.getBroadcast().getEnd() < minEndDate) {
                minEndDate = zappingInfo.getBroadcast().getEnd();
            }
        }

        // Calculate the remaining time until the first show expires.
        final long delay = minEndDate - System.currentTimeMillis()
                // Add a few seconds threshold
                + TimeUnit.SECONDS.toMillis(2);

        if (delay > 0) {
            Log.d(TAG, "Update now program list after." + TimeUnit.MILLISECONDS.toSeconds(delay) + " sec. Internet : "
                    + mInternetChecker.isInternetAvailable());

            RxUtils.disposeSilently(mRescheduleDisposable);
            mRescheduleDisposable = Completable.fromAction(() -> {
                Log.d(TAG, "On current program expired. Refresh channel list.");
                updateZappingInfoList();
            })
                    .delaySubscription(delay, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe();
        }
    }

    @Override
    protected void unregisterObserver() {
        super.unregisterObserver();

        // Cancel
        RxUtils.disposeSilently(mRescheduleDisposable);
        RxUtils.disposeSilently(mReloadDisposable);

        // Remove cable and internet connectivity change listeners.
        mInternetChecker.removeStateChangedListener(mOnInternetStateChangedListener);
    }

    /**
     * Reload channel list after the internet connectivity state changed.
     */
    private final InternetChecker.OnStateChangedListener mOnInternetStateChangedListener = (state) -> mReloadDisposable = Completable.fromAction(() -> {
        if (RxUtils.isDisposed(mEmitter)) {
            Log.e(TAG, "Could not handle internet connectivity change. Emitter already disposed.");
            return;
        }
        ZappingInfoObservable.this.updateZappingInfoList();
    })
            .subscribeOn(Schedulers.single())
            .observeOn(Schedulers.single())
            .subscribe();
}
