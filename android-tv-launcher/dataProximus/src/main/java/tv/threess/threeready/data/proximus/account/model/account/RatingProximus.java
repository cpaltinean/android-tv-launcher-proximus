package tv.threess.threeready.data.proximus.account.model.account;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.data.proximus.account.model.account.adapter.AccountRatingCodeAdapterProximus;

/**
 * Rating Proximus information used when getting/updating Account Information.
 *
 * Created by Daniela Toma on 7.04.2020.
 */
@XmlRootElement(name = "Rating")
public class RatingProximus implements Serializable {

    @XmlJavaTypeAdapter(AccountRatingCodeAdapterProximus.class)
    @XmlElement(name = "RatingCode")
    private Integer mRatingCode;

    @XmlElement(name = "RatingType")
    private RatingTypesProximus mRatingType;

    public RatingProximus() {
    }

    public RatingProximus(int ratingCode, RatingTypesProximus ratingType) {
        this.mRatingCode = ratingCode;
        this.mRatingType = ratingType;
    }

    public int getRatingCode() {
        return mRatingCode == null ? ParentalRating.Undefined.getMinimumAge() : mRatingCode;
    }

    public RatingTypesProximus getRatingType() {
        return mRatingType;
    }

}
