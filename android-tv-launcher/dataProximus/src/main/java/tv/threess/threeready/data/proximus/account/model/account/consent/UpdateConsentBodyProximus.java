package tv.threess.threeready.data.proximus.account.model.account.consent;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

/**
 * Class used for updating a consent in the microservice.
 *
 * @author Zsolt Bokor_
 * @since 2020.10.23
 */
public class UpdateConsentBodyProximus {

    @SerializedName("value")
    @JsonAdapter(ConsentValueAdapter.class)
    private final boolean mIsAccepted;

    public UpdateConsentBodyProximus(boolean isAccepted) {
        mIsAccepted = isAccepted;
    }
}
