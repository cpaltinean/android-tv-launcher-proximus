package tv.threess.threeready.data.proximus.tv.model.channel;

/**
 * Proximus verimatrix server information.
 *
 * @author Andor Lukacs
 * @since 2020-01-16
 */
public class VCASInfoTCH {
    private final String mVcasAddress;
    private final String mSubscriberIdHash;

    public VCASInfoTCH(String vcasAddress, String subscriberIdHash) {
        mVcasAddress = vcasAddress;
        mSubscriberIdHash = subscriberIdHash;
    }

    /**
     * @return The address of the verimatrix server.
     */
    public String getVCASAddress() {
        return mVcasAddress;
    }

    /**
     * @return The unique hash of the subscriber account.
     */
    public String getSubscriberIDHash() {
        return mSubscriberIdHash;
    }
}
