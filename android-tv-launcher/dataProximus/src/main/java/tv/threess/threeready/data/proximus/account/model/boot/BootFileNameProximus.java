package tv.threess.threeready.data.proximus.account.model.boot;

import com.google.gson.annotations.SerializedName;

/**
 * Proximus lighthouse boot file name response.
 * Contains the environment and base url.
 *
 * @author Barabas Attila
 * @since 3/1/21
 */
public class BootFileNameProximus {

    @SerializedName("name")
    private String mEnvironment;

    @SerializedName("auth_base_url")
    private String mBaseUrl;

    public String getEnvironment() {
        return mEnvironment;
    }

    public String getBaseUrl() {
        return mBaseUrl;
    }
}
