package tv.threess.threeready.data.proximus.account.model.boot;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Boot config option container which holds the config for 3rd party app integration.
 *
 * @author Barabas Attila
 * @since 2020.05.11
 */
public class PartnerConfigContainerProximus {

    @SerializedName("netflix")
    private NetflixPartnerConfigProximus mNetflixPartnerConfig;


    @Nullable
    public NetflixPartnerConfigProximus getNetflixPartnerConfig() {
        return mNetflixPartnerConfig;
    }
}
