package tv.threess.threeready.data.proximus.account.model.boot;

import com.google.gson.annotations.SerializedName;

/**
 * Boot config options for Netflix integration.
 *
 * @author Barabas Attila
 * @since 2020.05.11
 */
public class NetflixPartnerConfigProximus {

    @SerializedName("partnerId")
    private String mPartnerId;

    public String getPartnerId() {
        return mPartnerId;
    }
}
