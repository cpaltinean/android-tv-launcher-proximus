package tv.threess.threeready.data.proximus.home.model.editorial;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Editorial tile which opens the apps section.
 *
 * @author Barabas Attila
 * @since 6/21/21
 */
public class GoAppsEditorialItemsProximus extends SwimlaneEditorialItemProximus {

    public GoAppsEditorialItemsProximus(SwimLaneProximus swimLane,
                                        SwimLaneProximus.SwimlaneTile swimlaneTile, Integer position) {
        super(swimLane, swimlaneTile, position);
    }

    @Nullable
    @Override
    public EditorialItemAction getAction() {
        return mItemAction;
    }

    private final EditorialItemAction mItemAction =
            (EditorialItemAction.PageByIdEditorialItemAction) () -> PageConfig.APPS_MENU_ITEM_ID;
}
