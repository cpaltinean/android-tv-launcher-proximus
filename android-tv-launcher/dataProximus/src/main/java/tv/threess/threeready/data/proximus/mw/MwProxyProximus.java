/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.mw;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.Objects;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.account.setting.MassStorage;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.generic.receivers.HdmiConnectivityReceiver;
import tv.threess.threeready.api.generic.receivers.WaitTask;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.BluetoothUtils;
import tv.threess.threeready.api.middleware.CasUpdateReceiver;
import tv.threess.threeready.api.middleware.SystemUpdateReceiver;
import tv.threess.threeready.api.middleware.TvAdObserver;
import tv.threess.threeready.api.middleware.model.BaseInfo;
import tv.threess.threeready.api.middleware.model.BitStream;
import tv.threess.threeready.api.middleware.model.HdmiInfo;
import tv.threess.threeready.api.middleware.model.Scte35Info;
import tv.threess.threeready.api.middleware.model.ScteState;
import tv.threess.threeready.api.middleware.model.TifChannelList;
import tv.threess.threeready.api.tv.model.TifScanParameters;
import tv.threess.threeready.data.mw.BaseMwProxy;
import tv.threess.threeready.data.proximus.BuildConfig;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.proximus.account.TechnicolorCasSettings;
import tv.threess.threeready.data.proximus.tv.model.channel.IpTvParametersTch;
import tv.threess.threeready.data.proximus.tv.model.channel.TifChannelTch;
import tv.threess.threeready.data.proximus.tv.model.channel.VCASInfoTCH;
import tv.threess.threeready.data.proximus.tv.model.channel.VQEInfoTCH;
import tv.threess.threeready.data.utils.GsonUtils;
import tv.threess.threeready.data.utils.SqlUtils;

/**
 * Proximus specific MW methods
 *
 * @author Daniel Gliga
 * @since 2017.12.09
 */
public class MwProxyProximus extends BaseMwProxy implements Component {
    private static final String TAG = Log.tag(MwProxyProximus.class);

    public static final String PACKAGE_DTVINPUT = "com.technicolor.android.dtvinput";
    public static final String INSTALLER_SERVICE = "com.technicolor.android.dtvinput.setup.InstallerService";
    public static final String DTVPROVIDER_SERVICE = "com.technicolor.android.dtvprovider.service";

    private static final Uri VQE_CONFIG_URI = Uri.parse("content://com.technicolor.android.dtvprovider/vqesettings/");
    private static final Uri VQE_REJOIN_URI = Uri.parse("content://com.technicolor.android.dtvprovider/vqerejoinsettings/");

    private static final Uri SCTE_MARKER_URI = Uri.parse("content://com.technicolor.android.dtvprovider/scte35_section");
    private static final Uri SCTE_STATE_URI = Uri.parse("content://com.technicolor.android.dtvprovider/scte35_state");

    private static final Uri SYSTEM_URI = Uri.parse("content://technicolor/system");
    private static final Uri DISPLAY_URI = Uri.parse("content://technicolor/display");
    private static final Uri PROCESS_INFO_URI = Uri.parse("content://technicolor/process_details");
    public static final Uri LOADAVG_URI = Uri.parse("content://technicolor/proc/loadavg");
    private static final Uri IDS_URI = Uri.parse("content://technicolor/ids");

    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_DATA = "data";
    private static final String COLUMN_VALUE = "value";

    private static final String COLUMN_VQE_STATUS = "vqe_status";
    private static final String COLUMN_VQE_CHANNEL_CONFIG = "channel_config_path";
    private static final String COLUMN_VQE_NETWORK_CONFIG = "network_config_buffer";
    private static final String COLUMN_VQE_OVERWRITE_CONFIG = "overwrite_config_buffer";
    private static final String COLUMN_VQE_REJOIN_CONFIG = "rejoin_config_file";
    private static final String COLUMN_VQE_STATUS_COMPLETE = "complete";

    private static final String COLUMN_BOOT_REASON = "boot_reason";
    private static final String COLUMN_PROCESS_NAME = "process_name";

    private static final String VQE_CONFIG_KEY = "UPDATE_VQE_CONFIG";
    private static final String VQE_REJOIN_KEY = "UPDATE_VQE_REJOIN_PARAMETERS";

    private static final String LAUNCHER_START_FOLDER_NAME = "LauncherStart";
    private static final String LAUNCHER_START_FILE_NAME = "launcher_starts.txt";

    private static final String CHANNEL_CONFIG_FILE_NAME = "channel_config.txt";

    public static final String FIELD_IGMP_LOW_BANDWIDTH = "low-bandwidth";
    public static final String FIELD_IGMP_THRESHOLD_TIME = "threshold-time";

    public static final Uri USER_URI = Uri.parse("content://com.technicolor.android.user.authorities/settings");
    public static final Uri CAS_URI = Uri.parse("content://com.technicolor.android.cas.authorities/settings");

    private static final String INTENT_ACTION_CHECK_SYSTEM_UPDATE = "com.technicolor.ota.firmware.CHECK_SYSTEM_UPDATE";
    private static final String INTENT_ACTION_APPLY_SYSTEM_UPDATE = "com.technicolor.ota.firmware.APPLY_UPDATE";
    private static final String PACKAGE_OTA = "com.technicolor.ota";
    private static final String EXTRA_SYSTEM_UPDATE_RESET = "reset";
    private static final String EXTRA_SYSTEM_UPDATE_ROLLOUT_BYPASS = "rollout_bypass";

    private final Context mContext;

    public MwProxyProximus(Context context) {
        mContext = context;
    }

    @Override
    protected Context getContext() {
        return mContext;
    }

    @Override
    public String getSocId() {
        String socId = SqlUtils.queryFirst(getContext(),
                IDS_URI.buildUpon().appendPath("persist.vendor.socid").build(),
                cursor -> cursor.getString(cursor.getColumnIndex(COLUMN_VALUE)));

        Log.d(TAG, "Soc id :" + socId);
        return StringUtils.notNull(socId);
    }

    /**
     * Returns the MAC address of the network interface where the hardware address is set.
     * @return The MAC of the network interface or null in case of errors.
     */
    @Nullable
    @Override
    public String getEthernetMacAddress() {
        if (!TextUtils.isEmpty(mMacAddress)) {
            return mMacAddress;
        }

        String macAddress = SqlUtils.queryFirst(getContext(),
                IDS_URI.buildUpon().appendPath("persist.vendor.ethernetmac").build(),
                cursor -> cursor.getString(cursor.getColumnIndex(COLUMN_VALUE)));
        mMacAddress = StringUtils.notNull(macAddress).replace(":", "");

        Log.d(TAG, "Ethernet MAC :" + mMacAddress);
        return mMacAddress;
    }

    @Override
    public WaitTask<Boolean> startScan(TifScanParameters params) throws Exception {
        Context context = getContext();
        long paramsHash = params.getParamsHash();
        IpTvParametersTch tchParams = (IpTvParametersTch) params;
        VQEInfoTCH vqeInfo = tchParams.getVQEInfo();
        VCASInfoTCH casInfo = tchParams.getVCASInfo();
        String oldVCASAddress = BuildConfig.DEBUG ? null : TechnicolorCasSettings.System.get(null);

        Log.d(TAG, "Provide VCAS info" +
                ", old.address: " + oldVCASAddress +
                ", new.address: " + casInfo.getVCASAddress()
                + ", subscriberIDHash: " + casInfo.getSubscriberIDHash());

        TechnicolorCasSettings.System.put(casInfo.getVCASAddress());
        TechnicolorCasSettings.BackendAuthenticator.put(casInfo.getSubscriberIDHash());

        VQEConfigBuilder vqe = new VQEConfigBuilder(context)
                .networkConfig(vqeInfo.getNetworkConfig())
                .channelConfig(vqeInfo.getChannelConfig())
                .burstConfig(vqeInfo.getBurstConfig())
                .build();

        if (paramsHash != Settings.iptvPlaybackOptionsHash.get(-1L)) {
            Log.d(TAG, "startScan() - hash is different than the saved one, we need to scan: hash = [" + paramsHash + "], params = [" + params + "]");

            // Get my looper or default.
            Looper looper = Looper.myLooper();
            if (looper == null) {
                looper = Looper.getMainLooper();
            }
            Handler handler = new Handler(looper);

            TchScanTask scanTask = new TchScanTask(context, handler, tchParams, vqe);
            scanTask.addListener((success) -> {
                if (success) {
                    Settings.iptvPlaybackOptionsHash.edit().put(paramsHash);
                    SettingsProximus.vqeConfigHash.edit().put(vqeInfo.hashCode());
                } else {
                    Log.d(TAG, "Channel scan failed");
                }
                if (!TextUtils.isEmpty(oldVCASAddress) && !Objects.equals(casInfo.getVCASAddress(), oldVCASAddress)) {
                    Components.get(CasUpdateReceiver.class).notifyStateChanged();
                }
            });

            return scanTask.start();
        } else {
            Log.d(TAG, "startScan() - hash is the same as before, skipping scan. hash = [" + paramsHash + "]");
            if (!configureVqeParameters(vqe)) {
                Log.w(TAG, "Vqe configuration failed.");
                return new EmptyScanTask();
            }

            SettingsProximus.vqeConfigHash.edit().put(vqeInfo.hashCode());
            if (!TextUtils.isEmpty(oldVCASAddress) && !Objects.equals(casInfo.getVCASAddress() ,oldVCASAddress)) {
                Components.get(CasUpdateReceiver.class).notifyStateChanged();
            }
        }

        return new EmptyScanTask();
    }

    public boolean configureVqeParameters(VQEConfigBuilder vqeConfig) {
        // make VQEC configuration data in form of DtvProvider
        ContentValues configValues = new ContentValues();
        configValues.put(COLUMN_ID, VQE_CONFIG_KEY);

        Uri networkConfigUri = vqeConfig.getNetworkConfigUri();
        if (networkConfigUri != null) {
            configValues.put(COLUMN_VQE_NETWORK_CONFIG, networkConfigUri.toString());
            mContext.grantUriPermission(MwProxyProximus.DTVPROVIDER_SERVICE, networkConfigUri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION
            );
        }

        Uri channelConfigUri = vqeConfig.getChannelConfigUri();
        if (channelConfigUri != null) {
            configValues.put(COLUMN_VQE_CHANNEL_CONFIG, channelConfigUri.toString());
            mContext.grantUriPermission(MwProxyProximus.DTVPROVIDER_SERVICE, channelConfigUri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION
            );
        }

        Uri overwriteConfigUri = vqeConfig.getBurstConfigUri();
        if (overwriteConfigUri != null) {
            configValues.put(COLUMN_VQE_OVERWRITE_CONFIG, overwriteConfigUri.toString());
            mContext.grantUriPermission(MwProxyProximus.DTVPROVIDER_SERVICE, overwriteConfigUri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION
            );
        }

        // insert configuration data. DtvProvider sends data to Vqec library.
        ContentResolver cr = mContext.getContentResolver();
        int deleted = cr.delete(VQE_CONFIG_URI, null, null);
        Uri inserted = cr.insert(VQE_CONFIG_URI, configValues);
        Log.d(TAG, "VqeConfigUpdate: removed[" + deleted + "], inserted: " + inserted);

        try (Cursor c = cr.query(VQE_CONFIG_URI, null, null, null, null)) {
            if (c == null || !c.moveToFirst()) {
                throw new NullPointerException("Failed to read result from cursor");
            }

            String status = c.getString(c.getColumnIndex(COLUMN_VQE_STATUS));
            if (!COLUMN_VQE_STATUS_COMPLETE.equals(status)) {
                throw new InvalidParameterException("Vqe Configuration Update failed with status: " + status);
            }
        }
        return true;
    }

    @Override
    public void updateVqeConfig(Object config) throws IOException {
        VQEInfoTCH vqeConfig = (VQEInfoTCH) config;

        Context context = getContext();
        long oldConfigHash = SettingsProximus.vqeConfigHash.get(-1L);
        long newConfigHash = vqeConfig.hashCode();

        if (oldConfigHash == newConfigHash) {
            Log.d(TAG, "Ignore VQE config update. Already up to date.");
            return;
        }

        VQEConfigBuilder vqe = new VQEConfigBuilder(context)
                .networkConfig(vqeConfig.getNetworkConfig())
                .channelConfig(vqeConfig.getChannelConfig())
                .burstConfig(vqeConfig.getBurstConfig())
                .build();

        if (!configureVqeParameters(vqe)) {
            Log.w(TAG, "Failed to configure VQE");
            return;
        }

        SettingsProximus.vqeConfigHash.edit().put(newConfigHash);
    }

    public void updateRejoinConfig(String rejoinConfig) {
        long oldConfigHash = SettingsProximus.igmpConfigHash.get(-1L);
        long newConfigHash = rejoinConfig.hashCode();

        if (oldConfigHash == newConfigHash) {
            Log.d(TAG, "Ignore IGMP Rejoin config update. Already up to date.");
            return;
        }

        if (!configureVqeRejoin(rejoinConfig)) {
            Log.w(TAG, "Failed to configure IGMP Rejoin");
            return;
        }

        SettingsProximus.igmpConfigHash.edit().put(newConfigHash);
    }

    public boolean configureVqeRejoin(String rejoinConfigContent) {
        // make Rejoin VQEC configuration data in form of DtvProvider
        ContentValues rejoinConfigValue = new ContentValues();
        rejoinConfigValue.put(COLUMN_ID, VQE_REJOIN_KEY);
        rejoinConfigValue.put(COLUMN_VQE_REJOIN_CONFIG, rejoinConfigContent);

        // insert configuration data. DtvProvider sends data to Vqec library.
        ContentResolver cr = mContext.getContentResolver();
        int deleted = cr.delete(VQE_REJOIN_URI, null, null);
        Uri inserted = cr.insert(VQE_REJOIN_URI, rejoinConfigValue);
        Log.d(TAG, "VqeRejoinUpdate: removed[" + deleted + "], inserted: " + inserted);
        return inserted != null;
    }

    @Override
    public void saveHeartBeat() {
        try {
            MassStorage.batchEdit()
                    .put(MassStorage.lastKnownHeartBeatBootId, getBootId())
                    .put(MassStorage.lastKnownHeartBeatUptime, SystemClock.elapsedRealtime())
                    .put(MassStorage.lastKnownHeartBeatEpoch, System.currentTimeMillis())
                    .put(MassStorage.lastKnownHDMIConnection, Components.get(HdmiConnectivityReceiver.class).isConnected())
                    .put(MassStorage.lastKnownRemoteConnection, BluetoothUtils.isRemoteConnected(mContext))
                    .put(MassStorage.lastKnownScreenOn, isScreenOn())
                    .persistNow();
        } catch (Exception e) {
            Log.e(TAG, "Failed to save heart beat", e);
        }
    }

    /**
     * Get the HDMI connection status and EDID data.
     *
     * @return HDMI connection information.
     */
    @Override
    public HdmiInfo getHdmiInfo() {
        return SqlUtils.queryFirst(getContext(),
                DISPLAY_URI,
                cursor -> {
                    int indexName = cursor.getColumnIndex(COLUMN_NAME);
                    int indexValue = cursor.getColumnIndex(COLUMN_VALUE);

                    byte[] rawEdid = null;
                    byte[] extEdid = null;
                    boolean connected = false;
                    boolean hdcpActive = false;

                    cursor.moveToPosition(-1);
                    while (cursor.moveToNext()) {
                        String name = cursor.getString(indexName);

                        if ("raw_edid".equals(name)) {
                            rawEdid = cursor.getBlob(indexValue);
                        }

                        if ("extended_edid".equals(name)) {
                            extEdid = cursor.getBlob(indexValue);
                        }

                        if ("hdmi_conn".equals(name)) {
                            connected = cursor.getInt(indexValue) != 0;
                        }

                        if ("hdcp_state".equals(name)) {
                            hdcpActive = cursor.getInt(indexValue) != 0;
                        }
                    }
                    return new HdmiInfo(connected, rawEdid, extEdid, hdcpActive);
                }
        );
    }

    @Override
    public WaitTask<Void> waitForDtvSetup() {
        return new WaitDtvSetupTaskProximus(mContext);
    }

    @Override
    public <T extends BaseInfo<?>> T readProcessInfo(String packageName, T info) {
        Uri uri = PROCESS_INFO_URI.buildUpon().appendQueryParameter(COLUMN_PROCESS_NAME, packageName).build();
        SqlUtils.forEach(getContext(), uri, cursor -> {
            int indexName = cursor.getColumnIndex(COLUMN_PROCESS_NAME);
            String name = cursor.getString(indexName);
            if (packageName.equals(name.trim())) {
                info.readFrom(cursor);
            }
        });

        return info;
    }

    @Override
    public String getBootReason() {
        return SqlUtils.queryFirst(getContext(),
                SYSTEM_URI,
                cursor -> cursor.getString(cursor.getColumnIndex(COLUMN_BOOT_REASON))
        );
    }

    @Override
    public String readFileContent(Uri fileUri) {
        return SqlUtils.queryFirst(getContext(),
                fileUri,
                cursor -> cursor.getString(cursor.getColumnIndex(COLUMN_VALUE))
        );
    }

    @Override
    public boolean isDVBCableConnected() {
        return false;
    }

    /**
     * Check for system update on the server and start the download when available.
     *
     * @param reset         True if the download should be restarted in case it was already download or the download is in progress.
     * @param rollOutBypass True to bypass server daily update roll out quota.
     * @param manualCheck   True if the update check was initiated by the user.
     */
    @Override
    public void checkSystemUpdates(boolean reset, boolean rollOutBypass, boolean manualCheck) {
        try {
            Context context = getContext();
            final Intent intent = new Intent(INTENT_ACTION_CHECK_SYSTEM_UPDATE);
            intent.putExtra(EXTRA_SYSTEM_UPDATE_RESET, reset);
            intent.putExtra(EXTRA_SYSTEM_UPDATE_ROLLOUT_BYPASS, rollOutBypass);
            intent.setPackage(PACKAGE_OTA);
            context.startService(intent);
            Log.d(TAG, "Check for system update. Reset : " + reset
                    + ", rollOutBypass : " + rollOutBypass + ", manualCheck : " + manualCheck);
            Components.get(SystemUpdateReceiver.class).notifyUpdateCheckTriggered(manualCheck);
        } catch (Exception e) {
            Log.e(TAG, "Failed to start system update service", e);
        }
    }

    /**
     * @return The android serial number of the device.
     */
    @Override
    public String getAndroidSerialNumber() {
        String serialNr = SqlUtils.queryFirst(getContext(),
                IDS_URI.buildUpon().appendPath("ro.serialno").build(),
                cursor -> cursor.getString(cursor.getColumnIndex(COLUMN_VALUE)));

        Log.d(TAG, "Serial nr :" + serialNr);
        return StringUtils.notNull(serialNr);
    }

    /**
     * Reboot the device and apply the previously downloaded system image.
     */
    @Override
    public void applySystemUpdate() {
        try {
            Context context = getContext();
            final Intent intent = new Intent(INTENT_ACTION_APPLY_SYSTEM_UPDATE);
            intent.setPackage(PACKAGE_OTA);
            context.startService(intent);
            Log.d(TAG, "Apply system update.");
        } catch (Exception e) {
            Log.e(TAG, "Failed to start apply system update service", e);
        }
    }

    @Override
    public void bootCompletedAction() {
        Context context = getContext();
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        BufferedReader inReader = null;
        try {
            inReader = new BufferedReader(new FileReader(new File(
                    context.getExternalFilesDir(LAUNCHER_START_FOLDER_NAME), LAUNCHER_START_FILE_NAME)));
            while ((line = inReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException ex) {
            Log.d(TAG, "bootCompletedAction read file ex " + ex);
        } finally {
            FileUtils.closeSafe(inReader);
        }

        String currentValue = stringBuilder.toString();

        if (currentValue.isEmpty()) {
            currentValue = String.valueOf(1);
        } else {
            try {
                long currentNumericValue = Long.parseLong(currentValue);
                ++currentNumericValue;
                currentValue = String.valueOf(currentNumericValue);
            } catch (NumberFormatException ex) {
                currentValue = String.valueOf(1);
            }
        }

        FileWriter outWriter = null;
        try {
            outWriter = new FileWriter(new File(
                    context.getExternalFilesDir(LAUNCHER_START_FOLDER_NAME), LAUNCHER_START_FILE_NAME));
            outWriter.write(currentValue);
        } catch (IOException ex) {
            Log.d(TAG, "bootCompletedAction write file ex " + ex);
        } finally {
            FileUtils.closeSafe(outWriter);
        }

    }

    @Override
    public TifChannelList getTifChannels() {
        String[] projection = {
                android.media.tv.TvContract.Channels._ID,
                android.media.tv.TvContract.Channels.COLUMN_TYPE,
                android.media.tv.TvContract.Channels.COLUMN_INPUT_ID,
                android.media.tv.TvContract.Channels.COLUMN_DISPLAY_NUMBER,
                android.media.tv.TvContract.Channels.COLUMN_INTERNAL_PROVIDER_DATA,
        };
        TifChannelList result = new TifChannelList();
        Gson gson = GsonUtils.getGson();
        Uri uri = android.media.tv.TvContract.Channels.CONTENT_URI;
        String where = android.media.tv.TvContract.Channels.COLUMN_SERVICE_TYPE + "=?";
        String[] args = {android.media.tv.TvContract.Channels.SERVICE_TYPE_AUDIO_VIDEO};

        SqlUtils.forEach(getContext(),
                uri,
                projection,
                where,
                args,
                cursor -> {
                    try {
                        long id = cursor.getLong(0);
                        String type = cursor.getString(1);
                        String inputId = cursor.getString(2);
                        String displayNumber = cursor.getString(3);
                        String providerData = cursor.getString(4);
                        switch (type) {
                            case android.media.tv.TvContract.Channels.TYPE_DVB_C:
                                TifChannelTch.InternalProviderData internalDvbC = gson.fromJson(providerData, TifChannelTch.InternalProviderData.class);
                                result.add(new TifChannelTch(id, displayNumber, inputId, internalDvbC.getPublic().getLinkageId()));
                                break;

                            case android.media.tv.TvContract.Channels.TYPE_DVB_T:
                            case android.media.tv.TvContract.Channels.TYPE_DVB_T2:
                                TifChannelTch.InternalProviderData internalDvbT = gson.fromJson(providerData, TifChannelTch.InternalProviderData.class);
                                result.add(new TifChannelTch(id, displayNumber, inputId, internalDvbT.getPublic().getMappingId()));
                                break;

                            case android.media.tv.TvContract.Channels.TYPE_OTHER:
                                TifChannelTch.InternalProviderData internalIpTv = gson.fromJson(providerData, TifChannelTch.InternalProviderData.class);
                                result.add(new TifChannelTch(id, displayNumber, inputId, internalIpTv.getUri()));
                                break;
                        }
                    } catch (Exception e) {
                        Log.i(TAG, "Could not get channels from TIF", e);
                    }
                }
        );

        return result;
    }

    @Override
    public String getVCASPluginVersion() {
        return TechnicolorCasSettings.PluginVersion.get("");

    }

    @Override
    public String getVCASClientVersion() {
        return TechnicolorCasSettings.ClientVersion.get("");
    }

    public static File channelConfigFile(Context context) {
        return new File(context.getFilesDir(), CHANNEL_CONFIG_FILE_NAME);
    }

    public ContentObserver registerScteObserver(Handler handler, TvAdObserver tadObserver) {
        ContentObserver observer = new ContentObserver(null) {

            private Uri stateUri = SCTE_STATE_URI;

            @Override
            public void onChange(boolean selfChange, @Nullable Uri uri) {
                super.onChange(selfChange, uri);
                if (tadObserver == null) {
                    return;
                }
                try {
                    if (SCTE_MARKER_URI.equals(uri)) {
                        Scte35Info info = readScteInfo();
                        if (info != null) {
                            handler.post(() -> tadObserver.onMarkerChange(info));
                            stateUri = SCTE_STATE_URI.buildUpon()
                                    .appendPath(info.get(Scte35Info.Field.pts_time))
                                    .build();
                        }
                    }
                    if (stateUri.equals(uri)) {
                        ScteState state = readScteState();
                        if (state != null) {
                            handler.post(() -> tadObserver.onStateChange(state));
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, e);
                }
            }
        };
        try {
            mContext.getContentResolver().registerContentObserver(SCTE_MARKER_URI, false, observer);
            mContext.getContentResolver().registerContentObserver(SCTE_STATE_URI, true, observer);
        } catch (Exception e) {
            Log.e(TAG, "Failed to register content observer for scte markers", e);
        }
        return observer;
    }

    private Scte35Info readScteInfo() {
        byte[] data = SqlUtils.queryLast(mContext, SCTE_MARKER_URI, cursor -> {
            int column = cursor.getColumnIndex(COLUMN_DATA);
            return cursor.getBlob(column);
        });
        if (data == null) {
            return null;
        }
        return new Scte35Info(new BitStream(data));
    }

    private ScteState readScteState() {
        return SqlUtils.queryLast(mContext, SCTE_STATE_URI, cursor -> {
            ScteState result = new ScteState();
            result.readFrom(cursor);
            return result;
        });
    }
}
