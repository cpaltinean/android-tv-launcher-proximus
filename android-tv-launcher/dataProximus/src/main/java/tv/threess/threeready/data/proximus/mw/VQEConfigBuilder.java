package tv.threess.threeready.data.proximus.mw;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.core.content.FileProvider;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.json.JSONObject;
import tv.threess.threeready.api.BuildConfig;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.proximus.tv.model.channel.IpTvParametersTch;
import tv.threess.threeready.data.utils.GsonUtils;

/**
 * Utility class which can be used to build intent
 * to send VQE config files to the TCH DTV install service.
 *
 * @author Barabas Attila
 * @since 2020.03.12
 */
public class VQEConfigBuilder {
    private static final String TAG = VQEConfigBuilder.class.getCanonicalName();

    static final String ACTION_START_SCAN = "com.technicolor.android.dtvscan.action.ACTION_START_SCAN";
    static final String ACTION_CANCEL_SCAN = "com.technicolor.android.dtvscan.action.ACTION_CANCEL_SCAN";

    static final String NETWORK_CONFIG_FILE_NAME = "network_config.txt";
    static final String BURST_CONFIG_FILE_NAME = "overwrite_config.txt";
    static final String CHANNEL_SCAN_FILE_NAME = "channel_scan.json";
    static final String EXTRA_START_SCAN_PARAM = "params";

    private static final String FIELD_TYPE = "type";
    private static final String FIELD_COUNTRY = "country";
    private static final String FIELD_FILE_PROVIDER_URI = "file-provider-uri";

    private final Context mContext;

    private File channelConfigFile;
    private Uri channelConfigUri;

    private File networkConfigFile;
    private Uri networkConfigUri;

    private File burstConfigFile;
    private Uri burstConfigUri;

    public VQEConfigBuilder(Context context) {
        mContext = context;
    }

    public Uri getChannelConfigUri() {
        return channelConfigUri;
    }

    public Uri getNetworkConfigUri() {
        return networkConfigUri;
    }

    public Uri getBurstConfigUri() {
        return burstConfigUri;
    }

    public VQEConfigBuilder channelConfig(String channelConfig) throws IOException {
        if (channelConfig == null) {
            Log.w(TAG, "Using empty VQEChannel config for scan.");
            channelConfig = "";
        }

        // channel config parameter is mandatory to initiate the VQE-Client.
        channelConfigFile = FileUtils.writeToFile(channelConfig, MwProxyProximus.channelConfigFile(mContext));
        channelConfigUri = FileProvider.getUriForFile(mContext, BuildConfig.FILE_PROVIDER, channelConfigFile);
        return this;
    }

    public VQEConfigBuilder networkConfig(String networkConfig) throws IOException {
        if (networkConfig == null) {
            Log.w(TAG, "Ignore VQENetwork config update. Config is empty.");
            return this;
        }

        networkConfigFile = FileUtils.writeToFile(networkConfig, new File(mContext.getFilesDir(), NETWORK_CONFIG_FILE_NAME));
        networkConfigUri = FileProvider.getUriForFile(mContext, BuildConfig.FILE_PROVIDER, networkConfigFile);
        return this;
    }

    public VQEConfigBuilder burstConfig(String burstConfig) throws IOException {
        if (burstConfig == null) {
            Log.w(TAG, "Ignore VQEBurst config update. Config is empty");
            return this;
        }

        burstConfigFile = FileUtils.writeToFile(burstConfig, new File(mContext.getFilesDir(), BURST_CONFIG_FILE_NAME));
        burstConfigUri = FileProvider.getUriForFile(mContext, BuildConfig.FILE_PROVIDER, burstConfigFile);
        return this;
    }

    public VQEConfigBuilder build() throws IOException {
        if (channelConfigFile == null) {
            File file = MwProxyProximus.channelConfigFile(mContext);
            if (file.exists()) {
                channelConfigFile = file;
                channelConfigUri = FileProvider.getUriForFile(mContext, BuildConfig.FILE_PROVIDER, file);
            }
        }
        if (networkConfigFile == null) {
            File file = new File(mContext.getFilesDir(), NETWORK_CONFIG_FILE_NAME);
            if (file.exists()) {
                networkConfigFile = file;
                channelConfigUri = FileProvider.getUriForFile(mContext, BuildConfig.FILE_PROVIDER, file);
            }
        }
        if (burstConfigFile == null) {
            File file = new File(mContext.getFilesDir(), BURST_CONFIG_FILE_NAME);
            if (file.exists()) {
                burstConfigFile = file;
                channelConfigUri = FileProvider.getUriForFile(mContext, BuildConfig.FILE_PROVIDER, file);
            }
        }
        return this;
    }

    public Intent buildTifScanIntent(IpTvParametersTch params) throws Exception {
        Context context = mContext;
        File file = new File(context.getFilesDir(), VQEConfigBuilder.CHANNEL_SCAN_FILE_NAME);
        try (FileWriter writer = new FileWriter(file, false)) {
            GsonUtils.getGson().toJson(params.getParams(), writer);
        }

        Uri fileUri = FileProvider.getUriForFile(context, tv.threess.threeready.api.BuildConfig.FILE_PROVIDER, file);
        Log.d(TAG, "ChannelsSetup File: " + file.getAbsolutePath() + ", Uri: " + fileUri);

        JSONObject jsonConfig = new JSONObject();
        jsonConfig.put(FIELD_TYPE, params.getType());
        jsonConfig.put(FIELD_COUNTRY, params.getCountry());
        jsonConfig.put(FIELD_FILE_PROVIDER_URI, fileUri.toString());

        Intent intent = new Intent(ACTION_START_SCAN);
        intent.setPackage(MwProxyProximus.PACKAGE_DTVINPUT);
        intent.putExtra(VQEConfigBuilder.EXTRA_START_SCAN_PARAM, jsonConfig.toString());

        // Grant permission to access ChannelsSetup Uri to DtvInput package
        context.grantUriPermission(MwProxyProximus.PACKAGE_DTVINPUT, channelConfigUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
        context.grantUriPermission(MwProxyProximus.PACKAGE_DTVINPUT, networkConfigUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
        context.grantUriPermission(MwProxyProximus.PACKAGE_DTVINPUT, burstConfigUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
        context.grantUriPermission(MwProxyProximus.PACKAGE_DTVINPUT, fileUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
        return intent;
    }
}
