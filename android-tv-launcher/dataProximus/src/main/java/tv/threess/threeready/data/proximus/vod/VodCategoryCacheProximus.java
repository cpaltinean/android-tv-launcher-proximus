package tv.threess.threeready.data.proximus.vod;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.vod.model.IBaseVodSeries;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategoryProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategorySeriesProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodGlobalCategoryProximus;
import tv.threess.threeready.data.vod.VodCategoryCache;

/**
 * Memory cache for VOD categories.
 *
 * @author Barabas Attila
 * @since 6/7/21
 */
public class VodCategoryCacheProximus implements VodCategoryCache, Component {
    private static final String TAG = Log.tag(VodCategoryCacheProximus.class);

    private Map<String, VodCategoryProximus> mCatMap;
    private Map<String, List<VodCategoryProximus>> mChildCatMap;

    private Set<String> mNonEmptyLeafIds;

    private final CategoryMapInitializer mCategoryMapInitializer;
    private final LeftCategoryInitializer mLeftCategoryInitializer;

    public VodCategoryCacheProximus(CategoryMapInitializer categoryMapInitializer,
                                    LeftCategoryInitializer leafCategoryInitializer) {
        mCategoryMapInitializer = categoryMapInitializer;
        mLeftCategoryInitializer = leafCategoryInitializer;
    }

    public void updateCategoryTree(List<VodGlobalCategoryProximus> categories) {
        if (categories == null) {
            return;
        }

        Log.d(TAG, "Update vod category tree. Categories : " + categories.size());
        Map<String, VodCategoryProximus> catMap = new HashMap<>();
        Map<String, VodCategoryProximus> intCatMap = new HashMap<>();
        for (VodGlobalCategoryProximus category : categories) {
            catMap.put(category.getId(), category);
            intCatMap.put(category.getInternalId(), category);
        }

        Map<String, List<VodCategoryProximus>> childCatMap = new HashMap<>();
        for (VodGlobalCategoryProximus category : categories) {
            VodCategoryProximus parent = intCatMap.get(category.getParentId());
            if (parent == null) {
                continue;
            }

            List<VodCategoryProximus> children = childCatMap
                    .computeIfAbsent(parent.getId(), k -> new ArrayList<>());
            children.add(category);
        }

        mCatMap = catMap;
        mChildCatMap = childCatMap;
    }

    @Override
    @Nullable
    public IBaseVodSeries getVodCategorySeries(String seriesId) {
        VodCategoryProximus seriesCategory = getCategory(seriesId);
        if (seriesCategory == null) {
            return null;
        }

        List<VodCategoryProximus> seasons = getSubCategories(seriesId);
        return new VodCategorySeriesProximus(seriesCategory, seasons);
    }

    private Map<String, VodCategoryProximus> getCatMap() {
        if (mCatMap == null) {
            updateCategoryTree(mCategoryMapInitializer.getCategories());
        }

        return mCatMap;
    }

    private Map<String, List<VodCategoryProximus>> getChildCatMap() {
        if (mChildCatMap == null) {
            updateCategoryTree(mCategoryMapInitializer.getCategories());
        }

        return mChildCatMap;
    }

    public void updateCategoryContent(List<VodCategoryProximus> nonEmptyLeaf) {
        Set<String> nonEmptyLeafIds = new HashSet<>();
        for (VodCategoryProximus category : nonEmptyLeaf) {
            nonEmptyLeafIds.add(category.getId());
        }
        updateCategoryContent(nonEmptyLeafIds);
    }

    public void updateCategoryContent(Set<String> nonEmptyLeaf) {
        Log.d(TAG, "Update leaf categories. Categories : " + nonEmptyLeaf.size());
        mNonEmptyLeafIds = nonEmptyLeaf;
    }

    private Set<String> getLeafCategories() {
        if (mNonEmptyLeafIds == null) {
            updateCategoryContent(mLeftCategoryInitializer.getLeafCategories());
        }
        return mNonEmptyLeafIds;
    }

    /**
     * @param categoryId The unique identifier of the category to search for.
     * @return The category with the given id or null if there is no such.
     */
    @Nullable
    public VodCategoryProximus getCategory(String categoryId) {
        return getCatMap().get(categoryId);
    }

    /**
     * @param categoryId The category id to search children for.
     * @return A list of sub-category which has at least one leaf category with content.
     */
    public List<VodCategoryProximus> getSubCategories(String categoryId) {
        List<VodCategoryProximus> retList = new ArrayList<>();

        List<VodCategoryProximus> children = getChildCatMap().get(categoryId);
        for (VodCategoryProximus child : ArrayUtils.notNull(children)) {
            if (hasContent(child.getId())) {
                retList.add(child);
            }
        }

        return retList;
    }

    /**
     * @param categoryId The category id to search content for.
     * @return True if the category has at least one leaf in the tree with content.
     */
    public boolean hasContent(String categoryId) {
        VodCategoryProximus category = getCatMap().get(categoryId);
        if (category == null) {
            return false;
        }

        if (category.isLeaf()) {
            return getLeafCategories().contains(categoryId);
        }

        List<VodCategoryProximus> children = getChildCatMap().get(categoryId);
        for (VodCategoryProximus child : ArrayUtils.notNull(children)) {
            if (hasContent(child.getId())) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return True if the category tree is not initialized yet.
     */
    @Override
    public boolean isCategoryTreeInitialized() {
        return mCatMap == null;
    }

    /**
     * Interface through which the non empty leaf category cache can be initialized.
     */
    public interface LeftCategoryInitializer {

        Set<String> getLeafCategories();
    }

    /**
     * Interface through which the category tree can be initialized.
     */
    public interface CategoryMapInitializer {

        List<VodGlobalCategoryProximus> getCategories();

    }
}
