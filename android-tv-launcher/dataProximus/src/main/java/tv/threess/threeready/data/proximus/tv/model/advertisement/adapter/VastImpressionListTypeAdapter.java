package tv.threess.threeready.data.proximus.tv.model.advertisement.adapter;

import tv.threess.threeready.data.proximus.tv.model.advertisement.VastIdCdata;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * Type adapter to merge all the "Impression" elements into a single list.
 * For each vast request call this needs a fresh XmlMapper, so the impressions
 * from different xml files are not merged into a single list.
 * The need for this adapter is because between the impressions elements there is the creative tag:
 *      <Impression id="id1"><![CDATA[ url1 ]]></Impression>
 *      <Creatives> ... </Creatives>
 *      <Impression id="id2"><![CDATA[ url2 ]]></Impression>
 */
public class VastImpressionListTypeAdapter extends XmlAdapter<List<VastIdCdata>, List<VastIdCdata>> {

    private final List<VastIdCdata> mList = new ArrayList<>();

    @Override
    public List<VastIdCdata> unmarshal(List<VastIdCdata> v) throws Exception {
        mList.addAll(v);
        return mList;
    }

    @Override
    public List<VastIdCdata> marshal(List<VastIdCdata> v) throws Exception {
        return v;
    }
}
