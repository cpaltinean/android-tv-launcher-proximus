package tv.threess.threeready.data.proximus.vod.model.response;

import java.util.List;

/**
 * BTA backend response to of a VOD detail request.
 * It contains the VOD and all the available variants.
 *
 * @author Barabas Attila
 * @since 2020.04.27
 */
public class VodItemDetailResponseProximus extends VodBaseResponseProximus {

    public List<VodItemProximus> getVODs(List<String> vodIds) {
        return getGroupedVodItems(vodIds, getPriceMap());
    }
}
