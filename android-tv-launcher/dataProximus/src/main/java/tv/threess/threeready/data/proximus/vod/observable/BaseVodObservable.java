package tv.threess.threeready.data.proximus.vod.observable;

import android.content.Context;

import java.io.IOException;
import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IBaseVodSeries;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.data.account.AccountLocalCache;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.proximus.vod.VodProxyProximus;
import tv.threess.threeready.data.vod.VodCache;

/**
 * Rx observable for VOD observables.
 *
 * @author David
 * @since 22.03.2022
 */
public abstract class BaseVodObservable<T> extends BaseContentObservable<T> {

    protected final VodProxyProximus mVodProxy = Components.get(VodProxyProximus.class);
    protected final VodCache mVodCache = Components.get(VodCache.class);
    protected final AccountLocalCache mAccountCache = Components.get(AccountLocalCache.class);

    public BaseVodObservable(Context context) {
        super(context);
    }

    /**
     * Base VOD Item is not including all needed parameters.
     * This method returns a complete VOD item.
     */
    protected IVodItem getCompleteVod(IBaseVodItem vod) throws IOException {
        return mVodProxy.getCompleteVod(vod.getId(), vod.getSeriesId(), mVodCache.getRentalExpiration());
    }

    /**
     * @param series the series to get the last played vod in
     * @return The last played episode from the series or null if there is no such..
     */
    protected IBaseVodItem getLastPlayedVodInSeries(IBaseVodSeries series) {
        IBaseVodItem lastWatchedEpisode = null;
        long lastUpdated = 0;

        List<IBaseVodItem> bookmarkEpisodes = mVodCache.getContinueWatchingEpisodes(series.getId());
        for (IBaseVodItem episode : ArrayUtils.notNull(bookmarkEpisodes)) {
            IBookmark bookmark = mAccountCache.getVodBookmark(episode);
            if (bookmark == null) {
                continue;
            }

            long episodeBookmarkStartPosition = bookmark.getTimestamp();
            if (episodeBookmarkStartPosition > lastUpdated) {
                lastWatchedEpisode = episode;
                lastUpdated = episodeBookmarkStartPosition;
            }
        }

        return lastWatchedEpisode;
    }
}