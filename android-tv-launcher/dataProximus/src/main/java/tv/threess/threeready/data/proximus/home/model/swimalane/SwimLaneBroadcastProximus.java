package tv.threess.threeready.data.proximus.home.model.swimalane;

import java.util.concurrent.TimeUnit;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.data.proximus.tv.model.BaseBroadcastProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.MetaInfoProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.ProgramMetaInfoKeyProximus;

/**
 * BTA response representing a TV broadcast which is coming from the swim lane API.
 *
 * @author Barabas Attila
 * @since 2019.10.30
 */
@XmlRootElement(name = "Airing")
public class SwimLaneBroadcastProximus extends BaseBroadcastProximus {

    @XmlElement(name = "ChannelExtID")
    private String mChannelId;

    @XmlElement(name = "StartDateTime")
    private long mStartTime;

    @XmlElement(name = "Duration")
    private int mDuration;

    @XmlElement(name = "SchTrailID")
    private String mSchTrailId;

    public SwimLaneBroadcastProximus() {
        super(EMPTY_PROGRAM_META_INFO);
    }

    @Override
    public String getChannelId() {
        return mChannelId;
    }

    @Override
    public long getStart() {
        return mStartTime;
    }

    @Override
    public long getEnd() {
        return mStartTime + TimeUnit.MINUTES.toMillis(mDuration);
    }

    @Override
    public String getId() {
        return BaseBroadcastProximus.buildBroadcastId(mChannelId, mSchTrailId);
    }

    @Override
    public boolean isNextToLive() {
        return false;
    }

   void setMetaInfo(MetaInfoProximus<ProgramMetaInfoKeyProximus> programMetaInfo) {
        mProgramMetaInfo = programMetaInfo;
    }
}
