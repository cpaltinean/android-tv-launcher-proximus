package tv.threess.threeready.data.proximus.pvr.model.series.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.proximus.pvr.model.series.response.RetentionTypesProximus;

/**
 * Proximus Adapter used for RetentionType.
 *
 * Created by Daniela Toma on 04/24/2020.
 */
public class RetentionTypeAdapterProximus extends XmlAdapter<String , RetentionTypesProximus> {
    public static final String TAG = Log.tag(RetentionTypeAdapterProximus.class);

    @Override
    public RetentionTypesProximus unmarshal(String v)  {
        switch (v) {
            default:
                Log.w(TAG, "Unknown retention type. " + v);
            case "S":
                return RetentionTypesProximus.ShortTermConservation;
            case "L":
                return RetentionTypesProximus.LongTermConservation;
        }
    }

    @Override
    public String marshal(RetentionTypesProximus v) {
        throw new IllegalStateException("Not implemented.");
    }
}
