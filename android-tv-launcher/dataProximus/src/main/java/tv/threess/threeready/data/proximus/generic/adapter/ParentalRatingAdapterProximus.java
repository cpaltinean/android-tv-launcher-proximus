package tv.threess.threeready.data.proximus.generic.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;

/**
 * XML type adapter to read parental control rating from backend response.
 *
 * @author Barabas Attila
 * @since 2020.04.23
 */
public class ParentalRatingAdapterProximus extends XmlAdapter<String, ParentalRating> {
    private static final String TAG = Log.tag(ParentalRatingAdapterProximus.class);

    @Override
    public ParentalRating unmarshal(String v) {
        return readRating(v);
    }

    @Override
    public String marshal(ParentalRating v) throws Exception {
        throw new Exception("Not implemented.");
    }

    public static ParentalRating readRating(String value) {
        switch (value) {
            default:
                try {
                    int ratingCode = Integer.parseInt(value);
                    return ParentalRating.valueOf(ratingCode);
                } catch (Exception e) {
                    Log.w(TAG,"Couldn't parse age rating : " + value, e);
                }
                return ParentalRating.Undefined;

            case "G":
                return ParentalRating.RatedAll;

            case "":
            case "NR":
                return ParentalRating.Undefined;
        }
    }
}
