package tv.threess.threeready.data.proximus.iam.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Model represents the software statement of the JWT payload token.
 *
 * @author Barabas Attila
 * @since 2020.06.25
 */
public class IAMSoftwareStatementProximus {

    @SerializedName("software_scopes")
    private String mSoftwareScopes;

    @SerializedName("software_id")
    private String mSoftwareId;

    @SerializedName("software_mode")
    private String mSoftwareMode;

    @SerializedName("software_redirect_uris")
    private List<String> mSoftwareRedirectUrls;

    @SerializedName("software_client_name")
    private String mClientName;

    @SerializedName("iss")
    private String mIss;

    @SerializedName("software_client_description")
    private String mClientDescription;

    @SerializedName("software_version")
    private String mSoftwareVersion;

    @SerializedName("software_environment")
    private String mSoftwareEnvironment;

    @SerializedName("exp")
    private long mExp;

    @SerializedName("iat")
    private long mIat;

    @SerializedName("jti")
    private String mJti;

    public static class Builder {
        private final IAMSoftwareStatementProximus mSoftwareStatement;

        public Builder() {
            mSoftwareStatement = new IAMSoftwareStatementProximus();
            mSoftwareStatement.mJti = UUID.randomUUID().toString();
            mSoftwareStatement.mSoftwareId = "STB";
            mSoftwareStatement.mSoftwareMode = "test";
            mSoftwareStatement.mClientName = "Proximus TV";
            mSoftwareStatement.mIss = "http://es-proximus-tv.be";
            mSoftwareStatement.mClientDescription = "TV services, provided by Proximus";
            mSoftwareStatement.mSoftwareEnvironment = "staging";
        }

        public Builder expiration(long timeMillis) {
            mSoftwareStatement.mExp = TimeUnit.MILLISECONDS.toSeconds(timeMillis);
            return this;
        }

        public Builder issued(long timeMillis) {
            mSoftwareStatement.mIat = TimeUnit.MILLISECONDS.toSeconds(timeMillis);
            return this;
        }

        public Builder softwareVersion(String softwareVersion) {
            mSoftwareStatement.mSoftwareVersion = softwareVersion;
            return this;
        }

        public Builder redirectUrl(String redirectUrl) {
            mSoftwareStatement.mSoftwareRedirectUrls = new ArrayList<>();
            mSoftwareStatement.mSoftwareRedirectUrls.add(redirectUrl);
            return this;
        }

        public Builder scope(String scope) {
            mSoftwareStatement.mSoftwareScopes = scope;
            return this;
        }

        public IAMSoftwareStatementProximus build() {
            return mSoftwareStatement;
        }

    }
}
