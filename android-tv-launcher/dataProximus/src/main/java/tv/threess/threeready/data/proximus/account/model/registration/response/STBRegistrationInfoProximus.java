package tv.threess.threeready.data.proximus.account.model.registration.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * BTA doc which holds the information about the registration info of the current device.
 *
 * @author Barabas Attila
 * @since 2020.04.14
 */
@XmlRootElement(name = "STBRegistrationInfo")
public class STBRegistrationInfoProximus implements Serializable {

    @XmlElement(name = "STBSerialNum")
    private String mSerialNumber;

    @XmlElement(name = "STBSerialNumAndroid")
    private String mAndroidSerialNumber;

    @XmlElement(name = "STBHardwareVersion")
    private String mHardwareVersion;

    @XmlElement(name = "STBSoftwareVersion")
    private String mSoftwareVersion;

    @XmlElement(name = "SubscriberAccountNumber")
    private String mAccountNumber;

    @XmlElement(name = "CASDeviceID")
    private String mCASDeviceID;

    @XmlElement(name = "STBName")
    private String mSTBName;

    @XmlElement(name = "ESN")
    private String mESN;

    @XmlElement(name = "BEPDeviceTypeExternalID")
    private String mBEPDeviceType;

    @XmlElement(name = "DeviceMDP")
    private String mDeviceMDP;

    @XmlElement(name = "STBUIVersion")
    private String mSTBUiVersion;

    public String getSerialNumber() {
        return mSerialNumber;
    }

    public String getAndroidSerialNumber() {
        return mAndroidSerialNumber;
    }

    public String getHardwareVersion() {
        return mHardwareVersion;
    }

    public String getSoftwareVersion() {
        return mSoftwareVersion;
    }

    public String getAccountNumber() {
        return mAccountNumber;
    }

    public String getCASDeviceID() {
        return mCASDeviceID;
    }

    public String getSTBName() {
        return mSTBName;
    }

    public String getESN() {
        return mESN;
    }

    public String getBEPDeviceType() {
        return mBEPDeviceType;
    }

    public String getDeviceMDP() {
        return mDeviceMDP;
    }

    public String getSTBUiVersion() {
        return mSTBUiVersion;
    }
}
