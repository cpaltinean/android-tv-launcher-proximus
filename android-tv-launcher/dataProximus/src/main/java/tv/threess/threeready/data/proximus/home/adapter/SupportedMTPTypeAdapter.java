package tv.threess.threeready.data.proximus.home.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.data.proximus.pvr.model.RecordingProximus;

/**
 * Type adapter to read the XML value as supported MTP type enum.
 *
 * @author David Bondor
 * @since 21.09.2022
 */
public class SupportedMTPTypeAdapter extends XmlAdapter<String, RecordingProximus.MTPID> {

    @Override
    public RecordingProximus.MTPID unmarshal(String value) {
        switch (value) {
            default:
                return RecordingProximus.MTPID.UNSUPPORTED;
            case "DASH_IPTV_HD_ENCRYPTED" :
                return RecordingProximus.MTPID.DASH_IPTV_HD_ENCRYPTED;
            case "DASH_IPTV_SD_ENCRYPTED" :
                return RecordingProximus.MTPID.DASH_IPTV_SD_ENCRYPTED;
            case "DASH_IPTV_HD" :
                return RecordingProximus.MTPID.DASH_IPTV_HD;
            case "DASH_IPTV_SD" :
                return RecordingProximus.MTPID.DASH_IPTV_SD;
        }
    }

    @Override
    public String marshal(RecordingProximus.MTPID v) {
        throw new IllegalStateException("Not implemented.");
    }
}
