package tv.threess.threeready.data.proximus.home.model.page;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.model.menu.MenuItem;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.home.model.page.PageType;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.data.proximus.home.model.AccessPointProximus;
import tv.threess.threeready.data.proximus.home.model.menu.AccessPointMenuItemProximus;
import tv.threess.threeready.data.proximus.home.model.module.AccessPointLinkSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.module.ContentWall2SwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.module.ContinueWatchingSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.module.EditorialAppSwimlaneModulePresenter;
import tv.threess.threeready.data.proximus.home.model.module.FeaturedAppsSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.module.InstalledAppsSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.module.NetflixSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.module.NotificationSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.module.NowOnTvSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.module.RecordingSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.module.RentalsSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.module.ReplayChannelSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.module.ReplayChannelWallSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.module.SwimlaneModuleConfigProximus;
import tv.threess.threeready.data.proximus.home.model.module.VodCategorySwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.module.WishlistSwimlaneModuleProximus;
import tv.threess.threeready.data.proximus.home.model.selector.SwimlaneDataSourceSelector;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneHubResponseProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Page config of an access point defined by the hub.
 *
 * @author Barabas Attila
 * @since 5/26/21
 */
public class AccessPointPageConfigProximus implements PageConfig {

    private final AccessPointProximus mAccessPoint;
    private final SwimLaneHubResponseProximus mHub;

    private final String mParentId;
    private final boolean mUseGroupId;

    private boolean mIsContentWall2;

    private final List<ModuleConfig> mModuleConfigs = new ArrayList<>();
    private final List<MenuItem> mMenuItems = new ArrayList<>();

    public AccessPointPageConfigProximus(AccessPointProximus accessPoint, SwimLaneHubResponseProximus hub, boolean useGroupId) {
        mAccessPoint = accessPoint;
        mHub = hub;
        mUseGroupId = useGroupId;
        AccessPointProximus parent = hub.getParent(accessPoint);
        mParentId = parent == null ? null : parent.getId();

        // Build page config with subpages.
        if (mAccessPoint.getType() == AccessPointProximus.Type.PageGroup) {
            List<AccessPointProximus> subpages = hub.getSubPages(accessPoint);
            subpages.sort(Comparator.comparingInt(AccessPointProximus::getListSort));
            for (AccessPointProximus subpage : subpages) {
                mMenuItems.add(new AccessPointMenuItemProximus(subpage));
            }
        }
    }

    /**
     * Method used for initializing the module config.
     */
    private void initializeModuleConfig() {
        // Build page config with modules.
        List<AccessPointProximus.ApSwimlane> swimlaneReferences = mAccessPoint.getSwimlaneReferences();
        if (swimlaneReferences == null) {
            return;
        }

        swimlaneReferences.sort((sl1, sl2) -> Integer.compare(sl1.getSortOrder(), sl2.getSortOrder()));
        String appLanguage = LocaleUtils.getApplicationLanguage();
        List<DataSourceSelector> selectors = new ArrayList<>();
        for (AccessPointProximus.ApSwimlane swimlaneReference : swimlaneReferences) {
            SwimLaneProximus swimlane = mHub.getSwimlane(swimlaneReference.getSwimlaneId());
            if (!isSwimlaneValid(swimlane, appLanguage)) {
                continue;
            }

            // Group selector swimlanes.
            if (swimlane.isSelectorSwimlane()) {
                // Convert static tiles to selector options.
                selectors.add(new SwimlaneDataSourceSelector(swimlane));
                continue;
            }

            mIsContentWall2 = isContentWall2(swimlane, selectors);
            mModuleConfigs.add(createSwimlaneModule(swimlane, mHub, selectors));
        }
    }

    protected ModuleConfig createSwimlaneModule(SwimLaneProximus swimlane,
                                              SwimLaneHubResponseProximus hub, List<DataSourceSelector> selectors) {
        switch (swimlane.getDataSourceType()) {
            default:
            case Remote:
                // Content wall 2.0 module
                if (isContentWall2(swimlane, selectors)) {
                    return new ContentWall2SwimlaneModuleProximus(swimlane, selectors);
                }
                return new SwimlaneModuleConfigProximus(swimlane);
            case ContinueWatching:
                return new ContinueWatchingSwimlaneModuleProximus(swimlane);
            case Recordings:
                return new RecordingSwimlaneModuleProximus(swimlane);
            case AccessPointSwimlane:
                return new AccessPointLinkSwimlaneModuleProximus(swimlane, hub);
            case ReplayChannelSwimlane:
                if (swimlane.getClientUiClass() == SwimLaneProximus.ClientUiClass.Channel_Wall) {
                    return new ReplayChannelWallSwimlaneModuleProximus(swimlane, hub);
                }
                return new ReplayChannelSwimlaneModuleProximus(swimlane, hub);
            case VodCategorySwimlane:
                return new VodCategorySwimlaneModuleProximus(swimlane);
            case InstalledApps:
                return new InstalledAppsSwimlaneModuleProximus(swimlane);
            case RecommendedApps:
                return new FeaturedAppsSwimlaneModuleProximus(swimlane);
            case EditorialApps:
                return new EditorialAppSwimlaneModulePresenter(swimlane);
            case NowOnTv:
                return new NowOnTvSwimlaneModuleProximus(swimlane);
            case Rentals:
                return new RentalsSwimlaneModuleProximus(swimlane);
            case Wishlist:
                return new WishlistSwimlaneModuleProximus(swimlane);
            case SystemNotification:
                return new NotificationSwimlaneModuleProximus(swimlane);
            case Netflix:
                return new NetflixSwimlaneModuleProximus(swimlane);
        }
    }

    @Override
    public UILog.Page getPageName() {
        if (mIsContentWall2) {
            return UILog.Page.ContentWallFullContentFilterPage;
        }

        return PageConfig.super.getPageName();
    }

    @Override
    public String getReportName() {
        return mAccessPoint.getName();
    }

    @Override
    public String getReportReference() {
        return mAccessPoint.getId();
    }

    @Override
    public String getId() {
        return mUseGroupId ? mAccessPoint.getGroupId() : mAccessPoint.getId();
    }

    @Override
    public String getTitle() {
        return mAccessPoint.getDisplayNames().getLocalizedNameOrFirst();
    }

    @Override
    public PageType getType() {
        return PageType.MODULAR_PAGE;
    }

    @Nullable
    @Override
    public String getParent() {
        return mParentId;
    }

    @Nullable
    @Override
    public List<ModuleConfig> getModules() {
        if (ArrayUtils.isEmpty(mModuleConfigs)) {
            initializeModuleConfig();
        }
        return mModuleConfigs;
    }

    @Override
    public boolean hasSubPages() {
        for (ModuleConfig module : ArrayUtils.notNull(mModuleConfigs)) {
            if (!ArrayUtils.isEmpty(module.getSelectorList())) {
                return true;
            }
        }
        return !ArrayUtils.isEmpty(mMenuItems);
    }

    @Nullable
    @Override
    public List<MenuItem> getSubMenus() {
        return mMenuItems;
    }

    protected boolean isContentWall2(SwimLaneProximus swimlane, List<DataSourceSelector> selectors) {
        return selectors.size() > 0 && swimlane.getClientUiClass() == SwimLaneProximus.ClientUiClass.EPG_Wall;
    }

    /**
     * Swimlane must be not null and must match to the UI language
     */
    private boolean isSwimlaneValid(SwimLaneProximus swimlane, @NonNull String appLanguage) {
        if (swimlane == null) {
            return false;
        }
        if (swimlane.getUiLanguage() == null) {
            return true;
        }
        return swimlane.getUiLanguage().equals(appLanguage);
    }
}
