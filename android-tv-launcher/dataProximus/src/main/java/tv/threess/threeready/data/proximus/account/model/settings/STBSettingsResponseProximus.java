package tv.threess.threeready.data.proximus.account.model.settings;

import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.data.proximus.account.model.account.STBProperties;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;
import tv.threess.threeready.data.proximus.tv.adapter.STBSettingsMapAdapter;

/**
 * BTA response with the device's settings
 *
 * @author Solyom Zsolt
 * @since 2020.07.09
 */

@XmlRootElement(name = "BTAResponse")
public class STBSettingsResponseProximus extends BTAResponseProximus {

    @XmlElement(name = "Property")
    @XmlElementWrapper(name = "Properties")
    @XmlJavaTypeAdapter(STBSettingsMapAdapter.class)
    private Map<STBProperties, String> mSTBProperty;

    public Map<STBProperties, String> getSTBProperties() {
        return mSTBProperty;
    }
}
