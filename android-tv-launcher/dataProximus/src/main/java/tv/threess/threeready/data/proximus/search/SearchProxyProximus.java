package tv.threess.threeready.data.proximus.search;

import android.app.Application;

import java.io.IOException;
import java.util.List;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.search.model.LocalSearchType;
import tv.threess.threeready.api.search.model.SearchSuggestion;
import tv.threess.threeready.api.search.model.SearchTerm;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.proximus.generic.RetrofitServicesProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneContentResponseProximus;
import tv.threess.threeready.data.proximus.search.model.SearchType;
import tv.threess.threeready.data.proximus.search.retrofit.BTASearchRetrofitProximus;
import tv.threess.threeready.data.proximus.search.retrofit.response.SearchSuggestionsResponse;
import tv.threess.threeready.data.proximus.vod.model.response.VodShopLanguageProximus;
import tv.threess.threeready.data.search.SearchProxy;

/**
 * SearchProxy class implementation for the proximus backend.
 *
 * @author Barabas Attila
 * @since 2018.06.20
 */
public class SearchProxyProximus implements SearchProxy, Component {
    private static final String TAG = Log.tag(SearchProxyProximus.class);

    private static final int CLIENT_VERSION = 7;

    private final BTASearchRetrofitProximus mBtaSearchRetrofit =
            Components.get(RetrofitServicesProximus.class).getSearchRetrofit();

    protected final Application mApplication;

    public SearchProxyProximus(Application application) {
        mApplication = application;
    }

    @Override
    public List<SearchSuggestion> getSuggestions(String searchTerm) throws IOException {
        VodShopLanguageProximus shopLanguage = new VodShopLanguageProximus(
                SettingsProximus.shopLanguage.getList()
        );
        SearchSuggestionsResponse response = RetrofitServicesProximus.execute(
                mBtaSearchRetrofit.getSearchSuggestions(searchTerm, CLIENT_VERSION, shopLanguage)
        );
        return response.getSuggestionList();
    }

    /**
     * Perform a text based search for broadcasts with the given term.
     *
     * @param searchTerm The text for which the search should be triggered.
     * @return The search results for the given term.
     * @throws IOException In case of errors.
     */
    private SwimLaneContentResponseProximus epgUiLanguageSearch(SearchTerm searchTerm) throws IOException {
        boolean french = LocaleUtils.FRENCH_LANGUAGE_CODE.equals(LocaleUtils.getApplicationLanguage());
        SearchType searchType = searchTerm.isSuggestion()
            ? french ? SearchType.SuggestSearchProgramsFr : SearchType.SuggestSearchProgramsNl
            : french ? SearchType.DirectSearchProgramsFr : SearchType.DirectSearchProgramsNl;

        return RetrofitServicesProximus.execute(mBtaSearchRetrofit.doSearch(
                searchTerm.getTerm(), CLIENT_VERSION, searchType.getValue()
        ));
    }

    /**
     * Perform a text based search for broadcasts with the given term.
     *
     * @param searchTerm The text for which the search should be triggered.
     * @return The search results for the given term.
     * @throws IOException In case of errors.
     */
    private SwimLaneContentResponseProximus epgNonUiLanguageSearch(SearchTerm searchTerm) throws IOException {
        boolean french = LocaleUtils.FRENCH_LANGUAGE_CODE.equals(LocaleUtils.getApplicationLanguage());
        SearchType searchType = searchTerm.isSuggestion()
            ? french ? SearchType.SuggestSearchProgramsNotFr : SearchType.SuggestSearchProgramsNotNl
            : french ? SearchType.DirectSearchProgramsNotFr : SearchType.DirectSearchProgramsNotNl;

        return RetrofitServicesProximus.execute(mBtaSearchRetrofit.doSearch(
                searchTerm.getTerm(), CLIENT_VERSION, searchType.getValue()
        ));
    }

    /**
     * Perform a text based search for movies with the given term.
     *
     * @param searchTerm The text for which the search should be triggered.
     * @return The search results for the given term.
     * @throws IOException In case of errors.
     */
    private SwimLaneContentResponseProximus vodSearch(SearchTerm searchTerm) throws IOException {
        VodShopLanguageProximus shopLanguage = new VodShopLanguageProximus(
                SettingsProximus.shopLanguage.getList()
        );

        SearchType searchType = searchTerm.isSuggestion() ? SearchType.SuggestSearchVod : SearchType.DirectSearchVod;
        return RetrofitServicesProximus.execute(mBtaSearchRetrofit.doSearch(
                searchTerm.getTerm(), shopLanguage, CLIENT_VERSION, searchType.getValue()
        ));
    }

    @Override
    public List<IContentItem> broadcastVoiceSearch(SearchTerm searchTerm, int start, int limit) throws IOException {
        VodShopLanguageProximus shopLanguage = new VodShopLanguageProximus(
                SettingsProximus.shopLanguage.getList()
        );
        SearchType searchType = SearchType.VoiceSearchEpg;
        SwimLaneContentResponseProximus response = RetrofitServicesProximus.execute(mBtaSearchRetrofit.doSearch(
                searchTerm.getTerm(), shopLanguage, CLIENT_VERSION, searchType.getValue()
        ));

        return (List<IContentItem>) (List<?>) response.getContentItems();
    }

    @Override
    public List<IContentItem> vodVoiceSearch(SearchTerm searchTerm, int start, int limit) throws IOException {
        VodShopLanguageProximus shopLanguage = new VodShopLanguageProximus(
                SettingsProximus.shopLanguage.getList()
        );
        SearchType searchType = SearchType.VoiceSearchVod;
        SwimLaneContentResponseProximus response = RetrofitServicesProximus.execute(mBtaSearchRetrofit.doSearch(
                searchTerm.getTerm(), shopLanguage, CLIENT_VERSION, searchType.getValue()
        ));

        return (List<IContentItem>) (List<?>) response.getContentItems();
    }

    @Override
    public IContentItem contentVoicePlaySearch(SearchTerm searchTerm) throws IOException {
        VodShopLanguageProximus shopLanguage = new VodShopLanguageProximus(
                SettingsProximus.shopLanguage.getList()
        );

        SearchType searchType = SearchType.VoicePlayItem;
        SwimLaneContentResponseProximus response = RetrofitServicesProximus.execute(mBtaSearchRetrofit.doSearch(
                searchTerm.getTerm(), shopLanguage, CLIENT_VERSION, searchType.getValue()
        ));

        List<? extends IBaseContentItem> items = response.getContentItems();
        return items.isEmpty() ? null : (IContentItem) items.get(0);
    }

    @Override
    public ModuleData<IBaseContentItem> contentSearch(ModuleConfig config, int start, int count) throws IOException {
        LocalSearchType searchType = LocalSearchType.valueOf(config.getDataSource().getParams().getFilter(
                ModuleFilterOption.Mixed.Type.NAME)
        );

        SearchTerm term = config.getDataSource().getParams()
                .getFilter(ModuleFilterOption.Mixed.SearchTerm.NAME, SearchTerm.class);

        SwimLaneContentResponseProximus response;
        switch (searchType) {
            case BroadcastUiLanguage:
                response = epgUiLanguageSearch(term);
                break;

            case BroadcastNonUiLanguage:
                response = epgNonUiLanguageSearch(term);
                break;

            case Vod:
                response = vodSearch(term);
                break;

            default:
                Log.d(TAG, "Search type not implemented.");
                return null;
        }

        return new ModuleData<>(config, response.getContentItems(), response.getTotalCount() > count);
    }
}
