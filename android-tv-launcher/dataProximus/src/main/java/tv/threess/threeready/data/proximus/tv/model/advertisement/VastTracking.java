package tv.threess.threeready.data.proximus.tv.model.advertisement;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import tv.threess.threeready.api.tv.model.TvAdReplacement.Event;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * Tracking contains a URI to a location or file that the video player should request when a
 * specific named event occurs during the playback of the Nonlinear creative (the event name is
 * passed as an attribute of this element); the server can use requests to this URL for tracking
 * metrics associated with these events.
 */
public class VastTracking {
    @XmlAttribute(name = "event")
    private Event mEvent;

    @XmlValue
    @JacksonXmlCData
    private String mUrl;

    public Event getEvent() {
        return mEvent;
    }

    public String getUrl() {
        return mUrl;
    }

    @Override
    public String toString() {
        return "VastTracking{" +
                "Event=" + mEvent +
                ", Url='" + mUrl + '\'' +
                '}';
    }
}
