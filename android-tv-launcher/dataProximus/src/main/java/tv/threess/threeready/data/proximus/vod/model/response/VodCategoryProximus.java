package tv.threess.threeready.data.proximus.vod.model.response;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.data.proximus.generic.adapter.ParentalRatingAdapterProximus;

/**
 * Base model for VOD categories for both coming from BTA and global install.
 *
 * @author Barabas Attila
 * @since 2020.04.28
 */
public abstract class VodCategoryProximus {
    private static final String SERIES_LAYOUT_NAME = "Series_7";

    @XmlElement(name = "Poster")
    @XmlElementWrapper(name = "CategoryPosters")
    private List<CategoryPoster> mCategoryPosters;

    @XmlElement(name = "CategoryRating")
    private CategoryRating mCategoryRating;

    @XmlElement(name = "Layout")
    @XmlElementWrapper(name = "ClientUiLayouts")
    private List<Layout> mLayouts;

    @XmlElementWrapper(name = "SortingOptions")
    @XmlElement(name = "SortingOption")
    private List<VodCategorySortOptionProximus> mSortOptions;

    /**
     * @return Unique external identifier of the vod category.
     */
    public abstract String getId();

    /**
     * @return True if the category doesn't contain other subcategories.
     */
    public abstract boolean isLeaf();

    /**
     * @return Language independent name of the category.
     */
    public abstract String getName();

    /**
     * @return Localized title of the of the vod category.
     */
    public abstract String getTitle();

    public List<VodCategorySortOptionProximus> getSortOptions() {
        return mSortOptions;
    }

    /**
     * @return The sort option with the given name or null if there is no such.
     */
    @Nullable
    public VodCategorySortOptionProximus getSorting(String sortValue) {
        for (VodCategorySortOptionProximus sorting : ArrayUtils.notNull(mSortOptions)) {
            if (Objects.equals(sorting.getValue(), sortValue)) {
                return sorting;
            }
        }
        return null;
    }

    public ParentalRating getParentalRating() {
        return mCategoryRating == null ? ParentalRating.Undefined : mCategoryRating.mParentalRating;
    }

    public List<IImageSource> getImageSources() {
        return Collections.singletonList(getLocalizedImageOrFirst());
    }

    @Nullable
    public IImageSource getLocalizedImageOrFirst() {
        return getLocalizedImageOrFirst(LocaleUtils.getApplicationLanguage());
    }

    @Nullable
    public IImageSource getLocalizedImageOrFirst(String language) {
        if (mCategoryPosters == null) {
            return null;
        }

        // Filter image with empty name.
        List<CategoryPoster> posters = mCategoryPosters.stream()
                .filter((p) -> !TextUtils.isEmpty(p.mName.trim()))
                .collect(Collectors.toList());

        for (CategoryPoster categoryPoster : posters) {
            if (language.equalsIgnoreCase(categoryPoster.getLanguage())) {
                return categoryPoster;
            }
        }

        if (!posters.isEmpty()) {
            return posters.get(0);
        }

        return null;
    }

    /**
     * @return True if the category marked as it contains series.
     */
    public boolean hasSeriesLayout() {
        if (mLayouts == null) {
            return false;
        }

        for (Layout layout : mLayouts) {
            if (SERIES_LAYOUT_NAME.equalsIgnoreCase(layout.mName)) {
                return true;
            }
        }

        return false;
    }

    @XmlRootElement(name = "DisplayName")
    private static class CategoryDetail {

        @XmlAttribute(name = "Name")
        private String mDisplayName;

        @XmlAttribute(name = "LanguageType")
        private String mLanguage;
    }

    @XmlRootElement(name = "CategoryPosters")
    private static class CategoryPoster implements IImageSource {

        @XmlAttribute(name = "name")
        private String mName;

        @XmlAttribute(name = "language")
        private String mLanguage;

        @Override
        public String getUrl() {
            return mName;
        }

        @Override
        public int getHeight() {
            return 0;
        }

        @Override
        public int getWidth() {
            return 0;
        }

        @Override
        public Type getType() {
            return Type.DEFAULT;
        }

        public String getLanguage() {
            return mLanguage;
        }
    }

    @XmlRootElement(name = "CategoryRating")
    private static class CategoryRating implements Serializable {

        @XmlAttribute(name = "Code")
        @XmlJavaTypeAdapter(ParentalRatingAdapterProximus.class)
        private ParentalRating mParentalRating;
    }

    @XmlRootElement(name = "Layout")
    private static class Layout implements Serializable {
        @XmlElement(name = "Name")
        private String mName;
    }
}
