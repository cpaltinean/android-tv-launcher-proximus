package tv.threess.threeready.data.proximus.account.model.boot;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.tv.model.ImageSource;
import tv.threess.threeready.data.proximus.generic.model.BTAResponseProximus;


/**
 * This class holds WEBACCESS file parcing data.
 *
 * Created by Bartalus Csaba - Zsolt, since 18.02.2022
 */

@XmlRootElement(name = "DocumentUpdate")
public class WebAccessProximus extends BTAResponseProximus {

    private static final String QR_CODE_ID = "GDPR_QR_Code";

    @XmlElement(name = "WebAccess")
    private WebAccess mWebAccess;

    @XmlElement(name = "version")
    private String version;

    /**
     * @return from web access file link of the GDPR qr code.
     */
    public IImageSource getConsentQrCode() {
        for (Group group: ArrayUtils.notNull(mWebAccess.groups)) {
            for (ContentDisplay contentDisplay: ArrayUtils.notNull(group.mContentDisplay)) {
                if (QR_CODE_ID.equals(contentDisplay.mExternalId)) {
                    return new ImageSource(contentDisplay.mIcon, IImageSource.Type.APP);
                }
            }
        }

        return null;
    }

    @XmlRootElement(name = "WebAccess")
    private static class WebAccess {

        @XmlElement(name = "Group")
        List<Group> groups;
    }

    @XmlRootElement(name = "Group")
    private static class Group {

        @XmlElement(name = "Name")
        String mGroupName;

        @XmlElement(name = "ContentDisplay")
        List<ContentDisplay> mContentDisplay;
    }

    @XmlRootElement(name = "ContentDisplay")
    private static class ContentDisplay {
        @XmlElement(name = "icon")
        String mIcon;

        @XmlElement(name = "ExternalID")
        String mExternalId;

        @XmlElement(name = "Package")
        List<Package> mPackages;
    }

    @XmlRootElement(name = "Package")
    static class Package {
        @XmlElement(name = "id")
        int mId;
    }
}
