package tv.threess.threeready.data.proximus.home.model.module;

import androidx.annotation.Nullable;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.ApiConfig;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;
import tv.threess.threeready.api.home.model.module.ModuleDataSourcePaginationParams;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceService;
import tv.threess.threeready.api.home.model.module.ModuleDataType;
import tv.threess.threeready.api.home.model.module.ModuleType;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategoryProximus;

/**
 * Module config for a VOD category.
 *
 * @author Barabas Attila
 * @since 5/24/21
 */
public class VodCategoryDetailModuleConfigProximus implements ModuleConfig {

    private final ModuleType mModuleType;
    protected final VodCategoryProximus mCategory;
    private final ModuleDataSourcePaginationParams mPagination;

    public VodCategoryDetailModuleConfigProximus(VodCategoryProximus category, ModuleType moduleType) {
        mCategory = category;
        mModuleType = moduleType;
        mPagination = new ModuleDataSourcePaginationParams() {
            @Override
            public int getSize() {
                return Components.get(ApiConfig.class).getDefaultPageSize();
            }

            @Override
            public int getThreshold() {
                return getSize() / 2;
            }
        };
    }

    @Override
    public String getReportName() {
        return mCategory.getName();
    }

    @Override
    public String getReportReference() {
        return mCategory.getId();
    }

    @Nullable
    @Override
    public String getTitle() {
        return mCategory.getTitle();
    }

    @Override
    public ModuleType getType() {
        return mModuleType;
    }

    @Override
    public ModuleDataType getDataType() {
        return ModuleDataType.MIXED;
    }

    @Override
    public ModuleCardVariant getCardVariant() {
        // Portrait VOD movie collection
        return getType() == ModuleType.COLLECTION && mCategory.isLeaf()
                ? ModuleCardVariant.A2 : ModuleCardVariant.A1;
    }

    @Override
    public ModuleDataSourcePaginationParams getPagination() {
        return mPagination;
    }

    @Nullable
    @Override
    public ModuleDataSource getDataSource() {
        return mDataSource;
    }

    private final ModuleDataSource mDataSource = new ModuleDataSource() {
        @Override
        public ModuleDataSourceService getService() {
            return ModuleDataSourceService.TV;
        }

        @Override
        public ModuleDataSourceMethod getMethod() {
            return ModuleDataSourceMethod.TV_PROGRAM_INDEX;
        }
    };

    public VodCategoryProximus getCategory() {
        return mCategory;
    }

    public String getCategoryId() {
        return mCategory.getId();
    }
}
