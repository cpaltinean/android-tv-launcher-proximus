package tv.threess.threeready.data.proximus.tv.adapter;

import tv.threess.threeready.api.generic.helper.TimeUtils;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Type adapter to read String field (yyyy-MM-dd HH:mm:ss) as timestamp.
 */
public class AdTimestampTypeAdapter extends XmlAdapter<String, Long> {
    @Override
    public Long unmarshal(String time) throws Exception {
        return TimeUtils.getDateTimeParser().parse(time);
    }

    @Override
    public String marshal(Long v) throws Exception {
        throw new IllegalStateException("Not implemented.");
    }
}
