package tv.threess.threeready.data.proximus.home.model.page;

import tv.threess.threeready.api.home.model.module.ModuleType;
import tv.threess.threeready.api.home.model.page.ModulePageConfig;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.data.proximus.home.model.module.VodCategoryDetailModuleConfigProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategoryProximus;

/**
 * Page configuration for a leaf VOD category.
 * It contains a single collection module
 *
 * @author Barabas Attila
 * @since 6/15/21
 */
public class CategoryPageConfig extends ModulePageConfig {
    public CategoryPageConfig(VodCategoryProximus category) {
        super(new VodCategoryDetailModuleConfigProximus(category, ModuleType.COLLECTION),
                category.isLeaf() ? UILog.Page.StoreFullContentFilterPage : UILog.Page.StoreSubcategoryBrowsing);
    }
}
