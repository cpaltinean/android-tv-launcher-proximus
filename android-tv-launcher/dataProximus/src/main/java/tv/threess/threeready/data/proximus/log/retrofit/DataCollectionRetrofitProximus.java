package tv.threess.threeready.data.proximus.log.retrofit;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Tag;
import retrofit2.http.Url;
import tv.threess.threeready.data.proximus.home.model.editorial.BannerFeedbackRequest;
import tv.threess.threeready.data.proximus.log.model.SendEventsRequest;
import tv.threess.threeready.data.proximus.log.model.SendMetricsRequest;

/**
 * Retrofit interface to send events/metrics to data collection API.
 *
 * @author Zsolt Bokor_
 * @since 2020.04.29
 */
public interface DataCollectionRetrofitProximus {

    String BASE_URL = "https://collector.idtv.belgacom.net/";

    @POST
    Call<String> sendEvents(@Url String url, @HeaderMap Map<String, String> headers,
                            @Tag Long timeout, @Body SendEventsRequest sendEventsRequest);

    @POST
    Call<String> sendMetrics(@Url String url, @HeaderMap Map<String, String> headers,
                            @Tag Long timeout, @Body SendMetricsRequest sendMetricsRequest);

    @POST
    Call<ResponseBody> sendBannerFeedback(@Url String url, @Body BannerFeedbackRequest feedback);
}
