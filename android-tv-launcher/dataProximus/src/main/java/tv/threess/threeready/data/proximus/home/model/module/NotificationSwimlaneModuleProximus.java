package tv.threess.threeready.data.proximus.home.model.module;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceParams;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceService;
import tv.threess.threeready.api.home.model.module.ModuleDataType;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Module config to display critical system notifications.
 *
 * @author Barabas Attila
 * @since 6/30/21
 */
public class NotificationSwimlaneModuleProximus extends SwimlaneModuleConfigProximus {

    private final ModuleDataSourceParams mParams;

    public NotificationSwimlaneModuleProximus(SwimLaneProximus swimlane) {
        super(swimlane);
        mParams = new CriticalNotificationDataSourceParams(swimlane);
    }

    @Override
    public ModuleCardVariant getCardVariant() {
        return ModuleCardVariant.D1;
    }

    @Override
    public ModuleDataType getDataType() {
        return ModuleDataType.SYSTEM_NOTIFICATIONS;
    }

    @Nullable
    @Override
    public ModuleDataSource getDataSource() {
        return mDataSource;
    }

    private final ModuleDataSource mDataSource = new ModuleDataSource() {

        @Override
        public ModuleDataSourceService getService() {
            return ModuleDataSourceService.GMS;
        }

        @Override
        public ModuleDataSourceMethod getMethod() {
            return ModuleDataSourceMethod.ANDROID_TV_NOTIFICATIONS;
        }

        @Nullable
        @Override
        public ModuleDataSourceParams getParams() {
            return mParams;
        }
    };

    private static class CriticalNotificationDataSourceParams extends SwimlaneModuleDataSourceParams {
        public CriticalNotificationDataSourceParams(SwimLaneProximus swimlane) {
            super(swimlane);
        }

        @Nullable
        @Override
        public String getFilter(String key) {
            switch (key) {
                default:
                    return null;
                case ModuleFilterOption.Notification.Priority.NAME:
                    return ModuleFilterOption.Notification.Priority.Value.CRITICAL;
            }
        }
    }
}
