package tv.threess.threeready.data.proximus.home.model.editorial;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemStyle;
import tv.threess.threeready.api.home.model.module.EditorialItemStyle.VariantSize.Variant;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategoryProximus;

/**
 * Config for an editorial item which opens VOD category browsing.
 *
 * @author Barabas Attila
 * @since 5/24/21
 */
public class VodCategoryEditorialItemProximus implements EditorialItem {

    private final VodCategoryProximus mCategory;
    private final EditorialItemStyle.Size mSize;

    public VodCategoryEditorialItemProximus(VodCategoryProximus category, SwimLaneProximus.ClientUiClass uiClass) {
        this(category, uiClass == SwimLaneProximus.ClientUiClass.VodCat_Small
                ? Variant.Small : Variant.Large);
    }

    private VodCategoryEditorialItemProximus(VodCategoryProximus category, Variant variant) {
        mCategory = category;
        mSize = new EditorialItemStyle.VariantSize(variant);
    }

    @Override
    public String getId() {
        return mCategory.getId();
    }

    @Nullable
    @Override
    public EditorialItemAction getAction() {
        return mAction;
    }

    @Nullable
    @Override
    public String getFallbackTitle() {
        return mCategory.getTitle();
    }

    @Nullable
    @Override
    public EditorialItemStyle getStyle() {
        return mItemStyle;
    }


    private final EditorialItemStyle mItemStyle = new EditorialItemStyle() {

        @Nullable
        @Override
        public IImageSource getBackgroundImage() {
            return mCategory.getLocalizedImageOrFirst();
        }

        @Nullable
        @Override
        public Size getWidth() {
            return mSize;
        }

        @Nullable
        @Override
        public Size getHeight() {
            return mSize;
        }
    };

    private final EditorialItemAction mAction = new EditorialItemAction.CategoryEditorialItemAction() {
        @Nullable
        @Override
        public String getCategoryId() {
            return mCategory.getId();
        }
    };
}
