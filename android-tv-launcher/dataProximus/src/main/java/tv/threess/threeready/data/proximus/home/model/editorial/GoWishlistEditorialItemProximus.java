package tv.threess.threeready.data.proximus.home.model.editorial;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.data.proximus.home.model.module.WatchlistCollectionModuleProximus;
import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Swimlane tile which opens a collection with the watchlist.
 *
 * @author Barabas Attila
 * @since 6/18/21
 */
public class GoWishlistEditorialItemProximus extends SwimlaneEditorialItemProximus {
    private final ModuleConfig mModuleConfig = new WatchlistCollectionModuleProximus();

    public GoWishlistEditorialItemProximus(SwimLaneProximus swimLane,
                                           SwimLaneProximus.SwimlaneTile swimlaneTile, Integer position) {
        super(swimLane, swimlaneTile, position);
    }

    @Nullable
    @Override
    public EditorialItemAction getAction() {
        return mItemAction;
    }

    private final EditorialItemAction mItemAction = (EditorialItemAction.ModuleEditorialItemAction) () -> mModuleConfig;
}
