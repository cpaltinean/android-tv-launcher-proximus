package tv.threess.threeready.data.proximus.account;

import androidx.annotation.WorkerThread;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.BatchSettingEditor;
import tv.threess.threeready.api.account.setting.ReadableSetting;
import tv.threess.threeready.api.account.setting.SettingEditor;
import tv.threess.threeready.api.account.setting.SettingProperty;
import tv.threess.threeready.data.account.SettingsAccessProxy;

/**
 * Persistent cache to store proximus backend connection related data.
 *
 * @author Barabas Attila
 * @since 2020.03.18
 */
public enum SessionProximus implements SettingProperty, ReadableSetting {
    environment,
    securedBtaBaseUrl,
    unsecuredBtaBaseUrl,
    epgBaseUrl,
    epgSplicePath,
    availableVersionUrl,
    globalInstallUrl,
    globalInstallVersion,
    webAccessInstallUrl,

    // IAM
    iamBypassAllowed,
    iamAudience,
    iamRedirectUrl,
    iamRegisterUrl,
    iamAccessTokenUrl,
    iamScopes,
    iamGrantType,
    iamJWTKeyId,
    iamSSAKeyId,
    iamAccessToken,
    iamClientId,
    iamClientSecret,
    securedPaths,

    // VQE
    vqeChannelConfigUrl,
    vqeNetworkConfigUrl,
    vcasServerAddress,

    // Banner feedback
    bannerFeedbackUrl,

    operatorIsolationOverrideUrl,
    appInstanceId;

    private static final SettingsAccessProxy sSettingsAccess = Components.get(SettingsAccessProxy.class);

    @Override
    public String getName() {
        return name();
    }

    @Override
    public String getValue() {
        return sSettingsAccess.getSession(this);
    }

    /**
     * @return Editor to update the value of the key in the cache.
     */
    @WorkerThread
    public SettingEditor<SessionProximus> edit() {
        return new SettingEditor<>(this, sSettingsAccess::putSession);
    }

    /**
     * @return Editor to updated multiple values in a batch.
     */
    @WorkerThread
    public static BatchSettingEditor<SessionProximus> batchEdit() {
        return new BatchSettingEditor<>(sSettingsAccess::putSession);
    }
}
