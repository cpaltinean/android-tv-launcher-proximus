package tv.threess.threeready.data.proximus.vod.observable;

import android.content.Context;

import io.reactivex.ObservableEmitter;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.wrapper.VodRentOffers;

/**
 * This observable emits the {@link VodRentOffers} object
 * to the user interface.
 */
public class VodVariantRentOfferObservable extends BaseVodRentOfferObservable<VodRentOffers> {

    public VodVariantRentOfferObservable(Context context, IBaseVodItem vod) {
        super(context, vod);
    }

    protected void publishData(ObservableEmitter<VodRentOffers> emitter, VodRentOffers rentOffers) {
        emitter.onNext(rentOffers);
        emitter.onComplete();
    }
}
