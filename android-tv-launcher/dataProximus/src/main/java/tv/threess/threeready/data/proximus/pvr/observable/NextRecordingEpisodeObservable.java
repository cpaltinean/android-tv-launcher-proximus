package tv.threess.threeready.data.proximus.pvr.observable;

import android.content.Context;
import android.net.Uri;

import java.util.List;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.BingeWatchingInfo;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.pvr.PvrCache;
import tv.threess.threeready.data.pvr.PvrContract;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Rx observable to return the next recording episode from a series.
 *
 * @author David Bondor
 * @since 19.07.2022
 */
public class NextRecordingEpisodeObservable extends BaseContentObservable<BingeWatchingInfo<IRecordingSeries, IRecording>> {
    private final String TAG = Log.tag(NextRecordingEpisodeObservable.class);
    private final PvrCache mPvrCache = Components.get(PvrCache.class);
    private final IRecording mEpisode;
    private final IRecordingSeries mSeries;

    public NextRecordingEpisodeObservable(Context context, IRecording episode, IRecordingSeries series) {
        super(context);
        mEpisode = episode;
        mSeries = series;
    }

    @Override
    public void subscribe(ObservableEmitter<BingeWatchingInfo<IRecordingSeries, IRecording>> e) throws Exception {
        super.subscribe(e);

        if (mEpisode == null) {
            throw new Exception("No episode available.");
        }

        // Observe status changes.
        registerObserver(true,
                PvrContract.buildRecordingUriForSeries(mEpisode.getSeriesId()));

        publishNextEpisode(mSeries == null ? mPvrCache.getSeriesRecording(mEpisode.getSeriesId()) : mSeries);
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<BingeWatchingInfo<IRecordingSeries, IRecording>> observableEmitter) throws Exception {
        publishNextEpisode(mPvrCache.getSeriesRecording(mEpisode.getSeriesId()));
    }

    private void publishNextEpisode(IRecordingSeries series) throws Exception {
        if (RxUtils.isDisposed(mEmitter)) {
            // Already canceled.
            return;
        }

        if (series == null) {
            throw new Exception("No series available with id: " + mEpisode.getSeriesId());
        }

        List<IRecording> episodeList = series.getEpisodesSorted();
        if (ArrayUtils.isEmpty(episodeList)) {
            Log.d(TAG, "There are no episodes for this series with the following id: " + series.getId());
            return;
        }

        IRecording nextEpisode = null;
        for (int i = 0; i < episodeList.size() - 1; ++i) {
            if (mEpisode.getId().equals(episodeList.get(i).getId())) {
                nextEpisode = episodeList.get(i + 1);
                break;
            }
        }

        if (nextEpisode == null) {
            Log.d(TAG, "The received next episode is null.");
            return;
        }

        mEmitter.onNext(new BingeWatchingInfo<>(series, nextEpisode));
    }
}