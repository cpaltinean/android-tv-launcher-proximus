package tv.threess.threeready.data.proximus.pvr;

import android.app.Application;

import java.io.IOException;
import java.util.List;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.data.pvr.PvrProxy;
import tv.threess.threeready.api.pvr.model.IRecordingQuota;
import tv.threess.threeready.data.proximus.generic.RetrofitServicesProximus;
import tv.threess.threeready.data.proximus.pvr.model.request.RecordingAddRequestProximus;
import tv.threess.threeready.data.proximus.pvr.model.request.RecordingDeleteRequest;
import tv.threess.threeready.data.proximus.pvr.model.request.RecordingDeleteSeriesRequest;
import tv.threess.threeready.data.proximus.pvr.model.request.RecordingScheduleSeriesRequest;
import tv.threess.threeready.data.proximus.pvr.model.request.RecordingStopRequest;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingAddResponseProximus;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingDeleteResponse;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingDeleteSeriesResponse;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingListResponseProximus;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingScheduleSeriesResponse;
import tv.threess.threeready.data.proximus.pvr.model.response.RecordingStopResponse;
import tv.threess.threeready.data.proximus.pvr.model.response.SeriesRecordingListResponseProximus;
import tv.threess.threeready.data.proximus.pvr.model.series.response.RecordingQuotaProximus;
import tv.threess.threeready.data.proximus.pvr.model.series.response.RecordingQuotaResponseProximus;
import tv.threess.threeready.data.proximus.pvr.model.series.response.RetentionTypesProximus;
import tv.threess.threeready.data.proximus.pvr.retrofit.BTARecordingRetrofitProximus;
import tv.threess.threeready.data.proximus.tv.model.BaseBroadcastProximus;

/**
 * PvrProxy class implementation for the proximus backend.
 *
 * @author Denisa Trif
 * @since 2018.09.14
 */
public class PvrProxyProximus implements PvrProxy, Component {

    protected final Application mApp;

    private final BTARecordingRetrofitProximus mRecordingRetrofitProximus =
            Components.get(RetrofitServicesProximus.class).getRecordingRetrofit();

    public PvrProxyProximus(Application application) {
        mApp = application;
    }

    @Override
    public RecordingListResponseProximus getAllRecordings() throws IOException {
        return RetrofitServicesProximus.execute(mRecordingRetrofitProximus.getRecordingsList());
    }

    /**
     * Schedules a recording.
     *
     * @param broadcastId Broadcast id.
     * @param channelId   Channel id
     * @return response for addRecording call.
     * @throws IOException in case of errors.
     */
    RecordingAddResponseProximus scheduleRecording(String broadcastId, String channelId) throws IOException {
        String trailId = BaseBroadcastProximus.buildTrailId(broadcastId);
        RecordingAddRequestProximus scheduleRequest = new RecordingAddRequestProximus(trailId, channelId);
        return RetrofitServicesProximus.execute(mRecordingRetrofitProximus.scheduleRecording(scheduleRequest));
    }

    /**
     * Schedules a series.
     *
     * @param broadcastId Broadcast id.
     * @param channelId   Channel id.
     * @return response for addSeriesRecording call.
     * @throws IOException in case of errors.
     */
    RecordingScheduleSeriesResponse scheduleSeriesRecording(String broadcastId, String channelId) throws IOException {
        String tailId = BaseBroadcastProximus.buildTrailId(broadcastId);
        RecordingScheduleSeriesRequest request = new RecordingScheduleSeriesRequest(tailId, channelId);
        return RetrofitServicesProximus.execute(mRecordingRetrofitProximus.scheduleSeries(request));
    }

    /**
     * Stops an ongoing recording.
     *
     * @param recordingId Recording id.
     * @return response for stopRecording call.
     * @throws IOException in case of errors.
     */
    RecordingStopResponse stopRecording(String recordingId) throws IOException {
        RecordingStopRequest request = new RecordingStopRequest(recordingId);
        return RetrofitServicesProximus.execute(mRecordingRetrofitProximus.stopRecording(request));
    }

    /**
     * Cancel/Delete a future single recording.
     *
     * @param recordingId Recording id.
     * @return Response for deleteRecording call.
     * @throws IOException in case of errors.
     */
    RecordingDeleteResponse cancelRecording(String recordingId) throws IOException {
        RecordingDeleteRequest request = new RecordingDeleteRequest(recordingId);
        return RetrofitServicesProximus.execute(mRecordingRetrofitProximus.deleteRecordings(request));
    }

    /**
     * Cancels a series recording.
     *
     * @param userSeriesId Series id.
     * @return response for deleteSeriesRecording call.
     * @throws IOException in case of errors.
     */
    RecordingDeleteSeriesResponse deleteSeriesRecording(String userSeriesId) throws IOException {
        RecordingDeleteSeriesRequest request = new RecordingDeleteSeriesRequest(userSeriesId, true);
        return RetrofitServicesProximus.execute(mRecordingRetrofitProximus.deleteSeriesRecording(request));
    }

    /**
     * Deletes multiple recording items.
     *
     * @param recordingIds List of ids which needs to be deleted.
     * @return response for deleteRecordings call.
     * @throws IOException in case of errors.
     */
    RecordingDeleteResponse deleteRecordings(String... recordingIds) throws IOException {
        RecordingDeleteRequest request = new RecordingDeleteRequest(recordingIds);
        return RetrofitServicesProximus.execute(mRecordingRetrofitProximus.deleteRecordings(request));
    }

    /**
     * @return The recording quota from the backend.
     * @throws IOException In case of errors.
     */
    @Override
    public IRecordingQuota getSTCRecordingQuota() throws IOException {
        RecordingQuotaResponseProximus response = RetrofitServicesProximus.execute(mRecordingRetrofitProximus.getRecordingQuota());

        List<RecordingQuotaProximus> recordingQuotaList = response.getRecordingQuotaList();
        if (recordingQuotaList != null && !recordingQuotaList.isEmpty()) {
            for (RecordingQuotaProximus recordingQuotaItem : recordingQuotaList)
                if (recordingQuotaItem.getRetentionType() == RetentionTypesProximus.ShortTermConservation) {
                    return recordingQuotaItem;
                }
        }

        return null;
    }

    /**
     * @return The schedule series recordings from the backend.
     * @throws IOException In case of errors.
     */
    public SeriesRecordingListResponseProximus getScheduledSeriesRecordings() throws IOException {
        return RetrofitServicesProximus.execute(mRecordingRetrofitProximus.getSeriesRecordingList());
    }
}