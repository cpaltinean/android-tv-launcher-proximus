package tv.threess.threeready.data.proximus.vod.model.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Type adapter the 'Yes' from XML as boolean.
 *
 * @author Barabas Attila
 * @since 6/4/21
 */
public class YesAsBooleanTypeAdapter extends XmlAdapter<String, Boolean> {
    private static final String VALUE_YES = "Yes";
    private static final String VALUE_NO = "No";

    @Override
    public Boolean unmarshal(String v) {
        return VALUE_YES.equalsIgnoreCase(v);
    }

    @Override
    public String marshal(Boolean v) {
        if (v) {
         return VALUE_YES;
        }
        return VALUE_NO;
    }
}
