package tv.threess.threeready.data.proximus.vod.observable;

import android.content.Context;
import android.net.Uri;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IBaseVodSeries;
import tv.threess.threeready.api.vod.model.VodSeriesDetailModel;
import tv.threess.threeready.data.vod.VodCategoryCache;
import tv.threess.threeready.data.vod.VodContract;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Rx observable to subscribe to the purchase changes in the given series.
 *
 * @author Barabas Attila
 * @since 2019.02.06
 */
public class VodSeriesObservable extends BaseVodObservable<VodSeriesDetailModel> {
    private static final String TAG = Log.tag(VodSeriesObservable.class);

    private final VodCategoryCache mVodCategoryCache = Components.get(VodCategoryCache.class);

    private final String mVodSeriesId;

    private VodSeriesDetailModel mSeriesModel;
    private CopyOnWriteArrayList<IBaseVodItem> mEpisodes = new CopyOnWriteArrayList<>();

    public VodSeriesObservable(Context context, String seriesId) {
        super(context);
        mVodSeriesId = seriesId;
    }

    @Override
    public void subscribe(ObservableEmitter<VodSeriesDetailModel> emitter) throws Exception {
        super.subscribe(emitter);

        publishVodSeries();
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<VodSeriesDetailModel> emitter) throws IOException {
        if (uri.getEncodedPath() != null && VodContract.RentalExpiration.CONTENT_URI.getEncodedPath() != null
                && uri.getEncodedPath().startsWith(VodContract.RentalExpiration.CONTENT_URI.getEncodedPath())) {

            String id = uri.getLastPathSegment();
            IBaseVodItem episode = getEpisodeByVariantId(id);
            if (episode == null) {
                // Not in the list.
                return;
            }

            episode = getCompleteVod(episode);

            for (int i = 0; i < mEpisodes.size(); ++i) {
                if (Objects.equals(mEpisodes.get(i).getId(), episode.getId())) {
                    mEpisodes.set(i, episode);
                    mSeriesModel = new VodSeriesDetailModel(mSeriesModel.getBaseSeries(), mEpisodes,
                            mSeriesModel.isCompleted(), mSeriesModel.getLastPlayedEpisode());
                    emitter.onNext(mSeriesModel);
                    return;
                }
            }
        }
    }

    private IBaseVodItem getEpisodeByVariantId(String variantId) {
        for (IBaseVodItem episode : mEpisodes) {
            if (episode.isVariantIncluded(variantId)) {
                return episode;
            }
        }
        return null;
    }

    private void publishVodSeries() throws Exception {
        if (RxUtils.isDisposed(mEmitter)) {
            // Already canceled.
            return;
        }

        VodSeriesDetailModel vsm = null;
        IBaseVodSeries baseSeries = mVodCategoryCache.getVodCategorySeries(mVodSeriesId);
        if (baseSeries == null) {
            mEmitter.onError(new Exception("No series with id : " + mVodSeriesId));
            return;
        }

        IBaseVodItem lastPlayedEpisode = null;
        try {
            lastPlayedEpisode = getLastPlayedVodInSeries(baseSeries);
        } catch (Exception e) {
            Log.d(TAG, "Get last played episode failed.", e);
        }
        boolean lastPlayerEpisodeEmitted = lastPlayedEpisode == null;

        List<String> seasonIds = baseSeries.getSeasonIds();

        // Paginated load of episodes
        List<IBaseVodItem> episodes = new ArrayList<>();
        for (int i = 0; i < seasonIds.size(); ++i) {
            String seasonId = seasonIds.get(i);

            List<IBaseVodItem> seasonEpisodes = mVodProxy.getEpisodes(mVodSeriesId,
                    seasonId, mVodCache.getRentalExpiration());

            // Check if the last played episode in the list.
            if (!lastPlayerEpisodeEmitted) {
                for (IBaseVodItem episode : seasonEpisodes) {
                    if (episode.isVariantIncluded(lastPlayedEpisode.getMainVariant().getId())) {
                        lastPlayerEpisodeEmitted = true;
                        lastPlayedEpisode = episode;
                    }
                }
            }

            episodes.addAll(seasonEpisodes);

            vsm = new VodSeriesDetailModel(baseSeries, episodes,
                    i == seasonIds.size() - 1, lastPlayedEpisode);

            // Wait for last played episode or all the seasons
            if (lastPlayerEpisodeEmitted
                    || i == seasonIds.size() - 1) {
                mEmitter.onNext(vsm);
            }
        }

        // Subscribe for rental changes in episodes.
        for (IBaseVodItem vod : episodes) {
            registerObserver(VodContract.buildPurchaseUrisForVod(vod));
        }

        mEpisodes = new CopyOnWriteArrayList<>(episodes);
        mSeriesModel = vsm;
    }
}
