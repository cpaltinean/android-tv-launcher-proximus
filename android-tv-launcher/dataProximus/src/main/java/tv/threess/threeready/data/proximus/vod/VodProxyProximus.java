/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.vod;

import android.app.Application;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.generic.exception.InvalidPinException;
import tv.threess.threeready.api.generic.exception.PinChangedException;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.vod.exception.VodAlreadyRentedException;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IRentalExpiration;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.proximus.generic.RetrofitServicesProximus;
import tv.threess.threeready.data.proximus.generic.model.BTAErrorCodesProximus;
import tv.threess.threeready.data.proximus.vod.model.request.VodRentalRequest;
import tv.threess.threeready.data.proximus.vod.model.response.BaseVodItemProximus;
import tv.threess.threeready.data.proximus.vod.model.response.RentalExpirationResponseProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategoryContentResponseProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategoryProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodCategorySortOptionProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodItemDetailResponseProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodItemProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodRentalHistoryResponseProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodRentalResponse;
import tv.threess.threeready.data.proximus.vod.model.response.VodShopLanguageProximus;
import tv.threess.threeready.data.proximus.vod.model.response.VodVariantProximus;
import tv.threess.threeready.data.proximus.vod.retrofit.BTAVodRetrofitProximus;
import tv.threess.threeready.data.vod.VodProxy;

/**
 * VodProxy class implementation for the proximus backend.
 *
 * @author Barabas Attila
 * @since 2018.06.06
 */
public class VodProxyProximus implements VodProxy, Component {
    public static final String TAG = Log.tag(VodProxyProximus.class);

    // The maximum number of characters in batch vod variant id request.
    private static final int MAX_BATCH_VOD_VARIANT_ID_QUERY_CHAR_LIMIT = 1500;

    private final Application mApp;
    private final BTAVodRetrofitProximus mBTAVodRetrofit = Components.get(RetrofitServicesProximus.class).getVodRetrofit();

    public VodProxyProximus(Application application) {
        mApp = application;
    }

    /**
     * Get a VOD object by it's identified.
     *
     * @param id ID of VOD item.
     * @return The VOD item.
     */
    public VodItemProximus getVod(String id) throws IOException {
        List<VodItemProximus> vodItems = getVod(Collections.singletonList(id));
        if (vodItems.isEmpty()) {
            throw new IllegalStateException("No VOD found with id : " + id);
        }
        return vodItems.get(0);
    }

    /**
     * Returns a detailed vod item.
     */
    public VodItemProximus getCompleteVod(String vodId, String vodSeriesId, Map<String, IRentalExpiration> rentalExpirationMap) throws IOException {
        VodItemProximus vodItem = getVod(vodId);
        double priceLimit = SettingsProximus.freeRentalLimit.get(0d);
        int freeRentals = SettingsProximus.freeRentals.get(0);

        if (vodItem != null) {
            vodItem.updateCheapestPrice(priceLimit, freeRentals);
            vodItem.setRentalExpirationInfo(rentalExpirationMap);
            vodItem.setSeriesId(vodSeriesId);
        }
        return vodItem;
    }

    /**
     * Request multiple VODs in a batch.
     * @param ids The id of the VODs which needs to be returned.
     * @return A list with the requested VODs
     */
    @Override
    public List<VodItemProximus> getVod(List<String> ids) throws IOException {
        if (ids.isEmpty()) {
            return Collections.emptyList();
        }

        int i;
        // Join VOD ids
        StringBuilder joinedIds = new StringBuilder();
        for (i = 0; i < ids.size(); ++i) {
            String vodId = ids.get(i);

            // Batch batch variant id limit reached.
            if (joinedIds.length() + vodId.length() >= MAX_BATCH_VOD_VARIANT_ID_QUERY_CHAR_LIMIT) {
                break;
            }

            StringUtils.join(joinedIds, vodId, ",");
        }

        // Load VODs in batch
        VodItemDetailResponseProximus response = RetrofitServicesProximus.execute(
                mBTAVodRetrofit.getVodById(joinedIds.toString(),
                        SettingsProximus.hdProfile.get(false)));
        List<VodItemProximus> vodItems = new ArrayList<>(response.getVODs(ids));

        // Load next batch if still remaining.
        if (i != ids.size()) {
            vodItems.addAll(getVod(ids.subList(i, ids.size())));
        }

        return vodItems;
    }

    /**
     * @return A list of non empty leaf categories from the backend.
     * @throws IOException In case of backend or network errors.
     */
    public List<VodCategoryProximus> getLeafCategories() throws IOException {
        VodShopLanguageProximus shopLanguage = new VodShopLanguageProximus(SettingsProximus.shopLanguage.getList());
        return RetrofitServicesProximus.execute(mBTAVodRetrofit.getLeafCategories(
                SettingsProximus.hdProfile.get(false), shopLanguage)).getCategories();
    }

    @Override
    public List<IBaseVodItem> getEpisodes(String seriesId, String seasonId, Map<String, IRentalExpiration> rentalExpirationMap) throws IOException {
        VodCategoryContentResponseProximus response = RetrofitServicesProximus.execute(
                mBTAVodRetrofit.getRootCategoryContent(seasonId, 0,
                        SettingsProximus.hdProfile.get(false),
                        new VodShopLanguageProximus(SettingsProximus.shopLanguage.getList())
                )
        );

        List<IBaseVodItem> episodes = new ArrayList<>(response.getVODs());

        if (!episodes.isEmpty()) {
            double priceLimit = SettingsProximus.freeRentalLimit.get(0d);
            int freeRentals = SettingsProximus.freeRentals.get(0);

            episodes.forEach(vod -> {
                ((BaseVodItemProximus) vod).updateCheapestPrice(priceLimit, freeRentals);
                ((BaseVodItemProximus) vod).setRentalExpirationInfo(rentalExpirationMap);
                ((BaseVodItemProximus) vod).setSeriesId(seriesId);
            });
        }

        return episodes;
    }

    /**
     * Get all the credits that are available for the user.
     *
     * @return A list of credits that are available for the user.
     */
    @Override
    public int getAvailableCreditCount() {
        int creditCount = 0;
        long startDate = SettingsProximus.serviceStartDate.get(0L);
        long endDate = SettingsProximus.serviceEndDate.get(0L);
        if (startDate < System.currentTimeMillis() && endDate > System.currentTimeMillis()) {
            creditCount = SettingsProximus.freeRentals.get(0);
        }
        return creditCount;
    }

    /**
     * Get the rent offer for a specific vod variant.
     *
     * @param variant A variant to request offer for.
     * @return The rent offer for the variant or null if there is no offer available.
     */
    @Override
    @Nullable
    public IRentOffer getRentOffer(IVodVariant variant) {
        VodVariantProximus proximusVariant = (VodVariantProximus) variant;
        return proximusVariant.getRentOffer();
    }

    VodRentalResponse rentVodItem(String vodId, float price, String pin, boolean freeRental, String priceDetermination) throws IOException {
        VodRentalRequest request = new VodRentalRequest(vodId, price, freeRental, priceDetermination);

        try {
            return RetrofitServicesProximus.execute(mBTAVodRetrofit.rentVod(pin, request));
        } catch (BackendException e) {
            switch (e.getErrorCode()) {
                case BTAErrorCodesProximus.INVALID_PURCHASE_PIN:
                    throw new InvalidPinException(e);

                case BTAErrorCodesProximus.PURCHASE_PIN_RECENTLY_CHANGED:
                    throw new PinChangedException(e);

                case BTAErrorCodesProximus.ALREADY_RENTED:
                    throw new VodAlreadyRentedException(e);
            }
            throw e;
        }
    }

    /**
     * @return A list with rented VODs.
     * @throws IOException in case of errors.
     */
    @Override
    public RentalExpirationResponseProximus getRentedVodList() throws IOException {
        return RetrofitServicesProximus.execute(mBTAVodRetrofit.getRentedItems());
    }

    /**
     * @return A list with vod rental history items.
     * @throws IOException in case of errors.
     */
    public VodRentalHistoryResponseProximus getVodRentalHistory() throws IOException {
        return RetrofitServicesProximus.execute(mBTAVodRetrofit.getRentalHistory());
    }

    /**
     * @param categoryId The id of the category for which the VOD content needs to be returned.
     * @param start The pagination start index.
     * @param count The pagination max item count.
     * @return The response with VOD content of the category.
     *  @param sortOption The options to sort the category content by. If null the default sort option needs to be applied.
     * @throws IOException In case of errors.
     */
    public VodCategoryContentResponseProximus getVodCategoryContent(String categoryId, int start, int count,
                                                                     VodCategorySortOptionProximus sortOption) throws IOException {
        boolean hdInterest = SettingsProximus.hdProfile.get(false);
        VodShopLanguageProximus shopLanguage = new VodShopLanguageProximus(SettingsProximus.shopLanguage.getList());
        if (sortOption == null) {
            return RetrofitServicesProximus.execute(mBTAVodRetrofit.getCategoryContent(
                    categoryId, start, count, hdInterest, shopLanguage
            ));
        }

        return RetrofitServicesProximus.execute(mBTAVodRetrofit.getCategoryContent(
                categoryId, start, count, hdInterest, shopLanguage, sortOption.getValue()
        ));
    }
}
