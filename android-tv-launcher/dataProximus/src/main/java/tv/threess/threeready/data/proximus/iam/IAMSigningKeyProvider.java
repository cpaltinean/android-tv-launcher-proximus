package tv.threess.threeready.data.proximus.iam;

import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.spec.ECPrivateKeySpec;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import io.jsonwebtoken.io.Decoders;
import tv.threess.threeready.api.log.Log;

/**
 * Provide private key for SWT signing.
 *
 * @author Barabas Attila
 * @since 2020.03.20
 */
public class IAMSigningKeyProvider {
    private static final String TAG = Log.tag(IAMSigningKeyProvider.class);

    private static final Map<String, String> PRIVATE_KEY_MAP = new HashMap<String, String>() {{
        put("ES_ProximusTV_JWT", "ziPABCVsqaZc0UWzOgQAAnnU9Pz63f9NVKTNvRWSt5U"); // Old  staging
        put("TV-STB-V7-ES256-1554895683908", "cqDBz8pABvbJlE00nfY6y9oorha45SLxCTPMDn-Cfvw"); // Old production.

        put("ES_ProximusTV_SSA_V7", "9g93ieQ-03-XXIwVB-fkbxVwWENUtoui6UObIsiHdPs"); // SSA
        put("ES_ProximusTV_JWT_V7", "HpebKzVzBgxoRWOa7IZwUs0EseeRPI235Ter3ZEmfAM"); // PROD
        put("ES_ProximusTV_JWT_V7_Staging", "105FLZpxuDLsXS84T6uInJBr42-rkE2jLDn_1Gfjsr0"); // Staging
        put("ES_ProximusTV_JWT_V7_ITT", "Wp4TvGntyQ5rxNlEsQGepYUJtSHMiy5mUVw11mU1E1w"); // ITT

    }};

    /**
     * Provide preview key for SWT signing a give key id.
     * @param keyId The key id.
     * @return The private key which can be used for caching.
     */
    @Nullable
    public static PrivateKey getPrivateKey(String keyId) {
        BigInteger privateKeyString = new BigInteger(Decoders.BASE64URL.decode(PRIVATE_KEY_MAP.get(keyId)));
        return getPrivateKeyFromECBigIntAndCurve(privateKeyString, "secp256r1");
    }

    @Nullable
    private static PrivateKey getPrivateKeyFromECBigIntAndCurve(BigInteger s, String curveName)  {
        try {
            ECParameterSpec ecParameterSpec = ECNamedCurveTable.getParameterSpec(curveName);
            ECPrivateKeySpec privateKeySpec = new ECPrivateKeySpec(s, ecParameterSpec);
            KeyFactory keyFactory = KeyFactory.getInstance("EC", new BouncyCastleProvider());
            return keyFactory.generatePrivate(privateKeySpec);
        } catch (Exception e) {
            Log.e(TAG, "Failed to generate private key.", e);
        }

        return null;
    }

}
