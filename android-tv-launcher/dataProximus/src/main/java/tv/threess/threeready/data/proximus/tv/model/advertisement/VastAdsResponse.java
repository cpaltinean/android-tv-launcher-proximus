package tv.threess.threeready.data.proximus.tv.model.advertisement;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Ads")
public class VastAdsResponse {
    @XmlElement(name = "Ad")
    List<TvAdvertisementProximus> response;

    public List<TvAdvertisementProximus> getResponse() {
        return response;
    }
}
