package tv.threess.threeready.data.proximus.tv.observable;

import android.content.Context;
import android.net.Uri;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.ObservableEmitter;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.EpgFetchObservable;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.tv.TvProxy;
import tv.threess.threeready.data.tv.model.NoInfoBroadcast;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Rx observable to load broadcasts for a row in the EPG grid.
 *
 * @author Barabas Attila
 * @since 9/30/21
 */
public class EpgGridBroadcastObservable extends BaseContentObservable<List<IBroadcast>> implements EpgFetchObservable<List<IBroadcast>> {
    private static final String TAG = Log.tag(EpgGridBroadcastObservable.class);

    private final Translator mTranslator = Components.get(Translator.class);
    private final TvProxy mTvProxy = Components.get(TvProxy.class);

    private final TvChannel mChannel;
    private final boolean mIsAdultChannel;

    private List<? extends IBroadcast> mBroadcasts = Collections.emptyList();

    private final long mWindowStart;
    private final long mWindowEnd;

    private long mFrom;
    private long mTo;

    private Disposable mProgramDisposable;

    public EpgGridBroadcastObservable(Context context, TvChannel channel, List<IBroadcast> programs, boolean isAdultChannel, long windowStart, long windowEnd) {
        super(context);
        mChannel = channel;
        mIsAdultChannel = isAdultChannel;
        mWindowStart = windowStart;
        mWindowEnd = windowEnd;

        fillWindow(programs, false);
    }

    @Override
    public void subscribe(ObservableEmitter<List<IBroadcast>> e) throws Exception {
        super.subscribe(e);

        prefetchPrograms();
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<List<IBroadcast>> observableEmitter) {
        Log.d(TAG, "onChange : " + uri);
    }

    /**
     * Fetch and emmit programs on the given channel and selected time frame.
     */
    private void prefetchPrograms() {
        // Fetch broadcasts.
        boolean isLoaded = false;
        try {
            mBroadcasts = mTvProxy.getBroadcasts(mChannel.getId(), mFrom, mTo).getBroadcasts();
        } catch (Exception e) {
            isLoaded = true;
            Log.e(TAG, "Failed to broadcast list.", e);
        }

        publishPrograms(isLoaded);
    }

    private void publishPrograms(boolean isLoaded) {
        List<IBroadcast> broadcasts = new ArrayList<>(mBroadcasts);

        // Fill gaps in broadcasts list.
        if (!isLoaded) {
            fillGaps(broadcasts);
        }

        fillWindow(broadcasts, isLoaded);
        mEmitter.onNext(broadcasts);
    }

    /**
     * Fill the gaps in the broadcast list with dummy No info programs.
     *
     * @param programs The list of broadcast to fill gaps in.
     */
    private void fillGaps(List<IBroadcast> programs) {
        if (programs.size() <= 1) {
            return;
        }

        for (int index = 0; index < programs.size() - 1; ++index) {
            IBroadcast current = programs.get(index);
            IBroadcast next = programs.get(index + 1);

            if (next.getStart() < current.getEnd()) {
                Log.e(TAG, "Overlap detected. Channel : " + mChannel.getId()
                        + ", current end : " + Log.timestamp(current.getEnd())
                        + ", next start : " + Log.timestamp(next.getStart()));

            } else if (next.getStart() > current.getEnd()) {
                Log.w(TAG, "Fill gap. Channel : " + mChannel.getId()
                        + ", current end : " + Log.timestamp(current.getEnd())
                        + ", next start : " + Log.timestamp(next.getStart()));
                programs.add(++index, createDummyProgram(
                        current.getEnd(), next.getStart(), true));
            }
        }
    }

    /**
     * Add dummy broadcasts to the list to fill the whole timeline window with broadcasts.
     *
     * @param programs The list of broadcast to full with dummies.
     */
    public void fillWindow(List<IBroadcast> programs, boolean isLoaded) {
        if (programs.isEmpty()) {
            programs.add(createDummyProgram(mWindowStart, mWindowEnd, false));
            return;
        }

        IBroadcast firstProgram = programs.get(0);
        IBroadcast lastProgram = programs.get(programs.size() - 1);

        // Add dummy start padding program.
        if (firstProgram.getStart() > mWindowStart) {
            // add dummy between first program end and mFrom if there is a gap.
            if (firstProgram.getStart() > mFrom) {
                programs.add(0, createDummyProgram(mFrom, firstProgram.getStart(), true));
            }

            if (mWindowStart < mFrom) {
                programs.add(0, createDummyProgram(mWindowStart, firstProgram.getStart(), isLoaded));
            }
        }

        // Add dummy end padding program
        if (lastProgram.getEnd() < mWindowEnd) {
            // add dummy between last program end and mTo if there is a gap.
            if (lastProgram.getEnd() < mTo) {
                programs.add(createDummyProgram(lastProgram.getEnd(), mTo, true));
            }

            if (mWindowEnd > mTo){
                programs.add(createDummyProgram(lastProgram.getEnd(), mWindowEnd, isLoaded));
            }
        }
    }

    /**
     * Called when the visible timeline changes in the EPG grid and broadcasts needs to be loaded.
     * The timeline includes the extra prefetch time for the broadcasts.
     *
     * @param from The timestamp from which the broadcast needs to be loaded.
     * @param to   The timestamp until the broadcasts needs to be loaded.
     */
    public void prefetchPrograms(long from, long to) {
        if (mFrom == from && mTo == to) {
            // No change.
            return;
        }

        mFrom = from;
        mTo = to;

        if (RxUtils.isDisposed(mEmitter)) {
            return;
        }

        // Fetch broadcasts.
        prefetchProgramsAsync();
    }

    /**
     * Prefetch programs async on IO thread pool,
     */
    private void prefetchProgramsAsync() {
        RxUtils.disposeSilently(mProgramDisposable);
        mProgramDisposable = Schedulers.io().scheduleDirect(() -> {
            if (RxUtils.isDisposed(mEmitter)) {
                // Already disposed.
                return;
            }

            prefetchPrograms();
        });
    }


    @Override
    protected void unregisterObserver() {
        super.unregisterObserver();
        RxUtils.disposeSilently(mProgramDisposable);
        mFrom = mTo = 0;
    }

    private IBroadcast createDummyProgram(long start, long end, boolean isLoaded) {
        return new NoInfoBroadcast(mChannel.getId(),
                mTranslator.get(isLoaded ? TranslationKey.EPG_NO_INFO : TranslationKey.MODULE_CARD_LOADING),
                mIsAdultChannel ? ParentalRating.Rated18 : ParentalRating.Undefined, start, end, isLoaded);
    }
    @Override
    public boolean prefetchPrograms(String channelId, long from, long to) {
        if (channelId == null || !channelId.equals(mChannel.getId())) {
            return false;
        }
        prefetchPrograms(from, to);
        return true;
    }
}
