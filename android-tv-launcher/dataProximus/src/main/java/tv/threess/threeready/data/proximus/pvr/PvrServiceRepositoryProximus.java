package tv.threess.threeready.data.proximus.pvr;

import android.app.Application;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.data.pvr.BasePvrServiceRepository;
import tv.threess.threeready.data.pvr.PvrCache;

/**
 * Implementation of the Proximus specific pvr service handler methods.
 *
 * @author Denisa Trif
 * @since 2018.09.14
 */
public class PvrServiceRepositoryProximus extends BasePvrServiceRepository implements Component {

    private final PvrProxyProximus mPvrProxy = Components.get(PvrProxyProximus.class);
    private final PvrCache mPvrCache = Components.get(PvrCache.class);

    public PvrServiceRepositoryProximus(Application app) {
        super(app);
    }

    /**
     * Update recording and series recording cache with data from backend.
     * @throws Exception In case of errors.
     */
    @Override
    public void updateRecordings() throws Exception {
        pvrUpdater.batchUpdate()
                .runIfExpired(() -> mPvrCache.updateRecordingList(mPvrProxy.getAllRecordings().getRecordingList()))
                .runIfExpired(() ->  mPvrCache.updateSeriesRecording(mPvrProxy.getScheduledSeriesRecordings().getSeriesList()))
                .done();
    }
}
