package tv.threess.threeready.data.proximus.tv;

import android.content.Context;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.api.tv.model.ZappingInfo;
import tv.threess.threeready.data.proximus.tv.observable.ZappingInfoObservable;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Class which extends NowOnTvCache and holds the project specific implementations.
 * @see NowOnTvCache
 *
 * @author David Bondor
 * @since 2022.04.19
 */
public class NowOnTvCacheProximus extends NowOnTvCache {

    private final String TAG = Log.tag(NowOnTvCacheProximus.class);

    private Disposable mZappingDisposable;

    public NowOnTvCacheProximus(Context context) {
        super(context);
    }

    @Override
    public Completable initialize() {
        CompletableFuture<Void> completableFuture = new CompletableFuture<>();

        RxUtils.disposeSilently(mZappingDisposable);
        Observable.create(new ZappingInfoObservable(mContext))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<ZappingInfo>>() {
                    @Override
                    public void onSubscribe(Disposable disposable) {
                        mZappingDisposable = disposable;
                    }

                    @Override
                    public void onNext(List<ZappingInfo> zappingInfoList) {
                        Log.d(TAG, "Update zapping info : " + zappingInfoList.size());
                        mZappingInfoList = zappingInfoList;

                        mFavoriteChannelZappingInfoMap.clear();
                        mChannelZappingInfoMap.clear();

                        for (ZappingInfo zappingInfo : zappingInfoList) {
                            TvChannel channel = zappingInfo.getChannel();
                            if (zappingInfo.isFromFavorites()) {
                                mFavoriteChannelZappingInfoMap.put(channel.getId(), zappingInfo);
                            }
                            mChannelZappingInfoMap.put(channel.getId(), zappingInfo);
                        }
                        completableFuture.complete(null);

                        //notify that the memory cache was changed
                        mContext.getContentResolver().notifyChange(TvContract.Channel.CHANNEL_MEMORY_CACHE_CONTENT_URI, null);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "Could update zapping info list. ", e);
                        completableFuture.completeExceptionally(e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });

        return Completable.fromFuture(completableFuture);
    }
}
