package tv.threess.threeready.data.proximus.playback.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import tv.threess.threeready.api.generic.helper.TimeUtils;

/**
 * XMl type adapter to read the bucket time as UTC timestamp.
 * E.g. 020-07-14T12:30:00Z
 *
 * @author Barabas Attila
 * @since 2020.07.07
 */
public class BucketTimeTypeAdapter extends TypeAdapter<Long> {
    // invalid timestamp is 1970 new year's first second, because if the json does not contain the field, it is also 0
    protected static final long INVALID_TIMESTAMP = 0;

    @Override
    public void write(JsonWriter out, Long value) {
        throw new IllegalStateException("Not implemented.");
    }

    @Override
    public Long read(JsonReader in) throws IOException {
        if (in.hasNext()) {
            return TimeUtils.getDateTimeParser()
                    .parse(in.nextString());
        }
        return INVALID_TIMESTAMP;
    }

    public static boolean isValid(long... timestamps) {
        for (long timestamp : timestamps) {
            if (timestamp == INVALID_TIMESTAMP) {
                return false;
            }
        }
        return true;
    }
}
