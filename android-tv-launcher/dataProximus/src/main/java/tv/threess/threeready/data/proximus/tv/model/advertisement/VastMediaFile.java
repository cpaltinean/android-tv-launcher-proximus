package tv.threess.threeready.data.proximus.tv.model.advertisement;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * <MediaFile> contains the best media file to be downloaded or streamed for a given technology or device.
 *
 * When an ad may be served to multiple video platforms, one platform (i.e. device) may need the media file
 * in a different format than what another platform needs. More specifically, different devices are capable
 * of displaying video files with different encodings and containers, and at different bitrates.
 */
public class VastMediaFile {

    @XmlAttribute(name = "id")
    private String mId;

    @XmlAttribute(name = "width")
    private int mWidth;

    @XmlAttribute(name = "height")
    private int mHeight;

    @XmlAttribute(name = "bitrate")
    private int mBitrate;

    @XmlAttribute(name = "type")
    private String mType;

    @XmlAttribute(name = "delivery")
    private String mDelivery;

    @XmlValue
    @JacksonXmlCData
    private String mValue;

    public String getId() {
        return mId;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public int getBitrate() {
        return mBitrate;
    }

    public String getType() {
        return mType;
    }

    public String getDelivery() {
        return mDelivery;
    }

    public String getValue() {
        if (mValue == null) {
            return null;
        }
        return mValue.trim();
    }
}
