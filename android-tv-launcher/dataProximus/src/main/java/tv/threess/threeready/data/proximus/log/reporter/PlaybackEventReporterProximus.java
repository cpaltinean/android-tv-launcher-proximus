/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.log.reporter;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.concurrent.atomic.AtomicLong;

import io.reactivex.annotations.Nullable;
import tv.threess.threeready.api.account.setting.MassStorage;
import tv.threess.threeready.api.account.setting.MemoryCache;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.helper.ErrorType;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.GenericEvent;
import tv.threess.threeready.api.log.model.GenericKeyEvent;
import tv.threess.threeready.api.log.model.PlaybackEventDetails;
import tv.threess.threeready.api.log.model.PlaybackItemType;
import tv.threess.threeready.api.log.model.PlaybackLogEvent;
import tv.threess.threeready.api.log.model.ReporterInfo;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.log.model.VodItemType;
import tv.threess.threeready.api.middleware.model.HdmiInfo;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.data.proximus.log.model.LogDetailsProximus;
import tv.threess.threeready.data.proximus.log.model.SendEventsRequest;
import tv.threess.threeready.data.utils.GsonUtils;

/**
 * Specific playback event batch handler for Proximus
 *
 * @author Arnold Szabo
 * @since 2017.07.26
 */
public class PlaybackEventReporterProximus extends EventReporterProximus<PlaybackLogEvent, PlaybackEventDetails> {
    private static final String TAG = Log.tag(PlaybackEventReporterProximus.class);

    private static final Type TYPE_STRING_TO_LOG_DETAILS_PROXIMUS = new TypeToken<LogDetailsProximus>() {
    }.getType();

    // Keys
    private static final String KEY_CONNECTED_DEFLECT_STATE_ACTION = "connected_deflect_state_action";
    private static final String KEY_CONSECUTIVE_PLAYOUT_INDICATOR = "consecutive_playout_indicator";
    private static final String KEY_ERROR_CODE = "error_code";
    private static final String KEY_ERROR_KEY = "error_key";
    private static final String KEY_ERROR_MESSAGE = "error_message";
    private static final String KEY_ERROR_TYPE = "error_type";
    private static final String KEY_EXCEPTION_MESSAGE = "exception_message";
    private static final String KEY_EXCEPTION_TYPE = "exception_name";
    private static final String KEY_HDCP_STATUS = "hdcp_status";
    private static final String KEY_HDMI_CONN = "hdmi_connectivity";
    private static final String KEY_HDMI_CONN_OLD = "HD_conn";
    private static final String KEY_INTO_STANDBY = "into_standby";
    private static final String KEY_INTO_STANDBY_ELAPSED_TIME = "into_standby_elapsed_time";
    private static final String KEY_INVOCATION_POINT = "invocation_point";
    private static final String KEY_ISOLATION_STATE_ACTION = "isolation_state_action";
    private static final String KEY_OPERATIONAL_MODE = "operational_mode";
    private static final String KEY_OUT_OF_STANDBY = "out_of_standby";
    private static final String KEY_OUT_STANDBY_ELAPSED_TIME = "out_standby_elapsed_time";
    private static final String KEY_PLAYOUT_CAT = "playout_cat";
    private static final String KEY_PLAYOUT_URL = "playout_url";
    private static final String KEY_PREVIEW = "preview";
    private static final String KEY_STREAM_ADAPTIVITY_INDICATOR = "stream_adaptivity_indicator";
    private static final String KEY_STREAM_CONTENT_URL = "stream_content_url";
    private static final String KEY_STREAM_CONTROL_PROTOCOL = "stream_control_protocol";
    private static final String KEY_STREAM_MODE = "stream_mode";
    private static final String KEY_TIME_POSITION = "time_position";
    private static final String KEY_VIEWING_DURATION = "viewing_duration";
    private static final String KEY_VIEWING_IDENTIFIER = "viewing_identifier";
    private static final String KEY_VIEWING_TYPE = "viewing_type";

    // Known values
    private static final String VALUE_ACTION_MENU_VOD_TRAILER = "ActionMenuVod_trailer";
    private static final String VALUE_ACTION_MENU_VOD_WATCH = "ActionMenuVod_watch";
    private static final String VALUE_ALL_RECORDINGS_PAGE_STOP = "AllRecordingsPage_stop";
    private static final String VALUE_AP = "AP";
    private static final String VALUE_APPLICATION = "APPLICATION";
    private static final String VALUE_APP_EXIT = "APP-EXIT";
    private static final String VALUE_APP_START = "APP-START";
    private static final String VALUE_BE = "BE";
    private static final String VALUE_BOOT = "BOOT";
    private static final String VALUE_BUFFER_RESTART = "BUFFER-RESTART";
    private static final String VALUE_CATCHUP = "CATCHUP";
    private static final String VALUE_CATCH_UP = "CATCH-UP";
    private static final String VALUE_CC = "CC";
    private static final String VALUE_CHANNEL_DOWN = "CHANNEL-DOWN";
    private static final String VALUE_CHANNEL_UP = "CHANNEL-UP";
    private static final String VALUE_CONNECTED_DEFLECT = "CONNECTED-DEFLECT";
    private static final String VALUE_CONSECUTIVE = "CONSECUTIVE";
    private static final String VALUE_CONTINUOUS = "CONTINUOUS";
    private static final String VALUE_END = "END";
    private static final String VALUE_ENTERED = "ENTERED";
    private static final String VALUE_ERROR = "ERROR";

    private static final String VALUE_EXCEPTION = "EXCEPTION";
    private static final String VALUE_FAST_FORWARD = "FAST-FORWARD";
    private static final String VALUE_FULLSCREEN_TRICKPLAY_PAGE_LIVE = "TVFullscreenTrickplayPage_live";
    private static final String VALUE_FULLSCREEN_TRICKPLAY_PAGE_RESTART = "TVFullscreenTrickplayPage_restart";
    private static final String VALUE_MEDIA_FULLSCREEN_TRICKPLAY_PAGE_RESTART = "MediaFullscreenTrickplayPage_restart";
    private static final String VALUE_FULLSCREEN_TRICKPLAY_PAGE = "TVFullscreenTrickplayPage";
    private static final String VALUE_HDMI = "HDMI";
    private static final String VALUE_HDMI_HOT_PLUG = "HDMI-HOTPLUG";
    private static final String VALUE_INITIAL_PLAY = "INITIAL-PLAY";
    private static final String VALUE_ISOLATED_STATE = "ISOLATED-STATE";
    private static final String VALUE_ISOLATION = "ISOLATION";
    private static final String VALUE_UPGRADE = "UPGRADE";
    private static final String VALUE_LEFT = "LEFT";
    private static final String VALUE_MISC = "MISC";
    private static final String VALUE_MW = "MW";
    private static final String VALUE_N = "N";
    private static final String VALUE_NETWORK = "NETWORK";
    private static final String VALUE_NETWORK_PVR = "NETWORK-PVR";
    private static final String VALUE_NOINTERNET_START = "NOINTERNET-START";
    private static final String VALUE_NOSIGNAL_PLAYOUT = "NOSIGNAL-PLAYOUT";
    private static final String VALUE_NOSIGNAL_START = "NOSIGNAL-START";
    private static final String VALUE_NUMBER_ENTRY = "NUMBER-ENTRY";
    private static final String VALUE_ON_DEMAND = "ON-DEMAND";
    private static final String VALUE_P = "P";
    private static final String VALUE_PAUSE = "PAUSE";
    private static final String VALUE_PLAY = "PLAY";
    private static final String VALUE_PLAYBACK_BUFFERED = "PLAYBACK-BUFFERED";
    private static final String VALUE_PLAYER = "PLAYER";
    private static final String VALUE_PROGRAM_CHANGE = "PROGRAM-CHANGE";

    private static final String VALUE_RESUME_PLAY = "RESUME-PLAY";
    private static final String VALUE_REWIND = "REWIND";

    private static final String VALUE_SERIES_CONTENT_PAGE = "SeriesContentPage";
    private static final String VALUE_SERIES_CONTENT_PAGE_STOP = "SeriesContentPage_stop";
    private static final String VALUE_STANDBY = "STANDBY";
    private static final String VALUE_STARTOVER = "STARTOVER";
    private static final String VALUE_START_LOADING_SPINNER = "START-LOADING-SPINNER";
    private static final String VALUE_STOP_LOADING_SPINNER = "STOP-LOADING-SPINNER";
    private static final String VALUE_STORE_FULL_SERIES_CONTENT_PAGE = "StoreFullSeriesContentPage_start";
    private static final String VALUE_TIMESHIFT = "TIME-SHIFT";
    private static final String VALUE_TV_PAGE_CONTINUE = "TVPage_continue";
    private static final String VALUE_UNDEFINED = "UNDEFINED";
    private static final String VALUE_VIEWING = "VIEWING";

    private Event<PlaybackLogEvent, PlaybackEventDetails> mLastStartEvent = null;
    private final AtomicLong mStartTimestamp = new AtomicLong();

    public PlaybackEventReporterProximus(Context context, ReporterInfo reporterInfo) {
        super(context, reporterInfo);
    }

    @Override
    public boolean canSend(Event event) {
        if (event.getName(UILogEvent.Unknown) == UILogEvent.Display) {
            // TODO : Move this to the UI event reporter and remove the playback specific part.
            return true;
        }

        if (!PlaybackLogEvent.DOMAIN.equals(event.getDomain())
                && !GenericEvent.DOMAIN.equals(event.getDomain())) {
            // Not interested in that domain.
            return false;
        }

        // Save the playback started event
        switch (event.getName(PlaybackLogEvent.Unknown)) {
            case StartResolved:
                mLastStartEvent = event;

                // Check if there is a saved playback event in mass storage and send to the backend
                sendEventFromMassStorage();
                break;

            case PlaybackStarted:
                mLastStartEvent = event;
                if (TextUtils.isEmpty(MassStorage.lastPlaybackStartedEvent.get(null))) {
                    saveEventToMassStorage();
                }
                break;
        }

        switch (event.getName(PlaybackLogEvent.Unknown)) {
            case StartInitiated:
            case StartResolved:
            case PlaybackEnded:
            case SegmentDownloaded:
            case BitrateChanged:
            case StateChanged:
            case MassStoragePlaybackStopped:
                return false;
        }

        return true;
    }

    @Override
    public long getBatchPeriod() {
        return 0;
    }

    @Override
    @Nullable
    protected LogDetailsProximus formatPayload(Event event) {
        return formatPayload(event, true);
    }

    private LogDetailsProximus formatPayload(Event event, boolean addEventCounter) {
        LogDetailsProximus payload = null;
        switch (event.getDomain()) {

            case PlaybackLogEvent.DOMAIN:
                payload = formatPlaybackPayload(new Event<>(event, mLastStartEvent));
                break;

            case GenericEvent.DOMAIN:
                payload = formatGenericPayload(event);
                break;

            case UILogEvent.DOMAIN:
                payload = formatUiPayload(event);
                break;
        }
        if (payload == null) {
            return null;
        }

        addAgentSpecificParts(payload);
        if (addEventCounter) {
            super.addEventCounter(payload);
        }
        payload.add(KEY_EVENT_TIME, event.getTimestamp());
        return payload;
    }

    /**
     * Create CC DTO object from the event details which can be sent to the data bridge.
     *
     * @param event Event
     * @return Payload as DTO object.
     */
    private LogDetailsProximus formatPlaybackPayload(Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        if (!PlaybackLogEvent.DOMAIN.equals(event.getDomain())) {
            throw new IllegalArgumentException("Invalid playback domain: " + event.getDomain());
        }

        LogDetailsProximus payload = new LogDetailsProximus();

        if (!addGenericParameters(payload, event)) {
            return null;
        }

        payload.add(KEY_HDMI_CONN_OLD, mHdmiReceiver.isConnected());
        payload.add(KEY_HDMI_CONN, mHdmiReceiver.isConnected());
        payload.add(KEY_HDCP_STATUS, mHdmiReceiver.isHdcpActive());

        switch (event.getName(PlaybackLogEvent.Unknown)) {
            default:
                return null;

            case PlaybackStarted:
                payload.add(KEY_ACTION, getStartAction(event));
                addPlaybackStartData(payload, event);
                addConsecutivePlayoutIndicator(payload, event);
                break;

            case MassStoragePlaybackStopped:
                payload.add(KEY_ACTION, VALUE_STOP);
                payload.add(KEY_ACTION_TYPE, VALUE_STOP);
                payload.add(KEY_INVOCATION_POINT, VALUE_POWER_CYCLE);
                break;

            case PlaybackStopped:
            case PlaybackDisconnected:
            case PlaybackUnavailable:
                removeEventFromMassStorage();
                payload.add(KEY_ACTION, VALUE_STOP);
                addPlaybackStopData(payload, event);
                break;

            case PlaybackPaused:
                payload.add(KEY_ACTION, VALUE_PAUSE);
                addPlaybackStopData(payload, event);
                break;

            case PlaybackBuffered:
            case PlaybackResumed:
                payload.add(KEY_ACTION, VALUE_PLAY);
                addPlaybackStartData(payload, event);
                break;

            case PlaybackEnded:
                removeEventFromMassStorage();
                payload.add(KEY_ACTION, VALUE_END);
                addPlaybackStopData(payload, event);
                break;

            case PlaybackFailed:
                removeEventFromMassStorage();
                if (event.getDetail(PlaybackEventDetails.ERROR_NO_INTERNET, false)) {
                    payload.add(KEY_ACTION, VALUE_NOINTERNET_START);
                } else {
                    payload.add(KEY_ACTION, VALUE_NOSIGNAL_START);
                }
                addPlaybackStopData(payload, event);
                break;

            case StartLoadingSpinner:
                payload.add(KEY_ACTION, VALUE_START_LOADING_SPINNER);
                // When the loading is started the playback is stopped.
                addPlaybackStopData(payload, event);
                break;

            case StopLoadingSpinner:
                payload.add(KEY_ACTION, VALUE_STOP_LOADING_SPINNER);
                // When the loading is stopped the playback is started.
                addPlaybackStartData(payload, event);
                break;

            case ExecuteJump:
                payload.add(KEY_ACTION, getJumpDirection(event));
                addPlaybackStopData(payload, event);
                break;

            case JumpExecuted:
                payload.add(KEY_ACTION, getJumpDirection(event));
                addPlaybackStartData(payload, event);
                break;

            case AdTrigger:
                payload.add(KEY_TYPE, VALUE_ADVERTISEMENT);
                payload.add(KEY_SUB_TYPE, VALUE_AD_TRIGGER);
                addTadSpecificPart(payload, event);
                payload.add(KEY_SPLICE_EVENT_IDENTIFIER, event.getDetail(PlaybackEventDetails.SPLICE_ID));
                payload.add(KEY_TAP_REQUEST_SENT, event.getDetail(PlaybackEventDetails.TAP_REQUEST_SENT));
                payload.add(KEY_CHANNEL_INACTIVITY_THRESHOLD, event.getDetail(PlaybackEventDetails.CHANNEL_INACTIVITY_THRESHOLD));
                payload.add(KEY_LAST_ACTIVITY, event.getDetail(PlaybackEventDetails.LAST_ACTIVITY));
                break;

            case AdReplacement:
                payload.add(KEY_TYPE, VALUE_ADVERTISEMENT);
                payload.add(KEY_SUB_TYPE, VALUE_AD_REPLACEMENT);
                addTadSpecificPart(payload, event);
                payload.add(KEY_ADVERTISEMENT_REFERENCE, event.getDetail(PlaybackEventDetails.ADVERTISEMENT_REFERENCE));
                break;
        }

        addNavigationSessionPart(payload);

        return payload;
    }

    /**
     * Create object from the event details which can be sent to the data bridge.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Error+Events+%3A+Allignment+with+Error+Codes
     *
     * @param event Event
     * @return Payload as DTO object.
     */
    private LogDetailsProximus formatGenericPayload(Event<GenericEvent, GenericKeyEvent> event) {
        if (!GenericEvent.DOMAIN.equals(event.getDomain())) {
            throw new IllegalArgumentException("Invalid playback domain: " + event.getDomain());
        }

        String subType = null;
        String category = VALUE_MISC;
        Event<PlaybackLogEvent, PlaybackEventDetails> playbackEvent = null;
        switch (event.getDetail(GenericKeyEvent.DOMAIN, ErrorType.Domain.UNKNOWN)) {
            case APP:
                category = VALUE_AP;
                subType = VALUE_APPLICATION;
                break;

            case MIDDLEWARE:
                category = VALUE_MW;
                subType = VALUE_APPLICATION;
                break;

            case PLAYER:
                playbackEvent = mLastStartEvent;
                category = VALUE_P;
                if (!event.getDetail(GenericKeyEvent.INTERNET_AVAILABLE, true)) {
                    subType = VALUE_NETWORK;
                } else {
                    if (!event.getDetail(GenericKeyEvent.BACKEND_AVAILABLE, true)) {
                        subType = VALUE_ISOLATION;
                        break;
                    }
                    subType = VALUE_PLAYER;
                }
                break;

            case ENTITLEMENT:
            case PROVISIONING:
                category = VALUE_CC;
                subType = VALUE_APPLICATION;
                break;

            case NETWORK:
                category = VALUE_N;
                subType = VALUE_APPLICATION;
                break;

            case BACKEND:
                category = VALUE_BE;
                subType = VALUE_APPLICATION;
                break;
        }

        LogDetailsProximus payload = new LogDetailsProximus();
        switch (event.getName(GenericEvent.Unknown)) {
            default:
                return null;

            case IsolationMode:
                payload.add(KEY_TYPE, VALUE_TECHNICAL);
                payload.add(KEY_EVENT_TYPE_OLD, VALUE_TECHNICAL);
                String stateAction = event.getDetail(GenericKeyEvent.BACKEND_AVAILABLE, false) ?
                        VALUE_LEFT : VALUE_ENTERED;
                if (event.getDetail(GenericKeyEvent.STARTUP_FINISHED, false)) {
                    payload.add(KEY_SUB_TYPE, VALUE_CONNECTED_DEFLECT);
                    payload.add(KEY_EVENT_CAT_OLD, VALUE_CONNECTED_DEFLECT);
                    payload.add(KEY_CONNECTED_DEFLECT_STATE_ACTION, stateAction);
                } else {
                    if (MemoryCache.StartedAfterBoot.get(false)) {
                        payload.add(KEY_OPERATIONAL_MODE, VALUE_BOOT);
                    } else {
                        payload.add(KEY_OPERATIONAL_MODE, VALUE_APP_START);
                    }
                    payload.add(KEY_SUB_TYPE, VALUE_ISOLATED_STATE);
                    payload.add(KEY_EVENT_CAT_OLD, VALUE_ISOLATED_STATE);
                    payload.add(KEY_ISOLATION_STATE_ACTION, stateAction);
                }
                return payload;

            case ErrorEvent:
                payload.add(KEY_CATEGORY, VALUE_EVENT);
                payload.add(KEY_TYPE, VALUE_ERROR);
                payload.add(KEY_EVENT_TYPE_OLD, VALUE_ERROR);
                payload.add(KEY_SUB_TYPE, subType);
                payload.add(KEY_ERROR_TYPE, category);
                addErrorDetails(payload, event);
                break;

            case PlaybackException:
                addExceptionDetails(payload, event);
                break;
        }

        if (playbackEvent != null) {
            HdmiInfo info = mMwProxy.getHdmiInfo();
            payload.add(KEY_HDMI_CONN_OLD, info != null && info.isConnected());
            payload.add(KEY_HDMI_CONN, info != null && info.isConnected());
            payload.add(KEY_HDCP_STATUS, info != null && info.isHdcpActive());
            payload.add(KEY_PLAYOUT_URL, streamUrl(playbackEvent));
            switch (playbackEvent.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None)) {
                case LiveTvOtt:
                case LiveTvDvbC:
                case LiveTvDvbT:
                case LiveTvIpTv:
                    payload.add(KEY_PLAYOUT_CAT, VALUE_LINEAR);
                    payload.add(KEY_VIEWING_TYPE, VALUE_LINEAR);
                    addStreamErrorParameters(payload, playbackEvent);
                    break;

                case Recording:
                    payload.add(KEY_PLAYOUT_CAT, VALUE_NPVR);
                    payload.add(KEY_VIEWING_TYPE, VALUE_NPVR);
                    addStreamErrorParameters(payload, playbackEvent);
                    break;

                case Replay:
                    payload.add(KEY_PLAYOUT_CAT, VALUE_CUTV);
                    payload.add(KEY_VIEWING_TYPE, VALUE_CUTV);
                    addStreamErrorParameters(payload, playbackEvent);
                    break;

                case RadioOtt:
                case RadioIpTv:
                    payload.add(KEY_PLAYOUT_CAT, VALUE_RADIO);
                    payload.add(KEY_VIEWING_TYPE, VALUE_RADIO);
                    addStreamErrorParameters(payload, playbackEvent);
                    break;

                case Vod:
                case Trailer:
                    payload.add(KEY_PLAYOUT_CAT, VALUE_VOD);
                    payload.add(KEY_VIEWING_TYPE, VALUE_VOD);
                    payload.add(KEY_VOD_TITLE_OLD, playbackEvent.getDetail(PlaybackEventDetails.VOD_TITLE));
                    payload.add(KEY_ITEM_TITLE, playbackEvent.getDetail(PlaybackEventDetails.VOD_TITLE));
                    payload.add(KEY_ITEM_ID, playbackEvent.getDetail(PlaybackEventDetails.VOD_ITEM_IDENTIFIER));
                    payload.add(KEY_VOD_ITEM_IDENTIFIER_OLD, playbackEvent.getDetail(PlaybackEventDetails.VOD_ITEM_IDENTIFIER));
                    payload.add(KEY_EXTERNAL_ID, playbackEvent.getDetail(PlaybackEventDetails.VOD_EXTERNAL_IDENTIFIER));
                    payload.add(KEY_VOD_EXTERNAL_IDENTIFIER_OLD, playbackEvent.getDetail(PlaybackEventDetails.VOD_EXTERNAL_IDENTIFIER));
                    break;

                case App:
                    payload.add(KEY_PLAYOUT_CAT, VALUE_APP);
                    payload.add(KEY_ITEM_ID, playbackEvent.getDetail(PlaybackEventDetails.ITEM_ID));
                    payload.add(KEY_ITEM_TITLE, playbackEvent.getDetail(PlaybackEventDetails.ITEM_TITLE));
                    break;

                default:
                    payload.add(KEY_PLAYOUT_CAT, VALUE_UNDEFINED);
            }
        }

        addStandbyStatePart(payload, event.getTimestamp());
        return payload;
    }

    /**
     * Add the stream error parameters to the payload.
     */
    private void addStreamErrorParameters(LogDetailsProximus payload,
                                          Event<PlaybackLogEvent, PlaybackEventDetails> playbackEvent) {
        payload.add(KEY_ITEM_ID, playbackEvent.getDetail(PlaybackEventDetails.PROGRAM_ID));
        payload.add(KEY_PROGRAM_REFERENCE_NUMBER_OLD, playbackEvent.getDetail(PlaybackEventDetails.PROGRAM_ID));
        payload.add(KEY_ITEM_TITLE, playbackEvent.getDetail(PlaybackEventDetails.ITEM_TITLE));
        payload.add(KEY_PROGRAM_TITLE_OLD, playbackEvent.getDetail(PlaybackEventDetails.ITEM_TITLE));
        payload.add(KEY_CHANNEL_ID, playbackEvent.getDetail(PlaybackEventDetails.CHANNEL_ID));
        payload.add(KEY_CHANNEL_EXTERNAL_IDENTIFIER_OLD, playbackEvent.getDetail(PlaybackEventDetails.CHANNEL_ID));
        payload.add(KEY_SCHEDULED_TRAIL_IDENTIFIER, getScheduledTrailIdentifier(playbackEvent.getDetail(PlaybackEventDetails.PROGRAM_ITEM_ID)));
        payload.add(KEY_CALL_LETTER, playbackEvent.getDetail(PlaybackEventDetails.CHANNEL_CALL_LETTER));
    }

    /**
     * Create data collection payload from event.
     *
     * @param event Event
     * @return Payload as DTO object.
     */
    private LogDetailsProximus formatUiPayload(Event<UILogEvent, UILogEvent.Detail> event) {
        if (!UILogEvent.DOMAIN.equals(event.getDomain())) {
            throw new IllegalArgumentException("Invalid playback domain: " + event.getDomain());
        }

        LogDetailsProximus payload = new LogDetailsProximus();
        UILogEvent eventKind = event.getName(UILogEvent.Unknown);
        switch (eventKind) {
            default:
                return null;

            case Display:
                if (mLastStartEvent == null) {
                    return null;
                }

                if (!addGenericParameters(payload, mLastStartEvent)) {
                    return null;
                }

                addContentSpecificParameters(payload, mLastStartEvent);
                addTransportSpecificParameters(payload, mLastStartEvent);
                addActionSpecificParameters(payload, mLastStartEvent);
                addPositionSpecificParameters(payload, mLastStartEvent);

                payload.add(KEY_HDMI_CONN_OLD, event.getDetail(UILogEvent.Detail.HDMIPlugState, false));
                payload.add(KEY_HDMI_CONN, event.getDetail(UILogEvent.Detail.HDMIPlugState, false));
                if (event.getDetail(UILogEvent.Detail.ActiveState, false)) {
                    payload.add(KEY_ACTION, VALUE_PLAY);
                    addPlaybackStartData(payload, mLastStartEvent, event.getTimestamp());
                } else {
                    payload.add(KEY_ACTION, VALUE_STOP);
                    addPlaybackStopData(payload, event.getTimestamp());
                }
                payload.add(KEY_INVOCATION_POINT, VALUE_HDMI_HOT_PLUG);
                break;
        }
        return payload;
    }

    /**
     * Add the generic parameters to the json object.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Detailed+Specifications+for+Viewing+events
     */
    private boolean addGenericParameters(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        switch (event.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None)) {
            case LiveTvOtt:
            case LiveTvDvbC:
            case LiveTvDvbT:
            case LiveTvIpTv:
                addLiveTvViewingPart(payload, event);
                return true;

            case RadioOtt:
            case RadioIpTv:
                addRadioViewingPart(payload, event);
                return true;

            case Recording:
                addRecordingViewingPart(payload, event);
                return true;

            case Replay:
                addReplayViewingPart(payload, event);
                return true;

            case Vod:
            case Trailer:
                addVodViewingPart(payload, event);
                return true;
        }
        return false;
    }

    private void addTadSpecificPart(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        payload.add(KEY_ACTION_TYPE, VALUE_TRANSIENT);
        switch (event.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None)) {
            case RadioOtt:
            case RadioIpTv:
                payload.add(KEY_STREAM_PURPOSE, VALUE_RADIO);
                break;
            case LiveTvIpTv:
                if (event.getDetail(PlaybackEventDetails.IS_LIVE, true)) {
                    payload.add(KEY_STREAM_PURPOSE, VALUE_LINEAR);
                } else {
                    payload.add(KEY_STREAM_PURPOSE, VALUE_TIMESHIFT);
                }
                break;
            case Replay:
                payload.add(KEY_STREAM_PURPOSE, VALUE_CATCH_UP);
                break;
            case Vod:
                payload.add(KEY_STREAM_PURPOSE, VALUE_ON_DEMAND);
                break;
        }
        payload.add(KEY_CALL_LETTER, event.getDetail(PlaybackEventDetails.CHANNEL_CALL_LETTER));
        payload.add(KEY_CHANNEL_EXTERNAL_IDENTIFIER_OLD, event.getDetail(PlaybackEventDetails.CHANNEL_ID));
        payload.add(KEY_SCHEDULED_TRAIL_IDENTIFIER, getScheduledTrailIdentifier(event.getDetail(PlaybackEventDetails.PROGRAM_ITEM_ID)));
        payload.add(KEY_PROGRAM_REFERENCE_NUMBER_OLD, event.getDetail(PlaybackEventDetails.PROGRAM_ID));
        payload.add(KEY_PROGRAM_TITLE_OLD, event.getDetail(PlaybackEventDetails.PROGRAM_TITLE));
    }

    /**
     * Add the generic parameters for Live TV.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/LINEAR+viewing+events
     */
    private void addLiveTvViewingPart(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        payload.add(KEY_CATEGORY, VALUE_EVENT);
        payload.add(KEY_TYPE, VALUE_VIEWING);
        payload.add(KEY_EVENT_TYPE_OLD, VALUE_VIEWING);
        if (event.getDetail(PlaybackEventDetails.IS_LIVE, true)) {
            payload.add(KEY_SUB_TYPE, VALUE_LINEAR);
            payload.add(KEY_EVENT_CAT_OLD, VALUE_LINEAR);
        } else {
            payload.add(KEY_SUB_TYPE, VALUE_TIMESHIFT);
            payload.add(KEY_EVENT_CAT_OLD, VALUE_TSTV);
        }

        addContentSpecificParameters(payload, event);
        addTransportSpecificParameters(payload, event);
        addActionSpecificParameters(payload, event);
        addPositionSpecificParameters(payload, event);
    }

    /**
     * Add the generic parameters for Radio.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/RADIO+viewing+%28listening%29+events
     */
    private void addRadioViewingPart(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        payload.add(KEY_CATEGORY, VALUE_EVENT);
        payload.add(KEY_TYPE, VALUE_VIEWING);
        payload.add(KEY_EVENT_TYPE_OLD, VALUE_VIEWING);
        payload.add(KEY_SUB_TYPE, VALUE_RADIO);
        payload.add(KEY_EVENT_CAT_OLD, VALUE_RADIO);

        addContentSpecificParameters(payload, event);
        addTransportSpecificParameters(payload, event);
        addActionSpecificParameters(payload, event);
        addPositionSpecificParameters(payload, event);
    }

    /**
     * Add the generic parameters for Recording.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Network+PVR+viewing+events
     */
    private void addRecordingViewingPart(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        payload.add(KEY_CATEGORY, VALUE_EVENT);
        payload.add(KEY_EVENT_TYPE_OLD, VALUE_VIEWING);
        payload.add(KEY_TYPE, VALUE_VIEWING);
        payload.add(KEY_EVENT_CAT_OLD, VALUE_NPVR);
        payload.add(KEY_SUB_TYPE, VALUE_NETWORK_PVR);

        addContentSpecificParameters(payload, event);
        addTransportSpecificParameters(payload, event);
        addActionSpecificParameters(payload, event);
        addPositionSpecificParameters(payload, event);
    }

    /**
     * Add the generic parameters for Replay.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Catch+Up+TV+viewing+events
     */
    private void addReplayViewingPart(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        payload.add(KEY_CATEGORY, VALUE_EVENT);
        payload.add(KEY_EVENT_TYPE_OLD, VALUE_VIEWING);
        payload.add(KEY_TYPE, VALUE_VIEWING);
        payload.add(KEY_SUB_TYPE, VALUE_CATCH_UP);
        payload.add(KEY_EVENT_CAT_OLD, VALUE_CUTV);

        addContentSpecificParameters(payload, event);
        addTransportSpecificParameters(payload, event);
        addActionSpecificParameters(payload, event);
        addPositionSpecificParameters(payload, event);
    }

    /**
     * Add the generic parameters for VOD.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Video+On+Demand+viewing+events
     */
    private void addVodViewingPart(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        payload.add(KEY_CATEGORY, VALUE_EVENT);
        payload.add(KEY_EVENT_TYPE_OLD, VALUE_VIEWING);
        payload.add(KEY_TYPE, VALUE_VIEWING);
        payload.add(KEY_SUB_TYPE, VALUE_ON_DEMAND);
        payload.add(KEY_EVENT_CAT_OLD, VALUE_VOD);

        addContentSpecificParameters(payload, event);
        addTransportSpecificParameters(payload, event);
        addActionSpecificParameters(payload, event);
        addPositionSpecificParameters(payload, event);
    }

    /**
     * Add the content specific parameters for the given stream type.
     * https://confluenceprd-proximuscorp.msappproxy.net/pages/viewpage.action?pageId=122946819
     */
    private void addContentSpecificParameters(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        switch (event.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None)) {
            case LiveTvOtt:
            case LiveTvDvbC:
            case LiveTvDvbT:
            case LiveTvIpTv:
                addContentSpecificParametersForLinearTv(payload, event);
                break;
            case RadioOtt:
            case RadioIpTv:
            case Recording:
            case Replay:
                addContentSpecificParametersForBroadcastContent(payload, event);
                break;
            case Vod:
            case Trailer:
                addContentSpecificParametersForVodContent(payload, event);
                break;
        }
    }

    /**
     * Add content specific parameters for Linear Tv
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/LINEAR+viewing+events
     */
    private void addContentSpecificParametersForLinearTv(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        if (event.getDetail(PlaybackEventDetails.IS_LIVE, true)) {
            payload.add(KEY_CHANNEL_ID, event.getDetail(PlaybackEventDetails.CHANNEL_ID));
            payload.add(KEY_CHANNEL_EXTERNAL_IDENTIFIER_OLD, event.getDetail(PlaybackEventDetails.CHANNEL_ID));
        }
        addContentSpecificParametersForBroadcastContent(payload, event);
    }

    /**
     * Add the content specific parameters for broadcaster related content.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Content+specific+parameters
     */
    private void addContentSpecificParametersForBroadcastContent(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        payload.add(KEY_CALL_LETTER, event.getDetail(PlaybackEventDetails.CHANNEL_CALL_LETTER));
        payload.add(KEY_CHANNEL_EXTERNAL_IDENTIFIER_OLD, event.getDetail(PlaybackEventDetails.CHANNEL_ID));
        payload.add(KEY_CHANNEL_ID, event.getDetail(PlaybackEventDetails.CHANNEL_ID));
        payload.add(KEY_PROGRAM_TITLE_OLD, event.getDetail(PlaybackEventDetails.PROGRAM_TITLE));
        payload.add(KEY_ITEM_TITLE, event.getDetail(PlaybackEventDetails.PROGRAM_TITLE));
        payload.add(KEY_PROGRAM_REFERENCE_NUMBER_OLD, event.getDetail(PlaybackEventDetails.PROGRAM_ID));
        payload.add(KEY_ITEM_ID, event.getDetail(PlaybackEventDetails.PROGRAM_ID));
        payload.add(KEY_SCHEDULED_TRAIL_IDENTIFIER, getScheduledTrailIdentifier(event.getDetail(PlaybackEventDetails.PROGRAM_ITEM_ID)));
        payload.add(KEY_ITEM_TYPE, getItemType(event));
        payload.add(KEY_RESOLUTION, event.getDetail(PlaybackEventDetails.ITEM_RESOLUTION));
        payload.add(KEY_START_TIME, event.getDetail(PlaybackEventDetails.ITEM_START));
        payload.add(KEY_PROGRAM_DURATION, event.getDetail(PlaybackEventDetails.ITEM_DURATION, 0));
        payload.add(KEY_EPISODE_TITLE, event.getDetail(PlaybackEventDetails.EPISODE_TITLE));
        payload.add(KEY_SERIES_NR, event.getDetail(PlaybackEventDetails.SEASON_NR));
        payload.add(KEY_EPISODE_NR, event.getDetail(PlaybackEventDetails.EPISODE_NR));
        payload.add(KEY_PROGRAM_CATEGORY, event.getDetail(PlaybackEventDetails.PROGRAM_CATEGORY));
        payload.add(KEY_PROGRAM_SUB_CATEGORY, event.getDetail(PlaybackEventDetails.PROGRAM_SUB_CATEGORY));
    }

    /**
     * Add the content specific parameters for VOD content.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Content+specific+parameters
     */
    private void addContentSpecificParametersForVodContent(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        payload.add(KEY_VOD_TITLE_OLD, event.getDetail(PlaybackEventDetails.VOD_TITLE));
        payload.add(KEY_ITEM_TITLE, event.getDetail(PlaybackEventDetails.ITEM_TITLE));
        payload.add(KEY_GROUP_TITLE, event.getDetail(PlaybackEventDetails.GROUP_TITLE));
        payload.add(KEY_VOD_ITEM_IDENTIFIER_OLD, event.getDetail(PlaybackEventDetails.VOD_ITEM_IDENTIFIER));
        payload.add(KEY_ITEM_ID, event.getDetail(PlaybackEventDetails.VOD_ITEM_IDENTIFIER));
        payload.add(KEY_VOD_EXTERNAL_IDENTIFIER_OLD, event.getDetail(PlaybackEventDetails.VOD_EXTERNAL_IDENTIFIER));
        payload.add(KEY_EXTERNAL_ID, event.getDetail(PlaybackEventDetails.VOD_EXTERNAL_IDENTIFIER));
        payload.add(KEY_SERIES_TITLE, event.getDetail(PlaybackEventDetails.ITEM_TITLE));
        payload.add(KEY_SERIES_NR, event.getDetail(PlaybackEventDetails.SEASON_NR));
        payload.add(KEY_EPISODE_NR, event.getDetail(PlaybackEventDetails.EPISODE_NR));
        payload.add(KEY_GENRE, event.getDetail(PlaybackEventDetails.ITEM_GENRE));
        payload.add(KEY_SUPER_GENRE, event.getDetail(PlaybackEventDetails.ITEM_SUPER_GENRE));
        payload.add(KEY_VOD_TYPE, getVodType(event.getDetail(PlaybackEventDetails.VOD_TYPE, VodItemType.UNDEFINED)));
        payload.add(KEY_RESOLUTION, event.getDetail(PlaybackEventDetails.ITEM_RESOLUTION));
        boolean isTrailer = event.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.class) == PlaybackDomain.Trailer;
        payload.add(KEY_PREVIEW, isTrailer ? VALUE_TRUE : VALUE_FALSE);
    }

    /**
     * Add the transport specific parameters.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Transport+Specific+Parameters
     */
    private void addTransportSpecificParameters(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        payload.add(KEY_STREAM_CONTENT_URL, streamUrl(event));
        payload.add(KEY_STREAM_CONTROL_PROTOCOL, streamProtocol(event));
        payload.add(KEY_STREAM_MODE, streamCast(event));
        payload.add(KEY_STREAM_ADAPTIVITY_INDICATOR, streamRate(event));
    }

    /**
     * Add action specific parameters.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Action+Specific+parameters
     */
    private void addActionSpecificParameters(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        payload.add(KEY_INVOCATION_POINT, getInvocationPoint(event));
    }

    /**
     * Add the position specific parameters.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/%28Time%29+position+specific+parameters
     */
    private void addPositionSpecificParameters(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        payload.add(KEY_TIME_POSITION, event.getDetail(PlaybackEventDetails.POSITION, 0L));
    }

    /**
     * Add the consecutive_playout_indicator parameter for the PlaybackStarted and PlaybackStopped events.
     */
    private void addConsecutivePlayoutIndicator(LogDetailsProximus payload,
                                                Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        switch (event.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None)) {
            case LiveTvOtt:
            case LiveTvDvbC:
            case LiveTvDvbT:
            case LiveTvIpTv:
                if (!event.getDetail(PlaybackEventDetails.IS_LIVE, true)) {
                    // The parameter is required only for TSTV
                    payload.add(KEY_CONSECUTIVE_PLAYOUT_INDICATOR, getPlayoutIndicator(event));
                }
                break;

            case Recording:
            case Vod:
                payload.add(KEY_CONSECUTIVE_PLAYOUT_INDICATOR, getPlayoutIndicator(event));
                break;

            case Replay:
                long cuNow = event.getTimestamp();
                long cuStart = event.getDetail(PlaybackEventDetails.ITEM_START, event.getTimestamp());
                long cuEnd = cuStart + event.getDetail(PlaybackEventDetails.ITEM_DURATION, 0L);
                StartAction action = event.getDetail(PlaybackEventDetails.ACTION, StartAction.Undefined);
                if (action == StartAction.ProgramChanged) {
                    payload.add(KEY_CONSECUTIVE_PLAYOUT_INDICATOR, VALUE_CONTINUOUS);
                } else if (cuNow > cuStart && cuNow < cuEnd) {
                    payload.add(KEY_CONSECUTIVE_PLAYOUT_INDICATOR, VALUE_STARTOVER);
                } else {
                    payload.add(KEY_CONSECUTIVE_PLAYOUT_INDICATOR, VALUE_CATCHUP);
                }
                break;
        }
    }

    private void addPlaybackStartData(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        addPlaybackStartData(payload, event, event.getTimestamp());
    }

    private void addPlaybackStartData(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event, long timeStamp) {
        String actionType = VALUE_START;
        if (!event.getDetail(PlaybackEventDetails.IS_PLAYING, false) || !mStartTimestamp.compareAndSet(0, timeStamp)) {
            actionType = VALUE_TRANSIENT;
        }

        if (MassStorage.lastPlaybackStartTimestamp.get(0L) == 0) {
            MassStorage.lastPlaybackStartTimestamp.edit().put(timeStamp);
        }

        payload.add(KEY_ACTION_TYPE, actionType);
        payload.add(KEY_VIEWING_DURATION, 0);
        payload.add(KEY_VIEWING_IDENTIFIER, getIdentifier(timeStamp));
    }

    private void addPlaybackStopData(LogDetailsProximus payload, Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        addPlaybackStopData(payload, event.getTimestamp());
    }

    private void addPlaybackStopData(LogDetailsProximus payload, long timeStamp) {
        long startTimestamp = mStartTimestamp.getAndSet(0);
        MassStorage.lastPlaybackStartTimestamp.edit().put(0L);

        long eventStartTimestamp;
        String actionType;
        long viewingDuration;

        if (startTimestamp != 0) {
            eventStartTimestamp = startTimestamp;
            actionType = VALUE_STOP;
            viewingDuration = getViewingDuration(startTimestamp);
        } else {
            eventStartTimestamp = timeStamp;
            actionType = VALUE_TRANSIENT;
            viewingDuration = 0;
        }

        payload.add(KEY_ACTION_TYPE, actionType);
        payload.add(KEY_VIEWING_DURATION, viewingDuration);
        payload.add(KEY_VIEWING_IDENTIFIER, getIdentifier(eventStartTimestamp));
    }

    private void addErrorDetails(LogDetailsProximus payload, Event<GenericEvent, GenericKeyEvent> event) {
        payload.add(KEY_ERROR_CODE, event.getDetail(GenericKeyEvent.CODE, NONE));
        payload.add(KEY_ERROR_KEY, event.getDetail(GenericKeyEvent.KEY, NONE));
        payload.add(KEY_ERROR_MESSAGE, event.getDetail(GenericKeyEvent.MESSAGE, NONE));
    }

    private void addExceptionDetails(LogDetailsProximus payload, Event<GenericEvent, GenericKeyEvent> event) {
        payload.add(KEY_CATEGORY, VALUE_EVENT);
        payload.add(KEY_TYPE, VALUE_EXCEPTION);
        payload.add(KEY_EVENT_TYPE_OLD, VALUE_EXCEPTION);
        payload.add(KEY_SUB_TYPE, NONE);
        payload.add(KEY_EXCEPTION_TYPE, event.getDetail(GenericKeyEvent.KEY, NONE));
        payload.add(KEY_EXCEPTION_MESSAGE, event.getDetail(GenericKeyEvent.MESSAGE, NONE));
    }

    private void addStandbyStatePart(LogDetailsProximus payload, long timeStamp) {
        long intoStandbyTime = Settings.intoStandbyTime.get(0L);
        long outOfStandbyTime = Settings.outOfStandbyTime.get(0L);
        payload.add(KEY_INTO_STANDBY, intoStandbyTime);
        payload.add(KEY_OUT_OF_STANDBY, outOfStandbyTime);
        payload.add(KEY_INTO_STANDBY_ELAPSED_TIME, intoStandbyTime > 0
                ? timeStamp - intoStandbyTime : 0);
        payload.add(KEY_OUT_STANDBY_ELAPSED_TIME, outOfStandbyTime > 0
                ? timeStamp - outOfStandbyTime : 0);
    }

    private long getViewingDuration(long eventStartTime) {
        long playbackTime = 0;
        if (eventStartTime > 0) {
            playbackTime = System.currentTimeMillis() - eventStartTime;
        }
        return playbackTime;
    }

    private String getStartAction(Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        StartAction action = event.getDetail(PlaybackEventDetails.ACTION, StartAction.Undefined);
        if (action == StartAction.Restart) {
            String startPowerStateAction = getStartPowerStateAction();
            if (startPowerStateAction != null) {
                return startPowerStateAction;
            }
        }

        // Specific actions for NPVR
        if (event.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None) == PlaybackDomain.Recording) {
            boolean isResumeBookmark = event.getDetail(PlaybackEventDetails.RESUME_BOOKMARK, false);
            if (isResumeBookmark) {
                return VALUE_RESUME_PLAY;
            }
            return VALUE_INITIAL_PLAY;
        }
        return VALUE_PLAY;
    }

    private String getStartPowerStateAction() {
        // power-on might be caused by: app restart, exit standby, device reboot, ...
        if (MemoryCache.StartedAfterBoot.get(false)) {
            // the device was rebooted...

            // first we have to check if we had a software update.
            String tvAppVersion = mMwProxy.getAppVersion();
            String tuneTvAppVersion = MassStorage.tuneTvAppVersion.get(null);
            if (tvAppVersion != null && !tvAppVersion.equals(tuneTvAppVersion)) {
                // we have a new tv app version
                MassStorage.tuneTvAppVersion.edit().put(tvAppVersion);
                return VALUE_UPGRADE;
            }

            return VALUE_HARD_PWR_ON;
        }
        return null;
    }

    /**
     * @return Allowed values for the item_type parameter.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Content+specific+parameters
     */
    private String getItemType(Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        switch (event.getDetail(PlaybackEventDetails.ITEM_TYPE, PlaybackItemType.UNDEFINED)) {
            case SINGLE:
                return VALUE_SINGLE;

            case SERIES:
                return VALUE_SERIES;
        }
        return null;
    }

    /**
     * @return Allowed values for the action specific invocation_point parameter.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/The+parameter+%3A+invocation+point
     */
    private String getInvocationPoint(Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        switch (event.getName(PlaybackLogEvent.Unknown)) {
            case PlaybackBuffered:
                return VALUE_PLAYBACK_BUFFERED;

            case PlaybackUnavailable:
            case PlaybackDisconnected:
                return VALUE_NOSIGNAL_PLAYOUT;
        }

        switch (event.getDetail(PlaybackEventDetails.ACTION, StartAction.Undefined)) {
            case BtnUp:
                return VALUE_CHANNEL_UP;

            case BtnDown:
                return VALUE_CHANNEL_DOWN;

            case BtnNumber:
                return VALUE_NUMBER_ENTRY;

            case BtnRadio:
                return VALUE_RADIO;

            case BtnStop:
                return VALUE_STOP;

            case Pause:
                return VALUE_PAUSE;

            case Resume:
                return VALUE_PLAY;

            case Rent:
            case Ui:
                return getPageName(MemoryCache.LastOpenedPage.getEnum(UILog.Page.Unknown));

            case ProgramChanged:
                return VALUE_PROGRAM_CHANGE;

            case ContinuousPlay:
                PlaybackDomain currentDomain = event.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None);
                if (currentDomain == PlaybackDomain.Recording) {
                    return VALUE_SERIES_CONTENT_PAGE;
                }
                return VALUE_STORE_FULL_SERIES_CONTENT_PAGE;

            case ReplayEnded:
                return VALUE_CUTV;

            case RecordingEnded:
                return VALUE_ALL_RECORDINGS_PAGE_STOP;

            case SeriesRecordingEnded:
                return VALUE_SERIES_CONTENT_PAGE_STOP;

            case VodEnded:
                return VALUE_ACTION_MENU_VOD_WATCH;

            case TrailerEnded:
                return VALUE_ACTION_MENU_VOD_TRAILER;

            case JumpLive:
                return VALUE_FULLSCREEN_TRICKPLAY_PAGE_LIVE;

            case StandBy:
                return VALUE_STANDBY;

            case Hdmi:
                return VALUE_HDMI_HOT_PLUG;

            case Internet:
                return VALUE_NETWORK;

            case Restart:
                return VALUE_START;

            case StartOver:
                if (event.getDetail(PlaybackEventDetails.STREAM_TYPE, PlaybackDomain.None) == PlaybackDomain.Replay) {
                    return VALUE_FULLSCREEN_TRICKPLAY_PAGE_RESTART;
                }
                return VALUE_MEDIA_FULLSCREEN_TRICKPLAY_PAGE_RESTART;

            case LiveStartOver:
                return VALUE_FULLSCREEN_TRICKPLAY_PAGE;

            case ActivityStart:
                // TODO: We can't detect if an app was closed...
                return VALUE_APP_EXIT;

            case Rewind:
                return VALUE_REWIND;

            case Forward:
                return VALUE_FAST_FORWARD;

            case TSTVEnded:
                return VALUE_TSTV;

            case BufferRestart:
                return VALUE_BUFFER_RESTART;

            case TVPageContinue:
                return VALUE_TV_PAGE_CONTINUE;

            default:
                return NONE;
        }
    }

    /**
     * @return Jump direction based on the start action.
     */
    private String getJumpDirection(Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        PlaybackLogEvent logEvent = event.getName(PlaybackLogEvent.Unknown);
        boolean isPlaying = event.getDetail(PlaybackEventDetails.IS_PLAYING, false);
        String returnValue = logEvent == PlaybackLogEvent.ExecuteJump ? VALUE_STOP : VALUE_PLAY;

        switch (event.getDetail(PlaybackEventDetails.ACTION, StartAction.Undefined)) {
            case Rewind:
                if (!isPlaying) {
                    return VALUE_REWIND;
                }
                return returnValue;
            case Forward:
                if (!isPlaying) {
                    return VALUE_FAST_FORWARD;
                }
                return returnValue;
            case BufferRestart:
                return VALUE_REWIND;
            case TSTVEnded:
            case JumpLive:
                return VALUE_FAST_FORWARD;
            case BtnStop:
                return returnValue;
            default:
                return null;
        }
    }

    /**
     * @return Allowed values for the consecutive_playout_indicator field.
     */
    private String getPlayoutIndicator(Event<PlaybackLogEvent, PlaybackEventDetails> event) {
        switch (event.getDetail(PlaybackEventDetails.ACTION, StartAction.Undefined)) {
            case ProgramChanged:
            case ContinuousPlay:
                return VALUE_CONSECUTIVE;
            default:
                return VALUE_START;
        }
    }

    /**
     * Save a playback event to mass storage.
     */
    private void saveEventToMassStorage() {
        Event<PlaybackLogEvent, PlaybackEventDetails> event = new Event<>(PlaybackLogEvent.MassStoragePlaybackStopped);
        LogDetailsProximus payload = formatPayload(event, false);
        MassStorage.lastPlaybackStartedEvent.edit().put(GsonUtils.getGson().toJson(payload));
    }

    /**
     * Checks if there is a saved event in mass storage and sends to the backend.
     */
    private void sendEventFromMassStorage() {
        LogDetailsProximus payload =
                GsonUtils.getGson().fromJson(MassStorage.lastPlaybackStartedEvent.get(null),
                        TYPE_STRING_TO_LOG_DETAILS_PROXIMUS);
        if (payload == null) {
            return;
        }

        // Update the event timestamp with the last known heart beat epoch
        long lastHeartBeatEpoch = MassStorage.lastKnownHeartBeatEpoch.get(0L);
        long eventTimestamp = MassStorage.lastPlaybackStartTimestamp.get(0L);

        addEventCounter(payload);
        if (lastHeartBeatEpoch != 0 && eventTimestamp != 0) {
            payload.add(KEY_EVENT_TIME, lastHeartBeatEpoch);
            payload.add(KEY_VIEWING_DURATION, Math.max(lastHeartBeatEpoch - eventTimestamp, 0));
            payload.add(KEY_VIEWING_IDENTIFIER, getIdentifier(lastHeartBeatEpoch));
        }

        // Update the action_type according to the playback state
        if (eventTimestamp != 0) {
            payload.add(KEY_ACTION_TYPE, VALUE_STOP);
        } else {
            payload.add(KEY_ACTION_TYPE, VALUE_TRANSIENT);
        }

        try {
            sendEvents(getEndpoint(), getHeaders(), getTimeoutPeriod(),
                    new SendEventsRequest(payload));
            removeEventFromMassStorage();
        } catch (Exception ex) {
            android.util.Log.e(TAG, "Failed to send the event from mass storage", ex);
        }
    }

    /**
     * Removes the saved event from mass storage.
     */
    private void removeEventFromMassStorage() {
        MassStorage.batchEdit()
            .remove(MassStorage.lastPlaybackStartedEvent)
            .remove(MassStorage.lastPlaybackStartTimestamp)
            .persistNow();
    }
}
