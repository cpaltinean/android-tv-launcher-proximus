package tv.threess.threeready.data.proximus.tv.model.advertisement;

import tv.threess.threeready.api.generic.helper.ArrayUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Vast response contains the information which pre-downloaded ad file should be used as a replacement.
 */
@XmlRootElement(name = "VAST")
public class VastResponse {

    @XmlElement(name = "Ad")
    private List<VastAd> mAds;

    public String getId() {
        VastCreative creative = getCreative();
        if (creative == null) {
            return null;
        }

        return creative.getId();
    }

    protected VastInLine getInLine() {
        if (ArrayUtils.isEmpty(mAds)) {
            return null;
        }

        // The last ad server in the ad supply chain serves an <InLine> element.
        return mAds.get(mAds.size() - 1).getInLine();
    }

    protected VastCreative getCreative() {
        VastInLine inline = getInLine();
        if (inline == null) {
            return null;
        }

        List<VastCreative> creatives = inline.getCreatives();
        if (ArrayUtils.isEmpty(creatives)) {
            return null;
        }

        // use the first creative: there should be a single creative
        return creatives.get(0);
    }

    protected VastLinear getSequence() {
        VastCreative creative = getCreative();
        if (creative == null) {
            return null;
        }

        return creative.getSequence();
    }

    protected VastMediaFile getMediaFile() {
        VastLinear linear = getSequence();
        if (linear == null) {
            return null;
        }

        List<VastMediaFile> mediaFiles = linear.getMediaFiles();
        if (ArrayUtils.isEmpty(mediaFiles)) {
            return null;
        }

        // use the first media-file: there should be a single file
        return mediaFiles.get(0);
    }
}
