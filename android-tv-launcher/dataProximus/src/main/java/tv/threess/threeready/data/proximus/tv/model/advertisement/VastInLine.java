package tv.threess.threeready.data.proximus.tv.model.advertisement;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import tv.threess.threeready.data.proximus.tv.model.advertisement.adapter.VastImpressionListTypeAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

/**
 * The last ad server in the ad supply chain serves an <InLine> element.
 * Within the nested elements of an <InLine> element are all the files and URIs necessary to display the ad.
 */
public class VastInLine {
    @XmlElement(name = "AdSystem")
    private String mAdSystem;

    @XmlElement(name = "AdTitle")
    private String mAdTitle;

    @JacksonXmlCData
    @XmlElement(name = "Error")
    private String mError;

    @XmlElement(name = "Impression")
    @XmlJavaTypeAdapter(VastImpressionListTypeAdapter.class)
    private List<VastIdCdata> mImpressions;

    @XmlElementWrapper(name = "Creatives")
    @XmlElement(name = "Creative")
    private List<VastCreative> mCreatives;

    public String getAdSystem() {
        return mAdSystem;
    }

    public String getAdTitle() {
        return mAdTitle;
    }

    public String getError() {
        return mError;
    }

    public List<VastIdCdata> getImpressions() {
        return mImpressions;
    }

    public List<VastCreative> getCreatives() {
        return mCreatives;
    }
}
