package tv.threess.threeready.data.proximus.tv.model;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.data.proximus.generic.adapter.ParentalRatingAdapterProximus;
import tv.threess.threeready.data.proximus.generic.model.ImageSourceProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.MetaInfoProximus;
import tv.threess.threeready.data.proximus.tv.model.broadcast.response.ProgramMetaInfoKeyProximus;

/**
 * Base class for tv and recording broadcast common code.
 *
 * Created by Noemi Dali on 16.04.2020.
 */
public abstract class BaseBroadcastProximus implements IBroadcast {

    protected MetaInfoProximus<ProgramMetaInfoKeyProximus> mProgramMetaInfo;
    private final List<IImageSource> mImageSources = new ArrayList<>();

    public BaseBroadcastProximus(MetaInfoProximus<ProgramMetaInfoKeyProximus> programMetaInfo) {
        mProgramMetaInfo = programMetaInfo;
    }

    @Override
    public String getProgramId() {
        return mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.ProgramReferenceNo);
    }

    @Override
    public String getTitle() {
        return mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.Title);
    }

    @Override
    public ParentalRating getParentalRating() {
        return ParentalRatingAdapterProximus.readRating(
                mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.ParentalRating));
    }

    @Override
    public String getEpisodeTitle() {
        return mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.EpisodeTitle);
    }

    @Override
    public Integer getSeasonNumber() {
        String seasonNumberStr = mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.SeasonNumber);
        if (TextUtils.isEmpty(seasonNumberStr)) {
            return null;
        }

        return mProgramMetaInfo.getInt(ProgramMetaInfoKeyProximus.SeasonNumber);
    }

    @Override
    public Integer getEpisodeNumber() {
        String seasonNumberStr = mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.EpisodeNumber);
        if (TextUtils.isEmpty(seasonNumberStr)) {
            return null;
        }

        return mProgramMetaInfo.getInt(ProgramMetaInfoKeyProximus.EpisodeNumber);
    }

    @Override
    public String getReleaseYear() {
        return mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.ReleaseYear);
    }

    @Override
    @Nullable
    public String getSeriesId() {
        String seriesId = mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.SeriesId);
        if (TextUtils.isEmpty(seriesId)) {
            return null;
        }

        return seriesId;
    }

    @Override
    public List<String> getActors() {
        List<String> actors = new ArrayList<>();
        actors.add(mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.Actor1));
        actors.add(mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.Actor2));
        actors.add(mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.Actor3));
        actors.add(mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.Actor4));
        actors.add(mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.Actor5));
        actors.add(mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.Actor6));
        actors.removeIf(TextUtils::isEmpty);
        return actors;
    }

    @Override
    public List<String> getDirectors() {
        List<String> directors = new ArrayList<>();
        directors.add(mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.Director, null));
        directors.removeIf(TextUtils::isEmpty);
        return directors;
    }

    @Override
    public String getDescription() {
        return mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.Description);
    }

    @Override
    public List<IImageSource> getImageSources() {
        if (mImageSources.isEmpty()) {
            mImageSources.add(new ImageSourceProximus(
                    mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.PosterFileName)));
        }
        return mImageSources;
    }

    @Override
    public List<String> getGenres() {
        List<String> genres = new ArrayList<>();
        genres.add(mProgramMetaInfo.getCategory(ProgramMetaInfoKeyProximus.Category));
        genres.removeIf(Objects::isNull);
        return genres;
    }

    public List<String> getSubGenres() {
        List<String> subGenres = new ArrayList<>();
        subGenres.add(mProgramMetaInfo.getCategory(ProgramMetaInfoKeyProximus.Subcategory));
        subGenres.removeIf(Objects::isNull);
        return subGenres;
    }

    @Override
    public boolean isBlackListed() {
        return mProgramMetaInfo.getBoolean(ProgramMetaInfoKeyProximus.Blacklist);
    }

    @Override
    public boolean isNPVREnabled() {
        return !mProgramMetaInfo.getBoolean(ProgramMetaInfoKeyProximus.DisableNPVR, false);
    }

    @Override
    public String getAssetIdentifier() {
        return mProgramMetaInfo.getString(ProgramMetaInfoKeyProximus.AssetIdentifier, null);
    }

    @Override
    public boolean hasDescriptiveAudio() {
        return mProgramMetaInfo.getBoolean(ProgramMetaInfoKeyProximus.HasDescriptiveAudio, false);
    }

    @Override
    public boolean hasDescriptiveSubtitle() {
        return mProgramMetaInfo.getBoolean(ProgramMetaInfoKeyProximus.HasDescriptiveSubtitle, false);
    }

    /**
     * Build unique id for the broadcast.
     * @param channelId The channel id on which the broadcast available.
     * @param trailId The trail id of the broadcast.
     * @return The generated broadcast id.
     */
    public static String buildBroadcastId(String channelId, String trailId) {
        return channelId + "-" + trailId;
    }

    /**
     * @param broadcastId THe unique identifier of the broadcast.
     * @return The trail id of the broadcast.
     */
    public static String buildTrailId(String broadcastId) {
        return broadcastId.substring(broadcastId.indexOf("-") + 1);
    }

    public static String buildChannelId(String broadcastId) {
        return broadcastId.substring(0, broadcastId.indexOf("-"));
    }

    protected static final MetaInfoProximus<ProgramMetaInfoKeyProximus>
            EMPTY_PROGRAM_META_INFO = new MetaInfoProximus<>();
}
