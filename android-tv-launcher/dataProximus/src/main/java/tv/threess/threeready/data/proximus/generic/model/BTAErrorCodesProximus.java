package tv.threess.threeready.data.proximus.generic.model;

/**
 * Known error codes which needs to be handled by the application.
 *
 * @author Barabas Attila
 * @since 2020.04.14
 */
public interface BTAErrorCodesProximus {
    // http error codes
    String HTTP_FORBIDDEN = "403";
    String HTTP_NOT_FOUND = "404";
    String HTTP_REQUEST_TIMEOUT = "408";
    String HTTP_INTERNAL_SERVER_ERROR = "500";

    // generic error codes
    String SYSTEM_ERROR = "0xbe100000";
    String COMMUNICATION_ERROR_IN_INTERNAL_DEPENDENT_SYSTEMS = "0xbe100001";
    String REMOTE_COMMUNICATION_ERROR = "0xbe100002";
    String UNDEFINED_ERROR = "0xbe100003";
    String UUSD_NOT_REACHABLE = "0xbe100005";
    String TM_JMS_QUEUE_PROBLEM = "0xbe100006";
    String HTTP_INVALID_RESPONSE = "0xbe100013";
    String SYSTEM_ERROR_18 = "0xbe100018";
    String SYSTEM_ERROR_19 = "0xbe100019";

    // device registration related codes
    String INVALID_ACCOUNT_NUMBER = "-4";
    String DEVICE_LIMIT_REACHED = "0xbe100054";
    String INVALID_PIN = "0xbe100042";
    String INVALID_MAC = "0xbe100724";
    String STB_NOT_ASSIGNED = "0xbe100725";

    // vod rent related codes
    String INVALID_PURCHASE_PIN = "0xbe100730";
    String PURCHASE_PIN_RECENTLY_CHANGED = "0xbe104030";
    String ALREADY_RENTED = "0xbe104005";

    // rec related codes
    String EPG_OUT_OF_DATE = "0xbe104863";
}
