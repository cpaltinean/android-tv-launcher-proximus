package tv.threess.threeready.data.proximus.account.model.boot.adapter;

import android.text.TextUtils;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import tv.threess.threeready.data.proximus.account.model.boot.TMServerInfoProximus;

/**
 * Java type adapter to read string field as {@link TMServerInfoProximus.Type}
 *
 * @author Barabas Attila
 * @since 2020.04.03
 */
public class TMServerInfoTypeAdapter extends XmlAdapter<String, TMServerInfoProximus.Type> {

    @Override
    public TMServerInfoProximus.Type unmarshal(String v) {
        if (TextUtils.isEmpty(v)) {
            return null;
        }

        for (TMServerInfoProximus.Type type : TMServerInfoProximus.Type.values()) {
            if (v.equals(type.getKey())) {
                return type;
            }
        }
        return null;
    }

    @Override
    public String marshal(TMServerInfoProximus.Type v) {
        throw new IllegalStateException("Not implemented.");
    }
}
