package tv.threess.threeready.data.proximus.account.model.account.consent;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * Adapter to convert Proximus consent value to a boolean.
 *
 * @author Zsolt Bokor_
 * @since 2020.11.5
 */
public class ConsentValueAdapter extends TypeAdapter<Boolean> {

    @Override
    public void write(JsonWriter writer, Boolean isAccepted) throws IOException {
        writer.value(isAccepted ? ConsentValueProximus.OPTIN.name() : ConsentValueProximus.OPTOUT.name());
    }

    @Override
    public Boolean read(JsonReader reader) throws IOException {
        return ConsentValueProximus.OPTIN.name().equals(reader.nextString());
    }
}
