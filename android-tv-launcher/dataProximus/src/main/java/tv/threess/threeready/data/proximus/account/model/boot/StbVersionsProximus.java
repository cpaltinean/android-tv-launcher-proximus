package tv.threess.threeready.data.proximus.account.model.boot;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Base class for proximus server related information.
 */
@XmlRootElement(name = "STBVersions")
public class StbVersionsProximus implements Serializable {

    @XmlElement(name = "STBVersion")
    List<STBVersion> mSTBVersions;

    @XmlElementWrapper(name = "HWVersions")
    @XmlElement(name = "HardwareVersion")
    List<HardwareVersionProximus> mHWVersions;

    public static class STBVersion implements Serializable {
        @XmlElement(name = "HWVersion")
        private String mHWVersion;

        @XmlElement(name = "SWVersion")
        private String mSWVersion;

        @XmlElement(name = "UIVersion")
        private String mUIVersion;
    }
}