/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.proximus.log.reporter;

import android.content.Context;
import android.text.TextUtils;

import java.util.List;
import java.util.Objects;

import retrofit2.Response;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.MassStorage;
import tv.threess.threeready.api.account.setting.MemoryCache;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.generic.receivers.HdmiConnectivityReceiver;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.ReporterInfo;
import tv.threess.threeready.api.log.model.VodItemType;
import tv.threess.threeready.data.proximus.log.model.LogDetailsProximus;
import tv.threess.threeready.data.proximus.log.model.SendEventsRequest;

/**
 * Specific playback event batch handler for Proximus
 *
 * @author Arnold Szabo
 * @since 2017.07.26
 */
public abstract class EventReporterProximus<E extends Enum<E> & Event.Kind, D extends Enum<D> & Event.Detail<E>> extends BaseReporterProximus<E, D> {
    private static final String TAG = Log.tag(EventReporterProximus.class);

    protected static final String KEY_ACTION = "action";
    protected static final String KEY_EVENT_TIME = "event_time";
    protected static final String KEY_ACTION_TYPE = "action_type";
    protected static final String KEY_TYPE = "type";
    protected static final String KEY_EVENT_TYPE_OLD = "event_type";
    protected static final String KEY_SUB_TYPE = "sub_type";
    protected static final String KEY_EVENT_CAT_OLD = "event_cat";
    protected static final String KEY_HDMI_CONN = "hdmi_connectivity";
    protected static final String KEY_HDMI_CONN_OLD = "HD_conn";
    protected static final String KEY_SCAN_START_TIME = "scan_start_time";
    protected static final String KEY_SCAN_ACTION_COUNTER = "scan_action_counter";

    // Generic content keys
    protected static final String KEY_ITEM_ID = "item_id";
    protected static final String KEY_ITEM_TITLE = "item_title";
    protected static final String KEY_ITEM_TYPE = "item_type";

    // TV content keys
    protected static final String KEY_EXTERNAL_ID = "external_id";
    protected static final String KEY_CALL_LETTER = "call_letter";
    protected static final String KEY_CHANNEL_EXTERNAL_IDENTIFIER_OLD = "channel_external_identifier";
    protected static final String KEY_CHANNEL_ID = "channel_id";
    protected static final String KEY_START_TIME = "start_time";
    protected static final String KEY_PROGRAM_CATEGORY = "program_category";
    protected static final String KEY_PROGRAM_DURATION = "program_duration";
    protected static final String KEY_PROGRAM_REFERENCE_NUMBER_OLD = "program_reference_number";
    protected static final String KEY_PROGRAM_SUB_CATEGORY = "program_sub_category";
    protected static final String KEY_PROGRAM_TITLE_OLD = "program_title";
    protected static final String KEY_SCHEDULED_TRAIL_IDENTIFIER = "scheduled_trail_identifier";

    // VOD content keys
    protected static final String KEY_VOD_EXTERNAL_IDENTIFIER_OLD = "vod_external_identifier";
    protected static final String KEY_VOD_ITEM_IDENTIFIER_OLD = "vod_item_identifier";
    protected static final String KEY_VOD_TITLE_OLD = "vod_title";
    protected static final String KEY_VOD_TYPE = "vod_type";
    protected static final String KEY_GROUP_TITLE = "group_title";
    protected static final String KEY_GENRE = "genre";
    protected static final String KEY_SUPER_GENRE = "super_genre";
    protected static final String KEY_SERIES_TITLE = "series_title";
    protected static final String KEY_EPISODE_TITLE = "episode_title";
    protected static final String KEY_SERIES_NR = "series_number";
    protected static final String KEY_EPISODE_NR = "episode_number";
    protected static final String KEY_RENTAL_COST = "rental_cost";
    protected static final String KEY_RESOLUTION = "resolution";

    // Targeted ad keys
    protected static final String KEY_STREAM_PURPOSE = "stream_purpose";
    protected static final String KEY_SPLICE_EVENT_IDENTIFIER = "splice_event_identifier";
    protected static final String KEY_TAP_REQUEST_SENT = "tap_request_sent";
    protected static final String KEY_CHANNEL_INACTIVITY_THRESHOLD = "channel_inactivity_threshold";
    protected static final String KEY_LAST_ACTIVITY = "last_activity_timestamp";
    protected static final String KEY_ADVERTISEMENT_REFERENCE = "advertisement_reference";
    protected static final String KEY_ASSET_HANDLING = "asset_handling";
    protected static final String KEY_ASSET_HANDLING_RESULT = "asset_handling_result";
    protected static final String KEY_ASSET_ATTEMPT_COUNTER = "asset_attempt_counter";
    protected static final String KEY_ASSET_DOWNLOAD_ERROR = "asset_download_error_code";
    protected static final String KEY_ASSET_HANDLING_DURATION = "asset_handling_duration";
    protected static final String KEY_ASSET_RANDOM_TIMER = "asset_random_timer";
    protected static final String KEY_ASSET_HANDLING_FAILURE_CAUSE = "asset_handling_failure_cause";

    protected static final String VALUE_ACTIVE_2_STANDBY = "ACTIVE-STANDBY";
    protected static final String VALUE_APP = "APP";
    protected static final String VALUE_APP_RESTART = "APPRestart";
    protected static final String VALUE_EVENT = "EVENT";
    protected static final String VALUE_FVOD = "FVOD";
    protected static final String VALUE_HARD_PWR_OFF = "HARD-PWR-OFF";
    protected static final String VALUE_HARD_PWR_ON = "HARD-PWR-ON";
    protected static final String VALUE_INTERACTION = "INTERACTION";
    protected static final String VALUE_POWER_CYCLE = "POWER-CYCLE";
    protected static final String VALUE_PWR_OFF = "PWR-OFF";
    protected static final String VALUE_PWR_ON = "PWR-ON";
    protected static final String VALUE_SERIES = "SERIES";
    protected static final String VALUE_SINGLE = "SINGLE";
    protected static final String VALUE_SLEEP = "SLEEP";
    protected static final String VALUE_STANDBY = "STANDBY";
    protected static final String VALUE_STANDBY_2_ACTIVE = "STANDBY-ACTIVE";
    protected static final String VALUE_STARTUP = "STARTUP";
    protected static final String VALUE_SVOD = "SVOD";
    protected static final String VALUE_TECHNICAL = "TECHNICAL";
    protected static final String VALUE_TRANSIENT = "TRANSIENT";
    protected static final String VALUE_TSTV = "TSTV";
    protected static final String VALUE_TVOD = "TVOD";

    // Targeted ad values
    protected static final String VALUE_ADVERTISEMENT = "ADVERTISEMENT";
    protected static final String VALUE_AD_TRIGGER = "AD-TRIGGER";
    protected static final String VALUE_AD_REPLACEMENT = "AD-REPLACEMENT";
    protected static final String VALUE_AD_DOWNLOAD = "AD-DOWNLOAD";

    //Ui navigation page names
    protected static final String VALUE_MAIN_HUB = "MainHub";
    protected static final String VALUE_PROGRAM_ACTION_SUB_MENU_MAIN = "ProgramActionSubMenuMain";
    protected static final String VALUE_RECORD_ACTION_SUB_MENU_MAIN = "RecordActionSubMenuMain";
    protected static final String VALUE_RECORD_ACTION_SUB_MENU_MANAGE = "RecordActionSubMenuManage";
    protected static final String VALUE_RECORD_ACTION_SUB_MENU_CONFIRM = "RecordActionSubMenuConfirm";
    protected static final String VALUE_RECORD_ACTION_SUB_MENU_DIALOG = "RecordActionSubMenuDialog";
    protected static final String VALUE_APPLICATION_FULL_CONTENT_PAGE = "ApplicationFullContentPage";
    protected static final String VALUE_EXPLORE_PAGE = "ExplorePage";
    protected static final String VALUE_STORE_SUBCATEGORY_BROWSING_PAGE = "StoreSubcategoryBrowsing";
    protected static final String VALUE_TV_PAGE = "TVPage";
    protected static final String VALUE_ZAP_LIST_PAGE = "ZapListPage";
    protected static final String VALUE_MAIN_MENU = "MainMenu";
    protected static final String VALUE_MEDIA_PLAYER_PAGE = "MediaPlayerPage";
    protected static final String VALUE_SETTINGS = "Settings";
    protected static final String VALUE_NOTIFICATIONS = "Notifications";
    protected static final String VALUE_KEYWORD_ENTRY_PAGE = "KeywordEntryPage";
    protected static final String VALUE_GENERAL_SETTINGS = "GeneralSettings";
    protected static final String VALUE_STRIPE_LINK_PAGE = "StripeLinkPage";
    protected static final String VALUE_CHOOSE_VERSION_PAGE = "ChooseVersionPage";
    protected static final String VALUE_CONTENT_WALL_FULL_CONTENT_FILTER_PAGE = "ContentWallFullContentFilterPage";
    protected static final String VALUE_CONTENT_WALL_FULL_CONTENT_PAGE = "ContentWallFullContentPage";
    protected static final String VALUE_UNLOCK_PAGE_VOD_PURCHASE = "UnlockPageVodPurchase";
    protected static final String VALUE_ACTION_MENU_VOD = "ActionMenuVod";
    protected static final String VALUE_STORE_SERIES_FULL_CONTENT_PAGE = "StoreSeriesFullContentPage";
    protected static final String VALUE_STORE_FULL_CONTENT_FILTER_PAGE = "StoreFullContentFilterPage";
    protected static final String VALUE_SEARCH = "Search";
    protected static final String VALUE_MINI_EPG_PAGE = "MiniEPGPage";

    protected final HdmiConnectivityReceiver mHdmiReceiver = Components.get(HdmiConnectivityReceiver.class);

    protected EventReporterProximus(Context context, ReporterInfo reporterInfo) {
        super(context, reporterInfo);
    }

    @Override
    public long getBatchPeriod() {
        return 0;
    }

    @Override
    public void sendEvent(Event<E, D> event) throws Exception {
        LogDetailsProximus payload = formatPayload(event);
        if (payload == null) {
            android.util.Log.d(TAG, "Could not send event, no payload: " + event);
            return;
        }

        sendEvents(getEndpoint(), getHeaders(), getTimeoutPeriod(),
                new SendEventsRequest(payload));
    }

    @Override
    public void sendEvents(List<Event<E, D>> events) throws Exception {
        List<LogDetailsProximus> payloads = formatPayloadList(events);
        if (payloads.size() == 0) {
            android.util.Log.d(TAG, "Could not send events. Payload list is empty.");
            return;
        }

        sendEvents(getEndpoint(), getHeaders(), getTimeoutPeriod(),
                new SendEventsRequest(payloads));
    }

    /**
     * Send events to data collection API.
     *
     * @param url               Url of the collector server where the data will be sent.
     * @param headers           Custom headers for events from backend which need to be included.
     * @param sendEventsRequest Retrofit request class for sending events.
     */
    protected void sendEvents(String url, String headers, long timeout,
                            SendEventsRequest sendEventsRequest) throws Exception {
        if (TextUtils.isEmpty(url)) {
            throw new IllegalArgumentException("Could not send events. Url is null or empty.");
        }

        Response<String> response = mDataCollectionRetrofit.sendEvents(url, getHeadersMap(headers),
                timeout, sendEventsRequest).execute();

        // HTTP Error
        if (!response.isSuccessful() || !Objects.equals(response.body(), OK)) {
            throw new BackendException(response.code(), response.message());
        }
    }


    /**
     * @return Allowed values for the vod_type parameter.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Content+specific+parameters
     */
    protected String getVodType(VodItemType vodItemType) {
        switch (vodItemType) {
            case TVOD:
                return VALUE_TVOD;
            case FVOD:
                return VALUE_FVOD;
            case SVOD:
                return VALUE_SVOD;
        }
        return NONE;
    }

    /**
     * @return Unique identifier of the event.
     */
    protected String getIdentifier(long time) {
        String macAddress = mMwProxy.getEthernetMacAddress();
        return macAddress + "_" + time;
    }

    @Override
    protected void addAgentSpecificParts(LogDetailsProximus payload) {
        payload.add(KEY_CATEGORY, VALUE_EVENT);
        super.addAgentSpecificParts(payload);
    }

    /**
     * Add navigational reporting session related parameters.
     * https://confluenceprd-proximuscorp.msappproxy.net/display/PRXMS/Navigational+reporting+and+viewing+events
     */
    protected void addNavigationSessionPart(LogDetailsProximus payload) {
        payload.add(KEY_SCAN_START_TIME,
                MemoryCache.NavigationSessionStart.get(System.currentTimeMillis()));
        payload.add(KEY_SCAN_ACTION_COUNTER,
                MemoryCache.NavigationSessionCounter.get(0));
    }

    protected String getPageName(UILog.Page page) {
        if (page == null) {
            return NONE;
        }

        switch (page) {
            case Home:
                return VALUE_MAIN_HUB;
            case ProgramDetail:
                return VALUE_PROGRAM_ACTION_SUB_MENU_MAIN;
            case ActionMenuVod:
                return VALUE_ACTION_MENU_VOD;
            case StoreSeriesFullContentPage:
                return VALUE_STORE_SERIES_FULL_CONTENT_PAGE;
            case StoreFullContentFilterPage:
                return VALUE_STORE_FULL_CONTENT_FILTER_PAGE;
            case RecordActionSubMenuMain:
                return VALUE_RECORD_ACTION_SUB_MENU_MAIN;
            case RecordActionSubMenuManage:
                return VALUE_RECORD_ACTION_SUB_MENU_MANAGE;
            case RecordActionSubMenuConfirm:
                return VALUE_RECORD_ACTION_SUB_MENU_CONFIRM;
            case RecordActionSubMenuDialog:
                return VALUE_RECORD_ACTION_SUB_MENU_DIALOG;
            case ApplicationFullContentPage:
                return VALUE_APPLICATION_FULL_CONTENT_PAGE;
            case ExplorePage:
                return VALUE_EXPLORE_PAGE;
            case StoreSubcategoryBrowsing:
                return VALUE_STORE_SUBCATEGORY_BROWSING_PAGE;
            case TvPlayer:
                return VALUE_TV_PAGE;
            case MiniEPGPage:
                return VALUE_MINI_EPG_PAGE;
            case ZapListPage:
                return VALUE_ZAP_LIST_PAGE;
            case MainMenu:
                return VALUE_MAIN_MENU;
            case MediaPlayerPage:
                return VALUE_MEDIA_PLAYER_PAGE;
            case Search:
                return VALUE_SEARCH;
            case Settings:
                return VALUE_SETTINGS;
            case Notifications:
                return VALUE_NOTIFICATIONS;
            case KeywordEntryPage:
                return VALUE_KEYWORD_ENTRY_PAGE;
            case GeneralSettings:
                return VALUE_GENERAL_SETTINGS;
            case StripeLinkPage:
                return VALUE_STRIPE_LINK_PAGE;
            case ChooseVersionPage:
                return VALUE_CHOOSE_VERSION_PAGE;
            case ContentWallFullContentFilterPage:
                return VALUE_CONTENT_WALL_FULL_CONTENT_FILTER_PAGE;
            case ContentWallFullContentPage:
                return VALUE_CONTENT_WALL_FULL_CONTENT_PAGE;
            case UnlockPageVodPurchase:
                return VALUE_UNLOCK_PAGE_VOD_PURCHASE;
            case Unknown:
                return NONE;
            case StandBy:
                return VALUE_STANDBY;
            case Restart: {
                if (!isPowerCycle()) {
                    return VALUE_APP_RESTART;
                }

                return VALUE_POWER_CYCLE;
            }
            default:
                return page.name();
        }
    }

    protected boolean isPowerCycle() {
        return !mMwProxy.getBootId().equals(MassStorage.lastKnownHeartBeatBootId.get(null));
    }
}
