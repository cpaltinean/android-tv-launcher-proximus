package tv.threess.threeready.data.proximus.home.model.module;

import tv.threess.threeready.data.proximus.home.model.swimalane.SwimLaneProximus;

/**
 * Module config for a swimlane which contains a single VOD category.
 *
 * @author Barabas Attila
 * @since 5/24/21
 */
public class VodCategorySwimlaneModuleProximus extends SwimlaneModuleConfigProximus {

    public VodCategorySwimlaneModuleProximus(SwimLaneProximus swimlane) {
        super(swimlane);
    }

    public String getCategoryId() {
        return getSwimlane().getVodCategoryId();
    }
}
