package tv.threess.threeready.data.proximus.playback.model.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import tv.threess.threeready.api.vod.model.IVodVariant;

/**
 * BTA request model to open a streaming session for a VOD.
 *
 * @author Barabas Attila
 * @since 2020.05.07
 */
@XmlRootElement(name = "BTA")
public class VodStreamingSessionRequestProximus implements Serializable {

    @XmlElement(name = "URLDoc")
    private final VodStreamingSessionRequestProximus.URLDoc mURLDoc;

    public VodStreamingSessionRequestProximus(IVodVariant variant, boolean trailer) {
        mURLDoc = new URLDoc();
        mURLDoc.mVodId = variant.getId();
        mURLDoc.mTrailer = trailer;
    }

    @XmlRootElement(name = "URLDoc")
    private static class URLDoc implements Serializable {

        @XmlElement(name = "VoDExtID")
        private String mVodId;

        @XmlElement(name = "Trailer")
        private boolean mTrailer;

        @XmlElement(name = "AcceptedMIMETypes")
        private final String mimeTypes = "application/dash+xml";
    }
}
