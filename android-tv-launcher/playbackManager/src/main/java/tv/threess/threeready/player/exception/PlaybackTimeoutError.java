/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.exception;

import tv.threess.threeready.api.playback.player.PlaybackDomain;

/**
 * Exception thrown when the execution of a PlaybackCommand takes more than the acceptable time and it need to be cancelled.
 *
 * @author Dan Titiu
 * @since 27/09/16
 */
public class PlaybackTimeoutError extends PlaybackError {

    public PlaybackTimeoutError(PlaybackDomain domain) {
        super(getType(domain));
    }

    private static Type getType(PlaybackDomain domain) {
        switch (domain) {
            case LiveTvIpTv:
            case RadioIpTv:
                return Type.VIDEO_UNAVAILABLE_TIMEOUT_IPTV;

            case App:
                return Type.APP_OPEN_TIMEOUT;

            default:
                return Type.VIDEO_UNAVAILABLE_TIMEOUT_OTT;
        }
    }
}
