package tv.threess.threeready.player.control.base;

import android.content.Context;
import android.media.PlaybackParams;
import android.media.tv.TvContentRating;
import android.media.tv.TvInputManager;
import android.media.tv.TvTrackInfo;
import android.media.tv.TvView;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.config.model.PlaybackSettings;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.helper.NumberUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.api.playback.model.TrackInfo;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.TrackManager;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.exception.PlaybackTimeoutError;
import tv.threess.threeready.player.model.MediaTrackInfo;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.result.ExactFailure;
import tv.threess.threeready.player.result.ExactSuccess;
import tv.threess.threeready.player.result.VagueFailure;
import tv.threess.threeready.player.result.VagueSuccess;

/**
 * Base controls for TIF playback.
 *
 * @author Andor Lukacs
 * @since 7/3/18
 */
public abstract class BaseTvViewControl extends PlaybackControl<TvView> {

    protected static final String TRACK_EXTRA_SUBTITLE_TYPE = "subtitles_type";
    protected static final String TRACK_EXTRA_SUBTITLE_TYPE_HARD_OF_HEARING = "hard_of_hearing";

    protected static final String TRACK_EXTRA_AUDIO_TYPE = "audio_type";
    protected static final String TRACK_EXTRA_AUDIO_TYPE_NORMAL = "normal";

    private static final int BUFFERING_MESSAGE = 1;
    private static final int WEAK_SIGNAL_MESSAGE = 2;

    /**
     * Default playback speeds for states.
     */
    private static final double PLAYBACK_SPEED_PLAYING = 1;
    private static final double PLAYBACK_SPEED_PAUSED = 0;

    private static final long TIMEOUT_CANCEL = TimeUnit.SECONDS.toMillis(5);
    // Language map
    private static Map<String, String> mLanguageMap;

    // Selected track ids
    private String mSelectedSubtitleTrackID = null;
    private String mSelectedAudioTrackID = null;
    private String mSelectedVideoTrackID = null;

    // Available tracks
    private volatile MediaTrackInfo[] mSubtitlesTracks = PlaybackDetails.TRACKS_NONE;
    private volatile MediaTrackInfo[] mAudioTracks = PlaybackDetails.TRACKS_NONE;
    private volatile MediaTrackInfo[] mVideoTracks = PlaybackDetails.TRACKS_NONE;

    private final PlaybackSettings mPlaybackSettings = Components.get(PlaybackSettings.class);
    private final TrackManager mTrackManager = Components.get(TrackManager.class);
    private final MwRepository mMwRepository = Components.get(MwRepository.class);

    private boolean mSelectTracksOnlyOnce;

    protected final Handler mMainHandler;

    public BaseTvViewControl(Context context, PlaybackDomain domain, TvView tvView) {
        super(context, domain, tvView);
        mLanguageMap = Components.get(AppConfig.class).getAudioSubtitleLanguageMap();
        mMainHandler = new Handler(Looper.getMainLooper(), mHandlerCallback);
    }

    protected PlaybackError.Type getExceptionType(int reason) {
        return PlaybackError.Type.UNKNOWN;
    }

    /**
     * Start the playback on the given input service.
     *
     * @param cid              The id of the command which trigger the start.
     * @param contentStartTime The start time of the content which needs to start.
     * @param contentEndTime   The end time of the content which needs to start.
     * @param inputId          The id of the input service on which the playback should start.
     * @param playbackUri      The uri for the input service to start the playback for.
     * @param playbackParams   Extra playback parameters for the input service.
     */
    public void start(long cid, long contentStartTime, long contentEndTime, String inputId, Uri playbackUri, Bundle playbackParams) throws Exception {
        super.start(cid, contentStartTime, contentEndTime);

        mPlaybackView.setCallback(mTvInputCallback);
        mPlaybackView.setTimeShiftPositionCallback(mTimeShiftPositionCallback);

        final long now = System.currentTimeMillis();
        blockingPostOnUiThread(() -> {
            if (System.currentTimeMillis() - now > TIMEOUT_CANCEL) {
                // the system is slow, remove all playback related callbacks: starts, retries, etc
                removeCallbacks();
            }
            Log.d(TAG, "TvView Tune " + inputId + " uri: " + playbackUri);
            mPlaybackView.tune(inputId, playbackUri, playbackParams);

            // Mute the playback by default.
            mPlaybackView.setStreamVolume(0f);
        });
    }

    /**
     * Stop the currently running playback.
     *
     * @param cid The id of the command which triggered the stop.
     */
    @Override
    public void stop(final long cid) throws Exception {
        reset();
        blockingPostOnUiThread(() -> {
            Log.d(TAG, "TvView.reset");
            mPlaybackView.reset();
            offerResult(new ExactSuccess(cid));
        });
    }

    /**
     * Pause the currently running playback.
     *
     * @param cid The of the command which triggered the pause.
     */
    @Override
    public void pause(final long cid) throws Exception {
        // Reset playback speed because it is used on UI
        playbackSpeed = PLAYBACK_SPEED_PAUSED;
        blockingPostOnUiThread(() -> {
            Log.d(TAG, "TvView.timeShiftPause");
            mPlaybackView.timeShiftPause();
            offerResult(new ExactSuccess(cid));
            mMainHandler.removeMessages(BUFFERING_MESSAGE);
        });
    }

    /**
     * Jump to the given position in the current playback.
     *
     * @param cid      The id of the command which triggered the jump.
     * @param position THe absolute position in the content to jump.
     */
    @Override
    public void jump(long cid, long position) throws Exception {
        if (isUnskippablePosition(getCurrentPosition())) {
            offerResult(new ExactFailure(cid, PlaybackError.Type.UNSKIPPABLE_REGION));
            return;
        }

        blockingPostOnUiThread(() -> {
            long jumpPosition = NumberUtils.clamp(position, getSeekAbleStart(), getSeekAbleEnd());
            mPlaybackView.timeShiftSeekTo(convertContentToPlaybackPosition(jumpPosition));
            applyPosition(jumpPosition, true);
            Log.d(TAG, "TvView.timeShiftSeekTo: " + Log.timestamp(getCurrentPosition()));
            resetBufferingTimeout();
            offerResult(new ExactSuccess(cid));
        });
    }

    /**
     * Set the playback speed for the current playback.
     *
     * @param cid           The id of the command which triggered the playback speed change
     * @param playbackSpeed The new speed for the playback. Where 1.0 the normal speed.
     */
    @Override
    public void setSpeed(long cid, double playbackSpeed) throws Exception {
        if (isUnskippablePosition(getCurrentPosition())) {
            offerResult(new ExactFailure(cid, PlaybackError.Type.UNSKIPPABLE_REGION));
            return;
        }

        this.playbackSpeed = playbackSpeed;
        blockingPostOnUiThread(() -> {
            Log.d(TAG, "TvView.playbackSpeed: " + playbackSpeed);
            mPlaybackView.timeShiftSetPlaybackParams(new PlaybackParams().setSpeed((float) playbackSpeed));
            offerResult(new ExactSuccess(cid));
        });
    }

    /**
     * Resume the previously paused playback.
     *
     * @param cid The id of the command which triggered the resume.
     */
    @Override
    public void resume(final long cid) throws Exception {
        // Reset the playback speed because it is used on UI
        playbackSpeed = PLAYBACK_SPEED_PLAYING;
        blockingPostOnUiThread(() -> {
            Log.d(TAG, "TvView.timeShiftResume");
            mPlaybackView.timeShiftResume();
            offerResult(new ExactSuccess(cid));
        });
    }

    /**
     * Mute the volume of the currently running playback.
     *
     * @param cid The id of the command which triggered the mute.
     */
    @Override
    public void mute(long cid) throws Exception {
        blockingPostOnUiThread(() -> {
            Log.d(TAG, "TvView.setStreamVolume to 0.0");
            mPlaybackView.setStreamVolume(0f);
            offerResult(new ExactSuccess(cid));
        });
    }

    /**
     * Unmute the previously muted playback volume.
     *
     * @param cid The id of the command which triggered the unmute
     */
    @Override
    public void unMute(long cid) throws Exception {
        blockingPostOnUiThread(() -> {
            Log.d(TAG, "TvView.setStreamVolume to 1.0");
            mPlaybackView.setStreamVolume(1f);
            offerResult(new ExactSuccess(cid));
        });
    }

    /**
     * Select a new subtitle track in the playback.
     *
     * @param cid     The id of the command which triggered the track selection.
     * @param trackId The id of the subtitle which needs to be selected.
     */
    @Override
    public void selectSubtitleTrack(final long cid, final String trackId) throws Exception {
        blockingPostOnUiThread(() -> {
            Log.d(TAG, "Select subtitle track id: " + trackId);
            mPlaybackView.setCaptionEnabled(!TextUtils.isEmpty(trackId));
            mPlaybackView.selectTrack(TvTrackInfo.TYPE_SUBTITLE, trackId);
            offerResult(new ExactSuccess(cid));
        });
    }

    /**
     * Select a new audio track in the playback
     *
     * @param cid     The command which triggered the track selection.
     * @param trackId The id of the audio track which needs to be selected.
     */
    @Override
    public void selectAudioTrack(final long cid, final String trackId) throws Exception {
        blockingPostOnUiThread(() -> {
            Log.d(TAG, "TvView.selectTrack");
            mPlaybackView.selectTrack(TvTrackInfo.TYPE_AUDIO, trackId);
            offerResult(new ExactSuccess(cid));
        });
    }

    /**
     * Fill the playback details with information about the currently available
     * and selected video, subtitle and audio track.
     *
     * @param details The playback details which needs to be decorated.
     */
    @Override
    protected void fillMediaTrackInfo(PlaybackDetailsBuilder details) {
        // create new list to prevent list size modifications while creating details
        details.setAudioTracks(mAudioTracks);
        details.setSubtitleTracks(mSubtitlesTracks);
        details.setVideoTracks(mVideoTracks);

        // set selected audio, subtitle and video track position
        details.setSelectedAudioIndex(findTrackIndex(mAudioTracks, mSelectedAudioTrackID));
        details.setSelectedSubtitleIndex(findTrackIndex(mSubtitlesTracks, mSelectedSubtitleTrackID));
        details.setSelectedVideoIndex(findTrackIndex(mVideoTracks, mSelectedVideoTrackID));
    }

    /**
     * Called when the tv view failed to connect to the give tv input service.
     *
     * @param inputId The id of the input service for which the connection failed.
     */
    protected void onConnectionFailed(String inputId) {
        Log.d(TAG, "onConnectionFailed(inputId: " + inputId + ")");
        offerResult(new VagueFailure(getDomain(), PlaybackAction.Start, PlaybackError.Type.GENERIC));
        offerEvent(PlaybackEvent.PlaybackFailed);
    }

    /**
     * Called when the tv view looses to connection of the tv input service.
     *
     * @param inputId The id of the input service which is no longer connected.
     */
    protected void onDisconnected(String inputId) {
        removeCallbacks();
        Log.d(TAG, "onDisconnected(inputId: " + inputId + ")");
        offerEvent(PlaybackEvent.PlaybackDisconnected, new PlaybackError(PlaybackError.Type.DISCONNECTED));
        applyState(PlaybackState.Stopped);
    }

    /**
     * Resets the internal state that was built during playback. May be used by PlaybackCommands after finishing a
     * playback session on the control (e.g. on stop).
     */
    @Override
    public void reset() {
        super.reset();
        mSelectTracksOnlyOnce = true;
        mSelectedVideoTrackID = mSelectedAudioTrackID = mSelectedSubtitleTrackID = null;
        removeCallbacks();
    }

    /**
     * Called when the playback control is no longer needed,
     * all the previously occupied resource should be freed here.
     */
    @Override
    public void onDestroy() {
        mPlaybackView.setTimeShiftPositionCallback(null);
        mPlaybackView.setCallback(null);
        mPlaybackView.reset();
        removeCallbacks();
        super.onDestroy();
    }

    /**
     * Called when audio, subtitle or video track changes in the current playback.
     *
     * @param inputId The id of the currently active tv input service.
     * @param tracks  The list with all the currently available track.
     */
    protected void onTracksChanged(String inputId, List<TvTrackInfo> tracks) {
        List<MediaTrackInfo> audioList = new ArrayList<>();
        List<MediaTrackInfo> subList = new ArrayList<>();
        List<MediaTrackInfo> videoList = new ArrayList<>();

        for (TvTrackInfo track : tracks) {
            MediaTrackInfo info = decorateMediaTrackInfo(track);
            switch (track.getType()) {
                case TvTrackInfo.TYPE_AUDIO:
                    audioList.add(info);
                    break;
                case TvTrackInfo.TYPE_SUBTITLE:
                    subList.add(info);
                    break;
                case TvTrackInfo.TYPE_VIDEO:
                    videoList.add(info);
                    break;
            }
        }

        mAudioTracks = audioList.toArray(new MediaTrackInfo[0]);
        mSubtitlesTracks = subList.toArray(new MediaTrackInfo[0]);
        mVideoTracks = videoList.toArray(new MediaTrackInfo[0]);

        if (mSelectTracksOnlyOnce) {
            String id = getPlaybackId();
            String subtitleTrackId = findTrackId(mSubtitlesTracks, mTrackManager.getSelectedSubtitleTrack(id));
            if (subtitleTrackId == null) {
                TrackInfo preferred = Settings.subtitlesLanguage.getObject(TrackInfo.class, TrackInfo::fromLanguage);
                subtitleTrackId = findTrackId(mSubtitlesTracks, preferred);
            }
            mPlaybackView.setCaptionEnabled(!TextUtils.isEmpty(subtitleTrackId));
            mPlaybackView.selectTrack(TvTrackInfo.TYPE_SUBTITLE, subtitleTrackId);

            String audioTrackId = findTrackId(mAudioTracks, mTrackManager.getSelectedAudioTrack(id));
            if (audioTrackId == null) {
                boolean descriptive = mMwRepository.isTalkBackOn();
                TrackInfo preferred = Settings.audioLanguage.getObject(TrackInfo.class, TrackInfo::fromLanguage);
                if (preferred == null) {
                    preferred = new TrackInfo(LocaleUtils.getApplicationLanguage(), descriptive);
                }
                if (descriptive && !preferred.isDescriptive()) {
                    preferred = new TrackInfo(preferred.getLanguage(), true);
                }
                audioTrackId = findTrackId(mAudioTracks, preferred);
            }

            if (audioTrackId == null && mAudioTracks.length > 0 && mAudioTracks[0] != null) {
                // If there is no track to specifically select, select the first one
                // This is a workaround for PROX-9094 - no sound issue
                audioTrackId = mAudioTracks[0].getId();
            }
            mPlaybackView.selectTrack(TvTrackInfo.TYPE_AUDIO, audioTrackId);
            Log.d(TAG, "select automatic tracks: subtitle track: " + subtitleTrackId + " audio track: " + audioTrackId);

            mSelectTracksOnlyOnce = false;
        }

        offerEvent(PlaybackEvent.TracksChanged);

        Log.d(TAG, "onTracksChanged(inputId: " + inputId + ") -> audio : "
                + mAudioTracks.length + ", subtitle : " + mSubtitlesTracks.length + ", video : " + mVideoTracks.length);
    }

    /**
     * Called when the selected video, audio or subtitle track changes in input service.
     *
     * @param inputId The identifier of the input service where the track changed.
     * @param type    The type of the track. E.g video, audio, video.
     * @param trackId The identifier of the newly selected track.
     */
    protected void onTrackSelected(String inputId, int type, String trackId) {
        Log.d(TAG, "onTrackSelected(inputId: " + inputId + ", type: " + type + ", trackId: " + trackId + ")");
        switch (type) {
            case TvTrackInfo.TYPE_AUDIO:
                mSelectedAudioTrackID = trackId;
                mTrackManager.setAudioTrack(getPlaybackId(), findLanguage(mAudioTracks, trackId));
                break;
            case TvTrackInfo.TYPE_SUBTITLE:
                mSelectedSubtitleTrackID = trackId;
                mTrackManager.setSubtitleTrack(getPlaybackId(), findLanguage(mSubtitlesTracks, trackId));
                break;
            case TvTrackInfo.TYPE_VIDEO:
                mSelectedVideoTrackID = trackId;
                mTrackManager.setVideoTrack(getPlaybackId(), findLanguage(mVideoTracks, trackId));
                break;
        }

        printTrackInfo();
    }

    private void printTrackInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("Selected track changed.");
        sb.append("\nAudio languages :  \n");
        buildTrackInfoString(sb, mAudioTracks, mSelectedAudioTrackID);
        sb.append("Subtitle languages :  \n");
        buildTrackInfoString(sb, mSubtitlesTracks, mSelectedSubtitleTrackID);
        Log.d(TAG, sb.toString());
    }

    private void buildTrackInfoString(StringBuilder sb, MediaTrackInfo[] tracks, @Nullable String selectedId) {
        for (MediaTrackInfo track : tracks) {
            sb.append("    Language : ").append(track.getLanguage());
            if (Objects.equals(track.getId(), selectedId)) {
                sb.append(" Selected");
            }
            sb.append("\n");
        }
    }

    protected MediaTrackInfo decorateMediaTrackInfo(TvTrackInfo tvTrackInfo) {
        boolean descriptive = false;

        Bundle extras = tvTrackInfo.getExtra();
        if (extras != null) {
            if (TRACK_EXTRA_SUBTITLE_TYPE_HARD_OF_HEARING.equals(extras.getString(TRACK_EXTRA_SUBTITLE_TYPE, ""))) {
                descriptive = true;
            }

            String audio = extras.getString(TRACK_EXTRA_AUDIO_TYPE, "");
            if (!TextUtils.isEmpty(audio) && !TRACK_EXTRA_AUDIO_TYPE_NORMAL.equals(audio)) {
                descriptive = true;
            }
        }

        return new MediaTrackInfo(tvTrackInfo.getId(), tvTrackInfo.getLanguage(),
                tvTrackInfo.getType(), descriptive, MediaTrackInfo.SurroundType.None);
    }

    protected void onTimeShiftStartPositionChanged(String inputId, long contentTimeMs) {
        Log.v(TAG, "onTimeShiftStartPositionChanged: " + Log.timestamp(contentTimeMs));
    }

    protected void onTimeShiftCurrentPositionChanged(String inputId, long contentTimeMs) {
        Log.v(TAG, "onTimeShiftCurrentPositionChanged: " + Log.timestamp(contentTimeMs));
        applyPosition(contentTimeMs, false);
    }

    /**
     * Called when the video is started in the input service.
     *
     * @param inputId The identifier of the input service where video started.
     */
    protected void onVideoAvailable(String inputId) {
        Log.d(TAG, "onVideoAvailable(inputId: " + inputId + ")");
        removeCallbacks();
        offerResult(new VagueSuccess(getDomain(), PlaybackAction.Start));
        offerEvent(PlaybackEvent.PlaybackStarted);
        if (mCurrentPosition != Long.MIN_VALUE) {
            applyPosition(mCurrentPosition, true);
        }
    }

    /**
     * Called when the video playback is no longer available.
     *
     * @param inputId The identifier of the input service where the video no longer available,
     * @param reason  The code to identify the reason why the video playback stopped. E.g end reached, playback error etc.
     */
    protected void onVideoUnavailable(String inputId, int reason) {
        Log.d(TAG, "onVideoUnavailable(inputId: " + inputId + ", reason: " + reason + ")");

        switch (reason) {
            case TvInputManager.VIDEO_UNAVAILABLE_REASON_TUNING:
                break;
            case TvInputManager.VIDEO_UNAVAILABLE_REASON_BUFFERING:
                if (state != PlaybackState.Started) {
                    break;
                }
                offerEvent(PlaybackEvent.PlaybackBuffering);
                scheduleBufferingTimeout();
                break;
            case TvInputManager.VIDEO_UNAVAILABLE_REASON_WEAK_SIGNAL:
                if (state != PlaybackState.Started) {
                    break;
                }
                scheduleWeakSignalTimeout();
                break;
            default:  // stop playback and throw a PlaybackException
                PlaybackError error = new PlaybackError(getExceptionType(reason));
                Log.e(TAG, "Playback exception: " + error);

                removeCallbacks();
                offerResult(new VagueFailure(getDomain(), PlaybackAction.Start, error));
                offerEvent(PlaybackEvent.PlaybackUnavailable, error);
                applyState(PlaybackState.Stopped);
                mPlaybackView.reset();
                break;
        }
    }

    private MediaTrackInfo findLanguage(MediaTrackInfo[] tracks, String trackId) {
        for (MediaTrackInfo it : tracks) {
            if (it != null && it.getId() != null && it.getId().equals(trackId)) {
                return it;
            }
        }

        return null;
    }

    /**
     * Find the Id of a specified track language.
     * If that language does not exist, then fall back to the preferred track language.
     */
    private String findTrackId(MediaTrackInfo[] tracks, TrackInfo track) {
        if (track == null) {
            return null;
        }
        String lang = mLanguageMap.get(track.getLanguage());
        if (lang == null) {
            return null;
        }


        for (MediaTrackInfo it : tracks) {
            if (it == null) {
                continue;
            }
            if (lang.equals(mLanguageMap.get(it.getLanguage()))) {
                if (track.isDescriptive() == it.isDescriptive()) {
                    return it.getId();
                }
            }
        }

        for (MediaTrackInfo it : tracks) {
            if (it == null) {
                continue;
            }
            if (lang.equals(mLanguageMap.get(it.getLanguage()))) {
                return it.getId();
            }
        }
        return null;
    }

    private int findTrackIndex(MediaTrackInfo[] tracks, String trackId) {
        for (int index = 0; index < tracks.length; index++) {
            if (tracks[index] != null) {
                if (tracks[index].getId() != null && tracks[index].getId().equals(trackId)) {
                    return index;
                }
            }
        }

        return -1;
    }

    /**
     * @return Flag which marks if the control is capable of executing pause action.
     */
    @Override
    protected boolean canPause() {
        return true;
    }

    /**
     * Reset an existing buffering timeout.
     */
    private void resetBufferingTimeout() {
       if (mMainHandler.hasMessages(BUFFERING_MESSAGE)) {
           scheduleBufferingTimeout();
       }
    }

    /**
     * Schedule a buffering timeout and throw an error if the playback doesn't start during that time.
     */
    private void scheduleBufferingTimeout() {
        if (state == PlaybackState.TrickPlay
                || state == PlaybackState.TrickPlayPaused
                || state == PlaybackState.Paused) {
            // No buffering in paused/frozen state.
            return;
        }

        long timeout = mPlaybackSettings.getPlaybackBufferingEventDelay(TimeUnit.MILLISECONDS);
        Log.d(TAG, "Schedule buffering error. Timeout : "
                + TimeUnit.MILLISECONDS.toSeconds(timeout) + " seconds");
        mMainHandler.removeMessages(BUFFERING_MESSAGE);
        mMainHandler.sendEmptyMessageDelayed(BUFFERING_MESSAGE, timeout);
    }

    /**
     * Schedule a weak signal timeout and throw an error if the playback doesn't start during that time.
     */
    private void scheduleWeakSignalTimeout() {
        mMainHandler.removeMessages(WEAK_SIGNAL_MESSAGE);
        long timeout = mPlaybackSettings.getWeakSignalEventDelay(TimeUnit.MILLISECONDS);
        Log.d(TAG, "Schedule weak signal error. Timeout : "
                + TimeUnit.MILLISECONDS.toSeconds(timeout) + " seconds");
        mMainHandler.sendEmptyMessageDelayed(WEAK_SIGNAL_MESSAGE, timeout);
    }

    /**
     * Stop the three runnables so they don't run delayed if not needed anymore.
     */
    protected void removeCallbacks() {
        Log.d(TAG, "removeCallbacks() called");
        mMainHandler.removeMessages(BUFFERING_MESSAGE);
        mMainHandler.removeMessages(WEAK_SIGNAL_MESSAGE);
    }

    // TV CALLBACK
    private final TvView.TvInputCallback mTvInputCallback = new TvView.TvInputCallback() {

        /**
         * An error occurred and the end user has to be informed that we were not able to tune;
         * the TV App waits for further zapping request from the end user.
         */
        @Override
        public void onConnectionFailed(String inputId) {
            super.onConnectionFailed(inputId);
            BaseTvViewControl.this.onConnectionFailed(inputId);
        }

        /**
         * This is an unexpected situation;
         * the end user has to be informed;
         * the TV App waits for further zapping request from the end user.
         */
        @Override
        public void onDisconnected(String inputId) {
            super.onDisconnected(inputId);
            BaseTvViewControl.this.onDisconnected(inputId);
        }

        /**
         * This situation should not occur, in the context of the current project
         */
        @Override
        public void onChannelRetuned(String inputId, Uri channelUri) {
            super.onChannelRetuned(inputId, channelUri);
            Log.d(TAG, "onChannelRetuned(inputId: " + inputId + ", channelUri: " + channelUri + ")");
        }

        /**
         * The list of tracks has changed,
         * the TV App can the update the user interface (i.e. the banner)
         */
        @Override
        public void onTracksChanged(String inputId, List<TvTrackInfo> tracks) {
            super.onTracksChanged(inputId, tracks);
            BaseTvViewControl.this.onTracksChanged(inputId, tracks);
        }

        /**
         * A selected track has changed,
         * the TV App can the update the user interface (i.e. the banner)
         */
        @Override
        public void onTrackSelected(String inputId, int type, String trackId) {
            super.onTrackSelected(inputId, type, trackId);
            BaseTvViewControl.this.onTrackSelected(inputId, type, trackId);
        }

        /**
         * The size of the video has changed;
         * nothing is expected from the TV App
         */
        @Override
        public void onVideoSizeChanged(String inputId, int width, int height) {
            super.onVideoSizeChanged(inputId, width, height);
            Log.d(TAG, "onVideoSizeChanged(inputId: " + inputId + ", width: " + width + ", height: " + height + ")");
        }

        /**
         * The video is available after a tuning request for example,
         * the TV App can then display the banner for few seconds.
         */
        @Override
        public void onVideoAvailable(String inputId) {
            super.onVideoAvailable(inputId);
            BaseTvViewControl.this.onVideoAvailable(inputId);
        }

        /**
         * The video is not or no more available. If the reason is set to:
         * -VIDEO_UNAVAILABLE_REASON_UNKNOWN, then the end user has to be informed that an error occurred.
         * -VIDEO_UNAVAILABLE_REASON_TUNING, the video is no more available because a tuning has been request
         * -VIDEO_UNAVAILABLE_REASON_WEAK_SIGNAL the end user has to be informed the the signal is not good
         * -VIDEO_UNAVAILABLE_REASON_BUFFERING the end user has to be informed that there is latency in the video acquisition, a spinning wheel can be displayed
         * -VIDEO_UNAVAILABLE_REASON_AUDIO_ONLY, the current channel has only audio tracks then a picture can be displayed in order to replace the video
         */
        @Override
        public void onVideoUnavailable(String inputId, int reason) {
            super.onVideoUnavailable(inputId, reason);
            BaseTvViewControl.this.onVideoUnavailable(inputId, reason);
        }

        /**
         * The current content is not anymore blocked;
         * the TV App has to remove graphics if any in order to let the user watches the video.
         */
        @Override
        public void onContentAllowed(String inputId) {
            super.onContentAllowed(inputId);
            Log.d(TAG, "onContentAllowed(inputId: " + inputId + ")");
        }

        /**
         * The current content is blocked or the channel is blocked;
         * The TV App has to show a PIN code user interface.
         */
        @Override
        public void onContentBlocked(String inputId, TvContentRating rating) {
            super.onContentBlocked(inputId, rating);
            Log.d(TAG, "onContentBlocked(inputId: " + inputId + ", rating: " + rating + ")");
        }

        /**
         * The time shift status change,
         * the TV App can then update the user interface (i.e. the banner)
         */
        @Override
        public void onTimeShiftStatusChanged(String inputId, int status) {
            super.onTimeShiftStatusChanged(inputId, status);
            Log.d(TAG, "onTimeShiftStatusChanged(inputId: " + inputId + ", status: " + status + ")");
        }
    };

    // TIMESHIFT CALLBACK
    private final TvView.TimeShiftPositionCallback mTimeShiftPositionCallback = new TvView.TimeShiftPositionCallback() {
        @Override
        public void onTimeShiftStartPositionChanged(String inputId, long playbackPosition) {
            super.onTimeShiftStartPositionChanged(inputId, playbackPosition);
            BaseTvViewControl.this.onTimeShiftStartPositionChanged(
                    inputId, convertPlaybackToContentPosition(playbackPosition));
        }

        @Override
        public void onTimeShiftCurrentPositionChanged(String inputId, long playbackPosition) {
            super.onTimeShiftCurrentPositionChanged(inputId, playbackPosition);
            BaseTvViewControl.this.onTimeShiftCurrentPositionChanged(
                    inputId, convertPlaybackToContentPosition(playbackPosition));
        }
    };

    public abstract String getPlaybackId();

    private final Handler.Callback mHandlerCallback = msg -> {
        switch (msg.what) {
            case WEAK_SIGNAL_MESSAGE:
            case BUFFERING_MESSAGE:
                try {
                    mPlaybackView.reset();
                } catch (Exception e) {
                    Log.e(TAG, "Failed to stop playback", e);
                }

                BaseTvViewControl.this.reset();
                BaseTvViewControl.this.applyState(PlaybackState.Stopped);

                PlaybackError error = null;
                if (msg.what == WEAK_SIGNAL_MESSAGE) {
                    error = new PlaybackError(PlaybackError.Type.WEAK_SIGNAL);
                } else {
                    StartCommand startCommand = lastStartCommand;
                    if (startCommand != null) {
                        error = new PlaybackTimeoutError(startCommand.getDomain());
                    }
                }
                BaseTvViewControl.this.offerEvent(PlaybackEvent.PlaybackUnavailable, error);
                return true;
        }
        return false;
    };
}
