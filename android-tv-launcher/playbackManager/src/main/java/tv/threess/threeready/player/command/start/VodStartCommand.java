package tv.threess.threeready.player.command.start;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.VodControlProximus;
import tv.threess.threeready.player.control.base.PlaybackControl;

public class VodStartCommand extends ContentItemStartCommand<IVodVariant> {

    private final VodControlProximus mControl;
    private final IVodItem mVod;
    private final IVodPrice mVodPrice;
    private final IVodVariant mVodVariant;
    private final IVodSeries mVodSeries;
    private final Long mPosition;

    public VodStartCommand(long id, PlaybackType type, PlaybackControl control,
                           StartAction startAction, IVodItem vod, IVodPrice vodPrice, IVodVariant vodVariant, IVodSeries vodSeries, Long position) {
        super(id, type, startAction, control);
        mVod = vod;
        mVodVariant = vodVariant;
        mVodPrice = vodPrice;
        mVodSeries = vodSeries;
        mPosition = position;
        mControl = validateControl(control, VodControlProximus.class);
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        mControl.start(this);
    }

    public IVodItem getVodItem() {
        return mVod;
    }

    public IVodPrice getVodPrice() {
        return mVodPrice;
    }

    public Long getPosition() {
        return mPosition;
    }

    @Override
    public VodStartCommand createReTuneCommand(StartAction action, long cid) {
        return new VodStartCommand(cid, type, control, action, mVod, mVodPrice, mVodVariant, mVodSeries, null);
    }

    @Override
    public Comparable<?> playbackId() {
        IVodVariant vodVariant = mVodVariant;
        if (vodVariant == null) {
            return null;
        }
        return vodVariant.getId();
    }

    @Override
    public IVodVariant getContentItem() {
        return mVodVariant;
    }

    public IVodSeries getVodSeries() {
        return mVodSeries;
    }
}
