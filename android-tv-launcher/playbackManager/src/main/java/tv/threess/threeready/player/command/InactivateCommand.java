/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command;

import tv.threess.threeready.player.command.base.ImplicitCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.model.PlaybackEntireState;

/**
 * Implicit command that transition the state to Inactive.
 *
 * @author Dan Titiu
 * @since 24/11/2016
 */
public class InactivateCommand extends ImplicitCommand {

    public InactivateCommand(long id) {
        super(id, null, PlaybackAction.Inactivate, PlaybackState.Inactive);
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        // Inactivating is valid if the same domain is still active.
        return this.control.getCurrentState().active;
    }

    @Override
    protected void executeImplicit() {
        this.control.confirm(this.id);
    }

    @Override
    public void commit() {
        this.control.applyState(this.targetState);
        // Inactive states do not have a full status
        this.control.applyDetails(null);
        this.control.reset();
    }
}
