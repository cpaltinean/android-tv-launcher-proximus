/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command;

import tv.threess.threeready.player.command.base.ImplicitCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.result.EventResult;

/**
 * Implicit command that transition the state to Stopped.
 *
 * @author Dan Titiu
 * @since 25.06.2014
 */
public class StopCommand extends ImplicitCommand {

    public StopCommand(long id) {
        this(id, null);
    }

    public StopCommand(long id, PlaybackCommand injectedBy) {
        super(id, injectedBy, PlaybackAction.Stop, PlaybackState.Stopped);
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        // Stopping is valid if the same domain is still active.
        //return ds.exclusiveState.active && ds.equalsExclusiveDomain(this.type.getDomain());
        return this.control.getCurrentState().active;
    }

    @Override
    protected void executeImplicit() throws Exception {
        this.control.stop(this.getId());
    }

    @Override
    public void commit() {
        super.commit();
        this.control.applyState(this.targetState);
        // Inactive states do not have a full status
        this.control.applyDetails(null);
        this.control.reset();
    }

    @Override
    public boolean discards(EventResult eventResult) {
        super.discards(eventResult);
        return eventResult != null && eventResult.domain == getDomain() && eventResult.event.mDiscardable;
    }
}
