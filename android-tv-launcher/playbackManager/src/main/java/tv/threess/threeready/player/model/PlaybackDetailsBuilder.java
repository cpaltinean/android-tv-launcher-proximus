/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.model;

import androidx.annotation.NonNull;

import java.util.Arrays;

import tv.threess.threeready.api.generic.model.TimeRange;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;

/**
 * Builder of PlaybackDetails DTO.
 *
 * @author Dan Titiu
 * @since 21.06.2014
 */
//TODO: Recycle instances using a pool (see Handler Messages)
public class PlaybackDetailsBuilder extends PlaybackDetails {

    public PlaybackDetailsBuilder() {
        super();
        // Set implicit type in order to be able to get the default domain
        this.type = PlaybackType.Default;
    }

    public PlaybackDetailsBuilder setType(PlaybackType type) {
        this.type = type;
        return this;
    }

    public PlaybackDetailsBuilder setState(@NonNull PlaybackState state) {
        this.state = state;
        return this;
    }

    public PlaybackDetailsBuilder setPosition(long value) {
        this.currentPosition = value;
        return this;
    }

    public PlaybackDetailsBuilder setSeekAbleStart(long value) {
        this.seekAbleStart = value;
        return this;
    }

    public PlaybackDetailsBuilder setSeekAbleEnd(long value) {
        this.seekAbleEnd = value;
        return this;
    }

    public PlaybackDetailsBuilder setBufferStart(long value) {
        this.bufferStart = value;
        return this;
    }

    public PlaybackDetailsBuilder setBufferEnd(long value) {
        this.bufferEnd = value;
        return this;
    }

    public PlaybackDetailsBuilder setStart(long value) {
        this.startPosition = value;
        return this;
    }

    public PlaybackDetailsBuilder setEnd(long value) {
        this.endPosition = value;
        return this;
    }

    public PlaybackDetailsBuilder setUnskippableRanges(TimeRange[] value) {
        this.unskippableRanges = value;
        return this;
    }

    public PlaybackDetailsBuilder setLive(boolean value) {
        this.isLive = value;
        return this;
    }

    public PlaybackDetailsBuilder setPlaybackSpeed(double playbackSpeed) {
        this.playbackSpeed = playbackSpeed;
        return this;
    }

    public PlaybackDetailsBuilder setTeletext(boolean teletext) {
        this.teletext = teletext;
        return this;
    }

    public PlaybackDetailsBuilder setCanPause(boolean value) {
        this.canPause = value;
        return this;
    }

    public PlaybackDetailsBuilder setResumeBookmark(boolean resumeBookmark) {
        this.resumeBookmark = resumeBookmark;
        return this;
    }

    public PlaybackDetailsBuilder setAudioTracks(@NonNull MediaTrackInfo[] audioTracks) {
        this.audioTracks = audioTracks;
        return this;
    }

    public PlaybackDetailsBuilder setSelectedAudioIndex(int index) {
        this.selectedAudioIndex = index;
        return this;
    }

    public PlaybackDetailsBuilder setVideoTracks(@NonNull MediaTrackInfo[] videoTracks) {
        this.videoTracks = videoTracks;
        return this;
    }

    public PlaybackDetailsBuilder setSelectedVideoIndex(int index) {
        this.selectedVideoIndex = index;
        return this;
    }

    public PlaybackDetailsBuilder setSubtitleTracks(@NonNull MediaTrackInfo[] subtitleTracks) {
        this.subtitleTracks = subtitleTracks;
        return this;
    }

    public PlaybackDetailsBuilder setSelectedSubtitleIndex(int index) {
        this.selectedSubtitleIndex = index;
        return this;
    }

    public void setPlaybackUrl(String playbackUrl) {
        this.playbackUrl = playbackUrl;
    }

    public PlaybackDetailsBuilder from(PlaybackDetails details) {
        this.type = details.type;
        this.state = details.state;
        this.currentPosition = details.currentPosition;
        this.startPosition = details.startPosition;
        this.endPosition = details.endPosition;
        this.seekAbleStart = details.seekAbleStart;
        this.seekAbleEnd = details.seekAbleEnd;
        this.bufferStart = details.bufferStart;
        this.bufferEnd = details.bufferEnd;
        this.unskippableRanges = details.unskippableRanges;
        this.playbackSpeed = details.playbackSpeed;
        this.teletext = details.teletext;
        this.canPause = details.canPause;
        this.resumeBookmark = details.resumeBookmark;
        this.audioTracks = details.audioTracks;
        this.videoTracks = details.videoTracks;
        this.selectedAudioIndex = details.selectedAudioIndex;
        this.selectedVideoIndex = details.selectedVideoIndex;
        this.subtitleTracks = details.subtitleTracks;
        this.selectedSubtitleIndex = details.selectedSubtitleIndex;
        this.playbackUrl = details.playbackUrl;
        return this;
    }

    public PlaybackDetails build() {
        return this;
    }

    /**
     * Compares this instance with another given instance and returns true if the two instances are equal.
     * Used mainly during UnitTests.
     *
     * @param o the object to compare this instance with.
     * @return {@code true} if the specified object is equal to this {@code Object}; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object o) {
        // Shortcut for same instance
        if (this == o) {
            return true;
        }
        // Validate type
        if (!(o instanceof PlaybackDetailsBuilder)) {
            return false;
        }
        // Check fields
        PlaybackDetailsBuilder other = (PlaybackDetailsBuilder) o;
        return this.type == other.type && this.state == other.state
                && this.playbackSpeed == other.playbackSpeed
                && this.currentPosition == other.currentPosition
                && this.startPosition == other.startPosition
                && this.endPosition == other.endPosition
                && this.seekAbleStart == other.seekAbleStart
                && this.seekAbleEnd == other.seekAbleEnd
                && this.bufferStart == other.bufferStart
                && this.bufferEnd == other.bufferEnd
                && Arrays.equals(this.unskippableRanges, other.unskippableRanges)
                && this.teletext == other.teletext
                && this.canPause == other.canPause
                && this.resumeBookmark == other.resumeBookmark
                && this.selectedAudioIndex == other.selectedAudioIndex
                && this.selectedVideoIndex == other.selectedVideoIndex
                && this.selectedSubtitleIndex == other.selectedSubtitleIndex
                && Arrays.equals(this.audioTracks, other.audioTracks)
                && Arrays.equals(this.videoTracks, other.videoTracks)
                && Arrays.equals(this.subtitleTracks, other.subtitleTracks)
                && this.playbackUrl.equals(other.playbackUrl);
    }
}
