package tv.threess.threeready.player.exception;

import tv.threess.threeready.api.tv.model.TvAdReplacement;

/**
 * Exception that happens during the Ad stitching
 *
 * @author Andor Lukacs
 * @since 03.12.2021
 */
public class AdStitchError extends PlaybackError {
    private final TvAdReplacement.Error mError;

    public AdStitchError(TvAdReplacement.Error error, String message) {
        super(message);
        mError = error;
    }

    public TvAdReplacement.Error getError() {
        return mError;
    }
}
