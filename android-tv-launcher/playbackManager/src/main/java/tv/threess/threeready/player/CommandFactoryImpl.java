/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import android.content.Intent;
import android.os.Bundle;

import java.lang.ref.WeakReference;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.command.AbsoluteJumpCommand;
import tv.threess.threeready.player.command.AppendReplayCommand;
import tv.threess.threeready.player.command.ForceStopCommand;
import tv.threess.threeready.player.command.MuteCommand;
import tv.threess.threeready.player.command.PauseCommand;
import tv.threess.threeready.player.command.PrependReplayCommand;
import tv.threess.threeready.player.command.RelativeJumpCommand;
import tv.threess.threeready.player.command.ResumeCommand;
import tv.threess.threeready.player.command.SelectAudioCommand;
import tv.threess.threeready.player.command.SelectSubtitleCommand;
import tv.threess.threeready.player.command.StopCommand;
import tv.threess.threeready.player.command.TrickPlayStopCommand;
import tv.threess.threeready.player.command.UnMuteCommand;
import tv.threess.threeready.player.command.UpdateReplayPlaylistCommand;
import tv.threess.threeready.player.command.ValidationPositionCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.command.start.AppStartCommand;
import tv.threess.threeready.player.command.start.GdprStartCommand;
import tv.threess.threeready.player.command.start.IptvLiveStartCommand;
import tv.threess.threeready.player.command.start.RecordingStartCommand;
import tv.threess.threeready.player.command.start.ReplayStartCommand;
import tv.threess.threeready.player.command.start.TrailerStartCommand;
import tv.threess.threeready.player.command.start.TrickPlayStartCommand;
import tv.threess.threeready.player.command.start.VodStartCommand;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.base.PlaybackControl;

/**
 * Factory implementation for creating PlaybackCommands.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.06.06
 */
public class CommandFactoryImpl implements CommandFactory {

    private WeakReference<ControlProvider> mController;

    public CommandFactoryImpl() {
    }

    @Override
    public void initialize(WeakReference<ControlProvider> controller) {
        mController = controller;
    }

    @Override
    public StartCommand makeLiveStartCommand(long cid, StartAction startAction, TvChannel channel, IBroadcast broadcast) {
        ControlProvider provider = mController.get();
        if (channel.getType() == ChannelType.RADIO) {
            return new IptvLiveStartCommand(cid, PlaybackType.RadioIpTv, provider.getControl(PlaybackDomain.RadioIpTv),
                    startAction, channel, broadcast);
        }
        return new IptvLiveStartCommand(cid, PlaybackType.LiveTvIpTv, provider.getControl(PlaybackDomain.LiveTvIpTv),
                startAction, channel, broadcast);
    }

    /**
     * Create a command to start the replay playback.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param broadcast IBroadcast used to start the playback on.
     * @param position The position in milliseconds relative to the beginning of the stream
     *                 from where the playback should be started.
     * @param isFromFavourite Flag used to know if the channel id is from favourite channels list.
     * @return The newly created command.
     */
    @Override
    public StartCommand makeReplayStartCommand(long cid, StartAction startAction, IBroadcast broadcast, Long position, boolean isFromFavourite) {
        final PlaybackControl<?> control = mController.get().getControl(PlaybackDomain.Replay);
        return new ReplayStartCommand(cid, PlaybackType.Replay, control, startAction, broadcast, position, isFromFavourite);
    }

    /**
     * Create command to prepend the playlist with the previous replay.
     * @param cid The unique id of the prepend command.
     * @param broadcast The previous replay broadcast to which prepend the playlist
     * @return The newly created command.
     */
    @Override
    public PlaybackCommand makePrependReplayCommand(long cid, IBroadcast broadcast) {
        return new PrependReplayCommand(cid, broadcast);
    }

    /**
     * Create command to append the playlist with the next replay.
     *
     * @param cid       The unique id of the append command.
     * @param broadcast The next replay broadcast to which append the playlist
     * @return The newly created command.
     */
    @Override
    public PlaybackCommand makeAppendReplayCommand(long cid, IBroadcast broadcast) {
        return new AppendReplayCommand(cid, broadcast);
    }

    /**
     * Create command to update the playlist to cover all the broadcast in the continuous replay list with playback.
     * @param cid The unique id of the append command.
     * @return The newly created command.
     */
    @Override
    public PlaybackCommand makeUpdateReplayPlaylistCommand(long cid) {
        return new UpdateReplayPlaylistCommand(cid);
    }

    /**
     * Create a command to start the vod playback.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param vod The parent vod to start the playback for.
     * @param vodPrice The vod price used to start the playback.
     * @param vodVariant The vod variant to start the playback for.
     * @param vodSeries The vod series of the started vod.
     * @param position Force relative position where the playback should start.
     * @return The newly created command.
     */
    @Override
    public StartCommand makeVodStartCommand(long cid, StartAction startAction, IVodItem vod, IVodPrice vodPrice, IVodVariant vodVariant, IVodSeries vodSeries, Long position) {
        final PlaybackControl<?> control = mController.get().getControl(PlaybackDomain.Vod);
        return new VodStartCommand(cid, PlaybackType.Vod, control, startAction, vod, vodPrice, vodVariant, vodSeries, position);
    }

    /**
     * Create a command to start the recording playback.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param recording The recording to start the playback for.
     * @param series The series of the started recording.
     * @param position Force relative position where the playback should start.
     * @return The newly created command.
     */
    @Override
    public StartCommand makeRecordingStartCommand(long cid, StartAction startAction, IRecording recording, IRecordingSeries series, Long position) {
        final PlaybackControl<?> control = mController.get().getControl(PlaybackDomain.Recording);
        return new RecordingStartCommand(cid, PlaybackType.Recording, control, startAction, recording, series, position);
    }

    /**
     * Create a command to start the trailer playback.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param vod The parent vod to start the playback for.
     * @param vodVariant The vod variant to start the playback for.
     * @return The newly created command.
     */
    @Override
    public StartCommand makeTrailerStartCommand(long cid, StartAction startAction, IVodItem vod, IVodVariant vodVariant) {
        final PlaybackControl<?> control = mController.get().getControl(PlaybackDomain.Trailer);
        return new TrailerStartCommand(cid, PlaybackType.VodTrailer, control, startAction, vod, vodVariant, 0L);
    }

    /**
     * Create a command to start a GDPR video.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param vod The parent vod to start the playback for.
     * @param vodPrice The vod price used to start the playback.
     * @param vodVariant The vod variant to start the playback for.
     * @return The newly created command.
     */
    @Override
    public StartCommand makeGdprStartCommand(long cid, StartAction startAction, IVodItem vod, IVodPrice vodPrice, IVodVariant vodVariant) {
        final PlaybackControl<?> control = mController.get().getControl(PlaybackDomain.Gdpr);
        return new GdprStartCommand(cid, PlaybackType.Gdpr, control, startAction, vod, vodPrice, vodVariant);
    }

    /**
     * Create a command to start trick play playback.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param activeDomain The playback domain which currently active and the trick play should start on it.
     * @param playbackSpeed The speed of the trick play playback to start on.
     * @return The newly created command.
     */
    @Override
    public StartCommand makeTrickPlayStartCommand(long cid, StartAction startAction, PlaybackDomain activeDomain, float playbackSpeed) {
        final PlaybackControl<?> trickPlayControl = mController.get().getControl(PlaybackDomain.TrickPlay);
        final PlaybackControl<?> activeControl = mController.get().getControl(activeDomain);
        return new TrickPlayStartCommand(cid, startAction, PlaybackType.TrickPlay, trickPlayControl, activeControl, playbackSpeed);
    }

    /**
     * Create a command to start a 3rd party application.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param packageName The package name of the application.
     * @param activityName The name of the activity in the application.
     * @param bundle The extra parameters needs to be send to the activity.
     * @return The newly created command.
     */
    @Override
    public StartCommand makeAppStartCommand(long cid, StartAction startAction, String packageName, String activityName, Bundle bundle) {
        final PlaybackControl<?> control = mController.get().getControl(PlaybackDomain.App);
        return new AppStartCommand(cid, PlaybackType.App, control, startAction, packageName, activityName, bundle);
    }

    /**
     * Create a command to start a 3rd party application.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param packageName The package name of the application.
     * @param intent The intent to start the activity with.
     * @param bundle The extra parameters needs to be send to the activity.
     * @return The newly created command.
     */
    @Override
    public StartCommand makeAppStartCommand(long cid, StartAction startAction, String packageName, Intent intent, Bundle bundle) {
        final PlaybackControl<?> control = mController.get().getControl(PlaybackDomain.App);
        return new AppStartCommand(cid, PlaybackType.App, control, startAction, intent, packageName, bundle);
    }

    /**
     * Create a command to stop the current playback.
     * @param cid The unique identifier of the command.
     * @return The newly created command.
     */
    @Override
    public PlaybackCommand makeStopCommand(long cid) {
        return new StopCommand(cid);
    }

    /**
     * Create a command to stop the currently running trick play.
     * @param cid The unique identifier of the command.
     * @return The newly created command.
     */
    @Override
    public PlaybackCommand makeTrickPlayStopCommand(long cid, PlaybackDomain activeDomain) {
        final PlaybackControl<?> trickPlayControl = mController.get().getControl(PlaybackDomain.TrickPlay);
        final PlaybackControl<?> activeControl = mController.get().getControl(activeDomain);
        return new TrickPlayStopCommand(cid, trickPlayControl, activeControl);
    }

    /**
     * Create a command to mute the volume of the current playback.
     * @param cid The unique identifier of the command.
     * @return The newly created command.
     */
    @Override
    public PlaybackCommand makeMuteCommand(long cid) {
        return new MuteCommand(cid);
    }

    /**
     * Create a command to unmute the volume of the current playback.
     * @param cid The unique identifier of the command.
     * @return The newly created command.
     */
    @Override
    public PlaybackCommand makeUnMuteCommand(long cid) {
        return new UnMuteCommand(cid);
    }

    /**
     * Create a non cancelable command to stop the currently running playback.
     * @param cid The unique identifier of the command.
     * @return The newly created command.
     */
    @Override
    public PlaybackCommand makeForceStopCommand(long cid) {
        return new ForceStopCommand(cid);
    }

    /**
     * Create a command to pause the currently running playback.
     * @param cid The unique identifier of the command.
     * @return The newly created command.
     */
    @Override
    public PlaybackCommand makePauseCommand(long cid) {
        return new PauseCommand(cid);
    }

    /**
     * Create a command to resume the previously paused playback.
     * @param cid The unique identifier of the command.
     * @return The newly created command.
     */
    @Override
    public PlaybackCommand makeResumeCommand(long cid, StartAction startAction) {
        return new ResumeCommand(cid, startAction);
    }

    /**
     * Create a command which will validate the given position with the control and trigger any events necessary for the position.
     * E.g. EndReached, PlaylistAppend etc.
     * @param cid The unique identifier of the command.
     * @param position The absolute position which needs to be processed.
     * @return THe newly created command.
     */
    @Override
    public PlaybackCommand makeValidationPositionCommand(long cid, long position) {
        return new ValidationPositionCommand(cid, position);
    }

    /**
     * Create a command to jump in the playback relative to the current position with given amount.
     * @param cid The unique identifier of the command.
     * @param duration The amount in millis to jump.
     *                Negative value will result a backward jump, positive value a forward jump.
     * @param startAction The user action which triggered the jump.
     * @return The newly created command.
     */
    @Override
    public PlaybackCommand makeRelativeJumpCommand(long cid, StartAction startAction, long duration) {
        return new RelativeJumpCommand(cid, startAction, duration);
    }

    /**
     * Create a command to jump the the absolute position in the stream.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the jump.
     * @param position The absolute position in the stream to jump.
     * @return The newly created command.
     */
    @Override
    public PlaybackCommand makeAbsoluteJumpCommand(long cid,  StartAction startAction, long position) {
        return new AbsoluteJumpCommand(cid, startAction, position);
    }

    /**
     * Create a command to change the audio track in the current playback.
     * @param cid The unique identifier of the command.
     * @param trackId The identifier of the audio track which needs to be selected.
     * @return The newly created command.
     */
    @Override
    public PlaybackCommand makeSelectAudioCommand(long cid, String trackId) {
        return new SelectAudioCommand(cid, trackId);
    }

    /**
     * Create a command to change the subtitle in the current playback.
     * @param cid The unique identifier of the command.
     * @param trackId The identifier of the subtitle track which needs to be selected.
     * @return The newly created command.
     */
    @Override
    public PlaybackCommand makeSelectSubtitleCommand(long cid, String trackId) {
        if (trackId == null) {
            return new SelectSubtitleCommand(cid);
        } else {
            return new SelectSubtitleCommand(cid, trackId);
        }
    }
}
