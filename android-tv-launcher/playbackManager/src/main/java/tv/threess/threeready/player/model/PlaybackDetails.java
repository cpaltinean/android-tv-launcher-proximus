/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.model;

import androidx.annotation.NonNull;

import java.io.Serializable;

import tv.threess.threeready.api.generic.model.TimeRange;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;

/**
 * Parcelable DTO that stores details about current Playback state. Usually decorates a callback result.
 *
 * @author Dan Titiu
 * @since 21.06.2014
 */
public class PlaybackDetails implements Serializable {

    public static final MediaTrackInfo[] TRACKS_NONE = new MediaTrackInfo[0];

    public PlaybackDomain getDomain() {
        return this.type != null ? this.type.getDomain() : null;
    }

    public PlaybackType getType() {
        return this.type;
    }

    public PlaybackState getState() {
        return this.state;
    }

    /**
     * Current playback position in milliseconds, usually updated by callback events from lower levels.
     * It is also updated during command execution for result anticipation (anticipate the result and adjust on callback
     * if needed).
     * Position retrieval might correct values that get off the chart (e.g. while FastForwarding) by using the
     * {@link #getStart()} and {@link #getEnd()}.
     *
     * @return Current playback position in milliseconds.
     */
    public long getPosition() {
        return currentPosition;
    }

    /**
     * @return Current playback position in milliseconds relative to the start of the content.
     */
    public long getRelativePosition() {
        if (currentPosition <= 0) {
            return 0;
        }
        return currentPosition - startPosition;
    }

    /**
     * @return The utc timestamp representing the start of the playback (start time of broadcast, 0 for vod).
     */
    public long getStart() {
        return startPosition;
    }

    /**
     * @return The utc timestamp representing the end of the playback (end time of broadcast, duration for vod).
     */
    public long getEnd() {
        return endPosition;
    }

    /**
     * @return true if the playback position is near to the live position.
     */
    public boolean isLive() {
        return isLive;
    }

    /**
     * Returns the current speed step, has positive values for fast forward and negative values for rewind. Zero means
     * normal playback.
     *
     * @return the current speed step.
     */
    public double getPlaybackSpeed() {
        return this.playbackSpeed;
    }

    /**
     * @return Returns the timestamp from where the playback can not be rewind anymore, start of the seekable slot.
     */
    public long getSeekAbleStart() {
        return seekAbleStart;
    }

    /**
     * @return Returns the timestamp from where the playback can not be fast forward anymore, end of the seekable slot.
     */
    public long getSeekAbleEnd() {
        return seekAbleEnd;
    }

    /**
     * Returns the visible position of the buffer start.
     * In case of black list slots, or any other restrictions of the fast forwarding, the real blocking end will be the "seekAbleStart".
     */
    public long getBufferStart() {
        return bufferStart;
    }

    /**
     * Returns the visible position of the buffer end.
     * In case of black list slots, or any other restrictions of the fast forwarding, the real blocking end will be the "seekAbleEnd".
     */
    public long getBufferEnd() {
        return bufferEnd;
    }

    public TimeRange[] getUnskippableRanges() {
        return unskippableRanges;
    }

    /**
     * Returns true when the currently playing stream contains teletext.
     *
     * @return true when the currently playing stream contains teletext.
     */
    public boolean hasTeletext() {
        return this.teletext;
    }

    /**
     * Returns true when the currently playing stream can pause.
     */
    public boolean canPause() {
        return this.canPause;
    }

    /**
     * @return True if the current playback was resumed from a bookmark.
     */
    public boolean isResumeBookmark() {
        return resumeBookmark;
    }

    /**
     * Returns true when the currently playing stream contains subtitle tracks.
     *
     * @return true when the currently playing stream contains subtitle tracks.
     */
    public boolean hasSubtitle() {
        return this.subtitleTracks.length > 0;
    }

    /**
     * Returns the audio tracks which are currently available in the active stream.
     * Please consider the fact that reading the media tracks requires the stream to be processed and playing.
     *
     * @return the audio tracks which are currently available in the active stream.
     */
    @NonNull
    public MediaTrackInfo[] getAudioTracks() {
        return this.audioTracks;
    }

    /**
     * Returns the currently selected audio track.
     * Unlike subtitles, one audio track is always selected and cannot be deselected.
     *
     * @return the currently selected audio track.
     */
    public MediaTrackInfo getCurrentAudioTrack() {
        if (this.selectedAudioIndex >= 0) {
            return this.audioTracks[this.selectedAudioIndex];
        }

        return null;
    }

    /**
     * Returns the video tracks which are currently available in the active stream.
     * Please consider the fact that reading the media tracks requires the stream to be processed and playing.
     *
     * @return the video tracks which are currently available in the active stream.
     */
    @NonNull
    public MediaTrackInfo[] getVideoTracks() {
        return this.videoTracks;
    }

    /**
     * Returns the currently selected video track.
     *
     * @return the currently selected video track.
     */
    public MediaTrackInfo getCurrentVideoTrack() {
        if (this.selectedVideoIndex >= 0) {
            return this.videoTracks[this.selectedVideoIndex];
        }

        return null;
    }

    /**
     * Returns the subtitle tracks which are currently available in the active stream.
     * Please consider the fact that reading the media tracks requires the stream to be processed and playing.
     *
     * @return the subtitle tracks which are currently available in the active stream.
     */
    @NonNull
    public MediaTrackInfo[] getSubtitleTracks() {
        return this.subtitleTracks;
    }

    /**
     * Returns the currently selected subtitle track.
     * Subtitle tracks are not selected by default and can be deselected (disabled).
     *
     * @return the currently selected subtitle track.
     */
    public MediaTrackInfo getCurrentSubtitleTrack() {
        if (this.selectedSubtitleIndex >= 0) {
            return this.subtitleTracks[this.selectedSubtitleIndex];
        }

        return null;
    }

    public String getPlaybackUrl() {
        return this.playbackUrl;
    }

    public boolean isUnskippablePosition() {
        return TimeRange.remainingTime(unskippableRanges, currentPosition) > 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append("{type[").append(this.type);
        sb.append("],state[").append(this.state);
        sb.append("],position[").append(Log.timestamp(this.currentPosition));
        sb.append("],seekAble[").append(Log.timestamp(this.seekAbleStart)).append(", ").append(Log.timestamp(this.seekAbleEnd));
        sb.append("],buffer[").append(Log.timestamp(this.bufferStart)).append(", ").append(Log.timestamp(this.bufferEnd));
        sb.append("],bounds[").append(Log.timestamp(this.startPosition)).append(", ").append(Log.timestamp(this.endPosition));
        sb.append("],duration[").append(this.endPosition - this.startPosition);
        sb.append("],playbackSpeed[").append(this.playbackSpeed);
        sb.append("],isLive[").append(this.isLive);
        sb.append("],teletext[").append(this.teletext);
        sb.append("],canPause[").append(this.canPause);
        sb.append("],resumeBookmark[").append(this.resumeBookmark);
        sb.append("],audioIdx[").append(this.selectedAudioIndex);
        sb.append("],videoIdx[").append(this.selectedVideoIndex);
        sb.append("],subtitleIdx[").append(this.selectedSubtitleIndex);
        sb.append("]}");
        return sb.toString();
    }

    //
    // PRIVATE IMPLEMENTATION
    //

    protected PlaybackType type;

    @NonNull
    protected PlaybackState state = PlaybackState.Released;

    protected long currentPosition = -1;
    protected long startPosition;
    protected long endPosition;
    protected long seekAbleStart;
    protected long seekAbleEnd;
    protected long bufferStart;
    protected long bufferEnd;
    protected TimeRange[] unskippableRanges;
    protected boolean isLive;
    protected double playbackSpeed;

    protected boolean teletext;

    protected boolean canPause;
    protected boolean resumeBookmark;

    @NonNull
    protected MediaTrackInfo[] audioTracks = TRACKS_NONE;

    @NonNull
    protected MediaTrackInfo[] videoTracks = TRACKS_NONE;

    @NonNull
    protected MediaTrackInfo[] subtitleTracks = TRACKS_NONE;

    protected int selectedAudioIndex = -1;

    protected int selectedSubtitleIndex = -1;

    protected int selectedVideoIndex = -1;

    protected String playbackUrl;

    protected PlaybackDetails() {
    }
}
