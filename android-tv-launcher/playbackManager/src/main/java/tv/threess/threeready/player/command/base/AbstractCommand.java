/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command.base;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import tv.threess.threeready.player.contract.CommandValidity;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackRelation;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.result.EventResult;
import tv.threess.threeready.player.result.Result;

/**
 * Represents a single item from the playback queue, carrying a playback domain (e.g. LiveTV, NetworkPVR, VOD, etc) and
 * all information needed for issuing a command to the player(s).
 *
 * @author Dan Titiu
 * @since 05.02.2014
 */
public abstract class AbstractCommand implements PlaybackCommand {

    /**
     * Unique identifier for each command. Useful for pairing the command with its asynchronous result.
     */
    protected final long id;

    /**
     * Flag marking if a command has been cancelled or not.
     */
    private volatile boolean cancelled;

    /**
     * Playback action to be taken on the appropriate PlaybackControl.
     */
    @NonNull
    protected final PlaybackAction action;

    /**
     * Playback state that is reached once this command is successfully executed.
     */
    @NonNull
    protected final PlaybackState targetState;

    private volatile long mCmdOfferTimestamp;
    private long mCmdTimestamp;

    private final long mTriggerTime;
    private volatile long mQueueTime;
    private volatile long mCmdTransitionTime;
    private volatile long mExecutionTime;
    private volatile long mTotalTime;

    /**
     * Default constructor.
     *
     * @param id            Unique identifier for each command.
     * @param action        Playback action to be taken on the appropriate PlaybackControl.
     * @param targetState   Playback state that is reached once this command is successfully executed.
     */
    public AbstractCommand(final long id, @NonNull final PlaybackAction action, @NonNull final PlaybackState targetState) {
        this.id = id;
        this.action = action;
        this.targetState = targetState;
        this.cancelled = false;
        this.mTriggerTime = System.currentTimeMillis();
    }

    @Override
    public long getId() {
        return id;
    }

    @NonNull
    @Override
    public PlaybackAction getAction() {
        return this.action;
    }

    @NonNull
    @Override
    public PlaybackState getTargetState() {
        return this.targetState;
    }

    @Override
    public boolean isSynthetic() {
        return this.id < 0L;
    }

    /**
     * Check the relation between this command and the one specified.
     *
     * @param other    PlaybackCommand to relate to.
     * @param executing The other playback command is currently executing
     * @return The relation between this command and the one specified.
     */
    @Override
    public PlaybackRelation relate(final PlaybackCommand other, boolean executing) {
        // By default use actions to relate commands, but only if both domains are exclusive
        // or commands are for the same domain.
        if ((this.getDomain().exclusive && other.getDomain().exclusive) || this.getDomain().equals(other.getDomain())) {
            return this.action.relate(other.getAction());
        }

        return PlaybackRelation.None;
    }

    /**
     * Accumulate specific playback command value(s) from the specified command. By default it does nothing.
     * Please override this method for specific behavior.
     *
     * @param other    PlaybackCommand from which to accumulate value(s).
     */
    @Override
    public void accumulate(final PlaybackCommand other) {
        // No default accumulation
    }

    @Override
    public CommandValidity validate(PlaybackEntireState ds) {
        // Reuse verification logic
        return this.verify(ds) ? CommandValidity.Valid : CommandValidity.Invalid;
    }

    @Override
    public void cancel() {
        this.cancelled = true;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void assertNotCancelled() throws PlaybackError {
        if (isCancelled()) {
            throw new PlaybackError(PlaybackError.Type.CANCELED);
        }
    }

    @Override
    public boolean isFulfilledBy(Result result) {
        // By default commands need just one result. Commands that require several results should override.
        return result.matches(this);
    }

    @Override
    public void rollback(PlaybackError error, @Nullable Long nextCmdId) throws Exception {
        // No default rollback, kept only for future use
        onCmdCompleted();
    }

    @Override
    public void commit() {
        onCmdCompleted();
    }

    @Override
    public boolean discards(EventResult eventResult) {
        return false;
    }

    /**
     * equals() implementation used in unit tests.
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof AbstractCommand)) {
            return false;
        }
        AbstractCommand other = (AbstractCommand) o;
        return this.id == other.id && this.action.equals(other.action) && this.targetState.equals(other.targetState);
    }

    @Override
    public void offer() {
        mCmdOfferTimestamp = System.currentTimeMillis();
        long now = System.currentTimeMillis();
        mQueueTime = now - mCmdOfferTimestamp;
    }

    @Override
    public void execute() throws Exception {
        long now = System.currentTimeMillis();
        mCmdTransitionTime = now  - mCmdTimestamp;
        mCmdTimestamp = now;
    }

    @Override
    public long getTriggerTime() {
        return mTriggerTime;
    }

    @Override
    public long getQueueTime() {
        return mQueueTime;
    }

    @Override
    public long getTransitionCmdTime() {
        return mCmdTransitionTime;
    }

    @Override
    public long getProcessingTime() {
        return mExecutionTime;
    }

    @Override
    public long getCmdTotalTime() {
        return mTotalTime;
    }

    @Override
    public long getCommandInitiatedTimestamp() {
        return mCmdOfferTimestamp;
    }

    private void onCmdCompleted() {
        long now = System.currentTimeMillis();
        mExecutionTime = now - mCmdTimestamp;
        mTotalTime = now - mCmdOfferTimestamp;
    }

    protected <TControlClass> TControlClass validateControl(final PlaybackControl<?> control, Class<TControlClass> controlClass) {
        // Try to cast early and validate the given control
        if (!(controlClass.isInstance(control))) {
            throw new IllegalArgumentException(this.getClass().getSimpleName() + " requires a "
                    + controlClass.getSimpleName() + " but was called on " + control);
        }
        return (TControlClass) control;
    }
}
