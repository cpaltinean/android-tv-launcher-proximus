/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import java.util.List;
import java.util.Map;

import androidx.annotation.Nullable;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.model.PlaybackStatus;

/**
 * Interface for exposing and accessing the managed state of playback controls from dependent components.
 *
 * @author Dan Titiu
 * @since 30.04.2014
 */
public interface ControlProvider {

    /**
     * @return Currently active exclusive domain or null if there is no active domain.
     */
    @Nullable
    PlaybackDomain getActiveExclusiveDomain();

    /**
     * Exposes the {@link PlaybackControl} that is responsible for playback on a certain domain.
     *
     * @param domain    The {@link PlaybackDomain} for which to request the control instance.
     * @return the {@link PlaybackControl} that is responsible for playback on a certain domain.
     */
    PlaybackControl getControl(PlaybackDomain domain);

    /**
     * Retrieves the current playback status based on which control is active (not stopped).
     *
     * @return An instance of {@link PlaybackStatus}.
     */
    List<PlaybackStatus> getStatus();

    /**
     * Gather state parameters and builds details of playback for the currently active exclusive domain.
     * 
     *
     * @return The {@link PlaybackDetailsBuilder} for the active exclusive domain.
     */
    PlaybackDetailsBuilder getExclusiveDetails();

    /**
     * Gather state parameters and builds details of playback for the given exclusive domain.
     * @param type The type for which the playback details should be returned.
     *
     * @return The {@link PlaybackDetailsBuilder} for the requested domain.
     */
    PlaybackDetailsBuilder getDetails(PlaybackType type);

    /**
     * Gather state parameters and builds details of playback. May return an empty collection in case there is no
     * domain currently active or several items when more than one domain is active (not stopped).
     *
     * @return A list of {@link PlaybackDetails}.
     */
    Map<PlaybackDomain, PlaybackDetailsBuilder> getDetails();

    /**
     * Populates (refreshes) and returns the reference to the singleton instance of the entire state of PlaybackControls.
     *
     * @return the reference to the singleton instance of the entire state of PlaybackControls.
     */
    PlaybackEntireState getPlaybackEntireState();
}
