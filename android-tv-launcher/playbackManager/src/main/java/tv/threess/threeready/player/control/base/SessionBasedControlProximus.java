package tv.threess.threeready.player.control.base;

import android.content.Context;
import android.media.tv.TvView;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.BookmarkRepository;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.StreamingSessionRepository;
import tv.threess.threeready.api.playback.model.IOttStreamingSession;
import tv.threess.threeready.api.playback.model.OTTStreamingSessionContainer;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.model.OttPlayListItem;
import tv.threess.threeready.player.model.OttTuneParams;

/**
 * OTT playback control which handle the playback for the streaming session coming from backend.
 * @see IOttStreamingSession
 * @see StreamingSessionRepository
 *
 * @author Barabas Attila
 * @since 2020.06.29
 */
public abstract class SessionBasedControlProximus<TOTTStreamingSession extends IOttStreamingSession,
        TOTTStreamingSessionContainer extends OTTStreamingSessionContainer<TOTTStreamingSession>> extends OttControlProximus {

    protected final BookmarkRepository mBookmarkRepository = Components.get(BookmarkRepository.class);
    protected final StreamingSessionRepository mSessionHandler = Components.get(StreamingSessionRepository.class);

    private boolean mIsBingeWatchingEventSent = false;
    protected boolean mIsResumedBookmark = false;

    /**
     * Queue used for store the streaming sessions.
     */
    protected final TOTTStreamingSessionContainer mStreamingSessionContainer;

    public SessionBasedControlProximus(Context context, PlaybackDomain domain, TvView tvView,
                                       TOTTStreamingSessionContainer streamingSessionContainer) {
        super(context, domain, tvView);
        mStreamingSessionContainer = streamingSessionContainer;
    }

    /**
     * Start the playback on the OTT input service based on the opened streaming session.
     * @param cid The id of the command which triggered the start.
     * @param contentStartTime The start time of the content which needs to start.
     * @param contentEndTime The end time of the content which needs to start.
     * @param relativeStartPosition The position relative to the start time of the content from where the playback should start.
     * @param sessions The playback session to create playlist for continuous playback.
     */
    public void start(long cid, long contentStartTime, long contentEndTime, String assetIdentifier,
                      Long relativeStartPosition, TOTTStreamingSession... sessions) throws Exception {
        mStreamingSessionContainer.addAll(Arrays.asList(sessions));

        long startTime = contentStartTime;
        if (relativeStartPosition != null) {
            startTime += relativeStartPosition;
        }
        mCurrentPosition = startTime;

        // Generate tune params.
        OttTuneParams tuneParams = getTuneParams(startTime, assetIdentifier, Arrays.asList(sessions));
        start(cid, contentStartTime, contentEndTime, tuneParams);
    }

    @Override
    public OttTuneParams getTuneParams() {
        return getTuneParams(getCurrentPosition(), null, new ArrayList<>(mStreamingSessionContainer));
    }

    /**
     * Convert the give streaming session to OTT tune parameters.
     * @param startTime The position from where the playback needs to start.
     * @param streamingSessions The streaming sessions to create OTT playlist from.
     * @return The tune parameters to start the playback.
     */
    private OttTuneParams getTuneParams(long startTime, String assetIdentifier, List<TOTTStreamingSession> streamingSessions) {
        OttTuneParams.Builder builder = new OttTuneParams.Builder();

        for (int i=0; i<streamingSessions.size(); ++i) {
            TOTTStreamingSession session = streamingSessions.get(i);
            if (session.isTimeIncluded(startTime)) {
                builder.playListIndex(i);
                builder.startPosition(convertContentToPlaybackPosition(startTime));
            }
            builder.addPlayListItem(getPlayListItem(session));
        }
        builder.assetIdentifier(assetIdentifier);

        return builder.build();
    }

    /**
     * Convert the given streaming session the playlist item.
     * @param streamingSession The OTT streaming session
     * @return The converted play list item..
     */
    private OttPlayListItem getPlayListItem(TOTTStreamingSession streamingSession) {
        return new OttPlayListItem.Builder()
                .streamUrl(streamingSession.getUrl())
                .licenceUrl(streamingSession.getLicenseUrl())
                .drmClientId(Settings.ottDRMClientId.get(""))
                .playbackDomain(getDomain())
                .build();
    }

    /**
     * Prepend the current playlist with the new streaming sessions.
     * @param cid The id of the command which triggered to prepend.
     * @param streamingSessions The array of streaming session to prepend the playlist with.
     */
    protected void prepend(long cid, List<TOTTStreamingSession> streamingSessions) throws Exception {
        mStreamingSessionContainer.addAll(0, streamingSessions);

        // Generate append parameters.
        ArrayList<OttPlayListItem> prependParams = new ArrayList<>();
        for (TOTTStreamingSession session : streamingSessions) {
            prependParams.add(getPlayListItem(session));
        }

        prepend(cid, prependParams);
    }

    /**
     * Append the current playlist with the new streaming sessions.
     * @param cid The id of the command which triggered to append.
     * @param streamingSessions The array of streaming session to append the playlist with.
     */
    protected void append(long cid, List<TOTTStreamingSession> streamingSessions) throws Exception {
        mStreamingSessionContainer.addAll(streamingSessions);

        // Generate append parameters.
        ArrayList<OttPlayListItem> appendParams = new ArrayList<>();
        for (TOTTStreamingSession session : streamingSessions) {
            appendParams.add(getPlayListItem(session));
        }

        append(cid, appendParams);
    }

    /**
     * Convert the position in the playback to the position in the content.
     * @param playerPosition The position in the playback.
     * @return The content position.
     */
    protected long convertPlaybackToContentPosition(long playerPosition) {
        TOTTStreamingSession session = getCurrentSession();
        if (session != null) {
            long contentPosition = session.getStartTime() + playerPosition;

            Log.v(TAG, "convertToContentPosition() -> Session start: " + Log.timestamp(session.getStartTime()) +
                    ", player position: " + playerPosition +
                    ", contentPosition: " + Log.timestamp(contentPosition)
            );

            return contentPosition;
        }

        return super.convertPlaybackToContentPosition(playerPosition) ;
    }

    /**
     * @return The currently playing session or null if there is no such.
     */
    @Nullable
    protected TOTTStreamingSession getCurrentSession() {
        OttPlayListItem playListItem = getCurrentPlaylistItem();
        if (playListItem == null) {
            return null;
        }

        for (TOTTStreamingSession session : mStreamingSessionContainer) {
            if (Objects.equals(session.getUrl(), playListItem.getStreamUrl())) {
                return session;
            }
        }
        return null;
    }

    /**
     * Convert the position in the content to the position in the playback.
     * @param contentPosition The position in the content.
     * @return The position in the playback.
     */
    protected long convertContentToPlaybackPosition(long contentPosition) {
        TOTTStreamingSession session = mStreamingSessionContainer.findSessionForContentPosition(getCurrentPosition());
        if (session != null) {
            long playerPosition = contentPosition - session.getStartTime();

            Log.d(TAG, "convertToPlaybackPosition() -> Session start: " + Log.timestamp(session.getStartTime()) +
                    ", player position: " + playerPosition +
                    ", contentPosition: " + Log.timestamp(contentPosition)
            );

            return playerPosition;
        }
        return super.convertContentToPlaybackPosition(contentPosition);
    }

    /**
     * Stop all the previously opened streaming session.
     */
    private void stopStreamingSessions() {
        for (TOTTStreamingSession session : mStreamingSessionContainer) {
            mSessionHandler.closeStreamingSession(session);
        }
        mStreamingSessionContainer.clear();
    }

    @Override
    protected String getStreamUrl() {
        TOTTStreamingSession session = mStreamingSessionContainer.findSessionForContentPosition(getCurrentPosition());
        if (session != null) {
            return session.getUrl();
        }

        // if playback started, try to return the url from the playlist
        OttTuneParams tuneParams = mTuneParams;
        if (tuneParams != null) {
            return tuneParams.getDefaultPlaylistItem().getStreamUrl();
        }

        // no more place to search for url
        return null;
    }

    @Override
    public boolean isResumeBookmark() {
        return mIsResumedBookmark;
    }

    @Override
    public void processPosition(long contentPosition, boolean notifyChange) {
        super.processPosition(contentPosition, notifyChange);

        // Trigger binge watching.
        if (isBingeWatchingEnabled() && !mIsBingeWatchingEventSent
                && getCurrentState() == PlaybackState.Started) {
            offerEvent(PlaybackEvent.BingeWatchingReached);
            mIsBingeWatchingEventSent = true;
        }
    }

    @Override
    public void reset() {
        super.reset();
        mIsBingeWatchingEventSent = false;
        mIsResumedBookmark = false;

        try {
            stopStreamingSessions();
        } catch (Exception e) {
            Log.e(TAG, "Couldn't close streaming session.", e);
        }
    }
}
