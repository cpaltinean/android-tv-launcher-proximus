package tv.threess.threeready.player.command;

import java.util.Objects;

import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.player.command.base.ImplicitCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackRelation;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.ReplayControlProximus;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.model.PlaybackEntireState;

/**
 * Implicit command used for append a replay broadcast to the end of the current playlist.
 *
 * @author David Bondor
 * @since 2020.02.04
 */
public class AppendReplayCommand extends ImplicitCommand {

    private final IBroadcast mBroadcast;
    private ReplayControlProximus mControl;

    public AppendReplayCommand(final long id, IBroadcast broadcast) {
        super(id, null, PlaybackAction.Add, PlaybackState.None);
        mBroadcast = broadcast;
    }

    @Override
    public PlaybackCommand prepare(PlaybackType type, PlaybackControl control) {
        mControl = validateControl(control, ReplayControlProximus.class);
        return super.prepare(type, control);
    }

    @Override
    protected void executeImplicit() throws Exception {
        if (getDomain() == PlaybackDomain.Replay) {
            mControl.append(this);
        }
    }

    public IBroadcast getBroadcast() {
        return mBroadcast;
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        return mControl.getCurrentState().active;
    }


    @Override
    public PlaybackRelation relate(PlaybackCommand other, boolean executing) {
        if (other instanceof AppendReplayCommand
                && Objects.equals(mBroadcast.getId(), ((AppendReplayCommand) other).mBroadcast.getId())) {
            return PlaybackRelation.CancelOther;
        }
        return PlaybackRelation.None;
    }
}