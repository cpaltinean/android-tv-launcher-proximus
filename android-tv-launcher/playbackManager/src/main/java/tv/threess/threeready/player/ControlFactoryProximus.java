/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import android.content.Context;
import android.media.tv.TvView;
import android.view.SurfaceView;
import android.view.View;

import java.lang.ref.WeakReference;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.control.AppControl;
import tv.threess.threeready.player.control.ControlFactory;
import tv.threess.threeready.player.control.GdprControlProximus;
import tv.threess.threeready.player.control.InvalidControl;
import tv.threess.threeready.player.control.LiveOttControlProximus;
import tv.threess.threeready.player.control.RadioControlProximus;
import tv.threess.threeready.player.control.RecordingControlProximus;
import tv.threess.threeready.player.control.ReplayControlProximus;
import tv.threess.threeready.player.control.DtvControlProximus;
import tv.threess.threeready.player.control.TrailerControlProximus;
import tv.threess.threeready.player.control.TrickPlayControlProximus;
import tv.threess.threeready.player.control.VodControlProximus;
import tv.threess.threeready.player.control.base.PlaybackControl;

/**
 * Factory implementation for creating ComHem PlaybackControls.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.06.06
 */
public class ControlFactoryProximus implements ControlFactory {
    private static final String TAG = Log.tag(ControlFactoryProximus.class);

    private TvView mTvView;
    private SurfaceView mTrickPlayView;
    private View mTrickplayBackground;


    @Override
    public PlaybackControl createControl(PlaybackDomain domain, WeakReference<Context> context) {
        if (mTvView == null) {
            // No playback without surface.
            return null;
        }

        switch (domain) {
            case App:
                return new AppControl(context.get(), domain, mTvView);

            case Vod:
                return new VodControlProximus(context.get(), domain, mTvView);

            case LiveTvOtt:
                return new LiveOttControlProximus(context.get(), domain, mTvView);

            case LiveTvIpTv:
            case RadioIpTv:
                return new DtvControlProximus(context.get(), domain, mTvView);

            case Replay:
                return new ReplayControlProximus(context.get(), domain, mTvView);

            case Recording:
                return new RecordingControlProximus(context.get(), domain, mTvView);

            case RadioOtt:
                return new RadioControlProximus(context.get(), domain, mTvView);

            case Trailer:
                return new TrailerControlProximus(context.get(), domain, mTvView);

            case Gdpr:
                return new GdprControlProximus(context.get(), domain, mTvView);

            case TrickPlay:
                return new TrickPlayControlProximus(context.get(), domain, mTrickPlayView);

            case None:
                return new InvalidControl(context.get(), domain, mTvView);
        }
        return null;
    }

    @Override
    public void setTrickPlaySurface(SurfaceView surfaceView, View trickplayBackground) {
        Log.d(TAG, "setTrickPlaySurface() called with: surfaceView = [" + surfaceView + "]");
        mTrickPlayView = surfaceView;
        mTrickplayBackground = trickplayBackground;
    }

    @Override
    public void setTvView(TvView tvView) {
        mTvView = tvView;
    }

    @Override
    public SurfaceView getTrickPlaySurface() {
        return mTrickPlayView;
    }

    @Override
    public View getTrickplayBackground() {
        return mTrickplayBackground;
    }
}
