/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import android.os.Bundle;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.plugin.IPluginManager;
import tv.threess.threeready.player.plugin.PlaybackPlugin;
import tv.threess.threeready.player.result.EventResult;
import tv.threess.threeready.player.result.Result;

/**
 * Manager of {@link PlaybackPlugin}s: holds references to registered plugins and dispatches playback events to them.
 *
 * @author Dan Titiu
 * @since 28/02/2017
 */
class PluginManager implements IPluginManager {

    public PluginManager() {
        this.plugins = new CopyOnWriteArrayList<>();
    }
    
    public void registerPlugin(final PlaybackPlugin plugin) {
        unregisterPlugin(plugin);
        this.plugins.add(plugin);
    }


    public void unregisterPlugin(final PlaybackPlugin plugin) {
        this.plugins.removeIf(p -> p == plugin);
    }

    //
    // Lifecycleable
    //

    @Override
    public void onCreate() {
        for (PlaybackPlugin plugin : this.plugins) {
            plugin.onCreate();
        }
    }

    @Override
    public void onDestroy() {
        for (PlaybackPlugin plugin : this.plugins) {
            plugin.onDestroy();
        }
    }

    //
    // PlaybackPlugin
    //

    @Override
    public void onOfferCommand(PlaybackCommand command) throws PlaybackError {
        for (PlaybackPlugin plugin : this.plugins) {
            if (plugin.isSubscribed(command.getDomain())) {
                plugin.onOfferCommand(command);
            }
        }
    }

    @Override
    public void onBeforeExecute(PlaybackCommand command, PlaybackEntireState state, PlaybackDetailsBuilder details)
            throws PlaybackError {

        for (PlaybackPlugin plugin : this.plugins) {
            if (plugin.isSubscribed(command.getDomain())) {
                plugin.onBeforeExecute(command, state, details);
            }
        }
    }

    @Override
    public void onAfterExecute(PlaybackCommand command, PlaybackEntireState state, PlaybackDetailsBuilder details)
            throws PlaybackError {

        for (PlaybackPlugin plugin : this.plugins) {
            if (plugin.isSubscribed(command.getDomain())) {
                plugin.onAfterExecute(command, state, details);
            }
        }
    }

    @Override
    public void onResult(PlaybackCommand command, Result result) {
        for (PlaybackPlugin plugin : this.plugins) {
            if (plugin.isSubscribed(command.getDomain())) {
                plugin.onResult(command, result);
            }
        }
    }

    @Override
    public void onEvent(EventResult result, Bundle details) {
        for (PlaybackPlugin plugin : this.plugins) {
            if (plugin.isSubscribed(result.domain)) {
                plugin.onEvent(result, details);
            }
        }
    }
    
    //
    // PRIVATE IMPLEMENTATION
    //

    /**
     * List of registered plugins.
     */
    private final List<PlaybackPlugin> plugins;// package-private for inner-class optimization
}
