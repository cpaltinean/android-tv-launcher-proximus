package tv.threess.threeready.player.control;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.PlayerConfig;

/**
 * Control the playlist extension.
 *   - explicitly disabled playlist extensions.
 *   - disable extension when there is already an extension request
 *   - disable extension when the maximum number of retries reached.
 *
 * @author Barabas Attila
 * @since 8/10/22
 */
public class PlaylistLock {
    private final int maxRetryCount = Components.get(PlayerConfig.class)
            .getMaxPlaylistExtensionRetries();

    private int mRemainingRetries = maxRetryCount + 1;
    private boolean mRequested = false;
    private boolean mDisabled = true;

    /**
     * @return True if playlist extensions are enabled.
     */
    public synchronized boolean isEnabled() {
        return !mRequested && !mDisabled
                && mRemainingRetries > 0;
    }

    /**
     * Should be called when a playlist extension request failed.
     * The remaining retries will be reduced.
     */
    public synchronized void onRequestFailed() {
        mRequested = false;
        mRemainingRetries--;
    }

    /**
     * Should be called when a playlist extension request was successful.
     * The the remaining retries will be reset.
     *
     */
    public synchronized void onRequestSuccessful() {
        mRequested = false;
        mRemainingRetries = maxRetryCount + 1;
    }

    /**
     * Should be called when a new playlist extension was made.
     * Will reset after the request is executed.
     */
    public synchronized void setRequested() {
        mRequested = true;
    }

    /**
     * Reset the remaining retries and lock states to default.
     */
    public synchronized void reset() {
        mRequested = false;
        mDisabled = false;
        mRemainingRetries = maxRetryCount + 1;
    }

    /**
     * Disable the playlist extensions.
     */
    public synchronized void disable() {
        mDisabled = true;
    }
}
