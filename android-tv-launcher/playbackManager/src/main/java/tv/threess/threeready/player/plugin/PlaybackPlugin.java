/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.plugin;

import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.command.base.PlaybackCommand;

/**
 * Defines the interface of all playback plugins. Plugins listen/receive relevant Playback events and can interfere by
 * interrupting the flow at given step. The events received by the plugins are basically the steps of the
 * {@link PlaybackCommand} lifecycle.
 *
 * @author Dan Titiu
 * @since 20.10.2014
 */
public interface PlaybackPlugin extends IPluginManager {

    /**
     * Get all the domains that we want to subscribe to.
     * If null, then subscribe to all domains
     * @return
     * @param domain
     */
    default boolean isSubscribed(PlaybackDomain domain) {
        return true;
    }
}
