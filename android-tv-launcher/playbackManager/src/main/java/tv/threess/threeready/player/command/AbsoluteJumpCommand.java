package tv.threess.threeready.player.command;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.player.command.base.JumpCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.model.PlaybackEntireState;

/**
 * Implicit command that makes an absolute jump.
 *
 * @author David Bondor
 * @since 2020.02.25
 */
public class AbsoluteJumpCommand extends JumpCommand {

    private final long mPosition;

    public AbsoluteJumpCommand(long id, StartAction startAction, long position) {
        super(id, startAction, null, PlaybackAction.AbsoluteJump, PlaybackState.Started);
        mPosition = position;
    }

    @Override
    protected void executeImplicit() throws Exception {
        control.jump(id, mPosition);
    }

    @Override
    public boolean equals(Object o) {
        if (super.equals(o) && o instanceof AbsoluteJumpCommand) {
            AbsoluteJumpCommand other = (AbsoluteJumpCommand) o;
            return other.mPosition == mPosition;
        }

        return false;
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        // Jumping is valid on the same domain, while not Stopped
        return control.getCurrentState().active;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.replace(sb.length() - 1, sb.length(), ",mPosition[");
        sb.append(Log.timestamp(mPosition));
        sb.append("]}");
        return sb.toString();
    }

    public long getPosition() {
        return mPosition;
    }
}
