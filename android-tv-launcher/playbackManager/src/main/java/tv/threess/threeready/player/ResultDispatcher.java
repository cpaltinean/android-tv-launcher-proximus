/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import tv.threess.threeready.player.result.Result;

/**
 * Posts (queues) a playback command result on the result callback dispatcher to the playback interface.
 *
 * @author Dan Titiu
 * @since 21.07.2014
 */
public interface ResultDispatcher {

    /**
     * Posts (queues) a playback command result on the result callback dispatcher to the playback interface.
     * The exposure of this functionality is needed also to shortcut delayed commands from {@link PlaybackQueue} that
     * should take minimal effort.
     *
     * @param cr    CommandResult that will dispatch a callback to the {@link IPlaybackInterface}.
     */
    void postCommandResult(Result cr);
}
