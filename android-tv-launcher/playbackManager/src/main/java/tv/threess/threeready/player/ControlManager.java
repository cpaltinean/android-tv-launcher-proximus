/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import android.content.Context;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.ControlFactory;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.model.PlaybackStatus;

/**
 * Manager of PlaybackControls: makes sure they are fell fed and listened to.
 *
 * @author Dan Titiu
 * @since 30.04.2014
 */
public class ControlManager implements ControlProvider, Lifecycleable {

    private final ControlFactory mControlFactory;
    private final FeedbackConsumer mFeedbackConsumer;
    private final WeakReference<Context> mContext;

    /**
     * Default constructor.
     *
     * @param context             The Android context.
     * @param factory             The factory for PlaybackControls.
     * @param feedbackConsumer    Reference to the component that consumes feedback from controls.
     */
    ControlManager(final WeakReference<Context> context, final ControlFactory factory,
                   final FeedbackConsumer feedbackConsumer) {
        mContext = context;
        mControlFactory = factory;
        mFeedbackConsumer = feedbackConsumer;
        controls = new EnumMap<>(PlaybackDomain.class);
        state = new PlaybackEntireState();
    }

    /**
     * @return Currently active exclusive domain or null if there is no active domain.
     */
    @Nullable
    @Override
    public PlaybackDomain getActiveExclusiveDomain() {
        for (Map.Entry<PlaybackDomain, PlaybackControl<?>> e : controls.entrySet()) {
            final PlaybackControl<?> control = e.getValue();
            final PlaybackState state = control.getCurrentState();
            if (state.active) {
               return control.getDomain();
            }
        }

        return null;
    }

    @Override
    public PlaybackControl<?> getControl(final PlaybackDomain domain) {
        return controls.get(domain);
    }

    @Override
    public List<PlaybackStatus> getStatus() {
        final List<PlaybackStatus> stats = new ArrayList<>(2);
        for (Map.Entry<PlaybackDomain, PlaybackControl<?>> e : this.controls.entrySet()) {
            final PlaybackControl<?> control = e.getValue();
            final PlaybackState state = control.getCurrentState();
            if (state.active) {
                stats.add(control.getStatus());
            }
        }

        return stats;
    }

    @Override
    public PlaybackDetailsBuilder getExclusiveDetails() {
        PlaybackControl<?> control = getActiveControl();
        if (control == null) {
            return new PlaybackDetailsBuilder()
                    .setState(PlaybackState.Stopped);
        }
        return control.getDetails();
    }

    public PlaybackControl<?> getActiveControl() {
        for (Map.Entry<PlaybackDomain, PlaybackControl<?>> e : this.controls.entrySet()) {
            if (e.getKey().exclusive) {
                final PlaybackControl<?> control = e.getValue();
                if (control.getCurrentState().active) {
                    return control;
                }
            }
        }
        return null;
    }

    /**
     * @param type The type for which the playback details should be returned.
     *
     * @return The filled in builder of the details.
     */
    @Override
    public PlaybackDetailsBuilder getDetails(PlaybackType type) {
        final PlaybackControl<?> control = getControl(type.getDomain());
        if (control != null) {
            return control.getDetails().setType(type);
        }

        return new PlaybackDetailsBuilder().setType(type);
    }

    @Override
    public Map<PlaybackDomain, PlaybackDetailsBuilder> getDetails() {
        final Map<PlaybackDomain, PlaybackDetailsBuilder> stats = new EnumMap<>(PlaybackDomain.class);
        for (Map.Entry<PlaybackDomain, PlaybackControl<?>> e : this.controls.entrySet()) {
            final PlaybackControl<?> control = e.getValue();
            if (control.getCurrentState().active) {
                stats.put(e.getKey(), control.getDetails());
            }
        }

        return stats;
    }

    /**
     * Populates (refreshes) and returns the reference to the singleton instance of the entire state of PlaybackControls.
     * !Please do NOT modify this object outside this method.
     *
     * @return the reference to the singleton instance of the entire state of PlaybackControls.
     */
    @NonNull
    @Override
    public PlaybackEntireState getPlaybackEntireState() {
        // Cleanup state for fresh build
        this.state.recycle();

        PlaybackDomain domain;
        PlaybackState state;
        for (Map.Entry<PlaybackDomain, PlaybackControl<?>> e : this.controls.entrySet()) {
            domain = e.getKey();
            state = e.getValue().getCurrentState();
            // Only active domains count (both exclusive and inclusive).
            if (state.active) {
                final PlaybackStatus status = e.getValue().getStatus();
                if (domain.exclusive) {
                    // Only one exclusive domain allowed at a time, otherwise here they will overwrite each other
                    this.state.exclusiveState = state;
                    this.state.exclusiveType = status.getType();
                } else {
                    this.state.inclusive.put(status.getType(), state);
                }
            }
        }

        return this.state;
    }

    //
    // Lifecycleable
    //

    @Override
    public void onCreate() {
        for (PlaybackDomain domain : PlaybackDomain.values()) {
            final PlaybackControl<?> control = mControlFactory.createControl(domain, mContext);
            if (control != null) {
                control.registerFeedbackConsumer(mFeedbackConsumer);
                controls.put(domain, control);
            } else {
                Log.w(TAG, "No PlaybackControl defined for domain [" + domain + "]");
            }
        }

        for (PlaybackControl<?> pc : this.controls.values()) {
            pc.onCreate();
        }
    }

    @Override
    public void onDestroy() {
        for (PlaybackControl<?> pc : this.controls.values()) {
            pc.onDestroy();
        }
        controls.clear();
    }

    //
    // PRIVATE IMPLEMENTATION
    //

    static final String TAG = Log.tag(ControlManager.class);

    /**
     * Playback controls mapped by domain to which commands are dispatched.
     */
    private final Map<PlaybackDomain, PlaybackControl<?>> controls;

    @NonNull
    private final PlaybackEntireState state;
}
