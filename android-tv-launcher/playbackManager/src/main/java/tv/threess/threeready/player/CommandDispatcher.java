/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.crashlytics.FirebaseCrashlytics;

import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.exception.SessionOpenException;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.data.generic.BackendIsolationException;
import tv.threess.threeready.player.command.ClearCommand;
import tv.threess.threeready.player.command.InactivateCommand;
import tv.threess.threeready.player.command.ReleaseCommand;
import tv.threess.threeready.player.command.ResumeCommand;
import tv.threess.threeready.player.command.StopCommand;
import tv.threess.threeready.player.command.base.EventCommand;
import tv.threess.threeready.player.command.base.ImplicitCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.contract.CommandValidity;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.exception.PlaybackSessionError;
import tv.threess.threeready.player.exception.PlaybackTimeoutError;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.plugin.IPluginManager;
import tv.threess.threeready.player.result.EventResult;
import tv.threess.threeready.player.result.ExactFailure;
import tv.threess.threeready.player.result.Failure;
import tv.threess.threeready.player.result.Result;

/**
 * Central component in the playback management, dispatches commands by validating, injecting, executing, matching and
 * applying PlaybackCommands and integrating the plugins in the process.
 *
 * @author Dan Titiu
 * @since 24.04.2014
 */
class CommandDispatcher implements Lifecycleable, PlaybackDispatcher {

    /**
     * Constructor used only by manager.
     *
     * @param commandQ         Queue interface to provider of playback commands.
     * @param resultQ          Queue interface to provider of command results.
     * @param controller       The provider of the playback controls.
     * @param resultDispatcher Result (callback) dispatcher.
     * @param plugin           The dispatcher of plugin events.
     */
    CommandDispatcher(final QueueProvider<PlaybackCommand> commandQ, final QueueProvider<Result> resultQ,
                      final WeakReference<ControlProvider> controller,
                      final WeakReference<ResultDispatcher> resultDispatcher, @NonNull final IPluginManager plugin) {
        this.commandQ = commandQ;
        this.resultQ = resultQ;
        this.controller = controller;
        this.plugin = plugin;
        this.resultDispatcher = resultDispatcher;
    }

    @Override
    public void onCreate() {
        this.runnable = new DispatchCommandRunnable();

        // Looper on the dedicated thread for executing playback commands.
        this.executeHandler = this.createExecuteHandler();

        // Looper on the dedicated thread for dispatching playback commands to specialized controls.
        final HandlerThread htDispatch = new HandlerThread(":dispatchC");
        htDispatch.start();
        this.dispatchHandler = new Handler(htDispatch.getLooper());

        // Start taking commands and dispatching them.
        this.dispatchHandler.post(this.runnable);

        Log.d(TAG, "CREATED");
    }

    @Override
    public void onDestroy() {
        // Cancel the dispatching
        this.stopDispatching();

        // Stop the dedicated dispatching thread
        Looper l = this.dispatchHandler.getLooper();
        l.quit();
        // Make sure the thread gets out of suspended state (stops waiting for queues)
        l.getThread().interrupt();
        this.dispatchHandler = null;

        // Stop the dedicated execution thread
        l = this.executeHandler.getLooper();
        l.quit();
        // Make sure the thread gets out of suspended state (stops waiting for queues)
        l.getThread().interrupt();
        this.executeHandler = null;

        // Cleanup references from upper level to avoid circularity
        this.lastCommand = null;
        this.lastFailedCommand = null;
        this.dispatchingCommand = null;

        Log.d(TAG, "DESTROYED");
    }

    /**
     * Checks a {@link tv.threess.threeready.player.contract.PlaybackEvent} result against the current
     * dispatching command (if any) for validity. Some PlaybackCommands, while executing, may discard background events
     * which no longer apply after the command is successfully executed. E.g. while executing Stop on a PlaybackDomain,
     * the same PlaybackDomain emits an EndReached event, which is better off NOT to be dispatched to the interface.
     *
     * @param eventResult The {@link EventResult} to be validated.
     * @return true when the event should be discarded.
     */
    public boolean discardEvent(EventResult eventResult) {
        PlaybackCommand cmd = this.dispatchingCommand;
        return cmd != null && cmd.discards(eventResult);
    }

    @Override
    public StartCommand getLastCommand() {
        return this.lastCommand;
    }

    public StartCommand getLastFailedCommand() {
        return this.lastFailedCommand;
    }

    //
    // PRIVATE IMPLEMENTATION
    //

    static final String TAG = Log.tag(CommandDispatcher.class);// package-private for inner-class optimization

    /**
     * Handler on separate thread that executes commands and offers support for execution timeout.
     */
    private static final class ExecuteHandler extends Handler {

        public ExecuteHandler(Looper looper) {
            super(looper);
            this.lock = new ReentrantLock();
            this.done = lock.newCondition();
        }

        @Override
        public void handleMessage(Message msg) {
            final PlaybackCommand cmd = (PlaybackCommand) msg.obj;
            try {
                // All commands need to catch their exceptions and report them as failures
                cmd.execute();
            }  catch (Exception e) {
                error.set(e);
            } finally {
                // Allow this thread to be interrupted, especially when the execution hangs.
                try {
                    this.lock.lockInterruptibly();
                } catch (InterruptedException e) {
                    /*Carry on when interrupted, might be that the Looper quit*/
                }
                try {
                    // Safe to write volatile from multiple thread as long as in a critical (synchronized) block
                    this.executing = false;
                    this.done.signal();
                } finally {
                    this.lock.unlock();
                }
            }
        }

        /**
         * Executes command synchronously, awaiting the execution for a limited amount of time (timeout).
         *
         * @param cmd  The {@link PlaybackCommand} to be executed.
         * @param time Amount of time to limit the waiting to.
         * @param unit {@link TimeUnit} for the amount of time.
         * @return the remaining timeout after waiting for execution in the same unit given as argument.
         * @throws InterruptedException     in case the thread gets interrupted.
         * @throws PlaybackTimeoutError in case the execution takes longer than expected.
         */
        long executeCommand(final PlaybackCommand cmd, long time, TimeUnit unit) throws Exception {
            // Lock before posting the message to ensure correct precedence; otherwise we might end up in a deadlock
            // when the execution signals the finish before we get the chance to lock and await (highly improbable).
            this.lock.lockInterruptibly();
            try {
                // Safe to write volatile from multiple thread as long as in a critical (synchronized) block
                this.executing = true;
                // Clear out previous errors
                error.set(null);
                long timeout = unit.toNanos(time);

                final Message msg = this.obtainMessage();
                msg.obj = cmd;
                this.sendMessage(msg);

                // Wait for the command execution
                while (this.executing && timeout > 0L) {
                    timeout = this.done.awaitNanos(timeout);
                }

                Exception e = error.get();
                if (e != null) {
                    throw e;
                }

                // Raise exception in case of timeout
                if (this.executing) {
                    throw new PlaybackTimeoutError(cmd.getDomain());
                }


                return unit.convert(timeout, TimeUnit.NANOSECONDS);
            } finally {
                this.lock.unlock();
            }
        }

        /**
         * Lock for synchronising the execution with the waiting (dispatcher) thread.
         */
        private final Lock lock;

        /**
         * Condition for signalling the waiting (dispatcher) thread upon finished execution.
         */
        private final Condition done;

        /**
         * Flag indicating that the execution took place, needed to be checked in case of spurious interrupts while
         * waiting on the condition.
         */
        private volatile boolean executing = false;

        private final AtomicReference<Exception> error = new AtomicReference<>();
    }

    /**
     * Dummy runnable that runs until cancelled. Useful to delegate the work to a dedicated thread.
     */
    private final class DispatchCommandRunnable implements Runnable {

        private volatile boolean run = true;

        DispatchCommandRunnable() {
        }// package-private for inner-class optimization

        @Override
        public void run() {
            Log.d(TAG, "Started dispatching from queue.");
            while (this.run) {
                CommandDispatcher.this.dispatchNextCommand();
            }
            Log.d(TAG, "Stopped dispatching from queue.");
        }

        public void cancel() {
            this.run = false;
        }
    }

    /**
     * Cancels the runnable to not take and dispatch any command from the queue.
     */
    public void stopDispatching() {
        this.runnable.cancel();
    }

    /**
     * Blocking queue that provides playback commands. The take is expected to block when queue is empty until a command
     * is available.
     */
    private final QueueProvider<PlaybackCommand> commandQ;

    /**
     * Blocking queue that provides results to dispatched commands. Each command needs to be paired with a result.
     */
    private final QueueProvider<Result> resultQ;

    /**
     * The provider of the playback controls.
     */
    private final WeakReference<ControlProvider> controller;

    /**
     * Dispatcher of results to {@code IPlaybackInterface} callback.
     */
    private final WeakReference<ResultDispatcher> resultDispatcher;

    /**
     * The single internal plugin that dispatches calls to all registered PlaybackPlugins.
     */
    @NonNull
    private final IPluginManager plugin;

    /**
     * Handler on the dedicated worker thread for dispatching commands.
     */
    private Handler dispatchHandler;

    /**
     * Instance of cancellable dummy runnable.
     */
    private DispatchCommandRunnable runnable;

    /**
     * The {@link PlaybackCommand} being currently dispatched.
     */
    private volatile PlaybackCommand dispatchingCommand;

    /**
     * The last/latest {@link PlaybackCommand} that was executed. This may be used as last resort for preparing
     * ImplicitCommand after the last one from {@link PlaybackQueue} was canceled.
     */
    private volatile StartCommand lastCommand;

    private volatile StartCommand lastFailedCommand;

    /**
     * Handler on the dedicated worker thread for executing commands.
     */
    private ExecuteHandler executeHandler;

    /**
     * Take the next playback command and dispatch it to the appropriate control going through all dispatching steps:
     * dequeue, validate, pre-execute, execute, match, pair, apply, decorate, post-execute.
     */
    void dispatchNextCommand() {
        try {
            // Take the next command, wait until one is available.
            PlaybackCommand cmd = this.commandQ.take();
            Log.d(TAG, "Took from command Q: " + cmd);
            this.dispatchingCommand = cmd;
            this.dispatchCommand(cmd);
        } catch (InterruptedException e) {
            Log.w(TAG, "Interrupted while waiting for next PlaybackCommand");
        } finally {
            this.dispatchingCommand = null;
        }
    }

    private ExecuteHandler createExecuteHandler() {
        // Looper on the dedicated thread for executing playback commands.
        final HandlerThread htExecute = new HandlerThread(":executeC");
        htExecute.start();
        return new ExecuteHandler(htExecute.getLooper());
    }

    /**
     * Take the given playback command and dispatch it to the appropriate control going through all dispatching steps:
     * validate, pre-execute, execute, match, pair, apply, decorate, post-execute.
     *
     * @param cmd The {@code PlaybackCommand} to be dispatched.
     */
    private void dispatchCommand(final PlaybackCommand cmd) {
        final ControlProvider cp = this.controller.get();
        Log.i(TAG, "dispatchCommand: " + cmd);
        // Prepare validation input; Use local variable for less code and lookup optimization
        PlaybackEntireState ds = cp.getPlaybackEntireState();

        Result cr = null;
        try {
            cmd.assertNotCancelled();

            boolean inTransaction = ds.equalsExclusiveDomain(cmd.getDomain());
            boolean needsTransition = false;
            // Validate
            final CommandValidity v = cmd.validate(ds);
            switch (v) {
                case Invalid:
                    throw new IllegalArgumentException("Invalid command [" + cmd + "] for state [" + ds + "]");

                case TransitionalStopped:
                case TransitionalInactive:
                case TransitionalPlaying:
                    needsTransition = true;
                    // Intentional fall-through
                case Valid:
                    break;

                default:
                    // Just in case we forget the names of our own children
                    throw new UnsupportedOperationException("Unknown command validity [" + v + "]");
            }
            // Inject + Verify
            if (needsTransition) {
                if (ds.exclusiveType != null) {
                    // Inject transition as required.
                    // Commands for inclusive domains (Pip) should not use the Transitional* validity as the injected
                    // command will stop the exclusive domain.
                    final PlaybackCommand inj = this.createSyntheticCommand(ds.exclusiveType, v.targetState, inTransaction, cmd);
                    Log.d(TAG, "Injecting: " + inj);
                    this.dispatchCommand(inj);

                    // Prepare verification input
                    ds = cp.getPlaybackEntireState();

                    if (ds.exclusiveType == null && ds.exclusiveState.active) {
                        // Corrupted state. Stop each domain.
                        onCorruptedState(cp, ds);
                        ds = cp.getPlaybackEntireState();
                    }

                    // Verify: the result of the injected command is checked during verification
                    if (!cmd.verify(ds)) {
                        throw new IllegalArgumentException("Invalid command [" + cmd + "] for state [" + ds + "]");
                    }
                } else {
                    // Corrupted state. Stop each domain.
                    onCorruptedState(cp, ds);
                }
            }

            cmd.assertNotCancelled();

            //TODO: Optimise detail retrieval
            this.plugin.onBeforeExecute(cmd, ds, cp.getControl(cmd.getDomain()).getDetails());

            // Before dispatching a new command purge all existing results (accept no results before the execution)
            final Collection<Result> ors = this.resultQ.purge();
            if (ors.size() > 0) {
                Log.d(TAG, "Purged [" + ors.size() + "] obsolete results before execution");
            }

            // Execute command with timeout support in case the call hangs.
            long timeout = cmd.getTimeout();
            timeout = this.executeHandler.executeCommand(cmd, timeout, TimeUnit.MILLISECONDS);
            Log.d(TAG, "Command consumed: " + cmd);

            this.plugin.onAfterExecute(cmd, ds, cp.getControl(cmd.getDomain()).getDetails());

            // Wait for result from resultQ that matches the command.
            do {
                Log.d(TAG, "Waiting for next result..");
                long nanos = System.nanoTime();
                cr = this.resultQ.poll(timeout, TimeUnit.MILLISECONDS);
                if (cr == null) {
                    cmd.assertNotCancelled();
                    throw new PlaybackTimeoutError(cmd.getDomain());
                }
                Log.d(TAG, "Result received, checking..");
                // Subtract taken time from timeout on every loop
                timeout -= TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - nanos);
            } while (!cmd.isFulfilledBy(cr));
            Log.d(TAG, "Result matched!");

            cmd.assertNotCancelled();
        } catch (InterruptedException | CancellationException e) {
            Log.e(TAG, "Failed to dispatch PlaybackCommand " + cmd, e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "Failed to dispatch PlaybackCommand " + cmd, e);
            // A command was surely issued, create a failure result
            cr = new ExactFailure(cmd.getId(), new PlaybackError(PlaybackError.Type.INVALID, e));
        } catch (PlaybackError e) {
            Log.e(TAG, "Failed to dispatch PlaybackCommand " + cmd, e);
            // A command was surely issued, create a failure result
            cr = new ExactFailure(cmd.getId(), e);
        } catch (BackendIsolationException e) {
            Log.e(TAG, "Failed to dispatch PlaybackCommand " + cmd, e);
            cr = new ExactFailure(cmd.getId(), new PlaybackError(PlaybackError.Type.BACKEND_ISOLATED, e));
        } catch (SessionOpenException e) {
            Log.e(TAG, "Failed to dispatch PlaybackCommand " + cmd, e);
            // A command was surely issued, create a failure result
            cr = new ExactFailure(cmd.getId(), new PlaybackSessionError(e));
        } catch (Exception e) {
            Log.e(TAG, "Failed to dispatch PlaybackCommand " + cmd, e);
            // If a command was issued, create a failure result
            if (cmd != null) {
                cr = new ExactFailure(cmd.getId(), new PlaybackError(PlaybackError.Type.GENERIC, e));
            }
        } finally {
            this.completeCommandResult(cmd, cr);
        }
    }

    // TODO : Corrupted playback manager state.
    //  There is an active control without a playback type. Try to reset all the controls.
    //  Remove this workaround once we are sure that all the corruption cases are prevented.
    private void onCorruptedState(ControlProvider cp, PlaybackEntireState ds) {
        // Report corrupted state to firebase for tracking.
        PlaybackDomain activeDomain = cp.getActiveExclusiveDomain();
        IllegalStateException illegalStateException = new IllegalStateException(
                "Corrupted playback state. State [" + ds +"], active domain [" + activeDomain +"]");
        Log.e(TAG, "Corrupted playback state.", illegalStateException);
        FirebaseCrashlytics.getInstance().recordException(illegalStateException);

        // Reset state. Release all the controls.
        for (PlaybackDomain domain : PlaybackDomain.values()) {
            PlaybackControl<?> control = cp.getControl(domain);
            if (control != null) {
                try {
                    long commandId = PlaybackCommand.syntheticIdCounter.getNextId();
                    control.stop(commandId);
                    control.applyState(PlaybackState.Released);
                    control.applyDetails(null);
                    control.reset();
                } catch (Exception e) {
                    Log.e(TAG, "Failed to reset  control state.", e);
                }
            }
        }
    }


    /**
     * Perform the final processing of the PlaybackCommand after matching a CommandResult.
     *
     * @param cmd The {@code PlaybackCommand} that was recently dispatched.
     * @param cr  The {@code CommandResult} obtained after dispatching.
     */
    private void completeCommandResult(final PlaybackCommand cmd, Result cr) {
        // Eliminate null parameters
        if (cmd == null || cr == null) {
            return;
        }

        // Pair with command for dispatching callback
        cr.pairWith(cmd);
        // Apply the state change onto the control
        try {
            cr.apply(cmd, getNextCmdId());
        } catch (Exception e) {
            Log.e(TAG, "Could not apply command result for command [" + cmd + "] with result [" + cr + "]", e);
        }

        // If there is a result and the result handler is still alive dispatch the result to callbacks.
        final ResultDispatcher rd = this.resultDispatcher.get();
        if (rd != null) {
            // EventCommands trigger an EventResult instead of CommandResult
            final boolean isEvent = cmd instanceof EventCommand;
            if (isEvent) {
                cr = ((EventCommand) cmd).getEventResult(cr);
            }

            // Complete by decorating the result with information relevant for the result consumer and plugins.
            try {
                final ControlProvider cp = this.controller.get();
                PlaybackDetailsBuilder det;
                if (cmd.getDomain().exclusive) {
                    det = cp.getExclusiveDetails();//TODO: This could have state Released instead of Stopped/Inactive
                } else {
                    det = cp.getControl(cmd.getDomain()).getDetails();
                }
                // In case no domain is active, use the dispatched PlaybackCommand as fallback
                if (!det.getState().active) {
                    cmd.decorate(det);
                }

                cr.complete(det.build());
            } catch (Exception e) {
                Log.e(TAG, "Failed to complete CommandResult", e);
            }

            // Synthetic commands, except for EventCommands are not dispatched to callbacks
            if (isEvent || !cmd.isSynthetic()) {
                // Post the result to the result dispatcher handler
                rd.postCommandResult(cr);
            }
        }

        // Save reference to the last successful user StartCommand in order to be used as fallback for preparing ImplicitCommands.
        // Synthetic (injected) commands and those generated by events do not count as User commands.
        // Also only exclusive domains count as only they can have implicit commands that need preparation.
        if (cmd.getDomain().exclusive && !cmd.isSynthetic() && cmd instanceof StartCommand) {
            if (cr.isFavorable()) {
                this.lastCommand = (StartCommand) cmd;
                this.lastFailedCommand = null;
            } else {
                this.lastFailedCommand = (StartCommand) cmd;
            }
            // If we have a result then clear the dispatching command
            dispatchingCommand = null;
        }

        try {
            this.plugin.onResult(cmd, cr);
        } catch (Exception e) {
            Log.e(TAG, "Error while dispatching result to plugins", e);
        }

        cancelNextStartFallbackCommands(cmd, cr);
    }

    /**
     * Create (synthesize) a command that transitions the specified PlaybackDomain to the required PlaybackState.
     *
     * @param type        Current {@code StreamType} for which to make the transition.
     * @param targetState Target {@code PlaybackState} to transition to.
     * @return The new {@code PlaybackCommand}.
     */
    @NonNull
    private PlaybackCommand createSyntheticCommand(final PlaybackType type, final PlaybackState targetState, boolean inTransaction, PlaybackCommand trigger) {
        final ControlProvider cp = this.controller.get();
        if (cp == null) {
            // Routine check
            throw new IllegalStateException("Cannot create synthetic command: missing ControlProvider");
        }

        final PlaybackControl<?> control = cp.getControl(type.getDomain());
        final ImplicitCommand cmd;
        switch (targetState) {
            case Released:
                cmd = new ReleaseCommand(PlaybackCommand.syntheticIdCounter.getNextId());
                cmd.prepare(type, control);
                return cmd;

            case Stopped:
                if (!inTransaction) {
                    cmd = new StopCommand(PlaybackCommand.syntheticIdCounter.getNextId(), trigger);
                } else {
                    cmd = new ClearCommand(PlaybackCommand.syntheticIdCounter.getNextId(), trigger);
                }
                cmd.prepare(type, control);

                return cmd;
            case Inactive:
                cmd = new InactivateCommand(PlaybackCommand.syntheticIdCounter.getNextId());
                cmd.prepare(type, control);
                return cmd;

            case Started:
                cmd = new ResumeCommand(PlaybackCommand.syntheticIdCounter.getNextId());
                cmd.prepare(type, control);
                return cmd;

            default:
                throw new IllegalArgumentException("Don't know how to synthesise command for type [" + type + "] and target state [" + targetState + "]");
        }
    }

    private void cancelNextStartFallbackCommands(PlaybackCommand currentCommand, Result commandRes) {
        try {
            if (commandRes instanceof Failure || !(currentCommand instanceof StartCommand))
                return;
            commandQ.remove(currentCommand.getId());
        } catch (Exception ignore) {
            // ignored: probably nothing left in queue
        }
    }

    /**
     * Retrieves the next cmd id without remove it
     *
     */
    @Nullable
    public Long getNextCmdId() {
        Long nextCmdId = null;
        try {
            PlaybackCommand nextCmd = commandQ.peek();
            nextCmdId = nextCmd == null ? null : nextCmd.getId();
        } catch (Exception e) {
            Log.e(TAG, "Failed to get next command id.", e);
        }
        return nextCmdId;
    }

    @Nullable
    public PlaybackCommand getDispatchingCommand() {
        PlaybackCommand cmd = dispatchingCommand;
        if (cmd != null && cmd.getProcessingTime() == 0) {
            // the command did not finish executing yet.
            return cmd;
        }
        return null;
    }
}
