package tv.threess.threeready.player.command.start;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.GdprControlProximus;
import tv.threess.threeready.player.control.base.PlaybackControl;

/**
 * Command to start a GDPR video.
 *
 * @author Zsolt Bokor_
 * @since 2020.11.17
 */
public class GdprStartCommand extends ContentItemStartCommand<IVodVariant> {

    private final GdprControlProximus mControl;
    private final IVodItem mVod;
    private final IVodPrice mVodPrice;
    private final IVodVariant mVodVariant;

    public GdprStartCommand(long id, PlaybackType type, PlaybackControl<?> control,
                            StartAction startAction, IVodItem vod, IVodPrice vodPrice, IVodVariant vodVariant) {
        super(id, type, startAction, control);
        mControl = validateControl(control, GdprControlProximus.class);
        mVod = vod;
        mVodVariant = vodVariant;
        this.mVodPrice = vodPrice;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        mControl.start(this);
    }

    public IVodItem getVodItem() {
        return mVod;
    }

    public IVodPrice getVodPrice() {
        return mVodPrice;
    }

    @Override
    public IVodVariant getContentItem() {
        return mVodVariant;
    }

    @Override
    public GdprStartCommand createReTuneCommand(StartAction action, long cid) {
        return new GdprStartCommand(cid, type, control, action, mVod, mVodPrice, mVodVariant);
    }

    @Override
    public Comparable<?> playbackId() {
        IVodVariant vodVariant = mVodVariant;
        if (vodVariant == null) {
            return null;
        }
        return vodVariant.getId();
    }


}
