/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import java.lang.ref.WeakReference;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.player.result.Result;

/**
 * Simple dispatcher using a dedicated thread that delivers the feedback/result for each command back to the consumer
 * interface.
 *
 * @author Dan Titiu
 * @since 24.04.2014
 */
class CallbackDispatcher implements Lifecycleable, ResultDispatcher {
    static final String TAG = Log.tag(CallbackDispatcher.class);

    /**
     * Constructor used only by manager.
     *
     * @param callback    Callback registered through {@code IPlaybackInterface}.
     */
    CallbackDispatcher(final WeakReference<IPlaybackCallback> callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate() {
        // Looper on the dedicated thread for dispatching playback results to callbacks.
        if (resultHandler == null) {
            final HandlerThread htr = new HandlerThread(":dispatchR");
            htr.start();
            resultHandler = new Handler(htr.getLooper(), new DispatchResultCallback(this.callback));
        }

        Log.d(TAG, "CREATED");
    }

    @Override
    public void onDestroy() {
        // Stop the dedicated thread(s)

        if (resultHandler != null) {
            resultHandler.getLooper().quit();
            resultHandler = null;
        }

        Log.d(TAG, "DESTROYED");
    }

    /**
     * Posts (queues) a playback command result on the result callback dispatcher to the playback interface.
     * The exposure of this functionality is needed to shortcut delayed commands from {@code PlaybackManager} that
     * should not take any playback queue effort. Please do <em>NOT</em> use this method for directly dispatching results.
     *
     * @param cr    CommandResult that will dispatch a callback to the {@code IPlaybackInterface}.
     */
    public void postCommandResult(Result cr) {
        // Check for null in case dispatcher was destroyed
        final Handler h = this.resultHandler;
        if (h != null) {
            final Message msg = h.obtainMessage();
            msg.obj = cr;
            h.sendMessage(msg);
        }
    }

    //
    // PRIVATE IMPLEMENTATION
    //

    /**
     * Handler callback that encapsulates the dispatching of one result/callback on the dedicated thread.
     */
    private static final class DispatchResultCallback implements Handler.Callback {

        private final WeakReference<IPlaybackCallback> callback;

        public DispatchResultCallback(WeakReference<IPlaybackCallback> callback) {
            this.callback = callback;
        }

        @Override
        public boolean handleMessage(Message msg) {
            Result cr = null;
            try {
                cr = (Result) msg.obj;
                Log.d(TAG, "Dispatching " + cr.toString());
                cr.dispatchTo(this.callback.get());
            } catch (Exception e) {
                Log.e(TAG, "Failed to dispatch " + (cr != null ? cr.toString() : "CommandResult"), e);
            }
            return true;
        }
    }

    /**
     * Playback callback to which results are dispatched after matching a command.
     */
    final WeakReference<IPlaybackCallback> callback;

    /**
     * Handler on the dedicated worker thread for dispatching results.
     */
    private Handler resultHandler;
}
