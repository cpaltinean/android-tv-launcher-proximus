/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command.start;

import android.content.Intent;
import android.os.Bundle;

import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.gms.model.AppContentItem;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.AppControl;
import tv.threess.threeready.player.control.base.PlaybackControl;

/**
 * Start Application
 *
 * @author Karetka Mezei Zoltan
 * @since 2018.01.15
 */

public class AppStartCommand extends ContentItemStartCommand {

    private final AppControl mControl;
    private final Intent mIntent;
    private final String mPackageName;
    private final String mActivityName;
    private final Bundle mExtra;
    private final IContentItem mContentItem;

    private AppStartCommand(long id, PlaybackType type, PlaybackControl control, StartAction startAction,
                            Intent intent, String packageName, String activityName, Bundle extra) {
        super(id, type, startAction, control);
        mControl = validateControl(control, AppControl.class);
        mIntent = intent;
        mPackageName = packageName;
        mActivityName = activityName;
        mExtra = extra;
        mContentItem = new AppContentItem(mPackageName);
    }

    public AppStartCommand(long id, PlaybackType type, PlaybackControl control,
                           StartAction startAction, String packageName, String activityName, Bundle extra) {
        this(id, type, control, startAction, null, packageName, activityName, extra);
    }

    public AppStartCommand(long id, PlaybackType type, PlaybackControl control,
                           StartAction startAction, Intent intent, String packageName, Bundle extra ) {
        this(id, type, control, startAction, intent, packageName, null, extra);
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        mControl.start(this);
    }

    public Intent getIntent() {
        return mIntent;
    }

    public String getPackageName() {
        return mPackageName;
    }

    public String getActivityName() {
        return mActivityName;
    }

    public Bundle getExtras() {
        return mExtra;
    }

    @Override
    public String toString() {
        return super.toString("package", mPackageName);
    }

    @Override
    public AppStartCommand createReTuneCommand(StartAction action, long cid) {
        return new AppStartCommand(cid, type, control, action, mIntent, mPackageName, mActivityName, mExtra);
    }

    @Override
    public Comparable<?> playbackId() {
        return mPackageName;
    }

    @Override
    public IContentItem getContentItem() {
        return mContentItem;
    }
}
