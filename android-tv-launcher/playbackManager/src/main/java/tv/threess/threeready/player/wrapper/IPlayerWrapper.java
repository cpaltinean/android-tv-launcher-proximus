/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.wrapper;

import android.media.PlaybackParams;
import android.media.tv.TvTrackInfo;
import android.view.View;

import java.util.List;

import tv.threess.threeready.api.playback.listener.PlayerWrapperListener;
import tv.threess.threeready.player.model.OttPlayListItem;

/**
 * 3rd party Player (e.g ExoPlayer) wrapper interface
 * Used by TvInputServices
 *
 * @author Daniel Gliga
 * @since 2017.11.30
 */

public interface IPlayerWrapper<T>  {

    int VIDEO_UNAVAILABLE_REASON_ENDED = 0x2ff;


    /**
     * Method used for prepend the previous streams to the existing playlist.
     * @param prependParams Playback parameters to extend the playlist with.
     */
    void prepend(List<OttPlayListItem> prependParams);

    /**
     * Method used for append the next stream to the existing playlist.
     * @param appendParams Playback parameters to extend the playlist with.
     */
    void append(List<OttPlayListItem> appendParams);

    void release();

    void setSurface(T surface);

    void onSurfaceChanged(int format, int width, int height);

    void pause();

    void resume();

    void seekTo(long ms);

    /**
     * @return The index of the currently playing item.
     */
    int getCurrentPlaylistIndex();

    /**
     * @return The position in milliseconds in the current playlist item.
     */
    long getCurrentPosition();

    /**
     * @return The duration of the current playlist item in milliseconds.
     */
    long getDuration();

    void setPlayerWrapperListener(PlayerWrapperListener listener);

    void setStreamVolume(float vol);

    void setCaptionEnabled(boolean enabled);

    void setAudioTrack(String trackId);

    void setVideoTrack(String trackId);

    void setSubtitleTrack(String trackId);

    void setPlaybackParams(PlaybackParams params);

    List<TvTrackInfo> getAllTracks();

    String getSelectedTrack(int type);

    void setSpeed(double speed);

    double getSpeed();

    View createOverlayView();

}
