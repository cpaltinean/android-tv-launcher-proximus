/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.exception;

import java.util.Locale;

import tv.threess.threeready.api.log.helper.ErrorType;
import tv.threess.threeready.api.log.model.ErrorDetail;
import tv.threess.threeready.player.result.Failure;
import tv.threess.threeready.player.result.Result;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Thrown when a playback error is encountered.
 *
 * @author Daniel Gliga
 * @since 2018.01.26
 */

public class PlaybackError extends Exception {

    private final Type mReason;

    public PlaybackError(Type reason) {
        super(reason.name());
        mReason = reason;
    }

    public PlaybackError(Type reason, Throwable cause) {
        this(reason);
        initCause(cause);
    }

    public PlaybackError(Type reason, String message) {
        super(message);
        mReason = reason;
    }

    public PlaybackError(Throwable cause) {
        this(Type.UNKNOWN, cause);
    }

    public PlaybackError(String message) {
        this(Type.UNKNOWN, message);
    }

    public Type getReason() {
        return mReason;
    }

    public String getErrorCode() {
        return mReason.getCode();
    }

    public String getBodyTranslationKey() {
        return mReason.bodyTranslationKey();
    }

    public String getTitleTranslationKey() {
        return mReason.titleTranslationKey();
    }

    public enum Type implements ErrorDetail {
        UNKNOWN(0),
        CANCELED(0),
        INVALID(0),
        GENERIC(0),
        DELAYED(0),
        BLACKLISTED(0),
        BUFFER_START_REACHED(0),
        UNSKIPPABLE_REGION(0),

        // Castlabs player related errors
        AUDIO_TRACK_INITIALIZATION(1),
        AUDIO_DECODER_INITIALIZATION(2),
        AUDIO_WRITE_ERROR(3),
        AUDIO_DECRYPTION_ERROR(4),
        VIDEO_DECODER_INITIALIZATION(5),
        VIDEO_DECRYPTION_ERROR(6),
        MANIFEST_LOADING_FAILED(7, true, 1),
        SDK_INIT_ERROR(8),
        NO_RENDERER_FOUND(9),
        NO_PLAYABLE_CONTENT(10),
        PLAYBACK_ERROR(11, true, 0),
        API_MISMATCH(12),
        UNSUPPORTED_DRM(13),
        SDK_NOT_INITIALIZED(14),
        DRM_TODAY_EXCEPTION(15),
        DOWNLOAD_ERROR(16, false, 1),
        SECONDARY_DISPLAY(17),
        KEY_EXPIRED(18),
        DRM_KEY_DOWNLOAD_ERROR(19),
        CONNECTIVITY_LOST_ERROR(20),
        CONNECTIVITY_GAINED_INFO(21),
        BEHIND_LIVE_WINDOW(22),
        INVALID_PLAYER_LICENSE(23),
        DATA_DOWNLOAD_ERROR(24),
        DRM_EXCEPTION(25, false, 1),
        AUDIO_UNSUPPORTED(26),
        VIDEO_UNSUPPORTED(27),
        TEXT_UNSUPPORTED(28),
        USER_ID_NOT_PROVIDED(29),
        MANIFEST_PARSING_FAILED(30),
        AD_REQUEST_UNSUPPORTED(31),
        DRM_PROVISION_ERROR(32),

        // Technicolor player related errors
        ACCESS_DENIED(50),
        ACCESS_DENIED_INIT_ISSUE(51),
        ACCESS_DENIED_NO_SUBSCRIPTION(52),
        ACCESS_DENIED_PIN_REQUIRED(53),
        ACCESS_DENIED_GEO_BLACKOUT(54),
        ACCESS_DENIED_USAGE_RULE_ISSUE(55),
        TUNING_WHILE_SCANNING(56, TranslationKey.ERR_PLAYER_CHANNEL_SCAN_TITLE, TranslationKey.ERR_PLAYER_CHANNEL_SCAN_BODY),

        // Generic errors
        VIDEO_UNAVAILABLE_TIMEOUT_IPTV(130, false, 1),
        SESSION_OPEN_FAILED(131, TranslationKey.ERR_PLAYER_131_TITLE, TranslationKey.ERR_PLAYER_131_BODY),
        DISCONNECTED(132, false, 1),
        NO_PLAYBACK_OPTION(133),
        WEAK_SIGNAL(134),
        VIDEO_UNAVAILABLE_TIMEOUT_OTT(135, false, 1),
        VIDEO_UNAVAILABLE_REASON_ENDED(136),
        BACKEND_ISOLATED(137),
        UNKNOWN_HOST_DNS(138),
        SOCKET_TIMEOUT(139),
        INTERNET_UNAVAILABLE('A', 1, TranslationKey.ERR_NO_INTERNET),
        APP_OPEN_TIMEOUT(140),
        SESSION_OPEN_FAILED_MS(141, TranslationKey.ERR_PLAYER_131_TITLE, TranslationKey.ERR_PLAYER_131_BODY),
        TUNE_IN_BACKGROUND(142);

        private final int mCode;
        private final String mErrorCode;
        private final String mTitleTranslationKey;
        private final String mBodyTranslationKey;
        private final int mAutomaticRetryCount;
        private final boolean mManualRetry;

        Type(int code) {
            this('P', code, false, 0, TranslationKey.ERR_PLAYER_GENERIC_TITLE, TranslationKey.ERR_PLAYER_GENERIC_BODY);
        }

        Type(int code, boolean manualRetry, int automaticRetryCount) {
            this('P', code, manualRetry, automaticRetryCount, TranslationKey.ERR_PLAYER_GENERIC_TITLE, TranslationKey.ERR_PLAYER_GENERIC_BODY);
        }

        Type(char kind, int code, String titleTranslationKey) {
            this(kind, code, false, 0, titleTranslationKey, TranslationKey.ERR_PLAYER_GENERIC_BODY);
        }

        Type(int code, String titleTranslationKey, String bodyTranslationKey) {
            this('P', code, false, 0, titleTranslationKey, bodyTranslationKey);
        }

        Type(char kind, int code, boolean manualRetry, int automaticRetryCount, String titleTranslationKey, String bodyTranslationKey) {
            mCode = code;
            mManualRetry = manualRetry;
            mAutomaticRetryCount = automaticRetryCount;
            mErrorCode = String.format(Locale.ROOT, "%c%03d", kind, code);
            mBodyTranslationKey = bodyTranslationKey;
            mTitleTranslationKey = titleTranslationKey;
        }

        public String getCode() {
            return mErrorCode;
        }

        @Override
        public ErrorType errorType() {
            return null;
        }

        @Override
        public String bodyTranslationKey() {
            return mBodyTranslationKey;
        }

        @Override
        public String titleTranslationKey() {
            return mTitleTranslationKey;
        }

        public int getAutomaticRetryCount() {
            return mAutomaticRetryCount;
        }

        public boolean isManualRetry() {
            return mManualRetry;
        }

        /**
         * Returns true if the given playback result matches this error type.
         */
        public boolean matches(Result result) {
            PlaybackError error = null;
            if (result instanceof Failure) {
                error = ((Failure) result).getError();
            }

            if (error == null) {
                return false;
            }

            return this == error.mReason;
        }

        public static Type getFromErrorCode(int errorCode) {
            for (Type value : values()) {
                if (errorCode == value.mCode) {
                    return value;
                }
            }
            return Type.UNKNOWN;
        }
    }
}
