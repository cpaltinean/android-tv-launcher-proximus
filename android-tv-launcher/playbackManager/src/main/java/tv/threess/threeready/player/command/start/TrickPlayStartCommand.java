package tv.threess.threeready.player.command.start;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.TrickPlayControlProximus;
import tv.threess.threeready.player.control.base.BaseTvViewControl;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackEntireState;

/**
 * Start command for trick play. This command takes the playback url and license url passed though the extra details bundle
 * and puts it into the resolved arguments so the control can read them.
 *
 * @author Andor Lukacs
 * @since 1/21/19
 */
public class TrickPlayStartCommand extends StartCommand {

    private final PlaybackControl<?> mActiveControl;
    private final TrickPlayControlProximus mTrickPlayControl;
    private final float mPlaybackSpeed;

    public TrickPlayStartCommand(long id, StartAction startAction, PlaybackType type, PlaybackControl<?> control,
                                 PlaybackControl<?> activeControl, float playbackSpeed) {
        super(id, type, startAction, control, PlaybackAction.TrickPlay, PlaybackState.TrickPlay);
        mActiveControl = validateControl(activeControl, BaseTvViewControl.class);
        mTrickPlayControl = validateControl(control, TrickPlayControlProximus.class);
        mPlaybackSpeed = playbackSpeed;
    }

    public float getPlaybackSpeed() {
        return mPlaybackSpeed;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        mActiveControl.startTrickPlay(id, mPlaybackSpeed, mTrickPlayControl);
    }

    @Override
    public void commit() {
        super.commit();
        // Update the state of the active control
        if (mActiveControl.getCurrentState() == PlaybackState.Paused
                || mActiveControl.getCurrentState() == PlaybackState.TrickPlayPaused) {
            mActiveControl.applyState(PlaybackState.TrickPlayPaused);
            return;
        }

        // Set the default state
        mActiveControl.applyState(PlaybackState.TrickPlay);
    }

    @Override
    public void rollback(PlaybackError error, @Nullable Long nextCmdId) throws Exception {
        super.rollback(error, nextCmdId);

        // TODO : workaround to apply proper state for control even if the command was canceled.
        commit();
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        return mActiveControl.getCurrentState().active;
    }

    @Override
    public StartCommand createReTuneCommand(StartAction action, long cid) {
        return new TrickPlayStartCommand(cid, action, PlaybackType.TrickPlay, control,
                mActiveControl, mPlaybackSpeed);
    }

    @Override
    public Comparable<?> playbackId() {
        return null;
    }

    public PlaybackDomain getActiveDomain() {
        return mActiveControl.getDomain();
    }
}
