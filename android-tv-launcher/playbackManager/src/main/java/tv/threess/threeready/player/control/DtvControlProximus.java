/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.control;

import android.content.Context;
import android.database.ContentObserver;
import android.media.tv.TvContract;
import android.media.tv.TvInputManager;
import android.media.tv.TvTrackInfo;
import android.media.tv.TvView;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.MemoryCache;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.PlaybackSettings;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.generic.receivers.HdmiConnectivityReceiver;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.generic.receivers.ScanReceiver;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.TvAdObserver;
import tv.threess.threeready.api.middleware.model.Scte35Info;
import tv.threess.threeready.api.middleware.model.ScteState;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.PlaybackOption;
import tv.threess.threeready.api.tv.model.TvAdReplacement;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.tv.TvDatabase;
import tv.threess.threeready.data.tv.TvTadService;
import tv.threess.threeready.player.PlaybackManager;
import tv.threess.threeready.player.command.AdStitchCommand;
import tv.threess.threeready.player.command.start.IptvLiveStartCommand;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.control.base.BaseSurfaceControl;
import tv.threess.threeready.player.control.base.BaseTvViewControl;
import tv.threess.threeready.player.exception.AdStitchError;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.MediaTrackInfo;
import tv.threess.threeready.player.result.ExactFailure;
import tv.threess.threeready.player.result.ExactSuccess;

/**
 * Implementation of the middleware player control.
 * Supports play back of: DvbC, DvbT and IpTv.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.05.29
 */
public class DtvControlProximus extends BaseTvViewControl {

    // the access has been denied for an unknown reason
    private static final int VIDEO_UNAVAILABLE_REASON_ACCESS_DENIED_UNKNOWN = 0x100;

    // the access has not been granted because a conditional access initialization issue occurred
    private static final int VIDEO_UNAVAILABLE_REASON_ACCESS_DENIED_INIT_ISSUE = 0x101;

    // the access has not been granted because of a subscription issue
    private static final int VIDEO_UNAVAILABLE_REASON_ACCESS_DENIED_NO_SUBSCRIPTION = 0x102;

    // the access has been denied because a PIN entry is required
    private static final int VIDEO_UNAVAILABLE_REASON_ACCESS_DENIED_PIN_REQUIRED = 0x103;

    // the access has been denied due to a geographical restriction
    private static final int VIDEO_UNAVAILABLE_REASON_ACCESS_DENIED_GEO_BLACKOUT = 0x104;

    // the access has been denied because of an usage rule violation
    private static final int VIDEO_UNAVAILABLE_REASON_ACCESS_DENIED_USAGE_RULE_ISSUE = 0x105;

    // parameter for setting time-shift buffer start
    private static final String APPLICATION_BUNDLE_TIMESHIFT_START_POLICY_KEY = "timeshift_start_policy";
    private static final String APPLICATION_BUNDLE_TIMESHIFT_START_POLICY_VALUE = "never";

    // private command to start the ad replacement
    public static final String TAD_ACTION_STITCHING = "tad_stitching";
    public static final String TAD_EXTRA_FILE_LOCATION = "tad_file_location";
    public static final String TAD_EXTRA_START_PTS = "tad_start_pts";
    public static final String TAD_EXTRA_DURATION = "tad_duration";
    public static final String TAD_EXTRA_STATE = "tad_scte_state";

    /**
     * Default dispatching timeout for most {@link tv.threess.threeready.player.command.base.PlaybackCommand}s.
     */
    private static final long DEFAULT_TIMEOUT = TimeUnit.SECONDS.toMillis(12);
    private static final long VAST_RANDOMIZATION = TimeUnit.SECONDS.toMillis(4);

    public static final Number AUDIO_TYPE_DOLBY = 0x81; // AC-3
    public static final Number AUDIO_TYPE_DOLBY_PLUS = 0x84; // AC-3+

    private static final Pattern AUDIO_TRACK_ID_PROPERTIES = Pattern.compile("^.+ \\[Audio\\] ([^-]+)-([^-]+)-([^-]+)-(.+)-([^-]+)$");
    private static final String AUDIO_DESCRIPTION_FOR_THE_VISUALLY_IMPAIRED = "3";
    private static final String AUDIO_FOR_THE_HEARING_IMPAIRED = "2";
    private static final String MAIN_AUDIO_STREAM = "0";

    private boolean isLive = false;

    /**
     * The position in milliseconds of the start of the time-shift buffer, it should be less {@code System.currentTimeMillis()}
     * The position is absolute utc timestamp position.
     */
    protected volatile long bufferPosition = 0;
    /**
     * The position coming from the player.
     * NOTE that this should not be used directly, since it's unreliable
     * The proper position that needs to be used is {@link #mCurrentPosition}
     */
    protected volatile long playerPosition = 0;
    protected volatile String playbackUrl = null;

    /**
     * Variables used for restrict the PLTV related actions on IpTv.
     */
    private long mMaxTimeShiftBufferDuration;

    /**
     * Runnable used to resume the playback when the time-shift buffer expired.
     */
    private final Runnable mOnBufferExpiredRunnable = () -> {
        try {
            resume(-1);
        } catch (Exception e) {
            Log.e(TAG, "Could not resume in mOnBufferExpiredRunnable", e);
        }
        applyState(PlaybackState.Started);
        offerEvent(PlaybackEvent.BufferEndReached);
    };

    private IptvLiveStartCommand mStartCommand;
    private ContentObserver mScteObserver = null;

    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);
    private final ScanReceiver mScanReceiver = Components.get(ScanReceiver.class);
    private final TvRepository mTvRepository = Components.get(TvRepository.class);
    private final MwProxy mMwProxy = Components.get(MwProxy.class);
    private final TvCache mTvCache = Components.get(TvCache.class);
    private final HdmiConnectivityReceiver mHdmiReceiver = Components.get(HdmiConnectivityReceiver.class);

    public DtvControlProximus(Context context, PlaybackDomain domain, TvView tvView) {
        super(context, domain, tvView);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ContentObserver observer = mScteObserver;
        if (observer != null) {
            Context context = mPlaybackView.getContext();
            context.getContentResolver().unregisterContentObserver(observer);
        }
        mScteObserver = mMwProxy.registerScteObserver(mMainHandler, tadObserver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ContentObserver observer = mScteObserver;
        if (observer != null) {
            Context context = mPlaybackView.getContext();
            context.getContentResolver().unregisterContentObserver(observer);
        }
    }

    @Override
    public long getCurrentPosition() {
        if (bufferPosition <= 0 || mCurrentPosition < 0) {
            // IpTv without time-shift buffer is always at live position
            return System.currentTimeMillis();
        }
        return super.getCurrentPosition();
    }

    @Override
    protected void removeCallbacks() {
        super.removeCallbacks();
        mMainHandler.removeCallbacks(mOnBufferExpiredRunnable);
        mMainHandler.removeCallbacksAndMessages(AdStitchCommand.class);
    }

    @Override
    public long getSeekAbleStart() {
        long now = System.currentTimeMillis();
        if (bufferPosition <= 0) {
            // IpTv without time-shift buffer can not rewind
            return now;
        }

        long bufferStart = now - mMaxTimeShiftBufferDuration;
        return Math.max(bufferPosition, bufferStart);
    }

    @Override
    public long getSeekAbleEnd() {
        return System.currentTimeMillis();
    }

    @Override
    public void setSpeed(long cid, double playbackSpeed) throws Exception {
        if (bufferPosition <= 0 && getCurrentPosition() <= getSeekAbleStart()) {
            offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
            return;
        }
        super.setSpeed(cid, playbackSpeed);
    }

    @Override
    public void startTrickPlay(long cid, double playbackSpeed, BaseSurfaceControl trickPlayControl) throws Exception {
        super.startTrickPlay(cid, playbackSpeed, trickPlayControl);
        setSpeed(cid, playbackSpeed);
    }

    @Override
    public void stopTrickPlay(long cid, BaseSurfaceControl trickPlayControl) throws Exception {
        super.stopTrickPlay(cid, trickPlayControl);
        PlaybackState state = getCurrentState();
        if (state == PlaybackState.TrickPlay) {
            resume(cid);
        } else if (state == PlaybackState.TrickPlayPaused) {
            pause(cid);
        }
        offerResult(new ExactSuccess(cid));
    }

    @Override
    public boolean isLive() {
        if (bufferPosition <= 0) {
            // IpTv without time-shift buffer plays always live
            return true;
        }
        return isLive;
    }

    @Override
    public long getDispatchTimeout() {
        PlaybackSettings playbackSettings = Components.get(PlaybackSettings.class);
        long timeout = playbackSettings.getIpTvPlaybackTimeout(TimeUnit.MILLISECONDS);
        if (timeout > 0) {
            return timeout;
        }

        return DEFAULT_TIMEOUT;
    }

    @Override
    protected String getStreamUrl() {
        return playbackUrl;
    }

    @Override
    protected PlaybackError.Type getExceptionType(int reason) {
        switch (reason) {
            case TvInputManager.VIDEO_UNAVAILABLE_REASON_UNKNOWN:
                if (mScanReceiver.isInProgress()) {
                    return PlaybackError.Type.TUNING_WHILE_SCANNING;
                }
                break;

            case VIDEO_UNAVAILABLE_REASON_ACCESS_DENIED_UNKNOWN:
                return PlaybackError.Type.ACCESS_DENIED;

            case VIDEO_UNAVAILABLE_REASON_ACCESS_DENIED_INIT_ISSUE:
                return PlaybackError.Type.ACCESS_DENIED_INIT_ISSUE;

            case VIDEO_UNAVAILABLE_REASON_ACCESS_DENIED_NO_SUBSCRIPTION:
                return PlaybackError.Type.ACCESS_DENIED_NO_SUBSCRIPTION;

            case VIDEO_UNAVAILABLE_REASON_ACCESS_DENIED_PIN_REQUIRED:
                return PlaybackError.Type.ACCESS_DENIED_PIN_REQUIRED;

            case VIDEO_UNAVAILABLE_REASON_ACCESS_DENIED_GEO_BLACKOUT:
                return PlaybackError.Type.ACCESS_DENIED_GEO_BLACKOUT;

            case VIDEO_UNAVAILABLE_REASON_ACCESS_DENIED_USAGE_RULE_ISSUE:
                return PlaybackError.Type.ACCESS_DENIED_USAGE_RULE_ISSUE;
        }
        return super.getExceptionType(reason);
    }

    /**
     * The STB will listen to the PID containing SCTE markers in case:
     * <p>
     * The customer has opted-in
     * The STB HW has TA support
     * The line profile is TA enabled
     * The tuned channel is enabled for TA for live TV services
     * No trick play is active
     */
    public void replaceAd(AdStitchCommand command) throws Exception {
        Context context = this.context.get();
        if (this.state != PlaybackState.Started) {
            throw new PlaybackError("Trick play is active");
        }
        if (!Settings.targetedAdvertisingFlag.get(false)) {
            throw new PlaybackError("The customer has not opted-in for tad");
        }
        if (!Settings.tadHardwareEnabled.get(false)) {
            throw new PlaybackError("The STB HW has no TA support");
        }
        if (Settings.tadDownloadBandwidth.get(0) <= 0) {
            throw new PlaybackError("The line profile is not TA enabled");
        }
        if (!Settings.tadAccountEnabled.get(false)) {
            throw new PlaybackError("Ad replacement disabled by subscriber info");
        }

        Scte35Info marker = command.getMarker();
        if (marker.get(Scte35Info.Field.perform_replacement_in_live, 1) == 0) {
            throw new PlaybackError("Ad replacement disabled by scte35-marker");
        }

        TvChannel channel = mTvCache.getTvChannelById(getPlaybackId());
        if (channel != null && !(isLive ? channel.isLiveTadEnabled() : channel.isTimeShiftTadEnabled())) {
            throw new PlaybackError("The tuned channel is not enabled for TA for live TV services");
        }

        long userInactivity = System.currentTimeMillis() - MemoryCache.LastUserActivity.get(0L);
        long channelInactivity = channel == null ? 0 : channel.getTargetAdInactivity(TimeUnit.MILLISECONDS);
        if (channelInactivity != 0 && userInactivity > channelInactivity) {
            throw new PlaybackError("Ad replacement disabled due to inactivity of the channel");
        }
        if (channelInactivity != 0 && !mHdmiReceiver.isConnected()) {
            throw new PlaybackError("Ad replacement disabled by Hdmi plug status");
        }

        String channelId = mStartCommand.getChannelId();
        double duration = marker.getDurationSeconds();
        String segmentDesc = marker.get(Scte35Info.Field.splice_descriptor);
        TvAdReplacement vast = mTvRepository.getAdReplacement(channelId, duration, segmentDesc);
        command.setVastResponse(vast);
        File file = vast.getFile();
        if (file == null) {
            throw new AdStitchError(TvAdReplacement.Error.Unidentified, "File name not found in vast response");
        }

        if (!file.exists()) {
            Log.d(TAG, "advertisement file missing, triggering advertisement download");
            TvTadService.scheduleAdvertisementDownload(context, file.getName());
            throw new AdStitchError(TvAdReplacement.Error.NotFound, "Ad file not downloaded yet: " + file);
        }

        if (!LocaleUtils.getApplicationLanguage().equals(Settings.tadDownloadLanguage.get(null))) {
            Log.d(TAG, "language change detected, triggering advertisement download");
            TvTadService.scheduleAdvertisementDownload(context, file.getName());
        }
        else if (vast.isExpired()) {
            Log.d(TAG, "advertisement expired, triggering advertisement download");
            TvTadService.scheduleAdvertisementDownload(context, file.getName());
        }

        long markerDuration = marker.getDuration(TimeUnit.MILLISECONDS);
        long tasDuration = vast.getDuration(TimeUnit.MILLISECONDS);
        if (Math.abs(markerDuration - tasDuration) > Settings.tapAdDurationTolerance.get(0)) {
            throw new AdStitchError(TvAdReplacement.Error.DurationMismatch, "Marker duration[" + markerDuration + "] does not match tas duration[" + tasDuration + "]: " + file);
        }

        final long startPts = marker.get(Scte35Info.Field.pts_time, 0);
        final long durationHz = marker.get(Scte35Info.Field.duration, 0);
        mPlaybackView.post(() -> {
            Bundle extra = new Bundle();
            extra.putString(TAD_EXTRA_FILE_LOCATION, file.getAbsolutePath());
            extra.putLong(TAD_EXTRA_START_PTS, startPts);
            extra.putLong(TAD_EXTRA_DURATION, durationHz);
            Log.d(TAG, "sendAppPrivateCommand: " + TAD_ACTION_STITCHING, extra);
            mPlaybackView.sendAppPrivateCommand(TAD_ACTION_STITCHING, extra);

            offerResult(new ExactSuccess(command.getId()));
        });
    }

    public void start(IptvLiveStartCommand command) throws Exception {
        command.assertNotCancelled();
        mStartCommand = command;
        IBroadcast broadcast = command.getContentItem();

        List<PlaybackOption> playbackOptions = TvDatabase.queryPlaybackOptions(context.get(), broadcast.getChannelId(),
                mInternetChecker.isInternetAvailable(), false, true, false
        );
        if (playbackOptions.isEmpty()) {
            throw new PlaybackError(PlaybackError.Type.NO_PLAYBACK_OPTION,
                    "No playback options for channel: " + broadcast.getChannelId()
            );
        }
        command.assertNotCancelled();

        PlaybackOption playbackOption = playbackOptions.get(0);
        command.setBitrate(playbackOption.getPlaybackExtra());

        Uri channelUri = TvContract.buildChannelUri(playbackOption.getTifChannelId());
        playbackUrl = playbackOption.getPlaybackData();
        isLive = true;

        boolean isPLTVEnabled = Settings.isPLTVEnabled.get(false);
        mMaxTimeShiftBufferDuration = isPLTVEnabled ? Settings.maxTimeShiftBufferDuration.get(0L) : 0;

        // Disable the PLTV buffer.
        Bundle params = Bundle.EMPTY;
        if (!isPLTVEnabled) {
            params = new Bundle();
            params.putString(APPLICATION_BUNDLE_TIMESHIFT_START_POLICY_KEY, APPLICATION_BUNDLE_TIMESHIFT_START_POLICY_VALUE);
        }

        super.start(command.getId(), broadcast.getStart(), broadcast.getEnd(),
                playbackOption.getTifInputId(), channelUri, params);
    }

    @Override
    public void pause(long cid) throws Exception {
        // pause does not throw an error from DtvPlayer even if there is no time shift buffer
        // disable it if we know it will not work
        if (bufferPosition <= 0) {
            offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
            return;
        }
        // we need to check if the start of the buffer is smaller than the current progress coming from the player
        if (getBufferStart() >= playerPosition) {
            offerResult(new ExactFailure(cid, PlaybackError.Type.BUFFER_START_REACHED));
            return;
        }

        isLive = false;
        super.pause(cid);

        mMainHandler.removeCallbacks(mOnBufferExpiredRunnable);
        long currentBufferDuration = getBufferEnd() - getCurrentPosition();
        mMainHandler.postDelayed(mOnBufferExpiredRunnable,
                mMaxTimeShiftBufferDuration - currentBufferDuration);
    }

    @Override
    public void resume(long cid) throws Exception {
        super.resume(cid);
        mMainHandler.removeCallbacks(mOnBufferExpiredRunnable);
    }

    @Override
    protected boolean canPause() {
        return bufferPosition > 0;
    }

    @Override
    public void reset() {
        super.reset();
        mStartCommand = null;
        bufferPosition = 0;
        playerPosition = 0;
    }

    @Override
    public void jump(long cid, long position) throws Exception {
        if (bufferPosition <= 0) {
            offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
            return;
        }

        if (isLive && position >= getBufferEnd()) {
            offerResult(new ExactSuccess(cid));
            return;
        }

        mMainHandler.removeCallbacksAndMessages(AdStitchCommand.class);
        isLive = position >= getBufferEnd();
        super.jump(cid, position);
    }

    protected MediaTrackInfo decorateMediaTrackInfo(TvTrackInfo tvTrackInfo) {
        MediaTrackInfo.SurroundType surroundType = MediaTrackInfo.SurroundType.None;
        boolean descriptive = false;

        if (tvTrackInfo.getType() == TvTrackInfo.TYPE_AUDIO) {
            // fixme: another technicolor hack: J1278000000-2312
            // " [Audio] " + language + " - " + streamType + " - " + audioCodec + " - " + audioFormat + " - " + audioType
            // pid=601 [Audio] fre-15-aac-adts-stereo-3
            Matcher audioMatch = AUDIO_TRACK_ID_PROPERTIES.matcher(tvTrackInfo.getId());
            if (audioMatch.matches()) {
                switch (audioMatch.group(5)) {
                    case AUDIO_DESCRIPTION_FOR_THE_VISUALLY_IMPAIRED:
                    case AUDIO_FOR_THE_HEARING_IMPAIRED:
                        descriptive = true;
                        break;

                    case MAIN_AUDIO_STREAM:
                        break;
                }
            }
        }

        Bundle extras = tvTrackInfo.getExtra();
        if (extras != null) {
            if (TRACK_EXTRA_SUBTITLE_TYPE_HARD_OF_HEARING.equals(extras.getString(TRACK_EXTRA_SUBTITLE_TYPE, ""))) {
                descriptive = true;
            }

            String audio = extras.getString(TRACK_EXTRA_AUDIO_TYPE, "");
            if (!TextUtils.isEmpty(audio) && !TRACK_EXTRA_AUDIO_TYPE_NORMAL.equals(audio)) {
                descriptive = true;
            }

            Object streamType = extras.get(PlaybackManager.EXTRA_STREAM_TYPE);
            if (AUDIO_TYPE_DOLBY_PLUS.equals(streamType)) {
                surroundType = MediaTrackInfo.SurroundType.DolbyDigitalPlus;
            } else if (AUDIO_TYPE_DOLBY.equals(streamType)) {
                surroundType = MediaTrackInfo.SurroundType.DolbyDigital;
            }
        }

        return new MediaTrackInfo(tvTrackInfo.getId(), tvTrackInfo.getLanguage(), tvTrackInfo.getType(), descriptive, surroundType);
    }

    @Override
    public void processPosition(long contentPosition, boolean notifyChange) {
        super.processPosition(contentPosition, notifyChange);

        if (mStartCommand == null) {
            return;
        }

        if ((contentPosition < getContentStart() || contentPosition > getContentEnd()) && notifyChange) {
            IBroadcast broadcast = mStartCommand.getBroadcast(contentPosition, false);
            if (broadcast != null) {
                applyContentPositions(broadcast.getStart(), broadcast.getEnd());
            }

            offerEvent(PlaybackEvent.PlaybackItemChanged);
        }
    }

    @Override
    public String getPlaybackId() {
        return mStartCommand == null ? null : mStartCommand.getContentItem().getChannelId();
    }

    @Override
    protected void onVideoUnavailable(String inputId, int reason) {
        if (reason == TvInputManager.VIDEO_UNAVAILABLE_REASON_AUDIO_ONLY) {
            onVideoAvailable(inputId);
            return;
        }

        super.onVideoUnavailable(inputId, reason);
    }

    @Override
    protected void onTimeShiftStartPositionChanged(String inputId, long contentTimeMs) {
        super.onTimeShiftStartPositionChanged(inputId, contentTimeMs);

        if (mMaxTimeShiftBufferDuration == 0) {
            Log.v(TAG, "Time-shift buffer is disabled on backend.");
            return;
        }

        if (getDomain() == PlaybackDomain.RadioIpTv) {
            Log.v(TAG, "In Radio channel we don't have TimeShift buffer!");
            return;
        }

        if (bufferPosition > 0 && playerPosition > 0 && contentTimeMs > playerPosition
                && !isLive && getCurrentState() == PlaybackState.Paused) {
            Log.d(TAG, "Buffer reached. Resume playback.");
            mMainHandler.removeCallbacks(mOnBufferExpiredRunnable);
            mMainHandler.post(mOnBufferExpiredRunnable);
        }

        long maxBufferStart = getBufferEnd() - mMaxTimeShiftBufferDuration;
        bufferPosition = Math.max(contentTimeMs, maxBufferStart);
    }

    @Override
    protected void onTimeShiftCurrentPositionChanged(String inputId, long contentTimeMs) {
        super.onTimeShiftCurrentPositionChanged(inputId, contentTimeMs);
        // save the player position in order to enable the start position changed events
        // and not produce fake buffer end and buffer start reached events
        if (this.state == PlaybackState.Paused) {
            return;
        }
        playerPosition = contentTimeMs;
    }

    private final TvAdObserver tadObserver = new TvAdObserver() {

        @Override
        public void onMarkerChange(Scte35Info marker) {
            if (DtvControlProximus.this.state != PlaybackState.Started) {
                return;
            }
            Log.d(TAG, "SCTE marker parsed: " + marker);
            long delay = Settings.tapVASTRequestRandomization.get(VAST_RANDOMIZATION);
            mMainHandler.postDelayed(() -> offerEventCommand(new AdStitchCommand(DtvControlProximus.this, marker)),
                    AdStitchCommand.class, TimeBuilder.randomDuration(delay)
            );
        }

        @Override
        public void onStateChange(ScteState state) {
            if (DtvControlProximus.this.state != PlaybackState.Started) {
                return;
            }
            Log.d(TAG, "SCTE state changed: " + state);
            Bundle data = new Bundle();
            data.putSerializable(TAD_EXTRA_STATE, state);
            offerEvent(PlaybackEvent.AdReplacement, data);
        }
    };
}
