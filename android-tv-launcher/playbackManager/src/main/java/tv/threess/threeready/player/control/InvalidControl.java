/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.control;

import android.content.Context;
import android.view.View;

import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.result.ExactFailure;
import tv.threess.threeready.player.result.ExactSuccess;

/**
 * Controller class for applications
 *
 * @author Karetka Mezei Zoltan
 * @since 2018.01.16
 */
public class InvalidControl extends PlaybackControl<View> {

    /**
     * Default dispatching timeout for application start command.
     */
    private static final long TIMEOUT_DEFAULT = 1000L;

    public InvalidControl(Context context, PlaybackDomain domain, View playbackView) {
        super(context, domain, playbackView);
    }


    public void start(long cid, String channelId) {
        offerResult(new ExactFailure(cid, new PlaybackError(PlaybackError.Type.NO_PLAYBACK_OPTION,
                "No playback options for channel: "+ channelId))
        );
    }

    @Override
    public void stop(long cid) {
        offerResult(new ExactSuccess(cid));
    }

    @Override
    public void pause(long cid) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    public void resume(long cid) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    public void setSpeed(long cid, double playbackSpeed) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    public void jump(long cid, long position) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    public void selectAudioTrack(long cid, String trackId) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    public void selectSubtitleTrack(long cid, String trackId) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    public void mute(long cid) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    public void unMute(long cid) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    protected boolean canPause() {
        return false;
    }

    @Override
    public long getDispatchTimeout() {
        return TIMEOUT_DEFAULT;
    }

    @Override
    protected void fillMediaTrackInfo(PlaybackDetailsBuilder details) {
    }

    @Override
    protected String getStreamUrl() {
        return null;
    }
}
