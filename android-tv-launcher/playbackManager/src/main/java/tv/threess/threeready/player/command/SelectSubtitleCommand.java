/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command;

import tv.threess.threeready.player.command.base.PlaybackCommand;

/**
 * {@link PlaybackCommand} that selects or deselects a subtitle track. Subtitles are not selected by default and may be
 * deselected. This command deselects the currently selected subtitle track so it does not need an explicit track id for
 * deselection.
 *
 * @author Dan Titiu
 * @since 22/02/2017
 */
public class SelectSubtitleCommand extends SelectTrackCommand {

    public SelectSubtitleCommand(long id, String trackId) {
        super(id, trackId);
    }

    public SelectSubtitleCommand(long id) {
        super(id, null);
    }

    @Override
    protected void executeImplicit() throws Exception {
        this.control.selectSubtitleTrack(this.id, this.trackId);
    }
}
