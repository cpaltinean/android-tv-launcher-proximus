/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.contract;

/**
 * Enumerations of actions that can be performed during playback.
 *
 * @author Dan Titiu
 * @since 11.02.2014
 */
public enum PlaybackAction {

    /**
     * Start a new playback session, playing a new stream.
     */
    Start {
        @Override
        public PlaybackRelation relate(PlaybackAction other) {
            // Start action clears the queue, cancelling all queued commands, with some exceptions:
            // - Inactivate / Stop / Release should be cancelled only for the same domain
            //      (allow UI to release before switching domains and avoid injection when action already queued);
            switch (other) {
                default:
                    return PlaybackRelation.CancelOther;
            }
        }
    },

    /**
     * Selects a media track on the currently running player.
     * Player needs to be Playing in order to be able to select a media track.
     * Does not touch other actions but it only makes sense to perform the latest select on the same domain.
     */
    Select {
        @Override
        public PlaybackRelation relate(PlaybackAction other) {
            switch (other) {
                case Select:
                    return PlaybackRelation.CancelOther;
                default:
                    return PlaybackRelation.None;
            }
        }
    },

    /**
     * Adds a new stream to the playlist.
     */
    Add {
        @Override
        public PlaybackRelation relate(PlaybackAction other) {
            switch (other) {
                case Add:
                    return PlaybackRelation.CancelOther;
                default:
                    return PlaybackRelation.None;
            }
        }
    },


    /**
     * Update the currently running playlist item.
     */
    Update {
        @Override
        public PlaybackRelation relate(PlaybackAction other) {
            switch (other) {
                case Add:
                    return PlaybackRelation.CancelOther;
                default:
                    return PlaybackRelation.None;
            }
        }
    },

    /**
     * Changes to volume level of the currently running player.
     * Player needs to be Playing in order to be able to select a media track.
     * Cancels other volume related commands.
     */
    Volume {
        @Override
        public PlaybackRelation relate(PlaybackAction other) {
            switch (other) {
                case Volume:
                    return PlaybackRelation.CancelOther;
                default:
                    return PlaybackRelation.None;
            }
        }
    },

    /**
     * Pauses playback. Not applicable for live content.
     */
    Pause {
        @Override
        public PlaybackRelation relate(PlaybackAction other) {
            switch (other) {
                case Resume:
                    return PlaybackRelation.CancelBoth;
                case Pause:
                case TrickPlay:
                    return PlaybackRelation.CancelOther;
                default:
                    return PlaybackRelation.None;
            }
        }
    },

    /**
     * Resumes playback while paused or not in Started state.
     */
    Resume {
        @Override
        public PlaybackRelation relate(PlaybackAction other) {
            switch (other) {
                case Pause:
                    return PlaybackRelation.CancelBoth;
                case TrickPlay:
                case Resume:
                    return PlaybackRelation.CancelOther;
                default:
                    return PlaybackRelation.None;
            }
        }
    },

    /**
     * Puts the player into the trick mode state where playback is ongoing with a custom speed forward or backward.
     * Not applicable for live content.
     */
    TrickPlay {
        @Override
        public PlaybackRelation relate(PlaybackAction other) {
            switch (other) {
                case TrickPlay:
                case Pause:
                    return PlaybackRelation.CancelOther;
                default:
                    return PlaybackRelation.None;
            }
        }
    },

    /**
     * Moves the playback to a certain position in time relative to the duration of the played asset.
     * Not applicable for live content.
     */
    RelativeJump {
        @Override
        public PlaybackRelation relate(PlaybackAction other) {
            switch (other) {
                case RelativeJump:
                    return PlaybackRelation.Cumulate;
                default:
                    return PlaybackRelation.None;
            }
        }
    },

    /**
     * Moves the playback to a certain position.
     */
    AbsoluteJump {
        @Override
        public PlaybackRelation relate(PlaybackAction other) {
            switch (other) {
                case AbsoluteJump:
                case RelativeJump:
                    return PlaybackRelation.CancelOther;
                default:
                    return PlaybackRelation.None;
            }
        }
    },

    /**
     * Makes the current active playback control inactive without calling stop.
     * Useful when switching between exclusive domains that run on the same underlying player.
     */
    Inactivate {
        @Override
        public PlaybackRelation relate(PlaybackAction other) {
            // For the same domain, like Stop, Inactivate action cancels all other queued commands.
            return PlaybackRelation.CancelOther;
        }
    },

    /**
     * Stops currently active playback.
     */
    Stop {
        @Override
        public PlaybackRelation relate(PlaybackAction other) {
            // For the same domain, Stop action cancels all other queued commands.
            return PlaybackRelation.CancelOther;
        }
    },

    /**
     * Releases any states associated with a player (e.g. reserved bandwidths).
     * Similar with the Stop action this command cancels all other queued commands.
     */
    Release {
        @Override
        public PlaybackRelation relate(PlaybackAction other) {
            switch (other) {
                case Stop:
                    return PlaybackRelation.None;

                default:
                    return PlaybackRelation.CancelOther;
            }
        }
    },

    /**
     * Blanks the screen so the last drawn video frame is no longer displayed.
     * Playback must be Stopped on ALL domains.
     * Only one blank command is allowed after stopping playback.
     */
    Blank {
        @Override
        public PlaybackRelation relate(PlaybackAction other) {
            switch (other) {
                case Blank:
                    return PlaybackRelation.CancelOther;
                default:
                    return PlaybackRelation.None;
            }
        }
    },

    Validate {
        @Override
        public PlaybackRelation relate(PlaybackAction other){
            return PlaybackRelation.None;
        }
    };


    public abstract PlaybackRelation relate(final PlaybackAction other);
}
