package tv.threess.threeready.player.plugin;

import android.os.Bundle;

import tv.threess.threeready.player.Lifecycleable;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.result.EventResult;
import tv.threess.threeready.player.result.Result;

/**
 * Interface for the playback plugin manager
 *
 * @author Andor Lukacs
 * @since 14/12/2020
 */
public interface IPluginManager extends Lifecycleable {
    /**
     * Executes right before a PlaybackCommand is about to be added to the command queue.
     *
     * @param command The {@link PlaybackCommand} being dispatched.
     * @throws PlaybackError in case the plugin wants to block execution.
     */
    default void onOfferCommand(PlaybackCommand command) throws PlaybackError {}


    /**
     * Called right before the dispatched command is about to be executed.
     *
     * @param command The {@link PlaybackCommand} being dispatched.
     * @param state   The general state of playback.
     * @param details Active domain details before command execution.
     * @throws PlaybackError in case the plugin wants to block execution.
     */
    default void onBeforeExecute(PlaybackCommand command, PlaybackEntireState state, PlaybackDetailsBuilder details)
            throws PlaybackError {}


    /**
     * Called right after the command is executed but before the result.
     *
     * @param command The {@link PlaybackCommand} being dispatched.
     * @param state   The general state of playback.
     * @param details Active domain details after command execution.
     * @throws PlaybackError in case the plugin wants to block execution.
     */
    default void onAfterExecute(PlaybackCommand command, PlaybackEntireState state, PlaybackDetailsBuilder details)
            throws PlaybackError {}

    /**
     * Called after the command was executed and fulfilled by a result or failed.
     * This method SHOULD NOT try to control {@link PlaybackCommand} flow by throwing exceptions.
     *
     * @param command The {@link PlaybackCommand} being dispatched.
     * @param result  The {@link Result} obtained after executing command.
     */
    default void onResult(PlaybackCommand command, Result result) {}

    /**
     * Called when a {@link tv.threess.threeready.player.contract.PlaybackEvent} is triggered from background.
     * This method SHOULD NOT try to control {@link PlaybackCommand} flow by throwing exceptions.
     *
     * @param result  The {@link EventResult} carrying the {@link tv.threess.threeready.player.contract.PlaybackEvent}.
     */
    default void onEvent(EventResult result, Bundle details) {}
}
