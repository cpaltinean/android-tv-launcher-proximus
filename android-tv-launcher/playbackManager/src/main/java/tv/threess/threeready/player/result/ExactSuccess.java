/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.result;

import androidx.annotation.Nullable;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.command.base.PlaybackCommand;

/**
 * Command result representing a success that matches the command by its ID.
 *
 * @author Dan Titiu
 * @since 12.08.2014
 */
public class ExactSuccess extends ExactResult {
    private static final String TAG = Log.tag(ExactSuccess.class);

    public ExactSuccess(final long cid) {
        super(cid);
    }

    @Override
    public void apply(PlaybackCommand cmd, @Nullable Long nextCmdId) {
        cmd.commit();
    }

    @Override
    public boolean isFavorable() {
        return true;
    }

    @Override
    public void dispatchTo(IPlaybackCallback callback) {
        if (this.cid <= 0) {
            throw new IllegalStateException("Trying to dispatch callback without command id");
        }

        try {
            callback.onCommandSuccess(this.cmd, this.details);
        } catch (Exception e) {
            Log.e(TAG, "Failed to dispatch success for command id[" + cid + "]", e);
        }
    }
}
