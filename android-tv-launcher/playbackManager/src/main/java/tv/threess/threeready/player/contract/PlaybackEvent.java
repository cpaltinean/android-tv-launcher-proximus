/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.contract;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Enumeration of events that may occur during playback without specific user input and usually cannot be matched to a
 * playback command (e.g. EOS, BOS).
 *
 * @author Dan Titiu
 * @since 18.06.2014
 */
public enum PlaybackEvent implements Parcelable {

    /**
     * Playback buffering (not visible).
     */
    PlaybackBuffering(true),

    /**
     * Playback started (visible).
     */
    PlaybackStarted(true),

    /**
     * Add a new playback item to the end of the play list.
     */
    AppendPlaylist(false),

    /**
     * Add a new playback item to the beginning of the playlist.
     */
    PrependPlaylist(false),

    /**
     * Called when the current playlist needs to bee updated.
     */
    UpdatePlayList(false),

    /**
     * Called when an ad needs to be replaced.
     */
    StitchAd(false),

    /**
     * Playback item is changed on the play list.
     */
    PlaybackItemChanged(false),

    /**
     * The start of the next blacklisted slot reached.
     */
    NextContentBlacklisted(false),

    /**
     * The end of the previous blacklisted playback slot reached.
     */
    PreviousContentBlacklisted(false),

    /**
     * Playback failed unexpectedly (e.g. signal lost).
     */
    PlaybackFailed(true),

    /**
     * Playback is unavailable.
     */
    PlaybackUnavailable(true),

    /**
     * Playback is unavailable.
     */
    PlaybackDisconnected(true),

    /**
     * Playback stopped.
     */
    EndReached(true),

    /**
     * Playback buffer end reached.
     */
    BufferEndReached(true),

    /**
     * Display binge watching.
     */
    BingeWatchingReached(true),

    /**
     * Playback is loading and the loading spinner is shown.
     */
    StartLoadingSpinner(false),

    /**
     * Playback is loaded and the loading spinner is hidden.
     */
    StopLoadingSpinner(false),

    /**
     * Player state has be changed
     */
    PlayerStateChanged(false),

    /**
     * Playback bitrate has changed
     */
    BitrateChanged(false),

    /**
     * Audio, Video, Text segment downloaded
     */
    SegmentDownloaded(false),

    /**
     * Playback tracks have changed
     */
    TracksChanged(false),

    /**
     * Start command offered to playback queue
     */
    StartInitiated(false),

    /**
     * Backend session ended
     */
    BackendSessionInvalid(true),

    /**
     * AD replacement started or in progress
     */
    AdReplacement(false);

    //
    // PARCELABLE SPECIFIC FIELDS AND METHODS
    //

    public boolean mDiscardable = true;

    PlaybackEvent(boolean discardable) {
        mDiscardable = discardable;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name());
    }

    public static final Creator<PlaybackEvent> CREATOR = new Creator<PlaybackEvent>() {
        @Override
        public PlaybackEvent createFromParcel(final Parcel source) {
            return PlaybackEvent.valueOf(source.readString());
        }

        @Override
        public PlaybackEvent[] newArray(final int size) {
            return new PlaybackEvent[size];
        }
    };
}
