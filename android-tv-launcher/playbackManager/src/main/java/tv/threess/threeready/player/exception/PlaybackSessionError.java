package tv.threess.threeready.player.exception;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.playback.exception.SessionOpenException;

public class PlaybackSessionError extends PlaybackError {

    public PlaybackSessionError(SessionOpenException cause) {
        super(getType(cause), cause.getCause());
    }

    private static Type getType(SessionOpenException error) {
        if (error == null) {
            return Type.SESSION_OPEN_FAILED;
        }

        if (!Components.get(InternetChecker.class).isInternetAvailable()) {
            return Type.INTERNET_UNAVAILABLE;
        }

        Throwable cause = error.getCause();
        if (cause instanceof UnknownHostException) {
            return Type.UNKNOWN_HOST_DNS;
        }

        if (cause instanceof SocketTimeoutException || cause instanceof ConnectException) {
            return Type.SOCKET_TIMEOUT;
        }

        return error.isMicroService() ? Type.SESSION_OPEN_FAILED_MS : Type.SESSION_OPEN_FAILED;
    }
}
