/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.model;

import android.media.tv.TvTrackInfo;

import tv.threess.threeready.api.playback.model.TrackInfo;

/**
 * Parcelable DTO representing media (e.g. audio, subtitle) track details.
 *
 * @author Dan Titiu
 * @since 21/02/2017
 */
public class MediaTrackInfo extends TrackInfo {
    public static final String LANG_FRENCH_AD_SUBTITLE = "qad-s";
    public static final String LANG_FRENCH_AD_AUDIO = "qad";
    public static final String LANG_DUTCH_AD_GOT = "got";
    public static final String LANG_DUTCH_AD_BNL = "bnl";

    /**
     * The index of the media track.
     */
    private final String id;

    /**
     * The encoding type of the media track. For audio tracks the possible values are defined as constants in Api.
     */
    private final Type type;

    private final SurroundType surroundType;

    /**
     * Constructs an media track object based on the provided info.
     *
     * @param id        The media track id.
     * @param language  The language of the media track as a language code (3-letter, ISO 639-2).
     * @param type      The encoding type of the media track.
     */
    public MediaTrackInfo(String id, String language, int type, boolean descriptive, SurroundType surroundType) {
        super(language, descriptive);
        this.id = id;
        this.type = Type.valueOf(type);
        this.surroundType = surroundType;
    }

    /**
     * @return The media track index.
     */
    public String getId() {
        return id;
    }

    @Override
    public boolean isDescriptive() {
        switch (language) {
            // treat some temporary languages as descriptive
            case LANG_FRENCH_AD_SUBTITLE:
            case LANG_FRENCH_AD_AUDIO:
            case LANG_DUTCH_AD_GOT:
            case LANG_DUTCH_AD_BNL:
                return true;
        }
        return super.isDescriptive();
    }

    /**
     * @return The encoding type of the media track. For audio tracks the possible values are defined as constants in Api.
     */
    public Type getType() {
        return type;
    }

    public boolean isDolby() {
        return surroundType == SurroundType.DolbyDigital || surroundType == SurroundType.DolbyDigitalPlus;
    }

    public SurroundType getSurroundType() {
        return surroundType;
    }

    @Override
    public String toString() {
        return "Track{id[" + this.id +
                "],language[" + this.language +
                "],descriptive[" + this.descriptive +
                "],type[" + this.type +
                "],surroundType[" + this.surroundType +
                "]}";
    }

    public enum Type {
        Video,
        Audio,
        Subtitle,
        Unknown;

        private static Type valueOf(int type) {
            switch (type) {
                case TvTrackInfo.TYPE_AUDIO:
                    return Type.Audio;

                case TvTrackInfo.TYPE_VIDEO:
                    return Type.Video;

                case TvTrackInfo.TYPE_SUBTITLE:
                    return Type.Subtitle;
            }
            return Type.Unknown;
        }
    }

    public enum SurroundType {
        DolbyDigital,
        DolbyDigitalPlus,
        None
    }
}
