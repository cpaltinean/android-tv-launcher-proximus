/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command.base;

import androidx.annotation.NonNull;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.model.PlaybackStatusBuilder;

/**
 * Superclass for commands that target explicit {@link PlaybackType} and {@link PlaybackControl}.
 *
 * @author Dan Titiu
 * @since 21.07.2014
 */
public abstract class ExplicitCommand extends AbstractCommand {

    /**
     * Playback type defining also the domain on which to take action. Helps identifying the PlaybackControl.
     */
    @NonNull
    protected PlaybackType type;

    /**
     * Playback control onto which the command will execute.
     */
    @NonNull
    protected final PlaybackControl control;

    /**
     * Default constructor.
     *  @param id           Unique identifier for each command.
     * @param type          Playback domain on which to take action. Helps identifying the PlaybackControl.
     * @param action        Playback action to be taken on the appropriate PlaybackControl.
     * @param control       Playback control onto which the command will execute.
     * @param targetState   Playback state that is reached once this command is successfully executed.
     */
    public ExplicitCommand(final long id, @NonNull final PlaybackType type, final PlaybackAction action,
                           @NonNull final PlaybackControl control, final PlaybackState targetState) {
        super(id, action, targetState);
        this.type = type;
        this.control = control;
    }

    @NonNull
    @Override
    public PlaybackType getType() {
        return this.type;
    }

    @NonNull
    @Override
    public PlaybackDomain getDomain() {
        return this.type.getDomain();
    }

    @Override
    public long getTimeout() {
        return this.control.getDispatchTimeout();
    }

    @Override
    public void cancel() {
        super.cancel();
        this.control.cancel(this.id);
    }


    @Override
    public void decorate(PlaybackStatusBuilder status) {
        status.setType(this.type);
    }

    @Override
    public void decorate(PlaybackDetailsBuilder details) {
        details.setType(this.type);
    }

    @Override
    public boolean equals(Object o) {
        if (super.equals(o) && o instanceof ExplicitCommand) {
            ExplicitCommand other = (ExplicitCommand) o;
            return this.type.equals(other.type) && this.control.equals(other.control) && this.getClass().equals(other.getClass());
        }

        return false;
    }

    public String toString(String key, String value) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append("{control[");
        sb.append(this.control.getClass().getSimpleName());
        sb.append("-");
        sb.append(this.control.getDomain());
        sb.append("], type[");
        sb.append(this.type);
        if (this.isSynthetic()) {
            sb.append("],[synthetic");
        } else {
            sb.append("],id[");
            sb.append(this.id);
        }
        if (key != null) {
            sb.append("],").append(key).append("[");
            sb.append(value);
        }
        sb.append("]}");
        return sb.toString();
    }

    @Override
    public String toString() {
        return toString(null, null);
    }
}
