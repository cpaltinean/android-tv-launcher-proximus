package tv.threess.threeready.player.command.start;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.TrailerControlProximus;
import tv.threess.threeready.player.control.base.PlaybackControl;

public class TrailerStartCommand extends ContentItemStartCommand<IVodVariant> {

    private final TrailerControlProximus mControl;
    private final IVodItem mVod;
    private final IVodVariant mVodVariant;
    private final Long mPosition;

    public TrailerStartCommand(long id, PlaybackType type, PlaybackControl control,
                               StartAction startAction, IVodItem vod, IVodVariant vodVariant, Long position) {
        super(id, type, startAction, control);
        mControl = validateControl(control, TrailerControlProximus.class);
        mVod = vod;
        mVodVariant = vodVariant;
        mPosition = position;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        mControl.start(this);
    }

    public IVodItem getVodItem() {
        return mVod;
    }

    public Long getPosition() {
        return mPosition;
    }

    @Override
    public TrailerStartCommand createReTuneCommand(StartAction action, long cid) {
        return new TrailerStartCommand(cid, type, control, action, mVod, mVodVariant, null);
    }

    @Override
    public Comparable<?> playbackId() {
        IVodVariant vodVariant = mVodVariant;
        if (vodVariant == null) {
            return null;
        }
        return vodVariant.getId();
    }

    @Override
    public IVodVariant getContentItem() {
        return mVodVariant;
    }
}
