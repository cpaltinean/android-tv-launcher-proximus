package tv.threess.threeready.player.contract;

/**
 * @author Andor Lukacs
 * @since 10/26/18
 */
public enum SeekDirection {
    REWIND,
    FORWARD
}
