/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetails;

/**
 * Callback interface for playback related errors and messages.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.05.23
 */
public interface IPlaybackCallback {

    default void onCommandSuccess(PlaybackCommand cmd, PlaybackDetails details) {}

    default void onCommandFailure(PlaybackCommand cmd, PlaybackDetails details, PlaybackError error) {}

    default void onPlaybackEvent(PlaybackEvent event, PlaybackDetails details, PlaybackError error) {}
}
