/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.control;

import android.content.Context;
import android.media.tv.TvView;

import tv.threess.threeready.api.playback.model.IOttStreamingSession;
import tv.threess.threeready.api.playback.model.OTTStreamingSessionContainer;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.player.command.start.ReplayStartCommand;
import tv.threess.threeready.player.control.base.SessionBasedControlProximus;

/**
 * Playback controller for live radio streams.
 *
 * @author Barabas Attila
 * @since 2018.06.22
 */
//TODO: RENAME
public class RadioControlProximus extends SessionBasedControlProximus<IOttStreamingSession,
        OTTStreamingSessionContainer<IOttStreamingSession>> {

    public RadioControlProximus(Context context, PlaybackDomain domain, TvView tvView) {
        super(context, domain, tvView, new OTTStreamingSessionContainer<>());
    }

    /**
     * Start radio playback for the give broadcast and channel.
     */
    public void start(ReplayStartCommand command) throws Exception {
        command.assertNotCancelled();
        //TODO: once needed, create Radio start command for OTT
        Long relativePosition = command.getPosition();

        IBroadcast broadcast = command.getContentItem();
        IOttStreamingSession session = mSessionHandler.openChannelStreamingSession(broadcast.getChannelId());

        command.assertNotCancelled();
        start(command.getId(), broadcast.getStart(), broadcast.getEnd(), null, relativePosition, session);
    }

    @Override
    public long getCurrentPosition() {
        return System.currentTimeMillis();
    }

    @Override
    public long getSeekAbleEnd() {
        return getCurrentPosition();
    }

    @Override
    public boolean isLive() {
        return true;
    }

    @Override
    protected boolean canPause() {
        return false;
    }

    @Override
    public String getPlaybackId() {
        return null;
    }
}
