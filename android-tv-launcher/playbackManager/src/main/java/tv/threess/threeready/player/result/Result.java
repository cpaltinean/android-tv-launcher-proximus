/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.result;

import androidx.annotation.Nullable;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.model.PlaybackDetails;

/**
 * Represents the result of a playback command or background event. Results can represent success, failure, events but
 * also other non-binary kind of results.
 *
 * @author Dan Titiu
 * @since 21.06.2014
 */
public interface Result {

    /**
     * Checks if this result matches the {@link PlaybackCommand} or not. The matching can be performed by subclasses on
     * several different criteria and will conclude the execution of a PlaybackCommand.
     *
     * @param cmd    {@link PlaybackCommand Command} to matched.
     * @return true when the command and the result match.
     */
    boolean matches(PlaybackCommand cmd);

    /**
     * After a matching result was found for a command, the command and the result are paired. This is mostly useful for
     * results which are not exact and which matched the command on other criteria than the command ID.
     *
     * @param cmd    The matching {@link PlaybackCommand} to be paired with.
     */
    void pairWith(PlaybackCommand cmd);

    /**
     * Applies the target state of the command for the specific result onto the control, thus changing the control
     * state. A simple example would be to apply the target state of a command in case of a success onto the target
     * playback control. Another example would be to rollback in case of failure.
     *
     * @param cmd    The matching {@link PlaybackCommand} which decides the target state of the {@link PlaybackControl}.
     */
    void apply(PlaybackCommand cmd, @Nullable Long nextCmdId) throws Exception;

    /**
     * Returns true if this result applies a favorable change towards the {@link PlaybackCommand} goal.
     *
     * @return true if this result applies a favorable change towards the {@link PlaybackCommand} goal.
     */
    boolean isFavorable();

    /**
     * As the result is the carrier of the playback state obtained after executing the {@link PlaybackCommand command}
     * it needs to be completed with those details carried back to the consumer of the PlaybackInterface.
     *
     * @param details    PlaybackDetails obtained after executing the command.
     */
    void complete(PlaybackDetails details);

    /**
     * Dispatches itself back to the interface that issued the command. The reason we encapsulate the dispatching into
     * the result is because each type of result (e.g. success, failure) may dispatch itself to different methods of the
     * callback.
     *
     * @param callback    The callback instance.
     */
    void dispatchTo(IPlaybackCallback callback);

    /**
     * Returns the details associated with this result which were collected from command and/or control.
     * 
     * @return  The details associated with this result.
     */
    PlaybackDetails getDetails();
}
