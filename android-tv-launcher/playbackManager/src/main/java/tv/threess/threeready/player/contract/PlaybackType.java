/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.contract;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import tv.threess.threeready.api.playback.player.PlaybackDomain;

/**
 * Enumeration defining the types of playback performed on the client. This type classifies the playback from the
 * perspective of the played item which can be a Live TV channel, an Network PVR recording, a VOD Movie trailer, etc.
 * Each type belongs to a <tt>PlaybackDomain</tt>.
 *
 * @author Dan Titiu
 * @since 29.04.2014
 */
public enum PlaybackType implements Parcelable {

    /**
     * Default playback type for non-active playback
     */
    Default(PlaybackDomain.None),

    /**
     * Type representing the playback of Live TV channels.
     */
    LiveTvDvbC(PlaybackDomain.LiveTvDvbC),

    /**
     * Type representing the playback of Live TV channels.
     */
    LiveTvDvbT(PlaybackDomain.LiveTvDvbT),

    /**
     * Type representing the playback of Live TV channels.
     */
    LiveTvIpTv(PlaybackDomain.LiveTvIpTv),

    /**
     * Type representing the playback of Live TV channels.
     */
    LiveTvOtt(PlaybackDomain.LiveTvOtt),

    /**
     * Type representing the playback of Replay content.
     */
    Replay(PlaybackDomain.Replay),
    /**
     * Type representing the playback of radio content.
     */
    RadioIpTv(PlaybackDomain.RadioIpTv),

    /**
     * Type representing the playback of radio content.
     */
    RadioOtt(PlaybackDomain.RadioOtt),

    /**
     * Type representing the playback of recording content.
     */
    Recording(PlaybackDomain.Recording),

    /**
     * Type representing the playback of a VOD Movie trailer.
     */
    VodTrailer(PlaybackDomain.Trailer),

    /**
     * Type representing the playback of VOD Movie feature (the full movie).
     */
    Vod(PlaybackDomain.Vod),

    /**
     * Type representing the playback of a GDPR video.
     */
    Gdpr(PlaybackDomain.Gdpr),

    /**
     * Type representing a started 3rd party application.
     */
    App(PlaybackDomain.App),

    TrickPlay(PlaybackDomain.TrickPlay);


    /**
     * Constructor that requires the PlaybackDomain.
     *
     * @param domain    The <tt>PlaybackDomain</tt> to which this type belongs to.
     */
    PlaybackType(@NonNull PlaybackDomain domain) {
        this.domain = domain;
    }

    @NonNull
    public PlaybackDomain getDomain() {
        return domain;
    }

    //
    // PARCELABLE SPECIFIC FIELDS AND METHODS
    //

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name());
    }

    public static final Creator<PlaybackType> CREATOR = new Creator<PlaybackType>() {
        @Override
        public PlaybackType createFromParcel(final Parcel source) {
            final String src = source.readString();
            return src != null ? PlaybackType.valueOf(src) : null;
        }

        @Override
        public PlaybackType[] newArray(final int size) {
            return new PlaybackType[size];
        }
    };

    //
    // PRIVATE METHODS AND FIELDS
    //

    /**
     * The <tt>PlaybackDomain</tt> to which this type belongs to.
     */
    @NonNull
    private final PlaybackDomain domain;
}
