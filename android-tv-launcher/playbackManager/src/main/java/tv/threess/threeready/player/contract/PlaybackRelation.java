/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.contract;

/**
 * Enumeration defining relations between playback actions and commands having those actions.
 * These relations define how the commands behave when added to a queue with other pending commands.
 *
 * @author Dan Titiu
 * @since 09.04.2014
 */
public enum PlaybackRelation {

    /**
     * No relation between the actions. This is the default relation and allows the commands to be queued together.
     */
    None,

    /**
     * The command about to be queued will cancel all commands in the queue having actions that relate in this way.
     */
    CancelOther,

    /**
     * The command about to be queued will cancel the first command in the queue having the action relate in this way
     * and will also cancel itself (will not be queued).
     */
    CancelBoth,

    /**
     * The command about to be queued will cancel itself when encountering a command already in the queue with the
     * action that relates in this way.
     */
    CancelSelf,

    /**
     * The command about to be queued will cancel all commands in the queue having actions that relate in this way
     * and will accumulate the data from the canceled commands (e.g. several Jumps accumulated into one).
     */
    Cumulate
}
