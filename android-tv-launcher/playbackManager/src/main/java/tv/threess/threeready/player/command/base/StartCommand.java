/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command.base;

import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.player.contract.CommandValidity;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackRelation;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.result.EventResult;

/**
 * Abstract start PlaybackCommand that is the super of all PlaybackCommands that start playback.
 *
 * @author Dan Titiu
 * @since 25.06.2014
 */
public abstract class StartCommand extends ExplicitCommand {

    protected final StartAction mStartAction;
    protected volatile int mRetryCount = 0;

    protected StartCommand(long id, PlaybackType type, StartAction startAction, PlaybackControl<?> control) {
        super(id, type, PlaybackAction.Start, control, PlaybackState.Started);
        mStartAction = startAction;
    }

    protected StartCommand(long id, PlaybackType type, StartAction startAction,
                           PlaybackControl<?> control, PlaybackAction action, PlaybackState targetState) {
        super(id, type, action, control, targetState);
        mStartAction = startAction;
    }

    @Override
    public PlaybackRelation relate(PlaybackCommand other, boolean executing) {
        // there is no relation between fallback commands (same command id)
        if (this.id == other.getId()) {
            return PlaybackRelation.None;
        }

        // starting the same tune twice, cancels the first.
        if (executing && other instanceof StartCommand) {
            StartCommand cmd = (StartCommand) other;
            if (control == cmd.control && type == cmd.type) {
                Comparable<?> id = playbackId();
                if (id != null && id.equals(cmd.playbackId())) {
                    return PlaybackRelation.CancelOther;
                }
            }
        }
        return super.relate(other, executing);
    }

    @Override
    public CommandValidity validate(PlaybackEntireState ds) {
        // Start commands require Stopped state for other playback domains.
        if (this.verify(ds)) {
            return CommandValidity.Valid;
        }

        // Generally starting on a different exclusive domain requires the other exclusive domains to be stopped
        return CommandValidity.TransitionalStopped;
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        // Start commands for exclusive domains require Stopped state for other exclusive playback domains.
        // In case that we are on the same domain, we still inject a reset so it clears the internal data in the control.
        return !this.getDomain().exclusive // This command is not exclusive
                || !ds.exclusiveState.active; // No active exclusive domain
    }

    @Override
    public boolean discards(EventResult eventResult) {
        super.discards(eventResult);
        return eventResult != null && eventResult.domain == getDomain() && eventResult.event.mDiscardable;
    }


    /**
     * @return The user action which triggered the start.
     */
    public StartAction getStartAction() {
        return mStartAction;
    }

    @Override
    public void commit() {
        super.commit();
        this.control.applyState(this.targetState);
        this.control.applyDetails(this);
        this.mRetryCount = 0;
    }

    @Override
    @CallSuper
    public void execute() throws Exception {
        super.execute();
        control.reset();
    }

    public int getBitrate() {
        return 0;
    }

    public boolean isLive() {
        return false;
    }

    @Override
    public void rollback(PlaybackError error, @Nullable Long nextCmdId) throws Exception {
        if (nextCmdId == null || !nextCmdId.equals(getId())) {
            // there is no fallback command, rollback start and fail
            control.rollback();
        }
        super.rollback(error, nextCmdId);
    }

    /**
     * Creates a copy from the original command, so it can be executed again (restart the last started playback).
     * In some cases the original command and the copied command can have different behavior.
     * @param cid new command id.
     * @return a new command cloned from this.
     */
    public abstract StartCommand createReTuneCommand(StartAction action, long cid);

    public int getRetryCount() {
        return mRetryCount;
    }

    public void setRetryCount(int value) {
        mRetryCount = value;
    }

    /**
     * A unique id for the playback, based on it, the latest from
     * two consecutive start commands with the same id can be ignored.
     * @return unique id
     */
    public abstract Comparable<?> playbackId();
}
