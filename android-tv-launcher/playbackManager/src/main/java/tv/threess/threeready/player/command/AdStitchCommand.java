package tv.threess.threeready.player.command;

import tv.threess.threeready.api.tv.model.TvAdReplacement;
import tv.threess.threeready.api.middleware.model.Scte35Info;
import tv.threess.threeready.player.command.base.EventCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.DtvControlProximus;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.result.EventFailure;
import tv.threess.threeready.player.result.EventResult;
import tv.threess.threeready.player.result.Failure;
import tv.threess.threeready.player.result.Result;

public class AdStitchCommand extends EventCommand {
    private final DtvControlProximus mControl;
    private final Scte35Info mMarker;
    private volatile TvAdReplacement mVastResponse;

    public AdStitchCommand(PlaybackControl<?> control, Scte35Info marker) {
        super(PlaybackEvent.StitchAd, PlaybackType.LiveTvIpTv, PlaybackAction.Select, control, control.getCurrentState());
        mControl = validateControl(control, DtvControlProximus.class);
        mMarker = marker;
    }

    @Override
    public EventResult getEventResult(Result result) {
        if (result instanceof Failure) {
            return new EventFailure(this.event, getDomain(), (Failure) result);
        }
        return new EventResult(this.event, this.getDomain());
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        return this.control.getCurrentState().active;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        mControl.replaceAd(this);
    }

    public void setVastResponse(TvAdReplacement adReplacement) {
        mVastResponse = adReplacement;
    }

    public Scte35Info getMarker() {
        return mMarker;
    }

    public TvAdReplacement getVastResponse() {
        return mVastResponse;
    }
}
