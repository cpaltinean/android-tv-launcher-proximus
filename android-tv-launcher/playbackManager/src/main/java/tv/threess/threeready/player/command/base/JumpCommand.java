package tv.threess.threeready.player.command.base;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.result.EventResult;

/**
 * Base class for jump commands.
 *
 * @author Zsolt Bokor_
 * @since 2020.07.24
 */
public abstract class JumpCommand extends ImplicitCommand {

    private final StartAction mStartAction;

    /**
     * Default constructor.
     *
     * @param id          Unique identifier for each command.
     * @param injectedBy  Playback command was injected executing this command.
     * @param action      Playback action to be taken on the appropriate PlaybackControl.
     * @param targetState Playback state that is reached once this command is successfully executed.
     */
    public JumpCommand(long id, StartAction startAction, PlaybackCommand injectedBy,
                       PlaybackAction action, PlaybackState targetState) {
        super(id, injectedBy, action, targetState);
        mStartAction = startAction;
    }

    public boolean isRewind() {
        return mStartAction == StartAction.Rewind;
    }

    @Nullable
    public StartAction getStartAction() {
        return mStartAction;
    }

    @Override
    public boolean discards(EventResult eventResult) {
        super.discards(eventResult);
        return eventResult != null && eventResult.domain == getDomain() && eventResult.event.mDiscardable;
    }
}
