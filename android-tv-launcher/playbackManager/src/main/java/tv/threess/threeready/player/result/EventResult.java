/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.result;

import androidx.annotation.Nullable;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.contract.PlaybackEvent;

/**
 * PlaybackEvent wrapper that carries a PlaybackEvent to be dispatched to the PlaybackInterface by the ResultDispatcher.
 *
 * @author Dan Titiu
 * @since 01.08.2014
 */
public class EventResult extends AbstractResult {
    private static final String TAG = Log.tag(EventResult.class);

    /**
     * Playback event triggered from background that needs to be dispatched to PlaybackInterface.
     */
    public final PlaybackEvent event;

    /**
     * The PlaybackDomain on which the event was triggered.
     */
    public final PlaybackDomain domain;

    public EventResult(PlaybackEvent event, PlaybackDomain domain) {
        this.event = event;
        this.domain = domain;
    }

    @Override
    public boolean matches(PlaybackCommand cmd) {
        return false;
    }

    @Override
    public void pairWith(PlaybackCommand cmd) {
        // Playback events do not pair with commands
    }

    @Override
    public void apply(PlaybackCommand cmd, @Nullable Long nextCmdId) {
        // Playback events do not apply commands
    }

    @Override
    public boolean isFavorable() {
        return true;
    }

    @Override
    public void dispatchTo(IPlaybackCallback callback) {
        try {
            callback.onPlaybackEvent(this.event, this.details, null);
        } catch (Exception e) {
            Log.e(TAG, "Failed to dispatch event [" + this.event + "]", e);
        }
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{event[" + this.event + "],domain[" + this.domain + "]}";
    }
}
