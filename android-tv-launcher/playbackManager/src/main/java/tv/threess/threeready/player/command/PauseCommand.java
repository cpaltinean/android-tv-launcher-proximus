/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command;

import tv.threess.threeready.player.command.base.ImplicitCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.model.PlaybackEntireState;

/**
 * Implicit command that transition the state to Paused.
 *
 * @author Dan Titiu
 * @since 13.08.2014
 */
public class PauseCommand extends ImplicitCommand {

    public PauseCommand(long id) {
        this(id, PlaybackAction.Pause, PlaybackState.Paused);
    }

    protected PauseCommand(long id, PlaybackAction playbackAction, PlaybackState playbackState) {
        super(id, null, playbackAction, playbackState);
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        // Pausing is valid on the same domain, while active and not Paused
        final PlaybackState state = control.getCurrentState();
        switch (state) {
            case Paused:
                return false;
            default:
                return state.active;
        }
    }

    @Override
    protected void executeImplicit() throws Exception {
        control.pause(this.getId());
    }

    @Override
    public void commit() {
        super.commit();
        this.control.applyState(this.targetState);
    }
}
