/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command.start;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.LiveOttControlProximus;
import tv.threess.threeready.player.control.base.PlaybackControl;

/**
 * StartCommand that knows how to resolve and start an ott type channel playback.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.06.06
 */
public class OttLiveStartCommand extends BroadcastStartCommand {

    private final LiveOttControlProximus mControl;
    private final TvChannel mChannel;

    public OttLiveStartCommand(long id, PlaybackType type, PlaybackControl control,
                               StartAction startAction, TvChannel channel, IBroadcast broadcast) {
        super(id, type, startAction, control, broadcast);
        mControl = validateControl(control, LiveOttControlProximus.class);
        mChannel = channel;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        mControl.start(this);
    }

    public TvChannel getChannel() {
        return mChannel;
    }

    @Override
    public boolean isLive() {
        return true;
    }

    @Override
    public String toString() {
        return super.toString("channelId", mChannel.getId());
    }

    @Override
    public OttLiveStartCommand createReTuneCommand(StartAction action, long cid) {
        return new OttLiveStartCommand(cid, type, control, action, mChannel,  getContentItem());
    }

    @Override
    public Comparable<?> playbackId() {
        return mChannel.getId();
    }

    @Override
    public IBroadcast getContentItem() {
        return null;
    }
}
