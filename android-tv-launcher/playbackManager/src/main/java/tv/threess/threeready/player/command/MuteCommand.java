package tv.threess.threeready.player.command;

import tv.threess.threeready.player.command.base.ImplicitCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.model.PlaybackEntireState;

/**
 * Playback command to mute the volume of the current playback.
 *
 * @author Barabas Attila
 * @since 2019.07.11
 */
public class MuteCommand extends ImplicitCommand {

    public MuteCommand(long id) {
        super(id, null, PlaybackAction.Volume, PlaybackState.None);
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        // Selecting a media track is valid as long as the player is active
        return control.getCurrentState().active;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o) && o instanceof MuteCommand;
    }

    @Override
    protected void executeImplicit() throws Exception {
        control.mute(getId());
    }

    @Override
    public void commit() {
        super.commit();
    }
}
