/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.result;

import java.util.Arrays;

import androidx.annotation.Nullable;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.exception.PlaybackError;

/**
 * Unfavorable Result for an executed command that matches the command upon domain and action(s).
 *
 * @author Dan Titiu
 * @since 21.06.2014
 */
public class VagueFailure extends VagueResult implements Failure {
    private static final String TAG = Log.tag(VagueFailure.class);

    private final PlaybackError error;

    public VagueFailure(PlaybackDomain domain, PlaybackAction action, PlaybackError error) {
        super(domain, action);
        this.error = error;
    }

    public VagueFailure(PlaybackDomain domain, PlaybackAction action, PlaybackError.Type reason) {
        this(domain, action, new PlaybackError(reason));
    }

    @Override
    public void dispatchTo(IPlaybackCallback callback) {
        if (this.cid <= 0) {
            throw new IllegalStateException("Trying to dispatch callback without command id");
        }

        try {
            callback.onCommandFailure(this.cmd, this.details, getError());
        } catch (Exception e) {
            Log.e(TAG, "Failed to dispatch failure for command id[" + cid + "]", e);
        }
    }

    @Override
    public void apply(PlaybackCommand cmd, @Nullable Long nextCmdId) throws Exception {
        cmd.rollback(this.error, nextCmdId);
    }

    @Override
    public boolean isFavorable() {
        return false;
    }

    @Override
    public PlaybackError getError() {
        return error;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{domain[" + this.domain + "],action"
                + Arrays.toString(this.action) + ",error[" + this.error + "]}";
    }
}
