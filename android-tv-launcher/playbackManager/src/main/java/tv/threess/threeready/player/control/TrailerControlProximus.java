package tv.threess.threeready.player.control;

import android.content.Context;
import android.media.tv.TvView;

import tv.threess.threeready.api.playback.model.IOttStreamingSession;
import tv.threess.threeready.api.playback.model.OTTStreamingSessionContainer;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.command.start.TrailerStartCommand;
import tv.threess.threeready.player.control.base.SessionBasedControlProximus;

/**
 * Playback controller for VOD OTT trailer playback.
 *
 * @author Barabas Attila
 * @since 2020.07.14
 */
public class TrailerControlProximus extends SessionBasedControlProximus<IOttStreamingSession,
        OTTStreamingSessionContainer<IOttStreamingSession>> {

    public TrailerControlProximus(Context context, PlaybackDomain domain, TvView tvView) {
        super(context, domain, tvView, new OTTStreamingSessionContainer<>());
    }

    /**
     * Start trailer playback on the OTT input service.
     */
    public void start(TrailerStartCommand command) throws Exception {
        command.assertNotCancelled();
        IVodItem vod = command.getVodItem();
        IVodVariant variant = command.getContentItem();
        Long relativePosition = command.getPosition();

        IOttStreamingSession session = mSessionHandler
                .openTrailerStreamingSession(vod, variant);

        command.assertNotCancelled();
        start(command.getId(), 0, 0, null, relativePosition, session);
    }

    @Override
    public long getContentStart() {
        return 0;
    }

    @Override
    public long getContentEnd() {
        return mStreamDuration;
    }

    @Override
    public String getPlaybackId() {
        return null;
    }
}
