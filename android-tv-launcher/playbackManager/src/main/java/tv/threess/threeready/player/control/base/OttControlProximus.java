/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.control.base;

import android.content.Context;
import android.media.tv.TvContract;
import android.media.tv.TvTrackInfo;
import android.media.tv.TvView;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.PlaybackSettings;
import tv.threess.threeready.api.generic.model.InputServiceInfo;
import tv.threess.threeready.api.generic.model.TimeRange;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.GenericEvent;
import tv.threess.threeready.api.log.model.GenericKeyEvent;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.PlaybackManager;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.inputservice.OttInputService.OttSession;
import tv.threess.threeready.player.model.MediaTrackInfo;
import tv.threess.threeready.player.model.OttPlayListItem;
import tv.threess.threeready.player.model.OttTuneParams;
import tv.threess.threeready.player.result.ExactSuccess;
import tv.threess.threeready.player.result.VagueSuccess;
import tv.threess.threeready.player.wrapper.IPlayerWrapper;
import tv.threess.threeready.player.wrapper.ITifPlayerWrapper;

/**
 * Playback controller for ott streams.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.05.29
 */
public abstract class OttControlProximus extends BaseTvViewControl {

    private static final long DEFAULT_TIMEOUT = TimeUnit.SECONDS.toMillis(25);

    private final String mOttInputId;
    private final Uri mOttInputUri;

    private static final String AUDIO_TYPE_DOLBY = "ac-3";
    private static final String AUDIO_TYPE_DOLBY_PLUS = "ec-3";

    protected final PlaybackSettings mPlaybackSettings = Components.get(PlaybackSettings.class);
    private final InputServiceInfo mServiceInfo = Components.get(InputServiceInfo.class);

    /**
     * The duration of the stream in milliseconds.
     */
    protected long mStreamDuration;

    /**
     * The index of the current playlist item.
     */
    private OttPlayListItem mCurrentPlaylistItem;

    /**
     * The tune parameters for which the playback was originally started.
     */
    protected OttTuneParams mTuneParams;

    public OttControlProximus(Context context, PlaybackDomain domain, TvView tvView) {
        super(context, domain, tvView);
        mOttInputId = mServiceInfo.getOttInputId();
        mOttInputUri = TvContract.buildChannelUri(0); // Dummy channel id. The OTT channels are not in the TI.
    }

    /**
     * Start OTT playback on the embedded OTT TV input service.
     *
     * @param cid              The id of the command which triggered the start.
     * @param contentStartTime The start time of the content which needs to start.
     * @param contentEndTime   The end time of the content which needs to start.
     * @param tuneParams       The parameters necessary to start the OTT playback. E.g stream url, licence url etc.
     */
    public void start(long cid, long contentStartTime, long contentEndTime, OttTuneParams tuneParams) throws Exception {
        mTuneParams = tuneParams;

        Bundle params = new Bundle();
        params.putParcelable(OttSession.EXTRA_TUNE_PARAMS, tuneParams);
        params.putParcelable(OttSession.EXTRA_NOTIFICATION_RECEIVER, mPlaybackResultReceiver);
        mCurrentPlaylistItem = tuneParams.getDefaultPlaylistItem();

        start(cid, contentStartTime, contentEndTime, mOttInputId, mOttInputUri, params);
    }

    /**
     * Prepend the current playlist with items for continuous playback.
     *
     * @param cid           The identifier of the command which triggered the prepend.
     * @param prependParams The playback items to prepend the playlist with.
     */
    public void prepend(long cid, ArrayList<OttPlayListItem> prependParams) throws Exception {
        if (prependParams == null || prependParams.isEmpty()) {
            // Empty append params. Nothing to do.
            offerResult(new ExactSuccess(cid));
            return;
        }

        blockingPostOnUiThread(() -> {
            Log.d(TAG, "TvView.sendAppPrivateCommand -> prepend.");
            Bundle extras = new Bundle();
            extras.putParcelableArrayList(OttSession.EXTRA_PLAYLIST_PARAMS, prependParams);
            mPlaybackView.sendAppPrivateCommand(OttSession.ACTION_PREPEND, extras);
        });
    }

    /**
     * Append the current playlist with items for continuous playback.
     *
     * @param cid          The identifier of the command which triggered the append.
     * @param appendParams The playback items to append the playlist with.
     */
    public void append(long cid, ArrayList<OttPlayListItem> appendParams) throws Exception {
        if (appendParams == null || appendParams.isEmpty()) {
            // Empty append params. Nothing to do.
            offerResult(new ExactSuccess(cid));
            return;
        }

        blockingPostOnUiThread(() -> {
            Log.d(TAG, "TvView.sendAppPrivateCommand -> Append.");
            Bundle extras = new Bundle();
            extras.putParcelableArrayList(OttSession.EXTRA_PLAYLIST_PARAMS, appendParams);
            mPlaybackView.sendAppPrivateCommand(OttSession.ACTION_APPEND, extras);
        });
    }

    @Override
    public void applyContentPositions(long contentStartTime, long contentEndTime) {
        super.applyContentPositions(contentStartTime, contentEndTime);
        mStreamDuration = contentEndTime - contentStartTime;
    }

    @Override
    public void startTrickPlay(long cid, double playbackSpeed, BaseSurfaceControl trickPlayControl) throws Exception {
        super.startTrickPlay(cid, playbackSpeed, trickPlayControl);
        removeCallbacks();
        // Pause the playback if needed
        if (getCurrentState() == PlaybackState.Started) {
            pause(-1);
        }

        // Trick play is already started. Set speed.
        if (trickPlayControl.getCurrentState().active) {
            trickPlayControl.setSpeed(cid, playbackSpeed);
            return;
        }

        // Start the stream in trick play thumbnail.
        OttTuneParams activeTuneParams = getTuneParams();
        OttTuneParams.Builder trickPlayTuneParamBuilder = new OttTuneParams.Builder()
                .playListIndex(activeTuneParams.getDefaultItemIndex())
                .startPosition(activeTuneParams.getStartPosition());

        List<OttPlayListItem> playListItems = activeTuneParams.getPlayListItems();
        for (OttPlayListItem playListItem : playListItems) {
            trickPlayTuneParamBuilder.addPlayListItem(
                    playListItem.buildUpon()
                            .playbackSpeed((float) playbackSpeed)
                            .playbackDomain(PlaybackDomain.TrickPlay)
                            .build());
        }

        trickPlayControl.start(cid, trickPlayTuneParamBuilder.build());
    }

    @Override
    public void stopTrickPlay(long cid, BaseSurfaceControl trickPlayControl) throws Exception {
        super.stopTrickPlay(cid, trickPlayControl);
        // If the playback was paused by us, resume the playback
        trickPlayControl.stop(cid);
        if (getCurrentState() == PlaybackState.TrickPlay) {
            resume(-1);
        }
    }

    @Override
    public long getDispatchTimeout() {
        long timeout = Settings.ottPlaybackTimeout.get(0);
        if (timeout > 0) {
            return timeout;
        }

        PlaybackSettings playbackSettings = Components.get(PlaybackSettings.class);
        timeout = playbackSettings.getOttPlaybackTimeout(TimeUnit.MILLISECONDS);
        if (timeout > 0) {
            return timeout;
        }

        return DEFAULT_TIMEOUT;
    }

    @Override
    public long getSeekAbleEnd() {
        return Math.min(getContentEnd(), System.currentTimeMillis());
    }

    public OttPlayListItem getCurrentPlaylistItem() {
        return mCurrentPlaylistItem;
    }

    protected MediaTrackInfo decorateMediaTrackInfo(TvTrackInfo tvTrackInfo) {
        MediaTrackInfo.SurroundType surroundType = MediaTrackInfo.SurroundType.None;
        boolean descriptive = false;

        Bundle extras = tvTrackInfo.getExtra();
        if (extras != null) {
            if (TRACK_EXTRA_SUBTITLE_TYPE_HARD_OF_HEARING.equals(extras.getString(TRACK_EXTRA_SUBTITLE_TYPE, ""))) {
                descriptive = true;
            }

            String audio = extras.getString(TRACK_EXTRA_AUDIO_TYPE, "");
            if (!TextUtils.isEmpty(audio) && !TRACK_EXTRA_AUDIO_TYPE_NORMAL.equals(audio)) {
                descriptive = true;
            }

            Object streamType = extras.get(PlaybackManager.EXTRA_STREAM_TYPE);
            if (AUDIO_TYPE_DOLBY_PLUS.equals(streamType)) {
                surroundType = MediaTrackInfo.SurroundType.DolbyDigitalPlus;
            } else if (AUDIO_TYPE_DOLBY.equals(streamType)) {
                surroundType = MediaTrackInfo.SurroundType.DolbyDigital;
            }
        }

        return new MediaTrackInfo(tvTrackInfo.getId(), tvTrackInfo.getLanguage(),
                tvTrackInfo.getType(), descriptive, surroundType);
    }

    @Override
    public void reset() {
        mStreamDuration = 0;
        mCurrentPlaylistItem = null;
        mTuneParams = null;
        super.reset();
    }

    @Override
    protected PlaybackError.Type getExceptionType(int reason) {
        if (reason == IPlayerWrapper.VIDEO_UNAVAILABLE_REASON_ENDED) {
            // We throw this error code directly in case of end reached from the player
            // This has to be handled separately
            return PlaybackError.Type.VIDEO_UNAVAILABLE_REASON_ENDED;
        }
        if ((reason & ~ITifPlayerWrapper.VIDEO_UNAVAILABLE_ERROR_MASK) == ITifPlayerWrapper.VIDEO_UNAVAILABLE_ERROR_FLAG) {
            return PlaybackError.Type.getFromErrorCode(reason & ITifPlayerWrapper.VIDEO_UNAVAILABLE_ERROR_MASK);
        }
        return super.getExceptionType(reason);
    }

    /**
     * The tune parameters for which the playback was originally started.
     */
    public OttTuneParams getTuneParams() {
        return mTuneParams;
    }

    @Override
    protected void onVideoUnavailable(String inputId, int reason) {
        if (reason == IPlayerWrapper.VIDEO_UNAVAILABLE_REASON_ENDED) {
            if (state == PlaybackState.Stopped) {
                // In case the control is stopped and we get a video ended for the previous event, we can ignore the event
                return;
            }
            if (state == PlaybackState.Started) {
                offerEvent(PlaybackEvent.EndReached);
                return;
            }
        }

        super.onVideoUnavailable(inputId, reason);
    }

    /**
     * Receiver to get extra notifications for the OTT input service which are not supported by TIF.
     */
    // IPC CALLBACK
    protected final ResultReceiver mPlaybackResultReceiver = new ResultReceiver(new Handler(Looper.getMainLooper())) {
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            switch (resultCode) {
                case ITifPlayerWrapper.NOTIFY_PLAYER_STATE_CHANGE:
                    OttControlProximus.super.offerEvent(PlaybackEvent.PlayerStateChanged, resultData);
                    break;

                case ITifPlayerWrapper.NOTIFY_SEGMENT_DOWNLOADED:
                    OttControlProximus.super.offerEvent(PlaybackEvent.SegmentDownloaded, resultData);
                    break;

                case ITifPlayerWrapper.NOTIFY_BITRATE_CHANGE:
                    OttControlProximus.super.offerEvent(PlaybackEvent.BitrateChanged, resultData);
                    break;

                case ITifPlayerWrapper.NOTIFY_PLAYBACK_EXCEPTION_RECEIVED:
                    Log.event(new Event<>(GenericEvent.PlaybackException)
                            .copyDetail(GenericKeyEvent.KEY, resultData)
                            .copyDetail(GenericKeyEvent.MESSAGE, resultData)
                    );
                    break;

                case ITifPlayerWrapper.NOTIFY_PLAYLIST_ITEM_CHANGED:
                    resultData.setClassLoader(getClass().getClassLoader());
                    mCurrentPlaylistItem = resultData.getParcelable(ITifPlayerWrapper.EXTRA_PLAYLIST_ITEM);
                    Log.d(TAG, "Playlist item changed : " + mCurrentPlaylistItem);
                    break;

                case ITifPlayerWrapper.NOTIFY_DURATION_CHANGE:
                    mStreamDuration = resultData.getLong(ITifPlayerWrapper.EXTRA_PLAYBACK_DURATION, 0);
                    Log.d(TAG, "onReceiveResult() called with: ITifPlayerWrapper.NOTIFY_DURATION_CHANGE = [" + mStreamDuration + "]");
                    break;

                case ITifPlayerWrapper.NOTIFY_PLAYBACK_ITEM_ADDED:
                    OttControlProximus.super.offerResult(new VagueSuccess(getDomain(), PlaybackAction.Add));
                    break;

                case ITifPlayerWrapper.NOTIFY_UNSKIPPABLE_TIMELINE_CHANGED:
                    long[] starts = resultData.getLongArray(ITifPlayerWrapper.EXTRA_UNSKIPPABLE_STARTS);
                    long[] ends = resultData.getLongArray(ITifPlayerWrapper.EXTRA_UNSKIPPABLE_ENDS);
                    mUnskippableRanges = new TimeRange[starts.length];
                    for (int i = 0; i < starts.length; i++) {
                        long start = convertPlaybackToContentPosition(starts[i]);
                        long end = convertPlaybackToContentPosition(ends[i]);
                        mUnskippableRanges[i] = new TimeRange(start, end);
                    }
                    break;
            }
        }
    };
}
