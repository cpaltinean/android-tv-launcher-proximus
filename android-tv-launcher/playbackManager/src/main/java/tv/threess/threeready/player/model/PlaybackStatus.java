/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.model;

import android.os.Parcel;
import android.os.Parcelable;

import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;

/**
 * Parcelable DTO that stores current PlaybackState and StreamType.
 *
 * @author Dan Titiu
 * @since 21.06.2014
 */
public class PlaybackStatus implements Parcelable {

    public PlaybackDomain getDomain() {
        return this.type != null ? this.type.getDomain() : null;
    }

    public PlaybackType getType() {
        return this.type;
    }

    public PlaybackState getState() {
        return this.state;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append("{type[");
        sb.append(this.type);
        sb.append("],state[");
        sb.append(this.state);
        sb.append("]}");
        return sb.toString();
    }

    //
    // PARCELABLE SPECIFIC FIELDS AND METHODS
    //

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // Order is critical!
        dest.writeParcelable(this.type, flags);
        dest.writeParcelable(this.state, flags);
    }

    public static final Creator<PlaybackStatus> CREATOR = new Creator<PlaybackStatus>() {
        @Override
        public PlaybackStatus createFromParcel(final Parcel source) {
            return new PlaybackStatus(source);
        }

        @Override
        public PlaybackStatus[] newArray(final int size) {
            return new PlaybackStatus[size];
        }
    };

    //
    // PRIVATE METHODS AND FIELDS
    //

    protected PlaybackType type;

    protected PlaybackState state;

    protected PlaybackStatus() {}

    protected PlaybackStatus(final Parcel source) {// at least package-private for inner-class optimization
        // Order is critical!
        this.type = source.readParcelable(PlaybackType.class.getClassLoader());
        this.state = source.readParcelable(PlaybackState.class.getClassLoader());
    }
}
