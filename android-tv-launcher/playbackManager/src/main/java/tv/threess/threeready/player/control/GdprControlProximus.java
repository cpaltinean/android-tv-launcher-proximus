package tv.threess.threeready.player.control;

import android.content.Context;
import android.media.tv.TvView;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.model.IVodOttStreamingSession;
import tv.threess.threeready.api.playback.model.OTTStreamingSessionContainer;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.exception.VodAlreadyRentedException;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.command.start.GdprStartCommand;
import tv.threess.threeready.player.control.base.SessionBasedControlProximus;

/**
 * Playback controller for GDPR videos.
 *
 * @author Zsolt Bokor_
 * @since 2020.11.17
 */
public class GdprControlProximus extends SessionBasedControlProximus<IVodOttStreamingSession, OTTStreamingSessionContainer<IVodOttStreamingSession>> {

    private final VodRepository mVodRepository = Components.get(VodRepository.class);

    private IVodVariant mVariant;

    public GdprControlProximus(Context context, PlaybackDomain domain, TvView tvView) {
        super(context, domain, tvView, new OTTStreamingSessionContainer<>());
    }

    /**
     * Start VOD playback on the OTT input service.
     */
    public void start(GdprStartCommand command) throws Exception {
        command.assertNotCancelled();
        IVodItem vodItem = command.getVodItem();
        IVodVariant variant = command.getContentItem();
        IVodPrice price = command.getVodPrice();

        if (!variant.isRented()) {
            try {
                mVodRepository.rentVodItem(vodItem, variant, price);
            } catch (VodAlreadyRentedException e) {
                Log.w(TAG, "VOD was already rented.", e);
            }
        }

        command.assertNotCancelled();

        IVodOttStreamingSession session = mSessionHandler
                .openVodStreamingSession(vodItem, variant);

        command.assertNotCancelled();
        mVariant = variant;
        mIsResumedBookmark = false;

        start(command.getId(), 0, variant.getDuration(), null, 0L, session);
    }

    @Override
    public long getContentStart() {
        return 0;
    }

    @Override
    public long getContentEnd() {
        return mStreamDuration;
    }

    @Override
    public void reset() {
        super.reset();
        mVariant = null;
    }

    @Override
    public String getPlaybackId() {
        return mVariant == null ? null : mVariant.getId();
    }
}
