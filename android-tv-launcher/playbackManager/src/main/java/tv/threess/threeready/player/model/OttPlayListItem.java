package tv.threess.threeready.player.model;

import android.os.Parcel;
import android.os.Parcelable;

import tv.threess.threeready.api.playback.player.PlaybackDomain;

/**
 * Model represents a single item in the OTT player playlist.
 *
 * @author Barabas Attila
 * @since 2020.06.29
 */
public class OttPlayListItem implements Parcelable {
    private String mStreamUrl;
    private String mLicenceUrl;
    private String mDrmClientId;
    private PlaybackDomain mPlaybackDomain;
    private float mPlaybackSpeed = 1f;

    private OttPlayListItem() {
    }

    public Builder buildUpon() {
        return new Builder()
                .streamUrl(mStreamUrl)
                .licenceUrl(mLicenceUrl)
                .drmClientId(mDrmClientId)
                .playbackDomain(mPlaybackDomain)
                .playbackSpeed(mPlaybackSpeed);
    }

    /**
     * @return The url for HLS or DASH manifest.
     */
    public String getStreamUrl() {
        return mStreamUrl;
    }

    /**
     * @return The DRM licence URL if available.
     */
    public String getLicenceUrl() {
        return mLicenceUrl;
    }

    /**
     * @return The identifier of the DRM client.
     */
    public String getDrmClientId() {
        return mDrmClientId;
    }

    /**
     * @return The domain in which the playback was triggered.
     * E.g. replay, recording etc.
     */
    public PlaybackDomain getPlaybackDomain() {
        return mPlaybackDomain;
    }

    /**
     * @return The speed of the playback to start.
     * Where 1.0 is the normal speed.
     */
    public float getPlaybackSpeed() {
        return mPlaybackSpeed;
    }

    @Override
    public String toString() {
        return "OttPlayListItem{" +
                "mStreamUrl='" + mStreamUrl + '\'' +
                ", mPlaybackDomain=" + mPlaybackDomain +
                ", mPlaybackSpeed=" + mPlaybackSpeed +
                '}';
    }

    /**
     * Builder to create new append params with it.
     */
    public static class Builder {
        private final OttPlayListItem mParams;

        public Builder() {
            mParams = new OttPlayListItem();
        }

        public Builder streamUrl(String streamUrl) {
            mParams.mStreamUrl = streamUrl;
            return this;
        }

        public Builder licenceUrl(String licenceUrl) {
            mParams.mLicenceUrl = licenceUrl;
            return this;
        }

        public Builder drmClientId(String drmClientId) {
            mParams.mDrmClientId = drmClientId;
            return this;
        }

        public Builder playbackDomain(PlaybackDomain playbackDomain) {
            mParams.mPlaybackDomain = playbackDomain;
            return this;
        }

        public Builder playbackSpeed(float playbackSpeed) {
            mParams.mPlaybackSpeed = playbackSpeed;
            return this;
        }

        public OttPlayListItem build() {
            return mParams;
        }
    }

    protected OttPlayListItem(Parcel in) {
        mStreamUrl = in.readString();
        mLicenceUrl = in.readString();
        mDrmClientId = in.readString();
        mPlaybackDomain = in.readParcelable(PlaybackDomain.class.getClassLoader());
        mPlaybackSpeed = in.readFloat();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mStreamUrl);
        dest.writeString(mLicenceUrl);
        dest.writeString(mDrmClientId);
        dest.writeParcelable(mPlaybackDomain, flags);
        dest.writeFloat(mPlaybackSpeed);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OttPlayListItem> CREATOR = new Creator<OttPlayListItem>() {
        @Override
        public OttPlayListItem createFromParcel(Parcel in) {
            return new OttPlayListItem(in);
        }

        @Override
        public OttPlayListItem[] newArray(int size) {
            return new OttPlayListItem[size];
        }
    };
}
