/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command.base;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.model.CommandDecorator;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.model.PlaybackStatusBuilder;

/**
 * Superclass for commands that rely implicitly on the presumably active {@link PlaybackDomain} and {@link PlaybackType}
 * to execute and commit their state. Their borrow the {@link PlaybackType} and {@link PlaybackControl} from the last
 * processed command.
 *
 * @author Dan Titiu
 * @since 21.07.2014
 */
public abstract class ImplicitCommand extends AbstractCommand {
    private static final String TAG = Log.tag(ImplicitCommand.class);

    /**
     * Playback type defining also the domain on which to take action. Helps identifying the PlaybackControl.
     */
    protected PlaybackType type;

    /**
     * Playback control onto which the command will execute.
     */
    protected PlaybackControl control;

    // injected cause
    protected final PlaybackCommand injectedBy;

    /**
     * The timeout in milliseconds for dispatching this command resolved from the control on resolve step.
     */
    protected long timeout;

    /**
     * Default constructor.
     *
     * @param id        Unique identifier for each command.
     * @param injectedBy   Playback command was injected executing this command.
     * @param action    Playback action to be taken on the appropriate PlaybackControl.
     * @param targetState   Playback state that is reached once this command is successfully executed.
     */
    public ImplicitCommand(final long id, PlaybackCommand injectedBy, final PlaybackAction action, final PlaybackState targetState) {
        super(id, action, targetState);
        this.injectedBy = injectedBy;
    }

    @Override
    public PlaybackType getType() {
        return this.type;
    }

    @Override
    public PlaybackDomain getDomain() {
        if (this.type == null) {
            return null;
        }
        return this.type.getDomain();
    }

    @Override
    public long getTimeout() {
        return this.timeout;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        if (this.control == null) {
            throw new IllegalStateException("Execute called on implicit command without prepare");
        }
        this.executeImplicit();
    }

    @Override
    public void decorate(PlaybackStatusBuilder status) {
        status.setType(this.type);
    }

    @Override
    public void decorate(PlaybackDetailsBuilder details) {
        details.setType(this.type);
    }

    /**
     * Implicit commands lack the {@code StreamType} and {@code PlaybackControl} as they rely on the last processed
     * command implicitly. This method prepares the implicit commands with the {@code StreamType} and
     * {@code PlaybackControl} and possibly decorates (wraps) this command into a domain specific wrapper command.
     *
     * @param type       The {@code StreamType} implicitly chosen for this command.
     * @param control    The {@code PlaybackControl} implicitly chosen for this command.
     * @return The prepared instance of PlaybackCommand.
     */
    public PlaybackCommand prepare(final PlaybackType type, final PlaybackControl control) {
        assert this.type == null;// Prepare only once
        this.type = type;
        this.control = control;
        // Try to decorate if decorator exists and is registered
        try {
            final DecoratorKey key = new DecoratorKey(this.type.getDomain(), this.getClass());
            final CommandDecorator<ImplicitCommand> decorator;
            if ((decorator = getDecorator(key)) != null) {
                Log.d(TAG, "Found decorator for " + key);
                return decorator.decorate(this);
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to decorate " + this, e);
        }

        this.timeout = control.getDispatchTimeout();

        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (super.equals(o) && o instanceof ImplicitCommand) {
            ImplicitCommand other = (ImplicitCommand) o;
            return this.type == other.type && this.control == other.control && this.getClass().equals(other.getClass());
        }

        return false;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        if (this.type != null) {
            sb.append("{type[");
            sb.append(this.type);
            sb.append("],id[");
        } else {
            sb.append("{id[");
        }
        if (this.isSynthetic()) {
            sb.append("synthetic");
        } else {
            sb.append(this.id);
        }
        sb.append("]}");
        return sb.toString();
    }


    protected abstract void executeImplicit() throws Exception;

    public PlaybackCommand getInjectedBy() {
        return this.injectedBy;
    }

    //
    // STATIC DECORATING IMPLEMENTATION
    //

    /**
     * Key uniquely identifying a CommandDecorator by its PlaybackDomain and PlaybackCommand class, useful to be used as
     * a key in a Map.
     */
    private static class DecoratorKey {

        private final PlaybackDomain domain;

        private final Class<? extends PlaybackCommand> commandClass;

        private int hashCode;

        DecoratorKey(PlaybackDomain domain, Class<? extends PlaybackCommand> cmdCls) {// package-private for inner-class optimization
            this.domain = domain;
            this.commandClass = cmdCls;
        }

        @Override
        public int hashCode() {
            int hashCode = this.hashCode;
            if (hashCode == 0) {
                hashCode = this.domain.hashCode();
                hashCode = (31 * hashCode) + this.commandClass.hashCode();
                this.hashCode = hashCode;
            }
            return hashCode;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof DecoratorKey)) {
                return false;
            }
            DecoratorKey other = (DecoratorKey) o;

            return this.commandClass.equals(other.commandClass) && this.domain.equals(other.domain);
        }

        @Override
        public String toString() {
            return "Key{" + this.domain.toString() + "/" + this.commandClass.getSimpleName() + "}";
        }
    }


    private static final Map<DecoratorKey, CommandDecorator> DECORATORS = new HashMap<>(4);

    /**
     * Retrieves a PlaybackCommand decorator based on a key, type-safely.
     *
     * @param key    DecoratorKey uniquely identifying the decorator.
     * @param <T>    Type inferred be the caller that must be a subclass of ImplicitCommand.
     * @return the PlaybackCommand decorator based on a key, type-safely.
     */
    @SuppressWarnings("unchecked")
    private static <T extends ImplicitCommand> CommandDecorator<T> getDecorator(final DecoratorKey key) {
        return DECORATORS.get(key);
    }

    /**
     * Statically register a CommandDecorator into the {@code ImplicitCommand}s registry. Decorators registered here are
     * looked up and used in the PlaybackCommand prepare step, wrapping the prepared {@code ImplicitCommand}.
     *
     * @param domain       PlaybackDomain by which the CommandDecorator is looked up.
     * @param cmdCls       PlaybackCommand class by which the CommandDecorator is looked up.
     * @param decorator    CommandDecorator instance associated with the given {@code PlaybackDomain} and {@code PlaybackCommand} class.
     * @param <T>          Type which binds the decorator and the command class together to ensure consistency.
     */
    public static <T extends ImplicitCommand> void registerDecorator(final PlaybackDomain domain, final Class<T> cmdCls,
                                                                     @NonNull final CommandDecorator<T> decorator) {
        DECORATORS.put(new DecoratorKey(domain, cmdCls), decorator);
    }
}
