package tv.threess.threeready.player;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.model.TrackInfo;
import tv.threess.threeready.player.model.MediaTrackInfo;


/**
 * Subtitle an Audio track manager class for tv view Control
 *
 * @author Solyom Zsolt
 * @since 2020.07.31
 */
public class TrackManager implements Component {
    private final String TAG = Log.tag(TrackManager.class);

    // Selected tracks by user
    private volatile TrackInfo mSelectedAudioTrack = null;
    private volatile TrackInfo mSelectedSubtitleTrack = null;

    // Unique playback id
    private volatile String mPlaybackId = null;

    public TrackManager() {
    }

    public void setAudioTrack(String playbackId, TrackInfo track) {
        resetIfNeeded(playbackId);

        mPlaybackId = playbackId;
        mSelectedAudioTrack = track;
        Log.d(TAG, "setAudioTrack() called, PlaybackID: " + mPlaybackId + " Language: " + track);
    }

    public void setSubtitleTrack(String playbackId, TrackInfo track) {
        resetIfNeeded(playbackId);

        mPlaybackId = playbackId;
        mSelectedSubtitleTrack = track;
        Log.d(TAG, "setSubtitleTrack() called, PlaybackID: " + mPlaybackId + " Language: " + track);
    }

    public TrackInfo getSelectedSubtitleTrack(String playbackId) {
        resetIfNeeded(playbackId);
        return mSelectedSubtitleTrack;
    }

    public TrackInfo getSelectedAudioTrack(String playbackId) {
        resetIfNeeded(playbackId);
        return mSelectedAudioTrack;
    }

    public void resetIfNeeded(String playbackId) {
        if (playbackId == null || !playbackId.equals(mPlaybackId)) {
            Log.d(TAG, "Resetting all tracks to default!");
            mSelectedAudioTrack = mSelectedSubtitleTrack = null;
        }
    }

    public void setVideoTrack(String playbackId, MediaTrackInfo language) {
        // nada
    }
}
