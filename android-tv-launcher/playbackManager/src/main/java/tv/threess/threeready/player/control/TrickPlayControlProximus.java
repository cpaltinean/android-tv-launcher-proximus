package tv.threess.threeready.player.control;

import android.content.Context;
import android.media.tv.TvTrackInfo;
import android.view.SurfaceView;

import java.util.List;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.listener.PlayerWrapperListener;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.control.base.BaseSurfaceControl;
import tv.threess.threeready.player.model.OttTuneParams;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.result.ExactSuccess;
import tv.threess.threeready.player.result.VagueSuccess;

/**
 * Playback control for trickplay streams.
 * <p>
 * Here we have to be provided by a specific streaming url and license url, since we do not start a session by ourselves.
 * For the same reason we do not close a streaming session either when the trickplay session has ended.
 *
 * @author Andor Lukacs
 * @since 10/25/18
 */
public class TrickPlayControlProximus extends BaseSurfaceControl implements PlayerWrapperListener {
    private static final String TAG = Log.tag(TrickPlayControlProximus.class);

    private static final int TRICKPLAY_DISPATCH_TIMEOUT = 15000;

    public TrickPlayControlProximus(Context context, PlaybackDomain domain, SurfaceView playbackView) {
        super(context, domain, playbackView);
    }

    @Override
    public void jump(long cid, long position) {

    }

    @Override
    protected String getStreamUrl() {
        return "";
    }

    @Override
    public void start(long cid, OttTuneParams tuneParams) throws Exception {
        Log.d(TAG, "start() called with: cid = [" + cid + "], tuneParams = [" + tuneParams + "]");
        mPlayerWrapper.setPlayerWrapperListener(this);
        super.start(cid, tuneParams);
    }

    @Override
    public void stop(long cid) throws Exception {
        // In case STOP is called when there is no surface attached, do nothing
        if (mPlaybackView != null) {
            super.stop(cid);
        }
    }

    @Override
    public void selectSubtitleTrack(final long cid, final String trackId) {
        // Subtitles are not supported by the basic surface view
        Log.w(TAG, "Subtitles are not supported in trickplay.");
    }

    @Override
    public void selectAudioTrack(final long cid, final String trackId) throws Exception {
        blockingPostOnUiThread(() -> {
            Log.d(TAG, "SurfaceView.selectTrack");
            mPlayerWrapper.setAudioTrack(trackId);
            offerResult(new ExactSuccess(cid));
        });
    }

    @Override
    public void notifyVideoAvailable() {
        if (!getCurrentState().active) {  // check if controller is already active ( playback started )
            offerResult(new VagueSuccess(getDomain(), PlaybackAction.TrickPlay, PlaybackAction.TrickPlay));
            offerEvent(PlaybackEvent.PlaybackStarted);
        }
    }

    @Override
    public long getDispatchTimeout() {
        return TRICKPLAY_DISPATCH_TIMEOUT;
    }

    @Override
    protected void fillMediaTrackInfo(PlaybackDetailsBuilder details) {
    }

    @Override
    public void notifyVideoUnavailable(int reason) {
        Log.d(TAG, "notifyVideoUnavailable() called with: reason = [" + reason + "]");
    }

    @Override
    public void notifyTrackSelected(int type, String id) {
        Log.d(TAG, "notifyTrackSelected() called with: type = [" + type + "], id = [" + id + "]");
    }

    @Override
    public void notifyTracksChanged(List<TvTrackInfo> tracks) {
        Log.d(TAG, "notifyTracksChanged() called with: tracks = [" + tracks + "]");
    }

    @Override
    public void mute(long cid) {
        Log.d(TAG, "mute() called ");
    }

    @Override
    public void unMute(long cid) {
        Log.d(TAG, "unMute() called ");
    }
}
