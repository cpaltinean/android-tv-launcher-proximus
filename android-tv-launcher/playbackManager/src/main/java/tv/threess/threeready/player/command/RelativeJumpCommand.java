/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.JumpCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.model.PlaybackEntireState;

/**
 * Implicit command that transition the state to Paused.
 *
 * @author Dan Titiu
 * @since 13.08.2014
 */
public class RelativeJumpCommand extends JumpCommand {
    private static final String TAG = Log.tag(RelativeJumpCommand.class);

    private long mDuration;

    public RelativeJumpCommand(long id, StartAction startAction, long duration) {
        super(id, startAction,null, PlaybackAction.RelativeJump, PlaybackState.Started);
        mDuration = duration;
    }

    public long getDuration() {
        return mDuration;
    }

    @Override
    public void accumulate(PlaybackCommand other) {
        if (other instanceof RelativeJumpCommand) {
            mDuration += ((RelativeJumpCommand) other).mDuration;
        } else {
            Log.w(TAG, "Trying to accumulate RelativeJumpCommand with " + other.getClass().getSimpleName());
        }
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        // Jumping is valid on the same domain, while not Stopped
        return control.getCurrentState().active;
    }

    @Override
    protected void executeImplicit() throws Exception {
        control.jump(id, control.getCurrentPosition() + mDuration);
    }

    @Override
    public boolean equals(Object o) {
        if (super.equals(o) && o instanceof RelativeJumpCommand) {
            RelativeJumpCommand other = (RelativeJumpCommand) o;
            return other.mDuration == mDuration;
        }

        return false;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.replace(sb.length() - 1, sb.length(), ",mDuration[");
        sb.append(Log.formatDuration(mDuration));
        sb.append("]}");
        return sb.toString();
    }
}
