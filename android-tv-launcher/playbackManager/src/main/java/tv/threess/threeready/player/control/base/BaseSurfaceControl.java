package tv.threess.threeready.player.control.base;

import android.content.Context;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.model.OttTuneParams;
import tv.threess.threeready.player.result.ExactSuccess;
import tv.threess.threeready.player.wrapper.ISurfacePlayerWrapper;


/**
 * Base control for direct playback on specific surfaces.
 * This control does not use TIF, therefore the playback controls pass through directly to the player wrapper.
 * <p>
 * This is useful when there is a secondary control to be active next to the TIF one or when
 * a lightweight control is needed, ignoring the TIF and handling the playback in the main process.
 *
 * @author Andor Lukacs
 * @since 11/23/18
 */
public abstract class BaseSurfaceControl extends PlaybackControl<SurfaceView> {
    private final String TAG = Log.tag(getClass());

    protected final ISurfacePlayerWrapper mPlayerWrapper = Components.get(ISurfacePlayerWrapper.class);

    public BaseSurfaceControl(Context context, PlaybackDomain domain, SurfaceView playbackView) {
        super(context, domain, playbackView);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (mPlaybackView != null) {
            mPlaybackView.getHolder().addCallback(mSurfaceHolderCallback);
        }
    }

    public void start(long cid, OttTuneParams tuneParams) throws Exception {
        blockingPostOnUiThread(() -> {
            mPlayerWrapper.play(tuneParams);
        });
    }

    @Override
    public void stop(final long cid) throws Exception {
        blockingPostOnUiThread(() -> {
            Log.d(TAG, "SurfaceView.reset");
            mPlayerWrapper.release();
            offerResult(new ExactSuccess(cid));
        });
    }

    @Override
    public void pause(final long cid) throws Exception {
        blockingPostOnUiThread(() -> {
            Log.d(TAG, "SurfaceView.pause");
            mPlayerWrapper.pause();
            offerResult(new ExactSuccess(cid));
        });
    }

    @Override
    public void resume(final long cid) throws Exception {
        blockingPostOnUiThread(() -> {
            Log.d(TAG, "SurfaceView.resume");
            mPlayerWrapper.resume();
            offerResult(new ExactSuccess(cid));
        });
    }

    @Override
    public void setSpeed(long cid, double playbackSpeed) throws Exception {
        blockingPostOnUiThread(() -> {
            Log.d(TAG, "setSpeedStep() called with: cid = [" + cid + "], step = [" + playbackSpeed + "]");
            mPlayerWrapper.setSpeed(mPlayerWrapper.getSpeed() + playbackSpeed);
            offerResult(new ExactSuccess(cid));
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mPlaybackView != null) {
            mPlaybackView.getHolder().removeCallback(mSurfaceHolderCallback);
        }
    }

    @Override
    protected boolean canPause() {
        return false;
    }

    // Detect surface changes and dispatch them to the player.
    private final SurfaceHolder.Callback mSurfaceHolderCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            if (mPlayerWrapper != null) {
                mPlayerWrapper.setSurface(holder.getSurface());
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            if (mPlayerWrapper != null) {
                mPlayerWrapper.setSurface(holder.getSurface());
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            if (mPlayerWrapper != null) {
                mPlayerWrapper.setSurface(null);
            }
        }
    };
}
