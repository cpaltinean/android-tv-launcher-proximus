/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.contract;

/**
 * The outcome of a {@code PlaybackCommand} validation, used before executing the command.
 *
 * @author Dan Titiu
 * @since 28.07.2014
 */
public enum CommandValidity {

    /**
     * Command is valid and can be executed directly.
     */
    Valid,

    /**
     * Command is invalid and should not be executed in current domain and state.
     */
    Invalid,

    /**
     * Command can be executed (is valid) only after a transition is made into the target state for the current domain.
     */
    TransitionalStopped(PlaybackState.Stopped),

    /**
     * Command can be executed (is valid) only after a transition is made into the target state for the current domain.
     */
    TransitionalInactive(PlaybackState.Inactive),

    /**
     * Command can be executed (is valid) only after a transition is made into the target state for the current domain.
     */
    TransitionalPlaying(PlaybackState.Started);

    /**
     * The playback state which a transitional validity should target.
     * Only valid for Transitional validity, null otherwise.
     */
    public final PlaybackState targetState;

    //
    // PRIVATE METHODS AND FIELDS
    //

    CommandValidity() {
        this.targetState = null;
    }

    CommandValidity(PlaybackState targetState) {
        this.targetState = targetState;
    }
}
