/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command.start;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.RecordingControlProximus;
import tv.threess.threeready.player.control.base.PlaybackControl;

/**
 * Start recording for a broadcast command.
 *
 * @author Barabas Attila
 * @since 2018.10.24
 */
public class RecordingStartCommand extends ContentItemStartCommand<IRecording> {

    private final RecordingControlProximus mRecordingControl;
    private final IRecording mRecording;
    private final IRecordingSeries mSeries;
    private final Long mPosition;

    public RecordingStartCommand(long id, PlaybackType type, PlaybackControl control,
                                 StartAction startAction, IRecording recording, IRecordingSeries series, Long position) {
        super(id, type, startAction, control);
        mRecordingControl = validateControl(control, RecordingControlProximus.class);
        mRecording = recording;
        mSeries = series;
        mPosition = position;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        mRecordingControl.start(this);
    }

    public Long getPosition() {
        return mPosition;
    }

    @Override
    public RecordingStartCommand createReTuneCommand(StartAction action, long cid) {
        return new RecordingStartCommand(cid, type, control, action, mRecording, mSeries, null);
    }

    @Override
    public Comparable<?> playbackId() {
        if (mRecording == null) {
            return null;
        }
        return mRecording.getRecordingId();
    }

    @Override
    public IRecording getContentItem() {
        return mRecording;
    }

    public IRecordingSeries getRecordingSeries() {
        return mSeries;
    }
}
