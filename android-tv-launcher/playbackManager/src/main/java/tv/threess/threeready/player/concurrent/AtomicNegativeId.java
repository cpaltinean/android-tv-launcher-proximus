/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.concurrent;

import java.util.concurrent.atomic.AtomicLong;

/**
 * A {@code long} value that may be used as consecutive identifier generator and which eventually wraps around when
 * reaching {@code Long.MIN_VALUE}. It only emits negative values.
 *
 * @see AtomicLong
 * @author Dan Titiu
 * @since 06/03/2017
 */
public class AtomicNegativeId extends AtomicLong {

    private static final long serialVersionUID = -6755361461806162961L;

    public AtomicNegativeId() {
        super(0L);
    }

    public AtomicNegativeId(long initialValue) {
        super(initialValue);
    }

    public long getNextId() {
        for (;;) {
            long current = get();
            long next = current - 1;
            // Wrap around for negative only
            if (next > 0L) {
                next = -1L;
            }
            if (compareAndSet(current, next)) {
                return next;
            }
        }
    }
}
