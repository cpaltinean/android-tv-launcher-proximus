/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.model;

import tv.threess.threeready.player.command.base.PlaybackCommand;

/**
 * TODO: Javadoc
 *
 * @author Dan Titiu
 * @since 26.08.2014
 */
public interface CommandDecorator<T extends PlaybackCommand> {


    PlaybackCommand decorate(T obj);
}
