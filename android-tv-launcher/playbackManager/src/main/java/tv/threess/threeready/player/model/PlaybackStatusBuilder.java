/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.model;

import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;

/**
 * TODO: Javadoc
 *
 * @author Dan Titiu
 * @since 21.06.2014
 */
public class PlaybackStatusBuilder extends PlaybackStatus {

    public PlaybackStatusBuilder() {
        super();
    }

    public PlaybackStatusBuilder setType(PlaybackType type) {
        this.type = type;
        return this;
    }

    public PlaybackStatusBuilder setState(PlaybackState state) {
        this.state = state;
        return this;
    }

    public PlaybackStatus build() {
        return this;
    }

    /**
     * Compares this instance with another given instance and returns true if the two instances are equal.
     * Used mainly during UnitTests.
     *
     * @param o    the object to compare this instance with.
     * @return {@code true} if the specified object is equal to this {@code
     *         Object}; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object o) {
        // Shortcut for same instance
        if (this == o) {
            return true;
        }
        // Validate type
        if (!(o instanceof PlaybackStatusBuilder)) {
            return false;
        }
        // Check fields
        PlaybackStatusBuilder other = (PlaybackStatusBuilder) o;
        return this.type == other.type && this.state == other.state;
    }
}
