/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.model;

import java.util.EnumMap;
import java.util.Map;

import androidx.annotation.NonNull;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;

/**
 * State of all active PlaybackControls with their current {@link PlaybackType}.
 *
 * @author Dan Titiu
 * @since 08.08.2014
 */
public class PlaybackEntireState {

    @NonNull
    public PlaybackState exclusiveState = PlaybackState.Stopped;

    public PlaybackType exclusiveType;

    public final Map<PlaybackType,PlaybackState> inclusive = new EnumMap<>(PlaybackType.class);

    public PlaybackEntireState() {
    }

    /**
     * Checks if the exclusive StreamType/PlaybackDomain equals the one given as parameter.
     * Helper method that avoids boilerplate.
     *
     * @param domain    The PlaybackDomain to match.
     * @return True if the exclusive PlaybackDomain equals the given one.
     */
    public boolean equalsExclusiveDomain(PlaybackDomain domain) {
        return this.exclusiveType != null && this.exclusiveType.getDomain().equals(domain);
    }

    /**
     * Resets the instance as if it was freshly created. Useful for components that reuse the same instance over their
     * lifecycle.
     */
    public void recycle() {
        this.exclusiveState = PlaybackState.Stopped;
        this.exclusiveType = null;
        this.inclusive.clear();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.exclusiveType);
        sb.append("=");
        sb.append(this.exclusiveState);
        for (Map.Entry<PlaybackType,PlaybackState> e : this.inclusive.entrySet()) {
            sb.append(";");
            sb.append(e.getKey());
            sb.append("=");
            sb.append(e.getValue());
        }
        return sb.toString();
    }
}
