package tv.threess.threeready.player.control;

import android.content.Context;
import android.media.tv.TvView;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.NumberUtils;
import tv.threess.threeready.api.generic.model.BookmarkType;
import tv.threess.threeready.api.generic.model.ContinueWatchingConfig;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.model.IVodOttStreamingSession;
import tv.threess.threeready.api.playback.model.OTTStreamingSessionContainer;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.exception.VodAlreadyRentedException;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.command.start.VodStartCommand;
import tv.threess.threeready.player.control.base.SessionBasedControlProximus;

/**
 * Playback controller for VOD OTT playback.
 *
 * @author Barabas Attila
 * @since 2020.07.14
 */
public class VodControlProximus extends SessionBasedControlProximus<IVodOttStreamingSession,
        OTTStreamingSessionContainer<IVodOttStreamingSession>> {

    private final VodRepository mVodRepository = Components.get(VodRepository.class);

    private IVodItem mVodItem;
    private IVodVariant mVariant;

    public VodControlProximus(Context context, PlaybackDomain domain, TvView tvView) {
        super(context, domain, tvView, new OTTStreamingSessionContainer<>());
    }

    /**
     * Start VOD playback on the OTT input service.
     */
    public void start(VodStartCommand command) throws Exception {
        command.assertNotCancelled();
        IVodItem vodItem = command.getVodItem();
        IVodVariant variant = command.getContentItem();
        IVodPrice price = command.getVodPrice();

        if (!variant.isRented()) {
            try {
                mVodRepository.rentVodItem(vodItem, variant, price);
            } catch (VodAlreadyRentedException e) {
                Log.w(TAG, "VOD was already rented.", e);
            }
        }

        command.assertNotCancelled();

        IVodOttStreamingSession session = mSessionHandler
                .openVodStreamingSession(vodItem, variant);

        command.assertNotCancelled();
        mVodItem = vodItem;
        mVariant = variant;

        Long relativePosition = command.getPosition();
        IBookmark bookmark = mBookmarkRepository.getBookmark(variant, BookmarkType.VodVariant);

        // no start position (no start-over), fallback to bookmark position
        if (relativePosition == null && bookmark.getPosition() > 0) {
            double maxWatchedPercentage = Components.get(ContinueWatchingConfig.class).getVodWatchedMaxPercent();
            boolean isNearEnd = bookmark.isNearEnd(maxWatchedPercentage);
            relativePosition = isNearEnd ? 0 : bookmark.getPosition();
            mIsResumedBookmark = true;
        } else {
            mIsResumedBookmark = false;
        }

        start(command.getId(), 0, variant.getDuration(), null, relativePosition, session);
    }

    @Override
    public long getContentStart() {
        return 0;
    }

    @Override
    public long getContentEnd() {
        return mStreamDuration;
    }

    @Override
    public boolean isBingeWatchingEnabled() {
        return getBufferEnd() - getCurrentPosition() <=
                mPlaybackSettings.getBingeWatchingDisplayTime(TimeUnit.MILLISECONDS);
    }

    @Override
    public void reset() {
        saveBookmark();
        super.reset();
        mVodItem = null;
        mVariant = null;
    }

    @Override
    public String getPlaybackId() {
        return mVariant == null ? null : mVariant.getId();
    }

    private void saveBookmark() {
        long start = getContentStart();
        long end = getContentEnd();
        long position = NumberUtils.clamp(getCurrentPosition(), start, end);
        mBookmarkRepository.addVodBookmark(mVodItem, mVariant,
                position - start, end - start)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }
}
