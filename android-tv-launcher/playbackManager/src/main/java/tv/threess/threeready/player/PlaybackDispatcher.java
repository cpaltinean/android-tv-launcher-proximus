/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import tv.threess.threeready.player.command.base.ImplicitCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.result.EventResult;

/**
 * Interface for the dispatcher of PlaybackCommands.
 *
 * @author Dan Titiu
 * @since 16/02/2017
 */
public interface PlaybackDispatcher {

    /**
     * Returns true if the given event should be discarded. This usually is decided by the {@link PlaybackCommand}
     * currently being dispatched.
     *
     * @param eventResult The {@link EventResult} to be checked against the current {@link PlaybackCommand}.
     * @return true if the given event should be discarded.
     */
    boolean discardEvent(EventResult eventResult);

    /**
     * Returns the last command executed with a favorable result. In other words, the last command that had an impact on
     * the player state. The return value of this method may be used as a fallback for preparing an
     * {@link ImplicitCommand}.
     *
     * @return the last command executed with a favorable result.
     */
    PlaybackCommand getLastCommand();

    /**
     * Returns the last command that was failed in order to attempt a restore of it in cases where the environment changed
     * and it can result in a successful commmand e.g. in case of lost/restored network connection
     * @return
     */
    PlaybackCommand getLastFailedCommand();

    /**
     * Returns the current command that is being dispatched.
     * Dispatching command means it's undergoing one of the following steps:
     * validate, pre-execute, execute, match, pair, apply, decorate, post-execute.
     *
     * @return  The command that is undergoing dispatching.
     */
    PlaybackCommand getDispatchingCommand();
}
