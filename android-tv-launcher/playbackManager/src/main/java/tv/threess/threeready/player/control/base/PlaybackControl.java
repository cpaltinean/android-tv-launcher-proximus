/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.control.base;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;

import java.lang.ref.WeakReference;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.generic.helper.NumberUtils;
import tv.threess.threeready.api.generic.model.TimeRange;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.FeedbackConsumer;
import tv.threess.threeready.player.Lifecycleable;
import tv.threess.threeready.player.command.base.EventCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.model.PlaybackStatus;
import tv.threess.threeready.player.model.PlaybackStatusBuilder;
import tv.threess.threeready.player.result.EventFailure;
import tv.threess.threeready.player.result.EventResult;
import tv.threess.threeready.player.result.ExactFailure;
import tv.threess.threeready.player.result.ExactSuccess;
import tv.threess.threeready.player.result.Result;

/**
 * Interface for all PlaybackControls.
 *
 * @author Dan Titiu
 * @since 25.04.2014
 */
public abstract class PlaybackControl<TPlaybackView extends View> implements Lifecycleable {
    protected final String TAG = Log.tag(getClass());

    //
    // PLAYBACK SPECIFIC METHODS
    //

    /**
     * Called when he control is created.
     * The control initialization should be done here.
     */
    @Override
    public void onCreate() {
    }

    /**
     * Start the playback in control.
     *
     * @param cid              The id of the command which triggered the start.
     * @param contentStartTime The start time of the content which needs to start.
     * @param contentEndTime   The end time of the content which needs to start.
     */
    protected void start(long cid, long contentStartTime, long contentEndTime) {
        applyContentPositions(contentStartTime, contentEndTime);
    }

    /**
     * Cleanup the control in case a start command fails.
     */
    public void rollback() throws Exception {
        stop(-1);
    }


    /**
     * Stop the currently running playback.
     *
     * @param cid The id of the command which triggered the stop.
     */
    public abstract void stop(long cid) throws Exception;

    /**
     * Pause the currently running playback.
     *
     * @param cid The of the command which triggered the pause.
     */
    public abstract void pause(long cid) throws Exception;

    /**
     * Resume the previously paused playback.
     *
     * @param cid The id of the command which triggered the resume.
     */
    public abstract void resume(long cid) throws Exception;

    /**
     * Set the playback speed for the current playback.
     *
     * @param cid           The id of the command which triggered the playback speed change
     * @param playbackSpeed The new speed for the playback. Where 1.0 the normal speed.
     */
    public abstract void setSpeed(long cid, double playbackSpeed) throws Exception;

    /**
     * Starts the trick play for the current playback.
     *
     * @param cid              The id of the command which triggered the start of the trick play.
     * @param playbackSpeed    Playback speed to be applied.
     * @param trickPlayControl Instance of trick play control to start the trick play.
     */
    public void startTrickPlay(long cid, double playbackSpeed, BaseSurfaceControl trickPlayControl) throws Exception {
        // Nothing to do here.
    }

    /**
     * Stops the trick play.
     *
     * @param cid The id of the command which triggered the stop of the trick play.
     */
    public void stopTrickPlay(long cid, BaseSurfaceControl trickPlayControl) throws Exception {
        // Nothing to do here.
    }

    /**
     * Jump to the given position in the current playback.
     *
     * @param cid      The id of the command which triggered the jump.
     * @param position THe absolute position in the content to jump.
     */
    public abstract void jump(long cid, long position) throws Exception;

    /**
     * Select a new audio track in the playback
     *
     * @param cid     The command which triggered the track selection.
     * @param trackId The id of the audio track which needs to be selected.
     */
    public abstract void selectAudioTrack(long cid, String trackId) throws Exception;

    /**
     * Select a new subtitle track in the playback.
     *
     * @param cid     The id of the command which triggered the track selection.
     * @param trackId The id of the subtitle which needs to be selected.
     */
    public abstract void selectSubtitleTrack(long cid, String trackId) throws Exception;

    /**
     * Mute the volume of the currently running playback.
     *
     * @param cid The id of the command which triggered the mute.
     */
    public abstract void mute(long cid) throws Exception;

    /**
     * Unmute the previously muted playback volume.
     *
     * @param cid The id of the command which triggered the unmute
     */
    public abstract void unMute(long cid) throws Exception;

    /**
     * Returns the PlaybackDomain which this control is associated with.
     *
     * @return The PlaybackDomain which this control is associated with.
     */
    public PlaybackDomain getDomain() {
        return this.domain;
    }

    /**
     * Current playback speed, has positive values for fast forward and negative values for rewind.
     * Negative value should represent reverse playback.
     * Zero should represent paused playback state.
     * One should represent normal playback.
     *
     * @return the current speed step.
     */
    public double getPlaybackSpeed() {
        if (this.state == PlaybackState.TrickPlay) {
            return this.playbackSpeed;
        }
        if (this.state == PlaybackState.Started) {
            return 1;
        }
        if (this.state == PlaybackState.Paused) {
            return 0;
        }
        Log.v(TAG, "state not recognized: " + state);
        return 0;
    }

    /**
     * Current playback position in milliseconds, usually updated by callback events from lower levels.
     * It is also updated during command execution for result anticipation (anticipate the result and adjust on callback
     * if needed).
     *
     * @return the current playback position in milliseconds.
     */
    public long getCurrentPosition() {
        long deltaUpdate = System.currentTimeMillis() - mLastJumpTime;
        return (long) (mCurrentPosition + deltaUpdate * getPlaybackSpeed());
    }

    /**
     * Returns the start of the slot where seeking is allowed.
     *
     * @return The timestamp that position can not underflow.
     */
    public long getSeekAbleStart() {
        return getContentStart();
    }

    /**
     * Returns the end of the slot where seeking is allowed.
     *
     * @return The timestamp that position can not overflow.
     */
    public long getSeekAbleEnd() {
        return getContentEnd();
    }

    /**
     * Returns the visible start of the given buffer.
     * In case of black list slots, or any other restrictions, the real blocking start position will be the "seekAbleStart".
     */
    public long getBufferStart() {
        return getSeekAbleStart();
    }

    /**
     * Returns the visible end of the given buffer.
     * In case of black list slots, or any other restrictions, the real blocking end position will be the "seekAbleEnd".
     */
    public long getBufferEnd() {
        return getSeekAbleEnd();
    }

    /**
     * Start of the current content in milliseconds, updated when executing a new StartCommand.
     */
    public long getContentStart() {
        return mContentStartPosition;
    }

    /**
     * End of the current content in milliseconds, updated when executing a new StartCommand.
     */
    public long getContentEnd() {
        return mContentEndPosition;
    }

    /**
     * Returns true when the playback is live (near to {@link System#currentTimeMillis()}).
     */
    public boolean isLive() {
        return false;
    }

    /**
     * @return True if the current playback was resumed from a playback bookmark.
     */
    public boolean isResumeBookmark() {
        return false;
    }

    /**
     * Method used to turn on/off the binge watching functionality.
     */
    public boolean isBingeWatchingEnabled() {
        return false;
    }

    /**
     * Generates an ExactSuccess for the PlaybackCommand without performing any action. Useful for PlaybackCommands that
     * need only to change the state.
     *
     * @param cid The ID of the command being executed.
     */
    public void confirm(long cid) {
        this.offerResult(new ExactSuccess(cid));
    }

    /**
     * Generates an {@link ExactFailure} for the PlaybackCommand without performing any action. Useful for Commands that are cancelled.
     *
     * @param cid The ID of the command being executed.
     */
    public void cancel(long cid) {
        this.offerResult(new ExactFailure(cid, PlaybackError.Type.CANCELED));
    }

    /**
     * Retrieve the current PlaybackState of the control.
     *
     * @return the current PlaybackState of the control
     */
    @NonNull
    public PlaybackState getCurrentState() {
        return this.state;
    }

    /**
     * Apply the new state that resulted after executing successfully a command.
     *
     * @param state The {@code PlaybackState} to which the control transitioned successfully.
     */
    public void applyState(PlaybackState state) {
        // notify player state changed
        if (getCurrentState() != state) {
            offerEvent(PlaybackEvent.PlayerStateChanged);
        }
        this.state = state;
    }

    /**
     * Apply the details of the new state that resulted after executing successfully a command. As the details are
     * encapsulated in the command itself, we store the last [start] command.
     *
     * @param cmd The {@link StartCommand} that made the transition and holds relevant information.
     */
    public void applyDetails(StartCommand cmd) {
        this.lastStartCommand = cmd;
    }

    /**
     * Called when the current position changes in the playback.
     * Can be used to trigger certain action based on the new position.
     *
     * @param contentPosition The new position in the playback.
     * @param notifyChange    Notify the change
     */
    protected void processPosition(long contentPosition, boolean notifyChange) {
        // Nothing to do here.
    }

    /**
     * Process the given position and trigger any event necessary for ot.
     * E.g EndReached, append needed etc.
     *
     * @param commandId       The id of the command which triggered the position validation.
     * @param contentPosition The position on the content which needs to be validated.
     */
    public void validatePosition(long commandId, long contentPosition) {
        processPosition(contentPosition, false);
        offerResult(new ExactSuccess(commandId));
    }

    /**
     * Convert the position coming from the player to position in the content.
     * The content position should be between {@link #getContentStart()} and {@link #getContentEnd()}
     *
     * @param playerPosition The position in the player.
     * @return The corresponding position in the content.
     */
    protected long convertPlaybackToContentPosition(long playerPosition) {
        return playerPosition;
    }

    /**
     * Convert the position in the content to the corresponding position in the playback,
     *
     * @param contentPosition The position in the content.
     * @return The corresponding position in the playback.
     */
    protected long convertContentToPlaybackPosition(long contentPosition) {
        return contentPosition;
    }

    /**
     * Applies the playback position in milliseconds for the active control.
     * Player calls this method periodically (in every sec) to update the playback position.
     * Method is called after a relative/absolute jump too, to force update the playback position.
     * Contains a logic to avoid updates from the player for maximum 5 sec, after a relative/absolute jump.
     *
     * @param contentPosition The current position in the content.
     */
    protected void applyPosition(long contentPosition, boolean force) {
        long pos = NumberUtils.clamp(contentPosition, getSeekAbleStart(), getSeekAbleEnd());
        long now = System.currentTimeMillis();

        // In case the playback is not active, skip updating the current position
        if (!force && state != PlaybackState.Started && state != PlaybackState.Paused
                && state != PlaybackState.TrickPlay && state != PlaybackState.TrickPlayPaused &&
                state != PlaybackState.Blanked) {
            return;
        }

        if (now - mLastJumpTime >= MAXIMUM_JUMP_TIMEOUT) {
            // Too much time since last update
            force = true;
        }

        if (!force && mCurrentPosition > 0) {
            // Difference between the current position in control and the position from the player.
            long deltaJump = this.mCurrentPosition - pos;
            // Time elapsed in millis from the last jump
            long deltaUpdate = now - mLastJumpTime;
            // An invalid jump can appear after a relative/absolute jump if position in control is different
            // from the position in player. Player needs time to execute the jump.
            // In this case we ignore the position updates from the player.
            if (Math.abs(deltaJump / getPlaybackSpeed()) > deltaUpdate + MAXIMUM_JUMP_ERROR) {
                Log.d(TAG, "invalid jump detected: " + Log.timestamp(pos) + ", from: " + Log.timestamp(mCurrentPosition)
                        + ", deltaJump: " + deltaJump + ", deltaUpdate: " + deltaUpdate);

                return;
            }
        }

        mCurrentPosition = pos;
        mLastJumpTime = now;

        processPosition(pos, true);
    }

    /**
     * Change the start and end time of the current content.
     * E.g live start end times may change when the position advances to the next broadcast
     *
     * @param contentStartTime The start time of the content in milliseconds.
     * @param contentEndTime   The end time of the content in milliseconds.
     */
    protected void applyContentPositions(long contentStartTime, long contentEndTime) {
        Log.d(TAG, "Apply content position -> contentStart : "
                + Log.timestamp(contentStartTime) + ", contentEnd : " + Log.timestamp(contentEndTime));
        mContentStartPosition = contentStartTime;
        mContentEndPosition = contentEndTime;
    }

    public boolean isUnskippablePosition(long position) {
        return TimeRange.remainingTime(mUnskippableRanges, position) > 0;
    }

    /**
     * Resets the internal state that was built during playback. May be used by PlaybackCommands after finishing a
     * playback session on the control (e.g. on stop).
     */
    public void reset() {
        // This method should perform some specific internal cleanup, so controls should override if needed.
        this.mLastJumpTime = System.currentTimeMillis();
        this.mCurrentPosition = Long.MIN_VALUE;
        this.mContentStartPosition = 0;
        this.mContentEndPosition = 0;
        this.playbackSpeed = 0;
    }

    /**
     * Offer exact result for clear.
     * TODO: might need other actions as well
     *
     * @param id Id of the command.
     */
    public void clear(long id) {
        offerResult(new ExactSuccess(id));
    }

    /**
     * Get status of playback for this control.
     *
     * @return The PlaybackStatus containing StreamType and PlaybackState.
     */
    public PlaybackStatus getStatus() {
        //TODO: Read volatile fields atomically (together)
        final PlaybackStatusBuilder status = new PlaybackStatusBuilder();
        final PlaybackCommand lc = this.lastStartCommand;
        if (lc != null) {
            lc.decorate(status);
        }
        return status.setState(this.state).build();
    }

    /**
     * Build and populate details of playback for this control.
     *
     * @return The PlaybackDetailsBuilder containing playback details.
     */
    public PlaybackDetailsBuilder getDetails() {
        //TODO: Read volatile fields atomically (together)
        final PlaybackDetailsBuilder builder = new PlaybackDetailsBuilder();

        // Command decorates with information encapsulated in it (e.g. StreamType)
        final StartCommand lc = this.lastStartCommand;
        // No last command means no active state to decorate with
        if (lc != null) {
            lc.decorate(builder);
            // Media Tracks
            this.fillMediaTrackInfo(builder);

            builder.setPosition(getCurrentPosition());

            // set BufferEnd before BufferStart to invalidate buffers that are a few milliseconds long.
            builder.setSeekAbleEnd(getSeekAbleEnd());
            builder.setBufferEnd(getBufferEnd());

            builder.setSeekAbleStart(getSeekAbleStart());
            builder.setBufferStart(getBufferStart());

            builder.setUnskippableRanges(mUnskippableRanges);

            builder.setLive(isLive());
            builder.setCanPause(canPause());
            builder.setResumeBookmark(isResumeBookmark());

            builder.setPlaybackSpeed(getPlaybackSpeed());
        }

        // active state not needed for these properties
        builder.setPlaybackUrl(getStreamUrl());
        builder.setStart(getContentStart());
        builder.setEnd(getContentEnd());

        // Control sets state
        return builder.setState(this.state);
    }

    /**
     * @return Flag which marks if the control is capable of executing pause action.
     */
    protected abstract boolean canPause();

    /**
     * Returns the timeout in milliseconds for dispatching a certain action specific for this control's domain.
     *
     * @return the timeout in milliseconds for dispatching a certain action specific for this control's domain.
     */
    public abstract long getDispatchTimeout();

    //
    // CONTROL SPECIFIC METHODS
    //

    protected PlaybackControl(Context context, PlaybackDomain domain, TPlaybackView playbackView) {
        this.context = new WeakReference<>(context);
        this.domain = domain;
        mPlaybackView = playbackView;
    }

    /**
     * Offer a result to the queue to be paired with the playback command.
     *
     * @param result Playback result obtained by consuming a command.
     */
    protected void offerResult(Result result) {
        if (feedbackConsumer == null)
            return;
        this.feedbackConsumer.offerCommandResult(result);
    }

    /**
     * Offer a PlaybackCommand generated from a background event.
     *
     * @param command PlaybackCommand generated from a background event.
     */
    protected void offerEventCommand(EventCommand command) {
        if (feedbackConsumer == null)
            return;
        this.feedbackConsumer.offerEventCommand(command);
    }

    /**
     * Offer a playback event to be dispatched on the PlaybackInterface.
     *
     * @param event PlaybackEvent to be dispatched.
     */
    protected void offerEvent(EventResult event, Bundle details) {
        if (feedbackConsumer == null) {
            return;
        }
        this.feedbackConsumer.offerEvent(event, details);
    }

    protected void offerEvent(PlaybackEvent event, Bundle details) {
        this.offerEvent(new EventResult(event, this.domain), details);
    }

    protected void offerEvent(PlaybackEvent event, PlaybackError error) {
        this.offerEvent(new EventFailure(event, this.domain, error), Bundle.EMPTY);
    }

    protected void offerEvent(PlaybackEvent event) {
        this.offerEvent(new EventResult(event, this.domain), Bundle.EMPTY);
    }

    /**
     * Fill the playback details with information about the currently available
     * and selected video, subtitle and audio track.
     *
     * @param details The playback details which needs to be decorated.
     */
    protected abstract void fillMediaTrackInfo(PlaybackDetailsBuilder details);

    /**
     * The view which holds the surface on which the video will be displayed.
     */
    protected TPlaybackView mPlaybackView;

    /**
     * The Android context.
     */
    protected final WeakReference<Context> context;

    /**
     * The PlaybackDomain which this control is associated with.
     */
    protected final PlaybackDomain domain;

    /**
     * The (start) PlaybackCommand that was last executed on this control. Saved here in order to save the (current)
     * playback details.
     */
    protected volatile StartCommand lastStartCommand;

    /**
     * The state of the playback kept for each domain / control.
     */
    @NonNull
    protected volatile PlaybackState state = PlaybackState.Released;

    /**
     * Current speed step, has positive values for fast forward and negative values for rewind. Zero means normal playback.
     */
    protected volatile double playbackSpeed = 0;

    /**
     * The position in milliseconds of the playback, it should be between {@code startPosition, endPosition}
     * The position is absolute utc timestamp position.
     */
    protected volatile long mCurrentPosition = Long.MIN_VALUE;

    /**
     * The position in milliseconds for the start of the current play-out, it should be less {@code System.currentTimeMillis()}
     * The position is absolute utc timestamp position.
     */
    protected volatile long mContentStartPosition = 0;

    /**
     * The position in milliseconds for the end of the current play-out, it should be greater {@code System.currentTimeMillis()}
     * The position is absolute utc timestamp position.
     */
    protected volatile long mContentEndPosition = 0;

    protected volatile TimeRange[] mUnskippableRanges = null;

    private static final long MAXIMUM_JUMP_TIMEOUT = TimeUnit.SECONDS.toMillis(5);
    private static final long MAXIMUM_JUMP_ERROR = TimeUnit.SECONDS.toMillis(1);
    private volatile long mLastJumpTime = 0;
    //
    // PRIVATE IMPLEMENTATION
    //

    /**
     * Single reference to which feedback of sorts (e.g. Results, Events, EventCommands) can be offered to.
     */
    private FeedbackConsumer feedbackConsumer;

    @Override
    public void onDestroy() {
        // Cleanup references and reset state.
        this.feedbackConsumer = null;
        this.lastStartCommand = null;
        this.state = PlaybackState.Stopped;
    }

    public void registerFeedbackConsumer(FeedbackConsumer feedbackConsumer) {
        this.feedbackConsumer = feedbackConsumer;
    }

    /**
     * @return The stream url of the current playback.
     */
    protected abstract String getStreamUrl();

    /**
     * Post an action to be executed on the main thread while the current thread waits for the result
     */
    @WorkerThread
    protected void blockingPostOnUiThread(Runnable runnable) throws Exception {
        if (Thread.currentThread().getId() == Looper.getMainLooper().getThread().getId()) {
            // We are on the main thread, execute the action here
            runnable.run();
            return;
        }

        FutureTask<Exception> execute = new FutureTask<>(() -> {
            try {
                runnable.run();
                return null;
            } catch (Exception e) {
                return e;
            }
        });

        // execute the action on the main thread
        mPlaybackView.post(execute);

        // Wait for the execution to finish only if we are not on the main thread
        Exception e = execute.get(getDispatchTimeout(), TimeUnit.MILLISECONDS);
        if (e != null) {
            throw e;
        }
    }
}
