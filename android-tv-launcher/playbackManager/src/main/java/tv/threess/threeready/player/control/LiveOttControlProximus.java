/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.control;

import android.content.Context;
import android.media.tv.TvView;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.playback.model.IOttStreamingSession;
import tv.threess.threeready.api.playback.model.OTTStreamingSessionContainer;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.player.command.start.OttLiveStartCommand;
import tv.threess.threeready.player.control.base.SessionBasedControlProximus;

/**
 * Playback controller for live ott streams.
 *
 * @author Barabas Attila
 * @since 2018.06.22
 */
public class LiveOttControlProximus extends SessionBasedControlProximus<IOttStreamingSession,
        OTTStreamingSessionContainer<IOttStreamingSession>> {

    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    private long mLiveEdgeMillis;

    private IBroadcast mBroadcast;

    public LiveOttControlProximus(Context context, PlaybackDomain domain, TvView tvView) {
        super(context, domain, tvView, new OTTStreamingSessionContainer<>());
    }

    /**
     * Start live playback for the give broadcast and channel.
     */
    public void start(OttLiveStartCommand command) throws Exception {
        command.assertNotCancelled();
        mBroadcast = command.getContentItem();
        // TODO: implement playback options
//            List<PlaybackOption> playbackOptions = TvDatabase.queryPlaybackOptions(context.get().getContentResolver(), broadcast.getChannelId(),
//                    mInternetChecker.isInternetAvailable(), mMwHandler.isCableConnected(), true, false
//            );
//            if (playbackOptions == null) {
//                throw new PlaybackException("No playback options for channel: "
//                        + broadcast.getChannelId(), PlaybackException.Type.NO_VALID_PLAYBACK_OPTION_AT_TUNE);
//            }
//
//            PlaybackOption playbackOption = playbackOptions.get(0);

        command.assertNotCancelled();

        mLiveEdgeMillis = Settings.ottPlayerLiveEdgeMs.get(mAppConfig.getPlaybackSettings().getLiveEdgeLatencyMs());
        IOttStreamingSession session = mSessionHandler.openChannelStreamingSession(command.getChannel().getId());
        start(command.getId(), mBroadcast.getStart(), mBroadcast.getEnd(), null, null, session);
    }

    @Override
    public long getCurrentPosition() {
        return System.currentTimeMillis() - mLiveEdgeMillis;
    }

    @Override
    public long getSeekAbleEnd() {
        return getCurrentPosition();
    }

    @Override
    public boolean isLive() {
        return true;
    }

    @Override
    public String getPlaybackId() {
        return mBroadcast == null ? null : mBroadcast.getChannelId();
    }
}
