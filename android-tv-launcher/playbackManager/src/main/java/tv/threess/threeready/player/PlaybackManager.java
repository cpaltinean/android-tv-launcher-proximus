/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.command.base.EventCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.ControlFactory;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.MediaTrackInfo;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.model.PlaybackStatus;
import tv.threess.threeready.player.model.PlaybackStatusBuilder;
import tv.threess.threeready.player.plugin.PlaybackPlugin;
import tv.threess.threeready.player.result.EventResult;
import tv.threess.threeready.player.result.ExactFailure;
import tv.threess.threeready.player.result.Result;

/**
 * The top manager class for all playback management components. Implements the AIDL playback interface used by the
 * consumers of playback. Must be extended and implemented by those offering playback as a service for the consumer.
 * <p>
 * Ideas for the future, for non-exclusive domains (e.g. Pip):
 * - Create a lazily initialized commandQ pool, with one Q per domain, no relating, no extra processing
 * - Create a lazily initialized dispatcher pool, with one dispatcher per domain linked to appropriate Q
 *
 * @author Dan Titiu
 * @since 05.02.2014
 */
public abstract class PlaybackManager implements
        IPlaybackCallback, Lifecycleable, FeedbackConsumer, PlaybackDispatcher, Component {
    static final String TAG = Log.tag(PlaybackManager.class);// package-private for inner-class optimization

    public static final String EXTRA_STREAM_TYPE = "dvb_stream_type";

    private volatile boolean mInitialized = false;

    protected abstract StartCommand getStartedCommand();

    /**
     * Default constructor
     */
    public PlaybackManager(final WeakReference<Context> context, @NonNull ControlFactory controlFactory,
                           @NonNull CommandFactory commandFactory) {
        this.context = context;
        this.callbacks = new CopyOnWriteArrayList<>();
        this.pluginManager = new PluginManager();
        this.resultQ = new BlockingQueue<>();
        this.commandFactory = commandFactory;

        // Callback dispatcher needs reference to the registered PlaybackCallback(s)
        this.callbackDispatcher = new CallbackDispatcher(new WeakReference<>(this));
        final WeakReference<ResultDispatcher> rd = new WeakReference<>(this.callbackDispatcher);

        // Control manager depends on the ControlFactory and forwards the FeedbackConsumer to each control
        this.controller = new ControlManager(context, controlFactory, this);
        final WeakReference<ControlProvider> cp = new WeakReference<>(this.controller);
        this.commandFactory.initialize(cp);

        // Command queue depends on the CallbackDispatcher for dispatching results for commands that are dismissed directly from the queue
        // and depends on the ControlManager to inject the appropriate control into implicit commands
        // and depends on the CommandDispatcher for retrieving the last successful command
        this.commandQ = new PlaybackQueue(rd, new WeakReference<>(this), cp);

        // The CommandDispatcher, the central control unit depends on:
        // - Takes commands from command queue
        // - Takes command results coming from controls, from the result queue
        // - Depends on the ControlManager when synthesizing commands and obtaining current state
        // - Dispatches results to callbacks through the CallbackDispatcher
        // - Integrates the plugins into the command lifecycle
        this.commandDispatcher = new CommandDispatcher(this.commandQ, this.resultQ, cp, rd, this.pluginManager);

        mMainHandler = new Handler(Looper.getMainLooper());
    }

    public Context getContext() {
        return context.get();
    }

    public void registerPlugin(PlaybackPlugin plugin) {
        this.pluginManager.registerPlugin(plugin);
    }

    public void unregisterPlugin(PlaybackPlugin plugin) {
        this.pluginManager.unregisterPlugin(plugin);
    }

    //
    // Lifecycleable
    //

    @Override
    public void onCreate() {
        controller.onCreate();
        callbackDispatcher.onCreate();
        commandDispatcher.onCreate();
        commandQ.onCreate();
        pluginManager.onCreate();
        mInitialized = true;
    }

    @Override
    public void onDestroy() {
        mInitialized = false;
        pluginManager.onDestroy();
        commandQ.onDestroy();
        callbackDispatcher.onDestroy();
        commandDispatcher.onDestroy();
        controller.onDestroy();
        resultQ.clear();
    }

    //
    // IPlaybackInterface
    //

    /**
     * Starts a channel.
     *
     * @param channel The channel on which the playback should start.
     */
    protected StartCommand startChannel(StartAction startAction, TvChannel channel, IBroadcast broadcast) {
        final long cid = nextCommandId();
        Log.d(TAG, "startChannel(channelId: " + channel.getId() + ")");
        StartCommand command = commandFactory.makeLiveStartCommand(cid, startAction, channel, broadcast);
        offerCommand(command);
        return command;
    }

    /**
     * Start replay playback for the given broadcast event.
     *
     * @param event     The broadcast for which the replay playback needs to be start.
     * @param isFromFavourite Flag used to know if the channel id is from favourite channels list.
     * @return The id of the command which will start the playback.
     */
    protected StartCommand startReplay(StartAction startAction, IBroadcast event, Long position, boolean isFromFavourite) {
        final long cid = nextCommandId();
        Log.d(TAG, "startReplay(cid: " + cid + ", eventId: " + event.getId() + ", position: " + position + ")");
        StartCommand command = commandFactory.makeReplayStartCommand(cid, startAction, event, position, isFromFavourite);
        offerCommand(command);
        return command;
    }

    /**
     * Append the playlist with the previous replay broadcast.
     * @param broadcast The broadcast event to prepend the playlist with.
     * @return The unique identifier of the prepend command.
     */
    public long prependPreviousReplay(IBroadcast broadcast) {
        final long cid = nextCommandId();
        offerCommand(commandFactory.makePrependReplayCommand(cid, broadcast));
        return cid;
    }

    /**
     * Append the playlist with the next replay broadcast.
     * @param broadcast The broadcast event to append the playlist with.
     * @return The unique identifier of the append command.
     */
    public long appendNextReplay(IBroadcast broadcast) {
        final long cid = nextCommandId();
        offerCommand(commandFactory.makeAppendReplayCommand(cid, broadcast));
        return cid;
    }

    /**
     * Update the currently running item in the replay playlist.
     * @return The unique identifier of the command.
     */
    public long updateReplayPlaylist() {
        final long cid = nextCommandId();
        offerCommand(commandFactory.makeUpdateReplayPlaylistCommand(cid));
        return cid;
    }

    /**
     * Start recording playback for the broadcast.
     *
     * @param recording The recording for which the black should start.
     * @param series The series of the started recording.
     * @param position override the start position,  from bookmark, fo start over
     * @return The unique identifier of the command.
     */
    protected StartCommand startRecording(StartAction startAction, final IRecording recording, IRecordingSeries series, Long position) {
        final long cid = nextCommandId();
        Log.d(TAG, "startRecording(cid: " + cid + ", broadcastId: " + recording.getId() + ")");
        StartCommand command = commandFactory.makeRecordingStartCommand(cid, startAction, recording, series, position);
        offerCommand(command);
        return command;
    }

    /**
     * Starts a Vod.
     *
     * @param vodVariant vod vod variant used to send start session ( backend)
     *                   is not the playback url use for Exoplayer play, this URL will be in start session response
     * @return The command id.
     */
    protected StartCommand startVod(StartAction startAction, IVodItem vod, IVodPrice price, IVodVariant vodVariant, IVodSeries vodSeries, Long position) {
        final long cid = nextCommandId();

        Log.d(TAG, "startVod(cid: " + cid + ", vodVariant: " + vodVariant + ", position: " + position + ")");
        StartCommand command = commandFactory.makeVodStartCommand(cid, startAction, vod, price, vodVariant, vodSeries, position);
        offerCommand(command);

        return command;
    }

    protected StartCommand startTrailer(StartAction startAction, IVodItem vod, IVodVariant vodVariant) {
        final long cid = nextCommandId();

        Log.d(TAG, "startTrailer(cid: " + cid + ", vodVariant: " + vodVariant + ")");
        StartCommand command = commandFactory.makeTrailerStartCommand(cid, startAction, vod, vodVariant);
        offerCommand(command);

        return command;
    }

    protected StartCommand startGdpr(StartAction startAction, IVodItem vod, IVodPrice vodPrice, IVodVariant vodVariant) {
        final long cid = nextCommandId();

        Log.d(TAG, "startGdpr(cid: " + cid + ", vodVariant: " + vodVariant + ")");
        StartCommand command = commandFactory.makeGdprStartCommand(cid, startAction, vod, vodPrice, vodVariant);
        offerCommand(command);

        return command;
    }

    protected StartCommand startApplication(StartAction startAction, String packageName, String activityName, Bundle bundle) {
        final long cid = nextCommandId();

        Log.d(TAG, "startApplication(cid: " + cid + ", packageName: " + packageName + ")");
        StartCommand command = commandFactory.makeAppStartCommand(cid, startAction, packageName, activityName, bundle);
        offerCommand(command);

        return command;
    }

    protected StartCommand startApplication(StartAction startAction, String packageName, Intent intent, Bundle bundle) {
        final long cid = nextCommandId();

        Log.d(TAG, "startApplication(cid: " + cid + ", packageName: " + packageName + ")");
        StartCommand command = commandFactory.makeAppStartCommand(cid, startAction, packageName, intent, bundle);
        offerCommand(command);

        return command;
    }

    /**
     * Start the trick play mode on the non-exclusive playback controls, tuning on the secondary player.
     */
    protected long startTrickPlay(StartAction startAction, float playbackSpeed, PlaybackDomain activeDomain) {
        final long cid = nextCommandId();
        offerCommand(commandFactory.makeTrickPlayStartCommand(cid, startAction, activeDomain, playbackSpeed));
        return cid;
    }

    /**
     * Stop the trick play.
     */
    protected long stopTrickPlay(PlaybackDomain activeDomain) {
        final long cid = nextCommandId();
        offerCommand(commandFactory.makeTrickPlayStopCommand(cid, activeDomain));
        return cid;
    }

    /**
     * Try to re-tune to the last saved start command.
     * This tune is forced.
     * <p>
     * This can be used for a generic way of re-tuning to the last tuned item, without having to worry about it's type.
     *
     * @return The ID of the last start command, or -1 if there is no last start command.
     */
    public long reTryLastTuned(StartAction action, boolean isRetry) {
        StartCommand lastCommand = getStartedCommand();
        if (lastCommand == null) {
            Log.d(TAG, "Could not get last started command");
            return -1;
        }

        if (action == null) {
            action = lastCommand.getStartAction();
        }

        final long cid = this.nextCommandId();
        StartCommand command = lastCommand.createReTuneCommand(action, cid);
        if (command == null) {
            Log.d(TAG, "Could not clone last started command: " + lastCommand);
            return -1;
        }

        command.setRetryCount(isRetry ? lastCommand.getRetryCount() + 1 : 0);
        Log.d(TAG, "Re-tuning last started command: " + command);
        offerCommand(command);
        return cid;
    }

    /**
     * Try to re-tune to the last saved start command.
     * This tune is forced.
     * <p>
     * This can be used for a generic way of re-tuning to the last tuned item, without having to worry about it's type.
     *
     * @return The ID of the last start command, or -1 if there is no last start command.
     */
    public long reTryLastTuned(boolean isRetry) {
        return reTryLastTuned(null, isRetry);
    }

    public long stop() {
        final long cid = this.nextCommandId();
        offerCommand(commandFactory.makeStopCommand(cid));
        return cid;
    }

    /**
     * Mute the volume of the  currently running playback.
     */
    public void mute() {
        final long cid = nextCommandId();
        offerCommand(commandFactory.makeMuteCommand(cid));
    }

    /**
     * Unmute the volume of the  currently running playback.
     */
    public void unMute() {
        final long cid = nextCommandId();
        offerCommand(commandFactory.makeUnMuteCommand(cid));
    }

    public long forceStop() {
        final long cid = nextCommandId();
        offerCommand(commandFactory.makeForceStopCommand(cid));
        return cid;
    }

    protected long pause() {
        final long cid = nextCommandId();
        offerCommand(commandFactory.makePauseCommand(cid));
        return cid;
    }

    public long resume() {
        return resume(StartAction.Resume);
    }

    public long resume(StartAction startAction) {
        final long cid = nextCommandId();
        offerCommand(commandFactory.makeResumeCommand(cid, startAction));
        return cid;
    }

    /**
     * Create a command which will validate the given position with the control and trigger any events necessary for the position.
     * E.g. EndReached, PlaylistAppend etc.
     * @param position The absolute position which needs to be processed.
     * @return The id of the command.
     */
    protected long validatePosition(long position) {
        Log.d(TAG, "validatePosition() called with position: " + Log.timestamp(position));
        final long cid = nextCommandId();
        offerCommand(commandFactory.makeValidationPositionCommand(cid, position));
        return cid;
    }

    public void jumpByDuration(StartAction startAction, long duration) {
        final long cid = nextCommandId();
        offerCommand(commandFactory.makeRelativeJumpCommand(cid, startAction, duration));
    }

    public void jumpByPosition(StartAction startAction, long position) {
        final long cid = nextCommandId();
        offerCommand(commandFactory.makeAbsoluteJumpCommand(cid, startAction, position));
    }

    public PlaybackStatus getCurrentStatus() {
        for (PlaybackStatus ps : controller.getStatus()) {
            PlaybackDomain domain = ps.getDomain();
            if (domain != null && domain.exclusive) {
                return ps;
            }
        }

        return new PlaybackStatusBuilder().setState(PlaybackState.Stopped).build();
    }

    public long getPosition(long noPosition) {
        PlaybackControl<?> activeControl = controller.getActiveControl();
        if (activeControl == null) {
            return noPosition;
        }
        return activeControl.getCurrentPosition();
    }

    /**
     * @param type The type for which the playback details should be returned.
     * @return The playback details of the provided domain.
     */
    public PlaybackDetails getDetails(PlaybackType type) {
        return controller.getDetails(type).build();
    }

    public PlaybackDetails getExclusiveDetails() {
        PlaybackControl<?> control = controller.getActiveControl();
        if (control == null) {
            return new PlaybackDetailsBuilder()
                    .setState(PlaybackState.Stopped)
                    .build();
        }
        return control.getDetails().build();
    }

    public PlaybackDetails getNonExclusiveDetails() {
        for (Map.Entry<PlaybackDomain, PlaybackDetailsBuilder> e : controller.getDetails().entrySet()) {
            Log.d(TAG, "playback details: " + e.getValue().build());
            if (!e.getKey().exclusive) {
                return e.getValue().build();
            }
        }

        return new PlaybackDetailsBuilder()
                .setState(PlaybackState.Stopped)
                .build();
    }

    /**
     * Register a callback which will be notified when the playback manager state changes.
     * Command success, command failure, playback event etc.
     *
     * @param callback The callback which will be registered.
     */
    public void registerCallback(IPlaybackCallback callback) {
        unregisterCallback(callback);
        callbacks.add(new WeakReference<>(callback));
    }

    /**
     * Removes a previously registered playback callback.
     *
     * @param callback The callback which needs to be removed.
     */
    public void unregisterCallback(final IPlaybackCallback callback) {
        callbacks.removeIf((item) -> {
            IPlaybackCallback listener = item.get();
            return listener == null || listener == callback;
        });
    }

    public long selectAudio(MediaTrackInfo track) {
        final long cid = nextCommandId();
        offerCommand(commandFactory.makeSelectAudioCommand(cid, track.getId()));
        return cid;
    }

    public long selectSubtitle(MediaTrackInfo track) {
        final long cid = nextCommandId();
        offerCommand(commandFactory.makeSelectSubtitleCommand(cid, track.getId()));
        return cid;
    }

    public long deselectSubtitle() {
        final long cid = nextCommandId();
        offerCommand(commandFactory.makeSelectSubtitleCommand(cid, null));
        return cid;
    }

    public boolean isDolbyAvailable() {
        MediaTrackInfo selectedAudioTrack = getExclusiveDetails().getCurrentAudioTrack();
        if (selectedAudioTrack == null) {
            return false;
        }

        return selectedAudioTrack.isDolby();
    }

    //
    // IPlaybackCallback
    //

    @Override
    public void onCommandSuccess(PlaybackCommand cmd, PlaybackDetails details) {
        for (WeakReference<IPlaybackCallback> cw : callbacks) {
            try {
                IPlaybackCallback callback = cw.get();
                if (callback != null) {
                    callback.onCommandSuccess(cmd, details);
                }
            } catch (Exception e) {
                Log.e(TAG, "Failed to dispatch command success.", e);
            }
        }
    }

    @Override
    public void onCommandFailure(PlaybackCommand cmd, PlaybackDetails details, PlaybackError error) {
        for (WeakReference<IPlaybackCallback> cw : callbacks) {
            try {
                IPlaybackCallback callback = cw.get();
                if (callback != null) {
                    callback.onCommandFailure(cmd, details, error);
                }
            } catch (Exception e) {
                Log.e(TAG, "Failed to dispatch command failure.", e);
            }
        }
    }

    @Override
    public void onPlaybackEvent(PlaybackEvent event, PlaybackDetails details, PlaybackError error) {
        for (WeakReference<IPlaybackCallback> cw : callbacks) {
            try {
                IPlaybackCallback callback = cw.get();
                if (callback != null) {
                    callback.onPlaybackEvent(event, details, error);
                }
            } catch (Exception e) {
                Log.e(TAG, "Failed to dispatch playback event to callback.", e);
            }
        }
    }

    //
    // FeedbackConsumer
    //

    public void offerCommand(PlaybackCommand... commands) {
        long cmdId = -1;
        try {
            for (PlaybackCommand command : commands) {
                this.commandQ.offer(command);
            }
            if (commands.length > 0) {
                cmdId = commands[0].getId();
                // offer the first command from batch commands
                pluginManager.onOfferCommand(commands[0]);

                /// send start command offered event
                if (commands[0] instanceof StartCommand) {
                    onPlaybackEvent(PlaybackEvent.StartInitiated, getDetails(commands[0].getType()), null);
                }
            }
        } catch (PlaybackError e) {
            Log.e(TAG, e.getMessage());
            // A command was surely issued, create a failure result
            this.offerCommandResult(new ExactFailure(cmdId, e));
        }
    }

    @Override
    public void offerCommandResult(Result result) {
        // The result first needs to match the executed command
        this.resultQ.offer(result);
    }

    @Override
    public void offerEvent(EventResult result, Bundle details) {
        if (discardEvent(result)) {
            Log.d(TAG, "Discarded event " + result);
            return;
        }

        // Complete event only if not discarded to save some computing
        // Events should carry with them the details of their issuing domain
        final PlaybackControl<?> pc = this.controller.getControl(result.domain);
        result.complete(pc.getDetails().build());

        // The result goes directly to the interface
        this.callbackDispatcher.postCommandResult(result);
        // .. and plugins
        try {
            this.pluginManager.onEvent(result, details);
        } catch (Exception e) {
            Log.e(TAG, "Error while dispatching event to plugins", e);
        }
    }

    @Override
    public void offerEventCommand(EventCommand command) {
        EventResult eventResult = command.getEventResult(null);
        if (discardEvent(eventResult)) {
            Log.d(TAG, "Discarded command generated by event " + eventResult);
            return;
        }

        // After EventCommand is executed the interface gets the event
        this.commandQ.offerEventCommand(command);
    }

    //
    // Dispatcher
    //

    @Override
    public boolean discardEvent(EventResult eventResult) {
        return this.commandDispatcher.discardEvent(eventResult);
    }

    @Override
    public StartCommand getLastCommand() {
        return this.commandDispatcher.getLastCommand();
    }

    /**
     * The time when the last start command was initiated
     * @param defaultValue The default value to return when there is no start command.
     * @return The timestamp of the last start command.
     */
    public long getLastStartInitiatedTime(long defaultValue) {
        StartCommand lastCommand = getLastCommand();
        if (lastCommand != null) {
            return lastCommand.getCommandInitiatedTimestamp();
        }
        return defaultValue;
    }

    @Override
    public StartCommand getLastFailedCommand() {
        return this.commandDispatcher.getLastFailedCommand();
    }

    @Override
    public PlaybackCommand getDispatchingCommand() {
        return this.commandDispatcher.getDispatchingCommand();
    }

    /**
     * Check if there is a command waiting to be executed in queue
     */
    @SafeVarargs
    public final boolean isCommandDispatching(Class<? extends PlaybackCommand>... commandTypes) {
        PlaybackCommand cmd = commandDispatcher.getDispatchingCommand();
        for (Class<? extends PlaybackCommand> commandType : commandTypes) {
            if (cmd != null && commandType.isInstance(cmd)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if a command with the given type is queued or currently executing.
     *
     * @param cClass The type of the command which should verified.
     * @return True if the command with the give type is queued or currently executing.
     */
    public boolean isCommandQueued(Class<? extends PlaybackCommand> cClass) {
        return commandQ.isCommandQueued(cClass);
    }

    /**
     * Check if a command is in queue or is currently dispatching.
     *
     * @param commandTypes Array of command types
     * @return true in case any of the commands is queued or currently dispatching
     */
    @SafeVarargs
    public final boolean isCommandStarting(Class<? extends PlaybackCommand>... commandTypes) {
        if (commandTypes.length <= 0) {
            return false;
        }
        if (commandQ.isCommandQueued(commandTypes)) {
            return true;
        }
        return isCommandDispatching(commandTypes);
    }

    /**
     * Check if the current playback type is one of the parameter
     */
    public boolean inPlaybackType(PlaybackType... values) {
        PlaybackType current = this.getExclusiveDetails().getType();
        for (PlaybackType value : values) {
            if (current == value) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if the current playback domain is one of the parameter
     */
    public boolean inPlaybackDomain(PlaybackDomain... values) {
        PlaybackDomain current = getActiveExclusiveDomain();
        for (PlaybackDomain value : values) {
            if (current == value) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if the current playback state is one of the parameter
     */
    public boolean inPlaybackState(PlaybackState... values) {
        PlaybackControl<?> activeControl = controller.getActiveControl();
        PlaybackState state = PlaybackState.Stopped;
        if (activeControl != null) {
            state = activeControl.getCurrentState();
        }
        for (PlaybackState value : values) {
            if (state == value) {
                return true;
            }
        }
        return false;
    }

    public boolean isInitialized() {
        return mInitialized;
    }

    /**
     * @return Currently active exclusive domain or null if there is no active domain.
     */
    @Nullable
    public PlaybackDomain getActiveExclusiveDomain() {
        return controller.getActiveExclusiveDomain();
    }

    /**
     * Android context weakly referenced.
     */
    final WeakReference<Context> context;

    /**
     * Blocking queue that provides playback commands. The take is expected to block when queue is empty until a command
     * is available.
     */
    private final PlaybackQueue commandQ;

    /**
     * Blocking queue that provides results to dispatched commands. Each command needs to be paired with a result.
     */
    private final BlockingQueue<Result> resultQ;

    /**
     * Instance holding all the registered playback controllers.
     */
    protected final ControlManager controller;

    /**
     * Instance to the command dispatcher that handles command lifecycle.
     */
    private final CommandDispatcher commandDispatcher;

    @NonNull
    private final CommandFactory commandFactory;

    private final CallbackDispatcher callbackDispatcher;

    /**
     * Array which holds a week reference to all registered playback callback.
     */
    final CopyOnWriteArrayList<WeakReference<IPlaybackCallback>> callbacks;// package-private for inner-class optimization

    private final PluginManager pluginManager;


    long nextCommandId() {
        return PlaybackCommand.userIdCounter.getNextId();
    }

    protected final Handler mMainHandler;
}
