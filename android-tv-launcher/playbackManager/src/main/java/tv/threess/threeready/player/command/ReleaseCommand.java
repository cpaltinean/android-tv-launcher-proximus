/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command;

import tv.threess.threeready.player.command.base.ImplicitCommand;
import tv.threess.threeready.player.contract.CommandValidity;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.result.EventResult;

/**
 * Implicit command that transition the state to Released. It's purpose is to release resources allocated for playback
 * but not necessarily for player (e.g. stream management / bandwidth reservations).
 *
 * @author Dan Titiu
 * @since 25.06.2014
 */
public class ReleaseCommand extends ImplicitCommand {

    public ReleaseCommand(long id) {
        super(id, null, PlaybackAction.Release, PlaybackState.Released);
    }

    @Override
    public CommandValidity validate(PlaybackEntireState ds) {
        // Releasing is valid if the same domain is not active and NOT Released.
        if (!this.verify(ds)) {
            final PlaybackState state = this.control.getCurrentState();
            if (state.active) {
                // Releasing requires the state of its target to be stopped
                return CommandValidity.TransitionalStopped;
            }

            // If already inactive it's probably already Released
            return CommandValidity.Invalid;
        }

        return CommandValidity.Valid;

    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        // Releasing is valid if the same domain is not active and NOT Released.
        final PlaybackState state = this.control.getCurrentState();
        return !state.active && !PlaybackState.Released.equals(state);
    }

    @Override
    protected void executeImplicit() {
        // Does nothing on the player itself, just changes associated states.
        this.control.confirm(this.getId());
    }

    @Override
    public void commit() {
        super.commit();
        this.control.applyState(this.targetState);
        // Inactive states do not have a full status
        this.control.applyDetails(null);
        this.control.reset();
    }

    @Override
    public boolean discards(EventResult eventResult) {
        return eventResult != null && eventResult.domain == getDomain() && eventResult.event.mDiscardable;
    }
}
