/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.result;

import tv.threess.threeready.player.command.base.PlaybackCommand;

/**
 * Command result that matches the command by its ID.
 *
 * @author Dan Titiu
 * @since 12.08.2014
 */
public abstract class ExactResult extends AbstractResult {

    /**
     * Playback command ID which this result is paired with.
     */
    protected final long cid;

    protected PlaybackCommand cmd;

    /**
     * Default constructor.
     *
     * @param cid    PlaybackCommand ID which this result is paired with.
     */
    public ExactResult(final long cid) {
        this.cid = cid;
    }


    @Override
    public boolean matches(final PlaybackCommand cmd) {
        return cmd != null && cmd.getId() == this.cid;
    }

    @Override
    public void pairWith(PlaybackCommand cmd) {
        this.cmd = cmd;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{cid[" + this.cid + "]}";
    }
}
