package tv.threess.threeready.player.command.start;

import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.base.PlaybackControl;

/**
 * Abstract start command that is aware of the content item started.
 *
 * @author Andor Lukacs
 * @since 26/08/2020
 */
public abstract class ContentItemStartCommand<TContentItem extends IContentItem> extends StartCommand {

    protected ContentItemStartCommand(long id, PlaybackType type, StartAction startAction, PlaybackControl<?> control) {
        super(id, type, startAction, control);
    }

    public abstract TContentItem getContentItem();
}
