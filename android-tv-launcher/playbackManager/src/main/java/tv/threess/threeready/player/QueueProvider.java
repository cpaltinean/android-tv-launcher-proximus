/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.player.command.base.PlaybackCommand;

/**
 * Facade exposing a Queue to clients solely as an element provider.
 *
 * @author Dan Titiu
 * @since 24.04.2014
 */
public interface QueueProvider<E> {

    /**
     * Retrieves and removes the head of this queue, waiting if necessary
     * until an element becomes available.
     *
     * @return the head of this queue
     * @throws InterruptedException if interrupted while waiting
     */
    E take() throws InterruptedException;

    /**
     *  Retrieves but not removes the head of this queue
     * @return
     * @throws InterruptedException
     */
    E peek() throws InterruptedException;

    /**
     * Retrieves and removes the head of this queue, waiting up to the
     * specified wait time if necessary for an element to become available.
     *
     * @param timeout how long to wait before giving up, in units of
     *        <tt>unit</tt>
     * @param unit a <tt>TimeUnit</tt> determining how to interpret the
     *        <tt>timeout</tt> parameter
     * @return the head of this queue, or <tt>null</tt> if the
     *         specified waiting time elapses before an element is available
     * @throws InterruptedException if interrupted while waiting
     */
    E poll(long timeout, TimeUnit unit) throws InterruptedException;

    /**
     * Retrieves and removes all commands with cmdId from userQ
     */
    Collection<PlaybackCommand> remove(long id) throws InterruptedException;

    /**
     * Retrieves all content and empties the queue.
     *
     * @return the queued items or an empty list in case the queue is already empty.
     */
    Collection<E> purge() throws InterruptedException;

    /**
     * Cancel a command that is already in queued
     * @param qc
     */
    void cancelQueuedCommand(E qc);
}
