package tv.threess.threeready.player.wrapper;

import tv.threess.lib.di.Component;
import tv.threess.threeready.player.model.OttTuneParams;

/**
 * Interface for player wrapper that's based on regular surface playabck.
 *
 * @author Andor Lukacs
 * @since 6/4/19
 */
public interface ISurfacePlayerWrapper<T> extends IPlayerWrapper<T>, Component {

    /**
     * Method used for playing the selected stream with the given parameters.
     *
     * @param tuneParams The parameters necessary to start the playback
     */
    void play(OttTuneParams tuneParams);
}
