/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.result;

import androidx.annotation.Nullable;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.api.playback.player.PlaybackDomain;

/**
 * Favorable Result for an executed command that matches the command upon domain and action(s).
 *
 * @author Dan Titiu
 * @since 21.06.2014
 */
public class VagueSuccess extends VagueResult {
    private static final String TAG = Log.tag(VagueSuccess.class);

    public VagueSuccess(PlaybackDomain domain, PlaybackAction... action) {
        super(domain, action);
    }

    @Override
    public void apply(PlaybackCommand cmd, @Nullable Long nextCmdId) {
        cmd.commit();
    }

    @Override
    public boolean isFavorable() {
        return true;
    }

    @Override
    public void dispatchTo(IPlaybackCallback callback) {
        if (this.cid <= 0) {
            throw new IllegalStateException("Trying to dispatch callback without command id");
        }

        try {
            callback.onCommandSuccess(this.cmd, this.details);
        } catch (Exception e) {
            Log.e(TAG, "Failed to dispatch success for command id[" + cid + "]", e);
        }
    }
}
