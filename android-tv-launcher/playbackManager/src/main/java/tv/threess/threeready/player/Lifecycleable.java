/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

/**
 * Interface for Library-managed objects that have a lifecycle (e.g. create, destroy).
 *
 * @author Dan Titiu
 * @since 23.08.2012
 */
public interface Lifecycleable {

    /**
     * Called by the managing entity when this object is first created. The implementing objects should perform
     * creation at this step. Do not call this method directly.
     */
    default void onCreate() {}

    /**
     * Called by the managing entity to notify that it is no longer used and is being removed. The
     * object should clean up any resources it holds (threads, registered receivers, etc) at this point.
     * Upon return, there will be no more calls to this object and it is effectively dead.
     * Do not call this method directly.
     */
    default void onDestroy() {}

}
