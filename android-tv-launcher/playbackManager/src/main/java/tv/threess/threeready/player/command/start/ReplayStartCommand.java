/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command.start;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.ReplayControlProximus;
import tv.threess.threeready.player.control.base.PlaybackControl;

/**
 * Start Replay stream command
 *
 * @author Daniel Gliga
 * @since 2017.06.07
 */

public class ReplayStartCommand extends BroadcastStartCommand {

    private final ReplayControlProximus mReplayControl;
    private final Long mPosition;
    private final boolean mFromFavourite;


    public ReplayStartCommand(long id, PlaybackType type, PlaybackControl control,
                              StartAction startAction, IBroadcast broadcast, Long position, boolean isFromFavourite) {
        super(id, type, startAction, control, broadcast);
        mReplayControl = validateControl(control, ReplayControlProximus.class);
        mPosition = position;
        mFromFavourite = isFromFavourite;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        mReplayControl.start(this);
    }

    public Long getPosition() {
        return mPosition;
    }

    public boolean isFromFavourite() {
        return mFromFavourite;
    }

    public IBroadcast getContentItem() {
        //Continuous replay broadcasts are updated one by one
        long currentPosition = mReplayControl.getCurrentPosition();
        return getBroadcast(currentPosition, true);
    }

    @Override
    public ReplayStartCommand createReTuneCommand(StartAction action, long cid) {
        return new ReplayStartCommand(cid, type, control, action, getContentItem(), null, mFromFavourite);
    }

    @Override
    public Comparable playbackId() {
        IBroadcast broadcast = getContentItem();
        if (broadcast == null) {
            return null;
        }
        return broadcast.getId();
    }
}
