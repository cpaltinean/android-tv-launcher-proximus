/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import tv.threess.threeready.player.command.base.PlaybackCommand;

/**
 * Queue that hold the items in a linked list and supports blocking operations for multi-threading.
 *
 * @author Dan Titiu
 * @since 28.04.2014
 */
public class BlockingQueue<E> extends LinkedBlockingQueue<E> implements QueueConsumer<E>, QueueProvider<E> {

    private static final long serialVersionUID = -4355953418185867870L;

    @Override
    public Collection<PlaybackCommand> remove(long id) throws InterruptedException {
        // no implementation
        return null;
    }

    /**
     * Retrieves all content and empties the queue.
     *
     * @return the queued items or an empty list in case the queue is already empty.
     */
    @Override
    public Collection<E> purge() throws InterruptedException {
        final int count = this.size();
        if (count > 0) {
            List<E> result = new ArrayList<>(count);
            E item;
            // We poll one by one taking the risk that other treads might enqueue items meanwhile
            // which is less risky than getting all items at once and then clearing all items in a separate step.
            while ( (item = this.poll()) != null ) {
                result.add(item);
            }
            return result;
        }

        return Collections.emptyList();
    }

    @Override
    public void cancelQueuedCommand(E qc) {
        // no implementation
    }
}
