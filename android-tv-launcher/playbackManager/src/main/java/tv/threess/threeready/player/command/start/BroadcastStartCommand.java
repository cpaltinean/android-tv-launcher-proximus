package tv.threess.threeready.player.command.start;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.base.PlaybackControl;

/**
 * Abstract start command for broadcast related content, such as live start or replay tv.
 *
 * @author Andor Lukacs
 * @since 26/08/2020
 */
public abstract class BroadcastStartCommand extends ContentItemStartCommand<IBroadcast> {

    private List<IBroadcast> mBroadcastList;

    protected BroadcastStartCommand(long id, PlaybackType type, StartAction startAction, PlaybackControl<?> control, IBroadcast broadcast) {
        super(id, type, startAction, control);
        mBroadcastList = Collections.singletonList(broadcast);
    }

    public List<IBroadcast> getBroadcasts() {
        return mBroadcastList;
    }


    /**
     * Find the broadcast which includes the give content time.
     *
     * @param contentTime  The content time to search for.
     * @param defaultFirst True if in case there is not content at the given time the first should be returned
     * @return The broadcast with the given content time or null if there is no such.
     */
    @Nullable
    public IBroadcast getBroadcast(long contentTime, boolean defaultFirst) {
        for (IBroadcast broadcast : mBroadcastList) {
            if (broadcast.getStart() <= contentTime && broadcast.getEnd() >= contentTime) {
                return broadcast;
            }
        }

        //Fallback to first item if the broadcast list is not updated
        if (defaultFirst && !ArrayUtils.isEmpty(mBroadcastList)) {
            return mBroadcastList.get(0);
        }
        return null;
    }

    public void setBroadcastList(List<IBroadcast> broadcastList) {
        // Make a copy so the list is not modified from outside.
        mBroadcastList = new ArrayList<>(broadcastList);
    }

    public long getBufferStart(long currentPosition) {
        if (!mBroadcastList.isEmpty()) {
            for (int i = mBroadcastList.size() - 1; i >= 0; i--) {
                IBroadcast broadcast = mBroadcastList.get(i);

                // Skip programs after the currently playing program
                if (broadcast.getStart() > currentPosition) {
                    continue;
                }

                if (broadcast.isBlackListed() && !broadcast.isLive() && i < mBroadcastList.size() - 1) {
                    return mBroadcastList.get(i+1).getStart();
                }
            }
            return mBroadcastList.get(0).getStart();
        }
        return 0;
    }


    public long getBufferEnd(long currentPosition) {
        for (int i = 0; i < mBroadcastList.size(); i++) {
            IBroadcast broadcast = mBroadcastList.get(i);

            // Skip programs before the currently playing program
            if (broadcast.getEnd() < currentPosition) {
                continue;
            }

            if (broadcast.isBlackListed() && broadcast.getEnd() < System.currentTimeMillis()) {
                return broadcast.getStart();
            }
        }
        return System.currentTimeMillis();
    }
}
