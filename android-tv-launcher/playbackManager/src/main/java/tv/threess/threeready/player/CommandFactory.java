/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import android.content.Intent;
import android.os.Bundle;

import java.lang.ref.WeakReference;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;

/**
 * Factory interface for creating PlaybackCommands.
 *
 * @author Dan Titiu
 * @since 24/01/2017
 */
public interface CommandFactory {

    /**
     * Initialize dependencies before using the factory instance.
     *
     * @param controller The reference to the {@link ControlProvider}.
     */
    void initialize(final WeakReference<ControlProvider> controller);

    /**
     * Create a start command for live IPTV playback.
     * @param cid Command ID
     * @param startAction Action of start
     * @param channel Channel to start
     * @param broadcast Ongoing broadcast
     */
    StartCommand makeLiveStartCommand(long cid, StartAction startAction, TvChannel channel, IBroadcast broadcast);

    /**
     * Create a command to start the replay playback.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param broadcast IBroadcast used to start the playback on.
     * @param position The position in milliseconds relative to the beginning of the stream
     *                 from where the playback should be started.
     * @param isFromFavourite Flag used to know if the channel id is from favourite channels list.
     * @return The newly created command.
     */
    StartCommand makeReplayStartCommand(long cid, StartAction startAction, IBroadcast broadcast, Long position, boolean isFromFavourite);

    /**
     * Create command to prepend the playlist with the previous replay.
     * @param cid The unique id of the prepend command.
     * @param broadcast The previous replay broadcast to which prepend the playlist
     * @return The newly created command.
     */
    PlaybackCommand makePrependReplayCommand(long cid, IBroadcast broadcast);

    /**
     * Create command to append the playlist with the next replay.
     * @param cid The unique id of the append command.
     * @param broadcast The next replay broadcast to which append the playlist
     * @return The newly created command.
     */
    PlaybackCommand makeAppendReplayCommand(long cid, IBroadcast broadcast);

    /**
     * Create command to update the playlist to cover all the broadcast in the continuous replay list with playback.
     * @param cid The unique id of the append command.
     * @return The newly created command.
     */
    PlaybackCommand makeUpdateReplayPlaylistCommand(long cid);

    /**
     * Create a command to start the recording playback.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param recording The recording to start the playback for.
     * @param series The series of the started recording.
     * @param position Force relative position where the playback should start.
     * @return The newly created command.
     */
    StartCommand makeRecordingStartCommand(long cid, StartAction startAction, IRecording recording, IRecordingSeries series, Long position);

    /**
     * Create a command to start the vod playback.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param vod The parent vod to start the playback for.
     * @param vodPrice The vod vodPrice used to start the playback.
     * @param vodVariant The vod variant to start the playback for.
     * @param vodSeries The series of the started vod.
     * @param position Force relative position where the playback should start.
     * @return The newly created command.
     */
    StartCommand makeVodStartCommand(long cid, StartAction startAction, IVodItem vod, IVodPrice vodPrice, IVodVariant vodVariant, IVodSeries vodSeries, Long position);

    /**
     * Create a command to start the trailer playback.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param vod The parent vod to start the playback for.
     * @param vodVariant The vod variant to start the playback for.
     * @return The newly created command.
     */
    StartCommand makeTrailerStartCommand(long cid, StartAction startAction, IVodItem vod, IVodVariant vodVariant);

    /**
     * Create a command to start a GDPR video.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param vod The parent vod to start the playback for.
     * @param vodPrice The vod vodPrice used to start the playback.
     * @param vodVariant The vod variant to start the playback for.
     * @return The newly created command.
     */
    StartCommand makeGdprStartCommand(long cid, StartAction startAction, IVodItem vod, IVodPrice vodPrice, IVodVariant vodVariant);

    /**
     * Create a command to start trick play playback.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param activeDomain The playback domain which currently active and the trick play should start on it.
     * @param playbackSpeed The speed of the trick play playback to start on.
     * @return The newly created command.
     */
    StartCommand makeTrickPlayStartCommand(long cid, StartAction startAction, PlaybackDomain activeDomain, float playbackSpeed);

    /**
     * Create a command to start a 3rd party application.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param packageName The package name of the application.
     * @param activityName The name of the activity in the application.
     * @param bundle The extra parameters needs to be send to the activity.
     * @return The newly created command.
     */
    StartCommand makeAppStartCommand(long cid, StartAction startAction, String packageName, String activityName, Bundle bundle);

    /**
     * Create a command to start a 3rd party application.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the playback start.
     * @param packageName The package name of the application.
     * @param intent The intent to start the activity with.
     * @param bundle The extra parameters needs to be send to the activity.
     * @return The newly created command.
     */
    StartCommand makeAppStartCommand(long cid, StartAction startAction, String packageName, Intent intent, Bundle bundle);

    /**
     * Create a command to mute the volume of the current playback.
     * @param cid The unique identifier of the command.
     * @return The newly created command.
     */
    PlaybackCommand makeMuteCommand(long cid);

    /**
     * Create a command to unmute the volume of the current playback.
     * @param cid The unique identifier of the command.
     * @return The newly created command.
     */
    PlaybackCommand makeUnMuteCommand(long cid);

    /**
     * Create a command to stop the current playback.
     * @param cid The unique identifier of the command.
     * @return The newly created command.
     */
    PlaybackCommand makeStopCommand(long cid);

    /**
     * Create a command the stop the currently running trick play.
     * @param cid The unique identifier of the command.
     * @return The newly created command.
     */
    PlaybackCommand makeTrickPlayStopCommand(long cid, PlaybackDomain activeDomain);

    /**
     * Create a non cancelable command to stop the currently running playback.
     * @param cid The unique identifier of the command.
     * @return The newly created command.
     */
    PlaybackCommand makeForceStopCommand(long cid);

    /**
     * Create a command to pause the currently running playback.
     * @param cid The unique identifier of the command.
     * @return The newly created command.
     */
    PlaybackCommand makePauseCommand(long cid);

    /**
     * Create a command to resume the previously paused playback.
     * @param cid The unique identifier of the command.
     * @return The newly created command.
     */
    PlaybackCommand makeResumeCommand(long cid, StartAction startAction);

    /**
     * Create a command which will validate the given position with the control and trigger any events necessary for the position.
     * E.g. EndReached, PlaylistAppend etc.
     * @param cid The unique identifier of the command.
     * @param position The absolute position which needs to be processed.
     * @return THe newly created command.
     */
    PlaybackCommand makeValidationPositionCommand(long cid, long position);

    /**
     * Create a command to jump in the playback relative to the current position with given amount.
     * @param cid The unique identifier of the command.
     * @param duration The amount in millis to jump.
     *                Negative value will result a backward jump, positive value a forward jump.
     * @param startAction The user action which triggered the jump.
     * @return The newly created command.
     */
    PlaybackCommand makeRelativeJumpCommand(long cid, StartAction startAction, long duration);

    /**
     * Create a command to jump the the absolute position in the stream.
     * @param cid The unique identifier of the command.
     * @param startAction The user action which triggered the jump.
     * @param position The absolute position in the stream to jump.
     * @return The newly created command.
     */
    PlaybackCommand makeAbsoluteJumpCommand(long cid, StartAction startAction, long position);

    /**
     * Create a command to change the audio track in the current playback.
     * @param cid The unique identifier of the command.
     * @param trackId The identifier of the audio track which needs to be selected.
     * @return The newly created command.
     */
    PlaybackCommand makeSelectAudioCommand(long cid, String trackId);

    /**
     * Create a command to change the subtitle in the current playback.
     * @param cid The unique identifier of the command.
     * @param trackId The identifier of the subtitle track which needs to be selected.
     * @return The newly created command.
     */
    PlaybackCommand makeSelectSubtitleCommand(long cid, String trackId);
}