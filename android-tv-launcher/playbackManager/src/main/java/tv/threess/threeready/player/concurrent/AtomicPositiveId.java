/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.concurrent;

import java.util.concurrent.atomic.AtomicLong;

/**
 * A {@code long} value that may be used as consecutive identifier generator and which eventually wraps around when
 * reaching {@code Long.MAX_VALUE}. It only emits positive (non-zero) values.
 *
 * @see AtomicLong
 * @author Dan Titiu
 * @since 06/03/2017
 */
public class AtomicPositiveId extends AtomicLong {

    private static final long serialVersionUID = -9118340760352021637L;

    public AtomicPositiveId() {
        super(0L);
    }

    public AtomicPositiveId(long initialValue) {
        super(initialValue);
    }

    public long getNextId() {
        for (;;) {
            long current = get();
            long next = current + 1L;
            // Wrap around for positive only
            if (next < 0L) {
                next = 1L;
            }
            if (compareAndSet(current, next)) {
                return next;
            }
        }
    }
}
