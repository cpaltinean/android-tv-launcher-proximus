/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command.base;

import androidx.annotation.NonNull;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.result.EventResult;
import tv.threess.threeready.player.result.Result;

/**
 * PlaybackCommand that is synthesised as the result of a background PlaybackEvent. Implementing subclasses have the
 * role of synchronizing the state changes of the underlying player with the state maintained by TvLibrary.
 *
 * @author Dan Titiu
 * @since 09.10.2014
 */
public abstract class EventCommand extends ExplicitCommand {

    /**
     * Playback event triggered from background that needs to be dispatched to PlaybackInterface.
     */
    @NonNull
    protected final PlaybackEvent event;

    public EventCommand(@NonNull PlaybackEvent event, @NonNull PlaybackType type, PlaybackAction action,
                        @NonNull PlaybackControl control, PlaybackState targetState) {
        super(PlaybackCommand.syntheticIdCounter.getNextId(), type, action, control, targetState);
        this.event = event;
    }

    /**
     * Returns the EventResult attached to this command.
     *
     * @return the EventResult attached to this command.
     */
    public abstract EventResult getEventResult(Result result);

    @Override
    public boolean equals(Object o) {
        if (super.equals(o) && o instanceof EventCommand) {
            EventCommand other = (EventCommand) o;
            return other.event.equals(this.event);
        }

        return false;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.replace(sb.length() - 1, sb.length(), ",event[");
        sb.append(this.event);
        sb.append("]}");
        return sb.toString();
    }
}
