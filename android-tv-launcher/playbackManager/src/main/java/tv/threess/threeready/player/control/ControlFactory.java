/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.control;

import android.content.Context;
import android.media.tv.TvView;
import android.view.SurfaceView;
import android.view.View;

import java.lang.ref.WeakReference;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.control.base.PlaybackControl;

/**
 * Factory interface for creating PlaybackControls.
 *
 * @author Dan Titiu
 * @since 09/10/16
 */
public interface ControlFactory extends Component {

    /**
     * Creates a PlaybackControl for a certain PlaybackDomain.
     *
     * @param domain  The {@link PlaybackDomain} for which to create a {@link PlaybackControl}.
     * @param context The Android context.
     * @return the PlaybackControl for the given PlaybackDomain.
     */
    PlaybackControl createControl(PlaybackDomain domain, final WeakReference<Context> context);

    /**
     * Set the TV view on which the video will be rendered.
     * @param tvView The TV view which holds the surface.
     */
    void setTvView(TvView tvView);

    /**
     * Adds a specific {@link TvView} for the trick modes playback control.
     *
     * @param tvView The {@link TvView} assigned for trickmodes playback.
     * @param trickplayBackground
     */
    void setTrickPlaySurface(SurfaceView tvView, View trickplayBackground);

    SurfaceView getTrickPlaySurface();

    View getTrickplayBackground();

}
