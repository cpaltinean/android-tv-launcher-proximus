package tv.threess.threeready.player.command;

import tv.threess.threeready.player.command.base.ImplicitCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.result.EventResult;

/**
 * Command that resets the internal state of the player in the control.
 * This should be used once we are changing streams but not domains.
 * When changing domains see {@link StopCommand}
 *
 * @author Andor Lukacs
 * @since 5/8/19
 */
public class ClearCommand extends ImplicitCommand {


    public ClearCommand(long id) {
        this(id, null);
    }

    public ClearCommand(long id, PlaybackCommand injectedBy) {
        super(id, injectedBy, PlaybackAction.Stop, PlaybackState.Stopped);
    }

    @Override
    protected void executeImplicit() {
        // No need to do any playback action here.
        this.control.clear(this.getId());
    }

    @Override
    public void commit() {
        super.commit();
        this.control.applyState(this.targetState);
        // Inactive states do not have a full status
        this.control.applyDetails(null);
        this.control.reset();
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        // Stopping is valid if the same domain is still active.
        //return ds.exclusiveState.active && ds.equalsExclusiveDomain(this.type.getDomain());
        return this.control.getCurrentState().active;
    }

    @Override
    public boolean discards(EventResult eventResult) {
        super.discards(eventResult);
        return eventResult != null && eventResult.domain == getDomain() && eventResult.event.mDiscardable;
    }
}
