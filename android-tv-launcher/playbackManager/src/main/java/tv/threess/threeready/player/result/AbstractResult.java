/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.result;

import tv.threess.threeready.player.model.PlaybackDetails;

/**
 * The base implementation for all playback results.
 *
 * @author Dan Titiu
 * @since 14.08.2014
 */
public abstract class AbstractResult implements Result {

    /**
     * Playback details at the moment of result. Might be null for results for commands that do not perform (discarded).
     */
    protected PlaybackDetails details;

    @Override
    public void complete(final PlaybackDetails details) {
        this.details = details;
    }

    @Override
    public PlaybackDetails getDetails() {
        return this.details;
    }
}
