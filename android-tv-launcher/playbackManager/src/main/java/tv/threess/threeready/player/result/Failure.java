/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.result;

import tv.threess.threeready.player.exception.PlaybackError;

/**
 * Interface for all failure results.
 *
 * @author Dan Titiu
 * @since 15/03/2017
 */
public interface Failure {

    PlaybackError getError();

    default PlaybackError.Type getReason() {
        PlaybackError error = getError();
        if (error == null) {
            return PlaybackError.Type.UNKNOWN;
        }
        return error.getReason();
    }
}
