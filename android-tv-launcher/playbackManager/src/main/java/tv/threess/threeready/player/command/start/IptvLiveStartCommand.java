/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command.start;

import android.text.TextUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.DtvControlProximus;
import tv.threess.threeready.player.control.base.PlaybackControl;

/**
 * StartCommand that knows how to resolve and start a dvb-c type channel playback.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.06.06
 */
public class IptvLiveStartCommand extends BroadcastStartCommand {
    private static final String TAG = Log.tag(IptvLiveStartCommand.class);

    private final DtvControlProximus mControl;
    private final TvChannel mChannel;
    private boolean mIsFromFavorites;
    private int mBitrate;

    public IptvLiveStartCommand(long id, PlaybackType type, PlaybackControl control,
                                StartAction startAction, TvChannel channel, IBroadcast broadcast) {
        super(id, type, startAction, control, broadcast);
        mControl = validateControl(control, DtvControlProximus.class);
        mChannel = channel;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        mControl.start(this);
    }

    public String getChannelId() {
        return mChannel.getId();
    }

    public TvChannel getChannel() {
        return mChannel;
    }

    public void setIsFromFavorites(boolean isFromFavorites) {
        mIsFromFavorites = isFromFavorites;
    }

    public boolean isFromFavorites() {
        return mIsFromFavorites;
    }

    public void setBitrate(String bitrate) {
        if (TextUtils.isEmpty(bitrate)) {
            mBitrate = 0;
            return;
        }
        try {
            mBitrate = Integer.parseInt(bitrate);
        } catch (Exception e) {
            Log.w(TAG, "Failed to parse bitrate: " + bitrate, e);
            mBitrate = 0;
        }
    }

    @Override
    public int getBitrate() {
        return mBitrate;
    }

    @Override
    public boolean isLive() {
        return true;
    }

    @Override
    public String toString() {
        return super.toString("channelId", this.getChannelId());
    }

    @Override
    public IptvLiveStartCommand createReTuneCommand(StartAction action, long cid) {
        return new IptvLiveStartCommand(cid, type, control, action, mChannel, getContentItem());
    }

    @Override
    public Comparable<?> playbackId() {
        return getChannelId();
    }

    @Override
    public IBroadcast getContentItem() {
        long timeStamp = mControl.getCurrentPosition();
        return getBroadcast(timeStamp, true);
    }

    @Override
    public long getTimeout() {
        return control.getDispatchTimeout();
    }
}
