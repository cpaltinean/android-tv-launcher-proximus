/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.result;

import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.contract.PlaybackState;

/**
 * TODO: Javadoc
 *
 * @author Dan Titiu
 * @since 14.08.2014
 */
public abstract class FuzzyResult extends AbstractResult {

    /**
     * Playback domain on which action was taken and result was received from lower API.
     * Used to match the result with the command.
     */
    protected final PlaybackDomain domain;

    /**
     * PlaybackState targeted by the matching PlaybackCommand. Used to match the result with the command.
     */
    protected final PlaybackState state;

    /**
     * Playback command ID which this result is paired with.
     */
    protected long cid;

    protected PlaybackCommand cmd;

    /**
     * Default constructor.
     *
     * @param domain    PlaybackDomain on which action was taken and result was received from lower API.
     * @param state     PlaybackState targeted by the matching PlaybackCommand.
     */
    public FuzzyResult(PlaybackDomain domain, PlaybackState state) {
        this.domain = domain;
        this.state = state;
    }

    @Override
    public boolean matches(final PlaybackCommand cmd) {
        return cmd != null && cmd.getDomain().equals(this.domain) && cmd.getTargetState().equals(this.state);
    }

    @Override
    public void pairWith(PlaybackCommand cmd) {
        this.cmd = cmd;
        this.cid = cmd.getId();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(this.getClass().getSimpleName());
        sb.append(this.getClass().getSimpleName());
        sb.append("{domain[");
        sb.append(this.domain);
        sb.append("],state[");
        sb.append(this.state);
        if (this.cid > 0L) {
            sb.append("],cid[");
            sb.append(this.cid);
        }
        sb.append("]}");
        return sb.toString();
    }
}
