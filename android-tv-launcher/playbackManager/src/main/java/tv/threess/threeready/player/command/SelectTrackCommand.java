/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command;

import tv.threess.threeready.player.command.base.ImplicitCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.model.PlaybackEntireState;

/**
 *  {@link PlaybackCommand} that selects (or deselects) a media track as audio or subtitle tracks.
 *  It doesn't change the state of the player.
 *
 * @author Dan Titiu
 * @since 22/02/2017
 */
public abstract class SelectTrackCommand extends ImplicitCommand {

    public SelectTrackCommand(long id, String trackId) {
        super(id, null, PlaybackAction.Select, PlaybackState.None);// Dummy target state, should not be used

        this.trackId = trackId;
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        // Selecting a media track is valid as long as the player is active
        return control.getCurrentState().active;
    }

    @Override
    public boolean equals(Object o) {
        if (super.equals(o) && o instanceof SelectTrackCommand) {
            SelectTrackCommand other = (SelectTrackCommand) o;
            return this.trackId == other.trackId;
        }

        return false;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.replace(sb.length() - 1, sb.length(), ",trackId[");
        sb.append(this.trackId);
        sb.append("]}");
        return sb.toString();
    }

    //
    // PRIVATE IMPLEMENTATION
    //

    protected final String trackId;
}
