package tv.threess.threeready.player.result;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.exception.PlaybackError;

/**
 * PlaybackEvent wrapper that carries a EventFailure to be dispatched to the PlaybackInterface by the ResultDispatcher.
 *
 */
public class EventFailure extends EventResult implements Failure {
    private static final String TAG = Log.tag(EventFailure.class);

    private final PlaybackError error;

    public EventFailure(final PlaybackEvent event, final PlaybackDomain domain, PlaybackError error) {
        super(event, domain);
        this.error = error;
    }

    public EventFailure(final PlaybackEvent event, final PlaybackDomain domain, Failure failure) {
        this(event, domain, failure.getError());
    }

    @Override
    public boolean isFavorable() {
        return false;
    }

    @Override
    public void dispatchTo(IPlaybackCallback callback) {
        try {
            callback.onPlaybackEvent(this.event, this.details, this.error);
        } catch (Exception e) {
            Log.e(TAG, "Failed to dispatch event [" + this.event + "]", e);
        }
    }

    @Override
    public PlaybackError getError() {
        return this.error;
    }
}
