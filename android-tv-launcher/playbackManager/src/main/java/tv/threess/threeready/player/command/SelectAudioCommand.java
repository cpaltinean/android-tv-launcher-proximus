/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command;

import tv.threess.threeready.player.command.base.PlaybackCommand;

/**
 * {@link PlaybackCommand} that selects an audio track. One audio track is always selected while player is Started.
 *
 * @author Dan Titiu
 * @since 22/02/2017
 */
public class SelectAudioCommand extends SelectTrackCommand {

    public SelectAudioCommand(long id, String trackId) {
        super(id, trackId);
    }

    @Override
    protected void executeImplicit() throws Exception {
        this.control.selectAudioTrack(this.id, this.trackId);
    }
}
