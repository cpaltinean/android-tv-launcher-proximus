package tv.threess.threeready.player.inputservice;

import android.content.Context;
import android.media.PlaybackParams;
import android.media.tv.TvInputManager;
import android.media.tv.TvInputService;
import android.media.tv.TvTrackInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.view.Surface;
import android.view.View;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.listener.PlayerWrapperListener;
import tv.threess.threeready.player.model.OttPlayListItem;
import tv.threess.threeready.player.model.OttTuneParams;
import tv.threess.threeready.player.wrapper.ITifPlayerWrapper;


/**
 * Tv input service for ott streams
 * Uses ExoPlayer
 *
 * @author Daniel Gliga
 * @since 2017.06.06
 */
public class OttInputService extends TvInputService {
    public static final String TAG = Log.tag(OttInputService.class);

    @Override
    public Session onCreateSession(String inputId) {
        Log.d(TAG, "onCreateSession() called with: inputId = [" + inputId + "]");
        return new OttSession(this);
    }

    /*
     * SESSION CLASS
    */
    public static class OttSession extends Session implements PlayerWrapperListener {
        public static final String EXTRA_TUNE_PARAMS = "TUNE_PARAMS";
        public static final String EXTRA_PLAYLIST_PARAMS = "PLAYLIST_PARAMS";
        public static final String EXTRA_NOTIFICATION_RECEIVER = "NOTIFICATION_RECEIVER";

        public static final String ACTION_APPEND = "action_append";
        public static final String ACTION_PREPEND = "action_prepend";

        private final ITifPlayerWrapper mPlayerWrapper = Components.get(ITifPlayerWrapper.class);

        public OttSession(Context context) {
            super(context);

            mPlayerWrapper.setPlayerWrapperListener(this);
        }

        @Override
        public void onRelease() {
            mPlayerWrapper.release();
        }

        @Override
        public boolean onSetSurface(Surface surface) {
            mPlayerWrapper.setSurface(surface);
            return true;
        }

        @Override
        public void onSetStreamVolume(float volume) {
            mPlayerWrapper.setStreamVolume(volume);
        }

        @Override
        public View onCreateOverlayView() {
            // Show the subtitle in this view.
            Log.d(TAG, "onCreateOverlayView()");
            return mPlayerWrapper.createOverlayView();
        }

        @Override
        public boolean onTune(Uri playbackUri, Bundle params) {
            if (params == null) {
                Log.e(TAG, "Tune params cannot be null.");
                return false;
            }

            params.setClassLoader(getClass().getClassLoader());
            ResultReceiver rr = params.getParcelable(EXTRA_NOTIFICATION_RECEIVER);

            Log.d(TAG, "OnTune channelUri: " + playbackUri);
            notifyVideoUnavailable(TvInputManager.VIDEO_UNAVAILABLE_REASON_TUNING);

            // Get tune params.
            OttTuneParams tuneParams = params.getParcelable(EXTRA_TUNE_PARAMS);
            if (tuneParams == null) {
                Log.e(TAG, "No tune param provided.");
                return false;
            }


            setOverlayViewEnabled(true);
            mPlayerWrapper.play(tuneParams, rr);
            return true;
        }

        @Override
        public boolean onTune(Uri channelUri) {
            return onTune(channelUri, Bundle.EMPTY);
        }


        /**
         * Processes a private command sent from the application to the TV input. This can be used
         * to provide domain-specific features that are only known between certain TV inputs and
         * their clients.
         *
         * @param action Name of the command to be performed. This <em>must</em> be a scoped name,
         *            i.e. prefixed with a package name you own, so that different developers will
         *            not create conflicting commands.
         * @param data Any data to include with the command.
         */
        @Override
        public void onAppPrivateCommand(@NonNull String action, Bundle data) {
            Log.d(TAG, "Execute private command. Action :" + action);

            switch (action) {
                default:
                    Log.w(TAG, "Private command not supported. Action : " + action);
                    break;
                case ACTION_APPEND:
                    appendPlaylist(data);
                    break;
                case ACTION_PREPEND:
                    prependPlaylist(data);
                    break;
            }
        }

        /**
         * Prepend the playlist with a URL for continuous playback.
         * @param data The bundle which holds the playlist items.
         */
        private void prependPlaylist(Bundle data) {
            data.setClassLoader(getClass().getClassLoader());
            ArrayList<OttPlayListItem> prependParams = data.getParcelableArrayList(EXTRA_PLAYLIST_PARAMS);
            if (prependParams == null) {
                Log.w(TAG, "Skip playlist prepend. No params available.");
                return;
            }
            mPlayerWrapper.prepend(prependParams);
        }

        /**
         * Append the playlist with a URL for continuous playback.
         * @param data The bundle which holds the playlist items.
         */
        private void appendPlaylist(Bundle data) {
            data.setClassLoader(getClass().getClassLoader());
            ArrayList<OttPlayListItem> appendParams = data.getParcelableArrayList(EXTRA_PLAYLIST_PARAMS);
            if (appendParams == null) {
                Log.w(TAG, "Skip playlist append. No params available.");
                return;
            }
            mPlayerWrapper.append(appendParams);
        }

        @Override
        public void onSetCaptionEnabled(boolean enabled) {
            mPlayerWrapper.setCaptionEnabled(enabled);
        }

        @Override
        public boolean onSelectTrack(int type, String trackId) {
            switch (type) {
                case TvTrackInfo.TYPE_AUDIO:
                    mPlayerWrapper.setAudioTrack(trackId);
                    break;
                case TvTrackInfo.TYPE_SUBTITLE:
                    mPlayerWrapper.setSubtitleTrack(trackId);
                    break;
                case TvTrackInfo.TYPE_VIDEO:
                    mPlayerWrapper.setVideoTrack(trackId);
                    break;
            }
            return true;
        }

        @Override
        public void onSurfaceChanged(int format, int width, int height) {
            super.onSurfaceChanged(format, width, height);
            mPlayerWrapper.onSurfaceChanged(format, width, height);
        }

        @Override
        public void onTimeShiftPause() {
            mPlayerWrapper.pause();
        }

        @Override
        public void onTimeShiftResume() {
            mPlayerWrapper.resume();
        }

        @Override
        public void onTimeShiftSeekTo(long timeMs) {
            mPlayerWrapper.seekTo(timeMs);
        }

        @Override
        public long onTimeShiftGetCurrentPosition() {
            return mPlayerWrapper.getCurrentPosition();
        }

        @Override
        public void onTimeShiftSetPlaybackParams(PlaybackParams params) {
            mPlayerWrapper.setPlaybackParams(params);
        }
    }
}
