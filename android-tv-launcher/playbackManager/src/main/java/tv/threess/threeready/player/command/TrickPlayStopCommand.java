package tv.threess.threeready.player.command;

import androidx.annotation.Nullable;

import tv.threess.threeready.player.command.base.ExplicitCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.TrickPlayControlProximus;
import tv.threess.threeready.player.control.base.BaseTvViewControl;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackEntireState;

/**
 * Explicitly stop the trick play.
 * This command force stops the trick play session, ignoring the control state.
 *
 * @author Andor Lukacs
 * @since 2/5/19
 */
public class TrickPlayStopCommand extends ExplicitCommand {

    private final PlaybackControl<?> mActiveControl;
    private final TrickPlayControlProximus mTrickPlayControl;

    /**
     * Default constructor.
     *
     * @param id      Unique identifier for each command.
     * @param control Playback control onto which the command will execute.
     */
    public TrickPlayStopCommand(long id, PlaybackControl<?> control, PlaybackControl<?> activeControl) {
        super(id, PlaybackType.TrickPlay, PlaybackAction.Stop, control, PlaybackState.Stopped);
        mActiveControl = validateControl(activeControl, BaseTvViewControl.class);
        mTrickPlayControl = validateControl(control, TrickPlayControlProximus.class);
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        mActiveControl.stopTrickPlay(id, mTrickPlayControl);
    }

    @Override
    public void commit() {
        super.commit();
        applyStates();
    }

    @Override
    public void rollback(PlaybackError error, @Nullable Long nextCmdId) throws Exception {
        super.rollback(error, nextCmdId);
        applyStates();
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        return true;
    }

    /**
     * Applies the states for trick play and active control.
     */
    private void applyStates() {
        mTrickPlayControl.applyState(this.targetState);
        // Inactive states do not have a full status
        mTrickPlayControl.applyDetails(null);
        mTrickPlayControl.reset();

        if (mActiveControl.getCurrentState() == PlaybackState.TrickPlayPaused
                || mActiveControl.getCurrentState() == PlaybackState.TrickPlay) {
            // Update the state of the active control
            if (mActiveControl.getCurrentState() == PlaybackState.TrickPlayPaused) {
                mActiveControl.applyState(PlaybackState.Paused);
                return;
            }
            // Set the default state
            mActiveControl.applyState(PlaybackState.Started);
        }
    }
}
