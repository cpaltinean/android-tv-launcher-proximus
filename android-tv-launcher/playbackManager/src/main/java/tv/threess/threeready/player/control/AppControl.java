/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.control;

import android.content.Context;
import android.content.Intent;
import android.media.tv.TvView;
import android.text.TextUtils;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.gms.GmsRepository;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.command.start.AppStartCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.result.ExactFailure;
import tv.threess.threeready.player.result.ExactSuccess;

/**
 * Controller class for applications
 *
 * @author Karetka Mezei Zoltan
 * @since 2018.01.16
 */
public class AppControl extends PlaybackControl<TvView> {
    protected final String TAG = Log.tag(getClass());

    /**
     * Default dispatching timeout for application start command.
     */
    private static final long TIMEOUT_DEFAULT = TimeUnit.SECONDS.toMillis(2);

    private final GmsRepository mGmsRepository = Components.get(GmsRepository.class);

    public AppControl(Context context, PlaybackDomain domain, TvView tvView) {
        super(context, domain, tvView);
    }

    @Override
    protected void fillMediaTrackInfo(PlaybackDetailsBuilder details) {
        //Do nothing
    }

    @Override
    protected String getStreamUrl() {
        return null;
    }

    public void start(AppStartCommand command) throws Exception {
        blockingPostOnUiThread(() -> {
            Intent intent = command.getIntent();
            String packageName = command.getPackageName();
            String activityName = command.getActivityName();

            Log.d(TAG, "openApplication");

                boolean openApp;
                if (intent != null) {
                    openApp = PackageUtils.openApplication(context.get(), packageName, intent);
                } else if (TextUtils.isEmpty(activityName)) {
                    openApp = PackageUtils.openApplication(context.get(), packageName, command.getExtras());
                } else {
                    openApp = PackageUtils.openApplication(context.get(), packageName, activityName, command.getExtras());
                }

            if (openApp) {
                offerResult(new ExactSuccess(command.getId()));
            } else {
                offerResult(new ExactFailure(command.getId(), PlaybackError.Type.INVALID));
            }

            // Save last open time.
            mGmsRepository
                    .updateAppLastOpenedTime(packageName, System.currentTimeMillis())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe();
        });
    }

    @Override
    public void stop(final long cid) {
        Log.d(TAG, "stopApplication: nothing to do");
        offerResult(new ExactSuccess(cid));
    }

    @Override
    public void pause(long cid) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    public void resume(long cid) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    public void jump(long cid, long position) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    public void setSpeed(long cid, double playbackSpeed) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    public void selectAudioTrack(long cid, String trackId) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    public void selectSubtitleTrack(long cid, String trackId) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    public void mute(long cid) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    public void unMute(long cid) {
        offerResult(new ExactFailure(cid, PlaybackError.Type.INVALID));
    }

    @Override
    protected boolean canPause() {
        return false;
    }

    @Override
    public long getDispatchTimeout() {
        return TIMEOUT_DEFAULT;
    }
}
