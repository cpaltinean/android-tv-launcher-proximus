/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.contract.CommandValidity;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.InvalidControl;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.model.PlaybackEntireState;

/**
 * StartCommand which is executed when some prerequisites for other Start Commands are not met.
 *
 * @author Istvan Nagy
 * @since 2019.05.09
 */
public class InvalidStartCommand extends StartCommand {

    protected InvalidControl mInvalidControl;
    protected final String mChannelId;

    public InvalidStartCommand(long id, PlaybackType type, PlaybackControl control,
                               StartAction startAction, String channelId) {
        super(id, type, startAction, control);
        mInvalidControl = validateControl(control, InvalidControl.class);
        mChannelId = channelId;
    }

    @Override
    public CommandValidity validate(PlaybackEntireState ds) {
        return CommandValidity.Invalid;
    }

    @Override
    public void execute() throws Exception {
        super.execute();
        mInvalidControl.start(getId(), mChannelId);
    }

    @Override
    public String toString() {
        return super.toString("channelId", mChannelId);
    }

    @Override
    public InvalidStartCommand createReTuneCommand(StartAction action, long cid) {
        return new InvalidStartCommand(cid, type, control, action, mChannelId);
    }

    @Override
    public Comparable<?> playbackId() {
        return null;
    }
}
