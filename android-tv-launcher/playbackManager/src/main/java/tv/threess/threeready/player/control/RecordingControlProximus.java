/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.control;

import android.content.Context;
import android.media.tv.TvView;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.PlayerConfig;
import tv.threess.threeready.api.generic.helper.NumberUtils;
import tv.threess.threeready.api.generic.model.BookmarkType;
import tv.threess.threeready.api.generic.model.ContinueWatchingConfig;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.playback.model.IRecordingOttStreamingSession;
import tv.threess.threeready.api.playback.model.OTTStreamingSessionContainer;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.player.command.start.RecordingStartCommand;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.control.base.SessionBasedControlProximus;

/**
 * Playback controller for recording ott streams.
 *
 * @author Barabas Attila
 * @since 2018.10.24
 */
public class RecordingControlProximus extends SessionBasedControlProximus<IRecordingOttStreamingSession,
        OTTStreamingSessionContainer<IRecordingOttStreamingSession>> {

    private final TvRepository mTvRepository = Components.get(TvRepository.class);

    private final long mLiveRecordingDelay;

    private IRecording mRecording;

    public RecordingControlProximus(Context context, PlaybackDomain domain, TvView tvView) {
        super(context, domain, tvView, new OTTStreamingSessionContainer<>());
        mLiveRecordingDelay = Components.get(PlayerConfig.class)
                .getStartOverAppearingDelay(TimeUnit.MILLISECONDS);
    }

    /**
     * Start recording playback on the OTT input service.
     */
    public void start(RecordingStartCommand command) throws Exception {
        command.assertNotCancelled();
        IRecording recording = command.getContentItem();

        IRecordingOttStreamingSession session =
                mSessionHandler.openRecordingStreamingSession(recording);

        command.assertNotCancelled();
        mRecording = recording;
        Long relativePosition = command.getPosition();
        IBookmark bookmark = mBookmarkRepository.getBookmark(recording, BookmarkType.Recording);

        mIsResumedBookmark = false;

        // no start position (no start-over), fallback to bookmark position
        if (relativePosition == null && bookmark.getPosition() > 0) {
            double maxWatchedPercentage = Components.get(ContinueWatchingConfig.class).getRecordingWatchedPercent();
            boolean isNearEnd = bookmark.isNearEnd(maxWatchedPercentage);
            relativePosition = isNearEnd ? 0 : bookmark.getPosition();
            mIsResumedBookmark = true;
        } else if (relativePosition == null && mRecording.isLiveRecording() && bookmark.getPosition() <= 0) {
            // In case of live recording the relative position should be 1, otherwise the stream will start
            // from live point
            relativePosition = 1L;
        }

        start(command.getId(), recording.getRecordingStart(), recording.getRecordingEnd(), recording.getAssetIdentifier(), relativePosition, session);
    }

    @Override
    public void processPosition(long contentPosition, boolean notifyChange) {
        super.processPosition(contentPosition, notifyChange);

        // The end of the recorded content reached. Stop the playback.
        if (contentPosition >= getContentEnd()
                && getCurrentState() == PlaybackState.Started) {
            offerEvent(PlaybackEvent.EndReached);
        }
    }

    @Override
    public void reset() {
        saveBookmark();
        super.reset();
        mRecording = null;
    }

    @Override
    public long getSeekAbleEnd() {
        long seekAbleEnd = super.getSeekAbleEnd();
        // In case the end is in the future we are on an ongoing recording
        // We need to safeguard the end position so trickplay is not possible from the live position
        if (System.currentTimeMillis() < getContentEnd()) {
            seekAbleEnd -= mLiveRecordingDelay;
        }
        return seekAbleEnd;
    }

    @Override
    public long getBufferEnd() {
        if (System.currentTimeMillis() < getContentEnd()) {
            return System.currentTimeMillis() - mLiveRecordingDelay;
        }
        return super.getBufferEnd();
    }

    @Override
    public String getPlaybackId() {
        return mRecording == null ? null : mRecording.getRecordingId();
    }

    private void saveBookmark() {
        long start = getContentStart();
        long end = getContentEnd();
        long position = NumberUtils.clamp(getCurrentPosition(), start, end);
        mTvRepository.addRecordingBookmark(mRecording,
                position - start, end - start)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }
}
