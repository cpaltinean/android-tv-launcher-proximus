/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.contract;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Defines possible state for playback. Each PlaybackDomain is at every moment in time into one of the states defined
 * here.
 *
 * @author Dan Titiu
 * @since 06.05.2014
 */
public enum PlaybackState implements Parcelable {

    /**
     * Represents the stopped playback state with all states released (e.g. bandwidth reservations).
     * Domains with this state are considered inactive.
     */
    Released(false),

    /**
     * Represents the stopped playback state with cleared screen.
     * Domains with this state are considered inactive.
     */
    Blanked(false),

    /**
     * Represents the stopped playback state (possibly with last video frame on the screen).
     * Domains with this state are considered inactive.
     */
    Stopped(false),

    /**
     * Represents the playback state of a control which becomes inactive (i.e. stopped) without stopping the underlying
     * player. This state is required for controls on exclusive domains that share the same underlying player.
     */
    Inactive(false),

    /**
     * Represents the playback state when video is playing or application is started.
     */
    Started,

    /**
     * Represents the paused playback state (with last video frame on the screen).
     */
    Paused,

    /**
     * Represents the playback state when video is playing with a custom speed.
     */
    TrickPlay,

    /**
     * Represents the playback state when video is paused but the video in trick play view is playing with a custom speed.
     */
    TrickPlayPaused,

    /**
     * Used for stateless commands.
     */
    None(false);

    /**
     * Differentiates between playback states during which a player is active (running) and those states in which a
     * player is considered inactive, not running. This is useful for exclusive domains where only one domain may be
     * active at a point in time.
     */
    public final boolean active;

    PlaybackState() {
        this.active = true;
    }

    PlaybackState(boolean active) {
        this.active = active;
    }

    //
    // PARCELABLE SPECIFIC FIELDS AND METHODS
    //

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name());
    }

    public static final Parcelable.Creator<PlaybackState> CREATOR = new ParcelableCreator();

    private static final class ParcelableCreator implements Parcelable.Creator<PlaybackState> {

        @Override
        public PlaybackState createFromParcel(final Parcel source) {
            return PlaybackState.valueOf(source.readString());
        }

        @Override
        public PlaybackState[] newArray(final int size) {
            return new PlaybackState[size];
        }
    }
}
