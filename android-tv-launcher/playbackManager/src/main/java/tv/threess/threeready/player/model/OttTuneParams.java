package tv.threess.threeready.player.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Model representing the parameters necessary to start OTT playback playlist.
 *
 * @author Barabas Attila
 * @since 2020.06.29
 */
public class OttTuneParams implements Parcelable {
    private long mStartPosition;
    private int mDefaultItemIndex;
    private String mAssetIdentifier;

    private ArrayList<OttPlayListItem> mPlayListItems = new ArrayList<>();

    public OttTuneParams() {
    }

    protected OttTuneParams(Parcel in) {
        mStartPosition = in.readLong();
        mDefaultItemIndex = in.readInt();
        mAssetIdentifier = in.readString();
        mPlayListItems = in.createTypedArrayList(OttPlayListItem.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mStartPosition);
        dest.writeInt(mDefaultItemIndex);
        dest.writeString(mAssetIdentifier);
        dest.writeTypedList(mPlayListItems);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OttTuneParams> CREATOR = new Creator<OttTuneParams>() {
        @Override
        public OttTuneParams createFromParcel(Parcel in) {
            return new OttTuneParams(in);
        }

        @Override
        public OttTuneParams[] newArray(int size) {
            return new OttTuneParams[size];
        }
    };

    /**
     * @return The position relative to the start of the selected playlist item from where the playback should start.
     */
    public long getStartPosition() {
        return mStartPosition;
    }

    /**
     * @param unit The time unit in which the position needs to be returned.
     * @return The position relative to the start of the selected playlist item from where the playback should start.
     */
    public long getStartPosition(TimeUnit unit) {
        return unit.convert(mStartPosition, TimeUnit.MILLISECONDS);
    }

    /**
     * @return The index of the playlist from where the playback should start.
     */
    public int getDefaultItemIndex() {
        return mDefaultItemIndex;
    }

    public String getAssetIdentifier() {
        return mAssetIdentifier;
    }

    /**
     * @return All the items in the playlist for continuous playback.
     */
    public ArrayList<OttPlayListItem> getPlayListItems() {
        return mPlayListItems;
    }

    /**
     * @return The playlist item form where the playback should start by default.
     */
    public OttPlayListItem getDefaultPlaylistItem() {
        return mPlayListItems.get(mDefaultItemIndex);
    }


    /**
     * Builder to create a new tune param.
     */
    public static class Builder {

        private final OttTuneParams mParams = new OttTuneParams();

        public Builder addPlayListItem(OttPlayListItem playListItem) {
            mParams.mPlayListItems.add(playListItem);
            return this;
        }

        public Builder playListItems(ArrayList<OttPlayListItem> playListItems) {
            mParams.mPlayListItems = playListItems;
            return this;
        }

        public Builder startPosition(long position) {
            mParams.mStartPosition = position;
            return this;
        }

        public Builder playListIndex(int index) {
            mParams.mDefaultItemIndex = index;
            return this;
        }

        public Builder assetIdentifier(String value) {
            mParams.mAssetIdentifier = value;
            return this;
        }

        public OttTuneParams build() {
            return mParams;
        }
    }
}
