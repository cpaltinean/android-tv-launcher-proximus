/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command;

import tv.threess.threeready.player.model.PlaybackEntireState;

/**
 * Is same as StopCommand but it doesn't check the playerState
 *
 * @author Daniel Gliga
 * @since 2017.11.14
 */

public class ForceStopCommand extends StopCommand {
    public ForceStopCommand(long id) {
        super(id);
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        // it is execute even in playbackstate is inactive
        return true;
    }
}
