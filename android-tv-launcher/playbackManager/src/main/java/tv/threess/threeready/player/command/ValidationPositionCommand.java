package tv.threess.threeready.player.command;

import tv.threess.threeready.player.command.base.ImplicitCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.model.PlaybackEntireState;

/**
 * TODO: Class description
 *
 * @author Barabas Attila
 * @since 2020.08.08
 */
public class ValidationPositionCommand extends ImplicitCommand {

    private final long mPosition;

    public ValidationPositionCommand(long id, long position) {
        super(id,null, PlaybackAction.Validate, PlaybackState.Started);
        mPosition = position;
    }

    @Override
    protected void executeImplicit() {
        control.validatePosition(getId(), mPosition);
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        return control.getCurrentState().active;
    }
}
