/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.result;

import java.util.Arrays;

import androidx.annotation.NonNull;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.api.playback.player.PlaybackDomain;

/**
 * Command result that matches the command upon domain and action(s).
 *
 * @author Dan Titiu
 * @since 28.04.2014
 */
public abstract class VagueResult extends AbstractResult {

    /**
     * Playback domain on which action was taken and result was received from lower API.
     * Used to match the result with the command.
     */
    protected final PlaybackDomain domain;

    /**
     * Playback action which generated this result. Used to match the result with the command.
     */
    protected final PlaybackAction[] action;

    /**
     * Playback command ID which this result is paired with.
     */
    protected long cid;

    protected PlaybackCommand cmd;

    /**
     * Default constructor.
     *
     * @param domain    Playback domain on which action was taken and result was received from lower API.
     * @param action    Playback action which generated this result.
     */
    public VagueResult(PlaybackDomain domain, @NonNull PlaybackAction... action) {
        this.domain = domain;
        Arrays.sort( action );// Sort the enum array for easy search in match().
        this.action = action;
    }


    @Override
    public boolean matches(final PlaybackCommand cmd) {
        return cmd != null && cmd.getDomain().equals(this.domain)
                && Arrays.binarySearch(this.action, cmd.getAction()) >= 0;
    }

    @Override
    public void pairWith(PlaybackCommand cmd) {
        this.cmd = cmd;
        this.cid = cmd.getId();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(this.getClass().getSimpleName());
        sb.append("{domain[");
        sb.append(this.domain);
        sb.append("],action");
        sb.append(Arrays.toString(this.action));
        if (this.cid > 0L) {
            sb.append(",cid[");
            sb.append(this.cid);
            sb.append("]}");
        } else {
            sb.append('}');
        }
        return sb.toString();
    }
}
