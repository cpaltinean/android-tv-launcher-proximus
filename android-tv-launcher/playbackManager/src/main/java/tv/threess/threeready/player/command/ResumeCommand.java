/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.player.command.base.ImplicitCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.result.EventResult;

/**
 * Implicit command that transition the state from Paused to Started.
 *
 * @author Dan Titiu
 * @since 13.08.2014
 */
public class ResumeCommand extends ImplicitCommand {

    private final StartAction mStartAction;

    public ResumeCommand(long id) {
        this(id, StartAction.Resume);
    }

    public ResumeCommand(long id, StartAction startAction) {
        this(id, PlaybackAction.Resume, PlaybackState.Started, startAction);
    }

    protected ResumeCommand(long id, PlaybackAction playbackAction, PlaybackState targetState, StartAction startAction) {
        super(id, null, playbackAction, targetState);
        mStartAction = startAction;
    }

    @Nullable
    public StartAction getStartAction() {
        return mStartAction;
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        // Resuming is valid on the same domain, while active and not Started
        final PlaybackState state = this.control.getCurrentState();
        switch (state) {
            case Started:
                return false;
            default:
                return state.active;
        }
    }

    @Override
    protected void executeImplicit() throws Exception {
        this.control.resume(this.getId());
    }

    @Override
    public void commit() {
        super.commit();
        this.control.applyState(this.targetState);
    }

    @Override
    public boolean discards(EventResult eventResult) {
        super.discards(eventResult);
        return eventResult != null && eventResult.domain == getDomain() && eventResult.event.mDiscardable;
    }
}
