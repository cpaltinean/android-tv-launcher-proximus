package tv.threess.threeready.player.wrapper;

import android.os.ResultReceiver;

import tv.threess.lib.di.Component;
import tv.threess.threeready.player.model.OttTuneParams;

/**
 * Interface for Player wrapper based on TvView playback.
 *
 * @author Andor Lukacs
 * @since 6/4/19
 */
public interface ITifPlayerWrapper<T> extends IPlayerWrapper<T>, Component {
    int NOTIFY_PLAYER_STATE_CHANGE = 0x100;
    int NOTIFY_SEGMENT_DOWNLOADED = 0x102;
    int NOTIFY_BITRATE_CHANGE = 0x103;
    int NOTIFY_DURATION_CHANGE = 0x104;
    int NOTIFY_PLAYLIST_ITEM_CHANGED = 0x105;
    int NOTIFY_PLAYBACK_ITEM_ADDED = 0x106;
    int NOTIFY_PLAYBACK_EXCEPTION_RECEIVED = 0x107;
    int NOTIFY_UNSKIPPABLE_TIMELINE_CHANGED = 0x108;

    String EXTRA_PLAYBACK_DURATION = "playbackDuration";
    String EXTRA_UNSKIPPABLE_STARTS = "unskippableStarts";
    String EXTRA_UNSKIPPABLE_ENDS = "unskippableEnds";
    String EXTRA_PLAYLIST_ITEM = "playbackItemIndex";

    int VIDEO_UNAVAILABLE_ERROR_FLAG = 0x200;
    int VIDEO_UNAVAILABLE_ERROR_MASK = 0x0ff;

    /**
     * Method used for playing the selected stream with the given parameters.
     *
     * @param tuneParams The parameters necessary to start the playback
     * @param resultReceiver Receiver to get notification about the playback state changes.
     *                       E.g. duration change, track selection change etc.
     */
    void play(OttTuneParams tuneParams, ResultReceiver resultReceiver);

}