package tv.threess.threeready.player.command;

import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.command.base.ImplicitCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.ReplayControlProximus;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.model.PlaybackEntireState;

/**
 * Command to update the playlist to cover all the broadcast in the continuous replay list with playback.
 *
 * @author Barabas Attila
 * @since 2020.07.21
 */
public class UpdateReplayPlaylistCommand extends ImplicitCommand {

    private ReplayControlProximus mControl;

    public UpdateReplayPlaylistCommand(final long id) {
        super(id, null, PlaybackAction.Update, PlaybackState.None);
    }

    @Override
    public PlaybackCommand prepare(PlaybackType type, PlaybackControl control) {
        mControl = validateControl(control, ReplayControlProximus.class);
        return super.prepare(type, control);
    }

    @Override
    protected void executeImplicit() throws Exception {
        if (getDomain() == PlaybackDomain.Replay) {
            mControl.updatePlaylist(this);
        }
    }

    @Override
    public boolean verify(PlaybackEntireState ds) {
        return mControl.getCurrentState().active;
    }
}
