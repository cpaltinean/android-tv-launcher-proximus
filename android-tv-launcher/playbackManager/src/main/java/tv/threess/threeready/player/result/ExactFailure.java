/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.result;

import androidx.annotation.Nullable;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.exception.PlaybackError;

/**
 * Command result representing a failure that matches the command by its ID.
 *
 * @author Dan Titiu
 * @since 12.08.2014
 */
public class ExactFailure extends ExactResult implements Failure {
    private static final String TAG = Log.tag(ExactFailure.class);

    private final PlaybackError error;

    public ExactFailure(final long cid, PlaybackError error) {
        super(cid);
        this.error = error;
    }

    public ExactFailure(final long cid, PlaybackError.Type reason) {
        this(cid, new PlaybackError(reason));

    }

    @Override
    public void dispatchTo(IPlaybackCallback callback) {
        if (this.cid <= 0) {
            throw new IllegalStateException("Trying to dispatch callback without command id");
        }

        try {
            callback.onCommandFailure(this.cmd, this.details, this.getError());
        } catch (Exception e) {
            Log.e(TAG, "Failed to dispatch failure for command id[" + cid + "]", e);
        }
    }

    @Override
    public void apply(PlaybackCommand cmd, @Nullable Long nextCmdId) throws Exception {
        cmd.rollback(this.error, nextCmdId);
    }

    @Override
    public boolean isFavorable() {
        return false;
    }

    @Override
    public PlaybackError getError() {
        return error;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append("{cid[");
        sb.append(this.cid);
        sb.append("],error[");
        sb.append(this.error);
        sb.append("]}");
        return sb.toString();
    }
}
