/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.player.command.base.EventCommand;
import tv.threess.threeready.player.command.base.ImplicitCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackRelation;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.result.ExactFailure;
import tv.threess.threeready.player.result.Result;

/**
 * Queue that manages {@link PlaybackCommand} instances
 * in the order they were requested. The queue requires a thread for scanning and queuing commands. The offering of a
 * playback command needs to be thread safe so it is executed on a single dedicated thread.
 *
 * @author Dan Titiu
 * @since 05.02.2014
 */
public class PlaybackQueue implements Lifecycleable, QueueProvider<PlaybackCommand>, QueueConsumer<PlaybackCommand> {
    static final String TAG = Log.tag(PlaybackQueue.class);// package-private for inner-class optimization

    /**
     * Constructor used only by manager.
     *
     * @param rd            Result (callback) dispatcher.
     * @param cd            PlaybackCommand dispatcher.
     * @param controller    Manager and provider of controls.
     */
    public PlaybackQueue(final WeakReference<ResultDispatcher> rd, final WeakReference<PlaybackDispatcher> cd,
                         final WeakReference<ControlProvider> controller) {
        super();
        this.lock = new ReentrantLock();
        this.notEmpty = lock.newCondition();
        this.userQ = new LinkedList<>();
        this.eventQ = new LinkedList<>();
        this.resultDispatcher = rd;
        this.commandDispatcher = cd;
        this.controller = controller;
    }

    @Override
    public void onCreate() {
        // The queue requires a thread for scanning and queuing commands. The offering of a playback command needs to
        // be thread safe so it is executed on a single dedicated thread.
        final HandlerThread ht = new HandlerThread(":queuing");
        ht.start();
        this.offerHandler = new Handler(ht.getLooper(), new OfferCallback());
        Log.d(TAG, "CREATED");
    }

    @Override
    public void onDestroy() {
        final Looper l = this.offerHandler.getLooper();
        l.quit();
        l.getThread().interrupt();
        this.clear();
        Log.d(TAG, "DESTROYED");
    }

    /**
     * Offer a playback command to the (user) queue asynchronously.
     *
     * @param command    The PlaybackCommand to be queued.
     */
    public boolean offer(PlaybackCommand command) {
        command.offer();
        Message msg = this.offerHandler.obtainMessage();
        msg.obj = command;
        Log.d(TAG, "Offering command: " + command);
        return this.offerHandler.sendMessage(msg);
    }

    /**
     * Offer a playback command to the (user) queue synchronously, waiting up to the specified wait time if necessary and return the
     * result.
     *
     * @param command            The PlaybackCommand to be queued.
     * @param timeout            How long to wait before giving up, in units of <tt>unit</tt>.
     * @param unit               A <tt>TimeUnit</tt> determining how to interpret the <tt>timeout</tt> parameter.
     * @return <tt>true</tt> if successful, or <tt>false</tt> if the specified waiting time elapses before being able to .
     * @throws InterruptedException if interrupted while waiting
     * @throws TimeoutException if the waiting timed out
     * @throws ExecutionException when the FutureTask was aborted
     */
    public boolean offer(PlaybackCommand command, long timeout, TimeUnit unit)
            throws InterruptedException, TimeoutException, ExecutionException {
        final OfferTask t = new OfferTask(command);
        return this.offerHandler.post(t) && t.get(timeout, unit);
    }

    /**
     * Pop the head of the playback (both user and event) queue awaiting until the next command is available.
     *
     * @return The next PlaybackCommand that should be processed.
     * @throws InterruptedException if interrupted while waiting
     */
    public PlaybackCommand take() throws InterruptedException {
        Log.d(TAG, "take() called");
        this.lock.lockInterruptibly();
        try {
            PlaybackCommand c;
            // Event-generated commands have priority over user commands, so poll first the event queue.
            while ( (c = this.eventQ.poll()) == null
                    && (c = this.userQ.poll()) == null ) {

                // While waiting the lock is released until the thread resumes
                this.notEmpty.await();
            }
            return c;
        } finally {
            this.lock.unlock();
        }
    }

    @Override
    public PlaybackCommand peek() throws InterruptedException {
        Log.d(TAG, "peek() called");
        this.lock.lockInterruptibly();
        try {
            PlaybackCommand c;
            // Event-generated commands have priority over user commands, so poll first the event queue.
            if((c = this.eventQ.peek()) == null){
                c = this.userQ.peek();
            }
            return c;
        } finally {
            this.lock.unlock();
        }
    }

    /**
     * Retrieves and removes the head of this queue (both user and event), waiting up to the
     * specified wait time if necessary for an element to become available.
     *
     * @param timeout how long to wait before giving up, in units of
     *        <tt>unit</tt>
     * @param unit a <tt>TimeUnit</tt> determining how to interpret the
     *        <tt>timeout</tt> parameter
     * @return the head of this queue, or <tt>null</tt> if the
     *         specified waiting time elapses before an element is available
     * @throws InterruptedException if interrupted while waiting
     */
    @Override
    public PlaybackCommand poll(long timeout, TimeUnit unit) throws InterruptedException {
        Log.d(TAG, "poll() called with: timeout = [" + timeout + "], unit = [" + unit + "]");
        this.lock.lockInterruptibly();
        try {
            PlaybackCommand c;
            long nanos = unit.toNanos(timeout);
            // Event-generated commands have priority over user commands, so poll first the event queue.
            while ( (c = this.eventQ.poll()) == null
                    && (c = this.userQ.poll()) == null
                    && nanos > 0L ) {

                // While waiting the lock is released until the thread resumes
                nanos = this.notEmpty.awaitNanos(nanos);
            }
            return c;
        } finally {
            this.lock.unlock();
        }
    }

    /**
     * Retrieves and removes all commands with cmdId from userQ
     */
    public Collection<PlaybackCommand> remove(long cmdId) throws InterruptedException{
        Log.d(TAG, "remove() called with: cmdId = [" + cmdId + "]");
        this.lock.lockInterruptibly();
        try {
            if(userQ.isEmpty())
                return Collections.emptyList();

            final List<PlaybackCommand> result = new ArrayList<>();
            PlaybackCommand nextCmd = userQ.peek();
            while (nextCmd != null && nextCmd.getId() == cmdId){
                result.add(nextCmd);
                cancelQueuedCommand(nextCmd);
                userQ.remove(nextCmd);
                nextCmd = userQ.peek();
            }
            return result;
        } finally {
            this.lock.unlock();
        }
    }

    /**
     * Retrieves all content and empties the queue.
     *
     * @return the queued items or an empty list in case the queue is already empty.
     */
    @Override
    public Collection<PlaybackCommand> purge() throws InterruptedException {
        Log.d(TAG, "purge() called");
        this.lock.lockInterruptibly();
        try {
            final int count = this.eventQ.size() + this.userQ.size();
            if (count > 0) {
                final List<PlaybackCommand> result = new ArrayList<>(count);
                PlaybackCommand c;
                // Event-generated commands have priority over user commands, so poll first the event queue.
                while ( (c = this.eventQ.poll()) != null ) {
                    result.add(c);
                }
                while ( (c = this.userQ.poll()) != null ) {
                    result.add(c);
                }

                return result;
            }

            return Collections.emptyList();
        } finally {
            this.lock.unlock();
        }
    }
    
    //
    // PRIVATE METHODS AND FIELDS
    //

    /**
     * Runnable that offers a playback command to the queue asynchronously (does not return the result).
     */
    private class OfferRunnable implements Runnable, Callable<Boolean> {

        private final PlaybackCommand cmd;

        OfferRunnable(final PlaybackCommand cmd) {// package-private for inner-class optimization
            this.cmd = cmd;
        }

        @Override
        public void run() {
            try {
                this.call();
            } catch (Exception e) {
                Log.e(TAG, "Failed to offer " + this.cmd, e);
            }
        }

        @Override
        public Boolean call() throws Exception {
            return PlaybackQueue.this.processCommand(this.cmd);
        }
    }

    /**
     * FutureTask that offers a playback command to the queue synchronously and returns the result of queuing the command.
     */
    private class OfferTask extends FutureTask<Boolean> {

        OfferTask(final PlaybackCommand cmd) {// package-private for inner-class optimization
            super(new OfferRunnable(cmd));
        }
    }

    /**
     * Handler callback for processing the offer.
     */
    private class OfferCallback implements Handler.Callback {

        OfferCallback() {}// package-private for inner-class optimization

        @Override
        public boolean handleMessage(Message msg) {
            try {
                PlaybackQueue.this.processCommand((PlaybackCommand) msg.obj);
            } catch (Exception e) {
                Log.e(TAG, "Failed to offer " + msg.obj, e);
            }
            return true;
        }
    }

    /**
     * The queue for PlaybackCommands coming from UI/user.
     */
    private final Deque<PlaybackCommand> userQ;

    /**
     * The queue for PlaybackCommands generated by background events (e.g. {@link PlaybackEvent#EndReached}).
     */
    private final Deque<EventCommand> eventQ;

    /**
     * Dedicated handler for scanning queue and offering commands synchronously.
     */
    private Handler offerHandler;

    /**
     * Lock held for atomic operation by scanAndQueue, take, etc.
     */
    private final ReentrantLock lock;

    /**
     * Wait queue for waiting takes.
     */
    private final Condition notEmpty;

    /**
     * The provider of the playback controls.
     */
    private final WeakReference<ControlProvider> controller;

    /**
     * Dispatcher of command results callbacks. Needed here for shortcutting feedback on commands that fail before
     * executing (cancellation, delay, invalidation).
     */
    private final WeakReference<ResultDispatcher> resultDispatcher;


    private final WeakReference<PlaybackDispatcher> commandDispatcher;

    /**
     * The last command identifier that ensures commands are executed in their order and helps discarding late arrivals.
     */
    private volatile long lastCommandId = Long.MIN_VALUE;

    /**
     * Reference to the last EXCLUSIVE domain command that was queued which helps prepare implicit commands on the same
     * domain.
     */
    private volatile PlaybackCommand lastCommand;

    /**
     * Scans the command queue, evaluates relations between commands and modifies the queue accordingly before adding
     * the new command. This method must be executed <em>ONLY</em> on the thread of {@link #offerHandler}.
     *
     * @param oc    The offered PlaybackCommand.
     * @return True if the command was successfully queued.
     */
    boolean processCommand(PlaybackCommand oc) throws InterruptedException {// package-private for inner-class optimization
        // Validate order
        if (this.discardDelayed(this.lastCommandId, oc)) {
            return false;
        }

        // Prepare implicit commands with playback type and control from last command.
        if (oc instanceof ImplicitCommand) {
            final PlaybackCommand lc = this.lastCommand;
            try {
                oc = this.prepareImplicitCommand((ImplicitCommand) oc, lc);
            } catch (Exception e) {
                Log.e(TAG, "Failed to prepare ImplicitCommand", e);
                this.postFailedResult(oc, PlaybackError.Type.GENERIC, e);
                return false;
            }
        }

        this.lock.lockInterruptibly();// Locking for Deqeue access
        try {
            final Iterator<PlaybackCommand> i = this.userQ.descendingIterator();
            while (i.hasNext()) {
                final PlaybackCommand qc = i.next();
                // Relate the offered command to the queued one (order is important!)
                final PlaybackRelation r = oc.relate(qc, false);
                // The offered command has precedence over the queue, but it can also be dismissed.
                switch (r) {
                    case Cumulate:
                        // Accumulate the carried value (e.g. FFW, RWD, JUMP)..
                        oc.accumulate(qc);
                        // .. and cancel the one in the queue
                        // Intentional no-break fall-through
                    case CancelOther:
                        // Cancel the one in the queue.
                        Log.d(TAG, qc + " is cancelled by " + oc);
                        i.remove();
                        this.cancelQueuedCommand(qc);
                        break;

                    case CancelBoth:
                        // Cancel the one in the queue and the new one
                        Log.d(TAG, qc + " cancels and is cancelled by " + oc);
                        i.remove();
                        this.cancelQueuedCommand(qc);
                        // Intentional no-break fall-through
                    case CancelSelf:
                        // Cancel the offered command.
                        Log.d(TAG, oc + " self cancelled after relating to " + qc);
                        this.postFailedResult(oc, PlaybackError.Type.CANCELED);
                        // If offered command is not queued, do not save its reference as last command.
                        return false;

                    case None:
                        // Nothing to do..
                        break;

                    default:
                        Log.w(TAG, "Unknown PlaybackRelation [" + r + "]");
                        break;
                }
            }

            final PlaybackDispatcher pd = this.commandDispatcher.get();
            if (pd != null) {
                final PlaybackCommand dc = pd.getDispatchingCommand();
                // Relate the queued command to the currently dispatching one
                if (dc != null) {
                    switch (oc.relate(dc, true)) {
                        case CancelOther:
                            dc.cancel();
                            break;

                        case CancelBoth:
                            dc.cancel();
                            // Intentional no-break fall-through
                        case CancelSelf:
                            // Cancel the offered command.
                            Log.d(TAG, oc + " self cancelled after relating to current command");
                            this.postFailedResult(oc, PlaybackError.Type.CANCELED);
                            // If offered command is not queued, do not save its reference as last command.
                            return false;

                        default:
                            break;
                    }
                }
            }

            // Put the new command into the queue.
            final boolean result = this.userQ.offer(oc);
            if (result) {
                // Save last queued command id
                this.lastCommandId = oc.getId();

                // Save last queued exclusive-domain explicit command
                if (oc.getDomain().exclusive && oc instanceof StartCommand) {
                    this.lastCommand = oc;
                }
                // Signal the awaiting takers
                this.notEmpty.signal();
            }
            return result;
        } finally {
            this.lock.unlock();
        }
    }

    /**
     * Offer a playback command to the event queue asynchronously.
     *
     * @param cmd       The PlaybackCommand to be queued.
     */
    boolean offerEventCommand(EventCommand cmd) {// package-private for inner-class optimization
        this.lock.lock();
        try {
            final boolean result = this.eventQ.offer(cmd);
            // Signal the awaiting takers
            this.notEmpty.signal();
            return result;
        } finally {
            this.lock.unlock();
        }
    }

    /**
     * Clears the playback command queue thread-safely.
     */
    private void clear() {
        this.lock.lock();
        try {
            this.eventQ.clear();
            this.userQ.clear();
            this.lastCommand = null;
            this.lastCommandId = Long.MIN_VALUE;
        } finally {
            this.lock.unlock();
        }
    }

    /**
     * Check if the offered command is late compared to the last processed command and discard it if so.
     *
     * @param lid   The identifier of the last queued command.
     * @param oc    The offered PlaybackCommand.
     * @return True if the offered command was late and discarded.
     */
    private boolean discardDelayed(final long lid, final PlaybackCommand oc) {
        // Discard late arrivals
        if ( lid > oc.getId() ) {
            Log.w(TAG, oc + " was late so it is discarded.");
            this.postFailedResult(oc, PlaybackError.Type.DELAYED);
            return true;
        }

        return false;
    }

    /**
     * Implicit commands lack the playback type and control as they rely on the last command processed implicitly.
     * This method prepares the implicit commands using the last processed command.
     *
     * @param ic    Implicit command to be prepared.
     * @param lc    Last command processed.
     * @return The prepared instance of PlaybackCommand.
     */
    private PlaybackCommand prepareImplicitCommand(final ImplicitCommand ic, final PlaybackCommand lc) {
        PlaybackType type;
        if (lc == null) {
            throw new IllegalStateException("Implicit [" + ic.getAction() + "] called without a previous explicit command");
        }
        
        type = lc.getType();

        final ControlProvider cm = this.controller.get();
        if (cm == null) {
            throw new IllegalStateException("Missing ControlProvider");
        }

        final PlaybackControl<?> control = cm.getControl(type.getDomain());
        if (control == null) {
            throw new IllegalStateException("Missing PlaybackControl for domain " + type.getDomain());
        }

        return ic.prepare( type, control );
    }

    private void postFailedResult(final PlaybackCommand cmd, final PlaybackError.Type reason, Throwable cause) {
        final Result cr = new ExactFailure(cmd.getId(), new PlaybackError(reason, cause));
        // No need to pair exact results
        // We do not decorate with playback status as it is irrelevant for commands that failed before executing.
        this.resultDispatcher.get().postCommandResult(cr);
    }

    private void postFailedResult(final PlaybackCommand cmd, final PlaybackError.Type reason) {
        postFailedResult(cmd, reason, null);
    }

    public void cancelQueuedCommand(PlaybackCommand qc) {
        Log.d(TAG, "cancel command: " + qc.toString());
        // Announce the cancellation to the interface user
        this.postFailedResult(qc, PlaybackError.Type.CANCELED);
        // In case the last command gets cancelled, we need to replace it with the last (successfully) dispatched command
        // As long as all processing is done on the same thread, it's safe to write the field here.
        if (this.lastCommand == qc) {
            final PlaybackDispatcher pd = this.commandDispatcher.get();
            if (pd != null) {
                this.lastCommand = pd.getLastCommand();
                Log.d(TAG, "Using as last command the last executed: " + this.lastCommand);
            }
        }
    }

    /**
     * @param commandTypes  PlaybackCommand class type array
     * @return return true if there is a playback command instance of cClass in userQ
     */
    @SafeVarargs
    public final boolean isCommandQueued(Class<? extends PlaybackCommand>... commandTypes) {
        try {
            this.lock.lockInterruptibly();// Locking for Dequeue access
            for (PlaybackCommand nextCmd : userQ) {
                for (Class<? extends PlaybackCommand> commandType : commandTypes) {
                    if (commandType.isInstance(nextCmd)) {
                        return true;
                    }
                }
            }
        } catch (InterruptedException e) {
            Log.e(TAG, "Could not check if command is queued.", e);
        } finally {
            this.lock.unlock();
        }
        return false;
    }
}
