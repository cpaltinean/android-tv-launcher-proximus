/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Parcelable DTO representing hbb tv user agent components.
 *
 * @author Karetka Mezi Zoltan
 * @author Dan Titiu
 * @since 2016.07.08
 */
public class UserAgent implements Parcelable {

    public static final String SEP_DETAILS = "; ";

    public static class Builder {

        private String productVersion;
        private String platform;
        private final StringBuilder extras = new StringBuilder();

        public Builder setProductVersion(String productVersion) {
            this.productVersion = productVersion;
            return this;
        }

        public Builder setPlatform(String platform) {
            this.platform = platform;
            return this;
        }

        public Builder addExtra(String extra) {
            if (this.extras.length() > 0) {
                this.extras.append(SEP_DETAILS);
            }
            this.extras.append(extra);
            return this;
        }

        public UserAgent build() {
            return new UserAgent(this.productVersion, this.platform, this.extras.toString());
        }
    }

    public UserAgent(String productVersion, String platform, String extras) {
        this.productVersion = productVersion;
        this.platform = platform;
        this.extras = extras;
    }

    public String getProductVersion() {
        return productVersion;
    }

    public String getPlatform() {
        return platform;
    }

    public String getExtras() {
        return extras;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder(this.productVersion);
        result.append(" (");
        String sep = "";
        if (this.platform != null) {
            result.append(sep);
            result.append(this.platform);
            sep = SEP_DETAILS;
        }
        if (this.extras != null) {
            result.append(sep);
            result.append(this.extras);
        }
        result.append(')');
        return result.toString();
    }

    //
    // PARCELABLE
    //

    final static class ParcelableCreator implements Creator<UserAgent> {

        @Override
        public UserAgent createFromParcel(Parcel source) {
            return new UserAgent( source );
        }

        @Override
        public UserAgent[] newArray(int size) {
            return new UserAgent[size];
        }
    }

    /**
     * Constructor used by Parcelable creator.
     *
     * @param source The Parcel to de-serialize from.
     */
    protected UserAgent(Parcel source) {
        this.productVersion = source.readString();
        this.platform = source.readString();
        this.extras = source.readString();
    }

    /**
     * Parcelable specific field - Creator
     */
    public static final Creator<UserAgent> CREATOR = new ParcelableCreator();

    /**
     * Describe the kinds of special objects contained in this Parcelable's marshaled representation.
     *
     * @return int flag.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  Parcel destination.
     * @param flags Optional flags.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.productVersion);
        dest.writeString(this.platform);
        dest.writeString(this.extras);
    }

    //
    // PRIVATE IMPLEMENTATION
    //

    /**
     * Application name and version, e.g. HbbTV/1.2.1
     */
    private final String productVersion;

    /**
     * Platform, e.g. Linux
     */
    private final String platform;

    /**
     * Extra parameters into details section (e.g. language).
     */
    private final String extras;
}
