/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.control;

import android.content.Context;
import android.media.tv.TvView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.PlayerConfig;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.NumberUtils;
import tv.threess.threeready.api.generic.model.BookmarkType;
import tv.threess.threeready.api.generic.model.ContinueWatchingConfig;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.model.IReplayOttStreamingSession;
import tv.threess.threeready.api.playback.model.IReplayOttStreamingSession.IBlacklistSlot;
import tv.threess.threeready.api.playback.model.ReplayStreamingSessionContainer;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.player.command.AppendReplayCommand;
import tv.threess.threeready.player.command.PrependReplayCommand;
import tv.threess.threeready.player.command.UpdateReplayPlaylistCommand;
import tv.threess.threeready.player.command.start.ReplayStartCommand;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.control.base.SessionBasedControlProximus;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.OttTuneParams;
import tv.threess.threeready.player.result.ExactFailure;

/**
 * Playback controller for replay ott streams.
 *
 * @author Eugen Guzyk
 * @since 2018.07.30
 */
public class ReplayControlProximus extends SessionBasedControlProximus<IReplayOttStreamingSession,
        ReplayStreamingSessionContainer<IReplayOttStreamingSession>> {

    private final PlayerConfig mPlayerConfig = Components.get(PlayerConfig.class);
    private final long mLiveReplayDelay = mPlayerConfig.getStartOverAppearingDelay(TimeUnit.MILLISECONDS);
    private final long mPlaylistUpdateRetryMargin = mPlayerConfig.getPlaylistUpdateRetryMargin(TimeUnit.MILLISECONDS);

    private final PlaylistLock mAppendPlaylistLock = new PlaylistLock();
    private final PlaylistLock mPrependPlayListLock = new PlaylistLock();
    private final PlaylistLock mUpdatePlayListItemLock = new PlaylistLock();

    private ReplayStartCommand mStartCommand;

    private long mAccuracyCorrection;

    private final TvRepository mTvRepository = Components.get(TvRepository.class);

    public ReplayControlProximus(Context context, PlaybackDomain domain, TvView tvView) {
        super(context, domain, tvView, new ReplayStreamingSessionContainer<>());
    }

    /**
     * Start replay playback on OTT input service.
     *
     * @param command The command that triggered the start.
     */
    public void start(ReplayStartCommand command) throws Exception {
        command.assertNotCancelled();
        mStartCommand = command;
        IBroadcast broadcast = command.getContentItem();

        List<IReplayOttStreamingSession> sessions = new ArrayList<>();

        IReplayOttStreamingSession currentSession = mSessionHandler.openReplayStreamingSession(broadcast);
        sessions.add(currentSession);
        mAccuracyCorrection = currentSession.getAccurateStartTime() - broadcast.getStart();
        Log.d(TAG, "EPG start time : " + Log.timestamp(broadcast.getStart())
                + ", accurate start time : " + Log.timestamp(currentSession.getAccurateStartTime())
                + ", accuracy correction : " + mAccuracyCorrection);

        try {
            sessions.addAll(getAppendSessions(broadcast, currentSession));
        } catch (Exception e) {
            Log.e(TAG, "Failed to get append session.", e);
        }

        Long relativePosition = command.getPosition();

        IBookmark bookmark = mBookmarkRepository.getBookmark(broadcast, BookmarkType.Replay);

        command.assertNotCancelled();

        // no start position (no start-over), fallback to bookmark position
        if (relativePosition == null && bookmark.getPosition() > 0) {
            double maxWatchedPercentage = Components.get(ContinueWatchingConfig.class).getReplayWatchedPercent();
            boolean isNearEnd = bookmark.isNearEnd(maxWatchedPercentage);
            relativePosition = isNearEnd ? 0 : bookmark.getPosition();
            mIsResumedBookmark = true;
        } else {
            mIsResumedBookmark = false;
        }

        command.assertNotCancelled();
        start(command.getId(), broadcast.getStart(), broadcast.getEnd(), null,
                relativePosition, sessions.toArray(new IReplayOttStreamingSession[0]));

        mPrependPlayListLock.reset();
        mAppendPlaylistLock.reset();
        mUpdatePlayListItemLock.reset();
    }


    @Override
    public void start(long cid, long contentStartTime, long contentEndTime, OttTuneParams tuneParams) throws Exception {
        // Check if the init start position is blacklisted.
        long playbackInitTime = mStartCommand.getCommandInitiatedTimestamp();
        long accurateInitPosition = convertContentToAccuratePosition(mCurrentPosition);
        Log.d(TAG, "Accurate init position : " + Log.timestamp(accurateInitPosition));

        IBlacklistSlot blacklistSlot =
                // + 1 To avoid matching the end of the previous slot.
                mStreamingSessionContainer.getActiveBlacklistSlot(playbackInitTime, accurateInitPosition + 1);

        if (blacklistSlot != null) {
            Log.d(TAG, "Blacklisted initial position. Init Position : " + Log.timestamp(accurateInitPosition)
                    + ", slot start : " + Log.timestamp(blacklistSlot.getStart())
                    + ", slot end : " + Log.timestamp(blacklistSlot.getEnd()));
            offerResult(new ExactFailure(cid, PlaybackError.Type.BLACKLISTED));
            return;
        }

        super.start(cid, contentStartTime, contentEndTime, tuneParams);
    }

    /**
     * Prepend the playlist with the previous replay for continuous playback.
     */
    public void prepend(PrependReplayCommand command) {
        try {
            IBroadcast broadcast = command.getBroadcast();
            IReplayOttStreamingSession firstSession = mStreamingSessionContainer.firstSlot();
            prepend(command.getId(), getPrependSessions(broadcast, firstSession));
            mPrependPlayListLock.onRequestSuccessful();
        } catch (Exception e) {
            Log.e(TAG, "Failed to get prepend session.", e);
            offerResult(new ExactFailure(command.getId(), PlaybackError.Type.SESSION_OPEN_FAILED));
            mPrependPlayListLock.onRequestFailed();
        }
    }

    /**
     * Append the playlist with the next replay for continuous playback.
     */
    public void append(AppendReplayCommand command) {
        try {
            IBroadcast broadcast = command.getBroadcast();
            IReplayOttStreamingSession currentSession = mStreamingSessionContainer.lastSlot();
            append(command.getId(), getAppendSessions(broadcast, currentSession));
            mAppendPlaylistLock.onRequestSuccessful();
        } catch (Exception e) {
            Log.e(TAG, "Failed to get append session.", e);
            offerResult(new ExactFailure(command.getId(), PlaybackError.Type.SESSION_OPEN_FAILED));
            mAppendPlaylistLock.onRequestFailed();
        }
    }

    /**
     * Update the playlist to cover all the broadcasts with playback.
     */
    public void updatePlaylist(UpdateReplayPlaylistCommand command) {
        try {
            List<IBroadcast> broadcastList = mStartCommand.getBroadcasts();
            if (broadcastList.isEmpty()) {
                throw new IllegalStateException("No broadcast to update.");
            }

            command.assertNotCancelled();

            // Try to append sessions until the last broadcast
            IBroadcast broadcast = broadcastList.get(broadcastList.size() - 1);
            IReplayOttStreamingSession lastSession = mStreamingSessionContainer.lastSlot();
            append(command.getId(), getAppendSessions(broadcast, lastSession));
            mUpdatePlayListItemLock.onRequestSuccessful();
        } catch (Exception e){
            Log.e(TAG, "Failed to get update session.", e);
            offerResult(new ExactFailure(command.getId(), PlaybackError.Type.SESSION_OPEN_FAILED));
            mUpdatePlayListItemLock.onRequestFailed();
        }
    }

    @Override
    protected void applyPosition(long contentPosition, boolean force) {
        long accuratePosition = convertContentToAccuratePosition(contentPosition);
        Log.v(TAG, "Apply position. Content position : " + Log.timestamp(contentPosition)
                + ", accurate position : " + Log.timestamp(accuratePosition)
                + ", accuracy correction : " + (accuratePosition - contentPosition));
        super.applyPosition(contentPosition, force);
    }

    @Override
    protected long convertPlaybackToContentPosition(long playerPosition) {
        // Remove the playback accuracy to match the EPG time.
        return convertAccurateToContentPosition(super.convertPlaybackToContentPosition(playerPosition));
    }

    @Override
    protected long convertContentToPlaybackPosition(long contentPosition) {
        // Apply the accurate start time correction.
        return super.convertContentToPlaybackPosition(convertContentToAccuratePosition(contentPosition));
    }

    /**
     * Convert the content position from the EPG to accurate position in the playback.
     *
     * @param contentPosition The timestamp of the content position based on EPG.
     * @return The accurate position in the playback.
     */
    public long convertContentToAccuratePosition(long contentPosition) {
        return contentPosition + mAccuracyCorrection;
    }

    /**
     * Convert the accurate playback timestamp to the EPG position timestamp.
     *
     * @param accuratePosition The accurate position coming from the playback.
     * @return The corresponding position in the EPG.
     */
    public long convertAccurateToContentPosition(long accuratePosition) {
        return accuratePosition - mAccuracyCorrection;
    }

    /**
     * Open streaming sessions previous to the current one until the entire duration of the broadcast is covered for playback.
     *
     * @param broadcast      The broadcast for which the playback session needs to be opened.
     * @param currentSession The currently opened session to extend on.
     * @return An array with all the session needed to playback the entire broadcast.
     * @throws IOException In case of backend connect errors.
     */
    private List<IReplayOttStreamingSession> getPrependSessions(IBroadcast broadcast, IReplayOttStreamingSession currentSession) throws IOException {
        List<IReplayOttStreamingSession> sessions = new ArrayList<>();
        try {
            long timeNeedsIncluded = convertContentToAccuratePosition(broadcast.getStart());
            Log.d(TAG, "Prepend sessions until: " + Log.timestamp(timeNeedsIncluded) + ": " + broadcast);

            while (!currentSession.isTimeIncluded(timeNeedsIncluded)) {
                currentSession = mSessionHandler.openPreviousReplayStreamingSession(currentSession);
                Log.d(TAG, "Prepend session opened."
                        + " Session start: " + Log.timestamp(currentSession.getStartTime())
                        + ", session end: " + Log.timestamp(currentSession.getEndTime()));
                sessions.add(0, currentSession);
            }
        } catch (BackendException e) {
            if (sessions.isEmpty()) {
                throw e;
            }
        }
        return sessions;
    }

    /**
     * Open streaming sessions next to the current one until the entire duration of the broadcast is covered for playback.
     *
     * @param broadcast      The broadcast for which the playback session needs to be opened.
     * @param currentSession The currently opened session to extend on.
     * @return An array with all the session needed to playback the entire broadcast.
     * @throws IOException In case of backend connect errors.
     */
    private List<IReplayOttStreamingSession> getAppendSessions(IBroadcast broadcast, IReplayOttStreamingSession currentSession) throws IOException {
        List<IReplayOttStreamingSession> sessions = new ArrayList<>();
        try {
            long timeNeedsIncluded = Math.min(
                    convertContentToAccuratePosition(broadcast.getEnd()),
                    System.currentTimeMillis() - mPlaylistUpdateRetryMargin);
            Log.d(TAG, "Append sessions until: " + Log.timestamp(timeNeedsIncluded) + ": " + broadcast);

            while (!currentSession.isTimeIncluded(timeNeedsIncluded)) {
                currentSession = mSessionHandler.openNextReplayStreamingSession(currentSession);
                Log.d(TAG, "Append session opened."
                        + " Session start: " + Log.timestamp(currentSession.getStartTime())
                        + ", session end: " + Log.timestamp(currentSession.getEndTime()));
                sessions.add(currentSession);
            }
        } catch (BackendException e) {

            if (sessions.isEmpty()) {
                throw e;
            }
        }
        return sessions;
    }

    @Override
    public void processPosition(long contentPosition, boolean notifyChange) {
        ReplayStartCommand startCommand = mStartCommand;
        if (startCommand == null) {
            return;
        }

        // In case the playback is not active, skip position process.
        if (state != PlaybackState.Started && state != PlaybackState.Paused
                && state != PlaybackState.TrickPlay && state != PlaybackState.TrickPlayPaused &&
                state != PlaybackState.Blanked) {
            return;
        }

        // No broadcast available at that position. Close the playback.
        IBroadcast broadcast = startCommand.getBroadcast(contentPosition, false);
        if (broadcast == null) {
            offerEvent(PlaybackEvent.EndReached);
            return;
        }

        List<IBroadcast> broadcasts = startCommand.getBroadcasts();
        int currentBroadcastIndex = broadcasts.indexOf(broadcast);

        // Check if prepend was already called and that the previous program already has replay stream
        if (mPrependPlayListLock.isEnabled() && currentBroadcastIndex == 0) {
            mPrependPlayListLock.setRequested();
            offerEvent(PlaybackEvent.PrependPlaylist);
        }

        // Check if append was already called and that the next program already has replay stream
        if (mAppendPlaylistLock.isEnabled() && currentBroadcastIndex == broadcasts.size() - 1) {
            mAppendPlaylistLock.setRequested();
            offerEvent(PlaybackEvent.AppendPlaylist);
        }

        // Trigger session update to cover all the broadcasts in the playlist with streaming sessions.
        IBroadcast lastBroadcast = broadcasts.get(broadcasts.size() - 1);
        long retryPosition = convertContentToAccuratePosition(contentPosition) + mPlaylistUpdateRetryMargin;
        if (lastBroadcast.getEnd() > System.currentTimeMillis()) {
            // Trigger retry based on current time instead of playback time.
            retryPosition = System.currentTimeMillis() - mLiveReplayDelay;
        }

        IReplayOttStreamingSession session = mStreamingSessionContainer.lastSlot(null);
        if (mUpdatePlayListItemLock.isEnabled() && session != null && retryPosition > session.getEndTime()) {
            mUpdatePlayListItemLock.setRequested();
            offerEvent(PlaybackEvent.UpdatePlayList);
        }

        // Switch replay program.
        if (notifyChange) {
            long accuratePosition = convertContentToAccuratePosition(contentPosition);
            long playbackInitTime = startCommand.getCommandInitiatedTimestamp();
            // accuratePosition + 1 will always be in the future
            IBlacklistSlot slot = mStreamingSessionContainer.getActiveBlacklistSlot(playbackInitTime, accuratePosition + 1);
            if (slot != null) {
                Log.d(TAG, "Next blacklist slot reached. Accurate position: " + Log.timestamp(accuratePosition)
                        + ", content position: " + Log.timestamp(contentPosition)
                        + ", slot start: " + Log.timestamp(slot.getStart())
                        + ", slot end: " + Log.timestamp(slot.getEnd()));

                offerEvent(PlaybackEvent.NextContentBlacklisted);
                return;
            }
            // accuratePosition - 1 will always be in the past
            slot = mStreamingSessionContainer.getActiveBlacklistSlot(playbackInitTime, accuratePosition - 1);

            if (slot != null) {
                Log.d(TAG, "Previous blacklist slot reached. Accurate position: " + Log.timestamp(accuratePosition)
                        + ", content position: " + Log.timestamp(contentPosition)
                        + ", slot start: " + Log.timestamp(slot.getStart())
                        + ", slot end: " + Log.timestamp(slot.getEnd()));
                offerEvent(PlaybackEvent.PreviousContentBlacklisted);
                return;
            }

            if (contentPosition < getContentStart() || contentPosition >= getContentEnd()) {
                saveBookmark(contentPosition, getContentStart(), getContentEnd());
                applyContentPositions(broadcast.getStart(), broadcast.getEnd());
                offerEvent(PlaybackEvent.PlaybackItemChanged);
            }
        }
    }

    @Override
    public void reset() {
        saveBookmark(getCurrentPosition(),
                getContentStart(), getContentEnd());
        super.reset();
        mAccuracyCorrection = 0;

        mAppendPlaylistLock.disable();
        mPrependPlayListLock.disable();
        mUpdatePlayListItemLock.disable();

        mStartCommand = null;
    }

    @Override
    public long getSeekAbleStart() {
        ReplayStartCommand startCommand = mStartCommand;
        if (startCommand == null) {
            return 0;
        }


        List<IBroadcast> broadcastList = startCommand.getBroadcasts();
        if (broadcastList.isEmpty()) {
            return 0;
        }

        IBroadcast firstBroadcast = broadcastList.get(0);
        long contentPosition = getCurrentPosition();
        long accuratePosition = convertContentToAccuratePosition(contentPosition);
        long playbackInitTime = startCommand.getCommandInitiatedTimestamp();

        // Inside of a blacklisting slot. No seeking possible.
        if (mStreamingSessionContainer.getActiveBlacklistSlot(playbackInitTime, accuratePosition) != null) {
            return contentPosition;
        }

        // The end of the previous valid slot.
        IBlacklistSlot prevSlot = mStreamingSessionContainer
                .getPreviousBlacklistSlot(playbackInitTime, accuratePosition);
        if (prevSlot != null) {
            return convertAccurateToContentPosition(prevSlot.getEnd());
        }

        return firstBroadcast.getStart();
    }

    @Override
    public long getSeekAbleEnd() {
        IBroadcast lastBroadcast = getLastBroadcast();
        if (lastBroadcast == null) {
            return 0;
        }

        long contentPosition = getCurrentPosition();
        long accuratePosition = convertContentToAccuratePosition(contentPosition);
        long playbackInitTime = mStartCommand.getCommandInitiatedTimestamp();

        // Inside of a blacklisting slot. No seeking possible.
        if (mStreamingSessionContainer.getActiveBlacklistSlot(playbackInitTime, accuratePosition) != null) {
            return contentPosition;
        }

        // The start of the next valid slot.
        IBlacklistSlot nextSlot = mStreamingSessionContainer
                .getNextBlacklistSlot(playbackInitTime, accuratePosition);
        if (nextSlot != null) {
            return convertAccurateToContentPosition(nextSlot.getStart());
        }

        // If the program has ended, but the 40 seconds did not pass yet, still cut off the seekbar
        return Math.min(System.currentTimeMillis() - mLiveReplayDelay, lastBroadcast.getEnd());
    }

    @Override
    public long getBufferEnd() {
        return System.currentTimeMillis() - mLiveReplayDelay;
    }

    @Override
    public String getPlaybackId() {
        if (mStartCommand == null) {
            return null;
        }
        ReplayStartCommand startCommand = mStartCommand;

        List<IBroadcast> broadcasts = startCommand.getBroadcasts();
        if (ArrayUtils.isEmpty(broadcasts)) {
            return null;
        }

        IBroadcast broadcast = broadcasts.get(0);
        return broadcast == null ? null : broadcast.getChannelId();
    }

    /**
     * @return the last broadcast from the start command.
     */
    private IBroadcast getLastBroadcast() {
        ReplayStartCommand startCommand = mStartCommand;
        if (startCommand == null) {
            return null;
        }

        List<IBroadcast> broadcastList = startCommand.getBroadcasts();
        if (broadcastList.isEmpty()) {
            return null;
        }

        return broadcastList.get(broadcastList.size() - 1);
    }

    private void saveBookmark(long position, long start, long end) {
        ReplayStartCommand startCommand = mStartCommand;
        if (startCommand == null) {
            return;
        }

        position = NumberUtils.clamp(position, start, end);

        IBlacklistSlot blacklistSlot = mStreamingSessionContainer.getNextBlacklistSlot(
                convertContentToAccuratePosition(position), startCommand.getCommandInitiatedTimestamp());
        IBroadcast currentBroadcast = startCommand.getBroadcast(position, false);
        if (currentBroadcast != null && blacklistSlot == null) {
            // Save bookmark for the replay broadcast.
            mTvRepository.addReplayBookmark(currentBroadcast, position - start, end - start)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe();
        }
    }
}
