/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player.command.base;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.concurrent.AtomicNegativeId;
import tv.threess.threeready.player.concurrent.AtomicPositiveId;
import tv.threess.threeready.player.contract.CommandValidity;
import tv.threess.threeready.player.contract.PlaybackAction;
import tv.threess.threeready.player.contract.PlaybackRelation;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.model.PlaybackStatusBuilder;
import tv.threess.threeready.player.result.EventResult;
import tv.threess.threeready.player.result.Result;

/**
 * Represents a single item from the playback queue, carrying a playback domain (e.g. LiveTV, NetworkPVR, VOD, etc) and
 * all information needed for issuing a command to the player(s). It also encapsulates the specific method calls to the
 * appropriate PlaybackControl in order to consume the command.
 *
 * @author Dan Titiu
 * @since 15.05.2014
 */
public interface PlaybackCommand {

    /**
     * Positive ID generator for user commands.
     */
    AtomicPositiveId userIdCounter = new AtomicPositiveId();

    /**
     * Negative ID generator for synthetic commands.
     */
    AtomicNegativeId syntheticIdCounter = new AtomicNegativeId();

    /**
     * Unique identifier for each command. Useful for pairing the command with its asynchronous result.
     */
    long getId();

    /**
     * @return StreamType on which to take action. Helps identifying the PlaybackControl.
     */
    PlaybackType getType();

    /**
     * @return PlaybackDomain on which to take action. Helps identifying the PlaybackControl.
     */
    PlaybackDomain getDomain();

    /**
     * @return PlaybackAction to be taken on the appropriate PlaybackControl.
     */
    PlaybackAction getAction();

    /**
     * @return PlaybackState targeted by the PlaybackCommand. This state is reached by the PlaybackControl once the
     * command is successfully applied.
     */
    PlaybackState getTargetState();

    /**
     * Returns the timeout in milliseconds for dispatching this command.
     *
     * @return the timeout in milliseconds for dispatching this command.
     */
    long getTimeout();

    /**
     * @return True if this command is generated implicitly by PlaybackManagement, without an explicit UI call. One
     * valid example would be self-generated stop command between two start commands on different domains.
     */
    boolean isSynthetic();

    /**
     * Check the relation between this command and the one specified.
     *
     * @param other    PlaybackCommand to relate to.
     * @return The relation between this command and the one specified.
     */
    PlaybackRelation relate(PlaybackCommand other, boolean executing);

    /**
     * Accumulate specific playback command value(s) from the specified command. By default it does nothing.
     * Please override this method for specific behavior.
     *
     * @param other    PlaybackCommand from which to accumulate value(s).
     */
    void accumulate(PlaybackCommand other);

    /**
     * Validate this command against given (usually currently active) playback type and its state. Validation is
     * performed as the first step of command dispatching and its different from {@link #verify(PlaybackEntireState)} in
     * the fact that it can suggest an injected transition. Thus validation is performed before command injection, while
     * verification is performed after.
     *
     * @param ds       The current {@code PlaybackDomainState} to validate against.
     * @return The validation outcome.
     */
    CommandValidity validate(PlaybackEntireState ds);

    /**
     * Verify this command against given (usually currently active) playback type and its state. Verification is
     * performed after command injection and only if injection is performed. The main role of the verification is to
     * check that command injection was performed successfully.
     *
     * @param ds       The current {@code PlaybackDomainState} to verify against.
     * @return True if verification succeeds.
     */
    boolean verify(PlaybackEntireState ds);

    /**
     * Consumes the command by calling specific methods on the appropriate PlaybackControl.
     */
    void execute() throws Exception;

    /**
     * Cancel the command, which notifies the dispatcher that the command result will be ignored and failure results will be added to it.
     */
    void cancel();

    /**
     * Check whether the command has been cancelled or not.
     *
     * @return  Flag representing cancelled state.
     */
    boolean isCancelled();

    /**
     * Check if the command has been cancelled or not, and throws an {@link PlaybackError} when it is.
     */
    void assertNotCancelled() throws PlaybackError;

    /**
     * Checks if the given {@link Result} completes the execution of the command. The result must match the command.
     * Several results may match the same command until it is fulfilled.
     *
     * @param result The result to be checked if matches and fulfills the command.
     * @return true when the command is completed and needs no other results.
     */
    boolean isFulfilledBy(Result result);

    /**
     * Marks the result of the successful command execution by applying the playback state onto the control. Other
     * playback parameters might also get saved for later retrieval.
     */
    void commit();

    /**
     * Marks the result of the failed command execution by rolling back any state changes that may have been done on the
     * control before or during execution.
     * 
     * @param error The error that caused the command to be rolled back.
     */
    void rollback(PlaybackError error, @Nullable Long nextCmdId) throws Exception;

    /**
     * Populate the {@code PlaybackDetailsBuilder} with detailed information specific to this command
     * (e.g. StreamType, shape, etc).
     * 
     * @param details The details DTO to be decorated.
     */
    void decorate(PlaybackDetailsBuilder details);

    /**
     * Populate the {@code PlaybackStatusBuilder} with information specific to this command (e.g. StreamType).
     * 
     * @param status The status DTO to be decorated.
     */
    void decorate(PlaybackStatusBuilder status);

    /**
     * Verify this command against given eventResult as parameter. Returns true if the EventResult has to be discarded.
     *
     * @param eventResult The event to be checked if it needs to be discarded.
     * @return true when the event should be discarded.
     */
    boolean discards(EventResult eventResult);

    /**
     * Command offered to dispatcher
     */
    void offer();

    /**
     * @return The time from when the command was created
     */
    long getTriggerTime();

    /**
     * @return time from offer cmd until cmd is dispatched
     */
    long getQueueTime();

    /**
     * @return time needed to execute a transition cmd before executing this cmd;
     */
    long getTransitionCmdTime();

    /**
     * @return time starting from offer until cmd is resolved
     */
    long getProcessingTime();

    /**
     * @return time spent from offer command until cmd is resolved
     */
    long getCmdTotalTime();

    /**
     * @return timestamp on offer command
     */
    long getCommandInitiatedTimestamp();
}
