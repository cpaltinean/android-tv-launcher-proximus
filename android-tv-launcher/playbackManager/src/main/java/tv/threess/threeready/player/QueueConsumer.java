/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Facade exposing a Queue to clients solely as an element consumer.
 *
 * @author Dan Titiu
 * @since 25.04.2014
 */
public interface QueueConsumer<E> {

    /**
     * Offer an element to the queue asynchronously.
     *
     * @param element    The element to be queued.
     * @return <tt>true</tt> if the element was added to this queue, else
     *         <tt>false</tt>
     */
    boolean offer(E element);

    /**
     * Offer an element to the queue synchronously, waiting up to the specified wait time if necessary and return the
     * result.
     *
     * @param element The element to be queued.
     * @param timeout How long to wait before giving up, in units of <tt>unit</tt>.
     * @param unit A <tt>TimeUnit</tt> determining how to interpret the <tt>timeout</tt> parameter.
     * @return <tt>true</tt> if successful, or <tt>false</tt> if the specified waiting time elapses before being able to .
     * @throws InterruptedException if interrupted while waiting
     * @throws TimeoutException
     * @throws ExecutionException
     */
    boolean offer(E element, long timeout, TimeUnit unit)
            throws InterruptedException, TimeoutException, ExecutionException;
}
