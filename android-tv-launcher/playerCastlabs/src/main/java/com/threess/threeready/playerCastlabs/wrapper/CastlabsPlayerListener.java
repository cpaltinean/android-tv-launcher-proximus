package com.threess.threeready.playerCastlabs.wrapper;

import com.castlabs.android.player.DisplayInfo;
import com.castlabs.android.player.PlayerController;
import com.castlabs.android.player.PlayerListener;
import com.castlabs.android.player.exceptions.CastlabsPlayerException;

import androidx.annotation.NonNull;

/**
 * Wrapper around the castlabs player listener with default empty implementation.
 *
 * @author Barabas Attila
 * @since 2020.08.09
 */
public interface CastlabsPlayerListener extends PlayerListener {

    @Override
    default void onFatalErrorOccurred(@NonNull CastlabsPlayerException e) {

    }

    @Override
    default void onError(@NonNull CastlabsPlayerException e) {

    }

    @Override
    default void onStateChanged(@NonNull PlayerController.State state) {

    }

    @Override
    default void onSeekTo(long l) {

    }

    @Override
    default void onVideoSizeChanged(int i, int i1, float v) {

    }

    @Override
    default void onSeekRangeChanged(long l, long l1) {

    }

    @Override
    default void onPlaybackPositionChanged(long l) {

    }

    @Override
    default void onDisplayChanged(DisplayInfo info, boolean b) {

    }

    @Override
    default void onDurationChanged(long l) {

    }

    @Override
    default void onSpeedChanged(float v) {

    }

    @Override
    default void onPlayerModelChanged() {

    }

    @Override
    default void onFullyBuffered() {

    }
}
