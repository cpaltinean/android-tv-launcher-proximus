package com.threess.threeready.playerCastlabs.configuration;

import android.content.Context;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.text.TextUtils;

import com.castlabs.android.SdkConsts;
import com.castlabs.android.player.AbrConfiguration;
import com.castlabs.android.player.BufferConfiguration;
import com.castlabs.android.player.TrickplayConfiguration;

import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.config.model.PlaybackSettings;
import tv.threess.threeready.api.log.Log;

/**
 * Configuration builder for trickplay playback.
 * This configuration sets up the trickplay mode selection and the proper speed for the playback.
 *
 * @author Andor Lukacs
 * @since 3/6/19
 */
public class TrickplayConfig extends StreamConfig {
    private static final String TAG = Log.tag(TrickplayConfig.class);

    private static final int TRICKPLAY_BUFFER_SIZE = 1048576;

    public static class Builder extends StreamConfig.Builder<Builder> {

        public Builder(Context context, String url) {
            super(context, url);
            PlaybackSettings settings = Components.get(AppConfig.class).getPlaybackSettings();
            int lowMediaTime = Settings.lowMediaTimeTrickplay.get(settings.getLowMediaTimeTrickPlay());
            int highMediaTime = Settings.highMediaTimeTrickplay.get(settings.getHighMediaTimeTrickPlay());

            AbrConfiguration abrConfiguration = new AbrConfiguration.Builder()
                    .initialTrackSelection(SdkConsts.VIDEO_QUALITY_LOWEST, true)
                    .get();

            BufferConfiguration bufferConfiguration = new BufferConfiguration.Builder()
                    .bufferSizeBytes(TRICKPLAY_BUFFER_SIZE) // 1 MB
                    .drainWhileCharging(true) // save bandwidth
                    .lowMediaTime(lowMediaTime, TimeUnit.SECONDS)
                    .highMediaTime(highMediaTime, TimeUnit.SECONDS)
                    .get();

            mPlayerConfiguration.bufferConfiguration(bufferConfiguration);
            mPlayerConfiguration.abrConfiguration(abrConfiguration);

            mPlayerConfiguration.enableTrickplayMode(true);
        }

        public Builder setTrickplaySpeed(float speed) {
            TrickplayConfiguration trickplayConfiguration = new TrickplayConfiguration.Builder()
                    .speedupMode(TrickplayConfiguration.SpeedupMode.SEEK)
                    .speed(speed)
                    .keepAudioEnabled(false)
                    .preferTrickPlayTracks(true)
                    .get();

            mPlayerConfiguration.trickplayConfiguration(trickplayConfiguration);

            return this;
        }


        public com.castlabs.android.player.PlayerConfig build() {
            mPlayerConfiguration.audioTrackIndex(SdkConsts.DISABLED_TRACK);
            mPlayerConfiguration.audioTrackGroupIndex(SdkConsts.DISABLED_TRACK);

            if(mDrmConfiguration != null && !TextUtils.isEmpty(mDrmConfiguration.url)) {
                Log.d(TAG, "DrmCongifuration is not null, selecting secure decoder. " + mDrmConfiguration.url);
                mPlayerConfiguration.videoCodec("OMX.bcm.vdec.redux.avc.secure");
            } else {
                Log.d(TAG, "DrmCongifuration is null, selecting clear decoder.");
                mPlayerConfiguration.videoCodec("OMX.bcm.vdec.redux.avc");
            }

            printInfo();

            return super.build();
        }

        private void printInfo() {
            MediaCodecList list = new MediaCodecList(MediaCodecList.ALL_CODECS);
            MediaCodecInfo[] codecInfos = list.getCodecInfos();
            for (MediaCodecInfo info : codecInfos) {
                Log.d("MediaCodecs", info.getName());
            }
        }
    }
}
