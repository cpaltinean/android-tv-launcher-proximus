package com.threess.threeready.playerCastlabs.wrapper;

import android.content.Context;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.view.Surface;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.castlabs.android.player.AbstractStreamingEventListener;
import com.castlabs.android.player.StreamingEventListener;
import com.castlabs.android.player.TrackSelectionListener;
import com.castlabs.android.player.models.AudioTrack;
import com.castlabs.android.player.models.SubtitleTrack;
import com.castlabs.android.player.models.VideoTrack;
import com.castlabs.android.player.models.VideoTrackQuality;
import com.google.android.exoplayer2.C;

import tv.threess.threeready.api.generic.helper.BundleBuilder;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.GenericKeyEvent;
import tv.threess.threeready.api.log.model.PlaybackEventDetails;
import tv.threess.threeready.api.playback.listener.PlayerWrapperListener;
import tv.threess.threeready.player.model.OttPlayListItem;
import tv.threess.threeready.player.model.OttTuneParams;
import tv.threess.threeready.player.wrapper.ITifPlayerWrapper;

/**
 * Tv Input Framework specific wrapper over the castLabs player.
 *
 * @author Andor Lukacs
 * @since 6/4/19
 */
public class CastlabsTifPlayerWrapper extends CastlabsPlayerWrapper implements ITifPlayerWrapper<Surface> {

    private StreamingEventListener mStreamingEventListener = null;
    private TrackSelectionListener mTrackSelectionListener = null;
    private ResultReceiver mResultReceiver;

    public CastlabsTifPlayerWrapper(Context context) {
        super(context);

        createOverlayView();
    }

    @Override
    public void play(OttTuneParams tuneParams, ResultReceiver resultReceiver) {
        mResultReceiver = resultReceiver;
        super.play(tuneParams);
    }

    @Override
    public void release() {
        super.release();
        mResultReceiver = null;
        if (mStreamingEventListener != null) {
            getPlaylistControl().removeStreamingEventListener(mStreamingEventListener);
            mStreamingEventListener = null;
        }
        if (mTrackSelectionListener != null) {
            getPlaylistControl().removeTrackSelectionListener(mTrackSelectionListener);
            mTrackSelectionListener = null;
        }
    }

    @Override
    public void setPlayerWrapperListener(PlayerWrapperListener playerWrapperListener) {
        super.setPlayerWrapperListener(playerWrapperListener);
        addStreamingEventsListener();
    }

    @Override
    protected void handleDurationChanged(long durationMs) {
        if (mResultReceiver != null) {
            mResultReceiver.send(NOTIFY_DURATION_CHANGE, new BundleBuilder()
                    .put(EXTRA_PLAYBACK_DURATION, durationMs)
                    .build());
        }
    }

    @Override
    protected void handleUnskippableAdChanged(long[] starts, long[] ends) {
        if (mResultReceiver != null) {
            mResultReceiver.send(NOTIFY_UNSKIPPABLE_TIMELINE_CHANGED, new BundleBuilder()
                    .put(EXTRA_UNSKIPPABLE_STARTS, starts)
                    .put(EXTRA_UNSKIPPABLE_ENDS, ends)
                    .build());
        }
    }

    protected void handlePlaylistItemChanged(OttPlayListItem playListItem) {
        if (mResultReceiver != null) {
            mResultReceiver.send(NOTIFY_PLAYLIST_ITEM_CHANGED, new BundleBuilder()
                    .put(EXTRA_PLAYLIST_ITEM, playListItem)
                    .build());
        }
    }

    @Override
    protected void handlePlayerStateChange(long timestamp, String playbackState) {
        try {
            if (mResultReceiver != null && playbackState != null) {
                mResultReceiver.send(NOTIFY_PLAYER_STATE_CHANGE, new BundleBuilder()
                        .put(PlaybackEventDetails.PLAYER_STATE.name(), playbackState)
                        .put(PlaybackEventDetails.STATE_CHANGED_TIMESTAMP.name(), timestamp)
                        .build());
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to notify event", e);
        }
    }

    @Override
    protected void handlePlaybackException(String exceptionType, String exceptionMessage) {
        try {
            if (mResultReceiver != null) {
                mResultReceiver.send(NOTIFY_PLAYBACK_EXCEPTION_RECEIVED, new BundleBuilder()
                .put(GenericKeyEvent.KEY.getKey(), exceptionType)
                .put(GenericKeyEvent.MESSAGE.getKey(), exceptionMessage)
                .build());
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to notify event", e);
        }
    }

    @Override
    protected void handlePlaybackItemAdded() {
        try {
            if (mResultReceiver != null) {
                mResultReceiver.send(NOTIFY_PLAYBACK_ITEM_ADDED, new Bundle());
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to notify event", e);
        }
    }

    private void addStreamingEventsListener() {
        getPlaylistControl().addStreamingEventListener(mStreamingEventListener = new AbstractStreamingEventListener() {
            @Override
            public void onLoadCompleted(@NonNull com.google.android.exoplayer2.upstream.DataSpec dataSpec,
                                        int dataType, int trackId, int trigger,
                                        @Nullable com.google.android.exoplayer2.Format format,
                                        long mediaStartTimeMs, long mediaEndTimeMs, long elapsedRealtimeMs,
                                        long bytesLoaded, long loadDurationMs, int currentAttempt, int maxAttempts) {

                if (mResultReceiver == null) {
                    return;
                }

                final int notification;
                switch (dataType) {
                    default:
                        // skip notification
                        return;

                    case C.DATA_TYPE_MEDIA:
                        notification = NOTIFY_SEGMENT_DOWNLOADED;
                        break;
                }
                try {
                    mResultReceiver.send(notification, new BundleBuilder()
                            .put(PlaybackEventDetails.SEGMENT_SIZE.name(), bytesLoaded)
                            .put(PlaybackEventDetails.SEGMENT_TIME.name(), loadDurationMs)
                            .put(PlaybackEventDetails.PLAYBACK_URL.name(), dataSpec.uri.toString())
                            .build());
                } catch (Exception e) {
                    Log.e(TAG, "Failed to notify event", e);
                }
            }
        });
        getPlaylistControl().addTrackSelectionListener(mTrackSelectionListener = new TrackSelectionListener() {
            @Override
            public void onVideoQualitySelectionChanged(@NonNull VideoTrackQuality videoTrackQuality, int reason, String switchDescription, long bufferedDurationUs, long bitrateEstimate) {
                try {
                    Log.d(TAG, "onVideoQualitySelectionChanged(videoTrackQuality: " + videoTrackQuality + ", reason: " + reason + ", switchDescription: " + switchDescription + ", bufferedDurationUs: " + bufferedDurationUs + ", bitrateEstimate: " + bitrateEstimate + ")");
                    mResultReceiver.send(NOTIFY_BITRATE_CHANGE, new BundleBuilder()
                            .put(PlaybackEventDetails.BITRATE.name(), videoTrackQuality.getBitrate())
                            .build());
                } catch (Exception e) {
                    Log.e(TAG, "Failed to notify event", e);
                }
            }

            @Override
            public void onVideoTrackChanged(VideoTrack videoTrack) {
                Log.d(TAG, "onVideoTrackChanged(videoTrack: " + videoTrack + ")");
            }

            @Override
            public void onAudioTrackChanged(AudioTrack audioTrack) {
                Log.d(TAG, "onAudioTrackChanged(audioTrack: " + audioTrack + ")");
            }

            @Override
            public void onSubtitleTrackChanged(SubtitleTrack subtitleTrack) {
                Log.d(TAG, "onSubtitleTrackChanged(subtitleTrack: " + subtitleTrack + ")");
            }
        });
    }
}
