package com.threess.threeready.playerCastlabs.configuration;

import android.content.Context;

import com.castlabs.android.SdkConsts;

/**
 * Configuration builder for live radio playback.
 *
 * @author Andor Lukacs
 * @since 3/20/19
 */
public class RadioConfig extends StreamConfig {

    public static class Builder extends StreamConfig.Builder<Builder> {

        public Builder(Context context, String url) {
            super(context, url);
            mPlayerConfiguration.contentType(SdkConsts.CONTENT_TYPE_MP4);
        }
    }
}
