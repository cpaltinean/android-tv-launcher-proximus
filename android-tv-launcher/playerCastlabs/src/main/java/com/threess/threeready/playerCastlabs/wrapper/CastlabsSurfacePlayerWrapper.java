package com.threess.threeready.playerCastlabs.wrapper;

import android.content.Context;
import android.view.Surface;

import tv.threess.threeready.player.wrapper.ISurfacePlayerWrapper;

/**
 * Generic player wrapper for regularly attached surfaces.
 *
 * @author Andor Lukacs
 * @since 6/4/19
 */
public class CastlabsSurfacePlayerWrapper extends CastlabsPlayerWrapper implements ISurfacePlayerWrapper<Surface> {

    public CastlabsSurfacePlayerWrapper(Context context) {
        super(context);
    }

    @Override
    protected void handleDurationChanged(long durationMs) {
        // Send duration changed events to the playback control
    }

    @Override
    protected void handleUnskippableAdChanged(long[] starts, long[] ends) {
    }

    @Override
    protected void handlePlayerStateChange(long timestamp, String playbackState) {
        // Send player state change events to the playback control
    }

    @Override
    protected void handlePlaybackException(String exceptionType, String exceptionMessage) {
        // Handle playback exception.
    }

    @Override
    protected void handlePlaybackItemAdded() {
        // Send handle playback added events to the playback control
    }
}
