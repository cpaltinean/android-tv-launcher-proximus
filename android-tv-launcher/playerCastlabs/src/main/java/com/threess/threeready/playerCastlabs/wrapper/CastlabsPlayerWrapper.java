/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package com.threess.threeready.playerCastlabs.wrapper;

import android.content.Context;
import android.media.PlaybackParams;
import android.media.tv.TvInputManager;
import android.media.tv.TvTrackInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.castlabs.android.PlayerSDK;
import com.castlabs.android.SdkConsts;
import com.castlabs.android.player.DashDescriptor;
import com.castlabs.android.player.DisplayInfo;
import com.castlabs.android.player.InitialPositionProvider;
import com.castlabs.android.player.PlayerConfig;
import com.castlabs.android.player.PlayerListener;
import com.castlabs.android.player.SingleControllerPlaylist;
import com.castlabs.android.player.TimelineChangedListener;
import com.castlabs.android.player.exceptions.CastlabsPlayerException;
import com.castlabs.android.player.models.AudioTrack;
import com.castlabs.android.player.models.SubtitleTrack;
import com.castlabs.android.player.models.Timeline;
import com.castlabs.android.player.models.VideoTrack;
import com.castlabs.android.player.models.VideoTrackQuality;
import com.castlabs.sdk.debug.DebugPlugin;
import com.castlabs.sdk.okhttp.OkHttpPlugin;
import com.castlabs.sdk.subtitles.SubtitlesPlugin;
import com.castlabs.sdk.subtitles.SubtitlesView;
import com.google.android.exoplayer2.util.LongArray;
import com.threess.threeready.playerCastlabs.R;
import com.threess.threeready.playerCastlabs.configuration.RadioConfig;
import com.threess.threeready.playerCastlabs.configuration.StreamConfig;
import com.threess.threeready.playerCastlabs.configuration.TrickplayConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.PlaybackEventValues;
import tv.threess.threeready.api.playback.listener.PlayerWrapperListener;
import tv.threess.threeready.data.utils.OkHttpBuilder.OkHttpBuilder;
import tv.threess.threeready.player.PlaybackManager;
import tv.threess.threeready.player.model.MediaTrackInfo;
import tv.threess.threeready.player.model.OttPlayListItem;
import tv.threess.threeready.player.model.OttTuneParams;
import tv.threess.threeready.player.wrapper.IPlayerWrapper;
import tv.threess.threeready.player.wrapper.ITifPlayerWrapper;

/**
 * Wrapper around basic playback controls in Castlabs player.
 *
 * @author Daniel Gliga
 * @since 2017.11.30
 */
public abstract class CastlabsPlayerWrapper implements IPlayerWrapper<Surface> {
    protected final String TAG = Log.tag(getClass());

    @Nullable
    private SingleControllerPlaylist mSingleControllerPlaylist;

    private final List<OttPlayListItem> mPlayListItems = new ArrayList<>();

    private static final String ASSET_ID_SCHEME_ID_URI = "urn:smpte:ul:060E2B34.01040101.01200900.00000000";
    private static final String UNSKIPABLE_SCHEME_ID_URI = "urn:mediakind:dash:advertising:skip-properties";

    private Surface mSurface;
    private final Context mContext;
    private SubtitlesView mSubtitleView;
    private View mDebugView;
    private FrameLayout mOverlayView;

    private boolean mPlaybackInitiated;

    protected PlayerListener mPlayerListener = null;
    protected PlayerListener mOpenPlayerListener = null;

    private TimelineChangedListener mTimelineListener = null;

    private PlayerWrapperListener mPlayerWrapperListener;

    private Bundle mStoredPlaybackConfiguration;

    private long mJumpValue = 0;

    CastlabsPlayerWrapper(Context context) {
        Log.d(TAG, "CastlabsPlayerWrapper() called with: context = [" + context + "]");
        mContext = context;
    }

    protected synchronized SingleControllerPlaylist getPlaylistControl() {
        if (mSingleControllerPlaylist == null) {
            mSingleControllerPlaylist = initializePlaylistControl();
        }
        return mSingleControllerPlaylist;
    }

    private SingleControllerPlaylist initializePlaylistControl() {
        PlayerSDK.register(new SubtitlesPlugin());
        if (Log.canSendToLogcat(Log.Level.Debug)) {
            PlayerSDK.register(new DebugPlugin.Builder()
                    .logAll(true)
                    .logTransferInfo(false)
                    .debugOverlay(true)
                    .get());
        }
        PlayerSDK.register(new OkHttpPlugin(OkHttpBuilder.createClient(OkHttpBuilder.Dscp.AF22)));
        PlayerSDK.init(mContext);
        PlayerSDK.FORCE_HDCP_EXCEPTION = true;

        Settings.playerVersion.asyncEdit().put(PlayerSDK.getVersion());

        SingleControllerPlaylist singleControllerPlaylist = new SingleControllerPlaylist(mContext, mPlaylistListener);
        singleControllerPlaylist.setHdPlaybackEnabled(SdkConsts.ALLOW_HD_CLEAR_CONTENT
                | SdkConsts.ALLOW_HD_DRM_SOFTWARE
                | SdkConsts.ALLOW_HD_DRM_ROOT_OF_TRUST
                | SdkConsts.ALLOW_HD_DRM_SECURE_MEDIA_PATH);

        singleControllerPlaylist.setSSLSocketFactory(OkHttpBuilder.getSSLSocketFactory(OkHttpBuilder.Dscp.AF22));

        return singleControllerPlaylist;
    }

    @Override
    public void setPlayerWrapperListener(PlayerWrapperListener playerWrapperListener) {
        mPlayerWrapperListener = playerWrapperListener;

        getPlaylistControl().addTimelineChangedListener(mTimelineListener = timeline -> {
            LongArray starts = new LongArray();
            LongArray ends = new LongArray();
            for (Timeline.Period period : timeline.getPeriods()) {
                for (DashDescriptor descriptor : period.getDescriptors()) {
                    if (descriptor.getValue() != null && UNSKIPABLE_SCHEME_ID_URI.equals(descriptor.getSchemeIdUri())) {
                        try {
                            long start = period.getPositionInWindowMs();
                            long len = (long) (1000 * Double.parseDouble(descriptor.getValue()));
                            starts.add(start);
                            ends.add(start + len);
                            Log.v(TAG, "Period descriptor: " + descriptor.getSchemeIdUri() + " -> [" + Log.formatDuration(start) + ", " + Log.formatDuration(start + len) + ")");
                        } catch (Exception e) {
                            Log.e(TAG, "Failed to parse period descriptor", e);
                        }
                    }
                }
            }
            handleUnskippableAdChanged(starts.toArray(), ends.toArray());
        });

        getPlaylistControl().addPlayerListener(mPlayerListener = new PlayerListener() {
            @Override
            public void onFatalErrorOccurred(@NonNull CastlabsPlayerException e) {
                Log.d(TAG, "onFatalErrorOccurred()", e);
                if (e.getType() == CastlabsPlayerException.TYPE_BEHIND_LIVE_WINDOW) {
                    Log.d(TAG, "The exception is Behind Live Window, we need to save the configuration so we can restart.");
                    saveState();
                }
            }

            @Override
            public void onError(@NonNull CastlabsPlayerException e) {
                Log.e(TAG, "Error during playback.", e);

                if (e.getType() == CastlabsPlayerException.TYPE_BEHIND_LIVE_WINDOW) {
                    mPlayerWrapperListener.notifyVideoUnavailable(TvInputManager.VIDEO_UNAVAILABLE_REASON_BUFFERING);
                    handlePlaybackException(e.getCause() != null ? e.getCause().getClass().getName() : e.getClass().getName(), e.getCauseMessage());
                    try {
                        retry();
                        return;
                    } catch (Exception ex) {
                        Log.e(TAG, "Could not retry playback!", ex);
                    }
                }
                if (e.getSeverity() >= CastlabsPlayerException.SEVERITY_ERROR) {
                    mPlayerWrapperListener.notifyVideoUnavailable(ITifPlayerWrapper.VIDEO_UNAVAILABLE_ERROR_FLAG | e.getType());
                    handlePlaybackException(e.getCause() != null ? e.getCause().getClass().getName() : e.getClass().getName(), e.getCauseMessage());
                }
            }

            @Override
            public void onStateChanged(@NonNull SingleControllerPlaylist.State state) {
                Log.v(TAG, "onStateChanged: " + state);
                long now = System.currentTimeMillis();

                String playbackState = null;
                switch (state) {
                    case Preparing:
                        getPlaylistControl().setSurface(mSurface);
                        playbackState = PlaybackEventValues.STATE_PREPARING;
                        mPlayerWrapperListener.notifyVideoUnavailable(TvInputManager.VIDEO_UNAVAILABLE_REASON_TUNING);
                        break;

                    case Buffering:
                        playbackState = PlaybackEventValues.STATE_BUFFERING;
                        mPlayerWrapperListener.notifyVideoUnavailable(TvInputManager.VIDEO_UNAVAILABLE_REASON_BUFFERING);
                        break;

                    case Playing:
                        mPlaybackInitiated = true;
                        playbackState = PlaybackEventValues.STATE_PLAYING;
                        mPlayerWrapperListener.notifyVideoAvailable();
                        break;

                    case Pausing:
                        playbackState = PlaybackEventValues.STATE_PAUSING;
                        if (getPlaylistControl().isTrickplayMode() && !mPlaybackInitiated) {
                            mPlayerWrapperListener.notifyVideoAvailable();

                            mPlaybackInitiated = true;
                        }
                        break;

                    case Idle:
                        playbackState = PlaybackEventValues.STATE_IDLE;
                        break;

                    case Finished:
                        playbackState = PlaybackEventValues.STATE_FINISHED;
                        mPlayerWrapperListener.notifyVideoUnavailable(VIDEO_UNAVAILABLE_REASON_ENDED);
                        mPlaybackInitiated = false;
                        break;
                }

                handlePlayerStateChange(now, playbackState);
            }

            @Override
            public void onSeekTo(long newPosition) {
                Log.v(TAG, "onSeekTo() called with: newPosition = [" + newPosition + "]");
            }

            @Override
            public void onSeekCompleted() {
                Log.v(TAG, "onSeekCompleted() called");
            }

            @Override
            public void onVideoSizeChanged(int width, int height, float ration) {
                Log.v(TAG, "onVideoSizeChanged() called with: width = [" + width + "], height = [" + height + "], ration = [" + ration + "]");
            }

            @Override
            public void onSeekRangeChanged(long l, long l1) {
                Log.v(TAG, "onSeekRangeChanged() called with: l = [" + l + "], l1 = [" + l1 + "]");
            }

            @Override
            public void onPlaybackPositionChanged(long l) {
                Log.v(TAG, "onPlaybackPositionChanged() called with: l = [" + l + "], playlistIndex : "
                        + getPlaylistControl().getCurrentItemIndex() + ", playlist size : " + getPlaylistControl().getSize());
            }

            @Override
            public void onDisplayChanged(DisplayInfo displayInfo, boolean canPlay) {
                Log.v(TAG, "onDisplayChanged() called with: displayInfo = [" + displayInfo + "], canPlay = [" + canPlay + "]");
            }

            @Override
            public void onDurationChanged(long durationUs) {
                Log.v(TAG, "onDurationChanged() called with: durationUs = [" + durationUs + "]");
                long durationMs = TimeUnit.MICROSECONDS.toMillis(durationUs);
                handleDurationChanged(durationMs);
            }

            @Override
            public void onSpeedChanged(float v) {
                Log.v(TAG, "onSpeedChanged() called with: v = [" + v + "]");
            }

            @Override
            public void onPlayerModelChanged() {
                Log.v(TAG, "onPlayerModelChanged() called");
                mPlayerWrapperListener.notifyTracksChanged(getAllTracks());

                String audioId = getSelectedTrack(TvTrackInfo.TYPE_AUDIO);
                String videoId = getSelectedTrack(TvTrackInfo.TYPE_VIDEO);
                String textId = getSelectedTrack(TvTrackInfo.TYPE_SUBTITLE);

                mPlayerWrapperListener.notifyTrackSelected(TvTrackInfo.TYPE_AUDIO, audioId);
                mPlayerWrapperListener.notifyTrackSelected(TvTrackInfo.TYPE_VIDEO, videoId);
                mPlayerWrapperListener.notifyTrackSelected(TvTrackInfo.TYPE_SUBTITLE, textId);
            }

            @Override
            public void onTrackKeyStatusChanged() {

            }

            @Override
            public void onFullyBuffered() {
                Log.v(TAG, "onFullyBuffered() called");
            }
        });
    }

    @Override
    public void release() {
        Log.d(TAG, "release() called");
        getPlaylistControl().release();
        getPlaylistControl().setSurface(null);
        if (mPlayerListener != null) {
            getPlaylistControl().removePlayerListener(mPlayerListener);
            mPlayerListener = null;
        }

        if (mTimelineListener != null) {
            getPlaylistControl().removeTimelineChangedListener(mTimelineListener);
            mTimelineListener = null;
        }

        if (mOpenPlayerListener != null) {
            getPlaylistControl().removePlayerListener(mOpenPlayerListener);
            mOpenPlayerListener = null;
        }

        mPlaybackInitiated = false;

        mStoredPlaybackConfiguration = null;
        mJumpValue = 0;
    }

    @Override
    public void setSurface(Surface surface) {
        mSurface = surface;
    }

    @Override
    public void onSurfaceChanged(int format, int width, int height) {
        Log.v(TAG, "onSurfaceChanged() called with: format = [" + format + "], width = [" + width + "], height = [" + height + "]");
        if (mSubtitleView != null) {
            mSubtitleView.setMeasuredVideoDimensions(width, height);
        }
    }

    @Override
    public void setStreamVolume(float vol) {
        Log.v(TAG, "setStreamVolume() called with: vol = [" + vol + "]");
        getPlaylistControl().setVolume(vol);
    }

    public void play(OttTuneParams tuneParams) {
        List<OttPlayListItem> playListItems = tuneParams.getPlayListItems();

        try {
            getPlaylistControl().release();
            mPlayListItems.clear();

            OttPlayListItem initialPlaylistItem = playListItems.get(tuneParams.getDefaultItemIndex());
            PlayerConfig currentConfig = getPlayerConfig(initialPlaylistItem);

            // TODO : castlabs bug? selecting the playlist item by index is not working.
            mPlayListItems.add(initialPlaylistItem);
            getPlaylistControl().open(currentConfig);

            if (mOpenPlayerListener != null) {
                getPlaylistControl().removePlayerListener(mOpenPlayerListener);
            }
            getPlaylistControl().setInitialPositionProvider(timeline -> {
                String assetIdentifier = tuneParams.getAssetIdentifier();
                if (assetIdentifier == null) {
                    return InitialPositionProvider.DEFAULT_POSITION;
                }

                for (Timeline.Period period : timeline.getPeriods()) {
                    for (DashDescriptor descriptor : period.getDescriptors()) {
                        if (ASSET_ID_SCHEME_ID_URI.equals(descriptor.getSchemeIdUri()) && assetIdentifier.equals(descriptor.getValue())) {
                            return period.getPositionInWindowUs();
                        }
                    }
                }
                return InitialPositionProvider.DEFAULT_POSITION;
            });

            getPlaylistControl().addPlayerListener(mOpenPlayerListener = new CastlabsPlayerListener() {
                @Override
                public void onSeekCompleted() {
                    Log.v(TAG, "onSeekCompleted() called");
                }

                @Override
                public void onDurationChanged(long l) {
                    getPlaylistControl().seekWith(tuneParams.getStartPosition(TimeUnit.MICROSECONDS));

                    // Prepend playlist with the previous playback items.
                    if (tuneParams.getDefaultItemIndex() > 0) {
                        prepend(playListItems.subList(0, tuneParams.getDefaultItemIndex()));
                    }

                    // Append playlist with the next playback items.
                    if (tuneParams.getDefaultItemIndex() < playListItems.size() - 1) {
                        append(playListItems.subList(tuneParams.getDefaultItemIndex() + 1, playListItems.size()));
                    }

                    mOpenPlayerListener = null;
                    getPlaylistControl().removePlayerListener(this);
                }

                @Override
                public void onTrackKeyStatusChanged() {

                }
            });

            Log.d(TAG, "Open playlist. Size " + tuneParams.getPlayListItems().size()
                    + ", playlist index : " + tuneParams.getDefaultItemIndex()
                    + ", position : " + tuneParams.getStartPosition());
        } catch (Exception e) {
            Log.e(TAG, "Could not open stream.", e);
            mPlayerWrapperListener.notifyVideoUnavailable(
                    ITifPlayerWrapper.VIDEO_UNAVAILABLE_ERROR_FLAG | CastlabsPlayerException.TYPE_UNKNOWN);
        }
    }

    /**
     * Prepend the current playlist with a new item.
     *
     * @param prependParams The params for the new item in the play list.
     */
    @Override
    public void prepend(List<OttPlayListItem> prependParams) {
        Log.d(TAG, "prepend() -> params size : " + prependParams.size());

        if (prependParams.isEmpty()) {
            return;
        }

        for (int i = prependParams.size() - 1; i >= 0; --i) {
            OttPlayListItem prependParam = prependParams.get(i);
            PlayerConfig appendConfig = getPlayerConfig(prependParam);
            if (appendConfig == null) {
                Log.w(TAG, "Couldn't generate Castlabs player config for append.");
                return;
            }

            mPlayListItems.add(0, prependParam);
            getPlaylistControl().addItem(0, appendConfig);
        }

        handlePlaybackItemAdded();
    }

    /**
     * Append the current playlist with a new item.
     *
     * @param appendParams The params for the new item in the play list.
     */
    public void append(List<OttPlayListItem> appendParams) {
        Log.d(TAG, "append() -> params size : " + appendParams.size());

        if (appendParams.isEmpty()) {
            return;
        }

        for (OttPlayListItem appendParam : appendParams) {
            PlayerConfig appendConfig = getPlayerConfig(appendParam);
            if (appendConfig == null) {
                Log.w(TAG, "Couldn't generate Castlabs player config for append.");
                return;
            }

            mPlayListItems.add(appendParam);
            getPlaylistControl().addItem(appendConfig);
        }

        handlePlaybackItemAdded();
    }

    /**
     * Generate castalbs player config for the playlist item.
     *
     * @param playListItem The playlist item to generate player config from.
     * @return The castlabs player config which can be used to append the playlist.
     */
    private PlayerConfig getPlayerConfig(OttPlayListItem playListItem) {
        Log.d(TAG, "Append the next stream to the playlist."
                + " Playback url : " + playListItem.getStreamUrl()
                + " | Domain for playback: " + playListItem.getPlaybackDomain()
                + " | Licence URL: " + playListItem.getLicenceUrl()
                + " | DRM Client ID: " + playListItem.getDrmClientId());

        switch (playListItem.getPlaybackDomain()) {
            case TrickPlay:
                Log.d(TAG, "Trickplay configuration set up for the player.");
                return new TrickplayConfig.Builder(mContext, playListItem.getStreamUrl())
                        .setLicenseUrl(playListItem.getLicenceUrl(), playListItem.getDrmClientId())
                        .setTrickplaySpeed(playListItem.getPlaybackSpeed())
                        .build();
            case RadioOtt:
                Log.d(TAG, "Radio configuration set up for the player.");
                return new RadioConfig.Builder(mContext, playListItem.getStreamUrl())
                        .build();
            default:
                Log.d(TAG, "Using default configuration for the player, domain type: " + playListItem.getPlaybackDomain());
                return new StreamConfig.Builder(mContext, playListItem.getStreamUrl())
                        .setLicenseUrl(playListItem.getLicenceUrl(), playListItem.getDrmClientId())
                        .build();
        }
    }

    @Override
    public void setSpeed(double speed) {
        getPlaylistControl().setSpeed((float) speed);
    }

    @Override
    public double getSpeed() {
        return getPlaylistControl().getSpeed();
    }

    @Override
    public void setCaptionEnabled(boolean enabled) {
        if (mSubtitleView == null) {
            return;
        }

        if (enabled) {
            mSubtitleView.setVisibility(View.VISIBLE);
        } else {
            mSubtitleView.setVisibility(View.GONE);
        }
    }

    @Override
    public void setAudioTrack(String trackId) {
        if (trackId == null) {
            return;
        }

        // Find audio track by id.
        for (AudioTrack audioTrackTrack : getPlaylistControl().getAudioTracks()) {
            if (trackId.equals(audioTrackTrack.getId())) {
                getPlaylistControl().setAudioTrack(audioTrackTrack);
                mPlayerWrapperListener.notifyTrackSelected(TvTrackInfo.TYPE_AUDIO, trackId);
                return;
            }
        }
    }

    @Override
    public void setVideoTrack(String trackId) {
        if (trackId == null) {
            return;
        }

        // Find video track by id.
        for (VideoTrack videoTrack : getPlaylistControl().getVideoTracks()) {
            if (trackId.equals(videoTrack.getId())) {
                getPlaylistControl().setVideoTrack(videoTrack);
                mPlayerWrapperListener.notifyTrackSelected(TvTrackInfo.TYPE_VIDEO, trackId);
                return;
            }
        }
    }

    @Override
    public void setSubtitleTrack(String trackId) {
        if (trackId != null) {
            // Find subtitle track by id.
            for (SubtitleTrack subtitleTrack : getPlaylistControl().getSubtitleTracks()) {
                if (trackId.equals(subtitleTrack.getId())) {
                    getPlaylistControl().setSubtitleTrack(subtitleTrack);
                    mPlayerWrapperListener.notifyTrackSelected(TvTrackInfo.TYPE_SUBTITLE, trackId);
                    return;
                }
            }
        }

        getPlaylistControl().setSubtitleTrack(null);
        mPlayerWrapperListener.notifyTrackSelected(TvTrackInfo.TYPE_SUBTITLE, null);
    }

    @Override
    public void pause() {
        Log.d(TAG, "pause() called config = [" + getPlaylistControl().getPlayerConfig() + "]");
        getPlaylistControl().pause();
    }

    @Override
    public void resume() {
        Log.d(TAG, "resume() called config = [" + getPlaylistControl().getPlayerConfig() + "]");
        getPlaylistControl().play();
    }

    public void saveState() {
        Bundle state = new Bundle();
        getPlaylistControl().saveState(state);
        mStoredPlaybackConfiguration = state;
    }

    public void retry() {
        Log.d(TAG, "retry() called");

        // release the player and re-open it with the bundle
        getPlaylistControl().release();
        mPlayListItems.clear();

        Bundle configuration = mStoredPlaybackConfiguration;
        if (configuration != null && !configuration.isEmpty()) {
            getPlaylistControl().open(configuration);
        }
    }

    /**
     * Seek the player to the new position in milliseconds.
     */
    @Override
    public void seekTo(long ms) {
        Log.d(TAG, "seekTo() called with: ms = [" + ms + "]");

        mJumpValue = TimeUnit.MILLISECONDS.toMicros(ms);
        if (getPlaylistControl().getCurrentItemIndex() == getPlaylistControl().getSize() - 1
                && mJumpValue >= getPlaylistControl().getDuration()) {
            // avoid to seek outside of the stream.
            mJumpValue = getPlaylistControl().getDuration() - 1;
        }

        getPlaylistControl().seekWith(
                mJumpValue - getPlaylistControl().getPosition());
        mJumpValue = 0;
    }

    /**
     * Returns the current position of the playback in milliseconds.
     */
    @Override
    public long getCurrentPosition() {
        return TimeUnit.MICROSECONDS.toMillis(mJumpValue != 0 ? mJumpValue : getPlaylistControl().getPosition());
    }

    /**
     * @return The index of the currently playing item.
     */
    @Override
    public int getCurrentPlaylistIndex() {
        return mSingleControllerPlaylist.getCurrentItemIndex();
    }

    @Override
    public void setPlaybackParams(PlaybackParams params) {
        Log.d(TAG, "setPlaybackParams() called with: params = [" + params + "]");
    }

    @Override
    public List<TvTrackInfo> getAllTracks() {
        List<TvTrackInfo> allTracks = new ArrayList<>();
        allTracks.addAll(getVideoTracksInfo());
        allTracks.addAll(getAudioTracksInfo());
        allTracks.addAll(getSubtitlesTracksInfo());
        return allTracks;
    }

    @Override
    public String getSelectedTrack(int type) {
        switch (type) {
            case TvTrackInfo.TYPE_VIDEO:
                if (getPlaylistControl().getVideoTrack() != null) {
                    VideoTrack videoTrack = getPlaylistControl().getVideoTrack();
                    return videoTrack.getId() == null ?
                            String.valueOf(videoTrack.getOriginalTrackIndex()) : videoTrack.getId();
                }
                return null;

            case TvTrackInfo.TYPE_AUDIO:
                if (getPlaylistControl().getAudioTrack() != null) {
                    return getPlaylistControl().getAudioTrack().getId();
                }
                return null;

            case TvTrackInfo.TYPE_SUBTITLE:
                if (getPlaylistControl().getSubtitleTrack() != null) {
                    return getPlaylistControl().getSubtitleTrack().getId();
                }
                return null;
        }

        return null;
    }

    /**
     * Get the duration of the current media in milliseconds.
     * If the current media is live, this will return -1 for duration.
     */
    @Override
    public long getDuration() {
        return TimeUnit.MICROSECONDS.toMillis(getPlaylistControl().getDuration());
    }

    protected View createSubtitleView() {
        Log.d(TAG, "create Subtitle View() called");

        if (mSubtitleView == null) {

            mSubtitleView = new SubtitlesView(mContext);

            mSubtitleView.setLayoutParams(new MarginLayoutParams(
                    MarginLayoutParams.MATCH_PARENT, MarginLayoutParams.MATCH_PARENT));
            mSubtitleView.setVisibility(View.VISIBLE);

            getPlaylistControl().setComponentView(R.id.presto_castlabs_subtitles_view, mSubtitleView);
        }
        return mSubtitleView;
    }

    protected View createDebugView() {
        Log.d(TAG, "createDebugView() called");
        if (mDebugView == null) {
            View debugView = LayoutInflater.from(mContext).inflate(R.layout.debug_charts_layout, null);

            debugView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


            getPlaylistControl().setComponentView(R.id.presto_debug_overlay_view, debugView);
            debugView.setVisibility(View.VISIBLE);
            mDebugView = debugView;
        } else {
            Log.d(TAG, "No need to create new debug view");
        }

        return mDebugView;
    }

    protected void removeDebugView() {
        if (mDebugView != null) {
            mDebugView.setVisibility(View.GONE);
            mDebugView = null;
        }
        getPlaylistControl().unsetComponentView(R.id.presto_debug_overlay_view);
    }

    @Override
    public View createOverlayView() {
        Log.d(TAG, "createOverlayView() called");
        if (mOverlayView != null) {
            mOverlayView.removeAllViews();
        } else {
            mOverlayView = new FrameLayout(mContext);
            mOverlayView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }

        createSubtitleView();
        mOverlayView.addView(mSubtitleView);

        if (Settings.playerDebugOverlayEnabled.get(false)) {
            createDebugView();
            mOverlayView.addView(mDebugView);
        } else {
            mOverlayView.removeView(mDebugView);
            removeDebugView();
        }

        mOverlayView.setVisibility(View.VISIBLE);

        return mOverlayView;
    }

    protected abstract void handleDurationChanged(long durationMs);

    protected abstract void handleUnskippableAdChanged(long[] starts, long[] ends);

    /**
     * Called when the playing playlist item changes.
     *
     * @param playListItem The playlist item which is currently playing.
     */
    protected void handlePlaylistItemChanged(OttPlayListItem playListItem) {
    }

    protected abstract void handlePlayerStateChange(long timestamp, String playbackState);

    protected abstract void handlePlaybackItemAdded();

    protected abstract void handlePlaybackException(String exceptionType, String exceptionMessage);

    /**
     * Get the available video tracks converted to android TvTrackInfo
     */
    private List<TvTrackInfo> getVideoTracksInfo() {
        List<TvTrackInfo> convertedTracks = new ArrayList<>();
        List<VideoTrack> originalTracks = getPlaylistControl().getVideoTracks();

        for (VideoTrack videoTrack : originalTracks) {
            for (VideoTrackQuality videoTrackQuality : videoTrack.getQualities()) {
                convertedTracks.add(new TvTrackInfo.Builder(TvTrackInfo.TYPE_VIDEO,
                        videoTrack.getId() == null ? String.valueOf(videoTrackQuality.getOriginalTrackIndex()) : videoTrack.getId())
                        .setVideoWidth(videoTrackQuality.getWidth())
                        .setVideoHeight(videoTrackQuality.getHeight())
                        .setVideoPixelAspectRatio((float) videoTrackQuality.getWidth() / videoTrackQuality.getHeight())
                        .setVideoFrameRate(videoTrackQuality.getFrameRate())
                        .build());
            }
        }

        return convertedTracks;
    }

    protected static final String TRACK_EXTRA_SUBTITLE_TYPE = "subtitles_type";
    protected static final String TRACK_EXTRA_SUBTITLE_TYPE_HARD_OF_HEARING = "hard_of_hearing";

    protected static final String TRACK_EXTRA_AUDIO_TYPE = "audio_type";
    protected static final String TRACK_EXTRA_AUDIO_TYPE_NORMAL = "normal";
    protected static final String TRACK_EXTRA_AUDIO_TYPE_ACCESSIBILITY = "accessibility";

    /**
     * Get the available audio tracks converted to android TvTrackInfo
     */
    private List<TvTrackInfo> getAudioTracksInfo() {
        List<TvTrackInfo> convertedTracks = new ArrayList<>();
        List<AudioTrack> originalTracks = getPlaylistControl().getAudioTracks();
        Bundle extras = new Bundle();

        for (AudioTrack audioTrack : originalTracks) {
            TvTrackInfo.Builder trackInfo = new TvTrackInfo.Builder(TvTrackInfo.TYPE_AUDIO, audioTrack.getId() == null ? "" : audioTrack.getId());
            extras.putString(TRACK_EXTRA_AUDIO_TYPE, TRACK_EXTRA_AUDIO_TYPE_NORMAL);
            // Workaround for missing language code and accessibility options showing up as "qaa"
            if (!MediaTrackInfo.LANG_FRENCH_AD_AUDIO.equals(audioTrack.getLanguage()) && audioTrack.getDescriptors() != null) {
                for (DashDescriptor descriptor : audioTrack.getDescriptors()) {
                    if (descriptor.getType() == DashDescriptor.ACCESSIBILITY) {
                        extras.putString(TRACK_EXTRA_AUDIO_TYPE, TRACK_EXTRA_AUDIO_TYPE_ACCESSIBILITY);
                    }
                }
            }
            addDolbyCodecToBundle(extras, audioTrack.getCodecs());
            convertedTracks.add(trackInfo
                    .setAudioChannelCount(audioTrack.getAudioChannels())
                    .setAudioSampleRate(Math.round(audioTrack.getAudioSamplingRate()))
                    .setLanguage(audioTrack.getLanguage())
                    .setExtra(extras)
                    .build());
        }

        return convertedTracks;
    }

    private Bundle addDolbyCodecToBundle(Bundle bundle, String codec) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putString(PlaybackManager.EXTRA_STREAM_TYPE, codec);
        return bundle;
    }

    /**
     * Get the available subtitles tracks converted to android TvTrackInfo
     */
    private List<TvTrackInfo> getSubtitlesTracksInfo() {
        List<TvTrackInfo> convertedTracks = new ArrayList<>();
        List<SubtitleTrack> originalTracks = getPlaylistControl().getSubtitleTracks();
        Bundle extras = new Bundle();

        for (SubtitleTrack subtitleTrack : originalTracks) {
            // Workaround for missing language code and accessibility options showing up as "qaa"
            TvTrackInfo.Builder trackInfo = new TvTrackInfo.Builder(TvTrackInfo.TYPE_SUBTITLE, subtitleTrack.getId() == null ? "" : subtitleTrack.getId());
            if (!MediaTrackInfo.LANG_FRENCH_AD_AUDIO.equals(subtitleTrack.getLanguage()) && subtitleTrack.getDescriptors() != null) {
                for (DashDescriptor descriptor : subtitleTrack.getDescriptors()) {
                    if (descriptor.getType() == DashDescriptor.ACCESSIBILITY) {
                        extras.putString(TRACK_EXTRA_SUBTITLE_TYPE, TRACK_EXTRA_SUBTITLE_TYPE_HARD_OF_HEARING);
                    }
                }
            }
            convertedTracks.add(trackInfo
                    // Create a new language code for subtitle types only, as workaround
                    .setLanguage(MediaTrackInfo.LANG_FRENCH_AD_AUDIO.equals(subtitleTrack.getLanguage()) ? MediaTrackInfo.LANG_FRENCH_AD_SUBTITLE : subtitleTrack.getLanguage())
                    .setExtra(extras)
                    .build());
        }

        return convertedTracks;
    }

    private final SingleControllerPlaylist.PlaylistListener mPlaylistListener = new SingleControllerPlaylist.PlaylistListener() {
        @Override
        public void onItemChange(@NonNull PlayerConfig playerConfig) {
            int playlistItemIndex = mSingleControllerPlaylist.getCurrentItemIndex();
            if (playlistItemIndex < 0 || playlistItemIndex >= mPlayListItems.size()) {
                Log.e(TAG, "Item outside of playlist is playing.");
                return;
            }

            handlePlaylistItemChanged(mPlayListItems.get(playlistItemIndex));
        }
    };
}
