package com.threess.threeready.playerCastlabs.configuration;

import android.content.Context;
import android.text.TextUtils;

import com.castlabs.android.SdkConsts;
import com.castlabs.android.drm.Drm;
import com.castlabs.android.drm.DrmConfiguration;
import com.castlabs.android.network.NetworkConfiguration;
import com.castlabs.android.network.RetryConfiguration;
import com.castlabs.android.player.AbrConfiguration;
import com.castlabs.android.player.BufferConfiguration;
import com.castlabs.android.player.LiveConfiguration;
import com.castlabs.android.player.VideoFilterConfiguration;

import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.config.model.PlaybackSettings;
import tv.threess.threeready.api.generic.helper.BundleBuilder;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.mw.MwProxy;

/**
 * Base configuration for the playback.
 * All basic necessary configurations are set here for basic playback.
 *
 * @author Andor Lukacs
 * @since 3/6/19
 */
public class StreamConfig {
    private static final String TAG = Log.tag(StreamConfig.class);

    protected static final float BANDWIDTH_FRACTION_PERCENT = 0.95F;
    private static final String DRM_CLIENT_ID_HEADER = "deviceId";

    public static class Builder<T extends Builder> {
        protected DrmConfiguration mDrmConfiguration;
        protected final com.castlabs.android.player.PlayerConfig.Builder mPlayerConfiguration;

        public Builder(Context context, String url) {
            PlaybackSettings settings = Components.get(AppConfig.class).getPlaybackSettings();

            boolean isLowestInit = Settings.lowestInitTrack.get(settings.isLowestInitTrack());
            Log.d(TAG, "isLowestInit : " + isLowestInit);

            boolean abrOn = Settings.abrOn.get(settings.isAbrOn());
            Log.d(TAG, "abrOn : " + abrOn);

            long minDurationQualityIncrease = Settings.minDurationQualityIncrease.get(settings.getMinDurationQualityIncrease());
            Log.d(TAG, "minDurationQualityIncrease : " + minDurationQualityIncrease);

            long maxDurationQualityDecrease = Settings.maxDurationQualityDecrease.get(settings.getMaxDurationQualityDecrease());
            Log.d(TAG, "maxDurationQualityDecrease : " + maxDurationQualityDecrease);

            long minDurationToRetainAfterDiscard = Settings.minDurationRetainAfterDiscard.get(settings.getMinDurationRetainAfterDiscard());
            Log.d(TAG, "minDurationToRetainAfterDiscard : " + minDurationToRetainAfterDiscard);

            int minDegradationSamples = Settings.minDegradationSamples.get(settings.getMinDegradationSamples());

            AbrConfiguration abrConfiguration = new AbrConfiguration.Builder()
                    .initialTrackSelection(isLowestInit ? SdkConsts.VIDEO_QUALITY_LOWEST : SdkConsts.VIDEO_QUALITY_HIGHEST, !abrOn)
                    .method(AbrConfiguration.METHOD_COMMON_NBA)
                    .minDurationForQualityIncrease(minDurationQualityIncrease, TimeUnit.SECONDS) //low value (5 sec)
                    .maxDurationForQualityDecrease(maxDurationQualityDecrease, TimeUnit.SECONDS) //10-12 seconds
                    .minDurationToRetainAfterDiscard(minDurationToRetainAfterDiscard, TimeUnit.SECONDS) //buffer re-load for higher quality; 15 seconds
                    .bandwidthFraction(BANDWIDTH_FRACTION_PERCENT) // 95 percent fraction -> 5 percent error
                    .minDegradationSamples(minDegradationSamples)
                    .downloadTimeFactor(1) // No dynamic stream bandwidth
                    .get();

            int minPlaybackStart = Settings.minimumPlaybackStartSeconds.get(settings.getMinimumPlaybackStartSeconds());
            Log.d(TAG, "minimumPlaybackStartSeconds : " + minPlaybackStart);

            int bufferSize = Settings.bufferSize.get(settings.getBufferSize());
            Log.d(TAG, "bufferSize : " + bufferSize);

            int lowMediaTime = Settings.lowMediaTime.get(settings.getLowMediaTime());
            int highMediaTime = Settings.highMediaTime.get(settings.getHighMediaTime());
            boolean drainWhileCharging = Settings.drainWhileCharging.get(settings.isDrainWhileCharging());

            BufferConfiguration bufferConfiguration = new BufferConfiguration.Builder()
                    .minPlaybackStart(minPlaybackStart, TimeUnit.SECONDS)
                    .bufferSizeBytes(bufferSize) // 20 MB
                    .lowMediaTime(lowMediaTime, TimeUnit.SECONDS)
                    .highMediaTime(highMediaTime, TimeUnit.SECONDS)
                    .drainWhileCharging(drainWhileCharging) // save bandwidth
                    .get();

            int controlBandwidth = Settings.bandwidth.get(0);
            int bandwidthMin = Settings.bandwidthMinimum.get(settings.getControlMinimum());
            int maxBitrateFilter = Settings.maximumLimitedBitrate.get(settings.getMaximumLimitedBitrate());
            long maxBitrate = controlBandwidth > bandwidthMin ? Long.MAX_VALUE : maxBitrateFilter;
            Log.d(TAG, "limiting bandwidth: bandwidth = " + controlBandwidth + " configured minimum bandwidth: " + bandwidthMin + " maximum limited bitrate = " + maxBitrateFilter + " maxBitrate: " + maxBitrate);

            VideoFilterConfiguration videoFilterConfiguration = new VideoFilterConfiguration.Builder()
                    .maxBitrate(maxBitrate)
                    .get();

            int manifestRetryCount = Settings.manifestRetryCount.get(settings.getManifestRetryCount());
            Log.d(TAG, "manifestRetryCount : " + manifestRetryCount);

            long manifestRetryDelay = Settings.manifestRetryDelay.get(settings.getManifestRetryDelay());
            Log.d(TAG, "manifestRetryDelay : " + manifestRetryDelay);

            RetryConfiguration manifestRetryConfiguration = new RetryConfiguration.Builder()
                    .maxAttempts(manifestRetryCount)
                    .maxDelayMs(manifestRetryDelay)
                    .get();

            int segmentRetryCount = Settings.segmentRetryCount.get(settings.getSegmentRetryCount());
            Log.d(TAG, "segmentRetryCount : " + segmentRetryCount);

            long segmentRetryDelay = Settings.segmentRetryDelay.get(settings.getSegmentRetryDelay());
            Log.d(TAG, "segmentRetryDelay : " + segmentRetryDelay);

            RetryConfiguration segmentRetryConfiguration = new RetryConfiguration.Builder()
                    .maxAttempts(segmentRetryCount)
                    .maxDelayMs(segmentRetryDelay)
                    .get();

            int manifestConnectTimeout = Settings.manifestConnectTimeout.get(settings.getManifestConnectTimeout());
            Log.d(TAG, "manifestConnectTimeout : " + manifestConnectTimeout);

            int manifestReadTimeout = Settings.manifestReadTimeout.get(settings.getManifestReadTimeout());
            Log.d(TAG, "manifestReadTimeout : " + manifestReadTimeout);

            int segmentConnectTimeout = Settings.segmentConnectTimeout.get(settings.getSegmentConnectTimeout());
            Log.d(TAG, "segmentConnectTimeout : " + segmentConnectTimeout);

            int segmentReadTimeout = Settings.segmentReadTimeout.get(settings.getSegmentReadTimeout());
            Log.d(TAG, "segmentReadTimeout : " + segmentReadTimeout);

            int drmConnectionTimeout = Settings.drmConnectionTimeout.get(settings.getDrmConnectionTimeout());
            Log.d(TAG, "drmConnectionTimeout : " + drmConnectionTimeout);

            int drmReadTimeout = Settings.drmReadTimeout.get(settings.getDrmReadTimeout());
            Log.d(TAG, "drmReadTimeout : " + drmReadTimeout);

            int drmAcquisitionTimeout = Settings.drmAcquisitionTimeout.get(settings.getDrmAcquisitionTimeout());
            Log.d(TAG, "drmAcquisitionTimeout : " + drmAcquisitionTimeout);

            NetworkConfiguration networkConfiguration = new NetworkConfiguration.Builder()
                    .manifestConnectionTimeoutMs(manifestConnectTimeout)
                    .manifestReadTimeoutMs(manifestReadTimeout)
                    .segmentsConnectionTimeoutMs(segmentConnectTimeout)
                    .segmentsReadTimeoutMs(segmentReadTimeout)
                    .manifestRetryConfiguration(manifestRetryConfiguration)
                    .segmentsRetryConfiguration(segmentRetryConfiguration)
                    .drmAcquisitionTimeoutMs(drmAcquisitionTimeout)
                    .drmConnectionTimeoutMs(drmConnectionTimeout)
                    .drmReadTimeoutMs(drmReadTimeout)
                    .get();

            mDrmConfiguration = null;

            mPlayerConfiguration = new com.castlabs.android.player.PlayerConfig.Builder(url);
            mPlayerConfiguration.userID(Components.get(MwProxy.class).getEthernetMacAddress());
            mPlayerConfiguration.positionUs(1);
            mPlayerConfiguration.autoPlay(true);
            mPlayerConfiguration.abrConfiguration(abrConfiguration);
            mPlayerConfiguration.bufferConfiguration(bufferConfiguration);
            mPlayerConfiguration.contentType(SdkConsts.CONTENT_TYPE_DASH);
            mPlayerConfiguration.networkConfiguration(networkConfiguration);
            mPlayerConfiguration.videoFilterConfiguration(videoFilterConfiguration);
            mPlayerConfiguration.enableDashEventCallback(true);

            //Default live configuration on case of a livestream
            LiveConfiguration liveConfiguration = new LiveConfiguration();
            liveConfiguration.liveEdgeLatencyMs = Settings.ottPlayerLiveEdgeMs.get(settings.getLiveEdgeLatencyMs());

            mPlayerConfiguration.liveConfiguration(liveConfiguration);
        }

        public T setStreamUrl(String streamUrl) {
            mPlayerConfiguration.contentUrl(streamUrl);
            return (T) this;
        }

        public T setAbrConfiguration(AbrConfiguration abrConfiguration) {
            mPlayerConfiguration.abrConfiguration(abrConfiguration);
            return (T) this;
        }

        public T setBufferConfiguration(BufferConfiguration bufferConfiguration) {
            mPlayerConfiguration.bufferConfiguration(bufferConfiguration);
            return (T) this;
        }

        public T setPlaybackPosition(long playbackPosition) {
            mPlayerConfiguration.positionUs(playbackPosition);
            return (T) this;
        }

        public T setLicenseUrl(String licenseUrl, String drmClientID) {
            if (!TextUtils.isEmpty(licenseUrl)) {
                mDrmConfiguration = new DrmConfiguration(licenseUrl, true, Drm.Widevine, null, null,
                        new BundleBuilder().put(DRM_CLIENT_ID_HEADER, drmClientID).build());
                mPlayerConfiguration.drmConfiguration(mDrmConfiguration);
                mPlayerConfiguration.forceInStreamDrmInitData(true);
            }
            return (T) this;
        }

        public T setContentType(int contentType) {
            mPlayerConfiguration.contentType(contentType);
            return (T) this;
        }

        public T setNetworkConfiguration(NetworkConfiguration networkConfiguration) {
            mPlayerConfiguration.networkConfiguration(networkConfiguration);
            return (T) this;
        }

        public T setPreferredAudioLanguage(String audioLanguage) {
            if (!TextUtils.isEmpty(audioLanguage)) {
                mPlayerConfiguration.preferredAudioLanguage(audioLanguage);
            }
            return (T) this;
        }

        public T setPreferredSubtitleLanguage(String subtitleLanguage) {
            if (!TextUtils.isEmpty(subtitleLanguage)) {
                mPlayerConfiguration.preferredTextLanguage(subtitleLanguage);
            }
            return (T) this;
        }

        public T setLiveConfiguration(LiveConfiguration liveConfiguration) {
            mPlayerConfiguration.liveConfiguration(liveConfiguration);
            return (T) this;
        }

        public com.castlabs.android.player.PlayerConfig build() {
            return mPlayerConfiguration.get();
        }
    }
}
