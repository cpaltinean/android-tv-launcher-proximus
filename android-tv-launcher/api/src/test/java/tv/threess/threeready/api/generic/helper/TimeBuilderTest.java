/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.generic.helper;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * Test unit for utility class.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.02.14
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class TimeBuilderTest {

    private TimeBuilder build(int year, int month, int day, int hour, int min, int sec, int millis) {
        return TimeBuilder.utc().set(0)
                .set(Calendar.YEAR, year)
                .set(Calendar.MONTH, month - 1)
                .set(Calendar.DAY_OF_MONTH, day)
                .set(Calendar.HOUR_OF_DAY, hour)
                .set(Calendar.MINUTE, min)
                .set(Calendar.SECOND, sec)
                .set(Calendar.MILLISECOND, millis);
    }

    @Test
    public void testFloor() {
        TimeBuilder builder = TimeBuilder.utc();
        long now = build(2019, 3, 24, 7, 27, 33, 78).get();
        Assert.assertEquals("build", 1553412453078L, builder.set(now).get());

        long floorSec = build(2019, 3, 24, 7, 27, 33, 0).get();
        Assert.assertEquals("Floor to seconds", floorSec, builder.set(now).floor(TimeUnit.SECONDS).get());
        long floorSec5 = build(2019, 3, 24, 7, 27, 30, 0).get();
        Assert.assertEquals("Floor to seconds", floorSec5, builder.set(now).floor(TimeUnit.SECONDS, 5).get());

        long floorMin = build(2019, 3, 24, 7, 27, 0, 0).get();
        Assert.assertEquals("Floor to minutes", floorMin, builder.set(now).floor(TimeUnit.MINUTES).get());
        long floorMin5 = build(2019, 3, 24, 7, 25, 0, 0).get();
        Assert.assertEquals("Floor to minutes", floorMin5, builder.set(now).floor(TimeUnit.MINUTES, 5).get());
        long floorMin10 = build(2019, 3, 24, 7, 20, 0, 0).get();
        Assert.assertEquals("Floor to minutes", floorMin10, builder.set(now).floor(TimeUnit.MINUTES, 10).get());
        long floorMin15 = build(2019, 3, 24, 7, 15, 0, 0).get();
        Assert.assertEquals("Floor to minutes", floorMin15, builder.set(now).floor(TimeUnit.MINUTES, 15).get());

        long floorHour = build(2019, 3, 24, 7, 0, 0, 0).get();
        Assert.assertEquals("Floor to hours", floorHour, builder.set(now).floor(TimeUnit.HOURS).get());
        long floorHour5 = build(2019, 3, 24, 5, 0, 0, 0).get();
        Assert.assertEquals("Floor to hours", floorHour5, builder.set(now).floor(TimeUnit.HOURS, 5).get());
        long floorHour6 = build(2019, 3, 24, 6, 0, 0, 0).get();
        Assert.assertEquals("Floor to hours", floorHour6, builder.set(now).floor(TimeUnit.HOURS, 2).get());
        Assert.assertEquals("Floor to hours", floorHour6, builder.set(now).floor(TimeUnit.HOURS, 3).get());
        Assert.assertEquals("Floor to hours", floorHour6, builder.set(now).floor(TimeUnit.HOURS, 6).get());

        long floorDay = build(2019, 3, 24, 0, 0, 0, 0).get();
        Assert.assertEquals("Floor to days", floorDay, builder.set(now).floor(TimeUnit.DAYS).get());
    }

    @Test
    public void testCeil() {
        TimeBuilder builder = TimeBuilder.utc();
        long now = build(2019, 3, 24, 7, 27, 33, 78).get();
        Assert.assertEquals("build", 1553412453078L, builder.set(now).get());

        long ceilSec = build(2019, 3, 24, 7, 27, 34, 0).get();
        Assert.assertEquals("ceil to seconds", ceilSec, builder.set(now).ceil(TimeUnit.SECONDS).get());
        long ceilSec5 = build(2019, 3, 24, 7, 27, 35, 0).get();
        Assert.assertEquals("ceil to seconds", ceilSec5, builder.set(now).ceil(TimeUnit.SECONDS, 5).get());

        long ceilMin = build(2019, 3, 24, 7, 28, 0, 0).get();
        Assert.assertEquals("ceil to minutes", ceilMin, builder.set(now).ceil(TimeUnit.MINUTES).get());
        long ceilMin5 = build(2019, 3, 24, 7, 30, 0, 0).get();
        Assert.assertEquals("ceil to minutes", ceilMin5, builder.set(now).ceil(TimeUnit.MINUTES, 5).get());
        long ceilMin10 = build(2019, 3, 24, 7, 30, 0, 0).get();
        Assert.assertEquals("ceil to minutes", ceilMin10, builder.set(now).ceil(TimeUnit.MINUTES, 10).get());
        long ceilMin40 = build(2019, 3, 24, 7, 40, 0, 0).get();
        Assert.assertEquals("ceil to minutes", ceilMin40, builder.set(now).ceil(TimeUnit.MINUTES, 20).get());

        long ceilHour = build(2019, 3, 24, 8, 0, 0, 0).get();
        Assert.assertEquals("ceil to hours", ceilHour, builder.set(now).ceil(TimeUnit.HOURS).get());
        long ceilHour5 = build(2019, 3, 24, 10, 0, 0, 0).get();
        Assert.assertEquals("ceil to hours", ceilHour5, builder.set(now).ceil(TimeUnit.HOURS, 5).get());

        long ceilHour8 = build(2019, 3, 24, 8, 0, 0, 0).get();
        long ceilHour9 = build(2019, 3, 24, 9, 0, 0, 0).get();
        long ceilHour12 = build(2019, 3, 24, 12, 0, 0, 0).get();
        long ceilHour24 = build(2019, 3, 25, 0, 0, 0, 0).get();
        Assert.assertEquals("ceil to hours", ceilHour8, builder.set(now).ceil(TimeUnit.HOURS, 2).get());
        Assert.assertEquals("ceil to hours", ceilHour9, builder.set(now).ceil(TimeUnit.HOURS, 3).get());
        Assert.assertEquals("ceil to hours", ceilHour12, builder.set(now).ceil(TimeUnit.HOURS, 6).get());
        Assert.assertEquals("ceil to hours", ceilHour12, builder.set(now).ceil(TimeUnit.HOURS, 12).get());
        Assert.assertEquals("ceil to hours", ceilHour24, builder.set(now).ceil(TimeUnit.HOURS, 24).get());

        long ceilDay = build(2019, 3, 25, 0, 0, 0, 0).get();
        Assert.assertEquals("ceil to days", ceilDay, builder.set(now).ceil(TimeUnit.DAYS).get());
    }

    @Test
    public void testBoundaries() {
        TimeBuilder builder = TimeBuilder.utc();
        Assert.assertEquals("Day boundary past", 1384214400000L, builder.set(1384020727000L).floor(TimeUnit.DAYS).add(TimeUnit.DAYS, 3).get());
        Assert.assertEquals("Day boundary future", 1384300800000L, builder.set(1384020727000L).ceil(TimeUnit.DAYS).add(TimeUnit.DAYS, 3).get());
        Assert.assertEquals("Hour boundary past", 1384030800000L, builder.set(1384020727000L).floor(TimeUnit.HOURS).add(TimeUnit.HOURS, 3).get());
        Assert.assertEquals("Hour boundary future", 1384034400000L, builder.set(1384020727000L).ceil(TimeUnit.HOURS).add(TimeUnit.HOURS, 3).get());
        final long now = System.currentTimeMillis();
        final long boundary = (now + 300000L) - (now % 1000L);
        Assert.assertEquals("Hour boundary past", boundary, builder.set(now).floor(TimeUnit.SECONDS).add(TimeUnit.SECONDS, 300).get());
    }

    @Test
    public void testDistributionSlot() {
        final long now = System.currentTimeMillis();
        final int startH = 17;
        final int endH = 18;
        final Calendar c = Calendar.getInstance();
        TimeBuilder builder = TimeBuilder.local();
        c.setTimeInMillis(now);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.HOUR_OF_DAY, startH);
        final int year = c.get(Calendar.YEAR);
        final int day = c.get(Calendar.DAY_OF_YEAR);

        final long result = builder.floor(TimeUnit.HOURS).distribute(TimeUnit.HOURS, startH, endH).get();
        Assert.assertTrue("Distributed slot is after start", c.getTimeInMillis() <= result );
        c.set(Calendar.HOUR_OF_DAY, endH);
        Assert.assertTrue("Distributed slot is before end", c.getTimeInMillis() >= result );
        c.setTimeInMillis(result);
        Assert.assertEquals("Distributed slot is in the same year", year, c.get(Calendar.YEAR) );
        Assert.assertEquals("Distributed slot is in the same day", day, c.get(Calendar.DAY_OF_YEAR) );
        Assert.assertTrue("Distributed slot has the hour after start", startH <= c.get(Calendar.HOUR_OF_DAY) );
        Assert.assertTrue("Distributed slot has the hour before end", endH >= c.get(Calendar.HOUR_OF_DAY) );
    }

    @Test
    public void testDistribution() {
        final long now = System.currentTimeMillis();

        for (int i = 1; i < 100; i++) {
            Assert.assertTrue(now + TimeUnit.MILLISECONDS.toMillis(i) > TimeBuilder.utc(now).distribute(TimeUnit.MILLISECONDS, i).get());
            Assert.assertTrue(now - TimeUnit.MILLISECONDS.toMillis(i) < TimeBuilder.utc(now).distribute(TimeUnit.MILLISECONDS, -i).get());

            Assert.assertTrue(now + TimeUnit.SECONDS.toMillis(i) > TimeBuilder.utc(now).distribute(TimeUnit.SECONDS, i).get());
            Assert.assertTrue(now - TimeUnit.SECONDS.toMillis(i) < TimeBuilder.utc(now).distribute(TimeUnit.SECONDS, -i).get());

            Assert.assertTrue(now + TimeUnit.MINUTES.toMillis(i) > TimeBuilder.utc(now).distribute(TimeUnit.MINUTES, i).get());
            Assert.assertTrue(now - TimeUnit.MINUTES.toMillis(i) < TimeBuilder.utc(now).distribute(TimeUnit.MINUTES, -i).get());

            Assert.assertTrue(now + TimeUnit.HOURS.toMillis(i) > TimeBuilder.utc(now).distribute(TimeUnit.HOURS, i).get());
            Assert.assertTrue(now - TimeUnit.HOURS.toMillis(i) < TimeBuilder.utc(now).distribute(TimeUnit.HOURS, -i).get());

            Assert.assertTrue(now + TimeUnit.DAYS.toMillis(i) > TimeBuilder.utc(now).distribute(TimeUnit.DAYS, i).get());
            Assert.assertTrue(now - TimeUnit.DAYS.toMillis(i) < TimeBuilder.utc(now).distribute(TimeUnit.DAYS, -i).get());
        }

        Assert.assertEquals(now, TimeBuilder.utc(now).distribute(TimeUnit.MILLISECONDS, 0).get());
        Assert.assertEquals(now, TimeBuilder.utc(now).distribute(TimeUnit.SECONDS, 0).get());
        Assert.assertEquals(now, TimeBuilder.utc(now).distribute(TimeUnit.MINUTES, 0).get());
        Assert.assertEquals(now, TimeBuilder.utc(now).distribute(TimeUnit.HOURS, 0).get());
        Assert.assertEquals(now, TimeBuilder.utc(now).distribute(TimeUnit.DAYS, 0).get());
    }
}
