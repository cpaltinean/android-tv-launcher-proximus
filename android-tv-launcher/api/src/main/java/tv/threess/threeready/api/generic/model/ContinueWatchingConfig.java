package tv.threess.threeready.api.generic.model;

import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Component;

/**
 * Continue watching related config settings.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public interface ContinueWatchingConfig extends Component {
    /**
     *
     * @return The maximum number of items to be displayed in the continue watching stripe.
     */
    int getContinueWatchingStripeMaxSize();

    /**
     * @return The minimum watched time after which replay bookmark needs to be added.
     * @param unit The time unit to return value from.
     */
    long getReplayWatchedMinTime(TimeUnit unit);

    /**
     * @return The minimum watched time after which recording bookmark needs to be added.
     * @param unit The time unit to return value from.
     */
    long getRecordingWatchedMinTime(TimeUnit unit);

    /**
     * @return The minimum watched time after which VOD bookmark needs to be added.
     * @param unit The time unit to return value from.
     */
    long getVodWatchedMinTime(TimeUnit unit);

    /**
     * @return The maximum watched percentage
     * after which the bookmark for the replay needs to be removed.
     */
    double getReplayWatchedPercent();

    /**
     * @return The maximum watched percentage
     * after which the bookmark for the recording needs to be removed.
     */
    double getRecordingWatchedPercent();

    /**
     * @return The maximum watched percentage
     * after which the bookmark for the VOD needs to be removed.
     */
    double getVodWatchedMaxPercent();

    /**
     * @return The maximum time to keep the bookmarks.
     * @param unit The time unit to return value from.
     */
    long geAgeLimit(TimeUnit unit);
}
