/*
 * Entity class representing a record
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.generic.model;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Basic implementation of a season container.
 * Converts a list of episode into season.
 *
 * @author Barabas Attila
 * @since 2018.11.25
 */
public class Season<TContentItem extends IContentItem> implements ISeason<TContentItem> {

    private final Integer mSeasonNumber;
    private final List<TContentItem> mEpisodes;

    public Season(List<TContentItem> episodes) {
        this(null, episodes);
    }

    public Season(Integer seasonNumber, List<TContentItem> episodes) {
        mSeasonNumber = seasonNumber;
        mEpisodes = new ArrayList<>(episodes);
    }

    @Override
    @Nullable
    public Integer getSeasonNumber() {
        return mSeasonNumber;
    }

    @Override
    public List<TContentItem> getEpisodes() {
        return mEpisodes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Season<?> season = (Season<?>) o;
        return Objects.equals(mSeasonNumber, season.mSeasonNumber) &&
                Objects.equals(mEpisodes, season.mEpisodes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mSeasonNumber, mEpisodes);
    }
}
