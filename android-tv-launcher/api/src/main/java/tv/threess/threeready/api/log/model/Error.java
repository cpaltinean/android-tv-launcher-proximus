/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.log.model;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.io.Serializable;

import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.log.helper.ErrorType;

/**
 * This class will store the defined error types, which can be shown to the ui and add to the database
 *
 * @author Paul
 * @since 2017.06.16
 */
public class Error implements Serializable {

    private final ErrorType mErrorType;
    private final String mCustomCode;
    private final String mCustomTitle;
    private final String mCustomMessage;
    private final String mTitleTranslationKey;
    private final String mBodyTranslationKey;
    private final ErrorHandler mErrorHandler;
    private final ErrorDetail mErrorDetail;

    private Error(ErrorType errorType, ErrorDetail errorDetail, String customCode, String customTitle,
                  String customMessage, String titleTranslationKey, String bodyTranslationKey, ErrorHandler errorHandler) {
        mErrorType = errorType;
        mErrorDetail = errorDetail;
        mCustomCode = customCode;
        mCustomTitle = customTitle;
        mCustomMessage = customMessage;
        mTitleTranslationKey = titleTranslationKey;
        mBodyTranslationKey = bodyTranslationKey;
        mErrorHandler = errorHandler;
    }

    public ErrorType getErrorType() {
        return mErrorType;
    }

    public String getTitleTranslationKey() {
        return mTitleTranslationKey;
    }

    public String getBodyTranslationKey() {
        return mBodyTranslationKey;
    }

    /**
     * Returns the custom error code when it's available.
     * Or the error code form the generic type.
     */
    public String getErrorCode() {
        if (!TextUtils.isEmpty(mCustomCode)) {
            return mCustomCode;
        }

        return mErrorType.getCode();
    }

    public String getErrorTitle() {
        return mCustomTitle;
    }

    /**
     * Custom message which should be displayed for the error.
     * If available this will displayed instead of the generic error message of the given error type.
     */
    @Nullable
    public String getCustomMessage() {
        return mCustomMessage;
    }

    /**
     * Returns the domain of the error.
     * e.g. Player, Backend, App etc.
     */
    public ErrorType.Domain getDomain() {
        return mErrorType.getDomain();
    }

    public ErrorHandler getErrorHandler() {
        return mErrorHandler;
    }

    /**
     * Returns extra information and parameters about the error.
     */
    @Nullable
    public ErrorDetail getErrorDetail() {
        return mErrorDetail;
    }

    @Override
    public String toString() {
        return "Error{" +
                "mErrorType=" + mErrorType +
                ", mCustomCode='" + mCustomCode + '\'' +
                ", mCustomMessage='" + mCustomMessage + '\'' +
                ", mErrorHandler=" + mErrorHandler +
                ", mErrorDetail=" + mErrorDetail +
                '}';
    }

    /**
     * Builder class for errors.
     * At least the error type most be provided.
     */

    public static class Builder {
        private final ErrorType mErrorType;

        private String mCustomCode;
        private String mCustomMessage;
        private String mCustomTitle;
        private String mTitleTranslationKey;
        private String mBodyTranslationKey;

        private ErrorHandler mErrorHandler;
        private ErrorDetail mErrorDetail;

        public Builder(ErrorType type) {
            // Default values.
            mErrorType = type;
            mErrorHandler = ErrorHandler.GENERIC;
            mCustomCode = type == null ? null : type.getCode();
        }

        public Builder(ErrorDetail detail) {
            this(detail.errorType());
            mBodyTranslationKey = detail.bodyTranslationKey();
            mTitleTranslationKey = detail.titleTranslationKey();
            mErrorDetail = detail;
        }

        public Builder backendException(BackendException backendException) {
            mCustomCode = String.valueOf(backendException.getErrorCode());
            mCustomMessage = backendException.getErrorMessage();
            return this;
        }

        public Builder bodyTranslationKey(String value) {
            mBodyTranslationKey = value;
            return this;
        }

        public Builder titleTranslationKey(String value) {
            mTitleTranslationKey = value;
            return this;
        }

        public Builder customCode(String value) {
            mCustomCode = value;
            return this;
        }

        public Builder customMessage(String value) {
            mCustomMessage = value;
            return this;
        }

        public Builder customTitle(String value) {
            mCustomTitle = value;
            return this;
        }

        public void errorHandler(ErrorHandler value) {
            mErrorHandler = value;
        }

        public Error build() {
            return new Error(mErrorType, mErrorDetail, mCustomCode, mCustomTitle, mCustomMessage, mTitleTranslationKey, mBodyTranslationKey, mErrorHandler);
        }
    }
}
