/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.playback.model;

/**
 * Domain model representing and opened OTT streaming session.
 *
 * The model contains the bookmark position to be saved.
 *
 * @author Barabas Attila
 * @since 2018.06.22
 */
public interface IOttStreamingSession extends IBaseStreamingSession {

    /**
     * @return Playback url.
     * E.g URL of the dash/m3u8 manifest file.
     */
    String getUrl();

    /**
     * @return the mime type of the returned stream URL.
     * E.g application/dash+xml
     */
    String getMimeType();

    /**
     * @return The DRM licence url.
     */
    String getLicenseUrl();

    /**
     * Check if the time is included in the streaming session
     * @param time The time in milliseconds which needs to be verified
     * @return True if the time is included in the session, false otherwise.
     */
    boolean isTimeIncluded(long time);

    /**
     * @return The UTC start time of the streaming session.
     */
    long getStartTime();

    /**
     * @return The UTC end time of the streaming session.
     */
    long getEndTime();
}
