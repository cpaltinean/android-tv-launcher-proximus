package tv.threess.threeready.api.home.model.page;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.menu.MenuItem;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.log.UILog;

/**
 * Describes a modules or static page in the application.
 *
 * @author Barabas Attila
 * @since 5/14/21
 */
public interface PageConfig extends UIPageReportProperties, Serializable {

    String HOME_MENU_ITEM_ID = "HOME";
    String EXPLORE_MENU_ITEM_ID = "V7_Explore";
    String LIVE_TV = "LIVE_TV";
    String MY_VIDEOS_MENU_ITEM_ID = "MY_VIDEOS";
    String MODULE_TABNAVIGATION_MOVIES_FOR_RENT = "MODULE_TABNAVIGATION_MOVIES_FOR_RENT";
    String MODULE_TABNAVIGATION_BOUQUET = "MODULE_TABNAVIGATION_BOUQUET";
    String APPS_MENU_ITEM_ID = "APPS_GAMES";

    /**
     * @return Unique identifier of the page.
     */
    String getId();

    /**
     * @return The title of the page.
     * Can be a simple string or a translation key.
     * @see tv.threess.threeready.ui.generic.utils.TranslationKey
     */
    String getTitle();

    /**
     * @return The type of the page.
     * @see PageType
     */
    PageType getType();

    /**
     * @return The identifier of the parent container page.
     * Returns null if the page is not a sub-page.
     */
    @Nullable
    default String getParent() {
        return null;
    }

    /**
     * @return A list with all containing dynamic modules. {@link ModuleConfig )
     * Only available for modular pages. {@link PageType#MODULAR_PAGE}}
     */
    @Nullable
    default List<ModuleConfig> getModules() {
        return null;
    }

    /**
     * @return The unique identifier of the hint config
     * which should be displayed when the page opened for the first time.
     */
    @Nullable
    default String getHint() {
        return null;
    }

    /**
     * @return The layout config which should be applied on the page.
     * It holds the color and icon set configurations.
     */
    @Nullable
    default LayoutConfig getLayoutConfig() {
        return null;
    }

    /**
     * @return page name for ui report
     */
    default UILog.Page getPageName() {
        switch (getId()) {
            case HOME_MENU_ITEM_ID:
            case MY_VIDEOS_MENU_ITEM_ID:
            case MODULE_TABNAVIGATION_MOVIES_FOR_RENT:
            case MODULE_TABNAVIGATION_BOUQUET:
                return UILog.Page.Home;
            case LIVE_TV:
                return UILog.Page.MainMenu;
            case APPS_MENU_ITEM_ID:
                return UILog.Page.ApplicationFullContentPage;
            case EXPLORE_MENU_ITEM_ID:
                return UILog.Page.ExplorePage;
            default:
                return UILog.Page.StripeLinkPage;
        }
    }

    /**
     * @return The configured module of the page.
     */
    @Nullable
    default ModuleConfig getLastModule() {
        List<ModuleConfig> modules = getModules();
        if (modules == null || modules.isEmpty()) {
            return null;
        }

        return modules.get(modules.size() - 1);
    }

    /**
     * @return A list all sub-menu items in the page.
     */
    @Nullable
    default List<MenuItem> getSubMenus() {
        return null;
    }

    /**
     * @return True if the page has sub-pages.
     */
    default boolean hasSubPages() {
        return false;
    }

    /**
     * @return True if the page has modules.
     */
    default boolean hasModules() {
        List<ModuleConfig> modules = getModules();
        return modules != null && !modules.isEmpty();
    }

    /**
     * @return True if the page doesn't have sub-pages or modules.
     */
    default boolean isEmpty() {
        return !hasSubPages() && !hasModules();
    }
}
