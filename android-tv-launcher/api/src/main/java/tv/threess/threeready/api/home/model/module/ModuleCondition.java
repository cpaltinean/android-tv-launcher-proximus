package tv.threess.threeready.api.home.model.module;

import java.io.Serializable;

/**
 * Describe a condition which needs to fulfil for a module or editorial item to be displayed.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public interface ModuleCondition extends Serializable {

    /**
     * @return Enum represent the method trough the fulfilment can be verified.
     */
    ModuleConditionMethod getMethod();

    /**
     * @param key The key of the filter id.
     * @return The filter parameter with the given key.
     */
    String getFilter(String key);
}
