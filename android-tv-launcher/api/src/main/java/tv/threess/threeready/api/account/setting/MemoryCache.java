/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.account.setting;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountCache;

/**
 * Simple memory cache stored in content provider, to be accessible using a context,
 * even from different processes when needed.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.12.07
 */

public enum MemoryCache implements SettingProperty, ReadableSetting {
    StartedAfterBoot,
    LastUserActivity,

    NavigationSessionStart,
    NavigationSessionCounter,
    LastOpenedPage;

    private final AccountCache.MemoryCache sMemoryCache = Components.get(AccountCache.MemoryCache.class);

    @Override
    public String getName() {
        return name();
    }

    @Override
    public String getValue() {
        return sMemoryCache.getMemoryCache(this);
    }

    /**
     * @return Editor to update the value of the key in the cache.
     */
    public SettingEditor<MemoryCache> edit() {
        return new SettingEditor<>(this, sMemoryCache::putMemoryCache);
    }
}
