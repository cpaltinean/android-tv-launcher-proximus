package tv.threess.threeready.api.vod.model;

import java.util.List;

import tv.threess.threeready.api.generic.model.IContentItem;

/**
 * Basic interface for vod related content.
 *
 * @author Andor Lukacs
 * @since 10/2/18
 */
public interface IBaseVodItem extends IContentItem {

    int LAST_CHANCE_DAYS = 7;

    /**
     * @return True VOD can be purchased.
     */
    boolean isTVod();

    /**
     * @return True if the VOD is part of a subscription.
     */
    boolean isSVod();

    /**
     * @return True if the VOD is included in the user's subscription.
     */
    boolean isSubscribed();

    /**
     * @return True if the VOD has at least one variant which is available for rent.
     */
    boolean isRentable();

    /**
     * @return True if the user is already rented at least one of the variants.
     */
    boolean isRented();

    /**
     * @return True if the variant is free.
     */
    boolean isFree();

    /**
     * @return The time in millis when the last rental expires.
     */
    long getRentalPeriodEnd();

    /**
     * @return The time available from the rental period in millis.
     */
    default long getRentalRemainingTime() {
        return getRentalPeriodEnd() - System.currentTimeMillis();
    }

    String getItemTitle();

    String getGroupTitle();

    String getSeriesTitle();

    String getGroupId();

    /**
     * @return List of variant id which belongs to the VOD.
     */
    List<String> getVariantIds();

    /**
     * @return The default pain variant of the VOD.
     */
    IVodVariant getMainVariant();

    /**
     * @param variantId The id of the variant ot search for
     * @return True if the given variant is included in the VOD, false otherwise.
     */
    default boolean isVariantIncluded(String variantId) {
        return getVariantIds().contains(variantId);
    }

    String getSuperGenre();

    int getInternalId();

    boolean isHD();

    IVodPrice getCheapestPrice();

    boolean isLastChance();

    boolean isNew();
}
