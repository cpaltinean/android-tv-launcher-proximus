package tv.threess.threeready.api.generic.model;

import androidx.annotation.Nullable;

/**
 * Wrapper class for the next binge watching episode.
 *
 * @author Zsolt Bokor_
 * @since 2020.05.25
 */

public class BingeWatchingInfo<TSeries extends IBaseSeries, TContentItem extends IContentItem> {
    private final TSeries mSeries;
    private final TContentItem mNextEpisode;

    public BingeWatchingInfo(TSeries series, TContentItem nextEpisode) {
        mSeries = series;
        mNextEpisode = nextEpisode;
    }

    @Nullable
    public TSeries getSeries() {
        return mSeries;
    }

    @Nullable
    public TContentItem getNextEpisode() {
        return mNextEpisode;
    }

    @Override
    public String toString() {
        return "BingeWatchingInfo{" +
                "mSeries=" + mSeries +
                ", mNextEpisode=" + mNextEpisode +
                '}';
    }
}
