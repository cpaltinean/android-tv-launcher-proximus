package tv.threess.threeready.api.netflix;

import java.util.List;

/**
 * Entity class representing a netflix group item.
 * We need the group title and size for setting the Label titles in the Netflix Stripe.
 * Each group contains Netflix tiles that are used to display Netflix cards.
 *
 * @author David
 * @since 28.02.2022
 */
public interface INetflixGroup {

    /**
     * @return title of the netflix discovery group.
     */
    String getTitle();

    /**
     * @return A list of netflix tiles in the group.
     */
    List<INetflixTile> getItems();
}
