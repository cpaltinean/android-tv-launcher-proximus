package tv.threess.threeready.api.tv.model;

import tv.threess.threeready.api.generic.model.IImageSource;

/**
 * BroadcastImageSource entity held in the DB
 *
 * @author Denisa Trif
 * @since 19.04.2019
 */
public class ImageSource implements IImageSource {

    private final String mUrl;
    private final int mWidth;
    private final int mHeight;
    private final Type mType;

    public ImageSource() {
        this("", null, 0, 0);
    }

    public ImageSource(String url, Type type) {
        this(url, type, 0, 0);
    }

    public ImageSource(String url, Type type, int width, int height) {
        mUrl = url;
        mType = type;
        mWidth = width;
        mHeight = height;
    }

    @Override
    public String getUrl() {
        return mUrl;
    }

    @Override
    public int getHeight() {
        return mHeight;
    }

    @Override
    public int getWidth() {
        return mWidth;
    }

    public Type getType() {
        return mType;
    }
}
