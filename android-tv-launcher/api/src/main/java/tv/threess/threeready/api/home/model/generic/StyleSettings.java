package tv.threess.threeready.api.home.model.generic;

import java.io.Serializable;

/**
 * Button, font and content marker style settings.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public interface StyleSettings extends Serializable {

    /**
     * @return Enum which represents the style of the buttons which needs to be displayed in the app.
     */
    ButtonStyle getButtonStyle();

    /**
     * @return Enum which represent the fonts which needs to be used in the app.
     */
    FontStyle getFontStyle();

    /**
     * @return Enum which represents the style of the content markers which needs to be used in the app.
     */
    ContentMarkerStyle getContentMarkerStyle();
}
