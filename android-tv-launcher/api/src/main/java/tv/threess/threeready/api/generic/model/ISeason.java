/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.generic.model;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

/**
 * Generic content container which holds the information about the season
 * and a list with all available episodes in the season.
 *
 * @author Barabas Attila
 * @since 2018.11.20
 */
public interface ISeason<ContentItem extends IContentItem> extends Serializable {

    default String getSeasonTitle() {
        return null;
    }

    /**
     * @return The number of the season.
     */
    @Nullable
    Integer getSeasonNumber();

    /**
     * @return A list with all available episodes in the season.
     */
    List<ContentItem> getEpisodes();
}
