package tv.threess.threeready.api.config.model;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Component;

/**
 * Playback related config options.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public interface PlaybackSettings extends Component, Serializable {

    /**
     * @param unit The time unit to return the results in.
     * @return The maximum time to wait for IPTV playback start.
     */
    long getIpTvPlaybackTimeout(TimeUnit unit);

    /**
     * @param unit The time unit to return the results in.
     * @return The maximum time to wait for OTT playback start.
     */
    long getOttPlaybackTimeout(TimeUnit unit);

    /**
     * @param unit The time unit to return the results in.
     * @return The time with which delay the buffering event dispatching to the UI.
     */
    long getPlaybackBufferingEventDelay(TimeUnit unit);

    /**
     * @param unit The time unit to return the results in.
     * @return The time with which delay the weak signal event dispatching to the UI.
     */
    long getWeakSignalEventDelay(TimeUnit unit);

    /**
     * @param unit The time unit to return the results in.
     * @return The time to display the binge watching dialog before the end the playback.
     */
    long getBingeWatchingDisplayTime(TimeUnit unit);

    /**
     * @return The minimum time for the program start for start over to be available.
     */
    int getMinimumPlaybackStartSeconds();

    /**
     * @return The OTT playback buffer size.
     */
    int getBufferSize();

    /**
     * @return True if the playback needs to start with the lower quality.
     */
    boolean isLowestInitTrack();

    /**
     * @return True if adaptive bitrate is enabled.
     */
    boolean isAbrOn();

    /**
     * @return The minimum amount of that data is expected to be buffered before a quality increase is permitted.
     */
    long getMinDurationQualityIncrease();

    /**
     * @return The maximum amount of data that is expected to be buffered before a quality decrease is permitted.
     */
    long getMaxDurationQualityDecrease();

    /**
     * @return Duration of the buffer to retain after discarding chunks e.g.
     */
    long getMinDurationRetainAfterDiscard();

    /**
     * @return Time in milliseconds that will be used
     * by the buffer controls to determine if the buffer needs to be filled for trick play playback.
     */
    int getLowMediaTimeTrickPlay();

    /**
     * @return Time in milliseconds that will be used
     * by the buffer controls to determine if the buffer can be drained for trick play playback.
     */
    int getHighMediaTimeTrickPlay();

    /**
     * @return Time in milliseconds that will be used
     * by the buffer controls to determine if the buffer needs to be filled.
     */
    int getLowMediaTime();

    /**
     * @return Time in milliseconds that will be used
     * by the buffer controls to determine if the buffer can be drained.
     */
    int getHighMediaTime();

    /**
     * @return the maximum bitrate of the stream we need to set in the bandwidth filter configuration
     */
    int getMaximumLimitedBitrate();

    /**
     * @return the minimum value for the VQE control bandwidth which marks whether we
     */
    int getControlMinimum();

    /**
     * @return Flag indicating whether the buffer should be drained (until minPlaybackStartMs is reached) or not.
     */
    boolean isDrainWhileCharging();

    /**
     * @return The minium number of segments observed before buffer degradation detection is permitted.
     */
    int getMinDegradationSamples();

    /**
     * @return The maximum number of times to retry the manifest download in case of failure.
     */
    int getManifestRetryCount();

    /**
     * @return The time to wait between manifest download retries.
     */
    long getManifestRetryDelay();

    /**
     * @return The maximum number of times to retry the segment download in case of failure.
     */
    int getSegmentRetryCount();

    /**
     * @return The time to wait between segment download retries.
     */
    long getSegmentRetryDelay();

    /**
     * @return The manifest download HTTP connection timeout in millis.
     */
    int getManifestConnectTimeout();

    /**
     * @return The manifest download HTTP read timeout in millis.
     */
    int getManifestReadTimeout();

    /**
     * @return THe segment download HTTP connection timeout in millis.
     */
    int getSegmentConnectTimeout();

    /**
     * @return THe segment download HTTP read timeout in millis.
     */
    int getSegmentReadTimeout();

    /**
     * @return THe DRM licence download HTTP connection timeout in millis.
     */
    int getDrmConnectionTimeout();

    /**
     * @return THe DRM licence download HTTP read timeout in millis.
     */
    int getDrmReadTimeout();

    /**
     * @return The total maximum time to wait for DRM licence acquisition in millis.
     */
    int getDrmAcquisitionTimeout();

    /**
     * @return The prime time of the day.
     */
    PrimeTime getPrimeTime();

    /**
     * @return The delay between the stream and live.
     */
    int getLiveEdgeLatencyMs();

    /**
     * @param unit The time unit to return the results in.
     * @return The minimum seeking speed on seek bar.
     */
    long getMinimumSeekingSpeed(TimeUnit unit);

    /**
     * @param unit The time unit to return the results in.
     * @return The maximum seeking speed with fast forward in the playback.
     */
    long getMaximumSeekingSpeed(TimeUnit unit);

    /**
     * @param unit The time unit to return the results in.
     * @return The maximum seeking speed with fast rewind in the playback.
     */
    long getMaximumSeekingSpeedRewind(TimeUnit unit);

    /**
     * @return The percentage from the content duration
     * to which jump in the playback on a single fast forward or rewind press.
     */
    int getSinglePressJumpFactorPercentage();

    /**
     * @param unit The time unit to return the results in.
     * @return The time to jump in the playback with Left button presses.
     */
    long getSkipBack(TimeUnit unit);

    /**
     * @param unit The time unit to return the results in.
     * @return The time to jump in the playback with Right button presses.
     */
    long getSkipForward(TimeUnit unit);

    /**
     * @return The sampling rate on the long Left/Right button press for jump.
     */
    int getLongPressSkipFactor();

    /**
     * @param unit The time unit to return the results in.
     * @return The delay before displaying the loading spinner when the playback starts.
     */
    long getSpinnerDelayTimeout(TimeUnit unit);


    /**
     * Interface represents the prime time of the day.
     */
    interface PrimeTime extends Serializable {

        int getHour();

        int getMinutes();
    }
}
