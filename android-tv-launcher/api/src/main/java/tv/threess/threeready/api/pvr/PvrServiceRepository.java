/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.pvr;

import tv.threess.lib.di.Component;

/**
 * Extension methods that are invoked from the tv service to take project specific actions.
 *
 * @author Denisa Trif
 * @since 2018.09.14
 */
public interface PvrServiceRepository extends Component {

    /**
     * Update single and series recording cache.
     * @throws Exception In case of errors
     */
    void updateRecordings() throws Exception;

}
