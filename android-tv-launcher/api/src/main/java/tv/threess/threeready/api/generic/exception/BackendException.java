package tv.threess.threeready.api.generic.exception;

import android.text.TextUtils;

import java.io.IOException;

import tv.threess.threeready.api.BuildConfig;

/**
 * Thrown when an backend error is encountered.
 *
 * @author Daniel Gliga
 * @since 2017.06.26
 */
public class BackendException extends IOException {

    private final String mErrorCode;
    private final String mErrorMessage;
    private final String mRequestUrl;

    public BackendException(BackendException cause) {
        this(cause.mErrorCode, cause.mErrorMessage);
        this.initCause(cause);
    }

    public BackendException(String errorMessage) {
        this("", errorMessage);
    }

    public BackendException(int errorCode, String errorMessage) {
        this(String.valueOf(errorCode), errorMessage);
    }

    public BackendException(String errorCode, String errorMessage) {
        this(errorCode, errorMessage, null);
    }

    public BackendException(int errorCode, String errorMessage, String requestUrl) {
        this(String.valueOf(errorCode), errorMessage, requestUrl);
    }

    public BackendException(String errorCode, String errorMessage, String requestUrl) {
        mErrorCode = errorCode;
        mErrorMessage = errorMessage;
        mRequestUrl = requestUrl;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }

    public String getErrorCode() {
        return mErrorCode;
    }

    @Override
    public String getMessage() {
        StringBuilder result = new StringBuilder()
                .append("code: ").append(mErrorCode)
                .append(", message: ").append(mErrorMessage);

        if (BuildConfig.DEBUG && !TextUtils.isEmpty(mRequestUrl)) {
            result.append("\nRequest url: ").append(mRequestUrl);
        }
        return result.toString();
    }
}
