/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.startup;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Utility class to help building the startup step flow.
 * Only allows adding new steps.
 *
 * @author Barabas Attila
 * @since 2018.06.27
 */
public class StepQueueBuilder {

    private final LinkedList<Step> mStepList = new LinkedList<>();

    public StepQueueBuilder addStep(Step step) {
        mStepList.add(step);
        return this;
    }

    public Queue<Step> build() {
        return mStepList;
    }
}
