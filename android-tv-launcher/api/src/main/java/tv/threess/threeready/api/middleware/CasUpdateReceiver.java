package tv.threess.threeready.api.middleware;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.helper.ArrayUtils;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Component which receives the cas update status change.
 *
 * @author Karetka Mezei Zoltan
 * @since 2022.10.26
 */
public class CasUpdateReceiver implements Component {
    private final List<WeakReference<Listener>> mListeners = new CopyOnWriteArrayList<>();

    private volatile boolean mStateChanged = false;

    /**
     * Register a listener which will be notified when the cas state changes.
     */
    public void addStateChangedListener(Listener listener) {
        removeStateChangedListener(listener);
        mListeners.add(new WeakReference<>(listener));
    }

    /**
     * Remove the previously registered cas state change listener.
     */
    public void removeStateChangedListener(Listener listener) {
        mListeners.removeIf((item) -> {
            Listener ref = item.get();
            return ref == null || ref == listener;
        });
    }

    /**
     * Remove the previously registered cas state change listener.
     */
    public void notifyStateChanged() {
        mStateChanged = true;
        ArrayUtils.notifyAndCleanup(mListeners, Listener::onStateChanged);
    }

    public boolean isStateChanged() {
        return mStateChanged;
    }

    public interface Listener {
        void onStateChanged();
    }
}
