/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.log.helper;

/**
 * The type of the error.
 * It must contain the domain to identify the area from where there error was thrown,
 * and might also contain an error code to identify specific error cases.
 *
 * @author Barabas Attila
 * @since 2017.08.29
 */

public enum ErrorType {

    PLAYER(Domain.PLAYER, null),

    APP(Domain.APP, null),

    MIDDLEWARE(Domain.MIDDLEWARE, null),

    BACKEND(Domain.BACKEND, null),

    ENTITLEMENT(Domain.ENTITLEMENT, null),

    PROVISIONING(Domain.PROVISIONING, "A015"),

    INTERNET_CABLE_DISCONNECTED(Domain.NETWORK, "N001"),
    INTERNET_CABLE_CONNECTED(Domain.NETWORK, "N002"),
    INTERNET_NOT_AVAILABLE(Domain.NETWORK, "N003"),
    INTERNET_AVAILABLE(Domain.NETWORK, "N004"),
    INTERNET_OFFLINE_MODE(Domain.NETWORK, "N005");

    private final Domain mDomain;
    private final String mCode;

    ErrorType(Domain domain, String code) {
        mDomain = domain;
        mCode = code;
    }

    public String getCode() {
        return mCode;
    }

    public Domain getDomain() {
        return mDomain;
    }

    /**
     * The domain from where the error was thrown.
     */
    public enum Domain {
        PLAYER,
        APP,
        NETWORK,
        MIDDLEWARE,
        BACKEND,
        ENTITLEMENT,
        PROVISIONING,
        UNKNOWN
    }
}
