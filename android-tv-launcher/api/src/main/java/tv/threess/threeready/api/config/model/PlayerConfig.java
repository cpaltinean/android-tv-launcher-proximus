package tv.threess.threeready.api.config.model;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Component;

/**
 * Player related config options.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public interface PlayerConfig extends Component, Serializable {

    /**
     * Get the time that is needed to seek from the beginning of the program to the end in seconds.
     * This value is used to calculate the long press jump factor and the speed of the trickplay
     * playback.
     *
     * Note that this value is constant on all of the streams, no matter the duration, so the
     * actual speed and jump value will be dynamic, based on the duration of the playback.
     */
    long getSeekToEndDuration(TimeUnit unit);

    /**
     * @param unit The time unit to return the value in.
     * @return The minimum time to wait from a program start before allowing start over on it.
     */
    long getStartOverAppearingDelay(TimeUnit unit);

    /**
     * The maximum try to try to append/prepend the playlist after failure.
     */
    int getMaxPlaylistExtensionRetries();

    /**
     * @param unit The time unit to get the result in.
     * @return The time before the playlist end to try to append it.
     */
    long getPlaylistUpdateRetryMargin(TimeUnit unit);
}
