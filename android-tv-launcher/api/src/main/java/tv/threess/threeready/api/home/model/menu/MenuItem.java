package tv.threess.threeready.api.home.model.menu;

import java.io.Serializable;

import tv.threess.threeready.api.home.model.page.UIPageReportProperties;

/**
 * Class representing a menu item in the application.
 *
 * @author Barabas Attila
 * @since 5/14/21
 */
public interface MenuItem extends UIPageReportProperties, Serializable {

    /**
     * @return The title of the menu item.
     */
    String getTitle();

    /**
     * @return Unique identifier of the menu item.
     */
    String getId();


}
