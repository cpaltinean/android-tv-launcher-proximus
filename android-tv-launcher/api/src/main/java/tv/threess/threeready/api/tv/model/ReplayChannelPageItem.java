package tv.threess.threeready.api.tv.model;

import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.home.model.page.PageConfig;

/**
 * Entity class which holds the basic channel information and the page configuration.
 * Use this item for the replay channel cards.
 *
 * @author David Bondor
 * @since 4.28.2022
 */
public class ReplayChannelPageItem implements IBaseContentItem {

    private final TvChannel mChannel;
    private final PageConfig mPageConfig;

    public ReplayChannelPageItem(TvChannel channel, PageConfig pageConfig) {
        mChannel = channel;
        mPageConfig = pageConfig;
    }

    public TvChannel getTvChannel() {
        return mChannel;
    }

    public PageConfig getPageConfig() {
        return mPageConfig;
    }

    @Override
    public String getId() {
        return mChannel.getId();
    }
}
