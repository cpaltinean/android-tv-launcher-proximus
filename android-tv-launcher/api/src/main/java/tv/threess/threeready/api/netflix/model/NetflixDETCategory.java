package tv.threess.threeready.api.netflix.model;

import android.text.TextUtils;

/**
 * Enum class used for the potential Netflix DET categories.
 * If the category is DEFAULT we will send the DET request without setting the category.
 *
 * @author David
 * @since 16.03.2022
 */
public enum NetflixDETCategory {
    KIDS,
    MOVIES,
    TV_SHOWS,
    DOCUMENTARIES,
    DEFAULT;

    public static NetflixDETCategory findByName(String name) {
        if (TextUtils.isEmpty(name)) {
            return DEFAULT;
        }

        for (NetflixDETCategory category : NetflixDETCategory.values()) {
            if (category.name().equalsIgnoreCase(name)) {
                return category;
            }
        }

        return DEFAULT;
    }
}