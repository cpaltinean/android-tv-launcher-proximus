package tv.threess.threeready.api.account.model.account;

import java.io.Serializable;
import java.util.List;

/**
 * Class which holds the account related package information.
 *
 * Created by Noemi Dali on 27.03.2020.
 */
public interface IPackage extends Serializable {

    String PACKAGE_TYPE_DTV = "DTV";

    /**
     * @return The package`s id.
     */
    String getPackageId();

    /**
     * @return The package`s external id.
     */
    String getExternalId();

    /**
     * @return The package`s start date.
     */
    long getPackageStartDate();

    /**
     * @return The package`s end date.
     */
    long getPackageEndDate();

    /**
     * @return True if the package is locked.
     */
    boolean isLocked();

    /**
     * @return true if the package is a DTV one
     */
    default boolean isDTVPackage() {
        for (String packageType : getPackageTypes()) {
            if (PACKAGE_TYPE_DTV.equals(packageType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return The package's types: DTV, VOD, WWW, etc - can be multiple ones.
     */
    List<String> getPackageTypes();

    /**
     * @return True if the package is currently subscribed.
     */
    default boolean isSubscribed() {
        long now = System.currentTimeMillis();
        return getPackageStartDate() < now && getPackageEndDate() > now && !isLocked();
    }
}
