package tv.threess.threeready.api.account.setting;

/**
 * Each setting is represented by a key, this interface allows to override how the key is used.
 */
public interface SettingProperty {
    /**
     * Return the name of the setting
     */
    String getName();
}
