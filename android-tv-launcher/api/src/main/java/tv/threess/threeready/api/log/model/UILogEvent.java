/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.log.model;

/**
 * Entity class for UI log events, e.g. PageView, OpenApp, etc
 *
 * @author Nagy Istvan
 * @since 2017.12.20
 */

public enum UILogEvent implements Event.Kind {
    PageOpen,
    PageSelection,

    Started,        // application started
    Stopped,        // application stopped

    Standby,        // screen on off
    Startup,        // the decoder is started up
    Display,        // Hdmi cable

    ButtonPress,

    AppStarted,        // launch third party app
    AppStopped,        // stop third party app
    AppUpdated,        // update third party app
    AppInstalled,      // install third party app
    AppUninstalled,    // uninstall third party app
    Search,

    // recordings
    RecordingScheduled,
    SeriesRecScheduled,

    RecordingStarted,
    RecordingStopped,

    RecordingDeleted,
    SeriesRecDeleted,

    RecordingCanceled,
    SeriesRecCanceled,

    VodRental,

    AdDownload,

    Unknown;

    public static final String DOMAIN = "UI";

    @Override
    public String domain() {
        return DOMAIN;
    }

    public enum Detail implements Event.Detail<UILogEvent> {

        //Page view related
        Page,
        OpenedBy,
        PageName,
        PageId,
        FocusName,
        FocusId,
        NeedReset,

        // Application related
        PackageName,
        StartAction,

        // Standby/Start/Stop
        ActiveState,
        RemoteConnected,
        LastKnownHeartBeat,
        StartupTime,

        // HDMI related
        HDMIPlugState,
        HDMIPlugCount,

        //Search/SearchResult related
        SearchTerm,
        SearchTermType,
        SearchType,

        //Button press from Remote or UI
        ButtonName,

        // Recording related details
        ChannelCallLetter,
        ChannelId,
        Duration,
        EpisodeNr,
        EpisodeTitle,
        ItemTitle,
        ProgramCategory,
        ProgramId,
        ProgramSubCategory,
        SeasonNr,
        SeriesTitle,
        Start,

        // Vod Related details
        CurrencyUnit,
        Genre,
        GroupTitle,
        RentalCost,
        Resolution,
        SuperGenre,
        VodExternalIdentifier,
        VodItemIdentifier,
        VodTitle,
        VodType,

        //TAD Download
        AssetId,
        AssetHandlingResult,
        AssetAttemptCounter,
        DownloadErrorCode,
        AssetHandlingDuration,
        AssetRandomTimer,
        AssetHandlingFailureCause
    }
}
