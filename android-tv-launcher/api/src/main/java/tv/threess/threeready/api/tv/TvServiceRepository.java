/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.tv;

import java.io.IOException;

import tv.threess.lib.di.Component;

/**
 * Extension methods that are invoked from the tv service to take project specific actions.
 *
 * @author Barabas Attila
 * @since 2018.04.30
 */
public interface TvServiceRepository extends Component {

    /**
     * Update the channel lineup and playback options.
     * @throws Exception In case of errors.
     */
    void updateChannels() throws Exception;

    /**
     * update the configuration files needed to playback content
     */
    void updatePlaybackConfig() throws Exception;

    /**
     * Throws an exception if the channel scan failed and there are no playback options.
     */
    void checkTIFPlaybackOptions() throws Exception;

    /**
     * Periodic EPG cache window update.
     * It will cache the broadcast inside the cache window and remove the broadcast outside of the window.
     */
    void updateBroadcasts() throws Exception;

    /**
     * pre-download: download all the ad files returned by the backend.
     * if extras have file name inside :
     * on-marker ad: download one requested ad in case it is not available as pre-downloaded ad.
     */
    void downloadAdvertisements(long trigger, String filename) throws IOException;
}
