/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.startup;

import tv.threess.threeready.api.generic.helper.TimerLog;

/**
 * Callback class to return information about the test execution result to the invoker.
 *
 * @author Barabas Attila
 * @since 2018.06.28
 */
public interface StepCallback {

    /**
     * Called when the step was executed without errors.
     */
    void onFinished();

    /**
     * Called when the step execution failed.
     */
    void onFailed();

    /**
     * Returns the startup timer instance.
     * It's used when we need to pause/resume the timer.
     */
    TimerLog getStartupTimer();
}
