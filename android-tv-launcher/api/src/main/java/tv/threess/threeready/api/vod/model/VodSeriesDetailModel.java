package tv.threess.threeready.api.vod.model;

import androidx.annotation.Nullable;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.generic.model.ISeason;

/**
 * Model used for vod series detail pages, containing series information and also the last played episode..
 *
 * @author Szende Pal
 * @since 2/7/2022.
 */
public class VodSeriesDetailModel implements IVodSeries {

    private final IBaseVodSeries mBaseSeries;
    private final List<ISeason<IBaseVodItem>> mSeasons = new CopyOnWriteArrayList<>();
    private final boolean mIsCompleted;
    private final IBaseVodItem mLastPlayedEpisode;

    public VodSeriesDetailModel(IBaseVodSeries baseSeries, List<IBaseVodItem> episodes,
                                boolean isCompleted, IBaseVodItem lastPlayedEpisode) {
        mBaseSeries = baseSeries;
        mSeasons.addAll(groupEpisodes(episodes));
        mIsCompleted = isCompleted;
        mLastPlayedEpisode = lastPlayedEpisode;
    }

    @Override
    public List<ISeason<IBaseVodItem>> getSeasons() {
        return mSeasons;
    }

    @Override
    public boolean isCompleted() {
        return mIsCompleted;
    }

    @Override
    public List<IImageSource> getImageSources() {
        return mBaseSeries.getImageSources();
    }

    @Override
    public String getId() {
        IContentItem firstEpisode = getFirstEpisode();
        if (firstEpisode == null) {
            return "";
        }

        // Return the series id from the first episode.
        return firstEpisode.getSeriesId();
    }

    @Override
    public String getTitle() {
        return mBaseSeries.getTitle();
    }

    @Override
    public int getNumberOfSeasons() {
        return mSeasons.size();
    }

    @Override
    public List<String> getSeasonIds() {
        return mBaseSeries.getSeasonIds();
    }

    @Override
    public boolean isSmallCardType() {
        return false;
    }

    @Nullable
    public IBaseVodItem getLastPlayedEpisode() {
        return mLastPlayedEpisode;
    }

    public IBaseVodSeries getBaseSeries() {
        return mBaseSeries;
    }
}
