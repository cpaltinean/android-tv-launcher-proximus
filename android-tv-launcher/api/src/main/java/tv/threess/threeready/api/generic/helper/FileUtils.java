/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.generic.helper;

import android.content.Context;
import android.os.CancellationSignal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.TimeUnit;

import android.os.StatFs;
import tv.threess.threeready.api.log.Log;

/**
 * This class contains static methods that can be used to write or read objects to file
 *
 * @author Paul Boldijar
 * @since 2017.05.09
 */

public class FileUtils {
    private static final String TAG = Log.tag(FileUtils.class);

    public static File[] EMPTY_FILE_ARRAY = new File[0];

    public static boolean delete(File dir, boolean recursive) {
        if (dir == null) {
            return false;
        }
        if (recursive && dir.isDirectory()) {
            File[] files = dir.listFiles();
            if (files == null) {
                return false;
            }
            for (File file : files) {
                if (!delete(file, recursive)) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    public static File[] list(File dir) {
        File[] result = dir.listFiles();
        if (result == null) {
            return EMPTY_FILE_ARRAY;
        }
        return result;
    }

    public static long totalMemory(String path) {
        StatFs statFs = new StatFs(path);
        return statFs.getTotalBytes();
    }

    public static long freeSpace(String path) {
        StatFs statFs = new StatFs(path);
        return statFs.getAvailableBytes();
    }

    public static long freeSpace(File path) {
        return freeSpace(path.getAbsolutePath());
    }

    public static long lastAccessed(File path) {
        try {
            BasicFileAttributes attr = Files.readAttributes(path.toPath(), BasicFileAttributes.class);
            return attr.lastAccessTime().to(TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return 0;
    }

    public static void copy(OutputStream output, InputStream input) throws IOException {
        int count;
        byte[] buffer = new byte[10240];
        while ((count = input.read(buffer)) != -1) {
            output.write(buffer, 0, count);
        }
        output.flush();
    }

    public static void copy(Writer output, Reader input) throws IOException {
        int count;
        char[] buffer = new char[10240];
        while ((count = input.read(buffer)) != -1) {
            output.write(buffer, 0, count);
        }
        output.flush();
    }

    public static File writeToFile(String text, File file) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, false))) {
            writer.write(text);
        }
        Log.v(TAG, "Written file: " + file.getAbsolutePath());

        return file;
    }

    public static String readFromFile(File file) throws IOException {
        StringWriter result = new StringWriter();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            copy(result, reader);
        }
        return result.toString();
    }

    public static File writeToFile(Context context, InputStream text, String fileName) throws IOException {
        File file = new File(context.getFilesDir(), fileName);
        writeToFile(text, file);
        return file;
    }

    public static void writeToFile(InputStream inputStream, File file) throws IOException {
        try (OutputStream outputStream = new FileOutputStream(file)) {
            copy(outputStream, inputStream);
        }
    }

    public static void closeSafe(Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        } catch (Exception e) {
            Log.e(TAG, "Failed to close closeable", e);
        }
    }

    /**
     * Safely cancel the given signal, catching and ignoring all the exceptions.
     * @param signal The signal which needs to be canceled.
     */
    public static void cancelSafe(CancellationSignal signal) {
        if (signal == null) {
            return;
        }

        try {
            signal.cancel();
        } catch (Exception e) {
            Log.e(TAG, "Failed to cancel signal", e);
        }
    }
}
