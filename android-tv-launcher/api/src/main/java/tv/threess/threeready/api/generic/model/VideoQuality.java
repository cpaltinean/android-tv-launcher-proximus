package tv.threess.threeready.api.generic.model;

import androidx.annotation.NonNull;

/**
 * Represents the video quality of an asset.
 *
 * @author Barabas Attila
 * @since 2018.07.30
 */
public enum VideoQuality {
    SD("SD"),
    HD("HD"),
    UHD_4K("UHD 4K"),
    UHD_8K("UHD 8K");

    private final String mQuality;

    VideoQuality(String quality) {
        mQuality = quality;
    }

    @NonNull
    @Override
    public String toString() {
        return mQuality;
    }
}