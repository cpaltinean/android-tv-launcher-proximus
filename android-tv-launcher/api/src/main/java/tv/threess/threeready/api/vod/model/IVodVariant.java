package tv.threess.threeready.api.vod.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.model.VideoQuality;

/**
 * Vod variant item that contains all the necessary data for a vod item.
 * Note. A VOD item can contain multiple variants, all with different metadata.
 *
 * @author Andor Lukacs
 * @since 11/8/18
 */
public interface IVodVariant extends Serializable, IContentItem {

    /**
     * @return If the variant has trailer, false otherwise.
     */
    boolean hasTrailer();

    String getItemTitle();

    String getGroupTitle();

    String getGroupId();

    String getSeriesTitle();

    /**
     * @return The list of audio language available on the variant.
     */
    List<String> getAudioLanguages();

    /**
     * @return The list of subtitle language available on the variant.
     */
    List<String> getSubtitleLanguages();

    /**
     * @return True if it's a transactional VOD variant.
     */
    boolean isTVod();

    /**
     * @return True if the VOD is part of a subscription.
     */
    boolean isSVod();

    /**
     * @return True if the user is subscribe on the VOD variant.
     */
    boolean isSubscribed();

    /**
     * @return True if the variant currently available for rent.
     */
    boolean isRentable();

    /**
     * @return True if the variant is already rented.
     */
    boolean isRented();

    /**
     * @return True if the variant is free.
     */
   boolean isFree();

    /**
     * @return The highest available video quality of the VOD variant.
     */
    VideoQuality getHighestQuality();

    /**
     * @return The time when the VOD rental expires.
     */
    long getRentalPeriodEnd();

    /**
     * @return The time when the VOD rental starts.
     */
    long getRentalPeriodStart();

    /**
     * @return The time when the VOD variant availability expires.
     */
    long getLicensePeriodEnd();

    /**
     * @return The time available from the rental period in millis.
     */
    default long getRentalRemainingTime() {
        return getRentalPeriodEnd() - System.currentTimeMillis();
    }

    /**
     * @return The parent genre of the VOD variant. (e.g. Movies, Series)
     */
    String getSuperGenre();

    /**
     * @return Identifier of the VOD variant used for internal processes.
     */
    int getInternalId();

    boolean isHD();

    boolean isNew();

    /**
     * @return True if the variant has descriptive audio.
     */
    boolean hasDescriptiveAudio();

    /**
     * @return True if the variant has descriptive subtitles.
     */
    boolean hasDescriptiveSubtitle();

    /**
     * @return True if the audio, subtitle and quality are the same.
     */
    default boolean formatEquals(IVodVariant variant) {
        if (variant == this) {
            return true;
        }

        if (variant == null) {
            return false;
        }

        List<String> audioProfiles = getAudioLanguages();

        // Check if the audio profiles are the same
        if (audioProfiles.size() == variant.getAudioLanguages().size()) {
            for (int i = 0; i < audioProfiles.size(); i++) {
                String audioProfile = audioProfiles.get(i);
                String variantAudioProfile = variant.getAudioLanguages().get(i);

                if (!Objects.equals(audioProfile.toLowerCase(),
                        variantAudioProfile.toLowerCase())) {
                    return false;
                }
            }

            // Check if the subtitles are the same
            List<String> subtitles = getSubtitleLanguages();
            List<String> variantSubtitles = variant.getSubtitleLanguages();
            if (subtitles.size() == variantSubtitles.size()) {
                for (int i = 0; i < subtitles.size(); i++) {
                    if (subtitles.get(i) != null && variantSubtitles.get(i) != null &&
                            !Objects.equals(subtitles.get(i).toLowerCase(),
                                    variantSubtitles.get(i).toLowerCase())) {
                        return false;
                    }
                }

                return getHighestQuality() == variant.getHighestQuality();
            }
        }
        return false;
    }
}
