/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.account.helper;

import android.text.TextUtils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Class will contain helpful string methods.
 *
 * @author Paul
 * @since 2017.07.07
 */

public class StringUtils {
    private static final String TAG = Log.tag(StringUtils.class);

    private static final String PRICE_FORMAT = "%.2f";

    public static String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }

    public static String notNull(String original) {
        return TextUtils.isEmpty(original) ? "" : original;
    }

    public static String getLanguageName(String languageCode) {
        Locale languageLocale = new Locale(languageCode);
        return languageLocale.getDisplayLanguage(Locale.getDefault());
    }

    public static String getCapitalizedLanguageName(String languageCode) {
        return capitalizeFirstLetter(getLanguageName(languageCode));
    }

    /**
     * Gets the formatted price.
     *
     * @param price of the item.
     * @return formatted price.
     */
    public static String formatPrice(double price) {
        return String.format(Locale.getDefault(), PRICE_FORMAT, price);
    }


    /**
     * Mark the text with HTML strike through tag (<strike>)
     * @param text The original text.
     * @return The text with the tag.
     */
    public static String addHTMLStrikeTag(String text) {
        return "<strike>" + text + "</strike>";
    }


    /**
     * Format the Jump forward and backwards string for the player.
     *
     * @param jumpValueSec Jump seconds.
     * @param translator   Translator.
     * @return The string corresponding to the jumping value. It takes minutes first (if any) than seconds (if any).
     */
    public static String formatJumpString(int jumpValueSec, Translator translator) {
        String jumpString;

        long minutes = TimeUnit.SECONDS.toMinutes(jumpValueSec);
        long seconds = jumpValueSec - TimeUnit.MINUTES.toSeconds(minutes);

        String minuteText = translator.get(TranslationKey.SCREEN_PLAYER_SKIP_MINUTES);
        String secondText = translator.get(TranslationKey.SCREEN_PLAYER_SKIP_SECONDS);

        if (minutes > 0 && seconds > 0) {
            jumpString = String.format(Locale.getDefault(), "%d %s %d %s", minutes, minuteText, seconds, secondText);
        } else if (minutes > 0) {
            jumpString = String.format(Locale.getDefault(), "%d %s", minutes, minuteText);
        } else if (seconds >= 0) {
            jumpString = String.format(Locale.getDefault(), "%d %s", seconds, secondText);
        } else {
            jumpString = "";
        }

        return jumpString;
    }


    /**
     * @param data The byte array to convert hex string.
     * @return The hex value of the by array as string.
     */
    private static String convertToHex(byte[] data) {
        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            String hex = Integer.toHexString(0xff & data[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    /**
     * @param text The text to calculate has for.
     * @return The SHA-256 hash code of the text.
     */
    public static String sha256(String text)  {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedHash = digest.digest(
                    text.getBytes(StandardCharsets.UTF_8));
            return convertToHex(encodedHash);
        } catch (Exception e) {
            Log.w(TAG, "Couldn't calculate SHA-256 hash for : " + text, e);
        }

        return "";
    }

    /**
     * Append the string builder with the item and use the separator if there are other items in builder.
     * @param builder The string builder to append on.
     * @param item The item with which append.
     * @param separator The separator between the items.
     */
    public static void join(StringBuilder builder, String item, String separator) {
        if (builder.length() != 0) {
            builder.append(separator);
        }
        builder.append(item);
    }

    /**
     * Method used for check if {@param string1} contains {@param string2}
     */
    public static boolean containsIgnoreCase(String string1, String string2) {
        String s1 = string1.toLowerCase();
        String s2 = string2.toLowerCase();
        return s1.contains(s2);
    }
}
