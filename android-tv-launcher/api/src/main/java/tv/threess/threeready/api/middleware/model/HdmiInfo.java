package tv.threess.threeready.api.middleware.model;

public class HdmiInfo {
    private final boolean connected;
    private final boolean hdcpActive;
    private final byte[] rawEdid;
    private final byte[] extendedEdid;

    public HdmiInfo(boolean connected, byte[] rawEdid, byte[] extendedEdid, boolean hdcpActive) {
        this.connected = connected;
        this.rawEdid = rawEdid;
        this.extendedEdid = extendedEdid;
        this.hdcpActive = hdcpActive;
    }

    public boolean isConnected() {
        return connected;
    }

    public byte[] getRawEdid() {
        return rawEdid;
    }

    public byte[] getExtendedEdid() {
        return extendedEdid;
    }

    public boolean isHdcpActive() {
        return hdcpActive;
    }
}
