/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.tv;

import android.content.OperationApplicationException;
import android.os.RemoteException;

import androidx.annotation.WorkerThread;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.EpgFetchObservable;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.PlaybackOption;
import tv.threess.threeready.api.tv.model.TvAdReplacement;
import tv.threess.threeready.api.tv.model.TvChannel;

/**
 * Implementation of the specific TV repository methods generally serving UI components.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.05.04
 */
public interface TvRepository extends Component {

    /**
     * @return True if the channel of the given id is considered adult.
     */
    boolean isAdultChannel(String channelId);

    /**
     * execute the vast callback when the given event occur.
     */
    @WorkerThread
    void trackAdReplacement(TvAdReplacement mAdReplacement, TvAdReplacement.Event event);

    /**
     * execute the vast callback when the given event occur.
     */
    @WorkerThread
    void trackAdReplacement(TvAdReplacement replacement, TvAdReplacement.Error error);

    /**
     * This method will synchronise the favorite channels with the local db and with the the BEP.
     */
    @WorkerThread
    void synchronizeFavoriteChannels(Set<String> addChannel, Set<String> removeList) throws OperationApplicationException, RemoteException, IOException;

    /**
     * Start a service for cache the broadcast inside the cache window and remove the broadcast outside of the window.
     */
    void startBroadcastUpdate();

    /**
     * update the list of advertisements to be downloaded.
     */
    @WorkerThread
    TvAdReplacement getAdReplacement(String channelId, double duration, String segmentDesc) throws IOException;

    /**
     * Rx observable to get the last viewed position and the duration of a broadcast.
     * The observable will emit the position & duration each time when it changes.
     *
     * @param broadcast     The broadcast for which the last viewed position & duration should be returned.
     * @param continueWatch Flag that is used to decide which bookmark should be returned
     * @return Rx observable to subscribe on recording as the viewed position changes.
     */
    Observable<IBookmark> getBookmarkForBroadcast(IBroadcast broadcast, boolean continueWatch);

    /**
     * Rx observable to get the last viewed position and the duration of a broadcast.
     * The observable will emit the position & duration each time when it changes. This is used for the recording broadcast only.
     *
     * @param broadcast The broadcast for which the last viewed position & duration should be returned.
     * @return Rx observable to subscribe on recording as the viewed position changes.
     */
    Observable<IBookmark> getRecordingBookmarkForBroadcast(IBroadcast broadcast);

    /**
     * Rx observable to get the last viewed position and the duration of a broadcast.
     * The observable will emit the position & duration each time when it changes.
     *
     * @param broadcast The broadcast for which the last viewed position & duration should be returned.
     * @return Rx observable to subscribe on recording as the viewed position changes.
     */
    Observable<IBookmark> getReplayBookmarkForBroadcast(IBroadcast broadcast);

    /**
     * Add a playback bookmark for replay content asynchronously.
     *
     * @param broadcast The replay broadcast to add bookmark for.
     * @param position  The position of the bookmark relative to the start of the content.
     * @param duration  The duration of the content in millis.
     */
    Completable addReplayBookmark(IBroadcast broadcast, long position, long duration);

    /**
     * Add a playback bookmark for recording content asynchronously.
     *
     * @param recording The recording broadcast to add bookmark for.
     * @param position  The position of the bookmark relative to the start of the content.
     * @param duration  The duration of the content in millis.
     */
    Completable addRecordingBookmark(IRecording recording, long position, long duration);

    /**
     * Returns two lists of channels: favorite and not favorite.
     * Favorites are order by favorite position and not favorites by channel position
     */
    Observable<Map<TvChannelCacheType, List<TvChannel>>> getEpgChannels();

    /**
     * Returns a broadcast Observable for one EPG channel.
     */
    EpgFetchObservable<List<IBroadcast>> getEpgGridBroadcasts(TvChannel channel, List<IBroadcast> programs, boolean isAdultChannel, long windowStart, long windowEnd);

    /**
     * Method used for perform the channel scan.
     */
    Single<Boolean> scanChannels();

    /**
     * Get the package id for an application type channel.
     */
    Observable<String> getAppChannelPackageId(final String channelId);

    /**
     * Returns the last played channel id in case it changed.
     */
    Observable<String> getLastPlayedChannelIdChange();

    /**
     * @return The upcoming broadcasts between the given time parameters.
     */
    Observable<ModuleData<IBroadcast>> getUpcomingBroadcastsBetween(String channelId, long from, long to, boolean playable, ModuleConfig config);

    /**
     * @return A Broadcast find by id.
     */
    Observable<IBroadcast> getBroadcast(String broadcastId);

    /**
     * @return Broadcast details for the given base broadcast.
     */
    Observable<IBroadcast> getBroadcast(IBroadcast broadcast);

    /**
     * @param broadcast The broadcast for which the next should be returned.
     * @return Rx observable which emits the broadcast next to the given one.
     */
    Observable<IBroadcast> getNextBroadcast(IBroadcast broadcast);

    /**
     * @param broadcast The broadcast for which the previous should be returned.
     * @return Rx observable which emits the broadcast previous to the given one.
     */
    Observable<IBroadcast> getPreviousBroadcast(IBroadcast broadcast);

    /**
     * @return Rx completable which waits until the channel list is synchronized with backend.
     */
    Completable waitForChannelUpdate();

    /**
     * @return Rx completable which waits until the epg data is synchronized with backend.
     */
    Completable waitForBroadcastUpdate();

    /**
     * @return Tv channel found by channel number.
     */
    Observable<TvChannel> getChannelByNumber(int number);

    /**
     * @return The first favorite Tv Channel from channel list.
     */
    Observable<TvChannel> getFirstFavorite();

    /**
     * Provides an RX observable which returns all available baby channel.
     */
    Observable<List<TvChannel>> getBabyChannels();

    /**
     * @return The Channels for EPG Grid.
     */
    Observable<Map<TvChannelCacheType, List<TvChannel>>> getEpgGridChannels();

    /**
     * Used to fetch the broadcasts between 2 timestamps, it will get from local database if possible.
     */
    EpgFetchObservable<List<IBroadcast>> getEpgBroadcastsBetweenTimestamps(TvChannel channel,
                                                                   long from,
                                                                   long to,
                                                                   long windowStartLength,
                                                                   long windowEndLength,
                                                                   String dummyLoadingProgramTitle,
                                                                   String dummyNoInfoProgramTitle);

    /**
     * Used to fetch the broadcasts between 2 timestamps, it will get from local database if possible.
     */
    EpgFetchObservable<List<IBroadcast>> getEpgBroadcastsBetweenTimestamps(TvChannel channel,
                                                                   long from,
                                                                   long to,
                                                                   String dummyLoadingProgramTitle,
                                                                   String dummyNoInfoProgramTitle);

    /**
     * @param channelId     id of the channel
     * @param onlyAvailable if true exclude currently unavailable options (e.g. if no internet then exludes IPTV and OTT options)
     * @return playbackOptions list of a channel from database
     */
    Observable<List<PlaybackOption>> getPlaybackOptions(final String channelId, boolean onlyAvailable);
}
