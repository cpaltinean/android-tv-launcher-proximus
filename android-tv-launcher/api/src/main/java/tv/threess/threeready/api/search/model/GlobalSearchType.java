/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.search.model;

/**
 * Search types for google assistant.
 *
 * @author Barabas Attila
 * @since 2019.01.30
 */
public enum GlobalSearchType {
    Channel,
    Program,
    Movie
}
