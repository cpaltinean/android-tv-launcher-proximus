/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.playback.player;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Domain (or world) classification for playback closely related to the different API used.
 *
 * @author Dan Titiu
 * @since 11.02.2014
 */
public enum PlaybackDomain implements Parcelable {

    /**
     * Default domain for non-active playback
     */
    None(true, "None"),

    /**
     * Playback of Dvb-C Live TV stream.
     * This domain is {@link #exclusive exclusive}.
     */
    LiveTvDvbC("DVB-C"),

    /**
     * Playback of Dvb-T Live TV stream.
     * This domain is {@link #exclusive exclusive}.
     */
    LiveTvDvbT("DVB-T"),

    /**
     * Playback of IpTv Live TV stream.
     * This domain is {@link #exclusive exclusive}.
     */
    LiveTvIpTv("IPTV"),

    /**
     * Playback of Ott Live TV stream.
     * This domain is {@link #exclusive exclusive}.
     */
    LiveTvOtt("OTT Live"),

    /**
     * Playback of Ott Replay streams.
     * This domain is {@link #exclusive exclusive}.
     */
    Replay("Replay"),

    /**
     * Playback of radio streams.
     * This domain is {@link #exclusive exclusive}.
     */
    RadioOtt("RadioOtt"),

    /**
     * Playback of radio streams.
     * This domain is {@link #exclusive exclusive}.
     */
    RadioIpTv("RadioIpTv"),

    /**
     * Playback of recording streams.
     * This domain is {@link #exclusive exclusive}.
     */
    Recording("Recording"),

    /**
     * Playback of Vod streams.
     * This domain is {@link #exclusive exclusive}.
     */
    Vod("VOD"),

    /**
     * Playback of trailer streams
     * This domain is {@link #exclusive exclusive}.
     */
    Trailer("Trailer"),

    /**
     * Playback of GDPR videos.
     * This domain is {@link #exclusive exclusive}.
     */
    Gdpr("Gdpr"),

    /**
     * Playback domain for applications.
     * This domain is {@link #exclusive exclusive}.
     */
    App("App"),

    /**
     * Playback domain for trickplay.
     */
    TrickPlay(false, "TrickPlay");

    /**
     * Describes a playback domain as exclusive or not. Exclusive playback is not allowed during another exclusive
     * playback. Defaults to true when not specified in constructor.
     */
    public final boolean exclusive;

    public final String name;

    //
    // PARCELABLE SPECIFIC FIELDS AND METHODS
    //

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name());
    }

    public static final Parcelable.Creator<PlaybackDomain> CREATOR = new Parcelable.Creator<PlaybackDomain>() {
        @Override
        public PlaybackDomain createFromParcel(final Parcel source) {
            return PlaybackDomain.valueOf(source.readString());
        }

        @Override
        public PlaybackDomain[] newArray(final int size) {
            return new PlaybackDomain[size];
        }
    };

    //
    // PRIVATE METHODS AND FIELDS
    //

    PlaybackDomain(String name) {
        this.exclusive = true;
        this.name = name;
    }

    PlaybackDomain(boolean exclusive, String name) {
        this.exclusive = exclusive;
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
