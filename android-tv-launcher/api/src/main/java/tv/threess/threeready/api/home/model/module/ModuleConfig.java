package tv.threess.threeready.api.home.model.module;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.api.home.model.page.UIPageReportProperties;

/**
 * Configuration class for a user interface module.
 * A module is an independent element which can be placed on a page.
 *
 * @author Barabas Attila
 * @since 5/14/21
 */
public interface ModuleConfig extends UIPageReportProperties, Serializable {

    /**
     * @return The id of module or null if it`s not defined.
     */
    @Nullable
    default String getId() {
        return null;
    }

    /**
     * @return The title of the module or null if it's not defined.
     */
    @Nullable
    String getTitle();

    /**
     * @return The type of the module. E.g Stripe or collection.
     */
    ModuleType getType();

    /**
     * @return Thye type of data displayed in the module.
     */
    ModuleDataType getDataType();

    /**
     * @return The variant of the cards for which the module data needs to be displayed.
     */
    ModuleCardVariant getCardVariant();

    /**
     * @return A list of editorial items displayed in the module.
     */
    default List<EditorialItem> getEditorialItems() {
        return Collections.emptyList();
    }

    /**
     * @return The style of the editorial items inside the module.
     * The individual items can override the module style.
     */
    @Nullable
    default ModuleEditorialStyle getModuleStyle() {
        return null;
    }

    /**
     * @return The action which needs to be executed
     * when the user click and an editorial item in the module
     * The individual items can override the action defined for the module.
     */
    @Nullable
    default EditorialItemAction getEditorialItemAction() {
        return null;
    }

    /**
     * @return Data source which identifies the service and parameters to load the data from.
     */
    @Nullable
    default ModuleDataSource getDataSource() {
        return null;
    }

    /**
     * @return A list of condition which needs to be fulfilled before the module loads.
     */
    @Nullable
    default ModuleCondition[] getConditions() {
        return null;
    }

    /**
     * @return An array with the selector options needs to be applied on the data source.
     */
    default List<SelectorOption> getSelectorList() {
        return null;
    }

    /**
     * @param defaultSize Fallback value in case the the size is missing.
     * @return THe maximum number of items which needs to be displayed in the module.
     */
    default int getSize(int defaultSize) {
        return defaultSize;
    }

    /**
     * @return The minimum number of elements needed for the module to be displayed.
     */
    default int getMinElements() {
        return 1;
    }

    /**
     * @return Parameters which describes the module pagination behavior.
     */
    default ModuleDataSourcePaginationParams getPagination() {
        return null;
    }

    /**
     * @return The resource file name of the icon for the module.
     */
    default String getIcon() {
        return null;
    }

    /**
     * @return True if the configured module has a search term filter.
     */
    default boolean hasSearchTerm() {
        if (getDataSource() != null && getDataSource().getParams() != null) {
            ModuleDataSourceParams params = getDataSource().getParams();
            return params.hasFilter(ModuleFilterOption.Mixed.SearchTerm.NAME);
        }
        return false;
    }
}
