package tv.threess.threeready.api.generic.model;

/**
 * Types for items that can be added to the watchlist.
 * Currently contains:
 *  - programs (incl. recordings)
 *  - vod
 *
 * @author Andor Lukacs
 * @since 16/10/2020
 */
public enum WatchlistType {
    Broadcast,
    Vod
}
