package tv.threess.threeready.api.log.model;

import java.io.Serializable;

/**
 * Enum for playback item type.
 *
 * @author Zsolt Bokor_
 * @since 2020.07.14
 */
public enum PlaybackItemType implements Serializable {
    SINGLE,
    SERIES,

    UNDEFINED
}
