package tv.threess.threeready.api.tv.model;

import androidx.annotation.NonNull;

/**
 * Wrapper class which holds information about a channel and the currently running broadcast.
 *
 * @author Barabas Attila
 * @since 2018.03.06
 */

public class ZappingInfo {
    private final TvChannel mChannel;
    private IBroadcast mBroadcast;
    private final boolean mIsFromFavorites;

    public ZappingInfo(TvChannel channel, IBroadcast broadcast, boolean isFromFavorites) {
        mChannel = channel;
        mBroadcast = broadcast;
        mIsFromFavorites = isFromFavorites;
    }

    @NonNull
    public TvChannel getChannel() {
        return mChannel;
    }

    @NonNull
    public IBroadcast getBroadcast() {
        return mBroadcast;
    }

    public boolean isFromFavorites() {
        return mIsFromFavorites;
    }

    public void setBroadcast(@NonNull IBroadcast broadcast) {
        mBroadcast = broadcast;
    }
}
