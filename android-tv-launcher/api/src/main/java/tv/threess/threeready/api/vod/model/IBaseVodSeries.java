package tv.threess.threeready.api.vod.model;

import androidx.annotation.NonNull;

import java.util.List;

import tv.threess.threeready.api.generic.model.IBaseSeries;

/**
 * API model representing a VOD series.
 *
 * @author Barabas Attila
 * @since 2020.04.20
 */
public interface IBaseVodSeries extends IBaseSeries {

    /**
     * @return A list with unique identifier for each season in the series.
     */
    @NonNull
    List<String> getSeasonIds();

    /**
     * @return wether the card should be displayed as a small category card, or normal type card.
     */
    boolean isSmallCardType();
}
