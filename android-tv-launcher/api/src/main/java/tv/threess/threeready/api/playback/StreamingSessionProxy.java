/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.playback;

import java.io.IOException;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.playback.model.IOttStreamingSession;
import tv.threess.threeready.api.playback.model.IRecordingOttStreamingSession;
import tv.threess.threeready.api.playback.model.IReplayOttStreamingSession;
import tv.threess.threeready.api.playback.model.IVodOttStreamingSession;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;

/**
 * Proxy (pass-through) interface to access playback session related backend data.
 *
 * @author Barabas Attila
 * @since 2017.09.28
 */
public interface StreamingSessionProxy extends Component {
    /**
     * Open streaming session for live OTT playback.
     *
     * @param channelId The identifier of the channel for which the session should be opened.
     * @return The streaming session which holds the URL and DRM information.
     */
    IOttStreamingSession openChannelStreamingSession(String channelId) throws IOException;

    /**
     * Open streaming session for replay playback
     *
     * @param broadcast The broadcast for which the session should be opened.
     * @return The streaming session which holds the URL and DRM information.
     */
    IReplayOttStreamingSession openReplayStreamingSession(IBroadcast broadcast) throws IOException;

    /**
     * Open streaming session for replay playback previous to the currently opened session.
     *
     * @param currentSession The currently opened session.
     * @return The continuous/gap-free/overlap-free session previous to the currently opened.
     */
    IReplayOttStreamingSession openPreviousReplayStreamingSession(IReplayOttStreamingSession currentSession) throws IOException;

    /**
     * Open streaming session for replay playback next to the currently opened session.
     *
     * @param currentSession The currently opened session.
     * @return The continuous/gap-free/overlap-free session next to the currently opened.
     */
    IReplayOttStreamingSession openNextReplayStreamingSession(IReplayOttStreamingSession currentSession) throws IOException;

    /**
     * Open streaming session for recording playback
     *
     * @param recording The recording for which the session should be opened.
     * @return The streaming session which holds the URL and DRM information.
     */
    IRecordingOttStreamingSession openRecordingStreamingSession(IRecording recording) throws IOException;

    /**
     * Open streaming session for VOD playback.
     *
     * @param vodVariant The VOD variant for which the streaming session should be opened.
     * @param vod        The parent VOD of the variant.
     * @return The streaming session which holds the URL and DRM information.
     */
    IVodOttStreamingSession openVodStreamingSession(IVodItem vod, IVodVariant vodVariant) throws IOException;

    /**
     * Open streaming session for VOD trailer playback.
     *
     * @param vodVariant The VOD variant for which the streaming session should be opened.
     * @param vod        The parent VOD of the variant.
     * @return The streaming session which holds the URL and DRM information.
     */
    IOttStreamingSession openTrailerStreamingSession(IVodItem vod, IVodVariant vodVariant) throws IOException;

    /**
     * Close a previously opened streaming session.
     *
     * @param streamingSession The session which should be closed.
     */
    void closeStreamingSession(IOttStreamingSession streamingSession);

}
