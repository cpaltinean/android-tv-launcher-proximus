/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.startup;

/**
 * Interfaces which represents one step in the startup flow.
 * A step can display screens, dialogs and communicate with the backend.
 *
 * @author Barabas Attila
 * @since 2018.06.27
 */
public interface Step {

    /**
     * Execute the current step.
     *
     * @param callback - callback to provide information about the execution status.
     */
    void execute(StepCallback callback);


    /**
     * Called when the startup flow stopped.
     * All the ongoing requests and animation should be canceled here.
     */
    default void cancel() {
    }

}
