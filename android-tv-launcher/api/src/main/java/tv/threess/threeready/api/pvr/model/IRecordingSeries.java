package tv.threess.threeready.api.pvr.model;

import androidx.annotation.Nullable;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import tv.threess.threeready.api.generic.model.ISeason;
import tv.threess.threeready.api.tv.model.ITvSeries;

/**
 * Recording series container. Holds all the recording episodes of a series.
 *
 * @author Barabas Attila
 * @since 2018.11.26
 */
public interface IRecordingSeries extends ITvSeries<IRecording>, IBaseRecordingSeries {

    /**
     * @return The first playable episode from the series,
     * or null if there is no such episode.
     */
    @Nullable
    default IRecording getFirstPlayableEpisode() {
        for (ISeason<IRecording> season : getSeasons()) {
            for (IRecording episode : season.getEpisodes()) {
                if (episode.isRecordingFinished() && episode.canWatchRecording()) {
                    return episode;
                }
            }
        }
        return null;
    }

    @Override
    default List<IRecording> getEpisodesSorted() {
        return ITvSeries.super.getEpisodesSorted().stream().filter(e -> e.isPast() || e.isLive()).collect(Collectors.toList());
    }

    @Override
    default Comparator<IRecording> getSortComparator() {
        if (!hasNullEpisodeNumber()) {
            return ITvSeries.super.getSortComparator();
        }

        return (episodeLeft, episodeRight) -> {
            // Compare seasons.
            int seasonCompare = Comparator.nullsLast(Integer::compareTo)
                    .compare(episodeLeft.getSeasonNumber(), episodeRight.getSeasonNumber());

            if (seasonCompare == 0) {
                if (episodeLeft.getSeasonNumber() == null) {
                    //Sort the episodes by airing time.
                    return Long.compare(episodeLeft.getStart(), episodeRight.getStart());
                }
                // Seasons are equal. Compare episodes.
                return Comparator.nullsLast(Integer::compareTo)
                        .compare(episodeLeft.getEpisodeNumber(), episodeRight.getEpisodeNumber());
            }

            return seasonCompare;
        };
    }

    /**
     * @return True if in the episodes without season, there is at least one episode without episode number.
     */
    default boolean hasNullEpisodeNumber() {
        for (ISeason<IRecording> season : getSeasons()) {
            if (season.getSeasonNumber() == null) {
                for (IRecording episode : season.getEpisodes()) {
                    if (episode.getEpisodeNumber() == null) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @return True if the series has an episode which is live and partially recorded.
     */
    default boolean hasPartiallyRecordedLiveEpisode() {
        for (ISeason<IRecording> season : getSeasons()) {
            for (IRecording episode : season.getEpisodes()) {
                if (episode.isLive()
                        && episode.getRecordingEnd() < System.currentTimeMillis()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return True if the series has at least one completed recording.
     */
    default boolean hasCompletedEpisode() {
        for (ISeason<IRecording> season : getSeasons()) {
            for (IRecording episode : season.getEpisodes()) {
                if (episode.getRecordingEnd() < System.currentTimeMillis()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return True if the series has at least one ongoing episode.
     */
    default boolean hasOnGoingEpisode() {
        return getOnGoingEpisode() != null;
    }

    /**
     * @return If it is not null then returns ongoing episode.
     */
    default IRecording getOnGoingEpisode() {
        for (ISeason<IRecording> season : getSeasons()) {
            for (IRecording episode : season.getEpisodes()) {
                if (episode.getRecordingStart() < System.currentTimeMillis()
                        && episode.getRecordingEnd() > System.currentTimeMillis()) {
                    return episode;
                }
            }
        }
        return null;
    }

    /**
     * @return True if the series has at least one scheduled episode.
     */
    default boolean hasScheduledEpisode() {
        for (ISeason<IRecording> season : getSeasons()) {
            for (IRecording episode : season.getEpisodes()) {
                if (episode.getRecordingStart() > System.currentTimeMillis()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return True if the series has at least one live or future episode.
     */
    default boolean hasFutureOrLiveEpisode() {
        for (ISeason<IRecording> season : getSeasons()) {
            for (IRecording episode : season.getEpisodes()) {
                if (episode.getEnd() > System.currentTimeMillis()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return The number of available episodes in the series.
     */
    @Override
    default int getNumberOfRecordings() {
        return getNumberOfEpisodes();
    }
}


