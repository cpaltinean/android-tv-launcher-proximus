package tv.threess.threeready.api.middleware;

import android.os.Handler;
import android.os.Looper;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.log.Log;

/**
 * Component which receives and dispatches the system update status changes.
 *
 * @author Barabas Attila
 * @since 2020.09.11
 */
public abstract class SystemUpdateReceiver implements Component {
    private final String TAG = Log.tag(getClass());

    private static final Handler sMainHandler = new Handler(Looper.getMainLooper());
    private final List<WeakReference<Listener>> mListeners = new CopyOnWriteArrayList<>();

    /**
     * Needs to be called to start receiving the system update status changes.
     */
    public void registerReceiver() {}

    /**
     * Should be called when the system update status changes are not needed anymore.
     */
    public void unRegisterReceiver() {}

    /**
     * Add a listener to get notified when the system update status changes.
     * @param listenerToAdd The listener which needs to be registered.
     */
    public void addStateChangedListener(Listener listenerToAdd) {
        removeStateChangedListener(listenerToAdd);
        mListeners.add(new WeakReference<>(listenerToAdd));
    }

    /**
     * Remove a previously registered update status change listener.
     * @param listenerToRemove The previously registered listener which needs to be removed.
     */
    public void removeStateChangedListener(Listener listenerToRemove) {
        mListeners.removeIf((item) -> {
            Listener listener = item.get();
            return listener == null || listener == listenerToRemove;
        });
    }

    public void notifyUpdateCheckTriggered(boolean manualCheck) {
        Log.d(TAG, "Notify update check triggered. Manual check : " + manualCheck);
        ArrayUtils.notifyAndCleanup(mListeners, sMainHandler,
                listener -> listener.onUpdateCheckTriggered(manualCheck));
    }

    protected void notifyUpdateAvailable(UpdateUrgency urgency, boolean manualCheck) {
        Log.d(TAG, "Notify update available. Urgency : "
                + urgency + ", manual check : " + manualCheck);
        ArrayUtils.notifyAndCleanup(mListeners, sMainHandler,
                listener -> listener.onUpdateAvailable(urgency, manualCheck));
    }

    protected void notifyNoUpdateAvailable(boolean manualCheck) {
        Log.d(TAG, "Notify no update available. Manual check : " + manualCheck);
        ArrayUtils.notifyAndCleanup(mListeners, sMainHandler,
                listener -> listener.onNoUpdateAvailable(manualCheck));
    }

    protected void notifyUpdateCheckFailed(boolean manualCheck) {
        Log.d(TAG, "Notify update check failed. Manual check : " + manualCheck);
        ArrayUtils.notifyAndCleanup(mListeners, sMainHandler,
                listener -> listener.onUpdateCheckFailed(manualCheck));
    }

    protected void notifyUpdateDownloadFailed(UpdateUrgency urgency, boolean manualCheck) {
        Log.d(TAG, "Notify update download failed. Urgency : "
                + urgency + ", manual check : " + manualCheck);
        ArrayUtils.notifyAndCleanup(mListeners, sMainHandler,
                listener -> listener.onUpdateDownloadFailed(urgency, manualCheck));
    }

    protected void notifyUpdateDownload(UpdateUrgency urgency, boolean manualCheck) {
        Log.d(TAG, "Notify update downloaded. Urgency : " + urgency + ", manual check : " + manualCheck);
        ArrayUtils.notifyAndCleanup(mListeners, sMainHandler,
                listener -> listener.onUpdateDownload(urgency, manualCheck));
    }

    /**
     * Listener to get notification when the system update status changes.
     */
    public interface Listener {

        /**
         * Called when a new system update check was triggered.
         * @param manualCheck True if the system update check was triggered by the user.
         */
        default void onUpdateCheckTriggered(boolean manualCheck) {
        }

        /**
         * Called when there is a new system update available on the server.
         * @param urgency The urgency of the system update.
         * @param manualCheck True if the system update check was triggered by the user.
         */
        default void onUpdateAvailable(UpdateUrgency urgency, boolean manualCheck) {
        }

        /**
         * Called when there is no new system update available on the server.
         * @param manualCheck True if the system update check was triggered by the user.
         */
        default void onNoUpdateAvailable(boolean manualCheck) {
        }

        /**
         * Called when the checking the system update on the server fails.
         * @param manualCheck True if the system update check was triggered by the user.
         */
        default void onUpdateCheckFailed(boolean manualCheck) {
        }

        /**
         * Called when downloading of the new system update fails.
         * @param urgency The urgency of the system update.
         * @param manualCheck True if the system update check was triggered by the user.
         */
        default void onUpdateDownloadFailed(UpdateUrgency urgency, boolean manualCheck) {
        }

        /**
         * Called when the new system update image was successfully download and reboot needed to apply it.
         * @param urgency The urgency of the system update.
         * @param manualCheck True if the system update check wa triggered by the user.
         */
        default void onUpdateDownload(UpdateUrgency urgency, boolean manualCheck) {
        }
    }
}
