/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.home.model.module;

/**
 * Define all the valid methods for conditional stripe / tile configurations.
 */
public enum ModuleConditionMethod {
    SUBSCRIBED,
    NOT_SUBSCRIBED,
    BACKEND_AVAILABLE,
    INTERNET_AVAILABLE,
    ONLY_BACKEND_AVAILABLE
}
