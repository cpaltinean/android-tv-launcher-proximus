package tv.threess.threeready.api.account.model.service;

/**
 * Enum with the possible values of the consent request scenario query parameter.
 *
 * @author Zsolt Bokor_
 * @since 2020.10.22
 */
public enum ConsentRequestScenario {
    REGULAR_BOOT,
    WAKE_UP_FROM_STANDBY,
    NEW_SW_INSTALLED,
    FIRST_TIME_INSTALL,
    SETTING_SCREEN
}


