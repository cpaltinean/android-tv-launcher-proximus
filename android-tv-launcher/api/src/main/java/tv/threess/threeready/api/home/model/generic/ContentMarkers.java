package tv.threess.threeready.api.home.model.generic;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.Collection;
import java.util.EnumSet;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.home.model.module.MarkerType;

/**
 * Configuration for content markers (color, background, transparency and priority).
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public interface ContentMarkers extends Component, Serializable {
    /**
     * @param markerType The type of the content marker.
     * @return Content marker config option for the given type.
     */
    @Nullable
    ContentMarkerDetails getContentMarkerDetails(MarkerType markerType);

    /**
     * @param markerTypes A list of content marker types to choose the highest priority from.
     * @return The config for content marker which has the highest priority
     */
    @Nullable
    default ContentMarkerDetails getHighestPriorityContentMarker(Collection<MarkerType> markerTypes) {
        ContentMarkerDetails highPrioContentMarker = null;

        for (MarkerType markerType : markerTypes) {
            ContentMarkerDetails marker = getContentMarkerDetails(markerType);
            if (marker == null) {
                continue;
            }

            if (highPrioContentMarker == null
                    || highPrioContentMarker.getPriority() > marker.getPriority()) {
                highPrioContentMarker = marker;
            }
        }
        return highPrioContentMarker;
    }

    /**
     * Filter the available content markers based on where are displayed.
     */
    enum TypeFilter {
        All(EnumSet.allOf(MarkerType.class)),
        MiniEpg(EnumSet.of(MarkerType.NOW, MarkerType.NOW_PLAYING, MarkerType.NEXT, MarkerType.LATER, MarkerType.RADIO)),
        Epg(EnumSet.of(MarkerType.NOW, MarkerType.NOW_PLAYING, MarkerType.NEXT, MarkerType.LATER, MarkerType.RADIO)),
        Cards(EnumSet.of(MarkerType.NOW, MarkerType.NOW_PLAYING, MarkerType.NEW, MarkerType.PLANNED, MarkerType.DELETE_SOON, MarkerType.FREE, MarkerType.PRICE, MarkerType.SUBSCRIBE,
                MarkerType.LAST_CHANCE, MarkerType.INCLUDED, MarkerType.RENTED)),
        PlayOptions(EnumSet.of(MarkerType.NOW, MarkerType.NOW_PLAYING, MarkerType.RENTED, MarkerType.INCLUDED, MarkerType.SUBSCRIBE)),
        Player(EnumSet.of(MarkerType.NOW, MarkerType.RENTED, MarkerType.INCLUDED, MarkerType.SUBSCRIBE, MarkerType.RADIO));

        final EnumSet<MarkerType> markers;

        TypeFilter(EnumSet<MarkerType> markers) {
            this.markers = markers;
        }

        public EnumSet<MarkerType> getMarkers() {
            return markers;
        }
    }
}
