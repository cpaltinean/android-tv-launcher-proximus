package tv.threess.threeready.api.home;

import java.io.IOException;

import tv.threess.lib.di.Component;

/**
 * Handles home and client config service related business logic.
 *
 * @author Barabas Attila
 * @since 4/20/22
 */
public interface HomeServiceRepository extends Component {

    /**
     * Update and cache the client related configurations.
     * @throws IOException In case of errors.
     */
    void updateClientConfig() throws Exception;

    /**
     * Download and update the cache with new translations.
     * @throws IOException in case of errors.
     */
    void updateCachedTranslations() throws Exception;
}
