/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.home.model.module;

/**
 * Represents the variant of a module from the given type.
 * The variant is used define small differences between module types.
 *
 * Created by Barabas Attila on 4/13/2017.
 */

public enum ModuleCardVariant {
    A1,
    A2,
    A3,
    A4,
    A5,

    B1,
    B2,
    B3,

    C1,
    C2,

    D1,
    UNDEFINED
}
