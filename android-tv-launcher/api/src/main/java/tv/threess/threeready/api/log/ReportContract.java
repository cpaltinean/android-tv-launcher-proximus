/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.log;

import android.content.ContentResolver;
import android.net.Uri;

import tv.threess.threeready.api.BuildConfig;
import tv.threess.threeready.api.generic.BaseContract;

/**
 * Contract class to access error/event logs in user database
 *
 * @author Arnold Szabo
 * @since 2017.07.19
 */
public class ReportContract {
    public static final String AUTHORITY = BuildConfig.REPORT_PROVIDER;

    public static final Uri BASE_URI = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT).authority(AUTHORITY).build();

    public static final String CALL_UPDATE_REPORTER = "CALL_UPDATE_REPORTER";

    public static final String CALL_SEND_EVENT = "CALL_SEND_EVENT";

    public static final String CALL_SEND_EVENTS = "CALL_SEND_EVENTS";

    public static final String EXTRA_REPORTER_INFO = "EXTRA_REPORTER_INFO";

    /**
     * Contract class for logging event information.
     */
    public interface Event {

        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Event.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(Event.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(ReportContract.BASE_URI, PATH);

        //
        // Columns
        //

        /**
         * Id of the event.
         */
        String ID = BaseContract._ID;


        /**
         * Id of batch reporter.
         * <P>Type: String</P>
         */
        String REPORTER_ID = "reporter_id";

        /**
         * Name of the event.
         * <P>Type: Integer</P>
         */
        String NAME = "name";

        /**
         * Type of the event.
         * <P>Type: Integer</P>
         */
        String DOMAIN = "domain";

        /**
         * Timestamp of the event.
         * <P>Type: Long</P>
         */
        String TIMESTAMP = "timestamp";

        /**
         * Serialized event.
         * <P>Type: String</P>
         */
        String VALUE = "value";

        /**
         * Incident count of the event.
         * <P>Type: Integer</P>
         */
        String INCIDENT_COUNT = "incident_count";

        /**
         * Last incident of the event.
         * <P>Type: Long</P>
         */
        String LAST_INCIDENT_TIME = "last_incident_time";

        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;
    }

    /**
     * Contract class for event reporter configuration.
     */
    public interface Reporter {

        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Reporter.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(Reporter.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(ReportContract.BASE_URI, PATH);


        //
        // Columns
        //

        /**
         * Primary key.
         * Name of the event reporter. (class name of the reporter)
         * <P>Type: String</P>
         */
        String ID = "id";

        /**
         * Endpoint for collector server. Complete url.
         * <P>Type: String</P>
         */
        String ENDPOINT = "endpoint";

        /**
         * Headers which need to be included in the HTTP request.
         * <P>Type: String</P>
         */
        String HEADERS = "headers";

        /**
         * Maximum time in millis a message remains stored locally before being permanently removed.
         * <P>Type: Integer</P>
         */
        String MAX_KEEP_PERIOD = "max_keep_period";

        /**
         * Maximum number of messages that are stored locally before the oldest is removed permanently.
         * <P>Type: Integer</P>
         */
        String MAX_KEEP_SIZE = "max_keep_size";

        /**
         * Maximum number of events/metrics that can be sent to the server in a
         * single HTTP request.
         */
        String MAX_BATCH_SIZE = "max_batch_size";

        /**
         * Batch size to send logs at once
         * in case we saved the configured amount of events, trigger the upload of the events to the backend.
         * <P>Type: Integer</P>
         */
        String BATCH_SIZE = "batch_size";

        /**
         * Batch size to send logs at once
         * after the configured amount of time spent in millis, trigger the upload of the events to the backend.
         * <P>Type: Integer</P>
         */
        String BATCH_PERIOD = "batch_period";

        /**
         * Random period in millis which is used to calculate the delay for sending.
         * <P>Type: Integer</P>
         */
        String RANDOM_PERIOD = "random_period";

        /**
         * Timeout in millis for HTTP requests.
         * <P>Type: Integer</P>
         */
        String TIMEOUT_PERIOD = "timeout_period";

        /**
         * Delay in millis for the next event sending.
         * <P>Type: String</P>
         */
        String SENDING_DELAY = "sending_delay";
    }
}
