package tv.threess.threeready.api.home.model.generic;

import java.io.Serializable;

/**
 * Application type channel properties.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public interface ChannelApp extends Serializable {
    /**
     * @return The call letter of the channel.
     */
    String getCallLetter();

    /**
     * @return The package name which identifies the app.
     */
    String getPackageName();

    /**
     * @return The subscription package to which the channel belongs.
     */
    String getSubscriptionPackage();

    /**
     * @return TVO link for the channel for ordering subscription.
     */
    String getTvoLink();

    /**
     * @return True if subscription needed before openning the app.
     */
    boolean isSubscriptionNeeded();
}
