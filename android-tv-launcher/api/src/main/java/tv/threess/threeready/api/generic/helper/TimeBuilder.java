/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.generic.helper;

import java.util.Calendar;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Helper class to manipulate timestamps.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.05.02
 */
public class TimeBuilder {

    public static final TimeZone UTC = TimeZone.getTimeZone("UTC");
    public static final TimeZone LOCAL = TimeZone.getDefault();

    private static final Random random = new Random();

    private final Calendar calendar;

    public static TimeBuilder utc() {
        return new TimeBuilder(UTC);
    }

    public static TimeBuilder utc(long timestamp) {
        return new TimeBuilder(UTC).set(timestamp);
    }

    public static TimeBuilder local(long timestamp) {
        return new TimeBuilder(LOCAL).set(timestamp);
    }

    public static TimeBuilder local() {
        return new TimeBuilder(LOCAL);
    }

    /*package*/ TimeBuilder(TimeZone zone) {
        this.calendar = Calendar.getInstance(zone);
    }

    /**
     * Returns the value as milliseconds. The value is the number of milliseconds since Jan. 1, 1970, midnight GMT.
     *
     * @return timestamp in milliseconds.
     */
    public long get() {
        return calendar.getTimeInMillis();
    }

    /**
     * Get the value for the specified field
     * for {@link TimeUnit#MILLISECONDS} returns the millis of the second
     * for {@link TimeUnit#SECONDS} returns the second of the minute
     * for {@link TimeUnit#MINUTES} returns the minute of the hour
     * for {@link TimeUnit#HOURS} returns the hour of the day
     * for {@link TimeUnit#DAYS} returns the day of the month
     *
     * @param unit the time-unit field to get.
     * @return the value of the specified field.
     */
    public int get(TimeUnit unit) {
        switch (unit) {
            default:
                throw new IllegalArgumentException("unit: " + unit.name());

            case DAYS:
                return calendar.get(Calendar.DAY_OF_MONTH);

            case HOURS:
                return calendar.get(Calendar.HOUR_OF_DAY);

            case MINUTES:
                return calendar.get(Calendar.MINUTE);

            case SECONDS:
                return calendar.get(Calendar.SECOND);

            case MILLISECONDS:
                return calendar.get(Calendar.MILLISECOND);

            case MICROSECONDS:
            case NANOSECONDS:
                // resolution too high
                return 0;
        }
    }

    /**
     * Delegated method to {@link Calendar#get(int)}
     */
    public int get(int field) {
        return calendar.get(field);
    }

    /**
     * Set the value in milliseconds.
     *
     * @param millis the time-stamp as the number of milliseconds since Jan. 1, 1970.
     * @return this reference for operation chaining.
     */
    public TimeBuilder set(long millis) {
        calendar.setTimeInMillis(millis);
        return this;
    }

    /**
     * Add the value in milliseconds.
     *
     * @param duration the duration in milliseconds to be added to the current value.
     * @return this reference for operation chaining.
     */
    public TimeBuilder add(long duration) {
        calendar.setTimeInMillis(calendar.getTimeInMillis() + duration);
        return this;
    }

    /**
     * Sets the time zone with the given time zone value.
     *
     * @param keepTime chang ethe timezone without changing the time.
     * @param timeZone the given time zone.
     * @return this reference for operation chaining.
     */
    public TimeBuilder set(boolean keepTime, TimeZone timeZone) {
        if (keepTime) {
            long time = calendar.getTimeInMillis();
            calendar.setTimeZone(timeZone);
            calendar.setTimeInMillis(time);
            return this;
        }

        calendar.setTimeZone(timeZone);
        return this;
    }

    /**
     * Sets the time zone with the given time zone value.
     *
     * @param timeZone the given time zone.
     * @return this reference for operation chaining.
     */
    public TimeBuilder set(TimeZone timeZone) {
        return set(false, timeZone);
    }

    /**
     * Sets the value of the specified field:
     * for {@link TimeUnit#MILLISECONDS} set the millis of the second
     * for {@link TimeUnit#SECONDS} set the second of the minute
     * for {@link TimeUnit#MINUTES} set the minute of the hour
     * for {@link TimeUnit#HOURS} set the hour of the day
     * for {@link TimeUnit#DAYS} set the day of the month
     *
     * @param unit the time-unit field to set.
     * @return this reference for operation chaining.
     */
    public TimeBuilder set(TimeUnit unit, int value) {
        switch (unit) {
            default:
                throw new IllegalArgumentException("unit: " + unit.name());

            case DAYS:
                calendar.set(Calendar.DAY_OF_MONTH, value);
                break;

            case HOURS:
                calendar.set(Calendar.HOUR_OF_DAY, value);
                break;

            case MINUTES:
                calendar.set(Calendar.MINUTE, value);
                break;

            case SECONDS:
                calendar.set(Calendar.SECOND, value);
                break;

            case MILLISECONDS:
                calendar.set(Calendar.MILLISECOND, value);
                break;

            case MICROSECONDS:
            case NANOSECONDS:
                // resolution too high
                break;
        }
        return this;
    }

    /**
     * Delegated method to {@link Calendar#set(int, int)}
     *
     * @return this reference for operation chaining.
     */
    public TimeBuilder set(int field, int value) {
        calendar.set(field, value);
        return this;
    }

    /**
     * Add the specified amount to a field:
     * </br>for {@link TimeUnit#MILLISECONDS} adds the amount of milliseconds
     * </br>for {@link TimeUnit#SECONDS} adds the amount of seconds
     * </br>for {@link TimeUnit#MINUTES} adds the amount of minutes
     * </br>for {@link TimeUnit#HOURS} adds the amount of hours
     * </br>for {@link TimeUnit#DAYS} adds the amount of days
     *
     * @param unit the time-unit field to set.
     * @return this reference for operation chaining.
     */
    public TimeBuilder add(TimeUnit unit, int value) {
        switch (unit) {
            default:
                throw new IllegalArgumentException("unit: " + unit.name());

            case DAYS:
                calendar.add(Calendar.DAY_OF_MONTH, value);
                break;

            case HOURS:
                calendar.add(Calendar.HOUR_OF_DAY, value);
                break;

            case MINUTES:
                calendar.add(Calendar.MINUTE, value);
                break;

            case SECONDS:
                calendar.add(Calendar.SECOND, value);
                break;

            case MILLISECONDS:
                calendar.add(Calendar.MILLISECOND, value);
                break;

            case MICROSECONDS:
            case NANOSECONDS:
                //calendar.setTimeInMillis(calendar.getTimeInMillis() + unit.toMillis(value));
                // resolution too high
                break;
        }
        return this;
    }

    /**
     * <p>Floor the current datetime, leaving the field specified as the most significant field.</p>
     * For example, if you had the date-time of 28 Mar 2002 13:45:01.231,
     * </br>if you use as unit {@link TimeUnit#DAYS}, it would return 28 Mar 2002 00:00:00.000.
     * </br>if you use as unit {@link TimeUnit#HOURS}, it would return 28 Mar 2002 13:00:00.000.
     * </br>if you use as unit {@link TimeUnit#MINUTES}, it would return 28 Mar 2002 13:45:00.000.
     *
     * @param unit Resolution of the operation.
     * @return this reference for operation chaining.
     */
    public TimeBuilder floor(TimeUnit unit) {
        switch (unit) {
            default:
                throw new IllegalArgumentException("unit: " + unit.name());

            case DAYS:
                calendar.set(Calendar.HOUR_OF_DAY, 0);
            case HOURS:
                calendar.set(Calendar.MINUTE, 0);
            case MINUTES:
                calendar.set(Calendar.SECOND, 0);
            case SECONDS:
                calendar.set(Calendar.MILLISECOND, 0);
            case MILLISECONDS:
            case MICROSECONDS:
            case NANOSECONDS:
                // resolution too high to set something
                break;
        }
        return this;
    }

    /**
     * <p>Floor the current datetime, works the same way as {@link #floor(TimeUnit)},
     * but also ensures that at the value at the given resolution is a multiple of the multiplier parameter.</p>
     * For example, if you had the date-time of 28 Mar 2002 13:46:01.231,
     *
     * </br>using {@link TimeUnit#MINUTES} resolution and 5 as multiplier, it will return 28 Mar 2002 13:45:00.000.
     * </br>using {@link TimeUnit#MINUTES} resolution and 10 as multiplier, it will return 28 Mar 2002 13:40:00.000.
     *
     * @param unit Resolution of the operation.
     * @param multiplier unit resolution multiplier.
     * @return this reference for operation chaining.
     */
    public TimeBuilder floor(TimeUnit unit, int multiplier) {
        if (multiplier == 0) {
            // avoid division by zero
            return this.floor(unit);
        }
        int round = get(unit) % multiplier;
        return this.floor(unit).add(unit, -round);
    }

    /**
     * <p>Ceil the current datetime, leaving the field specified as the most significant field.</p>
     *
     * For example, if you had the date-time of 28 Mar 2002 13:45:01.231,
     * </br>if you use as unit {@link TimeUnit#DAYS}, it would return 29 Mar 2002 00:00:00.000.
     * </br>if you use as unit {@link TimeUnit#HOURS}, it would return 28 Mar 2002 14:00:00.000.
     * </br>if you use as unit {@link TimeUnit#MINUTES}, it would return 28 Mar 2002 13:46:00.000.
     *
     * @param unit Resolution of the operation.
     * @return this reference for operation chaining.
     */
    public TimeBuilder ceil(TimeUnit unit) {
        return this.floor(unit).add(unit, 1);
    }

    /**
     * <p>Ceil the current datetime, works the same way as {@link #ceil(TimeUnit)},
     * but also ensures that at the value at the given resolution is the next multiple of the multiplier parameter.</p>
     * For example, if you had the date-time of 28 Mar 2002 13:46:01.231,
     *
     * </br>using {@link TimeUnit#MINUTES} resolution and 5 as multiplier, it will return 28 Mar 2002 13:50:00.000.
     * </br>using {@link TimeUnit#MINUTES} resolution and 10 as multiplier, it will return 28 Mar 2002 13:50:00.000.
     *
     * @param unit Resolution of the operation.
     * @param multiplier unit resolution multiplier.
     * @return this reference for operation chaining.
     */
    public TimeBuilder ceil(TimeUnit unit, int multiplier) {
        if (multiplier == 0) {
            // avoid division by zero
            return this.ceil(unit);
        }
        int round = get(unit) % multiplier;
        return this.floor(unit).add(unit, multiplier - round);
    }

    /**
     * Distribute randomly between current value and the duration at the given time-unit resolution.
     * For example, if you had the date-time of 28 Mar 2002 13:46:01.231,
     *
     * </br>using {@link TimeUnit#HOURS} resolution and 2 as duration, it will return a date between 28 Mar 2002 13:46:01.231 and 28 Mar 2002 15:46:01.231.
     *
     * @param unit  Resolution of the duration.
     * @param duration The duration of the distribution time frame, negative values are allowed.
     * @return this reference for operation chaining.
     */
    public TimeBuilder distribute(TimeUnit unit, long duration) {
        return this.add(randomDuration(unit.toMillis(duration)));
    }

    /**
     * Distribute randomly between start and end at the given time-unit resolution.
     * For example, if you had the date-time of 28 Mar 2002 13:46:01.231,
     *
     * </br>using {@link TimeUnit#HOURS} resolution and 15, 17 as start and end, it will return a date between 28 Mar 2002 15:46:01.231 and 28 Mar 2002 17:46:01.231.
     * in case you need a timestamp between 15:00:00.000 and 17:00:00.000 yo will need to use the floor method before or after this operation.
     *
     * @param unit  Resolution of the operation.
     * @param start The beginning of the distribution time frame.
     * @param end   The end of the distribution time frame.
     * @return this reference for operation chaining.
     */
    public TimeBuilder distribute(TimeUnit unit, int start, int end) {
        return this.set(unit, start).distribute(unit, end - start);
    }

    /**
     * Returns the duration between current time and the given timestamp in milliseconds.
     *
     * @return duration in milliseconds.
     */
    public long duration(long timestamp) {
        return get() - timestamp;
    }

    /**
     * Returns the absolute duration between current time and the given timestamp in milliseconds.
     *
     * @return duration in milliseconds.
     */
    public long absDuration(long timestamp) {
        return Math.abs(duration(timestamp));
    }

    /**
     * Check if the current time is equal to the timestamp at the given resolution.
     */
    public boolean equals(long timestamp, TimeUnit resolution) {
        return resolution.convert(get(), TimeUnit.MILLISECONDS) == resolution.convert(timestamp, TimeUnit.MILLISECONDS);
    }

    /**
     * Returns text representation of the timestamp to be used for debugging or logging
     */
    @Override
    public String toString() {
        return toString(TimeUnit.MILLISECONDS);
    }

    public String toString(TimeUnit precision) {
        int year = get(Calendar.YEAR);
        int month = get(Calendar.MONTH) + 1;
        int day = get(Calendar.DAY_OF_MONTH);
        int hour = get(Calendar.HOUR_OF_DAY);
        int minute = get(Calendar.MINUTE);
        int second = get(Calendar.SECOND);
        int millis = get(Calendar.MILLISECOND);
        if (precision != null) {
            switch (precision) {
                case DAYS:
                    return String.format(Locale.ROOT, "%04d.%02d.%02d", year, month, day);

                case HOURS:
                    return String.format(Locale.ROOT, "%04d.%02d.%02d-%02d", year, month, day, hour);

                case MINUTES:
                    return String.format(Locale.ROOT, "%04d.%02d.%02d-%02d:%02d", year, month, day, hour, minute);

                case SECONDS:
                    return String.format(Locale.ROOT, "%04d.%02d.%02d-%02d:%02d:%02d", year, month, day, hour, minute, second);
            }
        }
        return String.format(Locale.ROOT, "%04d.%02d.%02d-%02d:%02d:%02d.%03d", year, month, day, hour, minute, second, millis);
    }

    public static long randomDuration(long duration) {
        if (duration == 0) {
            return 0;
        }
        long value = Math.abs(random.nextLong()) % Math.abs(duration);
        return duration < 0 ? -value : value;
    }
}
