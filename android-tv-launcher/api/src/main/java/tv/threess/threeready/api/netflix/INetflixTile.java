package tv.threess.threeready.api.netflix;

import tv.threess.threeready.api.generic.model.IBaseContentItem;

/**
 * Interface for a netflix tile response configuration.
 *
 * @author David
 * @since 2022.04.13
 */
public interface INetflixTile extends IBaseContentItem {

    /**
     * @return The title of the netflix tile.
     */
    String getTitle();


    /**
     * @return  The deeplink of the netflix tile.
     *          We use this value for starting the Netflix application.
     */
    String getDeepLink();


    /**
     * @return The description of the netflix tile.
     */
    String getDescription();

    /**
     * @return  True if we have Profile or SignUp tile.
     *          False if we have VideoTile.
     */
    boolean isProfileTile();
}
