package tv.threess.threeready.api.middleware;

import android.os.PowerManager;
import tv.threess.lib.di.Component;

/**
 * Interface for the MW service to provide project specific implementations.
 *
 * @author Barabas Attila
 * @since 2020.09.21
 */
public interface MwServiceRepository extends Component {

    /**
     * Check for system update and wait for download if needed.
     * The system update time is only triggered if the current check in time exceeds
     * the next check in time coming from server.
     * @param checkInTime  The current check in time.
     * @param updateFirstTime True if the update check needs to be triggered
     *                       even when the next check in time is not defined yet.
     *                       Usually this happens at FTI or clear data.
     * @return True if an update was downloaded.
     */
    boolean checkSystemUpdate(long checkInTime, boolean updateFirstTime);

    /**
     * Reboot if the device in standby if the configured uptime is reached.
     */
    void quiescentReboot();

    /**
     * Get the wakelock to keep the box upp and running while the Mw update is in progress
     */
    PowerManager.WakeLock getWakeLock();
}
