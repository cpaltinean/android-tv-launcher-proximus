package tv.threess.threeready.api.generic.model;

/**
 * Generic content container which holds the series information
 *
 * @author Barabas Attila
 * @since 2018.11.20
 */
public interface IBaseSeries extends IBaseContentItem {

    /**
     * @return Unique identifier of the series.
     */
    String getId();

    /**
     * @return Title of the whole series.
     */
    String getTitle();

}
