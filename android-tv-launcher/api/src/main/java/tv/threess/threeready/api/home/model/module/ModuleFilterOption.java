/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.home.model.module;

/**
 * Utility interface which holds the available module data source filtering options.
 *
 * @author Daniel Gliga
 * @see ModuleDataSourceParams#getFilter(String) ()
 * @since 2017.07.26
 */

public interface ModuleFilterOption {

    /**
     * Defined filtering options for channel type.
     */
    interface Channel {

        interface Type {
            String NAME = "type";

            interface Value {
                String FAVORITE = "favorites";
                String RADIO = "radio";
                String APP = "app";
            }
        }

        interface Language {
            String NAME = "lang";
        }

        interface ChannelId {
            String NAME = "channelId";
        }

    }

    /**
     * Defined filtering options for mixed type.
     */
    interface Mixed {

        interface SwimLaneId {
            String NAME = "swimLaneId";
        }

        interface CategoryId {
            String NAME = "categoryId";
        }

        interface Favorite {
            String NAME = "favorite";
        }

        interface Genre {
            String NAME = "genre";
        }

        interface Type {
            String NAME = "type";
        }

        interface Start {
            String NAME = "start";
        }

        interface HoursExtraTime{
            String NAME = "hoursExtraTime";
        }

        interface SearchTerm {
            String NAME = "SearchTerm";
        }
    }

    /**
     * Defined filtering options for netflix type.
     */
    interface Netflix {

        interface NetflixCategory {
            String NAME = "Category";
        }

        interface NetflixInteractionId {
            String NAME = "InterActionId";
        }
    }

    /**
     * Defined filtering options for app type.
     */
    interface App {
        interface Package {
            String NAME = "package";
        }

        interface Type {
            String NAME = "type";

            interface Value {
                String FEATURED = "featured";
                String INSTALLED = "installed";
                String STORE = "store";
            }
        }
    }

    /**
     * Defined filtering options for notifications.
     */
    interface Notification {
        interface Priority {
            String NAME = "priority";

            interface Value {
                String CRITICAL = "critical";
            }
        }
    }
}
