package tv.threess.threeready.api.home.model.generic;

import android.graphics.drawable.GradientDrawable;

import java.io.Serializable;

/**
 * Interface identifies a gradient color.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public interface GradientStyle extends Serializable {
    /**
     * @return The start color of the gradient.
     */
    Integer getFromColor();

    /**
     * @return The end color of the gradient.
     */
    Integer getToColor();

    /**
     * @return The direction of from where the gradient starts and ends.
     */
    GradientDrawable.Orientation getOrientation();

    default boolean hasBackgroundColor() {
        return getFromColor() != null || getToColor() != null;
    }
}
