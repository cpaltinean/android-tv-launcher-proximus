package tv.threess.threeready.api.log.model;

import tv.threess.threeready.api.log.Log;

import java.io.Serializable;

/**
 * Event model class that contains the configuration properties only
 *
 * @author Karetka Mezei Zoltan
 * @since 2020.07.31
 */
public abstract class BaseEvent implements Serializable {
    private final long mId;
    private final String mName;
    private final String mDomain;
    private final long mTimestamp;
    private final String mValue;
    private final Log.Level mLogLevel;
    private final long mCreatedTimestamp = System.currentTimeMillis();

    protected BaseEvent(long id, String name, String domain, long timestamp, String value, Log.Level logLevel) {
        mId = id;
        mName = name;
        mDomain = domain;
        mTimestamp = timestamp;
        mValue = value;
        mLogLevel = logLevel;
    }

    /**
     * Get the id of the event.
     *
     * @return id of the event.
     */
    public long getId() {
        return mId;
    }

    /**
     * Get the name of the event.
     *
     * @return name of the event.
     */
    public String getName() {
        return mName;
    }

    /**
     * Get the domain of the event.
     *
     * @return domain of the event.
     */
    public String getDomain() {
        return mDomain;
    }

    /**
     * Get the timestamp when the event was generated.
     *
     * @return timestamp when the event was generated.
     */
    public long getTimestamp() {
        return mTimestamp;
    }

    /**
     * Get the timestamp when the entity for the event was created.
     *
     * @return timestamp when the entity for the event was created.
     */
    public long getCreatedTimestamp() {
        return mCreatedTimestamp;
    }

    /**
     * Get the serialized event.
     *
     * @return Serialized event.
     */
    public String getValue() {
        return mValue;
    }

    /**
     * Get the current events log level.
     *
     * @return log level of the event.
     */
    public Log.Level getLogLevel() {
        return mLogLevel;
    }

    /**
     * @return Enum with a given name, otherwise the default value for enum.
     */
    public <V extends Enum<V> & Event.Kind> V getName(V defValue) {
        for (V v : defValue.getDeclaringClass().getEnumConstants()) {
            if (v.name().equals(mName) && v.domain().equals(mDomain)) {
                return v;
            }
        }
        return defValue;
    }
}
