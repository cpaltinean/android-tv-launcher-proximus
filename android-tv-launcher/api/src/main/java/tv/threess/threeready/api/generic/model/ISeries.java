package tv.threess.threeready.api.generic.model;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import tv.threess.threeready.api.pc.model.ParentalRating;

/**
 * Created by Noemi Dali on 18.06.2020.
 */
public interface ISeries<TContentItem extends IContentItem> extends IBaseSeries {

    Comparator<? super IContentItem> EPISODE_COMPARATOR = (o1, o2) -> Comparator.nullsLast(Integer::compareTo)
            .compare(o1.getEpisodeNumber(), o2.getEpisodeNumber());

    Comparator<? super ISeason<? extends IContentItem>> SEASON_COMPARATOR = (o1, o2) -> Comparator.nullsLast(Integer::compareTo)
            .compare(o1.getSeasonNumber(), o2.getSeasonNumber());

    /**
     * @return The first available episode of the series.
     */
    @Nullable
    default TContentItem getFirstEpisode() {
        ISeason<TContentItem> season = getMinimumSeason();
        if (season == null || season.getEpisodes().isEmpty()) {
            return null;
        }

        return season.getEpisodes().stream()
                .min(EPISODE_COMPARATOR).orElse(null);
    }

    @Nullable
    default ISeason<TContentItem> getMinimumSeason() {
        return getSeasons().stream()
                .min(SEASON_COMPARATOR).orElse(null);
    }

    /**
     * Get all the episodes sorted.
     */
    default List<TContentItem> getEpisodesSorted() {
        List<TContentItem> episodes = new ArrayList<>();
        for (ISeason<TContentItem> season : getSeasons()) {
            episodes.addAll(season.getEpisodes());
        }
        // Sort list by season/episode ascending.
        episodes.sort(getSortComparator());

        return episodes;
    }

    /**
     * Gets comparator for sorting the list by season/episode ascending.
     */
    default Comparator<TContentItem> getSortComparator() {
        return (episodeLeft, episodeRight) -> {
            // Compare seasons.
            int seasonCompare = Comparator.nullsLast(Integer::compareTo)
                    .compare(episodeLeft.getSeasonNumber(), episodeRight.getSeasonNumber());

            if (seasonCompare == 0) {
                // Seasons are equal. Compare episodes.
                return Comparator.nullsLast(Integer::compareTo)
                        .compare(episodeLeft.getEpisodeNumber(), episodeRight.getEpisodeNumber());
            }

            return seasonCompare;
        };
    }

    /**
     * @return The number of available episodes in the series.
     */
    default int getNumberOfEpisodes() {
        int nrOfEpisodes = 0;
        for (ISeason<TContentItem> season : getSeasons()) {
            nrOfEpisodes += season.getEpisodes().size();
        }

        return nrOfEpisodes;
    }

    default ParentalRating getParentalRating() {
        IContentItem firstEpisode = getFirstEpisode();
        if (firstEpisode == null) {
            return ParentalRating.Undefined;
        }

        // Return the parental rating from the first episode.
        return firstEpisode.getParentalRating();
    }

    /**
     * @return A list with all available seasons.
     */
    List<ISeason<TContentItem>> getSeasons();

    /**
     * @return True if the series has all the seasons loaded.
     */
    boolean isCompleted();

    /**
     * @return True if the given episode is part of the series false otherwise.
     */
    default boolean isEpisodeIncluded(TContentItem episode) {
        if (getSeasons() == null || getSeasons().isEmpty() || episode == null) {
            return false;
        }

        for (ISeason<TContentItem> season : getSeasons()) {
            for (TContentItem contentItem : season.getEpisodes()) {
                if (contentItem.getId().equals(episode.getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return Number of seasons in the series.
     */
    int getNumberOfSeasons();

    /**
     * Group episode list into seasons.
     */
    default List<ISeason<TContentItem>> groupEpisodes(List<TContentItem> episodes) {
        episodes = new ArrayList<>(episodes); // Don't modify the original list.

        Map<Integer, List<TContentItem>> seasonEpisodeMap = new LinkedHashMap<>();
        for (TContentItem episode : episodes) {
            Integer seasonNumber = episode.getSeasonNumber();
            if (seasonNumber == null) {
                // No season. Will be in leftovers.
                continue;
            }

            // Add episode to season.
            seasonEpisodeMap.computeIfAbsent(seasonNumber, ArrayList::new).add(episode);
        }

        // Create season containers.
        List<ISeason<TContentItem>> seasons = new ArrayList<>();
        for (Integer season : seasonEpisodeMap.keySet()) {
            List<TContentItem> seasonEpisodes = seasonEpisodeMap.get(season);
            if (seasonEpisodes != null) {
                seasons.add(new Season<>(season, seasonEpisodes));
                episodes.removeAll(seasonEpisodes);
            }
        }

        // Create season for leftovers.
        if (!episodes.isEmpty()) {
            seasons.add(new Season<>(episodes));
        }

        seasons.sort((o1, o2) -> Comparator.nullsLast(Integer::compareTo)
                .compare(o1.getSeasonNumber(), o2.getSeasonNumber()));

        return seasons;
    }
}
