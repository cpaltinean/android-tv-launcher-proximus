package tv.threess.threeready.api.middleware.model;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidParameterException;

public class BitStream {

    private static final String HEX = "0123456789ABCDEF";

    private final byte[] data;
    private int offset;

    public BitStream(byte[] data) {
        this.offset = 0;
        this.data = data;
    }

    public BitStream(String hex) {
        this.offset = 0;
        this.data = new byte[hex.length() / 2];
        for (int i = 0; i < this.data.length; ++i) {
            int b1 = hexToByte(hex.charAt(2 * i));
            int b2 = hexToByte(hex.charAt(2 * i + 1));
            if (b1 < 0 || b1 > 15 || b2 < 0 || b2 > 15) {
                throw new InvalidParameterException("invalid value at: " + (2 * i));
            }
            this.data[i] = (byte) ((b1 << 4) | b2);
        }
    }

    public BitStream reset() {
        this.offset = 0;
        return this;
    }

    public BitStream reset(int to) {
        this.offset = to;
        return this;
    }

    public int offset() {
        return offset;
    }

    public int length() {
        return data.length;
    }

    public int readBit() {
        int bytePos = offset / 8;
        int bitPos = offset % 8;
        int result = data[bytePos] >> (7 - bitPos);
        offset += 1;
        return result & 1;
    }

    public int readU32(int bits) {
        if (bits > 32) {
            throw new IndexOutOfBoundsException("Result does not fit in its type");
        }

        int result = 0;
        while (bits > 0) {
            result <<= 1;
            result |= readBit();
            bits -= 1;
        }
        return result;
    }

    public int readI32(int bits) {
        int high = 32 - bits;
        return (readU32(bits) << high) >> high;
    }

    public long readU64(int bits) {
        if (bits > 64) {
            throw new IndexOutOfBoundsException("Result does not fit in its type");
        }

        long result = 0;
        while (bits > 0) {
            result <<= 1;
            result |= readBit();
            bits -= 1;
        }
        return result;
    }

    public long readI64(int bits) {
        int high = 64 - bits;
        return (readU64(bits) << high) >> high;
    }

    public void skipBits(int bits) {
        this.offset += bits;
        if (this.offset >= this.data.length * 8) {
            throw new IndexOutOfBoundsException("index: " + this.offset + ", length: " + (this.data.length * 8));
        }
    }

    public byte[] readBytes(int bytes) {
        byte[] result = new byte[bytes];

        if (this.offset % 8 == 0) {
            // if the offset is byte aligned speedup using byte copy
            int offset = this.offset / 8;
            for (int i = 0; i < bytes; i++) {
                result[i] = this.data[offset + i];
                this.offset += 8;
            }
            return result;
        }
        // copy bit by bit
        for (int i = 0; i < bytes * 8; i++) {
            result[i / 8] |= readBit() << (7 - (i % 8));
        }
        return result;
    }

    public String readString(int bytes, Charset charset) {
        return new String(this.readBytes(bytes), charset);
    }

    public String readString(int bytes) {
        return new String(this.readBytes(bytes), StandardCharsets.US_ASCII);
    }

    public String readHex(int bytes, String separator) {
        return toHexString(this.readBytes(bytes), separator);
    }

    public String readHex(int bytes) {
        return readHex(bytes, null);
    }

    @Override
    public String toString() {
        return toHexString(this.data, null);
    }

    private static int hexToByte(char ch) {
        if (ch >= 'a' && ch <= 'z') {
            return ch - 'a' + 10;
        }
        if (ch >= 'A' && ch <= 'Z') {
            return ch - 'A' + 10;
        }
        if (ch >= '0' && ch <= '9') {
            return ch - '0';
        }
        return -1;
    }

    private static String toHexString(byte[] bytes, String separator) {
        StringBuilder result = new StringBuilder(bytes.length * 3);
        for (byte v : bytes) {
            if (separator != null && result.length() > 0) {
                result.append(separator);
            }
            result.append(HEX.charAt((v >>> 4) & 0x0f));
            result.append(HEX.charAt(v & 0x0f));
        }
        return result.toString();
    }
}
