/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.utils;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import io.reactivex.ObservableEmitter;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

/**
 * Utility class for RX related operations.
 *
 * @author Barabas Attila
 * @since 2018.07.02
 */
public class RxUtils {

    private static final int IO_CORE_THREADS = 8;
    private static final int IO_MAX_THREADS = 16;

    /**
     * Initialize the RX schedulers.
     */
    public static void initSchedulers() {
        // Initialize the IO scheduler.
        Scheduler ioScheduler = Schedulers.from(new ThreadPoolExecutor(IO_CORE_THREADS,
                IO_MAX_THREADS, 5, TimeUnit.SECONDS, new LinkedBlockingQueue<>()) {
            @Override
            protected <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
                return new FutureTask<T>(callable) {
                    @Override
                    public boolean cancel(boolean mayInterruptIfRunning) {
                        // Disable thread interruption to avoid resource corruption
                        // in components which doesn't handle thread interruption gracefully.
                        // E.g. OKHttp cache corruption
                        return super.cancel(false);
                    }
                };
            }
        });
        RxJavaPlugins.setInitIoSchedulerHandler(schedulerCallable -> ioScheduler);
        RxJavaPlugins.setIoSchedulerHandler(scheduler -> ioScheduler);
    }

    /**
     * Dispose the given RX observables without logs.
     */
    public static void disposeSilently(Collection<Disposable> disposables) {
        for (Disposable disposable : disposables) {
            disposeSilently(disposable);
        }
    }

    /**
     * Dispose the given RX observables without logs.
     */
    public static void disposeSilently(Disposable... disposables) {
        for (Disposable disposable : disposables) {
            disposeSilently(disposable);
        }
    }

    /**
     * Dispose the given RX observable without logs.
     */
    public static void disposeSilently(Disposable disposable) {
        if (disposable != null && !disposable.isDisposed()) {
            try {
                disposable.dispose();
            } catch (Exception e) {
                // Ignore
            }
        }
    }

    /**
     * @param emitter The emitter for which the disposed state needs to be checked.
     * @return True if the emitter is null or it's already disposed.
     */
    public static boolean isDisposed(ObservableEmitter<?> emitter) {
       return emitter == null || emitter.isDisposed();
    }
}
