package tv.threess.threeready.api.tv.model;

import java.io.IOException;
import java.util.List;

import tv.threess.threeready.api.pvr.model.IRecording;

/**
 * Response entity from backend.
 *
 * Created by Noemi Dali on 14.04.2020.
 */
public interface IRecordingListResponse<T extends IRecording> {

    /**
     * @return The list of recording requested from the backend.
     */
    List<T> getRecordingList() throws IOException;
}
