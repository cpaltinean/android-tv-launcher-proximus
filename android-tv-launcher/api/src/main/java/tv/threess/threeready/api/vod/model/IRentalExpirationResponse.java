package tv.threess.threeready.api.vod.model;

import java.util.List;

/**
 * Base response class for rented vods information.
 *
 * Created by Noemi Dali on 27.04.2020.
 */
public interface IRentalExpirationResponse<T extends IRentalExpiration> {

    /**
     * @return a list with rented vods.
     */
    List<T> getRentalInfos();
}
