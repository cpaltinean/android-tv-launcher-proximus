package tv.threess.threeready.api.generic.receivers;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import tv.threess.threeready.api.log.Log;

/**
 * Base class for task which waits for a result to load synchronously.
 *
 * @author Barabas Attila
 * @since 1/15/21
 */
public abstract class WaitTask<TResult> {
    protected static final String TAG = Log.tag(WaitTask.class);

    protected final FutureTask<TResult> mFutureTask;

    protected WaitTask() {
        mFutureTask = new FutureTask<>(this::result);
    }

    protected WaitTask(FutureTask<TResult> futureTask) {
        mFutureTask = futureTask;
    }

    abstract protected TResult result();

    protected void finish() {
        Log.d(TAG, "Finished task: " + this.getClass().getCanonicalName());
        mFutureTask.run();
    }

    /**
     * Cancel the current operation if possible
     */
    public void cancel(boolean mayInterruptIfRunning) {
        Log.d(TAG, "Cancel task: " + this.getClass().getCanonicalName());
        mFutureTask.cancel(mayInterruptIfRunning);
    }

    /**
     * Block the caller until the results loads or the timeout is reaches.
     * @param timeoutMillis The maximum time in millis to wait for the result to load.
     * @return The loaded results loaded.
     */
    public TResult waitForResult(long timeoutMillis) throws InterruptedException, ExecutionException, TimeoutException {
        return mFutureTask.get(timeoutMillis, TimeUnit.MILLISECONDS);
    }

    /**
     * Returns the status of the task did it run or not
     */
    public boolean isFinished() {
        return mFutureTask.isDone();
    }
}
