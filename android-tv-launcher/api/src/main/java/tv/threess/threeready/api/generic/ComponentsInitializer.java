package tv.threess.threeready.api.generic;

import tv.threess.threeready.api.config.model.Config;

/**
 * Interface to can trigger the initialization of the components container in the application.
 *
 * @author Barabas Attila
 * @since 2020.08.31
 */
public interface ComponentsInitializer {

    /**
     * Register the necessary components and component providers for the application.
     */
    void initialize();

    /**
     * Register the config components of the application.
     * @param config The config instance from which the components needs to be registered.
     */
    void initConfigComponents(Config config);
}
