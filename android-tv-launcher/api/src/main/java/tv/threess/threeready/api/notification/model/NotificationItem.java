package tv.threess.threeready.api.notification.model;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.io.Serializable;

/**
 * Notification entity created from the NotificationContentProvider data.
 *
 * @author Eugen Guzyk
 * @since 2018.08.23
 */
public class NotificationItem implements Serializable {

    public static final int TRUE = 1;
    public static final int FALSE = 0;

    String mSbnKey;
    String mPackageName;
    String mNotifTitle;
    String mNotifText;
    int mAutoDismiss;
    int mDismissible;
    int mOngoing;
    byte[] mSmallIcon;
    int mImportance;
    int mProgress;
    int mProgressMax;
    int mFlags;
    int mHasContentIntent;
    byte[] mBigPicture;
    String mContentButtonLabel;
    String mDismissButtonLabel;
    String mTag;
    boolean mIsRead;
    NotificationType mType;

    @Override
    public boolean equals(@Nullable Object obj) {
        return obj instanceof NotificationItem && TextUtils.equals(mSbnKey, ((NotificationItem) obj).mSbnKey);
    }

    public String getSbnKey() {
        return mSbnKey;
    }

    public String getPackageName() {
        return mPackageName;
    }

    public String getNotifTitle() {
        return mNotifTitle;
    }

    public String getNotifText() {
        return mNotifText;
    }

    public boolean isAutoDismiss() {
        return mAutoDismiss == TRUE;
    }

    public boolean isDismissible() {
        return mDismissible == TRUE;
    }

    public boolean isOngoing() {
        return mOngoing == TRUE;
    }

    public byte[] getSmallIcon() {
        return mSmallIcon;
    }

    public int getImportance() {
        return mImportance;
    }

    public int getProgress() {
        return mProgress;
    }

    public int getProgressMax() {
        return mProgressMax;
    }

    public int getFlags() {
        return mFlags;
    }

    public boolean hasContentIntent() {
        return mHasContentIntent == TRUE;
    }

    public byte[] getBigPicture() {
        return mBigPicture;
    }

    public String getContentButtonLabel() {
        return mContentButtonLabel;
    }

    public String getDismissButtonLabel() {
        return mDismissButtonLabel;
    }

    public String getTag() {
        return mTag;
    }

    public NotificationType getType() {
        return mType;
    }

    public boolean isRead() {
        return mIsRead;
    }

    public void setRead() {
        mIsRead = true;
    }

    public void setUnread() {
        mIsRead = false;
    }

    public static class Builder {
        NotificationItem mNotification = new NotificationItem();

        public Builder setSbnKey(String sbnKey) {
            mNotification.mSbnKey = sbnKey;
            return this;
        }

        public Builder setPackageName(String packageName) {
            mNotification.mPackageName = packageName;
            return this;
        }

        public Builder setNotifTitle(String notifTitle) {
            mNotification.mNotifTitle = notifTitle;
            return this;
        }

        public Builder setNotifText(String notifText) {
            mNotification.mNotifText = notifText;
            return this;
        }

        public Builder setAutoDismiss(int autoDismiss) {
            mNotification.mAutoDismiss = autoDismiss;
            return this;
        }

        public Builder setDismissible(int dismissible) {
            mNotification.mDismissible = dismissible;
            return this;
        }

        public Builder setOngoing(int ongoing) {
            mNotification.mOngoing = ongoing;
            return this;
        }

        public Builder setSmallIcon(byte[] smallIcon) {
            mNotification.mSmallIcon = smallIcon;
            return this;
        }

        public Builder setImportance(int importance) {
            mNotification.mImportance = importance;
            return this;
        }

        public Builder setProgress(int progress) {
            mNotification.mProgress = progress;
            return this;
        }

        public Builder setProgressMax(int progressMax) {
            mNotification.mProgressMax = progressMax;
            return this;
        }

        public Builder setFlags(int flags) {
            mNotification.mFlags = flags;
            return this;
        }

        public Builder setHasContentIntent(int hasContentIntent) {
            mNotification.mHasContentIntent = hasContentIntent;
            return this;
        }

        public Builder setBigPicture(byte[] bigPicture) {
            mNotification.mBigPicture = bigPicture;
            return this;
        }

        public Builder setContentButtonLabel(String contentButtonLabel) {
            mNotification.mContentButtonLabel = contentButtonLabel;
            return this;
        }

        public Builder setDismissButtonLabel(String dismissButtonLabel) {
            mNotification.mDismissButtonLabel = dismissButtonLabel;
            return this;
        }

        public Builder setTag(String tag) {
            mNotification.mTag = tag;
            return this;
        }

        public Builder setType(NotificationType type) {
            mNotification.mType = type;
            return this;
        }

        public Builder setRead(boolean isRead) {
            mNotification.mIsRead = isRead;
            return this;
        }

        public NotificationItem build() {
            return mNotification;
        }
    }
}
