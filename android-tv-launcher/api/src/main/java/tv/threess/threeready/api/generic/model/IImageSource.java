package tv.threess.threeready.api.generic.model;

import java.io.Serializable;

/**
 * Interface that defines an image source by its url, width and height.
 *
 * @author Denisa Trif
 * @since 21.05.2019
 */
public interface IImageSource extends Serializable {

    /**
     * @return The url of the image.
     */
    String getUrl();

    /**
     * @return The height of the image in pixel or 0 if not defined.
     */
    default int getHeight() {
        return 0;
    }

    /**
     * @return The width of the image in pixel or 0 if not defined.
     */
    default int getWidth() {
        return 0;
    }

    /**
     * @return The type of the image. Can be used to identify different
     * aspect ratio or places where the image should be displayed.
     */
    Type getType();

    enum Type {
        COVER, // Cover image of an asset.
        BACKGROUND, // Background image of an asset.
        BANNER, // The image is a marketing banner.
        APP, // Banner image of an application.
        DEFAULT, // Default image of the asset.
        URL, // Direct url or translation key for the url
    }
}
