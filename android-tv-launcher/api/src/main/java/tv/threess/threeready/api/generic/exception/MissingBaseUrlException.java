package tv.threess.threeready.api.generic.exception;

/**
 * Exception thrown when a request is made to the backend but the base URL is not available.
 *
 * @author Barabas Attila
 * @since 3/24/21
 */
public class MissingBaseUrlException extends BackendException {

    public MissingBaseUrlException(String errorMessage) {
        super(errorMessage);
    }
}
