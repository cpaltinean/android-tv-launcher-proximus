/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.vod;

import androidx.annotation.WorkerThread;

import java.io.IOException;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.model.BingeWatchingInfo;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodRentalTransaction;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.api.vod.model.VodSeriesDetailModel;
import tv.threess.threeready.api.vod.wrapper.LanguageRentOffer;
import tv.threess.threeready.api.vod.wrapper.VodRentOffers;

/**
 * Implementation of the specific VOD access data methods generally serving UI components.
 *
 * @author Barabas Attila
 * @since 2017.09.04
 */
public interface VodRepository extends Component {

    /**
     * Rent a VOD variant based on the rent offer and the currency specified.
     *
     * @param vod        The parent VOD of the variant.
     * @param vodVariant The VOD variant which should be rented.
     * @param price      The price specified for the offer, where we specify the currency and the amount.
     */
    @WorkerThread
    void rentVodItem(IVodItem vod, IVodVariant vodVariant, IVodPrice price) throws IOException;

    /**
     * Rent a VOD variant based on the rent offer and the currency specified.
     *
     * @param vod        The parent VOD of the variant.
     * @param vodVariant The VOD variant which should be rented.
     * @param price      The price specified for the offer, where we specify the currency and the amount.
     * @param pin        Renting pin
     */
    @WorkerThread
    void rentVodItem(IVodItem vod, IVodVariant vodVariant, IVodPrice price, String pin) throws IOException;

    /**
     * Rx observable which will return a map with all the available variant and rent offers.
     *
     * @param baseVod The VOD for which the rent offers should be returned.
     * @return Rx observable which will emit the rent offer map,
     * where the key is the variant and the value is the offer for the given variant.
     */
    Observable<VodRentOffers> getVariantRentOffers(IBaseVodItem baseVod);

    /**
     * Returns an observable of a vod's bookmark
     *
     * @param vod - the vod for which the bookmark is returned
     * @return the bookmark holding vod's duration & last viewed position
     */
    Observable<IBookmark> getBookmarkForVod(IBaseVodItem vod);

    /**
     * @param vodVariant The vod variant for which the bookmark should be returned.
     * @return Rx observable which will emit the bookmark of the VOD variant when it changes.
     */
    Observable<IBookmark> getBookmarkForVod(IVodVariant vodVariant);

    /**
     * Rx observable to subscribe to the purchase changes to the given VOD.
     *
     * @param vod The VOD which should be updated after purchase.
     * @return Rx observable which will emit the VOD with the give id.
     **/
    Observable<IVodItem> getVod(IBaseVodItem vod);

    /**
     * Rx observable which will return the series withV
     * the given id and subscribes to the purchase changes.
     *
     * @param seriesId The unique identifier of the VOD series.
     * @return Rx observable which will emit the series with the give id.
     */
    Observable<VodSeriesDetailModel> getVodSeries(String seriesId);

    /**
     * Rx observable which will return the VOD with
     * the given id and subscribes to the purchases changes.
     *
     * @param id The unique identifier of the VOD which should be returned.
     * @return Rx observable which will emit the VOD with the given id.
     */
    Observable<IVodItem> getVod(String id);

    /**
     * Returns an observable that emits the last played vod's bookmark in a vod series
     *
     * @param series - the series to find the last played vod's bookmark in
     * @return an Observable with the last played vod bookmark in the series
     */
    Observable<VodRentOffers> getLastPlayedVodInSeries(IVodSeries series);

    /**
     * Rx observable to return a vod from a series after a specific one.
     *
     * @param series The vod from a series
     * @return Rx observable which emits the next episode.
     */
    Observable<BingeWatchingInfo<IVodSeries, IVodItem>> getNextEpisode(IVodItem series, IVodSeries vodSeries);

    /**
     * Rx observable to return the rentals of the user.
     *
     * @return Rx observable which emits the rentals of the user.
     */
    Observable<ModuleData<IBaseContentItem>> getUserRentals(ModuleConfig moduleConfig, int start, int count);

    /**
     * Rx observable which will return a map with all the available variant and rent offers based on languages.
     *
     * @param baseVod The VOD for which the rent offers should be returned.
     * @return Rx observable which will emit the language rent offer map,
     * where the key is the language and the value is a list with the variants and it's offers
     */
    Observable<LanguageRentOffer> getLanguageRentOffers(IBaseVodItem baseVod);

    /**
     * Gets a list with user's history vod rentals.
     *
     * @return The list with user's history vod rentals.
     */
    Single<List<IVodRentalTransaction>> getVodRentalTransactions();

    /**
     * Gets the completed vod variant for the given base vod item.
     */
    Single<IVodItem> getCompleteVod(IBaseVodItem vod);

    /**
     * Wait until the VOD rentals and categories are synchronized with the cache.
     */
    Completable waitForVodSync();
}
