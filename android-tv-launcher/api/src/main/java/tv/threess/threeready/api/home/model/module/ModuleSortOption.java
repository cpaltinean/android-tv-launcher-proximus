/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.home.model.module;

/**
 * Utility interface which holds the available module data source sorting options.
 * @see ModuleDataSourceParams#getSort()
 *
 * @author Barabas Attila
 * @since 2018.08.23
 */
public interface ModuleSortOption {

    /**
     * Defined sorting options for channel type.
     */
    interface Channel {
        String FAVORITE = "favorites";
        String RANDOM = "random";
    }

    /**
     * Defined sorting options for app type.
     */
    interface App {
        String FEATURED = "featured";
        String RECENCY = "recency";
        String PACKAGE = "package";
        String STORE = "store";
    }

    /**
     * Define sort option for mixed type modules.
     */
    interface Mixed {
        interface SortBy {
            String NAME = "SortBy";
        }
    }
}
