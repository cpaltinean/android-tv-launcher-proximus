package tv.threess.threeready.api.home.model.generic;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

/**
 * Represents a hint for the user.
 *
 * @author Barabas Attila
 * @since 5/14/21
 */
public interface Hint extends Serializable {
    /**
     * @return Unique identifier of the hint.
     */
    String getId();

    /**
     * @return Title of the hint screen.
     */
    String getTitle();

    /**
     * @return True if the hint needs to be displayed at FTI.
     */
    boolean getEnabledTutorial();

    /**
     * @param position THe position of the image.
     * @return The hint image at the given position.
     */
    @Nullable
    String getImage(int position);

    /**
     * @return All the images which needs to be displayed in the hint.
     */
    List<String> getImages();
}
