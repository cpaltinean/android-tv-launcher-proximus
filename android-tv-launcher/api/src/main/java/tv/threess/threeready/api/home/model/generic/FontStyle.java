 /*
  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
  */
package tv.threess.threeready.api.home.model.generic;

 import java.io.Serializable;

 import tv.threess.lib.di.Component;

 /**
 * Enumeration class representing a font style used in the app.
 *
 * @author Barabas Attila
 * @since 2018.07.10
 */
public enum FontStyle implements Component, Serializable {
     DEFAULT ("default_fonts.json"),
     PROXIMUS("proximus_fonts.json");

     public final String fileName;

     FontStyle(String fileName) {
         this.fileName = fileName;
     }
 }
