package tv.threess.threeready.api.generic.model;

import android.content.ComponentName;
import android.media.tv.TvContract;

import tv.threess.lib.di.Component;

/**
 * Utility class used to share information's about the input services between the different modules.
 *
 * @author Barabas Attila
 * @since 2017.09.03
 */
public class InputServiceInfo implements Component {

    ComponentName mOttInputComponentName;


    public InputServiceInfo(ComponentName componentName) {
        mOttInputComponentName = componentName;
    }

    public String getOttInputId() {
        if (mOttInputComponentName == null) {
            return "";
        }

        return TvContract.buildInputId(mOttInputComponentName);
    }

    public ComponentName getOttInputComponentName() {
        return mOttInputComponentName;
    }
}
