package tv.threess.threeready.api.account.setting;

import java.util.Map;
import java.util.function.Consumer;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;

/**
 * Editor to asynchronously update a single setting value.
 *
 * @author Barabas Attila
 * @since 2020.06.04
 */
public class SettingAsyncEditor<TKey extends Enum<TKey> & SettingProperty> extends SettingEditor<TKey> {

    public SettingAsyncEditor(TKey key, Consumer<Map<TKey, String>> persist) {
        super(key, persist);
    }

    @Override
    public void persist(TKey key, String value) {
        Completable.fromAction(() -> super.persist(key, value))
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe();
    }
}
