package tv.threess.threeready.api.home.model.generic;

/**
 * Configuration for background playback.
 *
 * The options include:
 *  - Full screen playback
 *  - Mini player, with minimized surface
 *  - No visible video stream in the background
 *
 * @author Andor Lukacs
 * @since 7/26/18
 */
public enum BackgroundPlayerType {
    FULL_PLAYER,
    MINI_PLAYER,
    NONE
}
