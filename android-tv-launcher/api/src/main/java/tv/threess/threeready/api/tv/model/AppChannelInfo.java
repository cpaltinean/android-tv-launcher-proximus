package tv.threess.threeready.api.tv.model;

/**
 * Holds the extra information like packageName, next/previous channel number about an application channe.
 *
 * @author Barabas Attila
 * @since 2019.04.18
 */
public class AppChannelInfo {
    public final String packageName;
    public final int channelNumber;
    public final int nextChannelNumber;
    public final int prevChannelNumber;

    public AppChannelInfo(String packageName, int channelNumber, int nextChannelNumber, int prevChannelNumber) {
        this.packageName = packageName;
        this.channelNumber = channelNumber;
        this.nextChannelNumber = nextChannelNumber;
        this.prevChannelNumber = prevChannelNumber;
    }

}