package tv.threess.threeready.api.tv.model;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.generic.model.ISeries;
import tv.threess.threeready.api.generic.model.ISeason;

/**
 * Tv series container. Holds all the episodes and meta info about the series.
 *
 * @author Barabas Attila
 * @since 2018.12.10
 */
public interface ITvSeries<TBroadcast extends IBroadcast> extends ISeries<TBroadcast> {

    /**
     * @return The first available ongoing or future episode of the series,
     * or null if there is no such episode.
     */
    @Nullable
    default IBroadcast getFirstOngoingOrFutureEpisode() {
        for (ISeason<TBroadcast> season : getSeasons()) {
            for (TBroadcast episode : season.getEpisodes()) {
                if (episode.getEnd() > System.currentTimeMillis()) {
                    return episode;
                }
            }
        }
        return null;
    }

    /**
     * @return true if the series is recording capable.
     */
    default boolean isNPVREnabled() {
        IBroadcast firstEpisode = getFirstEpisode();
        if (firstEpisode == null) {
            return false;
        }

        return firstEpisode.isNPVREnabled();
    }

    /**
     * @return True if the series has a live episode.
     */
    default boolean hasLiveEpisode() {
        return getLiveEpisode() != null;
    }

    /**
     * returns live episode of series
     *
     * @return if null there is not live episode
     */
    default IBroadcast getLiveEpisode() {
        for (ISeason<TBroadcast> season : getSeasons()) {
            for (IBroadcast episode : season.getEpisodes()) {
                if (episode.isLive()) {
                    return episode;
                }
            }
        }
        return null;
    }
}
