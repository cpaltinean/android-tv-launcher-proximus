/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.generic.helper;

import android.text.TextUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Language and location related utility methods.
 *
 * @author Barabas Attila
 * @since 2017.05.31
 */
public class LocaleUtils {
    public static final String ENGLISH_LANGUAGE_CODE = "en";
    public static final String FRENCH_LANGUAGE_CODE = "fr";
    public static final String DUTCH_LANGUAGE_CODE = "nl";
    public static final String GERMAN_LANGUAGE_CODE = "de";
    public static final String ORIGINAL_VERSION_LANGUAGE_CODE = "OV";

    public static final String FRENCH_SHOP_LANGUAGE = "fr";
    public static final String DUTCH_SHOP_LANGUAGE = "nl";
    public static final String FRENCH_AND_DUTCH_SHOP_LANGUAGE = FRENCH_SHOP_LANGUAGE + "," + DUTCH_SHOP_LANGUAGE;

    private static final Set<String> SUPPORTED_LANGUAGES = new HashSet<String>() {{
        add(ENGLISH_LANGUAGE_CODE);
        add(FRENCH_LANGUAGE_CODE);
        add(DUTCH_LANGUAGE_CODE);
    }};

    /**
     * Return the current application language in ISO 639 format.
     */
    public static String getApplicationLanguage() {
        String deviceLang = Locale.getDefault().getLanguage();
        if (SUPPORTED_LANGUAGES.contains(deviceLang)) {
            return deviceLang;
        }

        // fallback to english
        return ENGLISH_LANGUAGE_CODE;
    }

    /**
     * @param languageIsoCode the ISO code for the language.
     * @return language name as string.
     */
    public static String getLanguageFromIso(String languageIsoCode) {
        return new Locale(languageIsoCode).getDisplayLanguage();
    }

    /**
     * @param languages That are present in the local settings.
     * @return The shop language name as string.
     */
    public static String getShopLanguage(List<String> languages) {
        if (ArrayUtils.isEmpty(languages)) {
            return LocaleUtils.FRENCH_LANGUAGE_CODE.equalsIgnoreCase(LocaleUtils.getApplicationLanguage())
                    ? LocaleUtils.FRENCH_SHOP_LANGUAGE : LocaleUtils.DUTCH_LANGUAGE_CODE;
        } else {
            return TextUtils.join(",", languages);
        }
    }
}
