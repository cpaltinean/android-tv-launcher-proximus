/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.netflix.model;

/**
 * Content category type for Netflix channel.
 *
 * @author Barabas Attila
 * @since 2017.10.19
 */
public enum CategoryType {
    BASIC_SD,
    BASIC_HD,
    MOVIES_SD,
    MOVIES_HD,
    CHILDREN_SD,
    CHILDREN_HD,
    LEARNING_SD,
    LEARNING_HD,
    INDIE_SD,
    INDIE_HD,
    OTHER;

    private final String mName;

    CategoryType() {
        mName = this.name();
    }

    CategoryType(String key) {
        mName = key;
    }
}
