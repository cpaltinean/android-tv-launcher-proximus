package tv.threess.threeready.api.account.setting;

import androidx.annotation.WorkerThread;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.JsonConverter;

/**
 * Class used to bulk update setting values.
 *
 * @author Barabas Attila
 * @since 2020.05.29
 */
public class BatchSettingEditor<TKey extends Enum<TKey> & SettingProperty> {

    private final Consumer<Map<TKey, String>> mPersist;
    private final HashMap<TKey, String> mValues = new HashMap<>();

    public BatchSettingEditor(Consumer<Map<TKey, String>> persist) {
        mPersist = persist;
    }

    public BatchSettingEditor(BiConsumer<TKey, String> persist) {
        // fallback case, process each item one by one
        this(map -> {
            for (Map.Entry<TKey, String> entry : map.entrySet()) {
                persist.accept(entry.getKey(), entry.getValue());
            }
        });
    }

    /**
     * Save the changed values.
     */
    @WorkerThread
    public void persistNow() {
        mPersist.accept(mValues);
    }

    /**
     * Save the changed values asynchronously.
     */
    public void persist() {
        Completable.fromAction(this::persistNow)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe();
    }

    /**
     * Store a value for the key.
     * {@link #persist()} or {@link #persistNow()} method needs to called afterwards to save the changes.
     * @param key The key for which the value needs to be changed.
     * @param value The string value which needs to be stored.
     */
    public BatchSettingEditor<TKey> put(TKey key, String value) {
        mValues.put(key, value);
        return this;
    }

    /**
     * Store a value for the key.
     * {@link #persist()} or {@link #persistNow()} method needs to called afterwards to save the changes.
     * @param key The key for which the value needs to be changed.
     * @param value The long value which needs to be stored.
     */
    public BatchSettingEditor<TKey> put(TKey key, long value) {
        mValues.put(key, String.valueOf(value));
        return this;
    }

    /**
     * Store a value for the key.
     * {@link #persist()} or {@link #persistNow()} method needs to called afterwards to save the changes.
     * @param key The key for which the value needs to be changed.
     * @param value The boolean value which needs to be stored.
     */
    public BatchSettingEditor<TKey> put(TKey key, boolean value) {
        mValues.put(key, String.valueOf(value));
        return this;
    }

    /**
     * Store a value for the key.
     * {@link #persist()} or {@link #persistNow()} method needs to called afterwards to save the changes.
     * @param key The key for which the value needs to be changed.
     * @param value The double value which needs to be stored.
     */
    public BatchSettingEditor<TKey> put(TKey key, double value) {
        mValues.put(key, String.valueOf(value));
        return this;
    }

    /**
     * Store a value for the key.
     * {@link #persist()} or {@link #persistNow()} method needs to called afterwards to save the changes.
     * @param key The key for which the value needs to be changed.
     * @param value The Json representation of the value which needs to be stored.
     */
    public BatchSettingEditor<TKey> put(TKey key, Object value) {
        if (value == null) {
            return remove(key);
        }
        mValues.put(key, Components.get(JsonConverter.class).toJson(value));
        return this;
    }

    /**
     * Remove the value for the given key.
     * @param key The key for which the value needs to be removed.
     */
    public BatchSettingEditor<TKey> remove(TKey key) {
        mValues.put(key, null);
        return this;
    }
}
