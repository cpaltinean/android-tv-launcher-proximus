package tv.threess.threeready.api.account.model.server;

import java.util.List;

/**
 * Interface used for the poster server info related minimal data.
 * 
 * @author David 
 * @since 16.11.2021
 */
public interface IPosterServerInfo {
    
    /**
     * @return Server name.
     */
    String getName();

    /**
     * @return The server`s default url built from server address, port and path.
     */
    String getDefaultUrl() throws NumberFormatException;

    /**
     * @return True if server allows image sizing.
     */
    boolean isImageResizable();

    /**
     * @return a list with allowed image sizes.
     */
    List<? extends IPosterServerInfoField> getAllowedSizes();
}
