/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.log.model;

/**
 * Playback event param keys enum
 *
 * @author Arnold Szabo
 * @since 2017.07.24
 */
public enum PlaybackEventDetails implements Event.Detail<PlaybackLogEvent> {
    ITEM_ID,
    ITEM_EXTERNAL_ID,
    ITEM_TYPE,
    ITEM_GENRE,
    ITEM_SUPER_GENRE,
    ITEM_TITLE,
    ITEM_START,
    ITEM_DURATION,
    ITEM_RESOLUTION,

    SEASON_NR,
    EPISODE_NR,
    EPISODE_TITLE,

    // Channel information
    CHANNEL_ID,
    CHANNEL_INTERNAL_ID,
    CHANNEL_CALL_LETTER,

    // Broadcast information
    PROGRAM_ID,
    PROGRAM_ITEM_ID,
    PROGRAM_TITLE,
    PROGRAM_CATEGORY,
    PROGRAM_SUB_CATEGORY,

    // Vod information
    VOD_TITLE,
    GROUP_TITLE,
    VOD_ITEM_IDENTIFIER,
    VOD_EXTERNAL_IDENTIFIER,
    VOD_TYPE,

    // Playback information
    PLAYBACK_INITIATED_TIMESTAMP,
    STATE_CHANGED_TIMESTAMP,
    PLAYER_STATE,
    STREAM_TYPE,
    IS_LIVE,
    IS_PLAYING,
    PLAYBACK_URL,
    AUDIO_LANGUAGE,
    SUBTITLE_LANGUAGE,
    BITRATE,
    POSITION,

    ACTION,
    RESUME_BOOKMARK,

    ERROR_NO_INTERNET,

    SEGMENT_SIZE,
    SEGMENT_TIME,

    //TAD details
    SPLICE_ID,
    TAP_REQUEST_SENT,
    LAST_ACTIVITY,
    ADVERTISEMENT_REFERENCE,
    CHANNEL_INACTIVITY_THRESHOLD
}
