/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.generic;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

/**
 * Base Contract class to access data in database.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.13
 */
public class BaseContract {

    /**
     * The unique ID for a row.
     * <P>Type: INTEGER (long)</P>
     */
    public static final String _ID = BaseColumns._ID;

    /**
     * The timestamp when the data was retrieved from the backend.
     * <P>Type: Timestamp(long)</P>
     */
    public static final String LAST_UPDATED = "last_updated";

    /**
     * The count aggregate function for selection.
     * <P>Type: INTEGER (long)</P>
     */
    public static final String COUNT_ALL = "COUNT(*)";

    /**
     * Additional query parameter to add limit to the queries.
     */
    public static final String QUERY_LIMIT = "limit";

    /**
     * Additional parameter to identify if it's part of a batch operation.
     */
    public static final String QUERY_BATCH = "batch";

    /**
     * Additional query parameter to add grouping to the queries.
     */
    public static final String GROUP_BY = "group_by";

    /**
     * Additional bulk insert parameter to allow replace on conflict.
     */
    public static final String BULK_REPLACE = "replace";

    /**
     * Convert contract class name to database table name.
     *
     * @param contract the contract class
     * @return snake case string of contract class name
     */
    public static String tableName(Class<?> contract) {
        String value = contract.getSimpleName();
        StringBuilder sb = new StringBuilder(value.length());
        char prev = value.charAt(0);
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (Character.isUpperCase(ch)) {
                ch = Character.toLowerCase(ch);
                if (Character.isLowerCase(prev)) {
                    if (i > 0) {
                        sb.append('_');
                    }
                }
            }
            sb.append(ch);
            prev = ch;
        }
        return sb.toString();
    }

    public static String mimeType(Class<?> contract) {
        return "vnd.android.cursor.dir/" + contract.getCanonicalName().toLowerCase();
    }

    /**
     * Append the uri with the batch update param.
     */
    public static Uri withBatchUpdate(Uri uri) {
        return uri.buildUpon().appendQueryParameter(QUERY_BATCH, String.valueOf(true)).build();
    }

    /**
     * Add limit to the returning row count of a query uri.
     *
     * Use only with {@link ContentProvider#query(Uri, String[], String, String[], String)}.
     *
     * @param uri uri to add the limit to.
     * @param limit limit the row count returned from database.
     * @return Uri containing the limit.
     */
    public static Uri withLimit(Uri uri, int limit) {
        return uri.buildUpon().appendQueryParameter(QUERY_LIMIT, String.valueOf(limit)).build();
    }

    /**
     * Add limit and offset to the returning row count of a query uri.
     *
     * Use only with {@link ContentProvider#query(Uri, String[], String, String[], String)}.
     *
     * @param uri uri to add the limit to.
     * @param limit limit the row count returned from database.
     * @param offset the offset of the first element.
     * @return Uri containing the limit.
     */
    public static Uri withLimit(Uri uri, int limit, int offset) {
        return uri.buildUpon().appendQueryParameter(QUERY_LIMIT, offset + ", " + limit).build();
    }

    /**
     * Add group by clause to the query.
     *
     * Use only with {@link ContentProvider#query(Uri, String[], String, String[], String)}.
     *
     * @param uri uri to add the group by clause to.
     * @param columns group by these columns.
     * @return Uri containing the group by.
     */
    public static Uri withGroupBy(Uri uri, String... columns) {
        return uri.buildUpon().appendQueryParameter(GROUP_BY, TextUtils.join(",", columns)).build();
    }

    /**
     * Turn on replace on conflict algorithm.
     * All the new content will be inserted or replaced, the old data will be kept.
     *
     * Use only with {@link ContentProvider#bulkInsert(Uri, ContentValues[])}.
     *
     * @param uri uri to add the override to.
     * @return Uri to be used in bulkInsert
     */
    public static Uri bulkReplace(Uri uri) {
        return uri.buildUpon().appendQueryParameter(BULK_REPLACE, "").build();
    }

    /**
     * Replace all the data in the table based on {@link #LAST_UPDATED} field.
     * All the new content will be inserted or replaced, the old data will be purged.
     *
     * Use only with {@link ContentProvider#bulkInsert(Uri, ContentValues[])}.
     *
     * @param uri uri to add the override to.
     * @return Uri to be used in bulkInsert
     */
    public static Uri bulkReplace(Uri uri, long timestamp) {
        return uri.buildUpon().appendQueryParameter(BULK_REPLACE, String.valueOf(timestamp)).build();
    }
}
