package tv.threess.threeready.api.pvr.model;

import tv.threess.threeready.api.tv.model.IBaseTvSeries;

/**
 * API model representing a recording series.
 *
 * @author Barabas Attila
 * @since 2020.04.20
 */
public interface IBaseRecordingSeries extends IBaseTvSeries {

    /**
     * @return The number of recordings is the series.
     */
    int getNumberOfRecordings();
}
