/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.account;


import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;

import java.io.IOException;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import tv.threess.lib.di.Component;
import tv.threess.threeready.api.account.model.account.IConsent;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.generic.model.IParentalControlSettings;
import tv.threess.threeready.api.generic.model.WatchlistType;
import tv.threess.threeready.api.home.model.local.FontConfig;
import tv.threess.threeready.api.log.model.Error;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.playback.model.TrackInfo;
import tv.threess.threeready.api.tv.epg.EpgLayoutType;
import tv.threess.threeready.api.tv.model.TvChannel;

/**
 * Extension methods for the config proxy to handle additional action and business logic.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.05.04
 */
public interface AccountRepository extends Component {

    /**
     * @return The name of the backend environment to which the application is connected.
     */
    String getEnvironment();

    /**
     * Register device on the managed network.
     *
     * @param lineNumber The account number of the user.
     * @param pin        The system PIN. Default 1234
     */
    Single<Boolean> registerDevice(String lineNumber, String pin);

    /**
     * Method used for send the impression event for the ninja apk.
     * We use the main handler for collecting the impression events and send in bucket.
     * Note: The preparation of the bucket and the post takes place on the main thread.
     */
    void sendImpression(String deepLink);

    /**
     * @return Observable which will emit a completely built url.
     */
    Observable<String> buildURL(String url);

    /**
     * @return Observable which will emit a completely built url, by using the given channel.
     */
    Observable<String> buildURL(String url, TvChannel channel);

    /**
     * @return RX observable that emits the shop languages
     */
    Single<String> getShopLanguage();

    /**
     * @param languages the language to set for the Shop language
     */
    Completable setShopLanguages(String languages);

    /**
     * Sets the preferred subtitle language.
     */
    Completable setPreferredSubtitleLanguages(TrackInfo language);

    /**
     * Set the global subtitle language in the TCH settings
     */
    Completable setPreferredAudioLanguage(TrackInfo language);

    /**
     * @return Rx observable to start the boot config update and wait to finish.
     */
    Completable updateBootConfig();

    /**
     * Used for start the caching service.
     */
    Completable caching();

    /**
     * @return True if the device needs to be registered.
     */
    Single<Boolean> isDeviceRegistrationRequired();

    /**
     * @return True if the user has replay plus subscription, false otherwise.
     */
    @WorkerThread
    boolean hasReplayPlusSubscription();

    /**
     * @return True if the user has replay subscription, false otherwise.
     */
    @WorkerThread
    boolean hasReplaySubscription();

    /**
     * @return True if the user has the package subscription, false otherwise.
     */
    @WorkerThread
    boolean isSubscribed(String toPackage);

    /**
     * @param favoriteChannels A String with all favorite channel id's joined by ','.
     */
    @WorkerThread
    void updateFavoriteChannels(String favoriteChannels) throws IOException;

    /**
     * Start a service for check and validate all the cache information with the backend.
     */
    void startPerformCaching();

    /**
     * Start a service for update the device current status and registration information.
     */
    void startUpdateDeviceRegistration();

    /**
     * Check if the provided item (by id) is in the watchlist.
     */
    Observable<Boolean> isInWatchlist(String contentId);

    /**
     * Add an item to the watchlist.
     *
     * @param contentId Id of the item (external ID)
     * @param type      Watchlist type
     * @return Completable with add action.
     */
    Completable addToWatchlist(WatchlistType type, String contentId);

    /**
     * Remove an item from the watchlist.
     *
     * @param contentId ID of the watchlisted item (NOT watchlist ID!)
     * @return Completable with remove action
     */
    Completable removeFromWatchlist(String contentId, WatchlistType type);

    /**
     * Rx observable to updatePowerConsumption status
     */
    Completable setPowerConsumptionStatus(boolean status);

    /**
     * Sets the epg layout.
     */
    Completable setEpgLayout(EpgLayoutType epgLayout);

    /**
     * Clear all the previously stored data.
     * Databases, files cache, settings, session, alarms etc.
     */
    Completable clearData();

    /**
     * Rx observable used to change parental control pin.
     *
     * @param pin The pin insert by the user.
     */
    Single<Boolean> changeParentalControlPIN(String pin);

    /**
     * Rx observable used to change the Purchase Pin.
     *
     * @param purchasePIN The Purchase Pin insert by the user.
     */
    Single<Boolean> changePurchasePIN(String purchasePIN);

    /**
     * Rx observable used to set Parental Age rating.
     *
     * @param parentalRating The Age rating choose by the user.
     */
    Completable setParentalRating(@NonNull ParentalRating parentalRating);

    /**
     * Rx observable to enable/disable the parental control settings.
     */
    Completable enableParentalControl(boolean enable);

    /**
     * Save hint viewed
     */
    Completable setHintViewed(final String hintId);

    /**
     * Deletes viewed hints from BE and from Settings DB.
     */
    Completable deleteViewedHints();

    /**
     * Method used for subscribe to errors with the given consumer.
     */
    Disposable subscribeToErrors(Consumer<Error> consumer);

    /**
     * @return The font configurations.
     */
    Observable<FontConfig[]> getFontConfigs();

    /**
     * Single which returns the netflix authentication token.
     */
    Single<String> getNetflixAuthenticationToken();

    /**
     * @param requestScenario Starting point of the request. E.g. boot screen / setting screen...
     * @return All consents from the microservice for the given scenario.
     */
    Single<List<? extends IConsent>> getConsents(String requestScenario);

    /**
     * @param consentId Id of the consent to be returned.
     * @return Single that emits the consent with the given id.
     */
    Single<IConsent> getConsent(String consentId);

    /**
     * @param consentId  Id of the consent to be modified.
     * @param isAccepted New state of the consent.
     * @return Completable to set the consent.
     */
    Completable setConsent(String consentId, boolean isAccepted);

    /**
     * @return link of GDPR QR code.
     */
    Single<IImageSource> getConsentQrCode();

    /**
     * @return The parental control configuration.
     * The timeouts, activation state and selected rating.
     */
    Observable<IParentalControlSettings> getParentalControlSettings();
}
