/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.playback.listener;

import android.media.tv.TvTrackInfo;

import java.util.List;

/**
 * PlayerWrapper listener
 *
 * @author Daniel Gliga
 * @since 2017.11.30
 */

public interface PlayerWrapperListener {

    void notifyVideoAvailable();

    void notifyVideoUnavailable(int reason);

    void notifyTrackSelected(int type, String id);

    void notifyTracksChanged(List<TvTrackInfo> tracks);
}
