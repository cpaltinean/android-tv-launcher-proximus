package tv.threess.threeready.api.generic.model;

import java.io.Serializable;

/**
 * Interface for watchlist items
 *
 * @author Andor Lukacs
 * @since 04/11/2020
 */
public interface IWatchlistItem extends Serializable {

    /**
     * @return The unque identifier of the watchlist item.
     */
    String getId();

    /**
     * @return The unique identifier of the watchlisted content.
     */
    String getContentId();

    /**
     * @return The type of the content the watchlisting made for. E.g Replay, VOD etc.
     */
    WatchlistType getType();

    /**
     * @return The time when the watchlist item was last updated.
     */
    long getTimestamp();
}
