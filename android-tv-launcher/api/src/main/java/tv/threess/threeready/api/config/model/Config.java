 /*
  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
  */
package tv.threess.threeready.api.config.model;

 import java.io.Serializable;
 import java.util.List;

 import tv.threess.lib.di.Component;
 import tv.threess.threeready.api.home.model.generic.ContentConfig;
 import tv.threess.threeready.api.home.model.generic.Hint;
 import tv.threess.threeready.api.home.model.generic.LayoutConfig;

 /**
 * Client configuration options.
 *
 * @author Barabas Attila
 * @since 5/12/21
 */
public interface Config extends Component, Serializable {

     /**
      * @return Backend communication related configs.
      */
    ApiConfig getApiConfig();

     /**
      * @return Application behavior related config options.
      * Caching, playback, featured apps etc.
      */
    AppConfig getAppConfig();

     /**
      * @return Page structure and content related config options.
      */
    ContentConfig getContentConfig();

     /**
      * @return Layout and theme related config options.
      */
    LayoutConfig getLayoutConfig();

     /**
      * @return Feature related config options.
      */
    FeatureControl getFeatureControl();

     /**
      * @return All the configured hints in the application.
      */
    List<Hint> getHints();

     /**
      * @return Hints needs to be displayed during the FTI.
      */
    List<Hint> getTutorialHints();

     /**
      * @return Hints which needs to be displayed when the internet connection drops.
      */
    List<Hint> getInternetCheckHints();
}
