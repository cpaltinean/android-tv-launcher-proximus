package tv.threess.threeready.api.home;

import java.io.Serializable;

import tv.threess.threeready.api.home.model.page.UIPageReportProperties;

/**
 * Model representing a sort or filtering option for a data source.
 *
 * @author Barabas Attila
 * @since 2020.08.24
 */
public interface SelectorOption extends UIPageReportProperties, Serializable {

    String getName();

    /**
     * @return True if it's selected by default.
     */
    boolean isDefault();

    /**
     * @return The empty page message.
     */
    default String getEmptyPageMessage() {
        return null;
    }

    /**
     * @return The number of items under each menu items.
     * If it is less than 0, omit it.
     */
    default int getItemCount() {
        return -1;
    }
}
