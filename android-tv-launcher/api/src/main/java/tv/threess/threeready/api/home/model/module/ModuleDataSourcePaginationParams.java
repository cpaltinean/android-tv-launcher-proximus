package tv.threess.threeready.api.home.model.module;

import java.io.Serializable;

/**
 * Data source configuration options for pagination.
 * Contains the page size and the threshold after which the next page should be requested.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public interface ModuleDataSourcePaginationParams extends Serializable {

    /**
     * @return The maximum number of items in a page.
     */
    int getSize();

    /**
     * @return The number of items in each page
     * after which the next page should be requested.
     */
    int getThreshold();
}
