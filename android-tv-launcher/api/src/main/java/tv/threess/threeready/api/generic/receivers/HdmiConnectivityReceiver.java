/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.generic.receivers;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Handler;
import android.os.HandlerThread;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.api.middleware.model.HdmiInfo;

/**
 * Listen to Hdmi plug and unplug events.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.12.07
 */
public class HdmiConnectivityReceiver extends BaseBroadcastReceiver implements Component {
    private static final String TAG = Log.tag(HdmiConnectivityReceiver.class);
    private static final IntentFilter INTENT_FILTER = new IntentFilter(AudioManager.ACTION_HDMI_AUDIO_PLUG);
    private static final int HDCP_POOLING_RATE = 500;

    private final List<WeakReference<Listener>> mConnectivityListeners = new CopyOnWriteArrayList<>();

    private final MwRepository mMwRepository = Components.get(MwRepository.class);

    private final Handler mHdcpHandler;

    private volatile boolean mHdmiConnected;
    private volatile boolean mHdcpActive;

    public HdmiConnectivityReceiver() {
        HandlerThread handlerThread = new HandlerThread(":HDCP");
        handlerThread.start();
        mHdcpHandler = new Handler(handlerThread.getLooper());
    }

    public boolean isConnected() {
        return mHdmiConnected;
    }

    public boolean isHdcpActive() {
        return mHdcpActive;
    }

    @Override
    public void registerReceiver(Context context) {
        super.registerReceiver(context);

        HdmiInfo hdmiInfo = mMwRepository.getHdmiInfo();
        if (hdmiInfo != null) {
            notifyConnectivityChange(hdmiInfo.isConnected());
            notifyHDCPStateChange(hdmiInfo.isHdcpActive());
        }

        if (isHdcpActive()) {
            cancelHDCPPooling();
        } else {
            scheduleDHCPPooling(false);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Intent received", intent);
        int plugState = intent.getIntExtra(AudioManager.EXTRA_AUDIO_PLUG_STATE, 0);

        notifyConnectivityChange(plugState != 0);
        scheduleDHCPPooling(false);
    }

    @Override
    public IntentFilter getIntentFilter() {
        return INTENT_FILTER;
    }

    /**
     * Notify all the registered listeners about the HDMI connectivity changes.
     * @param connected True if the HDMI is connected false otherwise.
     */
    private void notifyConnectivityChange(boolean connected) {
        if (mHdmiConnected == connected) {
            // No change.
            return;
        }

        Log.d(TAG, "Connectivity changed. Connected : " + connected);
        mHdmiConnected = connected;
        ArrayUtils.notifyAndCleanup(mConnectivityListeners,
                listener -> listener.onConnectionChanged(connected));
    }

    /**
     * Notify all the registered listeners about the HDCP state changes.
     * @param active True if the HDCP is active false otherwise.
     */
    private void notifyHDCPStateChange(boolean active) {
        if (mHdcpActive == active) {
            // No change.
            return;
        }

        Log.d(TAG, "HDCP state changed. Active : " + active);
        mHdcpActive = active;
        ArrayUtils.notifyAndCleanup(mConnectivityListeners,
                listener -> listener.onHdcpStateChanged(active));
    }

    public void addStateChangedListener(Listener listener) {
        removeStateChangedListener(listener);
        mConnectivityListeners.add(new WeakReference<>(listener));
    }

    public void removeStateChangedListener(Listener listenerToRemove) {
        mConnectivityListeners.removeIf((item) -> {
            Listener listener = item.get();
            return listener == null || listener == listenerToRemove;
        });
    }

    private void scheduleDHCPPooling(boolean delayed) {
        mHdcpHandler.removeCallbacks(mHDCPRunnable);
        mHdcpHandler.postDelayed(mHDCPRunnable, delayed ? HDCP_POOLING_RATE : 0);
    }

    private void cancelHDCPPooling() {
        mHdcpHandler.removeCallbacks(mHDCPRunnable);
    }

    private final Runnable mHDCPRunnable = () -> {
        HdmiInfo hdmiInfo = mMwRepository.getHdmiInfo();
        if (hdmiInfo != null) {
            notifyConnectivityChange(hdmiInfo.isConnected());
            notifyHDCPStateChange(hdmiInfo.isHdcpActive());
        }

        if (isHdcpActive()) {
            cancelHDCPPooling();
        } else {
            scheduleDHCPPooling(true);
        }
    };

    public interface Listener {
        /**
         * Called when the HDMI connectivity state changes.
         * @param isConnected True if the HDMI is connected false otherwise.
         */
        default void onConnectionChanged(boolean isConnected) {}

        /**
         * Called when the HDMI HDCP state changes.
         * @param active True if the HDCP is active false otherwise.
         */
        default void onHdcpStateChanged(boolean active) {}
    }
}