package tv.threess.threeready.api.account.model;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Component;

/**
 * Caching related config options.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public interface CacheSettings extends Component, Serializable {

    /**
     * @param unit The time unit to return the result in.
     * @return The size of the base EPG window which should be always in the cache.
     */
    long getEpgCacheWindowLength(TimeUnit unit);

    /**
     * @param unit The time unit to return the result in.
     * @return The frequency of the periodic channel cache updates.
     */
    long getChannelsUpdatePeriod(TimeUnit unit);

    /**
     * @param unit The time unit to return the result in.
     * @return The frequency of the periodic EPG cache updates.
     */
    long getEpgUpdatePeriod(TimeUnit unit);

    /**
     * @param unit The time unit to return the result in.
     * @return The frequency of the periodic recording cache updates.
     */
    long getRecordingUpdatePeriod(TimeUnit unit);

    /**
     * @param unit The time unit to return the result in.
     * @return The frequency of the periodic VOD cache updates.
     */
    long getVodPurchaseUpdatePeriod(TimeUnit unit);

    /**
     * @param unit The time unit to return the result in.
     * @return The time to keep the vod categories in cache.
     */
    long getVodCacheTime(TimeUnit unit);

    /**
     * @param unit The time unit to return the result in.
     * @return The frequency of the periodic global install updates.
     */
    long getGlobalInstallUpdatePeriod(TimeUnit unit);

    /**
     * @param unit The time unit to return the result in.
     * @return The frequency of the periodic boot config cache updates.
     */
    long getBootConfigUpdatePeriod(TimeUnit unit);

    /**
     * @param unit The time unit to return the result in.
     * @return The frequency of the periodic account info cache updates.
     */
    long getAccountInfoUpdatePeriod(TimeUnit unit);

    /**
     * @param unit The time unit to return the result in.
     * @return The frequency of the periodic watchlist cache updates.
     */
    long getWatchlistUpdatePeriod(TimeUnit unit);

    /**
     * @param unit THe time unit to return the results in.
     * @return The frequency of fetching translations at startup.
     */
    long geTranslationUpdatePeriod(TimeUnit unit);

    /**
     * @param unit The time unit to return the result in.
     *  * @return The frequency of fetching client config after startup.
     */
    long getConfigUpdatePeriod(TimeUnit unit);

    /**
     * @param unit The time unit to return the result in.
     * @return The frequency of the displaying the consents during startup
     */
    long getConsentUpdateLimit(TimeUnit unit);
}
