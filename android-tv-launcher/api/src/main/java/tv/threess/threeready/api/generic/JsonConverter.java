package tv.threess.threeready.api.generic;

import java.util.List;
import java.util.Map;

import tv.threess.lib.di.Component;

/**
 * Injectable utility class to convert from and to Json values
 */
public interface JsonConverter extends Component {
    /**
     * convert an object to json string
     */
    String toJson(Object value);

    /**
     * Convert json string to an object
     */
    <T> T fromJson(String value, Class<T> type);

    /**
     * Convert json string to a list where the elements are strings
     */
    List<String> toStringList(String value);

    /**
     * Convert json string to a map where the keys and values are strings
     */
    Map<String, String> toStringMap(String value);
}
