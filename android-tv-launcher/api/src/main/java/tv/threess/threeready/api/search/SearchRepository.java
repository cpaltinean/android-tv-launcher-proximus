/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.search;

import androidx.annotation.WorkerThread;

import java.io.IOException;
import java.util.List;

import io.reactivex.Observable;
import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.search.model.SearchSuggestion;
import tv.threess.threeready.api.search.model.SearchTerm;
import tv.threess.threeready.api.tv.model.TvChannel;

/**
 * Extension methods for the search proxy to handle additional action and business logic.
 *
 * @author Barabas Attila
 * @since 2018.04.25
 */
public interface SearchRepository extends Component {

    /**
     * Perform a text based search for a 3ready module.
     *
     * @param config The module which contains
     *                   the search term, filter options and sort option of the results.
     * @param start      The position of the first item from where the search results should be returned.
     * @param limit      The maximum number of search result returned.
     * @return A list of channel search result.
     */
    Observable<ModuleData<TvChannel>> channelSearch(ModuleConfig config, int start, int limit);

    /**
     * Perform channel search with text recognized from spoken language.
     *
     * @param searchTerm The recognized text for which the search should be triggered.
     * @param start      The position of the first item from where the search results should be returned.
     * @param limit      The maximum number of search result returned.
     * @return A list of channel search results for the given term.
     * @throws IOException In case of errors.
     */
    @WorkerThread
    List<TvChannel> channelVoiceSearch(SearchTerm searchTerm, int start, int limit) throws IOException;

    /**
     * Perform broadcast search with text recognized from spoken language.
     *
     * @param searchTerm The recognized text for which the search should be triggered.
     * @param start      The position of the first item from where the search results should be returned.
     * @param limit      The maximum number of search result returned.
     * @return A list of broadcast search results for the given term.
     * @throws IOException In case of errors.
     */
    @WorkerThread
    List<IContentItem> broadcastVoiceSearch(SearchTerm searchTerm, int start, int limit) throws IOException;

    /**
     * Perform vod search with text recognized from spoken language.
     *
     * @param searchTerm The recognized text for which the search should be triggered.
     * @param start      The position of the first item from where the search results should be returned.
     * @param limit      The maximum number of search result returned.
     * @return VOD search results for the given term.
     * @throws IOException In case of errors.
     */
    @WorkerThread
    List<IContentItem> vodVoiceSearch(SearchTerm searchTerm, int start, int limit) throws IOException;

    /**
     * Search for channel which should be tune after the 'switch to channel' voice commands
     *
     * @param searchTerm The recognized text for which the search should be triggered.
     * @return The channel for which the live playback should start.
     */
    @WorkerThread
    TvChannel channelVoicePlaySearch(SearchTerm searchTerm);

    /**
     * Search for content which should be opened for 'play' voice commands
     *
     * @param searchTerm The recognized text for which the search should be triggered.
     * @return The content item which should be opened.
     * @throws IOException In case of errors.
     */
    @WorkerThread
    IContentItem contentVoicePlaySearch(SearchTerm searchTerm) throws IOException;

    /**
     * @param text the string that is used to search for suggestions
     * @return the suggestions
     */
    @WorkerThread
    Observable<List<SearchSuggestion>> getSuggestions(final String text);
}
