package tv.threess.threeready.api.generic.model;


import java.io.Serializable;

/**
 * Interface represents the playback bookmark of a content.
 * Holds the last watched position.
 *
 * @author Daniela Toma
 * @since 2020.04.28
 */
public interface IBookmark extends Serializable {

    /**
     * @return The unique identifier of the bookmark made for.
     */
    String getContentId();

    /**
     * @return The type of the content the bookmark made for. E.g Replay, VOD etc.
     */
    BookmarkType getType();

    /**
     * @return The duration of the content in milliseconds.
     */
    long getDuration();

    /**
     * @return The position of the bookmark in milliseconds.  (Compared to the start)
     */
    long getPosition();

    /**
     * @return The time when the bookmark was last updated.
     */
    long getTimestamp();

    /**
     * @return true if the stream watched until {@link ContinueWatchingConfig#getRecordingWatchedPercent()}
     */
    boolean isNearEnd(double maxWatchPercent);

    default boolean canContinueWatching(double maxWatchPercent) {
        return getPosition() > 0 && !isNearEnd(maxWatchPercent);
    }

    /**
     * @return true if the bookmark duration is equal to 0.
     */
    default boolean isEmpty() {
        return getDuration() == 0;
    }
}