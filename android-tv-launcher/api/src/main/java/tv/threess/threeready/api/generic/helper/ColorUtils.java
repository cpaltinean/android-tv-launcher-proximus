/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.generic.helper;

import android.graphics.Color;
import android.text.TextUtils;

import tv.threess.threeready.api.log.Log;

/**
 * Color related utility functions.
 *
 * @author Barabas Attila
 * @since 2018.07.12
 */
public class ColorUtils {
    private static final String TAG = Log.tag(ColorUtils.class);

    /**
     * Parse string representation hex color code as Integer.
     */
    public static Integer parseColor(String colorString) {
        try {
            return TextUtils.isEmpty(colorString) ? null : Color.parseColor(colorString);
        } catch (Exception e) {
            Log.e(TAG, "Can't parse color '" + colorString + "'", e);
            return null;
        }
    }

    /**
     * Add transparency for the given color.
     */
    public static int applyTransparencyToColor(int color, float alphaFloat) {
        int alpha = (int) (255 * alphaFloat);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    /**
     * Returns a defaultColor(second param) if the first parameter is null.
     * Otherwise returns the first parameter.
     */
    public static Integer getColor(Integer colorCode, Integer defaultColor) {
        return colorCode == null ? defaultColor : colorCode;
    }

}
