package tv.threess.threeready.api.home.model.generic;

import java.io.Serializable;

import tv.threess.lib.di.Component;

/**
 * Notification related config option.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public interface NotificationSettings extends Component, Serializable {

    /**
     * @return The duration in millis for which display low priority notifications.
     */
    int getLowDuration();

    /**
     * @return The duration in millis for which display normal priority notifications.
     */
    int getNormalDuration();

    /**
     * @return The duration in millis for which display high priority notifications.
     */
    int getHighDuration();

    /**
     * @return The duration in millis for which display urgent priority notifications.
     */
    int getUrgentDuration();

    /**
     * @return The minimum time for which display notification when there are multiple in the queue.
     */
    int getMinimumDisplayDuration();

}
