package tv.threess.threeready.api.pvr.model;

/**
 * Model used for series recoridng detail pages, containing the series information and also the last played episode.
 *
 * @author Andor Lukacs
 * @since 2020-06-05
 */
public class SeriesRecordingDetailModel {

    private final IRecordingSeries seriesRecording;
    private final IRecording lastPlayedEpisode;

    public SeriesRecordingDetailModel(IRecordingSeries seriesRecording, IRecording lastPlayedEpisode) {
        this.seriesRecording = seriesRecording;
        this.lastPlayedEpisode = lastPlayedEpisode;
    }

    public IRecordingSeries getSeriesRecording() {
        return seriesRecording;
    }

    public IRecording getLastPlayedEpisode() {
        return lastPlayedEpisode;
    }
}
