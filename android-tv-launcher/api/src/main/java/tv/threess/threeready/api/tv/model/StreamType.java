package tv.threess.threeready.api.tv.model;

/**
 * Enumeration defining the types of playback sources.
 */
public enum StreamType {
    /**
     * Type representing "Digital Video Broadcasting - Cable".
     */
    DvbC,

    /**
     * Type representing "Digital Video Broadcasting — Terrestrial".
     */
    DvbT,

    /**
     * Type representing iptv streaming using multi-cast.
     */
    IpTv,

    /**
     * Type representing iptv streaming using uni-cast.
     */
    Ott,

    /**
     * Type representing application.
     */
    App
}
