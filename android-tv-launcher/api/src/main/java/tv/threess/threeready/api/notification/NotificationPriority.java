package tv.threess.threeready.api.notification;

import android.text.TextUtils;

/**
 * Class describing notifications priority levels.
 *
 * @author Eugen Guzyk
 * @since 2018.08.27
 */
public enum NotificationPriority {
    NONE,
    MIN,
    LOW,
    DEFAULT,
    HIGH,
    MAX;

    public static NotificationPriority fromString(String priority) {
        if (TextUtils.isEmpty(priority)) {
            return DEFAULT;
        }
        switch (priority) {
            case "none":
                return NONE;
            case "min":
                return MIN;
            case "low":
                return LOW;
            case "default":
            default:
                return DEFAULT;
            case "high":
                return HIGH;
            case "critical":
                return MAX;
        }
    }
}
