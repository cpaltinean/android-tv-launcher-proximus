/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.middleware;

import io.reactivex.Completable;
import io.reactivex.Single;
import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.receivers.WaitTask;
import tv.threess.threeready.api.middleware.model.HdmiInfo;

/**
 * Extension methods that handles the MW specific actions.
 *
 * @author Barabas Attila
 * @since 2018.04.25
 */
public interface MwRepository extends Component {

    /**
     * @return The software version of the middleware.
     */
    String getMWVersion();

    /**
     * Returns the MAC address of the network interface where the hardware address is set.
     *
     * @return The MAC of the network interface or null in case of errors.
     */
    String getEthernetMacAddress();

    /**
     * @return The currently installed TV App version
     */
    String getAppVersion();

    /**
     * @return True if the box is in active state, false if the box is in stand by state.
     */
    boolean isScreenOn();

    /**
     * @return True if talkback accessibility feature is on.
     */
    boolean isTalkBackOn();

    /**
     * Clear the cached talkback value, so next time it will be refreshed with the system value
     */
    void clearTalkBackCache();

    /**
     * Start the service for read the application usage stats from the system and report them as start stop events
     */
    void sendUsageStats();

    /**
     * Start service for check for system update and wait for download if needed.
     */
    void startCheckSystemUpdate();

    /**
     * @return A task which waits for the DTV service setup to finish.
     */
    WaitTask<Void> waitForDtvSetup();

    /**
     * @return The android serial number of the device.
     */
    Single<String> getAndroidSerialNumber();

    /**
     * @return VCAS client version name.
     */
    Single<String> getVCASClientVersion();

    /**
     * @return VCAS plugin version name.
     */
    Single<String> getVCASPluginVersion();

    /**
     * @return RX observable that emits the remote control version
     */
    Single<String> getRemoteControlVersion();

    /**
     * Check for system update on the server and start the download when available.
     *
     * @param reset         True if the download should be restarted in case it was already download or the download is in progress.
     * @param rollOutBypass True to bypass server daily update roll out quota.
     * @param manualCheck   True if the update check was initiated by the user.
     */
    Completable checkSystemUpdates(boolean reset, boolean rollOutBypass, boolean manualCheck);

    /**
     * Reboot the device and apply the previously downloaded system image.
     */
    Completable applySystemUpdate();

    /**
     * used when boot is completed, add boot finish actions here
     */
    Completable bootCompletedAction();

    /**
     * @return RX observable that waits for the HDCP status
     */
    Completable waitForHDCPActivation();

    /**
     * @return True if the application is the default launcher on the device.
     */
    boolean isDefaultLauncher();

    /**
     * @return The width of the screen in pixels.
     */
    int getWindowWidth();

    /**
     * @return The height of the screen in pixels.
     */
    int getWindowHeight();

    /**
     * @return True if the app initializing after a quiescent reboot.
     */
    boolean isInQuiescentReboot();

    /**
     * Get the HDMI connection status and EDID data.
     *
     * @return HDMI connection information  or null if there is no such.
     */
    HdmiInfo getHdmiInfo();

    /**
     * Get the content of the boot_id file, This file provides a UUID that changes on each boot of the machine.
     *
     * @return content of the boot_id file.
     */
    String getBootId();
}
