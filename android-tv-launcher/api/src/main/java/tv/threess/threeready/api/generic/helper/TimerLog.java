/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.generic.helper;

import android.os.SystemClock;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.log.Log;

/**
 * Log class to log time in human readable format.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.05.02
 */
public class TimerLog {

    private static final long DAY_NANOS = TimeUnit.DAYS.toNanos(1);
    private static final long HOUR_NANOS = TimeUnit.HOURS.toNanos(1);
    private static final long MIN_NANOS = TimeUnit.MINUTES.toNanos(1);
    private static final long SEC_NANOS = TimeUnit.SECONDS.toNanos(1);
    private static final long MILLI_NANOS = TimeUnit.MILLISECONDS.toNanos(1);

    private final String mTag;
    private long mStart;

    private long mPause = Long.MIN_VALUE;
    private long mResume;
    private long mPauseDuration;

    public TimerLog(String tag) {
        mTag = tag;
        mStart = SystemClock.elapsedRealtimeNanos();
    }

    public void v(String label, String message) {
        Log.v(mTag, format(getDuration(TimeUnit.NANOSECONDS), label, message));
    }

    public void v(String message) {
        Log.v(mTag, format(getDuration(TimeUnit.NANOSECONDS), null, message));
    }

    public void d(String message) {
        Log.d(mTag, format(getDuration(TimeUnit.NANOSECONDS), null, message));
    }

    /**
     * Reset the start time of the timer logger.
     */
    public void reset() {
        mStart = SystemClock.elapsedRealtimeNanos();
        mPause = Long.MIN_VALUE;
        mResume = 0;
        mPauseDuration = 0;
    }

    public void pause() {
        if (mPause <= 0) {
            mPause = SystemClock.elapsedRealtimeNanos();
        } else {
            mPause = Math.min(mPause, SystemClock.elapsedRealtimeNanos());
        }
    }

    /**
     * Cache the calculated pause duration of the timer.
     */
    public void resume() {
        mPauseDuration = getPauseDuration();
        mResume = SystemClock.elapsedRealtimeNanos();
        mPause = Long.MIN_VALUE;
    }

    /**
     * Returns the pause duration.
     * If it was just paused but not resumed, then the duration will be calculated with the current time.
     */
    private long getPauseDuration() {
        if (mPause > mResume) {
            return mPauseDuration + (SystemClock.elapsedRealtimeNanos() - mPause);
        }
        return mPauseDuration;
    }

    /**
     * When we return the timer duration, the duration of the pause must be taken into account.
     */
    public long getDuration(TimeUnit unit) {
        return unit.convert((SystemClock.elapsedRealtimeNanos() - mStart) - getPauseDuration(), TimeUnit.NANOSECONDS);
    }

    private static String format(long nanos, String label, String message) {
        double value = nanos;
        String suffix = "ns";

        if (nanos >= DAY_NANOS) {
            value /= DAY_NANOS;
            suffix = "d";
        }
        else if (nanos >= HOUR_NANOS) {
            value /= HOUR_NANOS;
            suffix = "h";
        }
        else if (nanos >= MIN_NANOS) {
            value /= MIN_NANOS;
            suffix = "m";
        }
        else if (nanos >= SEC_NANOS) {
            value /= SEC_NANOS;
            suffix = "s";
        } else if (nanos >= MILLI_NANOS) {
            value /= MILLI_NANOS;
            suffix = "ms";
        }

        if (label != null) {
            return String.format(Locale.ROOT, "[%.1f %s] %s: %s", value, suffix, label, message);
        }
        return String.format(Locale.ROOT, "[%.1f %s]: %s", value, suffix, message);
    }

}
