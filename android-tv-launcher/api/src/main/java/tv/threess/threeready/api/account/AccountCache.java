package tv.threess.threeready.api.account;

import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.account.model.account.IPackage;
import tv.threess.threeready.api.account.model.server.IPosterServerInfo;
import tv.threess.threeready.api.account.setting.SettingProperty;
import tv.threess.threeready.api.account.watchlist.IWatchListResponseItem;
import tv.threess.threeready.api.generic.model.BookmarkType;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.IWatchlistItem;
import tv.threess.threeready.api.generic.model.WatchlistType;
import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.vod.model.IBaseVodItem;

/**
 * Cache interface for reading/writing Config related data from/in the locale data base.
 *
 * @author David
 * @since 16.11.2021
 */
public interface AccountCache {

    interface MemoryCache extends Component {

        /**
         * @return the value for the the given in memory setting variable
         */
        <T extends Enum<T> & SettingProperty> String getMemoryCache(T key);

        /**
         * set the value for the given in memory setting variable
         */
        <T extends Enum<T> & SettingProperty> void putMemoryCache(T key, String value);
    }

    interface MassStorage extends Component {

        /**
         * @return the value for the the given setting variable
         */
        <T extends Enum<T> & SettingProperty> String getMassStorage(T key);

        /**
         * persist the value for the given key outside of the application
         * used to not loose a value in case a clear data is performed on the app
         */
        <T extends Enum<T> & SettingProperty> void putMassStorage(T key, String value);
    }

    interface Settings extends Component {
        /**
         * Query the value of a given setting.
         */
        <T extends Enum<T> & SettingProperty> String getSetting(T key);

        /**
         * Update the value of a given setting.
         */
        @WorkerThread
        <T extends Enum<T> & SettingProperty> int putSettings(Map<T, String> values);
    }

    interface Bookmarks extends Component {

        /**
         * Get the playback bookmark for content.
         *
         * @param id           The contentId for which the playback should be returned.
         * @param bookmarkType The type of the playback bookmark.
         * @return The playback bookmark for the content or an empty bookmark if it's not available.
         */
        @Nullable
        IBookmark getBookmark(String id, BookmarkType bookmarkType);

        /**
         * @param vod The vod to search VodVariant bookmarks.
         * @return The last updated VodVariant {@link IBookmark} of {@link IBaseVodItem}
         * or null if there is no VodVariant bookmark.
         */
        @Nullable
        IBookmark getVodBookmark(IBaseVodItem vod);

        /**
         * @return A limited list of the latest bookmarks which needs to be displayed as continue watching.
         */
        List<IBookmark> getContinueWatchingBookmarks();

        /**
         * Add bookmark to local database.
         *
         * @param contentId The id of the content for which the bookmark was created.
         * @param type      The type of the content for which the bookmark was created.
         * @param position  The bookmark position relative to the start in millis.
         * @param duration  The duration of the content in millis..
         */
        void addBookmark(String contentId, BookmarkType type, long position, long duration);

        /**
         * Delete the given bookmark from local database.
         *
         * @param contentIds   The id of the content for which the bookmark needs to be deleted.
         * @param bookmarkType The type of the bookmark which needs to deleted (e.g Replay, Recording etc.)
         */
        void deleteBookmarks(BookmarkType bookmarkType, String... contentIds);

        /**
         * Delete bookmarks which are older then the given timestamp.
         */
        void deleteOlderBookmarks(long olderThan);
    }

    interface WatchList extends Component {

        /**
         * Get all watchlist items from the database to display on the watchlist stripe.
         */
        List<IWatchlistItem> getWatchlistItems(ModuleDataSource dataSourceConfig, int start, int end);

        /**
         * @return watch list id by the given content id.
         */
        String getWatchlistId(String contentId);

        /**
         * Check if the given content ID exists in the watchlsit database.
         *
         * @param contentId ID of the content
         */
        Boolean isInWatchlist(String contentId);

        /**
         * Add an item to the watchlist based on type, content ID and channel ID.
         */
        void addToWatchlist(WatchlistType type, String watchListId, String contentId, String timeStamp) throws IOException;

        /**
         * Remove a watchlist item based on a content ID.
         */
        void removeFromWatchlist(WatchlistType type, String id, String contentId) throws IOException;

        /**
         * Update watch list with the given list.
         */
        void updateWatchlist(List<? extends IWatchListResponseItem> watchListResponseItems);
    }

    interface Packages extends Component {

        /**
         * @return the subscriber`s packages.
         */
        List<IPackage> getSubscriberPackages(String toPackage);

        /**
         * Insert the new packages.
         */
        void updatePackages(Collection<? extends IPackage> packages, long now);

        /**
         * @return True if the given package is subscribed.
         */
        boolean isPackageSubscribed(String toPackage);
    }

    interface PosterServers {

        /**
         * Update the poster server list with the given new list.
         */
        void updatePosterServers(List<? extends IPosterServerInfo> posterServerList);

        /**
         * @return the best possible image url.
         */
        String getBestPossibleUrl(String filteredImageUrl, int width, int height, String serverName);
    }
}
