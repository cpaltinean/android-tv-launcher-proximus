package tv.threess.threeready.api.config.model;

import java.io.Serializable;

import tv.threess.lib.di.Component;

/**
 * Feature behavior related config options.
 *
 * @author Barabas Attila
 * @since 5/14/21
 */
public interface FeatureControl extends Component, Serializable {

    /**
     * @return Include the favorite channels in the zapper.
     */
    boolean displayAllChannels();

    /**
     * @return True if the PVR is enabled in the app.
     */
    boolean isPVREnabled();

    /**
     * @return True if the application has local search.
     */
    boolean hasSearch();

    /**
     * True if the page configuration should be extended with the config from the backend.
     */
    boolean extendedPageConfig();

    /**
     * @return True if the application is in limited offline mode.
     */
    boolean isOffline();

    /**
     * @return True if the home page needs to be displayed when the device is coming out from standby.
     */
    boolean shouldOpenHomeAfterStandBy();

    /**
     * @return True if trickplay seeking is available in the app.
     */
    boolean isTrickplayEnabled();
}
