package tv.threess.threeready.api.log.model;

/**
 * Data collection log event
 *
 * @author Zsolt Bokor_
 * @since 2020.05.22
 */
public enum DataCollectionEvent implements Event.Kind {
    sendEvents,
    sendMetrics,

    Unknown;

    private static final String DOMAIN = "DataCollection";

    @Override
    public String domain() {
        return DOMAIN;
    }
}
