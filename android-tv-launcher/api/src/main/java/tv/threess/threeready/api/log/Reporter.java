/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.log;

import android.content.Context;

import androidx.annotation.Nullable;

import java.util.List;

import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.ReporterInfo;

/**
 * Base class of all event reporters.
 * Each subclass must override the sendEvent method, that maps the event that can be uploaded to the given backend.
 * Optionally the sendEvents can be also overridden to send multiple events in batch.
 *
 * @author Arnold Szabo
 * @since 2017.07.28
 */
public abstract class Reporter<E extends Enum<E> & Event.Kind, D extends Enum<D> & Event.Detail<E>> {
    protected final Context mContext;
    private String mEndpoint;
    private String mHeaders;
    private long mMaxKeepPeriod;
    private int mMaxKeepSize;
    private int mMaxBatchSize;
    private int mBatchSize;
    private long mBatchPeriod;
    private long mRandomPeriod;
    private long mTimeoutPeriod;
    private long mSendingDelay;

    public Reporter(Context context, ReporterInfo reporterInfo) {
        mContext = context;
        updateWithInfo(reporterInfo);
        // Set the previous random period and sending delay from database
        mRandomPeriod = reporterInfo.getRandomPeriod();
        mSendingDelay = reporterInfo.getSendingDelay();
    }

    /**
     * Update the reporter with the given reporter info.
     */
    public void update(ReporterInfo reporterInfo) {
        updateWithInfo(reporterInfo);
        // Generate a new sending delay if the random period changed
        if (mRandomPeriod != reporterInfo.getRandomPeriod()) {
            mRandomPeriod = reporterInfo.getRandomPeriod();
            mSendingDelay = (long) (Math.random() * mRandomPeriod) + 1;
        }
    }

    /**
     * Returns the ID of the reporter, must be unique in the database.
     * If the reporter should not save data into db, returns null.
     *
     * @return The unique identifier of the reporter.
     */
    public String getId() {
        return this.getClass().getCanonicalName();
    }

    /**
     * Checks if the given event can be sent on the given level with this reporter.
     * this function can also be used to disable the event reporter.
     */
    public abstract boolean canSend(Event<E, D> event);

    /**
     * @return True if the reporter is instant, otherwise false.
     * Instant reporters will not save the events to database.
     */
    public boolean isInstant() {
        return this.getBatchSize() == 0;
    }

    /**
     * Always persist the event and send at once.
     * @return True if the reporter is batch, otherwise false.
     */
    public boolean isBatch() {
        return this.getBatchSize() > 0;
    }

    /**
     * @return Endpoint for collector server. Complete url.
     */
    public String getEndpoint() {
        return mEndpoint;
    }

    /**
     * @return Headers which need to be included in the HTTP request.
     */
    public String getHeaders() {
        return mHeaders;
    }

    /**
     * @return Maximum duration of an event in millis before it gets obsolete, and discarded.
     */
    public long getMaxKeepPeriod() {
        return mMaxKeepPeriod;
    }

    /**
     * @return Maximum number of messages that are stored locally before the oldest is removed permanently.
     */
    public int getMaxKeepSize() {
        return mMaxKeepSize;
    }

    /**
     * @return Maximum number of events to send at once (in a single HTTP request).
     */
    public int getMaxBatchSize() {
        return mMaxBatchSize;
    }

    /**
     * Number of items to be sent in a batch:
     * - 0 indicates that the event must be sent instantly to the backend, without retry.
     * - 1 indicates that the event must be sent instantly, in case the send failed, persist to the database and try again later.
     * - n indicates to save up to n events into the database, then send them in a batch.
     *
     * @return number of items to be sent at once.
     */
    public int getBatchSize() {
        return mBatchSize;
    }

    /**
     * send the events periodically after this amount of time(in millis).
     * - 0 indicates that the event reporter does not send periodically the events
     *
     * @return timeout when to send the events in batch.
     */
    public long getBatchPeriod() {
        return mBatchPeriod;
    }

    /**
     * @return Random period in millis which is used to calculate the delay for sending.
     */
    public long getRandomPeriod() {
        return mRandomPeriod;
    }

    /**
     * @return Timeout in millis for HTTP requests.
     */
    public long getTimeoutPeriod() {
        return mTimeoutPeriod;
    }

    /**
     * @return Delay in millis for the next event sending.
     */
    public long getSendingDelay() {
        return mSendingDelay;
    }

    /**
     * Send the given event to the backend, or log it to logcat.
     * if the sending fails with an exception, and the logger is not instant,
     * the event will be persisted to db to be sent later (in batch).
     *
     * @param event event to be sent.
     * @throws Exception if the sending fails.
     */
    public abstract void sendEvent(Event<E, D> event) throws Exception;

    /**
     * Send multiple events in batch.
     * default implementation is to send the events one by one.
     */
    public void sendEvents(List<Event<E, D>> events) throws Exception {
        for (Event<E, D> event : events) {
            this.sendEvent(event);
        }
    }

    /**
     * Returns the serialized event which will be saved to database.
     *
     * @param event Event which will be serialized.
     * @return Serialized event.
     */
    @Nullable
    public abstract String serializeEvent(Event<E, D> event);

    /**
     * Update the reporter with the given reporter info.
     */
    private void updateWithInfo(ReporterInfo reporterInfo) {
        mEndpoint = reporterInfo.getEndpoint();
        mHeaders = reporterInfo.getHeaders();
        mMaxKeepPeriod = reporterInfo.getMaxKeepPeriod();
        mMaxKeepSize = reporterInfo.getMaxKeepSize();
        mMaxBatchSize = reporterInfo.getMaxBatchSize();
        mBatchSize = reporterInfo.getBatchSize();
        mBatchPeriod = reporterInfo.getBatchPeriod();
        mTimeoutPeriod = reporterInfo.getTimeoutPeriod();
    }
}
