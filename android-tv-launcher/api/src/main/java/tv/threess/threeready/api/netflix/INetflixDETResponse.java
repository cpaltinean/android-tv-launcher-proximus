package tv.threess.threeready.api.netflix;

import androidx.annotation.NonNull;

import java.io.Serializable;

import tv.threess.threeready.api.netflix.model.NetflixDETCategory;

/**
 * Basic interface vor Netflix DET response objects.
 *
 * @author David
 * @since 02.03.2022
 */
public interface INetflixDETResponse extends Serializable {

    /**
     * @return The category of the Netflix stripe (exp. MOVIES, KIDS, TV SHOWS)
     */
    @NonNull
    NetflixDETCategory getCategory();

    /**
     * @return The current ui language.
     */
    String getUILanguage();

    /**
     * @return The data which contains all the needed information about the stripe (in JSON format)
     */
    String getResponseData();

    /**
     * @return The time stamp when the refresh should be done.
     */
    long getMinRefreshAt();

    /**
     * @return The expiration date in millis.
     */
    long getExpireAt();
}