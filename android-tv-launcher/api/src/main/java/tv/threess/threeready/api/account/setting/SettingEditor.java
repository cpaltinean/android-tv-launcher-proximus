package tv.threess.threeready.api.account.setting;

import java.util.Collections;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.JsonConverter;

/**
 * Editor to synchronously update a single setting value.
 *
 * @author Barabas Attila
 * @since 2020.06.04
 */
public class SettingEditor<TKey extends Enum<TKey> & SettingProperty> {

    private final TKey mKey;
    private final BiConsumer<TKey, String> mPersist;

    public SettingEditor(TKey key, BiConsumer<TKey, String> persist) {
        mPersist = persist;
        mKey = key;
    }

    public SettingEditor(TKey key, Consumer<Map<TKey, String>> persist) {
        // fallback case, convert the value to a map containing a single item
        this(key, (k, value) -> persist.accept(Collections.singletonMap(k, value)));
    }

    protected void persist(TKey key, String value) {
        mPersist.accept(key, value);
    }

    /**
     * Store a value for the key in the cache.
     * @param value The string value which needs to be stored.
     */
    public void put(String value) {
        persist(mKey, value);
    }

    /**
     * Store a value for the key in the cache.
     * @param value The long value which needs to be stored.
     */
    public void put(long value) {
        put(String.valueOf(value));
    }

    /**
     * Store a value for the key in the cache.
     * @param value The boolean value which needs to be stored.
     */
    public void put(boolean value) {
        put(String.valueOf(value));
    }

    /**
     * Store a value for the key in the cache.
     * @param value The double value which needs to be stored.
     */
    public void put(double value) {
        put(String.valueOf(value));
    }

    /**
     * Store a value for the key in the cache.
     * @param value The object which needs to be stored.
     */
    public void put(Object value) {
        put(Components.get(JsonConverter.class).toJson(value));
    }

    /**
     * Store a value for the key in the cache.
     * @param value The value of type TEnum which needs to be stored.
     * @param <TEnum> The enumeration.
     */
    public <TEnum extends Enum<TEnum>> void put(TEnum value) {
        put(String.valueOf(value));
    }

    /**
     * Remove the value for the given key from the cache.
     */
    public void remove() {
        put((String) null);
    }
}
