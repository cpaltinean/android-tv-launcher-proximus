/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.generic.receivers;

import tv.threess.lib.di.Component;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Class for checking the Channel scan status in the application.
 */
public abstract class ScanReceiver implements Component {

    protected final List<WeakReference<Listener>> mListeners;

    public ScanReceiver() {
        mListeners = new CopyOnWriteArrayList<>();
    }

    public abstract boolean isInProgress();

    /**
     * Register a listener which will be notified when the state changes.
     * Each listener will be registered only once.
     */
    public void addListener(Listener listener) {
        removeListener(listener);
        mListeners.add(new WeakReference<>(listener));
    }

    /**
     * Remove a previously registered listener.
     * Cleanup also invalid listeners.
     */
    public void removeListener(Listener listener) {
        mListeners.removeIf((item) -> {
            Listener listenerItem = item.get();
            return listenerItem == null || listenerItem == listener;
        });
    }

    /**
     * Notify all the listeners that the current state changed
     */
    protected void notifyListeners(boolean inProgress) {
        for (WeakReference<Listener> item : mListeners) {
            Listener listener = item.get();
            if (listener == null) {
                continue;
            }
            listener.onScanStateChanged(inProgress);
        }
    }

    /**
     * Interface to notify the change.
     */
    public interface Listener {
        void onScanStateChanged(boolean inProgress);
    }
}
