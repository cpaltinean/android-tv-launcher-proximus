package tv.threess.threeready.api.pvr.model;

/**
 * Enum to identify the series recording status of a broadcast.
 *
 * @author Barabas Attila
 * @since 2018.10.25
 */
public enum  SeriesRecordingStatus {
    // Not schedule for recording.
    NONE,

    // Scheduled for future recording.
    SCHEDULED,
}
