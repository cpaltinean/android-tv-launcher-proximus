package tv.threess.threeready.api.vod.exception;

import tv.threess.threeready.api.generic.exception.BackendException;

/**
 * Exception thrown on trying to rent a VOD which is already rented.
 *
 * @author Barabas Attila
 * @since 1/8/21
 */
public class VodAlreadyRentedException extends BackendException {

    public VodAlreadyRentedException(BackendException cause) {
        super(cause);
    }
}
