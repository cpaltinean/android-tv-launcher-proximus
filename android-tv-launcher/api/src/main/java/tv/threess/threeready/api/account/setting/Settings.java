package tv.threess.threeready.api.account.setting;

import androidx.annotation.WorkerThread;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountCache;

/**
 * General Settings enum.
 *
 * @author Vlad
 * @since 2017.11.24
 */

public enum Settings implements SettingProperty, ReadableSetting {
    systemUpdateDownloadedTime,
    systemUpdateUrgency,
    systemUpdateNextCheckInTime,

    accountNumber,
    authenticated,
    deviceName,
    deviceIp,
    systemPIN,
    purchasePIN,

    imageCacheVersion,

    ottPlaybackTimeout,
    lastPlayedChannelNumber,
    lastPlayedRadioChannelNumber,

    lastConfigSyncTime, // Last time when the hub and client config was synchronized.
    lastTranslationSyncTime, // Last time when the translations were synchronized.
    lastChannelsSyncTime, // Last time when the channel list was synchronized.
    lastAccountInfoSyncTime, // Last time when the account info was synchronized.
    lastGlobalInstallSyncTime, // Last time when the boot config was synchronized.
    lastBootConfigSyncTime, // Last time when the boot config was synchronized.
    lastDRMClientSyncTime, // Last time when the DRM client was synchronized.
    lastSTBPropertiesSyncTime, // Last time when the STB properties was synchronized.
    lastBaseBroadcastSyncTime, // Last time when the base EPG cache was synchronized.
    lastBroadcastSyncTime, // The last time when the EPG was synchronized.
    lastRecordingSyncTime, // Last time when the recording list was synchronized.
    lastSeriesRecordingSyncTime, // The last time when the series recordings were synchronized.
    lastRentedVodSyncTime, // Last time when the purchased VOD list was updated.
    lastCategoryContentSyncTime, // Last time when the category content was updated.
    lastWatchlistSyncTime, // Last Time when the watchlist was synchronized.

    powerConsumptionStepDisplayed, // A flag which holds the power consumption tutorial tip status (shown/not shown)

    iptvPlaybackOptionsHash,
    isPLTVEnabled,
    maxTimeShiftBufferDuration,

    audioLanguage,
    subtitlesLanguage,

    bootId,
    forceLocalConfig,
    extendedPageConfig,

    startupFlowFinished, // Time in millis when the box finished the startup flow.
    intoStandbyTime, // Time in millis when the box last entered in standby.
    outOfStandbyTime, // Time in millis when the box last waked up from standby.
    startupTime, // The time in millis when the application started last time.
    activeStandbyDuration,

    ottPlayerLiveEdgeMs,

    //Player settings
    playerVersion,
    playerDebugOverlayEnabled,
    minimumPlaybackStartSeconds,
    bufferSize,
    lowestInitTrack,
    abrOn,
    minDurationQualityIncrease,
    maxDurationQualityDecrease,
    minDurationRetainAfterDiscard,
    lowMediaTimeTrickplay,
    highMediaTimeTrickplay,
    lowMediaTime,
    highMediaTime,
    drainWhileCharging,
    minDegradationSamples,
    manifestRetryCount,
    manifestRetryDelay,
    segmentRetryCount,
    segmentRetryDelay,
    manifestConnectTimeout,
    manifestReadTimeout,
    segmentConnectTimeout,
    segmentReadTimeout,
    bandwidthMinimum,
    maximumLimitedBitrate,
    bandwidth,

    drmConnectionTimeout,
    drmReadTimeout,
    drmAcquisitionTimeout,

    epgPastWindowLength,
    epgFutureWindowLength,

    purchasePinAttemptsNumber,
    purchasePinFailedTimestamp,

    systemPinAttemptsNumber,
    systemPinFailedTimestamp,

    quiescentRebootUptime, // The minimum uptime to apply a quiescent reboot.
    lastQuiescentRebootTime, // The last time when a quiescent reboot was triggered.
    ftiFinished,

    iptvDRMClientId,
    ottDRMClientId,
    netflixESN,
    netflixDETToken,

    // Rus Window
    remoteUpdatePollStartTime,
    remoteUpdatePollEndTime,

    // Targeted advertisement
    taCdnServerUrl,
    taCdnPublishUrl,

    tapServerUrl,
    tapVASTRequestRandomization,
    tapADRetrievalInterval,
    tapRequestRandomInterval,
    tapAdDurationTolerance,

    tasServerUrl,
    tasADRetrievalInterval,
    tasRequestRandomInterval,

    tadAccountEnabled,
    tadHardwareEnabled,
    tadDownloadLanguage,
    tadDownloadBandwidth,
    tadDownloadPercentage,
    targetedAdvertisingFlag,

    // Parental control
    enabledParentalControl,
    minAgeRating,

    // Consent Last Update
    consentLastUpdateTime,

    //Epg Layout Type
    epgLayoutType,

    internetCheckerTimeout,
    viewedHints;

    private static final AccountCache.Settings sSettingsAccess = Components.get(AccountCache.Settings.class);

    public String getName() {
        return name();
    }

    @Override
    public String getValue() {
        return sSettingsAccess.getSetting(this);
    }

    /**
     * @return Editor to update the value of the key in the cache.
     */
    @WorkerThread
    public SettingEditor<Settings> edit() {
        return new SettingEditor<>(this, sSettingsAccess::putSettings);
    }

    /**
     * @return Editor to asynchronously update the value of the key in the cache.
     */
    public SettingEditor<Settings> asyncEdit() {
        return new SettingAsyncEditor<>(this, sSettingsAccess::putSettings);
    }

    /**
     * @return Editor to updated multiple values in a batch.
     */
    @WorkerThread
    public static BatchSettingEditor<Settings> batchEdit() {
        return new BatchSettingEditor<>(sSettingsAccess::putSettings);
    }
}
