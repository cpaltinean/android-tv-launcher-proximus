package tv.threess.threeready.api.playback.model;

import java.io.Serializable;

/**
 * Data class representing a track info (e.g. audio, subtitle) track base.
 *
 * @author Karetka Mezei Zoltan
 */
public class TrackInfo implements Serializable {
    /**
     * The language of the media track as a language code (3-letter, ISO 639-2).
     */
    protected final String language;

    protected final boolean descriptive;

    public TrackInfo(String language, boolean descriptive) {
        this.language = language;
        this.descriptive = descriptive;
    }

    /**
     * @return The language of the media track as a language code (3-letter, ISO 639-2).).
     */
    public String getLanguage() {
        return language;
    }

    public boolean isDescriptive() {
        return descriptive;
    }

    public boolean sameAs(TrackInfo other) {
        return language.equals(other.language)
                && descriptive == other.descriptive;
    }

    public static TrackInfo fromLanguage(String language) {
        if (language == null) {
            return null;
        }
        return new TrackInfo(language, false);
    }
}
