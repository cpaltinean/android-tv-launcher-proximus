package tv.threess.threeready.api.home.model.page;

import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.List;

import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleType;
import tv.threess.threeready.api.log.UILog;

/**
 * Page config for single module.
 *
 * @author Barabas Attila
 * @since 6/15/21
 */
public class ModulePageConfig implements PageConfig {
    private final ModuleConfig mModuleConfig;
    private UILog.Page mPageName;

    public ModulePageConfig(ModuleConfig moduleConfig) {
        mModuleConfig = moduleConfig;
    }

    public ModulePageConfig(ModuleConfig moduleConfig, UILog.Page pageName) {
        mModuleConfig = moduleConfig;
        mPageName = pageName;
    }

    @Override
    public UILog.Page getPageName() {
        if (mPageName != null) {
            return mPageName;
        }

        if (mModuleConfig.getType() == ModuleType.COLLECTION) {
            return UILog.Page.ContentWallFullContentPage;
        }

        return PageConfig.super.getPageName();
    }

    @Override
    public String getReportName() {
        return mModuleConfig.getReportName();
    }

    @Override
    public String getReportReference() {
        return mModuleConfig.getReportReference();
    }

    @Override
    public String getId() {
        return String.valueOf(mModuleConfig.hashCode());
    }

    @Override
    public String getTitle() {
        return mModuleConfig.getTitle();
    }

    @Nullable
    @Override
    public List<ModuleConfig> getModules() {
        return Collections.singletonList(mModuleConfig);
    }

    @Override
    public PageType getType() {
        return PageType.MODULAR_PAGE;
    }
}
