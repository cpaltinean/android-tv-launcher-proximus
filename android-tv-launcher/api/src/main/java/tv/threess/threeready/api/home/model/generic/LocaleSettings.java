package tv.threess.threeready.api.home.model.generic;

import java.io.Serializable;

import tv.threess.lib.di.Component;

/**
 * Date and time related locale config options.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public interface LocaleSettings extends Component, Serializable {

    /**
     * @return The date format to display on the UI.
     */
    String getDateFormat();

    /**
     * @return The time format to display on the UI.
     */
    String getTimeFormat();

    /**
     * @return The currency to be displayed on the UI.
     */
    String getCurrency();
}
