package tv.threess.threeready.api.pc.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Enum holding parental rating data. The enums should be matching the ones in #ParentalRating object from CC code
 *
 * @author Denisa Trif
 * @since 2018.07.26
 */
public enum ParentalRating {
    Undefined(-1),

    // Returns a parental rating if it is rated for all
    RatedAll(0),

    //     Returns a parental rating if it is rated for 10 and higher
    Rated10(10),

    //     Returns a parental rating if it is rated for 12 and higher
    Rated12(12),

    //     Returns a parental rating if it is rated for 14 and higher
    Rated14(14),

    //     Returns a parental rating if it is rated for 16 and higher
    Rated16(16),

    //     Returns a parental rating if it is rated for 18 and higher
    Rated18(18);

    private final int mMinimumAge;

    ParentalRating(int ratingValue) {
        mMinimumAge = ratingValue;
    }

    /**
     * Get parental rating value
     *
     * @return - the rating value e.g -1,0, 10,12...
     */
    public int getMinimumAge() {
        return mMinimumAge;
    }

    /**
     * Checks if a parental rating is restricted so it's not undefined and it's not rated all.
     *
     * @return - true if the parental rating is not undefined / ratedall, otherwise false
     */
    public boolean isRestricted() {
        return this != Undefined && this != RatedAll;
    }

    /**
     * Use this function to find a parental rating enum value for a specific rating level.
     *
     * @param ratingValue - parental rating level value, e.g 0,-1,10,12..
     * @return Returns the rating which is closest and highest to the give value. Fallback is Undefined.
     */
    public static ParentalRating valueOf(int ratingValue) {
        if (ratingValue == -1) {
            return ParentalRating.Undefined;
        }

        List<ParentalRating> ratings = Arrays.asList(values());
        Collections.sort(ratings, (o1, o2) -> Integer.compare(o2.getMinimumAge(), o1.getMinimumAge()));

        ParentalRating currentRating = ParentalRating.Rated18;
        for (ParentalRating rating : ratings) {
            if (rating.getMinimumAge() < ratingValue) {
                break;
            }
            currentRating = rating;
        }

        return currentRating;
    }
}
