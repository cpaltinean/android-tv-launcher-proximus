package tv.threess.threeready.api.home.model.generic;

import java.io.Serializable;

/**
 * Model represents a translation for a term on a language.
 *
 * @author Barabas Attila
 * @since 5/16/21
 */
public interface Translation extends Serializable {

    /**
     * @return The key id of the term.
     */
    String getTerm();

    /**
     * @return The translation of the term.
     */
    String getDefinition();
}
