package tv.threess.threeready.api.tv.model;

/**
 * Parameters for the scanning process used to add data into the tv input framework database
 * used to scan dvb-c, dvb-t, and ip-tv channels
 */
public interface TifScanParameters {

    /**
     * type of the scan, for example "ip-vqe"
     * @return type of the scanning
     */
    String getType();

    /**
     * the ISO 3166-1 alpha-2 country code used at scan
     * @return country code
     */
    String getCountry();

    /**
     * params containing channel information that will be serialized to json and sent to the scanner
     * @return scan params
     */
    Object getParams();

    /**
     * enable or disable the scan, in case scan data is missing.
     */
    default boolean canScan() {
        return true;
    }

    /**
     * hash code of parameters used to skip the scanning in case the data did not change.
     */
    default long getParamsHash() {
        return System.currentTimeMillis();
    }
}
