/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.api.home.model.module;

/**
 * Enum represents the type of a module.
 * STRIPE - horizontal grid with items.
 * COLLECTION - vertical grid with items.
 *
 * Created by Szilard on 5/9/2017.
 */
public enum ModuleType {
    STRIPE,
    COLLECTION,
    UNDEFINED
}
