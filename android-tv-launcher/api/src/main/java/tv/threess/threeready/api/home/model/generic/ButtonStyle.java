 /*
  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
  */

package tv.threess.threeready.api.home.model.generic;

 import tv.threess.lib.di.Component;

 /**
 * Enumeration class representing the style of a button.
 *
 * @author Barabas Attila
 * @since 2018.07.06
 */
public enum ButtonStyle implements Component {
    /**
     * Button with slightly rounded corners.
     */
    DEFAULT,

    /**
     * Button with rounded bottom-left corner.
     */
    PROXIMUS,
}
