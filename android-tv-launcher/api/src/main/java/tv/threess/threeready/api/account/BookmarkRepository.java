package tv.threess.threeready.api.account;

import androidx.annotation.WorkerThread;

import io.reactivex.Completable;
import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.model.BookmarkType;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;

public interface BookmarkRepository extends Component {

    /**
     * Get the playback bookmark for content.
     *
     * @param contentItem  The content for which the playback should be returned.
     * @param bookmarkType The type of the playback bookmark.
     * @return The playback bookmark for the content or an empty bookmark if it's not available.
     */
    @WorkerThread
    IBookmark getBookmark(IContentItem contentItem, BookmarkType bookmarkType);

    /**
     * Add a playback bookmark for replay content.
     *
     * @param broadcast The replay broadcast to add bookmark for.
     * @param position  The position of the bookmark relative to the start of the content.
     * @param duration  The duration of the content in millis.
     */
    Completable addReplayBookmark(IBroadcast broadcast, long position, long duration);

    /**
     * Add a playback bookmark for recording content.
     *
     * @param recording The recording broadcast to add bookmark for.
     * @param position  The position of the bookmark relative to the start of the content.
     * @param duration  The duration of the content in millis.
     */
    Completable addRecordingBookmark(IRecording recording, long position, long duration);

    /**
     * Add a playback bookmark for vod content.
     *
     * @param vodItem  The vod item to add bookmark for.
     * @param variant  The vod variant to add bookmark for.
     * @param position The position of the bookmark relative to the start of the content.
     * @param duration The duration of the content in millis.
     */
    Completable addVodBookmark(IVodItem vodItem, IVodVariant variant, long position, long duration);
}
