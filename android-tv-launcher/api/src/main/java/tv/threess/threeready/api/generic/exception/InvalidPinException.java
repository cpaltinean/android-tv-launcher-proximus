package tv.threess.threeready.api.generic.exception;

/**
 * Exception for device registration error when the introduced PIN is not valid.
 *
 * @author Barabas Attila
 * @since 2020.04.14
 */
public class InvalidPinException extends BackendException {

    public InvalidPinException(BackendException cause) {
        super(cause);
    }
}
