package tv.threess.threeready.api.generic.exception;

/**
 * Exception received when a wrong Purchase PIN is insert and the Purchase PIN was recently changed.
 *
 * @author Daniela Toma
 * @since 2020.07.15
 */
public class PinChangedException extends InvalidPinException {

    public PinChangedException(BackendException cause) {
        super(cause);
    }
}
