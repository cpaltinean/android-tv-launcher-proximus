package tv.threess.threeready.api.home;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.List;

import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleType;

/**
 * Wrapper around the module and the data source for it.
 *
 * @author hunormadaras
 * @since 2019-05-28
 */
public final class ModuleData<TItem> {
    private final ModuleConfig mModuleConfig;

    private final String mTitle;
    private final List<TItem> mList;
    private final int mPageSize;
    private final int mOffset;
    private final boolean mHasMoreItems;

    private final List<DataSourceSelector> mSelectors;

    public ModuleData(ModuleConfig moduleConfig, List<TItem> list) {
        this(moduleConfig, null, list, null, -1, -1, false);
    }

    public ModuleData(ModuleConfig moduleConfig, List<TItem> list, boolean hasMoreItems) {
        this(moduleConfig, null, list, null, -1, -1, hasMoreItems);
    }

    public ModuleData(ModuleConfig moduleConfig, String title, List<TItem> list, List<DataSourceSelector> selectors) {
        this(moduleConfig, title, list, selectors, -1, -1, false);
    }

    public ModuleData(ModuleConfig moduleConfig, List<TItem> list, ModuleData<TItem> moduleData) {
        this(moduleConfig, moduleData.mTitle, list, moduleData.mSelectors, moduleData.mPageSize, moduleData.mOffset, moduleData.mHasMoreItems);
    }

    public ModuleData(ModuleConfig moduleConfig, String title, List<TItem> list, List<DataSourceSelector> selectors,
                      int limit, int offset, boolean hasMoreItems) {
        mModuleConfig = moduleConfig;
        mTitle = title;
        mList = list;
        mSelectors = selectors;
        mOffset = offset;
        mPageSize = limit;
        mHasMoreItems = hasMoreItems;
    }

    /**
     * @return A list of content item for the module.
     */
    public List<TItem> getItems() {
        return mList;
    }

    /**
     * @return True if the data source has more items than currently returned.
     */
    public boolean hasMoreItems() {
        return mHasMoreItems;
    }

    public int getPageSize(int defaultValue) {
        return mPageSize < 0 ? defaultValue : mPageSize;
    }

    public int getPageSize() {
        return mPageSize;
    }

    public int getOffset() {
        return mOffset;
    }

    public int getItemCount() {
        return mList.size();
    }

    /**
     * @return The available sort and filtering option for the data source.
     */
    @NonNull
    public List<DataSourceSelector> getSelectors() {
        return mSelectors == null ? Collections.emptyList() : mSelectors;
    }

    @NonNull
    public ModuleConfig getModuleConfig() {
        return mModuleConfig;
    }

    public boolean isVisible() {
        if (mModuleConfig.getType() == ModuleType.COLLECTION && mHasMoreItems) {
            return true;
        }

        return mList.size() >= mModuleConfig.getMinElements();
    }

    public String getTitle() {
        String title = mModuleConfig.getTitle();
        if (TextUtils.isEmpty(title)) {
            return mTitle;
        }
        return title;
    }

}
