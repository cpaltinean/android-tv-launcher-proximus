/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.log.model;

import java.io.Serializable;

import tv.threess.threeready.api.log.helper.ErrorType;

/**
 * Used to provide extra information about the error.
 * {@link Error#getErrorDetail()}
 *
 * @author Barabas Attila
 * @since 2017.08.28
 */
public interface ErrorDetail extends Serializable {

    ErrorType errorType();

    String bodyTranslationKey();

    String titleTranslationKey();
}
