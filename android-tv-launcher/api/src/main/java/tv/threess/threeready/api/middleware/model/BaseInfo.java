package tv.threess.threeready.api.middleware.model;

import android.database.Cursor;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.log.Log;

public abstract class BaseInfo<K extends Enum<K> & BaseInfo.Field> {
    protected final String TAG = Log.tag(getClass());

    protected final EnumMap<K, List<String>> mValues;
    protected final Class<K> mKeyType;

    protected BaseInfo(Class<K> keyType) {
        mValues = new EnumMap<>(keyType);
        mKeyType = keyType;
    }

    public int count(K field) {
        List<String> value = mValues.get(field);
        if (value == null) {
            return 0;
        }
        return value.size();
    }

    protected void put(K field, String value) {
        mValues.computeIfAbsent(field, k -> new ArrayList<>()).add(value);
    }

    public String get(K field, String defaultValue, int index) {
        List<String> value = mValues.get(field);
        if (value == null || value.size() < index) {
            return defaultValue;
        }
        return value.get(index);
    }

    public String get(K field, String defaultValue) {
        return get(field, defaultValue, 0);
    }

    public long get(K field, long defaultValue, int index) {
        String value = get(field, null, index);
        if (value == null) {
            return defaultValue;
        }
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException e) {
            Log.w(TAG, "failed to parse number", e);
            return defaultValue;
        }
    }

    public long get(K field, long defaultValue) {
        return get(field, defaultValue, 0);
    }

    public double get(K field, double defaultValue, int index) {
        String value = get(field, null, index);
        if (value == null) {
            return defaultValue;
        }
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            Log.w(TAG, "failed to parse number", e);
            return defaultValue;
        }
    }

    public double get(K field, double defaultValue) {
        return get(field, defaultValue, 0);
    }

    protected void readFrom(File file) {
        RandomAccessFile raf = null;
        try {
            raf = new RandomAccessFile(file, "r");
            mValues.clear();
            for (; ; ) {
                String line = raf.readLine();
                if (line == null) {
                    // end of file
                    break;
                }
                processLine(line);
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to read from file", e);
        } finally {
            FileUtils.closeSafe(raf);
        }
    }

    protected void readFrom(Cursor cursor, int keyColumn, int valueColumn) {
        mValues.clear();
        for (int i = 0; i < cursor.getCount(); i++) {
            if (!cursor.moveToPosition(i)) {
                continue;
            }
            K key = lookupKey(cursor.getString(keyColumn).trim());
            if (key == null) {
                continue;
            }
            String value = cursor.getString(valueColumn);
            if (value == null) {
                continue;
            }
            this.put(key, value);
        }
    }

    protected void readFrom(String command, String... args) {
        String[] cmd = new String[args.length + 1];
        cmd[0] = command;
        System.arraycopy(args, 0, cmd, 1, args.length);

        BufferedReader reader = null;
        try {
            Process process = Runtime.getRuntime().exec(cmd);
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            for (; ; ) {
                String line = reader.readLine();
                if (line == null) {
                    // end of file
                    break;
                }
                processLine(line);
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to read from command", e);
        } finally {
            FileUtils.closeSafe(reader);
        }
    }

    /**
     * Read data from current row of the cursor.
     * The name is mapped to the column name.
     */
    public void readFrom(Cursor cursor) {
        try {
            mValues.clear();
            for (int i = 0; i < cursor.getColumnCount(); i++) {
                K key = lookupKey(cursor.getColumnName(i).trim());
                if (key == null) {
                    continue;
                }
                String value = cursor.getString(i);
                if (value == null) {
                    continue;
                }
                this.put(key, value);
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to read from cursor", e);
        }
    }

    // process line `key : value`
    protected void processLine(String line) {
        int separator = line.indexOf(':');
        if (separator < 0) {
            // not a valid `key: value` pair
            return;
        }

        K key = lookupKey(line.substring(0, separator).trim());
        if (key == null) {
            return;
        }
        this.put(key, line.substring(separator + 1).trim());
    }

    protected K lookupKey(String key) {
        if (key == null) {
            return null;
        }
        for (K field : mKeyType.getEnumConstants()) {
            if (key.equals(field.getKey())) {
                return field;
            }
        }
        return null;
    }

    protected static String kibiBytes(String value) {
        if (value.endsWith("kB")) {
            value = value.substring(0, value.length() - 2).trim();
            return String.valueOf(Long.parseLong(value) * (1 << 10));
        }
        return value;
    }

    @Override
    public String toString() {
        StringBuilder details = new StringBuilder();
        for (K key : mValues.keySet()) {
            if (key.getKey() == null) {
                continue;
            }
            if (details.length() > 0) {
                details.append('\n');
            }
            details.append(key.getKey()).append(": `").append(mValues.get(key)).append("`");
        }
        return this.getClass().getSimpleName() + "\n" + details;
    }

    public interface Field {
        String getKey();
    }
}
