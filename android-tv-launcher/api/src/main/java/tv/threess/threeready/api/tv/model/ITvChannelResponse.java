/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.tv.model;

import java.io.Serializable;
import java.util.List;

/**
 * Response entity from backend.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.13
 */
public interface ITvChannelResponse<T extends TvChannel> {

    List<T> getChannels();

    Iterable<Option> getOptions(T channel);

    TifScanParameters getScanParameters();

    interface Option extends Serializable {

        StreamType getPlaybackType();

        String getPlaybackData();

        String getPlaybackExtra();
    }

    /**
     * Playback option for app type model.
     *
     * @author Barabas Attila
     * @since 2017.07.26
     */
     class App implements Option {

        public static final String NAME = "app";

        private final String mPackageId;

        // constructor used to fake app playback options
        public App(String packageId) {
            mPackageId = packageId;
        }

        @Override
        public StreamType getPlaybackType() {
            return StreamType.App;
        }

        @Override
        public String getPlaybackData() {
            return mPackageId;
        }

        @Override
        public String getPlaybackExtra() {
            return null;
        }
    }
}
