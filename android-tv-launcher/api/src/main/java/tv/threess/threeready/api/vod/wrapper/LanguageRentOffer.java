package tv.threess.threeready.api.vod.wrapper;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.vod.model.CurrencyType;
import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodVariant;

/**
 * This class remaps {@link VodRentOffers#getVariantOfferMap()} based on
 * language order which contains variant and it's offer.
 *
 * Created by Bartalus Csaba - Zsolt
 * since 2022.04.07
 */
public class LanguageRentOffer {

    private final Map<String, IRentOffer> mVariantOfferMap;
    private ArrayList<String> mLanguages;
    private final int mCredit;
    private final IVodItem mVod;

    /**
     * Initializes class variables and build up the language list.
     *
     * @param shopLanguage    shop language from config.
     * @param credit          available credit
     * @param variantOfferMap map contains vod variant as key and corresponding offer.
     */
    public LanguageRentOffer(String shopLanguage, IVodItem vod, int credit, Map<String, IRentOffer> variantOfferMap) {
        mCredit = credit;
        mVariantOfferMap = variantOfferMap;
        mVod = vod;
        fillLanguagesList(shopLanguage, variantOfferMap);
    }

    /**
     * This function saves the shop language and fill the {@see mShopLanguageOrder} list with languages
     * sort order.
     *
     * @param shopLanguage    shop language which is come from config.
     * @param variantOfferMap contains variants and it's corresponding offer.
     */
    private void fillLanguagesList(String shopLanguage, Map<String, IRentOffer> variantOfferMap) {
        mLanguages = new ArrayList<>();

        for (String variantId : variantOfferMap.keySet()) {
            IVodVariant variant = mVod.getVariantById(variantId);

            if (variant == null) {
                continue;
            }

            List<String> audioLanguages = variant.getAudioLanguages();

            if (variant.isFree()) {
                continue;
            }

            for (String language : audioLanguages) {
                if (mLanguages.contains(language)) {
                    continue;
                }

                mLanguages.add(language);
            }
        }
        int i = 0;
        if (LocaleUtils.FRENCH_AND_DUTCH_SHOP_LANGUAGE.equals(shopLanguage)) {
            i += swapTo(i, LocaleUtils.DUTCH_SHOP_LANGUAGE);
            i += swapTo(i, LocaleUtils.FRENCH_SHOP_LANGUAGE);
        } else if (LocaleUtils.DUTCH_LANGUAGE_CODE.equals(shopLanguage)) {
            i += swapTo(i, LocaleUtils.DUTCH_SHOP_LANGUAGE);
        } else {
            i += swapTo(i, LocaleUtils.FRENCH_SHOP_LANGUAGE);
        }

        swapTo(i, LocaleUtils.ORIGINAL_VERSION_LANGUAGE_CODE);
    }

    private int swapTo(int i, String lang) {
        int idx = mLanguages.indexOf(lang);

        if (idx == i) {
            return 1;
        }

        if (idx > i) {
            mLanguages.set(idx, mLanguages.get(i));
            mLanguages.set(i, lang);
            return 1;
        }
        return 0;
    }

    /**
     * Compares models for equal quality and price.
     *
     * @param includedModel model that is already in a list.
     * @param newModel      new model.
     * @return true if need to include the model.
     */
    private boolean shouldSkipVariant(VariantRentOffer includedModel, VariantRentOffer newModel) {
        IVodVariant includedVariant = includedModel.getVodVariant();
        IRentOffer includedOffer = includedModel.getOffer();

        IVodVariant newVariant = newModel.getVodVariant();
        IRentOffer newOffer = newModel.getOffer();

        IVodPrice newOfferPrice = newOffer.getPriceForCurrency(CurrencyType.Euro);
        IVodPrice includedOfferPrice = includedOffer.getPriceForCurrency(CurrencyType.Euro);
        if (newOfferPrice == null || includedOfferPrice == null) {
            return true;
        }

        return newVariant.formatEquals(includedVariant)
                && includedOfferPrice.getCurrencyAmount() >= newOfferPrice.getCurrencyAmount();
    }

    /**
     * Compares variants for equal quality and price.
     *
     * @param includedModel model that is already in a list.
     * @param newModel      new model.
     * @return true if need to replace the model.
     */
    private boolean shouldReplaceVariant(VariantRentOffer includedModel, VariantRentOffer newModel) {
        IVodVariant includedVariant = includedModel.getVodVariant();
        IRentOffer includedOffer = includedModel.getOffer();

        IVodVariant newVariant = newModel.getVodVariant();
        IRentOffer newOffer = newModel.getOffer();

        IVodPrice newOfferPrice = newOffer.getPriceForCurrency(CurrencyType.Euro);
        IVodPrice includedOfferPrice = includedOffer.getPriceForCurrency(CurrencyType.Euro);
        if (newOfferPrice == null || includedOfferPrice == null) {
            return false;
        }

        return includedOfferPrice.getCurrencyAmount() < newOfferPrice.getCurrencyAmount()
                && includedVariant.formatEquals(newVariant);

    }

    /**
     * @return returns available credit.
     */
    public int getCredit() {
        return mCredit;
    }

    /**
     * @param searchedLanguage The returned list filtered variants based on this language.
     * @return a {@link List} with {@see mVariantOfferMap} object from the map by searchedLanguage.
     */
    @NonNull
    public List<VariantRentOffer> getVariantRentOfferByLanguage(@NonNull String searchedLanguage) {
        List<VariantRentOffer> variantRentOffers = new ArrayList<>();

        // go through all pairs list
        for (String variantId : mVariantOfferMap.keySet()) {
            IVodVariant variant = mVod.getVariantById(variantId);

            if (variant == null) {
                continue;
            }

            // the variant is free for example if we have subscription for it
            if (variant.isFree()) {
                continue;
            }

            // go through all language profiles available in each pair
            for (String audioLanguage : variant.getAudioLanguages()) {
                //check if the language is equal to searched language
                if (!searchedLanguage.equals(audioLanguage)) {
                    continue;
                }

                VariantRentOffer variantRentOffer = new VariantRentOffer(variant, mVariantOfferMap.get(variantId));

                if (variantRentOffers.isEmpty()) {
                    variantRentOffers.add(variantRentOffer);
                    continue;
                }

                // language has already included in the list
                // go through all the pairs which were already included
                for (int i = 0; i < variantRentOffers.size(); i++) {

                    // check if it is needed to replace with a new variantOffer
                    if (shouldReplaceVariant(variantRentOffers.get(i), variantRentOffer)) {
                        variantRentOffers.set(i, variantRentOffer);
                        break;
                    }

                    // check if it is needed to skip new variant offer
                    if (shouldSkipVariant(variantRentOffers.get(i), variantRentOffer)) {
                        break;
                    }

                    // add a new variantOffer only when all included variantOffer checked
                    if (i == variantRentOffers.size() - 1) {
                        variantRentOffers.add(variantRentOffer);
                        break;
                    }
                }
            }
        }

        // Sort list by variant quality
        variantRentOffers.sort(Comparator.comparingInt(o -> o.getVodVariant().getHighestQuality().ordinal()));
        return variantRentOffers;
    }

    /**
     * @return returns the languages.
     */
    @NonNull
    public ArrayList<String> getLanguages() {
        return mLanguages;
    }

    /**
     * @return first language.
     */
    @NonNull
    public String getFirstLanguage() {
        ArrayList<String> languages = getLanguages();

        if (languages.isEmpty()) {
            return "";
        }

        return languages.get(0);
    }
}
