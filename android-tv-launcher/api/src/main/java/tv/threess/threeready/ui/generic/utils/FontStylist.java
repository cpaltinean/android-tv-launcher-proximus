/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.generic.utils;

import android.content.Context;
import android.graphics.Typeface;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import java.util.HashMap;
import java.util.Map;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.home.model.local.FontConfig;
import tv.threess.threeready.api.home.model.local.FontType;

/**
 * Utility class to get the font config for the given type.
 *
 * @author Barabas Attila
 * @since 2018.07.10
 */
public class FontStylist implements Component {

    private final Map<FontType, FontConfig> mFontConfigMap = new HashMap<>();
    private final Map<FontType, Typeface> mFontTypefaceMap = new HashMap<>();
    private final Context mContext;

    public FontStylist(Context context, FontConfig[] fontConfigs) {
        mContext = context;
        for (FontConfig fontConfig : fontConfigs) {
            mFontConfigMap.put(fontConfig.getFontType(), fontConfig);
        }
    }

    /**
     * @param fontStyle The type of the font.
     * @return The font display configuration for the give font type.
     */
    public FontConfig getFontConfig(FontType fontStyle) {
        FontConfig config = mFontConfigMap.get(fontStyle);
        if (config != null) {
            return config;
        }

        // Fallback to regular
        return mFontConfigMap.get(FontType.REGULAR);
    }

    /**
     * @param fontConfig The font config to request the type face for.
     * @return The type face for the corresponding font config.
     */
    @Nullable
    public Typeface getFont(FontConfig fontConfig) {
        // Check the case first
        FontType fontType = fontConfig.getFontType();
        if (mFontTypefaceMap.containsKey(fontType)) {
            return mFontTypefaceMap.get(fontType);
        }

        // Get font id by name.
        int fontId = mContext.getResources()
                .getIdentifier(fontConfig.getName(), "font", mContext.getPackageName());
        Typeface typeface = ResourcesCompat.getFont(mContext, fontId);
        mFontTypefaceMap.put(fontType, typeface);
        return typeface;
    }


}
