/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.generic.helper;

import android.content.ContentResolver;
import android.net.Uri;
import android.text.TextUtils;

import tv.threess.threeready.api.log.Log;

/**
 * Enhanced UriMatcher class.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.14
 */
public class UriMatcher<T extends Enum<T>> extends android.content.UriMatcher {
    private static final String TAG = Log.tag(UriMatcher.class);

    protected final Uri baseUri;
    private final Class<? extends T> enumClass;

    public UriMatcher(Uri baseUri) {
        this(baseUri, null);
    }

    public UriMatcher(String authority, Class<T> enumClass) {
        this(new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT).authority(authority).build(), enumClass);
    }

    public UriMatcher(Uri baseUri, Class<T> enumClass) {
        super(NO_MATCH);
        this.baseUri = baseUri;
        this.enumClass = enumClass;
    }

    public Uri getURI(String... paths) {
        Uri.Builder builder = this.baseUri.buildUpon();
        for (String path: paths) {
            builder.appendPath(path);
        }
        return builder.build();
    }

    public Uri getURIParts(String paths) {
       return getURI(paths.split("/"));
    }

    public UriMatcher add(int code, Uri uri, String... paths) {
        String path = uri.getPath();
        if (paths != null && paths.length > 0) {
            if (path != null) {
                path += '/' + TextUtils.join("/", paths);
            } else {
                path = TextUtils.join("/", paths);
            }
        }
        this.addURI(uri.getAuthority(), path, code);
        if (!baseUri.getAuthority().equals(uri.getAuthority())) {
            Log.w(TAG, "Authority mismatch[" + baseUri.getAuthority() + "] for uri: " + uri, new Exception());
        }
        return this;
    }

    public UriMatcher add(int code, String... paths) {
        return this.add(code, getURI(paths));
    }

    public UriMatcher<T> add(T code, Uri uri, String... paths) {
        this.add(code.ordinal(), uri, paths);
        return this;
    }

    public UriMatcher<T> add(T code, String... paths) {
        this.add(code.ordinal(), paths);
        return this;
    }

    public UriMatcher<T> addParts(T code, String paths) {
        return this.add(code, paths.split("/"));
    }

    public int matchInt(Uri uri) {
        int q = super.match(uri);
        if (q == NO_MATCH) {
            throw new UnsupportedOperationException("Unsupported URI [" + uri + "]");
        }
        return q;
    }

    public T matchEnum(Uri uri, T defValue) {
        int q = super.match(uri);
        if (q == NO_MATCH) {
            return defValue;
        }
        return enumClass.getEnumConstants()[q];
    }

    public T matchEnum(Uri uri) {
        int q = super.match(uri);
        if (q == NO_MATCH) {
            throw new UnsupportedOperationException("Unsupported URI [" + uri + "]");
        }
        return enumClass.getEnumConstants()[q];
    }
}
