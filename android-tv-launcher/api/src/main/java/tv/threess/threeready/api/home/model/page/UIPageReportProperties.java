package tv.threess.threeready.api.home.model.page;

/**
 * Interface used for page related UI event report properties.
 *
 * @author David
 * @since 21.12.2021
 */
public interface UIPageReportProperties {

    /**
     * @return page title for ui reporter.
     */
    String getReportName();

    /**
     * @return page id for ui reporter.
     */
    String getReportReference();
}
