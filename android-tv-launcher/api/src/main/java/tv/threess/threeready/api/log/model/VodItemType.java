package tv.threess.threeready.api.log.model;

/**
 * Enum for VOD item type.
 *
 * @author Zsolt Bokor_
 * @since 2020.07.22
 */
public enum VodItemType {
    TVOD, // Transaction VOD
    FVOD, // Free VOD, Transaction VOD with price: 0 EUR
    SVOD, // Subscription VOD
    UNDEFINED
}
