/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.log.model;

/**
 * Use to identify the component which should receive and handle the error.
 *
 * @author Barabas Attila
 * @since 2017.08.28
 */
public enum ErrorHandler {
    /**
    * Provide the error to the ui components,
    * but do not show a generic error message in case is not consumed.
    */
    CUSTOM,

    /**
     *  Provide the error to the ui components
     *  and show a generic error message in case is not consumed by any component.
     */
    GENERIC,

    /**
     * The error is not provided to ui components.
     */
    NONE
}
