package tv.threess.threeready.api.middleware.model;

import java.util.ArrayList;
import java.util.HashMap;

import tv.threess.threeready.api.tv.model.ITvChannelResponse;

/**
 * List of channels requested from tiff database after a DvbC, DvbT or ipTv scanning
 * contains also the mappings to match theses channels with the channels requested from backend
 */
public class TifChannelList {
    private final ArrayList<TifChannel> mTifChannels = new ArrayList<>();
    private final HashMap<String, TifChannel> mTifMapping = new HashMap<>();

    public int size() {
        return mTifChannels.size();
    }

    public TifChannel get(int index) {
        return mTifChannels.get(index);
    }

    public TifChannel get(String playbackData) {
        return mTifMapping.get(playbackData);
    }

    public TifChannel get(ITvChannelResponse.Option option) {
        return mTifMapping.get(option.getPlaybackData());
    }

    public void add(TifChannel tifChannel) {
        mTifChannels.add(tifChannel);
        mTifMapping.put(tifChannel.getData(), tifChannel);
    }
}
