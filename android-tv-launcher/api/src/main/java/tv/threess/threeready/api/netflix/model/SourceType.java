/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.netflix.model;

/**
 * The source_type is passed through an intent
 * when the Netflix app starts from different sections of the application.
 *
 * @author Barabas Attila
 * @since 2017.10.12
 */
public enum SourceType {
    NetflixButton(1),
    FeaturedApp(2),
    HomeIcon(3),
    Banner(9),
    Channel(17),
    DirectChannel(18),
    FavoriteChannel(26);

    private final int mCode;

    SourceType(int code) {
        mCode = code;
    }

    public int getCode() {
        return mCode;
    }
}
