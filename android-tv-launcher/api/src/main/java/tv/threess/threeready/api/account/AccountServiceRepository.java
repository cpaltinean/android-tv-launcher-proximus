/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.account;

import java.io.IOException;

import tv.threess.lib.di.Component;

/**
 * Extension methods that are invoked from the config service to take project specific actions.
 *
 * @author Barabas Attila
 * @since 2018.04.30
 */
public interface AccountServiceRepository extends Component {

    /**
     * Check and validate all the cache information with the backend.
     * @throws IOException In case of errors.
     */
    void performCaching() throws Exception;

    /**
     * Update endpoints and other backend related config options.
     * @throws Exception In case of errors.
     */
    void updateBootConfig() throws Exception;

    /**
     * Update the global install file.
     * @throws IOException In case of errors.
     */
    void updateGlobalInstall() throws Exception;

    /**
     * Update the account relating information from backend.
     * @throws Exception In case of errors.
     */
    void updateAccountInfo() throws Exception;

    /**
     * Update the account related information from backend (without cache check, aka forced update).
     * If there are DTV package changes (including locked state) it will refresh the channels list also.
     *
     * @throws Exception In case of errors.
     */
    void updateAccountPackagesInfo() throws Exception;

    /**
     * Update the STB properties from backend.
     * @throws IOException In case of errors.
     */
    void updateSTBProperties() throws Exception;

    /**
     * Update the device current status and registration information.
     * @throws IOException In case of errors.
     */
    void updateDeviceRegistration() throws IOException;

    /**
     * Update the device current status and registration information including netflix.
     * @param netflixESN The netflix serial number of the device.
     * @throws IOException In case of errors.
     */
    void updateDeviceRegistration(String netflixESN) throws IOException;

    /**
     * Downloads and updates the watchlist items stored in the database from the backend.
     * @throws Exception In case of errors.
     */
    void updateWatchlist() throws Exception;
}
