/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.middleware;

/**
 *  "automatic”: the new firmware version will be applied immediately after it's downloaded,
 *  the user doesn't have the capability to prevent it
 *  "optional" : the user will be notified when the system image is downloaded and can choose to apply it immediately
 *  or do it automatically when the device goes to standby.
 *  "mandatory”: the user will be notified when the system image is downloaded and can choose to apply it immediately
 *  or do it automatically when the device goes to standby, mandatory system updates will always by downloaded and applied during the FTI
 *
 * @author Barabas Attila
 * @since 2018.03.28
 */

public enum UpdateUrgency {
    Mandatory,
    Optional,
    Automatic;

    public static UpdateUrgency findByName(final String name, UpdateUrgency defaultValue) {
        for (UpdateUrgency updateUrgency : UpdateUrgency.values()) {
            if (updateUrgency.name().equalsIgnoreCase(name)) {
                return updateUrgency;
            }
        }
        return defaultValue;
    }

}
