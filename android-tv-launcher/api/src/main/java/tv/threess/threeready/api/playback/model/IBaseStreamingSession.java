package tv.threess.threeready.api.playback.model;

import java.io.Serializable;

/**
 * Base streaming session for all of the playback types.
 *
 * @author Andor Lukacs
 * @since 2019-12-19
 */
public interface IBaseStreamingSession extends Serializable {

    /**
     * @return Unique identifier of the streaming session.
     */
    String getId();
}
