/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.utils;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothProfile;

import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import tv.threess.threeready.api.log.Log;

/**
 * Helper class to retrieve characteristics from a bluetooth device
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.11.02
 */
public abstract class DeviceCharacteristicReader<T> extends BluetoothGattCallback implements Callable<T> {

    private static final String TAG = DeviceCharacteristicReader.class.getSimpleName();

    private static final long TIMEOUT = TimeUnit.SECONDS.toMillis(5);

    private final FutureTask<T> mTask = new FutureTask<>(this);
    private volatile Exception mError = null;
    private volatile T mResult = null;

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        Log.v(TAG, "GATT.onConnectionStateChange(status: " + status + ", newState: " + newState + ")");
        super.onConnectionStateChange(gatt, status, newState);

        if (gatt == null || status != BluetoothGatt.GATT_SUCCESS) {
            onError(new IllegalStateException("GATT operation failed, status: " + status));
            return;
        }
        try {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                gatt.discoverServices();
            }
        } catch (Exception e) {
            onError(e);
        }
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        Log.v(TAG, "GATT.onServicesDiscovered(status: " + status + ")");
        super.onServicesDiscovered(gatt, status);

        if (gatt == null || status != BluetoothGatt.GATT_SUCCESS) {
            onError(new IllegalStateException("GATT operation failed, status: " + status));
            return;
        }

        try {
            gatt.readCharacteristic(gatt
                    .getService(getServiceToRead())
                    .getCharacteristic(getCharacteristicToRead())
            );
        } catch (Exception e) {
            onError(e);
        }
    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        Log.v(TAG, "GATT.onCharacteristicRead(status: " + status + ", characteristic: " + characteristic + ")");
        super.onCharacteristicRead(gatt, characteristic, status);

        if (gatt == null || status != BluetoothGatt.GATT_SUCCESS) {
            onError(new IllegalStateException("GATT operation failed, status: " + status));
            return;
        }

        try {
            mResult = onResult(characteristic);
            mTask.run();
        } catch (Exception e) {
            onError(e);
        }
    }

    @Override
    public T call() {
        Log.v(TAG, "GATT.call(result: " + mResult + ")");
        return mResult;
    }

    protected void onError(Exception error) {
        Log.v(TAG, "GATT.onError(error: " + error + ")");
        mError = error;
        mTask.cancel(true);
    }

    protected abstract T onResult(BluetoothGattCharacteristic characteristic);

    protected abstract UUID getCharacteristicToRead();

    protected abstract UUID getServiceToRead();

    public T get() throws InterruptedException, ExecutionException, TimeoutException {
        try {
            return mTask.get(TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (CancellationException e) {
            if (mError != null) {
                e.initCause(mError);
            }
            throw e;
        }
    }

}
