package tv.threess.threeready.api.gms.model;

import androidx.annotation.Nullable;

import java.io.Serializable;

import tv.threess.threeready.api.generic.model.IImageSource;

/**
 * Information about a featured application.
 *
 * @author Barabas Attila
 * @since 6/25/21
 */
public interface EditorialAppInfo extends Serializable {

    /**
     * @return The name of the application.
     */
    String getName();

    /**
     * The unique package id of the application.
     */
    String getPackageName();

    /**
     * @return The banner of the application
     */
    @Nullable
    IImageSource getBanner();

}
