package tv.threess.threeready.api.gms.model;

import androidx.annotation.Nullable;

import java.util.List;

import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.pc.model.ParentalRating;

/**
 * Specific content item implementation for app start command.
 *
 * @author Zsolt Bokor_
 * @since 2020.10.05
 */
public class AppContentItem implements IContentItem {

    private final String mPackageName;

    public AppContentItem(String packageName) {
        mPackageName = packageName;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Nullable
    @Override
    public Integer getSeasonNumber() {
        return null;
    }

    @Nullable
    @Override
    public Integer getEpisodeNumber() {
        return null;
    }

    @Override
    public String getEpisodeTitle() {
        return null;
    }

    @Override
    public String getReleaseYear() {
        return null;
    }

    @Override
    public long getDuration() {
        return 0;
    }

    @Nullable
    @Override
    public String getSeriesId() {
        return null;
    }

    @Override
    public List<String> getActors() {
        return null;
    }

    @Override
    public List<String> getDirectors() {
        return null;
    }

    @Override
    public List<String> getGenres() {
        return null;
    }

    @Override
    public String getId() {
        return mPackageName;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public ParentalRating getParentalRating() {
        return null;
    }

    @Override
    public List<IImageSource> getImageSources() {
        return null;
    }

    @Override
    public boolean hasDescriptiveAudio() {
        return false;
    }

    @Override
    public boolean hasDescriptiveSubtitle() {
        return false;
    }
}
