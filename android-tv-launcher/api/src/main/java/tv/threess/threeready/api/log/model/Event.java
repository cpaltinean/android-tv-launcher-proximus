/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.log.model;

import android.os.Bundle;
import tv.threess.threeready.api.BuildConfig;
import tv.threess.threeready.api.log.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Event model class that extends the event with dynamically added details
 *
 * @author Arnold Szabo
 * @since 2017.07.20
 */
public final class Event<E extends Enum<E> & Event.Kind, D extends Event.Detail<E>> extends BaseEvent {

    private final List<Bundle> mDetails;

    public Event(long id, String name, String domain, long timestamp, String value,
                 Log.Level logLevel) {
        super(id, name, domain, timestamp, value, logLevel);
        mDetails = Collections.singletonList(new Bundle());
    }

    public Event(long id, String name, String domain, long timestamp, String value) {
        super(id, name, domain, timestamp, value, Log.Level.Debug);
        mDetails = Collections.singletonList(new Bundle());
    }

    public Event(String name, String domain, long timestamp, Bundle details) {
        super(-1, name, domain, timestamp, "", Log.Level.Debug);
        mDetails = Collections.singletonList(details);
    }

    public Event(E event, Log.Level logLevel) {
        super(-1, event.name(), event.domain(), System.currentTimeMillis(), "", logLevel);
        mDetails = Collections.singletonList(new Bundle());
    }

    public Event(E event, long mTimestamp) {
        super(-1, event.name(), event.domain(), mTimestamp, "", Log.Level.Debug);
        mDetails = Collections.singletonList(new Bundle());
    }

    public Event(E event) {
        super(-1, event.name(), event.domain(), System.currentTimeMillis(), "", Log.Level.Debug);
        mDetails = Collections.singletonList(new Bundle());
    }

    @SafeVarargs
    public Event(Event<E, D> event, Event<E, D>... events) {
        super(event.getId(), event.getName(), event.getDomain(), event.getTimestamp(), event.getValue(),
                event.getLogLevel());
        mDetails = new ArrayList<>();
        mDetails.add(event.getDetails());
        for (Event<E, D> currentEvent : events) {
            if (currentEvent == null) {
                continue;
            }
            mDetails.add(currentEvent.getDetails());
        }
    }

    /**
     * Check if the current event has a detail associated to the given key.
     * @param key name of the detail.
     * @return true if the event contains a detail associated to the given key.
     */
    public boolean hasDetail(D key) {
        for (Bundle detail : mDetails) {
            if (detail.containsKey(key.name())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the detail associated to the given key.
     * @param key name of the detail.
     * @return value of the detail for the given key as string.
     */
    public String getDetail(D key) {
        Object value = getDetail(key.name());
        if (value instanceof String || value == null) {
            // the value was persisted and read back from db?
            return (String) value;
        }
        return String.valueOf(value);
    }

    /**
     * Get the detail associated to the given key.
     * @param key name of the detail.
     * @param defValue default value to be returned in case the detail is missing.
     * @return value of the detail for the given key as string.
     */
    public String getDetail(D key, String defValue) {
        Object value = getDetail(key.name());
        if (value instanceof String) {
            return (String) value;
        }
        return defValue;
    }

    /**
     * Get the detail associated to the given key.
     * @param key name of the detail.
     * @param defValue default value to be returned in case the detail is missing.
     * @return value of the detail for the given key as long.
     */
    public long getDetail(D key, long defValue) {
        Object value = getDetail(key.name());
        if (value instanceof String) {
            // the value was persisted and read back from db?
            return Long.parseLong((String) value);
        }
        if (value == null) {
            return defValue;
        }
        return (Long) value;
    }

    /**
     * Get the detail associated to the given key.
     * @param key name of the detail.
     * @param defValue default value to be returned in case the detail is missing.
     * @return value of the detail for the given key as double.
     */
    public double getDetail(D key, double defValue) {
        Object value = getDetail(key.name());
        if (value instanceof String) {
            // the value was persisted and read back from db?
            return Double.parseDouble((String) value);
        }
        if (value == null) {
            return defValue;
        }
        return (Double) value;
    }

    /**
     * Get the detail associated to the given key.
     * @param key name of the detail.
     * @param defValue default value to be returned in case the detail is missing.
     * @return value of the detail for the given key as boolean.
     */
    public boolean getDetail(D key, boolean defValue) {
        Object value = getDetail(key.name());
        if (value instanceof String) {
            // the value was persisted and read back from db?
            return Boolean.parseBoolean((String) value);
        }
        if (value == null) {
            return defValue;
        }
        return (Boolean) value;
    }

    /**
     * Get the detail associated to the given key.
     * @param key name of the detail.
     * @param enumClass the value of the detail is a value from this enumeration class.
     * @return value of the detail for the given key as Enum.
     */
    public <V extends Enum<V>> V getDetail(D key, Class<V> enumClass) {
        Object value = getDetail(key.name());
        if (!(value instanceof String)) {
            // storing enumeration values as String
            return null;
        }
        for (V val : enumClass.getEnumConstants()) {
            if (value.equals(val.name())) {
                return val;
            }
        }
        return null;
    }

    /**
     * Get the detail associated to the given key.
     * @param key name of the detail.
     * @param defValue default value to be returned in case the detail is missing.
     * @return value of the detail for the given key of the enumeration type.
     */
    public <V extends Enum<V>> V getDetail(D key, V defValue) {
        V value = getDetail(key, defValue.getDeclaringClass());
        if (value == null) {
            return defValue;
        }
        return value;
    }

    /**
     * Add or replace the value associated to the key.
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the current object.
     */
    public Event<E, D> putDetail(D key, String value) {
        if (value == null || value.isEmpty()) {
            return this;
        }
        mDetails.get(0).putString(key.name(), value);
        return this;
    }

    /**
     * Add or replace the value associated to the key.
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the current object.
     */
    public Event<E, D> putDetail(D key, Number value) {
        if (value == null) {
            return this;
        }
        return putDetail(key, String.valueOf(value));
    }

    /**
     * Add or replace the value associated to the key.
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the current object.
     */
    public Event<E, D> putDetail(D key, long value) {
        mDetails.get(0).putLong(key.name(), value);
        return this;
    }

    /**
     * Add or replace the value associated to the key.
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the current object.
     */
    public Event<E, D> putDetail(D key, double value) {
        mDetails.get(0).putDouble(key.name(), value);
        return this;
    }

    /**
     * Add or replace the value associated to the key.
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the current object.
     */
    public Event<E, D> putDetail(D key, boolean value) {
        mDetails.get(0).putBoolean(key.name(), value);
        return this;
    }

    /**
     * Add or replace the value associated to the key.
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the current object.
     */
    public Event<E, D> putDetail(D key, Enum<?> value) {
        mDetails.get(0).putString(key.name(), value == null ? null : value.name());
        return this;
    }

    /**
     * Add but don't replace the value associated to the key.
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the current object.
     */
    public Event<E, D> addDetail(D key, String value) {
        if (this.hasDetail(key)) {
            return this;
        }
        return putDetail(key, value);
    }

    /**
     * Add value as String but don't replace the value associated to the key.
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the current object.
     */
    public Event<E, D> addDetailAsString(D key, Object value) {
        if (this.hasDetail(key)) {
            return this;
        }
        if (value == null) {
            return this;
        }
        return putDetail(key, String.valueOf(value));
    }

    /**
     * Add but don't replace the value associated to the key.
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the current object.
     */
    public Event<E, D> addDetail(D key, Number value) {
        if (this.hasDetail(key)) {
            return this;
        }
        return putDetail(key, value);
    }

    /**
     * Add but don't replace the value associated to the key.
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the current object.
     */
    public Event<E, D> addDetail(D key, long value) {
        if (this.hasDetail(key)) {
            return this;
        }
        return putDetail(key, value);
    }

    /**
     * Add but don't replace the value associated to the key.
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the current object.
     */
    public Event<E, D> addDetail(D key, double value) {
        if (this.hasDetail(key)) {
            return this;
        }
        return putDetail(key, value);
    }

    /**
     * Add but don't replace the value associated to the key.
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the current object.
     */
    public Event<E, D> addDetail(D key, boolean value) {
        if (this.hasDetail(key)) {
            return this;
        }
        return putDetail(key, value);
    }

    /**
     * Add but don't replace the value associated to the key.
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the current object.
     */
    public Event<E, D> addDetail(D key, Enum<?> value) {
        if (this.hasDetail(key)) {
            return this;
        }
        return putDetail(key, value);
    }

    /**
     * Get all the details of the event.
     *
     * @return details of the event.
     */
    public Bundle getDetails() {
        return mDetails.get(0);
    }

    /**
     * Add or replace the value from the given bundle, associated to the key.
     * @param key key with which the specified value is to be associated
     * @param values bundle of values(usually another events detail) to get vhe value from
     * @return the current object.
     */
    public Event<E, D> copyDetail(D key, Bundle values) {
        Object value = values.get(key.name());
        if (value == null) {
            return this;
        }
        return putDetail(key, String.valueOf(value));
    }

    /**
     * @param name Name of the key.
     * @return Object value for the given key.
     */
    private Object getDetail(String name) {
        for (Bundle detail : mDetails) {
            Object value = detail.get(name);
            if (value != null) {
                return value;
            }
        }
        return null;
    }

    public void remove(String name) {
        mDetails.get(0).remove(name);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("Event(domain: ").append(getDomain())
                .append(", event: ").append(getName())
                .append(", timestamp: ").append(Log.timestamp(getTimestamp()))
                .append(")");

        if (BuildConfig.DEBUG) {
            for (String  key : mDetails.get(0).keySet()) {
                result.append("\n").append(key).append(": ").append(mDetails.get(0).get(key));
            }
        }
        return result.toString();
    }

    /**
     * Each event kind must have a name and a domain.
     * ex: `PlaybackFailed` in the `Playback` domain.
     */
    public interface Kind extends Serializable {

        String name();

        String domain();

    }

    /**
     * Each event can have its own set of details.
     * ex: `ChannelId` can be a detail of a playback event.
     */
    public interface Detail<T extends Enum<T> & Kind> extends Serializable {

        String name();

    }
}
