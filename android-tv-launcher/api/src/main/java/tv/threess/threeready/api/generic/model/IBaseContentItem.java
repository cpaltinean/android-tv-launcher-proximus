package tv.threess.threeready.api.generic.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.pc.model.ParentalRating;

/**
 * Base interface for content items and series. This includes Tv Broadcast items, VOD Series and VOD items.
 *
 * @author Zsolt Bokor
 * @since 8/1/19
 */
public interface IBaseContentItem extends Serializable {
    /**
     * @return Identifier which uniquely identifies the item between the same type.
     */
    String getId();

    /**
     * @return Identifier which uniquely identifies the item even between different type of items.
     * E.g. Returns backs different identifier for broadcast and recording of the same program.
     */
    default String getGlobalId() {
        return getId();
    }

    /**
     * @return The title of the content item..
     */
    default String getTitle() {
        return "";
    }

    /**
     * @return The parental age rating of the content item.
     */
    default ParentalRating getParentalRating() {
        return ParentalRating.RatedAll;
    }

    /**
     * @return A list of available image for the content.
     */
    default List<IImageSource> getImageSources() {
       return new ArrayList<>();
    }
}
