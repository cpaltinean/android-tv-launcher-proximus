package tv.threess.threeready.api.home.model.module;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.Map;

import tv.threess.threeready.api.home.model.page.PageConfig;

/**
 * Editorial item action which executed when the user clicks on editorial tile.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public interface EditorialItemAction extends Serializable {

    /**
     * Editorial action to open a modular page by Id.
     */
    interface PageByIdEditorialItemAction extends EditorialItemAction {

        /**
         * @return The identifier of the page which needs to be opened.
         */
        String getPageId();
    }

    /**
     * Editorial action to open a modular page.
     */
    interface PageEditorialItemAction extends EditorialItemAction {

        /**
         * @return The page which needs to be opened.
         */
        PageConfig getPage();
    }

    /**
     * Editorial action to start dynamic category browsing.
     */
    interface CategoryEditorialItemAction extends EditorialItemAction {

        /**
         * @return The identifier of the vod category which needs to be opened.
         */
        String getCategoryId();

        /**
         * @return The message which needs to be displayed if the opened category is empty.
         */
        default String getEmptyPageMessage() {
            return null;
        }
    }

    /**
     * Editorial action to open a 3rd party app.
     */
    interface AppEditorialItemAction extends EditorialItemAction {
        /**
         * @return The package name of the application which needs to be opened.
         */
        String getPackageName();
    }

    /**
     * Editorial action to open an activity with an intent
     */
    interface IntentEditorialItemAction extends EditorialItemAction {
        /**
         * @return The action of the intent which needs to be sent.
         */
        String getIntentAction();

        /**
         * @return The package name of the application which needs to be opened.
         */
        String getPackageName();

        /**
         * @return The uri of the intent which needs to be sent.
         */
        @Nullable
        default String getUri() {
            return null;
        }

        /**
         * @return The extras of the intent which needs to be sent.
         */
        @Nullable
        default Map<String, String> getExtras() {
            return null;
        }
    }

    /**
     * Editorial action to open a web page.
     */
    interface WebPageEditorialItemAction extends EditorialItemAction {

        /**
         * @return The url of the web page to open.
         */
        String getHtmlUrl();
    }

    /**
     * Editorial action to open a page with a single module.
     */
    interface ModuleEditorialItemAction extends EditorialItemAction {

        /**
         * @return The module which needs to be opened.
         */
        ModuleConfig getModuleConfig();

        /**
         * @return The message which needs to be displayed if the opened module is empty.
         */
        default String getEmptyPageMessage() {
            return null;
        }
    }

    /**
     * Editorial action to open the EPG.
     */
    interface EpgEditorialItemAction extends EditorialItemAction {

    }

    /**
     * Editorial action to open detail page for a VOD item.
     */
    interface VodDetailItemAction extends  EditorialItemAction {

        /**
         * @return The unique identifier of the VOD
         * for which the detail page needs to be opened.
         */
        String getVodId();
    }

    /**
     * Editorial action to start VOD playback.
     */
    interface VodPlaybackItemAction extends  EditorialItemAction {

        /**
         * @return The unique identifier of the VOD item which needs to be started.
         */
        String getVodId();
    }
}
