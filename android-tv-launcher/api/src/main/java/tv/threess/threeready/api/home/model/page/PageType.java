package tv.threess.threeready.api.home.model.page;

/**
 * Created by Barabas Attila on 4/18/2017.
 */
public enum PageType {
    MODULAR_PAGE,
    STATIC_PAGE
}
