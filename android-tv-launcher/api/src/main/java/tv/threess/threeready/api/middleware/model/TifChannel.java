package tv.threess.threeready.api.middleware.model;

/**
 * Model describes a channel in the TIF database.
 *
 * @author Barabas Attila
 * @since 5/16/21
 */
public interface TifChannel {

    /**
     * @return The TIF channel id of the channel.
     */
    long getChannelId();

    /**
     * @return The id of the input service which inserted the channel.
     */
    String getInputId();

    /**
     * @return Extra data inserted with the channel.
     */
    String getData();
}
