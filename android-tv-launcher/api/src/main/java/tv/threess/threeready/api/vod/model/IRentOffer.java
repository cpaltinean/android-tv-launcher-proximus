package tv.threess.threeready.api.vod.model;

import java.io.Serializable;
import java.util.List;

import androidx.annotation.Nullable;

/**
 * Interface for vod rent offer content.
 *
 * @author Eugen Guzyk
 * @since 2018.11.13
 */
public interface IRentOffer extends Serializable {

    /**
     * @return The timestamp from when the offer is available.
     */
    long getOfferStart();

    /**
     * @return The timestamp until when the offer is available.
     */
    long getOfferEnd();

    /**
     * @return The duration of the availability in milliseconds after the rent.
     * (e.g available for 48 hour)
     */
    long getRentalTime();

    /**
     * @return The list of prices for rent.
     */
    List<? extends IVodPrice> getPriceOptions();

    /**
     * @return True if the offer is currently available.
     */
    default boolean isAvailable() {
        return getOfferStart() < System.currentTimeMillis()
                && getOfferEnd() > System.currentTimeMillis();
    }

    /**
     * @return True if the offer can be rented.
     */
    default boolean isRentable() {
        if (isFree()) {
            return false;
        }
        return isAvailable();
    }

    /**
     * @return True if the offer is free. (price = 0)
     */
    boolean isFree();

    /**
     * @return The price which is available for the give currency.
     */
    @Nullable
    default IVodPrice getPriceForCurrency(CurrencyType currencyType) {
        List<? extends IVodPrice> prices = getPriceOptions();
        if (prices == null) {
            return null;
        }

        for (IVodPrice vodPrice : prices) {
            if (vodPrice.getCurrencyType() == currencyType) {
                return vodPrice;
            }
        }
        return null;
    }

    /**
     * @param availableCredit The amount of credit available for the user.
     * @return True if the offer can be purchased with the user's credit.
     */
    default boolean canPurchaseForCredit(int availableCredit) {
        if (isFree()) {
            // free items can not be purchased
            return false;
        }
        IVodPrice price = getPriceForCurrency(CurrencyType.Credit);
        return availableCredit > 0
                && price != null && price.getCurrencyAmount() <= availableCredit;
    }

}
