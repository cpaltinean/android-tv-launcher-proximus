package tv.threess.threeready.api.tv.model;

import java.util.List;

import tv.threess.threeready.api.generic.model.IBaseSeries;

/**
 * API model representing a TV series.
 *
 * @author Barabas Attila
 * @since 2020.04.20
 */
public interface IBaseTvSeries extends IBaseSeries {

    /**
     * @return The id of the channel on which the series is broadcasting.
     */
    String getChannelId();

    /**
     * @return List of genre associated to the series.
     */
    List<String> getGenres();
}
