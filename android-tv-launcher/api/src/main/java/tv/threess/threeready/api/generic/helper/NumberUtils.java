package tv.threess.threeready.api.generic.helper;

import tv.threess.threeready.api.log.Log;

/**
 * Utility class for number formats. See {@link androidx.core.math.MathUtils} for similar functionality.
 */
public class NumberUtils {
    private static final String TAG = Log.tag(NumberUtils.class);

    private NumberUtils() { }

    /**
     * This method takes a numerical value and ensures it fits in a given numerical range. If the
     * number is smaller than the minimum required by the range, then the minimum of the range will
     * be returned. If the number is higher than the maximum allowed by the range then the maximum
     * of the range will be returned.
     *
     * @param value the value to be clamped.
     * @param min minimum resulting value.
     * @param max maximum resulting value.
     *
     * @return the clamped value.
     */
    public static long clamp(long value, long min, long max) {
        if (value < min) {
            return min;
        } else if (value > max) {
            return max;
        }
        return value;
    }

    public static int clamp(int value, int min, int max) {
        if (value < min) {
            return min;
        } else if (value > max) {
            return max;
        }
        return value;
    }

    /**
     * Parses the string argument as a signed decimal {@code long}.
     * The characters in the string must all be decimal digits, except
     * that the first character may be an ASCII minus sign {@code '-'}
     * ({@code \u005Cu002D'}) to indicate a negative value or an
     * ASCII plus sign {@code '+'} ({@code '\u005Cu002B'}) to
     * indicate a positive value. The resulting {@code long} value is
     * returned, exactly as if the argument and the radix {@code 10}
     * were given as arguments to the {@link
     * Long#parseLong(java.lang.String, int)} method.
     *
     * In case the parse did not work we return the fallback value specified.
     *
     * @param value     String value to be parsed
     * @param fallback  Value to return in case of error
     * @return          Parsed value of specified string
     */
    public static long parseLong(String value, long fallback) {
        try {
            return Long.parseLong(value);
        } catch (Exception e) {
            Log.d(TAG, "Failed to parse as long: " + value, e);
        }
        return fallback;
    }

    /**
     * Parse the string as an integer. in case it fails return the fallback value.
     * @param value string containing the value
     * @param fallback fallback value in case the parsing fails
     * @return the integer value parsed from the string parameter
     */
    public static int parseInt(String value, int fallback) {
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            Log.d(TAG, "Failed to parse as int: " + value, e);
        }
        return fallback;
    }
}
