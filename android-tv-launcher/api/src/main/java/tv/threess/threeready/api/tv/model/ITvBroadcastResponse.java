/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.tv.model;

import java.io.Serializable;
import java.util.List;

/**
 * Response entity from backend.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.13
 */
public interface ITvBroadcastResponse<T extends IBroadcast> extends Serializable {

    /**
     * @return The list of broadcast requested from the backend.
     */
    List<T> getBroadcasts();
}
