/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.pvr.exception;

/**
 * Base class for all the PVR exceptions
 * extend this one for specific exceptions i.e. PVRStorageNotEnoughException
 *
 * @author Barabas Attila
 * @since 2018.10.23
 */
public class PvrException extends Exception {

    public PvrException() {
    }

    public PvrException(String message) {
        super(message);
    }

    public PvrException(Throwable cause) {
        super(cause);
    }
}
