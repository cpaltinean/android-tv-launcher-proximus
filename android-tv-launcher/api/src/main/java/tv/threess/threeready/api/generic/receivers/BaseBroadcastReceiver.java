/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.generic.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.log.Log;

/**
 * Base class for broadcast receivers. This class wraps the basic {@link BroadcastReceiver}
 * with it's {@link IntentFilter} so when we are registering the receiver,
 * we know directly what filters we need.
 *
 * @author Lukacs Andor
 * @since 2017.05.08
 */
public abstract class BaseBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = Log.tag(BaseBroadcastReceiver.class);

    private boolean mRegistered = false;

    public abstract IntentFilter getIntentFilter();

    public void registerReceiver(Context context) {
        registerReceiver(context, null);
    }

    public void registerReceiver(Context context, @Nullable Handler workerHandler) {
        if (mRegistered) {
            Log.i(TAG, "BroadcastReceiver already registered. " + getClass().getName());
            return;
        }
        try {
            context.registerReceiver(this, getIntentFilter(), null, workerHandler);
            Log.d(TAG, "Receiver registered. " + getClass().getName());
            mRegistered = true;
        } catch (Exception e) {
            Log.e(TAG, "Fail to register receiver: " + getClass().getName(), e);
        }
    }

    public void unregisterReceiver(Context context) {
        if (!mRegistered) {
            Log.i(TAG, "BroadcastReceiver not registered. " + getClass().getName());
            return;
        }
        try {
            context.unregisterReceiver(this);
            Log.d(TAG, "Receiver unregister: " + getClass().getName());
            mRegistered = false;
        } catch (Exception e) {
            Log.e(TAG, "Fail to unregister receiver: " + getClass().getName(), e);
        }
    }
}
