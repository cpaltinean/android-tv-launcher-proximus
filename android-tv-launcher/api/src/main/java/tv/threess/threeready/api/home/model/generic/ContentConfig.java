package tv.threess.threeready.api.home.model.generic;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.home.model.menu.MenuItem;
import tv.threess.threeready.api.home.model.page.PageConfig;

/**
 * Page structure and content related config options.
 *
 * @author Barabas Attila
 * @since 5/14/21
 */
public interface ContentConfig extends Serializable, Component {

    /**
     * @return List of menu items which needs to be displayed in the main menu.
     */
    List<MenuItem> getMainMenuItemList();

    /**
     * @return All the configured content pages of the application.
     */
    List<PageConfig> getPageConfigs();

    /**
     * @param pageID The unique identifier of the page.
     * @return The page with the given id or null if there is no such.
     */
    @Nullable
    PageConfig getPageConfig(String pageID);

    /**
     * @param menuId The menu id to search for.
     * @return The menu item with the given id or null if there is no such.
     */
    @Nullable
    MenuItem getMenuItem(String menuId);
}
