package tv.threess.threeready.api.playback.model;

/**
 * OTT streaming session for Recording content.
 * Contains the necessary data to start the playback and the recording.
 *
 * @author Barabas Attila
 * @since 2020.05.13
 */
public interface IRecordingOttStreamingSession extends IOttStreamingSession {

}
