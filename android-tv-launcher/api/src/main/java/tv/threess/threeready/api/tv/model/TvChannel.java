/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.tv.model;

import androidx.annotation.Nullable;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.tv.ChannelType;

import java.io.Serializable;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Entity class which holds the basic channel information.
 *
 * @author Paul Boldijar
 * @since 2017.05.17
 */
public abstract class TvChannel implements IBaseContentItem, Serializable {

    protected boolean mIsFavorite;

    /**
     * @return Unique identifier of the channel.
     */
    public abstract String getId();

    /**
     * @return Internal identifier of the channel.
     */
    public abstract String getInternalId();

    /**
     * @return Call letter of the broadcaster channel.
     */
    public abstract String getCallLetter();

    /**
     * @return The display number of the channel.
     */
    public abstract int getNumber();

    /**
     * @return The display name of the channel.
     */
    public abstract String getName();

    /**
     * @return The main language of the channel.
     */
    public abstract String getLanguage();

    /**
     * @return The channel logo url.
     */
    public abstract String getChannelLogoUrl();

    /**
     * @return The type of the channel.
     * @see ChannelType
     */
    public abstract ChannelType getType();

    /**
     * @return true, if the user is entitled to the channel
     */
    public abstract boolean isUserTvSubscribedChannel();

    /**
     * Gets whether a warning is required regarding baby content.
     */
    public abstract boolean isBabyContentWarningRequired();

    /**
     * @return True if the channel is capable of replay playback.
     */
    public abstract boolean isReplayEnabled();

    /**
     * @return The maximum time in milliseconds from the current time
     * to the past until the user can watch replay.
     */
    public abstract long getReplayWindow();

    /**
     * @return true if the channel is recording capable.
     */
    public abstract boolean isNPVREnabled();

    /**
     * @return True if the channel has HD streams.
     */
    public abstract boolean isHDEnabled();

    /**
     * @return true if targeted adds are enabled playing live tv
     */
    public abstract boolean isLiveTadEnabled();

    /**
     * @return true if targeted adds are enabled playing time shift (pause live tv)
     */
    public abstract boolean isTimeShiftTadEnabled();

    /**
     * @return nonzero duration if inactivity detection is needed on this channel
     */
    public abstract long getTargetAdInactivity(TimeUnit unit);

    /**
     * @return True if the usr marked the channels as favorite.
     */
    public boolean isFavorite() {
        return mIsFavorite;
    }

    // TODO : find an other solution, shouldn't be setter in the api models
    public void setFavorite(boolean favorite) {
        mIsFavorite = favorite;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return ((obj instanceof TvChannel) && Objects.equals(this.getId(),((TvChannel) obj).getId()));
    }

}
