package tv.threess.threeready.api.playback.exception;


import tv.threess.threeready.api.generic.exception.BackendException;

/**
 * Thrown when a session open error is encountered.
 *
 * @author Bartalus Csaba
 * @since 2020.10.13
 */

public class SessionOpenException extends BackendException {
    private final boolean mMicroServiceCall;

    public SessionOpenException(Throwable throwable, boolean microService) {
        super(throwable.getMessage());
        initCause(throwable);
        mMicroServiceCall = microService;
    }

    public boolean isMicroService() {
        return mMicroServiceCall;
    }
}
