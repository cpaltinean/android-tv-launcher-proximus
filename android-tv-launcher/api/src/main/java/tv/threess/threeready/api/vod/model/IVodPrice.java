package tv.threess.threeready.api.vod.model;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.Map;

/**
 * Interface for vod price.
 * Contains:
 * - Amount
 * - Currency type
 *
 * @author Eugen Guzyk
 * @since 2018.11.13
 */
public interface IVodPrice extends Serializable {

    double getCurrencyAmount();

    CurrencyType getCurrencyType();

    @Nullable
    String getPriceDetermination();

    static IVodPrice getMinimumPrice(Map<String, IRentOffer> variantOffers, CurrencyType currencyType) {
        if (variantOffers == null) {
            return null;
        }

        IVodPrice minimPrice = null;
        for (IRentOffer offer : variantOffers.values()) {
            for (IVodPrice price : offer.getPriceOptions()) {
                if (price.getCurrencyType() != currencyType) {
                    continue;
                }
                if (currencyType != CurrencyType.Euro && offer.isFree()) {
                    continue;
                }
                if ((minimPrice == null || minimPrice.getCurrencyAmount() > price.getCurrencyAmount())) {
                    minimPrice = price;
                }
            }
        }
        return minimPrice;
    }
}
