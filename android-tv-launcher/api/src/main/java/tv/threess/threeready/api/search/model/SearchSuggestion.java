package tv.threess.threeready.api.search.model;

/**
 * Suggested term to trigger search queries.
 *
 * @author Barabas Attila
 * @since 2019.03.28
 */
public class SearchSuggestion extends SearchTerm {

    public SearchSuggestion(String term) {
        super(term);
    }

    @Override
    public boolean isSuggestion() {
        return true;
    }
}
