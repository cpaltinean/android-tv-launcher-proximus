package tv.threess.threeready.api.config.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.account.model.CacheSettings;
import tv.threess.threeready.api.generic.model.ContinueWatchingConfig;
import tv.threess.threeready.api.home.model.generic.ChannelApp;
import tv.threess.threeready.api.home.model.generic.ContentMarkers;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.home.model.generic.NotificationSettings;
import tv.threess.threeready.api.home.model.generic.OfflineModeSettings;
import tv.threess.threeready.api.home.model.generic.PlayApps;
import tv.threess.threeready.api.home.model.generic.StyleSettings;

/**
 * Application related config options.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public interface AppConfig extends Component, Serializable {

    /**
     * @return Player related config options.
     */
    PlayerConfig getPlayerConfig();

    /**
     * @return Font and button style related settings.
     */
    StyleSettings getStyleSettings();

    /**
     * @return Continue watching related config options.
     */
    ContinueWatchingConfig getContinueWatchingConfig();

    /**
     * @return Date and time format related settings.
     */
    LocaleSettings getLocaleSettings();

    /**
     * @return Featured and store app related settings.
     */
    PlayApps getPlayApps();

    /**
     * @return The minimum number of character for automatic search results.
     */
    int getSearchThreshold();

    /**
     * @return The time in seconds used for delaying the forced reboot and for showing the notification.
     */
    long getForcedRebootDelay(TimeUnit unit);

    /**
     * @return The Netflix partner Id. We need this for the DET response request.
     */
    String getNetflixPartnerId();

    /**
     * @return App package id and channel config map
     * for channels which are representing a 3rd party app.
     * E.g Netflix and Disney Plus channels
     */
    Map<String, ChannelApp> getChannelApps();

    /**
     * @return TVO link for replay plus ordering.
     */
    String getReplayPlusLink();

    /**
     * @return TVI link to access regional channels.
     */
    String getRegionalChannelLink();

    /**
     * @return Content marker related config options.
     */
    ContentMarkers getContentMarkers();

    /**
     * @return Maximum time in millis of use inactivity on home after which it automatically hides.
     */
    long getHomeScreenHideTimeout();

    /**
     * @return Maximum time in millis of use inactivity on Tv-Guide after which it automatically hides.
     */
    long getGuideHideTimeout();

    /**
     * @return Maximum time in millis of use inactivity on player UI after which it automatically hides.
     */
    long getPlayerUiHideTimeout();

    /**
     * @return Maximum time in millis of use inactivity on channel zapper after which it automatically hides.
     */
    long getZapperUiHideTimeout();

    /**
     * @param unit The time unit to return the result in.
     * @return The minimum time frame for which request programs in the Tv-Program guide.
     */
    long getProgramGuidePageSize(TimeUnit unit);

    /**
     * @return Generic timeout in millis for database queries and backend calls.
     */
    int getQueryTimeout();

    /**
     * @param unit The time unit to return the timeout in.
     * @return The maximum time to wait for the startup to finish.
     */
    long getStartupTimeout(TimeUnit unit);

    /**
     * @param unit The time unit to return the timeout in.
     * @return The maximum time to wait for the module data source resolver to finish.
     */
    long getModuleDataSourceTimeout(TimeUnit unit);

    /**
     * @return Names of the remote control known by the app.
     */
    Collection<String> getRemoteNames();

    /**
     * @param unit The time unit to return the result in.
     * @return The time to wait after tuning to a baby channel before displaying the warning dialog.
     */
    long getBabyChannelWarningDelay(TimeUnit unit);

    /**
     * @return The maximum channel number which is supported by the app.
     */
    int getMaxChannelNumber();

    /**
     * @param unit The time unit to return the result in.
     * @return The maximum time to wait for internet connectivity during startup.
     */
    long getInternetCheckTimeout(TimeUnit unit);

    /**
     * @param unit The time unit to return the result in.
     * @return The maximum time to wait for HDMI connection during startup.
     */
    long getHDMICheckTimeout(TimeUnit unit);

    /**
     * @param unit The time unit to return the result in.
     * @return THe minimum time to display a hint for.
     */
    long getHintsDisplayTime(TimeUnit unit);

    /**
     * @param unit The time unit to return the result in.
     * @return The time after which the parental control automatically activates after a temporal unblock.
     */
    long getParentalControlTemporalUnblockSeconds(TimeUnit unit);

    /**
     * @param unit The time unit to return the result in.
     * @return The time the devices stays in active standby before going into network standby.
     */
    long getActiveStandbyTimeout(TimeUnit unit);

    /**
     * @return Map the audio and subtitle languages coming from the playback to the languages displayed for the user.
     */
    Map<String, String> getAudioSubtitleLanguageMap();

    /**
     * @return Playback related config options.
     */
    PlaybackSettings getPlaybackSettings();

    /**
     * @return TVI link for channel renumbering.
     */
    String getChannelRenumberingLink();

    /**
     * @return Caching related config options.
     */
    CacheSettings getCacheSettings();

    /**
     * @return Notification related config options.
     */
    NotificationSettings getNotificationSettings();

    /**
     * @return Offline mode related config options.
     */
    OfflineModeSettings getOfflineModeSettings();

    /**
     * @return The out of box ordering of the applications.
     */
    List<String> getInitialAppOrder();

    /**
     * @return The maximum time to retry channel scans in cas of failures.
     */
    int getChannelScanRetries();

    /**
     * @param unit The time unit to return the results in.
     * @return The time to wait when the device goes to standby before applying a system update.
     */
    long getStandbySystemUpdateApplyDelay(TimeUnit unit);

    /**
     * @param unit The time unit to return the results in.
     * @return The maximum time to wait for a system update. (download, verify and install combined)
     */
    long getSystemUpdateTimeout(TimeUnit unit);

    /**
     * @param unit  The time unit to return the results in.
     * @return      The maximum time spent in the rus window including internet checks, TAD downloads and MW update
     */
    long getRusWindowTimeout(TimeUnit unit);

    /**
     * @return True if the startup flow needs to be fully executed after a quiescent reboot.
     */
    boolean isQuiescentStartupEnabled();

    /**
     * @return channel scan time out.
     */
    long getChannelScanTimeOut(TimeUnit unit);

    /**
     * @return The number of modules to load when a page opens.
     */
    int getPageInitModuleCount();

    /**
     * @return The modules to prefetch on a page outside of the visible window.
     */
    int getPagePrefetchModuleCount();
}
