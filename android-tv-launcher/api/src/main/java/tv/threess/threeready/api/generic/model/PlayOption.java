package tv.threess.threeready.api.generic.model;

import java.io.Serializable;

/**
 * Wrapper class for play options.
 *
 * @author Andrei Teslovan
 * @since 2018.12.11
 */
public class PlayOption implements Serializable {

    private final PlayOption.Type mType;

    public PlayOption( PlayOption.Type type) {
        mType = type;
    }

    public  PlayOption.Type getType() {
        return mType;
    }

    public enum Type {
        RECORDING,
        REPLAY,
        LIVE,
        RENTED,
        INCLUDED,
        RADIO
    }
}
