/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.home.model.generic;

import tv.threess.lib.di.Component;

/**
 * Enumeration class representing the style of a content marker.
 *
 * @author Barabas Attila
 * @since 2018.07.11
 */
public enum ContentMarkerStyle implements Component {
    DEFAULT,
    PROXIMUS,
}
