package tv.threess.threeready.api.home.model.module;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.AppEditorialItemAction;

/**
 * Class defines and editorial tile which can be displayed in a module.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public interface EditorialItem extends IBaseContentItem {

    /**
     * @return The title displayed when the background image cannot be loaded.
     */
    @Nullable
    default String getFallbackTitle() {
        return null;
    }

    /**
     * @return Layout and theme options of the editorial tile.
     */
    @Nullable
    default EditorialItemStyle getStyle() {
        return null;
    }

    /**
     * @return The action which needs to be executed when the user click on the editorial tile.
     */
    EditorialItemAction getAction();

    /**
     * @return The content description for the given item, used by TalkBack
     */
    default String getContentDescription() {
        return null;
    }

    /**
     * @return The position of the editorial tile in the module.
     * If null the tile should be displayed at the end of the modules.
     */
    @Nullable
    default Integer getPosition() {
        return null;
    }

    /**
     * @return The minimum number of item needed in the module for the editorial to be displayed.
     * If null the tile only should be added when the limit of the module reached but the module has more data.
     */
    default Integer getMinItemCount() {
        return null;
    }

    /**
     * @return The conditions which needs to fulfil for the editorial tile to be displayed.
     */
    @Nullable
    default ModuleCondition[] getConditions() {
        return null;
    }

    /**
     * @return True if the editorial item represents an application.
     */
    default boolean isApplication() {
        if (getAction() == null) {
            return false;
        }

        return getAction() instanceof AppEditorialItemAction;
    }

    /**
     * True if a feedback needs to be sent to the backend when the editorial item is displayed.
     */
    default boolean hasViewedFeedback() {
        return false;
    }

    /**
     * True if a feedback needs to be sent to the backend when the editorial item is clicked
     */
    default boolean hasClickedFeedback() {
        return false;
    }
}
