package tv.threess.threeready.api.generic.model;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Base interface for content items. This includes Tv Broadcast items and VOD items.
 *
 * @author Andor Lukacs
 * @since 10/2/18
 */
public interface IContentItem extends IBaseContentItem {

    String SEASON_EPISODE_NUMBER_FORMAT = "%1$d";

    String SEASON_EPISODE_SPACE = " ";

    /**
     * @return Short description of the content.
     */
    String getDescription();

    /**
     * @return The season number of the content if available or null.
     */
    @Nullable
    Integer getSeasonNumber();

    /**
     * @return The episode number of the content if available or null.
     */
    @Nullable
    Integer getEpisodeNumber();

    /**
     * @return The title of the episode.
     */
    String getEpisodeTitle();

    /**
     * @return The year when the content was released.
     */
    String getReleaseYear();

    /**
     * @return The duration of the content in milliseconds.
     */
    long getDuration();

    /**
     * @return The unique identifier of the series of which the content part of.
     * Or null if the content is not part of a series.
     */
    @Nullable
    String getSeriesId();

    /**
     * @return True if the content is part of a series, false otherwise.
     */
    default boolean isEpisode() {
        return !TextUtils.isEmpty(getSeriesId());
    }

    /**
     * @return The actor list of the content.
     */
    List<String> getActors();

    /**
     * @return The director list of the content.
     */
    List<String> getDirectors();

    /**
     * @return The list of genre representing the content.
     */
    List<String> getGenres();

    /**
     * @return The list of sub-genres representing the content.
     */
    default List<String> getSubGenres() {
        return Collections.emptyList();
    }

    /**
     * @return True if the content has an episode number, false otherwise.
     */
    default boolean hasEpisodeNumber() {
        return getEpisodeNumber() != null;
    }

    /**
     * @return True if the content has a season number, false otherwise.
     */
    default boolean hasSeasonNumber() {
        return getSeasonNumber() != null;
    }

    /**
     * @return true if the broadcast has descriptive audio.
     */
    boolean hasDescriptiveAudio();

    /**
     * @return true if the broadcast has descriptive subtitles.
     */
    boolean hasDescriptiveSubtitle();

    /**
     * Get title with season and episode, format: S01 E08 ProgramTitle
     */
    default String getTitleWithSeasonEpisode(Translator translator, String title, String titleSeparator) {
        String seasonPrefix = translator.get(TranslationKey.HOVER_SEASON);
        String episodePrefix = translator.get(TranslationKey.HOVER_EPISODE);

        // Display season info
        StringBuilder titleBuilder = new StringBuilder();
        if (hasSeasonNumber()) {
            titleBuilder.append(seasonPrefix);
            titleBuilder.append(String.format(Locale.getDefault(),
                    SEASON_EPISODE_NUMBER_FORMAT, getSeasonNumber()));
            titleBuilder.append(SEASON_EPISODE_SPACE);
        }

        // Display episode info
        if (hasEpisodeNumber()) {
            if (titleBuilder.length() > 0) {
                titleBuilder.append(SEASON_EPISODE_SPACE);
            }
            titleBuilder.append(episodePrefix);
            titleBuilder.append(String.format(Locale.getDefault(),
                    SEASON_EPISODE_NUMBER_FORMAT, getEpisodeNumber()));
        }
        if (!TextUtils.isEmpty(title)) {
            // Display title
            if (titleBuilder.length() > 0) {
                titleBuilder.append(titleSeparator).append(SEASON_EPISODE_SPACE);
            }
            titleBuilder.append(title);
        }
        return titleBuilder.toString();
    }

    default String getEpisodeTitleWithSeasonEpisode(Translator translator, String titleSeparator) {
        String title;

        if (TextUtils.isEmpty(getEpisodeTitle())) {
            title = getTitle();
        } else {
            title = getEpisodeTitle();
        }

        return getTitleWithSeasonEpisode(translator, title, titleSeparator);
    }

    default String getSeriesTitleWithSeasonEpisode(Translator translator, String titleSeparator) {
        String title;

        if (!TextUtils.isEmpty(getTitle())) {
            title = getTitle();
        } else if (!TextUtils.isEmpty(getEpisodeTitle())) {
            title = getEpisodeTitle();
        } else {
            title = "";
        }

        return getTitleWithSeasonEpisode(translator, title, titleSeparator);
    }

    default String getSeasonEpisodeNumber(Translator translator) {
        String seasonPrefix = translator.get(TranslationKey.HOVER_SEASON);
        String episodePrefix = translator.get(TranslationKey.HOVER_EPISODE);

        // Display season info
        StringBuilder titleBuilder = new StringBuilder();
        if (hasSeasonNumber()) {
            titleBuilder.append(seasonPrefix);
            titleBuilder.append(String.format(Locale.getDefault(),
                    SEASON_EPISODE_NUMBER_FORMAT, getSeasonNumber()));
            titleBuilder.append(SEASON_EPISODE_SPACE);
        }

        // Display episode info
        if (hasEpisodeNumber()) {
            if (titleBuilder.length() > 0) {
                titleBuilder.append(SEASON_EPISODE_SPACE);
            }
            titleBuilder.append(episodePrefix);
            titleBuilder.append(String.format(Locale.getDefault(),
                    SEASON_EPISODE_NUMBER_FORMAT, getEpisodeNumber()));
        }

        return titleBuilder.toString();
    }
}
