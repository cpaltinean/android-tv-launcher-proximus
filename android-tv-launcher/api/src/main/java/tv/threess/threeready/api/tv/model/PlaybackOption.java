/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.tv.model;

import java.io.Serializable;

/**
 * The entity that will represent a playback option of a channel
 *
 * @author Paul
 * @since 2017.05.24
 */

public class PlaybackOption implements Serializable {

    private final String mChannelId;

    private final long mTifChannelId;

    private final String mTifInputId;

    private final StreamType mPlaybackType;

    private final String mPlaybackData;

    private final String mPlaybackExtra;

    public PlaybackOption(String channelId, long tifChannelId, String tifInputId, String playbackType, String playbackData, String playbackExtra) {
        mChannelId = channelId;
        mTifChannelId = tifChannelId;
        mTifInputId = tifInputId;
        mPlaybackType = StreamType.valueOf(playbackType);
        mPlaybackData = playbackData;
        mPlaybackExtra = playbackExtra;
    }

    public String getChannelId() {
        return mChannelId;
    }

    public long getTifChannelId() {
        return mTifChannelId;
    }

    public String getTifInputId() {
        return mTifInputId;
    }

    public StreamType getPlaybackType() {
        return mPlaybackType;
    }

    public String getPlaybackData() {
        return mPlaybackData;
    }

    public String getPlaybackExtra() {
        return mPlaybackExtra;
    }
}
