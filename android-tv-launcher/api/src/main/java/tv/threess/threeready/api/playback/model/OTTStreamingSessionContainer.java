package tv.threess.threeready.api.playback.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Container for OTT streaming sessions for continuous playback.
 *
 * @author Barabas Attila
 * @since 11/23/20
 */
public class OTTStreamingSessionContainer<TOTTStreamingSession extends IOttStreamingSession>
        extends CopyOnWriteArrayList<TOTTStreamingSession> {

    public OTTStreamingSessionContainer() {
    }

    public OTTStreamingSessionContainer(Collection<? extends TOTTStreamingSession> c) {
        super(c);
    }

    public OTTStreamingSessionContainer(TOTTStreamingSession[] toCopyIn) {
        super(toCopyIn);
    }

    /**
     * Find the streaming session for the given content position.
     * @param contentPosition The position of in the content to find the session for.
     * @return The session for the given content position or null if there is no session.
     */
    @Nullable
    public TOTTStreamingSession findSessionForContentPosition(long contentPosition) {
        for (TOTTStreamingSession session : this) {
            if (session.isTimeIncluded(contentPosition)) {
                return session;
            }
        }

        // No session.
        if (isEmpty()) {
            return null;
        }

        return get(0);
    }

    /**
     * @return the first slot or throws an exception in case the slot list is empty
     */
    @NonNull
    public TOTTStreamingSession firstSlot() {
        return get(0);
    }

    /**
     * @return the last slot or throws an exception in case the slot list is empty
     */
    @NonNull
    public TOTTStreamingSession lastSlot() {
        return get(size() - 1);
    }

    /**
     * @return the last slot or the given fallback argument in case the slot list is empty
     */
    @Nullable
    public TOTTStreamingSession lastSlot(TOTTStreamingSession fallback) {
        if (isEmpty()) {
            return fallback;
        }
        return lastSlot();
    }
}
