package tv.threess.threeready.api.home.model.generic;

import android.graphics.Color;

import androidx.annotation.Nullable;

import java.io.Serializable;

import tv.threess.lib.di.Component;

/**
 *  Configuration class for layouts.
 *  Contains the colors, backgrounds which will be used in user interface.
 *
 * @author Barabas Attila
 * @since 5/14/21
 */
public interface LayoutConfig extends Component, Serializable {
    float ALPHA_PLACEHOLDER_FONT = 0.7f;

    /**
     * @return Url or file name of the logo displayed on the main menu.
     */
    @Nullable
    default String getLogo() {
        return null;
    }

    /**
     * @return The gradient start of the main color in the application.
     */
    default int getPrimaryColorStart() {
        return Color.WHITE;
    }

    /**
     * @return The gradient end of the main color in the application.
     */
    default int getPrimaryColorEnd() {
        return Color.WHITE;
    }

    /**
     * @return The placeholder background of the cards in the application.
     */
    default int getPlaceHolderColor() {
        return Color.BLACK;
    }

    /**
     * @return The main color of the fonts in the application.
     */
    default int getFontColor() {
        return Color.WHITE;
    }

    /**
     * @return The secondary/subtitle font color in the application.
     */
    default int getPlaceholderFontColor() {
        return Color.WHITE;
    }

    /**
     * @return The font color of the submenu items in the application.
     */
    default int getSettingsSubmenuColor() {
        return Color.WHITE;
    }

    /**
     * @return The background color of the focused cards in the application.
     */
    default int getFocusedBackground() {
        return Color.BLACK;
    }

    /**
     * @return The details text color in the application.
     */
    default int getPlaceholderTransparentFontColor() {
        return Color.WHITE;
    }

    /**
     * @return The background gradient start color of the buttons when pressed.
     */
    default int getButtonPressedColorStart() {
        return Color.BLACK;
    }

    /**
     * @return The background gradient end color of the buttons when pressed.
     */
    default int getButtonPressedColorEnd() {
        return Color.BLACK;
    }

    /**
     * @return The background gradient start color of the buttons when activated.
     */
    default int getButtonActiveColorStart() {
        return Color.BLACK;
    }

    /**
     * @return The background gradient end color of the buttons when activated.
     */
    default int getButtonActiveColorEnd() {
        return Color.BLACK;
    }

    /**
     * @return The background gradient start color of the buttons when focused.
     */
    default int getButtonFocusedColorStart() {
        return Color.BLACK;
    }

    /**
     * @return The background gradient end color of the buttons when focused.
     */
    default int getButtonFocusedColorEnd() {
        return Color.BLACK;
    }

    /**
     * @return The background gradient start color of the buttons on default state.
     */
    default int getButtonNoStateColorStart() {
        return Color.BLACK;
    }

    /**
     * @return The background gradient end color of the buttons on default state.
     */
    default int getButtonNoStateColorEnd() {
        return Color.BLACK;
    }

    /**
     * @return The background color of the buttons in loading state.
     */
    default int getLoadingButtonColor() {
        return Color.BLACK;
    }

    /**
     * @return The font color of the buttons in focused state.
     */
    default int getButtonFocusedFontColor() {
        return Color.WHITE;
    }

    /**
     * @return The color of the cursor in input fields.
     */
    default int getInputCursorColor() {
        return Color.WHITE;
    }

    /**
     * @return The color of the line under the focused menu item.
     */
    default int getMenuSelectorFocused() {
        return Color.WHITE;
    }

    /**
     * @return  The color of the line under the activated menu item.
     */
    default int getMenuSelectorActive() {
        return Color.WHITE;
    }

    /**
     * @return The color of the line under the menu item in default state.
     */
    default int getMenuSelectorNoState() {
        return Color.TRANSPARENT;
    }

    /**
     * @return The font color of the playback error message.
     */
    default int getErrorColor() {
        return Color.WHITE;
    }

    /**
     * @return The resource file name of the play icon.
     */
    @Nullable
    default String getPlayIcon() {
        return null;
    }

    /**
     * @return The resource file name of the pause icon.
     */
    @Nullable
    default String getPauseIcon() {
        return null;
    }

    /**
     * @return The resource file name of the rewind icon.
     */
    @Nullable
    default String getRewindIcon() {
        return null;
    }

    /**
     * @return The resource file name of the forward icon.
     */
    default String getForwardIcon() {
        return null;
    }

    /**
     * @return The url of resource file name of the logo on the startup screen.
     */
    @Nullable
    default String getWelcomeLogo() {
        return null;
    }

    /**
     * @return The background color of the startup screen.
     */
    default int getWelcomeColor() {
        return Color.BLACK;
    }

    /**
     * @return Thump switch color when it's checked.
     */
    default int getSwitchThumbCheckedColor() {
        return Color.BLACK;
    }

    /**
     * @return Thump track color when it's checked.
     */
    default int getSwitchTrackCheckedColor() {
        return Color.WHITE;
    }

    /**
     * @return Thump switch color when it's unchecked.
     */
    default int getSwitchThumbUncheckedColor() {
        return Color.BLACK;
    }

    /**
     * @return Thump track color when it's unchecked.
     */
    default int getSwitchTrackUncheckedColor() {
        return Color.BLACK;
    }

    /**
     * @return Radio button color when it's checked.
     */
    default int getRadioButtonCheckedColor() {
        return Color.WHITE;
    }

    /**
     * @return Radio button color when it's unchecked.
     */
    default int getRadioButtonUncheckedColor() {
        return Color.BLACK;
    }

    /**
     * @return The background color of the pages in the application.
     */
    default int getBackgroundColor() {
        return Color.BLACK;
    }

    /**
     * @return The background color of the notifications in the application.
     */
    default int getNotificationBackgroundColor() {
        return Color.BLACK;
    }

    /**
     * @return The Background image needs to be displayed in the application.
     */
    @Nullable
    default String getBackgroundImage() {
        return null;
    }

    /**
     * @return The message which needs to be displayed when the page is empty.
     */
    @Nullable
    default String getEmptyPageMessage() {
        return null;
    }

    /**
     * @return The message which needs to be displayed when
     * the page is empty and the app cannot connect to the backend.
     */
    default String getIsolationModeMessage() {
        return null;
    }

    LayoutConfig EMPTY = new LayoutConfig() {};
}
