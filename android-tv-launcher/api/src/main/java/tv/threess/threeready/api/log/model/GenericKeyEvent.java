package tv.threess.threeready.api.log.model;

/**
 * Generic event key enum
 *
 * @author Daniel Gliga
 * @since 2017.09.22
 */

public enum GenericKeyEvent implements Event.Detail<GenericEvent> {
    BACKEND_AVAILABLE,
    INTERNET_AVAILABLE,
    STARTUP_FINISHED,
    MESSAGE,
    DOMAIN,
    CODE,
    KEY;

    public String getKey() {
        return name();
    }
}
