package tv.threess.threeready.api.middleware.model;

import java.security.InvalidParameterException;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

/**
 * Parse scte-35 markers.
 *
 * @author Karetka Mezei Zoltan
 * @since 2021.08.13
 */
public class Scte35Info extends BaseInfo<Scte35Info.Field> {

    public Scte35Info(BitStream bits) {
        super(Field.class);
        splice_info_section(bits);
    }

    public long getDuration(TimeUnit unit) {
        // convert pts duration to seconds and return it in the requested unit
        return unit.convert(get(Scte35Info.Field.duration, 0) / 90, TimeUnit.MILLISECONDS);
    }

    public double getDurationSeconds() {
        // convert pts duration to seconds
        return get(Scte35Info.Field.duration, 0.) / 90000;
    }

    private void splice_info_section(BitStream data) {
        put(Field.table_id, data.readU32(8));
        put(Field.section_syntax_indicator, data.readU32(1));
        put(Field.private_indicator, data.readU32(1));
        put(Field.reserved, data.readU32(2));
        put(Field.section_length, data.readU32(12));
        put(Field.protocol_version, data.readU32(8));
        int encrypted_packet = put(Field.encrypted_packet, data.readBit());
        int encryption_algorithm = put(Field.encryption_algorithm, data.readU32(6));
        put(Field.pts_adjustment, data.readU64(33));
        put(Field.cw_index, data.readU32(8));
        put(Field.tier, data.readU32(12));

        int spliceCmdSize = put(Field.splice_command_length, data.readU32(12));
        int spliceCmdType = put(Field.splice_command_type, data.readU32(8));
        int spliceCmdStart = data.offset();
        switch (spliceCmdType) {
            default:
                throw new InvalidParameterException("Invalid splice command type: " + spliceCmdType);

            case 0x00:
                splice_null(data);
                break;

            case 0x04:
                splice_schedule(data);
                break;

            case 0x05:
                splice_insert(data);
                break;

            case 0x06:
                time_signal(data);
                break;

            case 0x07:
                bandwidth_reservation(data);
                break;

            case 0xff:
                private_command(data);
                break;
        }

        if (data.offset() - spliceCmdStart != 8 * spliceCmdSize) {
            throw new InvalidParameterException("Invalid splice command size: " + (data.offset() - spliceCmdStart));
        }

        int spliceDescriptorSize = put(Field.descriptor_loop_length, data.readU32(16));
        int spliceDescriptorStart = data.offset();
        if (spliceDescriptorSize > 0) {
            splice_descriptor(data);
        }

        if (data.offset() - spliceDescriptorStart != 8 * spliceDescriptorSize) {
            throw new InvalidParameterException("Invalid splice descriptor size: " + (data.offset() - spliceDescriptorStart) + ", not: " + (8 * spliceDescriptorSize));
        }

        data.reset(spliceDescriptorStart);
        byte[] spliceDescriptor = data.readBytes(spliceDescriptorSize);
        put(Field.splice_descriptor, Base64.getEncoder().encodeToString(spliceDescriptor));

        if (encrypted_packet != 0) {
            put(Field.encrypted_crc_32, data.readU32(32));
        }
        put(Field.crc_32, data.readU32(32));

        if (encrypted_packet != 0 || encryption_algorithm != 0) {
            throw new InvalidParameterException("Encryption detected, but is not implemented");
        }
    }

    private void splice_null(BitStream data) {
    }

    private void splice_schedule(BitStream data) {
        throw new InvalidParameterException("Invalid splice command type, splice_schedule not implemented");
    }

    private void splice_insert(BitStream data) {
        put(Field.splice_event_id, data.readU32(32));
        int splice_event_cancel_indicator = put(Field.splice_event_cancel_indicator, data.readBit());
        put(Field.reserved, data.readU32(7));

        if (splice_event_cancel_indicator == 0) {
            put(Field.out_of_network_indicator, data.readBit());
            int program_splice_flag = put(Field.program_splice_flag, data.readBit());
            int duration_flag = put(Field.duration_flag, data.readBit());
            int splice_immediate_flag = put(Field.splice_immediate_flag, data.readBit());
            put(Field.reserved, data.readU32(4));

            if ((program_splice_flag == 1) && (splice_immediate_flag == 0)) {
                splice_time(data);
            }
            if (program_splice_flag == 0) {
                int component_count = data.readU32(8);
                for (int i = 0; i < component_count; i++) {
                    put(Field.component_tag, data.readU32(8));
                    if (splice_immediate_flag == 0) {
                        splice_time(data);
                    }
                }
            }
            if (duration_flag == 1) {
                break_duration(data);
            }
            put(Field.unique_program_id, data.readU32(16));
            put(Field.avail_num, data.readU32(8));
            put(Field.avails_expected, data.readU32(8));
        }
    }

    private void time_signal(BitStream data) {
        splice_time(data);
    }

    private void bandwidth_reservation(BitStream data) {
        throw new InvalidParameterException("Invalid splice command type, bandwidth_reservation not implemented");
    }

    private void private_command(BitStream data) {
        throw new InvalidParameterException("Invalid splice command type, private_command not implemented");
    }

    private void splice_time(BitStream data) {
        int time_specified_flag = data.readBit();
        if(time_specified_flag == 1) {
            put(Field.reserved, data.readU32(6));
            put(Field.pts_time, data.readU64(33));
        } else {
            put(Field.reserved, data.readU32(7));
        }
    }

    private void break_duration(BitStream data) {
        put(Field.auto_return, data.readU32(1));
        put(Field.reserved, data.readU32(6));
        put(Field.duration, data.readU64(33));
    }

    private void splice_descriptor(BitStream data) {
        int tag = put(Field.splice_descriptor_tag, data.readU32(8));
        int n = put(Field.splice_descriptor_length, data.readU32(8));
        put(Field.splice_identifier, data.readBytes(4));
        int offset = data.offset() - 32;

        switch (tag) {
            case 0: // AvailDescriptor
                put(Field.provider_avail_id, data.readU32(32));
                break;

            case 2: // SegmentationDescriptor
                //put(Field.splice_bytes, data.readBytes(n - 4));
                segmentation_descriptor(data);
                break;

            case 1: // DTMFDescriptor
            case 3: // TimeDescriptor
            case 4: // AudioDescriptor
            default: // 0x05 – 0xFF: Reserved for future SCTE splice_descriptors
                throw new InvalidParameterException("Invalid splice descriptor tag: " + tag);
        }
        if (data.offset() != offset + 8 * n) {
            throw new InvalidParameterException("Invalid splice descriptor size: " + (data.offset() - offset));
        }
    }

    private void segmentation_descriptor(BitStream data) {
        put(Field.segmentation_event_id, data.readU32(32));
        int eventCancelIndicator = put(Field.segmentation_event_cancel_indicator, data.readU32(1));
        put(Field.reserved, data.readU32(7));
        if (eventCancelIndicator == 0) {
            int program_segmentation_flag = data.readBit();
            int segmentation_duration_flag = data.readBit();
            put(Field.delivery_not_restricted_flag, data.readBit());
            if (program_segmentation_flag == 0) {
                put(Field.web_delivery_allowed_flag,data.readU32(1));
                put(Field.no_regional_blackout_flag, data.readU32(1));
                put(Field.archive_allowed_flag, data.readU32(1));
                put(Field.device_restrictions, data.readU32(2));
            } else {
                put(Field.reserved, data.readU32(5));
            }
            if (program_segmentation_flag == 0) {
                int component_count = data.readU32(8);
                for (int i = 0; i < component_count; i++) {
                    put(Field.component_tag, data.readU32(8));
                    put(Field.reserved, data.readU32(7));
                    put(Field.pts_offset, data.readU64(33));
                }
            }
            if (segmentation_duration_flag == 1) {
                put(Field.segmentation_duration, data.readU64(40));
            }
            int upid_type = put(Field.segmentation_upid_type, data.readU32(8));
            int upid_length = put(Field.segmentation_upid_length, data.readU32(8));
            segmentation_upid(data, upid_type, upid_length);
            int typeId = put(Field.segmentation_type_id, data.readU32(8));
            put(Field.segment_num, data.readU32(8));
            put(Field.segments_expected, data.readU32(8));
            if (typeId == 0x34 || typeId == 0x36 || typeId == 0x38 || typeId == 0x3A) {
                put(Field.sub_segment_num, data.readU32(8));
                put(Field.sub_segments_expected, data.readU32(8));
            }
        }
    }

    private void segmentation_upid(BitStream data, int type, int length) {
        if (type != 0x0C) {
            throw new InvalidParameterException("Field segmentation_Upid_Type is not 0x0C (Managed Private UPID)");
        }

        int endOffset = data.offset() + length * 8;
        put(Field.format_identifier, data.readBytes(4));
        put(Field.private_upid_version, data.readI64(8));

        put(Field.type_of_replacement, data.readHex(2));
        data.reset(data.offset() - 10);  // skip back 10 bits
        put(Field.transmission_inside_break, data.readBit());
        put(Field.transmission_is_live, data.readBit());
        put(Field.perform_replacement_in_cpvr, data.readBit());
        put(Field.perform_replacement_in_npvr, data.readBit());
        put(Field.perform_replacement_in_replay, data.readBit());
        put(Field.perform_replacement_in_live, data.readBit());
        put(Field.replacement_is_multiple, data.readBit());
        put(Field.replacement_is_regional, data.readBit());
        put(Field.replacement_is_brand4brand, data.readBit());
        put(Field.replacement_language_fr_nl, data.readBit());

        put(Field.private_segment_id, data.readBytes(36));
        put(Field.private_program_id, data.readBytes(23));
        put(Field.private_cni, data.readHex(2));
        put(Field.private_ad_count_position, data.readI32(8));
        put(Field.private_brand, data.readBytes(32));
        put(Field.private_pod_id, data.readBytes(16));

        int sectorsCount = put(Field.private_sectors_count, data.readI32(8));
        if (sectorsCount < 0x00 || sectorsCount > 0x1e) {
            throw new InvalidParameterException("Invalid Sectors Count(values between 0x00 and 0x1E): " + sectorsCount);
        }
        for (int i = 0; i < sectorsCount; ++i) {
            put(Field.private_sectorial, data.readU32(16));
        }

        if (data.offset() != endOffset) {
            throw new InvalidParameterException("Invalid segmentation descriptor size: " + (data.offset() - endOffset));
        }
    }

    private long put(Field field, long value) {
        super.put(field, String.valueOf(value));
        return value;
    }

    private int put(Field field, int value) {
        super.put(field, String.valueOf(value));
        return value;
    }

    private void put(Field field, byte[] value) {
        super.put(field, new String(value));
    }

    public String get(Field field) {
        return super.get(field, null);
    }

    public enum Field implements BaseInfo.Field {
        table_id(null),
        section_syntax_indicator(null),
        private_indicator(null),
        section_length,
        protocol_version(null),
        encrypted_packet,
        encryption_algorithm(null),
        pts_adjustment(null),
        cw_index(null),
        tier(null),
        splice_command_length(null),
        splice_command_type,
        descriptor_loop_length(null),
        pts_time,
        splice_event_id,
        splice_event_cancel_indicator(null),
        out_of_network_indicator(null),
        program_splice_flag(null),
        duration_flag(null),
        splice_immediate_flag(null),
        component_tag(null),
        unique_program_id(null),
        avail_num(null),
        avails_expected(null),
        auto_return(null),
        duration,

        splice_descriptor_tag(null),
        splice_descriptor_length(null),
        splice_identifier,
        splice_descriptor,

        provider_avail_id(null),
        segmentation_event_id(null),
        segmentation_event_cancel_indicator(null),
        delivery_not_restricted_flag(null),
        web_delivery_allowed_flag(null),
        no_regional_blackout_flag(null),
        archive_allowed_flag(null),
        device_restrictions(null),
        pts_offset,
        segmentation_duration(null),
        segmentation_upid_type(null),
        segmentation_upid_length(null),
        segmentation_type_id(null),
        segment_num(null),
        segments_expected(null),
        sub_segment_num(null),
        sub_segments_expected(null),

        format_identifier,
        private_upid_version,
        private_segment_id,
        private_program_id,
        private_cni,
        private_ad_count_position,
        private_brand,
        private_pod_id,
        private_sectors_count(null),
        private_sectorial(null),

        type_of_replacement,
        transmission_inside_break(null),
        transmission_is_live(null),
        perform_replacement_in_cpvr(null),
        perform_replacement_in_npvr(null),
        perform_replacement_in_replay(null),
        perform_replacement_in_live,
        replacement_is_multiple(null),
        replacement_is_regional(null),
        replacement_is_brand4brand(null),
        replacement_language_fr_nl(null),

        crc_32,
        encrypted_crc_32(null),
        reserved(null);

        private final String mKey;
        Field(String key) {
            mKey = key;
        }

        Field() {
            mKey = name();
        }

        @Override
        public String getKey() {
            return mKey;
        }
    }
}
