package tv.threess.threeready.api.account.watchlist;

import tv.threess.threeready.api.generic.model.WatchlistType;

/**
 * Interface used for the watchlist response related minimal data.
 * 
 * @author David 
 * @since 16.11.2021
 */
public interface IWatchListResponseItem {

    String getId();

    String getTimestamp();

    WatchlistType getType();

    String getItemId();

    String getChannelId();
}
