/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.generic.receivers;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.MwRepository;

/**
 * Receive standby state changes of the device.
 *
 * @author Barabas Attila
 * @since 2017.11.02
 */
public class StandByStateChangeReceiver extends BaseBroadcastReceiver implements Component {
    private static final String TAG = Log.tag(StandByStateChangeReceiver.class);

    private final List<WeakReference<Listener>> mListeners = new CopyOnWriteArrayList<>();

    private boolean isInteractive;

    @Override
    public void registerReceiver(Context context) {
        super.registerReceiver(context);
        isInteractive = Components.get(MwRepository.class).isScreenOn();
    }

    /**
     * Register a listener which will be notified when the standby state changes.
     */
    public void addStateChangedListener(Listener listenerToAdd) {
        removeStateChangedListener(listenerToAdd);
        mListeners.add(new WeakReference<>(listenerToAdd));
    }

    /**
     * Remove the previously registered standby state change listener.
     */
    public void removeStateChangedListener(Listener listenerToRemove) {
        mListeners.removeIf((item) -> {
            Listener listener = item.get();
            return listener == null || listener == listenerToRemove;
        });
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Received intent : " + intent);

        if (intent == null || intent.getAction() == null) {
            return;
        }

        switch (intent.getAction()) {
            case Intent.ACTION_SCREEN_ON:
                isInteractive = true;
                break;
            case Intent.ACTION_SCREEN_OFF:
                isInteractive = false;
                break;
        }

        ArrayUtils.notifyAndCleanup(mListeners,
                listener -> listener.onStateChanged(isInteractive));
    }

    public boolean isScreenOn() {
        return isInteractive;
    }

    @Override
    public IntentFilter getIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        return intentFilter;
    }

    /**
     * Callback to notify the listeners about the component state changes.
     */
    public interface Listener {
        void onStateChanged(boolean screenOn);
    }
}
