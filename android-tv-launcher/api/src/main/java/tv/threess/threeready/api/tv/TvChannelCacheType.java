package tv.threess.threeready.api.tv;

/**
 * Enum used for the different type of channels used on Mini Epg / Channel zapper.
 *
 * @author David
 * @since 16.11.2021
 */
public enum TvChannelCacheType {
    FAVORITE,
    NOT_FAVORITE,
    ALL
}
