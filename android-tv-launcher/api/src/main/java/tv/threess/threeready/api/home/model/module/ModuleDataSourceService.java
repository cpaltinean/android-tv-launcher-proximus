/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.home.model.module;

import java.io.Serializable;

/**
 * Define the domain for the data source.
 *
 * Created by Barabas Attila on 4/12/2017.
 */

public enum ModuleDataSourceService implements Serializable {
    TV,
    VOD,
    PVR,
    GMS,
    NETFLIX,
    EDITORIAL,
    UNDEFINED
}
