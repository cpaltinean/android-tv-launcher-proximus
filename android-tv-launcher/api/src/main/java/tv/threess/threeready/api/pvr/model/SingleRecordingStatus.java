package tv.threess.threeready.api.pvr.model;

/**
 * Enum to identify the current single recording status of a broadcast.
 *
 * @author Barabas Attila
 * @since 2018.10.25
 */
public enum SingleRecordingStatus {
    // Not schedule for recording.
    NONE,

    // Scheduled for future recording.
    SCHEDULED,

    // Ongoing recording.
    RECORDING,

    // Recording already finished and available.
    COMPLETED
}
