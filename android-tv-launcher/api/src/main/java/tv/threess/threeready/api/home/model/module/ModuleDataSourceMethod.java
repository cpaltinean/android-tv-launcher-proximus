package tv.threess.threeready.api.home.model.module;

/**
 * Enum identifies the method through which the data needs to be requested.
 *
 * @author Barabas Attila
 * @since 5/16/21
 */
public enum ModuleDataSourceMethod {
    TV_PROGRAM_INDEX,
    TV_PROGRAM_SEARCH,
    TV_PROGRAM_NOW,
    CONTINUE_WATCHING,
    TV_CHANNEL_INDEX,
    TV_CHANNEL_SEARCH,
    REPLAY_CHANNEL_PAGE_ITEMS,
    TV_PROGRAM_NOW_NEXT,
    PROGRAMS_BETWEEN,
    WATCHLIST,
    NETFLIX,
    EDITORIAL_ITEMS,
    USER_RENTALS,
    ANDROID_TV_APPS,
    ANDROID_TV_NOTIFICATIONS,
    SCHEDULED_RECORDING,
    COMPLETED_RECORDING,
    UNKNOWN
}
