/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.generic.helper;

import static tv.threess.threeready.api.generic.helper.TimeBuilder.UTC;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.config.model.PlaybackSettings;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;

/**
 * Utility functions.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.14
 */
public class TimeUtils {
    // Divider used for TIME STAMP
    public static final String DIVIDER = "  I  ";

    //The long version of TIME STAMP: Day DayOfMonth Month | HH:mm - HH:mm;
    public static final String LONG_TIME_STAMP_FORMAT = "%1$s %2$s %3$s  I  %4$s - %5$s";

    // The short version of TIME STAMP: Day | HH:mm - HH:mm
    public static final String SHORT_TIME_STAMP_FORMAT = "%1$s  I  %2$s - %3$s";

    // The short version of START TIME STAMP: Day HH:mm
    public static final String SHORT_START_TIME_STAMP_FORMAT = "%1$s %2$s";

    //The long version of TIME STAMP on classic EPG: Day DayOfMonth Month
    public static final String EPG_LONG_TIME_STAMP_FORMAT = "%1$s %2$s %3$s";

    //The short version of TIME STAMP on classic EPG: Day
    public static final String EPG_SHORT_TIME_STAMP_FORMAT = "%1$s";

    // Version used in TvProgramGuideDatePresenter
    public static final String PROGRAM_GUIDE_TIME_STAMP_FORMAT = "%1$s  I  %2$s %3$s";

    // Long version used in FastScrollingView
    public static final String LONG_FAST_SCROLLING_TIME_STAMP_FORMAT = "%1$s %2$s %3$s  I  %4$s";

    // Short version used in FastScrollingViewLocale.US
    public static final String SHORT_FAST_SCROLLING_TIME_STAMP_FORMAT = "%1$s  I  %2$s";

    //Constant used to show DELETE_SOON content market when Recording will be deleted in 5 days.
    public static final int CONTENT_MARKET_DELETE_SOON_LESS_THAN_DAYS = 5;
    public static final int CONTENT_MARKET_DELETE_SOON_RECORDING_AVAILABILITY_DAYS = 60;

    // Default constant used to identify the rules used to convert between an Instant and a LocalDateTime
    public static final ZoneId DEFAULT_ZONE_ID = ZoneId.systemDefault();

    //Constant that shows the timestamp for the current running program.
    public static final long LIVE_PROGRAM_TIMESTAMP = -1;
    public static final Locale LOCALE_DATE_FORMAT = Locale.US;

    public static String formatDate(LocaleSettings localeSettings, long timestamp) {
        return getTimeFormat(DateTimeFormatter.ofPattern(localeSettings.getDateFormat(), LOCALE_DATE_FORMAT), timestamp);
    }

    /**
     * @param firstIntervalStart  left bound of the first interval
     * @param firstIntervalEnd    right bound of the first interval
     * @param secondIntervalStart left bound of the second interval
     * @param secondIntervalEnd   right bound if the second interval
     * @return true if the 2 intervals do not overlap
     */
    public static boolean gapBetween(long firstIntervalStart, long firstIntervalEnd,
                                     long secondIntervalStart, long secondIntervalEnd) {
        if (firstIntervalStart > firstIntervalEnd || secondIntervalStart > secondIntervalEnd) {
            throw new IllegalArgumentException("The intervals are not in order " + firstIntervalStart +
                    " " + firstIntervalEnd + " " + secondIntervalStart + " " + secondIntervalEnd);
        }
        return !(firstIntervalStart <= secondIntervalEnd && secondIntervalStart <= firstIntervalEnd);
    }

    /**
     * Use this method for create the EPG Window border by {@param borderTimeStamp}.
     * This border by default is 7 days (both for/start past and future/end)
     */
    public static long getEPGWindowBorder(long borderTimeStamp) {
        return TimeBuilder.utc()
                .add(borderTimeStamp)
                .floor(TimeUnit.DAYS)
                .get();
    }

    /**
     * Returns the next prime time relative to the given reference time
     *
     * @param referenceTime     position in time from where to find the next prime time
     * @param maxTimeWindow     how many data do we have in the epg
     * @param primeTimeSettings hour and minute of the prime time
     * @return the nearest time to present moment in milliseconds
     */
    public static long nextPrimeTime(long referenceTime, long maxTimeWindow, PlaybackSettings.PrimeTime primeTimeSettings) {
        TimeBuilder primeTime = TimeBuilder.utc();
        long now = primeTime.get();

        if (referenceTime == LIVE_PROGRAM_TIMESTAMP) {
            referenceTime = now;
        }

        // builder holds the reference day's prime-time
        primeTime.set(referenceTime).floor(TimeUnit.DAYS)
                .set(true, TimeBuilder.LOCAL)
                .set(TimeUnit.HOURS, primeTimeSettings.getHour())
                .set(TimeUnit.MINUTES, primeTimeSettings.getMinutes())
                .set(true, UTC);

        // reference time past prime time, advance to next day's prime time
        if (referenceTime >= primeTime.get()) {
            primeTime.add(TimeUnit.DAYS, 1);
        }

        // now is between reference and prime time, return it
        if (now > referenceTime && now < primeTime.get()) {
            return LIVE_PROGRAM_TIMESTAMP;
        }

        // prime time overflows the current time window, fallback to now
        if (primeTime.absDuration(now) > maxTimeWindow) {
            return LIVE_PROGRAM_TIMESTAMP;
        }

        return primeTime.get();
    }

    /**
     * Returns the previous prime time relative to the given reference time
     *
     * @param referenceTime         position in time from where to find the next prime time
     * @param maxTimeWindow         how many data do we have in the epg
     * @param primeTimeSettings     hour and minute of the prime time
     * @return the nearest time to present moment in milliseconds
     */
    public static long prevPrimeTime(long referenceTime, long maxTimeWindow, PlaybackSettings.PrimeTime primeTimeSettings) {
        TimeBuilder primeTime = TimeBuilder.utc();
        long now = primeTime.get();

        if (referenceTime == LIVE_PROGRAM_TIMESTAMP) {
            referenceTime = now;
        }

        // builder holds the reference day's prime-time
        primeTime.set(referenceTime).floor(TimeUnit.DAYS)
                .set(true, TimeBuilder.LOCAL)
                .set(TimeUnit.HOURS, primeTimeSettings.getHour())
                .set(TimeUnit.MINUTES, primeTimeSettings.getMinutes())
                .set(true, UTC);

        // reference time before prime time, advance to previous day's prime time
        if (referenceTime <= primeTime.get()) {
            primeTime.add(TimeUnit.DAYS, -1);
        }

        // now is between reference and prime time
        if (now > primeTime.get() && now < referenceTime) {
            return LIVE_PROGRAM_TIMESTAMP;
        }

        // prime time overflows the current time window, fallback to now
        if (primeTime.absDuration(now) > maxTimeWindow) {
            return LIVE_PROGRAM_TIMESTAMP;
        }

        return primeTime.get();
    }

    /**
     * Returns the correct time format.
     *
     * @param dateTimeFormatter formatter for printing and parsing date-time objects
     * @param time              the time in milliseconds
     * @param zoneId            is used to identify the rules used to convert between an Instant and a LocalDateTime
     */
    public static String getTimeFormat(DateTimeFormatter dateTimeFormatter, long time, ZoneId zoneId) {
        LocalDateTime localDateTime = Instant.ofEpochMilli(time).atZone(zoneId).toLocalDateTime();
        return localDateTime.format(dateTimeFormatter);
    }

    /**
     * Returns the correct time format.
     *
     * @param dateTimeFormatter formatter for printing and parsing date-time objects
     * @param time              the time in milliseconds
     */
    public static String getTimeFormat(DateTimeFormatter dateTimeFormatter, long time) {
        LocalDateTime localDateTime = Instant.ofEpochMilli(time).atZone(DEFAULT_ZONE_ID).toLocalDateTime();
        return localDateTime.format(dateTimeFormatter);
    }

    /**
     * Utility date and time parser.
     */
    public static class DateTimeParser {

        /**
         * Instance calendar.
         */
        private final Calendar mCalendar;

        /**
         * Not instantiable outside {@link TimeUtils}.
         */
        DateTimeParser() {
            this.mCalendar = Calendar.getInstance(UTC);
        }

        /**
         * Date and time parsing method for the fixed format {@code yyyy-MM-dd HH:mm:ss} (e.g. 2012-12-01 18:40:00Z,
         * 2013-03-15T18:00:00Z).<br>
         * This method was created for fast parsing date time strings in format during JSON response parsing.
         *
         * @param dateTime Date and time string in the fixed format {@code yyyy-MM-dd HH:mm:ss}
         *                 (e.g. 2012-12-01 18:40:00Z).
         * @return the parsed number of milliseconds since Jan. 1, 1970, midnight GMT.
         * @throws NumberFormatException
         */
        public long parse(String dateTime) throws NumberFormatException {
            this.mCalendar.clear();
            try {
                this.mCalendar.set(Calendar.YEAR, Integer.parseInt(dateTime.substring(0, 4)));
                this.mCalendar.set(Calendar.MONTH, Integer.parseInt(dateTime.substring(5, 7)) - 1);
                this.mCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateTime.substring(8, 10)));
                this.mCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(dateTime.substring(11, 13)));
                this.mCalendar.set(Calendar.MINUTE, Integer.parseInt(dateTime.substring(14, 16)));
                this.mCalendar.set(Calendar.SECOND, Integer.parseInt(dateTime.substring(17, 19)));
            } catch (NumberFormatException e) {
                throw (NumberFormatException) new NumberFormatException("Invalid datetime: " + dateTime).initCause(e);
            }
            return this.mCalendar.getTime().getTime();
        }

        /**
         * Date and time parsing method for the fixed format {@code yyyy-MM-dd'T'HH:mm:ss.SSS}
         * (e.g. 2013-02-15T08:50:17.5214029Z).<br>
         * This method was created for fast parsing date time strings in format during XML response parsing.
         *
         * @param dateTime Date and time parsing method for the fixed format {@code yyyy-MM-dd'T'HH:mm:ss.SSS}
         *                 (e.g. 2013-02-15T08:50:17.5214029Z).
         * @return the parsed number of milliseconds since Jan. 1, 1970, midnight GMT.
         * @throws NumberFormatException
         */
        public long parseMillis(String dateTime) throws NumberFormatException {
            this.parse(dateTime);
            // Support variable length or missing milliseconds
            int x = 20, max = Math.min(23, dateTime.length());
            while (x < max && Character.isDigit(dateTime.charAt(x))) {
                x++;
            }
            if (x > 20) {
                mCalendar.set(Calendar.MILLISECOND, Integer.parseInt(dateTime.substring(20, x)));
            }
            return mCalendar.getTimeInMillis();
        }
    }

    /**
     * parse duration of format "HH:mm:ss.SSS" and "HH:mm:ss"
     */
    public static long parseDuration(String text) throws Exception {
        long result = 0;
        int secPos = text.length();
        if (secPos > 4 && text.charAt(secPos - 4) == '.') {
            // duration contains also milliseconds
            result = Integer.parseInt(text.substring(secPos - 3));
            secPos -= 4;
        }

        int minPos = secPos - 3;
        if (text.charAt(minPos) != ':') {
            throw new Exception("Invalid duration: " + text);
        }

        int hourPos = minPos - 3;
        if (text.charAt(hourPos) != ':') {
            throw new Exception("Invalid duration: " + text);
        }

        result += TimeUnit.SECONDS.toMillis(Integer.parseInt(text.substring(secPos - 2, secPos)));
        result += TimeUnit.MINUTES.toMillis(Integer.parseInt(text.substring(minPos - 2, minPos)));
        result += TimeUnit.HOURS.toMillis(Integer.parseInt(text.substring(0, hourPos)));
        return result;
    }

    private static final class DateTimeParserProvider extends ThreadLocal<DateTimeParser> {

        DateTimeParserProvider() {}

        @Override
        protected DateTimeParser initialValue() {
            return new DateTimeParser();
        }
    }

    private static final DateTimeParserProvider dateTimeParserProvider = new DateTimeParserProvider();

    /**
     * Getter for a DateTimeParser instance.
     *
     * @return an instance of DateTimeParser.
     */
    public static DateTimeParser getDateTimeParser() {
        return dateTimeParserProvider.get();
    }

}
