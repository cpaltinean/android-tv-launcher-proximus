/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.pvr;

import io.reactivex.Completable;
import io.reactivex.Observable;
import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.model.BingeWatchingInfo;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.pvr.model.IBaseRecordingSeries;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingQuota;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.pvr.model.SeriesRecordingDetailModel;
import tv.threess.threeready.api.pvr.model.SeriesRecordingStatus;
import tv.threess.threeready.api.tv.model.IBroadcast;

/**
 * Implementation of the specific PVR handler methods generally serving UI components.
 *
 * @author Denisa Trif
 * @since 2018.09.14
 */
public interface PvrRepository extends Component {

    /**
     * @param broadcast the broadcast for which the recording capability should be checked.
     * @return true if the PVR feature is enabled and the broadcast is recording capable.
     */
    boolean isPVREnabled(IBroadcast broadcast);

    /**
     * Rx observable to get the current recording status of a broadcast.
     * The observable will emit the new status each time when it changes.
     *
     * @param broadcast The broadcast for which the status should be returned.
     * @return Rx observable to subscribe on recording status changes and errors.
     */
    Observable<RecordingStatus> getRecordingStatus(IBroadcast broadcast);

    /**
     * Rx observable to get the current recording status of a series.
     * The observable will emit the new status each time when it changes.
     *
     * @param series The series for which the status should be returned.
     * @return Rx observable to subscribe on recording status changes and errors.
     */
    Observable<SeriesRecordingStatus> getSeriesRecordingStatus(IBaseRecordingSeries series);

    /**
     * Rx observable to return recordings for a series.
     *
     * @param seriesId The id of teh series for which the recording should be returned.
     * @return Rx observable which emits the series information.
     */
    Observable<IRecordingSeries> getSeriesRecording(String seriesId);

    /**
     * Get the data model for the recording series detail pages that contains the series and the last played episode of the series.
     *
     * @param seriesId The id of teh series for which the recording should be returned.
     * @return A wrapper that contains the series, with all it's broadcasts and the last played episode with the bookmark
     */
    Observable<SeriesRecordingDetailModel> getSeriesRecordingDetailModel(String seriesId);

    /**
     * Rx observable to return a recording from a series after a specific one.
     *
     * @param episode The recording from a series
     * @return Rx observable which emits the next episode.
     */
    Observable<BingeWatchingInfo<IRecordingSeries, IRecording>> getNextEpisode(IRecording episode, IRecordingSeries series);

    /**
     * Load ongoing and recorded content for the module data source.
     * @param moduleConfig The module config which contains the data source params and filters.
     * @param start Pagination parameter, the provided data should start from this position.
     * @param count Pagination parameter, the number of the returned data should be limited to this value.
     * @return RX observable which emits from the data source.
     */
    Observable<ModuleData<IBaseContentItem>> getRecordedCategoryContent(final ModuleConfig moduleConfig, final int start, final int count);

    /**
     * Load scheduled content for the module data source.
     * @param moduleConfig The module config which contains the data source params and filters.
     * @param start Pagination parameter, the provided data should start from this position.
     * @param count Pagination parameter, the number of the returned data should be limited to this value.
     * @return RX observable which emits from the data source.
     */
    Observable<ModuleData<IBaseContentItem>> getScheduledCategoryContent(final ModuleConfig moduleConfig, final int start, final int count);

    /**
     * Wait until the recording data synchronized with backend.
     */
    Completable waitRecordingSync();

    /**
     * Rx completable to schedule a single recording for the future broadcast.
     *
     * @param broadcast The broadcast for which the recording should be scheduled.
     * @return Rx completable to subscribe on action and error.
     */
    Completable scheduleRecording(IBroadcast broadcast);

    /**
     * Rx completable to schedule broadcast to series recording
     *
     * @param broadcast The broadcast for which the series should be scheduled for recording.
     * @return Rx completable to subscribe on action and error.
     */
    Completable scheduleSeriesRecording(IBroadcast broadcast);

    /**
     * Rx completable to schedule broadcast to series recording
     *
     * @param series The series should be scheduled for recording.
     * @return Rx completable to subscribe on action and error.
     */
    Completable scheduleSeriesRecording(IRecordingSeries series);

    /**
     * Rx completable to cancel and ongoing or future broadcast from single recording.
     *
     * @param broadcast The broadcast for which the recording should be canceled.
     * @return Rx completable to subscribe on action and error.
     */
    Completable cancelRecording(IBroadcast broadcast);

    /**
     * Rx completable to cancel all future episode of a series from recording.
     *
     * @param broadcast The broadcast of a series for which all future episode should be canceled from recording.
     * @return Rx completable to subscribe on action and error.
     */
    Completable cancelSeriesRecording(IBroadcast broadcast);

    /**
     * Rx completable to cancel all future episode of a series from recording.
     *
     * @param series The series for which all future episode should be canceled from recording.
     * @return Rx completable to subscribe on action and error.
     */
    Completable cancelSeriesRecording(IRecordingSeries series);

    /**
     * Rx completable to cancel all future episode of a series and delete all past and ongoing episodes.
     *
     * @param series The series for which all future episode should be canceled and old episodes deleted.
     * @return Rx completable to subscribe on action and error.
     */
    Completable deleteAndCancelRecording(IRecordingSeries series);

    /**
     * Rx completable to delete the existing recording of the broadcast.
     *
     * @param recording The recording what should be deleted.
     * @return Rx completable to subscribe on action and error.
     */
    Completable deleteRecording(IRecording recording);

    /**
     * Rx completable to delete all completed episode of a series recording.
     *
     * @param series The series recording from which the completed episodes should be delted.
     * @return Rx completable to subscribe on action and error.
     */
    Completable deleteSeriesRecording(IRecordingSeries series);

    /**
     * Rx observable to return the short term consideration quota.
     *
     * @return Return the available recording space.
     */
    Observable<IRecordingQuota> getSTCRecordingQuota();
}
