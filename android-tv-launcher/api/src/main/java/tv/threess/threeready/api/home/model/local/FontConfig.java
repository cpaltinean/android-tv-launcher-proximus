package tv.threess.threeready.api.home.model.local;

import java.io.Serializable;

/**
 * Configure class to a give font type.
 * Contains the exact font family and type,
 * also some optional scale factor parameters for design fine tune.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public interface FontConfig extends Serializable {

    /**
     * @return Unique type of the font which identifies it.
     */
    FontType getFontType();

    /**
     * The exact name of the font which will be used for the given style.
     * Same as the name of the OTF\TTF file.
     */
    String getName();

    /**
     * Optional parameter to adjust the text size for the given font.
     */
    float getTextSizeFactor();

    /**
     * Optional parameter to adjust line spacing for the given font.
     */
    float getLineSpacingExtraFactor();

    /**
     * Optional parameter to adjust the letter spacing for the given font.
     */
    float getLetterSpacingFactor();
}
