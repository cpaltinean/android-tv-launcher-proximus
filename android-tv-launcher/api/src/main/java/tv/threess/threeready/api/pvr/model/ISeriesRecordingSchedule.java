package tv.threess.threeready.api.pvr.model;

import java.io.Serializable;

/**
 * Base interface for scheduled series recordings.
 *
 * Created by Noemi Dali on 17.04.2020.
 */
public interface ISeriesRecordingSchedule extends Serializable {

    /**
     * @return The series id.
     */
    String getSeriesRefNo();

    /**
     * @return The user series id.
     */
    String getUserSeriesId();

    /**
     * @return The channel id.
     */
    String getChannelId();

    /**
     * @return The series title.
     */
    String getTitle();
}
