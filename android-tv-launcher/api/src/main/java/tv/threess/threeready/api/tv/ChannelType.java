package tv.threess.threeready.api.tv;

/**
 * Type of the tv channel.
 *
 * @author Eugen Guzyk
 * @since 2018.08.17
 */
public enum ChannelType {
    TV, // Audio and video playback.
    APP, // Will open a 3rd party app.
    RADIO, // Audio only playback.
    REGIONAL // Will open web view.
}
