/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.generic.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import tv.threess.threeready.api.log.Log;

/**
 *
 * Base class for local broadcast receivers (LocalBroadcastManager is used). This class wraps the basic {@link BroadcastReceiver}
 * with it's {@link IntentFilter} so when we are registering the receiver, we know directly what filters we need.
 *
 * Created by Gliga Daniel on 1/18/2018.
 */

public abstract class BaseLocalBroadcastReceiver extends BroadcastReceiver{
    private static final String TAG = Log.tag(BaseLocalBroadcastReceiver.class);

    private boolean mRegistered = false;

    public abstract IntentFilter getIntentFilter();

    public void registerReceiver(Context context) {
        if (mRegistered) {
            Log.i(TAG, "BroadcastReceiver already registered.");
            return;
        }
        try {
            LocalBroadcastManager.getInstance(context).registerReceiver(this, getIntentFilter());
            mRegistered = true;
        } catch (Exception e) {
            Log.e(TAG, "Fail to register receiver: " + this.getClass().getCanonicalName(), e);
        }
    }

    public void unregisterReceiver(Context context) {
        if (!mRegistered) {
            Log.i(TAG, "BroadcastReceiver not registered.");
            return;
        }
        try {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(this);
            mRegistered = false;
        } catch (Exception e) {
            Log.e(TAG, "Fail to unregister receiver: " + this.getClass().getCanonicalName(), e);
        }
    }
}
