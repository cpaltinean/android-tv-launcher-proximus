package tv.threess.threeready.api.vod.model;

import java.io.Serializable;

import tv.threess.threeready.api.pc.model.ParentalRating;

/**
 * Base response class for vod rental transaction.
 *
 * @author Daniela Toma
 * @since 2022.02.08
 */
public interface IVodRentalTransaction extends Serializable {
    long getPurchaseDate();

    String getTitle();

    float getAmount();

    ParentalRating getRatingCode();
}
