package tv.threess.threeready.api.generic.model;

/**
 * Enumeration used for Bookmark Type.
 *
 * @author Daniela Toma
 * @since 2020.04.28
 */
public enum BookmarkType {
    Live,
    Replay,
    Recording,
    VodVariant
}
