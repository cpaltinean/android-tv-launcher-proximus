package tv.threess.threeready.api.generic.exception;

/**
 * Exception for the device registration error
 * when the device registration limit is reached on the account.
 *
 * @author Barabas Attila
 * @since 2020.04.14
 */
public class DeviceLimitReachedException extends BackendException {

    public DeviceLimitReachedException(BackendException cause) {
        super(cause);
    }
}
