/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.generic.receivers;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.log.Log;

/**
 * Base class to can subscribe on cable and internet connectivity changes.
 *
 * @author Barabas Attila
 * @since 2019.04.12
 */
public abstract class ConnectivityStateChangeReceiver {
    private final String TAG = Log.tag(getClass());

    private static final Handler sMainHandler = new Handler(Looper.getMainLooper());

    private final List<WeakReference<OnStateChangedListener>> mListeners = new CopyOnWriteArrayList<>();

    public void registerReceiver() {}

    public void unRegisterReceiver() {}

    /**
     * Register a listener which will be notified when the connectivity state of the component changes.
     */
    public void addStateChangedListener(OnStateChangedListener listenerToAdd) {
        removeStateChangedListener(listenerToAdd);
        mListeners.add(new WeakReference<>(listenerToAdd));
    }

    /**
     * Remove the previously registered connectivity change listener.
     */
    public void removeStateChangedListener(OnStateChangedListener listenerToRemove) {
        mListeners.removeIf((item) -> {
            OnStateChangedListener listener = item.get();
            return listener == null || listener == listenerToRemove;
        });
    }

    /**
     * Called when the connectivity state changes.
     */
    @CallSuper
    protected void onCheckFinished(OnStateChangedListener.State state) {
        Log.d(TAG, "Connectivity state check finished. " + state );
        ArrayUtils.notifyAndCleanup(mListeners, sMainHandler,
                listener -> listener.onCheckFinished(state));
    }

    /**
     * Called when the connectivity state changes.
     */
    @CallSuper
    protected void onStateChanged(OnStateChangedListener.State state) {
        Log.d(TAG, "Connectivity state changed: " + state);
        ArrayUtils.notifyAndCleanup(mListeners, sMainHandler,
                listener -> listener.onStateChanged(state));
    }

    /**
     * Callback to notify the listeners about the component state changes.
     */
    public interface OnStateChangedListener {
        class State {
            private boolean mCableConnected;

            private Exception mBackendError;
            private Exception mPreviousError;

            private boolean mInternetAvailable;
            private boolean mPrevInternetAvailable;

            public boolean isCableConnected() {
                return mCableConnected;
            }

            public boolean isBackendAvailable() {
                return mBackendError == null;
            }

            public boolean isBackendStateChanged() {
                return (mBackendError == null) != (mPreviousError == null);
            }

            public boolean isInternetAvailable() {
                return mInternetAvailable;
            }

            public boolean isInternetStateChanged() {
                return mInternetAvailable != mPrevInternetAvailable;
            }

            public boolean isInternetStateChanged(boolean toState) {
                return isInternetStateChanged() && mInternetAvailable == toState;
            }

            public boolean isStateChanged() {
                return isInternetStateChanged() || isBackendStateChanged();
            }

            @Nullable
            public Exception getBackendError() {
                return mBackendError;
            }

            private State() {
            }

            @Override
            public String toString() {
                return "State{" +
                        "isCableConnected=" + isCableConnected() +
                        ", isBackendAvailable=" + isBackendAvailable() +
                        ", isInternetAvailable=" + isInternetAvailable() +
                        '}';
            }

            public static class Builder extends State {

                public Builder() {
                    super();
                }

                public Builder cableConnected(boolean cableConnected) {
                    super.mCableConnected = cableConnected;
                    return this;
                }

                public Builder backendAvailable(Exception error, Exception previousError) {
                    super.mBackendError = error;
                    super.mPreviousError = previousError;
                    return this;
                }

                public Builder internetAvailable(boolean available, boolean previousAvailable) {
                    super.mInternetAvailable = available;
                    super.mPrevInternetAvailable = previousAvailable;
                    return this;
                }

                public State build() {
                    return this;
                }
            }
        }

        /**
         * Called when the connectivity status check finishes.
         */
        default void onCheckFinished(State state) {
            if (state.isStateChanged()) {
                onStateChanged(state);
            }
        }

        /**
         * Called when the connectivity status changes.
         * @param state The new connectivity state.
         */
        void onStateChanged(State state);
    }


    /**
     * State change listener which waits some time before reporting a disconnectivity.
     * If the connectivity state recovers during the time there will be no change reported.
     */
    public static abstract class DelayedStateChangedListener implements OnStateChangedListener  {
        private final long mErrorDispatchDelay;

        private final Handler mInternetTimeoutHandler = new Handler();
        private State mLastErrorState = null;
        private boolean mStateReported;

        public DelayedStateChangedListener(long errorDispatchDelay) {
            mErrorDispatchDelay = errorDispatchDelay;
        }

        @Override
        public void onCheckFinished(State state) {
            Log.d("DelayedStateChangedListener", "Last error : "
                    + mLastErrorState + ", reported : " + mStateReported + ", state : " + state);

            if (state.isInternetAvailable() && state.isBackendAvailable()) {
                if (mStateReported) {
                    onStateChanged(state);
                }
                resetState();
            } else {
                if (mLastErrorState == null) {
                    mInternetTimeoutHandler.postDelayed(mDispatchError, mErrorDispatchDelay);
                }
                mLastErrorState = state;
            }
        }

        /**
         * Reset the delay and the last state received.
         */
        public void resetState() {
            mLastErrorState = null;
            mInternetTimeoutHandler.removeCallbacks(null);
            mStateReported = false;
        }

        private final Runnable mDispatchError = () -> {
            if (mLastErrorState == null) {
                return;
            }
            mStateReported = true;
            onStateChanged(mLastErrorState);
        };
    }
}
