package tv.threess.threeready.api.log.model;

/**
 * Generic log event
 *
 * @author Daniel Gliga
 * @since 2017.09.22
 */

public enum GenericEvent implements Event.Kind {
    ErrorEvent,
    IsolationMode,
    PlaybackException,
    Unknown;

    public static final String DOMAIN = "Generic";

    @Override
    public String domain() {
        return DOMAIN;
    }
}
