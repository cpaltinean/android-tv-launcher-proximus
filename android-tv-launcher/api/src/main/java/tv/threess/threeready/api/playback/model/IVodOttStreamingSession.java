package tv.threess.threeready.api.playback.model;

/**
 * OTT streaming session for VOD content.
 * Contains the necessary data to start the playback and the VOD.
 *
 * @author Barabas Attila
 * @since 2020.05.15
 */
public interface IVodOttStreamingSession extends IOttStreamingSession {
}
