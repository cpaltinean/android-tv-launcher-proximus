/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.tv.model;

import java.io.File;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * Entity class which holds the basic advertisement information.
 *
 */
public interface TvAdReplacement extends Serializable {

    File getFile();

    String getFileName();

    boolean isExpired();

    boolean isOnMarker();

    long getDuration(TimeUnit unit);

    enum Event {
        /**
         * not to be confused with an impression, this event indicates that an individual creative
         * portion of the ad was viewed. An impression indicates the first frame of the ad was displayed; however
         * an ad may be composed of multiple creative, or creative that only play on some platforms and not
         * others. This event enables ad servers to track which ad creative are viewed, and therefore, which
         * platforms are more common.
         */
        creativeView,

        /**
         * this event is used to indicate that an individual creative within the ad was loaded and playback
         * began. As with creativeView, this event is another way of tracking creative playback.
         */
        start,

        /**
         * the creative played for at least 25% of the total duration.
         */
        firstQuartile,

        /**
         * the creative played for at least 50% of the total duration.
         */
        midpoint,

        /**
         * the creative played for at least 75% of the duration.
         */
        thirdQuartile,

        /**
         * The creative was played to the end at normal speed.
         */
        complete,

        /**
         * the user activated the mute control and muted the creative.
         */
        mute,

        /**
         * the user activated the mute control and unmuted the creative.
         */
        unmute,

        /**
         * the user clicked the pause control and stopped the creative.
         */
        pause,

        /**
         * the user activated the rewind control to access a previous point in the creative timeline.
         */
        rewind,

        /**
         * the user activated the resume control after the creative had been stopped or paused.
         */
        resume,

        /**
         * the user activated a control to extend the video player to the edges of the viewer’s screen.
         */
        fullscreen,

        /**
         * the user activated the control to reduce video player size to original dimensions.
         */
        exitFullscreen,

        /**
         * the user activated a control to expand the creative.
         */
        expand,

        /**
         * the user activated a control to reduce the creative to its original dimensions.
         */
        collapse,

        /**
         * the user activated a control that launched an additional portion of the creative.
         * The name of this event distinguishes it from the existing “acceptInvitation” event described in
         * the 2008 IAB Digital Video In-Stream Ad Metrics Definitions, which defines the “acceptInivitation”
         * metric as applying to non-linear ads only. The “acceptInvitationLinear” event extends the metric
         * for use in Linear creative.
         */
        acceptInvitationLinear,

        /**
         * the user clicked the close button on the creative. The name of this event distinguishes it
         * from the existing “close” event described in the 2008 IAB Digital Video In-Stream Ad Metrics
         * Definitions, which defines the “close” metric as applying to non-linear ads only.
         * The “closeLinear” event extends the “close” event for use in Linear creative.
         */
        closeLinear,

        /**
         * the user activated a skip control to skip the creative, which is a different control than the one
         * used to close the creative.
         */
        skip,

        /**
         * the creative played for a duration at normal speed that is equal to or greater than the
         * value provided in an additional attribute for offset . Offset values can be time in the format
         * HH:MM:SS or HH:MM:SS.mmm or a percentage value in the format n% . Multiple progress events with
         * different values can be used to track multiple progress points in the Linear creative timeline.
         */
        progress
    }

    enum Error {
        Err100(100, "XML parsing error."),
        Err101(101, "VAST schema validation error."),
        Err102(102, "VAST version of response not supported."),
        Err200(200, "Trafficking error. Video player received an Ad type that it was not expecting and/or cannot display."),
        Err201(201, "Video player expecting different linearity."),
        DurationMismatch(202, "Video player expecting different duration."),
        Err203(203, "Video player expecting different size."),
        Err300(300, "General Wrapper error."),
        Err301(301, "Timeout of VAST URI provided in Wrapper element, or of VAST URI provided in a subsequent Wrapper element."),
        Err302(302, "Wrapper limit reached, as defined by the video player. Too many Wrapper responses have been received with no InLine response."),
        Err303(303, "No Ads VAST response after one or more Wrappers."),
        Err400(400, "General Linear error. Video player is unable to display the Linear Ad."),
        NotFound(401, "File not found. Unable to find Linear/MediaFile from URI."),
        Err402(402, "Timeout of MediaFile URI."),
        Err403(403, "Couldn't find MediaFile that is supported by this video player, based on the attributes of the MediaFile element."),
        PlaybackFailed(405, "Problem displaying MediaFile. Video player found a MediaFile with supported type but couldn't display it.MediaFile may include:unsupported codecs, different MIME type than MediaFile@type,unsupported delivery method, etc."),
        Err500(500, "General NonLinearAds error."),
        Err501(501, "Unable to display NonLinear Ad because creative dimensions do not align with creative display area(i.e.creative dimension too large)."),
        Err502(502, "Unable to fetch NonLinearAds/NonLinear resource."),
        Err503(503, "Couldn't find NonLinear resource with supported type."),
        Err600(600, "General CompanionAds error."),
        Err601(601, "Unable to display Companion because creative dimensions do not fit within Companion display area(i.e., no available space)."),
        Err602(602, "Unable to display Required Companion."),
        Err603(603, "Unable to fetch CompanionAds/Companion resource."),
        Err604(604, "Couldn't find Companion resource with supported type."),
        Unidentified(900, "Unidentified error"),
        Err901(901, "General VPAID error");

        private final int mCode;
        private final String mDescription;
        Error(int code, String description) {
            mCode = code;
            mDescription = description;
        }

        public int getCode() {
            return mCode;
        }

        public String getDescription() {
            return mDescription;
        }
    }
}
