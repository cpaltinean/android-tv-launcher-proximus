package tv.threess.threeready.api.log.model;

import java.io.Serializable;

/**
 * Enumerating the sources from where the playback or the application was started.
 *
 * @author Eugen Guzyk
 * @since 2019.01.23
 */
public enum StartAction implements Serializable {
    Restart,                  // The STB is rebooted, or app is restarted.
    StandBy,                  // The STB exited from standby.
    Hdmi,                     // HDMI connection state changed.
    Startup,                  // Application is started in a non transient mode
    UsageStats,               // Application is started in a non transient mode
    ActivityStart,            // Main activity is started.
    ScanComplete,             // Channel scan completed successfully.

    BtnUp,                    // Change the channel with CH+
    BtnDown,                  // Change the channel with CH-
    BtnNumber,                // Change the channel by entering channel number
    BtnTv,                    // Playback started pressing `Tv` button on the remote.
    BtnApp,                   // Third party application is started using remote button.
    BtnRadio,                 // Playback started pressing `Radio` button on the remote.
    BtnGuide,                 // Playback started pressing `Guide` button on the remote.
    BtnStop,                  // The viewing is stopped pressing `Stop` button on the remote.
    RemoteBtn,                // Playback / App started pressing a button on the remote.

    Ui,                       // Playback started invoked from ui
    Search,                   // Playback started selecting a search result (GA)
    TVPageContinue,           // Playback started after pressing the Continue button on
                              // Replay back to live dialog

    Pause,                    // The viewing is paused.
    Resume,                   // The viewing is resumed.
    Forward,                  // A fast forward action is invoked
    Rewind,                   // A rewind action is invoked

    VodEnded,                 // Vod ended
    TrailerEnded,             // Trailer ended
    RecordingEnded,           // Recording (Single) ended
    SeriesRecordingEnded,     // Series recording ended
    ReplayEnded,              // Replay ended

    BufferRestart,            // Rewind action to the beginning of the buffer
    TSTVEnded,                // Forward action to the end of the buffer

    JumpLive,                 // Playback started pressing `Live` button on screen.
    StartOver,                // Playback started pressing `Star-over` button on screen.
    LiveStartOver,            // Playback restarted pressing `Start-over` button on the screen which changes playback domain.
    TrickPlay,                // Trick-play: stop live and start replay.
    Rent,                     // Vod Playback started from renting.

    ContinuousPlay,           // The play out of the NPVR/CUTV item is done in the context of a continuous viewing session (binge watching).
    ProgramChanged,           // New broadcast started because the previous ended or next catchup starts

    Internet,                 // Playback started reconnecting Internet cable.
    Tutorial,                 // Tutorial started.
    DeepLink,                 // Playback started from deep link.

    Undefined                 // Unknown
}
