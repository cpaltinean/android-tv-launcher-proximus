package tv.threess.threeready.api.account.model.account;

import java.io.Serializable;

/**
 * Interface for consents, which holds the consent related configurations.
 *
 * @author Zsolt Bokor_
 * @since 2020.11.3
 */
public interface IConsent extends Serializable {

    /**
     * @return Identifier of the consent.
     */
    String getIdentifier();

    /**
     * @return Title of the consent.
     */
    String getTitle();

    /**
     * @return Description of the consent.
     */
    String getDescription();

    /**
     * @return Tooltip/help description of the consent.
     */
    String getToolTip();

    /**
     * @return True if the user accepted this consent.
     */
    boolean isAccepted();

    /**
     * @return True if the user has already selected an option in the past.
     */
    boolean hasFeedback();

    /**
     * @return Url of the tutorial video.
     */
    String getTutorialVideoUrl();
}
