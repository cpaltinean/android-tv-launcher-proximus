package tv.threess.threeready.api.middleware.model;

import androidx.annotation.NonNull;

import java.io.Serializable;

/**
 * Targeted ad replacement state.
 *
 * @author Karetka Mezei Zoltan
 * @since 2022.02.18
 */
public class ScteState extends BaseInfo<ScteState.Field> implements Serializable {
    public static final String FAIL_REASON_UNKNOWN = "Unknwon";
    public static final String FAIL_REASON_INVALID_AD_FILE = "Invalid targeted ad file";
    public static final String FAIL_REASON_INVALID_START_PTS = "Invalid targeted ad start pts";
    public static final String FAIL_REASON_INVALID_AD_DURATION = "Invalid targeted ad duration";
    public static final String FAIL_REASON_RESERVED_POSITION = "Reserved position";
    public static final String FAIL_REASON_AV_PID_MISMATCH = "AV pid mismatch";

    public ScteState() {
        super(Field.class);
    }

    @NonNull
    public State getState() {
        String state = get(Field.state, null);
        if (state == null) {
            return null;
        }
        return State.valueOf(state);
    }

    public enum Field implements BaseInfo.Field {
        start_pts,
        state,
        progress,
        fail_reason;

        @Override
        public String getKey() {
            return name();
        }
    }

    public enum State {
        READY,
        START,
        COMPLETE,
        CANCEL,
        FAIL,
    }
}
