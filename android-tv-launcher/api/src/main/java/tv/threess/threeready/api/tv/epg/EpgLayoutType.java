package tv.threess.threeready.api.tv.epg;

/**
 * Enumeration used for the type of the Epg layout.
 *
 * @author Daniela Toma
 * @since 2021.11.09
 */
public enum EpgLayoutType {
    ClassicEpg,
    MiniEpg
}
