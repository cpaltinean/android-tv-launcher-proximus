package tv.threess.threeready.api.config.model;

import java.io.Serializable;

import tv.threess.lib.di.Component;

/**
 * API related client configuration options.
 *
 * @author Barabas Attila
 * @since 5/12/21
 */
public interface ApiConfig extends Component, Serializable {

    /**
     * @return The page size used when it's not defined by the module config.
     */
    int getDefaultPageSize();
}
