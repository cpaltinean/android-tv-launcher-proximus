package tv.threess.threeready.api.account.setting;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.JsonConverter;
import tv.threess.threeready.api.pc.model.ParentalRating;

/**
 * Interface for reading a setting value as different types.
 */
public interface ReadableSetting {

    /**
     * Read the value.
     * This method will be used to convert to different types.
     */
    String getValue();

    /**
     * @param defValue The default value which will be returned when the key not found.
     * @return The store value of the key as string.
     */
    default String get(String defValue) {
        String value = getValue();
        if (value == null) {
            return defValue;
        }
        return value;
    }

    /**
     * @param defValue The default value which will be returned when the key not found.
     * @return The store value of the key as long.
     */
    default long get(long defValue) {
        String value = getValue();
        if (value == null) {
            return defValue;
        }
        return Long.parseLong(value);
    }

    /**
     * @param defValue The default value which will be returned when the key not found.
     * @return The store value of the key as boolean.
     */
    default boolean get(boolean defValue) {
        String value = getValue();
        if (value == null) {
            return defValue;
        }
        return Boolean.parseBoolean(value);
    }

    /**
     * @param defValue The default value which will be returned when the key not found.
     * @return The store value of the key as int.
     */
    default int get(int defValue) {
        String value = getValue();
        if (value == null) {
            return defValue;
        }
        return Integer.parseInt(value);
    }

    /**
     * @param defValue The default value which will be returned when the key not found.
     * @return The store value of the key as double.
     */
    default double get(double defValue) {
        String value = getValue();
        if (value == null) {
            return defValue;
        }
        return Double.parseDouble(value);
    }

    /**
     * @param defValue The default value which will be returned when the key not found.
     * @return The store value of the key as TEnum.
     */
    default <TEnum extends Enum<TEnum>> TEnum getEnum(TEnum defValue) {
        String value = getValue();
        if (value == null) {
            return defValue;
        }
        return Enum.valueOf((Class<TEnum>)defValue.getClass(), value);
    }

    /**
     * @return True if the key has a non null value.
     */
    default boolean hasValue() {
        return getValue() != null;
    }

    /**
     * @param clazz The class of the object stored in the cache.
     * @return The store object in the cache.
     */
    @Nullable
    default <T> T getObject(Class<T> clazz) {
        String value = getValue();
        if (value == null) {
            return null;
        }

        return Components.get(JsonConverter.class).fromJson(value, clazz);
    }


    /**
     * @param clazz The class of the object stored in the cache.
     * @return The store object in the cache.
     */
    @Nullable
    default <T> T getObject(Class<T> clazz, Function<String, T> defValue) {
        String value = getValue();
        if (value == null) {
            return defValue.apply(null);
        }

        try {
            return Components.get(JsonConverter.class).fromJson(value, clazz);
        } catch (Exception e) {
            return defValue.apply(value);
        }
    }

    /**
     * @return The stored value of the key as list.
     */
    @Nullable
    default List<String> getList() {
        String value = getValue();
        if (value == null) {
            return null;
        }

        try {
            return Components.get(JsonConverter.class).toStringList(value);
        } catch (Exception e) {
            // For backward compatibility. Try split by ",".
            return Arrays.asList(TextUtils.split(value, ","));
        }
    }

    /**
     * @return The stored value of the key as HashSet.
     */
    default Set<String> getSet() {
        List<String> value = getList();
        if (value == null) {
            return new HashSet<>();
        }

        return new HashSet<>(value);
    }

    /**
     * @return The stored value of the key as map.
     */
    @Nullable
    default Map<String, String> getMap() {
        String value = getValue();
        if (value == null) {
            return null;
        }

        return Components.get(JsonConverter.class).toStringMap(value);
    }

    /**
     * @param defValue The default value which will be returned when the key not found.
     * @return The store value of the key as ParentalRating.
     */
    default ParentalRating getParentalRating(ParentalRating defValue) {
        long value = get(-1L);
        if(value == -1) {
            return defValue;
        }

        return ParentalRating.valueOf((int)value);
    }
}
