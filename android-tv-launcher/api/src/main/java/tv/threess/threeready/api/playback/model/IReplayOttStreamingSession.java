package tv.threess.threeready.api.playback.model;

import java.util.List;

/**
 * Model representing a streaming session for replay playback.
 *
 * @author Barabas Attila
 * @since 2020.05.13
 */
public interface IReplayOttStreamingSession extends IOttStreamingSession {

    /**
     * @return The accurate start time of the content inside of the opened session.
     */
    long getAccurateStartTime();

    /**
     * @return A list of time slots which is restricted in the playback.
     */
    List<? extends IBlacklistSlot> getBlacklistedSlots();

    /**
     * Model represents a time slot in the playback which is restricted.
     */
    interface IBlacklistSlot {
        /**
         * @return The UTC start time of the restricted slot.
         */
        long getStart();

        /**
         * @return The UTC end time of the restricted slot.
         */
        long getEnd();

        /**
         * @param playbackStartTime The time when the playback was started.
         * @return True if the blacklisted block is currently active and needs to be applied.
         */
        boolean isActive(long playbackStartTime);

        /**
         * @return The reason why the playback is restricted in the slot.
         */
        String getReason();
    }
}
