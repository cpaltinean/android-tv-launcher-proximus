package tv.threess.threeready.api.vod.model;

import tv.threess.threeready.api.generic.model.ISeries;

/**
 * Base interface VOD Series. It's equals with the generic series but it's required in the presenter selector.
 *
 * @author Zsolt Bokor
 * @since 8/1/19
 */
public interface IVodSeries extends ISeries<IBaseVodItem>, IBaseVodSeries {

}
