package tv.threess.threeready.api.home.model.generic;

import androidx.annotation.Nullable;

import java.io.Serializable;

import tv.threess.threeready.api.home.model.module.MarkerType;

/**
 * Config options of a content marker.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public interface ContentMarkerDetails extends Serializable {

    /**
     * @return The text color of the content marker.
     */
    int getColor();

    /**
     * @return The background color of the content marker.
     */
    int getBackground();

    /**
     * @return The transparency of the content marker.
     */
    float getTransparency();

    /**
     * @return The display priority of the content marker.
     */
    int getPriority();

    /**
     * @return An enum which identifies the type of the marker.
     * E.g Now playing, Rent etc.
     */
    MarkerType getMarkerType();

    /**
     * @return True if the display options are defined for the marker.
     */
    boolean isEmpty();

    /**
     * @return If the background of the marker is a gradient color.
     */
    boolean hasGradient();

    /**
     * @return The gradient style of the background
     */
    @Nullable
    GradientStyle getGradient();
}
