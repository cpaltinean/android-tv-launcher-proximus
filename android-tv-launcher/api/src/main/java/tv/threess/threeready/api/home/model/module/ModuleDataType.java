/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.home.model.module;

/**
 * Enum which represent the type of data displayed in the module.
 *
 * Created by Barabas Attila on 4/13/2017.
 */

public enum ModuleDataType {
    TV_PROGRAM,
    TV_CHANNEL,
    REPLAY_CHANNEL,
    TV_NOW_NEXT,
    APPS,
    MIXED,
    NETFLIX,
    CONTENT_CATEGORY,
    START_WATCHING,
    EDITORIAL,
    SYSTEM_NOTIFICATIONS,
    GALLERY,
    UNDEFINED
}
