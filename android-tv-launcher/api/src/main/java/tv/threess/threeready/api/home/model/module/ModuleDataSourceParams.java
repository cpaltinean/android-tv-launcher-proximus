package tv.threess.threeready.api.home.model.module;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.home.SelectorOption;

/**
 * Sorting and filtering parameters for a data source.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public interface ModuleDataSourceParams extends Serializable {

    /**
     * @return An array where the first item represents the start index and the second the item count.
     */
    default int[] getSize() {
        return ArrayUtils.EMPTY_INT;
    }

    /**
     * @return An array with the sort options needs to be applied on the data source.
     */
    default String[] getSort() {
        return ArrayUtils.EMPTY_STRING;
    }

    /**
     * @param key THe key of the sort option.
     * @return The sort option with the given key.
     */
    @Nullable
    default String getSort(String key) {
        return null;
    }

    /**
     * @param key The key of the filter.
     * @return True has value for that key.
     */
    default boolean hasFilter(String key) {
        return getFilter(key) != null;
    }

    /**
     * @param key The key of the filter.
     * @return The filter with the given key as string.
     */
    @Nullable
    default String getFilter(String key) {
        return null;
    }

    /**
     * @param key The key of the filter.
     * @return The filter with the given key as a list of strings.
     */
    @Nullable
    default List<String> getSortList(String key) {
        return null;
    }

    /**
     * @param key The key of the filter.
     * @param defValue The value to be returned with the filer with key is not available.
     * @return The filter with the given key as long.
     */
    default long getFilter(String key, long defValue) {
        String value = getFilter(key);
        if (value == null) {
            return defValue;
        }
        return Long.parseLong(value);
    }

    /**
     * @param key The key of the filter.
     * @param defValue The value to be returned with the filer with key is not available.
     * @return The filter with the given key as integer.
     */
    default int getFilter(String key, int defValue) {
        String value = getFilter(key);
        if (value == null) {
            return defValue;
        }
        return Integer.parseInt(value);
    }

    /**
     * @param key The key of the filter.
     * @param defValue The value to be returned with the filer with key is not available.
     * @return The filter with the given key as boolean.
     */
    default boolean getFilter(String key, boolean defValue) {
        String value = getFilter(key);
        if (value == null) {
            return defValue;
        }
        return Boolean.parseBoolean(value);
    }

    /**
     * @param key The key of of the filer.
     * @param clazz  The type of the value in which it needs to returned.
     * @return The value for the given key and type or null if there is no such.
     */
    @Nullable
    default <T extends Serializable> T getFilter(String key, Class<T> clazz) {
        return null;
    }

    /**
     * @param key The key of the filter.
     * @return The filter with the given key as a list of strings.
     */
    @Nullable
    default List<String> getFilterList(String key) {
        return null;
    }

    /**
     * Add a new filter value with the specific key.
     * @param key The key of the new filter.
     * @param value The value of the new filter.
     */
    default void addFilter(String key, Serializable value) {}
}
