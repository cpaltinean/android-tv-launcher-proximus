/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.log;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Process;
import android.text.TextUtils;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.BuildConfig;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.generic.helper.BundleBuilder;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.log.helper.ErrorType;
import tv.threess.threeready.api.log.model.Error;
import tv.threess.threeready.api.log.model.ErrorHandler;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.GenericEvent;
import tv.threess.threeready.api.log.model.GenericKeyEvent;
import tv.threess.threeready.api.log.model.ReporterInfo;


/**
 * Handles message and event logging, and sends errors to the UI if needed.
 *
 * @author Paul
 * @since 2017.06.15
 */
public class Log {
    private static final String TAG = tag(Log.class);

    private static final String WORKER_THREAD_NAME = "Logger";

    public static final String LOG_ACTION = "tv.threess.threeready.ACTION_LOG";
    public static final String ARG_ERROR = "error";
    private static final int MAX_EXTRA_FOR_ITERATIONS = 100;

    private static Level sLogcatLogLevel = Level.Verbose;
    private static Level sEventLogLevel = Level.None;

    private static WeakReference<Context> sContext = new WeakReference<>(null);
    private static Handler sHandler;

    /**
     * Function to build TAG based on the class.
     *
     * @param cls Class reference
     * @return TAG name
     */
    public static String tag(Class<?> cls) {
        return cls.getCanonicalName();
    }

    /**
     * Initialize context and log levels.
     */
    public static void init(Context context, Level logcatLogLevel, Level eventLogLevel) {
        sLogcatLogLevel = logcatLogLevel;
        sEventLogLevel = eventLogLevel;
        sContext = new WeakReference<>(context);

        if (sHandler != null) {
            return;
        }
        HandlerThread handlerThread = new HandlerThread(WORKER_THREAD_NAME, Process.THREAD_PRIORITY_BACKGROUND);
        handlerThread.start();
        sHandler = new Handler(handlerThread.getLooper());
    }

    /**
     * Update a reporter with the reporter info from backend.
     */
    public static void update(Class<? extends Reporter<?, ?>> reporter, ReporterInfo info) {
        try {
            sHandler.post(() -> sContext.get().getContentResolver().call(ReportContract.BASE_URI,
                    ReportContract.CALL_UPDATE_REPORTER,
                    reporter.getCanonicalName(),
                    new BundleBuilder().put(ReportContract.EXTRA_REPORTER_INFO, info).build()
            ));
        } catch (Exception e) {
            android.util.Log.e(TAG, "Failed to update reporter: " + reporter.getCanonicalName(), e);
        }
    }

    /**
     * @param level Level of the event to be sent.
     * @return True if we can send the event to the logcat, otherwise false.
     */
    public static boolean canSendToLogcat(Level level) {
        if (sLogcatLogLevel == null || level == null) {
            return false;
        }
        return level.mLevel >= sLogcatLogLevel.mLevel;
    }

    /**
     * @param level Level of the event to be sent.
     * @return True if we can send the event to data collection API, otherwise false.
     */
    private static boolean canSendToDataCollection(Level level) {
        if (sEventLogLevel == null || level == null || sContext.get() == null) {
            return false;
        }
        return level.mLevel >= sEventLogLevel.mLevel;
    }

    public static void v(String tag, String message, Throwable cause) {
        if (canSendToLogcat(Level.Verbose)) {
            android.util.Log.v(tag, toString(message, cause));
            event(Level.Verbose, tag, message, cause);
        }
    }

    public static void v(String tag, String message) {
        Log.v(tag, message, null);
    }

    public static void d(String tag, String message, Throwable cause) {
        if (canSendToLogcat(Level.Debug)) {
            android.util.Log.d(tag, toString(message, cause));
            event(Level.Debug, tag, message, cause);
        }
    }

    public static void d(String tag, String message) {
        Log.d(tag, message, (Throwable) null);
    }

    public static void d(String tag, String message, Intent intent) {
        if (intent == null) {
            return;
        }

        StringBuilder sbLog = new StringBuilder(message);
        // intent action
        String action = intent.getAction();
        if (!TextUtils.isEmpty(action)) {
            sbLog.append("\n").append("Intent action: ").append(action);
        }

        // intent uri
        Uri uri = intent.getData();
        if (uri != null) {
            sbLog.append("\n").append("Uri: ").append(uri);
        }

        d(tag, sbLog.toString(), intent.getExtras());
    }

    public static void d(String tag, String message, Bundle bundle) {
        StringBuilder sbLog = new StringBuilder(message);
        if (bundle != null) {
            int index = 0;
            for (String key : bundle.keySet()) {
                sbLog.append("\n").append("[").append(key).append("]: ").append(bundle.get(key));
                index++;
                // max allowed iterations 100
                if (index > MAX_EXTRA_FOR_ITERATIONS) {
                    break;
                }
            }
        }

        if (sbLog.length() > 0) {
            Log.d(tag, sbLog.toString());
        }
    }

    public static void i(String tag, String message, Throwable cause) {
        if (canSendToLogcat(Level.Info)) {
            android.util.Log.i(tag, toString(message, cause));
            event(Level.Info, tag, message, cause);
        }
    }

    public static void i(String tag, String message) {
        Log.i(tag, message, null);
    }

    public static void w(String tag, String message, Throwable cause) {
        if (canSendToLogcat(Level.Warn)) {
            android.util.Log.w(tag, toString(message, cause));
            event(Level.Warn, tag, message, cause);
        }
    }

    public static void w(String tag, String message) {
        Log.w(tag, message, null);
    }

    public static void e(String tag, String message, Throwable cause) {
        if (canSendToLogcat(Level.Error)) {
            android.util.Log.e(tag, toString(message, cause));
            event(Level.Error, tag, message, cause);
        }
    }

    public static void e(String tag, String message) {
        Log.e(tag, message, null);
    }

    public static void e(String tag, Throwable cause) {
        Log.e(tag, null, cause);
    }

    /** Log the error and notify the ui to display it */
    public static void error(String tag, String message, Error error) {
        Log.e(tag, message + "\nerror: " + error);
        Log.event(error);
        notifyUI(error);
    }

    /** Log the error and notify the ui to display it */
    public static void error(String tag, String message, Throwable throwable, ErrorType errorType) {
        Error.Builder errorBuilder = new Error.Builder(errorType);
        if (throwable instanceof BackendException) {
            errorBuilder.backendException((BackendException) throwable);
        }

        Error error = errorBuilder.build();
        Log.e(tag, message + "\nerror: " + error, throwable);
        Log.event(error);
        notifyUI(error);
    }

    /**
     * Send the error to other components.
     */
    private static void notifyUI(Error error) {
        if (error == null || error.getErrorHandler() == ErrorHandler.NONE) {
            return;
        }

        Intent intent = new Intent(LOG_ACTION);
        intent.putExtra(ARG_ERROR, error);
        LocalBroadcastManager.getInstance(sContext.get()).sendBroadcast(intent);
    }

    public static void event(Event<?, ?> event) {
        Context context = sContext.get();
        if (context == null || sHandler == null || event == null) {
            return;
        }

        sHandler.post(() -> {
            Bundle arg = new Bundle();
            arg.putSerializable(ReportContract.Event.NAME, event.getName());
            arg.putSerializable(ReportContract.Event.DOMAIN, event.getDomain());
            arg.putSerializable(ReportContract.Event.TIMESTAMP, event.getTimestamp());

            arg.putBundle(ReportContract.Event.VALUE, event.getDetails());

            context.getContentResolver().call(ReportContract.BASE_URI, ReportContract.CALL_SEND_EVENT,
                    null, arg);
        });
    }

    public static void event(Error error) {
        InternetChecker internetChecker = Components.get(InternetChecker.class);
        event(new Event<>(GenericEvent.ErrorEvent)
                .addDetail(GenericKeyEvent.MESSAGE, error.getCustomMessage())
                .addDetail(GenericKeyEvent.DOMAIN, error.getDomain())
                .addDetail(GenericKeyEvent.CODE, error.getErrorCode())
                .addDetail(GenericKeyEvent.BACKEND_AVAILABLE, internetChecker.isBackendAvailable())
                .addDetail(GenericKeyEvent.INTERNET_AVAILABLE, internetChecker.isInternetAvailable())
                .addDetail(GenericKeyEvent.KEY, error.getBodyTranslationKey())
        );
    }

    private static void event(Level level, String tag, String message, Throwable cause) {
        if (!canSendToDataCollection(level)) {
            return;
        }
        Event<Level, Level.Detail> event = new Event<>(level);
        event.addDetail(Level.Detail.message, message);
        event.addDetail(Level.Detail.level, level.name());
        event.addDetail(Level.Detail.tag, tag);
        if (cause != null) {
            event.addDetail(Level.Detail.error, cause.getMessage());
            event.addDetail(Level.Detail.trace, toString(null, cause));
        }
        Log.event(event);
    }

    public enum Level implements Event.Kind {
        Verbose(android.util.Log.VERBOSE),
        Debug(android.util.Log.DEBUG),
        Info(android.util.Log.INFO),
        Warn(android.util.Log.WARN),
        Error(android.util.Log.ERROR),
        None(Level.LOG_LEVEL_NONE);

        private static final int LOG_LEVEL_NONE = 8;
        private final int mLevel;

        Level(int level) {
            mLevel = level;
        }

        public int getLevel() {
            return mLevel;
        }

        @Override
        public String domain() {
            return "Logging";
        }

        private enum Detail implements Event.Detail<Level> {
            message,
            level,
            tag,

            error,
            trace
        }
    }

    // Logging methods only for debugging purposes
    public static void d(String tag, Cursor cursor, int maxRows, Throwable cause) {
        if (!BuildConfig.DEBUG) {
            return;
        }

        if (cursor == null || cursor.getCount() == 0) {
            android.util.Log.d(tag, "Cursor is null", cause);
            return;
        }

        int position = cursor.getPosition();
        try {
            int rows = cursor.getCount();
            int cols = cursor.getColumnCount();

            if (rows == 0) {
                android.util.Log.d(tag, "cursor is empty");
                return;
            }

            for (int row = 0, max = 0; row < maxRows; ++row) {
                if (!cursor.moveToPosition(row)) {
                    continue;
                }

                if (++max > MAX_EXTRA_FOR_ITERATIONS) {
                    break;
                }

                StringBuilder message = new StringBuilder();
                message.append("row[").append(row + 1).append('/').append(rows).append("]: {");
                for (int col = 0; col < cols; ++col) {
                    if (col > 0) {
                        message.append(", ");
                    }
                    message.append("\n\t").append(cursor.getColumnName(col)).append(": `");
                    switch (cursor.getType(col)) {
                        case Cursor.FIELD_TYPE_NULL:
                            message.append("null");
                            break;

                        case Cursor.FIELD_TYPE_INTEGER:
                            message.append(cursor.getLong(col));
                            break;

                        case Cursor.FIELD_TYPE_FLOAT:
                            message.append(cursor.getDouble(col));
                            break;

                        case Cursor.FIELD_TYPE_STRING:
                            message.append('"').append(cursor.getString(col)).append('"');
                            break;

                        case Cursor.FIELD_TYPE_BLOB:
                            message.append('"').append(new String(cursor.getBlob(col))).append('"');
                            break;
                    }
                    message.append('`');
                }
                message.append("\n}");

                android.util.Log.d(tag, message.toString());
            }
        } finally {
            cursor.moveToPosition(position);
        }
    }

    /**
     * Returns a string in a readable format for the specified duration.
     *
     * @param duration the duration in milliseconds
     * @return String
     */
    public static String formatDuration(long duration) {
        String sign = duration < 0 ? "-" : "";
        long millis = (int) Math.abs(duration % 1000);
        long seconds = (int) Math.abs(TimeUnit.MILLISECONDS.toSeconds(duration) % 60);
        long minutes = (int) Math.abs(TimeUnit.MILLISECONDS.toMinutes(duration) % 60);
        long hours = (int) Math.abs(TimeUnit.MILLISECONDS.toHours(duration));
        return String.format(Locale.getDefault(), "%s%d:%02d:%02d.%03d", sign, hours, minutes, seconds, millis);
    }

    public static String timestamp(long timestamp) {
        return TimeBuilder.local(timestamp).toString();
    }

    private static String toString(String message, Throwable cause) {
        if (cause == null) {
            return message;
        }

        StringWriter sw = new StringWriter();
        if (message != null) {
            sw.append(message).append('\n');
        }

        PrintWriter pw = new PrintWriter(sw);
        cause.printStackTrace(pw);
        pw.flush();

        return sw.toString();
    }
}
