/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.search.model;

/**
 * Local text based search types.
 *
 * @author Barabas Attila
 * @since 2017.07.19
 */
public enum LocalSearchType {
    Channel,
    BroadcastUiLanguage,
    BroadcastNonUiLanguage,
    Vod
}
