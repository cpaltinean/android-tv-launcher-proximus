/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.pc;

import android.os.Handler;
import android.os.SystemClock;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.model.IParentalControlSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Top manager class for all parental rating reflated components.
 * It handles the parental control configuration changes (activation, rating)
 * and decides if the content should be restricted or not.
 *
 * Other components can register listeners to get notified
 * about the parental control configuration changes.
 *
 *
 * @author Barabas Attila
 * @since 2019.06.02
 */
public class ParentalControlManager implements Component {
    private static final String TAG = Log.tag(ParentalControlManager.class);

    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);

    private long mTemporaryUnblockTimeoutMillis;

    /**
     * The {@link SystemClock#elapsedRealtime()} when the temporary unblock should be disabled.
     */
    private long mUnblockExpireElapsedTime = -1L;
    private final Handler mUnblockHandler = new Handler();

    private final CopyOnWriteArrayList<WeakReference<IParentalControlListener>> mParentalRatingListeners = new CopyOnWriteArrayList<>();
    private IContentItem mCurrentlyPlaying;

    private boolean mIsEnabled = false;
    private ParentalRating mSelectedParentalRating = ParentalRating.Rated18;

    private Disposable mParentalSettingsDisposable;

    /**
     * Initialize the parental control. Sets the default active state and the selected rating.
     */
    public Single<IParentalControlSettings> initialize() {
        mTemporaryUnblockTimeoutMillis = Components.get(AppConfig.class)
                .getParentalControlTemporalUnblockSeconds(TimeUnit.MILLISECONDS);

        CompletableFuture<IParentalControlSettings> completableFuture = new CompletableFuture<>();

        RxUtils.disposeSilently(mParentalSettingsDisposable);
        mParentalSettingsDisposable = mAccountRepository.getParentalControlSettings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(parentalControlSettings -> {
                    if (!completableFuture.isDone()) {
                        completableFuture.complete(parentalControlSettings);
                    }

                    Log.d(TAG, "Parental control changes. Enabled : " + parentalControlSettings.isEnabled()
                            + ", rating : " + parentalControlSettings.getParentalRating());

                    if (parentalControlSettings.isEnabled() == mIsEnabled
                            && parentalControlSettings.getParentalRating() == mSelectedParentalRating) {
                        return;
                    }

                    mIsEnabled = parentalControlSettings.isEnabled();
                    mSelectedParentalRating = parentalControlSettings.getParentalRating();

                    if (parentalControlSettings.isEnabled()) {
                        mCurrentlyPlaying = isRestricted(mCurrentlyPlaying) ? null : mCurrentlyPlaying;
                    }

                    mUnblockHandler.removeCallbacksAndMessages(null);
                    notifyListeners();
                });

        return Single.fromFuture(completableFuture);
    }

    /**
     * Set the selected parental rating for the manager.
     * The content restriction will be validated based on this value.
     * All the subscribed components will be notified about the rating change.
     * @param parentalRating - The new parental rating which will be used.
     * @return An RX completable to subscribed.
     */
    public Completable selectParentalRating(@NonNull ParentalRating parentalRating) {
        if (parentalRating == mSelectedParentalRating) {
            // Nothing changed. Ignore.
            return Completable.complete();
        }

        Log.d(TAG, "Set parental rating. " + parentalRating);
        return mAccountRepository
                .setParentalRating(parentalRating)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .andThen(Completable.fromAction(() -> {

                    mSelectedParentalRating = parentalRating;
                    mCurrentlyPlaying = isRestricted(mCurrentlyPlaying) ? null : mCurrentlyPlaying;
                    notifyListeners();
                }));
    }

    /**
     * Enable the parental control. The previously selected rating will be applied.
     * All the subscribed components will be notified about the parental control changes.
     * @return An RX completable to subscribed on the action.
     */
    public Completable enableParentalControl() {
        if (isParentalControlEnabled()) {
            // Nothing to do. Already enabled.
            return Completable.complete();
        }

        Log.d(TAG, "Enable parental control.");
        return mAccountRepository
                .enableParentalControl(true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Disable the parental control. The previously selected rating will be applied.
     * All the subscribed components will be notified about the parental control changes.
     * @return An RX completable to subscribed on the action.
     */
    public Completable disableParentalControl() {
        if (!isParentalControlEnabled()) {
            // Nothing to do. Already disabled.
            return Completable.complete();
        }

        Log.d(TAG, "Disabled parental control.");
        return mAccountRepository
                .enableParentalControl(false)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Temporally unblock the parental control.
     * The parental control will be activated again after th configured timeout.
     * The subscribed components will be notified when parental control unblocks and blocks again.
     */
    public void unblockContent() {
        if (!isParentalControlEnabled()) {
            // Nothing to do. The parental control is disabled.
            return;
        }

        // Temporally disable the parental control.
        mIsEnabled = false;
        notifyListeners();

        Log.d(TAG, "Temporally unblock parental control. "
                + TimeUnit.MILLISECONDS.toSeconds(mTemporaryUnblockTimeoutMillis) + " seconds");

        mUnblockExpireElapsedTime = SystemClock.elapsedRealtime() + mTemporaryUnblockTimeoutMillis;
        mUnblockHandler.postDelayed(mDisableTempUnblockRunnable, mTemporaryUnblockTimeoutMillis);
    }

    /**
     * Should be called when the box wakes up from standby.
     */
    public void onWakeFromStandby() {
        mUnblockHandler.removeCallbacksAndMessages(null);

        if (mUnblockExpireElapsedTime <= 0) {
            // No active temporal unblock.
            return;
        }

        // Calculate the new timeout for temporal unblock.
        long currentElapsedTime = SystemClock.elapsedRealtime();
        if (mUnblockExpireElapsedTime > currentElapsedTime) {
            mUnblockHandler.postDelayed(mDisableTempUnblockRunnable,
                    mUnblockExpireElapsedTime - currentElapsedTime);

        // Timer already expired, trigger immediately.
        } else {
            mUnblockHandler.post(mDisableTempUnblockRunnable);
        }
    }


    /**
     * Should be used to set the currently running item
     * which was started last time when the parental control was unblocked.
     * The playback and player UI will be not blocked for this item.
     * @param currentlyPlaying - The content item which was started last time.
     */
    public void setCurrentlyPlaying(@Nullable IContentItem currentlyPlaying) {
        Log.d(TAG, "Set currently playing item. " + currentlyPlaying);
        mCurrentlyPlaying = currentlyPlaying;
    }

    /**
     * @return True if the parental control is currently activated.
     */
    public boolean isParentalControlEnabled() {
        return mIsEnabled;
    }

    /**
     * @param contentItem The content item for which the parental control restriction should be checked.
     * @return True if the content should be restricted based on the current parental control settings.
     */
    public boolean isRestricted(IBaseContentItem contentItem) {
        return contentItem != null && isRestricted(contentItem, false);
    }

    /**
     * @param contentItem The content item for which the parental control restriction should be checked.
     * @param ignoreIfPlaying If true the currently running item will not be restricted.
     * @return True if the content should be restricted based on the current parental control settings.
     */
    public boolean isRestricted(IBaseContentItem contentItem, boolean ignoreIfPlaying) {
        IContentItem currentlyPlaying = mCurrentlyPlaying;

        // Ignore if content already playing.
        if (ignoreIfPlaying && contentItem != null && currentlyPlaying != null
                && Objects.equals(contentItem.getGlobalId(), currentlyPlaying.getGlobalId())) {
            return false;
        }

        // Ignore if one of the the content variant is already playing.
        if (ignoreIfPlaying && currentlyPlaying instanceof IVodItem
                && contentItem instanceof IVodVariant) {
            IVodItem playingVod = (IVodItem) currentlyPlaying;
            IVodVariant variant = (IVodVariant) contentItem;
            if (playingVod.getVariantIds().contains(variant.getId())) {
                return false;
            }
        }

        // Compare age rating.
        return contentItem != null && isRestricted(contentItem.getParentalRating());
    }

    /**
     * @param parentalRating The parental rating for which the restriction should be checked.
     * @return True if the content with the given rating should be restricted.
     */
    public boolean isRestricted(ParentalRating parentalRating) {
        return parentalRating != null && isParentalControlEnabled()
                && getSelectedParentalRating().getMinimumAge() <= parentalRating.getMinimumAge();
    }

    /**
     * @return The previously selected parental rating
     * or {@link ParentalRating#Undefined} if the parental control is not activated.
     */
    public ParentalRating getSelectedParentalRating() {
        if (isParentalControlEnabled()) {
            return mSelectedParentalRating;
        }

        return ParentalRating.Undefined;
    }

    /**
     * Add a listener which will be notified when the parental control changes.
     * To avoid memory leaks, all the registered listeners should be removed with {@link #removeListener}
     * @param parentalControlListener The listener which should be registered.
     */
    public void addListener(IParentalControlListener parentalControlListener) {
        removeListener(parentalControlListener);
        mParentalRatingListeners.add(new WeakReference<>(parentalControlListener));
    }

    /**
     * Used to remove a previously registered listener. {@see #addListener}
     * @param parentalControlListener The listener which should be removed.
     */
    public void removeListener(IParentalControlListener parentalControlListener) {
        mParentalRatingListeners.removeIf((item) -> {
            IParentalControlListener listener = item.get();
            return listener == null || listener == parentalControlListener;
        });
    }

    /**
     * Notifies all the registered listeners
     */
    private void notifyListeners() {
        for (WeakReference<IParentalControlListener> wl : mParentalRatingListeners) {
            IParentalControlListener listener = wl.get();
            if (listener != null) {
                listener.onParentalRatingChanged(getSelectedParentalRating());
            }
        }
    }

    /**
     * Runnable used to enable the parental control when the temporal unblock expires.
     */
    private final Runnable mDisableTempUnblockRunnable = () -> {
        Log.d(TAG, "Temporally unblock ends. Block content.");

        // Enable the parental control.
        mUnblockExpireElapsedTime = -1;
        mIsEnabled = true;
        notifyListeners();
    };

    /**
     * Listener in which true different components will be notified about the parental control changes.
     */
    public interface IParentalControlListener {
        void onParentalRatingChanged(ParentalRating parentalRating);
    }
}
