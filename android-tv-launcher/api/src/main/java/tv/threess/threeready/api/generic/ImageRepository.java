package tv.threess.threeready.api.generic;

import androidx.annotation.WorkerThread;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.tv.model.TvChannel;

/**
 *  Handler class used for addition business logic of image url generation.
 *
 * @author Barabas Attila
 * @since 2020.03.09
 */
public interface ImageRepository extends Component {

    /**
     * Get the best logo url for a channel.
     * @param channel The channel for which the logo url needs to be returned.
     * @param width The expected width of the logo in pixel.
     * @param height The expected height of the logo in pixel.
     * @return The channel logo url.
     */
    @WorkerThread
    String getLogoUrl(TvChannel channel, int width, int height);

    /**
     * Get the best image url for the content.
     * @param contentItem The content for which the url needs to be returned.
     * @param width The expected width of the image in pixel or 0 if not defined.
     * @param height The expected height of the image in pixel or 0 if not defined.
     * @return The image url of the content.
     */
    @WorkerThread
    String getCoverUrl(IBaseContentItem contentItem, int width, int height);

    /**
     * Gets the cover url for Voice Search using image name.
     * @param imageName The name of the image.
     * @param width The expected width of the image in pixel or 0 if not defined.
     * @param height The expected height of the image in pixel or 0 if not defined.
     * @param isVod Flag used to know if the cover is for Program or Vod.
     */
    @WorkerThread
    String getCoverUrlForVoiceSearch(String imageName, int width, int height, boolean isVod);

    /**
     * Get the best image url for an image source.
     * @param width The expected width of the image in pixel or 0 if not defined.
     * @param height The expected height of the image in pixel or 0 if not defined.
     * @return The image url of the source.
     */
    @WorkerThread
    String getImageSourceUrl(IImageSource imageSource, int width, int height);
}
