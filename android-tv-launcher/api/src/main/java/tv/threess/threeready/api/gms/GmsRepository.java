package tv.threess.threeready.api.gms;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import tv.threess.lib.di.Component;
import tv.threess.threeready.api.gms.model.InstalledAppInfo;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.notification.NotificationPriority;
import tv.threess.threeready.api.notification.model.NotificationItem;

/**
 * Implementation of the Gms specific repository.
 * Generally serving UI components.
 *
 * @author David Bondor
 * @since 2022.04.19
 */
public interface GmsRepository extends Component {

    /**
     * Returns a reactive stream of the installed application.
     * Filtered and sorted by the given data source parameters.
     */
    Observable<ModuleData<InstalledAppInfo>> getApps(final ModuleConfig config);

    /**
     * @return An RX observable which emits system notification for the data source.
     */
    Observable<ModuleData<NotificationItem>> getSystemNotifications(final ModuleConfig config);

    /**
     * @return An RX observable which emits system notification with the given priority.
     */
    Observable<List<NotificationItem>> getSystemNotifications(NotificationPriority priority);

    /**
     * Return a reactive stream of stored notifications updated with the system ones.
     */
    Observable<List<NotificationItem>> getUpdatedNotifications();

    /**
     * Return an observable to store notifications.
     */
    Completable saveNotifications(List<NotificationItem> notificationsList);

    /**
     * Updates the last opened time from an application.
     * @param packageName The package of the application which was opened.
     * @param lastOpenedTime The time when the app was opened.
     */
    Completable updateAppLastOpenedTime(final String packageName, final long lastOpenedTime);
}
