package tv.threess.threeready.api.home.model.generic;

import java.io.Serializable;
import java.util.List;

import tv.threess.threeready.api.gms.model.EditorialAppInfo;

/**
 * 3rd party app related config options.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public interface PlayApps extends Serializable {

    /**
     * @return Featured 3rd party app package ids.
     */
    List<EditorialAppInfo> getFeatured();

    /**
     * @return Store app package ids.
     */
    List<EditorialAppInfo> getStore();
}
