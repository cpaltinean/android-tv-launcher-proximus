/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.generic.receivers;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.log.Log;


/**
 * This class serves to get network connection changes
 *
 * Created by Dalma Medve on 1/30/2018.
 */
public class NetworkChangeReceiver extends BaseBroadcastReceiver implements Component {
    public static final String TAG = Log.tag(NetworkChangeReceiver.class);

    private final IntentFilter mIntentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);

    List<WeakReference<NetworkChangeListener>> mListeners = new CopyOnWriteArrayList<>();

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.d(TAG, "Network has changed. " + intent);
        boolean connected = !intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
        ArrayUtils.notifyAndCleanup(mListeners, listener -> listener.onNetworkChange(connected));
    }

    public void addConnectivityChangeListener(NetworkChangeListener listener) {
        removeConnectivityChangeListener(listener);
        mListeners.add(new WeakReference<>(listener));
    }

    /**
     * Remove the previously registered connectivity change listener.
     */
    public void removeConnectivityChangeListener(NetworkChangeListener listenerToRemove) {
        mListeners.removeIf((item) -> {
            NetworkChangeListener listener = item.get();
            return listener == null || listener == listenerToRemove;
        });
    }

    @Override
    public IntentFilter getIntentFilter() {
        return mIntentFilter;
    }

    public interface NetworkChangeListener {
        void onNetworkChange(boolean connected);
    }
}
