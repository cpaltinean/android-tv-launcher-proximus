package tv.threess.threeready.api.home.model.module;

import androidx.annotation.Nullable;

import java.io.Serializable;

/**
 * Describes the service, method and parameters to request data for a module.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public interface ModuleDataSource extends Serializable {

    /**
     * @return Enum which identifies the service to request data from.
     */
    ModuleDataSourceService getService();

    /**
     * @return The method on the service to request data from.
     */
    ModuleDataSourceMethod getMethod();

    /**
     * @return Extra parameters for the data requests.
     */
    @Nullable
    default ModuleDataSourceParams getParams() {
        return null;
    }
}
