package tv.threess.threeready.api.home.model.module;

import java.io.Serializable;

/**
 * Describes the style of a module.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public interface ModuleEditorialStyle extends Serializable {

    /**
     * @return The style which needs to be applied on the editorial items in the module.
     * The individual items in the module can override the style.
     */
    EditorialItemStyle getItemStyle();

    /**
     * @return True if the module has a title color.
     */
    boolean hasTitleColor();

    /**
     * @return The font color of hte module title.
     */
    int getTitleColor();

    /**
     * @return The color of the module background.
     */
    int getBackgroundColor();

    /**
     * @return The resource file name or url of the module background image.
     */
    String getBackgroundImage();

    /**
     * @return True if the module has a backgroundi mage.
     */
    boolean hasBackgroundImage();
}
