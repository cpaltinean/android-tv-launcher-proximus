package tv.threess.threeready.api.vod.model;

/**
 * Interface for currency type.
 *
 * @author Eugen Guzyk
 * @since 2018.11.14
 */
public enum CurrencyType {
    Credit,
    Euro,

    Unknown
}
