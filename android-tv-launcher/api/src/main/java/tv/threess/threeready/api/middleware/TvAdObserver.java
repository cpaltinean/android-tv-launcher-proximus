package tv.threess.threeready.api.middleware;

import tv.threess.threeready.api.middleware.model.Scte35Info;
import tv.threess.threeready.api.middleware.model.ScteState;

/**
 * Observer interface used to notify targeted advertisement related callbacks.
 *
 * @author Karetka Mezei Zoltan
 */
public interface TvAdObserver {

    /**
     * Callback fired when a SCTE marker is read from the stream.
     * @param marker the SCTE marker read from stream
     */
    void onMarkerChange(Scte35Info marker);

    /**
     * Callback fired when Ad replacement status change: started, stopped, failed or progress.
     * @param state the state the ad replacement is at the time the callback is fired.
     */
    void onStateChange(ScteState state);
}
