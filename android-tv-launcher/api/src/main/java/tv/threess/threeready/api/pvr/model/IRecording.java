/*
 * Entity class representing a record
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.pvr.model;

import tv.threess.threeready.api.tv.model.IBroadcast;

/**
 * Entity class representing a recording for a broadcast.
 *
 * @author Barabas Attila
 * @since 2018.10.23
 */
public interface IRecording extends IBroadcast {
    /**
     * @return unique identifier for the recording.
     */
    String getRecordingId();

    /**
     * @return Identifier which uniquely identifies the item even between different type of items.
     * E.g. Returns backs different identifier for broadcast and recording of the same program.
     */
    default String getGlobalId() {
        return getRecordingId();
    }

    /**
     * @return system recording id.
     */
    String getSystemRecordId();

    /**
     * @return True if the recording is already completed.
     */
    boolean isRecordingFinished();

    /**
     * @return true if the recording can be watched on the device.
     */
    default boolean canWatchRecording() {
        return getRecordingStart() < System.currentTimeMillis();
    }

    /**
     * @return the start time of the recording im milliseconds.
     * Only available for recording which are already completed.
     */
    long getRecordingStart();

    /**
     * @return the end time of the recording im milliseconds.
     * Only available for recording which are already completed.
     */
    long getRecordingEnd();

    /**
     * @return the time until the recording available to watch.
     * Only availability for completed and ongoing recordings.
     */
    long getAvailabilityTime();

    /**
     * @return True if the recording is failed.
     */
    boolean isFailedRecording();

    /**
     * @return the recorded status.
     */
    default SingleRecordingStatus getRecordedStatus() {
        if (isLiveRecording(true)) {
            return SingleRecordingStatus.RECORDING;
        }
        if (isRecordingFinished()) {
            return SingleRecordingStatus.COMPLETED;
        }
        return SingleRecordingStatus.SCHEDULED;
    }

    /**
     * @return in case the recording is a live one, then return true
     */
    default boolean isLiveRecording() {
        return isLiveRecording(false);
    }

    /**
     * @param useRecordingStart true if we want to use the recording start, which includes a pre-padding value.
     */
    default boolean isLiveRecording(boolean useRecordingStart) {
        long now = System.currentTimeMillis();
        return getRecordingEnd() > now
                && (useRecordingStart ? getRecordingStart() : getStart()) < now;
    }

    /**
     * Calculates the start padding for the recording. If rec doesn`t have padding, will return 0 (in case of partial rec).
     *
     * @return Calculated padding.
     */
    default long getStartPadding() {
        return getRecordingStart() < getStart() ? (getStart() - getRecordingStart()) : 0;
    }

    /**
     * Calculates the end padding for the recording. If rec doesn`t have padding, will return 0 (in case of partial rec).
     *
     * @return Calculated padding.
     */
    default long getEndPadding() {
        return getEnd() < getRecordingEnd() ? (getRecordingEnd() - getEnd()) : 0;
    }
}
