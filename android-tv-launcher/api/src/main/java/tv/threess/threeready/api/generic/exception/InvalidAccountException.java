package tv.threess.threeready.api.generic.exception;

/**
 * Exception for device registration error when the introduced line number is not correct.
 *
 * @author Barabas Attila
 * @since 2020.04.14
 */
public class InvalidAccountException extends BackendException {

    public InvalidAccountException() {
        super("", "");
    }

    public InvalidAccountException(BackendException cause) {
        super(cause);
    }
}
