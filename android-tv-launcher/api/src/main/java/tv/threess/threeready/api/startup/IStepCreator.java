/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.startup;

import java.util.Queue;

import tv.threess.lib.di.Component;

/**
 * Step creator interface to provide project specific FTI, startup, standby and offline flow..
 *
 * @author Barabas Attila
 * @since 2018.06.27
 */
public interface IStepCreator extends Component {

    /**
     * @return Steps which needs the be executed whe the application starts.
     */
    Queue<Step> getStartupSteps();

    /**
     * @return Steps which needs to be executed when the application starts at the first time.
     */
    Queue<Step> getFTISteps();

    /**
     * @return Steps which needs to be executed when the application is coming out from standby.
     */
    Queue<Step> getStandbySteps();

    /**
     * @return Steps which needs to be executed
     * when the application starts but there is no backend connection.
     */
    Queue<Step> getOfflineSteps();
}
