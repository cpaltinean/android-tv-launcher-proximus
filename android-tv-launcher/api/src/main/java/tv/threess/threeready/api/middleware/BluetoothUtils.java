package tv.threess.threeready.api.middleware;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.ui.generic.utils.DeviceCharacteristicReader;

public class BluetoothUtils {
    private static final UUID GATT_BATTERY_SERVICE_UUID = UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb");
    private static final UUID GATT_BATTERY_LEVEL_CHARACTERISTIC_UUID = UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb");

    private static final UUID GATT_FIRMWARE_VERSION_CHARACTERISTIC_UUID = UUID.fromString("00002a26-0000-1000-8000-00805f9b34fb");
    private static final UUID GATT_SOFTWARE_VERSION_CHARACTERISTIC_UUID = UUID.fromString("00002a28-0000-1000-8000-00805f9b34fb");
    private static final UUID GATT_DEV_INFORMATION_SERVICE_UUID = UUID.fromString("0000180a-0000-1000-8000-00805f9b34fb");

    private static <T> T readSync(Context context, BluetoothDevice device, DeviceCharacteristicReader<T> reader) throws InterruptedException, ExecutionException, TimeoutException {
        BluetoothGatt gatt = null;
        try {
            gatt = device.connectGatt(context, false, reader);
            return reader.get();
        } finally {
            if (gatt != null) {
                gatt.disconnect();
                gatt.close();
            }
        }
    }

    /**
     * Reads the firmware version of the bluetooth device, could be considered as the Realteck “middleware”.
     * The call is synchronous, so, it will wait and return the value.
     * Do not call this method from the main-thread, use {@code BluetoothDevice#connectGatt} method to read it asynchronously.
     *
     * @param context android context
     * @param device the bluetooth device
     * @return the device firmware version
     */
    public static String readFirmwareVersion(Context context, BluetoothDevice device) throws InterruptedException, ExecutionException, TimeoutException {
        return readSync(context, device, new DeviceCharacteristicReader<String>() {

            @Override
            protected UUID getCharacteristicToRead() {
                return GATT_FIRMWARE_VERSION_CHARACTERISTIC_UUID;
            }

            @Override
            protected UUID getServiceToRead() {
                return GATT_DEV_INFORMATION_SERVICE_UUID;
            }

            @Override
            protected String onResult(BluetoothGattCharacteristic characteristic) {
                return characteristic.getStringValue(0).trim()
                        .replace("\u0000", "");
            }
        });
    }

    /**
     * Reads the software version of the bluetooth device.
     * The call is synchronous, so, it will wait and return the value.
     * Do not call this method from the main-thread, use {@code BluetoothDevice#connectGatt} method to read it asynchronously.
     *
     * @param context android context
     * @param device the bluetooth device
     * @return the software version
     */
    public static String readSoftwareVersion(Context context, BluetoothDevice device) throws InterruptedException, ExecutionException, TimeoutException {
        return readSync(context, device, new DeviceCharacteristicReader<String>() {

            @Override
            protected UUID getCharacteristicToRead() {
                return GATT_SOFTWARE_VERSION_CHARACTERISTIC_UUID;
            }

            @Override
            protected UUID getServiceToRead() {
                return GATT_DEV_INFORMATION_SERVICE_UUID;
            }

            @Override
            protected String onResult(BluetoothGattCharacteristic characteristic) {
                return characteristic.getStringValue(0).trim()
                        .replace("\u0000", "");
            }
        });
    }

    /**
     * Reads the battery level of the bluetooth device.
     * The call is synchronous, so, it will wait and return the value.
     * Do not call this method from the main-thread, use {@code BluetoothDevice#connectGatt} method to read it asynchronously.
     *
     * @param context android context
     * @param device the bluetooth device
     * @return the battery level
     */
    public static int readBatteryLevel(Context context, BluetoothDevice device) throws InterruptedException, ExecutionException, TimeoutException {
        return readSync(context, device, new DeviceCharacteristicReader<Integer>() {

            @Override
            protected UUID getCharacteristicToRead() {
                return GATT_BATTERY_LEVEL_CHARACTERISTIC_UUID;
            }

            @Override
            protected UUID getServiceToRead() {
                return GATT_BATTERY_SERVICE_UUID;
            }

            @Override
            protected Integer onResult(BluetoothGattCharacteristic characteristic) {
                return (int) characteristic.getValue()[0];
            }
        });
    }

    /**
     * Returns the first connected device from the given list.
     * @param context Android context.
     * @param names List of device names.
     * @return the first device which matches the name from the given list and is connected.
     */
    public static BluetoothDevice getConnectedDevice(Context context, Collection<String> names) {
        return getDevice(context, true, names);
    }

    /**
     * Returns all the connected devices.
     * @param context Android context.
     * @return list of devices which are connected.
     */
    public static Set<BluetoothDevice> getConnectedDevices(Context context) {
        return ArrayUtils.notNull(getDevices(context, true));
    }

    /**
     * Returns all the paired devices.
     * @param context Android context.
     * @return list of devices which are paired.
     */
    public static Set<BluetoothDevice> getPairedDevices(Context context) {
        return ArrayUtils.notNull(getDevices(context, false));
    }

    private static Set<BluetoothDevice> getDevices(Context context, boolean connected) {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter == null || !adapter.isEnabled()) {
            return null;
        }

        // get paired devices and wrap into a modifiable set to allow method `retainAll`
        Set<BluetoothDevice> boundedDevices = adapter.getBondedDevices();
        if(boundedDevices == null) {
            return null;
        }

        Set<BluetoothDevice> devices = new HashSet<>(boundedDevices);
        if (connected) {
            BluetoothManager manager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
            devices.retainAll(ArrayUtils.notNull(manager.getConnectedDevices(BluetoothProfile.GATT)));
        }
        return devices;
    }

    private static BluetoothDevice getDevice(Context context, boolean connected, Collection<String> names) {
        Set<BluetoothDevice> devices = getDevices(context, connected);
        if (devices == null) {
            return null;
        }
        for (BluetoothDevice device : devices) {
            if (names.contains(device.getName())) {
                return device;
            }
        }
        return null;
    }

    public static boolean isRemoteConnected(Context context){
        Collection<String> remoteNames = Components.get(AppConfig.class).getRemoteNames();
        BluetoothDevice remote = BluetoothUtils.getConnectedDevice(context, remoteNames);
        return remote != null;
    }
}
