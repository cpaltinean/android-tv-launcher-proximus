package tv.threess.threeready.api.search.model;

import java.io.Serializable;

/**
 * Term to trigger search queries.
 *
 * @author Barabas Attila
 * @since 2019.03.29
 */
public class SearchTerm implements Serializable {

    private final String mTerm;

    public SearchTerm(String term) {
        mTerm = term;
    }

    public String getTerm() {
        return mTerm;
    }

    public boolean isSuggestion() {
        return false;
    }
}
