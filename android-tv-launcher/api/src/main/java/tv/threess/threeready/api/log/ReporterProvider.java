package tv.threess.threeready.api.log;

import tv.threess.lib.di.Component;

/**
 * Interface which provides the event/metric reporters for the application.
 *
 * @author Zsolt Bokor_
 * @since 2020.05.29
 */
public interface ReporterProvider extends Component {
    Class<? extends Reporter>[] getReporters();
}
