package tv.threess.threeready.api.home.model.generic;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Component;

/**
 * Offline mode related config options.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public interface OfflineModeSettings extends Component, Serializable {

    /**
     * @return Timeout to redisplay the offline mode notification when it becomes hidden but the issue still persist.
     */
    int getNotificationRedisplayPeriod();

    /**
     * @param unit The time unit to return the results in.
     * @return Minimum delay of testing backend availability
     */
    long getBackendCheckPeriodMin(TimeUnit unit);

    /**
     * @param unit The time unit to return the results in.
     * @return Maximum delay of testing backend availability.
     */
    long getBackendCheckPeriodMax(TimeUnit unit);

    /**
     * @param unit The time unit to return the results in.
     * @return The internet check frequency when the application is offline mode.
     * Should be greater then the ping timeout.
     */
    long getInternetCheckPeriod(TimeUnit unit);
}
