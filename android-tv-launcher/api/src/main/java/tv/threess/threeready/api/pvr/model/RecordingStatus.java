/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.pvr.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;


/**
 * Class which holds the recording
 * status for a single and series recording for a broadcast.
 *
 * @author Barabas Attila
 * @since 2018.10.23
 */
public class RecordingStatus implements Serializable {

    private final SingleRecordingStatus mSingleRecordingStatus;
    private final SeriesRecordingStatus mSeriesRecordingStatus;

    private final IRecording mRecording;

    private RecordingStatus() {
        mSingleRecordingStatus = SingleRecordingStatus.NONE;
        mSeriesRecordingStatus = SeriesRecordingStatus.NONE;
        mRecording = null;
    }

    public RecordingStatus(IRecording recording,
                           SingleRecordingStatus singleRecordingStatus,
                           SeriesRecordingStatus seriesRecordingStatus) {
        mRecording = recording;
        mSingleRecordingStatus = singleRecordingStatus == null
                ? SingleRecordingStatus.NONE : singleRecordingStatus;
        mSeriesRecordingStatus = seriesRecordingStatus == null
                ? SeriesRecordingStatus.NONE : seriesRecordingStatus;
    }

    /**
     * @return recording status of the asset.
     */
    @NonNull
    public SingleRecordingStatus getSingleRecordingStatus() {
        return mSingleRecordingStatus;
    }

    /**
     * @return recording status of the series.
     */
    @NonNull
    public SeriesRecordingStatus getSeriesRecordingStatus() {
        return mSeriesRecordingStatus;
    }

    /**
     * @return The recording a broadcast.
     * Returns null if the broadcast is not a recording.
     */
    @Nullable
    public IRecording getRecording() {
        return mRecording;
    }

    @Override
    public String toString() {
        return "RecordingStatus{" +
                "mSingleRecordingStatus=" + mSingleRecordingStatus +
                ", mSeriesRecordingStatus=" + mSeriesRecordingStatus +
                '}';
    }

    public static final RecordingStatus DEFAULT = new RecordingStatus();
}
