package tv.threess.threeready.api.home.model.module;

import androidx.annotation.Nullable;

import java.io.Serializable;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.home.model.generic.GradientStyle;

/**
 * Defines the appearance, size, color, images of an editorial tile.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public interface EditorialItemStyle extends Serializable {

    /**
     * @return The font size of the title needs to be displayed on the tile.
     */
    @Nullable
    default Float getTitleFontSize() {
        return null;
    }

    /**
     * @return The width of the editorial tile in dp.
     */
    @Nullable
    default Size getWidth() {
        return null;
    }

    /**
     * @return The height of the editorial tile in dp.
     */
    @Nullable
    default Size getHeight() {
        return null;
    }

    /**
     * @return The spacing between the editorial tiles in pixels.
     */
    @Nullable
    default Float getPadding() {
        return null;
    }

    /**
     * @return The text color of the title.
     */
    @Nullable
    default Integer getTitleColor() {
        return null;
    }

    /**
     * @return The background gradient color.
     */
    @Nullable
    default GradientStyle getGradient() {
        return null;
    }

    /**
     * @return Image resource which needs to be displayed as background.
     */
    @Nullable
    default IImageSource getBackgroundImage() {
        return null;
    }

    /**
     * @return Image resource which needs to be displayed as icon.
     */
    @Nullable
    default IImageSource getIcon() {
        return null;
    }

    /**
     * @return The scale factor of the tile when its focused.
     */
    @Nullable
    default Size getFocusScaleSize() {
        return null;
    }


    /**
     * Interface represents the different methods
     * to calculate the size of an editorial card.
     */
    interface Size extends Serializable {
    }

    /**
     * A defined fixed size for the editorial card.
     */
    class FixedSize implements Size {
        public final float size;

        public FixedSize(Float size) {
            this.size = size == null ? 0 : size;
        }
    }

    /**
     * A variant size of the editorial card.
     * The UI needs to decide the size based on the variant.
     */
    class VariantSize implements Size {
        public final Variant variant;

        public VariantSize(Variant variant) {
            this.variant = variant;
        }

        public enum Variant {
            Small,
            Large,
            ViewAll,
            App,
            SmallBanner,
            WideBanner
        }
    }

    /**
     * The size of the editorial needs to adapt to the other content displayed.
     */
    class AdaptiveSize implements Size {
    }


    VariantSize DEFAULT_SIZE = new VariantSize(VariantSize.Variant.Small);
}
