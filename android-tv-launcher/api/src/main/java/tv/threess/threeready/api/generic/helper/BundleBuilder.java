package tv.threess.threeready.api.generic.helper;

import android.os.Bundle;
import android.os.Parcelable;

import java.io.Serializable;

public class BundleBuilder {
    private final Bundle bundle = new Bundle();

    public BundleBuilder putAll(Bundle bundle) {
        this.bundle.putAll(bundle);
        return this;
    }

    public BundleBuilder put(String key, boolean value) {
        bundle.putBoolean(key, value);
        return this;
    }

    public BundleBuilder put(String key, byte value) {
        bundle.putByte(key, value);
        return this;
    }

    public BundleBuilder put(String key, char value) {
        bundle.putChar(key, value);
        return this;
    }

    public BundleBuilder put(String key, short value) {
        bundle.putShort(key, value);
        return this;
    }

    public BundleBuilder put(String key, int value) {
        bundle.putInt(key, value);
        return this;
    }

    public BundleBuilder put(String key, long value) {
        bundle.putLong(key, value);
        return this;
    }

    public BundleBuilder put(String key, float value) {
        bundle.putFloat(key, value);
        return this;
    }

    public BundleBuilder put(String key, double value) {
        bundle.putDouble(key, value);
        return this;
    }

    public BundleBuilder put(String key, String value) {
        bundle.putString(key, value);
        return this;
    }

    public BundleBuilder put(String key, Parcelable value) {
        bundle.putParcelable(key, value);
        return this;
    }

    public BundleBuilder put(String key, Serializable value) {
        bundle.putSerializable(key, value);
        return this;
    }

    public BundleBuilder put(String key, long[] value) {
        bundle.putSerializable(key, value);
        return this;
    }

    public Bundle build() {
        return bundle;
    }
}
