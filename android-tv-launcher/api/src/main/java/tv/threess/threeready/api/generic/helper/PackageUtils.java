/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.generic.helper;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import tv.threess.threeready.api.log.Log;

/**
 * Known package names and application utility methods.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.07.27
 */
public class PackageUtils {
    private static final String TAG = Log.tag(PackageUtils.class);

    private static final Uri PLAY_STORE_URI = Uri.parse("http://play.google.com/store/apps/details");

    private static final String PLAY_STORE_PARAM_ID = "id";

    public static final String SCHEMA_PACKAGE = "package";

    // Netflix
    public static final String NETFLIX_BROWSE_URL = "http://www.netflix.com/browse";
    public static final String NETFLIX_SOURCE_TYPE_PARAM = "source_type";
    public static final String NETFLIX_SOURCE_TYPE_PLAY_LOAD_PARAM = "source_type_payload";

    public static final String NETFLIX_CHANNEL_NUMBER_PARAM = "chID";
    public static final String NETFLIX_PREV_CHANNEL_NUMBER_PARAM = "prevChID";
    public static final String NETFLIX_NEXT_CHANNEL_NUMBER_PARAM = "nextChID";
    public static final String NETFLIX_CHANNEL_CATEGORY_TYPE_PARAM = "category";
    public static final String NETFLIX_MINI_EPG_EXTRA = "mini-epg";

    public static final String PACKAGE_NAME_NETFLIX = "com.netflix.ninja";
    public static final String PACKAGE_NAME_YOUTUBE = "com.google.android.youtube.tv";
    public static final String PACKAGE_NAME_TCH_IPTV = "com.technicolor.android.dtvinput";
    public static final String TALKBACK_SETTING_ACTIVITY_NAME = "com.android.talkback.TalkBackPreferencesActivity";

    private static boolean openPlayStore(Context context, String packageName) {
        Uri.Builder startUri = PLAY_STORE_URI.buildUpon();
        if (packageName != null && !packageName.isEmpty()) {
            startUri.appendQueryParameter(PLAY_STORE_PARAM_ID, packageName);
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, startUri.build());
        return openApp(context, "", intent);
    }

    public static boolean openApplication(Context context, String packageName, Intent intent) {
        if (!TextUtils.isEmpty(packageName) && context.getPackageManager().getLeanbackLaunchIntentForPackage(packageName) == null) { //app is disabled ?
            return openPlayStore(context, packageName);
        } else {
            return openApp(context, packageName, intent);
        }
    }

    private static boolean openApp(Context context, String packageName, Intent intent) {
        if (PACKAGE_NAME_NETFLIX.equals(packageName)) {
            intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        } else {
            // if application is already started, bring it to front.
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Send explicit intent.
        if (!TextUtils.isEmpty(packageName)) {
            intent.setPackage(packageName);
        }

        Log.d(TAG, "starting application", intent);
        context.startActivity(intent);
        return true;
    }

    public static boolean openApplication(Context context, String packageName, Bundle bundle) {
        PackageManager pm = context.getPackageManager();
        Intent intent = pm.getLeanbackLaunchIntentForPackage(packageName);
        if (intent == null) {
            Log.w(TAG, "Failed to start application, not installed package: " + packageName);
            return openPlayStore(context, packageName);
        }

        if (bundle != null) {
            // copy all extras from the original intent
            intent.putExtras(bundle);
        }
        return openApp(context, packageName, intent);
    }

    public static boolean openApplication(Context context, String packageName, String activityName, Bundle bundle) {
        PackageManager pm = context.getPackageManager();
        Intent intent = new Intent();
        intent.setClassName(packageName, activityName);
        if (bundle != null) {
            // copy all extras from the original intent
            intent.putExtras(bundle);
        }
        if (intent.resolveActivityInfo(pm, 0) == null) {
            Log.w(TAG, "Failed to start application, not installed package: " + packageName + " with activity: " + activityName);
            return openPlayStore(context, packageName);
        } else {
            return openApp(context, packageName, intent);
        }
    }

    /**
     * @return application name by package name.
     */
    public static String getAppNameByPackageName(Context context, String packageName) {
        try {
            PackageManager pm = context.getPackageManager();
            return String.valueOf(pm.getApplicationLabel(
                    pm.getApplicationInfo(packageName, PackageManager.GET_META_DATA)));
        } catch (Exception e) {
            Log.e(TAG, "Could not get app name for : " + packageName);
        }
        return "";
    }
}
