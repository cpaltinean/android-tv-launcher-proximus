package tv.threess.threeready.api.generic;

import io.reactivex.ObservableOnSubscribe;

/**
 * Interface to enable the reuse of the observable for fetching epg data with different time ranges.
 */
public interface EpgFetchObservable<T> extends ObservableOnSubscribe<T> {

    /**
     * Called when the visible timeline changes in the EPG grid and broadcasts needs to be loaded.
     * The timeline includes the extra prefetch time for the broadcasts.
     *
     * @param from The timestamp from which the broadcast needs to be loaded.
     * @param to   The timestamp until the broadcasts needs to be loaded.
     * @return false if the operation can not be started for some reason.
     */
    boolean prefetchPrograms(String channelId, long from, long to);
}
