package tv.threess.threeready.api.vod.model;

import java.io.Serializable;

/**
 * Base interface for rented vod information(expiration date).
 *
 * Created by Noemi Dali on 24.04.2020.
 */
public interface IRentalExpiration extends Serializable {

    /**
     * @return The rented item expiration time.
     */
    long getExpireTime();

    /**
     * @return The rental time of the rented item.
     */
    long getRentalTime();

    /**
     * @return The rented item movie reference id.
     */
    String getVariantId();
}
