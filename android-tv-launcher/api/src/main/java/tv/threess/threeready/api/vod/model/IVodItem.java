package tv.threess.threeready.api.vod.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Basic interface for vod related content.
 *
 * @author Andor Lukacs
 * @since 10/2/18
 */
public interface IVodItem extends IBaseVodItem {

    /**
     * @return A list of all available variants.
     */
    List<? extends IVodVariant> getVariantList();

    /**
     * @return the rented vod variants as a list.
     */
    default List<IVodVariant> getRentedVariants() {
        List<IVodVariant> variants = new ArrayList<>();
        for (IVodVariant vodVariant : getVariantList()) {
            if (vodVariant.isRented()) {
                variants.add(vodVariant);
            }
        }
        return variants;
    }

    /**
     * @return the subscribed vod variants as a list.
     */
    default List<IVodVariant> getSubscribedVariants() {
        List<IVodVariant> variants = new ArrayList<>();
        for (IVodVariant vodVariant : getVariantList()) {
            if (vodVariant.isSubscribed()) {
                variants.add(vodVariant);
            }
        }
        return variants;
    }

    /**
     * @return A list of variants which can be played without any transaction.
     */
    default List<IVodVariant> getPlayableVariants() {
        List<IVodVariant> variants = new ArrayList<>();
        for (IVodVariant vodVariant : getVariantList()) {
            if (vodVariant.isRented()
                    || vodVariant.isSubscribed()
                    || vodVariant.isFree()) {
                variants.add(vodVariant);
            }
        }
        return variants;
    }

    /**
     * @return True if the vod can be rented, otherwise false.
     */
    default boolean isRentable(int credit, Map<String, IRentOffer> variantOfferMap) {
        final IVodPrice minimPriceForCredit = IVodPrice.getMinimumPrice(variantOfferMap, CurrencyType.Credit);
        final IVodPrice minimPriceForEuro = IVodPrice.getMinimumPrice(variantOfferMap, CurrencyType.Euro);
        return (credit > 0 && minimPriceForCredit != null && minimPriceForCredit.getCurrencyAmount() <= credit)
                || (minimPriceForEuro != null && minimPriceForEuro.getCurrencyAmount() > 0);
    }

    /**
     * @return The variant with the specified variantId.
     */
    default IVodVariant getVariantById(String variantId) {
        for (IVodVariant variant : this.getVariantList()) {
            if (variant.getId().equals(variantId)) {
                return variant;
            }
        }

        return null;
    }
}
