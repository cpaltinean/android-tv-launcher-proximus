package tv.threess.threeready.api.home.model.error;

import java.io.Serializable;

import tv.threess.threeready.api.log.helper.ErrorType;

/**
 * Configuration options to display a generic error message in app.
 *
 * @author Barabas Attila
 * @since 5/16/21
 */
public interface ErrorConfig extends Serializable {
    /**
     * The priority of the error.
     * The app will hide the lower priority errors in order the displayed the higher ones.
     */
    String getType();

    /**
     * The type of the ui component where the error should be displayed.
     */
    DisplayMode getDisplayMode();

    /**
     * Type of the error message.
     * Can be a specific error type {@link ErrorType} or domain {@link ErrorType.Domain}.
     */
    int getPriority();

    /**
     * Display mode of the generic error message.
     * The error message can be shown on the video surface, notification or dialog.
     */
    enum DisplayMode {
        ALERT,
        NOTIFICATION,
        PLAYBACK_ERROR
    }
}
