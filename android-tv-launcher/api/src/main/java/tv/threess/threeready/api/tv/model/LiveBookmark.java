package tv.threess.threeready.api.tv.model;

import tv.threess.threeready.api.generic.model.BookmarkType;
import tv.threess.threeready.api.generic.model.IBookmark;

/**
 * Object that holds information of a bookmark for a live broadcast
 */
public class LiveBookmark implements IBookmark {

    private final String mContentId;
    private final long mStartTime;
    private final long mEndTime;

    public LiveBookmark(IBroadcast broadcast) {
        mContentId = broadcast.getId();
        mStartTime = broadcast.getStart();
        mEndTime = broadcast.getEnd();
    }

    @Override
    public String getContentId() {
        return mContentId;
    }

    @Override
    public long getDuration() {
        return mEndTime - mStartTime;
    }

    @Override
    public long getPosition() {
        return System.currentTimeMillis() - mStartTime;
    }

    @Override
    public BookmarkType getType() {
        return BookmarkType.Live;
    }

    @Override
    public long getTimestamp() {
        return 0;
    }

    @Override
    public boolean isNearEnd(double maxWatchPercent) {
        return false;
    }
}
