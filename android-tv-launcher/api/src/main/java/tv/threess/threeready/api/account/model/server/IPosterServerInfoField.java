package tv.threess.threeready.api.account.model.server;

import java.io.Serializable;

/**
 * CLass which holds the poster server allowed images information.
 *
 * Created by Noemi Dali on 02.04.2020.
 */
public interface IPosterServerInfoField extends Serializable {
    /**
     * @return The allowed image width.
     */
    Float getWidth();

    /**
     * @return The allowed image height.
     */
    Float getHeight();

    /**
     * @return The allowed image uri.
     */
    String getUri();

    /**
     * @return True if server allows resizable images.
     */
    boolean isResizable();
}
