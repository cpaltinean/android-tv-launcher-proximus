/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.generic.receivers;

import android.content.Context;

import androidx.annotation.WorkerThread;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.FeatureControl;

/**
 * Class for checking the internet in the application.
 * This is to be used in any location where we need internet status.
 *
 * @author Barabas Attila
 * @since 2017.08.19
 */
public abstract class InternetChecker extends ConnectivityStateChangeReceiver implements Component {
    protected final Context mContext;

    private final List<WeakReference<InternetCheckEnabler>> mInternetCheckEnablers = new CopyOnWriteArrayList<>();

    public InternetChecker(Context context) {
        super();
        mContext = context;
    }

    /**
     * @return The timestamp when the internet availability was last verified.
     */
    public abstract long getLastCheckedTime();

    /**
     * Check the internet connectivity and return the status.
     * @return True when device is connected to the internet.
     */
    @WorkerThread
    public abstract boolean checkInternetAvailability();

    /**
     * triggers the internet checker process to detect if internet and backend is available or not.
     */
    public abstract void triggerInternetCheck();

    /**
     * Returns true when cable is connected to the device.
     */
    public abstract boolean isCableConnected();

    /**
     * Returns true when device is connected to the internet.
     */
    public abstract boolean isInternetAvailable();

    /**
     * Returns true if the backend is available and reachable.
     */
    public abstract boolean isBackendAvailable();

    /**
     * Returns true if we have internet, but the backend is not available.
     */
    public boolean isBackendIsolated() {
        return !isBackendAvailable() && !isOffline();
    }

    /**
     * Returns true if the backend and internet not available.
     */
    public boolean isOffline() {
        if (!isCableConnected()) {
            return true;
        }
        if (Components.get(FeatureControl.class).isOffline()) {
            return true;
        }
        return !isInternetAvailable() && !isBackendAvailable();
    }

    /**
     * Called when a request successfully connected to backend.
     */
    public abstract void onRequestSuccessful();

    /**
     * Called when a request failed to connect to the backend.
     */
    public abstract void onRequestFailed(Exception error);


    /**
     * Add an internet check enabler to conditionally enable/disable the internet check.
     * @param enabler The enabler which returns true when the internet check is enabled.
     */
    public void addEnabler(InternetCheckEnabler enabler) {
        removeEnabler(enabler);
        mInternetCheckEnablers.add(new WeakReference<>(enabler));
    }

    /**
     * Remove a previously added enabler.
     * @param enablerToRemove The enabler to remove.
     */
    public void removeEnabler(InternetCheckEnabler enablerToRemove) {
        mInternetCheckEnablers.removeIf((item) -> {
            InternetCheckEnabler enabler = item.get();
            return enabler == null || enabler == enablerToRemove;
        });
    }

    /**
     * @return True if at least one enabler allows the internet check.
     */
    public boolean isInternetCheckEnabled() {
        for (WeakReference<InternetCheckEnabler> enabler : mInternetCheckEnablers) {
            if (enabler != null && enabler.get().checkEnabled()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Interface to conditionally enable/disable the internet checker.
     */
    public interface InternetCheckEnabler {
        boolean checkEnabled();
    }
}
