/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.generic.helper;

import android.os.Handler;
import android.text.TextUtils;
import android.util.Range;

import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Utility class for helping with array manipulation.
 *
 * @author Dan Titiu
 * @since 05.10.2012
 */
public final class ArrayUtils {

    public static final int[] EMPTY_INT = new int[0];
    public static final long[] EMPTY_LONG = new long[0];
    public static final String[] EMPTY_STRING = new String[0];

    /**
     * @return True if the given range is null or empty.
     */
    public static<T extends Comparable<? super T>> boolean isEmpty(Range<T> range) {
        return range == null || Objects.equals(range.getLower(), range.getUpper());
    }

    public static<T> boolean isEmpty(Collection<T> values) {
        return values == null || values.isEmpty();
    }

    public static<T> boolean isEmpty(T[] values) {
        return values == null || values.length == 0;
    }

    public static<K, V> Map<K, V> notNull(Map<K, V> items) {
        if (items == null) {
            return Collections.emptyMap();
        }
        return items;
    }

    public static<T> Collection<T> notNull(Collection<T> items) {
        if (items == null) {
            return Collections.emptyList();
        }
        return items;
    }

    public static<T> Set<T> notNull(Set<T> items) {
        if (items == null) {
            return Collections.emptySet();
        }
        return items;
    }

    public static String[] notNull(String[] items) {
        if (items == null) {
            return EMPTY_STRING;
        }
        return items;
    }


    /**
     * Convert array of objects to an array of primitives (Long[] -&gt; long[]).
     *
     * @param src    The source array containing objects.
     * @throws NullPointerException in case there is at least one null value in the array, or the array itself is null.
     * @return Array of primitive counterparts.
     */
    public static long[] toPrimitive(Long[] src) {
        long[] dst = new long[src.length];
        for (int x = 0; x < src.length; x++) {
            //noinspection UnnecessaryUnboxing
            dst[x] = src[x].longValue();
        }
        return dst;
    }

    /**
     * Convert array of primitives to a list of Objects.
     *
     * @param src    The source array containing primitives.
     * @return List of Object counterparts.
     */
    public static List<Integer> toList(int... src) {
        ArrayList<Integer> list = new ArrayList<>(src.length);
        for (int i : src) {
            list.add(i);
        }
        return list;
    }

    public static String[] toArray(Collection<String> source) {
        final int s = source.size();
        if (s == 0) {
            return EMPTY_STRING;
        }
        String[] arr = new String[s];
        int x = 0;
        for (String e : source) {
            arr[x++] = e;
        }

        return arr;
    }


    public static int[] parseInt(String[] src) {
        if (src == null || src.length == 0) {
            return EMPTY_INT;
        }

        final int[] res = new int[src.length];
        for (int x = 0; x < src.length; ++x) {
            res[x] = Integer.parseInt(src[x]);
        }

        return res;
    }

    /**
     * Check if two Collections contain the same elements, independent of order
     * @param a the first collection
     * @param b the second collection
     * @return true if a contains the same elements as b
     */
    public static <T> boolean isEqualCollection(Collection<T> a, Collection<T> b) {
        return a.size() == b.size() && a.containsAll(b);
    }

    public static <T> boolean isEqualCollection(T[] a, T[] b) {
        if (a.length != b.length) {
            return false;
        }
        return isEqualCollection(Arrays.asList(a), Arrays.asList(b));
    }

    public static String[] split(String values, String regex) {
        if (TextUtils.isEmpty(values)) {
            return EMPTY_STRING;
        }
        return values.split(regex);
    }

    public static<T> String toString(String separator, Collection<T> values) {
        if (values == null) {
            return String.valueOf((Object) null);
        }
        StringBuilder result = new StringBuilder("[");
        for (T value : values) {
            if (result.length() > 1) {
                result.append(separator);
            }
            result.append(value);
        }
        result.append("]");
        return result.toString();
    }

    public static<T> String toString(String separator, T[] values) {
        if (values == null) {
            return String.valueOf((Object) null);
        }
        StringBuilder result = new StringBuilder("[");
        for (T value : values) {
            if (result.length() > 1) {
                result.append(separator);
            }
            result.append(value);
        }
        result.append("]");
        return result.toString();
    }

    public static<T> String toString(T[] values) {
        return toString(", ", values);
    }

    public static<T> String toString(Collection<T> values) {
        return toString(", ", values);
    }

    public static <T> T first(List<T> values) {
        if (values == null || values.isEmpty()) {
            return null;
        }
        return values.get(0);
    }


    /**
     * Notify all the available listeners weak listeners
     * and remove the ones which are not available anymore.
     * @param listeners A list of weak listeners to notify.
     * @param handler Optional thread handler to notify the listner on.
     * @param notifier Interface to notify the listener.
     * @param <TListener> The type of the listener to notify.
     */
    public static <TListener> void notifyAndCleanup(List<WeakReference<TListener>> listeners,
                                                    @Nullable Handler handler,
                                                    Notifier<TListener> notifier) {
        boolean cleanupNeeded = false;
        for (WeakReference<TListener> listenerRef : listeners) {
            TListener listener = listenerRef.get();
            if (listener == null) {
                cleanupNeeded = true;
                continue;
            }

            if (handler == null) {
                notifier.notify(listener);
            } else {
                handler.post(() -> notifier.notify(listener));
            }
        }

        if (cleanupNeeded) {
            listeners.removeIf((item) -> item.get() == null);
        }
    }

    public static <TListener> void notifyAndCleanup(List<WeakReference<TListener>> listeners,
                                                    Notifier<TListener> notifier) {
        notifyAndCleanup(listeners, null, notifier);
    }

    /**
     * Interface to notify a listener.
     * @param <TListener> The type of the interface to notify.
     */
    public interface Notifier<TListener> {
        void notify(TListener listener);
    }
}
