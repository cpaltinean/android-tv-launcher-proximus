/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.gms.model;

import java.io.Serializable;

/**
 * Entity class which stores the basic information from an installed application.
 *
 * @author Barabas Attila
 * @since 2017.05.17
 */
public class InstalledAppInfo implements Serializable {

    private final String mPackageName;
    private final String mName;
    private final int mFeaturedIdx;
    private final int mStoreIdx;
    private final long mLastOpened;
    private final String mActivityName;

    public InstalledAppInfo(String packageName, String name,
                            long lastOpened, int featuredIdx, int storeIdx, String activityName) {
        mPackageName = packageName;
        mName = name;
        mLastOpened = lastOpened;
        mFeaturedIdx = featuredIdx;
        mStoreIdx = storeIdx;
        mActivityName = activityName;
    }

    /**
     * @return The package id of the application.
     */
    public String getPackageName() {
        return mPackageName;
    }

    /**
     * @return The name of the application.
     */
    public String getName() {
        return mName;
    }

    /**
     * @return The name of the activity which needs to be opened.
     */
    public String getActivityName() {
        return mActivityName;
    }

    /**
     * @return True if the application is featured app.
     */
    public boolean isFeatured() {
        return mFeaturedIdx >= 0;
    }

    /**
     * @return The index of the application in the featured app list
     */
    public int getFeaturedIndex() {
        return mFeaturedIdx;
    }

    /**
     * @return The timestamp when the application was last opened.
     */
    public long getLastOpened() {
        return mLastOpened;
    }

    /**
     * @return If the application is a store.
     */
    public boolean isStore(){
        return mStoreIdx >= 0;
    }

    /**
     * @return Index of the application in the store apps.
     */
    public int getStoreIndex() {
        return mStoreIdx;
    }
}
