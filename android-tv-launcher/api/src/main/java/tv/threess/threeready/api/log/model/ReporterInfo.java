package tv.threess.threeready.api.log.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.log.Reporter;

/**
 * Class which holds the reporter info.
 *
 * @author Zsolt Bokor_
 * @since 2020.05.28
 */
public class ReporterInfo implements Parcelable {
    private static final int BATCH_SIZE = 1;
    private static final int MAX_BATCH_SIZE = 200;

    private static final long BATCH_PERIOD = TimeUnit.MINUTES.toMillis(5);
    private static final long TIMEOUT_PERIOD = TimeUnit.SECONDS.toMillis(4);

    private static final int MAX_KEEP_SIZE = 500;
    private static final long MAX_KEEP_PERIOD = TimeUnit.MINUTES.toMillis(2880);

    public static final long EVENT_RANDOM_PERIOD = TimeUnit.SECONDS.toMillis(10);
    public static final String EVENT_HEADER = "{\"content-type\":\"application/json\",\"data-usage-type\":\"event\"}";

    public static final long METRICS_RANDOM_PERIOD = TimeUnit.MINUTES.toMillis(4);
    public static final String METRICS_HEADER = "{\"content-type\":\"application/json\",\"data-usage-type\":\"metric\"}";

    protected String mReporter = "";
    protected String mEndpoint = "";
    protected String mHeaders = "";
    protected long mMaxKeepPeriod = MAX_KEEP_PERIOD;
    protected int mMaxKeepSize = MAX_KEEP_SIZE;
    protected int mMaxBatchSize = MAX_BATCH_SIZE;
    protected int mBatchSize = BATCH_SIZE;
    protected long mBatchPeriod = BATCH_PERIOD;
    protected long mRandomPeriod = 0;
    protected long mTimeoutPeriod;
    protected long mSendingDelay;

    private ReporterInfo() {}

    protected ReporterInfo(Parcel in) {
        mReporter = in.readString();
        mEndpoint = in.readString();
        mHeaders = in.readString();
        mMaxKeepPeriod = in.readLong();
        mMaxKeepSize = in.readInt();
        mMaxBatchSize = in.readInt();
        mBatchSize = in.readInt();
        mBatchPeriod = in.readLong();
        mRandomPeriod = in.readLong();
        mTimeoutPeriod = in.readLong();
        mSendingDelay = in.readLong();
    }

    public static final Creator<ReporterInfo> CREATOR = new Creator<ReporterInfo>() {
        @Override
        public ReporterInfo createFromParcel(Parcel in) {
            return new ReporterInfo(in);
        }

        @Override
        public ReporterInfo[] newArray(int size) {
            return new ReporterInfo[size];
        }
    };

    public String getReporter() {
        return mReporter;
    }

    public String getEndpoint() {
        return mEndpoint;
    }

    public String getHeaders() {
        return mHeaders;
    }

    public long getMaxKeepPeriod() {
        return mMaxKeepPeriod;
    }

    public int getMaxKeepSize() {
        return mMaxKeepSize;
    }

    public int getMaxBatchSize() {
        return mMaxBatchSize;
    }

    public int getBatchSize() {
        return mBatchSize;
    }

    public long getBatchPeriod() {
        return mBatchPeriod;
    }

    public long getRandomPeriod() {
        return mRandomPeriod;
    }

    public long getTimeoutPeriod() {
        return mTimeoutPeriod;
    }

    public long getSendingDelay() {
        return mSendingDelay;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mReporter);
        dest.writeString(mEndpoint);
        dest.writeString(mHeaders);
        dest.writeLong(mMaxKeepPeriod);
        dest.writeInt(mMaxKeepSize);
        dest.writeInt(mMaxBatchSize);
        dest.writeInt(mBatchSize);
        dest.writeLong(mBatchPeriod);
        dest.writeLong(mRandomPeriod);
        dest.writeLong(mTimeoutPeriod);
        dest.writeLong(mSendingDelay);
    }

    public static class Builder extends ReporterInfo {

        public Builder setReporter(Class<? extends Reporter> cls) {
            mReporter = cls.getCanonicalName();
            return this;
        }

        public Builder setEndpoint(String endpoint) {
            mEndpoint = endpoint;
            return this;
        }

        public Builder setHeaders(String headers) {
            mHeaders = headers;
            return this;
        }

        public Builder setMaxKeepPeriod(long maxKeepPeriod) {
            if (maxKeepPeriod <= 0) {
                maxKeepPeriod = MAX_KEEP_PERIOD;
            }
            mMaxKeepPeriod = maxKeepPeriod;
            return this;
        }

        public Builder setMaxKeepSize(int maxKeepSize) {
            if (maxKeepSize <= 0) {
                maxKeepSize = MAX_KEEP_SIZE;
            }
            mMaxKeepSize = maxKeepSize;
            return this;
        }

        public Builder setMaxBatchSize(int maxBatchSize) {
            if (maxBatchSize <= 0) {
                maxBatchSize = MAX_BATCH_SIZE;
            }
            mMaxBatchSize = maxBatchSize;
            return this;
        }

        public Builder setBatchSize(int batchSize) {
            mBatchSize = batchSize;
            return this;
        }

        public Builder setBatchPeriod(long batchPeriod) {
            mBatchPeriod = batchPeriod;
            return this;
        }

        public Builder setRandomPeriod(long randomPeriod) {
            mRandomPeriod = randomPeriod;
            return this;
        }

        public Builder setTimeoutPeriod(long timeoutPeriod) {
            if (timeoutPeriod <= 0) {
                timeoutPeriod = TIMEOUT_PERIOD;
            }
            mTimeoutPeriod = timeoutPeriod;
            return this;
        }

        public Builder setSendingDelay(long sendingDelay) {
            mSendingDelay = sendingDelay;
            return this;
        }

        public ReporterInfo build() {
            return this;
        }
    }

    public static final ReporterInfo EMPTY = new ReporterInfo();
}
