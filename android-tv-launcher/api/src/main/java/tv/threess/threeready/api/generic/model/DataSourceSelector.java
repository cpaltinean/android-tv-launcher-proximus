package tv.threess.threeready.api.generic.model;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Container holds the currently selected and all the available option for a data source.
 *
 * Created by Noemi Dali on 14.08.2020.
 */
public class DataSourceSelector implements Serializable {

    private final List<SelectorOption> mSelectorOptions;
    private DisplayType mDisplayType;
    private SelectorOption mSelectedOption;

    public DataSourceSelector(List<SelectorOption> selectorOptions) {
        mSelectorOptions = selectorOptions;
        mSelectedOption = getDefaultOption();
    }

    public DataSourceSelector(List<SelectorOption> selectorOptions, DisplayType displayType) {
        mSelectorOptions = selectorOptions;
        mSelectedOption = getDefaultOption();
        mDisplayType = displayType;
    }

    public DataSourceSelector(List<SelectorOption> sortOptions, SelectorOption selectedOption) {
        mSelectorOptions = sortOptions;
        mSelectedOption = selectedOption;
    }

    /**
     * @return All the possible selector options on the data source.
     */
    public List<SelectorOption> getOptions() {
        return mSelectorOptions == null ? Collections.emptyList() : mSelectorOptions;
    }

    /**
     * @return The currently selected option on the data source.
     */
    @Nullable
    public SelectorOption getSelectedOption() {
        if (mSelectedOption == null) {
            return getDefaultOption();
        }
        return mSelectedOption;
    }

    /**
     * @return The default selector option on the data source.
     */
    @Nullable
    public SelectorOption getDefaultOption() {
        if (mSelectorOptions == null || mSelectorOptions.isEmpty()) {
            return null;
        }
        for (SelectorOption option : mSelectorOptions) {
            if (option.isDefault()) {
                return option;
            }
        }

        return mSelectorOptions.get(0);
    }

    /**
     * @return True if the selector has more then one option available.
     */
    public boolean hasMultipleOption() {
        return mSelectorOptions != null && mSelectorOptions.size() > 1;
    }

    /**
     * @return How the selector needs to be displayed on the UI.
     */
    public DisplayType getDisplayType() {
       return mDisplayType == null ? DisplayType.DropDown : mDisplayType;
    }

    /**
     * @return The text or translation key for the name of the selector.
     */
    public String getName() {
        return TranslationKey.SORT_OPTIONS_TITLE;
    }

    /**
     * Select the option at the give position for the data source.
     * @param position The position of the option.
     */
    public void selectOption(int position) {
        mSelectedOption = mSelectorOptions.get(position);
    }

    /**
     * Clears the selected option.
     */
    public void clearSelectOption() {
        mSelectedOption = null;
    }

    /**
     * Enum which represents how the selector needs to be displayed on the UI.
     */
    public enum DisplayType {
       DropDown,
       Menu
    }
}
