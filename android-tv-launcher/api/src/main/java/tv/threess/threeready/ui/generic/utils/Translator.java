/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.utils;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.helper.LocaleUtils;

/**
 * Helper class for translation.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.05.02
 */
public class Translator implements Component {
    private final static Pattern PARAM_PATTERN = Pattern.compile("%([A-Za-z0-9_]*)%");

    private volatile Map<String, Map<String, String>> mTranslations;

    public void setTranslations(Map<String, Map<String, String>> translations) {
        mTranslations = translations;
    }

    /**
     * @param key    the POEditor key
     * @param paramValueMap map of key-value strings, key is the param key, and value is what should the key be replaced with
     * @return the string after the replacement.
     *
     * Ex: if the input string is "I have %apples_number% apples and they are %apple_taste%." we could
     * use the next params: ["apples_number",30,"apple_taste","sweet"] and the output will be
     * "I have 30 apples and they are sweet."
     *
     */
    public String get(String key, Map<String, String> paramValueMap) {
        String input = get(key);
        if (key == null || key.equals(input)) {
            return key;
        }
        return replaceParams(input, paramValueMap);
    }

    /**
     * @param input The text where we need to perform the replacement process.
     * @param map   map of key-value strings, key is the param key, and value is what should the key be replaced with
     * @return the string after the replacement.
     */
    public String replaceParams(String input, Map<String, String> map) {
        Matcher matcher = PARAM_PATTERN.matcher(input);
        StringBuffer result = new StringBuffer();
        while (matcher.find()) {
            String id = matcher.group(1);
            String value = map.get(id);

            if (value == null) {
                value = "";
            }

            matcher.appendReplacement(result, value);
        }
        matcher.appendTail(result);
        return result.toString();
    }

    public String get(String key) {
        return get(key, key, LocaleUtils.getApplicationLanguage());
    }

    public String getEnglishTranslation(String key) {
        return get(key, key, LocaleUtils.ENGLISH_LANGUAGE_CODE);
    }


    public String get(String key, String defaultValue, String language) {
        Map<String, Map<String, String>> translations = mTranslations;
        if (key != null && translations != null && translations.containsKey(language)) {
            if (translations.get(language).get(key.toLowerCase()) != null) {
                return translations.get(language).get(key.toLowerCase());
            }
        }

        return defaultValue;
    }
}
