package tv.threess.threeready.api.pvr.model;

import java.io.Serializable;

/**
 * Entity class representing a recording quota.
 *
 * @author Andrei Teslovan
 * @since 2018.11.26
 */
public interface IRecordingQuota extends Serializable {

    /**
     * @return the quota available.
     */
    long getQuota();

    /**
     * @return the quota that is being used.
     */
    long getUsedQuota();

    /**
     * @return return the currently used recording storage percentage.
     */
    default int getUsedRecordingSpacePercentage() {
        return getQuota() == 0 ? 0 : Math.round(getUsedQuota() * 100 / getQuota());
    }

    /**
     * @return the data when the backend will start to cleanup the recording storage.
     */
    long getCleanupDate();
}
