/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.home.model.local;

/**
 * The abstract representation the type of the font which will be displayed on UI.
 * The exact font style and family will come config from the given type.
 * @see FontConfig3Ready
 *
 * @author Barabas Attila
 * @since 2018.07.10
 */
public enum FontType {
    // Regular
    REGULAR(0),
    REGULAR_ITALIC(1),

    // Bold
    BOLD(2),
    EXTRA_BOLD(3),
    BOLD_ITALIC(4),

    // Light
    LIGHT(5),
    LIGHT_ITALIC(6);

    public final int attrValue;

    FontType(int attrValue) {
        this.attrValue = attrValue;
    }

    /**
     * Get the font type from view attribute.
     */
    public static FontType forAttributeValue(int attrValue) {
        for (FontType fw : values()) {
            if (fw.attrValue == attrValue) {
                return fw;
            }
        }

        throw new IllegalArgumentException("Unknown attribute value for FontWeight: " + attrValue);
    }
}
