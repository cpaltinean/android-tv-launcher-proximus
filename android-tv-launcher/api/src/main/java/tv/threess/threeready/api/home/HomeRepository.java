/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.api.home;

import android.util.Pair;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.netflix.INetflixGroup;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.ReplayChannelPageItem;
import tv.threess.threeready.api.tv.model.TvChannel;

/**
 * Extension methods for the home proxy to handle additional action and business logic.
 *
 * @author Barabas Attila
 * @since 2018.10.11
 */
public interface HomeRepository extends Component {

    /**
     * Returns a observable which will emmit the category content data.
     *
     * @param moduleConfig        The config of the category content module.
     * @param start               The pagination start index.
     * @param count               The pagination count.
     * @param dataSourceSelectors Sorting and filtering options for the content.
     *                            If null the default will be applied.
     */
    Observable<ModuleData<IBaseContentItem>> getCategoryContent(final ModuleConfig moduleConfig, final int start, final int count,
                                                                List<DataSourceSelector> dataSourceSelectors);

    /**
     * Returns a observable which will emmit the continue watching content data.
     *
     * @param moduleConfig The config of the continue watching module.
     * @param start        The pagination start index.
     * @param count        The pagination count.
     */
    Observable<ModuleData<IBaseContentItem>> getContinueWatching(ModuleConfig moduleConfig, int start, int count);

    /**
     * Returns a observable which will emmit the watch list data.
     *
     * @param moduleConfig The config of the watchlist module.
     * @param start        The pagination start index.
     * @param count        The pagination count.
     */
    Observable<ModuleData<IBaseContentItem>> getWatchlist(ModuleConfig moduleConfig, int start, int count);

    /**
     * Returns a observable which will emit the netflix tiles.
     *
     * @param moduleConfig The config of the watchlist module.
     * @param count        The pagination count.
     */
    Observable<ModuleData<INetflixGroup>> getNetflixGroups(ModuleConfig moduleConfig, int count);

    /**
     * Returns a list of tv channel filtered and ordered by the given module config.
     */
    Observable<ModuleData<TvChannel>> getChannels(ModuleConfig moduleConfig, int count);


    /**
     * Returns a list of {@link ReplayChannelPageItem} filtered and ordered by the given module config.
     */
    Observable<ModuleData<ReplayChannelPageItem>> getReplayChannelPageItems(ModuleConfig moduleConfig, int start, int count,
                                                                            List<DataSourceSelector> dataSourceSelectors);

    /**
     * Returns a observable which will emmit the now/next broadcasts.
     *
     * @param moduleConfig The config of the now/next module.
     * @param count        The pagination count.
     */
    Observable<ModuleData<Pair<IBroadcast, IBroadcast>>> getNowNext(final ModuleConfig moduleConfig, final int count, List<DataSourceSelector> dataSourceSelectors);

    /**
     * Returns a observable which will emmit the now broadcasts.
     *
     * @param moduleConfig The config of the now module.
     * @param count        The pagination count.
     */
    Observable<ModuleData<IBroadcast>> getNowOnTV(final ModuleConfig moduleConfig, final int count);

    /**
     * Get channels results for local search.
     */
    Observable<ModuleData<TvChannel>> getChannelSearchResults(ModuleConfig moduleConfig, int start, int count);

    /**
     * Get movie and series results for local search.
     */
    Observable<ModuleData<IBaseContentItem>> getProgramSearchResults(ModuleConfig moduleConfig, int start, int count);

    /**
     * Creates a page config for a give subcategory.
     * If the subcategory contains only content items (leaf category), the page will contain a collection with the items.
     * If the subcategory contains only other categories, the page will contain an editorial stripe with the categories.
     *
     * @param subcategoryId For which the page config needs to be created.
     * @return Returns the generated page config.
     */
    Single<PageConfig> getSubCategoryPage(String subcategoryId);

    /**
     * Send a feedback to the backend when an editorial item was viewed.
     *
     * @param editorialItem The editorial item to send the feedback for.
     */
    Completable editorialViewedFeedback(EditorialItem editorialItem);

    /**
     * Send a feedback to the backend when an editorial item is clicked.
     *
     * @param editorialItem The editorial item to send feedback for.
     */
    Completable editorialClickedFeedback(EditorialItem editorialItem);

    /**
     * Method used for clear the Http cache of the given swimlane.
     *
     * @param moduleId For identifying the correct swimlane.
     */
    Completable clearCacheByModuleConfigId(String moduleId);

    /**
     * @return Completable which waits until the client config is updated.
     */
    Completable waitForConfig();

    /**
     * @return Completable which waits until the translations are updated.
     */
    Completable waitForTranslations();
}
