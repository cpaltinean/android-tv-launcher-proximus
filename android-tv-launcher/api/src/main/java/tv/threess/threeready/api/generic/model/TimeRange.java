package tv.threess.threeready.api.generic.model;

import tv.threess.threeready.api.generic.helper.ArrayUtils;

public class TimeRange {
    private final long mStart;
    private final long mEnd;

    public TimeRange(long start, long end) {
        this.mStart = start;
        this.mEnd = end;
    }

    public long getStart() {
        return mStart;
    }

    public long getEnd() {
        return mEnd;
    }

    /**
     * @return true if the current range contains the given timestamp
     */
    public boolean contains(long timestamp) {
        return timestamp >= mStart && timestamp < mEnd;
    }

    /**
     * @return the remaining duration from the current timestamp
     * to the end of the range which contains this timestamp.
     */
    public static long remainingTime(TimeRange[] ranges, long timestamp) {
        if (ArrayUtils.isEmpty(ranges)) {
            return 0;
        }
        for (TimeRange range : ranges) {
            if (range.contains(timestamp)) {
                return range.mEnd - timestamp;
            }
        }
        return 0;
    }
}
