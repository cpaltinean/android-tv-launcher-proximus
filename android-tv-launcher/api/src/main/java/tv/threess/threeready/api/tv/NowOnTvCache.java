package tv.threess.threeready.api.tv;

import android.content.Context;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import tv.threess.lib.di.Component;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.api.tv.model.ZappingInfo;

/**
 * Class which holds and updates the zapping info list in memory.
 * The zapping info list contains all the accessible channel and the currently running programs on them.
 * @see ZappingInfo
 *
 * @author Barabas Attila
 * @since 2019.08.28
 */
public abstract class NowOnTvCache implements Component {

    protected final Context mContext;
    protected final Map<String, ZappingInfo> mFavoriteChannelZappingInfoMap = new HashMap<>();
    protected final Map<String, ZappingInfo> mChannelZappingInfoMap = new HashMap<>();

    protected List<ZappingInfo> mZappingInfoList = new ArrayList<>();

    public NowOnTvCache(Context context) {
        mContext = context;
    }

    /**
     * Request and store the zapping info list.
     * @return Rx completable which completes when the list is filled with data.
     */
    public abstract Completable initialize();

    /**
     * @return The stored zapping info list.
     */
    public List<ZappingInfo> getZappingInfoList() {
        return mZappingInfoList;
    }

    /**
     * @param channelId  The unique identifier of the channel.
     * @param preferFavorite Prefer the favorite channel index if present.
     * @return The position of the channel in the list.
     */
    public int getPositionForChannel(String channelId, boolean preferFavorite) {
        // Index of channel.
        int favoriteChannelIndex = -1;
        int channelIndex = -1;

        for (int i = 0; i < mZappingInfoList.size(); i++) {
            ZappingInfo zappingInfo = mZappingInfoList.get(i);
            if (zappingInfo.getChannel().getId().equalsIgnoreCase(channelId)) {
                if (zappingInfo.isFromFavorites()) {
                    favoriteChannelIndex = i;
                }
                channelIndex = i;
            }
        }

        // Prefer favorite channel index if present.
        if (preferFavorite && favoriteChannelIndex != -1) {
            channelIndex = favoriteChannelIndex;
        }

        // Channel not found -> by default the first channel will be selected
        if (channelIndex < 0) {
            channelIndex = 0;
        }

        return channelIndex;
    }

    /**
     * @param channelId The id of the channel for which the zapping info should be returned.
     * @param isFavorite True if the favorite channels should be checked first.
     * @return The zapping info for the given channel or null if the channel doesn't exists.
     */
    @Nullable
    public ZappingInfo getZappingInfoForChannel(String channelId, boolean isFavorite) {
        if (channelId == null) {
            return null;
        }

        ZappingInfo zappingInfo = null;
        if (isFavorite) {
            zappingInfo = mFavoriteChannelZappingInfoMap.get(channelId);
        }

        if (zappingInfo == null) {
            zappingInfo = mChannelZappingInfoMap.get(channelId);
        }

        return zappingInfo;
    }

    /**
     * @param position The position on which the zapping info should be returned.
     * @return The zapping info at the given position or null if there is no such position in the list.
     */
    @Nullable
    public ZappingInfo getZappingInfoAtPosition(int position) {
        if (mZappingInfoList == null || mZappingInfoList.isEmpty()
                || position < 0 || position > mZappingInfoList.size()) {
            return null;
        }

        return mZappingInfoList.get(position);
    }

    @Nullable
    public ZappingInfo getNextChannelZappingInfo(String currentChannelId, int positionOffset) {
        int currentPosition = getPositionForChannel(currentChannelId, false);

        int length = mZappingInfoList.size();
        int pos = (currentPosition + positionOffset) % length;
        int nextPosition = pos < 0 ? length + pos : pos;

        return getZappingInfoAtPosition(nextPosition);
    }

    /**
     * Get the channel with the give id from memory cache.
     *
     * @param channelId The if of the channel which needs to be returned.
     * @return The channel with the give id or null if there is no such.
     */
    @Nullable
    public TvChannel getChannel(String channelId) {
        return getChannel(channelId, false);
    }

    /**
     * Get the channel with the give id from memory cache.
     *
     * @param channelId      The if of the channel which needs to be returned.
     * @param preferFavorite True if the favorite channel should returned if exists.
     * @return The channel with the give id or null if there is no such.
     */
    @Nullable
    public TvChannel getChannel(String channelId, boolean preferFavorite) {
        ZappingInfo zappingInfo = getZappingInfoForChannel(channelId, preferFavorite);
        if (zappingInfo != null) {
            return zappingInfo.getChannel();
        }
        return null;
    }

    /**
     * @return The call letter of the given channel id.
     * Or null if there is no such channel.
     */
    @Nullable
    public String getCallLetter(String channelId) {
        TvChannel channel = getChannel(channelId);
        return channel == null ? null : channel.getCallLetter();
    }

    /**
     * Get the channel and the currently running program on it.
     *
     * @param channelId      The id of the channel for which the zapping info needs to be returned.
     * @param preferFavorite True if the channel should be taken from the favorite list first.
     * @return The zapping info which holds the channel and the currently running program.
     */
    @Nullable
    public ZappingInfo getZappingInfo(String channelId, boolean preferFavorite) {
        return getZappingInfoForChannel(channelId, preferFavorite);
    }

    /**
     * Returns the last played radio channel id.
     */
    @Nullable
    public String getLastPlayedRadioChannelId() {
        return getLastPlayedChannelId(ChannelType.RADIO);
    }

    /**
     * @param currentChannelId The unique identifier of the channel based on which the next channel should be returned.
     * @return The next channel from the channel list.
     */
    @Nullable
    public TvChannel getNextChannel(String currentChannelId) {
        return getNextChannel(currentChannelId, 1);
    }

    /**
     * @param currentChannelId The unique identifier of the channel based on which the next channel should be returned.
     * @return The previous channel from the channel list.
     */
    @Nullable
    public TvChannel getPreviousChannel(String currentChannelId) {
        return getNextChannel(currentChannelId, -1);
    }

    /**
     * @param currentChannelId The unique identifier of the channel based on which the next channel should be returned.
     * @param positionOffset   The length and the direction of the step to the next channel.
     *                         If positive it will return the next channel, if negative the previous channel.
     * @return The next channel from the list.
     */
    @Nullable
    private TvChannel getNextChannel(String currentChannelId, int positionOffset) {
        ZappingInfo zappingInfo = getNextChannelZappingInfo(currentChannelId, positionOffset);
        return zappingInfo != null ? zappingInfo.getChannel() : null;
    }

    /**
     * Returns the last played tv channel id TV or Radio.
     */
    @Nullable
    public String getLastPlayedChannelId() {
        return getLastPlayedChannelId(ChannelType.TV);
    }

    /**
     * Returns the last played channel id based on the type and saved ch number.
     *
     * @param channelType Radio or TV
     * @return the channel id
     */
    @Nullable
    private String getLastPlayedChannelId(ChannelType channelType) {
        switch (channelType) {
            case RADIO:
                return getChannelId(getLastPlayedChannelNumber(Settings.lastPlayedRadioChannelNumber), Collections.singletonList(ChannelType.RADIO));
            case TV:
                return getChannelId(getLastPlayedChannelNumber(Settings.lastPlayedChannelNumber), Arrays.asList(ChannelType.TV, ChannelType.RADIO));
        }
        throw new IllegalArgumentException("Channel type not recognized: " + channelType.name());
    }

    private int getLastPlayedChannelNumber(Settings lastPlayedChannelNumber) {
        int lastChannelNumber = -1;
        try {
            lastChannelNumber = lastPlayedChannelNumber.get(lastChannelNumber);
        } catch (NumberFormatException e) {
            //nothing we can do
        }
        return lastChannelNumber;
    }

    /**
     * Returns the first channel's id based on the number and types.
     *
     * @param lastChannelNumber the last tuned channel number
     * @param channelTypes      the channel types used at filtering
     * @return the found channel's id or null in case there is no match
     */
    @Nullable
    private String getChannelId(final int lastChannelNumber, List<ChannelType> channelTypes) {
        return getZappingInfoList().stream().filter(
                zappingInfo -> !zappingInfo.isFromFavorites() && channelTypes.contains(zappingInfo.getChannel().getType())
                        && zappingInfo.getChannel().getNumber() >= lastChannelNumber)
                .findFirst()
                .map(zappingInfo -> zappingInfo.getChannel().getId()).orElse(null);
    }
}