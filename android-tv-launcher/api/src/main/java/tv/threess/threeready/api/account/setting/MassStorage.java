package tv.threess.threeready.api.account.setting;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountCache;

/**
 * Mass Storage provided by Technicolor.
 * Use this storage for store values which we need even after a clean data.
 *
 * @author David Bondor
 * @since 2020.5.25
 */
public enum MassStorage implements SettingProperty, ReadableSetting {
    previousBootId,
    tuneTvAppVersion,

    lastKnownHeartBeatBootId,
    lastKnownHeartBeatUptime,
    lastKnownHeartBeatEpoch,
    lastKnownHDMIConnection,
    lastKnownRemoteConnection,
    lastKnownScreenOn,
    lastPlaybackStartedEvent,
    lastPlaybackStartTimestamp,

    lastAppUsageEventsReportTime;

    private static final AccountCache.MassStorage sMassStorage = Components.get(AccountCache.MassStorage.class);

    @Override
    public String getName() {
        return name();
    }

    @Override
    public String getValue() {
        return sMassStorage.getMassStorage(this);
    }

    public SettingEditor<MassStorage> edit() {
        return new SettingEditor<>(this, sMassStorage::putMassStorage);
    }

    public static BatchSettingEditor<MassStorage> batchEdit() {
        return new BatchSettingEditor<>(sMassStorage::putMassStorage);
    }
}