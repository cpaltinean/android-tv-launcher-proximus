/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.log.model;

/**
 * Playback related events that can be sent or saved to db.
 *
 * @author Arnold Szabo
 * @since 2017.07.24
 */
public enum PlaybackLogEvent implements Event.Kind {

    // Command is initiated by the user, and added to the command queue
    StartInitiated,

    // Command is taken from the command queue, and starts executing
    ExecuteJump,         // Relative/Absolute jump initiated - stop event

    // Start command is resolved playback url received from the backend
    StartResolved,

    // Command was executed (success or fail).
    PlaybackStarted,     // Start command completed
    PlaybackPaused,      // Pause command completed
    PlaybackResumed,     // Resume command completed
    JumpExecuted,        // Relative/Absolute jump executed - start event
    PlaybackStopped,     // Stop or Clear command completed
    MassStoragePlaybackStopped, // Generated playback stopped event which is saved to mass storage
    PlaybackFailed,
    PlaybackUnavailable,
    PlaybackEnded,
    PlaybackDisconnected,   // playback process disconnected (usually due to a crash)
    PlaybackBuffered,       // Playback is interrupted and resumed without any user interaction.

    StartLoadingSpinner,
    StopLoadingSpinner,

    // Exo-player buffering notifications.
    SegmentDownloaded,
    BitrateChanged,
    StateChanged,

    // TAD events
    AdTrigger,
    AdReplacement,

    Unknown;

    public static final String DOMAIN = "Playback";

    @Override
    public String domain() {
        return DOMAIN;
    }
}
