/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.log.model;

/**
 * Playback event param keys enum
 *
 * @author Arnold Szabo
 * @since 2017.07.24
 */
public interface PlaybackEventValues {

    /**
     * The player started loading content and is loading the content meta-data.
     */
    String STATE_PREPARING = "preparing";

    /**
     * The player does not have enough data to start playing and is buffering now.
     */
    String STATE_BUFFERING = "buffering";

    /**
     * The player is prepared and currently playing.
     */
    String STATE_PLAYING = "playing";

    /**
     * The player is prepared and has enough data to start playback immediately but is currently paused.
     */
    String STATE_PAUSING = "pausing";

    /**
     * The player controller is not prepared or preparing and no content is loaded.
     * This is the initial player state and the state the player returns to after it is destroyed.
     */
    String STATE_IDLE = "idle";

    /**
     * Playback finished successfully and the end of the data stream was reached.
     */
    String STATE_FINISHED = "finished";

    /**
     * Playback process disconnected from tv view.
     */
    String DISCONNECTED = "disconnected";
}
