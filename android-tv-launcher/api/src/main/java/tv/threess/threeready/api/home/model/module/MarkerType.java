/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.api.home.model.module;

/**
 * This class will store the defined content marker types, which should be displayed on cards from stripes/collections
 *
 * @author Szende Pal
 * @since 2017.07.26
 */
public enum MarkerType {
    NOW,
    LATER,
    NEXT,
    NOW_PLAYING,
    FREE,
    RENTED,
    PLANNED,
    PRICE,
    RADIO,
    INCLUDED,
    SUBSCRIBE,
    DELETE_SOON,
    LAST_CHANCE,
    NEW,
    DEFAULT
}
