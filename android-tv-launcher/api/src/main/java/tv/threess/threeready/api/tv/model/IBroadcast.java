package tv.threess.threeready.api.tv.model;

import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.model.VideoQuality;
import tv.threess.threeready.api.pvr.model.SingleRecordingStatus;

/**
 * Basic interface vor TV broadcast objects.
 *
 * @author Andor Lukacs
 * @since 10/2/18
 */
public interface IBroadcast extends IContentItem {

    String getProgramId();

    String getChannelId();

    /**
     * @return start time of the broadcast in milliseconds
     */
    long getStart();

    /**
     * @return end time of the broadcast in milliseconds.
     */
    long getEnd();

    /**
     * @return The duration of the broadcast in milliseconds.
     */
    @Override
    default long getDuration() {
        return getEnd() - getStart();
    }

    default VideoQuality getVideoQuality(TvChannel channel) {
        if (channel != null && channel.isHDEnabled()) {
            return VideoQuality.HD;
        }
        return VideoQuality.SD;
    }

    /**
     * @param channel The channel on which the broadcast available.
     * @return True if the broadcast is available for replay and not blacklisted.
     */
    default boolean canReplay(TvChannel channel) {
        return channel != null && channel.isReplayEnabled()
                && isInReplayWindow(channel) && !isBlackListed();
    }

    /**
     * @param channel The channel on which the broadcast available.
     * @return True if the broadcast is available for replay,
     * even if it's currently running blacklisted program.
     */
    default boolean canStartOver(TvChannel channel, long playbackStartTime) {
        return channel != null && channel.isReplayEnabled()
                && isInReplayWindow(channel) && (!isBlackListed() || playbackStartTime < getEnd());
    }

    /**
     * @return True if the broadcast is in the replay window of the channel.
     */
    default boolean isInReplayWindow(TvChannel channel) {
        return getStart() >= System.currentTimeMillis() - channel.getReplayWindow();
    }

    /**
     * Check if this broadcast is playing on the the given channelId
     */
    default boolean isOnChannel(String channelId) {
        if (channelId == null) {
            return false;
        }
        return channelId.equals(getChannelId());
    }

    /**
     * Check if this broadcast is playing on the the given channel
     */
    default boolean isOnChannel(TvChannel channel) {
        if (channel == null) {
            return false;
        }
        return isOnChannel(channel.getId());
    }

    /**
     * Check if this broadcast is playing on the same channel as the given broadcast
     */
    default boolean isOnChannel(IBroadcast broadcast) {
        if (broadcast == null) {
            return false;
        }
        return isOnChannel(broadcast.getChannelId());
    }

    /**
     * @return True if the broadcast is blacklisted for replay playback.
     */
    boolean isBlackListed();

    /**
     * Checks if the program is next after the live program.
     *
     * @return True if the program is next after the live program, otherwise false.
     */
    boolean isNextToLive();

    /**
     * @return true if the broadcast is recording capable.
     */
    boolean isNPVREnabled();

    /**
     * @return the asset identifier used for unskipable advertisements.
     */
    String getAssetIdentifier();

    /**
     * @return true if the broadcast is a placeholder, no info or loading.
     */
    default boolean isGenerated() {
        return false;
    }

    /**
     * @return true if event start time if before current time and end time after current time
     */
    default boolean isLive() {
        long now = System.currentTimeMillis();
        return getStart() < now
                && getEnd() >= now;
    }

    /**
     * @return true if event start time is after current time
     */
    default boolean isFuture() {
        return getStart() > System.currentTimeMillis();
    }

    /**
     * @return true if event end time if before current time
     */
    default boolean isPast() {
        return getEnd() <= System.currentTimeMillis();
    }

    /**
     * @return true if the current broadcast is saved from the BE.
     */
    default boolean isLoaded() {
        return true;
    }

    /**
     * @return True if Add to watch list button should be shown, otherwise false.
     */
    default boolean shouldShowAddToWatchListButton(TvChannel channel, SingleRecordingStatus singleRecordingStatus) {
        if (isPast()) {
            return canReplay(channel)
                    || singleRecordingStatus == SingleRecordingStatus.COMPLETED
                    || singleRecordingStatus == SingleRecordingStatus.RECORDING;
        }
        return true;
    }
}

