/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.api.log;

import java.io.File;
import java.util.List;

import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.log.model.UILogEvent.Detail;
import tv.threess.threeready.api.log.model.VodItemType;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodVariant;

/**
 * Handle UI related event logging.
 *
 * @author Barabas Attila
 * @since 2018.01.10
 */

public class UILog {

    /**
     * Button press from remote.
     *
     * @param button The button to be logged.
     */
    public static void logButtonPressFromRemote(UILog.ButtonType button) {
        Log.event(new Event<>(UILogEvent.ButtonPress)
                .putDetail(Detail.ButtonName, button)
        );
    }

    /**
     * Trigger local or google assistant search.
     *
     * @param searchType     local|ga_voice|ga_text
     * @param searchTermType movie|series|channel
     * @param searchTerm     The entered search term
     */
    public static void logSearchTrigger(SearchType searchType,
                                        SearchTermType searchTermType, String searchTerm) {
        Log.event(new Event<>(UILogEvent.Search)
                .putDetail(Detail.SearchType, searchType)
                .putDetail(Detail.SearchTermType, searchTermType)
                .putDetail(Detail.SearchTerm, searchTerm)
        );
    }

    public static void logRecordingEvent(UILogEvent eventAction, IBroadcast broadcast, TvChannel channel) {
        if (eventAction == null || broadcast == null || channel == null) {
            return;
        }
        Log.event(new Event<>(eventAction)
                .addDetail(Detail.Duration, broadcast.isGenerated() ? 0 : broadcast.getDuration())
                .addDetail(Detail.Start, broadcast.getStart())
                .addDetail(Detail.ChannelId, broadcast.getChannelId())
                .addDetail(Detail.ChannelCallLetter, channel.getCallLetter())
                .addDetail(Detail.ProgramId, broadcast.getProgramId())
                .addDetail(Detail.ItemTitle, broadcast.getTitle())
                .addDetail(Detail.SeasonNr, broadcast.getSeasonNumber())
                .addDetail(Detail.EpisodeNr, broadcast.getEpisodeNumber())
                .addDetail(Detail.EpisodeTitle, broadcast.getEpisodeTitle())
                .addDetailAsString(Detail.Resolution, broadcast.getVideoQuality(channel))
                .addDetailAsString(Detail.ProgramCategory, broadcast.getGenres())
                .addDetailAsString(Detail.ProgramSubCategory, broadcast.getSubGenres())
        );
    }

    public static void logVodRental(IVodVariant vodVariant, IVodPrice price) {
        if (vodVariant == null || price == null) {
            return;
        }

        Event<UILogEvent, UILogEvent.Detail> event = new Event<>(UILogEvent.VodRental);
        event.addDetail(Detail.CurrencyUnit, price.getCurrencyType());

        List<String> genres = vodVariant.getGenres();
        if (genres != null && !genres.isEmpty()) {
            event.addDetail(Detail.Genre, genres.get(0));
        }

        event.addDetail(Detail.GroupTitle, vodVariant.getGroupTitle());
        event.addDetail(Detail.RentalCost, price.getCurrencyAmount());
        event.addDetailAsString(Detail.Resolution, vodVariant.getHighestQuality());
        event.addDetail(Detail.SuperGenre, vodVariant.getSuperGenre());
        event.addDetail(Detail.VodExternalIdentifier, vodVariant.getId());
        event.addDetail(Detail.VodItemIdentifier, vodVariant.getInternalId());
        event.addDetail(Detail.VodTitle, vodVariant.getItemTitle());
        event.addDetail(Detail.SeriesTitle, vodVariant.getSeriesTitle());
        event.addDetail(Detail.EpisodeNr, vodVariant.getEpisodeNumber());
        event.addDetail(Detail.SeasonNr, vodVariant.getSeasonNumber());

        if (vodVariant.isTVod()) {
            event.addDetail(Detail.VodType, vodVariant.isFree() ? VodItemType.FVOD : VodItemType.TVOD);
        } else if (vodVariant.isSVod()) {
            event.addDetail(Detail.VodType, VodItemType.SVOD);
        }
        Log.event(event);
    }

    /**
     * Log ad download events
     * @param adFile        downloaded ad
     * @param e             exception in case download failure
     * @param downloadTime  time it took to download
     * @param triggerDelay  delay between triggering the download and starting the download
     * @param attemptCount  how many times have we tried to download the ad
     */
    public static void logAdDownload(File adFile, Exception e, long downloadTime, long triggerDelay, int attemptCount) {
        Event<UILogEvent, UILogEvent.Detail> event = new Event<>(UILogEvent.AdDownload);
        event.addDetail(UILogEvent.Detail.AssetId, adFile != null ? adFile.getName() : null);
        event.addDetail(UILogEvent.Detail.AssetHandlingResult, adFile != null && adFile.exists());
        event.addDetail(UILogEvent.Detail.AssetAttemptCounter, attemptCount);
        if (e instanceof BackendException) {
            event.addDetail(UILogEvent.Detail.DownloadErrorCode, ((BackendException) e).getErrorCode());
        }
        event.addDetail(UILogEvent.Detail.AssetHandlingDuration, downloadTime > 0 ? downloadTime : 0);
        event.addDetail(UILogEvent.Detail.AssetRandomTimer, triggerDelay);
        event.addDetail(UILogEvent.Detail.AssetHandlingFailureCause, e != null ? e.getMessage() : null);
        Log.event(event);
    }

    /**
     * Search display type.
     */
    public enum SearchType {
        LOCAL,
        GA,
    }

    /**
     * Type of the requested search data.
     */
    public enum SearchTermType {
        CHANNEL,
        MOVIE,
        SERIES
    }

    /**
     * Type of the displayed view.
     */
    public enum MiniEpgType {
        ZAPPER,
        TV_GUIDE,
        FAVORITES
    }

    /**
     * Type of the displayed view.
     */
    public enum ButtonType {
        TV,
        HOME,
        GUIDE,
        PLAYPAUSE,
        BACK,
        OK,
        NETFLIX,
        RADIO,
        SUB,
        REWIND,
        FASTFORWARD,
        STOP,
        RECORD
    }

    /**
     * Enum represents the name of a reported page.
     */
    public enum Page {
        ActionMenuVod,
        ActionMenuVodDialog,
        ApplicationFullContentPage,
        BroadCasterInfoPage,
        CatalogueLanguage,
        ChannelRenumbering,
        CheckUpdatesPage,
        ChooseVersionPage,
        ContentWallFullContentFilterPage,
        ContentWallFullContentPage,
        DevicePreferencesPage,
        EpgGridPage,
        ExplorePage,
        GeneralLanguageSubtitleListPage,
        GeneralSettings,
        Home,
        KeywordEntryPage,
        LanguageList,
        MainMenu,
        MenuLanguage,
        MediaPlayerPage,
        MiniEPGPage,
        Notifications,
        PinCodeManagement,
        Restart,
        ProgramDetail,
        PVRSettings,
        RecordActionSubMenuConfirm,
        RecordActionSubMenuDialog,
        RecordActionSubMenuMain,
        RecordActionSubMenuManage,
        SecurityPrivacy,
        Search,
        Settings,
        SeriesContentPage,
        SortingOptionsPage,
        StandBy,
        StoreSeriesFullContentPage,
        StoreFullContentFilterPage,
        StoreSubcategoryBrowsing,
        StripeLinkPage,
        SupportPage,
        SystemInfoPage,
        Tips,
        TVGuideLayout,
        TvPlayer,
        Unknown,
        UnlockPageVodPurchase,
        WatchFromBroadcaster,
        WatchFromVOD,
        ZapListPage
    }

    /**
     * Enum represents the different actions which can open a page.
     */
    public enum OpenedBy {
        Ok,
        Back,
        Home,
        TvGuide,
        Radio,
        Timeout,
        Restart,
        Standby
    }
}
