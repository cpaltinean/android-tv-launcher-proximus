package tv.threess.threeready.api.playback.model;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.playback.model.IReplayOttStreamingSession.IBlacklistSlot;

/**
 * Container for replay streaming session for continue playback.
 *
 * @author Barabas Attila
 * @since 11/23/20
 */
public class ReplayStreamingSessionContainer<TReplayStreamingSession extends IReplayOttStreamingSession>
        extends OTTStreamingSessionContainer<TReplayStreamingSession> {

    public ReplayStreamingSessionContainer() {
    }

    /**
     * @param playbackInitTime The time when the playback was initiated.
     * @param accurateContentTime The accurate time in the content.
     * @return A blacklisting slots if there is an active at the given time.
     */
    @Nullable
    public IBlacklistSlot getActiveBlacklistSlot(long playbackInitTime, long accurateContentTime) {
        IReplayOttStreamingSession session = findSessionForContentPosition(accurateContentTime);
        if (session == null) {
            return null;
        }

        for (IBlacklistSlot slot : session.getBlacklistedSlots()) {
            if (slot.isActive(playbackInitTime)
                    && slot.getStart() <= accurateContentTime
                    && slot.getEnd() > accurateContentTime) {
                return slot;
            }
        }

        return null;
    }

    /**
     * @param playbackInitTime The time when the playback was initiated.
     * @return The list of blacklisting slots which are valid for the current playback.
     */
    public List<IBlacklistSlot> getActiveBlacklistedSlots(long playbackInitTime) {
        // Combine slots for all the sessions.
        List<IBlacklistSlot> blacklistSlots = new ArrayList<>();
        for (IReplayOttStreamingSession session : this) {
            blacklistSlots.addAll(session.getBlacklistedSlots());
        }

        // Remove slots which are not active yet.
        blacklistSlots.removeIf(slot -> !slot.isActive(playbackInitTime));

        // Sort the slots by start.
        blacklistSlots.sort((s1, s2) -> Long.compare(s1.getStart(), s2.getStart()));

        return blacklistSlots;
    }

    /**
     * @param playbackInitTime The time when the playback was initiated.
     * @param accurateContentPosition The accurate position in the content to search from.
     * @return The first active blacklist slot previous to the accurate content position;
     */
    public IBlacklistSlot getPreviousBlacklistSlot(long playbackInitTime, long accurateContentPosition) {
        IBlacklistSlot prevSlot = null;
        List<IBlacklistSlot> activeSlots = getActiveBlacklistedSlots(playbackInitTime);
        for (IBlacklistSlot slot : activeSlots) {
            if (slot.getStart() > accurateContentPosition) {
                break;
            }
            prevSlot = slot;
        }

        return prevSlot;
    }

    /**
     * @param playbackInitTime The time when the playback was initiated.
     * @param accurateContentPosition The accurate position in the content to search from.
     * @return The first active blacklist slot next to the accurate content position;
     */
    public IBlacklistSlot getNextBlacklistSlot(long playbackInitTime, long accurateContentPosition) {
        List<IBlacklistSlot> activeSlots = getActiveBlacklistedSlots(playbackInitTime);
        for (IBlacklistSlot slot : activeSlots) {
            if (slot.getStart() > accurateContentPosition) {
                return slot;
            }
        }
        return null;
    }
}
