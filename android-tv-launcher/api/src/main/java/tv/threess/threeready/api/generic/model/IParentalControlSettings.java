/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */
package tv.threess.threeready.api.generic.model;

import java.io.Serializable;

import tv.threess.threeready.api.pc.model.ParentalRating;

/**
 * Settings container which holds the parental control related configurations.
 *
 * @author Barabas Attila
 * @since 2019.07.12
 */
public interface IParentalControlSettings extends Serializable {

    /**
     * @return True if the parental control is enabled.
     */
    boolean isEnabled();

    /**
     * @return The parental rating which was previously selected by the user.
     */
    ParentalRating getParentalRating();

}
