package tv.threess.threeready.api.vod.wrapper;

import androidx.annotation.NonNull;

import java.util.Map;

import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IVodItem;

/**
 * Entity used for renting, containing all the needed information
 *
 * the number of available credit for the user.
 * the full vod item containing all the variants.
 * a map with all the available variants and rent offers.
 */
public class VodRentOffers {
    private final int mCredit;
    private final IVodItem mVod;
    private final Map<String, IRentOffer> mVariantOfferMap;

    public VodRentOffers(IVodItem movie, int credit, @NonNull Map<String, IRentOffer> variantOfferMap) {
        this.mVod = movie;
        this.mCredit = credit;
        this.mVariantOfferMap = variantOfferMap;
    }

    public IVodItem getVod() {
        return mVod;
    }

    public int getCredit() {
        return mCredit;
    }

    @NonNull
    public Map<String, IRentOffer> getVariantOfferMap() {
        return mVariantOfferMap;
    }
}
