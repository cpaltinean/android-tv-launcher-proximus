package tv.threess.threeready.api.vod.wrapper;

import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IVodVariant;


/**
 * This is a model class which contains a vod variant and it's corresponding offer.
 *
 * Created by Bartalus Csaba - Zsolt
 * since 2022.04.07
 */
public class VariantRentOffer {
    private final IVodVariant vodVariant;
    private final IRentOffer offer;

    public VariantRentOffer(IVodVariant vodVariant, IRentOffer offer) {
        this.vodVariant = vodVariant;
        this.offer = offer;
    }

    public IRentOffer getOffer() {
        return offer;
    }

    public IVodVariant getVodVariant() {
        return vodVariant;
    }
}
