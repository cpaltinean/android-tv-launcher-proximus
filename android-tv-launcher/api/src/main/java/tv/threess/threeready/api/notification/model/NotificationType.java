package tv.threess.threeready.api.notification.model;

/**
 * Enum used for the different notification types.
 *
 * @author David Bondor
 * @since 2022.04.19
 */
public enum NotificationType {
    SYSTEM_NOTIFICATION,
    APP_NOTIFICATION
}