#Proximus Android TV (v7)

# Project Details

## Prerequisites
- Gradle 6.1.1
- Android Studio
- A proper device with
   - connection to the Proximus VPN
   - TCH MW version 3.58.2+ (preferably the latest MW version, otherwise some features might not work correctly)

The following Android SDK's that can be installed separately or directly from Android Studio's SDK Manager:
- Android SDK Platform Tools
- Android SDK Tools
- Android SDK Platform with API Level 28 (since the Android Target SDK is 28)
- Android SDK Build Tools 29.0.2

# Project Setup
1. Clone the Repository
  As usual, you get started by cloning the project to your local machine from: https://gitlab.3ss.tv/3ready/android-tv-launcher-proximus

2. Open Application in Android Studio, start the Gradle sync process (usually it is started automatically). It can be started manually using the "Sync Project Project with gradle files" under the "Files" tab.

3. Next build a specific version of the app (See more details about the versions under the Section "Build Variants") with one of the following commands using Android Studio terminal or any other terminal (make sure when executing the command you need to be in the root folder of the project):
Proximus AndroidTV Debug version: gradlew app:assembleProximusDebug
Proximus AndroidTV Develop version: gradlew app:assembleProximusDevelop
Proximus AndroidTV Production version: gradlew app:assembleProximusProduction

4. Connect a real Android device to you development machine

5. Install the version of the app to a real device using the corresponding command:
gradlew installProximusDebug
gradlew installProximusDevelop
gradlew installProximusProduction

Note: This is valid for Android Studio 3.6 and 4.1, we do not guarantee the normal functioning for other versions.

## Build Variants
A build variant consists of a build type, on Proximus we have 3 build types (*debug, develop, production*).

Build Types:
- The *Debug* variants have default debug signing configuration with the debug flag enabled,
- The *Develop* and *Production* variants have a custom signing configuration.
- Proguard is enabled only on the *Develop* and *Production* build variants.
- Firebase Crashlytics is enabled only on the *Production* build variant.

## External Dependencies
The application communicates with BEP (BelgacomTV Entertainment Platform) a.k.a Proximus Back-end video platform.
If BEP is not reachable, then necessary data might not be received and missing and the full functionality of the application is not available.
In order to login to the service a Proximus line number with a PIN code is needed also.

# LICENSE

Copyright © 2021 Proximus

# Remote Control shortcuts
```text
<OK> + <HOME> for 3 seconds: Manual Pairing
<OK> + <DIGIT 0> for 3 seconds: Clear Pairing Table
<OK> + <DIGIT 1>: Switch to IR mode.
<OK> + <DIGIT 2>: Switch to BT mode.
<OK> + <DIGIT 3>: Set Volume keys as TV keys.
<OK> + <DIGIT 4>: Set Volume keys as STB keys.
<DIGIT 1> + <DIGIT 3> for 3 seconds: Reset TV configuration
<DIGIT 7> + <DIGIT 9> for 3 seconds: Factory Reset
<DIGIT 4> + <DIGIT 6> for 3 seconds, <Learn Key>: IR Learning
<DIGIT 1> + <DIGIT 6> for 3 seconds, <Learn Key>: Clear Learn Data
```

# Useful commands and tools

make the box to capture logs to an attached usb stick
* CreateDirectory <UsbStick>/Android/data/com.technicolor.android.usblogservice/files/uiw4020pxm
* insert the stick in the box
* reboot the box

open system settings
```shell
adb shell am start -a android.settings.SETTINGS
```

clear application data
```shell
adb shell pm clear be.px.stbtvclient
```

clear tv input framework data
```shell
adb shell pm clear com.android.providers.tv
```

dump memory
```shell
adb shell am dumpheap be.px.stbtvclient /data/local/tmp/mem.hprof
adb pull /data/local/tmp/mem.hprof
```

trigger internet check
```shell
adb shell am broadcast -a px.checkInternet
```

perform full caching
```shell
adb shell am broadcast -a tv.data.performCaching
```

trigger account info update
```shell
adb shell am broadcast -a tv.data.updateAccountInfo
```

trigger channel update
```shell
adb shell am broadcast -a tv.data.updateChannels --ez force-sync false --ez force-scan false
```

trigger broadcast update
```shell
adb shell am broadcast -a tv.data.updateBroadcasts
```

clear the tv cache, so http calls will go to the server
```shell
adb shell am broadcast -a fs.exec --esa cmd 'rm','-Rf','/data/data/be.px.stbtvclient/cache'
```

delete an entry from setting or session table
```shell
adb shell am broadcast -a px.deleteConfig --es settings "deviceIp"
adb shell am broadcast -a px.deleteConfig --es session "iamAccessToken"
```

reboot in recovery mode (to update the MW from usb stick)
```shell
adb reboot recovery
```

# Targeted Advertisement debug commands

trigger tad update metadata + download
```bash
adb shell am broadcast -a tv.data.downloadAds
```

List the content of the tad metadata database
```bash
adb shell content query --uri content://tv.threess.threeready.proximus.settings/TadMetadata
```

List the content of the tad partition
```bash
adb shell content query --uri content://tv.threess.threeready.proximus.settings/TadPartition
```

delete tad metadata from local database
```bash
adb shell content delete --uri content://tv.threess.threeready.proximus.settings/TadMetadata
```

delete all the downloaded files from the tad partition
```bash
adb shell content delete --uri content://tv.threess.threeready.proximus.settings/TadPartition
```

In case something more complicated needs to be done, any command can be executed
on the box with the result printed to logcat, like `ls`, `rm`, `cp`, etc
```bash
adb shell am broadcast -a fs.exec --esa cmd 'ls','-lAtruc','/tad/pre'
```

```bash
adb shell am broadcast -a fs.exec --esa cmd 'cp','-R','/tad/pre/1029244-rtbf-audio.ts','/sdcard/Download/'
```

```bash
adb shell am broadcast -a fs.exec --esa cmd 'rm','-Rf','/tad/pre/1029244-rtbf-audio.ts'
```

on Atv-11 the tch databases can also be checked with:
```bash
adb shell am broadcast -a px.query --es uri 'content://com.technicolor.android.dtvprovider/scte35_section'
adb shell am broadcast -a px.query --es uri 'content://com.technicolor.android.dtvprovider/scte35_state'
```
