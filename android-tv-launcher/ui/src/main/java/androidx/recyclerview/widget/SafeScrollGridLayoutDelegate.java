package androidx.recyclerview.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;

import java.util.ArrayList;

import tv.threess.threeready.api.log.Log;

/**
 * Gird layout which catches potential exception while the user scrolls and the adapter content changes.
 *
 * Related google issues
 * https://issuetracker.google.com/issues/177338150
 * https://issuetracker.google.com/issues/194682221
 *
 * TODO: remove it once we have a fix from google
 *
 * @author Barabas Attila
 * @since 6/15/22
 */
public class SafeScrollGridLayoutDelegate extends RecyclerView.LayoutManager {
    private static final String TAG = Log.tag(SafeScrollGridLayoutDelegate.class);

    private final RecyclerView.LayoutManager mDelegate;

    @SuppressLint("VisibleForTests")
    public static void setSafeScrollGridLayoutDelegate(RecyclerView recyclerView, RecyclerView.LayoutManager layout) {
        recyclerView.mLayout = new SafeScrollGridLayoutDelegate(layout);
    }

    public SafeScrollGridLayoutDelegate(RecyclerView.LayoutManager layoutManager) {
        mDelegate = layoutManager;
    }

    @Override
    public int scrollHorizontallyBy(int dx, RecyclerView.Recycler recycler, RecyclerView.State state) {
        try {
            return mDelegate.scrollHorizontallyBy(dx, recycler, state);
        } catch (Exception e) {
            Log.w(TAG, "Failed to scroll horizontally.", e);
        }
        return 0;
    }

    @Override
    public int scrollVerticallyBy(int dy, RecyclerView.Recycler recycler, RecyclerView.State state) {
        try {
            return mDelegate.scrollVerticallyBy(dy, recycler, state);
        } catch (Exception e) {
            Log.w(TAG, "Failed to scroll vertically.", e);
        }
        return 0;
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        try {
            mDelegate.onLayoutChildren(recycler, state);
        } catch (Exception e) {
            Log.w(TAG, "Failed to layout children. State : " + state, e);
        }
    }

    @Override
    public void collectAdjacentPrefetchPositions(int dx, int dy, RecyclerView.State state, LayoutPrefetchRegistry layoutPrefetchRegistry) {
        try {
            mDelegate.collectAdjacentPrefetchPositions(dx, dy, state, layoutPrefetchRegistry);
        } catch (Exception e) {
            Log.w(TAG, "Failed to collectAdjacentPrefetchPositions : " + state, e);
        }
    }

    // DELEGATES

    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return mDelegate.generateDefaultLayoutParams();
    }

    @Override
    public void setMeasuredDimension(Rect childrenBounds, int wSpec, int hSpec) {
        mDelegate.setMeasuredDimension(childrenBounds, wSpec, hSpec);
    }

    @Override
    public void requestLayout() {
        mDelegate.requestLayout();
    }

    @Override
    public void assertInLayoutOrScroll(String message) {
        mDelegate.assertInLayoutOrScroll(message);
    }

    @Override
    public void assertNotInLayoutOrScroll(String message) {
        mDelegate.assertNotInLayoutOrScroll(message);
    }

    @Override
    public void setAutoMeasureEnabled(boolean enabled) {
        mDelegate.setAutoMeasureEnabled(enabled);
    }

    @Override
    public boolean isAutoMeasureEnabled() {
        return mDelegate.isAutoMeasureEnabled();
    }

    @Override
    public boolean supportsPredictiveItemAnimations() {
        return mDelegate.supportsPredictiveItemAnimations();
    }

    @Override
    public void collectInitialPrefetchPositions(int adapterItemCount, LayoutPrefetchRegistry layoutPrefetchRegistry) {
        mDelegate.collectInitialPrefetchPositions(adapterItemCount, layoutPrefetchRegistry);
    }

    @Override
    public boolean isAttachedToWindow() {
        return mDelegate.isAttachedToWindow();
    }

    @Override
    public void postOnAnimation(Runnable action) {
        mDelegate.postOnAnimation(action);
    }

    @Override
    public boolean removeCallbacks(Runnable action) {
        return mDelegate.removeCallbacks(action);
    }

    @Override
    public void onAttachedToWindow(RecyclerView view) {
        mDelegate.onAttachedToWindow(view);
    }

    @Override
    public void onDetachedFromWindow(RecyclerView view) {
        mDelegate.onDetachedFromWindow(view);
    }

    @Override
    public void onDetachedFromWindow(RecyclerView view, RecyclerView.Recycler recycler) {
        mDelegate.onDetachedFromWindow(view, recycler);
    }

    @Override
    public boolean getClipToPadding() {
        return mDelegate.getClipToPadding();
    }

    @Override
    public void onLayoutCompleted(RecyclerView.State state) {
        mDelegate.onLayoutCompleted(state);
    }

    @Override
    public boolean checkLayoutParams(RecyclerView.LayoutParams lp) {
        return mDelegate.checkLayoutParams(lp);
    }

    @Override
    public RecyclerView.LayoutParams generateLayoutParams(ViewGroup.LayoutParams lp) {
        return mDelegate.generateLayoutParams(lp);
    }

    @Override
    public RecyclerView.LayoutParams generateLayoutParams(Context c, AttributeSet attrs) {
        return mDelegate.generateLayoutParams(c, attrs);
    }

    @Override
    public boolean canScrollHorizontally() {
        return mDelegate.canScrollHorizontally();
    }

    @Override
    public boolean canScrollVertically() {
        return mDelegate.canScrollVertically();
    }

    @Override
    public void scrollToPosition(int position) {
        mDelegate.scrollToPosition(position);
    }

    @Override
    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
        mDelegate.smoothScrollToPosition(recyclerView, state, position);
    }

    @Override
    public void startSmoothScroll(RecyclerView.SmoothScroller smoothScroller) {
        mDelegate.startSmoothScroll(smoothScroller);
    }

    @Override
    public boolean isSmoothScrolling() {
        return mDelegate.isSmoothScrolling();
    }

    @Override
    public int getLayoutDirection() {
        return mDelegate.getLayoutDirection();
    }

    @Override
    public void endAnimation(View view) {
        mDelegate.endAnimation(view);
    }

    @Override
    public void addDisappearingView(View child) {
        mDelegate.addDisappearingView(child);
    }

    @Override
    public void addDisappearingView(View child, int index) {
        mDelegate.addDisappearingView(child, index);
    }

    @Override
    public void addView(View child) {
        mDelegate.addView(child);
    }

    @Override
    public void addView(View child, int index) {
        mDelegate.addView(child, index);
    }

    @Override
    public void removeView(View child) {
        mDelegate.removeView(child);
    }

    @Override
    public void removeViewAt(int index) {
        mDelegate.removeViewAt(index);
    }

    @Override
    public void removeAllViews() {
        mDelegate.removeAllViews();
    }

    @Override
    public int getBaseline() {
        return mDelegate.getBaseline();
    }

    @Override
    public int getPosition(@NonNull View view) {
        return mDelegate.getPosition(view);
    }

    @Override
    public int getItemViewType(@NonNull View view) {
        return mDelegate.getItemViewType(view);
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View findContainingItemView(@NonNull View view) {
        return mDelegate.findContainingItemView(view);
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View findViewByPosition(int position) {
        return mDelegate.findViewByPosition(position);
    }

    @Override
    public void detachView(@NonNull View child) {
        mDelegate.detachView(child);
    }

    @Override
    public void detachViewAt(int index) {
        mDelegate.detachViewAt(index);
    }

    @Override
    public void attachView(@NonNull View child, int index, RecyclerView.LayoutParams lp) {
        mDelegate.attachView(child, index, lp);
    }

    @Override
    public void attachView(@NonNull View child, int index) {
        mDelegate.attachView(child, index);
    }

    @Override
    public void attachView(@NonNull View child) {
        mDelegate.attachView(child);
    }

    @Override
    public void removeDetachedView(@NonNull View child) {
        mDelegate.removeDetachedView(child);
    }

    @Override
    public void moveView(int fromIndex, int toIndex) {
        mDelegate.moveView(fromIndex, toIndex);
    }

    @Override
    public void detachAndScrapView(@NonNull View child, @NonNull RecyclerView.Recycler recycler) {
        mDelegate.detachAndScrapView(child, recycler);
    }

    @Override
    public void detachAndScrapViewAt(int index, @NonNull RecyclerView.Recycler recycler) {
        mDelegate.detachAndScrapViewAt(index, recycler);
    }

    @Override
    public void removeAndRecycleView(@NonNull View child, @NonNull RecyclerView.Recycler recycler) {
        mDelegate.removeAndRecycleView(child, recycler);
    }

    @Override
    public void removeAndRecycleViewAt(int index, @NonNull RecyclerView.Recycler recycler) {
        mDelegate.removeAndRecycleViewAt(index, recycler);
    }

    @Override
    public int getChildCount() {
        return mDelegate.getChildCount();
    }

    @Nullable
    @Override
    public View getChildAt(int index) {
        return mDelegate.getChildAt(index);
    }

    @Override
    public int getWidthMode() {
        return mDelegate.getWidthMode();
    }

    @Override
    public int getHeightMode() {
        return mDelegate.getHeightMode();
    }

    @Override
    public int getWidth() {
        return mDelegate.getWidth();
    }

    @Override
    public int getHeight() {
        return mDelegate.getHeight();
    }

    @Override
    public int getPaddingLeft() {
        return mDelegate.getPaddingLeft();
    }

    @Override
    public int getPaddingTop() {
        return mDelegate.getPaddingTop();
    }

    @Override
    public int getPaddingRight() {
        return mDelegate.getPaddingRight();
    }

    @Override
    public int getPaddingBottom() {
        return mDelegate.getPaddingBottom();
    }

    @Override
    public int getPaddingStart() {
        return mDelegate.getPaddingStart();
    }

    @Override
    public int getPaddingEnd() {
        return mDelegate.getPaddingEnd();
    }

    @Override
    public boolean isFocused() {
        return mDelegate.isFocused();
    }

    @Override
    public boolean hasFocus() {
        return mDelegate.hasFocus();
    }

    @Nullable
    @Override
    public View getFocusedChild() {
        return mDelegate.getFocusedChild();
    }

    @Override
    public int getItemCount() {
        return mDelegate.getItemCount();
    }

    @Override
    public void offsetChildrenHorizontal(int dx) {
        mDelegate.offsetChildrenHorizontal(dx);
    }

    @Override
    public void offsetChildrenVertical(int dy) {
        mDelegate.offsetChildrenVertical(dy);
    }

    @Override
    public void ignoreView(@NonNull View view) {
        mDelegate.ignoreView(view);
    }

    @Override
    public void stopIgnoringView(@NonNull View view) {
        mDelegate.stopIgnoringView(view);
    }

    @Override
    public void detachAndScrapAttachedViews(@NonNull RecyclerView.Recycler recycler) {
        mDelegate.detachAndScrapAttachedViews(recycler);
    }

    @Override
    public void measureChild(@NonNull View child, int widthUsed, int heightUsed) {
        mDelegate.measureChild(child, widthUsed, heightUsed);
    }

    @Override
    public boolean isMeasurementCacheEnabled() {
        return mDelegate.isMeasurementCacheEnabled();
    }

    @Override
    public void setMeasurementCacheEnabled(boolean measurementCacheEnabled) {
        mDelegate.setMeasurementCacheEnabled(measurementCacheEnabled);
    }

    @Override
    public void measureChildWithMargins(@NonNull View child, int widthUsed, int heightUsed) {
        mDelegate.measureChildWithMargins(child, widthUsed, heightUsed);
    }

    @Override
    public int getDecoratedMeasuredWidth(@NonNull View child) {
        return mDelegate.getDecoratedMeasuredWidth(child);
    }

    @Override
    public int getDecoratedMeasuredHeight(@NonNull View child) {
        return mDelegate.getDecoratedMeasuredHeight(child);
    }

    @Override
    public void layoutDecorated(@NonNull View child, int left, int top, int right, int bottom) {
        mDelegate.layoutDecorated(child, left, top, right, bottom);
    }

    @Override
    public void layoutDecoratedWithMargins(@NonNull View child, int left, int top, int right, int bottom) {
        mDelegate.layoutDecoratedWithMargins(child, left, top, right, bottom);
    }

    @Override
    public void getTransformedBoundingBox(@NonNull View child, boolean includeDecorInsets, @NonNull Rect out) {
        mDelegate.getTransformedBoundingBox(child, includeDecorInsets, out);
    }

    @Override
    public void getDecoratedBoundsWithMargins(@NonNull View view, @NonNull Rect outBounds) {
        mDelegate.getDecoratedBoundsWithMargins(view, outBounds);
    }

    @Override
    public int getDecoratedLeft(@NonNull View child) {
        return mDelegate.getDecoratedLeft(child);
    }

    @Override
    public int getDecoratedTop(@NonNull View child) {
        return mDelegate.getDecoratedTop(child);
    }

    @Override
    public int getDecoratedRight(@NonNull View child) {
        return mDelegate.getDecoratedRight(child);
    }

    @Override
    public int getDecoratedBottom(@NonNull View child) {
        return mDelegate.getDecoratedBottom(child);
    }

    @Override
    public void calculateItemDecorationsForChild(@NonNull View child, @NonNull Rect outRect) {
        mDelegate.calculateItemDecorationsForChild(child, outRect);
    }

    @Override
    public int getTopDecorationHeight(@NonNull View child) {
        return mDelegate.getTopDecorationHeight(child);
    }

    @Override
    public int getBottomDecorationHeight(@NonNull View child) {
        return mDelegate.getBottomDecorationHeight(child);
    }

    @Override
    public int getLeftDecorationWidth(@NonNull View child) {
        return mDelegate.getLeftDecorationWidth(child);
    }

    @Override
    public int getRightDecorationWidth(@NonNull View child) {
        return mDelegate.getRightDecorationWidth(child);
    }

    @Nullable
    @Override
    public View onFocusSearchFailed(@NonNull View focused, int direction, @NonNull RecyclerView.Recycler recycler, @NonNull RecyclerView.State state) {
        return mDelegate.onFocusSearchFailed(focused, direction, recycler, state);
    }

    @Nullable
    @Override
    public View onInterceptFocusSearch(@NonNull View focused, int direction) {
        return mDelegate.onInterceptFocusSearch(focused, direction);
    }

    @Override
    public boolean requestChildRectangleOnScreen(@NonNull RecyclerView parent, @NonNull View child, @NonNull Rect rect, boolean immediate) {
        return mDelegate.requestChildRectangleOnScreen(parent, child, rect, immediate);
    }

    @Override
    public boolean requestChildRectangleOnScreen(@NonNull RecyclerView parent, @NonNull View child, @NonNull Rect rect, boolean immediate, boolean focusedChildVisible) {
        return mDelegate.requestChildRectangleOnScreen(parent, child, rect, immediate, focusedChildVisible);
    }

    @Override
    public boolean isViewPartiallyVisible(@NonNull View child, boolean completelyVisible, boolean acceptEndPointInclusion) {
        return mDelegate.isViewPartiallyVisible(child, completelyVisible, acceptEndPointInclusion);
    }

    @Override
    public boolean onRequestChildFocus(@NonNull RecyclerView parent, @NonNull View child, @Nullable @org.jetbrains.annotations.Nullable View focused) {
        return mDelegate.onRequestChildFocus(parent, child, focused);
    }

    @Override
    public boolean onRequestChildFocus(@NonNull RecyclerView parent, @NonNull RecyclerView.State state, @NonNull View child, @Nullable @org.jetbrains.annotations.Nullable View focused) {
        return mDelegate.onRequestChildFocus(parent, state, child, focused);
    }

    @Override
    public void onAdapterChanged(@Nullable @org.jetbrains.annotations.Nullable RecyclerView.Adapter oldAdapter, @Nullable @org.jetbrains.annotations.Nullable RecyclerView.Adapter newAdapter) {
        mDelegate.onAdapterChanged(oldAdapter, newAdapter);
    }

    @Override
    public boolean onAddFocusables(@NonNull RecyclerView recyclerView, @NonNull ArrayList<View> views, int direction, int focusableMode) {
        return mDelegate.onAddFocusables(recyclerView, views, direction, focusableMode);
    }

    @Override
    public void onItemsChanged(@NonNull RecyclerView recyclerView) {
        mDelegate.onItemsChanged(recyclerView);
    }

    @Override
    public void onItemsAdded(@NonNull RecyclerView recyclerView, int positionStart, int itemCount) {
        mDelegate.onItemsAdded(recyclerView, positionStart, itemCount);
    }

    @Override
    public void onItemsRemoved(@NonNull RecyclerView recyclerView, int positionStart, int itemCount) {
        mDelegate.onItemsRemoved(recyclerView, positionStart, itemCount);
    }

    @Override
    public void onItemsUpdated(@NonNull RecyclerView recyclerView, int positionStart, int itemCount) {
        mDelegate.onItemsUpdated(recyclerView, positionStart, itemCount);
    }

    @Override
    public void onItemsUpdated(@NonNull RecyclerView recyclerView, int positionStart, int itemCount, @Nullable Object payload) {
        mDelegate.onItemsUpdated(recyclerView, positionStart, itemCount, payload);
    }

    @Override
    public void onItemsMoved(@NonNull RecyclerView recyclerView, int from, int to, int itemCount) {
        mDelegate.onItemsMoved(recyclerView, from, to, itemCount);
    }

    @Override
    public int computeHorizontalScrollExtent(@NonNull RecyclerView.State state) {
        return mDelegate.computeHorizontalScrollExtent(state);
    }

    @Override
    public int computeHorizontalScrollOffset(@NonNull RecyclerView.State state) {
        return mDelegate.computeHorizontalScrollOffset(state);
    }

    @Override
    public int computeHorizontalScrollRange(@NonNull RecyclerView.State state) {
        return mDelegate.computeHorizontalScrollRange(state);
    }

    @Override
    public int computeVerticalScrollExtent(@NonNull RecyclerView.State state) {
        return mDelegate.computeVerticalScrollExtent(state);
    }

    @Override
    public int computeVerticalScrollOffset(@NonNull RecyclerView.State state) {
        return mDelegate.computeVerticalScrollOffset(state);
    }

    @Override
    public int computeVerticalScrollRange(@NonNull RecyclerView.State state) {
        return mDelegate.computeVerticalScrollRange(state);
    }

    @Override
    public void onMeasure(@NonNull RecyclerView.Recycler recycler, @NonNull RecyclerView.State state, int widthSpec, int heightSpec) {
        mDelegate.onMeasure(recycler, state, widthSpec, heightSpec);
    }

    @Override
    public void setMeasuredDimension(int widthSize, int heightSize) {
        mDelegate.setMeasuredDimension(widthSize, heightSize);
    }

    @Override
    public int getMinimumWidth() {
        return mDelegate.getMinimumWidth();
    }

    @Override
    public int getMinimumHeight() {
        return mDelegate.getMinimumHeight();
    }

    @Nullable
    @Override
    public Parcelable onSaveInstanceState() {
        return mDelegate.onSaveInstanceState();
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        mDelegate.onRestoreInstanceState(state);
    }

    @Override
    public void onScrollStateChanged(int state) {
        mDelegate.onScrollStateChanged(state);
    }

    @Override
    public void removeAndRecycleAllViews(@NonNull RecyclerView.Recycler recycler) {
        mDelegate.removeAndRecycleAllViews(recycler);
    }

    @Override
    public void onInitializeAccessibilityNodeInfo(@NonNull RecyclerView.Recycler recycler, @NonNull RecyclerView.State state, @NonNull AccessibilityNodeInfoCompat info) {
        mDelegate.onInitializeAccessibilityNodeInfo(recycler, state, info);
    }

    @Override
    public void onInitializeAccessibilityEvent(@NonNull AccessibilityEvent event) {
        mDelegate.onInitializeAccessibilityEvent(event);
    }

    @Override
    public void onInitializeAccessibilityEvent(@NonNull RecyclerView.Recycler recycler, @NonNull RecyclerView.State state, @NonNull AccessibilityEvent event) {
        mDelegate.onInitializeAccessibilityEvent(recycler, state, event);
    }

    @Override
    public void onInitializeAccessibilityNodeInfoForItem(@NonNull RecyclerView.Recycler recycler, @NonNull RecyclerView.State state, @NonNull View host, @NonNull AccessibilityNodeInfoCompat info) {
        mDelegate.onInitializeAccessibilityNodeInfoForItem(recycler, state, host, info);
    }

    @Override
    public void requestSimpleAnimationsInNextLayout() {
        mDelegate.requestSimpleAnimationsInNextLayout();
    }

    @Override
    public int getSelectionModeForAccessibility(@NonNull RecyclerView.Recycler recycler, @NonNull RecyclerView.State state) {
        return mDelegate.getSelectionModeForAccessibility(recycler, state);
    }

    @Override
    public int getRowCountForAccessibility(@NonNull RecyclerView.Recycler recycler, @NonNull RecyclerView.State state) {
        return mDelegate.getRowCountForAccessibility(recycler, state);
    }

    @Override
    public int getColumnCountForAccessibility(@NonNull RecyclerView.Recycler recycler, @NonNull RecyclerView.State state) {
        return mDelegate.getColumnCountForAccessibility(recycler, state);
    }

    @Override
    public boolean isLayoutHierarchical(@NonNull RecyclerView.Recycler recycler, @NonNull RecyclerView.State state) {
        return mDelegate.isLayoutHierarchical(recycler, state);
    }

    @Override
    public boolean performAccessibilityAction(@NonNull RecyclerView.Recycler recycler, @NonNull RecyclerView.State state, int action, @Nullable Bundle args) {
        return mDelegate.performAccessibilityAction(recycler, state, action, args);
    }

    @Override
    public boolean performAccessibilityActionForItem(@NonNull RecyclerView.Recycler recycler, @NonNull RecyclerView.State state, @NonNull View view, int action, @Nullable Bundle args) {
        return mDelegate.performAccessibilityActionForItem(recycler, state, view, action, args);
    }

    @Override
    void setRecyclerView(RecyclerView recyclerView) {
        mDelegate.setRecyclerView(recyclerView);
    }

    @Override
    void setMeasureSpecs(int wSpec, int hSpec) {
        mDelegate.setMeasureSpecs(wSpec, hSpec);
    }

    @Override
    void setMeasuredDimensionFromChildren(int widthSpec, int heightSpec) {
        mDelegate.setMeasuredDimensionFromChildren(widthSpec, heightSpec);
    }

    @Override
    void dispatchAttachedToWindow(RecyclerView view) {
        mDelegate.dispatchAttachedToWindow(view);
    }

    @Override
    void dispatchDetachedFromWindow(RecyclerView view, RecyclerView.Recycler recycler) {
        mDelegate.dispatchDetachedFromWindow(view, recycler);
    }

    @Override
    void removeAndRecycleScrapInt(RecyclerView.Recycler recycler) {
        mDelegate.removeAndRecycleScrapInt(recycler);
    }

    @Override
    boolean shouldReMeasureChild(View child, int widthSpec, int heightSpec, RecyclerView.LayoutParams lp) {
        return mDelegate.shouldReMeasureChild(child, widthSpec, heightSpec, lp);
    }

    @Override
    boolean shouldMeasureChild(View child, int widthSpec, int heightSpec, RecyclerView.LayoutParams lp) {
        return mDelegate.shouldMeasureChild(child, widthSpec, heightSpec, lp);
    }

    @Override
    void stopSmoothScroller() {
        mDelegate.stopSmoothScroller();
    }

    @Override
    void onSmoothScrollerStopped(RecyclerView.SmoothScroller smoothScroller) {
        mDelegate.onSmoothScrollerStopped(smoothScroller);
    }

    @Override
    void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfoCompat info) {
        mDelegate.onInitializeAccessibilityNodeInfo(info);
    }

    @Override
    void onInitializeAccessibilityNodeInfoForItem(View host, AccessibilityNodeInfoCompat info) {
        mDelegate.onInitializeAccessibilityNodeInfoForItem(host, info);
    }

    @Override
    boolean performAccessibilityAction(int action, @Nullable @org.jetbrains.annotations.Nullable Bundle args) {
        return mDelegate.performAccessibilityAction(action, args);
    }

    @Override
    boolean performAccessibilityActionForItem(@NonNull @io.reactivex.annotations.NonNull View view, int action, @Nullable @org.jetbrains.annotations.Nullable Bundle args) {
        return mDelegate.performAccessibilityActionForItem(view, action, args);
    }

    @Override
    void setExactMeasureSpecsFrom(RecyclerView recyclerView) {
        mDelegate.setExactMeasureSpecsFrom(recyclerView);
    }

    @Override
    boolean shouldMeasureTwice() {
        return mDelegate.shouldMeasureTwice();
    }

    @Override
    boolean hasFlexibleChildInBothOrientations() {
        return mDelegate.hasFlexibleChildInBothOrientations();
    }
}
