/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.settings.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import tv.threess.threeready.ui.databinding.SettingsTextItemBinding;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.settings.listener.ItemClickListener;
import tv.threess.threeready.ui.settings.model.SettingsItem;

/**
 * Settings list item presenter
 *
 * @author Daniel Gliga
 * @since 2017.10.30
 */

public class SettingsTextItemPresenter extends BasePresenter<SettingsTextItemPresenter.ViewHolder, SettingsItem> {

    private final ItemClickListener mItemClickListener;

    public SettingsTextItemPresenter(Context context, ItemClickListener itemClickListener) {
        super(context);
        mItemClickListener = itemClickListener;
    }

    @Override
    public SettingsTextItemPresenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new SettingsTextItemPresenter.ViewHolder(
                SettingsTextItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindHolder(ViewHolder holder, SettingsItem settingsItem) {
        holder.mBinding.title.setText(settingsItem.getTitle());
        holder.mBinding.subtitle.setText(settingsItem.getSubtitle());
    }

    public static class ViewHolder extends BasePresenter.ViewHolder {

        SettingsTextItemBinding mBinding;

        public ViewHolder(SettingsTextItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }

    @Override
    public void onClicked(ViewHolder holder, SettingsItem settingsItem) {
        mItemClickListener.onItemClick(settingsItem);
    }
}
