package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.DrawableRes;

import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.pvr.model.SeriesRecordingStatus;
import tv.threess.threeready.api.pvr.model.SingleRecordingStatus;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Container for the Broadcast related icons.
 *
 * @author Bondor David
 * @since 06/17/2019
 */
public class BroadcastOrderedIconsContainer extends BaseOrderedIconsContainer {
    private RecordingStatus mRecordingStatus;
    private SeriesRecordingStatus mSeriesRecordingStatus;
    private boolean mIsSingleRecordingStatus;

    public BroadcastOrderedIconsContainer(Context context) {
        this(context, null, 0);
    }

    public BroadcastOrderedIconsContainer(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BroadcastOrderedIconsContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Displays the recording status, given a recordingStatus parameter and a flag that states if the
     * recording status is singleRecordingStatus in order to inflate the proper recording icon
     *
     * @param recordingStatus - the recording status to display
     * @param isSingle        - states if the recording status is singleRecordingStatus
     */
    public void showRecordingStatus(RecordingStatus recordingStatus, boolean isSingle) {
        if (this.mRecordingStatus == recordingStatus
                && mIsSingleRecordingStatus == isSingle
                && isIconDisplayed(Icons.RECORDING_STATUS)) {
            return;
        }

        mIsSingleRecordingStatus = isSingle;
        this.mRecordingStatus = recordingStatus;
        showIcon(Icons.RECORDING_STATUS, getDrawable(getRecordingStatusIcon(recordingStatus, isSingle)));
    }

    /**
     * Displays the recording status, given the series recording and the series recording status.
     *
     * @param series                The recording series for which the icon needs to be displayed.
     * @param seriesRecordingStatus The recording status of the series.
     */
    public void showRecordingStatus(IRecordingSeries series, SeriesRecordingStatus seriesRecordingStatus) {
        this.mSeriesRecordingStatus = seriesRecordingStatus;
        showIcon(Icons.RECORDING_STATUS, getDrawable(getSeriesRecordingIcon(series, seriesRecordingStatus)));
    }

    /**
     * Displays the given recording status.
     * @param recordingStatus The recording status of the program.
     */
    public void showRecordingStatus(RecordingStatus recordingStatus) {
        showRecordingStatus(recordingStatus, false);
    }

    /**
     * Displays the replay icon.
     */
    public void showReplay() {
        if (isIconDisplayed(Icons.REPLAY)) {
            return;
        }
        showIcon(Icons.REPLAY, getDrawable(R.drawable.ico_catchup));
    }

    /**
     * Hides replay icon
     */
    public void hideReplay() {
        hideIcon(Icons.REPLAY);
    }

    /**
     * Hides the recording status icon
     */
    public void hideRecordingStatus() {
        if (!isIconDisplayed(Icons.RECORDING_STATUS)) {
            return;
        }

        mRecordingStatus = null;
        mIsSingleRecordingStatus = false;
        hideIcon(Icons.RECORDING_STATUS);
    }
    /**
     * @return - the status of the recording
     */
    public RecordingStatus getRecordingStatus() {
        return mRecordingStatus;
    }

    @Override
    public void hideAll() {
        super.hideAll();
        mRecordingStatus = null;
        mIsSingleRecordingStatus = false;
    }

    @Override
    protected String getIconDescription(Translator translator, Icons icons) {
        if (icons == Icons.RECORDING_STATUS) {
            if (SeriesRecordingStatus.SCHEDULED == mSeriesRecordingStatus) {
                return translator.get(TranslationKey.TALKBACK_ICON_RECORDING_PLANNED);
            }

            if (mRecordingStatus == null) {
                return "";
            }

            switch (mRecordingStatus.getSingleRecordingStatus()) {
                case COMPLETED:
                    return translator.get(TranslationKey.TALKBACK_ICON_RECORDING_COMPLETED);
                case RECORDING:
                    return translator.get(TranslationKey.TALKBACK_ICON_RECORDING_ONGOING);
                case SCHEDULED:
                    return translator.get(TranslationKey.TALKBACK_ICON_RECORDING_PLANNED);
            }
        }

        return super.getIconDescription(translator, icons);
    }

    /**
     * Get the recording icon for a series based on the current status.
     * @param status The current series recording status of the series.
     * @param series The series for which the status should be displayed.
     */
    @DrawableRes
    public int getSeriesRecordingIcon(IRecordingSeries series, SeriesRecordingStatus status) {
        if (series == null || status == null) {
            return 0;
        }

        boolean scheduled = status == SeriesRecordingStatus.SCHEDULED;

        if (series.hasOnGoingEpisode()) {
            return R.drawable.ico_series_rec_recording;
        }
        if (scheduled) {
            return R.drawable.ico_series_rec_scheduled;
        }
        if (series.hasCompletedEpisode()) {
            return R.drawable.ico_series_rec_recorded;
        }
        if (series.hasScheduledEpisode()) {
            return R.drawable.ico_series_rec_scheduled;
        }

        return 0;
    }

    /**
     * Get the recording icon according to the status.
     * @param recordingStatus The current status of the recording.
     * @param onlySingle Show only the single recording status even if the series is scheduled.
     */
    @DrawableRes
    private int getRecordingStatusIcon(RecordingStatus recordingStatus, boolean onlySingle) {
        if (recordingStatus == null) {
            return 0;
        }

        boolean isSeriesRec = recordingStatus.getSeriesRecordingStatus() == SeriesRecordingStatus.SCHEDULED;
        SingleRecordingStatus singleRecordingStatus = recordingStatus.getSingleRecordingStatus();
        switch (singleRecordingStatus) {
            case COMPLETED:
                return isSeriesRec && !onlySingle ?
                        R.drawable.ico_series_rec_recorded : R.drawable.ico_rec_recorded;
            case RECORDING:
                return isSeriesRec && !onlySingle ?
                        (mRevertColorInFocusState ? R.drawable.epg_program_series_rec_icon_selector : R.drawable.ico_series_rec_recording) :
                        (mRevertColorInFocusState ? R.drawable.epg_program_rec_icon_selector : R.drawable.ico_rec_recording);
            case SCHEDULED:
                return isSeriesRec && !onlySingle ?
                        R.drawable.ico_series_rec_scheduled : R.drawable.ico_rec_scheduled;
        }

        return 0;
    }
}