/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.tv.presenter.broadcast;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.BaseA1LandscapeCardBinding;
import tv.threess.threeready.ui.databinding.BroadcastA1LandscapeCardBinding;
import tv.threess.threeready.ui.generic.view.BroadcastOrderedIconsContainer;
import tv.threess.threeready.ui.tv.view.ContentMarkersView;
import tv.threess.threeready.ui.tv.view.ProgressIndicatorView;

/**
 * Presenter class for broadcast type and A1 variant card. (landscape view)
 * <p>
 * Created by Szilard on 8/16/2017.
 */

public class BroadcastA1LandscapeCardPresenter extends BaseBroadcastLandscapeCardPresenter {
    private static final int MAX_RECYCLER_VIEW_COUNT = 20;

    public BroadcastA1LandscapeCardPresenter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        BroadcastA1LandscapeCardBinding binding = BroadcastA1LandscapeCardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        BaseA1LandscapeCardBinding baseBinding = BaseA1LandscapeCardBinding.bind(binding.getRoot());
        ViewHolder holder = new ViewHolder(binding, baseBinding);

        holder.mBaseBinding.label.setBackgroundColor(mLayoutConfig.getPlaceHolderColor());
        holder.mBaseBinding.title.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBaseBinding.details.setTextColor(mLayoutConfig.getPlaceholderTransparentFontColor());
        holder.mPlaceHolderDrawable.setColor(mLayoutConfig.getPlaceHolderColor());

        return holder;
    }

    @Override
    public int getMaxRecyclerViewCount() {
        return MAX_RECYCLER_VIEW_COUNT;
    }

    @Override
    public int getCardWidth(ModuleData<?> moduleData, IBroadcast broadcast) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.broadcast_a1_card_width_focused);
    }

    @Override
    public int getCardHeight(ModuleData<?> moduleData, IBroadcast broadcast) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.broadcast_a1_card_height_focused);
    }

    @Override
    protected int getCoverHeight(IBroadcast contentItem) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.broadcast_a1_landscape_cover_height);
    }

    @Override
    protected int getCoverWidth(IBroadcast contentItem) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.broadcast_a1_landscape_cover_width);
    }

    @Override
    protected int getItemAlignmentOffset() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.broadcast_a1_landscape_card_item_alignment_offset);
    }

    public static class ViewHolder extends BaseBroadcastLandscapeCardPresenter.ViewHolder {

        private final BroadcastA1LandscapeCardBinding mBinding;
        private final BaseA1LandscapeCardBinding mBaseBinding;

        public ViewHolder(BroadcastA1LandscapeCardBinding binding, BaseA1LandscapeCardBinding baseBinding) {
            super(binding.getRoot());
            mBinding = binding;
            mBaseBinding = baseBinding;
        }

        @Override
        @NotNull
        public TextView getDetailsView() {
            return mBaseBinding.details;
        }

        @Override
        @NotNull
        public TextView getTitleView() {
            return mBaseBinding.title;
        }

        @Override
        public TextView getInfoTextView() {
            return mBaseBinding.details;
        }

        @Override
        public ContentMarkersView getContentMarkerView() {
            return mBaseBinding.marker;
        }

        @Override
        public ImageView getCoverView() {
            return mBaseBinding.cardCover;
        }

        @Override
        public ImageView getChannelLogoView() {
            return mBaseBinding.providerLogo;
        }

        @Override
        public ImageView getGradientView() {
            return mBaseBinding.logoGradient;
        }

        @Override
        public ProgressIndicatorView getProgressIndicatorView() {
            return mBaseBinding.progressBar;
        }

        @Override
        public BroadcastOrderedIconsContainer getIconContainer() {
            return mBinding.iconContainer;
        }

        @Override
        public ColorDrawable getPlaceHolderDrawable() {
            return mPlaceHolderDrawable;
        }
    }
}
