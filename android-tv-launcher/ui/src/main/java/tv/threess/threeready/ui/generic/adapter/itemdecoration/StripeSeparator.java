package tv.threess.threeready.ui.generic.adapter.itemdecoration;

import android.graphics.drawable.Drawable;

/**
 *  Holds the label title and the start and end position for the area.
 *
 * @author Barabas Attila
 * @since 2018.08.02
 */
public class StripeSeparator {
    private final int mStartPosition;
    private final int mEndPosition;
    private final Drawable mSeparatorDrawable;

    public StripeSeparator(int startPosition, int endPosition, Drawable separatorDrawable) {
        mStartPosition = startPosition;
        mEndPosition = endPosition;
        mSeparatorDrawable = separatorDrawable;
    }

    public int getStartPosition() {
        return mStartPosition;
    }

    public int getEndPosition() {
        return mEndPosition;
    }

    public Drawable getSeparatorDrawable() {
        return mSeparatorDrawable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StripeSeparator label = (StripeSeparator) o;

        return mStartPosition == label.mStartPosition;
    }

    @Override
    public int hashCode() {
        return mStartPosition;
    }
}
