package tv.threess.threeready.ui.generic.presenter;

import android.view.View;

import androidx.annotation.DrawableRes;

import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.ui.generic.adapter.DetailPageButtonOrder;

/**
 * Model used for Action Buttons in @{@ActionButtonsPresenter}.
 *
 * @author Daniela Toma
 * @since 2020.07.31
 */
public class ActionModel {

    protected String mText;
    protected String mContentDescription;

    @DrawableRes
    int mRightDrawable;

    @DrawableRes
    int mLeftDrawable;

    DetailPageButtonOrder mDetailPageButtonOrder;

    private final OnItemClickListener mOnModelClickListener;
    private final View.OnFocusChangeListener mOnFocusChangeListener;

    public ActionModel(int leftDrawable, int rightDrawable, String text) {
        this(leftDrawable, rightDrawable, text, DetailPageButtonOrder.None, null);
    }

    public ActionModel(String text, DetailPageButtonOrder detailPageButtonOrder,
        OnItemClickListener onModelClickListener) {
        this(0, 0, text, detailPageButtonOrder, onModelClickListener, null);
    }

    public ActionModel(int leftDrawable, int rightDrawable, String text, DetailPageButtonOrder detailPageButtonOrder,
        OnItemClickListener onModelClickListener) {
        this(leftDrawable, rightDrawable, text, detailPageButtonOrder, onModelClickListener, null);
    }

    public ActionModel(int leftDrawable, int rightDrawable, String text, DetailPageButtonOrder sortButton,
        OnItemClickListener onModelClickListener, View.OnFocusChangeListener onFocusChangeListener) {
        mLeftDrawable = leftDrawable;
        mRightDrawable = rightDrawable;
        mText = text;
        mOnModelClickListener = onModelClickListener;
        mOnFocusChangeListener = onFocusChangeListener;
        mDetailPageButtonOrder = sortButton;
    }

    public DetailPageButtonOrder getDetailPageButtonOrder() {
        return mDetailPageButtonOrder;
    }

    public OnItemClickListener getOnModelClickListener() {
        return mOnModelClickListener;
    }

    View.OnFocusChangeListener getOnFocusChangeListener() {
        return mOnFocusChangeListener;
    }

    public String getText() {
        return mText;
    }

    public void setContentDescription(String contentDescription) {
        this.mContentDescription = contentDescription;
    }

    public String getContentDescription() {
        return StringUtils.notNull(mContentDescription);
    }

    public int getRightDrawable() {
        return mRightDrawable;
    }

    public int getLeftDrawable() {
        return mLeftDrawable;
    }

    /**
     * Interface to get notified when the user click on an item in the adapter.
     */
    public interface OnItemClickListener {

        void onClick();
    }
}