/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.assistent;

import android.app.Activity;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.os.Bundle;
import android.view.KeyEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.PlayerStateManager;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.player.view.ZappingInfoView;

/**
 * Allows the user to give playback commands via Google assistant
 *
 * @author Barabas Attila
 * @since 2019.16.01
 */
public class PlaybackAssistantController {
    private static final String TAG = Log.tag(PlaybackAssistantController.class);

    private final Activity mActivity;
    private MediaSession mMediaSession;
    private MediaSessionCallback mMediaSessionCallback;

    private final PlaybackDetailsManager mPlaybackDetailsManager;

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final ZappingInfoView mZappingInfoView = Components.get(ZappingInfoView.class);
    private final PlayerStateManager mPlayerStateManager = Components.get(PlayerStateManager.class);

    private IPlaybackCallback mPlaybackCallback;

    public PlaybackAssistantController(Activity activity, PlaybackDetailsManager manager) {
        mActivity = activity;
        mPlaybackDetailsManager = manager;
    }

    /**
     * Open a media session for the activity
     * and synchronize the playback state between the app and the media control.
     */
    public void createMediaSession() {
        mMediaSession = new MediaSession(mActivity, TAG);
        mMediaSessionCallback = new MediaSessionCallback();
        mMediaSession.setCallback(mMediaSessionCallback);

        mMediaSession.setActive(true);

        // Rebuild the available actions.
        PlaybackState.Builder stateBuilder = new PlaybackState.Builder();
        stateBuilder.setActions(mMediaSessionCallback.getAvailableActions());
        mMediaSession.setPlaybackState(stateBuilder.build());

        MediaController mediaController = new MediaController(mActivity, mMediaSession.getSessionToken());
        mediaController.registerCallback(new MediaController.Callback() {
            @Override
            public void onSessionEvent(@NonNull String event, @Nullable Bundle extras) {
                super.onSessionEvent(event, extras);
                Log.d(TAG, "onSession event: " + extras + " event: " + event);
            }
        });

        mActivity.setMediaController(mediaController);

        mPlaybackCallback = new PlaybackCallback();
        mPlaybackDetailsManager.registerCallback(mPlaybackCallback);
    }

    /**
     * Release the previously opened media session.
     */
    public void releaseSession() {
        if (mMediaSession != null) {
            mMediaSession.release();
        }

        mPlaybackDetailsManager.unregisterCallback(mPlaybackCallback);
    }

    private void updatePlaybackState(int state, long position) {
        PlaybackState.Builder stateBuilder = new PlaybackState.Builder()
                .setActions(mMediaSessionCallback.getAvailableActions());

        stateBuilder.setState(state, TimeUnit.SECONDS.toMillis(position), 1.0f);
        mMediaSession.setPlaybackState(stateBuilder.build());
    }

    /**
     * Playback callback to synchronize the playback state between the app and media controller.
     */
    private class PlaybackCallback implements IPlaybackCallback {
        @Override
        public void onCommandSuccess(PlaybackCommand cmd, PlaybackDetails details) {
            tv.threess.threeready.player.contract.PlaybackState state = details.getState();

            switch (state) {
                case Started:
                    updatePlaybackState(PlaybackState.STATE_PLAYING, details.getRelativePosition());
                    break;
                case Paused:
                    updatePlaybackState(PlaybackState.STATE_PAUSED, details.getRelativePosition());
                    break;
                case Inactive:
                case Blanked:
                case Released:
                    updatePlaybackState(PlaybackState.STATE_NONE, details.getRelativePosition());
                    break;
                case Stopped:
                    updatePlaybackState(PlaybackState.STATE_STOPPED, details.getRelativePosition());
                    break;
            }
        }
    }

    /**
     * Media session callback to receive playback commands from outside of the application.
     */
    private class MediaSessionCallback extends MediaSession.Callback {

        @Override
        public void onSkipToNext() {
            super.onSkipToNext();

            if (mNavigator.isInBackground()) {
                return;
            }
            if (mNavigator.isModalDialogVisible()) {
                // Ignore the onSkipToNext() if one blocking Dialog is shown.
                return;
            }

            Log.d(TAG, "MediaSessionCallback -> onSkipToNext");
            mZappingInfoView.zapToNextChannel(StartAction.BtnUp);
        }

        @Override
        public void onSkipToPrevious() {
            super.onSkipToPrevious();

            if (mNavigator.isInBackground()) {
                return;
            }
            if (mNavigator.isModalDialogVisible()) {
                // Ignore the onSkipToPrevious() if one blocking Dialog is shown.
                return;
            }

            Log.d(TAG, "MediaSessionCallback -> onSkipToPrevious");
            mZappingInfoView.zapToPreviousChannel(StartAction.BtnDown);
        }

        @Override
        public void onStop() {
            super.onStop();

            if (mNavigator.isInBackground()) {
                return;
            }
            if (mNavigator.isModalDialogVisible()) {
                // Ignore the onStop() if one blocking Dialog is shown.
                return;
            }

            Log.d(TAG, "MediaSessionCallback -> onStop");
            mNavigator.clearAllDialogsByKey(KeyEvent.KEYCODE_MEDIA_STOP);
            mPlayerStateManager.handleStopButton();
        }

        @Override
        public void onPlay() {
            super.onPlay();

            if (mNavigator.isInBackground()) {
                return;
            }
            if (mNavigator.isModalDialogVisible()) {
                // Ignore the onPlay() if one blocking Dialog is shown.
                return;
            }

            Log.d(TAG, "MediaSessionCallback -> onPlay");
            mPlaybackDetailsManager.resume();
        }

        @Override
        public void onPause() {
            super.onPause();

            if (mNavigator.isInBackground()) {
                return;
            }
            if (mNavigator.isModalDialogVisible()) {
                // Ignore the onPause() if one blocking Dialog is shown.
                return;
            }

            Log.d(TAG, "MediaSessionCallback -> onPause");
            mPlaybackDetailsManager.resumePause();
        }

        @Override
        public void onSeekTo(long pos) {
            super.onSeekTo(pos);

            if (mNavigator.isInBackground()) {
                return;
            }
            if (mNavigator.isModalDialogVisible()) {
                // Ignore the onSeekTo() if one blocking Dialog is shown.
                return;
            }

            Log.d(TAG, "jump : " + pos);

            if (mPlaybackDetailsManager.canSeek()) {
                StartAction startAction = pos >= 0 ? StartAction.Forward : StartAction.Rewind;
                mPlaybackDetailsManager.jumpByDuration(startAction, pos);
            }
        }

        private long getAvailableActions() {
            return PlaybackState.ACTION_PLAY_FROM_MEDIA_ID
                    | PlaybackState.ACTION_PLAY_FROM_SEARCH | PlaybackState.ACTION_STOP
                    | PlaybackState.ACTION_SKIP_TO_NEXT | PlaybackState.ACTION_SKIP_TO_PREVIOUS // Next/previous channel.
                    | PlaybackState.ACTION_PLAY_PAUSE | PlaybackState.ACTION_PLAY    // Enable play/pause commands
                    | PlaybackState.ACTION_PAUSE | PlaybackState.ACTION_SEEK_TO; // Enable jump and seek commands.

        }

    }
}
