package tv.threess.threeready.ui.generic.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.ButtonStyle;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Abstract base class to dialogs which has action buttons.
 * The dialogs which are inherited from this class has to provide the container for the buttons.
 * @see BaseButtonDialog#getButtonsContainer()
 *
 * @author Barabas Attila
 * @since 2018.09.26
 */
public abstract class BaseButtonDialog extends BaseDialogFragment {

    public static final String EXTRA_DIALOG_MODEL = "model";

    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    protected final ButtonStyle mButtonStyle = Components.get(ButtonStyle.class);
    protected final Translator mTranslator = Components.get(Translator.class);

    protected DialogListener mListener;

    protected DialogModel mModel;

    public void setDialogListener(DialogListener listener) {
        super.setDialogListener(listener);
        mListener = listener;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);

        // Read dialog model immediately.
        mModel = (DialogModel) args.getSerializable(EXTRA_DIALOG_MODEL);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mModel = getSerializableArgument(EXTRA_DIALOG_MODEL);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Add action buttons.
        for (int i = 0; i < mModel.getButtons().size(); ++i) {
            addButton(mModel.getButtons().get(i), i == 0);
        }
    }

    protected abstract ViewGroup getButtonsContainer();

    /**
     * Create and add a button view to the container based on the model.
     */
    protected void addButton(ButtonModel button, boolean isFirst) {
        FontTextView buttonTextView = (FontTextView) LayoutInflater.from(getContext()).inflate(R.layout.dialog_button_item, getButtonsContainer(), false);
        buttonTextView.setBackground(UiUtils.createButtonBackground(getActivity(), mLayoutConfig, mButtonStyle));
        buttonTextView.setTextColor(UiUtils.createButtonBrandingColorStateList(mLayoutConfig));
        buttonTextView.setText(button.getText());
        buttonTextView.setTag(button.getText());
        buttonTextView.setOnClickListener(v -> {
            // Dispatch to button click listener.
            if (button.getClickListener() != null) {
                button.getClickListener().onButtonClick(BaseButtonDialog.this);
            }

            // Dispatch to global click listener.
            if (mListener != null) {
                mListener.onButtonClick(BaseButtonDialog.this);
            }

            if (mModel.dismissOnBtnClick()) {
                // if mDismissOnBtnClick is set to true, close dialog if any button was clicked
                dismiss();
            }
        });

        addContentDescription(buttonTextView, button, isFirst);
        getButtonsContainer().addView(buttonTextView);
    }

    private void addContentDescription(FontTextView buttonTextView, ButtonModel button, boolean isFirst) {
        TalkBackMessageBuilder description = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_BUTTON_OTHER));
        if (isFirst) {
            description = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_BUTTON_DIALOG))
                    .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_DIALOG_BODY, mModel.getDescription());
        }

        buttonTextView.setContentDescription(description.replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BUTTON_NAME, button.getText()).toString());
    }


    @Override
    public boolean isCancelable() {
        if (mModel != null) {
            return mModel.isCancelable();
        }
        return super.isCancelable();
    }

    @Override
    public boolean blockKeys() {
        if (mModel != null) {
            return mModel.blockKeys();
        }
        return super.blockKeys();
    }

    @Override
    public int getPriority() {
        if (mModel != null) {
            return mModel.getPriority();
        }
        return super.getPriority();
    }

    /**
     * Model class which holds all the necessary information to display the button dialog.
     */
    protected static class DialogModel implements Serializable {
        String mTitle;
        String mDescription;
        String mSecondaryDesc;

        boolean mIsCancelable = true;
        int mPriority = DEFAULT_PRIORITY;
        boolean mBlockKeys = false;
        boolean mGradient = false;

        List<ButtonModel> mButtons = new ArrayList<>();
        boolean mDismissOnBtnClick = false;

        public List<ButtonModel> getButtons() {
            return mButtons;
        }

        public boolean dismissOnBtnClick() {
            return mDismissOnBtnClick;
        }

        public String getTitle() {
            return mTitle;
        }

        public String getDescription() {
            return mDescription;
        }

        public String getSecondaryDesc() {
            return mSecondaryDesc;
        }

        public boolean isCancelable() {
            return mIsCancelable;
        }

        public int getPriority() {
            return mPriority;
        }

        public boolean blockKeys() {
            return mBlockKeys;
        }

        public boolean shouldShowGradient() {
            return mGradient;
        }
    }

    /**
     * Model class which holds all the necessary information to display  a button in dialog.
     */
    protected static class ButtonModel implements Serializable {
        //TODO: is this really needed?
        private final String mText;
        private transient ButtonClickListener mClickListener;

        public ButtonModel(String text) {
            mText = text;
        }

        public ButtonModel(String text, ButtonClickListener clickListener) {
            mText = text;
            mClickListener = clickListener;
        }


        public String getText() {
            return mText;
        }

        public ButtonClickListener getClickListener() {
            return mClickListener;
        }
    }

    public interface DialogListener extends BaseDialogFragment.DialogListener {
        default void onButtonClick(BaseButtonDialog dialog) {}
    }

    public interface ButtonClickListener {
        void onButtonClick(BaseButtonDialog dialog);
    }

}
