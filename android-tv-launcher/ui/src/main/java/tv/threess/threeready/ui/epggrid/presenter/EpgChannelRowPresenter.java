package tv.threess.threeready.ui.epggrid.presenter;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.leanback.widget.ItemAlignmentFacet;
import androidx.leanback.widget.Presenter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.EpgGridChannelLayoutBinding;
import tv.threess.threeready.ui.epggrid.EpgChannelRowModel;
import tv.threess.threeready.ui.epggrid.listener.EpgBroadcastSelectedListener;
import tv.threess.threeready.ui.epggrid.view.EpgTimeLineView;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;

/**
 * Presenter which displays the channel and the program grid
 *
 * @author Andor Lukacs
 * @since 18/06/2021
 */
public class EpgChannelRowPresenter extends BasePresenter<EpgChannelRowPresenter.ViewHolder, EpgChannelRowModel> {
    private static final String TAG = Log.tag(EpgChannelRowPresenter.class);

    private static final String SPACE = " ";

    private final EpgBroadcastSelectedListener mBroadcastSelectedListener;
    private final EpgTimeLineView mTimeline;

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    private final SpannableString mChannelFavouriteIcon;

    public EpgChannelRowPresenter(Context context, EpgTimeLineView timeLine,
                                  EpgBroadcastSelectedListener broadcastSelectedListener) {
        super(context);
        mBroadcastSelectedListener = broadcastSelectedListener;
        mTimeline = timeLine;
        mChannelFavouriteIcon = new SpannableString(SPACE);
        mChannelFavouriteIcon.setSpan(new ImageSpan(mContext, R.drawable.ico_favourite_padding),
                0, 1, SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent, RecyclerView.RecycledViewPool recycledViewPool) {
        EpgGridChannelLayoutBinding binding = EpgGridChannelLayoutBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);

        holder.mAdapter.setHasStableIds(true);
        holder.mBinding.programGrid.setHasFixedSize(true);
        holder.mBinding.programGrid.setItemAnimator(null);  // Disable item animations
        holder.mBinding.programGrid.setTimeline(mTimeline);

        holder.mItemBridgeAdapter = new ItemBridgeAdapter(holder.mAdapter,
                new EpgProgramPresenter(mContext, mTimeline, mBroadcastSelectedListener), recycledViewPool);

        holder.mBinding.channelNumber.setTextColor(mLayoutConfig.getFontColor());

        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, EpgChannelRowModel rowModel, List<?> payLoads) {
        holder.mAdapter.replaceAll(rowModel.programs);
        holder.mBinding.programGrid.resetScroll();
    }

    @Override
    public void onBindHolder(ViewHolder holder, EpgChannelRowModel rowModel) {
        Log.d(TAG, "onBindHolder");
        TvChannel channel = rowModel.channel;

        updateChannelNumber(holder, rowModel);

        Glide.with(mContext)
                .load(channel)
                .override(
                        mContext.getResources().getDimensionPixelOffset(R.dimen.channel_icon_width),
                        mContext.getResources().getDimensionPixelOffset(R.dimen.epg_row_height))
                .fitCenter()
                .into(holder.mBinding.channelLogo);

        holder.mAdapter.replaceAll(rowModel.programs);
        holder.mBinding.programGrid.setAdapter(holder.mItemBridgeAdapter);
        holder.mBinding.programGrid.resetScroll();

        updateRowAlignment(holder);
    }

    /**
     * Align the focus on the third row while scrolling.
     * And on the top when the first item is reached.
     */
    private void updateRowAlignment(ViewHolder holder) {
        int position = (Integer) holder.view.getTag(R.integer.normalized_adapter_position);

        ItemAlignmentFacet facet = new ItemAlignmentFacet();
        ItemAlignmentFacet.ItemAlignmentDef alignmentDef = new ItemAlignmentFacet.ItemAlignmentDef();
        facet.setAlignmentDefs(new ItemAlignmentFacet.ItemAlignmentDef[] {alignmentDef});

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) holder.view.getLayoutParams();
        int rowHeight = layoutParams.height + layoutParams.topMargin + layoutParams.bottomMargin;

        if (position == 0) {
            alignmentDef.setItemAlignmentOffset(0);
        } else if (position == 1) {
            alignmentDef.setItemAlignmentOffset(-rowHeight);
        } else {
            alignmentDef.setItemAlignmentOffset(-2 * rowHeight);
        }

        alignmentDef.setItemAlignmentOffsetPercent(ItemAlignmentFacet.ITEM_ALIGN_OFFSET_PERCENT_DISABLED);
        holder.setFacet(ItemAlignmentFacet.class, facet);
    }

    /**
     * Set the channel number or set the favourite icon for the text view if the channel is favourite.
     */
    private void updateChannelNumber(ViewHolder holder, EpgChannelRowModel rowModel) {
        if (rowModel.isInFavouriteChannelList) {
            holder.mBinding.channelNumber.setText(mChannelFavouriteIcon);
            return;
        }

        holder.mBinding.channelNumber.setText(String.valueOf(rowModel.channel.getNumber()));
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);
        holder.mBinding.programGrid.setAdapter(null);
        Glide.with(mContext).clear(holder.mBinding.channelLogo);
    }

    @Override
    public long getStableId(EpgChannelRowModel rowModel) {
        return Objects.hash(rowModel.channel.getId());
    }

    public static class ViewHolder extends Presenter.ViewHolder {

        private final EpgGridChannelLayoutBinding mBinding;
        private final ArrayObjectAdapter<IBroadcast> mAdapter = new ArrayObjectAdapter<>();
        private ItemBridgeAdapter mItemBridgeAdapter;

        public ViewHolder(EpgGridChannelLayoutBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
