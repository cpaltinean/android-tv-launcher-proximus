package tv.threess.threeready.ui.home.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.HomeRepository;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Modular page for dynamic VOD category browsing.
 * The page config is generated based on the VOD category structure.
 *
 * @author Barabas Attila
 * @since 2019.09.05
 */
public class CategoryPageFragment extends ModularPageFragment {
    private static final String TAG = Log.tag(CategoryPageFragment.class);

    private static final String EXTRA_CATEGORY_ID = "categoryId";
    private static final String EXTRA_EMPTY_PAGE_MESSAGE = "emptyPageMessage";

    private final HomeRepository mHomeRepository = Components.get(HomeRepository.class);

    private Disposable mSubCategoryDisposable;

    private String mCategoryId;
    private String mEmptyPageMessage;

    public static CategoryPageFragment newInstance(String categoryId, String emptyPageMessage) {
        Bundle args = new Bundle();
        args.putString(EXTRA_CATEGORY_ID, categoryId);
        args.putString(EXTRA_EMPTY_PAGE_MESSAGE, emptyPageMessage);

        CategoryPageFragment fragment = new CategoryPageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCategoryId = getStringArgument(EXTRA_CATEGORY_ID);
        mEmptyPageMessage = getStringArgument(EXTRA_EMPTY_PAGE_MESSAGE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        RxUtils.disposeSilently(mSubCategoryDisposable);
    }

    /**
     * Load dynamic page config for vod category.
     */
    @Override
    protected void loadPage() {
        Log.d(TAG, "Load subcategory page." + mCategoryId);
        clearPageContent();
        RxUtils.disposeSilently(mSubCategoryDisposable);
        mSubCategoryDisposable = mHomeRepository.getSubCategoryPage(mCategoryId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::displayPage, throwable ->
                {
                    Log.e(TAG, "Couldn't load sub-category page", throwable);
                    showEmptyPageMessage(null);
                });
    }

    @Override
    @Nullable
    protected String getEmptyPageText() {
        return mEmptyPageMessage;
    }

}
