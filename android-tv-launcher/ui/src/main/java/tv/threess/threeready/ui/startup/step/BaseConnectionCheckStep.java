package tv.threess.threeready.ui.startup.step;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.receivers.ConnectivityStateChangeReceiver;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;

/**
 * Base step to wait for internet and backend connectivity during startup.
 *
 * @author Barabas Attila
 * @since 4/26/21
 */
public abstract class BaseConnectionCheckStep implements Step {
    private static final String TAG = Log.tag(BaseConnectionCheckStep.class);

    protected final InternetChecker mInternetChecker = Components.get(InternetChecker.class);

    private StepCallback mStepCallback;

    @Override
    public void execute(StepCallback callback) {
        Log.d(TAG, "Checking internet.");

        mStepCallback = callback;

        long outStandbyTime = Settings.outOfStandbyTime.get(0L);
        long intoStandbyTime = Settings.intoStandbyTime.get(0L);

        // Already checked and available.
        if (isConnectionAvailable()
                && outStandbyTime > intoStandbyTime
                && mInternetChecker.getLastCheckedTime() > outStandbyTime) {
            mStepCallback.onFinished();
            return;
        }

        // Trigger check and wait for connection.
        mInternetChecker.addStateChangedListener(mOnInternetStateChangedListener);
        mInternetChecker.triggerInternetCheck();
    }

    /**
     * @return True when connectivity is available and the step can finish.
     */
    protected abstract boolean isConnectionAvailable();

    @Override
    public void cancel() {
        mInternetChecker.removeStateChangedListener(mOnInternetStateChangedListener);
    }

    private final InternetChecker.OnStateChangedListener mOnInternetStateChangedListener =
            new ConnectivityStateChangeReceiver.OnStateChangedListener() {
                @Override
                public void onCheckFinished(State state) {
                    Log.d(TAG, "Internet state check finished, current state: " + state);
                    if (mStepCallback != null && isConnectionAvailable()) {
                        mStepCallback.onFinished();
                    }
                }

                @Override
                public void onStateChanged(State state) {
                }
            };
}