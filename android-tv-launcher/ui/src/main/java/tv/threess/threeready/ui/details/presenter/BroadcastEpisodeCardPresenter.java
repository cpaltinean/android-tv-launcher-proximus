/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.home.model.generic.ContentMarkers;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.pvr.model.SingleRecordingStatus;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.databinding.BroadcastEpisodeCardBinding;
import tv.threess.threeready.ui.details.DetailOpenedFrom;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.view.BaseOrderedIconsContainer;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.pvr.RecordingActionHelper;
import tv.threess.threeready.ui.tv.view.ContentMarkersView;

/**
 * Broadcast episode card presenter on the series details page.
 *
 * @author Barabas Attila
 * @since 2018.11.22
 */
public class BroadcastEpisodeCardPresenter<TBroadcast extends IBroadcast>
        extends BaseEpisodeCardPresenter<BroadcastEpisodeCardPresenter.ViewHolder, TBroadcast> {
    private static final String TAG = Log.tag(BroadcastEpisodeCardPresenter.class);

    protected final TvRepository mTvRepository = Components.get(TvRepository.class);
    protected final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);

    private final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);
    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);
    private final RecordingActionHelper mActionHelper = Components.get(RecordingActionHelper.class);

    protected final DetailOpenedFrom mOpenedFrom;

    public BroadcastEpisodeCardPresenter(Context context, DetailOpenedFrom openedFrom) {
        super(context);
        mOpenedFrom = openedFrom;
    }

    @Override
    public BaseEpisodeCardPresenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        BroadcastEpisodeCardBinding binding = BroadcastEpisodeCardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.title.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBinding.availability.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBinding.details.setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f));
        holder.mPlaceHolderDrawable.setColor(mLayoutConfig.getPlaceHolderColor());
        holder.mBinding.seriesInfo.setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f));
        holder.mBinding.description.setTextColor(mLayoutConfig.getPlaceholderFontColor());

        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, TBroadcast broadcast) {
        super.onBindHolder(holder, broadcast);

        // Update details.
        updateTitle(holder, broadcast);
        holder.mBinding.details.setText(LocaleTimeUtils.getDate(broadcast, mTranslator, mLocaleSettings));

        // Update content markers.
        updateMarkers(holder.mBinding.contentMarker, broadcast, holder.view.isFocused());

        // Update indicator icons.
        updateReplayIcon(holder, broadcast);
        updateParentalControlIcon(holder, broadcast);
        updateRecordingStatus(holder, broadcast);
        updateDescriptiveAudioIcon(holder, broadcast);
        updateDescriptiveSubtitleIcon(holder, broadcast);
        getBroadcastBookmark(holder, broadcast);
    }

    @Override
    public void onFocusedState(ViewHolder holder, TBroadcast contentItem) {
        super.onFocusedState(holder, contentItem);
        updateProgressIndicator(holder);
    }

    @Override
    public void onDefaultState(ViewHolder holder, TBroadcast tBroadcast) {
        super.onDefaultState(holder, tBroadcast);
        updateProgressIndicator(holder);
    }

    private void getBroadcastBookmark(ViewHolder holder, TBroadcast broadcast) {
        RxUtils.disposeSilently(holder.mBookmarkDisposable);
        holder.mBookmarkDisposable = mTvRepository.getBookmarkForBroadcast(
                broadcast, mOpenedFrom == DetailOpenedFrom.ContinueWatching)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        bookmark -> {
                            if (bookmark != null) {
                                holder.mBookmark = bookmark;
                                updateProgressIndicator(holder);
                            }
                        },
                        throwable -> Log.e(TAG, "Couldn't get the recording's bookmark.", throwable));
    }

    /**
     * Display progress bar.
     */
    private void updateProgressIndicator(ViewHolder holder) {
        if (holder.mBookmark != null && holder.mBookmark.getPosition() > 0) {
            holder.mBinding.progressBar.setProgressData(holder.mBookmark);
            holder.mBinding.progressBar.setVisibility(View.VISIBLE);
        } else {
            holder.mBinding.progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Invalidate the displayed content markers.
     */
    protected void updateMarkers(ContentMarkersView textView, TBroadcast broadcast, boolean isFocused) {
        textView.showMarkers(broadcast, ContentMarkers.TypeFilter.Cards);
    }

    private void updateReplayIcon(ViewHolder holder, TBroadcast broadcast) {
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        if (broadcast.canReplay(channel)) {
            holder.mBinding.iconsContainer.showReplay();
        } else {
            holder.mBinding.iconsContainer.hideReplay();
        }
    }

    private void updateAvailability(ViewHolder holder, TBroadcast broadcast, RecordingStatus recordingStatus) {
        String text = "";
        SingleRecordingStatus singleRecordingStatus = recordingStatus != null ? recordingStatus.getSingleRecordingStatus() : null;
        if (singleRecordingStatus != null && singleRecordingStatus != SingleRecordingStatus.SCHEDULED) {
            TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
            text = LocaleTimeUtils.getBroadcastAvailabilityTime(channel, broadcast, recordingStatus.getRecording(), mTranslator);
        }

        holder.mBinding.availability.setText(text);
    }

    private void updateRecordingStatus(ViewHolder holder, TBroadcast broadcast) {
        RxUtils.disposeSilently(holder.mRecStatusDisposable);
        holder.mRecStatusDisposable = mPvrRepository.getRecordingStatus(broadcast)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recordingStatus -> {
                    holder.mBinding.iconsContainer.showRecordingStatus(recordingStatus, true);
                    updateAvailability(holder, broadcast, recordingStatus);
                }, throwable -> {
                    Log.e(TAG, "Couldn't get the recording status.", throwable);
                    holder.mBinding.iconsContainer.hideRecordingStatus();
                });
    }

    @Override
    public boolean onKeyEvent(ViewHolder holder, TBroadcast broadcast, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN
                && event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_RECORD) {
            mActionHelper.handleRecKeyAction(broadcast,
                    holder.mBinding.iconsContainer.getRecordingStatus(), true);
            return true;
        }

        return super.onKeyEvent(holder, broadcast, event);
    }

    @Override
    public void onClicked(ViewHolder holder, TBroadcast broadcast) {
        super.onClicked(holder, broadcast);
        mNavigator.showBroadcastEpisodeDetailsDialog(broadcast,
                holder.mBookmark, holder.mBinding.iconsContainer.getRecordingStatus(), mOpenedFrom);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);

        holder.mBinding.iconsContainer.hideAll();
        holder.mBinding.availability.setText("");

        RxUtils.disposeSilently(holder.mRecStatusDisposable, holder.mBookmarkDisposable);
    }

    protected static class ViewHolder extends BaseEpisodeCardPresenter.ViewHolder {

        protected final BroadcastEpisodeCardBinding mBinding;
        IBookmark mBookmark;

        Disposable mBookmarkDisposable;

        Disposable mRecStatusDisposable;

        public ViewHolder(BroadcastEpisodeCardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        @NonNull
        @Override
        protected BaseOrderedIconsContainer getIconsContainer() {
            return mBinding.iconsContainer;
        }

        @NonNull
        @Override
        protected ImageView getCoverView() {
            return mBinding.cover;
        }

        @NonNull
        @Override
        protected FontTextView getTitleView() {
            return mBinding.title;
        }

        @NonNull
        @Override
        protected ContentMarkersView getContentMarkersView() {
            return mBinding.contentMarker;
        }

        @NonNull
        @Override
        protected FontTextView getSeriesInfoView() {
            return mBinding.seriesInfo;
        }

        @NonNull
        @Override
        protected FontTextView getDescriptionView() {
            return mBinding.description;
        }
    }
}
