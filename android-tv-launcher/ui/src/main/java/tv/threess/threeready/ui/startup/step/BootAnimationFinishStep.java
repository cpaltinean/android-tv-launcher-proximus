package tv.threess.threeready.ui.startup.step;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.MemoryCache;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * needed in case for notify technicolor that boot animation can be finished
 *
 * @author hunormadaras
 * @since 12/04/2019
 */
public class BootAnimationFinishStep implements Step {
    private static final String TAG = Log.tag(BootAnimationFinishStep.class);

    private final MwRepository mMwRepository = Components.get(MwRepository.class);

    private Disposable mDisposable;

    @Override
    public void execute(StepCallback callback) {
        RxUtils.disposeSilently(mDisposable);

        if (!MemoryCache.StartedAfterBoot.get(false)) {
            // Not rebooted. Continue.
            callback.onFinished();
            return;
        }

        mDisposable = mMwRepository.bootCompletedAction()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::onFinished, ex -> {
                    Log.d(TAG, "Failed to finish boot animation.", ex);
                    callback.onFinished();
                });
    }

    @Override
    public void cancel() {
        RxUtils.disposeSilently(mDisposable);
    }

}
