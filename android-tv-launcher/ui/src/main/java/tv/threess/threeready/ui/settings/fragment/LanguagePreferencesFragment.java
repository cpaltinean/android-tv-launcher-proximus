package tv.threess.threeready.ui.settings.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.settings.model.ISettingsItem;
import tv.threess.threeready.ui.settings.model.SettingsItem;

/**
 * Settings dialog showing language preferences options
 *
 * @author Denisa Trif
 * @since 22.04.2019
 */
public class LanguagePreferencesFragment extends BaseSettingsDialogFragment<SettingsItem> {
    private static final String TAG = Log.tag(LanguagePreferencesFragment.class);

    private final Navigator mNavigator = Components.get(Navigator.class);

    public static LanguagePreferencesFragment newInstance() {
        return new LanguagePreferencesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null) {
            view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.settings_left_frame_color));
        }
        return view;
    }

    @Override
    protected void setTitle() {
        getTitleView().setText(mTranslator.get(TranslationKey.SETTINGS_LANGUAGE));
    }

    @Override
    protected void initView() {
        List<SettingsItem> items = new ArrayList<>();

        SettingsItem languageAudioSubtitlesItem = new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_LANGUAGE_AUDIO_SUBTITLES), null);
        languageAudioSubtitlesItem.setPreferenceGroup(TranslationKey.SETTINGS_LANGUAGE_AUDIO_SUBTITLES);
        SettingsItem uiLanguageItem = new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_LANGUAGE_UI_LANGUAGE), null);
        uiLanguageItem.setPreferenceGroup(TranslationKey.SETTINGS_LANGUAGE_UI_LANGUAGE);
        SettingsItem shopLanguageItem = new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_LANGUAGE_SHOP_LANGUAGE), null);
        shopLanguageItem.setPreferenceGroup(TranslationKey.SETTINGS_LANGUAGE_SHOP_LANGUAGE);

        items.add(languageAudioSubtitlesItem);
        items.add(uiLanguageItem);
        items.add(shopLanguageItem);

        setItems(items);
    }

    @Override
    public void onItemClick(ISettingsItem clickedItem) {
        super.onItemClick(clickedItem);
        switch (clickedItem.getPreferenceGroup()) {
            case TranslationKey.SETTINGS_LANGUAGE_AUDIO_SUBTITLES:
                Log.d(TAG, "onItemClick: open Audio & Subtitle Preferences");
                mNavigator.showAudioAndSubtitlesPreferences();
                break;
            case TranslationKey.SETTINGS_LANGUAGE_UI_LANGUAGE:
                Log.d(TAG, "onItemClick: open UI language preferences");
                mNavigator.showUILanguagePreferences();
                Log.event(new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                    .addDetail(UILogEvent.Detail.Page, UILog.Page.MenuLanguage));
                break;
            case TranslationKey.SETTINGS_LANGUAGE_SHOP_LANGUAGE:
                Log.d(TAG, "onItemClick: open shop language preferences");
                mNavigator.showLanguageShopPreferences();
                break;
        }
    }

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        return new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                .addDetail(UILogEvent.Detail.Page, UILog.Page.LanguageList);
    }
}
