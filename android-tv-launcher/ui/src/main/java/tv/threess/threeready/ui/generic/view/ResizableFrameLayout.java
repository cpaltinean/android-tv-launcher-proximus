package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.IntProperty;
import android.util.Property;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Custom frame layout to animate layout size changes.
 *
 * @author Barabas Attila
 * @since 2017.11.01
 */
public class ResizableFrameLayout extends FrameLayout {

    private int mFrameWidth;
    private int mFrameHeight;

    public ResizableFrameLayout(@NonNull Context context) {
        this(context, null);
    }

    public ResizableFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ResizableFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ResizableFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        // Read android attributes
        int[] viewAttrsArray = new int[]{
                android.R.attr.layout_width, // 0
                android.R.attr.layout_height // 1
        };
        TypedArray viewTypeArray = context.obtainStyledAttributes(attrs, viewAttrsArray);

        try {
            mFrameWidth = viewTypeArray.getDimensionPixelOffset(0, ViewGroup.LayoutParams.WRAP_CONTENT);
            mFrameHeight = viewTypeArray.getDimensionPixelOffset(1, ViewGroup.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            // ignore
        } finally {
            viewTypeArray.recycle();
        }
    }


    public int getFrameWidth() {
        return mFrameWidth;
    }

    public void setFrameWidth(int width) {
        if (mFrameWidth != width) {
            getLayoutParams().width = width;
            requestLayout();
            mFrameWidth = width;
        }
    }

    public int getFrameHeight() {
        return mFrameHeight;
    }

    public void setFrameHeight(int height) {
        if (mFrameHeight != height
                && getLayoutParams() != null) {
            getLayoutParams().height = height;
            requestLayout();
            mFrameHeight = height;
        }
    }

    /**
     * A Property wrapper around the width functionality.
     * Used to animate layout height changes.
     */
    public static final Property<ResizableFrameLayout, Integer> IMAGE_HEIGHT =
            new IntProperty<ResizableFrameLayout>("FRAME_HEIGHT") {

                @Override
                public Integer get(ResizableFrameLayout resizableFrameLayout) {
                    return resizableFrameLayout.getFrameHeight();
                }

                @Override
                public void setValue(ResizableFrameLayout resizableFrameView, int height) {
                    resizableFrameView.setFrameHeight(height);
                }
            };

    /**
     * A Property wrapper around the width functionality.
     * Used to animate layout width changes.
     */
    public static final Property<ResizableFrameLayout, Integer> FRAME_WIDTH =
            new IntProperty<ResizableFrameLayout>("FRAME_WIDTH") {

                @Override
                public Integer get(ResizableFrameLayout resizableFrameView) {
                    return resizableFrameView.getFrameWidth();
                }

                @Override
                public void setValue(ResizableFrameLayout resizableFrameView, int width) {
                    resizableFrameView.setFrameWidth(width);
                }
            };
}
