package tv.threess.threeready.ui.home.presenter.module.collection.generic;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.MainMenuItemBinding;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.home.view.ModularPageView;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Presenter for a selector menu option in a collection.
 *
 * @author Barabas Attila
 * @since 9/16/21
 */
public class CollectionSelectorMenuPresenter extends BasePresenter<CollectionSelectorMenuPresenter.ViewHolder, SelectorOption> {

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final Translator mTranslator = Components.get(Translator.class);

    private final CollectionSelectorButtonPresenter.SelectorChangeListener mSelectorChangeListener;

    @Nullable
    private DataSourceSelector mSelector;

    private final ModularPageView.PageTitleCallback mPageTitle;

    public CollectionSelectorMenuPresenter(Context context,
                                           ModularPageView.PageTitleCallback pageTitle,
                                           CollectionSelectorButtonPresenter.SelectorChangeListener selectorChangeListener) {
        super(context);
        this.mPageTitle = pageTitle;
        mSelectorChangeListener = selectorChangeListener;
    }

    public void setSelector(@Nullable DataSourceSelector selector) {
        if (mSelector != null && selector == null) {
            mSelector.clearSelectOption();
            return;
        }
        mSelector = selector;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        MainMenuItemBinding binding = MainMenuItemBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.title.setTextColor(UiUtils.createFontBrandingColorStateList(mLayoutConfig));
        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, SelectorOption selectorOption, List<?> payLoads) {
        onBindHolder(holder, selectorOption);
    }

    @Override
    public void onBindHolder(ViewHolder holder, SelectorOption selectorOption) {
        holder.mBinding.title.setTextSize(TypedValue.COMPLEX_UNIT_PX, holder.view.getContext().getResources().getDimension(R.dimen.menu_text_size_submenu));

        String menuTitle = mTranslator.get(selectorOption.getName());
        if (selectorOption.getItemCount() >= 0) {
            menuTitle = mTranslator.replaceParams(menuTitle,
                    Collections.singletonMap(PlaceholderKey.NUMBER_OF_ITEMS,
                            String.valueOf(selectorOption.getItemCount())));
        }
        holder.mBinding.title.setText(menuTitle);

        if (mSelector != null) {
            holder.view.setActivated(
                    mSelector.getSelectedOption() == selectorOption);
        }

        holder.mBinding.selectorLine.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_PAGE_TABS))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_PAGE_TITLE, mPageTitle.getPageTitle())
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TAB_NAME, menuTitle)
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_POSITION, String.valueOf(mSelector.getOptions().indexOf(selectorOption) + 1))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TOTAL, String.valueOf(mSelector.getOptions().size()))
                        .toString()
        );
    }

    @Override
    public void onClicked(ViewHolder holder, SelectorOption selectorOption) {
        super.onClicked(holder, selectorOption);

        if (mSelectorChangeListener != null && mSelector != null) {
            mSelector.selectOption(mSelector.getOptions().indexOf(selectorOption));
            mSelectorChangeListener.onSelectedOptionChanged();
        }
    }

    @Override
    public long getStableId(SelectorOption selectorOption) {
        return Objects.hash(selectorOption.getName(), selectorOption.getItemCount());
    }

    public static class ViewHolder extends BasePresenter.ViewHolder {

        public final MainMenuItemBinding mBinding;

        public ViewHolder(MainMenuItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
