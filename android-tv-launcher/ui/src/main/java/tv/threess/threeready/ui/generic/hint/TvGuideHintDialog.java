package tv.threess.threeready.ui.generic.hint;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.databinding.TvGuideHintBinding;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Hint for Tv Guide.
 *
 * @author Daniela Toma
 * @since 2021.11.24
 */
public class TvGuideHintDialog extends HintsDialog {
    public static final String TAG = Log.tag(TvGuideHintDialog.class);

    private TvGuideHintBinding mBinding;

    public static TvGuideHintDialog newInstance(String hintId, boolean hintOpenedFromSettings) {
        Bundle args = new Bundle();
        args.putString(EXTRA_HINT_ID, hintId);
        args.putBoolean(EXTRA_HINT_OPENED_FROM_SETTINGS, hintOpenedFromSettings);
        TvGuideHintDialog fragment = new TvGuideHintDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected View inflateView() {
        if (mBinding == null) {
            mBinding = TvGuideHintBinding.inflate(LayoutInflater.from(getContext()));
        }

        return mBinding.getRoot();
    }

    @NonNull
    @Override
    protected ImageView getBackgroundImage() {
        return mBinding.background;
    }

    @NonNull
    @Override
    protected TextView getNextButton() {
        return mBinding.buttonContainer.nextBtn;
    }

    @NonNull
    @Override
    protected TextView getPreviousButton() {
        return mBinding.buttonContainer.previousBtn;
    }

    @Override
    protected void initViews() {
        mBinding.description.setText(mTranslator.get(TranslationKey.HINT_TEXT_JUMP_DAY_TV_GUIDE));
        mBinding.descriptionSmall.setText(mTranslator.get(TranslationKey.HINT_REVIEW_ALL_YOUR_TIPS));
        mBinding.date.setText(mTranslator.get(TranslationKey.HINT_DATE_JUMP_DAY_TV_GUIDE));
        mBinding.description.announceForAccessibility(mTranslator.get(TranslationKey.HINT_TEXT_JUMP_DAY_TV_GUIDE)
                + " " + mTranslator.get(TranslationKey.HINT_REVIEW_ALL_YOUR_TIPS));
    }
}
