/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.menu.view;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.widget.ImageView;

import androidx.leanback.widget.HorizontalGridView;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.local.FontType;
import tv.threess.threeready.api.home.model.menu.MenuItem;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.SubMenuBinding;

/**
 * A custom view which includes tab items representing a sub-page/screen.
 * Created by Szilard on 4/27/2017.
 */

public class SubMenuView extends BaseMenuView {
    public static final String TAG = Log.tag(SubMenuView.class);

    private final SubMenuBinding mBinding;
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    public SubMenuView(Context context) {
        this(context, null);
    }

    public SubMenuView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SubMenuView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mBinding = SubMenuBinding.inflate(LayoutInflater.from(getContext()), this);
    }

    @Override
    @NotNull
    protected ImageView getLogo() {
        return mBinding.logo;
    }

    @Override
    @NotNull
    protected HorizontalGridView getMenuView() {
        return mBinding.itemList;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mBinding.title.setFontType(FontType.BOLD);
        mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        mBinding.itemList.setPruneChild(false);
        mBinding.itemList.setFocusable(false);
        mBinding.background.setBackgroundColor(Color.BLACK);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    if (mBinding.itemList.getFocusedChild() != null) {
                        return mBinding.itemList.getChildViewHolder(mBinding.itemList.getFocusedChild()).getBindingAdapterPosition() == 0;
                    }
                    return findFocus() != null && !mMenuIconViewList.isEmpty()
                            && findFocus() == mMenuIconViewList.get(0);
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    if (mBinding.itemList.getFocusedChild() != null && mBinding.itemList.getAdapter() != null) {
                        return mBinding.itemList.getChildViewHolder(mBinding.itemList.getFocusedChild()).getBindingAdapterPosition() == mBinding.itemList.getAdapter().getItemCount() - 1;
                    }
                default:
                    return super.dispatchKeyEvent(event);
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public void setTitle(String title) {
        mBinding.title.setText(title);
    }

    public void setMenuPage(PageConfig pageConfig) {
        resetOldMenu();

        String title = mTranslator.get(pageConfig.getTitle());
        mBinding.title.setText(title);

        mMenuItemList = pageConfig.getSubMenus();

        mMenuAdapter.updateMenuItems(mMenuItemList);

        if (!ArrayUtils.isEmpty(mMenuItemList)) {
            if (mSelectedMenuItem != null && isMenuItemExist(mSelectedMenuItem)) {
                selectLastSelectedMenuItem();
            } else {
                setSelectedItem(getSelectedMenu(mMenuItemList.get(0).getId()));
            }

            mBinding.itemList.setFocusable(true);
            mBinding.gradient.getLayoutParams().height = getContext().getResources().getDimensionPixelSize(R.dimen.menu_main_gradient_height_large);
        } else {
            mBinding.itemList.setFocusable(false);
            mBinding.gradient.getLayoutParams().height = getContext().getResources()
                    .getDimensionPixelSize(R.dimen.menu_main_gradient_height_small);
        }

        mBinding.gradient.requestLayout();
    }

    /**
     * @return True if the menu has a title displayed.
     */
    public boolean hasTitle() {
        return !TextUtils.isEmpty(mBinding.title.getText());
    }

    /**
     * @return True if the submenu has items displayed.
     */
    public boolean hasMenuItems() {
        return mMenuItemList != null && !mMenuItemList.isEmpty();
    }

    private boolean isMenuItemExist(MenuItem item) {
        for (MenuItem mItem : mMenuItemList) {
            if (Objects.equals(mItem.getId(), item.getId())) {
                return true;
            }
        }
        return false;
    }

    public void setTitleMaxWidth(int maxWidth) {
        mBinding.title.setWidth(maxWidth);
    }

    @Override
    public UILog.Page getFocusSubPointName() {
        return null;
    }
}
