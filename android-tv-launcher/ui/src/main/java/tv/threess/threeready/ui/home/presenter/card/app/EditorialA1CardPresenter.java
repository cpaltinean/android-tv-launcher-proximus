package tv.threess.threeready.ui.home.presenter.card.app;

import static tv.threess.threeready.api.home.model.module.EditorialItemStyle.AdaptiveSize;
import static tv.threess.threeready.api.home.model.module.EditorialItemStyle.DEFAULT_SIZE;
import static tv.threess.threeready.api.home.model.module.EditorialItemStyle.FixedSize;
import static tv.threess.threeready.api.home.model.module.EditorialItemStyle.Size;
import static tv.threess.threeready.api.home.model.module.EditorialItemStyle.VariantSize;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.home.HomeRepository;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.generic.ContentConfig;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.AppEditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.CategoryEditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.EpgEditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.IntentEditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.ModuleEditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.PageByIdEditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.PageEditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.VodDetailItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.VodPlaybackItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.WebPageEditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemStyle;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.page.ModulePageConfig;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.netflix.model.SourceType;
import tv.threess.threeready.api.tv.model.ImageSource;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.EditorialA1CardBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.generic.presenter.BaseModularCardPresenter;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.generic.view.ResizableCardView;
import tv.threess.threeready.ui.utils.TitleFallbackImageViewTarget;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Presenter class to show configurable A1 editorial cards.
 *
 * @author Barabas Attila
 * @since 2018.08.24
 */
public class EditorialA1CardPresenter extends BaseModularCardPresenter<EditorialA1CardPresenter.ViewHolder, EditorialItem> {
    private static final String TAG = Log.tag(EditorialA1CardPresenter.class);

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final ContentConfig mContentConfig = Components.get(ContentConfig.class);
    private final HomeRepository mHomeRepository = Components.get(HomeRepository.class);

    private final PresenterSelector mCardPresenterSelector;

    private final int mFocusedCardElevation;
    private final int mDefaultCardElevation;

    public EditorialA1CardPresenter(Context context) {
        this(context, null);
    }

    public EditorialA1CardPresenter(Context context, PresenterSelector cardPresenterSelector) {
        super(context);
        mCardPresenterSelector = cardPresenterSelector;
        mFocusedCardElevation = context.getResources().getDimensionPixelOffset(R.dimen.card_focused_elevation);
        mDefaultCardElevation = context.getResources().getDimensionPixelOffset(R.dimen.card_default_elevation);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent, RecyclerView.RecycledViewPool recycledViewPool) {
        EditorialA1CardBinding binding = EditorialA1CardBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindHolder(ModuleData<?> moduleData, ViewHolder holder, EditorialItem item) {
        super.onBindHolder(moduleData, holder, item);

        ModuleConfig moduleConfig = moduleData.getModuleConfig();

        updateCardSize(moduleData, holder, item);
        updateTitle(moduleConfig, holder, item);
        loadBackground(moduleData, holder, item);
        loadIcon(moduleConfig, holder, item);
        reportViewedFeedback(holder, item);
        updateContentDescription(moduleData, holder, item);
    }

    /**
     * Report a feedback to the backend when the editorial item is viewed.
     */
    private void reportViewedFeedback(ViewHolder holder, EditorialItem item) {
        if (!item.hasViewedFeedback()) {
            // Nothing to report.
            return;
        }

        RxUtils.disposeSilently(holder.mViewedFeedbackDisposable);
        holder.mViewedFeedbackDisposable = mHomeRepository.editorialViewedFeedback(item)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    /**
     * Report a feedback to the backend when the editorial item is clicked.
     */
    private void reportClickedFeedback(ViewHolder holder, EditorialItem item) {
        if (!item.hasClickedFeedback()) {
            // Nothing to report.
            return;
        }

        RxUtils.disposeSilently(holder.mClickedFeedbackDisposable);
        holder.mClickedFeedbackDisposable = mHomeRepository.editorialClickedFeedback(item)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    /**
     * Calculate and apply the card size and scale animations.
     */
    private void updateCardSize(ModuleData<?> moduleData, ViewHolder holder, EditorialItem item) {
        float scaleFactor = getFocusScaleFactor(moduleData, item);

        // Width
        int focusedCardWidth = getCardWidth(moduleData, item);
        int defaultCardWidth = Math.round(focusedCardWidth / scaleFactor);
        holder.mCardView.setCardWidth(focusedCardWidth);
        holder.mCardView.setMaxHorizontalPadding((focusedCardWidth - defaultCardWidth) / 2);

        // Height
        int focusedCardHeight = getCardHeight(moduleData, item);
        int defaultCardHeight = Math.round(focusedCardHeight / scaleFactor);
        holder.mCardView.setCardHeight(focusedCardHeight);
        holder.mCardView.setMaxVerticalPadding((focusedCardHeight - defaultCardHeight) / 2);

        // Animation
        holder.view.setStateListAnimator(createScaleStateListAnimator(holder.mCardView));
    }

    private void updateContentDescription(ModuleData<?> moduleData, ViewHolder holder, EditorialItem item) {
        if (!mMwRepository.isTalkBackOn()) {
            return;
        }

        ModuleConfig module = moduleData.getModuleConfig();
        EditorialItemAction itemAction = item.getAction() == null ?
                module.getEditorialItemAction() : item.getAction();

        if (itemAction instanceof AppEditorialItemAction) {
            AppEditorialItemAction appInfo = (AppEditorialItemAction) itemAction;
            String appName = PackageUtils.getAppNameByPackageName(mContext, appInfo.getPackageName());
            holder.mBinding.imageCover.setContentDescription(
                    new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_STRIPE_EDITORIAL_APPS))
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_SWIMLANE, mTranslator.get(module.getTitle()))
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_APP_NAME, !TextUtils.isEmpty(appName) ? appName : appInfo.getPackageName())
                            .toString()
            );
            return;
        }

        EditorialItemStyle style = item.getStyle() != null ?
                item.getStyle() : module.getModuleStyle() != null ? module.getModuleStyle().getItemStyle() : null;

        if (item.getContentDescription() != null) {
            holder.view.setContentDescription(
                    new TalkBackMessageBuilder(mTranslator.get(item.getContentDescription()))
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_SWIMLANE, mTranslator.get(module.getTitle()))
                            .toString()
            );
            return;
        }

        if (style != null && style.getWidth() instanceof VariantSize &&
                ((VariantSize) style.getWidth()).variant == VariantSize.Variant.ViewAll) {
            holder.view.setContentDescription(
                    new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_TILE_VIEW_ALL))
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_SWIMLANE, mTranslator.get(module.getTitle()))
                            .toString()
            );
            return;
        }

        if (!TextUtils.isEmpty(item.getTitle()) || !TextUtils.isEmpty(item.getFallbackTitle())) {
            holder.mBinding.imageCover.setContentDescription(
                    new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_STRIPE_EDITORIAL))
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_SWIMLANE, getSwimlaneTitle(moduleData, item, module))
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TEXT, mTranslator.get(
                                    !TextUtils.isEmpty(item.getTitle()) ? item.getTitle() : item.getFallbackTitle()
                            ))
                            .toString()
            );
        }
    }

    /**
     * Gets swimlane title that should be used in content description.
     */
    private String getSwimlaneTitle(ModuleData<?> moduleData, EditorialItem item, ModuleConfig module) {
        String moduleTitle = mTranslator.get(module.getTitle());
        if (moduleTitle != null && !TextUtils.isEmpty(moduleTitle)) {
            return moduleTitle;
        }
        if (item.getFallbackTitle() != null && !TextUtils.isEmpty(item.getFallbackTitle())) {
            return item.getFallbackTitle();
        }
        return moduleData.getTitle() != null ? moduleData.getTitle() : "";
    }

    /**
     * Display the title of the editorial tile.
     */
    private void updateTitle(ModuleConfig moduleConfig, ViewHolder holder, EditorialItem item) {
        holder.mBinding.title.setTextColor(getTitleColor(moduleConfig, item));
        holder.mBinding.title.setTextSize(getFontSize(moduleConfig, item));
        updateTitle(holder, item.getTitle());
    }

    private void updateTitle(ViewHolder holder, String title) {
        if (TextUtils.isEmpty(title)) {
            holder.mBinding.title.setVisibility(View.GONE);
            return;
        }

        holder.mBinding.title.setText(mTranslator.get(title));
        holder.mBinding.title.setVisibility(View.VISIBLE);
    }

    private float getFontSize(ModuleConfig module, EditorialItem item) {
        // Get font sized from item style.
        if (item.getStyle() != null && item.getStyle().getTitleFontSize() != null) {
            return item.getStyle().getTitleFontSize();
        }

        // Get font size for module style.
        if (module.getModuleStyle() != null && module.getModuleStyle().getItemStyle() != null
                && module.getModuleStyle().getItemStyle().getTitleFontSize() != null) {
            return module.getModuleStyle().getItemStyle().getTitleFontSize();
        }
        return mContext.getResources().getDimension(R.dimen.editorial_card_title_size);
    }

    private float getFocusScaleFactor(ModuleData<?> moduleData, EditorialItem item) {
        Size size = getFocusScaleSize(moduleData, item);

        // Fixed scale size defined.
        if (size instanceof FixedSize) {
            FixedSize fixedSize = (FixedSize) size;
            return fixedSize.size;
        }


        // Variant based size.
        VariantSize.Variant variant = VariantSize.Variant.Small;
        if (size instanceof VariantSize) {
            VariantSize variantSize = (VariantSize) size;
            variant = variantSize.variant;
        }

        switch (variant) {
            default:
                return mContext.getResources().getFraction(R.fraction.editorial_card_scale_factor, 1, 1);
            case SmallBanner:
                return mContext.getResources().getFraction(R.fraction.small_banner_scale_factor, 1, 1);
            case WideBanner:
                return mContext.getResources().getFraction(R.fraction.wide_banner_scale_factor, 1, 1);
        }
    }

    @Nullable
    private Size getFocusScaleSize(ModuleData<?> moduleData, EditorialItem item) {
        // Use item style.
        if (item != null && item.getStyle() != null
                && item.getStyle().getFocusScaleSize() != null) {
            return item.getStyle().getFocusScaleSize();
        }

        // Use module style as fallback
        if (moduleData == null || moduleData.getModuleConfig().getModuleStyle() == null
                || moduleData.getModuleConfig().getModuleStyle().getItemStyle() == null) {
            return null;
        }

        return moduleData.getModuleConfig().getModuleStyle().getItemStyle().getFocusScaleSize();
    }

    @Override
    public void onUnbindHolder(ModuleData<?> moduleData, ViewHolder holder) {
        super.onUnbindHolder(moduleData, holder);

        RxUtils.disposeSilently(holder.mDetailDisposable,
                holder.mViewedFeedbackDisposable, holder.mClickedFeedbackDisposable);

        Glide.with(mContext).clear(holder.mBinding.imageCover);
        Glide.with(mContext).clear(holder.mBinding.iconCover);
        holder.mBinding.imageCover.setImageDrawable(null);
        holder.mBinding.iconCover.setImageDrawable(null);
    }

    @Override
    public void onClicked(ModuleData<?> moduleData, ViewHolder holder, EditorialItem item) {
        super.onClicked(moduleData, holder, item);

        reportClickedFeedback(holder, item);

        ModuleConfig module = moduleData.getModuleConfig();
        EditorialItemAction itemAction = item.getAction() == null ?
                module.getEditorialItemAction() : item.getAction();

        if (itemAction instanceof PageByIdEditorialItemAction) {
            openPageAction((PageByIdEditorialItemAction) itemAction);

        } else if (itemAction instanceof PageEditorialItemAction) {
            openPageAction(((PageEditorialItemAction) itemAction).getPage());
        } else if (itemAction instanceof ModuleEditorialItemAction) {
            openModuleAction((ModuleEditorialItemAction) itemAction);

        } else if (itemAction instanceof AppEditorialItemAction) {
            openApplication((AppEditorialItemAction) itemAction);

        } else if (itemAction instanceof IntentEditorialItemAction) {
            openIntent((IntentEditorialItemAction) itemAction);

        } else if (itemAction instanceof CategoryEditorialItemAction) {
            openCategoryBrowsing((CategoryEditorialItemAction) itemAction);

        } else if (itemAction instanceof WebPageEditorialItemAction) {
            openWebPage((WebPageEditorialItemAction) itemAction, module);

        } else if (itemAction instanceof EpgEditorialItemAction) {
            mNavigator.showEpg(true);

        } else if (itemAction instanceof VodDetailItemAction) {
            openVodDetail((VodDetailItemAction) itemAction);

        } else if (itemAction instanceof VodPlaybackItemAction) {
            startVodPlayback(holder, (VodPlaybackItemAction) itemAction);

        } else {
            Log.w(TAG, "Unknown editorial item action. " + itemAction);
        }
    }

    /**
     * Open ab activity with android intent.
     */
    private void openIntent(IntentEditorialItemAction action) {
        // Create intent with action and uri.
        Intent intent = new Intent(action.getIntentAction(), Uri.parse(action.getUri()));
        intent.setPackage(action.getPackageName());

        // Put the extras into the intent.
        if (action.getExtras() != null) {
            for (String key : action.getExtras().keySet()) {
                intent.putExtra(key, action.getExtras().get(key));
            }
        }

        // Start application with intent.
        mPlaybackDetailsManager.startApplication(StartAction.Ui, action.getPackageName(), intent);
    }

    /**
     * Open a 3rd party app.
     */
    private void openApplication(AppEditorialItemAction action) {
        mPlaybackDetailsManager.startApplication(StartAction.Ui, action.getPackageName(), SourceType.Banner);
    }

    /**
     * Open dynamic category browsing.
     */
    private void openCategoryBrowsing(CategoryEditorialItemAction action) {
        mNavigator.showCategoryPage(action.getCategoryId(), action.getEmptyPageMessage());
    }

    /**
     * Open a modular page.
     */
    private void openPageAction(PageByIdEditorialItemAction action) {
        openPageAction(mContentConfig.getPageConfig(action.getPageId()));
    }

    private void openPageAction(PageConfig pageConfig) {
        if (pageConfig == null) {
            Log.e(TAG, "Page not found.");
            return;
        }

        // Open page group with selection.
        if (pageConfig.getParent() != null) {
            PageConfig parentPage = mContentConfig.getPageConfig(pageConfig.getParent());
            if (parentPage != null && parentPage.hasSubPages()) {
                mNavigator.showDynamicPage(parentPage, pageConfig.getId());
                return;
            }
        }

        // Open page.
        mNavigator.showDynamicPage(pageConfig);
    }

    /**
     * Open a modular page for a single module.
     */
    private void openModuleAction(ModuleEditorialItemAction action) {
        ModuleConfig moduleConfig = action.getModuleConfig();
        if (moduleConfig == null) {
            Log.d(TAG, "Module not defined.");
            return;
        }

        mNavigator.showDynamicPage(new ModulePageConfig(moduleConfig));
    }

    /**
     * Open a webpage in a web view.
     */
    private void openWebPage(WebPageEditorialItemAction action, ModuleConfig moduleConfig) {
        mNavigator.openDeepLink(action.getHtmlUrl(), moduleConfig.getId());
    }

    /**
     * Open VOD detail page.
     */
    private void openVodDetail(VodDetailItemAction action) {
        mNavigator.showVodMovieDetails(action.getVodId());
    }

    /**
     * Start VOD playback for the given item.
     */
    private void startVodPlayback(ViewHolder holder, VodPlaybackItemAction action) {
        RxUtils.disposeSilently(holder.mDetailDisposable);
        holder.mDetailDisposable = mVodRepository.getVod(action.getVodId())
                .firstOrError()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(vodItem -> {
                    List<IVodVariant> playableVariants = vodItem.getPlayableVariants();
                    if (playableVariants.size() > 0) {
                        mNavigator.showVodPlayer(StartAction.Ui, playableVariants.get(0), vodItem, vodItem.getCheapestPrice());
                    } else {
                        mNavigator.showVodMovieDetails(action.getVodId());
                    }
                });
    }

    private int getTitleColor(ModuleConfig module, EditorialItem item) {
        // Get title color from item style.
        if (item.getStyle() != null && item.getStyle().getTitleColor() != null) {
            return item.getStyle().getTitleColor();
        }

        // Get title color for module style.
        if (module.getModuleStyle() != null && module.getModuleStyle().getItemStyle() != null
                && module.getModuleStyle().getItemStyle().getTitleColor() != null) {
            return module.getModuleStyle().getItemStyle().getTitleColor();
        }

        return mLayoutConfig.getFontColor();
    }

    /**
     * Load background for the editorial item.
     */
    private void loadBackground(ModuleData<?> moduleData, ViewHolder holder, EditorialItem item) {
        ModuleConfig module = moduleData.getModuleConfig();

        String title = item.getTitle();

        TitleFallbackImageViewTarget target = new TitleFallbackImageViewTarget(
                holder.mBinding.imageCover,
                holder.mBinding.title,
                mTranslator.get(title),
                mTranslator.get(item.getFallbackTitle()),
                () -> {
                    if (TextUtils.isEmpty(item.getTitle())) {
                        holder.mBinding.imageCover.setForeground(null);
                    } else {
                        holder.mBinding.imageCover.setForeground(new ColorDrawable(
                                ContextCompat.getColor(mContext, R.color.editorial_image_overlay)));
                    }
                });

        // Load background image based on item style.
        holder.mBinding.imageCover.setBackgroundColor(mLayoutConfig.getPlaceHolderColor());
        if (item.getStyle() != null && item.getStyle().getBackgroundImage() != null) {
            Glide.with(holder.mBinding.imageCover)
                    .load(switchToAppBannerIfNeeded(item, item.getStyle().getBackgroundImage()))
                    .override(getCardWidth(moduleData, item), getCardHeight(moduleData, item))
                    .into(target);

        // Load background color based on item style.
        } else if (item.getStyle() != null && item.getStyle().getGradient() != null && item.getStyle().getGradient().hasBackgroundColor()) {
            loadBackgroundColor(holder, item.getStyle().getGradient().getFromColor(), item.getStyle().getGradient().getToColor(),
                    item.getStyle().getGradient().getOrientation());

        // Load background image based on module style.
        } else if (module.getModuleStyle() != null && module.getModuleStyle().getItemStyle() != null
                && module.getModuleStyle().getItemStyle().getBackgroundImage() != null) {
            Glide.with(holder.mBinding.imageCover)
                    .load(switchToAppBannerIfNeeded(item,
                            module.getModuleStyle().getItemStyle().getBackgroundImage()))
                    .override(getCardWidth(moduleData, item), getCardHeight(moduleData, item))
                    .into(target);

            // Load background color based on module style.
        } else if (module.getModuleStyle() != null && module.getModuleStyle().getItemStyle() != null
                && module.getModuleStyle().getItemStyle().getGradient() != null
                && module.getModuleStyle().getItemStyle().getGradient().hasBackgroundColor()) {
            loadBackgroundColor(holder,
                    module.getModuleStyle().getItemStyle().getGradient().getFromColor(),
                    module.getModuleStyle().getItemStyle().getGradient().getToColor(),
                    module.getModuleStyle().getItemStyle().getGradient().getOrientation());

            // Load background color based on layout config.
        } else {
            loadBackgroundColor(holder,
                    mLayoutConfig.getPlaceHolderColor(), mLayoutConfig.getPlaceHolderColor(),
                    null);

            // Display the fallback title.
            if (TextUtils.isEmpty(title)){
                title = item.getFallbackTitle();
            }
            updateTitle(holder, title);
        }
    }

    /**
     * Switch the image source coming from the local config as app banner for app editorials
     */
    private IImageSource switchToAppBannerIfNeeded(EditorialItem item, IImageSource imageSource) {
        if (item.isApplication() && imageSource.getType() != IImageSource.Type.APP) {
            return new ImageSource(imageSource.getUrl(), IImageSource.Type.APP);
        }
        return imageSource;
    }

    /**
     * Create and set gradient background color for the card.
     */
    private void loadBackgroundColor(ViewHolder holder, int startColor, int endColor, GradientDrawable.Orientation orientation) {
        int[] color = new int[]{startColor, endColor};
        GradientDrawable gradientDrawable = new GradientDrawable(orientation == null ? GradientDrawable.Orientation.TOP_BOTTOM : orientation, color);
        holder.mBinding.imageCover.setBackground(gradientDrawable);
    }

    /**
     * Load the icon for the editorial item and align it with the title.
     */
    private void loadIcon(ModuleConfig module, ViewHolder holder, EditorialItem moduleItem) {
        EditorialItemStyle moduleItemStyle = null;

        if (moduleItem.getStyle() != null && moduleItem.getStyle().getIcon() != null) {
            moduleItemStyle = moduleItem.getStyle();
        }

        if (moduleItemStyle == null && module.getModuleStyle() != null
                && module.getModuleStyle().getItemStyle() != null
                && module.getModuleStyle().getItemStyle().getIcon() != null) {
            moduleItemStyle = module.getModuleStyle().getItemStyle();
        }

        if (moduleItemStyle == null) {
            holder.mBinding.iconCover.setVisibility(View.GONE);
        } else {
            holder.mBinding.iconCover.setVisibility(View.VISIBLE);
            Glide.with(holder.mBinding.iconCover)
                    .load(moduleItemStyle.getIcon())
                    .override(
                            mContext.getResources().getDimensionPixelOffset(R.dimen.editorial_icon_width),
                            mContext.getResources().getDimensionPixelOffset(R.dimen.editorial_icon_height)
                    )
                    .dontTransform()
                    .fitCenter()
                    .into(holder.mBinding.iconCover);
        }
    }

    @Override
    public int getCardWidth(ModuleData<?> moduleData, EditorialItem item) {
        Size itemWidth = getWidthSize(moduleData, item);
        float scaleSize = getFocusScaleFactor(moduleData, item);

        // Fixed size
        if (itemWidth instanceof FixedSize) {
            FixedSize fixedSize = (FixedSize) itemWidth;
            return Math.round(UiUtils.dpToPx(mContext, fixedSize.size) * scaleSize);
        }

        // Variant based size
        if (itemWidth instanceof VariantSize) {
            VariantSize variantSize = (VariantSize) itemWidth;
            return Math.round(getVariantWidth(variantSize) * scaleSize);
        }

        if (itemWidth instanceof AdaptiveSize) {
            return getAdaptiveWidth(moduleData);
        }

        return Math.round(getVariantWidth(DEFAULT_SIZE) * scaleSize);
    }

    /**
     * The size of the editorial card width.
     * Can be fixed, variant based or adaptive.
     */
    @Nullable
    private Size getWidthSize(ModuleData<?> moduleData, EditorialItem item) {
        // Use item style.
        if (item != null && item.getStyle() != null
                && item.getStyle().getWidth() != null) {
            return item.getStyle().getWidth();
        }

        // Use module style as fallback
        if (moduleData == null || moduleData.getModuleConfig().getModuleStyle() == null
                || moduleData.getModuleConfig().getModuleStyle().getItemStyle() == null) {
            return null;
        }

        return moduleData.getModuleConfig().getModuleStyle().getItemStyle().getWidth();
    }

    /**
     * @return The default width of the editorial card based on the variant type.
     */
    private int getVariantWidth(VariantSize variantSize) {
        switch (variantSize.variant) {
            default:
            case Small:
                return mContext.getResources().getDimensionPixelOffset(R.dimen.editorial_small_default_width);
            case Large:
                return mContext.getResources().getDimensionPixelOffset(R.dimen.editorial_large_default_width);
            case ViewAll:
                return mContext.getResources().getDimensionPixelOffset(R.dimen.editorial_view_all_default_width);
            case App:
                return mContext.getResources().getDimensionPixelOffset(R.dimen.app_a1_card_width);
            case SmallBanner:
                return mContext.getResources().getDimensionPixelOffset(R.dimen.small_banner_width);
            case WideBanner:
                return mContext.getResources().getDimensionPixelOffset(R.dimen.wide_banner_width);
        }
    }

    /**
     * @return The focused width of the editorial card
     * calculated based on the other cards in the module.
     */
    private int getAdaptiveWidth(ModuleData<?> moduleData) {
        if (mCardPresenterSelector == null) {
            Log.w(TAG, "Adaptive size set, without card selector defined.");
            return 0;
        }

        if (moduleData == null) {
            return 0;
        }

        int maxWidth = 0;
        for (Object item : moduleData.getItems()) {
            Presenter presenter = mCardPresenterSelector.getPresenter(item);
            if (presenter instanceof BaseCardPresenter) {
                BaseCardPresenter cardPresenter = (BaseCardPresenter<?,?>) presenter;
                int width = cardPresenter.getCardWidth(item);

                if (width > maxWidth) {
                    maxWidth = width;
                }
            }
        }

        return maxWidth;
    }

    @Override
    public int getCardHeight(ModuleData<?> moduleData, EditorialItem item) {
        Size itemHeight = getHeightSize(moduleData, item);
        float scaleSize = getFocusScaleFactor(moduleData, item);

        // Fixed size
        if (itemHeight instanceof FixedSize) {
            FixedSize fixedSize = (FixedSize) itemHeight;
            return Math.round(UiUtils.dpToPx(mContext, fixedSize.size) * scaleSize);
        }

        // Variant based size
        if (itemHeight instanceof VariantSize) {
            VariantSize variantSize = (VariantSize) itemHeight;
            return Math.round(getVariantHeight(variantSize) * scaleSize);
        }

        if (itemHeight instanceof AdaptiveSize) {
            return getAdaptiveHeight(moduleData);
        }

        return Math.round(getVariantHeight(DEFAULT_SIZE) * scaleSize);
    }

    /**
     * The size of the editorial card height.
     * Can be fixed, variant based or adaptive.
     */
    @Nullable
    private Size getHeightSize(ModuleData<?> moduleData, EditorialItem item) {
        // Use item style.
        if (item != null && item.getStyle() != null
                && item.getStyle().getHeight() != null) {
            return item.getStyle().getHeight();
        }

        // Use module style as fallback.
        if (moduleData == null || moduleData.getModuleConfig().getModuleStyle() == null
                || moduleData.getModuleConfig().getModuleStyle().getItemStyle() == null) {
            return null;
        }

        return moduleData.getModuleConfig().getModuleStyle().getItemStyle().getHeight();
    }

    /**
     * @return The default height of the editorial card based on the variant type.
     */
    private int getVariantHeight(VariantSize variantSize) {
        switch (variantSize.variant) {
            default:
            case Small:
                return mContext.getResources().getDimensionPixelOffset(R.dimen.editorial_small_default_height);
            case Large:
                return mContext.getResources().getDimensionPixelOffset(R.dimen.editorial_large_default_height);
            case ViewAll:
                return mContext.getResources().getDimensionPixelOffset(R.dimen.editorial_view_all_default_height);
            case App:
                return mContext.getResources().getDimensionPixelOffset(R.dimen.app_a1_card_height);
            case SmallBanner:
                return mContext.getResources().getDimensionPixelOffset(R.dimen.small_banner_height);
            case WideBanner:
                return mContext.getResources().getDimensionPixelOffset(R.dimen.wide_banner_height);
        }
    }

    /**
     * @return The focused height of the editorial card
     * calculated based on the other cards in the module.
     */
    private int getAdaptiveHeight(ModuleData<?> moduleData) {
        if (mCardPresenterSelector == null) {
            Log.w(TAG, "Adaptive size set, without card selector defined.");
            return 0;
        }

        if (moduleData == null) {
            return 0;
        }

        int maxHeight = 0;
        for (Object item : moduleData.getItems()) {
            Presenter presenter = mCardPresenterSelector.getPresenter(item);
            if (presenter instanceof BaseCardPresenter) {
                BaseCardPresenter cardPresenter = (BaseCardPresenter<?,?>) presenter;
                int height = cardPresenter.getCardHeight(item);

                if (height > maxHeight) {
                    maxHeight = height;
                }
            }
        }

        return maxHeight;
    }

    /**
     * Create focus state animator for the editorial card.
     */
    private StateListAnimator createScaleStateListAnimator(ResizableCardView cardView) {
        StateListAnimator stateListAnimator = new StateListAnimator();

        // Focused state
        AnimatorSet focusedAnimatorSet = new AnimatorSet();
        focusedAnimatorSet.playTogether(
                ObjectAnimator.ofInt(cardView, ResizableCardView.HORIZONTAL_PADDING, 0),
                ObjectAnimator.ofInt(cardView, ResizableCardView.VERTICAL_PADDING, 0),
                ObjectAnimator.ofFloat(cardView, View.TRANSLATION_Z, mFocusedCardElevation));
        stateListAnimator.addState(new int[]{android.R.attr.state_focused}, focusedAnimatorSet);

        // Default state
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(
                ObjectAnimator.ofInt(cardView, ResizableCardView.HORIZONTAL_PADDING, cardView.getMaxHorizontalPadding()),
                ObjectAnimator.ofInt(cardView, ResizableCardView.VERTICAL_PADDING, cardView.getMaxVerticalPadding()),
                ObjectAnimator.ofFloat(cardView, View.TRANSLATION_Z, mDefaultCardElevation));
        stateListAnimator.addState(new int[0], animatorSet);

        return stateListAnimator;
    }


    @Override
    public long getStableId(EditorialItem moduleItem) {
        return moduleItem.hashCode();
    }

    public static class ViewHolder extends BaseCardPresenter.ViewHolder {

        ResizableCardView mCardView;

        private final EditorialA1CardBinding mBinding;

        Disposable mDetailDisposable;
        Disposable mViewedFeedbackDisposable;
        Disposable mClickedFeedbackDisposable;

        public ViewHolder(EditorialA1CardBinding binding) {
            super(binding.getRoot());

            mCardView = binding.getRoot();
            mBinding = binding;
        }
    }
}
