package tv.threess.threeready.ui.home.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.recyclerview.widget.RecyclerView;

import tv.threess.threeready.ui.generic.view.BaseVerticalGridView;
import tv.threess.threeready.ui.generic.view.ResizableCardView;

/**
 * Vertical grid view which allows overlapping items.
 *
 * @author Barabas Attila
 * @since 2017.06.12
 */
public class CollectionGridView extends BaseVerticalGridView {

    private OnChildSelectedListener mOnChildSelectedListener;

    public CollectionGridView(Context context) {
        super(context);
    }

    public CollectionGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CollectionGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        addItemDecoration(mItemDecoration);
    }

    @Override
    public void onViewAdded(View child) {
        super.onViewAdded(child);

        OnFocusChangeListener chainedListener = child.getOnFocusChangeListener();
        child.setOnFocusChangeListener(new OnChildFocusChangeListener(chainedListener));
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        addOnChildViewHolderSelectedListener(mOnChildViewHolderSelectedListener);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeOnChildViewHolderSelectedListener(mOnChildViewHolderSelectedListener);
    }

    public void setOnChildSelectedListener(OnChildSelectedListener childSelectedListener) {
        mOnChildSelectedListener = childSelectedListener;
    }

    @Override
    public void dispatchSetActivated(boolean activated) {
        //Method from base class is not called because activated state does not have to be set to children.
    }

    /**
     * Adjust the spacing between the items in order to place the card near to each other in minimized state.
     * In maximized state the cards can overlap each other.
     */
    private final RecyclerView.ItemDecoration mItemDecoration = new RecyclerView.ItemDecoration() {
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            // Set spacing for card based on the card
            if (view instanceof ResizableCardView) {
                ResizableCardView resizableCardView = (ResizableCardView) view;

                // Calculate spacing based on min and maximum card size
                // .
                int horizontalSpacing = resizableCardView.getMaxHorizontalPadding();
                outRect.left -= horizontalSpacing;
                outRect.right -= horizontalSpacing;

                // Calculate spacing based on min and maximum card size.
                int verticalSpacing = resizableCardView.getMaxVerticalPadding();
                outRect.top -= verticalSpacing;
                outRect.bottom -= verticalSpacing;
            }
        }
    };

    public interface OnChildSelectedListener {
        void onChildSelected(int position);
    }

    private final OnChildViewHolderSelectedListener mOnChildViewHolderSelectedListener = new OnChildViewHolderSelectedListener() {
        int mPrevPosition;

        @Override
        public void onChildViewHolderSelected(RecyclerView parent, final ViewHolder child, int position, int subposition) {
            super.onChildViewHolderSelected(parent, child, position, subposition);
            if (mOnChildSelectedListener != null && position != mPrevPosition) {
                mOnChildSelectedListener.onChildSelected(position);
            }

            mPrevPosition = position;

        }
    };

    /**
     * The activated state is needed, because we decide by this state that the card has an overlay or not.
     */
    private static final class OnChildFocusChangeListener implements View.OnFocusChangeListener {
        View.OnFocusChangeListener mChainedListener;


        public OnChildFocusChangeListener(OnFocusChangeListener chainedListener) {
            mChainedListener = chainedListener;
        }

        @Override
        public void onFocusChange(View child, boolean hasFocus) {
            child.setActivated(hasFocus);

            if (mChainedListener != null) {
                mChainedListener.onFocusChange(child, hasFocus);
            }
        }
    }
}
