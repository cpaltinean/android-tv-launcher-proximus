/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.receiver;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.util.Collection;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.receivers.BaseBroadcastReceiver;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.BluetoothUtils;

/**
 * Class that will receive intent only when we receive a key that requires bluetooth pairing with the
 * remote control, check if remote is paired, if not, send notification to activity
 *
 * @author Boldijar Paul
 * @since 2017.09.29
 */
public class RemoteNotPairedReceiver extends BaseBroadcastReceiver {
    private static final String TAG = Log.tag(RemoteNotPairedReceiver.class);

    public static final String ACTION_RECEIVED_KEY = "android.intent.action.RECEIVED_KEY_THAT_REQUIRES_BLUETOOTH_REMOTE";

    private final IntentFilter mIntentFilter = new IntentFilter(ACTION_RECEIVED_KEY);
    private Listener mListener;
    private Context mApplicationContext;

    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(TAG, "Received intent: " + intent);
        Collection<String> remoteNames = mAppConfig.getRemoteNames();
        if (BluetoothUtils.getConnectedDevice(mApplicationContext, remoteNames) != null) {
            Log.d(TAG, "Remote is connected, No need to show notification");
            return;
        }

        Log.d(TAG, "Remote is not connected, and a key that requires bluetooth pair is received. Showing notification.");
        mListener.showPairRemoteNotification();
    }

    public void register(Context context, Listener listener) {
        mListener = listener;
        mApplicationContext = context.getApplicationContext();
        context.registerReceiver(this, getIntentFilter());
    }

    public void unregister(Context context) {
        context.unregisterReceiver(this);
        mListener = null;
        mApplicationContext = null;
    }

    @Override
    public IntentFilter getIntentFilter() {
        return mIntentFilter;
    }

    public interface Listener {
        void showPairRemoteNotification();
    }
}