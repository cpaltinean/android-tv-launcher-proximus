/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.epg.view;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.databinding.FastScrollProgramGuideBinding;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * This view is used to reach easier previous/next days.
 * Is displayed instead of TvProgramGuideView.
 *
 * Created by Szilard on 9/11/2017.
 */

public class FastScrollingView extends RelativeLayout {
    public static final String TAG = Log.tag(FastScrollingView.class);

    private final DateTimeFormatter mDateFormat = DateTimeFormatter.ofPattern("dd", Locale.getDefault());
    private final DateTimeFormatter mTimeFormat = DateTimeFormatter.ofPattern("HH:mm", Locale.getDefault());
    private final DateTimeFormatter mMinuteFormat = DateTimeFormatter.ofPattern("mm", Locale.getDefault());

    public final long PROGRAM_GUIDE_PAST_WINDOW_LENGTH;
    public final long PROGRAM_GUIDE_FUTURE_WINDOW_LENGTH;

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final Translator mTranslator = Components.get(Translator.class);

    private long mTime;
    private final FastScrollProgramGuideBinding mBinding;

    public FastScrollingView(Context context) {
        this(context, null);
    }

    public FastScrollingView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FastScrollingView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public FastScrollingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        mBinding = FastScrollProgramGuideBinding.inflate(LayoutInflater.from(context), this, true);

        PROGRAM_GUIDE_PAST_WINDOW_LENGTH = Settings.epgPastWindowLength.get(TimeUnit.DAYS.toMillis(7));
        PROGRAM_GUIDE_FUTURE_WINDOW_LENGTH = Settings.epgFutureWindowLength.get(TimeUnit.DAYS.toMillis(7));
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mBinding.guideDate.setBackgroundColor(mLayoutConfig.getPlaceHolderColor());
        mBinding.guideDate.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        mBinding.programCard.setBackgroundColor(mLayoutConfig.getPlaceHolderColor());
    }

    public void setTime(long time) {
        String minutes = TimeUtils.getTimeFormat(mMinuteFormat, time);
        mTime = time + TimeUnit.MINUTES.toMillis(60 - Integer.parseInt(minutes));
        displayDate();
    }

    public long getTime() {
        return mTime;
    }

    public void displayChannelLogo(TvChannel channel) {
        Glide.with(getContext().getApplicationContext()).clear(mBinding.channelLogo);
        Glide.with(getContext().getApplicationContext())
                .load(channel)
                .into(mBinding.channelLogo);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            if (mTime > System.currentTimeMillis() + PROGRAM_GUIDE_PAST_WINDOW_LENGTH
                    && mTime < System.currentTimeMillis() +  PROGRAM_GUIDE_FUTURE_WINDOW_LENGTH) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP) {
                    mTime -= TimeUnit.HOURS.toMillis(1);
                } else {
                    mTime += TimeUnit.HOURS.toMillis(1);
                }
                displayDate();
            }
        }
        return super.dispatchKeyEvent(event);
    }

    private void displayDate() {
        String dayOfWeek = LocaleTimeUtils.getDayNameLocalized(mTime, mTranslator, true);
        String dayOfMonth = TimeUtils.getTimeFormat(mDateFormat, mTime);
        String month = LocaleTimeUtils.getMonthNameLocalized(mTime, mTranslator, true);
        String time = TimeUtils.getTimeFormat(mTimeFormat, mTime);


        String aux;
        if (LocaleTimeUtils.getShouldIncludeDate(mTime)) {
            aux = String.format(TimeUtils.LONG_FAST_SCROLLING_TIME_STAMP_FORMAT, dayOfWeek, dayOfMonth, month, time);
        } else {
            aux = String.format(TimeUtils.SHORT_FAST_SCROLLING_TIME_STAMP_FORMAT, dayOfWeek, time);
        }

        SpannableString spannableString = new SpannableString(aux);

        int index = aux.indexOf(TimeUtils.DIVIDER);
        while (index >= 0) {
            spannableString.setSpan(new ForegroundColorSpan(mLayoutConfig.getPlaceholderFontColor()), index + 1, index + 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            index = aux.indexOf(TimeUtils.DIVIDER, index + 1);
        }

        mBinding.guideDate.setText(spannableString);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Glide.with(getContext().getApplicationContext()).clear(mBinding.channelLogo);
    }
}
