package tv.threess.threeready.ui.epggrid.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewParent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;

/**
 * Recycler view which displays the programs for a channel in the EPG grid.
 *
 * @author Barabas Attila
 * @since 8/20/21
 */
public class EpgProgramRowGridView extends RecyclerView {

    private static final long SCROLL_TIME = TimeUnit.MINUTES.toMillis(30);

    private EpgTimeLineView mTimeline;

    public EpgProgramRowGridView(@NonNull Context context) {
        this(context, null);
    }

    public EpgProgramRowGridView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EpgProgramRowGridView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setDescendantFocusability(FOCUS_BEFORE_DESCENDANTS);
        setFocusable(true);
        setLayoutManager(new LinearLayoutManager(
                context, LinearLayoutManager.HORIZONTAL, false)
        {
            @Override
            public boolean onRequestChildFocus(RecyclerView parent, RecyclerView.State state, View child,
                                               View focused) {
                // This disables the default scroll behavior for focus movement.
                return true;
            }
        });
    }

    public void setTimeline(EpgTimeLineView timeline) {
        mTimeline = timeline;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        resetScroll();
    }

    @Override
    public void onChildAttachedToWindow(@NonNull View child) {
        super.onChildAttachedToWindow(child);
        layoutVisibleChildArea(child);
    }

    @Override
    public View focusSearch(View focused, int direction) {
        if (focused == null) {
            return super.focusSearch(focused, direction);
        }

        IBroadcast focusedBroadcast = getBroadcastForChildView(focused);

        long from = mTimeline.getFrom();
        long to = mTimeline.getTo();

        // Check if the currently focused program needs scrolled
        if (isDirectionStart(direction) || direction == View.FOCUS_BACKWARD) {
            if (focusedBroadcast != null && focusedBroadcast.getStart() < from) {
                // The current entry starts outside of the view; Align or scroll to the left.
                // Check how much needs to be scrolled
                long durationUntilStart = mTimeline.getWindowStart() - from;
                mTimeline.smoothScrollByTime(Math.max(durationUntilStart, -SCROLL_TIME));
                return focused;
            }
        } else if (isDirectionEnd(direction) || direction == View.FOCUS_FORWARD) {
            if (focusedBroadcast != null && focusedBroadcast.getEnd() > to) {
                // The current entry ends outside of the view; Scroll to the right.
                // Check how much needs to be scrolled
                long durationUntilEnd = mTimeline.getWindowEnd() - to;
                mTimeline.smoothScrollByTime(Math.min(durationUntilEnd, SCROLL_TIME));

                return focused;
            }
        }

        // Check if scroll needed for the target program.
        View target = super.focusSearch(focused, direction);
        IBroadcast targetBroadcast = getBroadcastForChildView(target);
        if (isDirectionStart(direction) || direction == View.FOCUS_BACKWARD) {
            if (targetBroadcast != null && targetBroadcast.getStart() < from) {
                // The target entry starts outside the view; Scroll to the left.
                mTimeline.smoothScrollByTime(-SCROLL_TIME);
            }
        } else if (isDirectionEnd(direction) || direction == View.FOCUS_FORWARD) {
            if (targetBroadcast != null && targetBroadcast.getEnd() > to) {
                // The target entry starts outside the view; Scroll to the right.
                mTimeline.smoothScrollByTime(SCROLL_TIME);
            }
        }

        // Focus the reference time view at vertical navigation.
        if (direction == FOCUS_UP || direction == FOCUS_DOWN) {
            ViewParent parent = target.getParent();
            if (parent instanceof EpgProgramRowGridView) {
                View referenceChild = ((EpgProgramRowGridView) parent).getFocusReferenceChild();
                if (referenceChild  != null) {
                    return referenceChild;
                }
            }

            // Focused programs changed. Set focus reference time for next vertical navigation.
        } else if (targetBroadcast != null) {
            mTimeline.setFocusReferenceTime(targetBroadcast.getStart(), targetBroadcast.getEnd());
        }


        return target;
    }

    @Override
    protected void onFocusChanged(boolean gainFocus, int direction, @Nullable Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);

        if (gainFocus) {
            focusReferenceTime();
        }
    }


    @Override
    public void onViewAdded(View child) {
        super.onViewAdded(child);

        if (isFocused() && isInFocusReferenceTime(child)) {
            child.requestFocus();
        }
    }


    /**
     * Focus the child view which is inside of the focus reference time.
     */
    private void focusReferenceTime() {
        View child = getFocusReferenceChild();
        if (child != null) {
            child.requestFocus();
        }
    }

    /**
     * @return The child view in the focus reference time.
     */
    @Nullable
    private View getFocusReferenceChild() {
        long referenceTime = mTimeline.getFocusReferenceTime();
        for (int i = 0; i < getChildCount(); ++i) {
            View child = getChildAt(i);
            if (isInFocusReferenceTime(child, referenceTime)) {
                return child;
            }
        }

        return null;
    }

    /**
     * @param child The child to check if entitled for focus.
     * @return True if the given children should receive the focus in the row.
     */
    private boolean isInFocusReferenceTime(View child) {
        return isInFocusReferenceTime(child, mTimeline.getFocusReferenceTime());
    }

    /**
     * @param child The child to check if entitled for focus.
     * @param referenceTime The time at which the focus should be.
     * @return True if the given children should receive the focus in the row.
     */
    private boolean isInFocusReferenceTime(View child, long referenceTime) {
        IBroadcast broadcast = getBroadcastForChildView(child);
        return broadcast != null && broadcast.getStart() <= referenceTime
                && broadcast.getEnd() > referenceTime;
    }

    @Nullable
    private IBroadcast getBroadcastForChildView(View view) {
        if (view instanceof EpgProgramView) {
            EpgProgramView epgProgramView = (EpgProgramView)view;
            return epgProgramView.getBroadcast();
        }
        return null;
    }

    // Call this API after RTL is resolved. (i.e. View is measured.)
    private boolean isDirectionStart(int direction) {
        return getLayoutDirection() == LAYOUT_DIRECTION_LTR
                ? direction == View.FOCUS_LEFT : direction == View.FOCUS_RIGHT;
    }

    // Call this API after RTL is resolved. (i.e. View is measured.)
    private boolean isDirectionEnd(int direction) {
        return getLayoutDirection() == LAYOUT_DIRECTION_LTR
                ? direction == View.FOCUS_RIGHT : direction == View.FOCUS_LEFT;
    }

    @Override
    public void onScrolled(int dx, int dy) {
        super.onScrolled(dx, dy);
        layoutVisibleChildAreas();
    }

    private IBroadcast getBroadcastAtPosition(int position) {
        ItemBridgeAdapter adapter = (ItemBridgeAdapter) getAdapter();
        if (adapter == null) {
            return null;
        }
        return (IBroadcast) adapter.getItem(position);
    }

    /**
     * @param time The time stamp to search for.
     * @return The adapter position of the broadcast at the given time.
     */
    private int findBroadcastPositionAtTime(long time) {
        ItemBridgeAdapter adapter = (ItemBridgeAdapter) getAdapter();
        if (adapter == null) {
            return -1;
        }

        for (int i = 0; i < adapter.getItemCount(); ++i) {
            IBroadcast broadcast = (IBroadcast) adapter.getItem(i);
            if (broadcast.getStart() <= time && broadcast.getEnd() > time) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Align the program grid scroll position to the timeline.
     */
    public void resetScroll() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) getLayoutManager();
        if (layoutManager == null) {
            // No layout set yet.
            return;
        }

        long initTime = mTimeline.getFrom();
        int position = findBroadcastPositionAtTime(initTime);
        if (position >= 0) {
            IBroadcast broadcast = getBroadcastAtPosition(position);
            if (broadcast == null) {
                // No broadcast added yet
                return;
            }

            int offset = mTimeline.convertMillisToPixel(
                    Math.max(broadcast.getStart(), mTimeline.getWindowStart()) - initTime);
            layoutManager.scrollToPositionWithOffset(position, offset);
        } else {
            layoutManager.scrollToPosition(0);
        }
    }

    private void layoutVisibleChildAreas() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; ++i) {
            View child = getChildAt(i);
            layoutVisibleChildArea(child);
        }
    }

    private void layoutVisibleChildArea(View child) {
        if (child instanceof EpgProgramView) {
            EpgProgramView programView = (EpgProgramView) child;
            programView.layoutVisibleArea(mTimeline);
        }
    }
}
