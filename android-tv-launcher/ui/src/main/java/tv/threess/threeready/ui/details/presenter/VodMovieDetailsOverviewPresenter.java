package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextSwitcher;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.HorizontalGridView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.WatchlistType;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.api.vod.wrapper.VodRentOffers;
import tv.threess.threeready.ui.databinding.VodDetailsOverlayBinding;
import tv.threess.threeready.ui.generic.adapter.DetailPageButtonOrder;
import tv.threess.threeready.ui.generic.hint.HintManager;
import tv.threess.threeready.ui.generic.model.VodPlayOption;
import tv.threess.threeready.ui.generic.presenter.ActionModel;
import tv.threess.threeready.ui.generic.presenter.RentActionModel;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.tv.view.ProgressIndicatorView;

/**
 * Presenter class for a VOD movie detail overview.
 *
 * @author Andrei Teslovan
 * @since 2018.10.12
 */

public class VodMovieDetailsOverviewPresenter extends BaseSingleDetailOverviewPresenter<VodMovieDetailsOverviewPresenter.ViewHolder, IVodItem> {
    private static final String TAG = Log.tag(VodMovieDetailsOverviewPresenter.class);

    private static final String TRANSLATION_PRICE_EUROS = "%price_euros%";
    private static final String TRANSLATION_MINIMUM_PRICE_EUROS = "%minimum_price_euros%";
    private static final String TRANSLATION_CREDIT_AMOUNT = "%credit%";
    private static final String TRANSLATION_CREDIT_TOTAL_AMOUNT = "%total_credit%";

    private final VodRepository mVodRepository = Components.get(VodRepository.class);
    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);

    private Disposable mAddWatchlistDisposable;
    private Disposable mRemoveWatchlistDisposable;

    public VodMovieDetailsOverviewPresenter(Context context, IDetailLoaded callback) {
        super(context, callback);
    }

    @Override
    public void onBindHolder(ViewHolder holder, IVodItem item) {
        super.onBindHolder(holder, item);
        //No logo will be shown until the provider logos will be available.
        processRentOffers(holder, item);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);

        RxUtils.disposeSilently(holder.mDisposables);
    }

    @Override
    protected BaseDetailsOverviewPresenter.ViewHolder createViewHolder(ViewGroup parent) {
        VodDetailsOverlayBinding binding = VodDetailsOverlayBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        VodMovieDetailsOverviewPresenter.ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.awards.setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f));
        holder.mBinding.hoursRent.setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f));
        return holder;
    }

    @Override
    protected void updateTitle(ViewHolder holder, IVodItem movie) {
        String title = !TextUtils.isEmpty(movie.getSeriesTitle()) ? movie.getSeriesTitle() : movie.getTitle();
        holder.mBinding.title.setText(title);

        String subtitle = movie.getEpisodeTitleWithSeasonEpisode(mTranslator, " ");
        if (!TextUtils.isEmpty(subtitle) && !subtitle.equals(title)) {
            holder.mBinding.subtitle.setTextColor(mLayoutConfig.getFontColor());
            holder.mBinding.subtitle.setText(subtitle);
            holder.mBinding.subtitle.setVisibility(View.VISIBLE);
        } else {
            holder.mBinding.subtitle.setText(null);
        }
    }

    @Override
    protected void updateMarkersRow(ViewHolder holder, IVodItem movie) {
        // Display content marker
        holder.mBinding.contentMarker.showMarkers(movie);
        holder.mBinding.iconContainer.showParentalRating(movie.getParentalRating());

        updateDescriptiveAudioIcon(holder, movie);
        updateDescriptiveSubtitleIcon(holder, movie);

        holder.mBinding.iconContainer.showAudioProfiles(movie.getMainVariant().getAudioLanguages());
        if (movie.isHD()) {
            holder.mBinding.iconContainer.showHDIcon();
        }

        Log.d(TAG, "Is dolby available : " + mPlaybackManager.isDolbyAvailable());
        if (mPlaybackManager.isContentItemPlaying(movie) && mPlaybackManager.isDolbyAvailable()) {
            holder.mBinding.iconContainer.showDolby();
        }
    }

    /**
     * Displays descriptive subtitle icon.
     */
    private void updateDescriptiveSubtitleIcon(ViewHolder holder, IVodItem movie) {
        if (movie.getMainVariant().hasDescriptiveSubtitle()) {
            holder.mBinding.iconContainer.showDescriptiveSubtitle();
        } else {
            holder.mBinding.iconContainer.hideDescriptiveSubtitle();
        }
    }

    /**
     * Displays descriptive audio icon.
     */
    private void updateDescriptiveAudioIcon(ViewHolder holder, IVodItem movie) {
        if (movie.getMainVariant().hasDescriptiveAudio()) {
            holder.mBinding.iconContainer.showDescriptiveAudio();
        } else {
            holder.mBinding.iconContainer.hideDescriptiveAudio();
        }
    }

    @Override
    protected void updateInfoRow(ViewHolder holder, IVodItem movie) {
        List<String> infoParts = new ArrayList<>();

        String releaseYear = movie.getReleaseYear();
        if (!TextUtils.isEmpty(releaseYear)) {
            infoParts.add(releaseYear);
        }

        String genres = TextUtils.join(", ", movie.getGenres().stream().limit(1).toArray());
        if (!TextUtils.isEmpty(genres)) {
            infoParts.add(genres);
        }

        String duration = LocaleTimeUtils.getDuration(movie.getDuration(), mTranslator);
        if (!TextUtils.isEmpty(duration)) {
            infoParts.add(duration);
        }

        String availability = LocaleTimeUtils.getVodAvailabilityText(movie.getRentalRemainingTime(), mTranslator);
        if (!TextUtils.isEmpty(availability)) {
            infoParts.add(availability);
        }

        holder.mBinding.infoRow.setText(TextUtils.join(TimeUtils.DIVIDER, infoParts));
        holder.mBinding.infoRow.setVisibility(View.VISIBLE);
    }

    @Override
    protected void updateButtons(ViewHolder holder, IVodItem movie) {
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    protected void updateButtons(ViewHolder holder, @Nullable VodRentOffers rentOffers, Boolean inWatchlist) {
        if (rentOffers == null) {
            return;
        }
        
        Map<String, IRentOffer> variantOfferMap = rentOffers.getVariantOfferMap();
        List<ActionModel> actions = new ArrayList<>();

        // Add watch button.
        IVodItem movie = rentOffers.getVod();
        List<VodPlayOption> vodPlayOptions = VodPlayOption.generatePlayOptions(movie, variantOfferMap);
        if (!vodPlayOptions.isEmpty()) {
            actions.add(new ActionModel(0, 0,
                    mTranslator.get(TranslationKey.SCREEN_DETAIL_PLAY_BUTTON),
                    DetailPageButtonOrder.Watch,
                    () -> {
                        if (vodPlayOptions.size() > 1) {
                            mNavigator.showPlayOptionsDialog(vodPlayOptions, movie);
                        } else {
                            VodPlayOption playOption = vodPlayOptions.get(0);
                            mNavigator.clearAllDialogs();
                            mNavigator.showVodPlayer(StartAction.Ui, playOption.getVodVariant(), movie, playOption.getVodPrice());
                        }
                    }));
        }

        // Add trailer button
        for (IVodVariant variant : movie.getVariantList()) {
            if (variant.hasTrailer()) {
                actions.add(new ActionModel(
                        0, 0,
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_TRAILER_BUTTON),
                        DetailPageButtonOrder.Trailer,
                        () -> mNavigator.showTrailerPlayer(StartAction.Ui, variant, movie)));
                break;
            }
        }

        // Add rent button.
        int credit = rentOffers.getCredit();
        if (movie.isRentable(credit, variantOfferMap)) {
            actions.add(new RentActionModel(0, 0,
                    DetailPageButtonOrder.Rent,
                    () -> mNavigator.showRentConfirmationDialog(movie),
                    credit, variantOfferMap
            ));
        }

        // Add more info.
        if (movie.isSVod() && !movie.isSubscribed()) {
            actions.add(new ActionModel(0, 0,
                    mTranslator.get(TranslationKey.SCREEN_DETAIL_MORE_INFO_BUTTON),
                    DetailPageButtonOrder.MoreInfo,
                    () -> {
                        // TODO: Subscribe - opens webview with upselling page - to be finalized in Upselling story
                    }));
        }

        // Add watchlist button
        if (inWatchlist != null) {
            if (inWatchlist) {
                actions.add(new ActionModel(
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_REMOVEFROMWATCHLIST_BUTTON), DetailPageButtonOrder.RemoveFromWatchList,
                        () -> {
                            RxUtils.disposeSilently(mRemoveWatchlistDisposable);
                            mRemoveWatchlistDisposable = mAccountRepository.removeFromWatchlist(movie.getId(), WatchlistType.Vod)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(() -> Log.d(TAG, "Successfully removed the item from watchlist"), throwable -> Log.e(TAG, "Could not remove item from watchlist.", throwable));
                        }
                ));
            } else {
                actions.add(new ActionModel(
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_ADDTOWATCHLIST_BUTTON), DetailPageButtonOrder.AddToWatchList,
                        () -> {
                            RxUtils.disposeSilently(mAddWatchlistDisposable);
                            mAddWatchlistDisposable = mAccountRepository.addToWatchlist(WatchlistType.Vod, movie.getId())
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(() -> {
                                                Log.d(TAG, "Successfully added item to watchlist");
                                                Components.get(HintManager.class).showAddToWatchlistHints();
                                            }
                                            , throwable -> Log.e(TAG, "Could not add item to watchlist", throwable));
                        }
                ));
            }
        }

        DetailPageButtonOrder.sort(actions);
        int currentSelectedButtonIndex = holder.mBinding.actionGrid.getSelectedPosition();
        int nextFocusedButtonIndex = DetailPageButtonOrder.getFocusedButtonIndex(currentSelectedButtonIndex,
                                                                    (List) holder.mAdapter.getItems(),
                                                                    actions);
        updateActionModelDescriptions(actions, movie);
        holder.mAdapter.replaceAll(actions);
        holder.mBinding.actionGrid.setSelectedPosition(nextFocusedButtonIndex);
    }

    /**
     * Get the rent offers for each VodVariant. Check for cheapest price / biggest rental time and update the UI with this info.
     *
     * @param movie the vod for which we check this
     */
    private void processRentOffers(ViewHolder holder, IBaseVodItem movie) {
        RxUtils.disposeSilently(holder.mDisposables);
        holder.mDisposables.add(Observable.combineLatest(
                mVodRepository.getVariantRentOffers(movie),
                mAccountRepository.isInWatchlist(movie.getId()),
                Pair::new)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(vodVariantRentOffersBooleanPair -> {
                    VodRentOffers rentOffers = vodVariantRentOffersBooleanPair.first;
                    Boolean inWatchlist = vodVariantRentOffersBooleanPair.second;
                    Log.d(TAG, "Rent offers and watchlist state retrieved, inWatchlist = [" + inWatchlist + "]");
                    updateButtons(holder, rentOffers, inWatchlist);
                    if (!mIsPageLoaded) {
                        callbackDetail.loadPage();
                        mIsPageLoaded = true;
                    }
                }, throwable -> {
                    Log.e(TAG, "Could not get watchlist state", throwable);
                    updateButtons(holder, null, null);
                }));
    }

    @Override
    public long getStableId(IVodItem item) {
        return Objects.hash(item.getId(), item.isSubscribed(), item.isRented());
    }

    @Override
    protected Observable<IBookmark> getBookmark(ViewHolder holder, IVodItem item) {
        return mVodRepository.getBookmarkForVod(item);
    }

    public static class ViewHolder extends BaseSingleDetailOverviewPresenter.ViewHolder {

        private final VodDetailsOverlayBinding mBinding;

        private final List<Disposable> mDisposables = new ArrayList<>();

        public ViewHolder(VodDetailsOverlayBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        @NonNull
        @Override
        public HorizontalGridView getActionGridView() {
            return mBinding.actionGrid;
        }

        @NonNull
        @Override
        ProgressIndicatorView getProgressBar() {
            return mBinding.progressBar;
        }

        @NonNull
        @Override
        protected FontTextView getSubtitle() {
            return mBinding.subtitle;
        }

        @NonNull
        @Override
        protected ImageView getBigCoverImage() {
            return mBinding.coverBig;
        }

        @NonNull
        @Override
        protected ImageView getNonPlayableIcon() {
            return mBinding.nonPlayableIcon;
        }

        @NonNull
        @Override
        protected FontTextView getNonPlayableTextView() {
            return mBinding.nonPlayableTextView;
        }

        @NonNull
        @Override
        protected View getNonPlayableOverlay() {
            return mBinding.nonPlayableOverview;
        }

        @NonNull
        @Override
        protected View getFadingStart() {
            return mBinding.fadingStart;
        }

        @NonNull
        @Override
        protected ImageView getGradientBottom() {
            return mBinding.gradientBottom;
        }

        @NonNull
        @Override
        protected FontTextView getTitleView() {
            return mBinding.title;
        }

        @NonNull
        @Override
        protected FontTextView getInfoRow() {
            return mBinding.infoRow;
        }

        @NonNull
        @Override
        protected TextSwitcher getDescriptionView() {
            return mBinding.description;
        }

        @NonNull
        @Override
        protected ImageView getShowMoreBtn() {
            return mBinding.showMoreBtn;
        }

        @NonNull
        @Override
        protected FontTextView getCastView() {
            return mBinding.cast;
        }

        @NonNull
        @Override
        protected FontTextView getDirectorsView() {
            return mBinding.directors;
        }

        @NonNull
        @Override
        protected ImageView getCoverImage() {
            return mBinding.cover;
        }
    }
}
