package tv.threess.threeready.ui.generic.hint;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.epg.EpgLayoutType;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.MiniEpgNewLayoutHintBinding;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.settings.model.MainSettingsItem;

/**
 * Hint for mini epg new layout.
 *
 * @author Daniela Toma
 * @since 2021.11.19
 */
public class MiniEpgNewLayoutHintsDialog extends HintsDialog {
    public static final String TAG = Log.tag(MiniEpgNewLayoutHintsDialog.class);
    
    private MiniEpgNewLayoutHintBinding mBinding;

    private enum Type {
        MINI_EPG_NEW_LAYOUT_HINT,
        TV_GUIDE_CHANGE_BACK_LAYOUT
    }

    private final HintManager mHintManager = Components.get(HintManager.class);
    private Disposable mDisposable;

    public static MiniEpgNewLayoutHintsDialog newInstance(String hintId, boolean hintOpenedFromSettings) {
        Bundle args = new Bundle();
        args.putString(EXTRA_HINT_ID, hintId);
        args.putBoolean(EXTRA_HINT_OPENED_FROM_SETTINGS, hintOpenedFromSettings);
        MiniEpgNewLayoutHintsDialog fragment = new MiniEpgNewLayoutHintsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected View inflateView() {
        if (mBinding == null) {
            mBinding = MiniEpgNewLayoutHintBinding.inflate(LayoutInflater.from(getContext()));
        }

        return mBinding.getRoot();
    }

    @NonNull
    @Override
    protected ImageView getBackgroundImage() {
        return mBinding.background;
    }

    @NonNull
    @Override
    protected TextView getNextButton() {
        return mBinding.buttonContainer.nextBtn;
    }

    @NonNull
    @Override
    protected TextView getPreviousButton() {
        return mBinding.buttonContainer.previousBtn;
    }

    @Override
    protected void displayHint() {
        switch (Type.values()[mImagePosition]) {
            case MINI_EPG_NEW_LAYOUT_HINT:
                displayMiniEpgNewLayoutHint();
                break;
            case TV_GUIDE_CHANGE_BACK_LAYOUT:
                tvGuideChangeBackHint();
                break;
        }
        super.displayHint();
    }

    /**
     * Displays the hint for changing back the Tv Guide layout.
     */
    private void tvGuideChangeBackHint() {
        String description = mTranslator.get(TranslationKey.HINT_TEXT_CHANGE_IN_SETTINGS);
        mBinding.description.setText(description);
        mBinding.descriptionSmall.setVisibility(View.VISIBLE);
        mBinding.descriptionSmall.setText(mTranslator.get(TranslationKey.HINT_REVIEW_ALL_YOUR_TIPS));

        mBinding.buttonContainer.previousBtn.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_HINTS_EPG_BUTTON_DISMISS))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY, description)
                        .toString());
        mBinding.buttonContainer.nextBtn.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_HINTS_EPG_BUTTON_SETTINGS))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY, description)
                        .toString());

        updateLinearLayoutContainer(getResources().getDimensionPixelOffset(R.dimen.tv_guide_change_hint_description_width), getMargin(R.dimen.tv_guide_change_hint_description_left));
        updateButtons();
        updateImage(ContextCompat.getDrawable(mBinding.getRoot().getContext(), getTvGuideSettingsImage()), 
                getResources().getDimensionPixelOffset(R.dimen.tv_guide_change_hint_image_width),
                getResources().getDimensionPixelOffset(R.dimen.tv_guide_change_hint_image_height), 
                getResources().getDimensionPixelOffset(R.dimen.tv_guide_change_hint_image_left),
                0, 0);
        updatePointer(ContextCompat.getDrawable(mBinding.getRoot().getContext(), R.drawable.mini_epg_tv_guide_change_back_pointer), 
                getResources().getDimensionPixelOffset(R.dimen.tv_guide_change_hint_pointer_left),
                getResources().getDimensionPixelOffset(R.dimen.tv_guide_change_hint_pointer_top), 
                getResources().getDimensionPixelOffset(R.dimen.tv_guide_change_hint_pointer_bottom));
        mImagePosition = 0;
    }

    /**
     * Displays the hint for the new epg layout.
     */
    private void displayMiniEpgNewLayoutHint() {
        String description = mTranslator.get(TranslationKey.HINT_TEXT_NEW_LAYOUT);
        mBinding.description.setText(description);
        mBinding.descriptionSmall.setVisibility(View.GONE);

        mBinding.buttonContainer.previousBtn.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_HINTS_EPG_BUTTON_CHANGE))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY, description)
                        .toString());
        mBinding.buttonContainer.nextBtn.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_HINTS_EPG_BUTTON_KEEP))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY, description)
                        .toString());

        updateLinearLayoutContainer(getResources().getDimensionPixelOffset(R.dimen.mini_epg_new_layout_hint_description_width), getMargin(R.dimen.mini_epg_new_layout_hint_description_left));
        updateButtons();
        updateImage(ContextCompat.getDrawable(mBinding.getRoot().getContext(), getEpgLayoutClassicGuideImage()), 
                getResources().getDimensionPixelOffset(R.dimen.mini_epg_new_layout_hint_image_width),
                getResources().getDimensionPixelOffset(R.dimen.mini_epg_new_layout_hint_image_height), 
                getResources().getDimensionPixelOffset(R.dimen.mini_epg_new_layout_hint_image_left),
                getResources().getDimensionPixelOffset(R.dimen.mini_epg_new_layout_hint_image_top), 
                getResources().getDimensionPixelOffset(R.dimen.mini_epg_new_layout_hint_image_padding));

        updatePointer(ContextCompat.getDrawable(mBinding.getRoot().getContext(), R.drawable.mini_epg_hints_pointer),
                getResources().getDimensionPixelOffset(R.dimen.mini_epg_new_layout_hint_pointer_left),
                getResources().getDimensionPixelOffset(R.dimen.mini_epg_new_layout_hint_pointer_top), 
                getResources().getDimensionPixelOffset(R.dimen.mini_epg_new_layout_hint_pointer_bottom));
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        return event.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT;
    }

    @Override
    public void onStop() {
        super.onStop();
        RxUtils.disposeSilently(mDisposable);
    }

    /**
     * Updates the properties of buttons.
     */
    protected void updateButtons() {
        mBinding.buttonContainer.previousBtn.setVisibility(View.VISIBLE);
        mBinding.buttonContainer.nextBtn.setVisibility(View.VISIBLE);
        switch (Type.values()[mImagePosition]) {
            case MINI_EPG_NEW_LAYOUT_HINT:
                mBinding.buttonContainer.previousBtn.setText(mTranslator.get(TranslationKey.HINTS_BUTTON_CHANGE));
                mBinding.buttonContainer.previousBtn.setOnClickListener(v -> {
                    if (mImagePosition < Type.values().length) {
                        mImagePosition++;
                        mDisposable = mAccountRepository.setEpgLayout(EpgLayoutType.ClassicEpg)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(() -> Log.d(TAG, "Epg Layout was successfully changed from hint screen to: " + EpgLayoutType.ClassicEpg.name()),
                                        e -> Log.e(TAG, "Epg layout could not be changed from hint screen to: " + EpgLayoutType.ClassicEpg.name(), e));
                        displayHint();
                    }
                });

                mBinding.buttonContainer.nextBtn.setText((mTranslator.get(TranslationKey.HINTS_BUTTON_KEEP_CURRENT)));
                mBinding.buttonContainer.nextBtn.setOnClickListener(v -> {
                    mNavigator.clearAllDialogs();
                    mHintManager.showMiniEpgHints(null);
                });
                break;
            case TV_GUIDE_CHANGE_BACK_LAYOUT:
                mBinding.buttonContainer.previousBtn.setText(mTranslator.get(TranslationKey.DISMISS_BUTTON));
                mBinding.buttonContainer.previousBtn.setOnClickListener(v -> mNavigator.clearAllDialogs());

                mBinding.buttonContainer.nextBtn.setText(mTranslator.get(TranslationKey.HINTS_BUTTON_SETTINGS));
                mBinding.buttonContainer.nextBtn.setOnClickListener(v -> {
                    mNavigator.clearAllDialogs();
                    mNavigator.openSettings(MainSettingsItem.MainItemType.EPG_LAYOUT);
                });
                break;
        }
    }

    /**
     * Updates linear layout container's properties.
     */
    private void updateLinearLayoutContainer(int width, int leftMargin) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mBinding.epgNewLayout.getLayoutParams();
        layoutParams.width = width;
        layoutParams.leftMargin = leftMargin;
        mBinding.epgNewLayout.setLayoutParams(layoutParams);
    }

    /**
     * Updates image's properties.
     */
    private void updateImage(Drawable image, int width, int height, int marginLeft, int marginTop, int borderPadding) {
        mBinding.background.setImageDrawable(image);
        mBinding.background.setPadding(borderPadding, borderPadding, borderPadding, borderPadding);

        RelativeLayout.LayoutParams paramsImage = new RelativeLayout.LayoutParams(width, height);
        paramsImage.setMargins(marginLeft, marginTop, 0, 0);
        mBinding.background.setLayoutParams(paramsImage);
    }

    /**
     * Updates pointer's properties.
     */
    private void updatePointer(Drawable image, int marginLeft, int marginTop, int marginBottom) {
        mBinding.layoutPointer.setBackground(image);
        RelativeLayout.LayoutParams pointerParams = (RelativeLayout.LayoutParams) mBinding.layoutPointer.getLayoutParams();
        pointerParams.setMargins(marginLeft, marginTop, 0, marginBottom);
        mBinding.layoutPointer.setLayoutParams(pointerParams);
    }

    private int getEpgLayoutClassicGuideImage() {
        switch (LocaleUtils.getApplicationLanguage()) {
            case LocaleUtils.DUTCH_LANGUAGE_CODE:
                return R.drawable.epg_layout_classic_guide_nl;
            case LocaleUtils.FRENCH_LANGUAGE_CODE:
                return R.drawable.epg_layout_classic_guide_fr;
            default:
                return R.drawable.epg_layout_classic_guide_en;
        }
    }

    private int getTvGuideSettingsImage() {
        switch (LocaleUtils.getApplicationLanguage()) {
            case LocaleUtils.DUTCH_LANGUAGE_CODE:
                return R.drawable.tv_guide_hint_settings_nl;
            case LocaleUtils.FRENCH_LANGUAGE_CODE:
                return R.drawable.tv_guide_hint_settings_fr;
            default:
                return R.drawable.tv_guide_hint_settings_en;
        }
    }
}
