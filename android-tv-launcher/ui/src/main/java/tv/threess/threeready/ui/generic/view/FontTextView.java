/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.local.FontConfig;
import tv.threess.threeready.api.home.model.local.FontType;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.FontStylist;

/**
 * TextView which allows the user to define a font style to use as the view's typeface.
 *
 * Created by Szilard on 4/13/2017.
 */
public class FontTextView extends AppCompatTextView {

    private final FontStylist mFontStylist = Components.getOrNull(FontStylist.class);
    private FontConfig mFontConfig;

    public FontTextView(Context context) {
        this(context, null);
    }

    public FontTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        // Read font type from attributes.
        FontType font = FontType.REGULAR; // default
        if (attrs != null) {
            TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.FontTextView, defStyle, 0);
            try {
                int fwAttr = typedArray.getInteger(R.styleable.FontTextView_fontType, font.attrValue);
                font = FontType.forAttributeValue(fwAttr);
            } finally {
                typedArray.recycle();
            }
        }

        setFontType(font);

        // Override the sizes based on font type
        setTextSize(TypedValue.COMPLEX_UNIT_PX, super.getTextSize());
        setLineSpacing(super.getLineSpacingExtra(), super.getLineSpacingMultiplier());
        setLetterSpacing(super.getLetterSpacing());
    }

    public void setFontType(@NonNull final FontType fontStyle) {
        if (isInEditMode() || mFontStylist == null) {
            return;
        }

        mFontConfig = mFontStylist.getFontConfig(fontStyle);
        if (mFontConfig == null) {
            // Not custom font available.
            return;
        }

        // Get font id by name.
        Typeface typeFace = mFontStylist.getFont(mFontConfig);

        if (typeFace != null) {
            int style;
            switch (fontStyle) {
                case BOLD:
                case EXTRA_BOLD :
                    style = Typeface.BOLD;
                    break;

                case BOLD_ITALIC :
                    style = Typeface.BOLD_ITALIC;
                    break;

                case REGULAR_ITALIC:
                case LIGHT_ITALIC:
                    style = Typeface.ITALIC;
                    break;

                default:
                    style = Typeface.NORMAL;
            }

            setTypeface(typeFace, style);
        }
    }

    @Override
    public void setLineSpacing(float add, float mult) {
        float lineSpacingExtraFactor = mFontConfig == null
                ? 1f : mFontConfig.getLineSpacingExtraFactor();
        super.setLineSpacing(add * lineSpacingExtraFactor, mult);
    }

    @Override
    public void setLetterSpacing(float letterSpacing) {
        float letterSpacingExtraFactor = mFontConfig == null
                ? 1f : mFontConfig.getLetterSpacingFactor();
        super.setLetterSpacing(letterSpacing* letterSpacingExtraFactor);
    }

    @Override
    public void setTextSize(int unit, float size) {
        float textSizeFactor = mFontConfig == null
                ? 1f : mFontConfig.getTextSizeFactor();
        super.setTextSize(unit, size * textSizeFactor);
    }
}
