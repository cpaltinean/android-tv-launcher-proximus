/*
 *
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */
package tv.threess.threeready.ui.generic.player.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

/**
 * Custom view the handle the default focus in the player button container.
 * And calculate the proper size for the visible buttons.
 *
 * @author Barabas Attila
 * @since 2018.11.13
 */
public class PlayerButtonContainer extends LinearLayout {

    public PlayerButtonContainer(Context context) {
        this(context, null);
    }

    public PlayerButtonContainer(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PlayerButtonContainer(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public PlayerButtonContainer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        setFocusable(true);
        setDescendantFocusability(FOCUS_BEFORE_DESCENDANTS);
    }

    @Override
    public boolean requestFocus(int direction, Rect previouslyFocusedRect) {
        View selectedChild = getSelectedChild();
        if (selectedChild != null) {
            return selectedChild.requestFocus();
        }

        View firstChild = getFirstVisibleChild();
        if (direction == FOCUS_DOWN && firstChild != null) {
            return firstChild.requestFocus();
        }

        return super.requestFocus(direction, previouslyFocusedRect);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        calculateEqualPlayerButtonSizes();
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    /**
     * @return The first view child which is visible.
     */
    @Nullable
    private View getFirstVisibleChild() {
        for (int i = 0; i < getChildCount(); ++i) {
            View child = getChildAt(i);
            if (child != null && child.getVisibility() == View.VISIBLE) {
                return child;
            }
        }

        // No visible child.
        return null;
    }

    /**
     * @return The selected view child if is visible.
     */
    @Nullable
    private View getSelectedChild() {
        for (int i = 0; i < getChildCount(); ++i) {
            View child = getChildAt(i);
            if (child != null && child.isSelected() && child.getVisibility() == View.VISIBLE) {
                return child;
            }
        }

        // No selected and visible child.
        return null;
    }

    /**
     * @param child - set selected.
     */
    public void setSelectedChild(View child) {
        clearSelection();
        child.setSelected(true);
    }

    /**
     * Method used for clear the selection state on buttons.
     */
    public void clearSelection() {
        for (int i = 0; i < getChildCount(); ++i) {
            getChildAt(i).setSelected(false);
        }
}

    /**
     * Calculate the maximum width for all the displayed buttons,
     * and set the as the new width for each button.
     */
    private void calculateEqualPlayerButtonSizes() {
        // Get the max button width.
        int maxWidth = 0;
        for (int i = 0; i < getChildCount(); ++i) {
            View child = getChildAt(i);
            child.measure(ViewGroup.MarginLayoutParams.WRAP_CONTENT, ViewGroup.MarginLayoutParams.WRAP_CONTENT);

            if (maxWidth < child.getMeasuredWidth()) {
                maxWidth = child.getMeasuredWidth();
            }
        }

        // Apply max width to all the children.
        for (int i = 0; i < getChildCount(); ++i) {
            View child = getChildAt(i);
            if (child.getLayoutParams() != null) {
                child.getLayoutParams().width = maxWidth;
                child.setForegroundGravity(Gravity.CENTER);
            }
        }
    }
}
