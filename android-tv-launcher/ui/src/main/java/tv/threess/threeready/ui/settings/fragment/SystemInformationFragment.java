package tv.threess.threeready.ui.settings.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.settings.model.SettingsItem;

/**
 * Fragment to display system information settings
 *
 * @author Denisa Trif
 * @since 2018.03.19
 */
public class SystemInformationFragment extends BaseSettingsDialogFragment<SettingsItem> {
    private static final String TAG = Log.tag(SystemInformationFragment.class);

    private final MwRepository mMwRepository = Components.get(MwRepository.class);

    private Disposable mSystemInformationDisposable;

    public static SystemInformationFragment newInstance() {
        return new SystemInformationFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null) {
            view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.settings_left_frame_color));
        }
        return view;
    }

    protected void initView() {
        List<SettingsItem> items = new ArrayList<>();
        mSystemInformationDisposable = Single.zip(
                mMwRepository.getRemoteControlVersion().onErrorReturnItem(""),
                mMwRepository.getAndroidSerialNumber().onErrorReturnItem(""),
                mMwRepository.getVCASClientVersion().onErrorReturnItem(""),
                mMwRepository.getVCASPluginVersion().onErrorReturnItem(""),
                (o1, o2, o3, o4) -> new String[]{o1, o2, o3, o4})
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    MwRepository mwRepository = Components.get(MwRepository.class);
                    items.add(new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_SYSTEM_INFORMATION_LINE_NUMBER), Settings.accountNumber.get("")));
                    items.add(new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_SYSTEM_INFORMATION_NAME_OF_DECODER), Settings.deviceName.get("")));
                    items.add(new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_SYSTEM_INFORMATION_APP_VERSION), getValueOrDefault(mwRepository.getAppVersion())));
                    items.add(new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_SYSTEM_INFORMATION_MW_VERSION), getValueOrDefault(mwRepository.getMWVersion())));
                    items.add(new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_SYSTEM_INFORMATION_RC_VERSION), getValueOrDefault(result[0])));
                    items.add(new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_SYSTEM_INFORMATION_MAC_ADDRESS), getValueOrDefault(mwRepository.getEthernetMacAddress())));
                    items.add(new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_SYSTEM_INFORMATION_ANDROID_SERIAL_NO), getValueOrDefault(result[1])));
                    items.add(new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_SYSTEM_INFORMATION_VIEWRIGHT_VERSION), getValueOrDefault(result[2])));
                    items.add(new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_SYSTEM_INFORMATION_CAS_PLUGIN_VERSION), getValueOrDefault(result[3])));
                    setItems(items);
                }, exception -> Log.d(TAG, "Exception while initialization of the system information screen."));
    }

    @Override
    protected void setTitle() {
        getTitleView().setText(mTranslator.get(TranslationKey.SETTINGS_SYSTEM_INFORMATION));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        RxUtils.disposeSilently(mSystemInformationDisposable);
    }

    private String getValueOrDefault(String value) {
        if (TextUtils.isEmpty(value)) {
            return mTranslator.get(TranslationKey.SETTINGS_SYSTEM_INFORMATION_INFO_NA);
        }
        return value;
    }

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        return new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                .addDetail(UILogEvent.Detail.Page, UILog.Page.SystemInfoPage);
    }
}
