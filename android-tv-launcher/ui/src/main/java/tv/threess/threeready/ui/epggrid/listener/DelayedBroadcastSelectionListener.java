package tv.threess.threeready.ui.epggrid.listener;

import android.os.Handler;

import tv.threess.threeready.api.tv.model.IBroadcast;

/**
 * Listener which notifies the selected broadcast only after the configured amount of time.
 *
 * @author Barabas Attila
 * @since 8/26/21
 */
public class DelayedBroadcastSelectionListener implements EpgBroadcastSelectedListener {
    private static final long DEFAULT_NOTIFY_DELAY = 500;

    private final Handler mHandler = new Handler();
    private final EpgBroadcastSelectedListener mListener;
    private final long mDispatchDelay;

    private IBroadcast mSelectedBroadcast;

    public DelayedBroadcastSelectionListener(EpgBroadcastSelectedListener listener) {
        this(listener, DEFAULT_NOTIFY_DELAY);
    }

    public DelayedBroadcastSelectionListener(EpgBroadcastSelectedListener listener, long dispatchDelay) {
        mListener = listener;
        mDispatchDelay = dispatchDelay;
    }

    @Override
    public void onBroadcastSelected(IBroadcast broadcast) {
        if (broadcast == null) {
            return;
        }

        if (mSelectedBroadcast != null && broadcast.getId().equals(mSelectedBroadcast.getId())) {
            return;
        }

        long delay = mSelectedBroadcast == null ? 0 : mDispatchDelay;
        mSelectedBroadcast = broadcast;
        mHandler.removeCallbacks(mDispatchRunnable);
        mHandler.postDelayed(mDispatchRunnable, delay);
    }

    private final Runnable mDispatchRunnable = new Runnable() {
        @Override
        public void run() {
            if (mListener != null && mSelectedBroadcast != null) {
                mListener.onBroadcastSelected(mSelectedBroadcast);
            }
        }
    };
}
