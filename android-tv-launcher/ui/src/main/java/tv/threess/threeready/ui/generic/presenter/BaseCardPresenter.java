/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.presenter;

import android.content.Context;
import android.view.View;

import androidx.annotation.CallSuper;
import androidx.leanback.widget.ItemAlignmentFacet;
import androidx.leanback.widget.Presenter;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.ui.generic.activity.BaseActivity;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Abstract base presenter of all type and variant of cards in application.
 *
 * Created by Barabas Attila on 4/13/2017.
 */

public abstract class BaseCardPresenter<THolder extends BaseCardPresenter.ViewHolder, TItem>
        extends BasePresenter<THolder, TItem> {

    protected final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);
    protected final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);
    protected final InternetChecker mInternetChecker = Components.get(InternetChecker.class);
    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    protected final MwRepository mMwRepository = Components.get(MwRepository.class);

    public BaseCardPresenter(Context context) {
        super(context);
    }

    @Override
    @CallSuper
    public void onBindHolder(THolder holder, TItem item) {
        // Register playback state change listener.
        holder.mPlayerCallback = new PlayerCallback(holder, item);
        mPlaybackDetailsManager.registerCallback(holder.mPlayerCallback);

        // Register internet state change listener.
        holder.mOnInternetStateChangedListener = new InternetStateChangedListener(holder, item);
        mInternetChecker.addStateChangedListener(holder.mOnInternetStateChangedListener);

        // Register parental control listener.
        holder.mParentalControlListener = new ParentalControlListener(holder, item);
        mParentalControlManager.addListener(holder.mParentalControlListener);
    }

    @Override
    @CallSuper
    public void onUnbindHolder(THolder holder) {
        super.onUnbindHolder(holder);
        mPlaybackDetailsManager.unregisterCallback(holder.mPlayerCallback);
        holder.mPlayerCallback = null;

        if (holder.mOnInternetStateChangedListener != null) {
            mInternetChecker.removeStateChangedListener(holder.mOnInternetStateChangedListener);
        }

        if (holder.mParentalControlListener != null) {
            mParentalControlManager.removeListener(holder.mParentalControlListener);
        }
    }

    /**
     * Called when the active parental rating changed.
     */
    protected void onParentalRatingChanged(THolder holder, TItem item) {

    }

    /**
     * Called when the playback changed in background player.
     */
    protected void onPlaybackChanged(THolder holder, TItem item) {
    }

    /**
     * Called when the internet connectivity state changed.
     */
    protected void onInternetStateChanged(THolder holder, TItem item, boolean available) {
    }

    /**
     * Return the maximum width of the card in pixel.
     */
    public abstract int getCardWidth(TItem item);

    /**
     * Return the maximum height of the card in pixel.
     */
    public abstract int getCardHeight(TItem item);

    /**
     * Used to notify ui components about background playback changes.
     */
    private class PlayerCallback implements IPlaybackCallback {
        TItem mItem;
        THolder mHolder;

        public PlayerCallback(THolder holder, TItem item) {
            mItem = item;
            mHolder = holder;
        }

        @Override
        public void onCommandSuccess(PlaybackCommand cmd, PlaybackDetails details) {
            updateViewOnPlaybackChange();
        }

        @Override
        public void onCommandFailure(PlaybackCommand cmd, PlaybackDetails details, PlaybackError error) {
            updateViewOnPlaybackChange();
        }

        public void updateViewOnPlaybackChange() {
            mHolder.view.removeCallbacks(mUpdateRunnable);
            UiUtils.runOnUiThread(mUpdateRunnable);
        }

        Runnable mUpdateRunnable = new Runnable() {
            @Override
            public void run() {
                onPlaybackChanged(mHolder, mItem);
            }
        };
    }

    /**
     * Used to notify the cards about the internet state changes.
     */
    private class InternetStateChangedListener implements InternetChecker.OnStateChangedListener {
        TItem mItem;
        THolder mHolder;

        InternetStateChangedListener(THolder holder, TItem item) {
            mItem = item;
            mHolder = holder;
        }

        @Override
        public void onStateChanged(State state) {
            if (!state.isInternetStateChanged()) {
                return;
            }
            if (mHolder.view.getContext() instanceof BaseActivity) {
                BaseActivity activity = (BaseActivity) mHolder.view.getContext();
                if (activity == null || activity.isInstanceStateSaved()) {
                    // The activity is already destroyed. This listener will be removed soon. (WeakReference)
                    return;
                }
            }

            onInternetStateChanged(mHolder, mItem, state.isInternetAvailable());
        }
    }

    private class ParentalControlListener implements ParentalControlManager.IParentalControlListener {
        TItem mItem;
        THolder mHolder;

        ParentalControlListener(THolder holder, TItem item) {
            mItem = item;
            mHolder = holder;
        }

        @Override
        public void onParentalRatingChanged(ParentalRating parentalRating) {
            if (mHolder.view.getContext() instanceof BaseActivity) {
                BaseActivity activity = (BaseActivity) mHolder.view.getContext();
                if (activity == null || activity.isInstanceStateSaved()) {
                    // The activity is already destroyed. This listener will be removed soon. (WeakReference)
                    return;
                }
            }

            BaseCardPresenter.this.onParentalRatingChanged(mHolder, mItem);
        }
    }

    protected void setAlignmentOffset(ViewHolder holder, int offset) {
        ItemAlignmentFacet.ItemAlignmentDef facetDef = new ItemAlignmentFacet.ItemAlignmentDef();
        facetDef.setItemAlignmentOffset(offset);
        ItemAlignmentFacet facet = new ItemAlignmentFacet();
        facet.setAlignmentDefs(new ItemAlignmentFacet.ItemAlignmentDef[]{facetDef});
        holder.setFacet(ItemAlignmentFacet.class, facet);
    }

    public static class ViewHolder extends Presenter.ViewHolder {
        IPlaybackCallback mPlayerCallback;

        InternetChecker.OnStateChangedListener mOnInternetStateChangedListener;
        ParentalControlManager.IParentalControlListener mParentalControlListener;

        public ViewHolder(View view) {
            super(view);
        }
    }
}
