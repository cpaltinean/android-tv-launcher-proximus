package tv.threess.threeready.ui.generic.notification;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.Objects;

import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.NotificationBinding;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Class for displaying simple notifications in the upper right corner of the screen.
 * <p>
 * This class uses a {@link Notification.Builder} for building the notifications, according to the google Android O standards.
 *
 * @author Lukacs Andor
 * @since 2017.07.24
 */
public class Notification extends BaseNotification {
    protected static final String NOTIFICATION_MODEL_EXTRA = "extra.NOTIFICATION_MODEL";
    protected static final float ALPHA_BACKGROUND = 0.90f;

    protected NotificationBinding mBinding;
    protected NotificationModel mModel;

    protected static Notification newInstance(NotificationModel model) {
        Notification notification = new Notification();
        notification.mModel = model;

        Bundle arguments = new Bundle();
        arguments.putSerializable(NOTIFICATION_MODEL_EXTRA, model);

        notification.setArguments(arguments);
        return notification;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mModel = getSerializableArgument(NOTIFICATION_MODEL_EXTRA);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mBinding = NotificationBinding.inflate(inflater, container, false);
        if (mModel != null) {
            String notificationDescription =
                    new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_APP_NOTIFICATION))
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE, mModel.getTitle())
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY, mModel.getSubtext())
                            .toString();
            mBinding.layoutBackground.announceForAccessibility(notificationDescription);
            mBinding.layoutBackground.setContentDescription(notificationDescription);
        }

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mModel != null) {

            if (mModel.getIconId() != 0) {
                mBinding.icon.setImageResource(mModel.getIconId());
                mBinding.icon.setVisibility(View.VISIBLE);
            } else {
                mBinding.icon.setVisibility(View.GONE);
            }

            if (TextUtils.isEmpty(mModel.getTitle())) {
                mBinding.title.setVisibility(View.GONE);
            } else {
                mBinding.title.setVisibility(View.VISIBLE);
                mBinding.title.setText(mModel.getTitle());
            }

            if (!TextUtils.isEmpty(mModel.getSubtext())) {
                mBinding.subtext.setText(mModel.getSubtext());
                mBinding.subtext.setVisibility(View.VISIBLE);
            } else {
                mBinding.subtext.setVisibility(View.GONE);
                mBinding.title.setMaxLines(getResources().getInteger(R.integer.notification_description_max_line));
            }

            if (!TextUtils.isEmpty(mModel.getErrorCode())) {
                StringBuilder sb = new StringBuilder();
                sb.append(mTranslator.get(TranslationKey.ERR_CODE));

                sb.append(" ");
                sb.append(mModel.getErrorCode());

                mBinding.errorCodeName.setText(sb);
                mBinding.errorCodeName.setVisibility(View.VISIBLE);
            } else {
                mBinding.errorCodeName.setVisibility(View.GONE);
            }
        } else {
            mBinding.errorCodeName.setVisibility(View.GONE);
        }
        initBranding();
    }

    @Override
    public boolean isDisplayed(BaseNotification current) {
        return current instanceof Notification &&
                ((Notification) current).getModel() != null &&
                ((Notification) current).getModel().equals(mModel);
    }

    /**
     * @return True if the notification should be shown immediately
     * even if there are other notifications in the queue.
     */
    @Override
    public boolean skippDisplayQueue() {
        return mModel != null && mModel.mSkippDisplayQueue;
    }

    @Override
    public long getPriority() {
        if (mModel != null) {
            return mModel.getPriority();
        }

        return DEFAULT_PRIORITY;
    }

    @Override
    public Timeout getTimeOut() {
        if (mModel != null) {
            return mModel.getTimeOut();
        }
        return mTimeOut;
    }

    @Override
    public void setTimeOut(Timeout timeOut) {
        if (mModel != null) {
            mModel.setTimeout(timeOut);
            return;
        }
        mTimeOut = timeOut;
    }

    public NotificationModel getModel() {
        return mModel;
    }

    /**
     * Builder class for the notifications.
     */
    public static class Builder {

        @DrawableRes
        protected int mIconId;

        protected String mTitle;
        protected String mSubtext;
        private String mErrorCode;
        private boolean mSkippDisplayQueue;

        private Timeout mTimeOut;
        private int mPriority;

        public Builder() {
            mPriority = DEFAULT_PRIORITY;
            mTimeOut = Timeout.LENGTH_MEDIUM;
        }

        public Builder title(String title) {
            mTitle = title;
            return this;
        }

        public Builder subtext(String subText) {
            mSubtext = subText;
            return this;
        }

        public Builder errorCode(String errorCode) {
            if (errorCode != null && !errorCode.isEmpty()) {
                mErrorCode = errorCode;
            }
            return this;
        }

        public Builder iconId(int iconId) {
            mIconId = iconId;
            return this;
        }

        public Builder timeout(Timeout timeout) {
            mTimeOut = timeout;
            return this;
        }

        public Builder priority(int priority) {
            mPriority = priority;
            return this;
        }

        /**
         * @param skippDisplayQueue True if the notification should be shown
         *                          immediately even if there are other notifications in the queue.
         */
        public Builder skippDisplayQueue(boolean skippDisplayQueue) {
            mSkippDisplayQueue = skippDisplayQueue;
            return this;
        }

        public Notification build() {
            NotificationModel model = new NotificationModel(
                    mTitle, mSubtext, mErrorCode, mIconId, mTimeOut, mSkippDisplayQueue, mPriority);
            return Notification.newInstance(model);
        }
    }

    /**
     * Model for notifications.
     * <p>
     * Contains title, error code and timeout.
     */
    protected static class NotificationModel implements Serializable {
        private final String mTitle;
        private final String mSubtext;
        private final String mErrorCode;
        private final int mIconId;
        private final boolean mSkippDisplayQueue;
        private Timeout mTimeOut;
        private final int mPriority;

        public NotificationModel(String title, String subtext, int iconId) {
            this(title, subtext, null, iconId, Timeout.LENGTH_MEDIUM, false, BaseNotification.DEFAULT_PRIORITY);
        }

        public NotificationModel(String title, String errorCode, int iconId, Timeout timeOut, boolean skippDisplayQueue, int priority) {
            this(title, null, errorCode, iconId, timeOut, skippDisplayQueue, priority);
        }

        public NotificationModel(String title, String subtext, String errorCode, int iconId, Timeout timeOut,
                                 boolean skippDisplayQueue, int priority) {
            mTitle = title;
            mSubtext = subtext;
            mErrorCode = errorCode;
            mIconId = iconId;
            mTimeOut = timeOut;
            mSkippDisplayQueue = skippDisplayQueue;
            mPriority = priority;
        }

        public Timeout getTimeOut() {
            return mTimeOut;
        }

        public int getPriority() {
            return mPriority;
        }

        public String getTitle() {
            return mTitle;
        }

        public String getSubtext() {
            return mSubtext;
        }

        public String getErrorCode() {
            return mErrorCode;
        }

        public int getIconId() {
            return mIconId;
        }

        public void setTimeout(Timeout timeout) {
            mTimeOut = timeout;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof NotificationModel)) {
                return false;
            }

            NotificationModel other = (NotificationModel) o;
            return Objects.equals(this.mTitle, other.mTitle)
                    && Objects.equals(this.mSubtext, other.mSubtext) && Objects.equals(this.mErrorCode, other.mErrorCode);
        }
    }

    private void initBranding() {
        int backgroundColor = ColorUtils.applyTransparencyToColor(
                mLayoutConfig.getNotificationBackgroundColor(),
                ALPHA_BACKGROUND
        );
        mBinding.layoutBackground.setBackgroundColor(backgroundColor);
        mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        mBinding.subtext.setTextColor(mLayoutConfig.getFontColor());
        mBinding.errorCodeName.setTextColor(mLayoutConfig.getFontColor());
    }
}
