package tv.threess.threeready.ui.tv.presenter.channel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.leanback.widget.Presenter;

import com.bumptech.glide.Glide;

import java.util.Objects;

import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.tv.model.ReplayChannelPageItem;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.ChannelCardBinding;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.generic.presenter.BaseModularCardPresenter;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.utils.TitleFallbackImageViewTarget;

/**
 * Presenter class for {@link ReplayChannelPageItem} tile card.
 *
 * @author David Bondor
 * @since 4.28.2022
 */
public class ReplayChannelPageCardPresenter extends BaseModularCardPresenter<ReplayChannelPageCardPresenter.ViewHolder, ReplayChannelPageItem> {

    public ReplayChannelPageCardPresenter(Context context) {
        super(context);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        ChannelCardBinding binding = ChannelCardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.background.setBackgroundColor(mLayoutConfig.getPlaceHolderColor());
        holder.mBinding.logoText.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBinding.number.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        return holder;
    }

    @Override
    public void onBindHolder(ModuleData<?> moduleData, final ViewHolder holder, final ReplayChannelPageItem channel) {
        super.onBindHolder(moduleData, holder, channel);

        TvChannel tvChannel = channel.getTvChannel();

        Glide.with(mContext).clear(holder.mBinding.logo);
        Glide.with(mContext)
                .load(tvChannel)
                .into(new TitleFallbackImageViewTarget(
                        holder.mBinding.logo,
                        holder.mBinding.logoText,
                        tvChannel.getName()));

        holder.mBinding.number.setText(String.valueOf(tvChannel.getNumber()));
        updateContentDescription(moduleData, holder, tvChannel);
    }

    @Override
    public void onClicked(ModuleData<?> moduleData, ViewHolder holder, ReplayChannelPageItem replayChannelPageItem) {
        super.onClicked(moduleData, holder, replayChannelPageItem);

        // Open page.
        mNavigator.showDynamicPage(replayChannelPageItem.getPageConfig());
    }

    @Override
    public long getStableId(ReplayChannelPageItem replayChannelPageItem) {
        return Objects.hash(replayChannelPageItem.getId());
    }

    @Override
    public int getCardWidth(@Nullable ModuleData<?> moduleData, ReplayChannelPageItem replayChannelPageItem) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_channel_card_width);
    }

    @Override
    public int getCardHeight(@Nullable ModuleData<?> moduleData, ReplayChannelPageItem replayChannelPageItem) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_channel_card_height);
    }

    /**
     * Updates the content description used for Talkback.
     */
    private void updateContentDescription(ModuleData<?> moduleData, ViewHolder holder, TvChannel channel) {
        if (!mMwRepository.isTalkBackOn()) {
            return;
        }

        TalkBackMessageBuilder message = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_CARDS_CHANNEL_OTHER_TILE));
        if ((Integer) holder.view.getTag(R.integer.normalized_adapter_position) == 0) {
            message = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_CARDS_CHANNEL_FIRST_TILE))
                    .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_SWIMLANE, mTranslator.get(moduleData.getTitle()));
        }
        holder.view.setContentDescription(message
                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_CHANNEL_NAME, channel.getName())
                .toString());
    }

    public static class ViewHolder extends BaseCardPresenter.ViewHolder {

        protected final ChannelCardBinding mBinding;

        public ViewHolder(ChannelCardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
