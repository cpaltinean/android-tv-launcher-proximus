package tv.threess.threeready.ui.generic.model;

import androidx.annotation.Nullable;

import java.util.List;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.PlayerConfig;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.PlayOption;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.pvr.model.SingleRecordingStatus;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;

/**
 * Recording wrapper class which holds all the necessary information to start the playback.
 *
 * @author David Bondor
 * @since 19.07.2022
 */
public class RecordingPlayOption extends BroadcastPlayOption<IRecording> {

    private final IRecordingSeries mSeries;

    public RecordingPlayOption(Type type, IRecording broadcast, IRecordingSeries series) {
        super(type, broadcast);

        mSeries = series;
    }

    @Nullable
    public IRecordingSeries getRecordingSeries() {
        return mSeries;
    }

    /**
     * Generate available playback option for a broadcast.
     *
     * @param channel           The channel on which the broadcast is available.
     * @param broadcast         the broadcast for which the playback options should be generated.
     * @param bookmark          the bookmark corresponding to the given IBroadcast
     * @param recordingStatus   the current recording status of the broadcast
     * @param recordingPriority if true ignores all the other playback options, returns only the recording if available.
     * @param replayPriority    if the replay playback options should be prioritized over live.
     * @return Returns all available playback option.
     */
    public static List<PlayOption> generatePlayOptions(@Nullable TvChannel channel,
                                                       IBroadcast broadcast,
                                                       IRecordingSeries recordingSeries,
                                                       IBookmark bookmark,
                                                       @Nullable RecordingStatus recordingStatus,
                                                       boolean recordingPriority, boolean replayPriority) {
        PlayOption livePlayOption = getLivePlayOption(broadcast, channel, bookmark, replayPriority);
        PlayOption replayPlayOption = getReplayPlayOption(broadcast, channel);
        PlayOption recordingPlayOption = getSeriesRecordingPlayOption(recordingStatus, recordingSeries);

        return generateFinalOptions(livePlayOption, recordingPlayOption, replayPlayOption, recordingPriority);
    }

    /**
     * Generate available playback option for a recording.
     *
     * @param channel           The channel on which the broadcast is available.
     * @param broadcast         the broadcast for which the playback options should be generated.
     * @param recordingPriority if true ignores all the other playback options, returns only the recording if available.
     * @return Returns all available playback option.
     */
    public static List<PlayOption> generateRecordingPlayOptions(@Nullable TvChannel channel,
                                                                IRecording broadcast,
                                                                IRecordingSeries series,
                                                                boolean recordingPriority) {

        PlayOption livePlayOption = getLivePlayOption(broadcast, channel, null, false);
        PlayOption replayPlayOption = getReplayPlayOption(broadcast, channel);
        PlayOption recordingPlayOption = null;

        if (broadcast.isLiveRecording() || broadcast.isRecordingFinished()) {
            if (!broadcast.isFailedRecording()) {
                recordingPlayOption = new RecordingPlayOption(Type.RECORDING, broadcast, series);
            }
        }

        return generateFinalOptions(livePlayOption, recordingPlayOption, replayPlayOption, recordingPriority);
    }

    /**
     * @return The recording play option with the series.
     */
    protected static PlayOption getSeriesRecordingPlayOption(RecordingStatus recordingStatus, IRecordingSeries recordingSeries) {
        SingleRecordingStatus singleRecordingStatus = recordingStatus == null ?
                null : recordingStatus.getSingleRecordingStatus();

        if (singleRecordingStatus == SingleRecordingStatus.RECORDING) {
            long startOverAppearingDelay = Components.get(PlayerConfig.class)
                    .getStartOverAppearingDelay(TimeUnit.MILLISECONDS);
            // If the recording only started recently, we need to wait 25 until we are able to show the stream
            IRecording recording = recordingStatus.getRecording();
            if (recording != null && !recording.isFailedRecording()) {
                if (System.currentTimeMillis() - recording.getRecordingStart() > startOverAppearingDelay) {
                    return new RecordingPlayOption(Type.RECORDING, recording, recordingSeries);
                }
            }
        } else if (singleRecordingStatus == SingleRecordingStatus.COMPLETED) {
            IRecording recording = recordingStatus.getRecording();
            if (recording != null && !recording.isFailedRecording()) {
                return new RecordingPlayOption(Type.RECORDING, recording, recordingSeries);
            }
        }

        return null;
    }
}