/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.startup.step;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.generic.navigation.Navigator;

/**
 * Show the project specific startup page.
 *
 * @author Barabas Attila
 * @since 2018.06.29
 */
public class WelcomeScreenStep implements Step {

    private final Navigator mNavigator = Components.get(Navigator.class);

    @Override
    public void execute(StepCallback callback) {
        mNavigator.showWelcomeScreen();
        callback.onFinished();
    }
}
