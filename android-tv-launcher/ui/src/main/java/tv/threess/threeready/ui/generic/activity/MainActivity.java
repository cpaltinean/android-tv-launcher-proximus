/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.LayoutInflater;

import com.bumptech.glide.Glide;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.account.setting.MemoryCache;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.FeatureControl;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.generic.receivers.StandByStateChangeReceiver;
import tv.threess.threeready.api.home.model.error.ErrorConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Error;
import tv.threess.threeready.api.log.model.ErrorHandler;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.GenericEvent;
import tv.threess.threeready.api.log.model.GenericKeyEvent;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.search.model.GlobalSearchType;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.databinding.MainActivityBinding;
import tv.threess.threeready.ui.generic.activity.helper.KeySniffer;
import tv.threess.threeready.ui.generic.dialog.AlertDialog;
import tv.threess.threeready.ui.generic.dialog.BaseDialogFragment;
import tv.threess.threeready.ui.generic.dialog.ChannelSwitchDialog;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.notification.BaseNotification;
import tv.threess.threeready.ui.generic.notification.Notification;
import tv.threess.threeready.ui.generic.notification.NotificationManager;
import tv.threess.threeready.ui.generic.notification.OfflineModeNotification;
import tv.threess.threeready.ui.generic.player.fragment.PlayerFragment;
import tv.threess.threeready.ui.generic.receiver.GlobalKeyReceiver;
import tv.threess.threeready.ui.generic.receiver.RemoteNotPairedReceiver;
import tv.threess.threeready.ui.generic.utils.ErrorHelper;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.pvr.RecordingActionHelper;
import tv.threess.threeready.ui.search.activity.BaseGlobalSearchHandlerActivity;
import tv.threess.threeready.ui.startup.StartupFlow;
import tv.threess.threeready.ui.startup.types.FlowTypes;
import tv.threess.threeready.ui.update.CasUpdateDialogManager;
import tv.threess.threeready.ui.update.SystemUpdateDialogManager;
import tv.threess.threeready.ui.utils.SystemUtils;

/**
 * Main and launcher activity of the application.
 * Displayed after boot complete and home button press.
 * <p>
 * Created by Barabas Attila on 4/11/2017.
 */

public class MainActivity extends BaseActivity implements RemoteNotPairedReceiver.Listener {
    private static final String TAG = Log.tag(MainActivity.class);

    private static final int SEQUENCE_DEV_MODE = 101;
    private static final int SEQUENCE_DEV_MODE_PLAYER = 102;
    //Constant to show offline mode notification only after 10 seconds.
    private static final int SHOW_OFFLINE_MODE_NOTIFICATION_AFTER_MS = 10000;

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final NotificationManager mNotificationManager = Components.get(NotificationManager.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final ErrorHelper mErrorHelper = Components.get(ErrorHelper.class);
    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);
    private final RecordingActionHelper mRecordingActionHelper = Components.get(RecordingActionHelper.class);
    private final StartupFlow mStartupFlow = Components.get(StartupFlow.class);
    private final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);
    private final StandByStateChangeReceiver mStandByStateChangeReceiver = Components.get(StandByStateChangeReceiver.class);

    private final RemoteNotPairedReceiver mRemoteNotPairedReceiver;
    private final KeySniffer mKeySniffer;
    private final Handler mHandlerOfflineNotification = new Handler();
    private final SystemUpdateDialogManager mSystemUpdateDialogManager = new SystemUpdateDialogManager();
    private final CasUpdateDialogManager mCasUpdateReceiver = new CasUpdateDialogManager();

    private OfflineModeNotification mOfflineModeNotification;

    // Enable internet checks while the app is in foreground.
    private final InternetChecker.InternetCheckEnabler mInternetCheckEnabler = () -> !isInBackground();

    private long mActivityStartUptime = SystemClock.uptimeMillis();
    private long mIntentReceivedTime;
    private boolean mWasRestarted;
    private boolean mWasInStandby;

    public MainActivity() {
        mRemoteNotPairedReceiver = new RemoteNotPairedReceiver();
        mKeySniffer = new KeySniffer();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate. Recreate : " + (savedInstanceState != null));

        // Pass null instance state to prevent fragment recreation.
        super.onCreate(null);
        mWasRestarted = savedInstanceState != null;

        Intent intent = getIntent();
        Log.d(TAG, "Has home category : " + intent.hasCategory(Intent.CATEGORY_HOME));
        if (!intent.hasCategory(Intent.CATEGORY_HOME)) {
            Log.w(TAG, "Activity launched as standard. Restart as home.");
            SystemUtils.restartActivityAsHome(this);
            return;
        }

        MainActivityBinding binding = MainActivityBinding.inflate(LayoutInflater.from(this));
        setContentView(binding.getRoot());

        mInternetChecker.addEnabler(mInternetCheckEnabler);
        mStandByStateChangeReceiver.addStateChangedListener(mStandbyChangeListener);

        mNavigator.init(this, binding.fragmentOverlay);
        mNotificationManager.init(this);
        mSystemUpdateDialogManager.onCreate();
        mCasUpdateReceiver.onCreate();

        mKeySniffer.registerSequence(SEQUENCE_DEV_MODE, id -> {
                    if (id == SEQUENCE_DEV_MODE) {
                        mNavigator.showSecretPage();
                    }
                },
                KeyEvent.KEYCODE_DPAD_UP,
                KeyEvent.KEYCODE_DPAD_UP,
                KeyEvent.KEYCODE_DPAD_LEFT,
                KeyEvent.KEYCODE_DPAD_LEFT,
                KeyEvent.KEYCODE_DPAD_DOWN,
                KeyEvent.KEYCODE_DPAD_DOWN,
                KeyEvent.KEYCODE_DPAD_RIGHT,
                KeyEvent.KEYCODE_DPAD_RIGHT,
                KeyEvent.KEYCODE_DPAD_UP,
                KeyEvent.KEYCODE_DPAD_LEFT,
                KeyEvent.KEYCODE_DPAD_DOWN,
                KeyEvent.KEYCODE_DPAD_RIGHT);

        mKeySniffer.registerSequence(SEQUENCE_DEV_MODE_PLAYER, id -> {
                    if (id == SEQUENCE_DEV_MODE_PLAYER) {
                        mNavigator.showSecretPlayerPage();
                    }
                },
                KeyEvent.KEYCODE_DPAD_UP,
                KeyEvent.KEYCODE_DPAD_LEFT,
                KeyEvent.KEYCODE_DPAD_DOWN,
                KeyEvent.KEYCODE_DPAD_RIGHT,
                KeyEvent.KEYCODE_DPAD_UP,
                KeyEvent.KEYCODE_DPAD_UP,
                KeyEvent.KEYCODE_DPAD_LEFT,
                KeyEvent.KEYCODE_DPAD_LEFT,
                KeyEvent.KEYCODE_DPAD_DOWN,
                KeyEvent.KEYCODE_DPAD_DOWN,
                KeyEvent.KEYCODE_DPAD_RIGHT,
                KeyEvent.KEYCODE_DPAD_RIGHT);


        mStartupFlow.setStartupFlowType(
                Settings.ftiFinished.get(false)
                        ? FlowTypes.START_UP : FlowTypes.FTI);

        mNavigator.setStartupFlow(mStartupFlow);
        ReportUtils.reportPowerCycle(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d(TAG, "onNewIntent. Startup finished : "
                + mStartupFlow.isStartupFinished() + " Intent : " + intent);

        super.onNewIntent(intent);
        MemoryCache.LastUserActivity.edit().put(System.currentTimeMillis());

        setIntent(intent);
        if (mStartupFlow.isStartupFinished()) {
            handleIntent(intent);
        }
    }

    /**
     * Called when the user select a result in global search.
     *
     * @param resultId   The unique identifier of the channel, broadcast or vod result.
     * @param resultType The type of the search result.
     */
    private void onGlobalSearchResultSelected(String resultId, ParentalRating rating, GlobalSearchType resultType) {
        Log.d(TAG, "onGlobalSearchResultSelected . ResultId : "
                + resultId + ", result type : " + resultType);

        switch (resultType) {
            case Movie:
                if (mParentalControlManager.isRestricted(rating)) {
                    mNavigator.showParentalControlUnblockDialog(enteredPin -> mNavigator.showVodMovieDetails(resultId));
                } else {
                    mNavigator.showVodMovieDetails(resultId);
                }
                break;
            case Program:
                if (mParentalControlManager.isRestricted(rating)) {
                    mNavigator.showParentalControlUnblockDialog(enteredPin -> mNavigator.showProgramDetails(resultId));
                } else {
                    mNavigator.showProgramDetails(resultId);
                }
                break;
            case Channel:
                PlayerFragment playerFragment = mNavigator.getPlayerFragment();
                if (playerFragment != null) {
                    playerFragment.getChannelInfoView().zapToChannel(
                            StartAction.Search, resultId, false);
                }
                break;
        }
    }

    /**
     * Initialize Offline Mode Notification.
     * Called from @{@link tv.threess.threeready.ui.startup.step.StartupCompletedStep}
     */
    public void initializeOfflineModeNotification(OfflineModeNotification mOfflineModeNotification) {
        this.mOfflineModeNotification = mOfflineModeNotification;
        this.mOfflineModeNotification.enableAutoShow(this);
    }


    public boolean wasApplicationRestarted() {
        return mWasRestarted;
    }

    @Override
    public void setIntent(Intent intent) {
        super.setIntent(intent);
        mIntentReceivedTime = System.currentTimeMillis();
    }

    /**
     * @return The timestamp when the activity last time received an intent.
     */
    public long getIntentReceivedTime() {
        return mIntentReceivedTime;
    }

    /**
     * Handle and consume the deep link actions from the intent.
     */
    public void handleIntent(Intent intent) {
        if (intent == null || intent.getAction() == null) {
            return;
        }

        Log.d(TAG, "Handle intent. Intent : " + intent.getAction());
        switch (intent.getAction()) {
            // Handle global search result selection.
            case BaseGlobalSearchHandlerActivity.ACTION_SEARCH_RESULT:
                String resultId = intent.getStringExtra(BaseGlobalSearchHandlerActivity.EXTRA_SEARCH_RESULT_ID);
                ParentalRating rating = (ParentalRating) intent.getSerializableExtra(BaseGlobalSearchHandlerActivity.EXTRA_PARENTAL_RATING);
                GlobalSearchType resultType = (GlobalSearchType) intent.getSerializableExtra(
                        BaseGlobalSearchHandlerActivity.EXTRA_SEARCH_RESULT_TYPE);
                onGlobalSearchResultSelected(resultId, rating, resultType);
                //reset intent
                intent.setAction(android.content.Intent.ACTION_MAIN);
                intent.removeExtra(BaseGlobalSearchHandlerActivity.EXTRA_SEARCH_RESULT_ID);
                intent.removeExtra(BaseGlobalSearchHandlerActivity.EXTRA_PARENTAL_RATING);
                intent.removeExtra(BaseGlobalSearchHandlerActivity.EXTRA_SEARCH_RESULT_TYPE);
                return;

            // Handle global keys.
            default:
                try {
                    KeyEvent keyEvent;

                    // get the key event
                    if (intent.hasExtra(Intent.EXTRA_KEY_EVENT)) {
                        keyEvent = intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
                    } else {
                        keyEvent = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HOME);
                    }

                    // No key extra. Skip handling
                    if (keyEvent == null) {
                        Log.w(TAG, "Skip intent. No key extra.");
                        return;
                    }

                    // process the key event
                    if (keyEvent.getAction() == KeyEvent.ACTION_MULTIPLE) {
                        onKeyMultiple(keyEvent.getKeyCode(), keyEvent.getRepeatCount(), keyEvent);
                    } else if (keyEvent.getAction() == KeyEvent.ACTION_UP) {
                        onKeyUp(keyEvent.getKeyCode(), keyEvent);
                    } else {
                        onKeyDown(keyEvent.getKeyCode(), keyEvent);
                        if (keyEvent.isLongPress()) {
                            onKeyLongPress(keyEvent.getKeyCode(), keyEvent);
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Failed to process key event intent:" + intent, e);
                }
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        mActivityStartUptime = SystemClock.uptimeMillis();
        MemoryCache.LastUserActivity.edit().put(System.currentTimeMillis());
        mRemoteNotPairedReceiver.register(this, this);
        mInternetChecker.addStateChangedListener(mOnStateChangedListener);
        mSystemUpdateDialogManager.onStart();
        mRecordingActionHelper.onStart();
        mHandlerOfflineNotification.removeCallbacksAndMessages(null);

        // Restart startup flow if needed.
        if (mStartupFlow.isStartupFinished()) {

            // Start standby flow.
            if (mWasInStandby) {
                mParentalControlManager.onWakeFromStandby();
                mStartupFlow.startStartupFlow(FlowTypes.STAND_BY);
                mWasInStandby = false;
            }

        // Continue the startup flow
        } else {
            mStartupFlow.onStart();
        }

        Log.event(new Event<>(UILogEvent.AppStarted)
                .putDetail(UILogEvent.Detail.PackageName, getApplicationContext().getPackageName())
                .putDetail(UILogEvent.Detail.StartAction, StartAction.Startup)
        );

        try {
            Components.get(MwRepository.class).sendUsageStats();
        } catch (Exception e) {
            Log.e(TAG, "Failed to start usage stats service.", e);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Components.get(MwRepository.class).clearTalkBackCache();
        if (mOfflineModeNotification != null && mStartupFlow.isStartupFinished()) {
            mOfflineModeNotification.enableAutoShow(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mNavigator.onActivityPaused();
        if (mOfflineModeNotification != null) {
            mOfflineModeNotification.disableAutoShow();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        mNavigator.onActivityStopped();
        mRemoteNotPairedReceiver.unregister(this);
        mHandlerOfflineNotification.removeCallbacksAndMessages(null);
        mSystemUpdateDialogManager.onStop();
        mRecordingActionHelper.onStop();

        mInternetChecker.removeStateChangedListener(mOnStateChangedListener);
        mStartupFlow.onStop();

        GlobalKeyReceiver.enable(this);

        Glide.get(getApplicationContext()).clearMemory();
        Log.d(TAG, "Glide - clear memory");

        Log.event(new Event<>(UILogEvent.AppStopped)
                .putDetail(UILogEvent.Detail.PackageName, getApplicationContext().getPackageName())
                .addDetail(UILogEvent.Detail.Duration, SystemClock.uptimeMillis() - mActivityStartUptime)
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mNavigator.destroy();
        mNotificationManager.destroy();
        mCasUpdateReceiver.onDestroy();
        mSystemUpdateDialogManager.onDestroy();
        mInternetChecker.removeEnabler(mInternetCheckEnabler);
        mStandByStateChangeReceiver.removeStateChangedListener(mStandbyChangeListener);
    }

    @Override
    public void onError(Error error) {
        Log.d(TAG, "onError. Error : " + error);

        // Dispatch error to notification.
        BaseNotification notification = mNotificationManager.getNotification();
        if (notification != null && notification.onError(error)) {
            return;
        }

        // Dispatch error to content fragment
        BaseFragment currentFragment = mNavigator.getContentFragment();
        if (currentFragment != null && currentFragment.onError(error)) {
            return;
        }

        if (error.getErrorHandler() != ErrorHandler.GENERIC) {
            // No need to show generic errors for custom handlers.
            return;
        }

        // Generic error handling.
        ErrorConfig errorHandler = mErrorHelper.getErrorConfig(error);
        switch (errorHandler.getDisplayMode()) {

            // Display error message in player.
            case PLAYBACK_ERROR:
                PlayerFragment playerFragment = mNavigator.getPlayerFragment();
                if (playerFragment != null) {
                    playerFragment.onError(error);
                }
                break;

            // Display error message as notification.
            case NOTIFICATION:
                notification = new Notification.Builder()
                        .title(mErrorHelper.getErrorMessage(error))
                        .errorCode(error.getErrorCode())
                        .priority(errorHandler.getPriority())
                        .build();
                mNotificationManager.showNotification(notification);
                break;

            // Display error message as dialog.
            case ALERT:
                AlertDialog alertDialog = new AlertDialog.Builder()
                        .title(mErrorHelper.getErrorHeader(error))
                        .description(mErrorHelper.getErrorMessage(error))
                        .errorCode(error.getErrorCode())
                        .addButton(mTranslator.get(TranslationKey.OK_BUTTON))
                        .priority(errorHandler.getPriority())
                        .dismissOnBtnClick(true)
                        .build();

                mNavigator.showDialog(alertDialog);
                break;
        }
    }

    /**
     * Dispatch key events to the current content fragment.
     */
    @SuppressLint("RestrictedApi")
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        Log.d(TAG, "dispatchKeyEvent. KeyEvent : " + event);

        MemoryCache.LastUserActivity.edit().put(System.currentTimeMillis());
        mKeySniffer.consumeEvent(event);

        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                Log.event(new Event<>(UILogEvent.PageSelection)
                        .addDetail(UILogEvent.Detail.OpenedBy, UILog.OpenedBy.Back));
            }
        }

        if (event.getAction() == KeyEvent.ACTION_UP) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    UILog.logButtonPressFromRemote(UILog.ButtonType.OK);
                    break;
                case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                    UILog.logButtonPressFromRemote(UILog.ButtonType.PLAYPAUSE);
                    break;
                case KeyEvent.KEYCODE_MEDIA_STOP:
                    UILog.logButtonPressFromRemote(UILog.ButtonType.STOP);
                    break;
                case KeyEvent.KEYCODE_MEDIA_RECORD:
                    UILog.logButtonPressFromRemote(UILog.ButtonType.RECORD);
                    break;
                case KeyEvent.KEYCODE_BACK:
                    UILog.logButtonPressFromRemote(UILog.ButtonType.BACK);
                    break;
                case KeyEvent.KEYCODE_CAPTIONS:
                    UILog.logButtonPressFromRemote(UILog.ButtonType.SUB);
            }
        }

        BaseDialogFragment dialogFragment = mNavigator.getDialogFragment();
        if (dialogFragment != null && dialogFragment.isAdded()
                && dialogFragment.dispatchKeyEvent(event)) {
            return true;
        }

        // Dispatch key event to content fragment
        BaseFragment currentFragment = mNavigator.getContentFragment();
        if (currentFragment != null && currentFragment.isAdded()
                && currentFragment.dispatchKeyEvent(event)) {
            return true;
        }

        // Dispatch key event to player fragment
        PlayerFragment playerFragment = mNavigator.getPlayerFragment();
        if (playerFragment != null && playerFragment.isAdded()
                && playerFragment.dispatchKeyEvent(event)) {
            return true;
        }

        return super.dispatchKeyEvent(event);
    }

    @Override
    public void onBackPressed() {
        BaseFragment currentFragment = mNavigator.getContentFragment();
        PlayerFragment playerFragment = mNavigator.getPlayerFragment();

        // Dispatch back event to the current fragment.
        if (currentFragment != null && currentFragment.onBackPressed()) {
            return;
        }

        // Dispatch back event to the player fragment.
        if (playerFragment != null && playerFragment.onBackPressed()) {
            return;
        }

        // Prevent closing the main activity.
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            if (mNavigator.isContentOverlayDisplayed()) {
                mNavigator.hideContentOverlay();
            } else {
                mNavigator.showContentOverlay();
            }
        } else if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            // BackStack empty show home by default.
            mNavigator.showHome();
        } else {
            getSupportFragmentManager().popBackStackImmediate();
            mNavigator.showContentOverlay();
        }
    }

    /**
     * @return True if the startup flow already finished.
     */
    public boolean isStartupFinished() {
        return mStartupFlow.isStartupFinished() && !mWasInStandby;
    }

    /**
     * Dispatch key down events to the current content and player fragment.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyDown. Key event : " + event);

        if (keyCode == KeyEvent.KEYCODE_HOME) {
            UILog.logButtonPressFromRemote(UILog.ButtonType.HOME);
            Log.event(new Event<>(UILogEvent.PageSelection)
                    .addDetail(UILogEvent.Detail.OpenedBy, UILog.OpenedBy.Home));
        }

        // Consume play/pause. Prevent other playerFragment to handle it.
        if ((event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE)
                && !mNavigator.isStreamDisplayedOnTop()) {
            return true;
        }

        // Dispatch key event to notification
        BaseNotification notification = mNotificationManager.getNotification();
        if (notification != null && notification.isVisible()
                && notification.onKeyDown(event)) {
            return true;
        }

        BaseDialogFragment dialogFragment = mNavigator.getDialogFragment();
        if (dialogFragment != null && dialogFragment.isVisible()
                && dialogFragment.onKeyDown(event)) {
            return true;
        }

        // Dispatch key event to content fragment
        BaseFragment currentFragment = mNavigator.getContentFragment();
        if (currentFragment != null && currentFragment.isVisible()
                && currentFragment.onKeyDown(event)) {
            return true;
        }

        // Dispatch key event to player fragment
        BaseFragment playerFragment = mNavigator.getPlayerFragment();
        if (playerFragment != null && playerFragment.isVisible()
                && playerFragment.onKeyDown(event)) {
            return true;
        }

        switch (event.getKeyCode()) {

            // Clear backstack and show home.
            case KeyEvent.KEYCODE_HOME: {
                // Clear the opened dialogs
                mNavigator.clearAllDialogs();
                mNavigator.clearBackStack();
                mNavigator.showHome();
                return true;
            }

            // Consume play/pause. Prevent other applications like Play Music to handle it.
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    /**
     * Dispatch key up events to the current content fragment.
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyUp. Key event : " + event);

        if (event.getKeyCode() == KeyEvent.KEYCODE_GUIDE) {
            Log.event(new Event<>(UILogEvent.PageSelection)
                    .addDetail(UILogEvent.Detail.OpenedBy, UILog.OpenedBy.TvGuide));
        }

        // Consume play/pause. Prevent other playerFragment to handle it.
        if ((event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE)
                && !mNavigator.isStreamDisplayedOnTop()) {
            return true;
        }

        // Dispatch key event to dialog fragment
        BaseDialogFragment dialogFragment = mNavigator.getDialogFragment();
        if (dialogFragment != null && dialogFragment.isVisible()
                && dialogFragment.onKeyUp(event)) {
            return true;
        }

        // Dispatch key event to content fragment
        BaseFragment currentFragment = mNavigator.getContentFragment();
        if (currentFragment != null && currentFragment.isVisible()
                && currentFragment.onKeyUp(event)) {
            return true;
        }

        // Dispatch key event to content fragment
        BaseFragment playerFragment = mNavigator.getPlayerFragment();
        if (playerFragment != null && playerFragment.isVisible()
                && playerFragment.onKeyUp(event)) {
            return true;
        }

        if (ChannelSwitchDialog.handleExternalNumericalInput(this, mStartupFlow, mNavigator, event)) {
            return true;
        }

        return super.onKeyUp(keyCode, event);
    }

    /**
     * Dispatch long key press events to the current content fragment.
     */
    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyLongPress. KeyEvent : " + event);

        // Consume play/pause. Prevent other playerFragment to handle it.
        if ((event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE)
                && !mNavigator.isStreamDisplayedOnTop()) {
            return true;
        }

        if (keyCode == KeyEvent.KEYCODE_BACK
                && !Components.get(MwRepository.class).isDefaultLauncher()) {
            mNavigator.showHome();
            return true;
        }

        BaseDialogFragment dialogFragment = mNavigator.getDialogFragment();
        if (dialogFragment != null && dialogFragment.isVisible()
                && dialogFragment.onKeyLongPress(event)) {
            return true;
        }

        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            mNavigator.hideContentOverlay();
            return true;
        }

        // Dispatch key event to content fragment
        BaseFragment currentFragment = mNavigator.getContentFragment();
        if (currentFragment != null && currentFragment.isVisible()
                && currentFragment.onKeyLongPress(event)) {
            return true;
        }

        // Dispatch key event to player fragment
        BaseFragment playerFragment = mNavigator.getPlayerFragment();
        if (playerFragment != null && playerFragment.isVisible()
                && playerFragment.onKeyLongPress(event)) {
            return true;
        }

        return super.onKeyLongPress(keyCode, event);
    }

    /**
     * Dispatch user interaction events to the current content fragment.
     */
    @Override
    public void onUserInteraction() {
        // Dispatch user interaction event to dialog fragment
        BaseDialogFragment dialogFragment = mNavigator.getDialogFragment();
        if (dialogFragment != null) {
            dialogFragment.onUserInteraction();
        }

        // Dispatch user interaction event to content fragment
        BaseFragment currentFragment = mNavigator.getContentFragment();
        if (currentFragment != null) {
            currentFragment.onUserInteraction();
        }

        // Dispatch user interaction event to player fragment
        BaseFragment playerFragment = mNavigator.getPlayerFragment();
        if (playerFragment != null) {
            playerFragment.onUserInteraction();
        }

        super.onUserInteraction();
    }

    @Override
    public void showPairRemoteNotification() {
        BaseNotification notification = new Notification.Builder()
                .title(mTranslator.get(TranslationKey.ERR_REMOTE_NOT_PAIRED_MIC_PRESSED_MESSAGE))
                .build();
        mNotificationManager.showNotification(notification);
    }

    private final StandByStateChangeReceiver.Listener mStandbyChangeListener = screenOn -> {
        if (!screenOn) {
            mWasInStandby = true;
        }
    };

    /**
     * Runnable used to show the offline mode notification.
     */
    public final Runnable offlineNotificationRunnable = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "Runnable Show offline mode notification.");
            mNotificationManager.showNotification(mOfflineModeNotification);
        }
    };

    // Detect and react on internet connectivity changes.
    private final InternetChecker.OnStateChangedListener mOnStateChangedListener = (state) -> {
        if (state.isBackendStateChanged()) {
            // log event: backend isolated / restored
            Log.event(new Event<>(GenericEvent.IsolationMode)
                    .addDetail(GenericKeyEvent.STARTUP_FINISHED, mStartupFlow.isStartupFinished())
                    .addDetail(GenericKeyEvent.BACKEND_AVAILABLE, state.isBackendAvailable())
            );
        }

        if (mWasInStandby) {
            // Ignore. The whole standby flow will be executed.
            return;
        }

        Log.d(TAG, "Internet state changed." + state);

        // Dispatch state change to startup flow.
        if (!mStartupFlow.isStartupFinished()) {
            return;
        }

        if (state.isInternetStateChanged(true) && Components.get(FeatureControl.class).isOffline()) {
            Log.d(TAG, "Internet is back. Force restart app.");
            SystemUtils.restartApplication(MainActivity.this);
        }

        // backend availability changed, and internet was and is available
        if (state.isBackendStateChanged() && !state.isInternetStateChanged() && state.isInternetAvailable()) {
            Log.d(TAG, "backend availability changed: " + state.isBackendAvailable());
            if (state.isBackendAvailable()) {
                mNavigator.showLeaveIsolationModeDialog();
            } else {
                mNavigator.showEnterIsolationModeDialog();
            }
        }

        // Backend is back start caching.
        if (state.isBackendAvailable() && (state.isBackendStateChanged() || state.isInternetStateChanged())) {
            Components.get(AccountRepository.class).startPerformCaching();
        }

        // Skipp UI actions. Activity is not in foreground.
        if (isInBackground()) {
            return;
        }

        // Displays/hides the offline notification
        if (mOfflineModeNotification == null) {
            return;
        }

        if (state.isInternetAvailable()) {
            // Handle offline mode dialog.
            Log.d(TAG, "Hide offline mode notification.");
            mNotificationManager.hideNotification(mOfflineModeNotification);
            mHandlerOfflineNotification.removeCallbacksAndMessages(null);
        } else {
            mHandlerOfflineNotification.postDelayed(offlineNotificationRunnable, SHOW_OFFLINE_MODE_NOTIFICATION_AFTER_MS);
        }
    };
}
