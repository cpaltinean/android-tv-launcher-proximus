package tv.threess.threeready.ui.startup.step;

import android.animation.Animator;

import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.FeatureControl;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.generic.activity.MainActivity;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.AnimatorListenerAdapter;
import tv.threess.threeready.ui.generic.utils.AnimatorUtils;
import tv.threess.threeready.ui.startup.fragment.StartFragment;

/**
 * Base Startup Finished step
 *
 * @author Solyom Zsolt
 * @since 17/02/2020
 */
public abstract class BaseStartupFinishedStep implements Step {

    protected final Navigator mNavigator = Components.get(Navigator.class);
    protected final FeatureControl mFeatureControl = Components.get(FeatureControl.class);
    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);

    protected MainActivity mActivity;

    private Animator mFadeOutAnimator;

    BaseStartupFinishedStep() {
        mActivity = mNavigator.getActivity();
    }

    /**
     * Base Startup Finished step. Hides animation and finishes startup flow.
     *
     * @param callback - callback to provide information about the execution status.
     */
    @Override
    public void execute(StepCallback callback) {
        mActivity.runOnUiThread(() -> {
            AnimatorUtils.cancelAnimators(mFadeOutAnimator);
            mFadeOutAnimator = getFadeoutAnimation();
            if (mFadeOutAnimator == null) {
                onStartupCompleted(callback);
                return;
            }

            mFadeOutAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationFinished(Animator animation) {
                    super.onAnimationFinished(animation);
                    onStartupCompleted(callback);
                }
            });
            mFadeOutAnimator.start();
        });
    }

    @Override
    public void cancel() {
        AnimatorUtils.cancelAnimators(mFadeOutAnimator);
    }

    private Animator getFadeoutAnimation() {
        BaseFragment fragment = mNavigator.getContentFragment();
        return (fragment instanceof StartFragment) ?
                ((StartFragment) fragment).getFadeOutAnimation() : null;
    }

    protected void onStartupCompleted(StepCallback callback) {
        callback.getStartupTimer().pause();
        callback.onFinished();
        mNavigator.clearBackStack();
        mNavigator.triggerParentalControl();
        if (mInternetChecker.isBackendIsolated()) {
            mNavigator.showEnterIsolationModeDialog();
        }
        showContentFragmentAfterStartup();
        Settings.startupFlowFinished.edit().put(System.currentTimeMillis());
        Log.event(new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.Startup)
                .addDetail(UILogEvent.Detail.StartupTime, callback.getStartupTimer().getDuration(TimeUnit.MILLISECONDS)));
    }

    protected abstract void showContentFragmentAfterStartup();

}
