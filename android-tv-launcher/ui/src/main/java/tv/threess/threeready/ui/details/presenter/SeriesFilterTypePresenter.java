package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.ui.databinding.SeriesFilterItemBinding;
import tv.threess.threeready.ui.details.utils.SeriesFilterType;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * @author Szende Pal
 * @since 2/24/2022.
 */
public class SeriesFilterTypePresenter extends BasePresenter<SeriesFilterTypePresenter.ViewHolder, SeriesFilterType> {
    private final Translator mTranslator = Components.get(Translator.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    private final ISeriesFilterSelectionListener mFilterSelectionListener;
    private final ISelectedSeriesFilterListener mSelectedFilterListener;

    public SeriesFilterTypePresenter(Context context, ISeriesFilterSelectionListener filterSelectionListener, ISelectedSeriesFilterListener selectedFilter) {
        super(context);
        mFilterSelectionListener = filterSelectionListener;
        mSelectedFilterListener = selectedFilter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        SeriesFilterItemBinding binding = SeriesFilterItemBinding.inflate(
                LayoutInflater.from(mContext), parent, false);

        SeriesFilterTypePresenter.ViewHolder holder = new SeriesFilterTypePresenter.ViewHolder(binding);
        holder.mBinding.title.setTextColor(UiUtils.createFontBrandingColorStateList(mLayoutConfig));
        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, SeriesFilterType filter) {
        holder.view.setActivated(isActivated(filter));
        holder.mBinding.title.setText(mTranslator.get(filter.getText()));
    }

    /**
     * Checks if the specified SeriesFilterType is activated.
     * @param filter
     * @return
     */
    private boolean isActivated(SeriesFilterType filter) {
        if (mSelectedFilterListener == null) {
            return false;
        }

        return filter == mSelectedFilterListener.getFilter();
    }

    @Override
    public void onClicked(ViewHolder holder, SeriesFilterType filter) {
        super.onClicked(holder, filter);
        if (mFilterSelectionListener != null) {
            mFilterSelectionListener.onFilterSelected(holder, filter);
        }
    }

    static class ViewHolder extends Presenter.ViewHolder {
        private final SeriesFilterItemBinding mBinding;

        public ViewHolder(SeriesFilterItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }

    public interface ISeriesFilterSelectionListener {
        void onFilterSelected(ViewHolder holder, SeriesFilterType filter);
    }

    public interface ISelectedSeriesFilterListener {
        SeriesFilterType getFilter();
    }
}
