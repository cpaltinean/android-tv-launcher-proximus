/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;

import tv.threess.threeready.api.log.Log;

/**
 * Loading drawable from resource.
 *
 * @author Szende Pal
 * @since 2017.10.19
 */
public class DrawableUtils {
    private static final String TAG = Log.tag(DrawableUtils.class);

    private static final String RESOURCE_TYPE = "drawable";

    public static Drawable getDrawable(Context context, String resourceName) {
        try {
            int iconResource = context.getResources().getIdentifier(resourceName, RESOURCE_TYPE, context.getPackageName());

            return context.getResources().getDrawable(iconResource);
        } catch (Exception e) {
            Log.e(TAG, "The resource is not found");
        }
        return null;

    }

}
