/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.search.activity;

/**
 * Define the channel stripe in global search.
 *
 * @author Barabas Attila
 * @since 2017.07.13
 */
public class ChannelGlobalSearchHandlerActivity extends BaseGlobalSearchHandlerActivity {
}
