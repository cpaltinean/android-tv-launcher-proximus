package tv.threess.threeready.ui.generic.hint;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.FontTextView;

/**
 * Dialog for epg hints.
 * Shows 3 hint screens for epg.
 *
 * @author Zsolt Bokor_
 * @since 2020.06.15
 */

public abstract class EpgHintsDialog extends HintsDialog {

    protected abstract FontTextView getDescriptionSmall();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDescriptionSmall().setText(mTranslator.get(TranslationKey.HINT_REVIEW_ALL_YOUR_TIPS));
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        if(blockKeys()) {
            return true;
        }

        dismiss();
        return true;
    }
}
