/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.home.presenter.card.app;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.leanback.widget.Presenter;

import java.util.Objects;

import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.gms.model.InstalledAppInfo;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.netflix.model.SourceType;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.AppA1CardBinding;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.generic.presenter.BaseModularCardPresenter;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.utils.UiUtils;


/**
 * Presenter class for application type and A1 variant card.
 *
 * @author Barabas Attila
 * @since 2017.05.17
 */
public class AppA1CardPresenter extends BaseModularCardPresenter<AppA1CardPresenter.ViewHolder, InstalledAppInfo> {
    private static final String TAG = Log.tag(AppA1CardPresenter.class);

    private static final int MAX_RECYCLER_VIEW_COUNT = 30;

    public AppA1CardPresenter(Context context) {
        super(context);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        AppA1CardBinding binding = AppA1CardBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        binding.getRoot().setForeground(UiUtils.createAppCardDrawable(parent.getContext().getResources().getColor(R.color.program_guide_active_card_overlay, null)));

        return new ViewHolder(binding);
    }

    @Override
    public int getMaxRecyclerViewCount() {
        return MAX_RECYCLER_VIEW_COUNT;
    }

    @Override
    public void onBindHolder(ModuleData<?> module, ViewHolder holder, InstalledAppInfo appInfo) {
        super.onBindHolder(module, holder, appInfo);

        final PackageManager pm = holder.mBinding.cover.getContext().getPackageManager();
        Intent intent = pm.getLeanbackLaunchIntentForPackage(appInfo.getPackageName());

        Drawable img = null;

        if (intent != null) {
            intent.setClassName(appInfo.getPackageName(), appInfo.getActivityName());
            ComponentName cn = intent.getComponent();

            try {
                // Get app banner
                img = pm.getActivityBanner(cn);
                holder.mBinding.cover.setScaleType(ImageView.ScaleType.CENTER_CROP);

                // Try app logo
                if (img == null) {
                    Log.i(TAG, "Banner not found. Try for logo!");
                    img = pm.getActivityLogo(cn);
                    holder.mBinding.cover.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }

                // Try app icon
                if (img == null) {
                    Log.i(TAG, "Banner and logo not found. Try for icon!");
                    img = pm.getActivityIcon(cn);
                    holder.mBinding.cover.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                }
            } catch (PackageManager.NameNotFoundException e) {
                Log.e(TAG, "Could not find package name", e);
            }
        }

        if (img != null) {
            holder.mBinding.cover.setImageDrawable(img);
        } else {
            Log.i(TAG, "Session, banner, icon, logo not found. Show android icon!");
            holder.mBinding.cover.setImageResource(R.drawable.android_icon);
            holder.mBinding.cover.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }

        holder.mBinding.cover.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_STRIPE_EDITORIAL_APPS))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_SWIMLANE, mTranslator.get(module.getTitle()))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_APP_NAME, appInfo.getName())
                        .toString()
        );
    }

    @Override
    public void onClicked(ModuleData<?> moduleData, ViewHolder holder, final InstalledAppInfo appInfo) {
        super.onClicked(moduleData, holder, appInfo);
        mPlaybackDetailsManager.startApplication(StartAction.Ui
                ,appInfo.getPackageName(), appInfo.getActivityName(), SourceType.FeaturedApp);
    }

    @Override
    public int getCardWidth(ModuleData<?> moduleData, InstalledAppInfo appInfo) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.app_a1_card_width);
    }

    @Override
    public int getCardHeight(ModuleData<?> moduleData, InstalledAppInfo appInfo) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.app_a1_card_height);
    }

    @Override
    public long getStableId(InstalledAppInfo appInfo) {
        return Objects.hash(appInfo.getPackageName(), appInfo.getActivityName());
    }

    public static class ViewHolder extends BaseCardPresenter.ViewHolder {
        private final AppA1CardBinding mBinding;

        public ViewHolder(AppA1CardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
