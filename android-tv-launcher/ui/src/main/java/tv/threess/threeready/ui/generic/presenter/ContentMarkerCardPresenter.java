package tv.threess.threeready.ui.generic.presenter;

import android.content.Context;
import android.view.View;

import androidx.annotation.Nullable;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.tv.view.ContentMarkersView;
import tv.threess.threeready.ui.tv.view.ProgressIndicatorView;

/**
 * Base content marker presenter. Contains a progress indicator, a content marker view and a
 * bookmark for the current content(IBaseContentItem).
 *
 * @author Solyom Zsolt
 * @since 2020.02.24
 */

public abstract class ContentMarkerCardPresenter<THolder extends ContentMarkerCardPresenter.ViewHolder, TItem extends IBaseContentItem>
        extends CoverCardPresenter<THolder, TItem> {

    public ContentMarkerCardPresenter(Context context) {
        super(context);
    }

    @Override
    public void onBindHolder(ModuleData<?> moduleData, THolder holder, TItem item) {
        super.onBindHolder(moduleData, holder, item);
        updateContentMarkers(holder, item);
        updateBookmark(holder, item);
    }

    @Override
    protected void onPlaybackChanged(THolder holder, TItem item) {
        super.onPlaybackChanged(holder, item);
        updateContentMarkers(holder, item);
        updateProgressIndicator(holder);
    }

    /**
     * Update the progress bar visibility on the card.
     * The progress bar is visible if the content has a bookmark and the card is focused.
     */
    protected void updateProgressIndicator(THolder holder) {
        if (holder.bookmark != null && holder.bookmark.getPosition() > 0) {
            holder.getProgressIndicatorView().setVisibility(View.VISIBLE);
            holder.getProgressIndicatorView().setProgressData(holder.bookmark);
        } else {
            holder.getProgressIndicatorView().setVisibility(View.GONE);
        }
    }

    /**
     * Display one content marker based on priority on the card.
     */
    protected void updateContentMarkers(THolder holder, TItem item) {
    }

    /**
     * Get the selected items bookmark position for progress indicator.
     */
    protected void updateBookmark(THolder holder, TItem item) {
        holder.getProgressIndicatorView().setVisibility(View.GONE);
        holder.bookmark = null;

        RxUtils.disposeSilently(holder.mBookmarkDisposable);
        Observable<IBookmark> observable = getBookmarkObservable(holder, item);
        if (observable == null) {
            // Nothing to do.
            return;
        }

        holder.mBookmarkDisposable = observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        bookmark -> {
                            holder.bookmark = bookmark;
                            ContentMarkerCardPresenter.this.updateProgressIndicator(holder);
                        }
                );
    }

    /**
     * Returns a Bookmark Observable for the content item. It can be a Vod, VodVariant, Broadcast or Series.
     */
    @Nullable
    public abstract Observable<IBookmark> getBookmarkObservable(THolder holder, TItem item);

    @Override
    public void onFocusedState(ModuleData<?> moduleData, THolder holder, TItem item) {
        super.onFocusedState(moduleData, holder, item);
        updateProgressIndicator(holder);
    }

    @Override
    public void onDefaultState(ModuleData<?> moduleData, THolder holder, TItem item) {
        super.onDefaultState(moduleData, holder, item);
        updateProgressIndicator(holder);
    }

    @Override
    public void onUnbindHolder(ModuleData<?> moduleData, THolder holder) {
        super.onUnbindHolder(moduleData, holder);
        RxUtils.disposeSilently(holder.mBookmarkDisposable);
    }

    public abstract static class ViewHolder extends CoverCardPresenter.ViewHolder {

        public IBookmark bookmark;
        public Disposable mBookmarkDisposable;

        public ViewHolder(View view) {
            super(view);
        }

        public abstract ProgressIndicatorView getProgressIndicatorView();

        public abstract ContentMarkersView getContentMarkerView();
    }

}
