package tv.threess.threeready.ui.generic.adapter;

import java.util.Comparator;
import java.util.List;

import tv.threess.threeready.ui.generic.presenter.ActionModel;

/**
 * Enum class used for keep the correct button order every time.
 */
public enum DetailPageButtonOrder {
    None(0),
    Watch(1),
    Open(1),
    Dismiss(2),
    Trailer(2),
    Record(3),
    Cancel(3),
    Delete(3),
    Manage(3),
    Rent(4),
    MoreInfo(5),
    AddToWatchList(6),
    RemoveFromWatchList(6),
    ChosenLanguage(6);

    private final int mValue;

    DetailPageButtonOrder(int value) {
        this.mValue = value;
    }

    public static void sort(List<ActionModel> actions) {
        actions.sort(Comparator.comparing(actionModel -> actionModel.getDetailPageButtonOrder().getValue()));
    }

    public int getValue() {
        return mValue;
    }

    /**
     * Gets the last focused button index from the new list.
     * Exception occurs when the last focused button is rent.
     *
     * @param selectedIndex   last focused button index from the old action list.
     * @param oldActionModels the old action list.
     * @param newActionModels the new action list.
     */
    public static int getFocusedButtonIndex(int selectedIndex, List<ActionModel> oldActionModels, List<ActionModel> newActionModels) {
        if (selectedIndex == -1) {
            return 0;
        }

        if (selectedIndex > newActionModels.size() - 1) {
            return newActionModels.size() - 1;
        }

        // Exception case
        // if the old list was focused on Rent button,
        // check if the new list will contain the 'Watch' button return it's index
        // otherwise return the 'Rent' button index.
        ActionModel oldAction = oldActionModels.get(selectedIndex);

        if (oldAction.getDetailPageButtonOrder() == Rent) {
            int index = 0;
            for (int i = 0; i < newActionModels.size(); ++i) {
                if (newActionModels.get(i).getDetailPageButtonOrder() == Watch) {
                    return i;
                }
                if (newActionModels.get(i).getDetailPageButtonOrder() == Rent) {
                    index = i;
                }
            }
            return index;
        }

        // Else next focused button remains on the same index.
        return selectedIndex;
    }
}
