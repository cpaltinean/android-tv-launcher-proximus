/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.utils;

/**
 * Constant values for placeholders
 *
 * @author Paul
 * @since 2017.08.29
 */

public interface PlaceholderKey {

    String PARENTAL_CONTROL_RATING = "parental_control_rating";
    String NUMBER_OF_ITEMS = "number_of_items";


    // TalkBack replace key words
    String TALKBACK_DETAIL_TITLE_OF_SWIMLANE = "%title of the swimlane%";
    String TALKBACK_DETAIL_CHANNEL_NAME = "%channel name%";
    String TALKBACK_DETAIL_TITLE_OF_ASSET = "%title of asset%";
    String TALKBACK_DETAIL_TITLE_OF_ASSET_NOW = "%title of asset now%";
    String TALKBACK_DETAIL_TITLE_OF_ASSET_NEXT = "%title of asset next%";
    String TALKBACK_DETAIL_ICONS = "%icons%";
    String TALKBACK_DETAIL_CONTENT_MARKER = "%content marker%";
    String TALKBACK_DETAIL_AGE = "%age%";
    String TALKBACK_DETAIL_LANGUAGE_NAME = "%language name%";
    String TALKBACK_DETAIL_GENRE = "%genre%";
    String TALKBACK_DETAIL_START_TIME = "%start time%";
    String TALKBACK_DETAIL_BROADCAST_NEXT_START_TIME = "%next start time%";
    String TALKBACK_DETAIL_VOD_RELEASE_YEAR = "%year%";
    String TALKBACK_DETAIL_DURATION = "%duration%";
    String TALKBACK_DETAIL_SYNOPSYS = "%synopsis%";
    String TALKBACK_DETAIL_SERIES_EPISODE_NUMBER = "%number of episodes%";
    String TALKBACK_DETAIL_APP_NAME = "%app name%";
    String TALKBACK_DETAIL_TEXT = "%text%";
    String TALKBACK_DETAIL_BUTTON_NAME = "%button name%";
    String TALKBACK_DETAIL_AGE_RATING = "%age rating%";
    String TALKBACK_DETAIL_DATE = "%date%";
    String TALKBACK_DETAIL_ICON_NAME = "%icon name%";
    String TALKBACK_DETAIL_NAME = "%name%";
    String TALKBACK_DETAIL_POSITION = "%position%";
    String TALKBACK_DETAIL_TOTAL = "%total%";
    String TALKBACK_DETAIL_METADATA = "%metadata%";
    String TALKBACK_DETAIL_TAB_NAME = "%tab name%";
    String TALKBACK_DETAIL_PAGE_TITLE = "%page title%";
    String TALKBACK_DETAIL_TITLE = "%title%";
    String TALKBACK_DETAIL_TYPE = "%type%";
    String TALKBACK_DETAIL_CAST = "%cast%";
    String TALKBACK_DETAIL_AUDIO = "%audio%";
    String TALKBACK_DETAIL_SUBTITLE = "%subtitles%";
    String TALKBACK_DETAIL_INFO = "%info%";
    String TALKBACK_DETAIL_DIALOG_TITLE = "%dialog title%";
    String TALKBACK_DETAIL_DIALOG_BODY = "%dialog body%";
    String TALKBACK_DETAIL_RENTAL_PERIOD = "%rental period%";
    String TALKBACK_DETAIL_END_TIME = "%end time%";
    String TALKBACK_DETAIL_BODY = "%body%";
    String TALKBACK_DETAIL_PROFILE = "%profile name%";
}
