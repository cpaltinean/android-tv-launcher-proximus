/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.home.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.menu.MenuItem;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.DynamicPageBinding;
import tv.threess.threeready.ui.generic.fragment.BaseModularPageMenuFragment;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.home.view.ModularPageView;

/**
 * Dynamic page to show modules from a given modular page.
 *
 * Created by Szilard on 7/20/2017.
 */
public class ModularPageFragment extends BaseModularPageMenuFragment {
    public static final String TAG = Log.tag(ModularPageFragment.class);

    public static final String EXTRA_PAGE_CONFIG = "EXTRA_PAGE_CONFIG";
    public static final String EXTRA_SELECTED_PAGE_ID = "EXTRA_SELECTED_PAGE_ID";
    public static final String EXTRA_SHOULD_HIDE_ON_BACK = "EXTRA_SHOULD_HIDE_ON_BACK";

    protected final Translator mTranslator = Components.get(Translator.class);

    private DynamicPageBinding mBinding;

    private String mSelectedPageId;
    private boolean mShouldSkipOnBack;

    ModularPageView.PageTitleCallback mPageTitleCallback;

    @Override
    protected FrameLayout getPageContainer() {
        return mBinding.pageContainer;
    }

    @Override
    protected TextView getEmptyPageTextView() {
        return mBinding.emptyContentLayout.emptyContentText;
    }

    public static ModularPageFragment newInstance(@NonNull PageConfig pageConfig) {
        return newInstance(pageConfig, null);
    }

    public static ModularPageFragment newInstance(@NonNull PageConfig pageConfig, String selectedPageId) {
        return newInstance(pageConfig, selectedPageId, false);
    }

    public static ModularPageFragment newInstance(@NonNull PageConfig pageConfig, String selectedPageId, boolean shouldHideOnBack) {
        ModularPageFragment fragment = new ModularPageFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_PAGE_CONFIG, pageConfig);
        args.putString(EXTRA_SELECTED_PAGE_ID, selectedPageId);
        args.putBoolean(EXTRA_SHOULD_HIDE_ON_BACK, shouldHideOnBack);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSelectedPageId = getStringArgument(EXTRA_SELECTED_PAGE_ID);
        mShouldSkipOnBack = getBooleanArgument(EXTRA_SHOULD_HIDE_ON_BACK);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (mBinding == null) {
            // Inflate the layout for this fragment
            mBinding = DynamicPageBinding.inflate(inflater, container, false);
            loadPage();
        }

        return mBinding.getRoot();
    }

    /**
     * Called when the page for which the fragment was opened needs to be loaded and displayed.
     */
    protected void loadPage() {
        createPageStructure(getSerializableArgument(EXTRA_PAGE_CONFIG));
    }

    /**
     * Create the page and menu structure and load the first page.
     */
    protected void createPageStructure(PageConfig pageConfig) {
        if (pageConfig != null) {
            Activity activity = mNavigator.getActivity();
            mBinding.subMenu.setTitle(mTranslator.get(pageConfig.getTitle()));
            mBinding.subMenu.setTitleMaxWidth(activity.getResources().getDimensionPixelOffset(R.dimen.dynamic_page_title_max_width));

            MenuItem defaultMenu = getDefaultSelectedMenuItem(pageConfig);
            if (defaultMenu != null) {
                // Setup sub menu
                mBinding.subMenu.setSelectedItem(defaultMenu);
                mBinding.subMenu.setMenuPage(pageConfig);
                displayPage(defaultMenu);
            } else {
                displayPage(pageConfig);
            }
            mBinding.subMenu.setMenuItemClickListener(mItemClickListener);
        } else {
            showEmptyPageMessage(null);
        }
    }

    private MenuItem getDefaultSelectedMenuItem(PageConfig pageConfig) {
        List<MenuItem> subMenuItems = pageConfig.getSubMenus();

        if (ArrayUtils.isEmpty(subMenuItems)) {
            return null;
        }

        for (MenuItem menuItem : ArrayUtils.notNull(subMenuItems)) {
            if (Objects.equals(menuItem.getId(), mSelectedPageId)) {
                return menuItem;
            }
        }

        return subMenuItems.get(0);
    }

    @Override
    protected void displayPage(@NonNull PageConfig pageConfig) {
        ModularPageView modularPageView = addModularPageView();
        modularPageView.showTopFading(true);

        mPageTitleCallback = new ModularPageView.PageTitleCallback() {
            @Override
            public String getPageTitle() {
                String pageTitle = mTranslator.get(pageConfig.getTitle());
                if (pageTitle != null && !TextUtils.isEmpty(pageTitle)) {
                    return pageTitle;
                }

                return modularPageView.getSelectedModuleData().getTitle() != null ? modularPageView.getSelectedModuleData().getTitle() : "";
            }
        };
        modularPageView.setPageTitleCallback(mPageTitleCallback);

        if (pageConfig.isEmpty()) {
            showEmptyPageMessage(pageConfig);
        } else {
            modularPageView.loadPageConfig(pageConfig, this);
            if(mBinding.subMenu.hasMenuItems()) {
                modularPageView.setFadingSize((int) getResources().getDimension(R.dimen.grid_fading_edge_submenu_visible_size));
            }
        }

        if (mBinding.subMenu.hasMenuItems()) {
            modularPageView.setWindowAlignmentOffsetPercent(55);
            modularPageView.setHeaderSpacing(getResources().getDimensionPixelSize(R.dimen.container_top_padding_large));
        } else {
            modularPageView.setHeaderSpacing(getResources().getDimensionPixelOffset(R.dimen.modular_page_top));
        }
    }

    @Override
    public void onPageEmpty(PageConfig pageConfig) {
        showEmptyPageMessage(pageConfig);
    }

    @Override
    public void onModuleLoaded(ModuleConfig moduleConfig) {
        hideEmptyPageMessage();

        if (mShouldFocusOnContent) {
            mBinding.pageContainer.requestFocus();
            mShouldFocusOnContent = false;
        }
    }

    @Override
    public void onModuleUpdated() {
        reportNavigation();
    }

    @Override
    public void onPageLoaded(PageConfig pageConfig, ModuleData<?> data) {
        if (mModularPageView == null) {
            return;
        }

        super.onPageLoaded(pageConfig, data);
        hideEmptyPageMessage();

        // Display the title from module when not available on page.
        if (!mBinding.subMenu.hasTitle()) {
            mBinding.subMenu.setTitle(mTranslator.get(data.getTitle()));
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mShouldSkipOnBack) {
            mNavigator.popBackStack();
            mNavigator.hideContentOverlay();
            return true;
        }

        if (!mNavigator.isContentOverlayDisplayed()) {
            mNavigator.showContentOverlay();
            return true;
        }

        return super.onBackPressed();
    }

    @Nullable
    @Override
    protected Event<?, ?> generatePageOpenEvent() {
        if (mModularPageView == null
                || mModularPageView.getPageConfig() == null) {
            // Not loaded yet.
            return null;
        }

        return generatePageNavigationEvent(mBinding.subMenu);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPageTitleCallback != null) {
            mPageTitleCallback = null;
        }
    }
}
