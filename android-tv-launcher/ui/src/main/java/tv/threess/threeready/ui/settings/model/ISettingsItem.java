package tv.threess.threeready.ui.settings.model;

public interface ISettingsItem {

    String getTitle();

    String getPreferenceGroup();
}
