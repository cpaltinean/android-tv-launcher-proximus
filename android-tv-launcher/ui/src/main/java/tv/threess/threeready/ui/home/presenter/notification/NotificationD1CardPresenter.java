package tv.threess.threeready.ui.home.presenter.notification;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.notification.model.NotificationItem;
import tv.threess.threeready.api.notification.model.NotificationType;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.NotificationD1CardBinding;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.DetailPageButtonOrder;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.dialog.AlertDialog;
import tv.threess.threeready.ui.generic.hint.HintManager;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.ActionButtonsPresenter;
import tv.threess.threeready.ui.generic.presenter.ActionModel;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Presenter class for D1 variant system notifications cards.
 *
 * @author Eugen Guzyk
 * @since 2018.08.23
 */
public class NotificationD1CardPresenter extends BaseCardPresenter<NotificationD1CardPresenter.ViewHolder, NotificationItem> {
    private static final float ALPHA_BACKGROUND_COLOR = 0.1f;

    private static final String SYSTEM_NOTIFICATION_INTERNET_SBN_KEY = "0|com.google.android.tvrecommendations|1234|null|";

    private static final String NOTIFICATION_PACKAGE = "com.google.android.tvrecommendations";
    private static final String NOTIFICATION_EXTRA_SBN_KEY = "sbn_key";


    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final HintManager mHintManager = Components.get(HintManager.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final Navigator mNavigator = Components.get(Navigator.class);

    PresenterSelector mPresenterSelector;

    public NotificationD1CardPresenter(Context context) {
        super(context);
        mPresenterSelector = new InterfacePresenterSelector()
            .addClassPresenter(ActionModel.class, new ActionButtonsPresenter(mContext));
    }

    @Override
    public NotificationD1CardPresenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        NotificationD1CardBinding mBinding = NotificationD1CardBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        NotificationD1CardPresenter.ViewHolder holder = new NotificationD1CardPresenter.ViewHolder(mBinding);
        holder.mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        holder.mBinding.description.setTextColor(mLayoutConfig.getFontColor());
        holder.mBinding.backgroundLayout.setBackgroundColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFocusedBackground(), ALPHA_BACKGROUND_COLOR));

        LinearLayoutManager layoutManager = new LinearLayoutManager(parent.getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManager.setStackFromEnd(true);
        holder.mBinding.actionGrid.setLayoutManager(layoutManager);

        holder.mAdapter = new ArrayObjectAdapter<>();
        holder.mItemBridgeAdapter = new ItemBridgeAdapter(holder.mAdapter, mPresenterSelector);
        holder.mBinding.actionGrid.setAdapter(holder.mItemBridgeAdapter);

        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, NotificationItem notificationItem) {
        super.onBindHolder(holder, notificationItem);

        holder.mBinding.title.setText(notificationItem.getNotifTitle());
        holder.mBinding.description.setText(notificationItem.getNotifText());
        updateButtons(holder, notificationItem);
    }

    @Override
    public int getCardWidth(NotificationItem notificationItem) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.notification_d1_card_width);
    }

    @Override
    public int getCardHeight(NotificationItem notificationItem) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.notification_d1_card_height);
    }

    @Override
    public void onFocusedState(ViewHolder holder, NotificationItem notificationItem) {
        super.onFocusedState(holder, notificationItem);
        if (holder.mBinding.actionGrid.getAdapter() != null && holder.mBinding.actionGrid.getAdapter().getItemCount() > 0) {
            holder.mBinding.actionGrid.getChildAt(0).requestFocus();
        }
    }

    @Override
    public long getStableId(NotificationItem notificationItem) {
        return Objects.hash(notificationItem.getSbnKey());
    }

    static class ViewHolder extends BaseCardPresenter.ViewHolder {

        private final NotificationD1CardBinding mBinding;

        ArrayObjectAdapter<ActionModel> mAdapter;
        ItemBridgeAdapter mItemBridgeAdapter;

        public ViewHolder(NotificationD1CardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }

    private void updateButtons(NotificationD1CardPresenter.ViewHolder holder, NotificationItem notification) {
        List<ActionModel> actions = new ArrayList<>();

        // Add open button
        if (notification.hasContentIntent()) {
            ActionModel openAction = new ActionModel(
                    0, 0,
                    notification.getContentButtonLabel(),
                DetailPageButtonOrder.Open,
                    () -> openNotification(notification));
            actions.add(openAction);
        }

        // Add dismiss button
        if (notification.isDismissible()) {
            ActionModel watchAction = new ActionModel(
                    0, 0,
                    notification.getDismissButtonLabel(),
                DetailPageButtonOrder.Dismiss,
                    () -> dismissNotification(notification));
            actions.add(watchAction);
        }

        if (holder.mBinding.actionGrid.getAdapter() == null) {
            holder.mBinding.actionGrid.setAdapter(holder.mItemBridgeAdapter);
        }

        holder.mAdapter.replaceAll(actions);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);

        holder.mBinding.actionGrid.setAdapter(null);
    }

    /**
     * Open details for a local or system notification.
     * @param notification The notification to open details for.
     */
    private void openNotification(NotificationItem notification) {
        // Show internet connectivity hints
        if (!TextUtils.isEmpty(notification.getSbnKey())
                && notification.getSbnKey().contains(SYSTEM_NOTIFICATION_INTERNET_SBN_KEY)) {
            mHintManager.showInternetConnectionHints();

       // Show backend unavailability dialog.
        } else if (NotificationType.APP_NOTIFICATION == notification.getType()) {
            mNavigator.showDialogOnTop(new AlertDialog.Builder()
                    .title(mTranslator.get(TranslationKey.BE_UNAVAILABLE_MORE_INFO_TITLE))
                    .description(mTranslator.get(TranslationKey.BE_UNAVAILABLE_MORE_INFO_BODY))
                    .addButton(mTranslator.get(TranslationKey.OK_BUTTON))
                    .dismissOnBtnClick(true)
                    .build()
            );

        // Open system notification
        } else {
            Intent open = new Intent(Intent.ACTION_VIEW);
            open.setPackage(NOTIFICATION_PACKAGE);
            open.putExtra(NOTIFICATION_EXTRA_SBN_KEY, notification.getSbnKey());
            mContext.sendBroadcast(open);
        }
    }

    /**
     * Dismiss the currently show notification.
     * @param notification Notification to dismiss.
     */
    private void dismissNotification(NotificationItem notification) {
        Intent open = new Intent(Intent.ACTION_DELETE);
        open.setPackage(NOTIFICATION_PACKAGE);
        open.putExtra(NOTIFICATION_EXTRA_SBN_KEY, notification.getSbnKey());
        mContext.sendBroadcast(open);
    }
}
