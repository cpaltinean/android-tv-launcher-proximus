/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.home.presenter.module.stripe.generic;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.StripePresenterBinding;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ModularItemBridgeAdapter;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.generic.utils.DrawableUtils;
import tv.threess.threeready.ui.home.presenter.module.BaseStripeModulePresenter;


/**
 * Presenter class for a stripe module.
 * Responsible to load the data from the given data source and display it in a horizontal grid.
 * The loaded data will be displayed with cards. The type of the card is based on the module type and variant.
 * <p>
 * Created by Barabas Attila on 4/12/2017.
 */
public abstract class StripeModulePresenter extends BaseStripeModulePresenter<StripeModulePresenter.ViewHolder> {

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    public StripeModulePresenter(Context context, DataSourceLoader dataSourceLoader) {
        super(context, dataSourceLoader);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent, RecyclerView.RecycledViewPool pool) {
        StripePresenterBinding binding = StripePresenterBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        final ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.stripe.getTitleView().setTextColor(mLayoutConfig.getFontColor());
        holder.mBinding.stripe.setRecyclerViewPool(pool);

        holder.mBinding.stripe.getStripeGrid().setPadding(getHorizontalPadding(), 0, getHorizontalPadding(), getBottomPadding());

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) holder.mBinding.stripe.getStripeGrid().getLayoutParams();
        params.setMargins(0, getTopMargin(), 0, getBottomMargin());

        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, ModuleData<?> data) {
        holder.mBinding.stripe.getStripeGrid().setItemSpacing(getItemSpacing(data.getModuleConfig()));
        holder.mBinding.stripe.getStripeGrid().setRowHeight(getRowHeight(data));

        super.onBindHolder(holder, data, null);

        holder.mBinding.stripe.getStripeGrid().setHasFixedSize(true);

        updateStripeTitle(holder, data);
        setIconsForStripe(holder, data.getModuleConfig());

        updateAdapter(holder, data);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);

        holder.mBinding.stripe.getStripeGrid().setAdapter(null);
    }


    private void updateStripeTitle(ViewHolder holder, ModuleData<?> data) {
        String title = data.getTitle();
        if (TextUtils.isEmpty(title)) {
            holder.mBinding.stripe.getTitleView().setVisibility(View.GONE);
        } else {
            holder.mBinding.stripe.getTitleView().setVisibility(View.VISIBLE);
            holder.mBinding.stripe.getTitleView().setText(mTranslator.get(data.getTitle()));
        }
    }

    /**
     * Create a new or update the existing adapter for the module data.
     * @param holder The view holder of the collection.
     * @param moduleData The module data which needs to be displayed
     */
    private void updateAdapter(StripeModulePresenter.ViewHolder holder, ModuleData<?> moduleData) {
        if (holder.mBinding.stripe.getStripeGrid().getAdapter() != null) {
            ModularItemBridgeAdapter itemBridgeAdapter =
                    (ModularItemBridgeAdapter) holder.mBinding.stripe.getStripeGrid().getAdapter();
            ModuleConfig adapterModule = itemBridgeAdapter.getModuleData().getModuleConfig();
            if (Objects.equals(adapterModule, moduleData.getModuleConfig())) {
                itemBridgeAdapter.getAdapter().replaceAll(moduleData.getItems());
                return;
            }
        }

        ArrayObjectAdapter<Object> objectAdapter = new ArrayObjectAdapter<>(moduleData.getItems());
        objectAdapter.setHasStableIds(true);

        PresenterSelector cardPresenterSelector = getCardPresenterSelector(moduleData.getModuleConfig());
        RecyclerView.RecycledViewPool recycledViewPool = holder.mBinding.stripe.getRecyclerViewPool();
        ModularItemBridgeAdapter itemBridgeAdapter = new ModularItemBridgeAdapter(
                moduleData, objectAdapter, cardPresenterSelector, recycledViewPool);
        holder.mBinding.stripe.getStripeGrid().setAdapter(itemBridgeAdapter);
    }

    private void setIconsForStripe(ViewHolder holder, ModuleConfig moduleConfig) {
        if (!TextUtils.isEmpty(moduleConfig.getIcon())) {
            Drawable icon = DrawableUtils.getDrawable(mContext, moduleConfig.getIcon());
            if (icon != null) {
                icon.setBounds(0,
                        (int) mContext.getResources().getDimension(R.dimen.stripe_icon_margin_top),
                        (int) mContext.getResources().getDimension(R.dimen.stripe_icon_width),
                        (int) mContext.getResources().getDimension(R.dimen.stripe_icon_height));
                holder.mBinding.stripe.getTitleView().setCompoundDrawablePadding((int) mContext.getResources().getDimension(R.dimen.stripe_icon_margin_right));
                holder.mBinding.stripe.getTitleView().setCompoundDrawables(icon, null, null, null);
            } else {
                holder.mBinding.stripe.getTitleView().setCompoundDrawablePadding(0);
            }
        } else {
            holder.mBinding.stripe.getTitleView().setCompoundDrawables(null, null, null, null);
        }
    }

    protected abstract PresenterSelector getCardPresenterSelector(ModuleConfig moduleConfig);

    /**
     * Gets the value for horizontal padding.
     */
    protected abstract int getHorizontalPadding();

    /**
     * Gets the value for bottom padding.
     */
    protected abstract int getBottomPadding();

    /**
     * Gets the value for bottom margin.
     */
    protected abstract int getBottomMargin();

    /**
     * Gets the value for item spacing.
     */
    protected abstract int getItemSpacing(ModuleConfig moduleConfig);

    /**
     * Gets the value for top margin.
     */
    protected abstract int getTopMargin();

    /**
     * Gets the value for the row height.
     */
    protected abstract int getRowHeight(ModuleData<?> moduleData);

    protected static class ViewHolder extends Presenter.ViewHolder {
        public final StripePresenterBinding mBinding;

        public ViewGroup mParentView;

        public ViewHolder(StripePresenterBinding binding) {
            super(binding.getRoot());
            mParentView = (ViewGroup) view;
            mBinding = binding;
        }
    }

}