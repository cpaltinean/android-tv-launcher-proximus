package tv.threess.threeready.ui.settings.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.leanback.widget.VerticalGridView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.model.IVodRentalTransaction;
import tv.threess.threeready.ui.databinding.VodPurchaseHistoryLayoutBinding;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.settings.model.ParentalControlItem;
import tv.threess.threeready.ui.settings.model.VodPurchaseHistoryItem;
import tv.threess.threeready.ui.settings.presenter.VodRentalTransactionItemPresenter;

/**
 * Fragment used to display vod purchase history settings.
 *
 * @author Daniela Toma
 * @since 2022.02.07
 */
public class VodPurchaseHistoryFragment extends BaseSettingsDialogFragment<Object> {

    private VodPurchaseHistoryLayoutBinding mBinding;
    private Disposable mDisposable;

    private final VodRepository mVodRepository = Components.get(VodRepository.class);
    private final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);

    public static VodPurchaseHistoryFragment newInstance() {
        return new VodPurchaseHistoryFragment();
    }

    @Override
    protected void initView() {
        setSettingsMenuItems();

        RxUtils.disposeSilently(mDisposable);
        mDisposable = mVodRepository.getVodRentalTransactions()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(vodRentalList -> {
                    mBinding.totalText.setText(mTranslator.get(TranslationKey.SETTINGS_TOTAL_PURCHASES));
                    setVodPurchasedItems(vodRentalList);
                    mBinding.totalCount.setText(getAmountSum(vodRentalList));
                    mBinding.grid.requestFocus();
                });
    }

    /**
     * Sets the items for the Settings menu.
     */
    private void setSettingsMenuItems() {
        List<Object> items = new ArrayList<>();
        items.add(new VodPurchaseHistoryItem(
                mTranslator.get(TranslationKey.SETTINGS_VOD_PURCHASE_HISTORY),
                mTranslator.get(TranslationKey.VOD_PURCHASE_HISTORY_INFO)
        ));
        items.add(new ParentalControlItem(
                mTranslator.get(TranslationKey.SETTINGS_PURCHASE_PIN),
                ParentalControlItem.ParentalControlItemType.CHANGE_PURCHASE_PIN
        ));

        setItems(items);
    }

    /**
     * Gets the amount sum of user's vod rentals.
     */
    private String getAmountSum(List<? extends IVodRentalTransaction> vodRentalList) {
        double sum = 0;
        for (int i = 0; i < vodRentalList.size(); i++) {
            sum += vodRentalList.get(i).getAmount();
        }
        return mLocaleSettings.getCurrency() + " " + StringUtils.formatPrice(sum);
    }

    @Override
    @NonNull
    protected View inflateLayout() {
        if (mBinding == null) {
            mBinding = VodPurchaseHistoryLayoutBinding.inflate(LayoutInflater.from(getContext()));
        }

        return mBinding.getRoot();
    }

    @NonNull
    @Override
    protected View getRightFrame() {
        return mBinding.rightFrame;
    }

    @Override
    public VerticalGridView getVerticalGridView() {
        return mBinding.grid;
    }

    @Override
    public TextView getTitleView() {
        return mBinding.title;
    }

    @Override
    protected void setTitle() {
        mBinding.title.setText(mTranslator.get(TranslationKey.SETTINGS_VOD_PURCHASE));
    }

    /**
     * Sets vod purchase items.
     */
    protected <TVodRentalTransaction extends IVodRentalTransaction> void setVodPurchasedItems(Collection<TVodRentalTransaction> items) {
        InterfacePresenterSelector historyPresenterSelector = new InterfacePresenterSelector()
                .addClassPresenter(IVodRentalTransaction.class, new VodRentalTransactionItemPresenter(getContext()));

        ArrayObjectAdapter<TVodRentalTransaction> mAdapter = new ArrayObjectAdapter<>(items);
        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(mAdapter, historyPresenterSelector);
        mBinding.historyGridView.setAdapter(itemBridgeAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RxUtils.disposeSilently(mDisposable);
    }
}
