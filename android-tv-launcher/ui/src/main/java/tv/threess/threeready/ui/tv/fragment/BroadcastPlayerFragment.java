package tv.threess.threeready.ui.tv.fragment;

import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.ContentMarkers;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.pvr.model.SeriesRecordingStatus;
import tv.threess.threeready.api.pvr.model.SingleRecordingStatus;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.details.DetailOpenedFrom;
import tv.threess.threeready.ui.generic.player.view.PlayerButtonContainer;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.BaseOrderedIconsContainer;
import tv.threess.threeready.ui.generic.view.BroadcastOrderedIconsContainer;
import tv.threess.threeready.ui.pvr.RecordingActionHelper;
import tv.threess.threeready.ui.tv.view.ContentMarkersView;
import tv.threess.threeready.ui.tv.view.PlayerButtonView;
import tv.threess.threeready.ui.tv.view.PlayerSubtitleDetailView;
import tv.threess.threeready.ui.tv.view.SeekBarView;

import static tv.threess.threeready.ui.utils.UiUtils.runOnUiThread;

/**
 * Abstract base class for all player user interface which is displayed for a tv broadcast.
 *
 * @author Barabas Attila
 * @since 2018.11.06
 */
public abstract class BroadcastPlayerFragment extends BaseTrickPlayPlayerFragment<IBroadcast> {
    private static final String TAG = Log.tag(BroadcastPlayerFragment.class);

    private Disposable mRecStatusDisposable;

    private final RecordingActionHelper mRecordingActionHelper = Components.get(RecordingActionHelper.class);
    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);
    private final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);

    @Override
    protected abstract BroadcastPlayerViewHolder getViewHolder();

    @Override
    protected void reportDetailsFragmentSelection() {
        IBroadcast broadcast = getPlaybackData();

        if (broadcast == null) {
            return;
        }

        Log.event(new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageSelection)
                .addDetail(UILogEvent.Detail.OpenedBy, UILog.OpenedBy.Ok));
    }

    @Override
    protected void initializeButtons() {
        super.initializeButtons();

        PlayerButtonContainer buttonContainer = getViewHolder().getButtonContainer();
        PlayerButtonView jumpToLive = getViewHolder().getJumpToLive();
        jumpToLive.setTitle(mTranslator.get(TranslationKey.SCREEN_PLAYER_LIVE_BUTTON));
        jumpToLive.setOnClickListener(v -> {
            buttonContainer.clearSelection();
            onJumpToLiveClicked();
        });

        PlayerButtonView recordButton = getViewHolder().getRecordButton();
        recordButton.setTitle(mTranslator.get(TranslationKey.SCREEN_PLAYER_RECORD_BUTTON));
        recordButton.setOnClickListener(v -> {
            buttonContainer.setSelectedChild(recordButton);
            onRecordButtonClicked();
        });

        getViewHolder().getFavouriteIcon().setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        RxUtils.disposeSilently(mRecStatusDisposable);
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_RECORD) {
            mRecordingActionHelper.handleRecKeyAction(
                    getPlaybackData(), getViewHolder().getBroadcastOrderedContainer().getRecordingStatus());
            return true;
        }

        return super.onKeyUp(event);
    }

    /**
     * Called when the user clicks on the recording button.
     */
    private void onRecordButtonClicked() {
        mRecordingActionHelper.handleRecButtonAction(
                getPlaybackData(), getViewHolder().getBroadcastOrderedContainer().getRecordingStatus());
    }

    /**
     * Called when the user clicks on the jump to live button.
     */
    protected void onJumpToLiveClicked() {
        IBroadcast broadcast = getPlaybackData();
        if (broadcast == null) {
            Log.e(TAG, "Can't start live on null broadcast.");
            return;
        }

        mNavigator.showLivePlayer(StartAction.JumpLive, broadcast.getChannelId(),
                mPlaybackDetailsManager.isFavorite());
    }

    /**
     * Called when the user clicks on the more info button.
     * It will open the details page for the broadcast.
     *
     * @param broadcast The broadcast for which the detail page needs to be opened.
     */
    @Override
    protected void onMoreInfoButtonClicked(IBroadcast broadcast) {
        RecordingStatus recordingStatus = getViewHolder().getBroadcastOrderedContainer().getRecordingStatus();
        if (recordingStatus != null && recordingStatus.getRecording() != null
                && recordingStatus.getSeriesRecordingStatus() == SeriesRecordingStatus.SCHEDULED) {
            mNavigator.showSeriesRecordingDetailPage(recordingStatus.getRecording(), DetailOpenedFrom.Player);
        } else {
            mNavigator.showProgramDetails(broadcast);
        }

        reportDetailsFragmentSelection();
    }

    /**
     * Called when the active playback changes.
     * All the displayed information needs to be invalidated.
     */
    @Override
    public void onPlayerDataUpdate(IBroadcast event) {
        this.postOnUiThread(() ->{
            if (mPlaybackDetailsManager.isRegionalChannelPlaying()){
                mNavigator.hideContentOverlay();
                mNavigator.popBackStackImmediate();
                return;
            }

            //This can be called while the fragment is not attached.
            //This is called from a different thread
            if (!getViewHolder().getSeekbar().isSeekingInProgress()) {
                updatePlaybackDetails(event);
            }
        });
    }

    @Override
    public void onCommandFailure(PlaybackCommand cmd, PlaybackDetails details, PlaybackError error) {
        super.onCommandFailure(cmd, details, error);
        if (cmd instanceof StartCommand && cmd.getDomain() != PlaybackDomain.TrickPlay) {
            runOnUiThread(() -> {
                IBroadcast item = getPlaybackData();
                SeekBarView seekBar = getViewHolder().getSeekbar();
                if (item != null) {
                    seekBar.setBounds(item.getStart(), item.getEnd());
                }
                seekBar.setSecondaryProgress(System.currentTimeMillis());
            });
        }
    }

    /**
     * Update jumpToLive and startOver button visibility based on the active playback type.
     */
    @Override
    protected void updatePlaybackButtons(IBroadcast broadcast) {
        boolean restricted = mParentalControlManager.isRestricted(broadcast, true);

        boolean jumpToLiveChanged = changeButtonVisibility(getViewHolder().getJumpToLive(),
                !restricted && mPlaybackDetailsManager.canJumpToLive());

        boolean startOverChanged = changeButtonVisibility(getViewHolder().getStartOverButton(),
                !restricted && mPlaybackDetailsManager.canStartOver());

        if (jumpToLiveChanged || startOverChanged) {
            requestDefaultFocus();
        }
    }

    /**
     * Update all the displayed information on the player ui.
     *
     * @param broadcast The broadcast for which the information should be displayed.
     */
    @Override
    protected void updatePlaybackDetails(IBroadcast broadcast) {
        super.updatePlaybackDetails(broadcast);

        updateReplayIndicator(broadcast);
        updateRecordingStatus(broadcast);
    }

    /**
     * Method used for prepare the information what we need for the subtitle/description.
     *
     * @param broadcast The broadcast which contains all the information for description.
     */
    @Override
    protected void updateSubtitle(IBroadcast broadcast) {
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        if (channel != null) {
            updateSubtitleView(broadcast, channel);
        }
    }

    @Override
    protected void updateProviderLogo(IBroadcast contentItem) {
        TvChannel channel = mNowOnTvCache.getChannel(contentItem.getChannelId());

        if (channel == null) {
            return;
        }

        ImageView logoView = getViewHolder().getLogoView();
        Glide.with(mApp).clear(logoView);
        Glide.with(mApp)
                .load(channel)
                .into(logoView);
    }

    /**
     * Update displayed subtitle/description for the broadcast.
     *
     * @param broadcast The broadcast which contains all the information for description.
     * @param channel Contains the channel information what we need for description
     */
    private void updateSubtitleView(IBroadcast broadcast, TvChannel channel) {
        List<String> detailsList = new ArrayList<>();

        AppCompatImageView favouriteIcon = getViewHolder().getFavouriteIcon();
        if (channel.isFavorite()) {
            favouriteIcon.setVisibility(View.VISIBLE);
            detailsList.add("");
        } else {
            favouriteIcon.setVisibility(View.GONE);
            detailsList.add(String.valueOf(channel.getNumber()));
        }

        detailsList.add(LocaleTimeUtils.getDate(broadcast, mTranslator, mLocaleSettings));

        getViewHolder().getSubtitleView().updateSubtitle(null, detailsList);
    }

    /**
     * Update recording status and button view for the broadcast.
     *
     * @param broadcast The broadcast for which the status should be updated.
     */
    protected void updateRecordingStatus(IBroadcast broadcast) {
        RxUtils.disposeSilently(mRecStatusDisposable);

        if (!mPvrRepository.isPVREnabled(broadcast)) {
            updateRecordingIcon(null);
            updateRecordingButton(broadcast, null);
            return;
        }

        mRecStatusDisposable = mPvrRepository.getRecordingStatus(broadcast)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recordingStatus -> {
                    updateRecordingIcon(recordingStatus);
                    updateRecordingButton(broadcast, recordingStatus);
                }, throwable -> {
                    Log.e(TAG, "Couldn't get the recording status.", throwable);
                    updateRecordingIcon(null);
                    updateRecordingButton(broadcast,null);
                });
    }

    /**
     * Update visibility of the recording status indicator icon.
     * @param recordingStatus The recording status of the currently playing broadcast.
     */
    protected void updateRecordingIcon(@Nullable RecordingStatus recordingStatus) {
        BroadcastOrderedIconsContainer iconsContainer = getViewHolder().getBroadcastOrderedContainer();
        if (recordingStatus == null) {
            // Pvr not enabled for broadcast. Hide status.
            iconsContainer.hideRecordingStatus();
            return;
        }

        iconsContainer.showRecordingStatus(recordingStatus, true);
    }

    /**
     * Update visibility and message on the recording button
     * @param broadcast The currently playing broadcast.
     * @param recordingStatus The recording status of the currently playing broadcast.
     */
    protected void updateRecordingButton(IBroadcast broadcast, @Nullable RecordingStatus recordingStatus) {
        PlayerButtonView recordButton = getViewHolder().getRecordButton();
        if (broadcast.isPast()) {
            // No recording for past programs
            recordButton.setVisibility(View.GONE);
            return;
        }

        if (mParentalControlManager.isRestricted(getPlaybackData(), true)) {
            // Restricted content.
            recordButton.setVisibility(View.GONE);
            return;
        }

        if (recordingStatus == null) {
            // Pvr not enabled for broadcast. Hide button.
            recordButton.setVisibility(View.GONE);
            return;
        }

        // Update text on record button.
        recordButton.setVisibility(View.VISIBLE);
        SingleRecordingStatus singleRecordingStatus = recordingStatus.getSingleRecordingStatus();
        if (singleRecordingStatus == SingleRecordingStatus.RECORDING
                || singleRecordingStatus == SingleRecordingStatus.SCHEDULED) {
            recordButton.setTitle(mTranslator.get(TranslationKey.SCREEN_PLAYER_CANCEL_RECORDING_BUTTON));
        } else {
            recordButton.setTitle(mTranslator.get(TranslationKey.SCREEN_PLAYER_RECORD_BUTTON));
        }
    }

    /**
     * Update the displayed replay indicator for the broadcast.
     *
     * @param broadcast The broadcast which contains the replay data.
     */
    protected void updateReplayIndicator(IBroadcast broadcast) {
        BroadcastOrderedIconsContainer iconsContainer = getViewHolder().getBroadcastOrderedContainer();
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        if (broadcast.canReplay(channel)) {
            iconsContainer.showReplay();
        } else {
            iconsContainer.hideReplay();
        }
    }

    @Override
    protected void updateQualityIndicator(IBroadcast broadcast) {
        BaseOrderedIconsContainer iconsContainer = getViewHolder().getBaseOrderedIconsContainer();
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        if (channel != null && channel.isHDEnabled()) {
            iconsContainer.showHDIcon();
        } else {
            iconsContainer.hideHDIcon();
        }
    }

    /**
     * Update the displayed content marker for the broadcast.
     *
     * @param broadcast The broadcast on based the content marker should be displayed.
     */
    @Override
    protected void updateContentMarkers(IBroadcast broadcast) {
        ContentMarkersView contentMarkers = getViewHolder().getContentMarkerView();
        if (broadcast.isLive()) {
            contentMarkers.showMarkers(broadcast, ContentMarkers.TypeFilter.Player);
        } else {
            contentMarkers.setVisibility(View.GONE);
        }
    }

    @Override
    protected Event<?, ?> generatePageOpenEvent() {
        IBroadcast broadcast = getPlaybackData();
        if (broadcast == null) {
            // Not loaded yet. Nothing to report.
            return null;
        }

        return ReportUtils.createEvent(UILog.Page.TvPlayer, broadcast);
    }

    protected interface BroadcastPlayerViewHolder extends ContentPlayerViewHolder {

        PlayerSubtitleDetailView getSubtitleView();

        PlayerButtonView getJumpToLive();

        PlayerButtonView getRecordButton();

        ContentMarkersView getContentMarkerView();

        AppCompatImageView getFavouriteIcon();

        BroadcastOrderedIconsContainer getBroadcastOrderedContainer();
    }
}
