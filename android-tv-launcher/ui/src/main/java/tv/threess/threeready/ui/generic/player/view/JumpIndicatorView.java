package tv.threess.threeready.ui.generic.player.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.config.model.PlaybackSettings;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.contract.SeekDirection;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.JumpIndicatorLayoutBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.notification.NotificationManager;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * View for indicating the jumping on the player.
 * This view can have three different states:
 * - Jumping back - lower left corner
 * - Jumping forward - lower right corner
 * - Live indication - lower right corner - moved to LiveNotificationDialog
 * <p>
 * The view has two components, one indicating the direction of the jump (arrow left or right)
 * and a time indicator, showing how many seconds is the user jumping.
 *
 * @author Andor Lukacs
 * @since 8/27/18
 */

public class JumpIndicatorView extends RelativeLayout {
    private static final String TAG = Log.tag(JumpIndicatorView.class);

    private static final long JUMP_INDICATOR_SHOW_TIMEOUT = TimeUnit.SECONDS.toMillis(1);
    private static final int COMPOUND_ICON_PADDING_DP = 9;

    private JumpIndicatorLayoutBinding mBinding;

    private final Translator mTranslator = Components.get(Translator.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final PlaybackSettings mPlaybackSettings = Components.get(PlaybackSettings.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final NotificationManager mNotificationManager = Components.get(NotificationManager.class);

    PlaybackDetailsManager mPlaybackDetailsManager;

    private volatile long mJumpMillis = 0;

    private ObservableEmitter<KeyEvent> mKeyEventEmitter;
    private Disposable mKeyEventDisposable;

    private final Runnable mHideJumpRunnable = new Runnable() {
        @Override
        public void run() {
            mBinding.jumpBackText.setVisibility(GONE);
            mBinding.jumpForwardText.setVisibility(GONE);

            long jumpMillis = mJumpMillis;
            if (mPlaybackDetailsManager == null || jumpMillis == 0) {
                return;
            }

            IBroadcast livePlayerData = mPlaybackDetailsManager.getLivePlayerData();
            PlaybackDetails details = mPlaybackDetailsManager.getExclusiveDetails();

            if (details.getType() == PlaybackType.LiveTvOtt &&
                    mPlaybackDetailsManager.canSeek() && livePlayerData != null) {
                // in case of LiveTvOtt we have to start live replay first
                if (jumpMillis > 0) {
                    //we can't jump into the future
                    mJumpMillis = 0;
                    return;
                }
                mPlaybackDetailsManager.startReplay(StartAction.Rewind, livePlayerData,
                        false, Math.max(0, details.getRelativePosition() + jumpMillis));
            } else {
                long positionToJump = details.getPosition() + jumpMillis;

                IBroadcast broadcast = mPlaybackDetailsManager.getBroadcastPlayerData(positionToJump, false);
                if (broadcast != null) {
                    mNavigator.showParentalControlNotification(broadcast);
                }
                mPlaybackDetailsManager.jumpAndResume(positionToJump);
            }
            mJumpMillis = 0;
        }
    };

    public JumpIndicatorView(Context context) {
        super(context);
        init();
    }

    public JumpIndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public JumpIndicatorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Log.d(TAG, "onDetachedFromWindow() called");
        RxUtils.disposeSilently(mKeyEventDisposable);
        removeCallbacks(mHideJumpRunnable);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!shouldDisplayJumpIndicator()) {
            return true;
        }

        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
                Log.d(TAG, "onKeyUp() called with: keyCode = [" + keyCode + "], can seek = [" + (!isLivePlayer() || mPlaybackDetailsManager.canSeek()) + "]");
                if (isLivePlayer() && !mPlaybackDetailsManager.canSeek()) {
                    removeCallbacks(mHideJumpRunnable);
                    postDelayed(mHideJumpRunnable, JUMP_INDICATOR_SHOW_TIMEOUT);
                    mNotificationManager.showLiveNotification();
                }
                return true;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                Log.d(TAG, "onKeyUp() called with: keyCode = [" + keyCode + "], can seek = [" + (!isLivePlayer() || mPlaybackDetailsManager.canSeek()) + "]");
                if (mPlaybackDetailsManager.inPlaybackDomain(PlaybackDomain.LiveTvOtt)) {
                    removeCallbacks(mHideJumpRunnable);
                    postDelayed(mHideJumpRunnable, JUMP_INDICATOR_SHOW_TIMEOUT);
                    mNotificationManager.showLiveNotification();
                }
                return true;
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                if (isLivePlayer()) {
                    mNotificationManager.showLiveNotification();
                    return true;
                }
                return false;
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                if (isLivePlayer() && !mPlaybackDetailsManager.canSeek()) {
                    mNotificationManager.showLiveNotification();
                    return true;
                }
                return false;

        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!shouldDisplayJumpIndicator()) {
            return true;
        }
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
                Log.d(TAG, "onKeyDown() called with: keyCode = [" + keyCode + "], event = [" + event + "]");
                if (!isLivePlayer() || mPlaybackDetailsManager.canSeek()) {
                    mKeyEventEmitter.onNext(event);
                }
                return true;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                Log.d(TAG, "onKeyDown() called with: keyCode = [" + keyCode + "], event = [" + event + "]");
                if (!mPlaybackDetailsManager.inPlaybackDomain(PlaybackDomain.LiveTvOtt)) {
                    mKeyEventEmitter.onNext(event);
                }
                return true;
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                return isLivePlayer() && !mPlaybackDetailsManager.canSeek();
        }
        return super.onKeyDown(keyCode, event);
    }

    private boolean isLivePlayer() {
        return mPlaybackDetailsManager.getPlayerType() == PlaybackType.LiveTvIpTv;
    }

    /**
     * Initialize the custom layout
     */
    private void init() {
        Log.d(TAG, "init() called");

        mBinding = JumpIndicatorLayoutBinding.inflate(LayoutInflater.from(getContext()), this);

        mBinding.jumpBackText.setTextColor(mLayoutConfig.getButtonFocusedFontColor());
        mBinding.jumpForwardText.setTextColor(mLayoutConfig.getButtonFocusedFontColor());

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int drawablePadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, COMPOUND_ICON_PADDING_DP, displayMetrics);

        mBinding.jumpForwardText.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ico_jump_right, 0);
        mBinding.jumpForwardText.setCompoundDrawablePadding(drawablePadding);

        final long samplingPeriod = TimeUnit.SECONDS.toMillis(1) / mPlaybackSettings.getLongPressSkipFactor();

        Observable.create((ObservableOnSubscribe<KeyEvent>) e -> mKeyEventEmitter = e)
                .sample(samplingPeriod, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<KeyEvent>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "onSubscribe() called with: d = [" + d + "]");
                        mKeyEventDisposable = d;
                    }

                    @Override
                    public void onNext(KeyEvent keyEvent) {
                        PlaybackDetails details = mPlaybackDetailsManager.getExclusiveDetails();
                        if (details.isUnskippablePosition()) {
                            mNavigator.showPlayerUI(StartAction.Undefined, keyEvent);
                            mJumpMillis = 0;
                            return;
                        }
                        Log.d(TAG, "onNext() called with: keyEvent = [" + keyEvent + "]");
                        accumulateJump(getSeekDirection(keyEvent));
                        showJumpIndicator(details);
                    }

                    @Override
                    public void onError(Throwable t) {
                        Log.d(TAG, "onError() called", t);
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete() called");
                    }
                });

        Log.d(TAG, "Sampling time for jumps: " + samplingPeriod + " millis");
    }

    /**
     * Set the playback manager.
     */
    public void setPlaybackManager(PlaybackDetailsManager playbackManager) {
        mPlaybackDetailsManager = playbackManager;
    }


    /**
     * Accumulate the jump value based on the seek direction.
     * If the direction has changed (based on the value of the current jumping seconds saved)
     * then the jump is reset and we start again with the new direction.
     *
     * @param direction RelativeSeek direction, based on the key direction.
     */
    private void accumulateJump(SeekDirection direction) {
        long skipBackMillis = mPlaybackSettings.getSkipBack(TimeUnit.MILLISECONDS);
        long skipForwardMillis = mPlaybackSettings.getSkipForward(TimeUnit.MILLISECONDS);

        switch (direction) {
            case REWIND:
                if (mJumpMillis > 0) {
                    mJumpMillis = 0;
                }
                mJumpMillis -= skipBackMillis;
                break;
            case FORWARD:
                if (mJumpMillis < 0) {
                    mJumpMillis = 0;
                }
                mJumpMillis += skipForwardMillis;
                break;
        }
    }

    /**
     * Show the indicator for the player jumping on the left or right side, based on the jump direction.
     * Then after a timeout of {@link JumpIndicatorView#JUMP_INDICATOR_SHOW_TIMEOUT} milliseconds,
     * it will automatically hide.
     */
    private void showJumpIndicator(PlaybackDetails details) {
        if (this.mJumpMillis == 0) {
            // invalid jump command.
            return;
        }

        long skipForwardMillis = mPlaybackSettings.getSkipForward(TimeUnit.MILLISECONDS);
        long currentPos = details.getPosition();
        long jumpMillis = this.mJumpMillis;
        long positionToJump = currentPos + jumpMillis;

        if (positionToJump > currentPos) {
            mBinding.jumpBackText.setVisibility(GONE);
            boolean showJumpIndicator = true;
            long maxJump = details.getSeekAbleEnd();

            if (positionToJump > maxJump) {
                Log.d(TAG, "jump out of end bounds: " + jumpMillis + " > " + maxJump);
                jumpMillis = maxJump - currentPos;
                mNotificationManager.showLiveNotification();
                showJumpIndicator = false;
            }

            int jumpValueSec = (int) TimeUnit.MILLISECONDS.toSeconds(jumpMillis);
            mBinding.jumpForwardText.setText(StringUtils.formatJumpString(jumpValueSec, mTranslator));
            mBinding.jumpForwardText.setVisibility(showJumpIndicator ? VISIBLE : GONE);
        }

        if (positionToJump < currentPos) {
            mBinding.jumpForwardText.setVisibility(GONE);
            long minJump = details.getSeekAbleStart();
            boolean showJumpIndicator = true;

            jumpMillis = -jumpMillis;
            if (positionToJump < minJump) {
                Log.d(TAG, "jump out of start bounds: " + jumpMillis + " < " + minJump);
                if(mPlaybackDetailsManager.inPlaybackType(PlaybackType.LiveTvIpTv)) {
                    mNavigator.showSeekReachedEnd();
                    showJumpIndicator = false;
                }
                jumpMillis = currentPos - minJump;
            }

            int jumpValueSec = (int) TimeUnit.MILLISECONDS.toSeconds(jumpMillis);
            mBinding.jumpBackText.setText(StringUtils.formatJumpString(jumpValueSec, mTranslator));
            mBinding.jumpBackText.setVisibility(showJumpIndicator ? VISIBLE : GONE);
        }

        if (details.isLive() && positionToJump == currentPos) {
            mJumpMillis = 0;
            return;
        }

        removeCallbacks(mHideJumpRunnable);
        postDelayed(mHideJumpRunnable, JUMP_INDICATOR_SHOW_TIMEOUT);
    }

    private static SeekDirection getSeekDirection(KeyEvent event) {
        return event.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT ?
                SeekDirection.REWIND : SeekDirection.FORWARD;
    }

    /**
     * Returns true if we should display the jump indicator
     */
    private boolean shouldDisplayJumpIndicator() {
        TvChannel channel = mPlaybackDetailsManager.getChannelData();
        if (channel != null) {
            return channel.getType() == ChannelType.TV;
        }
        return true;
    }
}
