package tv.threess.threeready.ui.generic.fragment;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.home.HomeRepository;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.generic.navigation.Navigator;

/**
 * WebView fragment that is opened from SecretFragment.
 *
 * @author Andrei Teslovan
 * @since 2018.07.25
 */

public class WebViewFragment extends BaseWebViewFragment {
    public static final String TAG = Log.tag(WebViewFragment.class);

    private static final String EXTRA_URL = "URL";
    private static final String EXTRA_HIDE_CONTENT_OVERLAY_ON_EXIT = "HIDE_CONTENT_OVERLAY_EXIT";
    private static final String EXTRA_MODULE_ID = "EXTRA_MODULE_ID";

    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final HomeRepository mHomeRepository = Components.get(HomeRepository.class);

    private String mUrl;
    private boolean mHideContentOverlayOnExit = false;

    private String mModuleId;

    public static WebViewFragment newInstance(String url, String moduleConfigId, boolean hideContentOverlayOnExit) {
        Bundle args = new Bundle();
        args.putString(EXTRA_URL, url);
        args.putBoolean(EXTRA_HIDE_CONTENT_OVERLAY_ON_EXIT, hideContentOverlayOnExit);
        args.putString(EXTRA_MODULE_ID, moduleConfigId);

        WebViewFragment fragment = new WebViewFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUrl = getStringArgument(EXTRA_URL);
        mHideContentOverlayOnExit = getBooleanArgument(EXTRA_HIDE_CONTENT_OVERLAY_ON_EXIT);
        mModuleId = getStringArgument(EXTRA_MODULE_ID);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d(TAG, "Load URL : " + mUrl);
        getWebView().loadUrl(mUrl);
    }

    @Override
    public void onStart() {
        super.onStart();
        mInternetChecker.addStateChangedListener(mOnStateChangedListener);
    }

    @Override
    public boolean onBackPressed() {
        Log.d(TAG, "OnBackPressed. Hide content overlay on exit : " + mHideContentOverlayOnExit);
        if (mHideContentOverlayOnExit) {
            mNavigator.disableContentFrameLayoutTransition(() -> {
                mNavigator.hideContentOverlay();
                mNavigator.popBackStackImmediate();
            });
        } else {
            mNavigator.popBackStack();
        }

        return true;
    }

    @Override
    public void onStop() {
        super.onStop();
        mInternetChecker.removeStateChangedListener(mOnStateChangedListener);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (!TextUtils.isEmpty(mModuleId)) {
            mHomeRepository.clearCacheByModuleConfigId(mModuleId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe();
        }
    }

    private final InternetChecker.OnStateChangedListener mOnStateChangedListener = (state) -> {
        Log.d(TAG, "onStateChanged() called with: available = [" + state.isInternetAvailable() + "]");
        if (!state.isInternetAvailable()) {
            mNavigator.goBack();
        }
    };
}
