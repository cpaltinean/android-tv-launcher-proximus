/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.ImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import java.lang.reflect.Field;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.ButtonStyle;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.activity.MainActivity;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Contains UI related helper methods
 *
 * @author Paul
 * @since 2017.07.18
 */

public class UiUtils {
    private static final String TAG = Log.tag(UiUtils.class);

    public static ColorStateList createFontBrandingColorStateList(LayoutConfig layoutConfig) {
        return new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_focused},
                        new int[]{android.R.attr.state_activated},
                        new int[]{android.R.attr.state_selected},
                        new int[]{}
                },
                new int[]{
                        layoutConfig.getMenuSelectorFocused(),
                        layoutConfig.getMenuSelectorActive(),
                        layoutConfig.getMenuSelectorActive(),
                        layoutConfig.getMenuSelectorNoState(),
                }
        );
    }

    /**
     * Create background for button based on the button style and layout config.
     */
    public static Drawable createButtonBackground(Context context,
                                                  LayoutConfig layoutConfig, ButtonStyle buttonStyle) {
        int[] pressedStateList = new int[]{android.R.attr.state_pressed};
        int[] focusedStateList = new int[]{android.R.attr.state_focused};
        int[] hoveredStateList = new int[]{android.R.attr.state_hovered};
        int[] activeStateList = new int[]{android.R.attr.state_active};
        int[] defaultStateList = new int[]{};

        StateListDrawable stateListDrawable = new StateListDrawable();
        switch (buttonStyle) {
            case PROXIMUS:
                // Pressed
                stateListDrawable.addState(pressedStateList,
                        createProximusStyleButtonBackground(context,
                                layoutConfig.getButtonPressedColorStart(), layoutConfig.getButtonPressedColorEnd()));
                // Focused
                stateListDrawable.addState(focusedStateList,
                        createProximusStyleButtonBackground(context,
                                layoutConfig.getButtonFocusedColorStart(), layoutConfig.getButtonFocusedColorEnd()));
                // Hovered
                stateListDrawable.addState(hoveredStateList,
                        createProximusStyleButtonBackground(context,
                                layoutConfig.getButtonFocusedColorStart(), layoutConfig.getButtonFocusedColorEnd()));
                // Active
                stateListDrawable.addState(activeStateList,
                        createProximusStyleButtonBackground(context,
                                layoutConfig.getButtonActiveColorStart(), layoutConfig.getButtonActiveColorEnd()));
                // No state
                stateListDrawable.addState(defaultStateList,
                        createProximusStyleButtonBackground(context,
                                layoutConfig.getButtonNoStateColorStart(), layoutConfig.getButtonNoStateColorEnd()));
                break;
        }

        return stateListDrawable;
    }

    public static Drawable createSortOptionBackground(Context context, LayoutConfig layoutConfig) {
        int[] pressedStateList = new int[]{android.R.attr.state_pressed};
        int[] focusedStateList = new int[]{android.R.attr.state_focused};
        int[] activatedStateList = new int[]{android.R.attr.state_activated};
        int[] defaultStateList = new int[]{};

        StateListDrawable stateListDrawable = new StateListDrawable();

        stateListDrawable.addState(pressedStateList,
                createSortOptionBackground(context, layoutConfig.getButtonPressedColorStart(), layoutConfig.getButtonPressedColorEnd()));
        stateListDrawable.addState(focusedStateList,
                createSortOptionBackground(context, layoutConfig.getButtonFocusedColorStart(), layoutConfig.getButtonFocusedColorEnd()));
        stateListDrawable.addState(activatedStateList,
                createActivatedSortOptionBackground(context, layoutConfig.getButtonFocusedColorEnd()));
        stateListDrawable.addState(defaultStateList,
                createSortOptionBackground(context, Color.TRANSPARENT, Color.TRANSPARENT));
        return stateListDrawable;
    }

    private static Drawable createSortOptionBackground(Context context, int colorStart, int colorEnd) {
        float cornerRadius = context.getResources().getDimensionPixelOffset(R.dimen.sort_option_item_border_radius);
        int[] colors = {colorStart, colorEnd};

        GradientDrawable out = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
        out.setCornerRadii(new float[]{cornerRadius, cornerRadius, cornerRadius, cornerRadius, cornerRadius, cornerRadius, cornerRadius, cornerRadius});
        return out;
    }

    private static GradientDrawable createActivatedSortOptionBackground(Context context, int strokeColor) {
        float cornerRadius = context.getResources().getDimensionPixelOffset(R.dimen.sort_option_item_border_radius);
        int borderWidth = context.getResources().getDimensionPixelOffset(R.dimen.sort_option_activated_border_width);

        GradientDrawable out = new GradientDrawable();
        out.setStroke(borderWidth, strokeColor);
        out.setCornerRadii(new float[]{cornerRadius, cornerRadius, cornerRadius, cornerRadius, cornerRadius, cornerRadius, cornerRadius, cornerRadius});

        return out;
    }

    public static Drawable createLoadingButtonBackground(Context context, LayoutConfig layoutConfig) {
        int[] defaultStateList = new int[]{};
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(defaultStateList,
                createProximusStyleButtonBackground(context,
                        layoutConfig.getLoadingButtonColor(), layoutConfig.getLoadingButtonColor()));
        return stateListDrawable;
    }

    /**
     * Create background for FTI button based on the layout config.
     */
    public static Drawable createFtiButtonBackground(Context context,
                                                     LayoutConfig layoutConfig) {
        int[] pressedStateList = new int[]{android.R.attr.state_pressed};
        int[] focusedStateList = new int[]{android.R.attr.state_focused};
        int[] activeStateList = new int[]{android.R.attr.state_active};
        int[] defaultStateList = new int[]{};

        StateListDrawable stateListDrawable = new StateListDrawable();

        // Pressed
        stateListDrawable.addState(pressedStateList,
                createFtiStyleButtonBackground(context,
                        layoutConfig.getButtonPressedColorStart(), layoutConfig.getButtonPressedColorEnd()));
        // Focused
        stateListDrawable.addState(focusedStateList,
                createFtiStyleButtonBackground(context,
                        layoutConfig.getButtonFocusedColorStart(), layoutConfig.getButtonFocusedColorEnd()));
        // Active
        stateListDrawable.addState(activeStateList,
                createFtiStyleButtonBackground(context,
                        layoutConfig.getButtonActiveColorStart(), layoutConfig.getButtonActiveColorEnd()));
        // No state
        stateListDrawable.addState(defaultStateList,
                createFtiStyleButtonBackground(context,
                        Color.TRANSPARENT, Color.TRANSPARENT));

        return stateListDrawable;
    }


    /**
     * Create background for a Proximus style button.
     * Button with rounded bottom-left corner.
     */
    private static Drawable createProximusStyleButtonBackground(Context context, int colorStart, int colorEnd) {
        float cornerRadius = context.getResources().getDimension(R.dimen.rounded_button_radius_proximus);

        int[] colors = {colorStart, colorEnd};
        GradientDrawable out = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
        out.setCornerRadii(new float[]{0, 0, 0, 0, cornerRadius, cornerRadius, 0, 0});

        return out;
    }

    /**
     * Create background for a FTI style button.
     * Button with slightly rounded corners.
     */
    private static Drawable createFtiStyleButtonBackground(Context context, int colorStart, int colorEnd) {
        float cornerRadius = context.getResources().getDimension(R.dimen.rounded_fti_button_border_width_proximus);

        int[] colors = {colorStart, colorEnd};
        GradientDrawable out = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
        out.setCornerRadius(cornerRadius);

        return out;
    }

    public static Drawable createMoreButtonDrawable(Context context, LayoutConfig layoutConfig) {
        int[] pressedStateList = new int[]{android.R.attr.state_pressed};
        int[] focusedStateList = new int[]{android.R.attr.state_focused};
        int[] hoveredStateList = new int[]{android.R.attr.state_hovered};
        int[] activeStateList = new int[]{android.R.attr.state_active};
        int[] defaultStateList = new int[]{};

        StateListDrawable stateListDrawable = new StateListDrawable();
        int strokeColor = context.getResources().getColor(R.color.proximus_style_button_stroke);

        // Pressed
        stateListDrawable.addState(pressedStateList,
                createMoreButtonDrawable(context,
                        layoutConfig.getButtonPressedColorStart(), layoutConfig.getButtonPressedColorEnd()));

        // Focused
        stateListDrawable.addState(focusedStateList,
                createMoreButtonDrawable(context,
                        layoutConfig.getButtonFocusedColorStart(), layoutConfig.getButtonFocusedColorEnd()));
        // Hovered
        stateListDrawable.addState(hoveredStateList,
                createMoreButtonDrawable(context,
                        layoutConfig.getButtonFocusedColorStart(), layoutConfig.getButtonFocusedColorEnd()));

        // Active
        stateListDrawable.addState(activeStateList,
                createMoreButtonDrawable(context,
                        layoutConfig.getButtonActiveColorStart(), strokeColor));
        // No state
        stateListDrawable.addState(defaultStateList,
                createMoreButtonDrawable(context,
                        layoutConfig.getButtonNoStateColorStart(), strokeColor));

        return stateListDrawable;
    }

    private static GradientDrawable createMoreButtonDrawable(Context context, int color, int strokeColor) {
        int borderWidth = (int) context.getResources().getDimension(R.dimen.rounded_button_border_width_proximus);

        GradientDrawable out = new GradientDrawable();
        out.setColor(color);
        out.setStroke(borderWidth, strokeColor);

        return out;
    }

    /**
     * Set a semitransparent color to the string divider character.
     */
    public static SpannableString addDividerColor(String str, String divider, int color) {
        SpannableString spannableString = new SpannableString(str);
        int index = str.indexOf(divider);
        while (index >= 0) {
            spannableString.setSpan(new ForegroundColorSpan(color), index + 1, index + 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            index = str.indexOf(divider, index + 1);
        }

        return spannableString;
    }


    public static ColorStateList createButtonBrandingColorStateList(LayoutConfig layoutConfig) {
        return new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_focused},
                        new int[]{}
                },
                new int[]{
                        layoutConfig.getButtonFocusedFontColor(),
                        layoutConfig.getFontColor(),
                }
        );
    }

    public static StateListDrawable createFavoriteCardDrawable(int stateEnabledColor, int stateDefaultColor) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{-android.R.attr.state_enabled}, new ColorDrawable(stateEnabledColor));
        stateListDrawable.addState(new int[]{android.R.attr.state_selected}, new ColorDrawable(Color.TRANSPARENT));
        stateListDrawable.addState(new int[]{}, new ColorDrawable(stateDefaultColor));
        return stateListDrawable;
    }

    public static StateListDrawable createAppCardDrawable(int stateDefaultColor) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{android.R.attr.state_focused}, new ColorDrawable(Color.TRANSPARENT));
        stateListDrawable.addState(new int[]{}, new ColorDrawable(stateDefaultColor));
        return stateListDrawable;
    }

    public static int getDrawableFromString(String imageName, Context context) {
        if (TextUtils.isEmpty(imageName)) {
            return 0;
        }

        Resources resources = context.getResources();
        final int resourceId = resources.getIdentifier(imageName, "drawable",
                context.getPackageName());
        if (resourceId == 0) {
            Log.w(TAG, "Logo image not found! Name: " + imageName);
        }
        return resourceId;
    }

    /**
     * Load an image into a image view.
     * If the image is an URL, it will be added with glide, with scaling support for display density.
     * Otherwise, it will try to load the image from the drawable folders, with that name
     *
     * @param imageName the url or name of the local image from drawable
     * @param imageView the image container where we want to show the image
     */
    public static void loadImage(Translator translator, String imageName, final ImageView imageView) {
        loadImage(translator, imageName, imageView, null);
    }

    /**
     * Load an image into a image view.
     * If the image is an URL, it will be added with glide, with scaling support for display density.
     * Otherwise, it will try to load the image from the drawable folders, with that name
     *
     * @param imageName the url or name of the local image from drawable
     * @param imageView the image container where we want to show the image
     * @param imageLoadListener Listener to get notified when the image successfully loads.
     */
    public static void loadImage(Translator translator, String imageName,
                                 final ImageView imageView, @Nullable ImageLoadListener imageLoadListener) {
            if (imageName == null) {
                return;
            }

            // Get localized image URI.
            imageName = translator.get(imageName);

            // Image url
            if (URLUtil.isValidUrl(imageName)) {
                imageView.setVisibility(View.VISIBLE);
                Glide.with(imageView.getContext())
                        .asBitmap()
                        .load(imageName)
                        // adding this placeholder, so if the image is set twice, it won't flicker
                        .placeholder(imageView.getDrawable())
                        .centerInside()
                        .dontTransform()
                        .into(new ImageViewTarget<Bitmap>(imageView) {
                            @Override
                            protected void setResource(@Nullable Bitmap resource) {
                                imageView.setImageBitmap(resource);
                            }

                            @Override
                            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                super.onLoadFailed(errorDrawable);
                                if (imageLoadListener != null) {
                                    imageLoadListener.onImageLoadFailed();
                                }
                            }

                            @Override
                            public void onResourceReady(@NonNull Bitmap resource,
                                                        @Nullable Transition<? super Bitmap> transition) {
                                super.onResourceReady(resource, transition);
                                if (imageLoadListener != null) {
                                    imageLoadListener.onImageLoaded();
                                }
                            }
                        });


           // Local image
            } else {
                imageView.setVisibility(View.VISIBLE);
                int res = UiUtils.getDrawableFromString(imageName, imageView.getContext());
                if (res != 0) {
                    imageView.setImageResource(res);
                    if (imageLoadListener != null) {
                        imageLoadListener.onImageLoaded();
                    }
                } else {
                    if (imageLoadListener != null) {
                        imageLoadListener.onImageLoadFailed();
                    }
                }
            }
    }


    /**
     * Utility method to set color for the edit text cursor.
     * @param view The edit text for which the cursor color should be changed.
     * @param color The color of the cursor
     */
    public static void setCursorColor(EditText view, @ColorInt int color) {
        try {
            // Get the cursor resource id
            Field field = TextView.class.getDeclaredField("mCursorDrawableRes");
            field.setAccessible(true);
            int drawableResId = field.getInt(view);

            // Get the editor
            field = TextView.class.getDeclaredField("mEditor");
            field.setAccessible(true);
            Object editor = field.get(view);

            // Get the drawable and set a color filter
            Drawable drawable = ContextCompat.getDrawable(view.getContext(), drawableResId);
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);

            // Set the drawables
            field = editor.getClass().getDeclaredField("mDrawableForCursor");
            field.setAccessible(true);
            field.set(editor, drawable);
        } catch (Exception e) {
            Log.e(TAG, "Failed to set cursor drawable.", e);
        }
    }

    /**
     * Returns {@code true} if the given view is a descendant of the give container.
     */
    public static boolean isDescendant(ViewGroup container, View view) {
        if (view == null) {
            return false;
        }
        for (ViewParent p = view.getParent(); p != null; p = p.getParent()) {
            if (p == container) {
                return true;
            }
        }
        return false;
    }


    /**
     * Use this function to find the parental rating icon for a rating.
     *
     * @param parentalRating - input parental rating enum
     * @return - drawable of the rating level, null for the not defined / ratedAll
     */
    public static int getParentalRatingIcon(ParentalRating parentalRating) {
        switch (parentalRating) {
            case Rated10:
                return R.drawable.ico_parental_10;
            case Rated12:
                return R.drawable.ico_parental_12;
            case Rated14:
                return R.drawable.ico_parental_14;
            case Rated16:
                return R.drawable.ico_parental_16;
            case Rated18:
                return R.drawable.ico_parental_18;
            default:
                return 0;
        }
    }

    public static int dpToPx(Context context, float dp) {
        Resources r = context.getResources();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dp, r.getDisplayMetrics());
    }


    /**
     * Listener to get notified when an image resource loads.
     */
    public interface ImageLoadListener {
        void onImageLoaded();
        void onImageLoadFailed();
    }

    public static boolean runOnUiThread(Runnable work) {
        MainActivity activity = Components.get(Navigator.class).getActivity();
        if (activity == null || activity.isInstanceStateSaved()) {
            return false;
        }

        activity.runOnUiThread(work);
        return true;
    }
}
