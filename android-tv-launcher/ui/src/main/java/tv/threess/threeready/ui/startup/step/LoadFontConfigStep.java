package tv.threess.threeready.ui.startup.step;

import android.content.Context;

import java.util.concurrent.TimeUnit;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.home.model.local.FontConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.FontStylist;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Startup step to load and apply the font config.
 *
 * @author Barabas Attila
 * @since 2018.07.10
 */
public class LoadFontConfigStep implements Step {
    private static final String TAG = Log.tag(LoadFontConfigStep.class);

    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);
    private final Context mContext = Components.get(Navigator.class).getActivity();

    private Disposable mDisposable;


    @Override
    public void execute(StepCallback callback) {
        mAccountRepository.getFontConfigs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(mAppConfig.getQueryTimeout(), TimeUnit.SECONDS)
                .firstOrError()
                .subscribe(new SingleObserver<FontConfig[]>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mDisposable = d;
                    }

                    @Override
                    public void onSuccess(FontConfig[] value) {
                        Components.registerInstance(new FontStylist(mContext, value));
                        Log.d(TAG, "Font config injected");
                        callback.onFinished();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.w(TAG, "Failed to load the from config", e);

                        callback.onFinished();
                    }
                });
    }

    @Override
    public void cancel() {
        RxUtils.disposeSilently(mDisposable);
    }
}
