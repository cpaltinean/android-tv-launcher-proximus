package tv.threess.threeready.ui.settings.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.vod.model.IVodRentalTransaction;
import tv.threess.threeready.ui.databinding.VodPurchaseHistoryItemBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Presenter class for vod rental transaction items.
 *
 * @author Daniela Toma
 * @since 2022.02.09
 */
public class VodRentalTransactionItemPresenter extends BasePresenter<VodRentalTransactionItemPresenter.ViewHolder, IVodRentalTransaction> {
    public static final String TAG = Log.tag(VodRentalTransactionItemPresenter.class);

    private final Translator mTranslator = Components.get(Translator.class);
    private final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);

    public VodRentalTransactionItemPresenter(Context context) {
        super(context);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new VodRentalTransactionItemPresenter.ViewHolder(
                VodPurchaseHistoryItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false)
        );
    }

    @Override
    public void onBindHolder(ViewHolder holder, IVodRentalTransaction iVodRentalTransaction) {
        // Register parental control listener.
        holder.mParentalControlListener = new ParentalControlListener(holder, iVodRentalTransaction);
        mParentalControlManager.addListener(holder.mParentalControlListener);

        updateInfoRow(holder, iVodRentalTransaction);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);
        mParentalControlManager.removeListener(holder.mParentalControlListener);
    }

    private void updateInfoRow(ViewHolder holder, IVodRentalTransaction iVodRentalTransaction) {
        updateTitle(holder, iVodRentalTransaction);

        String day = LocaleTimeUtils.getDate(iVodRentalTransaction.getPurchaseDate(), mTranslator);
        holder.mBinding.historyDate.setText(day);
        String price = mLocaleSettings.getCurrency() + " " + StringUtils.formatPrice(iVodRentalTransaction.getAmount());
        holder.mBinding.historyAmount.setText(price);
        holder.mBinding.iconContainer.showParentalRating(iVodRentalTransaction.getRatingCode());
    }

    @Override
    public void onClicked(ViewHolder holder, IVodRentalTransaction iVodRentalTransaction) {
        if (mParentalControlManager.isRestricted(iVodRentalTransaction.getRatingCode())) {
            // Content blocked. Request PIN.
            mNavigator.showParentalControlUnblockDialog();
        }
    }

    private void updateTitle(ViewHolder holder, IVodRentalTransaction iVodRentalTransaction) {
        if (mParentalControlManager.isRestricted(iVodRentalTransaction.getRatingCode())) {
            holder.mBinding.title.setText(mTranslator.get(TranslationKey.CARDS_TITLE_CONTENT_LOCKED));
            holder.mBinding.lockItem.setVisibility(View.VISIBLE);
        } else {
            holder.mBinding.title.setText(iVodRentalTransaction.getTitle());
            holder.mBinding.lockItem.setVisibility(View.GONE);
        }
    }

    private class ParentalControlListener implements ParentalControlManager.IParentalControlListener {

        ViewHolder mHolder;
        IVodRentalTransaction mItem;

        ParentalControlListener(ViewHolder holder, IVodRentalTransaction item) {
            mItem = item;
            mHolder = holder;
        }

        @Override
        public void onParentalRatingChanged(ParentalRating parentalRating) {
            updateTitle(this.mHolder, this.mItem);
        }
    }

    public static class ViewHolder extends BasePresenter.ViewHolder {

        private final VodPurchaseHistoryItemBinding mBinding;
        ParentalControlManager.IParentalControlListener mParentalControlListener;

        public ViewHolder(VodPurchaseHistoryItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
