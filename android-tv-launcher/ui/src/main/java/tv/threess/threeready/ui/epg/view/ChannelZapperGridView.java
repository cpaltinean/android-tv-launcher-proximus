/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.epg.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.leanback.widget.HorizontalGridView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SafeScrollGridLayoutDelegate;

import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.epg.presenter.TvChannelZapperCardPresenter;
import tv.threess.threeready.ui.epg.presenter.TvProgramGuideCardPresenter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeSectionSeparatorDecorator;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeSeparator;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeSeparatorProvider;

/**
 * Horizontal grid view which holds the channel zapper cards. {@link TvChannelZapperCardPresenter}
 * The selected channel card has extra horizontal spacing
 * in order to provide space for the displayed program card. {@link TvProgramGuideCardPresenter}
 *
 * @author Barabas Attila
 * @since 2017.04.26
 */
public class ChannelZapperGridView extends HorizontalGridView {

    private int mSelectedChildSpacing;

    protected ItemBridgeAdapter mAdapter;

    protected StripeSeparatorProvider<StripeSeparator> mSeparatorProvider;
    StripeSectionSeparatorDecorator mSectionSeparatorDecorator;

    public ChannelZapperGridView(Context context) {
        this(context, null);
    }

    public ChannelZapperGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChannelZapperGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        setFocusable(false);

        // Align select child always to the middle
        setWindowAlignment(WINDOW_ALIGN_NO_EDGE);

        setItemSpacing(getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_item_spacing));
        mSelectedChildSpacing = getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_selected_item_spacing);

        int cardTranslationY = getContext().getResources().getDimensionPixelOffset(R.dimen.zapper_card_translation_y);
        mSectionSeparatorDecorator = new StripeSectionSeparatorDecorator(cardTranslationY, this::getChildDrawTranslateOffset);

        addItemDecoration(mSectionSeparatorDecorator);
    }

    @Override
    public void setLayoutManager(@Nullable  RecyclerView.LayoutManager layout) {
        super.setLayoutManager(layout);
        SafeScrollGridLayoutDelegate.setSafeScrollGridLayoutDelegate(this, layout);
    }

    // Receive notification about data set changes.
    private final RecyclerView.AdapterDataObserver mAdapterDataObserver = new RecyclerView.AdapterDataObserver() {

        @Override
        public void onChanged() {
            super.onChanged();
            updateSeparators();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
            updateSeparators();
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            super.onItemRangeMoved(fromPosition, toPosition, itemCount);
            updateSeparators();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            updateSeparators();
        }
    };

    private void updateSeparators() {
       if (mSeparatorProvider != null) {
           mSectionSeparatorDecorator.setSeparators(mSeparatorProvider.getSeparators());
       }
    }

    public void setAdapter(ItemBridgeAdapter adapter, StripeSeparatorProvider<StripeSeparator> separatorProvider) {
        // Unregister previous data source observer.
        if (mAdapter != null) {
            mAdapter.unregisterAdapterDataObserver(mAdapterDataObserver);
        }

        mAdapter = adapter;
        mSeparatorProvider = separatorProvider;

        super.setAdapter(adapter);
        mSectionSeparatorDecorator.setAdapter(adapter);

        // Observe data source changes
        if (adapter != null) {
            adapter.registerAdapterDataObserver(mAdapterDataObserver);
        }

        updateSeparators();
    }

    @Override
    public void onViewAdded(View child) {
        super.onViewAdded(child);

        child.setActivated(isActivated());
    }

    @Override
    public boolean drawChild(Canvas canvas, View child, long drawingTime) {
        if (!shouldDrawChild(child)) {
            return true;
        }

        // Shift the child to left and right from middle.
        canvas.save();
        canvas.translate(getChildDrawTranslateOffset(child), 0);
        boolean ret = super.drawChild(canvas, child, drawingTime);
        canvas.restore();
        return ret;
    }

    protected boolean shouldDrawChild(View child) {
       return getChildDrawTranslateOffset(child) != 0;
    }

    /**
     * Calculate the translation for the child from the middle.
     */
    public int getChildDrawTranslateOffset(View child) {
        // Calculate the selected child alignment position.
        int parentAlignmentPosition = getWindowAlignmentPosition();
        int childAlignmentCenterX = child.getLeft() + child.getWidth() / 2;
        int centerAlignmentDiff = parentAlignmentPosition - childAlignmentCenterX;

        if (centerAlignmentDiff == 0) {
            return 0;
        } else if (centerAlignmentDiff < 0) {
            return mSelectedChildSpacing;
        } else {
            return -mSelectedChildSpacing;
        }
    }

    /**
     * Calculate the window window alignment position
     */
    public int getWindowAlignmentPosition() {
        return Math.round(getWidth() * getWindowAlignmentOffsetPercent() / 100f) + getWindowAlignmentOffset();
    }

    /**
     * Returns the currently selected view child.
     */
    @Nullable
    public View getSelectedChild() {
        ViewHolder holder = getSelectedViewHolder();
        if (holder != null) {
            return holder.itemView;
        }

        return null;
    }

    /**
     * Returns the currently selected view holder.
     */
    @Nullable
    protected ViewHolder getSelectedViewHolder() {
        int selectedPosition = getSelectedPosition();
        if (selectedPosition != NO_POSITION) {
            return findViewHolderForAdapterPosition(selectedPosition);
        }

        return null;
    }

    public int getSelectedChildSpacing() {
        return mSelectedChildSpacing;
    }

    public void setSelectedChildSpacing(int selectedChildSpacing) {
        mSelectedChildSpacing = selectedChildSpacing;
        invalidate();
    }
}
