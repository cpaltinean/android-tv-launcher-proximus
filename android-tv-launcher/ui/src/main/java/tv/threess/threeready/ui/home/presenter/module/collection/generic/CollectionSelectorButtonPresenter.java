package tv.threess.threeready.ui.home.presenter.module.collection.generic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.model.generic.ButtonStyle;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.databinding.CollectionButtonSelectorBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.home.view.ModularPageView;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Presenter to display a selector button for a collection.
 *
 * @author Barabas Attila
 * @since 9/4/21
 */
public class CollectionSelectorButtonPresenter extends BasePresenter<CollectionSelectorButtonPresenter.ViewHolder, DataSourceSelector> {

    private final Translator mTranslator = Components.get(Translator.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final ButtonStyle mButtonStyle = Components.get(ButtonStyle.class);

    private final SelectorChangeListener mChangeListener;
    private final ModularPageView.PageTitleCallback mPageTitle;

    public CollectionSelectorButtonPresenter(Context context, ModularPageView.PageTitleCallback pageTitle, SelectorChangeListener changeListener) {
        super(context);
        mChangeListener = changeListener;
        mPageTitle = pageTitle;
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        CollectionButtonSelectorBinding binding = CollectionButtonSelectorBinding
                .inflate(LayoutInflater.from(parent.getContext()), parent, false);

        ViewHolder holder = new ViewHolder(binding);

        holder.mBinding.selectorButton.setBackground(
                UiUtils.createButtonBackground(parent.getContext(), mLayoutConfig, mButtonStyle));
        holder.mBinding.selectorButton.setTextColor(
                UiUtils.createButtonBrandingColorStateList(mLayoutConfig));

        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, DataSourceSelector selector) {
        if (selector.getSelectedOption() != null) {
            String buttonName = mTranslator.get(selector.getSelectedOption().getName());
            holder.mBinding.selectorButton.setText(buttonName);

            String pageTitle = mPageTitle.getPageTitle();
            holder.mBinding.selectorButton.setContentDescription(
                    new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_COLLECTION_SORT))
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_PAGE_TITLE, pageTitle)
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BUTTON_NAME, buttonName)
                            .toString()
            );
        }
    }

    @Override
    public void onClicked(ViewHolder holder, DataSourceSelector selector) {
        super.onClicked(holder, selector);

        mNavigator.showSelectorOptionChangeDialog(selector,
                clickedOption -> {
                    selector.selectOption(selector.getOptions().indexOf(clickedOption));

                    if (mChangeListener != null) {
                        mChangeListener.onSelectedOptionChanged();
                    }
                });
    }

    protected static class ViewHolder extends Presenter.ViewHolder {

        private final CollectionButtonSelectorBinding mBinding;

        public ViewHolder(CollectionButtonSelectorBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }


    /**
     * Listener to get notified when the selected option changes.
     */
    public interface SelectorChangeListener {

        void onSelectedOptionChanged();
    }
}
