package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.DrawableUtils;

/**
 * Custom view to display the icon for a playback action like Play/Pause/Rewind.
 *
 * @author Barabas Attila
 * @since 2018.10.04
 */
public class PlaybackActionView extends AppCompatImageView {
    private static final String TAG = Log.tag(PlaybackActionView.class);

    private final Drawable mBackgroundDrawable;
    private final Drawable mPlayDrawable;
    private final Drawable mFastForwardDrawable;
    private final Drawable mFastRewindDrawable;
    private final Drawable mPauseDrawable;

    public PlaybackActionView(Context context) {
        this(context, null);
    }

    public PlaybackActionView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PlaybackActionView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mBackgroundDrawable = context.getResources().getDrawable(R.drawable.playback_action_button_background, null);

        LayoutConfig layoutConfig = Components.get(LayoutConfig.class);
        mPlayDrawable = DrawableUtils.getDrawable(context, layoutConfig.getPlayIcon());
        mFastForwardDrawable = DrawableUtils.getDrawable(context, layoutConfig.getForwardIcon());
        mFastRewindDrawable = DrawableUtils.getDrawable(context, layoutConfig.getRewindIcon());
        mPauseDrawable = DrawableUtils.getDrawable(context, layoutConfig.getPauseIcon());
    }

    /**
     * Display the proper icon and backend for the selected action.
     */
    public void setAction(Action action) {
        if (action == null) {
            setImageDrawable(null);
            setBackground(null);
            return;
        }

        Log.d(TAG, "Set action " + action);
        switch (action) {
            case FAST_FORWARD:
                setImageDrawable(mFastForwardDrawable);
                setBackground(mBackgroundDrawable);
                break;
            case FAST_REWIND:
                setImageDrawable(mFastRewindDrawable);
                setBackground(mBackgroundDrawable);
                break;
            case PAUSE:
                setImageDrawable(mPauseDrawable);
                setBackground(mBackgroundDrawable);
                break;
            case PLAY:
            default:
                //There is no drawable for the play action
                setImageDrawable(null);
                setBackground(null);
                break;
        }
    }

    public enum Action {
        PLAY, PAUSE, FAST_FORWARD, FAST_REWIND
    }
}
