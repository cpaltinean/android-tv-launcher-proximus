package tv.threess.threeready.ui.epggrid.view;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.generic.helper.NumberUtils;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.ui.epggrid.presenter.EpgGridTimelinePresenter;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;

/**
 * Recycler view to display the EPG timeline row.
 *
 * @author Barabas Attila
 * @since 8/27/21
 */
public class EpgTimeLineView extends RecyclerView {

    // The time offset to display the init time at when the timeline displayed first time.
    public final static long INIT_TIME_DISPLAY_OFFSET = TimeUnit.MINUTES.toMillis(30);

    private static final int sWidthPerMinute = 20;

    private int mScrolledX;
    private long mWindowStart;
    private long mWindowEnd;

    private long mFocusReferenceStartTime;
    private long mFocusReferenceEndTime;

    private final Calendar mCalendar = Calendar.getInstance(TimeZone.getDefault());

    private final List<TimeLineChangeListener> mChangeListeners = new ArrayList<>();
    private final ArrayObjectAdapter<Long> mTimeAdapter = new ArrayObjectAdapter<>();

    public EpgTimeLineView(@NonNull Context context) {
        this(context, null);
    }

    public EpgTimeLineView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EpgTimeLineView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setLayoutManager(new LinearLayoutManager(
                getContext(), LinearLayoutManager.HORIZONTAL, false));
        setHasFixedSize(true);

        mTimeAdapter.setHasStableIds(true);
        ItemBridgeAdapter timeAdapter = new ItemBridgeAdapter(mTimeAdapter,
                new EpgGridTimelinePresenter(getContext(), this));
        setAdapter(timeAdapter);
    }

    /**
     * Set the length for the EPG timeline.
     * The EPG will only display data between these two times.
     * @param windowStart The UTC timestamp of the very start of the EPG.
     * @param windowEnd The UTC timestamp of the very end of the EPG.
     */
    public void setTimeWindow(long initTime, long windowStart, long windowEnd) {
        mWindowStart = windowStart;
        mWindowEnd = windowEnd;
        mFocusReferenceStartTime = mFocusReferenceEndTime = initTime;

        List<Long> timeList = new ArrayList<>();

        int initTimePosition = 0;

        // Scroll timeline so the init time is visible.
        long scrollTime = NumberUtils.clamp(initTime - INIT_TIME_DISPLAY_OFFSET, windowStart, windowEnd);
        long start = TimeBuilder.utc(windowStart).floor(TimeUnit.MINUTES, 10).get();
        long increment = TimeUnit.MINUTES.toMillis(10);
        for (; start <= windowEnd; start += increment) {
            if (start <= scrollTime && start + increment > scrollTime) {
                initTimePosition = timeList.size();
            }
            timeList.add(start);
        }

        mTimeAdapter.replaceAll(timeList);
        mScrolledX = convertMillisToPixel(scrollTime - windowStart);

        // Scroll to init time.
        LinearLayoutManager layoutManager = (LinearLayoutManager) getLayoutManager();
        if (layoutManager != null) {
            layoutManager.scrollToPositionWithOffset(initTimePosition,
                    convertMillisToPixel(timeList.get(initTimePosition) - scrollTime));
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        notifyTimeLineChange(getFrom(), getTo());
    }

    /**
     * Add a listener to get notification about the visible timeline changes.
     * @param listener The listener to register.
     */
    public void addTimeLineChangeListener(TimeLineChangeListener listener) {
        mChangeListeners.add(listener);
    }

    /**
     * Remove a previously registered timeline change listener.
     * @param listener The listener to remove.
     */
    public void removeTimeLineChangeListener(TimeLineChangeListener listener) {
        mChangeListeners.remove(listener);
    }

    /**
     * @return The very start of the EPG timeline.
     * No data displayed before this time.
     */
    public long getWindowStart() {
        return mWindowStart;
    }

    /**
     * @return The very end of the EPG timeline.
     * No data displayed after this time.
     */
    public long getWindowEnd() {
        return mWindowEnd;
    }

    /**
     * @return The start of the currently visible area of the timeline in UTC.
     */
    public long getFrom() {
        return getWindowStart() + convertPixelToMillis(mScrolledX);
    }

    /**
     * @return The end of the currently visible area of the timeline in UTC.
     */
    public long getTo() {
        return getFrom() + convertPixelToMillis(getWidth());
    }


    /**
     * Set the timeframe in which to focus should be in the timeline.
     * @param focusReferenceStartTime The start of the focus time frame in millis.
     * @param focusReferenceEndTime The end of the focus time frame in millis.
     */
    public void setFocusReferenceTime(long focusReferenceStartTime, long focusReferenceEndTime) {
        mFocusReferenceStartTime = focusReferenceStartTime;
        mFocusReferenceEndTime = focusReferenceEndTime;
    }

    /**
     * @return The time in millis at which the program needs to be focused on the selected channel.
     */
    public long getFocusReferenceTime() {
        long clampedStart = NumberUtils.clamp(mFocusReferenceStartTime, getFrom(), getTo());
        long clampedEnd = NumberUtils.clamp(mFocusReferenceEndTime, getFrom(), getTo());
        return clampedStart + (clampedEnd - clampedStart) / 2;
    }

    /**
     * @return True if the current time is visible on the timeline.
     */
    public boolean isLiveTimeVisible() {
        long now = System.currentTimeMillis();
        return getFrom() <= now && getTo() > now;
    }

    /**
     * Relative smooth scroll by the given amount of time.
     *
     * @param time The tim to scroll by in millis.
     */
    public void smoothScrollByTime(long time) {
        smoothScrollBy(convertMillisToPixel(time), 0);
    }

    public void scrollByTime(long time) {
        scrollBy(convertMillisToPixel(time), 0);
    }

    @Override
    public void onScrolled(int dx, int dy) {
        super.onScrolled(dx, dy);
        long prevFrom = getFrom();
        long prevTo = getTo();

        mScrolledX += dx;

        long currentFrom = getFrom();
        long currentTo = getTo();

        // Notify timeline change.
        if (currentFrom != prevFrom || currentTo != prevTo) {
            notifyTimeLineChange(currentFrom, currentTo);
        }

        // Notify selected date change.
        mCalendar.setTimeInMillis(prevFrom);
        int prevDay = mCalendar.get(Calendar.DAY_OF_YEAR);
        mCalendar.setTimeInMillis(currentFrom);
        int currentDay = mCalendar.get(Calendar.DAY_OF_YEAR);
        if (prevDay != currentDay) {
            notifySelectedDateChanged(
                    TimeBuilder.local(currentFrom).floor(TimeUnit.DAYS).get());
        }
    }

    private void notifyTimeLineChange(long from, long to) {
        for (TimeLineChangeListener listener : mChangeListeners) {
            listener.onTimeLineChanged(from, to);
        }
    }

    private void notifySelectedDateChanged(long selectedDate) {
        for (TimeLineChangeListener listener : mChangeListeners) {
            listener.onSelectedDateChanged(selectedDate);
        }
    }

    /**
     * Gets the time in millis that corresponds to the given pixels in the program guide.
     */
    public long convertPixelToMillis(int pixel) {
        return pixel * TimeUnit.MINUTES.toMillis(1) / sWidthPerMinute;
    }

    /**
     * Gets the number of pixels in program guide table that corresponds to the given milliseconds.
     */
    public int convertMillisToPixel(long millis) {
        return (int) (millis * sWidthPerMinute / TimeUnit.MINUTES.toMillis(1));
    }

    /**
     * @return The duration of the interval inside the timeline window.
     */
    public long getDurationInWindow(long start, long end) {
        return Math.min(end, mWindowEnd) - Math.max(start, mWindowStart);
    }

    /**
     * Listener to get notification about currently visible time in the timeline.
     */
    public interface TimeLineChangeListener {

        /**
         * Called when the visible time in the timeline changed.
         * @param from The UTC timestamp of the start of the visible timeline.
         * @param to The UTC timestamp of the end of the visible timeline.
         */
        default void onTimeLineChanged(long from, long to) {}

        /**
         * @param selectedDate The UTC timestamp of the day currently visible in the timeline.
         */
        default void onSelectedDateChanged(long selectedDate) {}
    }
}
