/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.details.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.details.DetailOpenedFrom;
import tv.threess.threeready.ui.details.presenter.SeriesDetailsOverviewPresenter;
import tv.threess.threeready.ui.details.presenter.SeriesRecordingDetailsOverviewPresenter;
import tv.threess.threeready.ui.details.presenter.SeriesRecordingRowPresenter;
import tv.threess.threeready.ui.details.presenter.SeriesRowPresenter;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Detail page for a series recording.
 * This will show an overlay with the series info and an episode list.
 *
 * @author Barabas Attila
 * @since 2018.11.21
 */
public class SeriesRecordingDetailsFragment extends BaseSeriesDetailFragment<IRecording, IRecordingSeries, SeriesRecordingDetailsOverviewPresenter.ViewHolder> {
    public static final String TAG = Log.tag(SeriesRecordingDetailsFragment.class);

    private static final String EXTRA_RECORDING_SERIES_ID = "EXTRA_RECORDING_SERIES_ID";
    private static final String EXTRA_RECORDING = "EXTRA_RECORDING";
    private static final String EXTRA_OPENED_FROM = "EXTRA_OPENED_FROM";

    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);
    private DetailOpenedFrom mOpenedFrom;

    public static SeriesRecordingDetailsFragment newInstance(IRecording broadcast, DetailOpenedFrom openedFrom) {
        SeriesRecordingDetailsFragment fragment = new SeriesRecordingDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_RECORDING, broadcast);
        args.putSerializable(EXTRA_OPENED_FROM, openedFrom);
        fragment.setArguments(args);
        return fragment;
    }

    public static SeriesRecordingDetailsFragment newInstance(String seriesId, DetailOpenedFrom openedFrom) {
        SeriesRecordingDetailsFragment fragment = new SeriesRecordingDetailsFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_RECORDING_SERIES_ID, seriesId);
        args.putSerializable(EXTRA_OPENED_FROM, openedFrom);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSeriesId = getStringArgument(EXTRA_RECORDING_SERIES_ID);
        mEpisode = getSerializableArgument(EXTRA_RECORDING);
        mOpenedFrom = getSerializableArgument(EXTRA_OPENED_FROM);
    }

    /**
     * Load and display series details for the episode.
     *
     * @param episode The episode for which the series details should be loaded.
     */
    protected void loadSeriesDetails(final IRecording episode) {
        // Dispose channel list request
        RxUtils.disposeSilently(mDetailsDisposable);
        mDetailsDisposable = mPvrRepository.getSeriesRecording(episode.getSeriesId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(seriesRecording -> addUIData(seriesRecording, episode), throwable -> {
                    Log.e(TAG, "Failed to open series details for " + episode.getId(), throwable);
                    mNavigator.goBack();
                });
    }

    /**
     * Load and display series details
     *
     * @param seriesId The series for which the details should be loaded.
     */
    @Override
    protected void loadSeriesDetails(String seriesId) {
        // Dispose channel list request
        RxUtils.disposeSilently(mDetailsDisposable);
        mDetailsDisposable = mPvrRepository.getSeriesRecordingDetailModel(seriesId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataModel -> addUIData(dataModel.getSeriesRecording(), dataModel.getLastPlayedEpisode()), throwable -> {
                    Log.e(TAG, "Failed to open series details for " + seriesId, throwable);
                    mNavigator.goBack();
                });
    }

    @Override
    protected SeriesDetailsOverviewPresenter<SeriesRecordingDetailsOverviewPresenter.ViewHolder, IRecording, IRecordingSeries> getOverviewPresenter(IRecordingSeries series) {
        return new SeriesRecordingDetailsOverviewPresenter(getContext(), series, mOpenedFrom);
    }

    @Override
    protected SeriesRowPresenter<IRecording> getSeriesRowPresenter(IRecordingSeries series, IRecording selectedEpisode) {
        return new SeriesRecordingRowPresenter(
                getContext(),
                selectedEpisode,
                mOpenedFrom, this
        );
    }

    @Override
    protected void addExtraPresenter(InterfacePresenterSelector classPresenterSelector, IRecordingSeries series, IRecording selectedEpisode) {
        classPresenterSelector.addClassPresenter(IRecording.class, getOverviewPresenter(series));
        classPresenterSelector.addClassPresenter(IRecordingSeries.class, getSeriesRowPresenter(series, selectedEpisode));
    }

    @Override
    protected int getOverviewItemOffset(SeriesRecordingDetailsOverviewPresenter.ViewHolder viewHolder) {
        return getResources().getDimensionPixelOffset(R.dimen.series_detail_page_grid_row_top_margin);
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    protected Event<?, ?> generatePageOpenEvent() {
        if (mSeries == null) {
            return null;
        }

        IRecording recording = mEpisode != null ? mEpisode : mSeries.getFirstEpisode();

        if (recording == null) {
            return null;
        }

        return new Event<>(UILogEvent.PageOpen)
                .addDetail(UILogEvent.Detail.Page, UILog.Page.SeriesContentPage)
                .addDetail(UILogEvent.Detail.PageName, mSeries.getTitle())
                .addDetail(UILogEvent.Detail.FocusName, recording.getEpisodeTitle())
                .addDetail(UILogEvent.Detail.FocusId, ReportUtils.getFocusId(recording));
    }
}
