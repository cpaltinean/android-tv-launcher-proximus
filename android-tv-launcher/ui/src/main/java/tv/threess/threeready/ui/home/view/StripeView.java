/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.home.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.leanback.widget.HorizontalGridView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SafeScrollGridLayoutDelegate;

import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeItemSpaceDecorator;

/**
 * Horizontal grid view which allows overlapping items.
 *
 * @author Barabas Attila
 * @since 2017.05.24
 */
public class StripeView extends HorizontalGridView {

    private int mRowHeight;
    private int mItemSpacing;

    public StripeView(Context context) {
        super(context);
    }

    public StripeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StripeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onChildAttachedToWindow(View child) {
        super.onChildAttachedToWindow(child);
        child.setActivated(isActivated());
    }

    @Override
    public void setLayoutManager(@Nullable RecyclerView.LayoutManager layout) {
        super.setLayoutManager(layout);
        SafeScrollGridLayoutDelegate.setSafeScrollGridLayoutDelegate(this, layout);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        addItemDecoration(new StripeItemSpaceDecorator());
    }

    @Override
    public void setRowHeight(int height) {
        if (mRowHeight == height) {
            // Avoid redundant layouts.
            return;
        }
        mRowHeight = height;
        super.setRowHeight(height);
    }

    @Override
    public void setItemSpacing(int spacing) {
        if (mItemSpacing == spacing) {
            // Avoid redundant layouts.
            return;
        }

        mItemSpacing = spacing;
        super.setItemSpacing(spacing);
    }

    /**
     * The activated state is needed, because we decide by this state that the card has an overlay or not.
     */
    @Override
    public void requestChildFocus(View child, View focused) {
        // Active stripe when focus received.
        dispatchSetActivated(true);
        setActivated(true);

        super.requestChildFocus(child, focused);
    }

    @Override
    public View focusSearch(View focused, int direction) {
        View view = super.focusSearch(focused, direction);

        if (indexOfChild(view) == NO_POSITION) {
            // Deactivate stripe on focus lost.
            dispatchSetActivated(false);
            setActivated(false);
        }
        return view;
    }
}
