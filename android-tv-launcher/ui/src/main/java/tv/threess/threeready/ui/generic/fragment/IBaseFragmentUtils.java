package tv.threess.threeready.ui.generic.fragment;

import android.os.Bundle;
import android.os.Parcelable;

import androidx.annotation.Nullable;

import java.io.Serializable;

/**
 * Utils interface used for hold base methods for all fragments.
 *
 * @author David Bondor
 * @since 25.07.2022
 */
public interface IBaseFragmentUtils {

    Bundle getArguments();

    /**
     * Method used for getting the required value from the arguments by {@param key}.
     */
    default <T extends Serializable> T getSerializableArgument(String key) {
        return getSerializableArgument(key, null);
    }

    @SuppressWarnings("unchecked")
    @Nullable
    default <T extends Serializable> T getSerializableArgument(String key, T defValue) {
        if (getArguments() == null) {
            return defValue;
        }
        return (T) getArguments().get(key);
    }

    default <T extends Parcelable> T getParcelableArgument(String key) {
        return getParcelableArgument(key, null);
    }

    @SuppressWarnings("unchecked")
    @Nullable
    default <T extends Parcelable> T getParcelableArgument(String key, T defValue) {
        if (getArguments() == null) {
            return defValue;
        }
        return (T) getArguments().getParcelable(key);
    }

    default String getStringArgument(String key) {
        return getStringArgument(key, null);
    }

    default String getStringArgument(String key, String defValue) {
        if(getArguments() == null) {
            return defValue;
        }

        return getArguments().getString(key);
    }

    default boolean getBooleanArgument(String key) {
        return getBooleanArgument(key, false);
    }

    default boolean getBooleanArgument(String key, boolean defValue) {
        if (getArguments() == null) {
            return defValue;
        }

        Boolean b = (Boolean) getArguments().get(key);
        return b != null && b;
    }

    /**
     * Method used for remove a specific value from argument by {@param key}
     */
    default void removeArgument(String key) {
        if (getArguments() == null) {
            return;
        }
        getArguments().remove(key);
    }
}
