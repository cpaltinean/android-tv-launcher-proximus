package tv.threess.threeready.ui.epg.view;

import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.config.model.PlaybackSettings;
import tv.threess.threeready.api.generic.EpgFetchObservable;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.hint.HintManager;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.UserInactivityDetector;
import tv.threess.threeready.ui.generic.view.FontTextView;

/**
 * Program guide view which has 2 state. Collapsed and expended.
 * In collapsed state displays only one program.
 * In expanded state can display the programs for 7 days in past and 7 days in future
 *
 * @author Barabas Attila
 * @since 2017.11.16
 */

public abstract class ExpandableProgramGuideView extends ProgramGuideView {
    private static final String TAG = Log.tag(ExpandableProgramGuideView.class);
    private static final long TIME_SELECTION_DISPATCH_TIMEOUT = 200;

    private static final long EPG_MAX_WINDOW_LENGTH = TimeUnit.DAYS.toMillis(7);
    public final long PROGRAM_GUIDE_PAST_WINDOW_LENGTH;
    public final long PROGRAM_GUIDE_FUTURE_WINDOW_LENGTH;

    public static final long LIVE_PROGRAM_TIMESTAMP = -1;

    private final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final HintManager mHintManager = Components.get(HintManager.class);
    private final PlaybackSettings mPlaybackSettings = Components.get(PlaybackSettings.class);

    UserInactivityDetector mUserInactivityDetector;

    long mScrollReferenceTime = LIVE_PROGRAM_TIMESTAMP;

    EpgFetchObservable<List<IBroadcast>> mBroadcastObservable;
    private Disposable mProgramsDisposable;

    private TvGuideStateChangeListener mTvGuideStateChangeListener;
    protected OnReportListener mReportListener;

    private boolean mExpandProgramGuide = false;
    private Runnable mUpdateScrollTimeRunnable;
    private IBroadcast mLastSelectedProgram;
    private final long mPageSize;

    public ExpandableProgramGuideView(Context context) {
        this(context, null);
    }

    public ExpandableProgramGuideView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExpandableProgramGuideView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ExpandableProgramGuideView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        PROGRAM_GUIDE_PAST_WINDOW_LENGTH = Settings.epgPastWindowLength.get(EPG_MAX_WINDOW_LENGTH);
        PROGRAM_GUIDE_FUTURE_WINDOW_LENGTH = Settings.epgFutureWindowLength.get(EPG_MAX_WINDOW_LENGTH);

        AppConfig appConfig = Components.get(AppConfig.class);
        mUserInactivityDetector = new UserInactivityDetector(appConfig.getGuideHideTimeout(),
                this::onUserInactivity);

        ProgramGuideGridView gridView = getProgramGuideGridView();
        gridView.setWindowAlignmentOffsetPercent(0f);
        gridView.setWindowAlignmentOffset(getResources()
                .getDimensionPixelSize(R.dimen.tv_program_guide_item_alignment_offset));

        getMoreDownView().setText(mTranslator.get(TranslationKey.SREEN_EPG_TV_GUIDE));

        mPageSize = appConfig.getProgramGuidePageSize(TimeUnit.MILLISECONDS);
    }

    @NonNull
    protected abstract FontTextView getMoreDownView();

    @NonNull
    protected abstract ExpandableProgramGuideGridView getGridView();

    public void setTvGuideStateChangeListener(TvGuideStateChangeListener tvGuideStateChangeListener) {
        mTvGuideStateChangeListener = tvGuideStateChangeListener;
    }

    public void setReportListener(OnReportListener reportListener) {
        mReportListener = reportListener;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        getProgramGuideGridView().addOnChildViewHolderSelectedListener(mProgramGuideOnChildViewHolderSelected);
        mUserInactivityDetector.start();
    }

    public void onUserInteraction() {
        mUserInactivityDetector.reset();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        ExpandableProgramGuideGridView gridView = getGridView();
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_DOWN:
                    // Expend program guide.
                    if (!gridView.isExpanded()) {
                        expand(true);
                        return true;
                    }
                    break;

                case KeyEvent.KEYCODE_BACK:
                    // Collapse program guide.
                    mHintManager.removeScheduledContentFragmentHint();
                    if (mNavigator.isContentOverlayDisplayed()) {
                        if (gridView.isExpanded()) {
                            collapse(false);
                        } else {
                            mNavigator.hideContentOverlay();
                        }
                        return true;
                    }

                    return false;
                case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                    goToNextPreviousDayPrimeTime(true);
                    return true;
                case KeyEvent.KEYCODE_MEDIA_REWIND:
                    goToNextPreviousDayPrimeTime(false);
                    return true;
            }
        }

        return super.dispatchKeyEvent(event);
    }

    /**
     * Goes to the next/previous day prime time, when you reached the last prime time in the stripe the running program will be selected.
     *
     * @param isFuture flag stating if the times are in future or past.
     */
    private void goToNextPreviousDayPrimeTime(boolean isFuture) {
        if (!getGridView().isExpanded()) {
            return;
        }

        long referenceTime = mScrollReferenceTime;
        IBroadcast selected = getSelectedProgram();
        if (selected != null) {
            if (isFuture) {
                referenceTime = Math.max(referenceTime, selected.getEnd());
            } else {
                referenceTime = Math.min(referenceTime, selected.getStart());
            }
        }

        long primeTime = isFuture ?
                TimeUtils.nextPrimeTime(referenceTime, PROGRAM_GUIDE_FUTURE_WINDOW_LENGTH, mPlaybackSettings.getPrimeTime()) :
                TimeUtils.prevPrimeTime(referenceTime, PROGRAM_GUIDE_PAST_WINDOW_LENGTH, mPlaybackSettings.getPrimeTime());

        if (primeTime == LIVE_PROGRAM_TIMESTAMP) {
            mScrollReferenceTime = LIVE_PROGRAM_TIMESTAMP;
            selectCurrentProgram();
        } else {
            selectProgramAtTime(primeTime);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getGridView().removeOnChildViewHolderSelectedListener(mProgramGuideOnChildViewHolderSelected);
        mUserInactivityDetector.stop();
        mHandler.removeCallbacks(mUpdateScrollTimeRunnable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RxUtils.disposeSilently(mProgramsDisposable);
    }

    protected void onSelectedTimeChanged(long time, IBroadcast program) {
        if (!getGridView().isExpanded()) {
            mLastSelectedProgram = program;

            // The EPG is collapsed. Doesn't need to be loaded.
            return;
        }

        if (program == null || mLastSelectedProgram == null ||
                (Objects.equals(program.getChannelId(), mLastSelectedProgram.getChannelId()) &&
                        !Objects.equals(program.getId(), mLastSelectedProgram.getId()))) {
            Log.d(TAG, "Set the scroll reference time: " + Log.timestamp(time));
            mScrollReferenceTime = time;
        }

        mLastSelectedProgram = program;
        if (time == LIVE_PROGRAM_TIMESTAMP) {
            time = System.currentTimeMillis();
        }

        // Calculate the page size for the EPG load.
        if (isProgramReloadNeeded(time)) {
            loadProgramsAt(time);
        }
    }


    @Override
    public void onSelectedChannelChanged(@NonNull TvChannel channel, boolean isFromFavoriteSection) {
        mSelectedChannel = channel;
        mTvProgramGuidePresenter.setFromFavoriteSection(isFromFavoriteSection);

        if (getGridView().isExpanded()) {
            // Calculate the reference time from where to load program.
            long scrollReferenceTime = mScrollReferenceTime;
            if (scrollReferenceTime == LIVE_PROGRAM_TIMESTAMP) {
                scrollReferenceTime = System.currentTimeMillis();
            }

            // Reset loaded interval.
            mFrom = mTo = scrollReferenceTime ;

            loadProgramsAt(scrollReferenceTime);
        } else {
            String channelId = mPlaybackDetailsManager.getChannelId();

            long scrollTimePosition = System.currentTimeMillis();
            if (!channel.getId().equals(channelId)) {
                // Load live program.
                mScrollReferenceTime = LIVE_PROGRAM_TIMESTAMP;
                loadPrograms(channel, scrollTimePosition, scrollTimePosition);
                return;
            }

            // Load currently playing
            mScrollReferenceTime = initScrollReferenceTime();
            if(mScrollReferenceTime != LIVE_PROGRAM_TIMESTAMP) {
                scrollTimePosition = mScrollReferenceTime;
            }
            loadPrograms(channel, scrollTimePosition, scrollTimePosition);
        }
    }

    /**
     * @param time The time from where the program availability needs to be checked.
     * @return True if the programs at the reference time and next and previous pages are already loaded.
     */
    protected boolean isProgramReloadNeeded(long time) {
        long pageThreshold = mPageSize / 2;
        return time - pageThreshold < mFrom || time + pageThreshold > mTo;
    }

    /**
     * Load the programs at the given time if needed and prefetch the previous at next pages.
     * @param time The reference time where the program needs to be loaded.
     */
    protected void loadProgramsAt(long time) {
        Log.d(TAG, "Reload programs at time : " + Log.timestamp(time));
        // Calculate page start.
        long from = Math.min(mFrom, time - mPageSize);
        long windowStart = TimeBuilder.utc()
                .set(System.currentTimeMillis() - PROGRAM_GUIDE_PAST_WINDOW_LENGTH)
                .ceil(TimeUnit.HOURS).get();
        if (from < windowStart) {
            from = windowStart;
        }

        // Calculate page end.
        long to = Math.max(mTo, time + mPageSize);
        long windowEnd = TimeBuilder.utc()
                .set(System.currentTimeMillis() + PROGRAM_GUIDE_FUTURE_WINDOW_LENGTH)
                .ceil(TimeUnit.HOURS).get();
        if (to > windowEnd) {
            to = windowEnd;
        }

        // Load programs for page.
        if (from < mFrom || to > mTo) {
            loadPrograms(mSelectedChannel, from, to);
        }
    }

    /**
     * Load programs for database and backend for the selected channel.
     */
    @Override
    protected void loadPrograms(final TvChannel channel, long from, long to) {
        Log.d(TAG, "Load programs. Channel " + channel.getName() + ", " + channel.getId()
                + ", from " + Log.timestamp(from) + ", to " + Log.timestamp(to));

        if (from > to) {
            Log.e(TAG, "From (" + from + ") cannot be bigger or equal than to (" + to + ")");
            return;
        }

        mFrom = from;
        mTo = to;

        // Load app channel.
        if (channel.getType() == ChannelType.APP || channel.getType() == ChannelType.REGIONAL) {
            Single.create((SingleOnSubscribe<String>) e -> e.onSuccess("Success"))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<String>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            RxUtils.disposeSilently(mProgramsDisposable);
                            mProgramsDisposable = d;
                        }

                        @Override
                        public void onSuccess(String o) {
                            Log.d(TAG, "App channel loaded.");
                            if (mAdapter.size() != 0) {
                                mAdapter.clear();
                            }

                            // Update the current program
                            mAdapter.add(channel);
                            getGridView().setSelectedPosition(0);
                            invalidateMoreButtonVisibility();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, "Could not load app channel.", e);
                        }
                    });
            return;
        }

        if (mBroadcastObservable != null && mBroadcastObservable.prefetchPrograms(channel.getId(), from, to)) {
            // current observable can be reused
            return;
        }

        mBroadcastObservable = mTvRepository.getEpgBroadcastsBetweenTimestamps(channel, from, to,
                PROGRAM_GUIDE_PAST_WINDOW_LENGTH, PROGRAM_GUIDE_PAST_WINDOW_LENGTH,
                mTranslator.get(TranslationKey.MODULE_CARD_LOADING),
                mTranslator.get(TranslationKey.EPG_NO_INFO));

        Observable.create(mBroadcastObservable)
                .subscribeOn(Schedulers.io())
                .map(programs -> {
                    // Add timestamp divider for a day change
                    List<Object> programList = new ArrayList<>();
                    for (int i = 0; i < programs.size(); i++) {
                        programList.add(programs.get(i));
                        if (i != programs.size() - 1 && programs.get(i) != null && programs.get(i + 1) != null
                                && !LocaleTimeUtils.getDayNameLocalized(programs.get(i).getStart(), mTranslator, true)
                                .equals(LocaleTimeUtils.getDayNameLocalized(programs.get(i + 1).getStart(), mTranslator, true))) {
                            programList.add(programs.get(i + 1).getStart());
                        }
                    }

                    return programList;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Object>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        RxUtils.disposeSilently(mProgramsDisposable);
                        mProgramsDisposable = d;
                    }

                    @Override
                    public void onNext(final List<Object> programs) {
                        Log.d(TAG, "Program list loaded. " + programs.size() + ", first program type: " + ArrayUtils.first(programs));

                        replacePrograms(programs);
                        invalidateMoreButtonVisibility();

                        if (mKeyEvent != null) {
                            dispatchKeyEvent(mKeyEvent);
                            mKeyEvent = null;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "Could not load program list.", e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * Wait to scroll to finish and then replace programs.
     */
    private void replacePrograms(final List<Object> programs) {
        mAdapter.replaceAll(programs);
        selectCurrentProgram();
    }

    /**
     * Select program at the selected time position.
     */
    protected void selectCurrentProgram() {
        if (mScrollReferenceTime == LIVE_PROGRAM_TIMESTAMP) {
            selectLiveProgram();
        } else {
            selectProgramAtTime(mScrollReferenceTime);
        }
    }

    /**
     * Moves the focus to the live program in the grid.
     */
    private void selectLiveProgram() {
        for (int i = 0; i < mAdapter.size(); ++i) {
            // Ignore app channels.
            if (!(mAdapter.get(i) instanceof IBroadcast)) {
                continue;
            }

            IBroadcast program = (IBroadcast) mAdapter.get(i);
            if (program != null && program.isLive()) {
                getGridView().scrollToPosition(i);
                onSelectedTimeChanged(LIVE_PROGRAM_TIMESTAMP, program);
                break;
            }
        }
    }

    /**
     * Method used for update the scroll reference time if the current program is changed.
     */
    public void onCurrentProgramChanged(IBroadcast broadcast) {
        if (mSelectedChannel != null && Objects.equals(mSelectedChannel.getId(), broadcast.getChannelId()) &&
                !getGridView().isExpanded()) {
            mScrollReferenceTime = initScrollReferenceTime();
            long time = mScrollReferenceTime == LIVE_PROGRAM_TIMESTAMP ?
                    System.currentTimeMillis() : mScrollReferenceTime;
            loadPrograms(mSelectedChannel, time, time);
        }
    }

    /**
     * Moves the focus to last program which starts before timestamp
     */
    protected void selectProgramAtTime(long timestamp) {
        if (mAdapter.size() == 0
                || !(mAdapter.get(0) instanceof IBroadcast)) {
            return;
        }

        for (int i = 0; i < mAdapter.size(); i++) {
            IBroadcast broadcast = null;
            if (mAdapter.get(i) instanceof IBroadcast) {
                broadcast = (IBroadcast) mAdapter.get(i);
            }
            if (broadcast == null) {
                continue;
            }

            // Select program at time.
            if (broadcast.getEnd() > timestamp) {
                onSelectedTimeChanged(timestamp, broadcast);
                getGridView().setSelectedPosition(i);
                return;
            }
        }

        if (timestamp > System.currentTimeMillis()) {
            // Select last program.
            getGridView().setSelectedPosition(mAdapter.size() - 1);
        } else {
            // Select first program.
            getGridView().setSelectedPosition(0);
        }
    }

    /**
     * Display a whole vertical list with programs.
     */
    protected void expand(boolean animated) {
        ExpandableProgramGuideGridView gridView = getGridView();
        if (!gridView.isExpanded()
                && mSelectedChannel != null
                && (mSelectedChannel.getType() == ChannelType.TV || mSelectedChannel.getType() == ChannelType.RADIO)) {

            mUserInactivityDetector.start();
            setShouldExpandProgramGuide(true);

            gridView.expand(animated);
            //set the expansion on the card presenter
            mTvProgramGuidePresenter.setIsProgramGuideExpanded(true);

            // Show EPG hint.
            mHintManager.showEpgLayoutHints();

            invalidateMoreButtonVisibility();
            onSelectedTimeChanged(initScrollReferenceTime(), null);

            if (mTvGuideStateChangeListener != null) {
                mTvGuideStateChangeListener.onTvGuideOpened();
            }
        } else {
            mUserInactivityDetector.start();
        }
    }

    /**
     * @return The time which needs to be focused when the EPG opens.
     */
    public long initScrollReferenceTime() {
        if (mSelectedChannel == null
                || !Objects.equals(mPlaybackDetailsManager.getChannelId(), mSelectedChannel.getId())) {
            return LIVE_PROGRAM_TIMESTAMP;
        }

        if (mPlaybackDetailsManager.inPlaybackType(PlaybackType.Vod,
                PlaybackType.VodTrailer, PlaybackType.LiveTvOtt, PlaybackType.Recording)) {
            return LIVE_PROGRAM_TIMESTAMP;
        }

        long now = System.currentTimeMillis();
        long windowStart = now - PROGRAM_GUIDE_PAST_WINDOW_LENGTH;
        long position = mPlaybackDetailsManager.getPosition(now);

        if (position > windowStart) {
            return position;
        }

        return LIVE_PROGRAM_TIMESTAMP;
    }

    protected boolean shouldExpandProgramGuide() {
        return mExpandProgramGuide;
    }

    public void setShouldExpandProgramGuide(boolean expandProgramGuide) {
        mExpandProgramGuide = expandProgramGuide;
    }

    /**
     * Display only the selected program.
     */
    protected void collapse(final boolean animated) {
        if (mSelectedChannel == null) {
            return;
        }

        setShouldExpandProgramGuide(false);
        mScrollReferenceTime = initScrollReferenceTime();
        selectCurrentProgram();

        // stop inactivity detector to avoid inactivity action after collapsed guide.
        mUserInactivityDetector.stop();

        post(() -> {
            if (getGridView().isExpanded()) {
                getGridView().collapse(animated);

                if (mTvGuideStateChangeListener != null) {
                    mTvGuideStateChangeListener.onTvGuideClosed();
                }

                //set the collapsing on the card presenter
                mTvProgramGuidePresenter.setIsProgramGuideExpanded(false);
                invalidateMoreButtonVisibility();
            }
        });
    }

    @Override
    protected void onInternetStateChanged(boolean available) {
        if (mSelectedChannel != null && available) {
            if (getGridView().isExpanded()) {
                loadPrograms(mSelectedChannel, mFrom, mTo);
            } else {
                loadPrograms(mSelectedChannel,
                        System.currentTimeMillis(), System.currentTimeMillis());
            }
        }
    }

    /**
     * Display or hide the more button based on the channel type and grid state.
     */
    protected void invalidateMoreButtonVisibility() {
        FontTextView moreUpView = getMoreDownView();
        if (mSelectedChannel != null
                && (mSelectedChannel.getType() == ChannelType.TV
                || mSelectedChannel.getType() == ChannelType.RADIO)) {
            moreUpView.setVisibility(getGridView().isExpanded() ? View.GONE : View.VISIBLE);
        } else {
            moreUpView.setVisibility(View.GONE);
        }
    }

    /**
     * Get the currently selected program in program guide.
     * It will return null in case of app channels.
     */
    @Nullable
    public IBroadcast getSelectedProgram() {
        int selectedPosition = NO_POSITION;

        ExpandableProgramGuideGridView gridView = getGridView();
        View focusedView = gridView.getFocusedChild();
        if (focusedView != null) {
            selectedPosition = gridView
                    .getChildViewHolder(focusedView).getBindingAdapterPosition();
        }

        if (selectedPosition == NO_POSITION) {
            return null;
        }

        if (mAdapter.get(selectedPosition) instanceof IBroadcast) {
            return (IBroadcast) mAdapter.get(selectedPosition);
        }

        return null;
    }

    public IBroadcast getLastSelectedProgram() {
        return mLastSelectedProgram;
    }

    /**
     * Get the program on {@param selectedPosition}, if it`s available.
     */
    @Nullable
    public IBroadcast getProgramOnPosition(int selectedPosition) {
        if (selectedPosition == NO_POSITION) {
            return null;
        }

        if (selectedPosition < mAdapter.size() && mAdapter.get(selectedPosition) instanceof IBroadcast) {
            return (IBroadcast) mAdapter.get(selectedPosition);
        }

        return null;
    }

    protected void onUserInactivity() {
        // If hint doesn't show, hide program guide on user inactivity for 5 minutes
        if (getGridView().isExpanded()) {
            collapse(true);
        } else if (mTvGuideStateChangeListener != null) {
            mTvGuideStateChangeListener.onTvGuideClosed();
        }
    }

    private final OnChildViewHolderSelectedListener mProgramGuideOnChildViewHolderSelected = new OnChildViewHolderSelectedListener() {
        @Override
        public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subPosition) {
            super.onChildViewHolderSelected(parent, child, position, subPosition);

            // First selection. Notify immediately.
            if (mUpdateScrollTimeRunnable == null) {
                notifyProgramSelected(position);
                mUpdateScrollTimeRunnable = () -> {
                    mUpdateScrollTimeRunnable = null;
                };

            // Notify selection delayed. Wait for fast scroll.
            } else {
                mHandler.removeCallbacks(mUpdateScrollTimeRunnable);
                mUpdateScrollTimeRunnable = () -> {
                    notifyProgramSelected(position);
                    mUpdateScrollTimeRunnable = null;
                };
            }

            mHandler.postDelayed(mUpdateScrollTimeRunnable, TIME_SELECTION_DISPATCH_TIMEOUT);
        }

        private void notifyProgramSelected(int position) {
            IBroadcast selectedProgram = getProgramOnPosition(position);
            if (selectedProgram != null) {
                onSelectedTimeChanged(selectedProgram.isLive()
                        ? LIVE_PROGRAM_TIMESTAMP : selectedProgram.getStart(), selectedProgram);
                if (mReportListener != null) {
                    mReportListener.reportNavigation();
                }
            }
        }
    };

    public interface TvGuideStateChangeListener {
        void onTvGuideClosed();

        void onTvGuideOpened();
    }

    public interface OnReportListener {
        void reportNavigation();
    }
}
