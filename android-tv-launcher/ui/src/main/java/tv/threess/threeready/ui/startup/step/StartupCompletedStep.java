/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.startup.step;

import android.content.Intent;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.ui.generic.notification.OfflineModeNotification;

/**
 * Startup step to start the main page of the application.
 * All other steps should be already finished at this point.
 *
 * @author Barabas Attila
 * @since 2018.06.29
 */
public class StartupCompletedStep extends BaseStartupFinishedStep {
    private static final String TAG = Log.tag(StartupCompletedStep.class);

    private final long mStepCreatedTimestamp = System.currentTimeMillis();

    @Override
    public void onStartupCompleted(StepCallback callback) {
        super.onStartupCompleted(callback);
        mActivity.initializeOfflineModeNotification(OfflineModeNotification.newInstance());
        mNavigator.tuneToLastChannel(StartAction.Restart, true, false);

        PlaybackDetailsManager playbackDetailsManager = Components.get(PlaybackDetailsManager.class);
        IContentItem playingItem = playbackDetailsManager.getPlayingItem();

        if (playingItem != null) {
            Log.event(new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageSelection)
                    .addDetail(UILogEvent.Detail.Page, UILog.Page.Restart)
                    .addDetail(UILogEvent.Detail.OpenedBy, UILog.OpenedBy.Restart));
        }

    }

    @Override
    protected void showContentFragmentAfterStartup() {
        if (mActivity.wasApplicationRestarted()) {
            mNavigator.showHome();
        } else {
            mNavigator.showEpg(true);
        }

        if (mStepCreatedTimestamp < mActivity.getIntentReceivedTime()) {
            Intent intent = mActivity.getIntent();
            Log.d(TAG, "Handle intent received during startup. Intent : " + intent);
            mActivity.handleIntent(intent);
        }
    }

}
