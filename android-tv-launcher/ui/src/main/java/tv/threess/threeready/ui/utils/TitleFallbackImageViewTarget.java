package tv.threess.threeready.ui.utils;

import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import tv.threess.threeready.api.log.Log;

/**
 * Target view which handling glide image loading on cards where exist a logo and a text.
 * If there is no logo then need to make a visible text on card or vice versa.
 *
 * Created by Bartalus Csaba - Zsolt
 * since 2022.05.12
 */

public class TitleFallbackImageViewTarget extends DrawableImageViewTarget {

    private static final String TAG = Log.tag(TitleFallbackImageViewTarget.class);

    private final String mTitle;
    private final String mFallbackTitle;
    private final TextView mTitleTv;
    private final onResourceReadyListener mListener;

    public TitleFallbackImageViewTarget(@NonNull ImageView view, @NonNull TextView titleTv, String fallbackTitle) {
        this(view, titleTv, null, fallbackTitle, null);
    }

    public TitleFallbackImageViewTarget(@NonNull ImageView view, @NonNull TextView titleTv, String title, String fallbackTitle,
                                        @Nullable onResourceReadyListener listener) {
        super(view);
        mTitle = title;
        mFallbackTitle = fallbackTitle;
        mTitleTv = titleTv;
        mListener = listener;
    }

    @Override
    public void onLoadFailed(@Nullable Drawable errorDrawable) {
        super.onLoadFailed(errorDrawable);

        updateTitle(true);
        Log.e(TAG, "onLoadFailed() for drawable. Fallback title was set.");
    }

    @Override
    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
        super.onResourceReady(resource, transition);

        updateTitle(false);

        if (mListener != null) {
            mListener.onResourceReady();
        }

        Log.d(TAG, "onResourceReady() the image loaded with success");
    }

    public interface onResourceReadyListener {
        void onResourceReady();
    }

    private void updateTitle(boolean isResourceLoadFailed) {
        if (!TextUtils.isEmpty(mTitle)) {
            mTitleTv.setVisibility(View.VISIBLE);
            mTitleTv.setText(mTitle);
            return;
        }

        if (isResourceLoadFailed) {
            mTitleTv.setVisibility(View.VISIBLE);
            mTitleTv.setText(mFallbackTitle);
            return;
        }

        mTitleTv.setVisibility(View.GONE);
    }
}
