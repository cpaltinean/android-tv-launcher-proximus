package tv.threess.threeready.ui.home.presenter.module.stripe.gallery;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.leanback.widget.HorizontalGridView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.api.vod.wrapper.VodRentOffers;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.ui.databinding.GalleryVodOverviewBinding;
import tv.threess.threeready.ui.generic.adapter.DetailPageButtonOrder;
import tv.threess.threeready.ui.generic.model.VodPlayOption;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.ActionModel;
import tv.threess.threeready.ui.generic.presenter.RentActionModel;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.generic.view.FontTextView;

/**
 * Gallery overview presenter for {@link IBaseVodItem} items.
 *
 * @author Andor Lukacs
 * @since 12/6/18
 */
public class VodGalleryOverviewPresenter extends BaseGalleryOverviewPresenter<VodGalleryOverviewPresenter.ViewHolder, IBaseVodItem> {
    private static final String TAG = Log.tag(VodGalleryOverviewPresenter.class);

    private final Translator mTranslator = Components.get(Translator.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final PlaybackDetailsManager mPlaybackManager = Components.get(PlaybackDetailsManager.class);
    private final VodRepository mVodRepository = Components.get(VodRepository.class);

    public VodGalleryOverviewPresenter(Context context, ModuleData<?> moduleData) {
        super(context, moduleData);
    }

    @Override
    public ViewHolder createViewHolder(ViewGroup parent) {
        Log.d(TAG, "createViewHolder() called with: parent = [" + parent + "]");
        GalleryVodOverviewBinding binding = GalleryVodOverviewBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindHolder(ViewHolder holder, IBaseVodItem vod) {
        super.onBindHolder(holder, vod);
        Log.d(TAG, "onBindHolder() called with: holder = [" + holder + "], vod = [" + vod + "]");

        updateMarkersRow(holder, vod);
        updateInfoRow(holder, vod);
        processRentOffers(holder, vod);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);

        RxUtils.disposeSilently(holder.mDisposables);
    }

    @Override
    protected void onParentalRatingChanged(ViewHolder holder, IBaseVodItem item) {
        super.onParentalRatingChanged(holder, item);

        updateInfoRow(holder, item);
    }

    private void updateMarkersRow(ViewHolder holder, IBaseVodItem vod) {
        // Display content marker
        holder.mBinding.contentMarker.showMarkers(vod);

        holder.mBinding.iconsContainer.showParentalRating(vod.getParentalRating());

        updateDescriptiveAudioIcon(holder, vod);
        updateDescriptiveSubtitleIcon(holder, vod);

        if (vod.isHD()) {
            holder.mBinding.iconsContainer.showHDIcon();
        } else {
            holder.mBinding.iconsContainer.hideHDIcon();
        }

        Log.d(TAG, "Is dolby available : " + mPlaybackManager.isDolbyAvailable());
        if (mPlaybackManager.isContentItemPlaying(vod) && mPlaybackManager.isDolbyAvailable()) {
            holder.mBinding.iconsContainer.showDolby();
        } else {
            holder.mBinding.iconsContainer.hideDolby();
        }
    }

    private void updateDescriptiveSubtitleIcon(ViewHolder holder, IBaseVodItem vod) {
        if (vod.hasDescriptiveSubtitle()) {
            holder.mBinding.iconsContainer.showDescriptiveSubtitle();
        } else {
            holder.mBinding.iconsContainer.hideDescriptiveSubtitle();
        }
    }

    private void updateDescriptiveAudioIcon(ViewHolder holder, IBaseVodItem vod) {
        if (vod.hasDescriptiveAudio()) {
            holder.mBinding.iconsContainer.showDescriptiveAudio();
        } else {
            holder.mBinding.iconsContainer.hideDescriptiveAudio();
        }
    }


    /**
     * Get the rent offers for each VodVariant. Check for cheapest price / biggest rental time and update the UI with this info.
     *
     * @param movie the vod for which we check this
     */
    private void processRentOffers(ViewHolder holder, IBaseVodItem movie) {
        holder.mDisposables.add(mVodRepository.getVariantRentOffers(movie)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> updateButtons(holder, result),
                        throwable -> Log.d(TAG, "Error getting credits. ", throwable))
        );
    }

    private void updateInfoRow(VodGalleryOverviewPresenter.ViewHolder holder, IBaseVodItem vod) {
        holder.mBinding.infoRow.setText(getInfoRow(vod));
        holder.mBinding.infoRow.setVisibility(View.VISIBLE);
    }

    protected void updateButtons(ViewHolder holder, VodRentOffers rentOffers) {
        Map<String, IRentOffer> variantOfferMap = rentOffers.getVariantOfferMap();

        List<ActionModel> actions = new ArrayList<>();

        // Add watch button.
        IVodItem movie = rentOffers.getVod();
        List<VodPlayOption> vodPlayOptions = VodPlayOption.generatePlayOptions(movie, variantOfferMap);
        if (!vodPlayOptions.isEmpty()) {
            actions.add(new ActionModel(
                    mTranslator.get(TranslationKey.SCREEN_DETAIL_PLAY_BUTTON), DetailPageButtonOrder.Watch,
                    () -> {
                        if (vodPlayOptions.size() > 1) {
                            mNavigator.showPlayOptionsDialog(vodPlayOptions, movie);
                        } else {
                            VodPlayOption playOption = vodPlayOptions.get(0);
                            mNavigator.clearAllDialogs();
                            mNavigator.showVodPlayer(StartAction.Ui, playOption.getVodVariant(), movie, playOption.getVodPrice());
                        }
                    }));
        }

        // Add rent button.
        int credit = rentOffers.getCredit();
        if (movie.isRentable(credit, variantOfferMap)) {
            actions.add(new RentActionModel(DetailPageButtonOrder.Rent,
                    () -> mNavigator.showRentConfirmationDialog(movie),
                    credit, variantOfferMap
            ));
        }

        // Add trailer button
        for (IVodVariant variant : movie.getVariantList()) {
            if (variant.hasTrailer()) {
                actions.add(new ActionModel(
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_TRAILER_BUTTON), DetailPageButtonOrder.Trailer,
                        () -> mNavigator.showTrailerPlayer(StartAction.Ui, variant, movie)));
                break;
            }
        }

        // Add more info button.
        actions.add(new ActionModel(
                mTranslator.get(TranslationKey.SCREEN_DETAIL_MORE_INFO_BUTTON), DetailPageButtonOrder.MoreInfo,
                () -> mNavigator.showVodMovieDetails(movie)
        ));

        for (ActionModel actionModel : actions) {
            actionModel.setContentDescription(
                    new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_GALLERY_VOD_ITEM))
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BUTTON_NAME, actionModel.getText())
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_ASSET, movie.getTitle())
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_METADATA, getInfoRow(movie))
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_AGE_RATING, movie.getParentalRating().name())
                            .toString());
        }

        updateActionButtons(holder, movie, actions);
    }

    private String getInfoRow(IBaseVodItem vod) {
        List<String> infoList = new ArrayList<>();

        if (!mParentalControlManager.isRestricted(vod)) {
            infoList.add(vod.getReleaseYear());
            infoList.add(vod.getGenres().stream().findFirst().orElse(""));
            infoList.add(LocaleTimeUtils.getDuration(vod.getDuration(), mTranslator));
        }

        String availability = LocaleTimeUtils.getVodAvailabilityText(vod.getRentalRemainingTime(), mTranslator);
        if (!TextUtils.isEmpty(availability)) {
            infoList.add(availability);
        }

        return TextUtils.join(TimeUtils.DIVIDER, infoList);
    }

    static class ViewHolder extends BaseGalleryOverviewPresenter.ViewHolder {
        List<Disposable> mDisposables = new ArrayList<>();
        private final GalleryVodOverviewBinding mBinding;

        ViewHolder(GalleryVodOverviewBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        @Override
        @NotNull
        protected FontTextView getTitleView() {
            return mBinding.galleryTitle;
        }

        @Override
        @NotNull
        protected FontTextView getInfoRow() {
            return mBinding.infoRow;
        }

        @Override
        @NotNull
        protected ImageView getCoverImage() {
            return mBinding.imageCover;
        }

        @Override
        @NotNull
        protected HorizontalGridView getActionsGridView() {
            return mBinding.actionGrid;
        }
    }
}
