package tv.threess.threeready.ui.tv.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A custom drawable for seekBarView.
 * Responsible to load image from resources and calculates the image width, height and image bounds.
 *
 * @author Solyom Zsolt
 * @since 2020.03.17
 */
public class SeekBarDrawable extends Drawable {

    private final Drawable mDrawableImage;
    private final int mImageHeight;
    private final int mImageWidth;
    private final int mImageHalfHeight;
    private final int mImageHalfWidth;

    public SeekBarDrawable(Context context, int drawableId) {
        mDrawableImage = context.getResources().getDrawable(drawableId, null);
        mImageWidth = mDrawableImage.getIntrinsicWidth();
        mImageHeight = mDrawableImage.getIntrinsicHeight();
        mImageHalfHeight = mImageHeight / 2;
        mImageHalfWidth = mImageWidth / 2;
    }

    public Drawable getImage() {
        return mDrawableImage;
    }

    public int getImageHeight() {
        return mImageHeight;
    }

    public int getImageWidth() {
        return mImageWidth;
    }

    public int getImageHalfHeight() {
        return mImageHalfHeight;
    }

    public int getImageHalfWidth() {
        return mImageHalfWidth;
    }

    private Rect getBounds(float screenCoord, int height) {
        int left = (int) screenCoord - mImageHalfWidth;
        int top = (height / 2) - mImageHalfHeight;
        return new Rect(left, top, left + mImageWidth, top + mImageHeight);
    }

    public void calculateBounds(float screenCoord, int height) {
        mDrawableImage.setBounds(getBounds(screenCoord, height));
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        mDrawableImage.draw(canvas);
    }

    @Override
    public void setAlpha(int alpha) {
        mDrawableImage.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        mDrawableImage.setColorFilter(colorFilter);
    }

    @Override
    public int getOpacity() {
        return mDrawableImage.getOpacity();
    }

}
