package tv.threess.threeready.ui.settings.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.settings.model.SettingsItem;

/**
 * Fragment to display recordings settings.
 *
 * @author Andrei
 * @since 2018.11.22
 */
public class RecordingsSettingsFragment extends BaseSettingsDialogFragment<SettingsItem> {
    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);

    private Disposable mDisposable;

    public static RecordingsSettingsFragment newInstance() {
        return new RecordingsSettingsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null) {
            view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.settings_left_frame_color));
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        RxUtils.disposeSilently(mDisposable);
    }

    @Override
    protected void setTitle() {
        getTitleView().setText(mTranslator.get(TranslationKey.SETTINGS_RECORDINGS_TITLE));
    }

    protected void initView() {
        List<SettingsItem> items = new ArrayList<>();

        mDisposable = mPvrRepository.getSTCRecordingQuota()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recordingQuota -> {
                    int usedSpacePercentage = recordingQuota.getUsedRecordingSpacePercentage();
                    items.add(new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_RECORDINGS_AVAILABLE_SPACE),
                            (usedSpacePercentage >= 100 ? 0 : 100 - usedSpacePercentage) + "%"));
                    setItems(items);
                });
    }

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        return new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                .addDetail(UILogEvent.Detail.Page, UILog.Page.PVRSettings);
    }
}
