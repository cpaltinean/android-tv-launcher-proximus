package tv.threess.threeready.ui.epg.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.leanback.widget.Presenter;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.epg.presenter.SingleProgramGuidePresenter;
import tv.threess.threeready.ui.epg.presenter.TvProgramGuideCardPresenter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.utils.AnimatorUtils;

/**
 * Vertical program guide grid with collapse and expand animations.
 *
 * @author Barabas Attila
 * @since 2017.12.04
 */

public class ExpandableProgramGuideGridView extends ProgramGuideGridView  {

    private boolean mIsExpanded = false;
    private ValueAnimator mFadeAnimator;

    public ExpandableProgramGuideGridView(Context context) {
        this(context, null);
    }

    public ExpandableProgramGuideGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExpandableProgramGuideGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        collapse(false);
    }


    @Override
    public void onViewAdded(View child) {
        super.onViewAdded(child);

        invalidateFadeAnimation();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        AnimatorUtils.cancelAnimators(mFadeAnimator);
    }

    public boolean isExpanded() {
        return mIsExpanded;
    }

    /**
     * Display the whole program list in the grid.
     */
    public void expand(final boolean animated) {
        mIsExpanded = true;

        AnimatorUtils.cancelAnimators(mFadeAnimator);

        mFadeAnimator = ValueAnimator.ofFloat(0f, 1f);
        mFadeAnimator.addUpdateListener(updateListener -> invalidateFadeAnimation());
        mFadeAnimator.setupEndValues();
        mFadeAnimator.setDuration(animated ? getResources().getInteger(R.integer.content_frame_fade_duration) : 0);
        mFadeAnimator.start();
    }

    /**
     * Display only the selected program in the gird.
     */
    public void collapse(boolean animated) {
        mIsExpanded = false;

        stopScroll();

        AnimatorUtils.cancelAnimators(mFadeAnimator);

        mFadeAnimator = ValueAnimator.ofFloat(1f, 0f);
        mFadeAnimator.addUpdateListener(updateListener -> invalidateFadeAnimation());
        mFadeAnimator.setupEndValues();
        mFadeAnimator.setDuration(animated ? getResources().getInteger(R.integer.content_frame_fade_duration) : 0);
        mFadeAnimator.start();
    }

    @Override
    protected void onChildViewHolderSelected(ViewHolder child, int position) {
        super.onChildViewHolderSelected(child, position);

        invalidateFadeAnimation();
    }


    private void invalidateFadeAnimation() {
        if (mFadeAnimator == null) {
            return;
        }

        for (int i = 0; i < getChildCount(); ++i) {
            View child = getChildAt(i);

            // No need to animate the selected child.
            if (getChildAdapterPosition(child) == getSelectedPosition()) {
                child.setAlpha(1f);
                continue;
            }

            // Animate child
            child.setAlpha((float) mFadeAnimator.getAnimatedValue());
        }
    }

    /**
     * Show or hide the channel logo on the selected card.
     */
    public void setChannelLogoVisibility(int visible) {
        ViewHolder holder = getSelectedViewHolder();

        ItemBridgeAdapter.PresenterViewHolder bridgeHolder = (ItemBridgeAdapter.PresenterViewHolder) holder;
        if (bridgeHolder != null) {
            Presenter.ViewHolder presenterHolder = bridgeHolder.getViewHolder();
            if (presenterHolder != null) {
                if (presenterHolder instanceof TvProgramGuideCardPresenter.ViewHolder) {
                    ((TvProgramGuideCardPresenter.ViewHolder) bridgeHolder.getViewHolder()).getChannelLogoView().setVisibility(visible);
                } else if (presenterHolder instanceof SingleProgramGuidePresenter.ViewHolder) {
                    ((SingleProgramGuidePresenter.ViewHolder) bridgeHolder.getViewHolder()).getLogoView().setVisibility(visible);
                }
            }
        }
    }
}
