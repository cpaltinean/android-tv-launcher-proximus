package tv.threess.threeready.ui.settings.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.leanback.widget.VerticalGridView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.tv.epg.EpgLayoutType;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.EpgSettingsLayoutBinding;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.settings.model.EpgLayoutItem;

/**
 * Epg layout dialog showing the available options for displaying the epg.
 *
 * @author Daniela Toma
 * @since 2021.11.08
 */
public class EpgLayoutFragment extends BaseSettingsDialogFragment<EpgLayoutItem> {
    private static final String TAG = Log.tag(EpgLayoutFragment.class);

    private EpgSettingsLayoutBinding mBinding;

    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);

    Disposable mDisposable;

    public static EpgLayoutFragment newInstance() {
        return new EpgLayoutFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.settings_left_frame_color));
    }

    @Override
    protected void initView() {
        List<EpgLayoutItem> items = new ArrayList<>();
        items.add(new EpgLayoutItem(
                TranslationKey.SETTINGS_EPG_LAYOUT_CLASSIC_EPG,
                TranslationKey.CLASSIC_EPG_INFO,
                EpgLayoutType.ClassicEpg));
        items.add(new EpgLayoutItem(
                TranslationKey.SETTINGS_EPG_LAYOUT_MINI_EPG,
                TranslationKey.MINI_EPG_INFO,
                EpgLayoutType.MiniEpg));
        setItems(items);

        EpgLayoutType selectedItemType = Settings.epgLayoutType.getEnum(EpgLayoutType.MiniEpg);
        loadEpgLayoutImage(selectedItemType);
        focusCheckedItem(selectedItemType);

        mBinding.settingsGridView.addOnChildViewHolderSelectedListener(mEpgLayoutOnChildViewHolderSelected);
    }

    /**
     * Puts the focus on the checked item.
     */
    private void focusCheckedItem(EpgLayoutType selectedItemType) {
        if (mBinding.settingsGridView.getAdapter() == null) {
            return;
        }

        for (int i = 0; i < mBinding.settingsGridView.getAdapter().getItemCount(); i++) {
            EpgLayoutItem item = mAdapter.get(i);
            if (item.getEpgLayoutType() == selectedItemType) {
                mBinding.settingsGridView.setSelectedPosition(i);
                return;
            }
        }
    }

    @Override
    public void checkEpgLayoutItemRadioButton(EpgLayoutItem selectedItem) {
        mDisposable = mAccountRepository.setEpgLayout(selectedItem.getEpgLayoutType())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                            reportNavigation();
                            Log.d(TAG, "Successfully set the epg layout to: " + selectedItem.getEpgLayoutType().name());
                            if (mAdapter != null) {
                                mAdapter.notifyDataSourceChanged();
                            }
                        },
                        e -> {
                            Log.e(TAG, "Could not set the epg layout to " + selectedItem.getEpgLayoutType().name(), e);
                            mNavigator.showFailedToSaveSettingsDialog();
                        });


    }

    @Override
    protected void setTitle() {
        mBinding.title.setText(mTranslator.get(TranslationKey.SETTINGS_EPG_LAYOUT));
    }

    @NonNull
    protected View inflateLayout(){
        if (mBinding == null) {
            mBinding = EpgSettingsLayoutBinding.inflate(LayoutInflater.from(getContext()));
        }

        return mBinding.getRoot();
    }

    @NonNull
    protected View getRightFrame() {
        return mBinding.rightFrame;
    }

    protected VerticalGridView getVerticalGridView() {
        return mBinding.settingsGridView;
    }

    protected TextView getTitleView() {
        return mBinding.title;
    }

    @Override
    public void onStop() {
        super.onStop();
        RxUtils.disposeSilently(mDisposable);
    }

    /**
     * Loads the image for the specified EpgLayoutType.
     */
    private void loadEpgLayoutImage(EpgLayoutType type) {
        int imageId;
        if (type == EpgLayoutType.MiniEpg) {
            imageId = getMiniEpgImage();
        } else {
            imageId = getClassicEpgImage();
        }
        mBinding.epgImage.setImageDrawable(ContextCompat.getDrawable(mBinding.getRoot().getContext(), imageId));
    }

    /**
     * @return Gets the image that will be displayed when Mini guide is selected.
     */
    private int getMiniEpgImage() {
        switch (LocaleUtils.getApplicationLanguage()) {
            case LocaleUtils.DUTCH_LANGUAGE_CODE:
                return R.drawable.epg_layout_mini_guide_nl;
            case LocaleUtils.FRENCH_LANGUAGE_CODE:
                return R.drawable.epg_layout_mini_guide_fr;
            default:
                return R.drawable.epg_layout_mini_guide_en;
        }
    }

    /**
     * @return Gets the image that will be displayed when Classic guide is selected.
     */
    private int getClassicEpgImage() {
        switch (LocaleUtils.getApplicationLanguage()) {
            case LocaleUtils.DUTCH_LANGUAGE_CODE:
                return R.drawable.epg_layout_classic_guide_nl;
            case LocaleUtils.FRENCH_LANGUAGE_CODE:
                return R.drawable.epg_layout_classic_guide_fr;
            default:
                return R.drawable.epg_layout_classic_guide_en;
        }
    }

    private final OnChildViewHolderSelectedListener mEpgLayoutOnChildViewHolderSelected = new OnChildViewHolderSelectedListener() {
        @Override
        public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
            super.onChildViewHolderSelected(parent, child, position, subposition);
            EpgLayoutItem selectedItem = mAdapter.get(position);
            loadEpgLayoutImage(selectedItem.getEpgLayoutType());
        }
    };

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        int selectedPosition = mBinding.settingsGridView.getSelectedPosition();

        if (selectedPosition >= 0 && selectedPosition < mAdapter.size()) {
            EpgLayoutItem selectedItem = mAdapter.get(selectedPosition);

            return new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                    .addDetail(UILogEvent.Detail.Page, UILog.Page.TVGuideLayout)
                    .addDetail(UILogEvent.Detail.PageName, selectedItem.getTitle());
        }

        return null;
    }
}
