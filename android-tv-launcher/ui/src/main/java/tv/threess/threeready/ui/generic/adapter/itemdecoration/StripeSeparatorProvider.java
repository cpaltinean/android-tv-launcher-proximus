package tv.threess.threeready.ui.generic.adapter.itemdecoration;

import java.util.List;

/**
 * Provides section separators for stripe decorator to display.
 *
 * @author Barabas Attila
 * @since 2018.08.06
 */
public interface StripeSeparatorProvider <T extends StripeSeparator> {

    List<T> getSeparators();
}
