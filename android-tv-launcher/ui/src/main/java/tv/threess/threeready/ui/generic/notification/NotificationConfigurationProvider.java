package tv.threess.threeready.ui.generic.notification;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.R;

/**
 * Content provider for providing a configuration for the system notifications to the system.
 *
 * @author Eugen Guzyk
 * @since 2018.10.05
 */
public class NotificationConfigurationProvider extends ContentProvider {
    private static final String TAG = "NotificationConfigurationProvider";

    private static final String AUTHORITY = "tvlauncher.config";
    private static final String CONFIGURATION_DATA = "configuration";

    private static final int CONFIGURATION = 1;

    private static final UriMatcher sUriMatcher;

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(AUTHORITY, CONFIGURATION_DATA, CONFIGURATION);
    }

    @Nullable
    @Override
    public ParcelFileDescriptor openFile(@NonNull Uri uri, @NonNull String mode) throws
            FileNotFoundException {
        PipeDataWriter<?> writer;

        int uriMatch = sUriMatcher.match(uri);

        if (uriMatch == CONFIGURATION) {
            writer = (PipeDataWriter<Object>) (output, uri1, mimeType, opts, args) -> {
                FileOutputStream out = null;
                InputStream stream = null;

                Context context = getContext();
                if (context == null) {
                    return;
                }

                try {
                    // The configuration file is static and resides in
                    // the res/raw folder of the app.
                    stream = context.getResources().openRawResource(
                            R.raw.system_notification_configuration);

                    out = new FileOutputStream(
                            output.getFileDescriptor());
                    byte[] buffer = new byte[8192];
                    int count;
                    while ((count = stream.read(buffer)) != -1) {
                        out.write(buffer, 0, count);
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Failed to send file " + uri1, e);
                } finally {
                    FileUtils.closeSafe(stream);
                    FileUtils.closeSafe(out);
                }
            };
        } else {
            throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        return openPipeHelper(uri, "text/xml", null, null, writer);
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getType(@NonNull Uri uri) {
        throw new UnsupportedOperationException();
    }
}
