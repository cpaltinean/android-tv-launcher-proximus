package tv.threess.threeready.ui.startup.step;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * FTI step for notifying the user about the energy saver settings.
 *
 * @author Barabas Attila
 * @since 2018.07.02
 */
public class SaveEnergyStep implements Step {
    public static final String TAG = Log.tag(SaveEnergyStep.class);

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);

    private Disposable mDisposable;

    @Override
    public void execute(StepCallback callback) {
        if (Settings.powerConsumptionStepDisplayed.get(false)) {
            callback.onFinished();
        } else {
            mDisposable =
                    mAccountRepository.setPowerConsumptionStatus(true)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> {
                                        Log.d(TAG, "Power consumption status updated!");
                                        mNavigator.showSaveEnergySettingsFragment(callback);
                                    },
                                    e -> {
                                        Log.e(TAG, "Failed to update the power consumption status!", e);
                                        mNavigator.showSaveEnergySettingsFragment(callback);
                                    });
        }

    }

    @Override
    public void cancel() {
        RxUtils.disposeSilently(mDisposable);
    }

}
