package tv.threess.threeready.ui.settings.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.TimeUnit;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.helper.BundleBuilder;
import tv.threess.threeready.api.home.model.generic.ButtonStyle;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.databinding.ChannelScanBinding;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.receiver.GlobalKeyReceiver;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Fragment to display channel scanning content
 *
 * @author Karetka Mezei Zoltan
 * @since 2020.10.04
 */
public class ChannelScanFragment extends BaseFragment {
    private static final String TAG = Log.tag(ChannelScanFragment.class);

    private static final String USER_TRIGGERED = "USER_TRIGGERED";

    private static final long CLOSE_SUCCESS_DELAY = TimeUnit.SECONDS.toMillis(3);

    private final AppConfig mAppConfig = Components.get(AppConfig.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final TvRepository mTvRepository = Components.get(TvRepository.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final ButtonStyle mButtonStyle = Components.get(ButtonStyle.class);

    private ChannelScanBinding mBinding;

    private String scanStatus = TranslationKey.CHANNEL_SCAN_TITLE;
    private Disposable mDisposable;
    private View mView;

    private int mRetries = mAppConfig.getChannelScanRetries();

    private final Runnable mDismiss = this::close;

    public static ChannelScanFragment newInstance(boolean userTriggered) {
        ChannelScanFragment fragment = new ChannelScanFragment();
        fragment.setArguments(new BundleBuilder()
                .put(USER_TRIGGERED, userTriggered)
                .build()
        );
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = ChannelScanBinding.inflate(inflater, container, false);
        mView = mBinding.getRoot();
        return mView;
    }

    @Override
    public void onViewCreated(@androidx.annotation.NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.title.setText(mTranslator.get(TranslationKey.CHANNEL_SCAN_TITLE));
        mBinding.description.setText(mTranslator.get(TranslationKey.CHANNEL_SCAN_BODY));

        mBinding.close.setText(mTranslator.get(TranslationKey.CLOSE_BUTTON));
        mBinding.close.setTextColor(UiUtils.createButtonBrandingColorStateList(mLayoutConfig));
        mBinding.close.setBackground(UiUtils.createButtonBackground(getContext(), mLayoutConfig, mButtonStyle));
        mBinding.close.setOnClickListener(v -> close());

        mBinding.offlineButton.setText(mTranslator.get(TranslationKey.USE_OFFLINE_BUTTON));
        mBinding.offlineButton.setTextColor(UiUtils.createButtonBrandingColorStateList(mLayoutConfig));
        mBinding.offlineButton.setBackground(UiUtils.createButtonBackground(getContext(), mLayoutConfig, mButtonStyle));
        mBinding.offlineButton.setOnClickListener(v -> useOffline());

        view.announceForAccessibility(mTranslator.get(TranslationKey.TALKBACK_SCREEN_CHANNEL_SCAN));
    }

    @Override
    public void onStart() {
        super.onStart();
        performChannelScan();
    }

    @Override
    public void onStop() {
        RxUtils.disposeSilently(mDisposable);
        mView.removeCallbacks(mDismiss);
        super.onStop();
    }

    private void close() {
        mNavigator.tuneToLastChannel(StartAction.ScanComplete);
        mNavigator.popBackStack();
    }

    private void rebootBox() {
        PowerManager powerManager = (PowerManager) mApp.getSystemService(Context.POWER_SERVICE);
        powerManager.reboot(null);
    }

    private void useOffline() {
        mNavigator.popBackStack();
    }

    private void performChannelScan() {
        RxUtils.disposeSilently(mDisposable);

        mBinding.close.setVisibility(View.GONE);
        mBinding.offlineButton.setVisibility(View.GONE);
        mBinding.progressBar.setVisibility(View.VISIBLE);
        mBinding.title.setText(mTranslator.get(TranslationKey.CHANNEL_SCAN_TITLE));
        mBinding.description.setText(mTranslator.get(TranslationKey.CHANNEL_SCAN_BODY));

        mRetries -= 1;
        mTvRepository.scanChannels()
            .timeout(mAppConfig.getChannelScanTimeOut(TimeUnit.SECONDS), TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new SingleObserver<Boolean>() {
                @Override
                public void onSubscribe(Disposable d) {
                    mDisposable = d;
                }

                @Override
                public void onSuccess(Boolean success) {
                    resolveScanResult(success);
                }

                @Override
                public void onError(Throwable e) {
                    Log.e(TAG, "Channel scan failed!", e);
                    resolveScanResult(false);
                }
            });
    }

    private void resolveScanResult(boolean success) {
        String keyTitle;
        String keyBody;
        String keyClose;
        if (success) {
            // Close
            keyClose = mTranslator.get(TranslationKey.CLOSE_BUTTON);
            mBinding.close.setOnClickListener(v -> close());
            keyTitle = TranslationKey.CHANNEL_SCAN_COMPLETE_TITLE;
            keyBody = TranslationKey.CHANNEL_SCAN_COMPLETE_BODY;
            mView.postDelayed(mDismiss, CLOSE_SUCCESS_DELAY);
            scanStatus = keyTitle;
        } else if (mRetries > 0) {
            // Try again
            keyClose = mTranslator.get(TranslationKey.TRY_AGAIN_BUTTON);
            mBinding.close.setOnClickListener(v -> performChannelScan());
            keyTitle = TranslationKey.CHANNEL_SCAN_FAIL_TITLE;
            keyBody = TranslationKey.CHANNEL_SCAN_FAIL_BODY;
            scanStatus = keyTitle;
        } else {
            // Reboot
            keyClose = mTranslator.get(TranslationKey.SCAN_FAILED_ALERT_BUTTON_REBOOT);
            mBinding.close.setOnClickListener(v -> rebootBox());
            if (getBooleanArgument(USER_TRIGGERED)) {
                keyTitle = TranslationKey.SCAN_TRIGGERING_FAILED_ALERT_TITLE;
                keyBody = TranslationKey.SCAN_TRIGGERING_FAILED_ALERT_BODY;
            } else {
                keyTitle = TranslationKey.SCAN_FAILED_ALERT_TITLE;
                keyBody = TranslationKey.SCAN_FAILED_ALERT_BODY;
            }
            scanStatus = TranslationKey.SCAN_FAILED_ALERT_TITLE;
        }

        reportNavigation();
        mBinding.close.setText(keyClose);
        mBinding.close.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_SCREEN_CHANNEL_SCAN_COMPLETE))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_DIALOG_TITLE, mTranslator.get(keyTitle))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BUTTON_NAME, keyClose)
                        .toString()
        );

        mBinding.close.setVisibility(View.VISIBLE);
        mBinding.offlineButton.setVisibility(success ? View.GONE : View.VISIBLE);
        mBinding.progressBar.setVisibility(View.GONE);
        mBinding.title.setText(mTranslator.get(keyTitle));
        mBinding.description.setText(mTranslator.get(keyBody));
        mView.restoreDefaultFocus();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        GlobalKeyReceiver.disable(context);
    }

    @Override
    public void onDetach() {
        RxUtils.disposeSilently(mDisposable);
        mView.removeCallbacks(mDismiss);
        GlobalKeyReceiver.enable(getContext());
        super.onDetach();
    }

    // Block all user interaction for system check for updates action, allow it for available/not available system update screens
    private boolean blocked(KeyEvent event) {
        if (!isVisible(mBinding.close, mBinding.offlineButton)) {
            // no button is visible, block the key
            return true;
        }
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_DPAD_UP:
            case KeyEvent.KEYCODE_DPAD_DOWN:
            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_DPAD_RIGHT:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                // allow navigation keys if any of the button is visible
                return false;
        }
        return true;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (blocked(event)) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    public boolean onKeyDown(KeyEvent event) {
        if (blocked(event)) {
            return true;
        }
        return super.onKeyDown(event);
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        if (blocked(event)) {
            return true;
        }
        return super.onKeyUp(event);
    }

    @Override
    public boolean onKeyLongPress(KeyEvent event) {
        if (blocked(event)) {
            return true;
        }
        return super.onKeyLongPress(event);
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    private static boolean isVisible(View... views) {
        for (View view : views) {
            if (view.getVisibility() == View.VISIBLE) {
                return true;
            }
        }
        return false;
    }

    @Nullable
    @Override
    protected Event<?, ?> generatePageOpenEvent() {
        return new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                .addDetail(UILogEvent.Detail.Page, UILog.Page.BroadCasterInfoPage)
                .addDetail(UILogEvent.Detail.PageName, scanStatus);
    }
}