package tv.threess.threeready.ui.settings.listener;

import tv.threess.threeready.ui.settings.model.EpgLayoutItem;

/**
 * This class will be used to notify if an EpgLayoutItem was selected.
 *
 * @author Daniela Toma
 * @since 2021.11.10
 */
public interface EpgLayoutItemClickListener extends ItemClickListener {
    default void checkEpgLayoutItemRadioButton(EpgLayoutItem selectedItem) {
    }
}
