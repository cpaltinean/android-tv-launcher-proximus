/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.media.AudioManager;
import android.os.PowerManager;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.List;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.player.inputservice.OttInputService;

import static android.content.pm.PackageManager.GET_SERVICES;

/**
 * Static helper for accessing device related information and operations.
 *
 * @author Barabas Attila
 * @since 2017.11.02
 */
public class SystemUtils {
    private static final String TAG = Log.tag(SystemUtils.class);

    /**
     * Force close and restart the application.
     */
    public static void restartApplication(Activity activity) {
        Intent intent = buildHomeIntent(new Intent());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(activity, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);

        // Restart the app after a small delay.
        if (mgr != null) {
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 200, pendingIntent);
        }

        // Kill the current process.
        activity.finishAffinity();
        System.exit(0);
    }

    /**
     * Restart a standard activity as home.
     */
    public static void restartActivityAsHome(Activity activity) {
        activity.finish();
        activity.startActivity(
                buildHomeIntent(activity.getIntent()));
    }

    /**
     * @return Modify the given default intent to open the app as home.
     */
    private static Intent buildHomeIntent(Intent intent) {
        // Clear standard activity category flags.
        intent.removeCategory(Intent.CATEGORY_DEFAULT);
        intent.removeCategory(Intent.CATEGORY_LAUNCHER);
        intent.removeCategory(Intent.CATEGORY_LEANBACK_LAUNCHER);

        // Add home category.
        intent.addCategory(Intent.CATEGORY_HOME);

        if (TextUtils.isEmpty(intent.getAction())) {
            intent.setAction(Intent.ACTION_MAIN);
        }

        // Home cannot be started with explicit intent.
        intent.setComponent(null);
        intent.setPackage(null);

        return intent;
    }

    /**
     * Reboot the box
     */
    public static void reboot(Activity activity, String reason) {
        try {
            PowerManager powerManager = (PowerManager) activity.getSystemService(Context.POWER_SERVICE);
            powerManager.reboot(reason);
        } catch (Exception e) {
            restartApplication(activity);
        }
    }

    /**
     * Stop background sound of other Music apps if any
     */
    public static void requestAudioFocus(Context context) {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        // Request audio focus for playback
        int result = am.requestAudioFocus(null, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            Log.d(TAG, "AudioManager.AUDIOFOCUS_REQUEST_GRANTED");
        }
    }

    public static void abandonAudioFocus(Context context) {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        // Request audio focus for playback
        am.abandonAudioFocus(null);
        Log.d(TAG, "AudioManager.abandonAudioFocus()");
    }

    /**
     * Sets the volume to maximum for audio streams
     */
    public static void resetVolume(Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int[] streamTypes = new int[]{
                AudioManager.STREAM_SYSTEM,
                AudioManager.STREAM_MUSIC,
                AudioManager.STREAM_NOTIFICATION,
                AudioManager.STREAM_DTMF,
                AudioManager.STREAM_ACCESSIBILITY
        };

        for (int streamType : streamTypes) {
            audioManager.setStreamVolume(streamType, audioManager.getStreamMaxVolume(streamType), AudioManager.FLAG_PLAY_SOUND);
        }
    }

    /**
     * Display the virtual keyboard.
     */
    public static void showSoftInput(Activity activity) {
        InputMethodManager inputMethodManager = activity != null ? (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE) : null;
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(0, 0);
        }
    }

    /**
     * Display the virtual keyboard for a specific EditText.
     */
    public static void showSoftInput(Activity activity, EditText editText) {
        InputMethodManager inputMethodManager = activity != null ? (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE) : null;
        if (inputMethodManager != null) {
            inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    /**
     * Hide the displayed virtual keyboard.
     */
    public static void hideSoftInput(Activity activity) {
        InputMethodManager inputMethodManager = activity != null ? (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE) : null;
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    public static void disableComponent(Context context, Class<?> componentClass) {
        PackageManager pm = context.getPackageManager();
        int enabled = pm.getComponentEnabledSetting(new ComponentName(context, componentClass));
        if (PackageManager.COMPONENT_ENABLED_STATE_DISABLED != enabled) {
            pm.setComponentEnabledSetting(new ComponentName(context, componentClass),
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
        }
    }

    public static void enableComponent(Context context, Class<?> componentClass) {
        PackageManager pm = context.getPackageManager();
        int enabled = pm.getComponentEnabledSetting(new ComponentName(context, componentClass));
        if (PackageManager.COMPONENT_ENABLED_STATE_ENABLED != enabled) {
            pm.setComponentEnabledSetting(new ComponentName(context, componentClass),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
        }
    }

    /**
     * Check whether the given service class is in a separate process and it is currently the active process.
     *
     * @param context       Context for process check.
     * @param serviceClass  Class that is a separate service.
     * @return              true if the specified service process is active and false if not
     */
    public static boolean isInServiceProcess(Context context, Class<? extends Service> serviceClass) {
        Log.d(TAG, "Check whether the " + serviceClass.getCanonicalName() + " service process is the current process.");
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo;
        try {
            packageInfo = packageManager.getPackageInfo(context.getPackageName(), GET_SERVICES);
        } catch (Exception e) {
            Log.d(TAG, "Could not get package info for " + context.getPackageName());
            return false;
        }
        String mainProcess = packageInfo.applicationInfo.processName;

        ComponentName component = new ComponentName(context, serviceClass);
        ServiceInfo serviceInfo;
        try {
            serviceInfo = packageManager.getServiceInfo(component, 0);
        } catch (PackageManager.NameNotFoundException ignored) {
            // Service is disabled.
            return false;
        }

        if (serviceInfo.processName.equals(mainProcess)) {
            Log.d(TAG, String.format("Did not expect service %s to run in main process %s", serviceClass, mainProcess));
            // Technically we are in the service process, but we're not in the service dedicated process.
            return false;
        }

        int myPid = android.os.Process.myPid();
        ActivityManager activityManager =
                (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.RunningAppProcessInfo myProcess = null;
        List<ActivityManager.RunningAppProcessInfo> runningProcesses;
        try {
            runningProcesses = activityManager.getRunningAppProcesses();
        } catch (SecurityException exception) {
            // https://github.com/square/leakcanary/issues/948
            Log.d(TAG, "Could not get running app processes", exception);
            return false;
        }
        if (runningProcesses != null) {
            for (ActivityManager.RunningAppProcessInfo process : runningProcesses) {
                if (process.pid == myPid) {
                    myProcess = process;
                    break;
                }
            }
        }
        if (myProcess == null) {
            Log.d(TAG, "Could not find running process for "+ myPid);
            return false;
        }

        return myProcess.processName.equals(serviceInfo.processName);
    }

    /**
     * Check whether we are in the main process.
     *
     * @param context   Context for getting the process list.
     * @return          true if we are int he main process and false if not
     */
    public static boolean isInMainProcess(Context context) {
        Log.d(TAG, "Check whether the main process is the current process.");
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo;
        try {
            packageInfo = packageManager.getPackageInfo(context.getPackageName(), GET_SERVICES);
        } catch (Exception e) {
            Log.d(TAG, "Could not get package info for " + context.getPackageName());
            return false;
        }
        String mainProcess = packageInfo.applicationInfo.processName;

        int myPid = android.os.Process.myPid();
        ActivityManager activityManager =
                (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.RunningAppProcessInfo myProcess = null;
        List<ActivityManager.RunningAppProcessInfo> runningProcesses;
        try {
            runningProcesses = activityManager.getRunningAppProcesses();
        } catch (SecurityException exception) {
            Log.d(TAG, "Could not get running app processes", exception);
            return false;
        }
        if (runningProcesses != null) {
            for (ActivityManager.RunningAppProcessInfo process : runningProcesses) {
                if (process.pid == myPid) {
                    myProcess = process;
                    break;
                }
            }
        }
        if (myProcess == null) {
            Log.d(TAG, "Could not find running process for "+ myPid);
            return false;
        }

        return myProcess.processName.equals(mainProcess);
    }

    /**
     * Get the currently running processes type.
     * Based on the process type we might need to handle some initialization differently.
     *
     * @param context   Context for process check
     * @return          The {@link ProcessType} based on the currently running process.
     */
    public static ProcessType getCurrentProcessType(Context context) {
        if(SystemUtils.isInMainProcess(context)) {
            return ProcessType.MAIN;
        }

        if(SystemUtils.isInServiceProcess(context, OttInputService.class)) {
            return ProcessType.PLAYER;
        }

        return ProcessType.UNDEFINED;
    }

    /**
     * Enum to identify processes in the app for initialization
     */
    public enum ProcessType {
        MAIN,
        PLAYER,
        UNDEFINED
    }
}
