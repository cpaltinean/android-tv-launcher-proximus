package tv.threess.threeready.ui.generic.dialog;

import android.os.Bundle;
import android.view.KeyEvent;

import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Custom action bar dialog for the back to live warning.
 * It displays a message to notify the user that from replay program will go to live.
 *
 * @author Daniela Toma
 * @since 2019.11.07
 */
public class ReplayBackToLiveDialog extends ActionBarDialog {

    private boolean shouldDismiss = false;

    public static ReplayBackToLiveDialog newInstance(Translator translator, ButtonClickListener buttonClickListener) {
        DialogModel model = new ActionBarDialog.Builder()
            .title(translator.get(TranslationKey.ACTION_BAR_BACK_TO_LIVE_TITLE))
            .description(translator.get(TranslationKey.ACTION_BAR_BACK_TO_LIVE_BODY))
            .addButton(translator.get(TranslationKey.CONTINUE_BUTTON), buttonClickListener)
            .addButton(translator.get(TranslationKey.CANCEL_BUTTON))
            .dismissOnBtnClick(true)
            .blockKeys(false)
            .cancelable(false)
            .buildModel();

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_DIALOG_MODEL, model);
        ReplayBackToLiveDialog dialog = new ReplayBackToLiveDialog();
        dialog.setArguments(args);

        return dialog;
    }

    @Override
    public boolean onKeyDown(KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_CHANNEL_DOWN:
            case KeyEvent.KEYCODE_CHANNEL_UP:
            case KeyEvent.KEYCODE_0:
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_2:
            case KeyEvent.KEYCODE_3:
            case KeyEvent.KEYCODE_4:
            case KeyEvent.KEYCODE_5:
            case KeyEvent.KEYCODE_6:
            case KeyEvent.KEYCODE_7:
            case KeyEvent.KEYCODE_8:
            case KeyEvent.KEYCODE_9:
                shouldDismiss = true;
                return true;
            case KeyEvent.KEYCODE_TV_RADIO_SERVICE:
                return true;
        }
        return super.onKeyDown(event);
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {

        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_BACK:
                dismiss();
                return false;
            case KeyEvent.KEYCODE_CHANNEL_UP:
            case KeyEvent.KEYCODE_CHANNEL_DOWN:
            case KeyEvent.KEYCODE_0:
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_2:
            case KeyEvent.KEYCODE_3:
            case KeyEvent.KEYCODE_4:
            case KeyEvent.KEYCODE_5:
            case KeyEvent.KEYCODE_6:
            case KeyEvent.KEYCODE_7:
            case KeyEvent.KEYCODE_8:
            case KeyEvent.KEYCODE_9:
                if (shouldDismiss) {
                    dismiss();
                    shouldDismiss = false;
                }
                return true;
            case KeyEvent.KEYCODE_TV_RADIO_SERVICE:
                return true;
        }
        return super.onKeyUp(event);
    }
}
