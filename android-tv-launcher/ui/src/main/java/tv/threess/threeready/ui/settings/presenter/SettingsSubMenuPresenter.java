/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.settings.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.ui.databinding.SettingsTitleBinding;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;

/**
 * This class will bind the layout to the model of radio button items.
 *
 * @author Paul
 * @since 2017.05.30
 */

public class SettingsSubMenuPresenter extends BasePresenter<SettingsSubMenuPresenter.ViewHolder, String> {

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    public SettingsSubMenuPresenter(Context context) {
        super(context);
    }

    @Override
    public void onBindHolder(ViewHolder holder, String title) {
        holder.mBinding.title.setText(title);
    }

    @Override
    public SettingsSubMenuPresenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        ViewHolder holder = new ViewHolder(SettingsTitleBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));

        holder.mBinding.title.setTextColor(mLayoutConfig.getSettingsSubmenuColor());

        return holder;
    }

    public static class ViewHolder extends Presenter.ViewHolder {

        private final SettingsTitleBinding mBinding;

        public ViewHolder(SettingsTitleBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
