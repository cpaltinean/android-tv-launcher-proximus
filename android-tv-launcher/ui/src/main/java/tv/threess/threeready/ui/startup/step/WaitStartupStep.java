/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.startup.step;

import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.home.HomeRepository;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.CasUpdateReceiver;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Startup step to wait for background data update.
 *
 * @author Barabas Attila
 * @since 2018.06.29
 */
public class WaitStartupStep implements Step {
    private static final String TAG = Log.tag(WaitStartupStep.class);

    private final AppConfig mAppConfig = Components.get(AppConfig.class);
    private final HomeRepository mHomeRepository = Components.get(HomeRepository.class);
    private final CasUpdateReceiver mCasUpdateReceiver = Components.get(CasUpdateReceiver.class);
    private final TvRepository mTvRepository = Components.get(TvRepository.class);
    private final VodRepository mVodRepository = Components.get(VodRepository.class);
    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);

    private Disposable mDisposable;

    @Override
    public void execute(StepCallback callback) {
        mDisposable = Completable.concatArray(
                mHomeRepository.waitForConfig(),
                mHomeRepository.waitForTranslations(),
                mTvRepository.waitForChannelUpdate(),
                mTvRepository.waitForBroadcastUpdate(),
                mVodRepository.waitForVodSync(),
                mPvrRepository.waitRecordingSync())
                .timeout(mAppConfig.getStartupTimeout(TimeUnit.SECONDS), TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() ->{
                    if (mCasUpdateReceiver.isStateChanged()) {
                        Log.d(TAG, "Cas updated, the box will reboot soon");
                        return;
                    }
                    callback.onFinished();
                }, (e) -> {
                    if (mCasUpdateReceiver.isStateChanged()) {
                        Log.d(TAG, "Cas updated, the box will reboot soon");
                        return;
                    }
                    Log.e(TAG, "Couldn't wait the startup.", e);
                    callback.onFinished();
                });
    }

    @Override
    public void cancel() {
        RxUtils.disposeSilently(mDisposable);
    }
}
