package tv.threess.threeready.ui.settings.model;

import tv.threess.threeready.api.pc.model.ParentalRating;

/**
 * Entity class representing a parental control lock item.
 *
 * @author Daniela Toma
 * @since 2019.05.28
 */
public class ParentalControlLockItem {
    private final ParentalRating mParentalRating;

    public ParentalControlLockItem(ParentalRating parentalRating) {
        mParentalRating = parentalRating;
    }

    public ParentalRating getParentalRating() {
        return mParentalRating;
    }
}
