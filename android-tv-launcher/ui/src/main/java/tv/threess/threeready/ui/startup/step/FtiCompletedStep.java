package tv.threess.threeready.ui.startup.step;

import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.startup.StepCallback;

/**
 * Startup step to finish the first time install flow.
 *
 * @author Barabas Attila
 * @since 2019.04.12
 */
public class FtiCompletedStep extends StartupCompletedStep {


    @Override
    public void execute(StepCallback callback) {
        Settings.ftiFinished.asyncEdit().put(true);
        super.execute(callback);
    }
}
