/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.tv.presenter.vod;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;

import org.jetbrains.annotations.NotNull;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.VodA1PortraitCardBinding;
import tv.threess.threeready.ui.generic.view.VodOrderedIconsContainer;
import tv.threess.threeready.ui.tv.view.ContentMarkersView;
import tv.threess.threeready.ui.tv.view.ProgressIndicatorView;

/**
 * Presenter class for vod type and A1 variant card. (portrait view)
 * <p>
 * Created by Szilard on 8/17/2017.
 */

public class VodA1StripeCardPresenter extends VodPortraitCardPresenter<VodA1StripeCardPresenter.ViewHolder, IBaseVodItem> {

    private static final int MAX_RECYCLER_VIEW_COUNT = 20;
    private static int EXPAND_ANIMATION_DELAY;

    public VodA1StripeCardPresenter(Context context) {
        super(context);
        EXPAND_ANIMATION_DELAY = mContext.getResources().getInteger(R.integer.vod_a1_portrait_card_detail_animation_duration);
    }

    @Override
    public int getCardWidth(ModuleData<?> moduleData, IBaseVodItem vod) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.vod_a1_portrait_card_width);
    }

    @Override
    public int getCardHeight(ModuleData<?> moduleData, IBaseVodItem vod) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.vod_a1_portrait_card_height);
    }

    @Override
    protected int getCoverHeight(IBaseVodItem vod) {
        return getCardHeight(vod);
    }

    @Override
    protected int getCoverWidth(IBaseVodItem vod) {
        return getCardWidth(vod);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        VodA1PortraitCardBinding binding = VodA1PortraitCardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.detailContainer.setBackgroundColor(mLayoutConfig.getPlaceHolderColor());
        holder.mBinding.title.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBinding.cardDetail.setTextColor(mLayoutConfig.getPlaceholderTransparentFontColor());
        holder.mBinding.description.setTextColor(mLayoutConfig.getPlaceholderTransparentFontColor());
        holder.mPlaceHolderDrawable.setColor(mLayoutConfig.getPlaceHolderColor());

        return holder;
    }

    @Override
    public int getMaxRecyclerViewCount() {
        return MAX_RECYCLER_VIEW_COUNT;
    }

    @Override
    protected int getItemAlignmentOffset() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.base_vod_portrait_card_first_animation_item_alignment_offset);
    }

    @Override
    public void onUnbindHolder(ModuleData<?> moduleData, ViewHolder holder) {
        super.onUnbindHolder(moduleData, holder);
        cancelAnimationRunnable(holder);
    }

    @Override
    public void onFocusedState(ModuleData<?> moduleData, ViewHolder holder, IBaseVodItem vod) {
        super.onFocusedState(moduleData, holder, vod);
        expand(holder);
    }

    private void cancelAnimationRunnable(ViewHolder holder) {
        ExpandRunnable animationRunnable = holder.mExpandRunnable;
        if (animationRunnable != null) {
            holder.view.removeCallbacks(animationRunnable);
        }
    }

    private void expand(ViewHolder holder) {
        cancelAnimationRunnable(holder);
        ExpandRunnable animationRunnable = new ExpandRunnable(holder);
        holder.mExpandRunnable = animationRunnable;
        holder.view.postDelayed(animationRunnable, EXPAND_ANIMATION_DELAY);
    }

    @Override
    protected void loadCoverImage(ViewHolder holder, IBaseVodItem item, int coverWidth, int coverHeight, RequestBuilder<Drawable> requestBuilder) {
        requestBuilder
                .apply(new RequestOptions()
                        .placeholder(holder.getPlaceHolderDrawable())
                        .override(coverWidth, coverHeight)
                        .optionalCenterCrop())
                .transition(new DrawableTransitionOptions().dontTransition())
                .into(holder.getCoverView());
    }

    /**
     * Runnable to expand the portrait card after a delay.
     */
    private class ExpandRunnable implements Runnable {
        ViewHolder mHolder;

        private ExpandRunnable(ViewHolder holder) {
            mHolder = holder;
        }

        @Override
        public void run() {
            // Set description padding based on title line count..
            int textWidth = getMeasuredTextWidth(mHolder.getDescriptionView());
            final int viewWidth = mContext.getResources().getDimensionPixelOffset(R.dimen.base_vod_portrait_detail_width);
            if (textWidth > viewWidth) {
                mHolder.getDescriptionView().setPadding(0,
                        0,
                        mContext.getResources().getDimensionPixelOffset(R.dimen.base_vod_portrait_left_right_padding), 0);
            } else {
                mHolder.getDescriptionView().setPadding(0,
                        mContext.getResources().getDimensionPixelOffset(R.dimen.base_vod_portrait_detail_padding_top),
                        mContext.getResources().getDimensionPixelOffset(R.dimen.base_vod_portrait_left_right_padding), 0);
            }
        }
    }


    protected static class ViewHolder extends VodPortraitCardPresenter.ViewHolder {

        private ExpandRunnable mExpandRunnable;

        private final VodA1PortraitCardBinding mBinding;

        @Override
        @NotNull
        public TextView getDetailsView() {
            return mBinding.cardDetail;
        }

        public ViewHolder(VodA1PortraitCardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        @Override
        @NotNull
        public TextView getTitleView() {
            return mBinding.title;
        }

        @Override
        public TextView getInfoTextView() {
            return mBinding.cardDetail;
        }

        @Override
        public ContentMarkersView getContentMarkerView() {
            return mBinding.contentMarker;
        }

        @Override
        public ImageView getCoverView() {
            return mBinding.cover;
        }

        @Override
        public ProgressIndicatorView getProgressIndicatorView() {
            return mBinding.progressBar;
        }

        @Override
        public VodOrderedIconsContainer getIconContainer() {
            return mBinding.iconContainer;
        }

        @Override
        public TextView getDescriptionView() {
            return mBinding.description;
        }

        @Override
        public ColorDrawable getPlaceHolderDrawable() {
            return mPlaceHolderDrawable;
        }
    }
}
