package tv.threess.threeready.ui.epg.presenter;

import android.content.Context;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Card presenter for {@link tv.threess.threeready.api.tv.ChannelType#REGIONAL} channels
 *
 * @author hunormadaras
 * @since 2019-09-03
 */
public class RegionalProgramGuidePresenter extends SingleProgramGuidePresenter {

    public RegionalProgramGuidePresenter(Context context) {
        super(context);
    }

    @Override
    public void onClicked(ViewHolder holder, TvChannel channel) {
        super.onClicked(holder, channel);
        if (mNavigator.getPlayerFragment() != null) {
            mNavigator.getPlayerFragment().goToChannel(StartAction.Ui, channel.getId(), false);
        }
    }

    @Override
    void setTitle(ViewHolder holder, TvChannel channel) {
        holder.mBinding.title.setText(
                mTranslator.get(TranslationKey.REGIONAL_CHANNELS_PANCARTE_MORE_INFO_BUTTON));
    }
}
