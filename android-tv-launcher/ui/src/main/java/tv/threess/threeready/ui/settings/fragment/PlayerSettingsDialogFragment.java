/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.settings.fragment;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.leanback.widget.VerticalGridView;

import java.util.ArrayList;
import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.model.TrackInfo;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.model.MediaTrackInfo;
import tv.threess.threeready.ui.databinding.LanguageSubscreenLayoutBinding;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.settings.model.RadioButtonItem;
import tv.threess.threeready.ui.settings.model.MediaTrackRadioItem;

/**
 * This class will handle in app settings
 *
 * @author Paul
 * @since 2017.05.30
 */

public class PlayerSettingsDialogFragment extends BaseSettingsDialogFragment<Object> {
    public static final String TAG = Log.tag(PlayerSettingsDialogFragment.class);

    private static final String ARG_SUBTITLES = "ARG_SUBTITLES";
    private static final String ARG_SELECTED_SUBTITLE_ID = "ARG_SELECTED_SUBTITLE_ID";
    private static final String ARG_AUDIO_TRACKS = "ARG_AUDIO_TRACKS";
    private static final String ARG_SELECTED_AUDIO_TRACK_ID = "ARG_SELECTED_AUDIO_TRACK_ID";

    private final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    private LanguageSubscreenLayoutBinding mBinding;

    List<Object> mItems = new ArrayList<>();

    public static PlayerSettingsDialogFragment newInstance(ArrayList<MediaTrackInfo> subtitles, ArrayList<MediaTrackInfo> audioTracks,
                                                           String selectedSubtitleId, String selectedAudioTrackId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_SUBTITLES, subtitles);
        args.putSerializable(ARG_AUDIO_TRACKS, audioTracks);
        args.putString(ARG_SELECTED_SUBTITLE_ID, selectedSubtitleId);
        args.putString(ARG_SELECTED_AUDIO_TRACK_ID, selectedAudioTrackId);

        PlayerSettingsDialogFragment fragment = new PlayerSettingsDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    protected View inflateLayout() {
        if (mBinding == null) {
            mBinding = LanguageSubscreenLayoutBinding.inflate(LayoutInflater.from(getContext()));
        }

        return mBinding.getRoot();
    }

    @NonNull
    protected View getRightFrame() {
        return mBinding.rightFrame;
    }

    protected VerticalGridView getVerticalGridView() {
        return mBinding.settingsGrid;
    }

    protected TextView getTitleView() {
        return mBinding.title;
    }

    private void addOption(String key, ArrayList<MediaTrackInfo> tracks) {
        String title = mTranslator.get(key);
        mItems.add(title);
        // check if item should be checked
        String selectedSubtitle = getStringArgument(ARG_SELECTED_SUBTITLE_ID);
        String selectedAudioTrack = getStringArgument(ARG_SELECTED_AUDIO_TRACK_ID);

        for (MediaTrackInfo mediaTrackInfo : tracks) {
            String language = mediaTrackInfo.getLanguage().toLowerCase();
            language = StringUtils.getCapitalizedLanguageName(getAudioSubtitleLanguage(language, language));
            if (mediaTrackInfo.isDescriptive()) {
                switch (mediaTrackInfo.getType()) {
                    case Subtitle:
                        language += " - " + mTranslator.get(TranslationKey.SCREEN_HARD_OF_HEARING);
                        break;

                    case Audio:
                        language += " - " + mTranslator.get(TranslationKey.SCREEN_AUDIO_TYPE);
                        break;
                }
            }

            if (mediaTrackInfo.getSurroundType() == MediaTrackInfo.SurroundType.DolbyDigital) {
                language += " - " + mTranslator.get(TranslationKey.SCREEN_AUDIO_DOLBY_DIGITAL);
            } else if (mediaTrackInfo.getSurroundType() == MediaTrackInfo.SurroundType.DolbyDigitalPlus) {
                language += " - " + mTranslator.get(TranslationKey.SCREEN_AUDIO_DOLBY_DIGITAL_PLUS);
            }

            MediaTrackRadioItem item = new MediaTrackRadioItem(language, mediaTrackInfo, key);
            if (mediaTrackInfo.getId().equals(selectedAudioTrack)) {
                item.setChecked(true);
            }
            if (mediaTrackInfo.getId().equals(selectedSubtitle)) {
                item.setChecked(true);
            }
            mItems.add(item);
        }
    }

    /**
     * Gets the text that will be displayed for the specified language code.
     */
    private String getAudioSubtitleLanguage(String languageCode, String fallback) {
        if (languageCode == null) {
            return fallback;
        }

        languageCode = mAppConfig.getAudioSubtitleLanguageMap().get(languageCode);
        if (languageCode == null) {
            return fallback;
        }

        languageCode = mTranslator.get(languageCode.toUpperCase());
        if (languageCode == null) {
            return fallback;
        }

        return languageCode;
    }

    protected void initView() {
        ArrayList<MediaTrackInfo> audioTracks = getSerializableArgument(ARG_AUDIO_TRACKS);
        if (audioTracks != null && audioTracks.size() > 0) {
            addOption(TranslationKey.SCREEN_AUDIO_SUBTITLES_AUDIO_LANGUAGE, audioTracks);
        }

        ArrayList<MediaTrackInfo> subtitleTracks = getSerializableArgument(ARG_SUBTITLES);
        if (subtitleTracks != null && subtitleTracks.size() > 0) {
            addOption(TranslationKey.SCREEN_AUDIO_SUBTITLES_SUBTITLES, subtitleTracks);
            MediaTrackRadioItem offItem = new MediaTrackRadioItem(mTranslator.get(TranslationKey.SCREEN_SUBTITLE_OFF),
                    new TrackInfo(TranslationKey.SCREEN_SUBTITLE_OFF, false), TranslationKey.SCREEN_AUDIO_SUBTITLES_SUBTITLES);

            if (TranslationKey.SCREEN_SUBTITLE_OFF.equals(getStringArgument(ARG_SELECTED_SUBTITLE_ID))) {
                offItem.setChecked(true);
            }
            mItems.add(offItem);
        }

        setItems(mItems);
    }

    @Override
    protected void setTitle() {
        mBinding.title.setText(mTranslator.get(TranslationKey.SCREEN_AUDIO_SUBTITLES_TITLE));
        mBinding.description.setVisibility(View.VISIBLE);
        mBinding.description.setText(mTranslator.get(TranslationKey.SCREEN_AUDIO_SUBTITLES_SUBTITLE));
    }

    @Override
    protected boolean onOptionsSelected(RadioButtonItem item) {
        if (item.getPreferenceGroup().equals(TranslationKey.SCREEN_AUDIO_SUBTITLES_SUBTITLES)) {
            MediaTrackRadioItem trackInfo = (MediaTrackRadioItem) item;
            if (TranslationKey.SCREEN_SUBTITLE_OFF.equals(item.getValue())) {
                Log.d(TAG, "Subtitles are now removed!");
                mPlaybackDetailsManager.deselectSubtitle();
            } else {
                Log.d(TAG, "Setting language for subtitles: " + item.getValue());
                mPlaybackDetailsManager.selectSubtitle((MediaTrackInfo) trackInfo.getTrackInfo());
            }
        }
        if (item.getPreferenceGroup().equals(TranslationKey.SCREEN_AUDIO_SUBTITLES_AUDIO_LANGUAGE)) {
            MediaTrackRadioItem trackInfo = (MediaTrackRadioItem) item;
            Log.d(TAG, "Setting language for audio tracks: " + item.getValue());
            mPlaybackDetailsManager.selectAudio((MediaTrackInfo) trackInfo.getTrackInfo());
        }
        return super.onOptionsSelected(item);
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        super.onKeyUp(event);
        if (event.getKeyCode() == KeyEvent.KEYCODE_CAPTIONS) {
            dismiss();
            return true;
        }
        return false;
    }
}
