/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.menu.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;

/**
 * Custom view for the menu icons.
 * The selectors are created programmatically, the colors come from config.
 *
 * Created by Szilard on 4/18/2017.
 */

public class MenuIconView extends androidx.appcompat.widget.AppCompatImageView {

    protected ColorStateList mTint;
    protected ColorStateList mBackgroundTint;

    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    public MenuIconView(Context context) {
        this(context, null);
    }

    public MenuIconView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MenuIconView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    protected void initialize() {
        // The colors come from config, the selectors will be created programmatically
        int[][] backgroundStates = new int[][]{
                new int[]{android.R.attr.state_pressed},
                new int[]{android.R.attr.state_focused},
                new int[]{}
        };

        int[] backgroundColors = new int[]{
                mLayoutConfig.getButtonPressedColorEnd(),
                mLayoutConfig.getButtonFocusedColorStart(),
                Color.TRANSPARENT
        };
        mBackgroundTint = new ColorStateList(backgroundStates, backgroundColors);


        int[][] states = new int[][]{
                new int[]{android.R.attr.state_pressed},
                new int[]{}
        };

        int[] colors = new int[]{
                mLayoutConfig.getFontColor(),
                mLayoutConfig.getFontColor(),
        };
        mTint = new ColorStateList(states, colors);
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        // Update the colors, when the state is changed
        if (mBackgroundTint != null && mBackgroundTint.isStateful()) {
            updateBackgroundTintColor();
        }
        if (mTint != null && mTint.isStateful()) {
            updateTintColor();
        }
    }

    protected void updateBackgroundTintColor() {
        int color = mBackgroundTint.getColorForState(getDrawableState(), 0);

        GradientDrawable gd;
        if (color == mLayoutConfig.getButtonFocusedColorStart()) {
            gd = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,
                    new int[]{mLayoutConfig.getButtonFocusedColorStart(), mLayoutConfig.getButtonFocusedColorEnd()});

        } else if (color == mLayoutConfig.getButtonPressedColorStart()) {
            gd = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,
                    new int[]{mLayoutConfig.getButtonPressedColorStart(), mLayoutConfig.getButtonPressedColorEnd()});

        } else {
            gd = new GradientDrawable();
            gd.setColor(color);
        }

        gd.setShape(GradientDrawable.OVAL);
        setBackground(gd);
    }

    protected void updateTintColor() {
        int color = mTint.getColorForState(getDrawableState(), 0);
        setColorFilter(color);
    }
}
