package tv.threess.threeready.ui.epggrid;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.databinding.ClassicEpgBinding;
import tv.threess.threeready.ui.epggrid.listener.DelayedBroadcastSelectionListener;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.hint.HintManager;

/**
 * Fragment displaying the classical EPG grid and detail over view for the selected program.
 *
 * @author Barabas Attila
 * @since 12/11/21
 */
public class EpgGridFragment extends BaseFragment {

    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final PlaybackDetailsManager mPlaybackManager = Components.get(PlaybackDetailsManager.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final HintManager mHintManager = Components.get(HintManager.class);
    private IContentItem mReportableContentItem;
    private ClassicEpgBinding mBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mBinding != null) {
            // Already initialized.
            restoreFocus();
            return mBinding.getRoot();
        }

        mBinding = ClassicEpgBinding.inflate(inflater, container, false);
        
        mBinding.headerClock.setTextColor(mLayoutConfig.getFontColor());

        mBinding.epgGrid.setBroadcastSelectionListener(new DelayedBroadcastSelectionListener(
                broadcast -> {
                    mBinding.epgPreview.updateDetails(broadcast);
                    updateChannelLogo(broadcast.getChannelId());

                    if (broadcast.isLoaded()) {
                        if (mReportableContentItem == null) {
                            mReportableContentItem = broadcast;
                            reportNavigation();
                            return;
                        }
                        mReportableContentItem = broadcast;
                    }
                }));

        // Init EPG grid.
        mBinding.epgGrid.initiate(getDefaultChannel(), getDefaultTime());

        // Show Tv Guide hint.
        mHintManager.showEpgLayoutHints();

        return mBinding.getRoot();
    }

    private String getDefaultChannel() {
        String channelId = mPlaybackManager.getChannelId();
        if (TextUtils.isEmpty(channelId)) {
            return mNowOnTvCache.getLastPlayedChannelId();
        }

        return channelId;
    }

    private long getDefaultTime() {
        IBroadcast playingBroadcast = mPlaybackManager.getBroadcastPlayerData();
        if (playingBroadcast != null && !playingBroadcast.isLive()) {
            return playingBroadcast.getStart();
        }

        return System.currentTimeMillis();
    }

    /**
     * Called when the user enters a channel number.
     *
     * @param channelId  The id of the requested channel
     * @param isFavorite True if the channel was selected from the favorite list.
     */
    public void onChannelSwitch(String channelId, boolean isFavorite) {
        Log.d(TAG, "Select channel : " + channelId);
        mBinding.epgGrid.selectChannel(channelId);
    }

    /**
     * Focus the currently playing program in the EPG,
     * or live if there is no such program.
     */
    public void selectNowPlaying() {
        mReportableContentItem = null;
        mBinding.epgGrid.select(getDefaultChannel(), getDefaultTime());
    }

    /**
     * Updates the logo of the channel.
     * @param channelId The id of the channel.
     */
    private void updateChannelLogo(String channelId) {
        Context context = getContext();
        if (context == null) {
            return;
        }

        Glide.with(context.getApplicationContext()).clear(mBinding.headerLogo);
        TvChannel channel = mNowOnTvCache.getChannel(channelId);
        if (channel != null) {
            Glide.with(context.getApplicationContext())
                    .load(channel)
                    .into(mBinding.headerLogo);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBinding.epgGrid.destroy();
        mBinding.epgPreview.destroy();
        Context context = getContext();
        if (context != null) {
            Glide.with(context.getApplicationContext()).clear(mBinding.headerLogo);
        }
    }

    @Nullable
    @Override
    protected Event<?, ?> generatePageOpenEvent() {
        if (mReportableContentItem == null) {
            return null;
        }

        return ReportUtils.createEvent(UILog.Page.EpgGridPage, mReportableContentItem);
    }
}
