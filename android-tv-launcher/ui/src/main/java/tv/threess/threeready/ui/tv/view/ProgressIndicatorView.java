/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.tv.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.model.LiveBookmark;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.utils.WeakHandler;

/**
 * View component which renders a progress bar on a card view for a currently running program/vod/recording etc.
 * The progress is automatically updated based on the current time while the progress bar is displayed.
 *
 * @author Barabas Attila
 * @since 2017.04.27
 */
public class ProgressIndicatorView extends View {
    public static final String TAG = Log.tag(ProgressIndicatorView.class);

    private static final long LIVE_PROGRESS_UPDATE_RATE = TimeUnit.MINUTES.toMillis(1);

    WeakHandler mHandler = new WeakHandler();

    protected ProgressIndicatorListener mOnProgressCompleteListener;

    protected IBookmark mBookmark;

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    Paint mProgressPaint;
    Paint mBackgroundPaint;

    int mRoundRadius;
    boolean mRounded;

    public ProgressIndicatorView(Context context) {
        this(context, null);
    }

    public ProgressIndicatorView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProgressIndicatorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        // Read rounded attribute
        if (attrs != null) {
            TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ProgressIndicatorView, defStyle, 0);
            try {
                mRounded = typedArray.getBoolean(R.styleable.ProgressIndicatorView_rounded, false);
            } finally {
                typedArray.recycle();
            }
        }

        mRoundRadius = context.getResources().getDimensionPixelOffset(R.dimen.rounded_progress_bar_radius);

        // Create background paint.
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setColor(context.getResources().getColor(R.color.progress_indicator_background, null));

        // Create progress paint.
        mProgressPaint = new Paint();
    }

    /**
     * Sets the progress for this view based on a bookmark
     *
     * @param bookmark - the bookmark containing data for the progress view
     */
    public void setProgressData(IBookmark bookmark) {
        stopAutoUpdate();
        mBookmark = bookmark;
        if (bookmark != null) {
            if (bookmark instanceof LiveBookmark) {
                mHandler.post(mUpdateRunnable);
            }
        }
        invalidate();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopAutoUpdate();
    }

    /**
     * Stop the automatic progress update.
     */
    protected void stopAutoUpdate() {
        mHandler.removeCallbacks(mUpdateRunnable);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (changed) {
            mProgressPaint.setShader(new LinearGradient(0, 0, getWidth(), getHeight(),
                    mLayoutConfig.getPrimaryColorStart(), mLayoutConfig.getPrimaryColorEnd(), Shader.TileMode.MIRROR));
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (mBookmark != null) {
            //the asset's position has reached the end
            if (mBookmark.getPosition() > mBookmark.getDuration()) {
                onProgressComplete();
            }
            //the position is between the start and end of the asset -> draw the progress
            else if (mBookmark.getPosition() > 0 && mBookmark.getDuration() > 0) {
                int width = (int) ((mBookmark.getPosition() * getWidth()) / mBookmark.getDuration());
                drawCanvas(canvas, width);
            }
        }
    }

    private void drawCanvas(Canvas canvas, int width) {
        if (mRounded) {
            // Rounded progress indicator.
            canvas.drawRoundRect(1, 0, getWidth(), getHeight(),
                    mRoundRadius, mRoundRadius, mBackgroundPaint);
            canvas.drawRoundRect(0, 0, width, getHeight(),
                    mRoundRadius, mRoundRadius, mProgressPaint);

        } else {
            // Rectangular progress indicator.
            canvas.drawRect(0, 0, getWidth(), getHeight(), mBackgroundPaint);
            canvas.drawRect(0, 0, width, getHeight(), mProgressPaint);
        }
    }

    public void setProgressListener(ProgressIndicatorListener listener) {
        mOnProgressCompleteListener = listener;
    }

    private void onProgressComplete() {
        stopAutoUpdate();
        if (mOnProgressCompleteListener != null) {
            mOnProgressCompleteListener.onProgressCompleted();
        }
    }

    /**
     * Invalidate and reschedule the progress indicator update
     */
    private final Runnable mUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            invalidate();
            if (mBookmark != null) {
                long updateRate = Math.min(mBookmark.getDuration() - mBookmark.getPosition(), LIVE_PROGRESS_UPDATE_RATE);

                if (updateRate > 0) {
                    mHandler.postDelayed(mUpdateRunnable, updateRate);
                }
            }
        }
    };

    public interface ProgressIndicatorListener {
        void onProgressCompleted();
    }

}
