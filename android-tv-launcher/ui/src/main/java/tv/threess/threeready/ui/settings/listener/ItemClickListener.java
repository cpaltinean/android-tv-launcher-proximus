/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.settings.listener;

import tv.threess.threeready.ui.settings.model.ISettingsItem;

/**
 * This class will be used to notify if a radio button or toggle state is changed
 *
 * @author Paul
 * @since 2017.05.30
 */

public interface ItemClickListener {
    void onItemClick(ISettingsItem position);
}
