package tv.threess.threeready.ui.generic.adapter;

import android.view.KeyEvent;

import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.ui.generic.presenter.BaseModularCardPresenter;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;

/**
 * Custom item bridge adapter which passes the module data for the modular card presenters.
 * @see BaseModularCardPresenter
 *
 * @author Barabas Attila
 * @since 2018.01.11
 */

public class ModularItemBridgeAdapter extends ItemBridgeAdapter {
    private ModuleData<?> mModuleData;

    public ModularItemBridgeAdapter(ArrayObjectAdapter<?> adapter, Presenter presenter, boolean circular) {
        super(adapter, presenter, circular);
    }

    public ModularItemBridgeAdapter(ModuleData<?> moduleData, ArrayObjectAdapter<?> adapter, Presenter presenter) {
        super(adapter, presenter);
        mModuleData = moduleData;
    }

    public ModularItemBridgeAdapter(ModuleData<?> moduleData, ArrayObjectAdapter<?> adapter, PresenterSelector presenter) {
        super(adapter, presenter);
        mModuleData = moduleData;
    }

    public ModularItemBridgeAdapter(ModuleData<?> moduleData,
                                    ArrayObjectAdapter<?> adapter, Presenter presenter,
                                    RecyclerView.RecycledViewPool recycledViewPool) {
        super(adapter, presenter, recycledViewPool);
        mModuleData = moduleData;
    }

    public ModularItemBridgeAdapter(ModuleData<?> moduleData,
                                    ArrayObjectAdapter<?> adapter, PresenterSelector presenterSelector,
                                    RecyclerView.RecycledViewPool recycledViewPool) {
        super(adapter, presenterSelector, recycledViewPool);
        mModuleData = moduleData;
    }

    public ModuleData<?> getModuleData() {
        return mModuleData;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onBindViewHolder(BasePresenter<Presenter.ViewHolder, Object> presenter,
                                    Presenter.ViewHolder holder, Object item, List<?> payloads) {
        if (presenter instanceof BaseModularCardPresenter) {
            BaseModularCardPresenter.ViewHolder modularHolder = (BaseModularCardPresenter.ViewHolder) holder;
            BaseModularCardPresenter modularPresenter = (BaseModularCardPresenter) presenter;
            modularPresenter.onBindHolder(mModuleData, modularHolder, item, payloads);
        } else {
            super.onBindViewHolder(presenter, holder, item, payloads);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onBindViewHolder(BasePresenter presenter, Presenter.ViewHolder holder, Object item) {
        if (presenter instanceof BaseModularCardPresenter) {
            BaseModularCardPresenter.ViewHolder modularHolder = (BaseModularCardPresenter.ViewHolder) holder;
            BaseModularCardPresenter modularPresenter = (BaseModularCardPresenter) presenter;
            modularPresenter.onBindHolder(mModuleData, modularHolder, item);
        } else {
            super.onBindViewHolder(presenter, holder, item);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onDefaultViewHolder(BasePresenter presenter, Presenter.ViewHolder holder, Object item) {
        if (presenter instanceof BaseModularCardPresenter) {
            BaseModularCardPresenter.ViewHolder modularHolder = (BaseModularCardPresenter.ViewHolder) holder;
            BaseModularCardPresenter modularPresenter = (BaseModularCardPresenter) presenter;
            modularPresenter.onDefaultState(mModuleData, modularHolder, item);
        } else {
            super.onDefaultViewHolder(presenter, holder, item);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onFocusedViewHolder(BasePresenter presenter, Presenter.ViewHolder holder, Object item) {
        if (presenter instanceof BaseModularCardPresenter) {
            BaseModularCardPresenter.ViewHolder modularHolder = (BaseModularCardPresenter.ViewHolder) holder;
            BaseModularCardPresenter modularPresenter = (BaseModularCardPresenter) presenter;
            modularPresenter.onFocusedState(mModuleData, modularHolder, item);
        } else {
            super.onFocusedViewHolder(presenter, holder, item);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onUnbindHolder(BasePresenter presenter, Presenter.ViewHolder holder, Object item) {
        if (presenter instanceof BaseModularCardPresenter) {
            BaseModularCardPresenter.ViewHolder modularHolder = (BaseModularCardPresenter.ViewHolder) holder;
            BaseModularCardPresenter modularPresenter = (BaseModularCardPresenter) presenter;
            modularPresenter.onUnbindHolder(mModuleData, modularHolder);
        } else {
            super.onUnbindHolder(presenter, holder, item);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    protected boolean onViewHolderKeyEvent(BasePresenter presenter, Presenter.ViewHolder holder, Object item, KeyEvent event) {
        if (presenter instanceof BaseModularCardPresenter) {
            BaseModularCardPresenter.ViewHolder modularHolder = (BaseModularCardPresenter.ViewHolder) holder;
            BaseModularCardPresenter modularPresenter = (BaseModularCardPresenter) presenter;
            return modularPresenter.onKeyEvent(mModuleData, modularHolder, item, event);
        }

        return super.onViewHolderKeyEvent(presenter, holder, item, event);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onViewHolderClicked(BasePresenter presenter, Presenter.ViewHolder holder, Object item) {
        if (presenter instanceof BaseModularCardPresenter) {
            BaseModularCardPresenter.ViewHolder modularHolder = (BaseModularCardPresenter.ViewHolder) holder;
            BaseModularCardPresenter modularPresenter = (BaseModularCardPresenter) presenter;
            modularPresenter.onClicked(mModuleData, modularHolder, item);
        } else {
            super.onViewHolderClicked(presenter, holder, item);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    protected boolean onViewHolderLongClicked(BasePresenter presenter, Presenter.ViewHolder holder, Object item) {
        if (presenter instanceof BaseModularCardPresenter) {
            BaseModularCardPresenter.ViewHolder modularHolder = (BaseModularCardPresenter.ViewHolder) holder;
            BaseModularCardPresenter modularPresenter = (BaseModularCardPresenter) presenter;
            modularPresenter.onLongClicked(mModuleData, modularHolder, item);
        }
        return super.onViewHolderLongClicked(presenter, holder, item);
    }
}
