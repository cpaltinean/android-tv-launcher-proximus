package tv.threess.threeready.ui.epggrid.presenter;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.TimelineViewBinding;
import tv.threess.threeready.ui.epggrid.view.EpgTimeLineView;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;

/**
 * {javadoc}
 *
 * @author Barabas Attila
 * @since 18/09/2021
 */
public class EpgGridTimelinePresenter extends BasePresenter<EpgGridTimelinePresenter.ViewHolder, Long> {

    private static final long BLOCK_SIZE = TimeUnit.MINUTES.toMillis(10);

    private static final String SPACE = " ";
    private final  ImageSpan mMinuteMarkerSpan = new ImageSpan(mContext, R.drawable.classic_epg_dot);

    private final EpgTimeLineView mTimeline;
    private final SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    public EpgGridTimelinePresenter(Context context, EpgTimeLineView timeline) {
        super(context);
        mTimeline = timeline;
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        TimelineViewBinding binding = TimelineViewBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.timelineText.setTextColor(mLayoutConfig.getFontColor());
        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, Long timeValue) {
        ViewGroup.LayoutParams params = holder.mBinding.timelineText.getLayoutParams();

        long blockEnd = timeValue + BLOCK_SIZE;

        // Calculate size for view.
        long windowStart = mTimeline.getWindowStart();
        params.width = mTimeline.convertMillisToPixel(blockEnd - Math.max(timeValue, windowStart));
        holder.mBinding.timelineText.setTranslationX(-mTimeline.convertMillisToPixel(BLOCK_SIZE / 2));
        holder.mBinding.timelineText.requestLayout();

        // Outside the window, nothing to display
        if (timeValue < windowStart) {
            holder.mBinding.timelineText.setText(null);

        // Every 30 minutes display time
        } else if (timeValue % TimeUnit.MINUTES.toMillis(30) == 0) {
            holder.mBinding.timelineText.setText(mSimpleDateFormat.format(timeValue));

        // Every 10 minute display a dot.
        } else {
            SpannableString dot = new SpannableString(SPACE);
            dot.setSpan(mMinuteMarkerSpan, 0, 1, SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.mBinding.timelineText.setText(dot);
        }
    }

    @Override
    public long getStableId(Long timeValue) {
        return timeValue;
    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private final TimelineViewBinding mBinding;

        public ViewHolder(TimelineViewBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
