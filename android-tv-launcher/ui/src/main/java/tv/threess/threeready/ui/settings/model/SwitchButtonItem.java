package tv.threess.threeready.ui.settings.model;

/**
 * Model class for switch/toggle button
 *
 * @author Denisa Trif
 * @since 29.05.2019
 */
public class SwitchButtonItem extends RadioButtonItem {

    private final String mSubtitle;

    public SwitchButtonItem(String title, String subtitle, String value, String preferenceGroup) {
        super(title, value, preferenceGroup);
        mSubtitle = subtitle;
    }

    public String getSubtitle() {
        return mSubtitle;
    }
}
