package tv.threess.threeready.ui.epg.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.RelativeLayout;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.epg.presenter.AppProgramGuidePresenter;
import tv.threess.threeready.ui.epg.presenter.RegionalProgramGuidePresenter;
import tv.threess.threeready.ui.epg.presenter.TvProgramGuideCardPresenter;
import tv.threess.threeready.ui.epg.presenter.TvProgramGuideDatePresenter;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.WeakHandler;

/**
 * Program guide view which displays a program on the selected channel.
 *
 * @author Barabas Attila
 * @since 2017.11.16
 */

public abstract class ProgramGuideView extends RelativeLayout {
    private static final String TAG = Log.tag(ProgramGuideView.class);

    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);
    protected final Translator mTranslator = Components.get(Translator.class);
    protected final TvRepository mTvRepository = Components.get(TvRepository.class);

    protected WeakHandler mHandler = new WeakHandler();

    ArrayObjectAdapter<Object> mAdapter;
    PresenterSelector mPresenterSelector;
    PresenterSelector mSingeProgramPresenterSelector;
    TvProgramGuideCardPresenter mTvProgramGuidePresenter;
    RegionalProgramGuidePresenter mRegionalProgramGuidePresenter;
    AppProgramGuidePresenter mAppProgramGuidePresenter;
    TvProgramGuideDatePresenter mTvProgramGuideDatePresenter;

    protected TvChannel mSelectedChannel;
    protected long mFrom = System.currentTimeMillis();
    protected long mTo = System.currentTimeMillis();

    private Disposable mCurrentProgramDisposable;
    protected KeyEvent mKeyEvent;

    public ProgramGuideView(Context context) {
        this(context, null);
    }

    public ProgramGuideView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProgramGuideView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ProgramGuideView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        inflateLayout();

        ProgramGuideGridView gridView = getProgramGuideGridView();
        gridView.setHasFixedSize(true);
        gridView.setItemAnimator(null);

        mTvProgramGuidePresenter = new TvProgramGuideCardPresenter(getContext());
        mTvProgramGuideDatePresenter = new TvProgramGuideDatePresenter(getContext());
        mRegionalProgramGuidePresenter = new RegionalProgramGuidePresenter(getContext());
        mAppProgramGuidePresenter = new AppProgramGuidePresenter(getContext());

        mSingeProgramPresenterSelector = new PresenterSelector() {

            @Override
            public Presenter getPresenter(Object item) {
                if (((TvChannel) item).getType() == ChannelType.REGIONAL) {
                    return mRegionalProgramGuidePresenter;
                }
                return mAppProgramGuidePresenter;
            }

            @Override
            public Presenter[] getPresenters() {
                return new Presenter[]{mRegionalProgramGuidePresenter,
                        mAppProgramGuidePresenter};
            }
        };

        // Set presenter selector.
        mPresenterSelector = new InterfacePresenterSelector()
                .addClassPresenterSelector(TvChannel.class, mSingeProgramPresenterSelector)
                .addClassPresenter(IBroadcast.class, mTvProgramGuidePresenter)
                .addClassPresenter(Long.class, mTvProgramGuideDatePresenter);

        // Set empty adapter
        mAdapter = new ArrayObjectAdapter<>();
        mAdapter.setHasStableIds(true);
        mAdapter.setPresenterSelector(mPresenterSelector);

        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(mAdapter);
        itemBridgeAdapter.setHasStableIds(true);

        getProgramGuideGridView().setAdapter(itemBridgeAdapter);
    }

    @NonNull
    protected abstract ProgramGuideGridView getProgramGuideGridView();

    protected abstract void inflateLayout();

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mInternetChecker.addStateChangedListener(mOnInternetChangedListener);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mInternetChecker.removeStateChangedListener(mOnInternetChangedListener);
    }

    @CallSuper
    public void onDestroy() {
        getProgramGuideGridView().setAdapter(null);
        RxUtils.disposeSilently(mCurrentProgramDisposable);
    }

    public void onSelectedChannelChanged(@NonNull TvChannel channel, boolean isFromFavoriteSection) {
        mSelectedChannel = channel;
        mTvProgramGuidePresenter.setFromFavoriteSection(isFromFavoriteSection);

        loadPrograms(channel, System.currentTimeMillis(), System.currentTimeMillis());
    }

    /**
     * Load and display the current live program on the given channel.
     */
    protected void loadPrograms(final TvChannel channel, long from, long to) {
        mFrom = from;
        mTo = to;

        // Dispose current program request
        RxUtils.disposeSilently(mCurrentProgramDisposable);

        // Load app channel.
        if (channel.getType() == ChannelType.APP || channel.getType() == ChannelType.REGIONAL) {
            Single.create((SingleOnSubscribe<String>) e ->
                    e.onSuccess("Success"))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<String>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            mCurrentProgramDisposable = d;
                        }

                        @Override
                        public void onSuccess(String o) {
                            Log.d(TAG, "App channel loaded.");
                            if (mAdapter.size() != 0) {
                                mAdapter.clear();
                            }

                            // Update the current program
                            mAdapter.add(channel);
                            getProgramGuideGridView().setSelectedPosition(0);
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, "Could not load app channel.", e);
                        }
                    });
            return;
        }

        Observable.create(mTvRepository.getEpgBroadcastsBetweenTimestamps(channel, from, to,
                mTranslator.get(TranslationKey.MODULE_CARD_LOADING),
                mTranslator.get(TranslationKey.EPG_NO_INFO)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<IBroadcast>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mCurrentProgramDisposable = d;
                    }

                    @Override
                    public void onNext(List<IBroadcast> programs) {
                        Log.d(TAG, "Broadcasts between timestamp loaded: " + programs.size());
                        IBroadcast broadcast = programs.isEmpty() ? null : programs.get(0);

                        if (mAdapter.size() != 0) {
                            mAdapter.clear();
                        }

                        // Update the current program
                        mAdapter.add(broadcast);

                        // Dispatch key event after loading completed.
                        if (mKeyEvent != null) {
                            dispatchKeyEvent(mKeyEvent);
                            mKeyEvent = null;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "Could not load program list.", e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    protected void onInternetStateChanged(boolean available) {
        if (mSelectedChannel != null && available) {
            loadPrograms(mSelectedChannel,
                    System.currentTimeMillis(), System.currentTimeMillis());
        }
    }

    /**
     * Reload programs after the internet is back.
     */
    private final InternetChecker.OnStateChangedListener mOnInternetChangedListener = (state) -> {
        if (!state.isInternetStateChanged()) {
            return;
        }
        onInternetStateChanged(mInternetChecker.isInternetAvailable());
    };

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP
                && event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER
                && mSelectedChannel != null
                && mSelectedChannel.getType() == ChannelType.TV
                && mAdapter.size() > 0
                && mAdapter.get(0) instanceof IBroadcast
                && !mSelectedChannel.getId().equalsIgnoreCase(((IBroadcast) mAdapter.get(0)).getChannelId())) {
            // Save the key event, because the broadcasts are not updated yet.
            mKeyEvent = event;
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    public IContentItem getLastSelectedProgram() {
        return null;
    }
}
