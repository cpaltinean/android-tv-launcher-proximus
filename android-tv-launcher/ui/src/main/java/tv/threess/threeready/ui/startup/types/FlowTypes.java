package tv.threess.threeready.ui.startup.types;

/**
 * used at @{@link tv.threess.threeready.ui.startup.StartupFlow} to handle different cases of
 * application start
 *
 * @author hunormadaras
 * @since 08/02/2019
 */
public enum FlowTypes {

    START_UP,
    FTI,
    OFFLINE,
    STAND_BY

}
