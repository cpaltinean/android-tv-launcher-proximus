package tv.threess.threeready.ui.generic.player.plugin;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.WorkerThread;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.command.AbsoluteJumpCommand;
import tv.threess.threeready.player.command.RelativeJumpCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.start.TrickPlayStartCommand;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.plugin.PlaybackPlugin;
import tv.threess.threeready.ui.generic.navigation.Navigator;

/**
 * Playback manager plugin which displayed an upselling message
 * when the a Replay user tries to Fast Forward or Skip.
 *
 * @author Barabas Attila
 * @since 2018.10.02
 */
public class ReplayPlusOfferPlugin implements PlaybackPlugin {
    private static final String TAG = Log.tag(ReplayPlusOfferPlugin.class);

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final PlaybackDetailsManager mPlaybackManager = Components.get(PlaybackDetailsManager.class);
    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);

    private final Handler mHandler = new Handler(Looper.getMainLooper());

    @Override
    public void onCreate() {
        Log.d(TAG, "Create replay plus offer plugin.");
    }

    @Override
    public boolean isSubscribed(PlaybackDomain domain) {
        return domain == PlaybackDomain.Replay || domain == PlaybackDomain.TrickPlay;
    }

    @Override
    public void onBeforeExecute(PlaybackCommand command, PlaybackEntireState state, PlaybackDetailsBuilder details) throws PlaybackError {
        if (command instanceof TrickPlayStartCommand) {
            TrickPlayStartCommand cmd = (TrickPlayStartCommand) command;
            // Block forward trickplay - playbackSpeed > 0
            if (cmd.getPlaybackSpeed() > 0 && cmd.getActiveDomain() == PlaybackDomain.Replay) {
                showReplayPlusSubscriptionDialog();
            }
        }

        else if (command instanceof RelativeJumpCommand) {
            RelativeJumpCommand cmd = (RelativeJumpCommand) command;
            // Block forward jumping
            if (cmd.getDuration() > 0) {
                showReplayPlusSubscriptionDialog();
            }
        }

        else if (command instanceof AbsoluteJumpCommand) {
            AbsoluteJumpCommand cmd = (AbsoluteJumpCommand) command;
            // Block forward seeking
            if (cmd.getPosition() > mPlaybackManager.getPosition(System.currentTimeMillis())) {
                showReplayPlusSubscriptionDialog();
            }
        }
    }

    @WorkerThread
    private void showReplayPlusSubscriptionDialog() throws PlaybackError {
        if (!mAccountRepository.hasReplayPlusSubscription()) {
            showDialog();
        }
    }

    private void showDialog() throws PlaybackError {
        mHandler.removeCallbacksAndMessages(null);
        mHandler.post(mNavigator::showReplayPlusOfferDialog);

        throw new PlaybackError(PlaybackError.Type.INVALID,
                "Seek and jump commands are only available with replay plus"
        );
    }
}
