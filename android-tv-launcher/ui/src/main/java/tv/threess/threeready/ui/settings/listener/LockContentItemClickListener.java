package tv.threess.threeready.ui.settings.listener;

import tv.threess.threeready.ui.settings.model.LockContentItem;

/**
 * This class will be used to notify if a Parental Age rating was selected.
 *
 * @author Daniela Toma
 * @since 2019.06.28
 */
public interface LockContentItemClickListener extends ItemClickListener {
    default void checkLockContentRadioButton(LockContentItem selectedItem){
    }
}
