package tv.threess.threeready.ui.generic.player.plugin;

import android.os.Bundle;

import io.reactivex.disposables.Disposable;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.BingeWatchingInfo;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.command.StopCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.plugin.PlaybackPlugin;
import tv.threess.threeready.player.result.EventResult;
import tv.threess.threeready.player.result.Result;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Base plugin for binge watching functionality.
 *
 * @author Zsolt Bokor_
 * @since 2020.04.15
 */
public abstract class BaseBingeWatchingPlugin implements PlaybackPlugin {
    private static final String TAG = Log.tag(BaseBingeWatchingPlugin.class);

    protected final Navigator mNavigator = Components.get(Navigator.class);
    protected final PlaybackDetailsManager mPlaybackManager = Components.get(PlaybackDetailsManager.class);

    private Disposable mDisposable;

    @Override
    public void onDestroy() {
        RxUtils.disposeSilently(mDisposable);
    }

    @Override
    public void onResult(PlaybackCommand command, Result result) {
        // do not trigger on trick-play related events
        if (command.getDomain() == PlaybackDomain.TrickPlay || command.getDomain() == PlaybackDomain.None) {
            return;
        }

        if (command instanceof StartCommand) {
            // Reset the next episode
            mPlaybackManager.setNextEpisode(null);
            RxUtils.disposeSilently(mDisposable);

            if (result.isFavorable() && command.getType() == getPlaybackType()) {
                mDisposable = preloadNextEpisode();
            }

        } else if (command instanceof StopCommand) {
            mPlaybackManager.setNextEpisode(null);
            RxUtils.disposeSilently(mDisposable);
        }
    }

    @Override
    public void onEvent(EventResult result, Bundle details) {
        if (result.event == PlaybackEvent.BingeWatchingReached &&
                mPlaybackManager.inPlaybackType(getPlaybackType())) {
            onBingeWatchingReached();
        }
    }

    /**
     * Method used to return the playback type which activates the plugin.
     */
    protected abstract PlaybackType getPlaybackType();

    /**
     * Method used to preload the next episode.
     */
    protected abstract Disposable preloadNextEpisode();

    /**
     * Method used to display the binge watching dialog with the next program.
     */
    private void onBingeWatchingReached() {
        // If the next program is loaded we should display the binge watching dialog
        BingeWatchingInfo bingeWatchingInfo = mPlaybackManager.getNextEpisode();
        if (bingeWatchingInfo != null && bingeWatchingInfo.getSeries() != null &&
                bingeWatchingInfo.getNextEpisode() != null) {
            // If the stream is visible the content overlay will be hidden
            // and "binge watching" dialog will be cancelled
            if (mNavigator.isStreamDisplayedOnTop()) {
                mNavigator.hideContentOverlay();
                mNavigator.showBingeWatching(bingeWatchingInfo);
            }
        } else {
            Log.d(TAG, "The next episode for binge watching is not loaded!");
        }
    }
}
