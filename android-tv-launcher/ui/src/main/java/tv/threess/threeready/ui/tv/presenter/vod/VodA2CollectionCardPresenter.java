/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.tv.presenter.vod;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import io.reactivex.Observable;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.VodA2CollectionCardBinding;
import tv.threess.threeready.ui.generic.presenter.SimpleVodCardPresenter;
import tv.threess.threeready.ui.tv.view.ContentMarkersView;
import tv.threess.threeready.ui.tv.view.ProgressIndicatorView;

/**
 * Presenter class for vod type and A2 variant card. (portrait view for collections only)
 *
 * @author Solyom Zsolt
 * @since 2020.1.28
 */

public class VodA2CollectionCardPresenter extends SimpleVodCardPresenter<SimpleVodCardPresenter.ViewHolder, IBaseVodItem> {

    private static final int MAX_RECYCLER_VIEW_COUNT = 20;

    public VodA2CollectionCardPresenter(Context context) {
        super(context);
    }

    @Override
    public int getCardWidth(ModuleData<?> moduleData, IBaseVodItem vod) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.vod_a2_portrait_card_width);
    }

    @Override
    public int getCardHeight(ModuleData<?> moduleData, IBaseVodItem vod) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.vod_a2_portrait_card_height);
    }

    @Override
    protected int getCoverWidth(IBaseVodItem vod) {
        return getCardWidth(vod);
    }

    @Override
    protected int getCoverHeight(IBaseVodItem vod) {
        return getCardHeight(vod);
    }

    @Override
    protected int getItemAlignmentOffset() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.base_vod_portrait_card_first_animation_item_alignment_offset);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        VodA2CollectionCardBinding binding = VodA2CollectionCardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mPlaceHolderDrawable.setColor(mLayoutConfig.getPlaceHolderColor());
        return holder;
    }

    @Override
    public Observable<IBookmark> getBookmarkObservable(SimpleVodCardPresenter.ViewHolder holder, IBaseVodItem vod) {
        return mVodRepository.getBookmarkForVod(vod);

    }

    @Override
    public int getMaxRecyclerViewCount() {
        return MAX_RECYCLER_VIEW_COUNT;
    }

    protected static class ViewHolder extends SimpleVodCardPresenter.ViewHolder {

        private final VodA2CollectionCardBinding mBinding;

        ColorDrawable mPlaceHolderDrawable = new ColorDrawable();

        public ViewHolder(VodA2CollectionCardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        @Override
        public TextView getTitleView() {
            return null;
        }

        @Override
        public ContentMarkersView getContentMarkerView() {
            return mBinding.contentMarker;
        }

        @Override
        public ImageView getCoverView() {
            return mBinding.cover;
        }

        @Override
        public ProgressIndicatorView getProgressIndicatorView() {
            return mBinding.progressBar;
        }

        @Override
        public ColorDrawable getPlaceHolderDrawable() {
            return mPlaceHolderDrawable;
        }
    }
}
