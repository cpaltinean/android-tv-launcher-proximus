package tv.threess.threeready.ui.settings.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.databinding.SettingsMainItemBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.settings.model.SupportItem;
import tv.threess.threeready.ui.utils.SystemUtils;

/**
 * Presenter class for the support settings items.
 *
 * @author Karetka Mezei Zoltan
 * @since 2020.10.04
 */
public class SupportItemPresenter extends BasePresenter<SupportItemPresenter.ViewHolder, SupportItem> {
    public static final String TAG = Log.tag(SupportItemPresenter.class);

    protected final Navigator mNavigator = Components.get(Navigator.class);

    public SupportItemPresenter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(
                SettingsMainItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onClicked(ViewHolder holder, SupportItem item) {
        super.onClicked(holder, item);
        switch (item.getType()) {
            case SYSTEM_INFORMATION:
                mNavigator.openSystemInformationSettings();
                break;

            case CHECK_FOR_UPDATES:
                mNavigator.openSystemUpdateFragment(true);
                break;

            case CHANNEL_SCAN:
                mNavigator.showChannelScanDialog(true);
                break;

            case CLEAR_DATA:
                Components.get(AccountRepository.class).clearData()
                        .andThen(Completable.fromAction(() -> SystemUtils.restartApplication(mNavigator.getActivity())))
                        .subscribeOn(Schedulers.io())
                        .subscribe();
                break;

            case APP_RESTART:
                SystemUtils.restartApplication(mNavigator.getActivity());
                break;

            default:
                Log.d(TAG, "Invalid item type: " + item.getType());
        }
    }

    @Override
    public void onBindHolder(ViewHolder holder, SupportItem item) {
        holder.mBinding.title.setText(item.getTitle());
        holder.mBinding.image.setBackground(item.getIcon());
    }

    public static class ViewHolder extends BasePresenter.ViewHolder {

        private final SettingsMainItemBinding mBinding;

        public ViewHolder(SettingsMainItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
