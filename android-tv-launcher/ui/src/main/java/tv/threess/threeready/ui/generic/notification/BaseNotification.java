/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.notification;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.helper.ErrorType;
import tv.threess.threeready.api.log.model.Error;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.utils.AnimatorUtils;
import tv.threess.threeready.ui.generic.utils.Translator;

import java.util.Queue;

/**
 * Base class for notifications.
 *
 * @author Zsolt Bokor
 * @since 2020.03.16
 */
public abstract class BaseNotification extends BaseFragment {
    private static final String TAG = Log.tag(BaseNotification.class);

    protected static final int NOTIFICATION_FADE_OUT_DURATION = 500;
    protected static final int DEFAULT_PRIORITY = 100;

    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    protected final Translator mTranslator = Components.get(Translator.class);

    private final AnimatorSet mAnimatorSet = new AnimatorSet();
    protected Timeout mTimeOut = Timeout.LENGTH_MEDIUM;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //  Cancel fade animation.
        AnimatorUtils.cancelAnimators(mAnimatorSet);
    }

    @Override
    public boolean onError(Error error) {
        if (ErrorType.PROVISIONING.getCode().equalsIgnoreCase(error.getErrorCode())) {
            return false;
        }
        return super.onError(error);
    }

    /**
     * Returns true if the notification and the currently displayed notification are the same
     */
    public abstract boolean isDisplayed(BaseNotification current);

    /**
     * Returns true if the notification needs to be shown if is already queued to be displayed
     */
    public boolean showIfEnqueued(Queue<BaseNotification> queued) {
        return true;
    }

    /**
     * @return True if the notification should be shown immediately
     * even if there are other notifications in the queue.
     */
    public boolean skippDisplayQueue() {
        return false;
    }

    public abstract long getPriority();

    public Timeout getTimeOut() {
        return mTimeOut;
    }

    public void setTimeOut(Timeout timeOut) {
        mTimeOut = timeOut;
    }

    public void hideNotificationAnimation() {
        View view = getView();
        if (view == null) {
            return;
        }

        // Cancel previous
        AnimatorUtils.cancelAnimators(mAnimatorSet);

        ObjectAnimator fadeAnimation = ObjectAnimator.ofFloat(getView(), View.ALPHA, 1, 0);
        ObjectAnimator slideOutAnimation = ObjectAnimator.ofFloat(getView(), View.TRANSLATION_Y, 0, getResources().getDimensionPixelOffset(R.dimen.notification_slide_out_dist));
        mAnimatorSet.playTogether(fadeAnimation, slideOutAnimation);

        mAnimatorSet.addListener(new android.animation.AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                try {
                    FragmentManager fm = getChildFragmentManager();
                    Fragment notificationContainer = fm.findFragmentById(R.id.notification_container);
                    if (notificationContainer == null) {
                        return;
                    }

                    FragmentTransaction transaction = fm.beginTransaction();
                    transaction.remove(notificationContainer);
                    transaction.commitAllowingStateLoss();
                } catch (Exception e) {
                    Log.w(TAG, "Could not hide notification.", e);
                }
            }

        });
        mAnimatorSet.setDuration(NOTIFICATION_FADE_OUT_DURATION);
        mAnimatorSet.start();
    }

    /**
     * Timeout for notifications.
     * The timeouts are different based on the notification priority.
     */
    public enum Timeout {
        LENGTH_SHORT,
        LENGTH_MINIMUM,
        LENGTH_MEDIUM,
        LENGTH_LONG,
        LENGTH_PERMANENT,
        LENGTH_RENT_NOTIFICATION,
        LENGTH_FORCED_REBOOT_NOTIFICATION
    }
}
