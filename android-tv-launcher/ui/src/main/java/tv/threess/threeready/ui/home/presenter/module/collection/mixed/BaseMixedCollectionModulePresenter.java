/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.home.presenter.module.collection.mixed;

import android.content.Context;

import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.home.presenter.module.collection.generic.CollectionModulePresenter;
import tv.threess.threeready.ui.home.view.ModularPageView;

/**
 * Presenter used to display mixed type collections.
 * <p>
 * Created by Szilard on 6/21/2017.
 */
public abstract class BaseMixedCollectionModulePresenter extends CollectionModulePresenter {

    BaseMixedCollectionModulePresenter(Context context, ModularPageView.PageTitleCallback pageTitle, DataSourceLoader dataSourceLoader) {
        super(context, pageTitle, dataSourceLoader);
    }

    /**
     * Gets the value for horizontal padding.
     */
    @Override
    protected int getHorizontalPadding() {
        return 0;
    }

    /**
     * Gets the value for top padding.
     */
    @Override
    protected int getTopPadding(ViewHolder holder) {
        return 0;
    }

    /**
     * Gets the value for bottom padding.
     */
    @Override
    protected int getBottomPadding() {
        return 0;
    }
}
