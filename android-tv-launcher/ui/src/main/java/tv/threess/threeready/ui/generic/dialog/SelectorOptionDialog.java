package tv.threess.threeready.ui.generic.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.ui.databinding.SelectorOptionDialogBinding;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Dialog to change to sort and filter options for a collection.
 *
 * Created by Noemi Dali on 05.08.2020.
 */
public class SelectorOptionDialog extends BaseDialogFragment {

    private static final String EXTRA_SELECTOR_OPTIONS = "EXTRA_SELECTOR_OPTIONS";
    private static final String EXTRA_CLICK_LISTENER = "EXTRA_CLICK_LISTENER";

    private DataSourceSelector mSelector;
    private OnGridItemClickListener mItemClickListener;

    private final Translator mTranslator = Components.get(Translator.class);

    protected SelectorOptionDialogBinding mBinding;

    public static SelectorOptionDialog newInstance(DataSourceSelector sorting, OnGridItemClickListener onGridItemClickListener) {
        SelectorOptionDialog sortOptionChangeDialog = new SelectorOptionDialog();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_SELECTOR_OPTIONS, sorting);
        args.putSerializable(EXTRA_CLICK_LISTENER, onGridItemClickListener);
        sortOptionChangeDialog.setArguments(args);
        return sortOptionChangeDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSelector = getSerializableArgument(EXTRA_SELECTOR_OPTIONS);
        mItemClickListener = getSerializableArgument(EXTRA_CLICK_LISTENER);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mBinding = SelectorOptionDialogBinding.inflate(inflater, container, false);

        mBinding.title.setText(mTranslator.get(mSelector.getName()));
        mBinding.optionsGrid.setupGridItems(mSelector, (OnGridItemClickListener) clickedOption -> {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClicked(clickedOption);
            }
            dismiss();
        });

        return mBinding.getRoot();
    }

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        return new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                .addDetail(UILogEvent.Detail.Page, UILog.Page.SortingOptionsPage)
                .addDetail(UILogEvent.Detail.PageName, mSelector.getSelectedOption() != null ?
                        mSelector.getSelectedOption().getReportName() : null);
    }

    /**
     * Listener to get notified when a sort options is selected.
     */
    public interface OnGridItemClickListener extends Serializable {
        void onItemClicked(SelectorOption clickedOption);
    }
}
