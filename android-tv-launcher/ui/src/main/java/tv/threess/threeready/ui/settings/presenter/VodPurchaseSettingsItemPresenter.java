package tv.threess.threeready.ui.settings.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.leanback.widget.Presenter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.ui.databinding.VodPurchaseSettingsItemBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.settings.model.VodPurchaseHistoryItem;

/**
 * Presenter class for vod purchase history items.
 *
 * @author Daniela Toma
 * @since 2022.02.07
 */
public class VodPurchaseSettingsItemPresenter extends BasePresenter<VodPurchaseSettingsItemPresenter.ViewHolder, VodPurchaseHistoryItem> {

    public static final String TAG = VodPurchaseSettingsItemPresenter.class.getCanonicalName();

    protected final Navigator mNavigator = Components.get(Navigator.class);

    public VodPurchaseSettingsItemPresenter(Context context) {
        super(context);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new VodPurchaseSettingsItemPresenter.ViewHolder(
                VodPurchaseSettingsItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindHolder(ViewHolder holder, VodPurchaseHistoryItem vodPurchaseHistoryItem) {
        holder.mBinding.title.setText(vodPurchaseHistoryItem.getTitle());
        holder.mBinding.description.setText(vodPurchaseHistoryItem.getDescription());
    }

    public static class ViewHolder extends BasePresenter.ViewHolder {

        private final VodPurchaseSettingsItemBinding mBinding;

        public ViewHolder(VodPurchaseSettingsItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
