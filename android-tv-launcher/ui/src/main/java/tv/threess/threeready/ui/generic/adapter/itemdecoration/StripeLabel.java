package tv.threess.threeready.ui.generic.adapter.itemdecoration;

import android.graphics.drawable.Drawable;

/**
 * Holds the label title and icon for the area.
 *
 * @author Barabas Attila
 * @since 2018.08.02
 */
public class StripeLabel extends StripeSeparator {
    private String mTitle;
    private Drawable mLeftDrawable;
    private boolean mShouldTruncate;

    public StripeLabel(int startPosition, int endPosition, String title, boolean shouldTruncate) {
        super(startPosition, endPosition, null);
        mTitle = title;
        mShouldTruncate = shouldTruncate;
    }

    public StripeLabel(int startPosition, int endPosition, String title, Drawable separatorDrawable) {
        super(startPosition, endPosition, separatorDrawable);
        mTitle = title;
    }

    public StripeLabel(int startPosition, int endPosition, String title,
                       Drawable separatorDrawable, Drawable leftDrawable) {
        super(startPosition, endPosition, separatorDrawable);
        mTitle = title;
        mLeftDrawable = leftDrawable;
    }

    public String getTitle() {
        return mTitle;
    }

    public Drawable getLeftDrawable() {
        return mLeftDrawable;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public boolean shouldTruncate() {
        return mShouldTruncate;
    }

    public boolean isPositionInSection(int position) {
        return position >= getStartPosition() && position < getEndPosition();
    }
}
