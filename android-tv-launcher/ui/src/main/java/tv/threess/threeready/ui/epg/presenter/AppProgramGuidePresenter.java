package tv.threess.threeready.ui.epg.presenter;

import android.content.Context;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.netflix.model.SourceType;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Card presenter for {@link tv.threess.threeready.api.tv.ChannelType#APP} channels
 *
 * @author hunormadaras
 * @since 2019-09-03
 */
public class AppProgramGuidePresenter extends SingleProgramGuidePresenter {
    private static final String APP_NAME_PLACEHOLDER = "%app_name%";

    public AppProgramGuidePresenter(Context context) {
        super(context);
    }

    @Override
    public void onClicked(ViewHolder holder, TvChannel channel) {
        super.onClicked(holder, channel);
        mNavigator.checkSubscriptionAndOpenApp(StartAction.Ui, channel, channel.isFavorite() ?
                SourceType.FavoriteChannel : SourceType.Channel);
    }

    @Override
    void setTitle(ViewHolder holder, TvChannel channel) {
        holder.mBinding.title.setText(
                mTranslator
                        .get(TranslationKey.SCREEN_EPG_APP_TITLE)
                        .replace(APP_NAME_PLACEHOLDER, channel.getName()));
    }
}
