package tv.threess.threeready.ui.utils.glide;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader;

import java.io.InputStream;
import java.util.List;

import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.generic.model.IImageSource;

/**
 * Default glide model loader. Will return the first available image url.
 *
 * Created by Noemi Dali on 06.04.2020.
 */
public class DefaultGlideModelLoader extends BaseGlideUrlLoader<IBaseContentItem> {

    public DefaultGlideModelLoader(ModelLoader<GlideUrl, InputStream> concreteLoader) {
        super(concreteLoader);
    }

    @Override
    protected String getUrl(IBaseContentItem iBaseContentItem, int width, int height, Options options) {
        List<IImageSource> images = iBaseContentItem.getImageSources();
        return images == null || images.isEmpty() ? "" : images.get(0).getUrl();

    }

    @Override
    public boolean handles(@NonNull IBaseContentItem iBaseContentItem) {
        return true;
    }
}
