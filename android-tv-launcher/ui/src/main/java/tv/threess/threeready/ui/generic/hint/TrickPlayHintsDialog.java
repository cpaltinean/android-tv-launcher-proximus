package tv.threess.threeready.ui.generic.hint;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.TrickplayHintsDialogBinding;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Dialog for trickplay hints.
 * Sequence of 2 hint screens.
 *
 * @author Zsolt Bokor_
 * @since 2020.06.11
 */
public class TrickPlayHintsDialog extends HintsDialog {
    
    private TrickplayHintsDialogBinding mBinding;

    private enum Type {
        SEEKING_HINTS,
        TRICKPLAY_HINTS
    }

    public static TrickPlayHintsDialog newInstance(String hintId, boolean hintOpenedFromSettings) {
        Bundle args = new Bundle();
        args.putString(EXTRA_HINT_ID, hintId);
        args.putBoolean(EXTRA_HINT_OPENED_FROM_SETTINGS, hintOpenedFromSettings);
        TrickPlayHintsDialog fragment = new TrickPlayHintsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected View inflateView() {
        if (mBinding == null) {
            mBinding = TrickplayHintsDialogBinding.inflate(LayoutInflater.from(getContext()));
        }

        return mBinding.getRoot();
    }

    @NonNull
    @Override
    protected ImageView getBackgroundImage() {
        return mBinding.background;
    }

    @NonNull
    @Override
    protected TextView getNextButton() {
        return mBinding.buttonContainer.nextBtn;
    }

    @NonNull
    @Override
    protected TextView getPreviousButton() {
        return mBinding.buttonContainer.previousBtn;
    }

    /**
     * Display the current hint.
     */
    @Override
    protected void displayHint() {
        Type currentType = Type.values()[mImagePosition];
        if (currentType == Type.SEEKING_HINTS) {
            mBinding.gradient.setVisibility(View.GONE);
            mBinding.progressBar.setVisibility(View.GONE);
            mBinding.leftTime.setVisibility(View.GONE);
            mBinding.remoteControl.setImageDrawable(getDrawable(R.drawable.remote_control_left_right_buttons));

            setPointerBottomMargin(getMargin(R.dimen.trickplay_hints_dialog_pointer_bottom_margin_seeking_screen));
            setDescriptionTopMargin(getMargin(R.dimen.trickplay_hints_dialog_description_top_margin_seeking_screen));
            mBinding.description.setText(mTranslator.get(TranslationKey.HINT_TEXT_TRICKPLAY_JUMP));

            mBinding.descriptionSmall.setVisibility(View.GONE);
            mBinding.thumbnail.setVisibility(View.GONE);
            mBinding.icons.setVisibility(View.GONE);
            mBinding.buttonContainer.previousBtn.setVisibility(View.GONE);
            mBinding.buttonContainer.nextBtn.setText(mTranslator.get(TranslationKey.HINTS_BUTTON_NEXT));
            mBinding.buttonContainer.nextBtn.setContentDescription(
                    new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_HINTS_BUTTON_NEXT))
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY, mTranslator.get(TranslationKey.HINT_TEXT_TRICKPLAY_JUMP))
                            .toString());
            mBinding.background.setImageDrawable(null);
            if (mHintOpenedFromSettings) {
                mBinding.background.setBackgroundColor(ContextCompat.getColor(mBinding.getRoot().getContext(), R.color.alert_dialog_bg));
            }
        } else {
            mBinding.gradient.setVisibility(View.VISIBLE);
            mBinding.progressBar.setVisibility(View.VISIBLE);
            mBinding.leftTime.setVisibility(View.VISIBLE);
            mBinding.remoteControl.setImageDrawable(getDrawable(R.drawable.remote_control_rw_ff_buttons));

            setPointerBottomMargin(getMargin(R.dimen.trickplay_hints_dialog_pointer_bottom_margin_trickplay_screen));
            setDescriptionTopMargin(getMargin(R.dimen.trickplay_hints_dialog_description_top_margin_trickplay_screen));
            mBinding.description.setText(mTranslator.get(TranslationKey.HINT_TEXT_TRICKPLAY_SEEKING));

            mBinding.descriptionSmall.setVisibility(View.VISIBLE);
            mBinding.descriptionSmall.setText(mTranslator.get(TranslationKey.HINT_REVIEW_ALL_YOUR_TIPS));

            mBinding.thumbnail.setVisibility(View.VISIBLE);
            mBinding.icons.setVisibility(View.VISIBLE);
            mBinding.buttonContainer.previousBtn.setVisibility(View.VISIBLE);
            mBinding.buttonContainer.previousBtn.setContentDescription(mTranslator.get(TranslationKey.TALKBACK_HINTS_BUTTON_PREVIOUS));
            mBinding.buttonContainer.nextBtn.setText(mTranslator.get(TranslationKey.HINTS_BUTTON_DONE));
            mBinding.buttonContainer.nextBtn.setContentDescription(
                    new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_HINTS_BUTTON_DONE))
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY,
                                    mTranslator.get(TranslationKey.HINT_TEXT_TRICKPLAY_SEEKING) + " " + mTranslator.get(TranslationKey.HINT_REVIEW_ALL_YOUR_TIPS))
                            .toString());
            mBinding.background.setImageDrawable(getDrawable(R.drawable.trickplay_hints_background));
        }
    }

    @Override
    protected boolean isLastImage() {
        return mImagePosition + 1 == Type.values().length;
    }

    /**
     * Sets the bottom margin for the pointer.
     */
    private void setPointerBottomMargin(int bottomMargin) {
        RelativeLayout.LayoutParams pointerLayoutParams =
                (RelativeLayout.LayoutParams) mBinding.dialogPointer.getLayoutParams();
        pointerLayoutParams.bottomMargin = bottomMargin;
        mBinding.dialogPointer.setLayoutParams(pointerLayoutParams);
    }

    /**
     * Sets the top margin for description.
     */
    private void setDescriptionTopMargin(int topMargin) {
        RelativeLayout.LayoutParams descriptionLayoutParams =
                (RelativeLayout.LayoutParams) mBinding.description.getLayoutParams();
        descriptionLayoutParams.topMargin = topMargin;
        mBinding.description.setLayoutParams(descriptionLayoutParams);
    }

    @Override
    protected int getScreenNumbers() {
        return Type.values().length;
    }
}
