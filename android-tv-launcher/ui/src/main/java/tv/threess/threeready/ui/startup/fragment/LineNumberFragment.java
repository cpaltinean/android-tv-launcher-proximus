package tv.threess.threeready.ui.startup.fragment;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.generic.exception.DeviceLimitReachedException;
import tv.threess.threeready.api.generic.exception.InvalidAccountException;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.helper.ErrorType;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.GenericEvent;
import tv.threess.threeready.api.log.model.GenericKeyEvent;
import tv.threess.threeready.ui.databinding.LineNumberFragmentBinding;
import tv.threess.threeready.ui.generic.dialog.AlertDialog;
import tv.threess.threeready.ui.generic.dialog.BasePinDialog;
import tv.threess.threeready.ui.generic.dialog.PinDialog;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.startup.StartupFlow;
import tv.threess.threeready.ui.utils.SystemUtils;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Startup step fragment for setting the user's line number.
 *
 * @author Eugen Guzyk
 * @since 2018.06.27
 */
public class LineNumberFragment extends BaseFragment {

    private final Translator mTranslator = Components.get(Translator.class);
    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final StartupFlow mStartupFlow = Components.get(StartupFlow.class);

    private AlertDialog mErrorDialog;
    private LineNumberFragmentBinding mBinding;

    public static LineNumberFragment newInstance() {
        return new LineNumberFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mBinding = LineNumberFragmentBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        updateLayout();

        mBinding.numberInput.requestFocus();
        SystemUtils.showSoftInput(getActivity());
    }

    @Override
    public boolean onBackPressed() {
        //Do nothing on back press
        return true;
    }


    public void updateLayout() {
        mBinding.ftiBootCommonLayout.title.setText(mTranslator.get(TranslationKey.FTI_LINE_NUMBER_TITLE));
        mBinding.ftiBootCommonLayout.title.setTextColor(mLayoutConfig.getFontColor());

        mBinding.ftiBootCommonLayout.body.setText(mTranslator.get(TranslationKey.FTI_LINE_NUMBER_BODY));
        mBinding.ftiBootCommonLayout.body.setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f));

        mBinding.numberInput.getBackground().setColorFilter(mLayoutConfig.getInputCursorColor(), PorterDuff.Mode.SRC_IN);
        UiUtils.setCursorColor(mBinding.numberInput, mLayoutConfig.getInputCursorColor());

        mBinding.numberInput.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                String lineNumber = mBinding.numberInput.getText().toString();
                if (lineNumber.length() >= 9) {
                    SystemUtils.hideSoftInput(getActivity());
                    requestPin(lineNumber);
                    return true;
                } else {
                    showError(new InvalidAccountException());
                }
            }
            return true;
        });
    }

    private void requestPin(String lineNumber) {
        PinDialog pinDialog = new PinDialog.Builder()
                .setTitle(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_TITLE))
                .setReenterTitle(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_RE_ENTER_TITLE))
                .setBlockedTitle(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_BLOCKED_TITLE))
                .setBody(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_BODY))
                .setReenterBody(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_RE_ENTER_BODY))
                .setBlockedBody(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_BLOCKED_BODY))
                .setReenterHint(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_RE_ENTER_HINT))
                .setBlockedHint(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_BLOCKED_HINT))
                .setPinAttemptsSetting(Settings.systemPinAttemptsNumber)
                .setPinFailedTimestampSetting(Settings.systemPinFailedTimestamp)
                .setPinValidator(pin -> mAccountRepository.registerDevice(lineNumber, pin)
                        .doOnError(throwable -> {
                            if (throwable instanceof Exception) {
                                Log.e(TAG, "Device registration failed.", throwable);
                                mNavigator.getActivity().runOnUiThread(() -> showError((Exception) throwable));
                            }
                        })
                )
                .setOnLoadingCallback(new BasePinDialog.OnLoadingCallback() {
                    @Override
                    public void startLoading() {
                        mNavigator.showLoading();
                    }

                    @Override
                    public void stopLoading() {
                        mNavigator.hideLoading();
                    }
                })
                .setOnPinSuccessCallback((String pin) -> {
                    // Close the page.
                    mNavigator.clearAllDialogs();
                    mNavigator.goBack();

                    // Restart the startup flow.
                    if (mStartupFlow != null) {
                        mStartupFlow.startStartupFlow();
                    }
                })
                .build();
        mNavigator.showDialog(pinDialog);
    }

    /**
     * Show device registration error message for the user.
     *
     * @param error The exception which caused the error.
     */
    private void showError(Exception error) {
        SystemUtils.hideSoftInput(getActivity());

        AlertDialog.Builder builder = new AlertDialog.Builder()
                .cancelable(false)
                .dismissOnBtnClick(true);

        // Invalid account number error.
        if (error instanceof InvalidAccountException) {
            InvalidAccountException invalidAccountError = (InvalidAccountException) error;
            builder.title(mTranslator.get(TranslationKey.FTI_LINE_NUMBER_ERROR_TITLE))
                    .description(mTranslator.get(TranslationKey.FTI_LINE_NUMBER_ERROR_BODY))
                    .addButton(mTranslator.get(
                            TranslationKey.FTI_LINE_NUMBER_ERROR_BUTTON_TRY_AGAIN), dialog -> hideErrorDialog())
                    .errorCode(invalidAccountError.getErrorCode());

            // Device limit reached error.
        } else if (error instanceof DeviceLimitReachedException) {
            DeviceLimitReachedException deviceLimitReachedError = (DeviceLimitReachedException) error;
            builder.title(mTranslator.get(TranslationKey.MAX_NUMBER_DECODERS_ALERT_HEADER))
                    .description(mTranslator.get(TranslationKey.MAX_NUMBER_DECODERS_ALERT_BODY))
                    .addButton(mTranslator.get(TranslationKey.OK_BUTTON), dialog -> hideErrorDialog())
                    .errorCode(deviceLimitReachedError.getErrorCode());

            // Generic backend error.
        } else if (error instanceof BackendException) {
            BackendException backendException = (BackendException) error;
            builder.title(mTranslator.get(TranslationKey.GENERIC_ERROR_HANDLING_TITLE))
                    .description(mTranslator.get(TranslationKey.GENERIC_ERROR_HANDLING_BODY))
                    .addButton(mTranslator.get(TranslationKey.TRY_AGAIN_BUTTON), dialog -> hideErrorDialog())
                    .errorCode(backendException.getErrorCode());

            // Generic error.
        } else {
            builder.title(mTranslator.get(TranslationKey.GENERIC_ERROR_HANDLING_TITLE))
                    .description(mTranslator.get(TranslationKey.GENERIC_ERROR_HANDLING_BODY))
                    .addButton(mTranslator.get(TranslationKey.OK_BUTTON), dialog -> hideErrorDialog());
        }

        mErrorDialog = builder.build();

        mNavigator.showDialog(mErrorDialog);

        Log.event(new Event<>(GenericEvent.ErrorEvent)
                .addDetail(GenericKeyEvent.MESSAGE, builder.getDescription())
                .addDetail(GenericKeyEvent.DOMAIN, ErrorType.Domain.ENTITLEMENT)
                .addDetail(GenericKeyEvent.CODE, builder.getErrorCode())
                .addDetail(GenericKeyEvent.KEY, builder.getTitle()));
    }

    private void hideErrorDialog() {
        if (mErrorDialog != null) {
            mErrorDialog.dismiss();
        }

        mBinding.numberInput.requestFocus();
        SystemUtils.showSoftInput(getActivity());
    }
}
