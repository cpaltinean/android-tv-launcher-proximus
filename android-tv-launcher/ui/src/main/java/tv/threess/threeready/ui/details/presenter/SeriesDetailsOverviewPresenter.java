package tv.threess.threeready.ui.details.presenter;


import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import androidx.leanback.widget.HorizontalGridView;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.model.ISeries;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.presenter.ActionButtonsPresenter;
import tv.threess.threeready.ui.generic.presenter.ActionModel;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.presenter.LoadingButtonPresenter;
import tv.threess.threeready.ui.generic.presenter.LoadingModel;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.FontTextView;

/**
 * Generic detail overview presenter for a series.
 *
 * @author Barabas Attila
 * @since 2018.11.23
 */
public abstract class SeriesDetailsOverviewPresenter<THolder extends SeriesDetailsOverviewPresenter.ViewHolder, TItem extends IContentItem, TSeries extends ISeries<TItem>>
        extends BaseDetailsOverviewPresenter<THolder, TItem> {

    protected TSeries mSeries;

    protected PresenterSelector mPresenterSelector;
    private final List<Object> mActions;

    public SeriesDetailsOverviewPresenter(Context context, TSeries series) {
        super(context);
        mSeries = series;

        //Set presenter selector.
        mPresenterSelector = new InterfacePresenterSelector()
            .addClassPresenter(ActionModel.class, new ActionButtonsPresenter(mContext))
            .addClassPresenter(LoadingModel.class, new LoadingButtonPresenter(mContext));
        mActions = new ArrayList<>();
    }

    public void updateSeries(TSeries series) {
        mSeries = series;
    }

    protected abstract String getGenres(TItem episode);

    @Override
    public void onBindHolder(THolder holder, TItem item, List<?> payLoads) {
        super.onBindHolder(holder, item, payLoads);
        updateDescription(holder, item);
        updateInfoRow(holder, item);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        SeriesDetailsOverviewPresenter.ViewHolder holder = (ViewHolder) super.onCreateViewHolder(parent);
        holder.mAdapter = new ArrayObjectAdapter<>();
        holder.mItemBridgeAdapter = new ItemBridgeAdapter(holder.mAdapter, mPresenterSelector);
        holder.getActionGridView().setAdapter(holder.mItemBridgeAdapter);

        return holder;
    }

    @Override
    protected int getDescriptionLayout() {
        return R.layout.series_details_overlay_description;
    }

    @Override
    protected float getDescriptionWidth() {
        return mContext.getResources().getDimensionPixelSize(R.dimen.series_details_container_width);
    }

    @Override
    protected int getMarginStartForShowMore(View view) {
        return view.getResources().getDimensionPixelOffset(R.dimen.series_details_more_info_margin_start);
    }

    @Override
    protected void updateTitle(THolder holder, TItem item) {
        holder.getTitleView().setText(mSeries.getTitle());
    }

    @Override
    protected void updateInfoRow(THolder holder, TItem item) {
        List<String> infoParts = new ArrayList<>();

        String releaseYear = item.getReleaseYear();
        if (!TextUtils.isEmpty(releaseYear)) {
            infoParts.add(releaseYear);
        }

        String genres = getGenres(item);
        if (!TextUtils.isEmpty(genres)) {
            infoParts.add(genres);
        }

        int numberOfSeasons = mSeries.getNumberOfSeasons();
        if (numberOfSeasons > 0) {
            if (numberOfSeasons > 1) {
                infoParts.add(numberOfSeasons
                        + " " + mTranslator.get(TranslationKey.SCREEN_DETAIL_PROGRAM_SEASONS));
            } else {
                infoParts.add(numberOfSeasons
                        + " " + mTranslator.get(TranslationKey.SCREEN_DETAIL_PROGRAM_SEASON));
            }
        }

        FontTextView infoRow = holder.getInfoRow();
        infoRow.setText(TextUtils.join(TimeUtils.DIVIDER, infoParts));
        infoRow.setVisibility(View.VISIBLE);
    }

    /**
     * in case of series, the description contains the actors and directors list too
     *
     * @param holder view
     * @param item   data
     */
    @Override
    protected void updateDescription(THolder holder, TItem item) {

        String description = item.getDescription();
        String actors = TextUtils.join(", ", item.getActors());
        String directors = TextUtils.join(", ", item.getDirectors());

        StringBuilder descriptionContent = new StringBuilder();

        if (!TextUtils.isEmpty(description)) {
            descriptionContent.append(description);
        }

        StringBuilder secondaryDesc = new StringBuilder();

        addExtraContentToDescription(secondaryDesc, actors, TranslationKey.SCREEN_DETAIL_CAST_CREW);
        addExtraContentToDescription(secondaryDesc, directors, TranslationKey.SCREEN_DETAIL_CAST_CREW_ONE);

        if (TextUtils.isEmpty(descriptionContent)) {
            holder.getDescriptionView().setVisibility(View.GONE);
            return;
        }

        showDescription(holder, item, descriptionContent.toString(), secondaryDesc.toString());

    }

    protected void showLoadingButton(THolder holder) {
        //Set adapter.
        mActions.add(new LoadingModel(mTranslator.get(TranslationKey.MODULE_ACTION_BUTTON_LOADING)));
        holder.mAdapter.replaceAll(mActions);
    }

    /**
     * in case of series this appears is the description, so they are gone
     *
     * @param holder view
     * @param item   data
     */
    @Override
    protected void updateCast(THolder holder, TItem item) {
        holder.getCastView().setVisibility(View.GONE);
    }

    /**
     * in case of series this appears is the description, so they are gone
     *
     * @param holder view
     * @param item   data
     */
    @Override
    protected void updateDirectors(THolder holder, TItem item) {
        holder.getDirectorsView().setVisibility(View.GONE);
    }

    private void addExtraContentToDescription(StringBuilder extraContent, String data, String translationTitleKey) {
        if (!TextUtils.isEmpty(data)) {
            extraContent.append(TextUtils.isEmpty(extraContent.toString()) ? "" : "\n");
            extraContent.append(mTranslator.get(translationTitleKey)).append(": ").append(data);
        }
    }

    public abstract static class ViewHolder extends BaseDetailsOverviewPresenter.ViewHolder {
        ArrayObjectAdapter<Object> mAdapter;
        ItemBridgeAdapter mItemBridgeAdapter;

        public ViewHolder(View view) {
            super(view);
        }

        @NotNull
        public abstract HorizontalGridView getActionGridView();
    }

}
