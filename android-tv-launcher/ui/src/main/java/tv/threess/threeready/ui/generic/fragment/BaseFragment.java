/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.fragment;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.helper.ErrorType;
import tv.threess.threeready.api.log.model.Error;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.ui.generic.interfaces.IReportablePage;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.UserInactivityDetector;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Created by Barabas Attila on 4/11/2017.
 */
public class BaseFragment extends Fragment implements IReportablePage, IBaseFragmentUtils {
    public final String TAG = Log.tag(getClass());

    protected final UserInactivityDetector mUserInactivityDetector = new UserInactivityDetector(this::onUserInactivity);
    protected Application mApp;

    private ViewTreeObserver mViewTreeObserver;
    private View mFocusedView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activity activity = getActivity();
        if (activity == null) {
            return;
        }

        mApp = activity.getApplication();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewTreeObserver = view.getViewTreeObserver();
        if (mViewTreeObserver != null) {
            mViewTreeObserver.addOnGlobalFocusChangeListener(mOnGlobalFocusChangeListener);
        }
        handleBackgroundChange(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        mUserInactivityDetector.start();
        Components.get(Navigator.class).reportNavigationIfNeeded(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mUserInactivityDetector.stop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mViewTreeObserver != null) {
            mViewTreeObserver
                    .removeOnGlobalFocusChangeListener(mOnGlobalFocusChangeListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUserInactivityDetector.stop();
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        return false;
    }

    public boolean onKeyDown(KeyEvent event) {
        return false;
    }

    public boolean onKeyUp(KeyEvent event) {
        return false;
    }

    public boolean onKeyLongPress(KeyEvent event) {
        return false;
    }

    public void onUserInteraction() {
        mUserInactivityDetector.reset();
    }

    protected void onUserInactivity() {
    }

    protected void reportPageSelectionByTimeout() {
        Log.event(new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageSelection)
                .addDetail(UILogEvent.Detail.OpenedBy, UILog.OpenedBy.Timeout));
    }

    /**
     * @return The navigation event when the page opened.
     * Or null if such event cannot be generated.
     */
    @Nullable
    protected Event<?, ?> generatePageOpenEvent() {
        return null;
    }

    /**
     * Called when the back button pressed on the fragment.
     *
     * @return True if the back button press is handled by the fragment.
     */
    public boolean onBackPressed() {
        Log.event(new Event<>(UILogEvent.PageSelection)
                .addDetail(UILogEvent.Detail.OpenedBy, UILog.OpenedBy.Back));
        return false;
    }

    /**
     * @param error The error that must be handled
     * @return true if the error was handled, false otherwise
     */
    public boolean onError(Error error) {
        return ErrorType.PROVISIONING.getCode().equalsIgnoreCase(error.getErrorCode());
    }

    /**
     * Called when the container which holds the content fragment becomes visible.
     *
     * @see Navigator#showContentOverlay()
     */
    public void onContentShown() {
        reportNavigation();
    }

    /**
     * Called when the container which holds the content fragment becomes transparent.
     *
     * @see Navigator#hideContentOverlay()
     */
    public void onContentHide() {
    }

    /**
     * Override this method for pages which may have hints
     */
    public String getPageId() {
        return "";
    }

    /**
     * Global focus listener to persist the previously focused view when the user leaves the page.
     */
    private final ViewTreeObserver.OnGlobalFocusChangeListener mOnGlobalFocusChangeListener = (oldFocus, newFocus) -> {
        View rootView = getView();

        if (rootView == newFocus || oldFocus == null) {
            // Ignore. Same as the default.
            return;
        }


        if (rootView instanceof ViewGroup) {
            ViewGroup rootViewGroup = (ViewGroup) rootView;

            if (rootViewGroup.getDescendantFocusability() == ViewGroup.FOCUS_BLOCK_DESCENDANTS) {
                // Focus disabled on fragment.
                return;
            }

            if (UiUtils.isDescendant(rootViewGroup, newFocus)) {
                // Save the currently focused view.
                mFocusedView = newFocus;
            }
        }
    };

    /**
     * Restore the focus on the last focused view when the fragment was displayed.
     */
    protected void restoreFocus() {
        if (mFocusedView != null && !mFocusedView.hasFocus()) {
            mFocusedView.requestFocus();
        }
    }

    protected void postOnUiThread(Runnable work) {
        View view = getView();
        if (view == null || isDetached() || isRemoving() || getActivity() == null) {
            return;
        }
        view.post(work);
    }

    protected boolean hasNoFocusedView() {
        return mFocusedView == null;
    }

    /**
     * Report navigation on the page.
     */
    @Override
    public void reportNavigation() {
        Event<?, ?> event = generatePageOpenEvent();
        if (event == null) {
            // Nothing to report.
            return;
        }

        Log.event(event);
    }

    @Override
    public boolean isClearingFromBackstack() {
        return isRemoving();
    }

    public void onDialogDisplayed() {
        mUserInactivityDetector.stop();
    }

    public void onAllDialogDismissed() {
        mUserInactivityDetector.start();
    }

    public void handleBackgroundChange(View view) {
        view.setBackgroundColor(Components.get(LayoutConfig.class).getBackgroundColor());
    }
}
