/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import tv.threess.threeready.api.tv.model.IBroadcast;

/**
 * Callback interface for playback related events, including live broadcast change events.
 *
 * @author Szende Pal
 * @since 2018.05.21
 */
public interface IPlaybackDetailsCallback extends IPlaybackCallback {
    /**
     * Called when the currently playing broadcast changes.
     * This can happen because because the playback reaches the end of the current broadcast
     * or the users seeks outside of the current broadcast.
     * @param currentEvent The broadcast which is currently playing.
     */
    default void onPlayerDataUpdate(IBroadcast currentEvent) {
    }
}
