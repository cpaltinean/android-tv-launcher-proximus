package tv.threess.threeready.ui.startup.step;

import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Step that waits for HDCP active status.
 * When we start the box we need to wait for the HDCP to be available in order for us to be able to play back streams.
 *
 * @author Andor Lukacs
 * @since 2019-11-27
 */
public class WaitForHDCPStep implements Step {
    private static final String TAG = Log.tag(WaitForHDCPStep.class);

    private final AppConfig mAppConfig = Components.get(AppConfig.class);
    private final MwRepository mMwRepository = Components.get(MwRepository.class);

    private StepCallback mCallback;
    private Disposable mDisposable;

    @Override
    public void cancel() {
        RxUtils.disposeSilently(mDisposable);
    }

    @Override
    public void execute(StepCallback callback) {
        mCallback = callback;
        mCallback.getStartupTimer().pause();

        RxUtils.disposeSilently(mDisposable);

        Completable completable = mMwRepository.waitForHDCPActivation();

        // Block startup flow after QR if the screen is off.
        if (mAppConfig.isQuiescentStartupEnabled() || !mMwRepository.isInQuiescentReboot()) {
            completable = completable.timeout(
                    mAppConfig.getHDMICheckTimeout(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
        }

        mDisposable = completable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    mCallback.onFinished();
                    mCallback.getStartupTimer().resume();
                }, error -> {
                    Log.d(TAG, "Failed to wait for HDCP activation.");
                    mCallback.onFinished();
                    mCallback.getStartupTimer().resume();
                });
    }
}
