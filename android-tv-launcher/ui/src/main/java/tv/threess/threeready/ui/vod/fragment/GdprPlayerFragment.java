package tv.threess.threeready.ui.vod.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.Objects;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.contract.PlaybackType;

/**
 * Player for GDPR videos.
 *
 * @author Zsolt Bokor_
 * @since 2020.11.17
 */
public class GdprPlayerFragment extends VodPlayerFragment {
    private static final String TAG = Log.tag(GdprPlayerFragment.class);

    private static final String EXTRA_VOD = "EXTRA_VOD";
    private static final String EXTRA_VOD_PRICE = "EXTRA_VOD_PRICE";
    private static final String EXTRA_VOD_VARIANT = "EXTRA_VOD_VARIANT";

    public static GdprPlayerFragment newInstance(IVodItem vod, IVodPrice price, IVodVariant vodVariant) {
        GdprPlayerFragment fragment = new GdprPlayerFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_VOD, vod);
        args.putSerializable(EXTRA_VOD_PRICE, price);
        args.putSerializable(EXTRA_STARTED_ITEM, vodVariant);
        args.putSerializable(EXTRA_VOD_VARIANT, vodVariant);
        fragment.setArguments(args);
        return fragment;
    }

    private IVodItem mVod;
    private IVodPrice mVodPrice;
    private IVodVariant mVodVariant;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVod = getSerializableArgument(EXTRA_VOD);
        mVodPrice = getSerializableArgument(EXTRA_VOD_PRICE);
        mVodVariant = getSerializableArgument(EXTRA_VOD_VARIANT);
    }

    @Nullable
    @Override
    public IVodVariant getPlaybackData() {
        return mPlaybackDetailsManager.getGdprVariantPlayerData();
    }

    @Override
    protected boolean shouldStartPlayback() {
        return true;
    }

    @Override
    protected boolean isForActivePlayback() {
        IVodVariant playerData = mPlaybackDetailsManager.getGdprVariantPlayerData();
        if (playerData == null || mVodVariant == null) {
            return false;
        }

        return mPlaybackDetailsManager.getPlayerType() == PlaybackType.Gdpr &&
                Objects.equals(playerData.getId(), mVodVariant.getId());
    }

    @Override
    protected void startPlayback(StartAction action) {
        if (mVod == null || mVodVariant == null) {
            Log.e(TAG, "Could not start the playback, no arguments provided.");
            return;
        }

        Log.d(TAG, "startPlayback() called with: vod = [" + mVod.getTitle() + " "
                + mVod.getId() + "], vod variant = [" + mVodVariant.getTitle() + " " +
                mVodVariant.getId() + "], action = [" + action + "]");

        mPlaybackDetailsManager.startGpdr(action, mVod, mVodPrice, mVodVariant, action == StartAction.StartOver);
    }

    @Override
    protected void updateTitle(IVodVariant contentItem) {
        if (mVod != null) {
            mBinding.detailsLayout.programTitle.setText(mVod.getTitle());
        }
    }

    @Override
    protected void updateSubtitle(IVodVariant contentItem) {
        mBinding.detailsLayout.subtitleDetails.setVisibility(View.GONE);
    }

    @Override
    protected void updateQualityIndicator(IVodVariant variant) {
        mBinding.detailsLayout.iconsContainer.setVisibility(View.GONE);
    }

    @Override
    protected void updateContentMarkers(IVodVariant contentItem) {
        mBinding.detailsLayout.contentMarker.setVisibility(View.GONE);
    }

    @Override
    protected void updateMoreInfoButton(IVodVariant contentItem) {
        mBinding.buttonsContainer.moreInfoButton.setVisibility(View.GONE);
    }
}
