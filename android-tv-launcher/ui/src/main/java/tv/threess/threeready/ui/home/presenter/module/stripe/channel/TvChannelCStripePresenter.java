package tv.threess.threeready.ui.home.presenter.module.stripe.channel;

import android.content.Context;
import android.view.ViewGroup;

import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.home.presenter.card.app.EditorialA1CardPresenter;
import tv.threess.threeready.ui.home.presenter.module.stripe.generic.MultipleRowStripeModulePresenter;
import tv.threess.threeready.ui.tv.presenter.channel.TvChannelC1CardPresenter;

/**
 * Presenter used to display channel type, C variant stripes.
 *
 * @author Barabas Attila-Bela
 * @since 2017.06.10
 */
public class TvChannelCStripePresenter extends MultipleRowStripeModulePresenter {

    Map<ModuleCardVariant, PresenterSelector> mVariantCardPresenterMap = new HashMap<>();

    public TvChannelCStripePresenter(Context context, DataSourceLoader dataSourceLoader) {
        super(context, dataSourceLoader);
        // TvChannel C1 Stripe
        mVariantCardPresenterMap.put(ModuleCardVariant.C1, new ClassPresenterSelector()
                .addClassPresenter(TvChannel.class, new TvChannelC1CardPresenter(context))
                .addClassPresenter(EditorialItem.class, new EditorialA1CardPresenter(context))
        );
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent, RecyclerView.RecycledViewPool pool) {
        ViewHolder holder = (ViewHolder) super.onCreateViewHolder(parent, pool);

        holder.mBinding.multipleRowStripe.setRowHeight(mContext.getResources().getDimensionPixelOffset(R.dimen.tv_channel_card_height));
        holder.mBinding.multipleRowStripe.setItemSpacing(mContext.getResources().getDimensionPixelOffset(R.dimen.tv_channel_c_stripe_card_item_spacing));

        // Set padding based card type
        int horizontalPadding = mContext.getResources().getDimensionPixelSize(R.dimen.tv_channel_c_stripe_left_margin);
        int topPadding = mContext.getResources().getDimensionPixelSize(R.dimen.tv_channel_stripe_top_margin);
        int marginBottom = mContext.getResources().getDimensionPixelSize(R.dimen.tv_channel_stripe_bottom_margin);
        holder.mBinding.multipleRowStripe.setRowPadding(horizontalPadding, topPadding, horizontalPadding, mContext.getResources().getDimensionPixelSize(R.dimen.tv_channel_stripe_distance_between_rows));

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) holder.mBinding.multipleRowStripe.getLayoutParams();
        params.setMargins(0, 0, 0, marginBottom);

        holder.mBinding.multipleRowStripe.setWindowAlignmentOffsetPercent(mContext.getResources().getInteger(R.integer.tv_channel_stripe_window_alignment_percent));
        holder.mBinding.multipleRowStripe.setZ(1f);

        return holder;
    }

    @Override
    protected PresenterSelector getCardPresenterSelector(ModuleConfig moduleConfig) {
        return mVariantCardPresenterMap.get(moduleConfig.getCardVariant());
    }
}
