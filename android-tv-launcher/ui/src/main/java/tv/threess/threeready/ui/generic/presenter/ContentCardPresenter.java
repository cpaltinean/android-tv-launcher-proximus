package tv.threess.threeready.ui.generic.presenter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.view.BaseOrderedIconsContainer;
import tv.threess.threeready.ui.utils.ImageTransform;

/**
 * Base presenter class to handle the basic state changes
 * and display information about a content item.
 * <p>
 * Needs to be extended to display specific information
 * about the different content types (program, vod etc.)
 * <p>
 * Created by Dalma Medve on 11/16/2017.
 */

public abstract class ContentCardPresenter<THolder extends ContentCardPresenter.ViewHolder, TItem extends IBaseContentItem>
        extends TitleCardPresenter<THolder, TItem> {

    protected final Navigator mNavigator = Components.get(Navigator.class);
    protected final TvRepository mTvRepository = Components.get(TvRepository.class);
    protected final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);

    public ContentCardPresenter(Context context) {
        super(context);
    }

    @Override
    public void onBindHolder(ModuleData<?> moduleData, THolder holder, TItem item) {
        super.onBindHolder(moduleData, holder, item);
        updateInfoRow(holder, item);
    }

    /**
     * Called when the parental control configuration changes.
     */
    @Override
    protected void onParentalRatingChanged(THolder holder, TItem item) {
        super.onParentalRatingChanged(holder, item);
        updateInfoRow(holder, item);
    }

    private void updateInfoRow(THolder holder, TItem item) {
        if (mParentalControlManager.isRestricted(item)) {
            holder.getDetailsView().setVisibility(View.GONE);
        } else {
            updateInfoRowText(holder, item);
            holder.getDetailsView().setVisibility(View.VISIBLE);
        }
    }

    /**
     * Update the info row on the cards. This displays information like the genre, duration etc.
     */
    protected abstract void updateInfoRowText(THolder holder, TItem item);

    protected void updateProviderLogo(ImageView gradientView, ImageView channelLogoView, String channelId) {
        Glide.with(mContext).clear(channelLogoView);
        Glide.with(mContext)
                .load(mNowOnTvCache.getChannel(channelId))
                .apply(ImageTransform.REQUEST_OPTIONS)
                .transition(new DrawableTransitionOptions().dontTransition())
                .dontAnimate()
                .into(new DrawableImageViewTarget(channelLogoView) {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        super.onResourceReady(resource, transition);
                        view.setVisibility(View.VISIBLE);
                        gradientView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        view.setVisibility(View.GONE);
                        gradientView.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public long getStableId(TItem contentItem) {
        return Objects.hash(contentItem.getId());
    }

    public abstract static class ViewHolder extends TitleCardPresenter.ViewHolder {

        @NotNull
        public abstract TextView getDetailsView();

        protected ViewHolder(View view) {
            super(view);
        }

        protected abstract BaseOrderedIconsContainer getIconContainer();
    }


}
