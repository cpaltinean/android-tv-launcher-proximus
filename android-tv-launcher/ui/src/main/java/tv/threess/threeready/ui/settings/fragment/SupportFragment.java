package tv.threess.threeready.ui.settings.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.settings.model.SupportItem;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Fragment to display Proximus support settings.
 *
 * @author Karetka Mezei Zoltan
 * @since 2020.10.04
 */
public class SupportFragment extends BaseSettingsDialogFragment<SupportItem> {
    private static final String ITEM_TYPE = "ITEM_TYPE";

    private SupportItem.Type mItemType;

    public static SupportFragment newInstance() {
        return new SupportFragment();
    }

    public static SupportFragment newInstance(SupportItem.Type type) {
        SupportFragment settingsFragment = new SupportFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(ITEM_TYPE, type);
        settingsFragment.setArguments(bundle);

        return settingsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mItemType = getSerializableArgument(ITEM_TYPE);
        return view;
    }

    public void initView() {
        View rootView = getRootView();
        if (rootView == null) {
            return;
        }

        List<SupportItem> items = new ArrayList<>();

        items.add(new SupportItem(SupportItem.Type.SYSTEM_INFORMATION,
                mTranslator.get(TranslationKey.SETTINGS_SYSTEM_INFORMATION),
                ContextCompat.getDrawable(rootView.getContext(),R.drawable.ico_system_information)
        ));

        items.add(new SupportItem(SupportItem.Type.CHECK_FOR_UPDATES,
                mTranslator.get(TranslationKey.SETTINGS_CHECK_FOR_UPDATE),
                ContextCompat.getDrawable(rootView.getContext(),R.drawable.ico_updates)));

        items.add(new SupportItem(SupportItem.Type.CHANNEL_SCAN,
                mTranslator.get(TranslationKey.SETTINGS_CHANNEL_SCAN),
                ContextCompat.getDrawable(rootView.getContext(),R.drawable.ico_channel_scan)
        ));

        items.add(new SupportItem(SupportItem.Type.CLEAR_DATA,
                mTranslator.get(TranslationKey.SETTINGS_CLEAR_DATA),
                ContextCompat.getDrawable(rootView.getContext(),R.drawable.ico_clear_data)
        ));

        items.add(new SupportItem(SupportItem.Type.APP_RESTART,
                mTranslator.get(TranslationKey.SETTINGS_APP_RESTART),
                ContextCompat.getDrawable(rootView.getContext(),R.drawable.ico_restart_app)
        ));

        setItems(items);
    }

    @Override
    protected void setItems(Collection<SupportItem> values) {
        super.setItems(values);
        if (mItemType == null) {
            return;
        }

        // Default selection.
        int position = 0;
        for (SupportItem item : values) {
            if (item != null && item.getType() == mItemType) {
                getVerticalGridView().setSelectedPosition(position);
                break;
            }
            position++;
        }
    }

    @Override
    protected void setTitle() {
        getTitleView().setText(mTranslator.get(TranslationKey.APP_SETTINGS_SUPPORT));
    }

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        return new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                .addDetail(UILogEvent.Detail.Page, UILog.Page.SupportPage);
    }

}
