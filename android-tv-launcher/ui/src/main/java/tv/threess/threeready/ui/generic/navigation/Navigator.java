/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.generic.navigation;

import android.animation.Animator;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.account.model.account.IConsent;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.config.model.FeatureControl;
import tv.threess.threeready.api.generic.exception.BackendException;
import tv.threess.threeready.api.generic.exception.InvalidPinException;
import tv.threess.threeready.api.generic.exception.PinChangedException;
import tv.threess.threeready.api.generic.model.BingeWatchingInfo;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.generic.model.IBaseSeries;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.model.PlayOption;
import tv.threess.threeready.api.home.model.generic.ChannelApp;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.helper.ErrorType;
import tv.threess.threeready.api.log.model.Error;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.netflix.model.SourceType;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.epg.EpgLayoutType;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.api.tv.model.ZappingInfo;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.RestartMode;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.model.MediaTrackInfo;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.details.DetailOpenedFrom;
import tv.threess.threeready.ui.details.fragment.BroadcastEpisodeDialogFragment;
import tv.threess.threeready.ui.details.fragment.PlayOptionsDialog;
import tv.threess.threeready.ui.details.fragment.ProgramDetailsFragment;
import tv.threess.threeready.ui.details.fragment.RecordingEpisodeDialogFragment;
import tv.threess.threeready.ui.details.fragment.SeriesRecordingDetailsFragment;
import tv.threess.threeready.ui.details.fragment.VodEpisodeDialogFragment;
import tv.threess.threeready.ui.details.fragment.VodMovieDetailsFragment;
import tv.threess.threeready.ui.details.fragment.VodSeriesDetailsFragment;
import tv.threess.threeready.ui.epg.fragment.EpgFragment;
import tv.threess.threeready.ui.epggrid.EpgGridFragment;
import tv.threess.threeready.ui.generic.activity.MainActivity;
import tv.threess.threeready.ui.generic.dialog.ActionBarDialog;
import tv.threess.threeready.ui.generic.dialog.AlertDialog;
import tv.threess.threeready.ui.generic.dialog.BabyChannelWarningDialog;
import tv.threess.threeready.ui.generic.dialog.BaseButtonDialog.ButtonClickListener;
import tv.threess.threeready.ui.generic.dialog.BaseDialogFragment;
import tv.threess.threeready.ui.generic.dialog.BasePinDialog;
import tv.threess.threeready.ui.generic.dialog.BingeWatchingDialog;
import tv.threess.threeready.ui.generic.dialog.ChangePinDialog;
import tv.threess.threeready.ui.generic.dialog.ErrorCode;
import tv.threess.threeready.ui.generic.dialog.LoadingDialog;
import tv.threess.threeready.ui.generic.dialog.PinDialog;
import tv.threess.threeready.ui.generic.dialog.RentConfirmationDialog;
import tv.threess.threeready.ui.generic.dialog.ReplayBackToLiveDialog;
import tv.threess.threeready.ui.generic.dialog.ReplayPlusOfferDialog;
import tv.threess.threeready.ui.generic.dialog.SelectorOptionDialog;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.fragment.BasePlayerFragment;
import tv.threess.threeready.ui.generic.fragment.WebViewFragment;
import tv.threess.threeready.ui.generic.interfaces.IReportablePage;
import tv.threess.threeready.ui.generic.model.BroadcastPlayOption;
import tv.threess.threeready.ui.generic.model.RecordingPlayOption;
import tv.threess.threeready.ui.generic.model.VodPlayOption;
import tv.threess.threeready.ui.generic.notification.Notification;
import tv.threess.threeready.ui.generic.notification.NotificationManager;
import tv.threess.threeready.ui.generic.notification.SingleNotification;
import tv.threess.threeready.ui.generic.player.fragment.PlayerFragment;
import tv.threess.threeready.ui.generic.player.view.ZappingInfoView;
import tv.threess.threeready.ui.generic.utils.AnimatorListenerAdapter;
import tv.threess.threeready.ui.generic.utils.AnimatorUtils;
import tv.threess.threeready.ui.generic.utils.DeepLinkHelper;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.home.fragment.CategoryPageFragment;
import tv.threess.threeready.ui.home.fragment.HomeFragment;
import tv.threess.threeready.ui.home.fragment.ModularPageFragment;
import tv.threess.threeready.ui.pvr.fragment.RecordingPlayerFragment;
import tv.threess.threeready.ui.search.fragment.SearchFragment;
import tv.threess.threeready.ui.secret.SecretFragment;
import tv.threess.threeready.ui.secret.SecretPlayerFragment;
import tv.threess.threeready.ui.secret.SecretWebViewFragment;
import tv.threess.threeready.ui.settings.fragment.ApplicationHintsFragment;
import tv.threess.threeready.ui.settings.fragment.BaseSettingsDialogFragment;
import tv.threess.threeready.ui.settings.fragment.ChannelScanFragment;
import tv.threess.threeready.ui.settings.fragment.EpgLayoutFragment;
import tv.threess.threeready.ui.settings.fragment.LanguagePreferencesFragment;
import tv.threess.threeready.ui.settings.fragment.LanguagePreferencesSubscreensFragment;
import tv.threess.threeready.ui.settings.fragment.LockContentFragment;
import tv.threess.threeready.ui.settings.fragment.ParentalControlSettingsFragment;
import tv.threess.threeready.ui.settings.fragment.PlayerSettingsDialogFragment;
import tv.threess.threeready.ui.settings.fragment.PrivacyInfoFragment;
import tv.threess.threeready.ui.settings.fragment.PrivacyPreferencesFragment;
import tv.threess.threeready.ui.settings.fragment.RecordingsSettingsFragment;
import tv.threess.threeready.ui.settings.fragment.SettingsFragment;
import tv.threess.threeready.ui.settings.fragment.SupportFragment;
import tv.threess.threeready.ui.settings.fragment.SystemInformationFragment;
import tv.threess.threeready.ui.settings.fragment.SystemUpdateFragment;
import tv.threess.threeready.ui.settings.fragment.VodPurchaseHistoryFragment;
import tv.threess.threeready.ui.settings.model.MainSettingsItem;
import tv.threess.threeready.ui.settings.model.SupportItem;
import tv.threess.threeready.ui.startup.StartupFlow;
import tv.threess.threeready.ui.startup.fragment.ConsentFragment;
import tv.threess.threeready.ui.startup.fragment.ConsentMoreInfoFragment;
import tv.threess.threeready.ui.startup.fragment.LineNumberFragment;
import tv.threess.threeready.ui.startup.fragment.ParentalControlSetupFragment;
import tv.threess.threeready.ui.startup.fragment.SaveEnergySettingsFragment;
import tv.threess.threeready.ui.startup.fragment.StartFragment;
import tv.threess.threeready.ui.startup.fragment.WelcomeFragment;
import tv.threess.threeready.ui.startup.types.FlowTypes;
import tv.threess.threeready.ui.tv.fragment.BaseTrickPlayPlayerFragment;
import tv.threess.threeready.ui.tv.fragment.RadioPlayerFragment;
import tv.threess.threeready.ui.tv.fragment.TvPlayerFragment;
import tv.threess.threeready.ui.update.dialog.SystemUpdateOptionalDialog;
import tv.threess.threeready.ui.utils.UiUtils;
import tv.threess.threeready.ui.vod.fragment.GdprPlayerFragment;
import tv.threess.threeready.ui.vod.fragment.TrailerPlayerFragment;
import tv.threess.threeready.ui.vod.fragment.VodPlayerFragment;

/**
 * Navigation helper between the main ui components of the application.
 * <p>
 * Created by Barabas Attila on 4/24/2017.
 */
public class Navigator implements Component {
    private static final String TAG = Log.tag(Navigator.class);

    // Notification.
    private static final String NOTIFICATION_APP_ACTION = "com.android.tv.action.OPEN_NOTIFICATIONS_PANEL";
    private static final String NOTIFICATION_PANEL_PACKAGE_NAME = "com.google.android.tvlauncher";
    private static final String NOTIFICATION_PANEL_ACTIVITY = "com.google.android.tvlauncher.notifications.NotificationsSidePanelActivity";

    // Global search
    private static final String EXTRA_SEARCH_TYPE = "search_type";
    private static final int SEARCH_TYPE_KEYBOARD = 2;

    protected MainActivity mActivity;

    private LayoutTransition mContentLayoutTransition;
    ViewGroup mFragmentOverlay;

    private final NotificationManager mNotificationManager = Components.get(NotificationManager.class);
    private final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);
    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);
    private final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);
    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);
    private final TvRepository mTvRepository = Components.get(TvRepository.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final Translator mTranslator = Components.get(Translator.class);

    List<IContentChangeListener> mContentChangeListeners = new ArrayList<>();

    private StartupFlow mStartupFlow;

    private Animator mContentFadeInAnimator;
    private Animator mContentFadeOutAnimator;

    private Disposable mSeriesRecordingsDisposable;
    private Disposable mIsSubscribedDisposable;
    private Disposable mChannelInfoDisposable;
    private Disposable mUrlDisposable;

    private LoadingDialog mLoadingDialog;

    public void init(MainActivity activity, FrameLayout fragmentOverlay) {
        mActivity = activity;

        mFragmentOverlay = fragmentOverlay;
        mContentLayoutTransition = ((ViewGroup) mFragmentOverlay.getParent()).getLayoutTransition();

        mActivity.getSupportFragmentManager().addOnBackStackChangedListener(mOnBackStackChangedListener);
    }

    public void destroy() {
        MainActivity activity = mActivity;
        if (activity == null) {
            return;
        }
        activity.getSupportFragmentManager().removeOnBackStackChangedListener(mOnBackStackChangedListener);
    }


    ///////////////////////////////////
    // GENERIC
    ///////////////////////////////////

    public MainActivity getActivity() {
        return mActivity;
    }

    /**
     * Returns true when then content fragment is displayed.
     */
    public boolean isContentOverlayDisplayed() {
        return mFragmentOverlay.getVisibility() == View.VISIBLE;
    }

    /**
     * Use to check if the player UI is on the top of the stack
     *
     * @return true if the player UI is on the top, otherwise false
     */
    public boolean isPlayerUIOnTop() {
        return getContentFragment() instanceof BasePlayerFragment;
    }

    /**
     * @return true if the player UI is on the top and trick-play player is shown, otherwise false
     */
    public boolean isTrickPlayerDisplayedOnTop() {
        BaseFragment contentFragment = getContentFragment();
        if (contentFragment instanceof BaseTrickPlayPlayerFragment) {
            return ((BaseTrickPlayPlayerFragment<?>) contentFragment).isTrickPlayShown();
        }
        return false;
    }

    /**
     * Used to check if the player fragment is on top, with or without the player UI displayed
     *
     * @return true if the player fragment and/or player ui are on top, false otherwise
     */
    public boolean isStreamDisplayedOnTop() {
        return isPlayerUIOnTop() || !isContentOverlayDisplayed();
    }

    /**
     * Use to check if the player can handle PLAY/PAUSE, FF, FR actions
     *
     * @return true if can, false is can't
     */
    public boolean canHandleMediaKeys() {
        BaseFragment contentFragment = getContentFragment();
        return !isContentOverlayDisplayed() || (contentFragment instanceof BasePlayerFragment);
    }

    /**
     * @return The currently displayed content fragment
     * or null if there is no fragment.
     */
    @Nullable
    public BaseFragment getContentFragment() {
        return (BaseFragment) mActivity.getSupportFragmentManager()
                .findFragmentById(R.id.fragment_overlay);
    }

    /**
     * Returns last opened dialog which is still visible.
     */
    @Nullable
    public BaseDialogFragment getDialogFragment() {
        List<Fragment> fragments = mActivity.getSupportFragmentManager().getFragments();
        for (int i = fragments.size() - 1; i >= 0; --i) {
            Fragment fragment = fragments.get(i);
            if (fragment instanceof BaseDialogFragment) {
                BaseDialogFragment dialogFragment = (BaseDialogFragment) fragment;
                if (dialogFragment.isAdded() && !dialogFragment.isDismissed()) {
                    return dialogFragment;
                }
            }
        }

        return null;
    }

    public IReportablePage getPreviousReportablePage() {
        IReportablePage dialogPage = getDialogFragment();
        if (dialogPage != null && !dialogPage.isClearingFromBackstack()) {
            return dialogPage;
        }

        IReportablePage contentPage = getContentFragment();
        if (contentPage != null && !contentPage.isClearingFromBackstack()) {
            return contentPage;
        }

        return null;
    }

    /**
     * Add a transaction for the frament operation list and this transaction will run after the all
     * the transaction executing this. This is important for the dialogs and fragment reporting.
     * <p>
     * By example there are problems when we open more than one dialog (like most of case in the settings)
     * and then we need to report just the last opened dialog.
     *
     * @param reportablePage the fragment what executed the navigation report check
     */
    public void reportNavigationIfNeeded(IReportablePage reportablePage) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        FragmentManager fm = mActivity.getSupportFragmentManager();
        fm.beginTransaction().runOnCommit(() -> {
            IReportablePage page = getPreviousReportablePage();
            if (page != reportablePage) {
                return;
            }

            page.reportNavigation();
        }).commit();
    }

    /**
     * Returns true if a non cancelable dialog is displayed.
     */
    public boolean isModalDialogVisible() {
        BaseDialogFragment dialogFragment = getDialogFragment();
        return dialogFragment != null && !dialogFragment.isCancelable();
    }

    /**
     * Dismissing all dialogs in case of different keys can be handled on that side
     *
     * @param keyCode key code
     * @return true in case at least one dismiss was done
     */
    public boolean clearAllDialogsByKey(int keyCode) {

        if (mActivity.isInstanceStateSaved()) {
            return false;
        }

        boolean dismissWasDone = false;

        List<Fragment> fragments = mActivity.getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            if (fragment instanceof BaseDialogFragment) {
                BaseDialogFragment dialogFragment = (BaseDialogFragment) fragment;
                if (dialogFragment.isCancelable() && dialogFragment.handleKeys(keyCode)) {
                    dismissWasDone = true;
                }
            }
        }

        return dismissWasDone;

    }

    /**
     * Close all displayed non modal dialog.
     */
    public void clearAllDialogs() {
        clearAllDialogs(false);
    }

    /**
     * Close all display dialog.
     *
     * @param includeModal - if true it will close also the non cancelable dialogs.
     */
    public void clearAllDialogs(boolean includeModal) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();

        for (Fragment fragment : fragments) {
            if (fragment instanceof BaseDialogFragment) {
                BaseDialogFragment dialogFragment = (BaseDialogFragment) fragment;

                if (dialogFragment.isCancelable() || includeModal) {
                    dialogFragment.dismiss();
                    try {
                        fragmentManager.executePendingTransactions();
                    } catch (Exception e) {
                        //TODO: Change hint dialog displaying order.
                    }
                }
            }
        }
    }

    /**
     * Returns the current player fragment.
     */
    @Nullable
    public PlayerFragment getPlayerFragment() {
        return (PlayerFragment) mActivity.getSupportFragmentManager()
                .findFragmentById(R.id.player_fragment);
    }

    /**
     * This method will trigger the Parental Control settings to check if the playback is restricted.
     * If the playback is restricted then it checks if there are any active parental control rules.
     * If true, then mutes the playback and locks the player.
     */
    public void triggerParentalControl() {
        PlayerFragment playerFragment = getPlayerFragment();
        if (playerFragment == null) {
            return;
        }
        playerFragment.triggerParentalControl();
    }

    public void hideBackgroundOverlay() {
        mFragmentOverlay.setBackgroundResource(0);
    }

    /**
     * Show the semitransparent content frame overlay.
     */
    public void showContentOverlay() {
        AnimatorUtils.cancelAnimators(mContentFadeOutAnimator, mContentFadeInAnimator);
        if (isContentOverlayDisplayed()
                && mFragmentOverlay.getAlpha() == 1f) {
            // Already shown.
            return;
        }

        Log.d(TAG, "Show content overlay.");
        mFragmentOverlay.setVisibility(View.VISIBLE);
        mFragmentOverlay.setAlpha(1f);
        mFragmentOverlay.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);

        FragmentManager fm = mActivity.getSupportFragmentManager();
        fm.beginTransaction().runOnCommit(() -> {
            BaseFragment currentFragment = getContentFragment();
            if (currentFragment != null && currentFragment.isVisible()) {
                notifyContentShown(currentFragment);
                currentFragment.onContentShown();
            }
        }).commit();


        PlayerFragment playerFragment = getPlayerFragment();
        if (playerFragment != null) {
            playerFragment.setLoadingIndicatorColor(true);
        }
    }

    public void showContentOverlayWithFadeIn() {
        // do nothing if the fade in animator is running or the content overlay is visible
        if (AnimatorUtils.isRunning(mContentFadeInAnimator)) {
            Log.d(TAG, "Content Fade InAnimator is running -> return");
            return;
        }

        if (isContentOverlayDisplayed()) {
            Log.d(TAG, "Content Overlay is already displayed -> return");
            return;
        }

        AnimatorUtils.cancelAnimators(mContentFadeInAnimator, mContentFadeOutAnimator);

        showContentOverlay();

        mContentFadeInAnimator = ObjectAnimator.ofFloat(mFragmentOverlay, View.ALPHA, 0f, 1f);
        mContentFadeInAnimator.setDuration(mActivity.getResources().getInteger(R.integer.content_frame_fade_duration));
        mContentFadeInAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                disableContentFrameLayoutTransition();
            }

            @Override
            public void onAnimationFinished(Animator animation) {
                super.onAnimationFinished(animation);
                enableContentFrameLayoutTransition();
            }
        });

        mContentFadeInAnimator.start();

    }

    /**
     * Hide the semitransparent content frame overlay.
     */
    public void hideContentOverlay() {
        AnimatorUtils.cancelAnimators(mContentFadeInAnimator, mContentFadeOutAnimator);
        if (!isContentOverlayDisplayed()) {
            // Already hidden,
            return;
        }

        Log.d(TAG, "hideContentOverlay");
        PlayerFragment playerFragment = getPlayerFragment();
        if (playerFragment != null) {
            playerFragment.setLoadingIndicatorColor(false);
        }

        mFragmentOverlay.setVisibility(View.GONE);

        // Clear and block any focus while hidden.
        mFragmentOverlay.clearFocus();
        mFragmentOverlay.setDescendantFocusability(
                ViewGroup.FOCUS_BLOCK_DESCENDANTS);

        BaseFragment currentFragment = getContentFragment();

        notifyContentHide(currentFragment);
        if (currentFragment != null && currentFragment.isVisible()) {
            currentFragment.onContentHide();
        }
    }

    public void hideContentOverlayWithFadeOut() {
        // do nothing if the fade out animator is running or the content overlay isn't visible
        if (AnimatorUtils.isRunning(mContentFadeOutAnimator) || !isContentOverlayDisplayed()) {
            return;
        }

        Log.d(TAG, "hideContentOverlayWithFadeOut");

        // Cancel fade animator.
        AnimatorUtils.cancelAnimators(mContentFadeInAnimator, mContentFadeOutAnimator);

        mContentFadeOutAnimator = ObjectAnimator.ofFloat(mFragmentOverlay, View.ALPHA, 1f, 0f);
        mContentFadeOutAnimator.setDuration(mActivity.getResources().getInteger(R.integer.content_frame_fade_duration));

        mContentFadeOutAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                disableContentFrameLayoutTransition();
            }

            @Override
            public void onAnimationFinished(Animator animation) {
                super.onAnimationFinished(animation);
                hideContentOverlay();
                enableContentFrameLayoutTransition();
            }
        });

        mContentFadeOutAnimator.start();
    }

    /**
     * Enable the default fade animation for the content frame layout changes.
     */
    private void enableContentFrameLayoutTransition() {
        ((ViewGroup) mFragmentOverlay.getParent())
                .setLayoutTransition(mContentLayoutTransition);
    }

    /**
     * Disable the default fade animation for the content frame layout change
     */
    private void disableContentFrameLayoutTransition() {
        ((ViewGroup) mFragmentOverlay.getParent())
                .setLayoutTransition(null);
    }

    /**
     * Disable the default fade animation for the content frame layout change,
     * run the runnable what is needs to be executed before enables the default fade animation
     * for the frame layout change.
     */
    public void disableContentFrameLayoutTransition(Runnable runnable) {
        ((ViewGroup) mFragmentOverlay.getParent())
                .setLayoutTransition(null);
        runnable.run();
        enableContentFrameLayoutTransition();
    }

    /**
     * Returns true when the default layout animations are disabled for the content frame.
     */
    private boolean isContentFrameLayoutTransactionDisable() {
        return ((ViewGroup) mFragmentOverlay.getParent()).getLayoutTransition() == null;
    }

    /**
     * Show the welcome screen of the startup.
     */
    public void showWelcomeScreen() {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        showContentOverlay();

        if (getContentFragment() instanceof WelcomeFragment) {
            // Nothing to do. Already displayed.
            return;
        }

        showContentFragment(new WelcomeFragment());
    }

    /**
     * Set the startup flow in order to be able to differentiate whether we are in the startup or not by other fragment.
     *
     * @param startupFlow Can be either FTI flow or regular startup flow.
     */
    public void setStartupFlow(StartupFlow startupFlow) {
        mStartupFlow = startupFlow;
    }

    /**
     * Check if the startup has finished.
     * This is useful when there are certain actions that have to be canceled or done while the app is still in startup flow.
     *
     * @return True if the startup flow is active, false otherwise
     */
    public boolean isStartupFinished() {
        if (mStartupFlow != null) {
            return mStartupFlow.isStartupFinished();
        }

        return false;
    }

    /**
     * Close current fragment and goes back to previews screen
     */
    public void goBack() {
        if (mActivity != null
                && !mActivity.isInstanceStateSaved()) {
            mActivity.onBackPressed();
        }
    }

    /**
     * Pop content fragment back tack.
     * Go back the previous page.
     */
    public void popBackStack() {
        if (mActivity != null
                && !mActivity.isInstanceStateSaved()) {
            mActivity.getSupportFragmentManager().popBackStack();
        }
    }

    public void popBackStackImmediate() {
        if (mActivity != null && !mActivity.isInstanceStateSaved()) {
            mActivity.getSupportFragmentManager().popBackStackImmediate();
        }
    }

    /**
     * Remove everything from the back stack.
     */
    public void clearBackStack() {
        try {
            FragmentManager fm = mActivity.getSupportFragmentManager();
            fm.executePendingTransactions();
            if (fm.getBackStackEntryCount() > 0) {
                FragmentManager.BackStackEntry firstEntry = fm.getBackStackEntryAt(0);
                fm.popBackStackImmediate(firstEntry.getId(),
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        } catch (Exception e) {
            Log.e(TAG, "Could not clear the back stack.", e);
        }
    }

    /**
     * Remove player ui and epg from back stack.
     */
    private void popSkippingPlayerUI() {
        if (!(getContentFragment() instanceof EpgFragment)
                && !(getContentFragment() instanceof BasePlayerFragment)) {
            return;
        }

        FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
        for (int i = fragmentManager.getBackStackEntryCount() - 2; i >= 0; i--) {
            BaseFragment fragment = (BaseFragment) fragmentManager.findFragmentByTag(fragmentManager.getBackStackEntryAt(i).getName());
            if (!(fragment instanceof BasePlayerFragment)
                    && !(fragment instanceof EpgFragment)) {
                fragmentManager.popBackStack(fragmentManager.getBackStackEntryAt(i).getId(), 0);
                break;
            }
            if (i == 0) {
                // Clear back stack when no fragment is found which is not a player ui or epg.
                fragmentManager.popBackStack(fragmentManager.getBackStackEntryAt(0).getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }
    }

    /**
     * Returns the current entry from the back stack.
     */
    private FragmentManager.BackStackEntry getCurrentEntryFromBackStack() {
        FragmentManager fm = mActivity.getSupportFragmentManager();
        FragmentManager.BackStackEntry current = null;
        if (fm.getBackStackEntryCount() > 0) {
            current = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1);
        }
        return current;
    }


    //////////////////////////////////
    //////    PLAYBACK_ERROR    //////
    //////////////////////////////////

    /**
     * Show player surface.
     */
    public void showPlayerSurface(StepCallback callback) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        if (getPlayerFragment() != null) {
            // Player fragment already displayed.
            if (callback != null) {
                callback.onFinished();
            }
            return;
        }

        PlayerFragment playerFragment = PlayerFragment.newInstance(callback);
        mActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.player_fragment, playerFragment, PlayerFragment.TAG)
                .commitAllowingStateLoss();
    }

    /**
     * This method is used to implement the actions from WebView: will display the player depending on the Url.
     *
     * @param url Th Url used to decide if the player will be shown.
     */
    public void doDeepLinkActionFromWebView(String url) {
        if (mPlaybackDetailsManager == null) {
            return;
        }

        try {
            DeepLinkHelper helper = new DeepLinkHelper(url);
            if (helper.canProcessUrl()) {
                getActivity().getSupportFragmentManager().popBackStack();

                if (DeepLinkHelper.APP_TYPE.equals(helper.getType()) &&
                        DeepLinkHelper.ACTION_OPEN.equals(helper.getAction())) {
                    // Subscription process completed, open application
                    mPlaybackDetailsManager.startApplication(StartAction.DeepLink, helper.getId(), SourceType.Channel);
                    return;
                }
                showLivePlayer(StartAction.DeepLink, helper.getId(), false);
            }
        } catch (Exception ex) {
            Log.d(TAG, "Error on processing the url: " + ex);
        }
    }

    /**
     * Checks if the user has the subscription needed, then starts the application or starts the subscription process.
     */
    public void checkSubscriptionAndOpenApp(StartAction action, TvChannel channel, SourceType sourceType) {
        Map<String, ChannelApp> channelApps = Components.get(AppConfig.class).getChannelApps();
        if (channel == null || !channelApps.containsKey(channel.getCallLetter())) {
            return;
        }
        ChannelApp channelApp = channelApps.get(channel.getCallLetter());

        if (channelApp == null) {
            return;
        }

        // Check if subscription needed
        if (!channelApp.isSubscriptionNeeded()) {
            openApp(action, channel.getId(), sourceType);
            return;
        }

        RxUtils.disposeSilently(mIsSubscribedDisposable);

        mIsSubscribedDisposable = Single.create(
                (SingleOnSubscribe<Boolean>) emitter -> emitter.onSuccess(mAccountRepository.isSubscribed(channelApp.getSubscriptionPackage())))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isSubscribed -> {
                            if (isSubscribed) {
                                // User is subscribed, start the application
                                openApp(action, channel.getId(), sourceType);
                            } else {
                                // User is not subscribed, start the subscription process
                                openDeepLink(channelApp.getTvoLink());
                            }
                        },
                        throwable -> Log.e(TAG, "Could not retrieve the subscribed state for package.", throwable));
    }

    private void openApp(StartAction action, String channelId, SourceType sourceType) {
        RxUtils.disposeSilently(mChannelInfoDisposable);
        mChannelInfoDisposable = mTvRepository.getAppChannelPackageId(channelId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(packageId -> {
                    Log.d(TAG, "Found package name for APP: " + packageId);
                    if (mPlaybackDetailsManager != null) {
                        mPlaybackDetailsManager.startApplication(action, packageId, sourceType);
                    }
                });
    }

    /**
     * Show live player ui.
     */
    public void showLivePlayer(StartAction action, String channelId, boolean isFavorite) {
        showLivePlayer(action, channelId, isFavorite, null, true);
    }

    /**
     * Show live player ui.
     *
     * @param action     event action
     * @param channelId  which channel player should be displayed
     * @param isFavorite the state of the favorite flag
     * @param event      event which could trigger the showing action, this can be null
     */
    public void showLivePlayer(StartAction action, String channelId, boolean isFavorite, KeyEvent event, boolean shouldStartPlayback) {
        if (mActivity.isInstanceStateSaved() || TextUtils.isEmpty(channelId)) {
            return;
        }

        if (mPlaybackDetailsManager == null) {
            return;
        }

        if (mPlaybackDetailsManager.isAppChannelPlaying(channelId)
                || mPlaybackDetailsManager.isRegionalChannelPlaying(channelId)) {
            hideContentOverlay();
            return;
        }

        // Get currently playing item.
        ZappingInfo zappingInfo = mNowOnTvCache.getZappingInfo(channelId, isFavorite);
        if (zappingInfo == null) {
            Log.e(TAG, "No channel with id. " + channelId);
            return;
        }
        IBroadcast broadcast = zappingInfo.getBroadcast();

        // Already added. Show player UI.
        if (getContentFragment() instanceof TvPlayerFragment) {
            TvPlayerFragment tvPlayerFragment = (TvPlayerFragment) getContentFragment();
            IBroadcast playingBroadcast = tvPlayerFragment.getPlayerStartedItem();
            if (playingBroadcast != null && Objects.equals(broadcast.getChannelId(), playingBroadcast.getChannelId())) {
                showContentOverlayWithFadeIn();
                return;
            }
        }

        popSkippingPlayerUI();

        // Add live player fragment.
        TvPlayerFragment tvPlayerFragment = TvPlayerFragment.newInstance(isFavorite, broadcast, shouldStartPlayback);
        showContentFragment(action, tvPlayerFragment, event);
    }

    /**
     * Show replay player ui.
     */
    public void showReplayPlayer(StartAction action, IBroadcast broadcast, KeyEvent event, boolean shouldStartPlayback, boolean isFromFavourites) {
        if (mActivity.isInstanceStateSaved() || broadcast == null) {
            return;
        }

        // Already added. Show player ui.
        if (getContentFragment() instanceof TvPlayerFragment) {
            showContentOverlayWithFadeIn();
            return;
        }

        popSkippingPlayerUI();

        // Add replay player fragment..
        TvPlayerFragment replayPlayerFragment = TvPlayerFragment.newInstance(broadcast, shouldStartPlayback, isFromFavourites);
        showContentFragment(action, replayPlayerFragment, event);
    }

    /**
     * Show recording player UI and start the recording if not playing.
     *
     * @param action    the current {@link StartAction}.
     * @param recording The recorded broadcast
     *                  for which the player UI should be shown and the playback started.
     * @param event     event which could trigger the showing action, this can be null
     */
    public void showRecordingPlayer(StartAction action, IRecording recording, KeyEvent event, boolean shouldStartPlayback) {
        showRecordingPlayer(action, recording, null, event, shouldStartPlayback);
    }

    public void showRecordingPlayer(StartAction action, IRecording recording, IRecordingSeries recordingSeries, KeyEvent event, boolean shouldStartPlayback) {
        if (mActivity.isInstanceStateSaved() || recording == null) {
            return;
        }

        // Already added. Show player ui.
        if (getContentFragment() instanceof RecordingPlayerFragment) {
            RecordingPlayerFragment fragment = (RecordingPlayerFragment) getContentFragment();
            IBroadcast playerStartedItem = fragment.getPlayerStartedItem();
            if (playerStartedItem != null && Objects.equals(playerStartedItem.getId(), recording.getId())) {
                showContentOverlayWithFadeIn();
                return;
            }
        }

        popSkippingPlayerUI();

        // Add replay player fragment..
        RecordingPlayerFragment recordingPlayerFragment = RecordingPlayerFragment.newInstance(recording, recordingSeries, shouldStartPlayback);
        showContentFragment(action, recordingPlayerFragment, event);
    }

    /**
     * Given a broadcast wrapper handle which player to show.
     */
    public void showPlayer(PlayOption playOption, boolean isFromFavourites) {
        switch (playOption.getType()) {
            case LIVE:
                BroadcastPlayOption<IBroadcast> broadcastPlayOption = (BroadcastPlayOption) playOption;
                IBroadcast broadcast = broadcastPlayOption.getContentItem();
                showLivePlayer(StartAction.Ui, broadcast.getChannelId(), false);
                break;

            case REPLAY:
                broadcastPlayOption = (BroadcastPlayOption) playOption;
                broadcast = broadcastPlayOption.getContentItem();
                mPlaybackDetailsManager.startReplay(StartAction.Ui, broadcast, false, false, isFromFavourites);
                showReplayPlayer(StartAction.Ui, broadcast, null, false, isFromFavourites);
                break;

            case RECORDING:
                RecordingPlayOption recordingPlayOption = (RecordingPlayOption) playOption;
                showRecordingPlayer(StartAction.Ui, recordingPlayOption.getContentItem(), null, true);
                break;

            case RENTED:
            case INCLUDED:
                VodPlayOption vodPlayOption = (VodPlayOption) playOption;
                showVodPlayer(StartAction.Ui, vodPlayOption.getVodVariant(), vodPlayOption.getVod(), vodPlayOption.getVodPrice());
                break;

            case RADIO:
                broadcastPlayOption = (BroadcastPlayOption) playOption;
                broadcast = broadcastPlayOption.getContentItem();
                showRadioPlayer(StartAction.Ui, broadcast.getChannelId(), false);
                break;
        }
    }

    /**
     * Show vod player ui.
     */
    public void showVodPlayer(StartAction action, IVodVariant vodVariant, IVodItem vod, IVodPrice price) {
        showVodPlayer(action, vodVariant, vod, null, price);
    }

    public void showVodPlayer(StartAction action, IVodVariant vodVariant, IVodItem vod, IVodSeries vodSeries, IVodPrice price) {
        showVodPlayer(action, vodVariant, vod, vodSeries, price, null, true);
    }

    public void showVodPlayer(StartAction action, IVodVariant vodVariant, IVodItem vod, IVodSeries vodSeries, IVodPrice price, KeyEvent event, boolean shouldStartPlayback) {
        if (mActivity.isInstanceStateSaved() || vodVariant == null || vod == null) {
            return;
        }

        // Already added. Show player ui.
        if (getContentFragment() instanceof VodPlayerFragment) {
            VodPlayerFragment fragment = (VodPlayerFragment) getContentFragment();
            IVodVariant playerStartedItem = fragment.getPlayerStartedItem();
            if (playerStartedItem != null &&
                    Objects.equals(playerStartedItem.getId(), vodVariant.getId())) {
                showContentOverlayWithFadeIn();
                return;
            }
        }

        popSkippingPlayerUI();

        // Add vod player fragment.
        VodPlayerFragment vodPlayerFragment = VodPlayerFragment.newInstance(vodVariant, vod, vodSeries, price, shouldStartPlayback);
        showContentFragment(action, vodPlayerFragment, event);
    }

    /**
     * Show trailer player ui.
     */
    public void showTrailerPlayer(StartAction action, IVodVariant vodVariant, IVodItem vod) {
        showTrailerPlayer(action, vodVariant, vod, null, true);
    }

    public void showTrailerPlayer(StartAction action, IVodVariant vodVariant, IVodItem vod, KeyEvent event, boolean shouldStartPlayback) {
        if (mActivity.isInstanceStateSaved() || vodVariant == null || vod == null) {
            return;
        }

        // Already added. Show player ui.
        if (getContentFragment() instanceof TrailerPlayerFragment) {
            TrailerPlayerFragment fragment = (TrailerPlayerFragment) getContentFragment();
            IVodVariant startedItem = fragment.getPlayerStartedItem();
            if (startedItem != null && vodVariant.getId().equals(startedItem.getId())) {
                showContentOverlayWithFadeIn();
                return;
            }
        }

        popSkippingPlayerUI();

        // Add trailer player fragment.
        TrailerPlayerFragment trailerPlayerFragment = TrailerPlayerFragment.newInstance(vodVariant, vod, shouldStartPlayback);
        showContentFragment(action, trailerPlayerFragment, event);
    }

    public void showGdprPlayer(StartAction action, IVodItem vod, IVodPrice vodPrice, IVodVariant vodVariant) {
        if (mActivity.isInstanceStateSaved() || vod == null || vodVariant == null) {
            return;
        }

        // Already added. Show Player UI.
        if (getContentFragment() instanceof GdprPlayerFragment) {
            GdprPlayerFragment fragment = (GdprPlayerFragment) getContentFragment();
            IVodVariant startedItem = fragment.getPlayerStartedItem();
            if (startedItem != null && vodVariant.getId().equals(startedItem.getId())) {
                showContentOverlayWithFadeIn();
                return;
            }
        }

        popSkippingPlayerUI();

        // Add Gdpr player fragment.
        GdprPlayerFragment gdprPlayerFragment = GdprPlayerFragment.newInstance(vod, vodPrice, vodVariant);
        showContentFragment(action, gdprPlayerFragment, null);
    }

    /**
     * Show radio player ui.
     */
    public void showRadioPlayer(StartAction action, String channelId, boolean isFavorite) {
        showRadioPlayer(action, channelId, isFavorite, null, true);
    }

    /**
     * Show radio player ui.
     */
    public void showRadioPlayer(StartAction action, String channelId, boolean isFavorite, KeyEvent event, boolean shouldStartPlayback) {
        if (mActivity.isInstanceStateSaved() || TextUtils.isEmpty(channelId)) {
            return;
        }

        if (getContentFragment() instanceof RadioPlayerFragment) {
            RadioPlayerFragment livePlayerFragment = (RadioPlayerFragment) getContentFragment();
            if (channelId.equals(livePlayerFragment.getChannelId())) {
                showContentOverlayWithFadeIn();
                return;
            }
        }

        popSkippingPlayerUI();

        // Add radio player fragment.
        RadioPlayerFragment radioPlayerFragment = RadioPlayerFragment.newInstance(channelId, isFavorite, shouldStartPlayback);
        showContentFragment(action, radioPlayerFragment, event);
    }

    /////////////////////////////////////
    // HOME
    /////////////////////////////////////

    /**
     * Show the main page of the application.
     */
    public void showHome() {
        if (mActivity.isInstanceStateSaved() || mPlaybackDetailsManager == null) {
            return;
        }

        showContentOverlay();
        HomeFragment homeFragment = HomeFragment.newInstance();
        showContentFragment(homeFragment);
        clearAllDialogs();
    }

    /**
     * Show dynamic page.
     */
    public void showDynamicPage(PageConfig pageConfig) {
        showDynamicPage(pageConfig, null);
    }

    public void showDynamicPage(PageConfig pageConfig, boolean shouldHideOnBack) {
        showDynamicPage(pageConfig, null, shouldHideOnBack);
    }

    public void showDynamicPage(PageConfig pageConfig, String selectedMenuItem) {
        showDynamicPage(pageConfig, selectedMenuItem, false);
    }

    public void showDynamicPage(PageConfig pageConfig, String selectedMenuItem, boolean shouldHideOnBack) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        showContentOverlay();
        ModularPageFragment modularPageFragment = ModularPageFragment.newInstance(pageConfig, selectedMenuItem, shouldHideOnBack);
        showContentFragment(modularPageFragment);
    }

    /**
     * Show dynamic VOD category browsing page for the given category.
     *
     * @param categoryId The category id for which the dynamic page should be displayed.
     */
    public void showCategoryPage(String categoryId, String emptyPageMessage) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        showContentOverlay();
        CategoryPageFragment categoryPage = CategoryPageFragment.newInstance(categoryId, emptyPageMessage);
        showContentFragment(categoryPage);
    }

    /////////////////////////////////////
    // EPG
    ////////////////////////////////////

    /**
     * Show EPG for the currently running playback.
     */
    public void showEpgForPlayback() {
        showEpgForPlayback(false);
    }

    /**
     * Show epg for currently running playback and expand the program guide if the param
     * expandProgramGuide is true
     *
     * @param expandProgramGuide - if this is true, expand the tv program guide on the EPG
     */
    private void showEpgForPlayback(boolean expandProgramGuide) {
        if (mPlaybackDetailsManager == null) {
            Log.e(TAG, "Could not show the EPG. Playback manager is not initialized yet.");
            return;
        }

        if (mPlaybackDetailsManager.getPlayerType() == PlaybackType.RadioIpTv ||
                mPlaybackDetailsManager.getPlayerType() == PlaybackType.RadioOtt) {
            showEpgForPlayback(mNowOnTvCache.getLastPlayedRadioChannelId(), expandProgramGuide);
        } else {
            showEpgForPlayback(mNowOnTvCache.getLastPlayedChannelId(), expandProgramGuide);
        }
    }

    /**
     * Show EPG with for the given channelId or currently running channel, if channelId is null.
     *
     * @param channelId          - the channel id for which the EPG should open. if null, open EPG for currently running channel
     * @param expandProgramGuide - if true, expand tv program guide
     */
    private void showEpgForPlayback(@NonNull String channelId, boolean expandProgramGuide) {
        if (mPlaybackDetailsManager == null) {
            Log.e(TAG, "Could not show the EPG. Playback manager is not initialized yet.");
            return;
        }

        boolean isFavorite = mPlaybackDetailsManager.isFavorite();

        // If the Guide button is pressed during an app is displayed, we have to show the Tv Guide.
        if (mPlaybackDetailsManager.isAppPlaybackType()) {
            showEpg(channelId, isFavorite, expandProgramGuide);
            return;
        }

        switch (mPlaybackDetailsManager.getPlayerType()) {
            case LiveTvIpTv:
            case LiveTvOtt:
            case RadioIpTv:
            case RadioOtt:
            case Vod:
            case Recording:
            case VodTrailer:
                Log.d(TAG, "Show EPG for the current playback.");
                showEpg(channelId, isFavorite, expandProgramGuide);
                break;

            case Replay:
                Log.d(TAG, "Show EPG for replay playback.");
                IBroadcast broadcast = mPlaybackDetailsManager.getReplayPlayerData();
                if (broadcast != null) {
                    showEpg(broadcast.getChannelId(), isFavorite, expandProgramGuide);
                } else {
                    showEpg(mPlaybackDetailsManager.getChannelId(), isFavorite, expandProgramGuide);
                }
                break;
        }
    }

    /**
     * Returns the previous entry from back stack.
     */
    public FragmentManager.BackStackEntry getPreviousEntryFromBackStack() {
        FragmentManager fm = mActivity.getSupportFragmentManager();
        FragmentManager.BackStackEntry prevEntry = null;
        if (fm.getBackStackEntryCount() > 1) {
            prevEntry = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 2);
        }
        return prevEntry;
    }

    /**
     * Open classical EPG by Guide button press from remote.
     */
    public void showEpgGridFromGuideButton() {
        clearAllDialogs();

        BaseFragment currentFragment = getContentFragment();

        // Already displayed. Just toggle visibility.
        if (currentFragment instanceof EpgGridFragment) {
            EpgGridFragment epgFragment = (EpgGridFragment) currentFragment;
            if (isContentOverlayDisplayed()
                    && !mPlaybackDetailsManager.isAppPlaybackType()
                    && !mActivity.isInBackground()) {
                hideContentOverlayWithFadeOut();
            } else {
                epgFragment.selectNowPlaying();
                showContentOverlayWithFadeIn();
            }
            return;
        }

        // Tune to last channel.
        if (!mPlaybackDetailsManager.inPlaybackType(
                PlaybackType.LiveTvIpTv, PlaybackType.RadioIpTv, PlaybackType.Replay)) {
            mPlaybackDetailsManager.startChannel(StartAction.BtnGuide,
                    mNowOnTvCache.getLastPlayedChannelId(), RestartMode.Channel);
        }

        showEpgGridFragment();
    }

    /**
     * Shows the epg layout selected by the user.
     */
    public void showEpgLayoutFromGuideButton() {
        EpgLayoutType epgLayoutType = Settings.epgLayoutType.getEnum(EpgLayoutType.MiniEpg);
        switch (epgLayoutType) {
            case MiniEpg:
                showEpgFromGuideButton();
                break;
            default:
                showEpgGridFromGuideButton();
                break;
        }
    }

    /**
     * Open the classical EPG grid.
     */
    public void showEpgGridFragment() {
        BaseFragment currentFragment = getContentFragment();
        if (currentFragment instanceof EpgGridFragment) {
            // Already displayed.
            return;
        }

        if (!isContentOverlayDisplayed()) {
            clearBackStack();
        }

        showContentFragment(new EpgGridFragment());
    }

    /**
     * Show the channel zapper and program guide component for the specified channel.
     */
    private void showEpg(String channelId, boolean isFavorite) {
        showEpg(channelId, isFavorite, false);
    }

    /**
     * Displays the EPG for the specified channel id
     *
     * @param channelId          - the id of the channel that the epg should put focus on
     * @param isFavorite         - flag that states if the specified channel is added to favorites
     * @param expandProgramGuide - flag that states if the EPG should be displayed with the program guide expanded
     */
    private void showEpg(String channelId, boolean isFavorite, boolean expandProgramGuide) {
        if (mPlaybackDetailsManager == null) {
            return;
        }

        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        // We can't show EPG in offline mode.
        if (Components.get(FeatureControl.class).isOffline()) {
            showHome();
            return;
        }

        FragmentManager.BackStackEntry currentEntry = getCurrentEntryFromBackStack();
        if (currentEntry != null && EpgFragment.TAG.equals(currentEntry.getName())) {
            // Nothing to do. EPG already displayed.
            Log.d(TAG, "Epg is already on the top of the backstack. Showing content overlay.");
            ((EpgFragment) getContentFragment()).openTvGuide(expandProgramGuide);
            showContentOverlayWithFadeIn();
            return;
        }

        // Pop backstack to redisplay EPG.
        FragmentManager.BackStackEntry prevEntry = getPreviousEntryFromBackStack();
        if (prevEntry != null && EpgFragment.TAG.equals(prevEntry.getName())) {
            Log.d(TAG, "Epg is in the backstack, showing that instance.");
            mActivity.getSupportFragmentManager().popBackStackImmediate(prevEntry.getName(), 0);
            showContentOverlayWithFadeIn();
            return;
        }

        // Replace the current player UI with EPG
        popSkippingPlayerUI();

        BaseFragment epgFragment = EpgFragment.newInstance(channelId, isFavorite, expandProgramGuide);
        showContentFragment(epgFragment);
    }

    /**
     * Show the channel zapper and program guide component for the last played channel.
     */
    public void showEpg(boolean isFavorite) {
        if (mPlaybackDetailsManager != null) {
            showEpg(mPlaybackDetailsManager.getChannelId(), isFavorite);
        }
    }

    public void showEpgFromGuideButton() {
        Log.d(TAG, "Show EPG from guide button press");
        clearAllDialogs();

        if (getPlayerFragment() != null) {
            ZappingInfoView zappingInfoView = getPlayerFragment().getChannelInfoView();
            if (zappingInfoView != null && zappingInfoView.isZapAnimatorRunning()) {
                zappingInfoView.skipZapAnimator(StartAction.Ui);
            }
        }

        BaseFragment contentFragment = getContentFragment();
        if (contentFragment instanceof EpgFragment) {
            if (isContentOverlayDisplayed() && !mActivity.isInBackground()) {
                hideContentOverlay();
            } else {
                Log.d(TAG, "Show EPG from guide button press - open TV guide");
                if (!mPlaybackDetailsManager.inPlaybackType(
                        PlaybackType.LiveTvIpTv,
                        PlaybackType.RadioIpTv,
                        PlaybackType.Replay)) {
                    tuneToLastChannel(StartAction.BtnGuide, true, false, true);
                    return;
                }
                ((EpgFragment) contentFragment).openTvGuide(true);
                showContentOverlay();
            }
        } else if (mPlaybackDetailsManager != null &&
                mPlaybackDetailsManager.inPlaybackType(
                        PlaybackType.LiveTvIpTv,
                        PlaybackType.RadioIpTv,
                        PlaybackType.Replay)) {
            Log.d(TAG, "Show EPG from guide button press - from LivePlayer, ReplayPlayer or RadioPlayer");
            showEpgForPlayback(true);
        } else {
            Log.d(TAG, "Show EPG from guide button press - from the app - other cases");
            tuneToLastChannel(StartAction.BtnGuide, false, false, true);
        }
    }

    /**
     * Start last tuned channel. If no channel was played before tunes to first channel.
     */
    public void tuneToLastChannel(StartAction action) {
        tuneToLastChannel(action, false, false, false);
    }

    public void tuneToLastChannel(StartAction action, boolean forceRestart, boolean hideOverlay) {
        tuneToLastChannel(action, forceRestart, hideOverlay, false);
    }

    public void tuneToLastChannelAndShowEpg() {
        if (mPlaybackDetailsManager == null) {
            Log.e(TAG, "Could not tune to the last channel, missing Playback detail manager!");
            return;
        }
        String lastPlayedChannelId = mNowOnTvCache.getLastPlayedChannelId();
        mPlaybackDetailsManager.startChannel(StartAction.BtnGuide, lastPlayedChannelId, RestartMode.Forced);
        EpgLayoutType epgLayoutType = Settings.epgLayoutType.getEnum(EpgLayoutType.MiniEpg);
        if (epgLayoutType == EpgLayoutType.MiniEpg) {
            showEpgForPlayback(lastPlayedChannelId, true);
        } else {
            showEpgGridFragment();
        }
    }

    /**
     * Start last tuned channel. If no channel was played before tunes to first channel.
     *
     * @param forceRestart Force restart the stream even if the channel is already playing.
     * @param hideOverlay  Hide the content overlay after the tune to display the playback in full screen.
     */
    private void tuneToLastChannel(StartAction action, boolean forceRestart, boolean hideOverlay, boolean showEpg) {
        if (mPlaybackDetailsManager == null) {
            Log.e(TAG, "Could not tune to the last channel, missing Playback detail manager!");
            return;
        }
        Log.d(TAG, "tuneToLastChannel() called with: forceRestart = [" + forceRestart + "], hideOverlay = [" + hideOverlay + "]");
        String lastPlayedChannelId = mNowOnTvCache.getLastPlayedChannelId();
        Log.d(TAG, "Last tuned channel: " + lastPlayedChannelId);
        if (hideOverlay) {
            hideContentOverlay();
        }
        Log.d(TAG, "showLivePlayer tuneToLastChannel channelId: " + lastPlayedChannelId + " forceRestart: " + forceRestart);
        mPlaybackDetailsManager.startChannel(action, lastPlayedChannelId, forceRestart ? RestartMode.Forced : RestartMode.LiveChannel);
        if (showEpg) {
            showEpgForPlayback(lastPlayedChannelId, true);
        }

    }

    /////////////////////////////////////////
    // DETAILS
    ////////////////////////////////////////

    /**
     * Show detail page for the given broadcast.
     *
     * @param broadcast The broadcast for which the detail needs to open.
     */
    public void showProgramDetails(IBroadcast broadcast) {
        showProgramDetails(broadcast, DetailOpenedFrom.Default);
    }

    /**
     * Show detail page for the given broadcast.
     *
     * @param broadcast  The broadcast for which the detail needs to open.
     * @param openedFrom The place from where the detail page was opened.
     */
    public void showProgramDetails(IBroadcast broadcast, DetailOpenedFrom openedFrom) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        if (broadcast == null) {
            Log.w(TAG, "Cannot open detail page without valid program.");
            return;
        }

        showContentOverlay();
        BaseFragment fragment = ProgramDetailsFragment.newInstance(broadcast, openedFrom);
        showContentFragment(fragment);
    }

    /**
     * Show detail page for the given broadcast.
     *
     * @param broadcastId The id of the broadcast for which the detail needs to open.
     */
    public void showProgramDetails(String broadcastId) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }
        if (TextUtils.isEmpty(broadcastId)) {
            Log.w(TAG, "Cannot open detail page without valid program.");
            return;
        }

        showContentOverlay();
        BaseFragment fragment = ProgramDetailsFragment.newInstance(broadcastId);
        showContentFragment(fragment);
    }

    /**
     * Close the program detail of the given broadcast if currently opened.
     *
     * @param broadcastId The identifier of the the broadcast for which the detail page should be closed.
     */
    public void closeProgramDetailPage(String broadcastId) {
        BaseFragment currentFragment = getContentFragment();

        if (currentFragment instanceof ProgramDetailsFragment) {
            ProgramDetailsFragment programDetailsFragment = (ProgramDetailsFragment) currentFragment;
            if (programDetailsFragment.isOpenedFor(broadcastId)) {
                clearAllDialogs(false);
                mActivity.getSupportFragmentManager().popBackStack();
            }
        }
    }

    /**
     * Open series recording page for the given episode.
     *
     * @param broadcast  The episode for which the series recording detail page needs to be opened.
     * @param openedFrom The place from where the detail page was opened.
     */
    public void showSeriesRecordingDetailPage(IRecording broadcast, DetailOpenedFrom openedFrom) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        if (broadcast == null) {
            Log.w(TAG, "Cannot open detail page without valid program.");
            return;
        }

        showContentOverlay();
        BaseFragment fragment = SeriesRecordingDetailsFragment.newInstance(broadcast, openedFrom);
        showContentFragment(fragment);
    }

    /**
     * Open series recording detail page for the given seris.
     *
     * @param seriesId   The series id for which the series recording page needs to be openned.
     * @param openedFrom The place form where the detail page was openned.
     */
    public void showSeriesRecordingDetailPage(String seriesId, DetailOpenedFrom openedFrom) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        if (TextUtils.isEmpty(seriesId)) {
            Log.w(TAG, "Cannot open detail page without valid series.");
            return;
        }

        showContentOverlay();
        BaseFragment fragment = SeriesRecordingDetailsFragment.newInstance(seriesId, openedFrom);
        showContentFragment(fragment);
    }

    /**
     * Show detail page for a VOD movie.
     *
     * @param movieId The identifier of the VOD for which the detail page should be opened.
     */
    public void showVodMovieDetails(String movieId) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        if (TextUtils.isEmpty(movieId)) {
            Log.w(TAG, "Cannot open vod movie page without valid movieId.");
            return;
        }

        showContentOverlay();
        BaseFragment fragment = VodMovieDetailsFragment.newInstance(movieId);
        showContentFragment(fragment);
    }

    /**
     * Show detail page for a VOD movie.
     *
     * @param movie The vod for which the detail page should be displayed.
     */
    public void showVodMovieDetails(IBaseVodItem movie) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }
        if (movie == null) {
            Log.w(TAG, "Cannot open detail page without valid movie.");
            return;
        }

        showContentOverlay();
        if (movie.isEpisode()) {
            BaseFragment fragment = VodSeriesDetailsFragment.newInstance(movie);
            showContentFragment(fragment);
        } else {
            BaseFragment fragment = VodMovieDetailsFragment.newInstance(movie);
            showContentFragment(fragment);
        }
    }

    /**
     * Show vod series detail page.
     */
    public void showVodSeriesDetails(String seriesId) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }
        if (TextUtils.isEmpty(seriesId)) {
            Log.w(TAG, "Cannot open detail page.");
            return;
        }
        showContentOverlay();

        BaseFragment fragment = VodSeriesDetailsFragment.newInstance(seriesId);

        showContentFragment(fragment);
    }

    /**
     * Open dialog for a TV episode which displays basic information and actions.
     *
     * @param broadcast       The TV program for which the dialog will be displayed.
     * @param recordingStatus The current recording status of the broadcast.
     *                        If null, no recording action will be possible on the dialog.
     * @param openedFrom      The section from where the detail page was opened.
     */
    public void showBroadcastEpisodeDetailsDialog(IBroadcast broadcast,
                                                  IBookmark bookmark,
                                                  @Nullable RecordingStatus recordingStatus,
                                                  DetailOpenedFrom openedFrom) {
        BroadcastEpisodeDialogFragment dialogFragment = BroadcastEpisodeDialogFragment
                .newInstance(broadcast, recordingStatus, openedFrom, bookmark);
        showDialogOnTop(dialogFragment);
    }

    /**
     * Open dialog for a Recording episode which displays basic information and actions.
     *
     * @param recording       The Recording program for which the dialog will be displayed.
     * @param recordingStatus The current recording status of the broadcast.
     *                        If null, no recording action will be possible on the dialog.
     * @param openedFrom      The section from where the detail page was opened.
     */
    public void showRecordingEpisodeDetailsDialog(IRecording recording,
                                                  IRecordingSeries recordingSeries,
                                                  IBookmark bookmark,
                                                  @Nullable RecordingStatus recordingStatus,
                                                  DetailOpenedFrom openedFrom) {
        RecordingEpisodeDialogFragment dialogFragment = RecordingEpisodeDialogFragment
                .newInstance(recording, recordingSeries, recordingStatus, openedFrom, bookmark);
        showDialogOnTop(dialogFragment);
    }

    /**
     * Open dialog for a VOD Series episode which displays basic information and actions.
     *
     * @param series  The series of the episode.
     * @param episode The VOD episode for which the dialog will be displayed.
     */
    public void showVodEpisodeDetailsDialog(IVodSeries series, IBaseVodItem episode) {
        VodEpisodeDialogFragment dialogFragment = VodEpisodeDialogFragment.newInstance(episode, series);
        showDialogOnTop(dialogFragment);
    }


    ///////////////////////////////////
    // DIALOGS
    ////////////////////////////////////

    /**
     * Display the given dialog.
     * If a dialog already shown, only the dialog with highest priority remain displayed.
     */
    public void showDialog(BaseDialogFragment dialog) {
        BaseDialogFragment displayedDialog = getDialogFragment();
        if (displayedDialog == null
                || dialog.getPriority() >= displayedDialog.getPriority()) {
            showDialogFragment(dialog);
        }
    }


    /**
     * Close all current dialogs and displays the given dialog.
     */
    private void showDialogFragment(BaseDialogFragment dialogFragment) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        try {
            dialogFragment.show(mActivity.getSupportFragmentManager(), BaseDialogFragment.TAG);
            clearAllDialogs(true);
        } catch (IllegalStateException e) {
            Log.e(TAG, "Cannot perform this action after onSaveInstanceState ", e);
        }
    }

    /**
     * Series a new dialog without closing other dialogs
     */
    public void showDialogOnTop(BaseDialogFragment dialogFragment) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        dialogFragment.show(mActivity.getSupportFragmentManager(), BaseDialogFragment.TAG);
    }

    /**
     * Showing full screen loading dialog.
     */
    public void showLoading() {
        mLoadingDialog = LoadingDialog.newInstance(mLoadingDialog);
        showDialogOnTop(mLoadingDialog);
    }

    /**
     * Dismissing full screen loading dialog.
     */
    public void hideLoading() {
        if (mLoadingDialog != null) {
            mLoadingDialog.dismiss();
            mLoadingDialog = null;
        }
    }

    ////////////////////////////
    // ANDROID
    ///////////////////////////

    /**
     * Show the secret dev. page where the user can change the environment.
     */
    public void showSecretPage() {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        showContentOverlay();
        showContentFragment(new SecretFragment());
    }

    public void showSecretPlayerPage() {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        showContentOverlay();
        showContentFragment(new SecretPlayerFragment());
    }

    /**
     * Open global or local search based on feature flag.
     */
    public void openSearch() {
        if (Components.get(FeatureControl.class).hasSearch()) {
            openLocalSearch();
        } else {
            openGlobalSearch();
        }
    }

    /**
     * Open local search
     */
    private void openLocalSearch() {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        SearchFragment searchFragment = new SearchFragment();
        showContentFragment(searchFragment);
    }

    /**
     * Open the global google search activity.
     */
    private void openGlobalSearch() {
        try {
            final Intent intent = new Intent(Intent.ACTION_ASSIST)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
            intent.putExtra(EXTRA_SEARCH_TYPE, SEARCH_TYPE_KEYBOARD);
            mActivity.startActivity(intent);
        } catch (Exception e) {
            Log.e(TAG, "Could not open the global search.", e);
        }
    }

    /**
     * Open the android tv settings.
     *
     * @param restore - if true the previous state of the activity will be restored.
     */
    public void openAndroidSettings(boolean restore) {
        try {
            Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
            if (restore) {
                intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            }
            mActivity.startActivity(intent);
        } catch (Exception e) {
            Log.e(TAG, "Could not open the android settings.", e);
        }
    }

    /**
     * Open standalone android tv notifications app.
     */
    public void openAndroidNotifications() {
        try {
            mActivity.startActivity(new Intent(NOTIFICATION_APP_ACTION));
        } catch (Exception e) {
            Log.e(TAG, "Could not open android notifications.", e);
            openAndroidNotificationSidePanel();
        }
    }

    /**
     * Open android tv notification panel.
     */
    private void openAndroidNotificationSidePanel() {
        try {
            Intent intent = new Intent();
            intent.setComponent(new ComponentName(NOTIFICATION_PANEL_PACKAGE_NAME, NOTIFICATION_PANEL_ACTIVITY));
            mActivity.startActivity(intent);
        } catch (Exception e) {
            Log.e(TAG, "Could not open android notifications.", e);
        }
    }

    /**
     * Displays the player settings dialog, showing the available subtitle & audio tracks
     */
    public void showPlayerOptionsDialog() {
        if (getContentFragment() instanceof BasePlayerFragment || !isContentOverlayDisplayed()) {
            // we need at least 2 audio tracks to choose from
            ArrayList<MediaTrackInfo> audioTrackList = mPlaybackDetailsManager.getAvailableAudioTracks(2);

            // we need at least one subtitle track, because we'll also have an off option.
            ArrayList<MediaTrackInfo> subtitleTrackList = mPlaybackDetailsManager.getAvailableSubtitlesTracks(1);

            //only show the player settings if we have the minimum valid number of audio | subtitles tracks
            if (subtitleTrackList.isEmpty() && audioTrackList.isEmpty()) {
                return;
            }

            PlaybackDetails details = mPlaybackDetailsManager.getExclusiveDetails();
            MediaTrackInfo currentTvSubtitleTrack = details.getCurrentSubtitleTrack();
            MediaTrackInfo currentAudioTrack = details.getCurrentAudioTrack();


            String currentSubtitleId = currentTvSubtitleTrack != null ? currentTvSubtitleTrack.getId() : TranslationKey.SCREEN_SUBTITLE_OFF;
            String currentAudioTrackId = currentAudioTrack != null ? currentAudioTrack.getId() : null;

            BaseSettingsDialogFragment playerSettingsDialogFragment =
                    PlayerSettingsDialogFragment.newInstance(subtitleTrackList, audioTrackList, currentSubtitleId, currentAudioTrackId);
            showDialogFragment(playerSettingsDialogFragment);
        }
    }

    /**
     * Cancel the fade out animator before showing the player.
     */
    public void cancelFadeOutAnimator() {
        if (AnimatorUtils.isRunning(mContentFadeOutAnimator)) {
            Log.d(TAG, "Cancel fade out animator.");
            AnimatorUtils.cancelAnimators(mContentFadeOutAnimator);
        }
    }

    /**
     * Show player ui for the currently playing asset.
     */
    public void showPlayerUI() {
        showPlayerUI(StartAction.Undefined, null);
    }

    /**
     * in case we need to pass to the player the key event, use this function
     *
     * @param event which triggered the show player
     */
    public void showPlayerUI(StartAction action, KeyEvent event) {
        if (mActivity.isInstanceStateSaved() || mPlaybackDetailsManager == null
                || mPlaybackDetailsManager.isRegionalChannelPlaying()) {
            return;
        }
        boolean isFavorite = mPlaybackDetailsManager.isFavorite();
        String channelId = mPlaybackDetailsManager.getChannelId();

        switch (mPlaybackDetailsManager.getPlayerType()) {
            case LiveTvIpTv:
            case LiveTvOtt:
                showLivePlayer(action, channelId, isFavorite, event, false);
                break;

            case Replay:
                IBroadcast replayBroadcast = mPlaybackDetailsManager.getBroadcastPlayerData();
                showReplayPlayer(action, replayBroadcast, event, false, isFavorite);
                break;

            case Recording:
                IRecording pvrBroadcast = mPlaybackDetailsManager.getRecordingPlayerData();
                showRecordingPlayer(action, pvrBroadcast, event, false);
                break;

            case Vod:
                IVodItem vod = mPlaybackDetailsManager.getVodPlayerData();
                IVodVariant vodVariant = mPlaybackDetailsManager.getVodVariantPlayerData();
                IVodPrice price = mPlaybackDetailsManager.getVodPricePlayerData();
                showVodPlayer(action, vodVariant, vod, null, price, event, false);
                break;

            case VodTrailer:
                IVodItem trailerVod = mPlaybackDetailsManager.getVodPlayerData();
                showTrailerPlayer(action, mPlaybackDetailsManager.getVodVariantPlayerData(), trailerVod, event, false);
                break;

            case Gdpr:
                IVodItem gdprVod = mPlaybackDetailsManager.getGdprPlayerData();
                IVodVariant gdprVodVariant = mPlaybackDetailsManager.getGdprVariantPlayerData();
                IVodPrice gdprVodPrice = mPlaybackDetailsManager.getGdprVodPrice();
                showGdprPlayer(action, gdprVod, gdprVodPrice, gdprVodVariant);
                break;

            case RadioIpTv:
            case RadioOtt:
                showRadioPlayer(action, channelId, isFavorite, event, false);
                break;
        }

        BaseFragment contentFragment = getContentFragment();
        if (contentFragment instanceof BasePlayerFragment) {
            PlaybackDetails details = mPlaybackDetailsManager.getExclusiveDetails();
            ((BasePlayerFragment) contentFragment).showUnskippableUI(details, null);
        }

    }

    /**
     * Shows a new binge watching dialog with the next episode.
     */
    public void showBingeWatching(BingeWatchingInfo<IBaseSeries, IContentItem> nextEpisode) {
        showDialog(BingeWatchingDialog.newInstance(nextEpisode));
    }

    /**
     * shows a new detail page according to the player type, also pops back-stack
     */
    public void showNewDetailPageByPlayer() {
        if (mPlaybackDetailsManager == null) {
            return;
        }

        IContentItem contentItem = mPlaybackDetailsManager.getPlayingItem();
        if (mParentalControlManager.isRestricted(contentItem)) {
            // Restricted content. Skipp detail page.
            goBack();
            return;
        }

        getActivity().getSupportFragmentManager().popBackStackImmediate();
        switch (mPlaybackDetailsManager.getPlayerType()) {
            case Vod:
            case VodTrailer:
                showVodMovieDetails(mPlaybackDetailsManager.getVodPlayerData());
                break;
            case Recording:
                showRecordingOrProgramDetailPageByPlayerData();
                break;
            case Replay:
                showProgramDetails(mPlaybackDetailsManager.getReplayPlayerData(), DetailOpenedFrom.Player);
                break;
        }
    }

    /**
     * gets @{@link IRecordingSeries} from program data and opens detail page according
     * to the @{@link PlaybackDetailsManager#getRecordingPlayerData()}
     * if we have and the number of episode is not just one then show program else recording detail
     * page
     */
    private void showRecordingOrProgramDetailPageByPlayerData() {
        if (mPlaybackDetailsManager != null
                && mPlaybackDetailsManager.getRecordingPlayerData() != null) {

            // TODO : check this we request the series twice.
            RxUtils.disposeSilently(mSeriesRecordingsDisposable);
            IBroadcast data = mPlaybackDetailsManager.getRecordingPlayerData();
            mSeriesRecordingsDisposable = mPvrRepository.getSeriesRecording(data.getSeriesId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .firstElement()
                    .subscribe(seriesRecording -> {
                        if (seriesRecording != null && seriesRecording.getNumberOfEpisodes() > 1) {
                            showSeriesRecordingDetailPage(seriesRecording.getId(), DetailOpenedFrom.Player);
                        } else {
                            showProgramDetails(data, DetailOpenedFrom.Player);
                        }
                    }, throwable -> showProgramDetails(data, DetailOpenedFrom.Player));
        }
    }

    /**
     * Show dialog to the user to notify that the system update failed.
     */
    public void showSystemUpdateFailureDialog() {
        final ActionBarDialog dialog = new ActionBarDialog.Builder()
                .title(mTranslator.get(TranslationKey.UPGRADE_FAILED_ALERT_TITLE))
                .description(mTranslator.get(TranslationKey.UPGRADE_FAILED_ALERT_BODY))
                .priority(SystemUpdateOptionalDialog.SYSTEM_UPDATE_DIALOG_PRIORITY)
                .addButton(mTranslator.get(TranslationKey.UPGRADE_FAILED_ALERT_BUTTON_TRY_AGAIN),
                        d -> openSystemUpdateFragment(false))
                .addButton(mTranslator.get(TranslationKey.CANCEL_BUTTON),
                        d -> closeSystemUpdateFragment())
                .cancelable(true)
                .blockKeys(false)
                .dismissOnBtnClick(true)
                .build();
        showDialogOnTop(dialog);
    }

    /**
     * Show system update fragment.
     *
     * @param fromSettings True if the system update page was opened from settings.
     */
    public void openSystemUpdateFragment(boolean fromSettings) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        BaseFragment contentFragment = getContentFragment();
        if (contentFragment instanceof SystemUpdateFragment) {
            ((SystemUpdateFragment) contentFragment).checkForUpdate();
            return;
        }

        clearAllDialogs();
        showContentFragment(SystemUpdateFragment.newInstance(fromSettings));
    }

    /**
     * Close the system update check fragment if it's displayed.
     */
    public void closeSystemUpdateFragment() {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        BaseFragment contentFragment = getContentFragment();
        if (contentFragment instanceof SystemUpdateFragment) {
            ((SystemUpdateFragment) contentFragment).goBack();
        }
    }

    /**
     * handle event actions when activity is paused
     */
    public void onActivityPaused() {
        RxUtils.disposeSilently(mSeriesRecordingsDisposable, mChannelInfoDisposable);
    }

    public void onActivityStopped() {
        AnimatorUtils.cancelAnimators(mContentFadeInAnimator, mContentFadeOutAnimator);

        RxUtils.disposeSilently(mSeriesRecordingsDisposable, mUrlDisposable,
                mIsSubscribedDisposable);
    }

    private final FragmentManager.OnBackStackChangedListener mOnBackStackChangedListener =
            new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    FragmentManager.BackStackEntry currentEntry = getCurrentEntryFromBackStack();
                    notifyBackStackChanged(getContentFragment());

                    Log.d(TAG, "onBackStackChanged. "
                            + "Entry count : " + mActivity.getSupportFragmentManager().getBackStackEntryCount() + " , "
                            + "Current entry :" + (currentEntry != null ? currentEntry.getName() : null));
                }
            };

    public void hideLoadingIndicator() {
        PlayerFragment playerFragment = getPlayerFragment();
        if (playerFragment != null) {
            playerFragment.hideLoadingIndicator();
        }
    }

    /**
     * Method to show the content fragment.
     * <p>
     * The method creates the fragment transaction on the fragment container and does the extra animations and work that is needed for the fragment change.
     *
     * @param contentFragment Instance of {@link BaseFragment} that we want to show.
     */
    protected void showContentFragment(BaseFragment contentFragment) {
        showContentFragment(contentFragment, false);
    }

    /**
     * Method to show the content fragment.
     * <p>
     * The method creates the fragment transaction on the fragment container and does the extra animations and work that is needed for the fragment change.
     *
     * @param contentFragment Instance of {@link BaseFragment} that we want to show.
     * @param shouldHideView  In case of we don't show UI part just adding to stack, this has to be true
     */
    private void showContentFragment(BaseFragment contentFragment, boolean shouldHideView) {
        Log.d(TAG, "Showing content fragment: " + contentFragment.TAG);
        FragmentTransaction fragmentTransaction = mActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_overlay, contentFragment, contentFragment.TAG)
                .addToBackStack(contentFragment.TAG);

        fragmentTransaction.commitAllowingStateLoss();
        if (shouldHideView) {
            hideContentOverlay();
        } else {
            showContentOverlay();
        }
    }

    /**
     * Show a content fragment that is a player, setting also the event action used starting the playback
     */
    private void showContentFragment(StartAction action, BasePlayerFragment fragment, KeyEvent event) {
        showContentFragment(action, fragment, false, event);
    }

    /**
     * Show a content fragment that is a player, setting also the event action used starting the playback
     *
     * @param action         Proper action
     * @param fragment       Fragment to show
     * @param shouldHideView In case of we don't show UI part just adding to stack, this has to be true
     * @param event          event which could trigger the showing action, this can be null
     */
    private void showContentFragment(StartAction action, BasePlayerFragment fragment, boolean shouldHideView, KeyEvent event) {
        if (action != null) {
            Bundle arguments = fragment.getArguments();
            if (arguments == null) {
                arguments = new Bundle();
            }
            arguments.putSerializable(BasePlayerFragment.EXTRA_ARG_ACTION, action);
            arguments.putParcelable(BasePlayerFragment.EXTRA_TRIGGER_KEY_EVENT, event);
            fragment.setArguments(arguments);
        }

        showContentFragment(fragment, shouldHideView);
    }

    /**
     * Open local settings.
     */
    public void openSettings() {
        SettingsFragment settingsFragment = SettingsFragment.newInstance();
        showDialogFragment(settingsFragment);
    }

    /**
     * Open local settings.
     *
     * @param itemType - menu item to be selected
     */
    public void openSettings(MainSettingsItem.MainItemType itemType) {
        showDialogFragment(SettingsFragment.newInstance(itemType));
    }

    /**
     * Open support settings.
     *
     * @param itemType - menu item to be selected
     */
    public void openSupport(SupportItem.Type itemType) {
        openSettings(MainSettingsItem.MainItemType.SUPPORT);
        showDialogFragment(SupportFragment.newInstance(itemType));
    }

    /**
     * Display an action bar with a message which warns the user that it's
     * harmful for the health of babies to watch TV.
     */
    public void showBabyChannelWarning() {
        mActivity.getSupportFragmentManager().executePendingTransactions();
        if (getDialogFragment() instanceof BabyChannelWarningDialog) {
            return;
        }
        showDialog(BabyChannelWarningDialog.newInstance(mTranslator));
    }

    public void hideBabyChannelDialog() {
        DialogFragment dialog = getDialogFragment();
        if (dialog instanceof BabyChannelWarningDialog) {
            dialog.dismiss();
        }
    }

    /**
     * Display an action bar which warns the user that fast-forward is not permitted on this account.
     */
    public void showReplayPlusOfferDialog() {
        mActivity.getSupportFragmentManager().executePendingTransactions();
        if (getDialogFragment() instanceof ReplayPlusOfferDialog) {
            return;
        }
        showDialog(ReplayPlusOfferDialog.newInstance(mTranslator));
    }

    /**
     * Show line number page for device registration.
     */
    public void showLineNumberFragment() {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        showContentOverlay();
        showContentFragment(LineNumberFragment.newInstance());
    }

    /**
     * Display confirmation dialog for VOD rent.
     */
    public void showRentConfirmationDialog(IVodItem vod) {
        RentConfirmationDialog rentConfirmationDialog =
                RentConfirmationDialog.newInstance(vod);
        showDialog(rentConfirmationDialog);
    }

    /**
     * Display purchase pin dialog.
     */
    public void showPurchasePinDialog(IVodItem vod, IVodVariant variant, IRentOffer offer, IVodPrice price) {
        PinDialog pinDialog = new PinDialog.Builder()
                .setTitle(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_TITLE))
                .setReenterTitle(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_RE_ENTER_TITLE))
                .setPurchasePinChangedTitle(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_PIN_CHANGED_TITLE))
                .setBlockedTitle(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_BLOCKED_TITLE))
                .setBody(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_BODY))
                .setReenterBody(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_RE_ENTER_BODY))
                .setBlockedBody(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_BLOCKED_BODY))
                .setPurchasePinChangedBody(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_PIN_CHANGED_BODY))
                .setReenterHint(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_RE_ENTER_HINT))
                .setBlockedHint(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_BLOCKED_HINT))
                .setPurchasePinChangedHint(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_PIN_CHANGED_HINT))
                .setPinAttemptsSetting(Settings.purchasePinAttemptsNumber)
                .setPinFailedTimestampSetting(Settings.purchasePinFailedTimestamp)
                .setPinValidator(pin -> Single.create(emitter -> {
                    try {
                        Components.get(VodRepository.class).rentVodItem(vod, variant, price, pin);
                        emitter.onSuccess(true);
                    } catch (PinChangedException e) {
                        Log.e(TAG, "Rent failed, purchase pin was recently changed: " + e);
                        throw e;
                    } catch (InvalidPinException e) {
                        Log.e(TAG, "Rent failed, pin is not valid: " + e);
                        throw e;
                    } catch (BackendException e) {
                        Log.e(TAG, "Rent failed: " + e);
                        clearAllDialogs();
                        mNotificationManager.showRentFailedNotification();
                    }
                }))
                .setOnPinSuccessCallback((String pin) -> {
                    //Shows Rent Notification or Age Rating Notification.
                    if (!vod.getParentalRating().isRestricted()) {
                        mNotificationManager.showRentNotification(vod, variant);
                    }
                    showVodPlayer(StartAction.Rent, variant, vod, price);
                    clearAllDialogs();

                })
                .setEvent(ReportUtils.createEvent(UILog.Page.UnlockPageVodPurchase, vod))
                .build();
        showDialog(pinDialog);
    }

    /**
     * Show the parental control PIN dialog which temporally unblock the parental control in the app.
     *
     * @param callback Callback to execute once the pin had been entered correctly.
     * @param listener Listener to react to dialog events e.g. start, dismiss, ...
     */
    public void showParentalControlUnblockDialog(BasePinDialog.OnPinSuccessCallback callback,
                                                 BaseDialogFragment.DialogListener listener) {
        if (isPinDialogDisplayed() && callback == null) {
            // Pin dialog already shown.
            return;
        }

        PinDialog dialog = showParentalControlPINDialog(enteredPin -> {
            mParentalControlManager.unblockContent();
            mNotificationManager.showNotification(new Notification.Builder()
                    .title(mTranslator.get(TranslationKey.NOTIFICATION_PARENTAL_CONTROL_TEMPORARY_UNLOCK))
                    .build());

            if (callback != null) {
                callback.onSuccess(enteredPin);
            }
        });

        if (listener != null) {
            dialog.setDialogListener(listener);
        }
    }

    public void showParentalControlUnblockDialog(BasePinDialog.OnPinSuccessCallback callback) {
        showParentalControlUnblockDialog(callback, null);
    }

    public void showParentalControlUnblockDialog(BaseDialogFragment.DialogListener listener) {
        showParentalControlUnblockDialog(null, listener);
    }

    public void showParentalControlUnblockDialog() {
        showParentalControlUnblockDialog(null, null);
    }

    /**
     * Opens Pin dialog when user clicks on Parental Control & PIN from Settings.
     *
     * @return THe dialog which will be displayed.
     */
    public PinDialog showParentalControlPINDialog(BasePinDialog.OnPinSuccessCallback successCallback) {
        PinDialog pinDialog = new PinDialog.Builder()
                .setTitle(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_TITLE))
                .setReenterTitle(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_RE_ENTER_TITLE))
                .setBlockedTitle(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_BLOCKED_TITLE))
                .setReenterBody(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_RE_ENTER_TITLE))
                .setBody(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_BODY))
                .setReenterBody(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_RE_ENTER_BODY))
                .setBlockedBody(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_BLOCKED_BODY))
                .setReenterHint(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_RE_ENTER_HINT))
                .setBlockedHint(mTranslator.get(TranslationKey.SYSTEM_PIN_ENTRY_SCREEN_BLOCKED_HINT))
                .setPinAttemptsSetting(Settings.systemPinAttemptsNumber)
                .setPinFailedTimestampSetting(Settings.systemPinFailedTimestamp)
                .setPinValidator(
                        pin -> Single.create(
                                emitter -> {
                                    String cachedPin = Settings.systemPIN.get(null);
                                    if (cachedPin == null) {
                                        throw new NullPointerException("Unable to verify PIN");
                                    }

                                    emitter.onSuccess(Objects.equals(cachedPin, pin));
                                }
                        ))
                .setOnPinSuccessCallback(successCallback)
                .setEvent(new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                        .addDetail(UILogEvent.Detail.Page, UILog.Page.PinCodeManagement))
                .build();
        showDialogOnTop(pinDialog);

        return pinDialog;
    }

    /**
     * @return True if Pin dialog is displayed, otherwise false.
     */
    public boolean isPinDialogDisplayed() {
        return getDialogFragment() instanceof BasePinDialog;
    }

    /**
     * Opens Change Pin dialog.
     */
    public void showChangePINCode() {
        ChangePinDialog changePinDialog = new ChangePinDialog.Builder()
                .setTitle(mTranslator.get(TranslationKey.CHANGE_SYSTEM_PIN_SCREEN_TITLE))
                .setReenterTitle(mTranslator.get(TranslationKey.CHANGE_SYSTEM_PIN_SCREEN_RE_ENTER_TITLE))
                .setBody(mTranslator.get(TranslationKey.CHANGE_SYSTEM_PIN_SCREEN_BODY))
                .setReenterBody(mTranslator.get(TranslationKey.CHANGE_SYSTEM_PIN_SCREEN_RE_ENTER_BODY))
                .setMissMatchHint(mTranslator.get(TranslationKey.CHANGE_SYSTEM_PIN_SCREEN_HINT))
                .setErrorHint(mTranslator.get(TranslationKey.CHANGE_PIN_SCREEN_ERROR_HINT))
                .setPinValidator(mAccountRepository::changeParentalControlPIN)
                .build();
        showDialogOnTop(changePinDialog);
    }

    /**
     * Opens Change Purchase Pin dialog.
     */
    public void showChangePurchasePIN() {
        ChangePinDialog changePurchaseDialog = new ChangePinDialog.Builder()
                .setTitle(mTranslator.get(TranslationKey.CHANGE_PURCHASE_PIN_SCREEN_TITLE))
                .setReenterTitle(mTranslator.get(TranslationKey.CHANGE_PURCHASE_PIN_SCREEN_RE_ENTER_TITLE))
                .setBody(mTranslator.get(TranslationKey.CHANGE_PURCHASE_PIN_SCREEN_BODY))
                .setReenterBody(mTranslator.get(TranslationKey.CHANGE_PURCHASE_PIN_SCREEN_RE_ENTER_BODY))
                .setMissMatchHint(mTranslator.get(TranslationKey.CHANGE_PURCHASE_PIN_SCREEN_HINT))
                .setErrorHint(mTranslator.get(TranslationKey.CHANGE_PIN_SCREEN_ERROR_HINT))
                .setPinValidator(mAccountRepository::changePurchasePIN)
                .build();
        showDialogOnTop(changePurchaseDialog);
    }

    /**
     * Shows Enter purchase screen dialog.
     */
    public void showPurchasePin() {
        PinDialog purchasePinDialog = new PinDialog.Builder()
                .setTitle(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_TITLE))
                .setReenterTitle(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_RE_ENTER_TITLE))
                .setPurchasePinChangedTitle(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_PIN_CHANGED_TITLE))
                .setBlockedTitle(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_BLOCKED_TITLE))
                .setBody(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_BODY))
                .setReenterBody(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_RE_ENTER_BODY))
                .setBlockedBody(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_BLOCKED_BODY))
                .setPurchasePinChangedBody(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_PIN_CHANGED_BODY))
                .setReenterHint(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_RE_ENTER_HINT))
                .setBlockedHint(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_BLOCKED_HINT))
                .setPurchasePinChangedHint(mTranslator.get(TranslationKey.PURCHASE_PIN_ENTRY_SCREEN_PIN_CHANGED_HINT))
                .setPinAttemptsSetting(Settings.purchasePinAttemptsNumber)
                .setPinFailedTimestampSetting(Settings.purchasePinFailedTimestamp)
                .setPinValidator(
                        pin -> Single.create(emitter -> {
                            String cachedPin = Settings.purchasePIN.get(null);
                            if (cachedPin == null) {
                                throw new NullPointerException("Unable to verify PIN");
                            }

                            emitter.onSuccess(Objects.equals(cachedPin, pin));
                        }))
                .setOnPinSuccessCallback(pin -> showChangePurchasePIN())
                .build();
        showDialogOnTop(purchasePinDialog);
    }

    /**
     * Open the proximus recordings settings.
     */
    public void openRecordingsSettings() {
        RecordingsSettingsFragment recordingsSettingsFragment = RecordingsSettingsFragment.newInstance();
        showDialogOnTop(recordingsSettingsFragment);
    }

    /**
     * Display a dialog with all available option for playback.
     *
     * @param playOptionList the playback option list which will be displayed on the dialog.
     * @param contentItem    the content which will be reported
     */
    public void showPlayOptionsDialog(List<? extends PlayOption> playOptionList, IContentItem contentItem) {
        showPlayOptionsDialog(playOptionList, false, false, contentItem);
    }

    /**
     * Display a dialog with all available option for playback.
     *
     * @param playOptionList  the playback option list which will be displayed on the dialog.
     * @param isFromFavourite Flag used to know if the channel id is from favourite channels list.
     * @param contentItem     the content which will be reported
     */
    public void showPlayOptionsDialog(List<? extends PlayOption> playOptionList, boolean isFromFavourite, IContentItem contentItem) {
        showPlayOptionsDialog(playOptionList, false, isFromFavourite, contentItem);
    }

    public void showPlayOptionsDialog(List<? extends PlayOption> playOptionList, boolean continueWatch, boolean isFromFavourite, IContentItem contentItem) {
        PlayOptionsDialog playOptionsDialog =
                PlayOptionsDialog.newInstance(playOptionList, continueWatch, isFromFavourite, contentItem);
        showDialog(playOptionsDialog);
    }

    /**
     * Opens system information settings.
     */
    public void openSystemInformationSettings() {
        SystemInformationFragment systemInformationFragment = SystemInformationFragment.newInstance();
        showDialogOnTop(systemInformationFragment);
    }

    /**
     * Opens Support settings.
     */
    public void openSupportSettings() {
        SupportFragment systemInformationFragment = SupportFragment.newInstance();
        showDialogOnTop(systemInformationFragment);
    }

    /**
     * Opens hints and tips page
     */
    public void openApplicationHintPage() {
        ApplicationHintsFragment applicationHintsFragment = ApplicationHintsFragment.newInstance();
        showDialogOnTop(applicationHintsFragment);
    }

    /**
     * Opens system information settings.
     */
    public void openLanguagePreferencesSettings() {
        LanguagePreferencesFragment languagePreferencesFragment = LanguagePreferencesFragment.newInstance();
        showDialogOnTop(languagePreferencesFragment);
    }

    /**
     * Opens parental control fragment.
     */
    public void openParentalControlSettings() {
        ParentalControlSettingsFragment parentalControlAndPINFragment = ParentalControlSettingsFragment.newInstance();
        showDialogOnTop(parentalControlAndPINFragment);
    }

    /**
     * Opens lock content fragment with age rating options.
     */
    public void openLockContentSettings() {
        LockContentFragment lockContentFragment = LockContentFragment.newInstance();
        showDialogOnTop(lockContentFragment);
    }

    /**
     * Opens epg layout fragment with the available types.
     */
    public void openEpgLayoutSettings() {
        EpgLayoutFragment epgSettingsLayoutFragment = new EpgLayoutFragment();
        showDialogOnTop(epgSettingsLayoutFragment);
    }

    /**
     * Opens vod purchase history fragment.
     */
    public void openVodPurchaseHistorySettings() {
        VodPurchaseHistoryFragment vodPurchaseHistoryFragment = new VodPurchaseHistoryFragment();
        showDialogOnTop(vodPurchaseHistoryFragment);
    }

    public void showWebView(String url, boolean hideContentOverlayAfterClose) {
        showWebView(url, hideContentOverlayAfterClose, null);
    }

    public void showWebView(String url, boolean hideContentOverlayAfterClose, String configId) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        WebViewFragment webViewFragment = WebViewFragment.newInstance(url, configId, hideContentOverlayAfterClose);
        showContentFragment(webViewFragment);
    }

    /**
     * Opens the given URL in a web view.
     */
    public void openDeepLink(String url) {
        openDeepLink(url, null);
    }

    public void openDeepLink(String url, String configId) {
        RxUtils.disposeSilently(mUrlDisposable);
        mUrlDisposable = mAccountRepository.buildURL(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(formattedUrl -> {
                            Log.d(TAG, "Formatted URL: " + formattedUrl);
                            showWebView(formattedUrl, false, configId);
                        },
                        throwable -> Log.d(TAG, "Error getting parameters. " + throwable));
    }

    /**
     * Opens the given URL in a web view.
     */
    public void openDeepLinkForChannel(String url, TvChannel channel) {
        RxUtils.disposeSilently(mUrlDisposable);
        mUrlDisposable = mAccountRepository.buildURL(url, channel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(formattedUrl -> {
                            Log.d(TAG, "Formatted URL: " + formattedUrl);
                            showWebView(formattedUrl, true);
                        },
                        throwable -> Log.d(TAG, "Error getting parameters. " + throwable));
    }

    /**
     * Display audio and subtitles preferences
     */
    public void showAudioAndSubtitlesPreferences() {
        LanguagePreferencesSubscreensFragment fragment = new LanguagePreferencesSubscreensFragment()
                .newInstance(LanguagePreferencesSubscreensFragment.SubScreen.AUDIO_SUBTITLES);
        showDialogOnTop(fragment);
    }

    /**
     * Display device language settings preferences
     */
    public void showUILanguagePreferences() {
        Intent intent = new Intent(android.provider.Settings.ACTION_LOCALE_SETTINGS);
        mActivity.startActivity(intent);
    }

    /**
     * Display language shop preferences
     */
    public void showLanguageShopPreferences() {
        LanguagePreferencesSubscreensFragment fragment = new LanguagePreferencesSubscreensFragment()
                .newInstance(LanguagePreferencesSubscreensFragment.SubScreen.SHOP_LANGUAGE);
        showDialogOnTop(fragment);
    }

    public void openPrivacySettings(IConsent consentDisplayed) {
        showDialogOnTop(PrivacyPreferencesFragment.newInstance(consentDisplayed));
    }

    public void openPrivacySettings() {
        openPrivacySettings(null);
    }

    public void openPrivacyInfoFragment() {
        showContentFragment(PrivacyInfoFragment.newInstance());
        clearAllDialogs();
    }

    /**
     * Shows a consent fragment with one or a sequence of consents.
     *
     * @param stepCallback Callback for the startup flow manager.
     * @param consents     List of consents to be shown.
     */
    public void showConsents(StepCallback stepCallback, IConsent... consents) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }
        showContentFragment(ConsentFragment.newInstance(stepCallback, consents));
    }

    /**
     * Show the More privacy settings fragment, which is a part of the FTI flow.
     *
     * @param consent      Consent to be shown.
     * @param stepCallback Callback for the startup flow manager;
     */
    public void showConsentsMoreInfoFragment(IConsent consent, StepCallback stepCallback) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }
        showContentFragment(ConsentMoreInfoFragment.newInstance(consent, stepCallback));
    }

    /**
     * Shows the enter isolation mode dialog with 'Ok' option.
     */
    public void showEnterIsolationModeDialog() {
        showDialogOnTop(new AlertDialog.Builder()
                .title(mTranslator.get(TranslationKey.BE_UNAVAILABLE_ALERT_HEADER))
                .description(mTranslator.get(TranslationKey.BE_UNAVAILABLE_ALERT_BODY))
                .addButton(mTranslator.get(TranslationKey.OK_BUTTON), BaseDialogFragment::dismiss)
                .build()
        );
    }

    /**
     * Shows the exit isolation mode dialog with 'Ok' option.
     */
    public void showLeaveIsolationModeDialog() {
        showDialogOnTop(new AlertDialog.Builder()
                .title(mTranslator.get(TranslationKey.BE_RESTORED_ALERT_HEADER))
                .description(mTranslator.get(TranslationKey.BE_RESTORED_ALERT_BODY))
                .addButton(mTranslator.get(TranslationKey.OK_BUTTON), d -> {
                    mStartupFlow.startStartupFlow(FlowTypes.START_UP);
                })
                .build()
        );
    }


    /**
     * Shows the offline mode dialog with 'Retry' and 'Skip' options.
     */
    public void showNoInternetAlertDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder()
                .title(mTranslator.get(TranslationKey.ERR_A001_NO_INTERNET))
                .description(mTranslator.get(TranslationKey.ERR_A001_NO_INTERNET_DESC))
                .errorCode(ErrorCode.ERROR_CODE_NO_CONNECTION)
                .priority(BaseDialogFragment.OFFLINE_MODE_DIALOG_PRIORITY)
                .dismissOnBtnClick(true)
                .cancelable(false)
                .addButton(mTranslator.get(TranslationKey.ERR_A001_NO_INTERNET_BUTTON),
                        dialog -> mStartupFlow.startStartupFlow())
                .addButton(mTranslator.get(TranslationKey.ERR_A001_NO_INTERNET_BUTTON_USE_OFFLINE), dialog -> {
                    Log.event(new Error.Builder(ErrorType.INTERNET_OFFLINE_MODE).build());
                    mStartupFlow.startStartupFlow(FlowTypes.OFFLINE);
                })
                .build();

        alertDialog.setAllowEnterTransitionOverlap(false);
        alertDialog.setAllowReturnTransitionOverlap(false);
        showDialog(alertDialog);
    }

    /**
     * Shows backend not reachable dialog dialog with 'Retry' and 'Offline' options.
     */
    public void showNoBackendAlertDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder()
                .title(mTranslator.get(TranslationKey.BACK_END_FAIL_ERROR_TITLE))
                .description(mTranslator.get(TranslationKey.BACK_END_FAIL_ERROR_BODY))
                .priority(BaseDialogFragment.OFFLINE_MODE_DIALOG_PRIORITY)
                .dismissOnBtnClick(true)
                .cancelable(false)
                .addButton(mTranslator.get(TranslationKey.TRY_AGAIN_BUTTON),
                        dialog -> mStartupFlow.startStartupFlow())
                .addButton(mTranslator.get(TranslationKey.USE_OFFLINE_BUTTON),
                        dialog -> mStartupFlow.startStartupFlow(FlowTypes.OFFLINE))
                .build();
        alertDialog.setAllowEnterTransitionOverlap(false);
        alertDialog.setAllowReturnTransitionOverlap(false);
        showDialog(alertDialog);
    }

    /**
     * Shows error dialog when settings could not be persisted on the backend with 'Ok' option.
     */
    public void showFailedToSaveSettingsDialog() {
        showDialogOnTop(new AlertDialog.Builder()
                .title(mTranslator.get(TranslationKey.ERR_BE_UNAVAILABLE_SETTING_CHANGE_TITLE))
                .description(mTranslator.get(TranslationKey.ERR_BE_UNAVAILABLE_SETTING_CHANGE_BODY))
                .addButton(mTranslator.get(TranslationKey.OK_BUTTON), BaseDialogFragment::dismiss)
                .build()
        );
    }

    /**
     * Shows error message dialog with OK option when an error happened on backend.
     */
    public void showGenericErrorMessageDialog() {
        showDialogOnTop(new AlertDialog.Builder()
                .title(mTranslator.get(TranslationKey.ERR_CONNECTIVITY_RENT_TITLE))
                .description(mTranslator.get(TranslationKey.ERR_CONNECTIVITY_RENT_BODY))
                .addButton(mTranslator.get(TranslationKey.OK_BUTTON), BaseDialogFragment::dismiss)
                .build());
    }

    /**
     * Displays an alert dialog to the user stating that the recording is not playable
     */
    public void showSeekReachedEnd() {
        if (mPlaybackDetailsManager == null || mPlaybackDetailsManager.getLivePlayerData() == null) {
            return;
        }

        long playbackStartTime = mPlaybackDetailsManager.getLastStartInitiatedTime(System.currentTimeMillis());
        IBroadcast broadcast = mPlaybackDetailsManager.getLivePlayerData();
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        boolean canReplay = broadcast.canStartOver(channel, playbackStartTime);
        String title = canReplay ? mTranslator.get(TranslationKey.TIMESHIFT_NOTIFICATION_TITLE) : mTranslator.get(TranslationKey.TIMESHIFT_NOTIFICATION_NON_REPLAY_STREAM_TITLE);
        String text = canReplay ? mTranslator.get(TranslationKey.TIMESHIFT_NOTIFICATION_BODY) : mTranslator.get(TranslationKey.TIMESHIFT_NOTIFICATION_NON_REPLAY_STREAM_BODY);
        mNotificationManager.showNotification(new Notification.Builder()
                .skippDisplayQueue(true)
                .title(title)
                .subtext(text)
                .build());
    }

    /**
     * Displays an alert dialog to notify the user the next/previous program is restricted.
     *
     * @param parentalRating Parental age rating of the next/previous program.
     */
    public void showParentalControlNotification(ParentalRating parentalRating) {
        int icon = UiUtils.getParentalRatingIcon(parentalRating);

        mNotificationManager.showNotification(new SingleNotification.Builder()
                .iconId(icon)
                .title(mTranslator.get(TranslationKey.REWIND_PARENTAL_LOCKED_NOTIFICATION_TITLE))
                .subtext(mTranslator.get(TranslationKey.REWIND_PARENTAL_LOCKED_NOTIFICATION_BODY))
                .build());
    }

    /**
     * Displays the parental Control notification when the user jump in a content which is blocked.
     *
     * @param broadcast the broadcast where the user wants to jump.
     *                  In case of {@link tv.threess.threeready.player.contract.SeekDirection#REWIND}
     *                  skip the lock.
     */
    public void showParentalControlNotification(IBroadcast broadcast) {
        PlayerFragment playerFragment = getPlayerFragment();
        if (mParentalControlManager.isRestricted(broadcast) && playerFragment != null && !playerFragment.isPlaybackLocked()) {
            showParentalControlNotification(broadcast.getParentalRating());
        }
    }

    /**
     * Displays an alert dialog to notify the user that from Replay will go to Live stream.
     */
    public void showBackToLiveAlert(ButtonClickListener buttonClickListener) {
        showDialog(ReplayBackToLiveDialog.newInstance(mTranslator, buttonClickListener));
    }

    /**
     * Displays a dialog with the different sort and filter options.
     */
    public void showSelectorOptionChangeDialog(DataSourceSelector selector,
                                               SelectorOptionDialog.OnGridItemClickListener listener) {
        if (selector == null || selector.getOptions().isEmpty()) {
            return;
        }
        SelectorOptionDialog sortOptionChangeDialog = SelectorOptionDialog.newInstance(selector, listener);
        showDialog(sortOptionChangeDialog);
    }

    /**
     * Register a listener which will be notified when the content frame visibility changes.
     * To avoid memory leaks each registered listener should be removed. {@link #removeContentChangeListener(IContentChangeListener)}
     *
     * @param listener The listener which will be registered.
     */
    public void addContentChangeListener(IContentChangeListener listener) {
        mContentChangeListeners.add(listener);
    }

    /**
     * Remove a previously registered listener. {@link #addContentChangeListener(IContentChangeListener)}
     *
     * @param listener The listener which needs to be removed.
     */
    public void removeContentChangeListener(IContentChangeListener listener) {
        mContentChangeListeners.remove(listener);
    }

    /**
     * Notify all the active listeners about the backstack change.
     *
     * @param fragment The currently visible fragment.
     */
    private void notifyBackStackChanged(BaseFragment fragment) {
        for (IContentChangeListener listener : mContentChangeListeners) {
            listener.onBackStackChanged(fragment);
        }
    }

    /**
     * Notify all the active listeners about the content frame shown.
     *
     * @param fragment The currently visible fragment.
     */
    private void notifyContentShown(BaseFragment fragment) {
        for (IContentChangeListener listener : mContentChangeListeners) {
            listener.onContentShown(fragment);
        }
    }

    /**
     * Notify all the active listeners about the content hide.
     *
     * @param fragment The currently active content fragment.
     */
    private void notifyContentHide(BaseFragment fragment) {
        for (IContentChangeListener listener : mContentChangeListeners) {
            listener.onContentHide(fragment);
        }
    }

    /**
     * delegated method to the activity
     */
    public boolean isInBackground() {
        return mActivity.isInBackground();
    }

    public void showChannelScanDialog(boolean userTriggered) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        if (getContentFragment() instanceof ChannelScanFragment) {
            // Nothing to do. Already displayed.
            return;
        }

        clearAllDialogs();
        showContentFragment(ChannelScanFragment.newInstance(userTriggered));
    }

    /**
     * Shows My videos page.
     */
    public void showMyVideos() {
        if (mActivity.isInstanceStateSaved() || mPlaybackDetailsManager == null) {
            return;
        }
        HomeFragment homeFragment;

        if (getContentFragment() instanceof HomeFragment) {
            homeFragment = (HomeFragment) getContentFragment();
            homeFragment.showMyVideos();
            return;
        }
        //Add Home fragment.
        clearBackStack();
        homeFragment = HomeFragment.newInstanceMyVideo();
        showContentFragment(homeFragment);
    }

    /**
     * Show the save energy settings fragment, which is a part of the FTI flow.
     *
     * @param callback Callback for the startup flow manager.
     */
    public void showSaveEnergySettingsFragment(StepCallback callback) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }
        showContentFragment(SaveEnergySettingsFragment.newInstance(callback));
    }

    /**
     * Show the parental control page of the application.
     */
    public void showParentalControl(StepCallback callback, ParentalRating parentalRating) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        showContentOverlay();
        showContentFragment(ParentalControlSetupFragment.newInstance(callback, parentalRating));
    }

    /**
     * Pop back currently displayed startup page.
     */
    public void popStartFragment() {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        Fragment contentFragment = getContentFragment();
        if (contentFragment instanceof StartFragment) {
            mActivity.getSupportFragmentManager().popBackStack();
        }
    }

    /**
     * Show the secret WebView fragment.
     */
    public void showSecretWebViewFragment(String url) {
        if (mActivity.isInstanceStateSaved()) {
            return;
        }

        showContentFragment(SecretWebViewFragment.newInstance(url));
    }

    /**
     * Listener to get notification about the content frame visibility changes.
     */
    public interface IContentChangeListener {
        /**
         * Called when the active fragment in the content frame changes.
         * This can be triggered by adding a new fragment or pressing back.
         *
         * @param fragment The currently active fragment.
         */
        default void onBackStackChanged(BaseFragment fragment) {
        }

        /**
         * Called when the content frame is shown. (alpha = 1)
         *
         * @param fragment The currently active fragment.
         */
        default void onContentShown(BaseFragment fragment) {
        }

        /**
         * Called when the content frame is hidden (alpha = 0)
         *
         * @param fragment THe currently active fragment.
         */
        default void onContentHide(@Nullable BaseFragment fragment) {
        }

    }
}
