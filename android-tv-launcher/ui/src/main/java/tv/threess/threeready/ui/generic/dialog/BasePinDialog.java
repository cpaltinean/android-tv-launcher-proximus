package tv.threess.threeready.ui.generic.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.LinkedList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.PinDialogBinding;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Abstract base class for Pin dialogs.
 *
 * @author Daniela Toma
 * @since 2019.06.10
 */
public abstract class BasePinDialog extends BaseDialogFragment {

    protected static final int NUMBER_OF_DIGITS = 4;

    protected OnPinSuccessCallback mOnPinSuccessCallback;
    protected OnLoadingCallback mOnLoadingCallback;

    protected String mTitle;
    protected String mReenterTitle;
    protected String mBlockedTitle;
    protected String mBody;
    protected String mReenterBody;
    protected String mBlockedBody;
    protected String mReenterHint;
    protected String mBlockedHint;

    protected Settings mPinAttemptsSetting;
    protected Settings mPinFailedTimestampSetting;

    protected PinValidator mPinValidator;
    protected Disposable mPinValidateDisposable;
    protected long mFailedTimestamp;
    protected long mAttemptsNumber;
    protected boolean mIsValidatingPin;

    protected Event<UILogEvent, UILogEvent.Detail> mEvent;

    protected final Translator mTranslator = Components.get(Translator.class);

    protected PinDialogBinding mBinding;

    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    private List<PinDigitView> mPinDigitViews;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPinDigitViews = new LinkedList<>();

        // callback to check if every digit is entered
        PinDigitView.OnEnterDigitCallback onEnterDigitCallback = this::onValidatePin;

        // create a digits view list
        for (int i = 0; i < NUMBER_OF_DIGITS; i++) {
            // create a digit
            PinDigitView digitView = new PinDigitView(getContext());
            digitView.setOnEnterDigitCallback(onEnterDigitCallback);

            // get a side length in pixels
            int side = (int) getResources().getDimension(R.dimen.pin_dialog_digit_side);

            // check if we need to add a margin from the right of a digit
            int marginRight = 0;
            if (i < NUMBER_OF_DIGITS - 1) {
                marginRight = (int) getResources().getDimension(R.dimen.pin_dialog_digit_margin_right);
            }

            // create and set the layout params to each digit view
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(side, side);
            params.setMargins(0, 0, marginRight, 0);
            digitView.setLayoutParams(params);

            if (i == 0) {
                digitView.setContentDescription(
                        new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_SCREEN_PIN))
                                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_DIALOG_BODY, mBinding != null ? mBinding.body.getText() : "")
                                .toString()
                );
            }

            mPinDigitViews.add(digitView);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mBinding = PinDialogBinding.inflate(inflater, container, false);

        mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        mBinding.body.setTextColor(mLayoutConfig.getFontColor());
        mBinding.hint.setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f));
        mBinding.countdown.setVisibility(View.GONE);

        for (PinDigitView view : mPinDigitViews) {
            mBinding.digitsContainer.addView(view);
        }

        return mBinding.getRoot();
    }

    @Nullable
    public Event<UILogEvent, UILogEvent.Detail> getEvent() {
        return mEvent;
    }

    /**
     * Validates the entered Pin.
     */
    protected abstract void onValidatePin();

    /**
     * Focuses the next digit.
     */
    protected void focusNextDigit() {
        View focusedDigit = mBinding.digitsContainer.findFocus();
        if (focusedDigit != null) {
            View nextDigit = focusedDigit.focusSearch(View.FOCUS_RIGHT);
            if (nextDigit instanceof PinDigitView) {
                nextDigit.requestFocus();
            }
        }
    }

    /**
     * @return true if all digits are entered.
     */
    protected boolean isAllEntered() {
        for (PinDigitView digitView : mPinDigitViews) {
            if (digitView.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return The entered PIN as string
     */
    protected String getEnteredPin() {
        StringBuilder builder = new StringBuilder();
        for (PinDigitView digitView : mPinDigitViews) {
            builder.append(digitView.getDigit());
        }
        return builder.toString();
    }

    protected abstract void setEnterState();

    protected void setReenterState() {
        mBinding.title.setText(mReenterTitle);
        mBinding.body.setText(mReenterBody);
        mBinding.body.setTextColor(mLayoutConfig.getFontColor());
    }

    /**
     * Focus on the first digit in a row.
     */
    protected void focusFirstDigit() {
        mPinDigitViews.get(0).requestFocus();
    }

    /**
     * Reset all digits of the PIN dialog
     */
    protected void resetDigits() {
        for (PinDigitView digitView : mPinDigitViews) {
            digitView.reset();
        }
    }

    protected void showLoading() {
        if (mOnLoadingCallback != null) {
            mOnLoadingCallback.startLoading();
        }
    }

    protected void hideLoading() {
        if (mOnLoadingCallback != null) {
            mOnLoadingCallback.stopLoading();
        }
    }

    /**
     * Callback to detect when the entered PIN is correct.
     */
    public interface OnPinSuccessCallback {

        void onSuccess(String enteredPin);
    }

    /**
     * Callback to detect when to show or hide loading, in case of it is needed.
     */
    public interface OnLoadingCallback {
        void startLoading();

        void stopLoading();
    }

    /**
     * Interface to provide an RX observable for the dialog which can validate the entered PIN.
     */
    public interface PinValidator {

        Single<Boolean> validatePin(String pin);
    }
}
