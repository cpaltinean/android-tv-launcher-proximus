package tv.threess.threeready.ui.generic.hint;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.MiniEpgHintsDialogBinding;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.FontTextView;

/**
 * Dialog for mini epg hints.
 *
 * @author Zsolt Bokor_
 * @since 2020.06.22
 */
public class MiniEpgHintsDialog extends EpgHintsDialog {

    private MiniEpgHintsDialogBinding mBinding;

    public static MiniEpgHintsDialog newInstance(String hintId, boolean hintOpenedFromSettings) {
        Bundle args = new Bundle();
        args.putString(EXTRA_HINT_ID, hintId);
        args.putBoolean(EXTRA_HINT_OPENED_FROM_SETTINGS, hintOpenedFromSettings);
        MiniEpgHintsDialog fragment = new MiniEpgHintsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected View inflateView() {
        if (mBinding == null) {
            mBinding = MiniEpgHintsDialogBinding.inflate(LayoutInflater.from(getContext()));
        }

        return mBinding.getRoot();
    }

    @Override
    protected FontTextView getDescriptionSmall() {
        return mBinding.descriptionSmall;
    }

    @NonNull
    @Override
    protected ImageView getBackgroundImage() {
        return mBinding.background;
    }

    @NonNull
    @Override
    protected TextView getPreviousButton() {
        return mBinding.buttonContainer.previousBtn;
    }

    @NonNull
    @Override
    protected TextView getNextButton() {
        return mBinding.buttonContainer.nextBtn;
    }

    @Override
    protected int getBackgroundImageId() {
        return R.drawable.mini_epg_hints_background;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.description.setText(mTranslator.get(TranslationKey.HINT_TEXT_JUMP_DAY_MINI_EPG));
        view.setContentDescription(mTranslator.get(TranslationKey.HINT_TEXT_JUMP_DAY_MINI_EPG)
                + " " + mTranslator.get(TranslationKey.HINT_REVIEW_ALL_YOUR_TIPS));
    }
}
