package tv.threess.threeready.ui.epggrid;

import android.os.Handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.EpgFetchObservable;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.TvChannelCacheType;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.epggrid.view.EpgTimeLineView;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Class to handle the load, prefetch and storing the EPG data.
 *
 * @author Barabas Attila
 * @since 9/28/21
 */
public class EpgDataLoader implements Disposable, EpgTimeLineView.TimeLineChangeListener {
    private static final String TAG = Log.tag(EpgDataLoader.class);

    private final TvRepository mTvRepository = Components.get(TvRepository.class);

    private final Map<TvChannel, EpgFetchObservable<List<IBroadcast>>> mChannelEpgLoadersMap = new HashMap<>();

    // Disposables.
    private final Map<TvChannel, Disposable> mChannelEpgDisposableMap = new HashMap<>();
    private Disposable mChannelDisposable;

    private final EpgTimeLineView mTimeline;

    // The extra time to prefetch broadcast for outside the visible timeline.
    private long mExtraTimePrefetch = TimeUnit.HOURS.toMillis(8);
    // The number of channels to prefetch next and previous to the selected channel.
    private int mExtraChannelPrefetch = 7;
    // The time to wait after a timeline change before fetching data.
    private long mTimePrefetchDelay = 500;
    // Time time to wait after a selected channel position change before fetching data.
    private long mChannelPrefetchDelay = 500;
    // True if the load should try to prefetch EPG programs for a whole day at once.
    private boolean mOptimizePrefetchForDays = true;

    private final EpgDataLoadedListener mDataLoadedListener;

    private List<EpgChannelRowModel> mChannelRows = new ArrayList<>();

    private long mPrefetchFrom;
    private long mPrefetchTo;

    private int mChannelPrefetchStart;
    private int mChannelPrefetchEnd;

    private final Handler mHandler = new Handler();

    public EpgDataLoader(EpgTimeLineView timeline,
                         EpgDataLoadedListener dataLoadedListener) {
        mTimeline = timeline;
        mDataLoadedListener = dataLoadedListener;
    }

    public EpgDataLoader(EpgTimeLineView timeLine,
                         long extraTimePrefetch, int extraChannelPrefetch,
                         long timePrefetchDelay, long channelPrefetchDelay,
                         boolean optimizePrefetchForDays,
                         EpgDataLoadedListener dataLoadedListener) {
        mTimeline = timeLine;
        mExtraTimePrefetch = extraTimePrefetch;
        mExtraChannelPrefetch = extraChannelPrefetch;
        mTimePrefetchDelay = timePrefetchDelay;
        mChannelPrefetchDelay = channelPrefetchDelay;
        mOptimizePrefetchForDays = optimizePrefetchForDays;
        mDataLoadedListener = dataLoadedListener;
    }

    /**
     * Initiate loading of the EPG channels.
     */
    public void initiate() {
        // Dispose previous.
        dispose();

        // Calculate init prefetch times.
        mPrefetchFrom = calculatePrefetchFrom(mTimeline.getFrom());
        mPrefetchTo = calculatePrefetchTo(mTimeline.getTo());

        // Load channels.
        mChannelDisposable = mTvRepository.getEpgGridChannels()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(channelMap -> {
                    // Dispose previous
                    RxUtils.disposeSilently(mChannelEpgDisposableMap.values());
                    mChannelEpgDisposableMap.clear();
                    mChannelEpgLoadersMap.clear();
                    mChannelPrefetchStart = mChannelPrefetchEnd = 0;

                    mChannelRows = new ArrayList<>();

                    // Favorite channels
                    addChannelRows(channelMap.get(TvChannelCacheType.FAVORITE), true);
                    // All channels
                    addChannelRows(channelMap.get(TvChannelCacheType.ALL), false);

                    for (int i = 0; i < mChannelRows.size(); ++i) {
                        createEpgGridObservers(mChannelRows.get(i));
                    }

                    mDataLoadedListener.onChannelListLoaded(mChannelRows);
                }, (e) -> Log.e(TAG, "Error while retrieving Channels: " + e));
    }

    /**
     * Adds the list of Tv channels in the list of EpgChannelRowModel.
     *
     * @param channels              The list of TV Channels.
     * @param isFavoriteChannelList Flag used to know if the list of channels is favorite.
     */
    private void addChannelRows(List<TvChannel> channels, boolean isFavoriteChannelList) {
        for (TvChannel channel : ArrayUtils.notNull(channels)) {
            mChannelRows.add(new EpgChannelRowModel(channel, new ArrayList<>(), isFavoriteChannelList));
        }
    }

    private void createEpgGridObservers(EpgChannelRowModel rowModel) {
        EpgFetchObservable<List<IBroadcast>> observable = mTvRepository.getEpgGridBroadcasts(
                rowModel.channel,
                rowModel.programs,
                mTvRepository.isAdultChannel(rowModel.channel.getId()),
                mTimeline.getWindowStart(), mTimeline.getWindowEnd()
        );

        mChannelEpgLoadersMap.put(rowModel.channel, observable);
    }

    private long calculatePrefetchFrom(long from) {
        long prefetchFrom = from - mExtraTimePrefetch;
        if (mOptimizePrefetchForDays) {
            prefetchFrom = TimeBuilder.utc(prefetchFrom).floor(TimeUnit.DAYS).get();
        }
        return Math.max(mTimeline.getWindowStart(), prefetchFrom);
    }

    private long calculatePrefetchTo(long to) {
        long prefetchTo = to + mExtraTimePrefetch;
        if (mOptimizePrefetchForDays) {
            prefetchTo = TimeBuilder.utc(prefetchTo).ceil(TimeUnit.DAYS).get();
        }
        return Math.min(mTimeline.getWindowEnd(), prefetchTo);
    }

    /**
     * Called when a new row is selected in the EPG.
     *
     * @param position The position of the row in the grid.
     */
    public void onPositionSelected(int position) {
        Log.d(TAG, "Position changed " + position);

        int channelPrefetchStart = calculatePrefetchStartIndex(position);
        int channelPrefetchEnd = calculatePrefetchEndIndex(position);

        if (mChannelPrefetchStart == channelPrefetchStart
                && mChannelPrefetchEnd == channelPrefetchEnd) {
            // No change in prefetch positions.
            return;
        }

        mChannelPrefetchStart = channelPrefetchStart;
        mChannelPrefetchEnd = channelPrefetchEnd;

        Log.d(TAG, "Selected position changed. Position : " + position
                + ", prefetch start : " + mChannelPrefetchStart + ", end : " + mChannelPrefetchEnd);

        boolean isScheduled = mHandler.hasMessages(0);
        mHandler.removeCallbacks(mLoadProgramsRunnable);
        mHandler.postDelayed(mLoadProgramsRunnable, isScheduled ? mChannelPrefetchDelay : 0);
    }

    private int calculatePrefetchStartIndex(int position) {
        return Math.max(0, position - mExtraChannelPrefetch);
    }

    private int calculatePrefetchEndIndex(int position) {
        return position + mExtraChannelPrefetch;
    }

    /**
     * Called when the visible timeline changes in the EPG.
     *
     * @param from The UTC timestamp of the start of the visible timeline.
     * @param to   The UTC timestamp of the end of the visible timeline.
     */
    @Override
    public void onTimeLineChanged(long from, long to) {
        mPrefetchFrom = calculatePrefetchFrom(from);
        mPrefetchTo = calculatePrefetchTo(to);

        mHandler.removeCallbacks(mLoadProgramsRunnable);
        mHandler.postDelayed(mLoadProgramsRunnable, mTimePrefetchDelay);
    }

    /**
     * Runnable to load programs for the visible EPG grid with some prefetch.
     */
    private final Runnable mLoadProgramsRunnable = new Runnable() {
        @Override
        public void run() {
            // Start loading from the middle of the prefetch window.
            int prefetchStart = mChannelPrefetchStart +
                    (mChannelPrefetchEnd - mChannelPrefetchStart) / 2;

            List<EpgChannelRowModel> channelRows = mChannelRows;

            for (int i = 0; i < channelRows.size() / 2; ++i) {

                // Load programs previous to prefetch starts
                int prevPosition = prefetchStart - i;
                if (prevPosition >= 0) {
                    prefetchRow(prevPosition, channelRows.get(prevPosition % channelRows.size()));
                }

                // Load programs next to prefetch start.
                int nextPosition = prefetchStart + i;
                prefetchRow(nextPosition, channelRows.get(nextPosition % channelRows.size()));
            }
        }

        private void prefetchRow(final int position, EpgChannelRowModel rowModel) {
            TvChannel channel = rowModel.channel;
            Disposable disposable = mChannelEpgDisposableMap.get(channel);

            EpgFetchObservable<List<IBroadcast>> epgRowObservable = mChannelEpgLoadersMap.get(channel);
            if (epgRowObservable == null) {
                Log.w(TAG, "Missing row observable for channel. Channel id : " + channel.getId());
                return;
            }

            // Cancel row subscription outside prefetch window.
            if (position < mChannelPrefetchStart || position > mChannelPrefetchEnd) {
                RxUtils.disposeSilently(disposable);
                mChannelEpgDisposableMap.remove(channel);
                return;
            }

            epgRowObservable.prefetchPrograms(channel.getId(), mPrefetchFrom, mPrefetchTo);

            // Subscribe to new row.
            if (disposable == null) {
                disposable = Observable.create(epgRowObservable)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(programs -> {
                            rowModel.programs.clear();
                            rowModel.programs.addAll(programs);
                            mDataLoadedListener.onProgramUpdate(position, rowModel);
                        });
                mChannelEpgDisposableMap.put(channel, disposable);
            }
        }
    };

    @Override
    public void dispose() {
        mHandler.removeCallbacks(null);
        RxUtils.disposeSilently(mChannelDisposable);
        RxUtils.disposeSilently(mChannelEpgDisposableMap.values());
    }

    @Override
    public boolean isDisposed() {
        return mChannelDisposable.isDisposed();
    }

    /**
     * Interface to get notifications when the EPG is loaded.
     */
    public interface EpgDataLoadedListener {

        /**
         * Called when the EPG channel list loads or changes.
         *
         * @param rowsModels The list of channels which needs to be displayed.
         */
        void onChannelListLoaded(List<EpgChannelRowModel> rowsModels);

        /**
         * Called when the programs in row are updated.
         *
         * @param position The position of the row in the grid which was loaded.
         * @param rowModel The row model which contains the new programs.
         */
        void onProgramUpdate(int position, EpgChannelRowModel rowModel);
    }
}
