package tv.threess.threeready.ui.generic.hint;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;

import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.PlayerUiButtonHintsDialogBinding;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Dialog for player UI button hints.
 * Sequence of 5 hint screens.
 *
 * @author Zsolt Bokor_
 * @since 2020.06.12
 */
public class PlayerUIButtonHintsDialog extends HintsDialog {

    private PlayerUiButtonHintsDialogBinding mBinding;

    private enum Type {
        START_OVER_BUTTON_HINTS,
        JUMP_TO_LIVE_BUTTON_HINTS,
        RECORD_BUTTON_HINTS,
        INFO_BUTTON_HINTS,
        SUBTITLES_BUTTON_HINTS
    }

    public static PlayerUIButtonHintsDialog newInstance(String hintId, boolean hintOpenedFromSettings) {
        Bundle args = new Bundle();
        args.putString(EXTRA_HINT_ID, hintId);
        args.putBoolean(EXTRA_HINT_OPENED_FROM_SETTINGS, hintOpenedFromSettings);
        PlayerUIButtonHintsDialog fragment = new PlayerUIButtonHintsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected View inflateView() {
        if (mBinding == null) {
            mBinding = PlayerUiButtonHintsDialogBinding.inflate(LayoutInflater.from(getContext()));
        }

        return mBinding.getRoot();
    }

    @NonNull
    @Override
    protected ImageView getBackgroundImage() {
        return mBinding.background;
    }

    @NonNull
    @Override
    protected TextView getNextButton() {
        return mBinding.buttonContainer.nextBtn;
    }

    @NonNull
    @Override
    protected TextView getPreviousButton() {
        return mBinding.buttonContainer.previousBtn;
    }

    @Override
    protected int getBackgroundImageId() {
        return R.drawable.player_ui_button_hints_background;
    }

    @Override
    protected void initViews() {
        super.initViews();
        mBinding.descriptionSmall.setText(mTranslator.get(TranslationKey.HINT_REVIEW_ALL_YOUR_TIPS));
        mBinding.buttonContainer.previousBtn.setContentDescription(mTranslator.get(TranslationKey.TALKBACK_HINTS_BUTTON_PREVIOUS));
    }

    @Override
    protected void displayHint() {
        switch (Type.values()[mImagePosition]) {
            case START_OVER_BUTTON_HINTS:
                UiUtils.loadImage(mTranslator, mTranslator.get(TranslationKey.PLAYER_UI_HINTS_START_OVER), mBinding.buttonImage);
                mBinding.description.setText(mTranslator.get(TranslationKey.HINT_TEXT_PLAYER_STARTOVER));
                setDescriptionMargins(getMargin(R.dimen.player_ui_hints_dialog_description_left_margin_start_over),
                        getMargin(R.dimen.player_ui_hints_dialog_description_top_margin_start_over));
                mBinding.descriptionSmall.setVisibility(View.GONE);
                mBinding.buttonContainer.nextBtn.setContentDescription(
                        new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_HINTS_BUTTON_NEXT))
                                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY, mTranslator.get(TranslationKey.HINT_TEXT_PLAYER_STARTOVER))
                                .toString());
                break;

            case JUMP_TO_LIVE_BUTTON_HINTS:
                UiUtils.loadImage(mTranslator, mTranslator.get(TranslationKey.PLAYER_UI_HINTS_JUMP_TO_LIVE), mBinding.buttonImage);
                mBinding.description.setText(mTranslator.get(TranslationKey.HINT_TEXT_PLAYER_JUMP_TO_LIVE));
                setDescriptionMargins(getMargin(R.dimen.player_ui_hints_dialog_description_left_margin_jump_to_live),
                        getMargin(R.dimen.player_ui_hints_dialog_description_top_margin_jump_to_live));
                mBinding.descriptionSmall.setVisibility(View.GONE);
                mBinding.buttonContainer.nextBtn.setContentDescription(
                        new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_HINTS_BUTTON_NEXT))
                                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY, mTranslator.get(TranslationKey.HINT_TEXT_PLAYER_JUMP_TO_LIVE))
                                .toString());
                break;

            case RECORD_BUTTON_HINTS:
                UiUtils.loadImage(mTranslator, mTranslator.get(TranslationKey.PLAYER_UI_HINTS_RECORD), mBinding.buttonImage);
                mBinding.description.setText(mTranslator.get(TranslationKey.HINT_TEXT_PLAYER_RECORD));
                setDescriptionMargins(getMargin(R.dimen.player_ui_hints_dialog_description_left_margin_record),
                        getMargin(R.dimen.player_ui_hints_dialog_description_top_margin_record));
                mBinding.descriptionSmall.setVisibility(View.GONE);
                mBinding.buttonContainer.nextBtn.setContentDescription(
                        new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_HINTS_BUTTON_NEXT))
                                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY, mTranslator.get(TranslationKey.HINT_TEXT_PLAYER_RECORD))
                                .toString());
                break;

            case INFO_BUTTON_HINTS:
                UiUtils.loadImage(mTranslator, mTranslator.get(TranslationKey.PLAYER_UI_HINTS_INFO), mBinding.buttonImage);
                mBinding.description.setText(mTranslator.get(TranslationKey.HINT_TEXT_PLAYER_INFO));
                setDescriptionMargins(getMargin(R.dimen.player_ui_hints_dialog_description_left_margin_info),
                        getMargin(R.dimen.player_ui_hints_dialog_description_top_margin_info));
                mBinding.descriptionSmall.setVisibility(View.GONE);
                mBinding.buttonContainer.nextBtn.setContentDescription(
                        new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_HINTS_BUTTON_NEXT))
                                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY, mTranslator.get(TranslationKey.HINT_TEXT_PLAYER_INFO))
                                .toString());
                break;

            case SUBTITLES_BUTTON_HINTS:
                UiUtils.loadImage(mTranslator, mTranslator.get(TranslationKey.PLAYER_UI_HINTS_SUBTITLES), mBinding.buttonImage);
                mBinding.description.setText(mTranslator.get(TranslationKey.HINT_TEXT_PLAYER_SUBTITLES));
                setDescriptionMargins(getMargin(R.dimen.player_ui_hints_dialog_description_left_margin_subtitles),
                        getMargin(R.dimen.player_ui_hints_dialog_description_top_margin_subtitles));
                mBinding.descriptionSmall.setVisibility(View.VISIBLE);
                mBinding.buttonContainer.nextBtn.setContentDescription(
                        new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_HINTS_BUTTON_DONE))
                                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY,
                                        mTranslator.get(TranslationKey.HINT_TEXT_PLAYER_SUBTITLES) + " " + mTranslator.get(TranslationKey.HINT_REVIEW_ALL_YOUR_TIPS))
                                .toString());
                break;
        }
        mBinding.buttonContainer.nextBtn.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED);
    }

    @Override
    protected boolean isLastImage() {
        return mImagePosition + 1 == Type.values().length;
    }

    /**
     * Adjust description margins according to the selected screen.
     */
    private void setDescriptionMargins(int leftMargin, int topMargin) {
        RelativeLayout.LayoutParams descriptionLayoutParams =
                (RelativeLayout.LayoutParams) mBinding.description.getLayoutParams();
        descriptionLayoutParams.leftMargin = leftMargin;
        descriptionLayoutParams.topMargin = topMargin;
        mBinding.description.setLayoutParams(descriptionLayoutParams);
    }

    @Override
    protected int getScreenNumbers() {
        return Type.values().length;
    }
}
