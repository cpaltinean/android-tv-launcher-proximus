/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;

import androidx.annotation.NonNull;
import androidx.leanback.widget.HorizontalGridView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.api.vod.wrapper.VodRentOffers;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.SeriesVodDetailsOverlayBinding;
import tv.threess.threeready.ui.generic.adapter.DetailPageButtonOrder;
import tv.threess.threeready.ui.generic.model.VodPlayOption;
import tv.threess.threeready.ui.generic.presenter.ActionModel;
import tv.threess.threeready.ui.generic.presenter.RentActionModel;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.FontTextView;

/**
 * Presenter class for the overview part of a series vod detail page.
 * Shows information about the selected episode and series.
 *
 * @author Zsolt Bokor
 * @since 2019.01.08
 */
public class VodSeriesDetailsOverviewPresenter
        extends SeriesDetailsOverviewPresenter<VodSeriesDetailsOverviewPresenter.ViewHolder, IBaseVodItem, IVodSeries> {
    private static final String TAG = Log.tag(VodSeriesDetailsOverviewPresenter.class);

    private final VodRepository mVodRepository = Components.get(VodRepository.class);

    public VodSeriesDetailsOverviewPresenter(Context context, IVodSeries series) {
        super(context, series);

        mSeries = series;
    }

    @Override
    public void onBindHolder(ViewHolder holder, IBaseVodItem item, List<?> payLoads) {
        if (mSeries.isCompleted() && isSeriesPayLoad(payLoads)) {
            updateButtons(holder, item);
        }

        super.onBindHolder(holder, item, payLoads);
    }

    private boolean isSeriesPayLoad(List<?> payLoads) {
        for (Object o : payLoads) {
            if (o instanceof IVodSeries) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBindHolder(ViewHolder holder, IBaseVodItem item) {
        super.onBindHolder(holder, item);
        //No logo will be shown until the provider logos will be available.
        ((RelativeLayout.LayoutParams) holder.mBinding.title.getLayoutParams()).bottomMargin = mContext.getResources()
                .getDimensionPixelOffset(R.dimen.vod_series_detail_title_bottom_margin);
    }

    @Override
    protected BaseDetailsOverviewPresenter.ViewHolder createViewHolder(ViewGroup parent) {
        SeriesVodDetailsOverlayBinding binding = SeriesVodDetailsOverlayBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    protected String getGenres(IBaseVodItem episode) {
        List<String> genres = episode.getGenres();
        if (ArrayUtils.isEmpty(genres)) {
            return "";
        }
        return genres.get(0);
    }

    @Override
    protected void updateMarkersRow(ViewHolder holder, IBaseVodItem item) {
        holder.mBinding.contentMarker.setVisibility(View.GONE);
    }

    protected void updateButtons(ViewHolder holder, IBaseVodItem episode) {
        if (mSeries.isCompleted()) {
            addButtons(holder, mSeries);
        } else {
            showLoadingButton(holder);
        }
    }

    protected void addButtons(ViewHolder holder, IVodSeries series) {
        if (holder.mLastPlayedVodDisposable == null || holder.mLastPlayedVodDisposable.isDisposed()) {
            holder.mLastPlayedVodDisposable = mVodRepository.getLastPlayedVodInSeries(series)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(result -> {
                                Log.d(TAG, "Get episode that should be shown in series detail page");
                                holder.mVariantOfferMap = result.getVariantOfferMap();
                                updateButtons(holder, result, series);
                            },
                            throwable -> Log.d(TAG, "Error getting credits. " + throwable));
        }
    }

    private void updateButtons(ViewHolder holder, VodRentOffers rentOffers, IVodSeries series) {
        List<ActionModel> actions = new ArrayList<>();
        IVodItem episode = rentOffers.getVod();

        if (episode != null) {
            String buttonPrefix;
            if (holder.mLastPlayerEpisode == null) {
                buttonPrefix = TranslationKey.SCREEN_DETAIL_PLAY_BUTTON;
            } else {
                buttonPrefix = TranslationKey.SCREEN_DETAIL_RESUME_BUTTON;
            }

            IVodItem vodToWatch = holder.mLastPlayerEpisode == null ? episode : holder.mLastPlayerEpisode;
            List<VodPlayOption> vodPlayOptions = VodPlayOption.generatePlayOptions(vodToWatch, series, holder.mVariantOfferMap);
            if (!vodPlayOptions.isEmpty()) {
                actions.add(new ActionModel(0, 0,
                        mTranslator.get(buttonPrefix) + " " + vodToWatch.getTitleWithSeasonEpisode(mTranslator, "", " "),
                        DetailPageButtonOrder.Watch,
                        () -> {
                            if (vodPlayOptions.size() == 1) {
                                VodPlayOption playOption = vodPlayOptions.get(0);
                                mNavigator.clearAllDialogs();
                                mNavigator.showVodPlayer(StartAction.Ui, playOption.getVodVariant(), vodToWatch, series, playOption.getVodPrice());
                            } else {
                                mNavigator.showPlayOptionsDialog(vodPlayOptions, episode);
                            }
                        }
                ));
            }

            // Add trailer button
            for (IVodVariant variant : episode.getVariantList()) {
                if (variant.hasTrailer()) {
                    actions.add(new ActionModel(
                            0, 0,
                            mTranslator.get(TranslationKey.SCREEN_DETAIL_TRAILER_BUTTON),
                            DetailPageButtonOrder.Trailer,
                            () -> mNavigator.showTrailerPlayer(StartAction.Ui, variant, episode)));
                    break;
                }
            }

            int credit = rentOffers.getCredit();
            if (vodToWatch.isRentable(credit, rentOffers.getVariantOfferMap())) {
                actions.add(new RentActionModel(
                        0, 0,
                        DetailPageButtonOrder.Rent,
                        () -> mNavigator.showRentConfirmationDialog(episode),
                        credit, rentOffers.getVariantOfferMap()
                ));
            }
        }

        updateActionModelDescriptions(actions, episode);

        holder.mAdapter.replaceAll(actions);

        if (!actions.isEmpty() && holder.view.hasFocus()) {
            holder.mBinding.actionGrid.requestFocus();
        }
    }

    @Override
    public long getStableId(IBaseVodItem item) {
        // Check the content and availability status for stable id.
        return Objects.hash(item.getId(), item.isSubscribed(),
                item.isRented(), mSeries.getNumberOfSeasons());
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);
        RxUtils.disposeSilently(holder.mLastPlayedVodDisposable);
    }

    public static class ViewHolder extends SeriesDetailsOverviewPresenter.ViewHolder {

        private final SeriesVodDetailsOverlayBinding mBinding;
        IVodItem mLastPlayerEpisode;

        Map<String, IRentOffer> mVariantOfferMap;
        Disposable mLastPlayedVodDisposable;

        public ViewHolder(SeriesVodDetailsOverlayBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        @NonNull
        @Override
        public HorizontalGridView getActionGridView() {
            return mBinding.actionGrid;
        }

        @NonNull
        @Override
        protected ImageView getNonPlayableIcon() {
            return mBinding.nonPlayableIcon;
        }

        @NonNull
        @Override
        protected FontTextView getNonPlayableTextView() {
            return mBinding.nonPlayableTextView;
        }

        @NonNull
        @Override
        protected View getNonPlayableOverlay() {
            return mBinding.nonPlayableOverview;
        }

        @NonNull
        @Override
        protected FontTextView getTitleView() {
            return mBinding.title;
        }

        @NonNull
        @Override
        protected FontTextView getInfoRow() {
            return mBinding.infoRow;
        }

        @NonNull
        @Override
        protected TextSwitcher getDescriptionView() {
            return mBinding.description;
        }

        @NonNull
        @Override
        protected ImageView getShowMoreBtn() {
            return mBinding.showMoreBtn;
        }

        @NonNull
        @Override
        protected FontTextView getCastView() {
            return mBinding.cast;
        }

        @NonNull
        @Override
        protected FontTextView getDirectorsView() {
            return mBinding.directors;
        }
    }
}
