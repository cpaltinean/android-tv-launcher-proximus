package tv.threess.threeready.ui.epg.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.ui.R;

/**
 * Vertical expandable program guide grid with favorite mode animations.
 *
 * @author Barabas Attila
 * @since 2017.12.04
 */

public class FavoriteModeProgramGuideGridView extends ExpandableProgramGuideGridView {

    public FavoriteModeProgramGuideGridView(Context context) {
        this(context, null);
    }

    public FavoriteModeProgramGuideGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FavoriteModeProgramGuideGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * The focused program card width/height animation.
     */
    public List<ValueAnimator> getSizeAnimatorList(boolean isFavoriteMode) {
        List<ValueAnimator> animatorList = new ArrayList<>();

        if (getSelectedChild() == null) {
            return animatorList;
        }
        final View selectedChild = getSelectedChild();
        final ViewHolder holder = getChildViewHolder(selectedChild);
        if (holder != null) {
            holder.setIsRecyclable(false);
        }
        // height animation
        ValueAnimator animator = ValueAnimator.ofInt(isFavoriteMode ? getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_height_focused) :
                        getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_focused_card_width),
                isFavoriteMode ? getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_focused_card_width) :
                        getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_height_focused));
        animator.addUpdateListener(updateListener -> {
            int val = (Integer) updateListener.getAnimatedValue();
            LayoutParams layoutParams = (LayoutParams) selectedChild.getLayoutParams();
            layoutParams.height = val;
            selectedChild.setLayoutParams(layoutParams);
        });
        animatorList.add(animator);

        // width animation
        animator = ValueAnimator.ofInt(isFavoriteMode ? getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_width_focused) :
                        getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_focused_card_width),
                isFavoriteMode ? getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_focused_card_width) :
                        getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_width_focused));
        animator.addUpdateListener(updateListener -> {
            int val = (Integer) updateListener.getAnimatedValue();
            LayoutParams layoutParams = (LayoutParams) selectedChild.getLayoutParams();
            layoutParams.width = val;
            selectedChild.setLayoutParams(layoutParams);
        });
        animatorList.add(animator);

        // translation_y animation
        ObjectAnimator objectAnimator = isFavoriteMode ?
                ObjectAnimator.ofFloat(selectedChild, View.TRANSLATION_Y, 0, getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_translation_y)) :
                ObjectAnimator.ofFloat(selectedChild, View.TRANSLATION_Y, getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_translation_y), 0);
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                animation.setupEndValues();
            }
        });
        animatorList.add(objectAnimator);

        // translation_x animation
        objectAnimator = isFavoriteMode ?
                ObjectAnimator.ofFloat(selectedChild, View.TRANSLATION_X, 0, getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_translation_x)) :
                ObjectAnimator.ofFloat(selectedChild, View.TRANSLATION_X, getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_translation_x), 0);
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (holder != null) {
                    holder.setIsRecyclable(true);
                }
                animation.setupEndValues();
            }
        });
        animatorList.add(objectAnimator);
        return animatorList;
    }

}
