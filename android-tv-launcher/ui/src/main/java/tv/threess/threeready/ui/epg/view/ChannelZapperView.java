package tv.threess.threeready.ui.epg.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.RelativeLayout;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.FeatureControl;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.TvChannelCacheType;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.epg.presenter.TvChannelZapperCardPresenter;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.adapter.ModularItemBridgeAdapter;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeSeparator;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.tv.helper.FavoriteChannelsCache;

/**
 * Channel zapper view which displayes all the available channels in a horizontal grid.
 *
 * @author Barabas Attila
 * @since 2017.11.16
 */

public abstract class ChannelZapperView extends RelativeLayout {
    private static final String TAG = Log.tag(ChannelZapperView.class);

    private static final long CHANNEL_SELECTION_DISPATCH_TIMEOUT = 500;

    protected final TvRepository mTvRepository = Components.get(TvRepository.class);

    protected final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    protected final FavoriteChannelsCache mFavoriteChannelsCache = Components.get(FavoriteChannelsCache.class);

    protected final FeatureControl mFeatureControl = Components.get(FeatureControl.class);
    protected final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);

    String mInitChannelId;
    boolean mInitChannelFavorite;

    protected TvChannel mSelectedChannel;
    OnChannelSelectedListener mChannelSelectedListener;

    Runnable mNotifyChannelChangeRunnable;

    ItemBridgeAdapter mAdapter;

    ArrayObjectAdapter<TvChannel> mObjectAdapter = new ArrayObjectAdapter<>();

    List<TvChannel> mChannels = new ArrayList<>();
    List<TvChannel> mFavChannels = new ArrayList<>();
    List<TvChannel> mAllChannels = new ArrayList<>();

    Disposable mChannelListDisposable;

    public ChannelZapperView(Context context) {
        this(context, null);
    }

    public ChannelZapperView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChannelZapperView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ChannelZapperView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        inflateView();

        ChannelZapperGridView gridView = getChannelZapperGridView();
        gridView.setHorizontalSpacing(getContext().getResources().getDimensionPixelOffset(R.dimen.zapper_separator_padding));
        mAdapter = new ModularItemBridgeAdapter(mObjectAdapter,
                new TvChannelZapperCardPresenter(context), true);

        gridView.setAdapter(mAdapter, () -> {
            List<StripeSeparator> separators = new ArrayList<>();
            if (mFavChannels.size() > 0) {
                separators.add(new StripeSeparator(0, mFavChannels.size(),
                        ContextCompat.getDrawable(context, R.drawable.section_separator)));
            }
            separators.add(new StripeSeparator(mFavChannels.size(), mObjectAdapter.size(),
                    ContextCompat.getDrawable(context, R.drawable.section_separator)));
            return separators;
        });

        loadChannels();
    }

    protected abstract void inflateView();

    @NonNull
    protected abstract ChannelZapperGridView getChannelZapperGridView();


    public void setInitialChannel(String channelId, boolean isFavorite) {
        if (mChannels != null && !mChannels.isEmpty()) {
            selectChannel(channelId, isFavorite);
        } else {
            mInitChannelId = channelId;
            mInitChannelFavorite = isFavorite;
        }
    }

    /**
     * Set listener which will be notified on channel selection changes.
     */
    public void setOnChannelSelectedListener(@Nullable OnChannelSelectedListener channelSelectionChangedListener) {
        mChannelSelectedListener = channelSelectionChangedListener;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        getChannelZapperGridView().addOnChildViewHolderSelectedListener(mOnChildViewHolderSelectedListener);
    }

    protected void onChannelSelected(TvChannel channel, boolean isFromFavoriteSection) {
        if (channel == null) {
            return;
        }

        boolean changed = mSelectedChannel == null
                || !mSelectedChannel.getId().equals(channel.getId());
        mSelectedChannel = channel;

        if (mChannelSelectedListener != null) {
            mChannelSelectedListener.onChannelSelected(channel, isFromFavoriteSection, changed);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        getChannelZapperGridView().removeOnChildViewHolderSelectedListener(mOnChildViewHolderSelectedListener);

        removeCallbacks(mNotifyChannelChangeRunnable);
    }

    @CallSuper
    public void onDestroy() {
        getChannelZapperGridView().setAdapter(null);
        RxUtils.disposeSilently(mChannelListDisposable);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    selectPrevChannel();
                    return true;

                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    selectNextChannel();
                    return true;
            }
        }

        return super.dispatchKeyEvent(event);
    }

    /**
     * Load and display the available channel list.
     */
    private void loadChannels() {
        Log.d(TAG, "Load channels.");

        RxUtils.disposeSilently(mChannelListDisposable);
        mTvRepository.getEpgChannels()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Map<TvChannelCacheType, List<TvChannel>>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        mChannelListDisposable = d;
                    }

                    @Override
                    public void onNext(@NonNull Map<TvChannelCacheType, List<TvChannel>> map) {
                        if (!map.isEmpty()) {

                            List<TvChannel> favChannels = map.get(TvChannelCacheType.FAVORITE);
                            List<TvChannel> allChannels = map.get(mFeatureControl.displayAllChannels()
                                    ? TvChannelCacheType.ALL : TvChannelCacheType.NOT_FAVORITE);
                            setChannels(favChannels, allChannels);

                            initChannelSelection();
                        } else {
                            Log.e(TAG, "No channel found.");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "Failed to get channels!", e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    public void initChannelSelection() {
        if (mChannels == null || mChannels.isEmpty()) {
            Log.e(TAG, "Channel list is empty.");
            return;
        }

        final String lastPlayedChannel = mNowOnTvCache.getLastPlayedChannelId();

        String channelId;
        boolean isFavorite = false;

        if (!TextUtils.isEmpty(mPlaybackDetailsManager.getChannelId())) {
            channelId = mPlaybackDetailsManager.getChannelId();
            isFavorite = mPlaybackDetailsManager.isFavorite();
        } else if (!TextUtils.isEmpty(lastPlayedChannel)) {
            channelId = lastPlayedChannel;
        } else {
            channelId = mInitChannelId;
            isFavorite = mInitChannelFavorite;
        }

        TvChannel channel = null;
        for (TvChannel channelIt : mChannels) {
            if (channelId.equals(channelIt.getId())) {
                channel = channelIt;
                break;
            }
        }

        if (channel == null) {
            selectChannel(mChannels.get(0).getId(), false);
        } else {
            selectChannel(channel.getId(), isFavorite);
        }
    }

    /**
     * Display the given channel list in the zapper.
     */
    protected void setChannels(List<TvChannel> favChannels, List<TvChannel> allChannels) {
        mFavChannels.clear();
        if (favChannels != null) {
            mFavChannels.addAll(favChannels);
        }

        mAllChannels.clear();
        if (allChannels != null) {
            mAllChannels.addAll(allChannels);
        }

        // Compose channel list.
        mChannels.clear();
        mChannels.addAll(mFavChannels);
        mChannels.addAll(mAllChannels);


        // Notify adapter about data source change.
        mObjectAdapter.replaceAll(mChannels);

        Log.d(TAG, "Channel list update. " + mChannels.size());
    }

    public void onContentShown() {
        if (TextUtils.isEmpty(mPlaybackDetailsManager.getChannelId())) {
            selectChannel(mNowOnTvCache.getLastPlayedChannelId(), true);
        } else {
            selectChannel(mPlaybackDetailsManager.getChannelId(), mPlaybackDetailsManager.isFavorite());
        }
    }

    /**
     * Select the given channel in the horizontal grid.
     */
    protected void selectChannel(TvChannel channel) {

        if (channel == null) {
            return;
        }
        selectChannel(channel.getId(), channel.isFavorite());
    }

    /**
     * Select the given channel in the horizontal grid.
     */
    protected void selectChannel(@NonNull String channelId, boolean isFavorite) {
        int position = -1;

        List<TvChannel> channelList;
        if (isFavorite) {
            channelList = mChannels;
        } else {
            channelList = mChannels.subList(mFavChannels.size(), mChannels.size());
        }

        // Find channel position.
        for (int i = 0; i < channelList.size(); i++) {
            if (Objects.equals(channelId, channelList.get(i).getId())) {
                position = i;
                break;
            }
        }

        if (!isFavorite) {
            position += mFavChannels.size();
        }

        int finalPosition = mAdapter.getCircularPosition(position);
        getChannelZapperGridView().post(() -> getChannelZapperGridView().setSelectedPosition(finalPosition));

        TvChannel channel = (TvChannel) mAdapter.getItem(finalPosition);
        onChannelSelected(channel, isFavorite);
    }

    /**
     * Select the channel next to the currently selected channel in the horizontal gird.
     */
    private void selectNextChannel() {
        ChannelZapperGridView gridView = getChannelZapperGridView();
        int nextPosition = gridView.getSelectedPosition() + 1;
        if (nextPosition < mAdapter.getItemCount()) {
            gridView.setSelectedPositionSmooth(nextPosition);
        }
    }

    /**
     * Select the channel previous to the currently selected channel in the horizontal gird.
     */
    private void selectPrevChannel() {
        ChannelZapperGridView gridView = getChannelZapperGridView();
        int prevPosition = gridView.getSelectedPosition() - 1;
        if (prevPosition >= 0) {
            gridView.setSelectedPositionSmooth(prevPosition);
        }
    }

    private final OnChildViewHolderSelectedListener mOnChildViewHolderSelectedListener = new OnChildViewHolderSelectedListener() {

        @Override
        public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
            super.onChildViewHolderSelected(parent, child, position, subposition);

            // First selection. Notify immediately.
            if (mNotifyChannelChangeRunnable == null) {
                notifyChannelChange(position);
                mNotifyChannelChangeRunnable = () -> mNotifyChannelChangeRunnable = null;

            // Notify selection delayed. Wait for fast scroll.
            } else {
                removeCallbacks(mNotifyChannelChangeRunnable);
                mNotifyChannelChangeRunnable = () -> {
                    notifyChannelChange(position);
                    mNotifyChannelChangeRunnable = null;
                };
            }

            postDelayed(mNotifyChannelChangeRunnable, CHANNEL_SELECTION_DISPATCH_TIMEOUT);
        }

        private void notifyChannelChange(int position) {
            int selectedPosition = mAdapter.getNormalizedPosition(position);
            TvChannel channel = (TvChannel) mAdapter.getItem(position);
            onChannelSelected(channel, selectedPosition < mFavChannels.size());
        }
    };

    public TvChannel getSelectedChannel() {
        return mSelectedChannel;
    }

    /**
     * Listener to detect channel selection changes in the zapper view.
     */
    public interface OnChannelSelectedListener {
        void onChannelSelected(@NonNull TvChannel channel, boolean isFromFavoriteSection, boolean changed);
    }
}
