package tv.threess.threeready.ui.generic.dialog;

/**
 * Constant values for alert dialog error codes
 *
 * @author Daniel Gliga
 * @since 2017.06.20
 */

public interface ErrorCode {

    /* FTI Error Codes */
    String ERROR_CODE_NO_CONNECTION = "A001";

    /* Detail Page Error Codes */
    String ERROR_CODE_LOAD_DETAIL_PAGE = "A012";
}
