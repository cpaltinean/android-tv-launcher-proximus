/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.tv.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.core.content.res.ResourcesCompat;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.PlayerIconViewBinding;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Displays the Start Over, Subtitle and Favorite buttons with text on the Live player.
 * The buttons are focusable and clickable.
 *
 * @author Szende Pal
 * @since 2017.05.18
 */
public class PlayerButtonView extends LinearLayout {
    public static final String TAG = Log.tag(PlayerButtonView.class);

    private final Translator mTranslator = Components.get(Translator.class);

    private PlayerIconViewBinding mBinding;

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    public PlayerButtonView(Context context) {
        this(context, null);
    }

    public PlayerButtonView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PlayerButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initialize();

        // Read rounded attribute
        if (attrs != null) {
            TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.PlayerButtonView, defStyleAttr, 0);
            try {
                setTitle(typedArray.getString(R.styleable.PlayerButtonView_title));
                setResourceId(typedArray.getResourceId(R.styleable.PlayerButtonView_resourceId, 0));
            } finally {
                typedArray.recycle();
            }
        }
    }

    public void initialize() {
        mBinding = PlayerIconViewBinding.inflate(LayoutInflater.from(getContext()), this);

        mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        mBinding.title.setVisibility(INVISIBLE);
        setOrientation(VERTICAL);

        mBinding.icon.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                mBinding.title.setVisibility(VISIBLE);
            } else {
                mBinding.title.setVisibility(INVISIBLE);
            }
        });

        mBinding.icon.setNextFocusDownId(mBinding.icon.getId());
    }

    public void setTitle(String title) {
        mBinding.title.setText(title);
        mBinding.icon.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_BUTTON_PLAYER))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BUTTON_NAME, title)
                        .toString()
        );
    }

    public void setResourceId(int resourceId) {
        mBinding.icon.setResourceId(resourceId);
        mBinding.icon.setImageDrawable(ResourcesCompat.getDrawable(getResources(), resourceId, null));
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        mBinding.icon.setOnClickListener(l);
    }
}

