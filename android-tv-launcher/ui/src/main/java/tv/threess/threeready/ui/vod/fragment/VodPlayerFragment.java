/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.vod.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.widget.TextView;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import tv.threess.threeready.api.generic.model.BingeWatchingInfo;
import tv.threess.threeready.api.home.model.generic.ContentMarkers;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.SeriesPlaybackDetailsCallback;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.databinding.PlayerBaseContainerBinding;
import tv.threess.threeready.ui.databinding.VodPlayerBinding;
import tv.threess.threeready.ui.generic.notification.Notification;
import tv.threess.threeready.ui.generic.player.view.PlayerButtonContainer;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.BaseOrderedIconsContainer;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.generic.view.PlaybackActionView;
import tv.threess.threeready.ui.tv.fragment.BaseTrickPlayPlayerFragment;
import tv.threess.threeready.ui.tv.view.PlayerButtonView;
import tv.threess.threeready.ui.tv.view.SeekBarView;

import static tv.threess.threeready.ui.utils.UiUtils.runOnUiThread;

/**
 * Player for a Vod.
 *
 * @author Szende Pal
 * @since 2017.06.22
 */
public class VodPlayerFragment extends BaseTrickPlayPlayerFragment<IVodVariant> implements SeriesPlaybackDetailsCallback {
    public static final String TAG = Log.tag(VodPlayerFragment.class);

    private static final String EXTRA_VOD_VARIANT = "EXTRA_VOD_VARIANT";
    private static final String EXTRA_VOD = "EXTRA_VOD";
    private static final String EXTRA_VOD_SERIES = "EXTRA_VOD_SERIES";
    private static final String EXTRA_VOD_PRICE = "EXTRA_VOD_PRICE";

    public static VodPlayerFragment newInstance(IVodVariant vodVariant, IVodItem vod, IVodSeries vodSeries, IVodPrice vodPrice, boolean shouldStartPlayback) {
        Bundle args = new Bundle();
        args.putBoolean(EXTRA_SHOULD_START_PLAYBACK, shouldStartPlayback);
        args.putSerializable(EXTRA_VOD_VARIANT, vodVariant);
        args.putSerializable(EXTRA_STARTED_ITEM, vodVariant);
        args.putSerializable(EXTRA_VOD, vod);
        args.putSerializable(EXTRA_VOD_PRICE, vodPrice);
        args.putSerializable(EXTRA_VOD_SERIES, vodSeries);

        VodPlayerFragment fragment = new VodPlayerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private Notification mAgeRestrictionNotification;

    protected VodPlayerBinding mBinding;
    protected PlayerBaseContainerBinding mBaseContainerBinding;

    @Override
    protected View getRootView() {
        return mBinding != null ? mBinding.getRoot() : null;
    }

    @Override
    public View inflateRootView() {
        if (mBinding == null) {
            mBinding = VodPlayerBinding.inflate(LayoutInflater.from(getContext()));
            mBaseContainerBinding = PlayerBaseContainerBinding.bind(mBinding.getRoot());
        }

        return mBinding.getRoot();
    }

    @Override
    protected ContentPlayerViewHolder getViewHolder() {
        return mViewHolder;
    }

    @Override
    protected void reportDetailsFragmentSelection() {

    }

    @Override
    @Nullable
    public IVodVariant getPlaybackData() {
        return mPlaybackDetailsManager.getVodVariantPlayerData();
    }

    @Override
    protected boolean isForActivePlayback() {
        IVodVariant playerData = mPlaybackDetailsManager.getVodVariantPlayerData();
        IVodVariant currentVod = getSerializableArgument(EXTRA_VOD_VARIANT);
        if (playerData == null || currentVod == null) {
            return false;
        }

        return mPlaybackDetailsManager.getPlayerType() == PlaybackType.Vod &&
                Objects.equals(currentVod.getId(), playerData.getId());
    }

    @Override
    protected void updateProviderLogo(IVodVariant contentItem) {

    }

    @Override
    protected void updatePlaybackDetails(IVodVariant contentItem) {
        super.updatePlaybackDetails(contentItem);
        updateAudioLanguageIndicator();
    }

    @Override
    protected void updateSubtitle(IVodVariant contentItem) {
        List<String> detailsList = new ArrayList<>();
        String subtitle;
        if (contentItem.isEpisode()) {
            subtitle = contentItem.getEpisodeTitleWithSeasonEpisode(mTranslator, "");
            mBinding.detailsLayout.subtitleDetails.updateSubtitle(subtitle, null);
        } else {
            if (contentItem.getReleaseYear() != null) {
                detailsList.add(contentItem.getReleaseYear());
            }

            String genres = TextUtils.join(", ", contentItem.getGenres().stream().limit(1).toArray());
            if (!TextUtils.isEmpty(genres)) {
                detailsList.add(genres);
            }

            detailsList.add(LocaleTimeUtils.getDuration(contentItem.getDuration(), mTranslator));

            mBinding.detailsLayout.subtitleDetails.updateSubtitle(null, detailsList);
        }
    }

    @Override
    protected void updateQualityIndicator(IVodVariant variant) {
        if (variant.isHD()) {
            mBinding.detailsLayout.iconsContainer.showHDIcon();
        } else {
            mBinding.detailsLayout.iconsContainer.hideHDIcon();
        }
    }

    @Override
    protected void updateContentMarkers(IVodVariant contentItem) {
        // Display content marker
        mBinding.detailsLayout.contentMarker.showMarkers(contentItem, ContentMarkers.TypeFilter.Player);
    }

    protected void updateAudioLanguageIndicator() {
        if (mPlaybackDetailsManager.getVodVariantPlayerData() != null) {
            mBinding.detailsLayout.iconsContainer.showAudioProfiles(mPlaybackDetailsManager.getVodVariantPlayerData().getAudioLanguages());
        }
    }

    @Override
    protected void startPlayback(StartAction action) {
        IVodVariant vodVariant = getSerializableArgument(EXTRA_VOD_VARIANT);
        IVodItem vod = getSerializableArgument(EXTRA_VOD);
        IVodPrice vodPrice = getSerializableArgument(EXTRA_VOD_PRICE);
        IVodSeries vodSeries = getSerializableArgument(EXTRA_VOD_SERIES);
        if (vod == null || vodVariant == null) {
            Log.e(TAG, "No VOD provided to start the playback.");
            return;
        }

        if (!mPlaybackDetailsManager.isVodPlaying(vodVariant)) {
            mAgeRestrictionNotification = mNotificationManager
                    .showAgeRestrictionLegalNotificationIfNeeded(vodVariant.getParentalRating());
        }

        Log.d(TAG, "startPlayback() called with: vod = [" + vod.getTitle() + " "
                + vod.getId() + "], vod variant = [" + vodVariant.getTitle() + " " + vodVariant.getId() + "], action = [" + action + "]");

        if (action == StartAction.StartOver) {
            mPlaybackDetailsManager.startVod(
                    action, vod, vodPrice, vodVariant, vodSeries, true, true);
        } else {
            mPlaybackDetailsManager.startVod(
                    action, vod, vodPrice, vodVariant, vodSeries, false, false);
        }
    }

    @Override
    protected void initializeButtons() {
        super.initializeButtons();

        PlayerButtonContainer buttonContainer = getViewHolder().getButtonContainer();
        mBinding.buttonsContainer.playNextEpisode.setTitle(mTranslator.get(TranslationKey.SCREEN_PLAYER_PLAY_NEXT_BUTTON));
        mBinding.buttonsContainer.playNextEpisode.setOnClickListener(v -> {
            buttonContainer.clearSelection();
            onPlayNextEpisodeClicked();
        });

        mBinding.buttonsContainer.playNextEpisode.setVisibility(getNextEpisode() == null ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onNextEpisodeLoaded() {
        runOnUiThread(() -> mBinding.buttonsContainer.playNextEpisode.setVisibility(getNextEpisode() == null ? View.GONE : View.VISIBLE));
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mAgeRestrictionNotification != null) {
            mNotificationManager.hideNotification(mAgeRestrictionNotification);
        }
    }

    @Override
    protected void onMoreInfoButtonClicked(IVodVariant contentItem) {
        IVodItem vod = mPlaybackDetailsManager.getVodPlayerData();
        if (vod == null) {
            // if no vod is present, open the detail page of the variant
            mNavigator.showVodMovieDetails(contentItem.getId());
            return;
        }
        mNavigator.showVodMovieDetails(vod);

        reportDetailsFragmentSelection();
    }

    @Override
    protected void updateTitle(IVodVariant contentItem) {
        mBinding.detailsLayout.programTitle.setText(mParentalControlManager.isRestricted(contentItem, true)
                ? mTranslator.get(TranslationKey.CARDS_TITLE_CONTENT_LOCKED)
                : contentItem.getTitle());
    }

    @Nullable
    @Override
    protected Event<?, ?> generatePageOpenEvent() {
        IVodVariant vod = getPlaybackData();
        if (vod == null) {
            return null;
        }

        return ReportUtils.createEvent(UILog.Page.MediaPlayerPage, vod);
    }

    private void onPlayNextEpisodeClicked() {
        IVodItem vod = getNextEpisode();
        if (vod != null) {
            IVodVariant playingVodVariant = mPlaybackDetailsManager.getVodVariantPlayerData();
            IVodPrice price = mPlaybackDetailsManager.getVodPricePlayerData();
            // Set the default vod variant for the next episode
            IVodVariant variantToStart = vod.getMainVariant();

            for (IVodVariant variant : vod.getSubscribedVariants()) {
                if (playingVodVariant != null && playingVodVariant.formatEquals(variant)) {
                    variantToStart = variant;
                    break;
                }
            }
            mNavigator.showVodPlayer(StartAction.ContinuousPlay, variantToStart, vod, price);
        }
    }

    private IVodItem getNextEpisode() {
        BingeWatchingInfo<?, ?> bingeEpisode = mPlaybackDetailsManager.getNextEpisode();
        if (bingeEpisode != null && bingeEpisode.getNextEpisode() instanceof IVodItem) {
            return (IVodItem) bingeEpisode.getNextEpisode();
        }
        return null;
    }

    private final ContentPlayerViewHolder mViewHolder = new ContentPlayerViewHolder() {
        @Override
        public View getUnskippableContainer() {
            return mBinding.unskippableAdvertisement.unskippableAdvertisement;
        }

        @Override
        public SeekBarView getSeekbar() {
            return mBinding.seekBarLayout.seekBar;
        }

        @Override
        public View getSeekBarLayout() {
            return mBinding.seekBarLayout.seekBarLayout;
        }

        @Override
        public ViewGroup getDetailContainer() {
            return mBinding.detailsLayout.detailContainer;
        }

        @Override
        public PlayerButtonContainer getButtonContainer() {
            return mBinding.buttonsContainer.buttonContainer;
        }

        @Override
        public TextView getUnskippableMessage() {
            return mBinding.unskippableAdvertisement.unskippableMessage;
        }

        @Override
        public TextView getUnskippableTime() {
            return mBinding.unskippableAdvertisement.unskippableTime;
        }

        @Override
        public FontTextView getPlayedTimeView() {
            return mBinding.seekBarLayout.playedTime;
        }

        @Override
        public FontTextView getLeftTimeView() {
            return mBinding.seekBarLayout.leftTime;
        }

        @Override
        public PlaybackActionView getPlaybackActionView() {
            return mBaseContainerBinding.playbackAction;
        }

        @Override
        public PlayerButtonView getStartOverButton() {
            return mBinding.buttonsContainer.startOver;
        }

        @Override
        public PlayerButtonView getOptionsButton() {
            return mBinding.buttonsContainer.optionsButton;
        }

        @Override
        public PlayerButtonView getMoreInfoButton() {
            return mBinding.buttonsContainer.moreInfoButton;
        }

        @Override
        public FontTextView getTitleView() {
            return mBinding.detailsLayout.programTitle;
        }

        @Override
        public ImageView getLogoView() {
            return mBaseContainerBinding.logo;
        }

        @Override
        public BaseOrderedIconsContainer getBaseOrderedIconsContainer() {
            return mBinding.detailsLayout.iconsContainer;
        }
    };
}
