/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.startup.fragment;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;

/**
 * Base class for all the startup screens.
 *
 * @author Barabas Attila
 * @since 2018.06.29
 */
public abstract class StartFragment extends BaseFragment {
    public static final String TAG = Log.tag(StartFragment.class);

    protected StepCallback mStepCallback;

    private static final int PAGE_FADE_OUT_DURATION = 1000;

    public abstract void updateLayout();

    public Animator getFadeOutAnimation() {
        Animator animator = ObjectAnimator.ofFloat(getView(), View.ALPHA, 1f, 0f);
        animator.setDuration(PAGE_FADE_OUT_DURATION);
        return animator;
    }

    public void setStepCallback(StepCallback stepCallback) {
        mStepCallback = stepCallback;
    }
}
