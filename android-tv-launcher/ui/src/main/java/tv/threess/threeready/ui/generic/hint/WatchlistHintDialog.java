package tv.threess.threeready.ui.generic.hint;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import tv.threess.lib.di.Components;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.databinding.AddToWatchlistHintsDialogBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Dialog for Watchlist hints.
 *
 * @author Daniela Toma
 * @since 2021.01.25
 */
public class WatchlistHintDialog extends HintsDialog {
    
    private AddToWatchlistHintsDialogBinding mBinding; 

    public static WatchlistHintDialog newInstance(String hintId, boolean hintOpenedFromSettings) {
        Bundle args = new Bundle();
        args.putString(EXTRA_HINT_ID, hintId);
        args.putBoolean(EXTRA_HINT_OPENED_FROM_SETTINGS, hintOpenedFromSettings);
        WatchlistHintDialog fragment = new WatchlistHintDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected View inflateView() {
        if (mBinding == null) {
            mBinding = AddToWatchlistHintsDialogBinding.inflate(LayoutInflater.from(getContext()));
        }

        return mBinding.getRoot();
    }

    @NonNull
    @Override
    protected ImageView getBackgroundImage() {
        return mBinding.background;
    }

    @NonNull
    @Override
    protected TextView getNextButton() {
        return mBinding.buttonContainer.nextBtn;
    }

    @NonNull
    @Override
    protected TextView getPreviousButton() {
        return mBinding.buttonContainer.previousBtn;
    }

    @Override
    protected void initViews() {
        String description = mTranslator.get(TranslationKey.HINT_TEXT_WATCHLIST);
        mBinding.description.setText(description);
        mBinding.buttonContainer.nextBtn.setText(mTranslator.get(TranslationKey.HINTS_BUTTON_GO_TO_WATCHLIST));

        mBinding.buttonContainer.nextBtn.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.HINT_TEXT_WATCHLIST))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY, description)
                        .toString()
        );

        UiUtils.loadImage(mTranslator, mTranslator.get(TranslationKey.WATCHLIST_HINT_IMAGE), mBinding.background);

        mBinding.buttonContainer.nextBtn.setOnClickListener(l -> {
            Navigator mNavigator = Components.get(Navigator.class);
            dismiss();
            mNavigator.showMyVideos();
        });
    }

    @Override
    protected boolean handlesNavigationButtons(KeyEvent event) {
        return (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER);
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER) {
            mBinding.buttonContainer.nextBtn.performClick();
            return true;
        }
        return super.onKeyUp(event);
    }
}
