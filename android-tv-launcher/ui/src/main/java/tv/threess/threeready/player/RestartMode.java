package tv.threess.threeready.player;

/**
 * Controls if the currently running stream needs to be restarted or not,
 * in case the same channel is tuned again.
 */
public enum RestartMode {

    /**
     * Does not restart the currently playing live stream if the same channelId is used for tuning.
     */
    LiveChannel,

    /**
     * Does not restart the currently playing live, replay or recording stream if the same channelId is used for tuning.
     */
    Channel,

    /**
     * Force restart the stream even if the same channel is started.
     */
    Forced,
}
