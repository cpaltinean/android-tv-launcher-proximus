package tv.threess.threeready.ui.startup.step;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;

/**
 * Shows screen after stand by is finished and removes loading screen
 *
 *
 * @author Solyom Zsolt
 * @since 17/02/2020
 */
public class StandByCompletedStep extends BaseStartupFinishedStep {

    @Override
    protected void showContentFragmentAfterStartup() {
        PlaybackDetailsManager playbackDetailsManager = Components.get(PlaybackDetailsManager.class);
        IContentItem playingItem = playbackDetailsManager.getPlayingItem();

        if (playingItem != null) {
            Log.event(new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageSelection)
                .addDetail(UILogEvent.Detail.Page, UILog.Page.StandBy)
                .addDetail(UILogEvent.Detail.OpenedBy, UILog.OpenedBy.Standby));
        }

        if (mFeatureControl.shouldOpenHomeAfterStandBy()) {
            mNavigator.showHome();
        } else {
            mNavigator.showPlayerUI();
        }
    }
}
