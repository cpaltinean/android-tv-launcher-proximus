package tv.threess.threeready.ui.epggrid.presenter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.leanback.widget.Presenter;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.api.netflix.model.SourceType;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.pvr.model.SeriesRecordingStatus;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.BroadcastViewBinding;
import tv.threess.threeready.ui.details.DetailOpenedFrom;
import tv.threess.threeready.ui.epggrid.listener.EpgBroadcastSelectedListener;
import tv.threess.threeready.ui.epggrid.view.EpgTimeLineView;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.pvr.RecordingActionHelper;

/**
 * Presenter to display a program card in the grid.
 *
 * @author Barabas Attila
 * @since 15/06/2021
 */
public class EpgProgramPresenter extends BasePresenter<EpgProgramPresenter.ViewHolder, IBroadcast> {
    private static final String TAG = Log.tag(EpgProgramPresenter.class);

    private static final int MAX_RECYCLER_VIEW_COUNT = 100;
    private static final String SHORT_PROGRAM_TITLE = "...";
    private static final long MIN_DURATION_FOR_TITLE = TimeUnit.MINUTES.toMillis(5);
    private static final long MIN_DURATION_FOR_ICONS = TimeUnit.MINUTES.toMillis(15);

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final RecordingActionHelper mRecordingActionHelper = Components.get(RecordingActionHelper.class);
    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);
    private final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);
    private final MwRepository mMwRepository = Components.get(MwRepository.class);

    private final int mItemSpacing;

    private final EpgBroadcastSelectedListener mBroadcastSelectedListener;
    private final EpgTimeLineView mTimeline;

    public EpgProgramPresenter(Context context, EpgTimeLineView epgTimeline, EpgBroadcastSelectedListener listener) {
        super(context);
        mTimeline = epgTimeline;
        mBroadcastSelectedListener = listener;
        mItemSpacing = context.getResources().getDimensionPixelOffset(R.dimen.epg_grid_item_spacing);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        BroadcastViewBinding binding = BroadcastViewBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.program.setBackground(createBackgroundStateListDrawable());
        holder.mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, IBroadcast broadcast) {
        holder.mBinding.program.setBroadcast(broadcast);
        updateCardParams(holder, broadcast);
        updateParentalRatingListener(holder);
        updateTitle(holder, broadcast);
        updateIconContainer(holder, broadcast);
        updateContentDescription(holder, broadcast);
    }

    /**
     * Calculate the program view size based on the broadcast duration.
     */
    private void updateCardParams(ViewHolder holder, IBroadcast broadcast) {
        // set card size
        ViewGroup.LayoutParams params = holder.mBinding.program.getLayoutParams();
        params.width = mTimeline.convertMillisToPixel(mTimeline.getDurationInWindow(broadcast.getStart(), broadcast.getEnd()))
                - mItemSpacing * 2;
        holder.mBinding.program.requestLayout();
    }

    /**
     * Display the broadcast title.
     */
    private void updateTitle(ViewHolder holder, IBroadcast broadcast) {
        if (shouldHideTitle(broadcast)) {
            holder.mBinding.title.setText(SHORT_PROGRAM_TITLE);
            return;
        }

        if (mParentalControlManager.isRestricted(broadcast)) {
            holder.mBinding.title.setText(mTranslator.get(TranslationKey.CARDS_TITLE_CONTENT_LOCKED));
            return;
        }

        holder.mBinding.title.setText(broadcast.getTitle());
    }

    private void updateIconContainer(ViewHolder holder, IBroadcast broadcast) {
        if (shouldHideIcons(broadcast)) {
            holder.mBinding.iconContainer.setVisibility(View.GONE);
            return;
        }

        holder.mBinding.iconContainer.setVisibility(View.VISIBLE);
        updateRecordingIcon(holder, broadcast);
    }

    /**
     * Check if the broadcast is to short for title to be displayed.
     */
    private boolean shouldHideTitle(IBroadcast broadcast) {
        return mTimeline.getDurationInWindow(
                broadcast.getStart(), broadcast.getEnd()) <= MIN_DURATION_FOR_TITLE;
    }

    /**
     * Check if the broadcast is to short for icons to be displayed.
     */
    private boolean shouldHideIcons(IBroadcast broadcast) {
        return mTimeline.getDurationInWindow(
                broadcast.getStart(), broadcast.getEnd()) <= MIN_DURATION_FOR_ICONS;
    }

    /**
     * Display the recording icon.
     */
    private void updateRecordingIcon(ViewHolder holder, IBroadcast broadcast) {
        RxUtils.disposeSilently(holder.mRecStatusDisposable);
        holder.mRecStatusDisposable = mPvrRepository.getRecordingStatus(broadcast)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(holder.mBinding.iconContainer::showRecordingStatus
                        , throwable -> {
                            Log.e(TAG, "Couldn't get the recording status.", throwable);
                            holder.mBinding.iconContainer.hideRecordingStatus();
                        });
    }

    private void updateParentalRatingListener(ViewHolder holder) {
        if (holder.mParentalControlListener == null) {
            holder.mParentalControlListener = new ParentalControlListener(holder);
        }

        mParentalControlManager.removeListener(holder.mParentalControlListener);
        mParentalControlManager.addListener(holder.mParentalControlListener);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);
        holder.mBinding.program.setBroadcast(null);
        RxUtils.disposeSilently(holder.mRecStatusDisposable);
        mParentalControlManager.removeListener(holder.mParentalControlListener);
    }

    @Override
    public void onFocusedState(ViewHolder holder, IBroadcast iBroadcast) {
        super.onFocusedState(holder, iBroadcast);

        if (mBroadcastSelectedListener != null) {
            mBroadcastSelectedListener.onBroadcastSelected(iBroadcast);
        }
    }

    @Override
    public void onClicked(ViewHolder holder, IBroadcast broadcast) {
        if (broadcast.isGenerated()) {
            // No detail page for loading programs.
            return;
        }

        Log.event(ReportUtils.createSelectionSelection(broadcast));

        if (mParentalControlManager.isRestricted(broadcast)) {
            // Content blocked. Request PIN.
            mNavigator.showParentalControlUnblockDialog();
            return;
        }

        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        if (channel != null) {
            if (channel.getType() == ChannelType.APP) {
                return;
            }

            if (channel.getType() == ChannelType.REGIONAL && mNavigator.getPlayerFragment() != null) {
                mNavigator.getPlayerFragment().goToChannel(StartAction.Ui, channel.getId(), false);
                return;
            }
        }

        RecordingStatus recordingStatus = holder.mBinding.iconContainer.getRecordingStatus();
        if (recordingStatus != null && recordingStatus.getRecording() != null
                && recordingStatus.getSeriesRecordingStatus() == SeriesRecordingStatus.SCHEDULED) {
            mNavigator.showSeriesRecordingDetailPage(recordingStatus.getRecording(), DetailOpenedFrom.Default);
        } else {
            mNavigator.showProgramDetails(broadcast);
        }
    }

    @Override
    public boolean onKeyEvent(ViewHolder holder, IBroadcast broadcast, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER) {
            TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
            if(channel != null && channel.getType() == ChannelType.APP) {
                mNavigator.checkSubscriptionAndOpenApp(StartAction.Ui, channel, channel.isFavorite() ?
                        SourceType.FavoriteChannel : SourceType.Channel);
                return true;
            }
        }

        if (event.getAction() == KeyEvent.ACTION_UP) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    holder.view.setPressed(true);
                    break;
                case KeyEvent.KEYCODE_MEDIA_RECORD:
                    mRecordingActionHelper.handleRecKeyAction(broadcast,
                            holder.mBinding.iconContainer.getRecordingStatus());
                    return true;
            }
        }

        return super.onKeyEvent(holder, broadcast, event);
    }

    @Override
    public int getMaxRecyclerViewCount() {
        return MAX_RECYCLER_VIEW_COUNT;
    }

    @Override
    public long getStableId(IBroadcast broadcast) {
        return Objects.hash(broadcast.getId());
    }

    private Drawable createBackgroundStateListDrawable() {
        int[] focusedStateList = new int[]{android.R.attr.state_focused};
        int[] defaultStateList = new int[]{};

        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(focusedStateList, createBackgroundDrawable(
                mLayoutConfig.getButtonFocusedColorStart(), mLayoutConfig.getButtonFocusedColorEnd()));
        stateListDrawable.addState(defaultStateList, createBackgroundDrawable(
                mLayoutConfig.getButtonNoStateColorStart(), mLayoutConfig.getButtonNoStateColorEnd()));
        return stateListDrawable;
    }

    private Drawable createBackgroundDrawable(int colorStart, int colorEnd) {
        GradientDrawable drawable = new GradientDrawable(
                GradientDrawable.Orientation.LEFT_RIGHT, new int[]{colorStart, colorEnd});
        int radius = mContext.getResources().getDimensionPixelOffset(R.dimen.epg_program_background_radius);
        drawable.setCornerRadii(new float[]{radius, radius, radius, radius, radius, radius, radius, radius});
        return drawable;
    }

    /**
     * Updates content description for Talkback.
     */
    private void updateContentDescription(ViewHolder holder, IBroadcast broadcast) {
        if (!mMwRepository.isTalkBackOn()) {
            return;
        }

        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());

        String contentDescription = mTranslator.get(mTranslator.get(TranslationKey.TALKBACK_EPG_TILE_LIVE));
        if (broadcast.isPast()) {
            contentDescription = mTranslator.get(mTranslator.get(TranslationKey.TALKBACK_EPG_TILE_PAST));
        } else if (broadcast.isFuture()) {
            contentDescription = mTranslator.get(mTranslator.get(TranslationKey.TALKBACK_EPG_TILE_FUTURE));
        }

        holder.view.setContentDescription(
                new TalkBackMessageBuilder(contentDescription)
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_CHANNEL_NAME, channel != null ? channel.getName() : "")
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_ASSET, broadcast.getTitle())
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_START_TIME, LocaleTimeUtils.getStartDate(broadcast, mTranslator, mLocaleSettings))
                        .toString()
        );
    }

    private class ParentalControlListener implements ParentalControlManager.IParentalControlListener {

        ViewHolder mHolder;

        public ParentalControlListener(ViewHolder holder) {
            mHolder = holder;
        }

        @Override
        public void onParentalRatingChanged(ParentalRating parentalRating) {
            updateTitle(mHolder, mHolder.mBinding.program.getBroadcast());
        }
    }

    public static class ViewHolder extends Presenter.ViewHolder {

        private final BroadcastViewBinding mBinding;
        private ParentalControlListener mParentalControlListener;

        private Disposable mRecStatusDisposable;

        public ViewHolder(@NonNull BroadcastViewBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
