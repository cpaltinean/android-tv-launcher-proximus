/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;

import java.util.HashSet;
import java.util.Set;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.MemoryCache;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.ui.utils.SystemUtils;

/**
 * Global key receiver to handle the Tv, Svt and Netflix remote buttons.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.07.24
 */
public class GlobalKeyReceiver extends BroadcastReceiver {
    private static final String TAG = Log.tag(GlobalKeyReceiver.class);

    private static final String ACTION_GLOBAL_BUTTON = "android.intent.action.GLOBAL_BUTTON";

    private static final int KEYCODE_NETFLIX_BUTTON = KeyEvent.KEYCODE_BUTTON_3;

    //if the launcher receives two key events within this interval the second one will be ignored
    private static final long THRESHOLD = 50;

    //variable to store the last key event to prevent flicker after rc sleep
    private static KeyEvent mLastKeyEvent;

    // keys that require a bluetooth connection for the remote (won't work on IR)
    private final Set<Integer> mBluetoothKeys;

    public GlobalKeyReceiver() {
        mBluetoothKeys = new HashSet<>();
        mBluetoothKeys.add(KeyEvent.KEYCODE_F3);
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        Log.d(TAG, "onReceive. Intent : " + intent);
        MemoryCache.LastUserActivity.edit().put(System.currentTimeMillis());

        PlaybackDetailsManager playbackManager =  Components.getOrNull(PlaybackDetailsManager.class);
        Log.d(TAG, "Injected playbackManager: " + playbackManager);

        if (ACTION_GLOBAL_BUTTON.equals(intent.getAction())) {
            KeyEvent event = intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);

            // process only when key was released.
            if (event == null || event.getAction() != KeyEvent.ACTION_UP) {
                return;
            }

            if (mLastKeyEvent != null && event.getKeyCode() == mLastKeyEvent.getKeyCode() &&
                    event.getEventTime() - mLastKeyEvent.getEventTime() < THRESHOLD) {
                Log.d(TAG, "ignoring key event: " + event);
                return;
            } else {
                mLastKeyEvent = event;
            }

            if (!Settings.authenticated.get(false) && event.getKeyCode() != KEYCODE_NETFLIX_BUTTON) {
                Log.d(TAG, "User is not logged in/registered");
                return;
            }

            // Check if remote is pairing with bluetooth.
            if (mBluetoothKeys.contains(event.getKeyCode())) {
                Intent broadcastIntent = new Intent(RemoteNotPairedReceiver.ACTION_RECEIVED_KEY);
                context.sendBroadcast(broadcastIntent);
            }

            // Open application for key.
            String packageName;
            StartAction source;

            Log.d(TAG, "Key event intercepted via GlobalKeyReceiver: " + event);
            switch (event.getKeyCode()) {
                case KEYCODE_NETFLIX_BUTTON:
                    UILog.logButtonPressFromRemote(UILog.ButtonType.NETFLIX);
                    packageName = PackageUtils.PACKAGE_NAME_NETFLIX;
                    source = StartAction.BtnApp;

                    if (playbackManager != null && !playbackManager.isInitialized()) {
                        playbackManager = null;
                    }
                    break;

                case KeyEvent.KEYCODE_GUIDE:
                    UILog.logButtonPressFromRemote(UILog.ButtonType.GUIDE);
                    packageName = context.getApplicationContext().getPackageName();
                    source = StartAction.BtnGuide;
                    playbackManager = null;
                    break;

                case KeyEvent.KEYCODE_TV_RADIO_SERVICE:
                    UILog.logButtonPressFromRemote(UILog.ButtonType.RADIO);
                    packageName = context.getApplicationContext().getPackageName();
                    source = StartAction.BtnRadio;
                    playbackManager = null;
                    break;

                default:
                    return;
            }

            // Log app open event.
            if (packageName != null) {
                // playback manager is not available (there is no playback in fti)
                if (playbackManager != null) {
                    playbackManager.startApplication(source, packageName, intent.getExtras());
                } else {
                    try {
                        PackageUtils.openApplication(context, packageName, intent.getExtras());
                        if (!(packageName.equalsIgnoreCase(context.getApplicationContext().getPackageName()))) {
                            Log.event(new Event<>(UILogEvent.AppStarted)
                                    .putDetail(UILogEvent.Detail.StartAction, source)
                                    .putDetail(UILogEvent.Detail.PackageName, packageName)
                            );
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "failed to start application", e);
                    }
                }
            }
        }
    }

    // Disable remote Netflix, STV, etc button
    public static void disable(Context context) {
        SystemUtils.disableComponent(context, GlobalKeyReceiver.class);
    }

    public static void enable(Context context) {
        SystemUtils.enableComponent(context, GlobalKeyReceiver.class);
    }
}