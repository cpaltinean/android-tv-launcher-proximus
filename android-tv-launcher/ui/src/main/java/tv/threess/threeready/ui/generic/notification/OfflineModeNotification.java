/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.generic.notification;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.home.model.generic.OfflineModeSettings;
import tv.threess.threeready.ui.generic.dialog.ErrorCode;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Offline mode notification which displayed in case there is no internet connection.
 *
 * @author Barabas Attila
 * @since 2017.08.19
 */
public class OfflineModeNotification extends Notification {

    private static final int NOTIFICATION_PRIORITY = 100;

    public static OfflineModeNotification newInstance() {
        Bundle args = new Bundle();

        OfflineModeNotification fragment = new OfflineModeNotification();
        fragment.setArguments(args);
        return fragment;
    }

    Activity mActivity;

    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);
    private final OfflineModeSettings mOfflineModeSettings = Components.get(OfflineModeSettings.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final NotificationManager mNotificationManager = Components.get(NotificationManager.class);

    long mRedisplayPeriod;

    public OfflineModeNotification() {
        mRedisplayPeriod = TimeUnit.SECONDS.toMillis(
                mOfflineModeSettings.getNotificationRedisplayPeriod());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        NotificationModel model = new NotificationModel(mTranslator.get(TranslationKey.ERR_NO_INTERNET),
                ErrorCode.ERROR_CODE_NO_CONNECTION, 0, Timeout.LENGTH_PERMANENT, false, NOTIFICATION_PRIORITY);

        getArguments().putSerializable(NOTIFICATION_MODEL_EXTRA, model);

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Cancel automatic redisplay.

        if (mNotificationManager.getTimeoutHandler() != null) {
            mNotificationManager.getTimeoutHandler().removeCallbacks(mRedisplayRunnable);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // Reschedule offline notification display after the given period.
        if (mNotificationManager.getTimeoutHandler() != null && !mInternetChecker.isInternetAvailable()) {
            mNotificationManager.getTimeoutHandler().postDelayed(mRedisplayRunnable, mRedisplayPeriod);
        }
    }


    /**
     * Enable to show automatically the offline mode notification when the internet connection lost.
     */
    public void enableAutoShow(Activity activity) {
        mActivity = activity;

        if (mInternetChecker.isInternetAvailable()) {
            mNotificationManager.hideNotification(this);
        } else {
            mNotificationManager.showNotification(this);
        }
    }

    /**
     * Disable the automatic offline notification display mechanism.
     */
    public void disableAutoShow() {
        // Cancel automatic redisplay.
        if (mNotificationManager.getTimeoutHandler() != null) {
            mNotificationManager.getTimeoutHandler().removeCallbacks(mRedisplayRunnable);
        }
    }

    /**
     * Displays the notification.
     */
    public void showNotification() {
        mNotificationManager.showNotification(this);
    }

    /**
     * Redisplay the notification after X seconds when it becomes hidden but the issue still persists.
     */
    private final Runnable mRedisplayRunnable = () -> {
        if (!mInternetChecker.isInternetAvailable()) {
            showNotification();
        }
    };
}
