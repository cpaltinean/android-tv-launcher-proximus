package tv.threess.threeready.ui.home.presenter.module.stripe.mixed;

import android.content.Context;
import android.text.TextUtils;

import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.pvr.model.IBaseRecordingSeries;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.ReplayChannelPageItem;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IBaseVodSeries;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.generic.presenter.BaseModularCardPresenter;
import tv.threess.threeready.ui.generic.presenter.EmptyPresenter;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.home.presenter.card.app.EditorialA1CardPresenter;
import tv.threess.threeready.ui.home.presenter.module.stripe.generic.StripeModulePresenter;
import tv.threess.threeready.ui.pvr.presenter.SeriesRecordingA1CardPresenter;
import tv.threess.threeready.ui.tv.presenter.broadcast.BroadcastA1LandscapeCardPresenter;
import tv.threess.threeready.ui.tv.presenter.channel.ReplayChannelPageCardPresenter;
import tv.threess.threeready.ui.tv.presenter.channel.TvChannelC1CardPresenter;
import tv.threess.threeready.ui.tv.presenter.vod.VodA1StripeCardPresenter;
import tv.threess.threeready.ui.tv.presenter.vod.VodSeriesA1BigCardPresenter;
import tv.threess.threeready.ui.tv.presenter.vod.VodSeriesA1SmallCardPresenter;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Presenter used to display mixed type, A variant stripes.
 *
 * @author Barabas Attila
 * @since 2017.06.11
 */
public class MixedStripeModulePresenter extends StripeModulePresenter {

    private final int mTopPadding;

    Map<ModuleCardVariant, PresenterSelector> mVariantCardPresenterMap = new HashMap<>();


    public MixedStripeModulePresenter(Context context, DataSourceLoader dataSourceLoader) {
        super(context, dataSourceLoader);
        // Mixed A1 stripe
        mTopPadding = mContext.getResources().getDimensionPixelOffset(R.dimen.mixed_stripe_top_padding);

        InterfacePresenterSelector presenterSelector = new InterfacePresenterSelector()
                .addClassPresenterSelector(IContentItem.class, new AlignmentPresenterSelector(
                        new BroadcastA1LandscapeCardPresenter(mContext),
                        new VodA1StripeCardPresenter(mContext)))
                .addClassPresenter(TvChannel.class, new TvChannelC1CardPresenter(context))
                .addClassPresenter(ReplayChannelPageItem.class, new ReplayChannelPageCardPresenter(context))
                .addClassPresenterSelector(IBaseVodSeries.class, new SmallLargeCardPresenterSelector(context))
                .addClassPresenter(IBaseRecordingSeries.class, new SeriesRecordingA1CardPresenter(context));
        presenterSelector.addClassPresenter(EditorialItem.class, new EditorialA1CardPresenter(context, presenterSelector));
        mVariantCardPresenterMap.put(ModuleCardVariant.A1, presenterSelector);
    }

    @Override
    protected PresenterSelector getCardPresenterSelector(ModuleConfig moduleConfig) {
        return mVariantCardPresenterMap.get(moduleConfig.getCardVariant());
    }

    /**
     * Gets the value for horizontal padding.
     */
    @Override
    protected int getHorizontalPadding() {
        return mContext.getResources().getDimensionPixelSize(R.dimen.stripe_title_left_margin);
    }

    /**
     * Gets the value for bottom padding.
     */
    @Override
    protected int getBottomPadding() {
        return 0;
    }

    /**
     * Gets the value for bottom margin.
     */
    @Override
    protected int getBottomMargin() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.mixed_stripe_module_bottom_margin);
    }

    /**
     * Gets the value for item spacing.
     */
    @Override
    protected int getItemSpacing(ModuleConfig moduleConfig) {
        // Use the module style as default.
        Float itemSpacing = null;
        if (moduleConfig.getModuleStyle() != null
                && moduleConfig.getModuleStyle().getItemStyle() != null) {
            itemSpacing = moduleConfig.getModuleStyle().getItemStyle().getPadding();
        }

        // Use default value
        if (itemSpacing == null) {
            return mContext.getResources().getDimensionPixelOffset(R.dimen.mixed_stripe_item_spacing);
        }

        return UiUtils.dpToPx(mContext, itemSpacing);
    }

    /**
     * Gets the value for row height.
     */
    @Override
    protected int getRowHeight(ModuleData<?> moduleData) {
        ModuleConfig moduleConfig = moduleData.getModuleConfig();

        Integer rowHeight = null;

        // Get the maximum height of the items
        for (Object data : moduleData.getItems()) {
            BaseModularCardPresenter presenter = (BaseModularCardPresenter)
                    getCardPresenterSelector(moduleConfig).getPresenter(data);
            if (presenter == null) {
                continue;
            }

            int itemHeight = presenter.getCardHeight(moduleData, data);
            if (rowHeight == null || rowHeight < itemHeight) {
                rowHeight = itemHeight;
            }
        }

        int topPadding = TextUtils.isEmpty(moduleData.getTitle()) ? 0 : mTopPadding;

        // Use default height.
        if (rowHeight == null) {
            rowHeight = mContext.getResources()
                    .getDimensionPixelOffset(R.dimen.mixed_stripe_row_height);
        }

        return rowHeight + topPadding;
    }

    /**
     * Gets the value for top margin.
     */
    @Override
    protected int getTopMargin() {
        return 0;
    }

    private static class AlignmentPresenterSelector extends PresenterSelector {
        private final BaseCardPresenter<?,?> mBroadcastLandscapeCardPresenter;
        private final BaseCardPresenter<?,?> mVodPortraitCardPresenter;

        AlignmentPresenterSelector(BaseCardPresenter<?,?> broadcastLandscapeCardPresenter,
                                   BaseCardPresenter<?,?> vodPortraitCardPresenter) {
            mBroadcastLandscapeCardPresenter = broadcastLandscapeCardPresenter;
            mVodPortraitCardPresenter = vodPortraitCardPresenter;
        }

        @Override
        public Presenter[] getPresenters() {
            return new Presenter[]{mBroadcastLandscapeCardPresenter, mVodPortraitCardPresenter};
        }

        @Override
        public Presenter getPresenter(Object item) {
            if (item instanceof IBroadcast) {
                return mBroadcastLandscapeCardPresenter;
            }

            if (item instanceof IBaseVodItem) {
                return mVodPortraitCardPresenter;
            }

            return new EmptyPresenter();
        }
    }

    private static class SmallLargeCardPresenterSelector extends PresenterSelector {
        private final BaseCardPresenter<?,?> mSmallCardPresenter;
        private final BaseCardPresenter<?,?> mLargeCardPresenter;

        SmallLargeCardPresenterSelector(Context context) {
            mSmallCardPresenter = new VodSeriesA1SmallCardPresenter(context);
            mLargeCardPresenter = new VodSeriesA1BigCardPresenter(context);
        }

        @Override
        public Presenter[] getPresenters() {
            return new Presenter[]{mSmallCardPresenter, mLargeCardPresenter};
        }

        @Override
        public Presenter getPresenter(Object obj) {
            IBaseVodSeries item = (IBaseVodSeries) obj;
            if (item.isSmallCardType()) {
                return mSmallCardPresenter;
            }
            return mLargeCardPresenter;
        }
    }
}
