package tv.threess.threeready.ui.generic.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.PlayOption;
import tv.threess.threeready.api.generic.model.VideoQuality;
import tv.threess.threeready.api.vod.model.CurrencyType;
import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.model.IVodVariant;

/**
 * Vod wrapper class which holds all the necessary information to start a VOD playback.
 *
 * @author Barabas Attila
 * @since 2018.12.17
 */
public class VodPlayOption extends PlayOption {

    private final IVodItem mVod;
    private final IVodVariant mVodVariant;
    private final IVodSeries mVodSeries;
    private final IVodPrice mVodPrice;
    private final String mAudioLanguage;


    public VodPlayOption(PlayOption.Type type, IVodItem vod,
                         IVodVariant vodVariant, String audioLanguage) {
        this(type, vod, vodVariant, null, null, audioLanguage);
    }

    public VodPlayOption(PlayOption.Type type, IVodItem vod,
                         IVodVariant vodVariant, IVodSeries vodSeries, IVodPrice vodPrice, String audioLanguage) {
        super(type);
        mVod = vod;
        mVodVariant = vodVariant;
        mVodSeries = vodSeries;
        mVodPrice = vodPrice;
        mAudioLanguage = audioLanguage;
    }

    public IVodItem getVod() {
        return mVod;
    }

    public IVodVariant getVodVariant() {
        return mVodVariant;
    }

    @Nullable
    public IVodSeries getVodSeries() {
        return mVodSeries;
    }

    public IVodPrice getVodPrice() {
        return mVodPrice;
    }

    public String getAudioLanguage() {
        return mAudioLanguage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VodPlayOption that = (VodPlayOption) o;
        return Objects.equals(mVod, that.mVod) &&
                Objects.equals(mVodVariant, that.mVodVariant) &&
                Objects.equals(mVodPrice, that.mVodPrice) &&
                Objects.equals(mAudioLanguage, that.mAudioLanguage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mVod, mVodVariant, mAudioLanguage);
    }


    /**
     * Generate all the available playback option for a VOD.
     *
     * @param vod             The VOD for which the playback options should be generated.
     * @param variantOfferMap A map which contains all the available rent offer for the VOD variants.
     * @return A list of playback options of the VOD which can be started.
     */
    public static List<VodPlayOption> generatePlayOptions(IVodItem vod, @NonNull Map<String, IRentOffer> variantOfferMap) {
        return generatePlayOptions(vod, null, variantOfferMap);
    }

    public static List<VodPlayOption> generatePlayOptions(IVodItem vod, IVodSeries vodSeries, @NonNull Map<String, IRentOffer> variantOfferMap) {
       List<VodPlayOption> playOptions = new ArrayList<>();

        // Add subscribed play options.
        for (IVodVariant vodVariant : vod.getSubscribedVariants()) {
            IRentOffer offer = variantOfferMap.get(vodVariant.getId());
            IVodPrice price = offer == null ? null : offer.getPriceForCurrency(CurrencyType.Euro);
            if (price != null) {
                for (String audioLanguage : vodVariant.getAudioLanguages()) {
                    playOptions.add(new VodPlayOption(PlayOption.Type.INCLUDED, vod, vodVariant, vodSeries, price, audioLanguage));
                }
            }
        }

        // Add free play options
        for (IVodVariant vodVariant : vod.getVariantList()) {
            if (vodVariant.isSubscribed()) {
                continue;
            }

            IRentOffer rentOffer = variantOfferMap.get(vodVariant.getId());
            IVodPrice price = rentOffer == null ? null : rentOffer.getPriceForCurrency(CurrencyType.Euro);
            if (rentOffer != null && price != null && rentOffer.isFree()) {
                for (String audioLanguage : vodVariant.getAudioLanguages()) {
                    playOptions.add(new VodPlayOption(PlayOption.Type.INCLUDED, vod, vodVariant, vodSeries, price, audioLanguage));
                }
            }
        }

        // Add rented play options.
        for (IVodVariant vodVariant : vod.getRentedVariants()) {
            if (vodVariant.isSubscribed()) {
                continue;
            }

            for (String audioLanguage : vodVariant.getAudioLanguages()) {
                playOptions.add(new VodPlayOption(
                        PlayOption.Type.RENTED, vod, vodVariant, audioLanguage));
            }
        }

        // TODO : check the case when there is no audio profile available.
        // keep the playback options with the best quality with the same audio language and subtitle
        playOptions.removeIf(vodPlayOption -> {
            for (VodPlayOption playOption : playOptions) {
                final String language = playOption.getAudioLanguage();
                final List<String> subtitles = playOption.getVodVariant().getSubtitleLanguages();
                final VideoQuality quality = playOption.getVodVariant().getHighestQuality();

                if (!language.equals(vodPlayOption.getAudioLanguage())) {
                    // skip options with different audio languages
                    continue;
                }

                IVodVariant variant = vodPlayOption.getVodVariant();
                if (!ArrayUtils.isEqualCollection(subtitles, variant.getSubtitleLanguages())) {
                    // skip options with different subtitles
                    continue;
                }

                if (quality.ordinal() > variant.getHighestQuality().ordinal()) {
                    // remove the playback options if it has a higher quality
                    return true;
                }
            }
            return false;
        });

        return playOptions;
    }
}
