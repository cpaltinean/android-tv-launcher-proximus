/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.tv.presenter.channel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.ChannelCardBinding;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;

/**
 * Abstract base presenter of channel type and C1-C3-C5 variant card.
 *
 * Created by Szilard on 5/2/2017.
 */

public class TvChannelCCardPresenter extends BaseChannelCardPresenter<TvChannelCCardPresenter.ViewHolder> {
    private static final int MAX_RECYCLER_VIEW_COUNT = 50;

    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    TvChannelCCardPresenter(Context context) {
        super(context);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        ChannelCardBinding binding = ChannelCardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.background.setBackgroundColor(mLayoutConfig.getPlaceHolderColor());
        holder.mBinding.logoText.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBinding.number.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        return holder;
    }

    @Override
    public int getMaxRecyclerViewCount() {
        return MAX_RECYCLER_VIEW_COUNT;
    }

    @Override
    public int getCardWidth(ModuleData<?> moduleData, TvChannel channel) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_channel_card_width);
    }

    @Override
    public int getCardHeight(ModuleData<?> moduleData, TvChannel tvChannel) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_channel_card_height);
    }

    public static class ViewHolder extends BaseCardPresenter.ViewHolder {

        protected final ChannelCardBinding mBinding;

        public ViewHolder(ChannelCardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
