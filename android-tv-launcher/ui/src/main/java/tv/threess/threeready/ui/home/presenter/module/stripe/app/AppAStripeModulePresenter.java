package tv.threess.threeready.ui.home.presenter.module.stripe.app;

import android.content.Context;

import androidx.core.content.ContextCompat;
import androidx.leanback.widget.PresenterSelector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tv.threess.threeready.api.gms.model.InstalledAppInfo;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.adapter.ModularItemBridgeAdapter;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeLabel;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeSeparatorProvider;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.home.presenter.card.app.AppA1CardPresenter;
import tv.threess.threeready.ui.home.presenter.card.app.EditorialA1CardPresenter;

/**
 * Presenter used to display application type, A variant stripes.
 *
 * @author Barabas Attila
 * @since 2017.06.11
 */
public class AppAStripeModulePresenter extends AppStripeModulePresenter {

    Map<ModuleCardVariant, PresenterSelector> mVariantCardPresenterMap = new HashMap<>();

    public AppAStripeModulePresenter(Context context, DataSourceLoader dataSourceLoader) {
        super(context, dataSourceLoader);

        InterfacePresenterSelector presenterSelector = new InterfacePresenterSelector();
        presenterSelector.addClassPresenter(InstalledAppInfo.class, new AppA1CardPresenter(context))
                .addClassPresenter(EditorialItem.class, new EditorialA1CardPresenter(context, presenterSelector));
        mVariantCardPresenterMap.put(ModuleCardVariant.A1, presenterSelector);
    }

    @Override
    protected StripeSeparatorProvider<StripeLabel> getLabelProvider(ModuleData<?> moduleData, ModularItemBridgeAdapter adapter) {
        return () -> {
            // Get the end of the features app section.
            int installedEndPos = adapter.getAdapter().size();
            int featureEndPos;
            for (featureEndPos = 0; featureEndPos < adapter.getAdapter().size(); ++featureEndPos) {
                InstalledAppInfo appInfo = (InstalledAppInfo) adapter.getAdapter().get(featureEndPos);
                if (!appInfo.isFeatured()) {
                    break;
                }
            }

            int storeEndPos;
            for (storeEndPos = featureEndPos; storeEndPos < adapter.getAdapter().size(); ++storeEndPos) {
                InstalledAppInfo appInfo = (InstalledAppInfo) adapter.getAdapter().get(storeEndPos);
                if (!appInfo.isStore()) {
                    break;
                }
            }

            // Define the features and all app sections.
            List<StripeLabel> labels = new ArrayList<>();
            if (featureEndPos > 0) {
                labels.add(new StripeLabel(0, featureEndPos, mTranslator.get(TranslationKey.MODULE_ANDROID_FEATURED_APPS),
                        ContextCompat.getDrawable(mContext, R.drawable.section_separator)));
            }
            if (storeEndPos > featureEndPos) {
                labels.add(new StripeLabel(featureEndPos, storeEndPos, mTranslator.get(TranslationKey.MODULE_ANDROID_GET_MORE_APPS),
                        ContextCompat.getDrawable(mContext, R.drawable.section_separator)));
            }

            if (installedEndPos > storeEndPos) {
                labels.add(new StripeLabel(storeEndPos, installedEndPos, mTranslator.get(TranslationKey.MODULE_ANDROID_APPS),
                        ContextCompat.getDrawable(mContext, R.drawable.section_separator)));
            }

            if (labels.size() == 1) {
                labels.get(0).setTitle(mTranslator.get(moduleData.getModuleConfig().getTitle()));
            }

            return labels;
        };
    }

    @Override
    protected PresenterSelector getCardPresenterSelector(ModuleConfig moduleConfig) {
        return mVariantCardPresenterMap.get(moduleConfig.getCardVariant());
    }
}
