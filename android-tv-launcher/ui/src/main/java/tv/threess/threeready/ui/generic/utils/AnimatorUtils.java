package tv.threess.threeready.ui.generic.utils;

import android.animation.Animator;

/**
 * Animator related utility methods.
 *
 * @author Zsombor
 * @since 2018.02.09
 */

public class AnimatorUtils {

    /**
     * Cancel the animations.
     */
    public static void cancelAnimators(Animator... animators) {
        for (Animator animator : animators) {
            if (animator != null
                    && animator.isRunning()) {
                animator.cancel();
            }
        }
    }

    /**
     * @return True if the current animator is running, false otherwise
     */
    public static boolean isRunning(Animator animator) {
        return animator != null && animator.isRunning();
    }
}
