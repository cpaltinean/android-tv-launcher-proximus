package tv.threess.threeready.ui.settings.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.settings.model.ParentalControlItem;
import tv.threess.threeready.ui.settings.model.ParentalControlItem.ParentalControlItemType;
import tv.threess.threeready.ui.settings.model.ParentalControlLockItem;

/**
 * Parental control dialog showing parental control options
 *
 * @author Daniela Toma
 * @since 2019.05.23
 */
public class ParentalControlSettingsFragment extends BaseSettingsDialogFragment<Object> {
    public static final String TAG = Log.tag(ParentalControlSettingsFragment.class);

    private final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);

    public static ParentalControlSettingsFragment newInstance() {
        return new ParentalControlSettingsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null) {
            view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.settings_left_frame_color));
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mParentalControlManager.addListener(mParentalControlListener);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mParentalControlManager.removeListener(mParentalControlListener);
    }

    @Override
    protected void setTitle() {
        getTitleView().setText(mTranslator.get(TranslationKey.SETTINGS_PARENTAL_CONTROL));
    }

    @Override
    protected void initView() {
        List<Object> items = new ArrayList<>();

        ParentalControlItem parentalControlItem = new ParentalControlItem(
                mTranslator.get(TranslationKey.FTI_PARENTAL_CONTROL_TITLE), ParentalControlItemType.PARENTAL_CONTROL);
        ParentalControlItem changePINCodeItem = new ParentalControlItem(
                mTranslator.get(TranslationKey.SETTINGS_PARENTAL_CONTROL_CHANGE_PIN), ParentalControlItemType.CHANGE_PIN_CODE);

        items.add(parentalControlItem);

        // Add locked item.
        if (mParentalControlManager.isParentalControlEnabled()) {
            items.add(new ParentalControlLockItem(mParentalControlManager.getSelectedParentalRating()));
        }

        items.add(changePINCodeItem);

        setItems(items);
    }

    private void updateLockContent(ParentalRating parentalRating) {
        if (mAdapter == null) {
            // Not initialized yet.
            return;
        }

        // Check if already added.
        boolean alreadyAdded = false;
        for (Object item : mAdapter.getItems()) {
            if (item instanceof ParentalControlLockItem) {
                alreadyAdded = true;
                break;
            }
        }

        // Enabled. Add lock item if enabled.
        if (parentalRating != ParentalRating.Undefined) {
            // Add the lock content item.
            if (alreadyAdded) {
                mAdapter.replace(1, new ParentalControlLockItem(parentalRating));
            } else {
                mAdapter.add(1, new ParentalControlLockItem(parentalRating));
            }

        // Not enable. Remove the previously added lock item.
        } else if (alreadyAdded) {
            mAdapter.remove(mAdapter.get(1));
        }

        mAdapter.notifyArrayItemRangeChanged(0, 1);
    }

    private final ParentalControlManager.IParentalControlListener mParentalControlListener = this::updateLockContent;
}
