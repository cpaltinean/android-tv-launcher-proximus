/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.Error;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.search.activity.BaseGlobalSearchHandlerActivity;

/**
 * Created by Barabas Attila on 4/11/2017.
 */

public class BaseActivity extends FragmentActivity {
    private static final String TAG = Log.tag(BaseActivity.class);

    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);

    protected List<Disposable> mDisposableList = new ArrayList<>();

    private boolean mIsInBackground = true;
    private boolean mIsInstanceStateSaved;

    @Override
    @CallSuper
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Log.d(TAG, "onNewIntent. Intent : " + intent);
        mIsInstanceStateSaved = false;
        mIsInBackground = false;
    }

    @Override
    @CallSuper
    protected void onStart() {
        Log.d(TAG, "onStart");

        mIsInBackground = false;
        mIsInstanceStateSaved = false;

        super.onStart();

        // Subscribe to errors.
        Disposable disposable = mAccountRepository.subscribeToErrors(this::onError);
        mDisposableList.add(disposable);
    }

    /**
     * @param keyCode The key code to compare with.
     * @return True if the activity was started with the given key.
     */
    public boolean isStartedWithKey(int keyCode) {
        Intent intent = getIntent();
        KeyEvent keyEvent = intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
        return keyEvent != null && keyEvent.getKeyCode() == keyCode;
    }


    /**
     * @return True if the activity was started from a global search result.
     */
    public boolean wasStartedForSearchResult() {
        Intent intent = getIntent();
        return BaseGlobalSearchHandlerActivity.ACTION_SEARCH_RESULT.equals(intent.getAction());
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    @CallSuper
    protected void onStop() {
        Log.d(TAG, "onStop");

        super.onStop();

        mIsInBackground = true;

        RxUtils.disposeSilently(mDisposableList);
        mDisposableList.clear();
    }

    @Override
    @CallSuper
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        Log.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
        mIsInstanceStateSaved = true;
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    public boolean isInBackground() {
        return mIsInBackground;
    }

    /**
     * Callback to handle errors occurred on some part of the application.
     */
    public void onError(Error error) {
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        mIsInstanceStateSaved = false;
    }

    /**
     * Returns true after the instance state saved.
     */
    public boolean isInstanceStateSaved() {
        return mIsInstanceStateSaved;
    }
}
