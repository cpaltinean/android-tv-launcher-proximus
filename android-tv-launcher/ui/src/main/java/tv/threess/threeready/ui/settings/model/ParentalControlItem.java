package tv.threess.threeready.ui.settings.model;

/**
 * Entity class representing a parental control item.
 *
 * @author Daniela Toma
 * @since 2019.05.22
 */
public class ParentalControlItem {

    private final String mTitle;
    private final ParentalControlItemType mType;

    public ParentalControlItem(String title, ParentalControlItemType type) {
        mTitle = title;
        mType = type;
    }

    public String getTitle() {
        return mTitle;
    }

    public ParentalControlItemType getType() {
        return mType;
    }

    public enum ParentalControlItemType {
        PARENTAL_CONTROL,
        CHANGE_PIN_CODE,
        CHANGE_PURCHASE_PIN
    }
}
