package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import java.util.Objects;
import java.util.function.Consumer;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.home.model.generic.ContentMarkers;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.ui.databinding.VodMetadataPresenterBinding;
import tv.threess.threeready.ui.generic.model.VodPlayOption;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.vod.utils.VodPlayOptionsMetadataProvider;

/**
 * Presenter class for Vod Variant metadata info on Play Options dialog.
 *
 * @author Andrei Teslovan
 * @since 2018.12.13
 */
public class VodPlayOptionsMetadataPresenter extends BasePresenter<VodPlayOptionsMetadataPresenter.ViewHolder, VodPlayOption> {
    private static final String TAG = Log.tag(VodPlayOptionsMetadataPresenter.class);

    private final PlaybackDetailsManager mPlaybackManager = Components.get(PlaybackDetailsManager.class);
    private final VodRepository mVodRepository = Components.get(VodRepository.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    @NonNull
    private final Consumer<String> mDetailsProvider;

    public VodPlayOptionsMetadataPresenter(Context context, @NonNull Consumer<String> provider) {
        super(context);
        mDetailsProvider = provider;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        VodMetadataPresenterBinding binding = VodMetadataPresenterBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        holder.mBinding.infoRow.setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f));
        holder.mBinding.audioInfo.setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f));
        holder.mBinding.subtitlesInfo.setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f));
        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, VodPlayOption item) {
        updateTitle(holder, item.getVodVariant());
        updateMarkers(holder, item.getVodVariant());
        updateIconsRow(holder, item.getVodVariant());
        updateAudioInfo(holder, item.getAudioLanguage());
        updateSubtitleInfo(holder, item.getVodVariant());
        updateInfoRow(holder, item.getVodVariant());
        updateProgressBar(holder, item);
        updateContentDescription(holder, item);
    }

    /**
     * By the details provider we can update the play option card`s content description.
     */
    private void updateContentDescription(ViewHolder holder, VodPlayOption playOption) {
        IVodVariant vodVariant = playOption.getVodVariant();

        mDetailsProvider.accept(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_PLAY_OPTIONS))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TYPE, playOption.getType().name())
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE, VodPlayOptionsMetadataProvider.getTitle(mTranslator, vodVariant))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_CONTENT_MARKER, holder.mBinding.contentMarker.getText())
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_ICONS, holder.mBinding.iconContainer.getIconsDescription(mTranslator))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_AUDIO, VodPlayOptionsMetadataProvider.getAudioInfo(mTranslator, playOption.getAudioLanguage()))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_SUBTITLE, VodPlayOptionsMetadataProvider.getSubtitleInfo(mTranslator, vodVariant))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_INFO, VodPlayOptionsMetadataProvider.getInfoRow(mTranslator, mLocaleSettings, vodVariant))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_START_TIME, "")
                        .toString()
        );
    }

    private void updateProgressBar(ViewHolder holder, VodPlayOption playOption) {
        holder.mBinding.progressBar.setProgressData(null);
        holder.mBookmarkDisposable = mVodRepository.getBookmarkForVod(playOption.getVodVariant())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        holder.mBinding.progressBar::setProgressData,
                        throwable ->
                                Log.e(TAG, "Couldn't get the broadcast's last position.", throwable)
                );
    }

    private void updateTitle(ViewHolder holder, IVodVariant vodVariant) {
        holder.mBinding.title.setText(VodPlayOptionsMetadataProvider.getTitle(mTranslator, vodVariant));
        holder.mBinding.title.setVisibility(View.VISIBLE);
    }

    private void updateMarkers(ViewHolder holder, IVodVariant vodVariant) {
        // Display content marker
        holder.mBinding.contentMarker.showMarkers(vodVariant, ContentMarkers.TypeFilter.PlayOptions);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);

        RxUtils.disposeSilently(holder.mBookmarkDisposable);
    }

    private void updateIconsRow(ViewHolder holder, IVodVariant vodVariant) {
        holder.mBinding.iconContainer.showParentalRating(vodVariant.getParentalRating());
        updateDescriptiveAudioIcon(holder, vodVariant);
        updateDescriptiveSubtitleIcon(holder, vodVariant);
        if (vodVariant.isHD()) {
            holder.mBinding.iconContainer.showHDIcon();
        } else {
            holder.mBinding.iconContainer.hideHDIcon();
        }

        Log.d(TAG, "Is dolby available : " + mPlaybackManager.isDolbyAvailable());
        if (mPlaybackManager.isContentItemPlaying(vodVariant) && mPlaybackManager.isDolbyAvailable()) {
            holder.mBinding.iconContainer.showDolby();
        } else {
            holder.mBinding.iconContainer.hideDolby();
        }
    }

    private void updateDescriptiveAudioIcon(ViewHolder holder, IVodVariant vodVariant) {
        if (vodVariant.hasDescriptiveAudio()) {
            holder.mBinding.iconContainer.showDescriptiveAudio();
        } else {
            holder.mBinding.iconContainer.hideDescriptiveAudio();
        }
    }

    private void updateDescriptiveSubtitleIcon(ViewHolder holder, IVodVariant vodVariant) {
        if (vodVariant.hasDescriptiveSubtitle()) {
            holder.mBinding.iconContainer.showDescriptiveSubtitle();
        } else {
            holder.mBinding.iconContainer.hideDescriptiveSubtitle();
        }
    }

    private void updateAudioInfo(ViewHolder holder, String audioProfile) {
        String audioInfo = VodPlayOptionsMetadataProvider.getAudioInfo(mTranslator, audioProfile);
        holder.mBinding.audioInfo.setText(audioInfo);
        holder.mBinding.audioInfo.setVisibility(TextUtils.isEmpty(audioInfo) ? View.GONE : View.VISIBLE);
    }

    private void updateSubtitleInfo(ViewHolder holder, IVodVariant variant) {
        String info = VodPlayOptionsMetadataProvider.getSubtitleInfo(mTranslator, variant);
        holder.mBinding.subtitlesInfo.setText(info);
        holder.mBinding.subtitlesInfo.setVisibility(TextUtils.isEmpty(info) ? View.GONE : View.VISIBLE);
    }

    protected void updateInfoRow(ViewHolder holder, IVodVariant vodVariant) {
        String info = VodPlayOptionsMetadataProvider.getInfoRow(mTranslator, mLocaleSettings, vodVariant);
        holder.mBinding.infoRow.setText(info);
        holder.mBinding.infoRow.setVisibility(TextUtils.isEmpty(info) ? View.GONE : View.VISIBLE);
    }

    @Override
    public long getStableId(VodPlayOption vodPlayOption) {
        return Objects.hash(vodPlayOption);
    }

    public static class ViewHolder extends BasePresenter.ViewHolder {

        private final VodMetadataPresenterBinding mBinding;
        Disposable mBookmarkDisposable;

        public ViewHolder(VodMetadataPresenterBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
