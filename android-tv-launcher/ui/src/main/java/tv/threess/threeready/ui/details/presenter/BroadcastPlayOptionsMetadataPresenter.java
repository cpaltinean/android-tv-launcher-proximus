package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import java.util.function.Consumer;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.home.model.generic.ContentMarkers;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.ui.databinding.BroadcastMetadataPresenterBinding;
import tv.threess.threeready.ui.generic.model.BroadcastPlayOption;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
/**
 * Presenter class for broadcast metadata info on Play Options dialog.
 *
 * @author Andrei Teslovan
 * @since 2018.12.13
 */
public class BroadcastPlayOptionsMetadataPresenter extends BasePresenter<BroadcastPlayOptionsMetadataPresenter.ViewHolder, BroadcastPlayOption<IBroadcast>> {
    private static final String TAG = Log.tag(BroadcastPlayOptionsMetadataPresenter.class);

    private final PlaybackDetailsManager mPlaybackManager = Components.get(PlaybackDetailsManager.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);
    private final TvRepository mTvRepository = Components.get(TvRepository.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    private final boolean mContinueToWatch;

    @NonNull
    private final Consumer<String> mDetailProvider;

    public BroadcastPlayOptionsMetadataPresenter(Context context, @NonNull Consumer<String> provider, boolean continueToWatch) {
        super(context);
        mContinueToWatch = continueToWatch;
        mDetailProvider = provider;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        BroadcastMetadataPresenterBinding binding = BroadcastMetadataPresenterBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        holder.mBinding.infoRow.setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f));
        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, BroadcastPlayOption<IBroadcast> playOption) {
        updateTitle(holder, playOption.getContentItem());
        updateMarkers(holder, playOption);
        updateIconsRow(holder, playOption.getContentItem());
        updateRecordingStatus(holder, playOption);
        updateInfoRow(holder, playOption.getContentItem());
        updateProgressBar(holder, playOption);
        updateContentDescription(holder, playOption);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);

        holder.mBinding.iconsContainer.hideAll();
        RxUtils.disposeSilently(holder.mRecStatusDisposable, holder.mBookmarkDisposable);
    }

    private void updateTitle(ViewHolder holder, IBroadcast broadcast) {

        holder.mBinding.title.setText(broadcast
                .getEpisodeTitleWithSeasonEpisode(mTranslator, " "));
        holder.mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        holder.mBinding.title.setVisibility(View.VISIBLE);
    }

    private void updateMarkers(ViewHolder holder, BroadcastPlayOption<IBroadcast> playOption) {
        IBroadcast broadcast = playOption.getContentItem();
        // Display content marker
        holder.mBinding.contentMarker.showMarkers(broadcast, ContentMarkers.TypeFilter.PlayOptions);
    }

    private void updateIconsRow(ViewHolder holder, IBroadcast broadcast) {
        holder.mBinding.iconsContainer.showParentalRating(broadcast.getParentalRating());

        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        if (broadcast.canReplay(channel)) {
            holder.mBinding.iconsContainer.showReplay();
        } else {
            holder.mBinding.iconsContainer.hideReplay();
        }

        updateDescriptiveAudioIcon(holder, broadcast);
        updateDescriptiveSubtitleIcon(holder, broadcast);

        Log.d(TAG, "Is dolby available : " + mPlaybackManager.isDolbyAvailable());
        if (mPlaybackManager.isBroadcastPlaying(broadcast) && mPlaybackManager.isDolbyAvailable()) {
            holder.mBinding.iconsContainer.showDolby();
        } else {
            holder.mBinding.iconsContainer.hideDolby();
        }
    }

    private void updateDescriptiveAudioIcon(ViewHolder holder, IBroadcast broadcast) {
        if (broadcast.hasDescriptiveAudio()) {
            holder.mBinding.iconsContainer.showDescriptiveAudio();
        } else {
            holder.mBinding.iconsContainer.hideDescriptiveAudio();
        }
    }

    private void updateDescriptiveSubtitleIcon(ViewHolder holder, IBroadcast broadcast) {
        if (broadcast.hasDescriptiveSubtitle()) {
            holder.mBinding.iconsContainer.showDescriptiveSubtitle();
        } else {
            holder.mBinding.iconsContainer.hideDescriptiveSubtitle();
        }
    }

    protected void updateInfoRow(ViewHolder holder, IBroadcast broadcast) {
        String date = LocaleTimeUtils.getDate(broadcast, mTranslator, mLocaleSettings);
        holder.mBinding.infoRow.setText(date);
        holder.mBinding.infoRow.setVisibility(TextUtils.isEmpty(date) ? View.GONE : View.VISIBLE);
    }

    private void updateRecordingStatus(ViewHolder holder, BroadcastPlayOption<IBroadcast> playOption) {
        RxUtils.disposeSilently(holder.mRecStatusDisposable);
        holder.mBinding.iconsContainer.hideRecordingStatus();
        holder.mRecStatusDisposable = mPvrRepository.getRecordingStatus(playOption.getContentItem())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recordingStatus -> {
                    holder.mBinding.iconsContainer.showRecordingStatus(recordingStatus);
                    updateContentDescription(holder, playOption);
                }, throwable -> {
                    Log.e(TAG, "Couldn't get the recording status.", throwable);
                    holder.mBinding.iconsContainer.hideRecordingStatus();
                });
    }

    /**
     * By using the details provider we can set the content description for the Play Options card.
     */
    private void updateContentDescription(ViewHolder holder, BroadcastPlayOption<IBroadcast> playOption) {
        IBroadcast broadcast = playOption.getContentItem();

        mDetailProvider.accept(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_PLAY_OPTIONS))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TYPE, playOption.getType().name())
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE, broadcast.getEpisodeTitleWithSeasonEpisode(mTranslator, " "))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_CONTENT_MARKER, holder.mBinding.contentMarker.getText())
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_ICONS, holder.mBinding.iconsContainer.getIconsDescription(mTranslator))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_AUDIO, "")
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_SUBTITLE, "")
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_INFO, LocaleTimeUtils.getDate(broadcast, mTranslator, mLocaleSettings))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_START_TIME, LocaleTimeUtils.getStartDate(broadcast, mTranslator, mLocaleSettings))
                        .toString()
        );
    }

    private void updateProgressBar(ViewHolder holder, BroadcastPlayOption<IBroadcast> playOption) {
        RxUtils.disposeSilently(holder.mBookmarkDisposable);
        IBroadcast broadcast = playOption.getContentItem();
        if (!broadcast.isFuture()) {
            Observable<IBookmark> bookmarkObservable;

            switch (playOption.getType()) {
                case RECORDING:
                    bookmarkObservable = mTvRepository.getRecordingBookmarkForBroadcast(broadcast);
                    break;
                case REPLAY:
                    bookmarkObservable = mTvRepository.getReplayBookmarkForBroadcast(broadcast);
                    break;
                default:
                    bookmarkObservable = mTvRepository.getBookmarkForBroadcast(broadcast, mContinueToWatch);
            }

            holder.mBookmarkDisposable = bookmarkObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            holder.mBinding.progressBar::setProgressData,
                            throwable -> Log.e(TAG, "Couldn't get the broadcast's last position.", throwable));
        }
        holder.mBinding.progressBar.setProgressListener(() -> holder.mBinding.progressBar.setVisibility(View.GONE));
    }

    @Override
    public long getStableId(BroadcastPlayOption<IBroadcast> broadcastPlayOption) {
        return System.identityHashCode(broadcastPlayOption);
    }

    public static class ViewHolder extends BasePresenter.ViewHolder {

        private final BroadcastMetadataPresenterBinding mBinding;

        Disposable mBookmarkDisposable;
        Disposable mRecStatusDisposable;

        public ViewHolder(BroadcastMetadataPresenterBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
