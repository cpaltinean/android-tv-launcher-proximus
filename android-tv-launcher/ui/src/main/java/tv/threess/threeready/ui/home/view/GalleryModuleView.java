package tv.threess.threeready.ui.home.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.recyclerview.widget.RecyclerView;
import tv.threess.threeready.ui.databinding.GalleryModuleViewBinding;

import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

/**
 * View for displaying the gallery with a stripe
 *
 * @author Eugen Guzyk
 * @since 2018.09.26
 */

public class GalleryModuleView extends RelativeLayout {

    public GalleryModuleViewBinding mBinding;

    public GalleryModuleView(Context context) {
        this(context, null);
    }

    public GalleryModuleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GalleryModuleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    public void initialize() {
        mBinding = GalleryModuleViewBinding.inflate(LayoutInflater.from(getContext()), this);
        mBinding.moduleGridView.setHasFixedSize(true);
        mBinding.overviewGridView.setHasFixedSize(true);
    }

    public void setRecyclerViewPool(RecyclerView.RecycledViewPool pool) {
        mBinding.moduleGridView.setRecycledViewPool(pool);
    }

    public RecyclerView.RecycledViewPool getRecyclerViewPool() {
        return mBinding.moduleGridView.getRecycledViewPool();
    }

    @Override
    public void requestChildFocus(View child, View focused) {
        // Active stripe when focus received.
        setActivated(true);
        dispatchSetActivated(true);

        super.requestChildFocus(child, focused);
    }

    @Override
    public View focusSearch(View focused, int direction) {
        View view = super.focusSearch(focused, direction);

        if (mBinding.moduleGridView.indexOfChild(view) == NO_POSITION) {
            // Deactivate stripe on focus lost.
            dispatchSetActivated(false);
            setActivated(false);
        }

        return view;
    }

}
