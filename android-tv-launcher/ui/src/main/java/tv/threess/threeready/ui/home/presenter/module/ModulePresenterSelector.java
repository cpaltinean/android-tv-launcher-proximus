/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.home.presenter.module;

import android.content.Context;
import android.util.Pair;

import androidx.leanback.widget.PresenterSelector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleDataType;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.home.presenter.module.collection.channel.TvChannelCCollectionModulePresenter;
import tv.threess.threeready.ui.home.presenter.module.collection.mixed.MixedA1CollectionModulePresenter;
import tv.threess.threeready.ui.home.presenter.module.collection.mixed.MixedA2PortraitCollectionModulePresenter;
import tv.threess.threeready.ui.home.presenter.module.collection.nownext.NowNextA2CollectionModulePresenter;
import tv.threess.threeready.ui.home.presenter.module.stripe.app.AppACollectionModulePresenter;
import tv.threess.threeready.ui.home.presenter.module.stripe.app.AppAStripeModulePresenter;
import tv.threess.threeready.ui.home.presenter.module.stripe.app.AppBStripeModulePresenter;
import tv.threess.threeready.ui.home.presenter.module.stripe.channel.TvChannelAStripePresenter;
import tv.threess.threeready.ui.home.presenter.module.stripe.channel.TvChannelCStripePresenter;
import tv.threess.threeready.ui.home.presenter.module.stripe.gallery.GalleryAStripePresenter;
import tv.threess.threeready.ui.home.presenter.module.stripe.mixed.MixedStripeModulePresenter;
import tv.threess.threeready.ui.home.presenter.module.stripe.netflix.NetflixStripeModulePresenter;
import tv.threess.threeready.ui.home.presenter.module.stripe.notification.NotificationStripeModulePresenter;
import tv.threess.threeready.ui.home.presenter.module.stripe.nownext.NowNextAStripeModulePresenter;
import tv.threess.threeready.ui.home.view.ModularPageView;

/**
 * Presenter selector used to obtain {@link BaseModulePresenter} for the given {@link ModuleConfig}.
 * Created by Barabas Attila on 4/18/2017.
 */
public class ModulePresenterSelector extends PresenterSelector {

    Map<Pair<ModuleDataType, ModuleCardVariant>, BaseModulePresenter<?>> mStripeModulePresenterMap = new HashMap<>();
    Map<Pair<ModuleDataType, ModuleCardVariant>, BaseModulePresenter<?>> mCollectionModulePresenterMap = new HashMap<>();


    public ModulePresenterSelector(Context context, ModularPageView.PageTitleCallback pageTitle, DataSourceLoader dataSourceLoader) {
        // Gallery A stripe presenters
        GalleryAStripePresenter galleryAStripePresenter = new GalleryAStripePresenter(context, dataSourceLoader);
        mStripeModulePresenterMap.put(Pair.create(ModuleDataType.GALLERY, ModuleCardVariant.A1), galleryAStripePresenter);

        // Tv channel A stripe presenters
        TvChannelAStripePresenter channelAStripePresenter = new TvChannelAStripePresenter(context, dataSourceLoader);
        mStripeModulePresenterMap.put(Pair.create(ModuleDataType.TV_CHANNEL, ModuleCardVariant.A1), channelAStripePresenter);

        // Tv channel C stripe presenters
        TvChannelCStripePresenter channelCStripePresenter = new TvChannelCStripePresenter(context, dataSourceLoader);
        mStripeModulePresenterMap.put(Pair.create(ModuleDataType.TV_CHANNEL, ModuleCardVariant.C1), channelCStripePresenter);

        // Now Next A stripe presenters
        NowNextAStripeModulePresenter nowNextAStripeModulePresenter = new NowNextAStripeModulePresenter(context, dataSourceLoader);
        mStripeModulePresenterMap.put(Pair.create(ModuleDataType.TV_NOW_NEXT, ModuleCardVariant.A1), nowNextAStripeModulePresenter);

        // App A stripe presenters
        AppAStripeModulePresenter appAStripeModulePresenter = new AppAStripeModulePresenter(context, dataSourceLoader);
        mStripeModulePresenterMap.put(Pair.create(ModuleDataType.APPS, ModuleCardVariant.A1), appAStripeModulePresenter);

        // App B stripe presenters
        AppBStripeModulePresenter appBStripeModulePresenter = new AppBStripeModulePresenter(context, dataSourceLoader);
        mStripeModulePresenterMap.put(Pair.create(ModuleDataType.APPS, ModuleCardVariant.B1), appBStripeModulePresenter);

        // Mixed A, B stripe presenters
        MixedStripeModulePresenter mixedStripeModulePresenter = new MixedStripeModulePresenter(context, dataSourceLoader);
        mStripeModulePresenterMap.put(Pair.create(ModuleDataType.MIXED, ModuleCardVariant.A1), mixedStripeModulePresenter);
        mStripeModulePresenterMap.put(Pair.create(ModuleDataType.TV_PROGRAM, ModuleCardVariant.A1), mixedStripeModulePresenter);
        mStripeModulePresenterMap.put(Pair.create(ModuleDataType.REPLAY_CHANNEL, ModuleCardVariant.A1), mixedStripeModulePresenter);
        mStripeModulePresenterMap.put(Pair.create(ModuleDataType.EDITORIAL, ModuleCardVariant.A1), mixedStripeModulePresenter);

        // Netflix stripe presenter
        NetflixStripeModulePresenter netflixStripeModulePresenter = new NetflixStripeModulePresenter(context, dataSourceLoader);
        mStripeModulePresenterMap.put(Pair.create(ModuleDataType.NETFLIX, ModuleCardVariant.A1), netflixStripeModulePresenter);

        //Notifications stripe presenters
        NotificationStripeModulePresenter notificationStripeModulePresenter = new NotificationStripeModulePresenter(context, dataSourceLoader);
        mStripeModulePresenterMap.put(Pair.create(ModuleDataType.SYSTEM_NOTIFICATIONS, ModuleCardVariant.D1), notificationStripeModulePresenter);

        //Now Next A2 collection
        NowNextA2CollectionModulePresenter nowNextA2CollectionModulePresenter = new NowNextA2CollectionModulePresenter(context, pageTitle, dataSourceLoader);
        mCollectionModulePresenterMap.put(Pair.create(ModuleDataType.TV_NOW_NEXT, ModuleCardVariant.A2), nowNextA2CollectionModulePresenter);

        // Channel C collection
        TvChannelCCollectionModulePresenter tvChannelCCollectionModulePresenter = new TvChannelCCollectionModulePresenter(context, pageTitle, dataSourceLoader);
        mCollectionModulePresenterMap.put(Pair.create(ModuleDataType.TV_CHANNEL, ModuleCardVariant.C1), tvChannelCCollectionModulePresenter);
        mCollectionModulePresenterMap.put(Pair.create(ModuleDataType.REPLAY_CHANNEL, ModuleCardVariant.C1), tvChannelCCollectionModulePresenter);

        // Mixed A collection
        MixedA1CollectionModulePresenter mixedA1CollectionModulePresenter = new MixedA1CollectionModulePresenter(context, pageTitle, dataSourceLoader);
        mCollectionModulePresenterMap.put(Pair.create(ModuleDataType.MIXED, ModuleCardVariant.A1), mixedA1CollectionModulePresenter);

        // Mixed A2 portrait collection
        MixedA2PortraitCollectionModulePresenter mixedA2PortraitCollectionModulePresenter = new MixedA2PortraitCollectionModulePresenter(context, pageTitle, dataSourceLoader);
        mCollectionModulePresenterMap.put(Pair.create(ModuleDataType.MIXED, ModuleCardVariant.A2), mixedA2PortraitCollectionModulePresenter);

        //App A Collection Presenter
        AppACollectionModulePresenter appACollectionModulePresenter = new AppACollectionModulePresenter(context, pageTitle, dataSourceLoader);
        mCollectionModulePresenterMap.put(Pair.create(ModuleDataType.APPS, ModuleCardVariant.A1), appACollectionModulePresenter);

    }

    @Override
    public BaseModulePresenter<?>[] getPresenters() {
        List<BaseModulePresenter<?>> presenters = new ArrayList<>();
        presenters.addAll(mStripeModulePresenterMap.values());
        presenters.addAll(mCollectionModulePresenterMap.values());
        BaseModulePresenter<?>[] presenter = new BaseModulePresenter[presenters.size()];
        return presenters.toArray(presenter);
    }

    @Override
    public BaseModulePresenter<?> getPresenter(Object item) {
        if (item instanceof ModuleData) {
            return getModuleDataPresenter((ModuleData) item);
        }

        if (item instanceof ModuleConfig) {
            return getModulePresenter((ModuleConfig) item);
        }

        return null;
    }

    public BaseModulePresenter<?> getModulePresenter(ModuleConfig moduleConfig) {
        switch (moduleConfig.getType()) {
            case STRIPE:
                return mStripeModulePresenterMap.get(
                        Pair.create(moduleConfig.getDataType(), moduleConfig.getCardVariant()));
            case COLLECTION:
                return mCollectionModulePresenterMap.get(
                        Pair.create(moduleConfig.getDataType(), moduleConfig.getCardVariant()));
            default:
                return null;
        }
    }

    public BaseModulePresenter<?> getModuleDataPresenter(ModuleData moduleData) {
        return getModulePresenter(moduleData.getModuleConfig());
    }
}
