/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.loader;

import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.generic.resolver.DataSourceResolver;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Utility class to load the module data
 * sources asynchronously but returns the loaded data consecutive.
 *
 * @author Barabas Attila
 * @since 2017.09.20
 */
public class DataSourceLoader {
    private static final String TAG = Log.tag(DataSourceLoader.class);

    private static final int THREAD_POOL_SIZE = 3;

    // Fixed sized thread pool executor used for loading module data sources.
    private static final ThreadPoolExecutor DATA_SOURCE_EXECUTOR = new ThreadPoolExecutor(THREAD_POOL_SIZE, THREAD_POOL_SIZE,
            0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>()) {
        @Override
        protected <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
            return new FutureTask<T>(callable) {
                @Override
                public boolean cancel(boolean mayInterruptIfRunning) {
                    // Disable thread interruption to avoid resource corruption
                    // in components which doesn't handle thread interruption gracefully.
                    // E.g. OKHttp cache corruption
                    return super.cancel(false);
                }
            };
        }
    };

    private static final Scheduler DATA_SOURCE_SCHEDULER = Schedulers.from(DATA_SOURCE_EXECUTOR);

    private final DataSourceResolver mDataSourceResolver;

    private final List<ModuleConfig> mModuleConfigs;
    private final Map<ModuleConfig, DataSourceInfo> mModuleDataSourceInfoMap;
    private final DataSourceCallback mCallback;

    private int mVisibleModulesToLoad;

    public DataSourceLoader(DataSourceResolver dataSourceResolver,
                            DataSourceCallback callback, List<ModuleConfig> configs) {
        mDataSourceResolver = dataSourceResolver;
        mCallback = callback;
        mModuleConfigs = configs;
        mModuleDataSourceInfoMap = new HashMap<>();
    }

    public List<ModuleConfig> getModuleConfigs() {
        return mModuleConfigs;
    }


    /**
     * Load data sources for modules until at least the defined number of non empty module is loaded.
     * @param numberOfModules The minimum non empty modules to load.
     */
    public void loadVisibleModules(int numberOfModules) {
        mVisibleModulesToLoad = numberOfModules;

        int availableModules = 0;
        for (ModuleConfig moduleConfig : mModuleConfigs) {

            if (availableModules >= numberOfModules) {
                // All necessary module is already loaded or loading.
                break;
            }


            DataSourceInfo dataSourceInfo = mModuleDataSourceInfoMap.get(moduleConfig);

            if (dataSourceInfo != null) {
                // Already loaded or loading
                if (dataSourceInfo.isVisible() || dataSourceInfo.isLoading()) {
                    availableModules++;
                }

            } else {
                // Start loading modules.
                loadModule(moduleConfig, mCallback.getDefaultLimit(moduleConfig));
                availableModules++;
            }
        }
    }

    /**
     * Load the module data source and update the adapter with the data afterwards.
     * @param moduleConfig The module for which the data source needs to be loaded.
     * @param limit The pagination limit of the data source items. Start offset and count.
     */
    private void loadModule(ModuleConfig moduleConfig, Pair<Integer, Integer> limit) {
        Log.d(TAG, "Subscribe to data source : " + moduleConfig
                + ", data source load queue size: " + DATA_SOURCE_EXECUTOR.getQueue().size());

        // Cancel previous observables.
        DataSourceInfo prevDataSource = mModuleDataSourceInfoMap.get(moduleConfig);
        unsubscribe(prevDataSource);

        DataSourceInfo dataSourceInfo = new DataSourceInfo(moduleConfig);
        mModuleDataSourceInfoMap.put(moduleConfig, dataSourceInfo);

        ModuleDataObserver moduleDataObserver = new ModuleDataObserver(moduleConfig, limit);
        dataSourceInfo.mModuleDataObservers.add(moduleDataObserver);
        mDataSourceResolver
                .resolve(moduleConfig, limit.first, limit.second, null)
                .subscribeOn(DATA_SOURCE_SCHEDULER)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(moduleDataObserver);
    }

    /**
     * @return True if all the modules are loaded, otherwise false.
     */
    public boolean allModulesLoaded() {
        for (DataSourceInfo info : mModuleDataSourceInfoMap.values()) {
            if(!info.isLoaded())
                return false;
        }
        return true;
    }

    /**
     * Reload a previously loaded module data source.
     * @param moduleConfig The module of the data source which needs to be reloaded.
     * @param dataSourceSelectors The selected sort option for the data source.
     *                  If null the default sort option needs to be used.
     */
    public void reloadModule(ModuleConfig moduleConfig, @Nullable List<DataSourceSelector> dataSourceSelectors) {
        DataSourceInfo prevDataSource = mModuleDataSourceInfoMap.get(moduleConfig);
        if (prevDataSource == null || prevDataSource.mModuleDataObservers.isEmpty()) {
            Log.e(TAG, "Cannot reload module data source. Module not loaded yet. " + moduleConfig);
            return;
        }

        DataSourceInfo dataSourceInfo = new DataSourceInfo(moduleConfig);
        mModuleDataSourceInfoMap.put(moduleConfig, dataSourceInfo);

        Pair<Integer, Integer> limit = prevDataSource.mModuleDataObservers.get(0).mLimit;
        ModuleDataObserver moduleDataObserver = new ModuleDataObserver(moduleConfig, limit);

        unsubscribe(prevDataSource);

        dataSourceInfo.mModuleDataObservers.add(moduleDataObserver);
        mDataSourceResolver
                .resolve(moduleConfig, limit.first, limit.second, dataSourceSelectors)
                .subscribeOn(DATA_SOURCE_SCHEDULER)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(moduleDataObserver);
    }

    /**
     * Request a new page for a previously loaded data source.
     * @param moduleConfig The module config on which a new page needs to be requested.
     * @param limit The pagination limit of the data source items. Start offset and count.
     */
    public void requestPage(ModuleConfig moduleConfig, Pair<Integer, Integer> limit) {
        DataSourceInfo dataSourceInfo = mModuleDataSourceInfoMap.get(moduleConfig);
        if (dataSourceInfo == null || dataSourceInfo.mModuleData ==null) {
            Log.e(TAG, "No data source info available.");
            return;
        }

        // Check if the requested page already loaded.
        for (ModuleDataObserver moduleDataObserver : dataSourceInfo.mModuleDataObservers) {
            if (moduleDataObserver.contains(limit)) {
                return;
            }
        }

        // No more page available.
        if (!dataSourceInfo.mModuleData.hasMoreItems()) {
            return;
        }

        // Subscribe to data source page.
        ModuleDataObserver moduleDataObserver = new ModuleDataObserver(moduleConfig, limit);
        dataSourceInfo.mModuleDataObservers.add(moduleDataObserver);

        mDataSourceResolver
                .resolve(moduleConfig, limit.first, limit.second, dataSourceInfo.mModuleData.getSelectors())
                .subscribeOn(DATA_SOURCE_SCHEDULER)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(moduleDataObserver);

    }

    /**
     * Unsubscribe from all previously subscribed data source.
     */
    public void unSubscribeAll() {
        for (ModuleConfig moduleConfig : mModuleConfigs) {
            DataSourceInfo dataSourceInfo = mModuleDataSourceInfoMap.get(moduleConfig);
            unsubscribe(dataSourceInfo);
        }
    }

    /**
     * Unsubscribe RX observable from the given data source.
     * @param dataSourceInfo The data source from which the observables needs to be removed.
     */
    private void unsubscribe(DataSourceInfo dataSourceInfo) {
        if (dataSourceInfo == null) {
            // There is nothing to unsubscribe.
            return;
        }

        for (ModuleDataObserver observer : dataSourceInfo.mModuleDataObservers) {
            observer.unSubscribe();
        }
    }

    public ModuleData<?> getContentData(ModuleConfig moduleConfig) {
        DataSourceInfo dataSourceInfo = mModuleDataSourceInfoMap.get(moduleConfig);
        if (dataSourceInfo != null) {
            return dataSourceInfo.mModuleData;
        }
        return null;
    }

    /**
     * Holds the RX observables associated to the data source.
     */
    private class DataSourceInfo {
        private ModuleData<?> mModuleData;

        private final List<ModuleDataObserver> mModuleDataObservers;
        private boolean mIsLoaded;
        private boolean mIsReturned;

        public DataSourceInfo(ModuleConfig moduleConfig) {
            mModuleDataObservers = new ArrayList<>();
            mModuleData = new ModuleData<>(moduleConfig, new ArrayList<>());
        }

        public boolean isVisible() {
            return mIsLoaded && mModuleData.isVisible();
        }

        public boolean isLoading() {
            return !mIsLoaded;
        }

        public boolean isLoaded() {
            return mIsLoaded;
        }
    }


    public void onDataSourceLoaded(DataSourceInfo dataSourceInfo) {
        if (dataSourceInfo.mIsReturned) {
            // Update data source
            mCallback.onDataSourceLoaded(dataSourceInfo.mModuleData);

        } else {
            for (ModuleConfig moduleConfig : mModuleConfigs) {
                dataSourceInfo = mModuleDataSourceInfoMap.get(moduleConfig);
                if (dataSourceInfo == null || dataSourceInfo.isLoading()) {
                    break;
                }

                // Return all consecutive data.
                if (!dataSourceInfo.mIsReturned) {
                    mCallback.onDataSourceLoaded(dataSourceInfo.mModuleData);
                    dataSourceInfo.mIsReturned = true;
                }
            }
        }

        loadVisibleModules(mVisibleModulesToLoad);
    }

    /**
     * RX observer to detect data source load and changes.
     */
    protected class ModuleDataObserver implements Observer<ModuleData<Object>> {
        private final ModuleConfig mModuleConfig;
        private final List<Disposable> mDisposables = new ArrayList<>();
        private final Pair<Integer, Integer> mLimit;

        public ModuleDataObserver(ModuleConfig moduleConfig,
                                  Pair<Integer, Integer> limit) {
            mModuleConfig = moduleConfig;
            mLimit = limit;
        }

        public boolean contains(Pair<Integer, Integer> limit) {
            if (mLimit == null && limit == null) {
                return true;
            }

            if (mLimit == null || limit == null) {
                return false;
            }

            return mLimit.first <= limit.first
                    && mLimit.first + mLimit.second >= limit.first + limit.second;
        }

        public void unSubscribe() {
            RxUtils.disposeSilently(mDisposables);
        }

        @Override
        public void onSubscribe(@NonNull Disposable d) {
            mDisposables.add(d);
        }

        @Override
        public void onNext(@NonNull ModuleData<Object> data) {
            Log.d(TAG, "Data source loaded. " + mModuleConfig.getTitle() + ", size : " + data.getItems().size());

            DataSourceInfo dataSourceInfo = mModuleDataSourceInfoMap.get(mModuleConfig);
            if (dataSourceInfo == null) {
                return;
            }

            ArrayList<Object> list = new ArrayList<>();
            if (mLimit != null && data.getPageSize() > 0) {
                list.addAll(dataSourceInfo.mModuleData.getItems());
                // Update page in cached data.
                int start = Math.min(Math.max(0, mLimit.first), list.size());
                int end = Math.min(start + 1 + data.getItems().size(), list.size());

                if (start != end) {
                    list.removeAll(list.subList(start, end));
                }

                list.addAll(start, data.getItems());
            } else {
                // Replace the cached data.
                list.addAll(data.getItems());
            }

            dataSourceInfo.mIsLoaded = true;
            dataSourceInfo.mModuleData = new ModuleData<>(mModuleConfig, list, data);

            onDataSourceLoaded(dataSourceInfo);
        }

        @Override
        public void onError(@NonNull Throwable e) {
            Log.w(TAG, "Could not load data source. " + mModuleConfig, e);

            DataSourceInfo dataSourceInfo = mModuleDataSourceInfoMap.get(mModuleConfig);
            if (dataSourceInfo == null) {
                return;
            }
            dataSourceInfo.mIsLoaded = true;

            onDataSourceLoaded(dataSourceInfo);
        }

        @Override
        public void onComplete() {
        }
    }

    public interface DataSourceCallback {

        /**
         * Called when the loader initiate the loading.
         * The default page size of the module needs to be returned here.
         * @param moduleConfig The module for which the loading will be initiated.
         * @return The page limit to load data for.
         */
        Pair<Integer, Integer> getDefaultLimit(ModuleConfig moduleConfig);


        /**
         * Called when the data source of a module loads.
         * @param moduleDataSource Thew newly loaded data source of the module.
         */
        void onDataSourceLoaded(ModuleData<?> moduleDataSource);
    }
}
