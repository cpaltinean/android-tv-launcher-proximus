package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.PlayOptionsCardBinding;
import tv.threess.threeready.ui.generic.model.BroadcastPlayOption;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.utils.TitleFallbackImageViewTarget;

/**
 * Presenter class for recording actions play options screen
 *
 * @author Andrei Teslovan
 * @since 2018.12.10
 */
public class BroadcastPlayOptionsCardPresenter<TItem extends BroadcastPlayOption<? extends IBroadcast>> extends BaseCardPresenter<BroadcastPlayOptionsCardPresenter.ViewHolder, TItem> {

    protected final Navigator mNavigator = Components.get(Navigator.class);

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);

    private final boolean mFromFavourite;

    public BroadcastPlayOptionsCardPresenter(Context context, boolean isFromFavourite) {
        super(context);
        mFromFavourite = isFromFavourite;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        PlayOptionsCardBinding binding = PlayOptionsCardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.logoText.setTextColor(mLayoutConfig.getFontColor());
        holder.view.setBackgroundColor(mLayoutConfig.getPlaceHolderColor());
        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, TItem playOption) {
        super.onBindHolder(holder, playOption);

        TvChannel channel = mNowOnTvCache.getChannel(playOption.getContentItem().getChannelId());

        String channelName = null;
        if (channel != null)  {
            channelName = channel.getName();
        }

        Glide.with(mContext).clear(holder.mBinding.logo);
        Glide.with(mContext)
                .load(channel)
                .override(getCardWidth(playOption), getCardHeight(playOption))
                .optionalCenterInside()
                .into(new TitleFallbackImageViewTarget(
                        holder.mBinding.logo,
                        holder.mBinding.logoText,
                        channelName));
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);
        Glide.with(mContext).clear(holder.mBinding.logo);
    }

    @Override
    public int getCardWidth(TItem playOption) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.play_options_screen_card_width);
    }

    @Override
    public int getCardHeight(TItem playOption) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.play_options_screen_card_height);
    }

    @Override
    public void onClicked(ViewHolder holder, TItem playOption) {
        super.onClicked(holder, playOption);
        mNavigator.showPlayer(playOption, mFromFavourite);
        mNavigator.clearAllDialogs();
    }

    @Override
    public long getStableId(TItem playOption) {
        return Objects.hash(playOption);
    }

    public static class ViewHolder extends BaseCardPresenter.ViewHolder {

        private final PlayOptionsCardBinding mBinding;

        public ViewHolder(PlayOptionsCardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
