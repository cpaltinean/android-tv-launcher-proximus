package tv.threess.threeready.ui.search.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.BaseGridView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.home.model.generic.ContentConfig;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.EditorialItemAction.ModuleEditorialItemAction;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.UILog.SearchTermType;
import tv.threess.threeready.api.log.UILog.SearchType;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.search.SearchRepository;
import tv.threess.threeready.api.search.model.SearchSuggestion;
import tv.threess.threeready.api.search.model.SearchTerm;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.SearchBinding;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.home.view.ModularPageView;
import tv.threess.threeready.ui.search.view.SuggestionsAutoCompleteTextView;
import tv.threess.threeready.ui.utils.SystemUtils;

/**
 * Display local search results based on the configuration file.
 *
 * @author Barabas Attila
 * @since 2017.12.18
 */

public class SearchFragment extends BaseFragment {
    public static final String TAG = Log.tag(SearchFragment.class);

    private static final String SEARCH_PAGE_ID = "SEARCH";
    private static final String CHANNEL_FILTER_TYPE = "Channel";
    private static final String MOVIE_FILTER_TYPE = "Movie";
    private static final String SERIES_FILTER_TYPE = "Series";
    
    View mLastFocusInStripeView;

    Disposable mSuggestionDisposable;

    ModularPageView mStripeModularPageView;

    private final Translator mTranslator = Components.get(Translator.class);
    private final ContentConfig mContentConfig = Components.get(ContentConfig.class);
    private final SearchRepository mSearchRepository = Components.get(SearchRepository.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    SearchTerm mCurrentSearchTerm;
    boolean mHideKeyboardWhenLoaded;

    List<ModuleConfig> mSearchModules;
    
    private SearchBinding mBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get page config for search.
        PageConfig pageConfig = getSearchPageConfig();
        if (pageConfig != null) {
            mSearchModules = pageConfig.getModules();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (mBinding == null) {
            mBinding = SearchBinding.inflate(inflater, container, false);

            mBinding.noResult.setText(mTranslator.get(TranslationKey.SCREEN_SEARCH_NO_RESULTS));

            mBinding.search.setPrivateImeOptions("EscapeNorth=1;VoiceDismiss=1;");
            mBinding.search.setHint(mTranslator.get(TranslationKey.SCREEN_SEARCH));
            mBinding.search.setImeActionLabel(mTranslator.get(TranslationKey.SEARCH_BUTTON), EditorInfo.IME_ACTION_DONE);
            mBinding.search.setInputListener(mInputListener);
            mBinding.search.setThreshold(1);

            // Display keyboard when focused.
            mBinding.search.setOnFocusChangeListener((v, hasFocus) -> {
                if (getActivity() == null) {
                    return;
                }

                if (hasFocus) {
                    SystemUtils.showSoftInput(getActivity());
                    loadSuggestions(mBinding.search.getText().toString());
                } else {
                    SystemUtils.hideSoftInput(getActivity());
                }
            });

            // Show keyboard on click.
            mBinding.search.setOnClickListener(v -> {
                if (getActivity() == null) {
                    return;
                }

                SystemUtils.showSoftInput(getActivity());
                loadSuggestions(mBinding.search.getText().toString());
            });

            mBinding.search.setOnEditorActionListener((v, actionId, event) -> {
                // Trigger search.
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_GO) {

                    if (TextUtils.isEmpty(mBinding.search.getText())) {
                        return true;
                    }
                    triggerSearch(new SearchTerm(mBinding.search.getText().toString()), false);
                    if (getCurrentDisplayModuleCount() > 0) {
                        SystemUtils.hideSoftInput(getActivity());
                        if (mLastFocusInStripeView != null) {
                            mLastFocusInStripeView.requestFocus();
                        } else {
                            mBinding.pageContainer.requestFocus();
                        }
                    } else {
                        mHideKeyboardWhenLoaded = true;
                    }
                    return true;

                    // Move focus on the first result.
                } else if (actionId == EditorInfo.IME_ACTION_NONE) {
                    if (getCurrentDisplayModuleCount() > 0) {
                        SystemUtils.hideSoftInput(getActivity());
                        if (mLastFocusInStripeView != null) {
                            mLastFocusInStripeView.requestFocus();
                        } else {
                            mBinding.pageContainer.requestFocus();
                        }
                    }
                    return true;
                }

                return false;
            });
        }

        return mBinding.getRoot();
    }

    @Override
    public boolean onKeyDown(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP) {
            mLastFocusInStripeView = mBinding.pageContainer.findFocus();
        }

        return super.onKeyDown(event);
    }

    @Override
    public void onContentShown() {
        super.onContentShown();
        restoreFocus();
    }

    @Override
    protected void restoreFocus() {
        if(getCurrentDisplayModuleCount() == 0) {
            mBinding.search.requestFocus();
            return;
        }
        super.restoreFocus();
    }

    @Override
    public void onPause() {
        super.onPause();
        mBinding.search.setFocusable(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        mBinding.search.setFocusable(true);
        if (hasNoFocusedView()) {
            mBinding.search.requestFocus();
        }
    }

    @Override
    public void onDestroyView() {
        SystemUtils.hideSoftInput(getActivity());
        RxUtils.disposeSilently(mSuggestionDisposable);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Un subscribe all data source observers.
        for (int i = 0; i < mBinding.pageContainer.getChildCount(); ++i) {
            ModularPageView page = (ModularPageView) mBinding.pageContainer.getChildAt(i);
            page.setAdapter(null);
            page.cancelPageLoading();
        }
    }

    private PageConfig getSearchPageConfig() {
        for (PageConfig pageConfig : mContentConfig.getPageConfigs()) {
            if (SEARCH_PAGE_ID.equals(pageConfig.getId())) {
                return pageConfig;
            }
        }
        return null;
    }

    private int getCurrentDisplayModuleCount() {
        return mStripeModularPageView != null && mStripeModularPageView.getAdapter() != null
                ? mStripeModularPageView.getAdapter().getItemCount() : 0;
    }

    /**
     * Load search suggestions for the given term.
     */
    private void loadSuggestions(final String searchTerm) {
        if (mSuggestionDisposable != null) {
            mSuggestionDisposable.dispose();
            mSuggestionDisposable = null;
        }

        // Clear suggestions
        if (TextUtils.isEmpty(searchTerm)) {
            mBinding.search.setSuggestions(Collections.emptyList());
            return;
        }

        // Load suggestions.
        mSearchRepository.getSuggestions(searchTerm)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<SearchSuggestion>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mSuggestionDisposable = d;
                    }

                    @Override
                    public void onNext(List<SearchSuggestion> suggestions) {
                        mBinding.search.setSuggestions(suggestions);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "Could not load suggestions", e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void showFading() {
        mBinding.fadingEdgeLayout.setFadeEdges(true, false, false, false);
        mBinding.fadingEdgeLayout.setFadeSizes((int) getResources().getDimension(R.dimen.fading_edge_visible_size), 0, 0, 0);
    }

    private void hideFading() {
        mBinding.fadingEdgeLayout.setFadeSizes((int) getResources().getDimension(R.dimen.fading_edge_invisible_size), 0, 0, 0);
        mBinding.fadingEdgeLayout.setFadeEdges(true, false, false, false);
    }

    /**
     * Trigger search for the stripes.
     */
    private void triggerSearch(SearchTerm term, boolean forced) {
        // Trigger search for stripes.
        triggerSearch(term, mSearchModules, forced);
    }

    /**
     * Trigger search for given modules.
     */
    private void triggerSearch(SearchTerm term, List<ModuleConfig> moduleConfigs, boolean forced) {
        if (!forced && mCurrentSearchTerm != null && mCurrentSearchTerm.getTerm().equals(term.getTerm())) {
            // Already triggered.
            return;
        }

        mCurrentSearchTerm = term;
        if (moduleConfigs == null) {
            return;
        }

        // Set search term for data sources
        for (ModuleConfig moduleConfig : moduleConfigs) {
            if (moduleConfig.getDataSource() == null
                    || moduleConfig.getDataSource().getParams() == null) {
                continue;
            }

            addSearchTerm(moduleConfig, term);

            String typeFilter = moduleConfig.getDataSource()
                    .getParams().getFilter(ModuleFilterOption.Mixed.Type.NAME);
            if (typeFilter != null) {
                switch (typeFilter) {
                    case CHANNEL_FILTER_TYPE:
                        UILog.logSearchTrigger(SearchType.LOCAL, SearchTermType.CHANNEL, mCurrentSearchTerm.getTerm());
                        break;
                    case MOVIE_FILTER_TYPE:
                        UILog.logSearchTrigger(SearchType.LOCAL, SearchTermType.MOVIE, mCurrentSearchTerm.getTerm());
                        break;
                    case SERIES_FILTER_TYPE:
                        UILog.logSearchTrigger(SearchType.LOCAL, SearchTermType.SERIES, mCurrentSearchTerm.getTerm());
                }
            }

        }

        displayModules();
    }

    private void addSearchTerm(ModuleConfig moduleConfig, SearchTerm searchTerm) {
        if (moduleConfig.getDataSource() == null || moduleConfig.getDataSource().getParams() == null) {
            return;
        }

        // Add Search term to module.
        moduleConfig.getDataSource().getParams()
                .addFilter(ModuleFilterOption.Mixed.SearchTerm.NAME, searchTerm);

        if (moduleConfig.getEditorialItems() == null) {
            return;
        }

        // Add search term to editorials
        for (EditorialItem moduleItem : moduleConfig.getEditorialItems()) {
            if (moduleItem.getAction() instanceof ModuleEditorialItemAction) {
                ModuleEditorialItemAction moduleAction = (ModuleEditorialItemAction) moduleItem.getAction();
                addSearchTerm(moduleAction.getModuleConfig(), searchTerm);
            }
        }
    }

    protected void displayModules() {
        ModularPageView modularPageView = addViewToContainer();
        modularPageView.loadPageConfig(getSearchPageConfig(), mPageLoadListener);
        modularPageView.setWindowAlignment(BaseGridView.WINDOW_ALIGN_LOW_EDGE);
        modularPageView.setPadding(0, (int) getResources().getDimension(R.dimen.search_modular_page_padding), 0, 0);

        mStripeModularPageView = modularPageView;
        mStripeModularPageView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (shouldHideFading()) {
                    hideFading();
                } else {
                    showFading();
                }
            }
        });
    }

    private boolean shouldHideFading() {
        return mStripeModularPageView.getChildAt(0) != null &&
                (mStripeModularPageView.getChildAt(0).getY() >= -getResources().getDimension(R.dimen.scroll_cut_threshold) ||
                        mStripeModularPageView.getFocusedChild() == null ||
                        mStripeModularPageView.getChildAdapterPosition(
                                mStripeModularPageView.getFocusedChild()) == 0);
    }

    @SuppressLint("InflateParams")
    protected ModularPageView addViewToContainer() {
        ModularPageView modularPageView = (ModularPageView) LayoutInflater.from(getContext()).inflate(R.layout.modular_page, null);
        modularPageView.setLayoutParams(new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // Un subscribe all data source observers.
        for (int i = 0; i < mBinding.pageContainer.getChildCount(); ++i) {
            ModularPageView page = (ModularPageView) mBinding.pageContainer.getChildAt(i);
            page.setAdapter(null);
            page.cancelPageLoading();
        }

        mBinding.pageContainer.removeAllViews();
        mBinding.pageContainer.addView(modularPageView);
        return modularPageView;
    }

    /**
     * Subscribe on input field changes.
     */
    private final SuggestionsAutoCompleteTextView.InputListener mInputListener =
            new SuggestionsAutoCompleteTextView.InputListener() {

                @Override
                public void onKeyboardCanceled() {
                    if (getCurrentDisplayModuleCount() > 0) {
                        mBinding.pageContainer.requestFocus();
                        if (mLastFocusInStripeView != null) {
                            mLastFocusInStripeView.requestFocus();
                        }
                    } else {
                        mNavigator.getActivity().onBackPressed();
                    }
                }

                @Override
                public void onTextChanged(final SearchTerm searchTerm) {
                    if (searchTerm.getTerm().length() >= mAppConfig.getSearchThreshold()) {
                        triggerSearch(searchTerm, false);
                    }

                    loadSuggestions(searchTerm.getTerm());
                }
            };

    /**
     * Subscribe on result page changes.
     */
    private final ModularPageView.PageLoadListener mPageLoadListener = new ModularPageView.PageLoadListener() {
        @Override
        public void onModuleLoaded(ModuleConfig moduleConfig) {
            mBinding.noResult.setVisibility(View.GONE);

            if (mHideKeyboardWhenLoaded) {
                SystemUtils.hideSoftInput(getActivity());
                mBinding.pageContainer.requestFocus();
            }

            mHideKeyboardWhenLoaded = false;
        }

        @Override
        public void onPageEmpty(PageConfig pageConfig) {
            mBinding.noResult.setVisibility(View.VISIBLE);
            mBinding.search.requestFocus();
        }
    };

    @Override
    public boolean onBackPressed() {
        if (!mNavigator.isContentOverlayDisplayed()) {
            mNavigator.showContentOverlay();
            return true;
        }

        return super.onBackPressed();
    }

    @Nullable
    @Override
    protected Event<?, ?> generatePageOpenEvent() {
        Event<UILogEvent, UILogEvent.Detail> event = new Event<>(UILogEvent.PageOpen);
        event.addDetail(UILogEvent.Detail.Page, UILog.Page.KeywordEntryPage);

        return event;
    }
}
