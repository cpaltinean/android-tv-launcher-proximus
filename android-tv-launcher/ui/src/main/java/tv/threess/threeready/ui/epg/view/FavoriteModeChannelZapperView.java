package tv.threess.threeready.ui.epg.view;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.ButtonStyle;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.FavoriteModeChannelZapperBinding;
import tv.threess.threeready.ui.generic.hint.HintManager;
import tv.threess.threeready.ui.generic.utils.AnimatorListenerAdapter;
import tv.threess.threeready.ui.generic.utils.AnimatorUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.ImageTransform;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Channel zapper view which has also the favorite management capability.
 *
 * @author Barabas Attila
 * @since 2017.12.04
 */

public class FavoriteModeChannelZapperView extends ChannelZapperView {
    private static final String TAG = Log.tag(FavoriteModeChannelZapperView.class);

    private final HintManager mHintManager = Components.get(HintManager.class);
    private final Translator mTranslator = Components.get(Translator.class);

    private int mChannelLogoWidth, mChannelLogoHeight; // are used for animations
    private boolean mChannelLogoReady;

    private EpgView.FavoriteModeTriggerListener mFavoriteModeChangedListener;

    private boolean mIsFavoriteMode;

    Animator mFadeOutAnimator;
    Animator mFadeInAnimator;
    
    private FavoriteModeChannelZapperBinding mBinding;

    public FavoriteModeChannelZapperView(Context context) {
        this(context, null);
    }

    public FavoriteModeChannelZapperView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FavoriteModeChannelZapperView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public FavoriteModeChannelZapperView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        ButtonStyle buttonStyle = Components.get(ButtonStyle.class);
        LayoutConfig layoutConfig = Components.get(LayoutConfig.class);
        mBinding.addRemoveButton.setBackground(UiUtils.createButtonBackground(context, layoutConfig, buttonStyle));
        mBinding.addRemoveButton.setTextColor(UiUtils.createButtonBrandingColorStateList(layoutConfig));
        mBinding.addRemoveButton.setOnClickListener(v -> {
            mHintManager.hideManageChannelHints();
            if (mSelectedChannel != null) {
                if (mSelectedChannel.isFavorite()) {
                    removeFromFavorite(mSelectedChannel);
                } else {
                    addToFavorite(mSelectedChannel);
                }
            }
        });

        mBinding.doneButton.setBackground(UiUtils.createButtonBackground(context, layoutConfig, buttonStyle));

        mBinding.doneButton.setText(mTranslator.get(TranslationKey.SREEN_EPG_EXIT));
        mBinding.doneButton.setTextColor(UiUtils.createButtonBrandingColorStateList(layoutConfig));
        mBinding.doneButton.setOnClickListener(v -> {
            mHintManager.hideManageChannelHints();
            mFavoriteChannelsCache.saveChanges();
            triggerDefaultMode();
        });

        mBinding.channelZapperGrid.setWindowAlignmentOffsetPercent(0f);
        mBinding.channelZapperGrid.setWindowAlignmentOffset(
                context.getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_item_alignment_offset));
    }

    @Override
    protected void inflateView() {
        if (mBinding != null) {
            return;
        }
        
        mBinding = FavoriteModeChannelZapperBinding.inflate(LayoutInflater.from(getContext()), this);
    }

    @NonNull
    @Override
    protected ChannelZapperGridView getChannelZapperGridView() {
        return mBinding.channelZapperGrid;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                if (mIsFavoriteMode) {
                    triggerDefaultMode();
                    return true;
                }
            }
        }

        return super.dispatchKeyEvent(event);
    }

    /**
     * Adds channel to favorites in DataBase and changes the text below card
     */
    public void addToFavorite(final TvChannel channel) {
        if (channel == null) {
            return;
        }

        mFavoriteChannelsCache.addFavoriteChannel(channel.getId());
        for (TvChannel displayedChannel : mChannels) {
            if (displayedChannel.getId() != null
                    && displayedChannel.getId().equals(channel.getId())) {
                displayedChannel.setFavorite(true);
            }
        }

        mFavChannels.add(channel);

        updateFavoriteButtons(channel);
    }

    /**
     * Removes channel from favorites and changes the text below card
     */
    public void removeFromFavorite(final TvChannel channel) {
        if (channel == null) {
            return;
        }

        mFavoriteChannelsCache.removeFavoriteChannel(channel.getId());
        for (TvChannel displayedChannel : mChannels) {
            if (displayedChannel.getId() != null
                    && displayedChannel.getId().equals(channel.getId())) {
                displayedChannel.setFavorite(false);
            }
        }

        mFavChannels.remove(channel);

        updateFavoriteButtons(channel);
    }


    private void updateChannels() {
        Log.d(TAG, "Update channels.");


        // Fade the channel zapper out
        mFadeOutAnimator = fadeAnimation(false);
        mFadeOutAnimator.setDuration(getResources().getInteger(R.integer.manage_channel_screen_animation_duration));
        mFadeOutAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationFinished(Animator animation) {
                super.onAnimationFinished(animation);

                // Compose channel list.
                mChannels.clear();
                mFavChannels = mFavChannels.stream().distinct().collect(Collectors.toList());
                mChannels.addAll(mFavChannels);
                mChannels.addAll(mAllChannels);

                // Notify adapter about data source change.
                mObjectAdapter.replaceAll(mChannels);

                selectChannel(mSelectedChannel);

                // Fade a channel zapper in.
                mFadeInAnimator = fadeAnimation(true);
                mFadeInAnimator.setDuration(getResources().getInteger(R.integer.manage_channel_screen_animation_duration));
                mFadeInAnimator.start();
            }
        });
        mFadeOutAnimator.start();
    }

    private void updateFavoriteButtons(TvChannel channel) {
        if (channel.isFavorite()) {
            mBinding.addRemoveButton.setText(mTranslator.get(TranslationKey.SCREEN_EPG_FAVORITES_REMOVE));
            mBinding.doneButton.getLayoutParams().width = getContext().getResources().getDimensionPixelOffset(R.dimen.manage_channel_screen_favorite_remove_button_width);
            mBinding.addRemoveButton.getLayoutParams().width = getContext().getResources().getDimensionPixelOffset(R.dimen.manage_channel_screen_favorite_remove_button_width);
        } else {
            mBinding.addRemoveButton.setText(mTranslator.get(TranslationKey.SCREEN_EPG_FAVORITES_ADD));
            mBinding.doneButton.getLayoutParams().width = getContext().getResources().getDimensionPixelOffset(R.dimen.manage_channel_screen_favorite_add_button_width);
            mBinding.addRemoveButton.getLayoutParams().width = getContext().getResources().getDimensionPixelOffset(R.dimen.manage_channel_screen_favorite_add_button_width);
        }
        mBinding.channelZapperGrid.updateFavoriteStates();
    }

    public void setFavoriteModeTriggerListener(EpgView.FavoriteModeTriggerListener favoriteModeChangedListener) {
        mFavoriteModeChangedListener = favoriteModeChangedListener;
    }

    @Override
    protected void onChannelSelected(TvChannel channel, boolean isFromFavoriteSection) {
        super.onChannelSelected(channel, isFromFavoriteSection);
        if (channel == null) {
            return;
        }

        if (channel.getType() == ChannelType.APP
                || channel.getType() == ChannelType.REGIONAL) {
            loadAppLogo();
        } else {
            loadChannelLogo();
        }

        updateFavoriteButtons(channel);
    }

    public void loadAppLogo() {
        mChannelLogoReady = false;

        Glide.with(getContext().getApplicationContext()).clear(mBinding.netflixLogo);
        Glide.with(getContext().getApplicationContext())
                .load(mSelectedChannel)
                .override(
                        getContext().getResources().getDimensionPixelOffset(R.dimen.zapper_netflix_logo_max_width),
                        getContext().getResources().getDimensionPixelOffset(R.dimen.zapper_netflix_logo_max_height))
                .optionalCenterInside()
                .dontAnimate()
                .into(new DrawableImageViewTarget(mBinding.netflixLogo) {
                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        mChannelLogoReady = false;
                    }

                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        super.onResourceReady(resource, transition);
                        mChannelLogoWidth = resource.getIntrinsicWidth();
                        mChannelLogoHeight = resource.getIntrinsicHeight();
                        mChannelLogoReady = true;
                    }
                });
    }

    public void loadChannelLogo() {
        mChannelLogoReady = false;

        // Load channel logo
        Glide.with(getContext().getApplicationContext()).clear(mBinding.logo);
        Glide.with(getContext().getApplicationContext())
                .load(mSelectedChannel)
                .apply(ImageTransform.REQUEST_OPTIONS.clone().dontAnimate())
                .into(new DrawableImageViewTarget(mBinding.logo) {
                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        mChannelLogoReady = false;
                    }

                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        super.onResourceReady(resource, transition);
                        mChannelLogoWidth = resource.getIntrinsicWidth();
                        mChannelLogoHeight = resource.getIntrinsicHeight();
                        mChannelLogoReady = true;
                    }
                });
    }

    private void triggerDefaultMode() {
        if (mFavoriteModeChangedListener != null) {
            mFavoriteModeChangedListener.onTriggerFavoriteMode(false);
        }
    }

    public void onTriggerDefaultMode() {
        mIsFavoriteMode = false;
        mBinding.channelZapperGrid.setFavoriteMode(false);
    }

    public void onDefaultMode() {
        mBinding.doneButton.setVisibility(GONE);
        mBinding.addRemoveButton.setVisibility(GONE);

        updateChannels();
    }

    public void onTriggerFavoriteMode() {
        mIsFavoriteMode = true;
        mBinding.channelZapperGrid.setFavoriteMode(true);
        mBinding.channelZapperGrid.setSelectedChannelLogoVisibility(GONE);
        mBinding.addRemoveButton.setVisibility(VISIBLE);
        mBinding.doneButton.setVisibility(VISIBLE);
    }

    public void onFavoriteMode() {
        mBinding.addRemoveButton.requestFocus();
        mBinding.channelZapperGrid.setSelectedChannelLogoVisibility(View.VISIBLE);
    }

    /**
     * needed in case some animation are not ending or canceling in time and grid
     * doesn't gets back visible
     */
    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mBinding.channelZapperGrid.setVisibility(VISIBLE);
        mBinding.channelZapperGrid.setAlpha(1);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        AnimatorUtils.cancelAnimators(mFadeOutAnimator, mFadeInAnimator);
        Glide.with(getContext().getApplicationContext()).clear(mBinding.netflixLogo);
        Glide.with(getContext().getApplicationContext()).clear(mBinding.logo);
    }

    /**
     * Create the animators, which are needed for opening/closing manage channel screen.
     */
    public List<Animator> getFavoriteModeAnimator(final boolean isFavoriteMode) {
        List<Animator> animatorList = new ArrayList<>();

        // PADDING ANIMATION
        // Change the padding of the selected child.
        ValueAnimator paddingAnimator = isFavoriteMode ?
                ValueAnimator.ofInt(getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_selected_item_spacing), 0) :
                ValueAnimator.ofInt(1, getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_selected_item_spacing));
        paddingAnimator.addUpdateListener(animation -> {
            int padding = (int) animation.getAnimatedValue();
            mBinding.channelZapperGrid.setSelectedChildSpacing(padding);
        });
        animatorList.add(paddingAnimator);

        // FADE IN/OUT ANIMATION
        // Change buttons visibility based on the openManageChannel parameter
        float from, to;
        if (isFavoriteMode) {
            from = 0f;
            to = 1f;
        } else {
            from = 1f;
            to = 0f;
        }

        // Add fade in/out animations for buttons.
        animatorList.add(ObjectAnimator.ofFloat(mBinding.addRemoveButton, View.ALPHA, from, to));
        animatorList.add(ObjectAnimator.ofFloat(mBinding.doneButton, View.ALPHA, from, to));

        // TRANSLATION ANIMATION
        // The zapper changes its position, while the manage favorite screen is opening/closing.
        animatorList.add(isFavoriteMode ?
                ObjectAnimator.ofFloat(mBinding.channelZapperGrid, View.TRANSLATION_Y, 0, getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_translation_y)) :
                ObjectAnimator.ofFloat(mBinding.channelZapperGrid, View.TRANSLATION_Y, getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_translation_y), 0));

        // LOGO ANIMATION
        if (mChannelLogoWidth == 0 || mChannelLogoHeight == 0 || !mChannelLogoReady) {
            // if the resource is not available do not start the logo animation
            mBinding.logo.setVisibility(GONE);
            mBinding.netflixLogo.setVisibility(GONE);
            return animatorList;
        }

        if (mSelectedChannel.getType() == ChannelType.APP
            || mSelectedChannel.getType() == ChannelType.REGIONAL) {
            animatorList.addAll(getAppLogoAnimation(isFavoriteMode));
        } else {
            animatorList.addAll(getChannelLogoAnimation(isFavoriteMode));
        }

        return animatorList;
    }

    private List<Animator> getChannelLogoAnimation(boolean isFavoriteMode) {
        List<Animator> animatorList = new ArrayList<>();

        // mBinding.logo is a static view, it is positioned like the logo on the program card
        mBinding.logo.setVisibility(VISIBLE);

        // the size of the logo, which is displayed on channel zapper
        int zapperLogoWidth = getContext().getResources().getDimensionPixelSize(R.dimen.tv_channel_zapper_card_logo_width);
        int zapperLogoHeight = getContext().getResources().getDimensionPixelSize(R.dimen.tv_channel_zapper_card_logo_height);

        // the channel logo view height, which is displayed on the program guide card
        int logoViewHeight = getResources().getDimensionPixelOffset(R.dimen.channel_logo_height);
        // these are needed for translation animation of the logo
        int widthOffset = 0, heightOffset = 0;

        ValueAnimator widthAnimator, heightAnimator;

        // Set the animator values based on the logo height/width
        // mChannelLogoWidth - the width of the loaded image
        // mChannelLogoHeight - the height of the loaded image
        // animatedViewHeight - calculated view height if the maximum value is used for view width(zapperLogoWidth)
        int animatedViewHeight = zapperLogoWidth * logoViewHeight / mChannelLogoWidth;
        if (isFavoriteMode) {
            if (animatedViewHeight <= zapperLogoHeight) {
                // width = maximum value
                widthOffset = zapperLogoWidth / 2;
                heightOffset = (zapperLogoHeight - animatedViewHeight) / 2;
                widthAnimator = ValueAnimator.ofInt(mChannelLogoWidth, zapperLogoWidth);
                heightAnimator = ValueAnimator.ofInt(logoViewHeight, animatedViewHeight);
            } else {
                // can not use the maximum value for width, because the image does not fit in the view
                // widthOffset -> the half width of the scaled logo
                widthOffset = (zapperLogoHeight * mChannelLogoWidth / mChannelLogoHeight) / 2;
                // the logo is transformed(check the loadChannelLogo() method) -> loaded image height is not equal with the view height(on program card)
                // it is needed to calculate a vertical padding based on the views height to keep the aspect ratio
                heightOffset = (zapperLogoHeight - zapperLogoWidth * mChannelLogoHeight / mChannelLogoWidth) / 2;
                heightAnimator = ValueAnimator.ofInt(logoViewHeight, zapperLogoWidth * mChannelLogoHeight / mChannelLogoWidth);
                widthAnimator = ValueAnimator.ofInt(mChannelLogoWidth, zapperLogoHeight * mChannelLogoWidth / mChannelLogoHeight);
            }
        } else {
            if (animatedViewHeight <= zapperLogoHeight) {
                widthAnimator = ValueAnimator.ofInt(zapperLogoWidth, mChannelLogoWidth);
                heightAnimator = ValueAnimator.ofInt(animatedViewHeight, logoViewHeight);
            } else {
                widthOffset = mBinding.logo.getWidth() / 2;
                widthAnimator = ValueAnimator.ofInt(zapperLogoHeight * mChannelLogoWidth / mChannelLogoHeight, mChannelLogoWidth);
                heightAnimator = ValueAnimator.ofInt(zapperLogoWidth * mChannelLogoHeight / mChannelLogoWidth, logoViewHeight);
            }
        }

        // logo width animation
        widthAnimator.addUpdateListener(animator -> {
            mBinding.logo.getLayoutParams().width = (Integer) animator.getAnimatedValue();
            mBinding.logo.requestLayout();
        });
        animatorList.add(widthAnimator);

        // logo height animation
        heightAnimator.addUpdateListener(valueAnimator -> {
            mBinding.logo.getLayoutParams().height = (Integer) valueAnimator.getAnimatedValue();
            mBinding.logo.requestLayout();
        });
        animatorList.add(heightAnimator);

        heightOffset += logoViewHeight;

        // Channel logo animation -> translation_y
        ObjectAnimator translationY;
        if (isFavoriteMode) {
            translationY = ObjectAnimator.ofFloat(mBinding.logo, View.TRANSLATION_Y, 0,
                    getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_translation_y) // zapper translation y value
                            + heightOffset);// the calculated offset
        } else {
            translationY = ObjectAnimator.ofFloat(mBinding.logo, View.TRANSLATION_Y, mBinding.logo.getY() // current position of the view
                    - getResources().getDimensionPixelOffset(R.dimen.zapper_channel_logo_top_margin), 0);
        }
        animatorList.add(translationY);

        // Channel logo animation -> translation_x
        ObjectAnimator translationXAnimator;
        if (isFavoriteMode) {
            translationXAnimator =
                    ObjectAnimator.ofFloat(mBinding.logo, View.TRANSLATION_X, 0, mBinding.channelZapperGrid.getWindowAlignmentPosition()
                            - getResources().getDimensionPixelOffset(R.dimen.zapper_channel_logo_start_margin) // x coordinate of channel logo
                            - widthOffset);
        } else {
            translationXAnimator =
                    ObjectAnimator.ofFloat(mBinding.logo, View.TRANSLATION_X, mBinding.logo.getX() // current position of the view
                            - getResources().getDimensionPixelOffset(R.dimen.zapper_channel_logo_start_margin), 0);
        }
        translationXAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mBinding.logo.setVisibility(GONE);
                super.onAnimationEnd(animation);
            }
        });
        animatorList.add(translationXAnimator);

        return animatorList;
    }

    private List<ValueAnimator> getAppLogoAnimation(boolean isFavoriteMode) {
        List<ValueAnimator> animatorList = new ArrayList<>();

        // mBinding.netflixLogo is a static view, it is positioned like the logo on the program card
        mBinding.netflixLogo.setVisibility(VISIBLE);

        // the width of the logo, which is displayed on channel zapper
        int zapperLogoWidth = getContext().getResources().getDimensionPixelSize(R.dimen.tv_channel_zapper_netflix_logo_width);
        int zapperLogoHeight = getContext().getResources().getDimensionPixelSize(R.dimen.tv_channel_zapper_netflix_logo_height);

        // the netflix logo view height, which is displayed on the program guide card
        int logoViewHeight = getResources().getDimensionPixelOffset(R.dimen.zapper_netflix_logo_height);
        int logoViewWidth = getResources().getDimensionPixelOffset(R.dimen.zapper_netflix_logo_width);

        ValueAnimator widthAnimator, heightAnimator;

        if (isFavoriteMode) {
            widthAnimator = ValueAnimator.ofInt(logoViewWidth, zapperLogoWidth);
            heightAnimator = ValueAnimator.ofInt(logoViewHeight, zapperLogoHeight);
        } else {
            widthAnimator = ValueAnimator.ofInt(zapperLogoWidth, logoViewWidth);
            heightAnimator = ValueAnimator.ofInt(zapperLogoHeight, logoViewHeight);
        }

        // logo width animation
        widthAnimator.addUpdateListener(animator -> {
            mBinding.netflixLogo.getLayoutParams().width = (Integer) animator.getAnimatedValue();
            mBinding.netflixLogo.requestLayout();
        });
        animatorList.add(widthAnimator);

        // logo height animation
        heightAnimator.addUpdateListener(valueAnimator -> {
            mBinding.netflixLogo.getLayoutParams().height = (Integer) valueAnimator.getAnimatedValue();
            mBinding.netflixLogo.requestLayout();
        });
        animatorList.add(heightAnimator);

        // Channel logo animation -> translation_y
        ObjectAnimator translationY;
        if (isFavoriteMode) {
            translationY = ObjectAnimator.ofFloat(mBinding.netflixLogo, View.TRANSLATION_Y,
                    0, getResources().getDimensionPixelOffset(R.dimen.zapper_netflix_translation_y_offset));
        } else {
            translationY = ObjectAnimator.ofFloat(mBinding.netflixLogo, View.TRANSLATION_Y,
                    getResources().getDimensionPixelOffset(R.dimen.zapper_netflix_translation_y_offset), 0);
        }
        animatorList.add(translationY);

        // Channel logo animation -> translation_x
        ObjectAnimator translationXAnimator;
        float netflixLogoWidth =  getResources().getDimensionPixelOffset(R.dimen.zapper_netflix_logo_width);
        if (isFavoriteMode) {
            translationXAnimator =
                    ObjectAnimator.ofFloat(mBinding.netflixLogo, View.TRANSLATION_X,
                            0, ( netflixLogoWidth - zapperLogoWidth) / 2);
        } else {
            translationXAnimator =
                    ObjectAnimator.ofFloat(mBinding.netflixLogo, View.TRANSLATION_X,
                            (netflixLogoWidth - zapperLogoWidth) / 2, 0);
        }

        translationXAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mBinding.netflixLogo.setVisibility(GONE);
                super.onAnimationEnd(animation);
            }
        });
        animatorList.add(translationXAnimator);

        return animatorList;
    }

    public Animator fadeAnimation(final boolean fadeIn) {
        float from, to;
        if (fadeIn) {
            mBinding.channelZapperGrid.setVisibility(VISIBLE);
            from = 0f;
            to = 1f;
        } else {
            from = 1f;
            to = 0f;
        }

        // Add fade in/out animations for buttons.
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mBinding.channelZapperGrid, View.ALPHA, from, to);
        objectAnimator.addListener(
                new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (!fadeIn) {
                            mBinding.channelZapperGrid.setVisibility(GONE);
                        }
                        super.onAnimationEnd(animation);
                    }
                }
        );
        return objectAnimator;
    }
}
