package tv.threess.threeready.ui.settings.model;

/**
 * Entity class representing a settings item (title + description).
 *
 * @author Andrei Teslovan
 * @since 2018.11.23
 */
public class SettingsItem implements ISettingsItem {

    private final String mTitle;
    private String mSubtitle;
    private String mPreferenceGroup;

    public SettingsItem(String title, String subtitle) {
        mTitle = title;
        mSubtitle = subtitle;
    }

    public SettingsItem(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setPreferenceGroup(String preferenceGroup) {
        mPreferenceGroup = preferenceGroup;
    }

    @Override
    public String getPreferenceGroup() {
        return mPreferenceGroup;
    }

    public String getSubtitle() {
        return mSubtitle;
    }
}
