/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.tv.presenter.vod;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.vod.model.IBaseVodItem;

/**
 * Abstract base presenter of portrait vod card.
 * <p>
 * Created by Szilard on 6/8/2017.
 */

public abstract class VodPortraitCardPresenter<THolder extends VodPortraitCardPresenter.ViewHolder, TItem extends IBaseVodItem>
        extends VodCardPresenter<THolder, TItem> {

    VodPortraitCardPresenter(Context context) {
        super(context);
    }

    @Override
    public void onBindHolder(ModuleData<?> moduleData, THolder holder, TItem item) {
        super.onBindHolder(moduleData, holder, item);

        updateDescription(holder, item);
    }

    protected void updateDescription(THolder holder, TItem item) {
        holder.getDescriptionView().setText(
                mParentalControlManager.isRestricted(item) ? "" : item.getDescription());
    }

    protected int getMeasuredTextWidth(TextView textview) {
        return (int) textview.getPaint().measureText(textview.getText().toString());
    }

    @Override
    public void onFocusedState(ModuleData<?> moduleData, THolder holder, TItem vod) {
        super.onFocusedState(moduleData, holder, vod);
        updateParentalRatingIcon(holder, vod);
        updateDescriptiveAudioIcon(holder, vod);
        updateDescriptiveSubtitleIcon(holder, vod);
    }

    @Override
    public void onDefaultState(ModuleData<?> moduleData, THolder holder, TItem vod) {
        super.onDefaultState(moduleData, holder, vod);
        updateParentalRatingIcon(holder, vod);
        updateDescriptiveAudioIcon(holder, vod);
        updateDescriptiveSubtitleIcon(holder, vod);
    }

    @Override
    protected void onParentalRatingChanged(THolder holder, TItem item) {
        super.onParentalRatingChanged(holder, item);
        updateDescription(holder, item);
    }


    protected abstract static class ViewHolder extends VodCardPresenter.ViewHolder {

        public ViewHolder(View view) {
            super(view);
        }

        public abstract TextView getDescriptionView();
    }
}
