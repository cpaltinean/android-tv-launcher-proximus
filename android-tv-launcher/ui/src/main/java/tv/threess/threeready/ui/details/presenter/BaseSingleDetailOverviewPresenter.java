package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import org.jetbrains.annotations.NotNull;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.details.utils.ImageTargetView;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.presenter.ActionButtonsPresenter;
import tv.threess.threeready.ui.generic.presenter.ActionModel;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.tv.view.ProgressIndicatorView;
import tv.threess.threeready.ui.utils.TransitionTarget;

/**
 * Presenter class for a single detail overview.
 *
 * @author Daniela Toma
 * @since 2020.05.25
 */
public abstract class BaseSingleDetailOverviewPresenter<THolder extends BaseSingleDetailOverviewPresenter.ViewHolder, TItem extends IContentItem>
        extends BaseDetailsOverviewPresenter<THolder, TItem> {
    private static final String TAG = Log.tag(BaseSingleDetailOverviewPresenter.class);

    protected IDetailLoaded callbackDetail;

    protected boolean mIsPageLoaded;

    protected PresenterSelector mPresenterSelector;

    protected ImageTargetView mImageTargetView;

    /**
     * Gets the bookmark for the specified item.
     */
    protected abstract Observable<IBookmark> getBookmark(THolder holder, TItem item);

    public BaseSingleDetailOverviewPresenter(Context context, IDetailLoaded callback) {
        super(context);

        callbackDetail = callback;
        mPresenterSelector = new InterfacePresenterSelector()
                .addClassPresenter(ActionModel.class, new ActionButtonsPresenter(mContext));
    }

    /**
     * Updates progress bar according to item existing bookmark.
     */
    protected void updateProgressBar(THolder holder, TItem item) {
        RxUtils.disposeSilently(holder.mBookmarkDisposable);
        holder.mBookmarkDisposable = getBookmark(holder, item)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(b -> {
                    holder.mBookmark = b;
                    updateProgressState(holder, item);
                }, throwable -> Log.e(TAG, "Couldn't get the broadcast's bookmark", throwable));
    }

    protected void updateProgressState(final THolder holder, TItem item) {
        updateProgressIndicators(holder);
    }

    /**
     * Display progress bar when available.
     */
    protected void updateProgressIndicators(final THolder holder) {
        ProgressIndicatorView progressBar = holder.getProgressBar();
        if (holder.mBookmark != null && holder.mBookmark.getPosition() > 0) {
            progressBar.setProgressData(holder.mBookmark);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        BaseSingleDetailOverviewPresenter.ViewHolder holder = (ViewHolder) super.onCreateViewHolder(parent);
        holder.mAdapter = new ArrayObjectAdapter<>();
        holder.mItemBridgeAdapter = new ItemBridgeAdapter(holder.mAdapter, mPresenterSelector);
        holder.getActionGridView().setAdapter(holder.mItemBridgeAdapter);

        mImageTargetView = new ImageTargetView(mContext, holder.getGradientBottom(), holder.getFadingStart(), holder.getBigCoverImage(), holder.getCoverImage());

        return holder;
    }

    @Override
    protected int getDescriptionLayout() {
        return R.layout.details_overlay_description;
    }

    @Override
    protected float getDescriptionWidth() {
        return mContext.getResources().getDimensionPixelSize(R.dimen.details_container_width);
    }

    @Override
    protected int getMarginStartForShowMore(View view) {
        return view.getResources().getDimensionPixelOffset(R.dimen.details_more_info_margin_start);
    }

    @Override
    public void onBindHolder(THolder holder, TItem item) {
        super.onBindHolder(holder, item);

        updateProgressBar(holder, item);
        updateImage(holder, item);
    }

    @Override
    public void onUnbindHolder(THolder holder) {
        super.onUnbindHolder(holder);

        RxUtils.disposeSilently(holder.mBookmarkDisposable);

        Glide.with(mContext).clear(mImageTargetView.getFirstTransitionTarget());
        Glide.with(mContext).clear(mImageTargetView.getSecondTransitionTarget());
        mImageTargetView.resetCurrentIndex();
    }

    protected void updateImage(THolder holder, TItem item) {
        int width = mContext.getResources().getDimensionPixelSize(R.dimen.details_cover_width);
        int height = mContext.getResources().getDimensionPixelSize(R.dimen.details_cover_height);

        TransitionTarget target = mImageTargetView.getCurrentIndex() % 2 == 0
                ? mImageTargetView.getFirstTransitionTarget() : mImageTargetView.getSecondTransitionTarget();

        Glide.with(mContext).clear(target);
        Glide.with(mContext).load(item)
                .fallback(R.drawable.content_fallback_image_landscape)
                .error(R.drawable.content_fallback_image_landscape)
                .format(DecodeFormat.PREFER_RGB_565)
                .override(width, height)
                .optionalCenterInside()
                .into(target);

        mImageTargetView.increaseCurrentIndex();
    }

    public abstract static class ViewHolder extends BaseDetailsOverviewPresenter.ViewHolder {

        IBookmark mBookmark;

        Disposable mBookmarkDisposable;

        ArrayObjectAdapter<ActionModel> mAdapter;
        ItemBridgeAdapter mItemBridgeAdapter;

        public ViewHolder(View view) {
            super(view);
        }

        @NotNull
        protected abstract ImageView getBigCoverImage();

        @NotNull
        abstract ProgressIndicatorView getProgressBar();

        @NotNull
        protected abstract FontTextView getSubtitle();

        @NotNull
        protected abstract ImageView getCoverImage();

        @NotNull
        protected abstract View getFadingStart();

        @NotNull
        protected abstract ImageView getGradientBottom();

    }
}
