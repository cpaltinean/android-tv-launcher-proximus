/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.tv.presenter.broadcast;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.generic.ContentMarkers;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.pvr.model.SeriesRecordingStatus;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.details.DetailOpenedFrom;
import tv.threess.threeready.ui.generic.presenter.ContentCardPresenter;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.BroadcastOrderedIconsContainer;
import tv.threess.threeready.ui.pvr.RecordingActionHelper;

/**
 * Abstract base presenter of broadcast type card.
 * <p>
 * Created by Szilard on 6/15/2017.
 */

abstract class BroadcastCardPresenter<THolder extends BroadcastCardPresenter.ViewHolder, TItem extends IBroadcast>
        extends ContentCardPresenter<THolder, TItem> {
    private static final String TAG = Log.tag(BroadcastCardPresenter.class);

    protected final PvrRepository mPvrRepository = Components.get(PvrRepository.class);
    protected final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);
    protected final RecordingActionHelper mRecordingActionHelper = Components.get(RecordingActionHelper.class);

    BroadcastCardPresenter(Context context) {
        super(context);
    }

    @Override
    public void onBindHolder(ModuleData<?> moduleData, THolder holder, TItem broadcast) {
        super.onBindHolder(moduleData, holder, broadcast);

        updateProviderLogo(holder.getGradientView(), holder.getChannelLogoView(), broadcast.getChannelId());
        holder.isFromContinueWatching = moduleData != null && moduleData.getModuleConfig().getDataSource() != null
                && moduleData.getModuleConfig().getDataSource().getMethod() == ModuleDataSourceMethod.CONTINUE_WATCHING;
        updateIndicators(moduleData, holder, broadcast);
    }

    @Override
    public void onUnbindHolder(ModuleData<?> moduleData, THolder holder) {
        super.onUnbindHolder(moduleData, holder);

        // reset the position of progress bar
        holder.bookmark = null;

        holder.getIconContainer().hideAll();

        // reset Channel Logo and Gradient View visibility
        holder.getChannelLogoView().setVisibility(View.GONE);
        holder.getGradientView().setVisibility(View.GONE);

        // Cancel image loadings
        Glide.with(mContext).clear(holder.getChannelLogoView());

        RxUtils.disposeSilently(holder.mRecStatusDisposable);
    }

    @Override
    protected void updateInfoRowText(THolder holder, TItem broadcast) {
        String aux = LocaleTimeUtils.getDate(broadcast, mTranslator, mLocaleSettings);
        SpannableString spannableString = new SpannableString(aux);

        int index = aux.indexOf(TimeUtils.DIVIDER);
        while (index >= 0) {
            spannableString.setSpan(new ForegroundColorSpan(
                            mLayoutConfig.getPlaceholderTransparentFontColor()),
                    index + 1, index + 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            index = aux.indexOf(TimeUtils.DIVIDER, index + 1);
        }

        holder.getInfoTextView().setText(spannableString);
    }

    @Override
    protected void updateContentMarkers(THolder holder, TItem broadcast) {
        holder.getContentMarkerView().showMarkers(broadcast, ContentMarkers.TypeFilter.Cards);
    }

    @Override
    protected void openDetailPage(THolder holder, TItem broadcast) {
        DetailOpenedFrom openedFrom = DetailOpenedFrom.Default;
        if (holder.isFromContinueWatching) {
            openedFrom = DetailOpenedFrom.ContinueWatching;
        } else if (broadcast instanceof IRecording) {
            openedFrom = DetailOpenedFrom.Recording;
        }

        RecordingStatus recordingStatus = holder.getIconContainer().getRecordingStatus();
        if (recordingStatus != null && recordingStatus.getRecording() != null
                && recordingStatus.getSeriesRecordingStatus() == SeriesRecordingStatus.SCHEDULED) {
            mNavigator.showSeriesRecordingDetailPage(recordingStatus.getRecording(), openedFrom);
        } else {
            mNavigator.showProgramDetails(broadcast, openedFrom);
        }
    }

    @Override
    public boolean onKeyEvent(ModuleData<?> moduleData, THolder holder, TItem broadcast, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_MEDIA_RECORD:
                    mRecordingActionHelper.handleRecKeyAction(broadcast,
                            holder.getIconContainer().getRecordingStatus());
                    return true;
            }
        }

        return super.onKeyEvent(moduleData, holder, broadcast, event);
    }

    protected void updateContentDescription(ModuleData<?> moduleData, THolder holder, TItem item) {
        if (!mMwRepository.isTalkBackOn()) {
            return;
        }

        if (moduleData == null || ArrayUtils.isEmpty(moduleData.getItems())) {
            return;
        }

        TalkBackMessageBuilder message = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_TV_OTHER_TILES));
        if ((Integer) holder.view.getTag(R.integer.normalized_adapter_position) == 0) {
            message = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_TV_FIRST_TILE))
                    .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_SWIMLANE, mTranslator.get(moduleData.getTitle()));
        }

        TvChannel channel = mNowOnTvCache.getChannel(item.getChannelId());

        holder.view.setContentDescription(message
                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_CHANNEL_NAME, channel != null ? channel.getName() : item.getChannelId())
                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_ASSET, item.getTitle())
                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_ICONS, holder.getIconContainer().getIconsDescription(mTranslator))
                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_START_TIME, LocaleTimeUtils.getStartDate(item, mTranslator, mLocaleSettings))
                .toString()
        );
    }

    /**
     * Display logos and progress bar.
     */
    protected void updateIndicators(ModuleData<?> moduleData, THolder holder, TItem broadcast) {
        updateContentDescription(moduleData, holder, broadcast);
        updateProgressIndicator(holder);
        updateParentalRatingIcon(holder, broadcast);
        updateRecordingIcon(moduleData, holder, broadcast);
        updateReplayIcon(holder, broadcast);
        updateDescriptiveAudioIcon(holder, broadcast);
        updateDescriptiveSubtitleIcon(holder, broadcast);
    }

    protected void updateReplayIcon(THolder holder, TItem broadcast) {
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        if (broadcast.canReplay(channel)) {
            holder.getIconContainer().showReplay();
        } else {
            holder.getIconContainer().hideReplay();
        }
    }

    protected void updateRecordingIcon(ModuleData<?> moduleData, THolder holder, TItem broadcast) {
        RxUtils.disposeSilently(holder.mRecStatusDisposable);
        holder.mRecStatusDisposable = mPvrRepository.getRecordingStatus(broadcast)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        recordingStatus -> {
                            holder.getIconContainer().showRecordingStatus(recordingStatus);
                            updateContentDescription(moduleData, holder, broadcast);
                            holder.view.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED);
                        }
                        , throwable -> {
                            Log.e(TAG, "Couldn't get the recording status.", throwable);
                            holder.getIconContainer().hideRecordingStatus();
                        });
    }

    protected void updateParentalRatingIcon(THolder holder, TItem broadcast) {
        if (holder.view.isFocused()) {
            holder.getIconContainer().showParentalRating(broadcast.getParentalRating());
        } else {
            holder.getIconContainer().hideParentalRating();
        }
    }

    protected void updateDescriptiveAudioIcon(THolder holder, TItem broadcast) {
        if (broadcast.hasDescriptiveAudio()) {
            holder.getIconContainer().showDescriptiveAudio();
        } else {
            holder.getIconContainer().hideDescriptiveAudio();
        }
    }

    protected void updateDescriptiveSubtitleIcon(THolder holder, TItem broadcast) {
        if (broadcast.hasDescriptiveSubtitle()) {
            holder.getIconContainer().showDescriptiveSubtitle();
        } else {
            holder.getIconContainer().hideDescriptiveSubtitle();
        }
    }

    @Override
    public Observable<IBookmark> getBookmarkObservable(THolder holder, TItem broadcast) {
        return mTvRepository.getBookmarkForBroadcast(broadcast, holder.isFromContinueWatching);
    }

    protected abstract static class ViewHolder extends ContentCardPresenter.ViewHolder {
        Disposable mRecStatusDisposable;

        public boolean isFromContinueWatching;

        ColorDrawable mPlaceHolderDrawable = new ColorDrawable();

        protected ViewHolder(View view) {
            super(view);
        }

        public abstract TextView getInfoTextView();

        public abstract ImageView getChannelLogoView();

        public abstract ImageView getGradientView();

        @Override
        public abstract BroadcastOrderedIconsContainer getIconContainer();
    }
}
