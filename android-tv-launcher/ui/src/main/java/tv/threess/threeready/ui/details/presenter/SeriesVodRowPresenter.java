/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.details.presenter;

import android.content.Context;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tv.threess.threeready.api.generic.model.ISeason;
import tv.threess.threeready.api.generic.model.ISeries;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.ui.details.utils.SeriesFilterType;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;

/**
 * Presenter for the season/episode grid on a series recording detail page.
 *
 * @author Barabas Attila
 * @since 2018.11.25
 */
public class SeriesVodRowPresenter extends SeriesRowPresenter<IBaseVodItem> {

    private IVodSeries mSeries;

    public SeriesVodRowPresenter(Context context, @Nullable IBaseVodItem selectedEpisode,
                                 EpisodeSelectionListener<IBaseVodItem> episodeSelectionListener) {
        super(context, selectedEpisode, episodeSelectionListener);
        mEpisodePresenterSelector = new InterfacePresenterSelector().addClassPresenter(
                IBaseVodItem.class, new VodEpisodeCardPresenter(context, () -> mSeries));
    }

    @Override
    public void onBindHolder(ViewHolder<IBaseVodItem> holder, ISeries<IBaseVodItem> series) {
        super.onBindHolder(holder, series);

        mSeries = (IVodSeries) series;
    }

    @Override
    protected List<SeriesFilterType> getFilters(ViewHolder<IBaseVodItem> holder) {
        return Collections.singletonList(SeriesFilterType.Episode);
    }

    @Override
    protected List<IBaseVodItem> filterEpisodes(ViewHolder<IBaseVodItem> holder, SeriesFilterType selectedFilter) {
        List<IBaseVodItem> filterEpisodes = new ArrayList<>();
        if (selectedFilter == SeriesFilterType.Episode) {
            filterEpisodes = holder.mSeries.getEpisodesSorted();
        }

        return filterEpisodes;
    }

    @Override
    protected List<ISeason<IBaseVodItem>> getSeasons(ViewHolder<IBaseVodItem> holder) {
        return holder.mSeries.getSeasons();
    }
}
