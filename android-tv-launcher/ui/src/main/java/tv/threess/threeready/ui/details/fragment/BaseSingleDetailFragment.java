package tv.threess.threeready.ui.details.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.leanback.widget.HorizontalGridView;

import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.details.presenter.BaseDetailsOverviewPresenter;
import tv.threess.threeready.ui.generic.fragment.BaseDetailFragment;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.api.pc.ParentalControlManager;

/**
 * base for @{@link VodMovieDetailsFragment} and @{@link ProgramDetailsFragment} to handle data and
 * presenters
 *
 * @author hunormadaras
 * @since 06/02/2019
 */
public abstract class BaseSingleDetailFragment<TItem extends IContentItem, TOverviewHolder extends BaseDetailsOverviewPresenter.ViewHolder > extends BaseDetailFragment<TOverviewHolder> {

    protected String mDataId;
    protected TItem mData;

    /**
     * adds the corresponding presenter to the selector
     *
     * @param classPresenterSelector selector from the @{@link tv.threess.threeready.ui.home.view.ModularPageView}
     */
    protected abstract void addExtraPresenter(InterfacePresenterSelector classPresenterSelector);

    /**
     * load and display data details
     *
     * @param dataId id of the data
     */
    protected abstract void loadDataDetails(String dataId);

    /**
     * load and display data details
     *
     * @param data id of the data
     */
    protected abstract void loadDataDetails(TItem data);


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mBinding == null) {
            createView(inflater, container);
            if (mData == null) {
                loadDataDetails(mDataId);
            } else {
                loadDataDetails(mData);
            }

        // Skipp detail page from back stack when it's restricted.
        } else if (mParentalControlManager.isRestricted(mData)) {
            mNavigator.popBackStack();
        }

        mParentalControlManager.addListener(mParentalControlListener);

        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mParentalControlManager.removeListener(mParentalControlListener);
    }

    /**
     * adds the data to the adapter, which will updates its self thanks
     * for @{@link tv.threess.threeready.ui.home.view.ModularPageView} not
     *
     * @param data data which should be added
     */
    protected void addUIData(TItem data) {
        if (mModularPageView == null
                || mModularPageView.getObjectAdapter() == null) {
            return;
        }

        if (mModularPageView.getObjectAdapter().getItems().size() == 0) {
            addExtraPresenter(mModularPageView.getClassPresenterSelector());
            mModularPageView.getObjectAdapter().add(0, data);
        } else {
            mModularPageView.getObjectAdapter().replace(0, data);
        }

        mData = data;
        mNavigator.reportNavigationIfNeeded(this);
    }

    protected int calculateOverviewItemOffset(BaseDetailsOverviewPresenter.ViewHolder viewHolder) {
        if (viewHolder == null) {
            return 0;
        }

        // Force measure and layout the overview to calculate the correct position.
        viewHolder.view.forceLayout();
        Activity activity = mNavigator.getActivity();
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(
            activity.getResources().getDimensionPixelOffset(R.dimen.details_screen_width), MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
        viewHolder.view.measure(widthMeasureSpec, heightMeasureSpec);
        viewHolder.view.layout(0, 0, viewHolder.view.getMeasuredWidth(), viewHolder.view.getMeasuredHeight());

        //check if buttons are shown
        int lastBottom = 0;
        HorizontalGridView actionGridView = viewHolder.getActionGridView();
        if (actionGridView.getVisibility() == View.VISIBLE) {
            lastBottom = actionGridView.getBottom();
        } else if (actionGridView.getVisibility() == View.VISIBLE) {
            lastBottom = actionGridView.getBottom();
        } else if (actionGridView.getVisibility() == View.VISIBLE) {
            lastBottom = actionGridView.getBottom();
        } else if (actionGridView.getVisibility() == View.VISIBLE) {
            lastBottom = actionGridView.getBottom();
        }

        int overviewHeight = viewHolder.view.getMeasuredHeight();
        int overviewMinHeight = activity.getResources().getDimensionPixelOffset(R.dimen.details_screen_meta_info_min_height);
        int stripeTopMargin = activity.getResources().getDimensionPixelOffset(R.dimen.details_screen_first_stripe_margin);

        int offset = Math.max(overviewMinHeight, lastBottom) - overviewHeight;
        return offset + stripeTopMargin;
    }


    // Close the detail page when is restricted by the parental control.
    private final ParentalControlManager.IParentalControlListener mParentalControlListener =
            parentalRating -> {
                if (mParentalControlManager.isRestricted(mData)) {
                    mNavigator.clearAllDialogs();
                    mNavigator.popBackStack();
                }
            };
}
