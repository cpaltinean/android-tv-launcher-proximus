/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.generic.player.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;
import java.util.List;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.api.tv.model.ZappingInfo;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.RestartMode;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.ChannelSwitchInfoBinding;
import tv.threess.threeready.ui.epg.fragment.EpgFragment;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.AnimatorListenerAdapter;
import tv.threess.threeready.ui.generic.utils.AnimatorUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.api.pc.ParentalControlManager;

/**
 * This view is used to display the next/previous channel information.
 * The information is displayed, when the user presses the left/right button on rc and no ui is displayed.
 * <p>
 * Created by Szilard on 1/16/2018.
 */

public class ZappingInfoView extends RelativeLayout implements Component {
    public static final String TAG = Log.tag(ZappingInfoView.class);

    private static final long TUNE_TO_CHANNEL_DELAY = 700;

    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);
    private final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final Translator mTranslator = Components.get(Translator.class);

    protected ChannelSwitchInfoBinding mBinding;

    private final List<IZappingInfoStateChangeListener> mZappingInfoStateChangeListeners = new ArrayList<>();

    private AnimatorSet mZapAnimator;

    private final Handler mHandler = new Handler(Looper.getMainLooper());

    private String mChannelId;
    private boolean isFavorite;

    private long mFirstEventPressedTime;
    private long mLastProcessedEventTime;

    private final long mLongPressSampleRate;
    private final long mZapAnimationDuration;
    private final long mZapAnimationFadeOutDelay;
    private final long mShortZapAnimationDuration;

    public ZappingInfoView(Context context) {
        this(context, null);
    }

    public ZappingInfoView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ZappingInfoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ZappingInfoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        mLongPressSampleRate = getResources().getInteger(R.integer.channel_switch_long_press_sample_rate);
        mZapAnimationDuration = getResources().getInteger(R.integer.channel_switch_animation_duration);
        mZapAnimationFadeOutDelay = getResources().getInteger(R.integer.channel_switch_fade_out_animation_delay);
        mShortZapAnimationDuration = getResources().getInteger(R.integer.channel_switch_short_animation_duration);

        mBinding = ChannelSwitchInfoBinding.inflate(LayoutInflater.from(context), this);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mPlaybackDetailsManager.registerCallback(mPlaybackCallback);

        // Set the current channel to the currently playing one.
        updateChannelData();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        stopAnimation();
        mHandler.removeCallbacksAndMessages(null);
        mPlaybackDetailsManager.unregisterCallback(mPlaybackCallback);
        Glide.with(getContext().getApplicationContext()).clear(mBinding.logo);
    }

    @Override
    protected void onVisibilityChanged(@NonNull View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);

        if (changedView != this) {
            return;
        }

        if (visibility == View.VISIBLE) {
            notifyZappingShown();
        } else {
            notifyZappingHidden();
        }
    }

    /**
     * Zap to the next channel with animation.
     */
    public void zapToNextChannel(StartAction action) {
        zapToPosition(action, getNextChannelPosition());
    }

    /**
     * Zap to the previous channel with animation.
     */
    public void zapToPreviousChannel(StartAction action) {
        zapToPosition(action, getPreviousChannelPosition());
    }

    /**
     * Zap to the give channel with animation.
     *
     * @param channelId The unique identifier of the channel which should be started.
     */
    public void zapToChannel(StartAction action, String channelId, boolean isFromFavourite) {
        zapToPosition(action, mNowOnTvCache.getPositionForChannel(channelId, isFromFavourite));
    }

    /**
     * Zap to the channel with animation at the given position.
     *
     * @param position The position of the channel which should be started.
     */
    private void zapToPosition(StartAction action, int position) {
        ZappingInfo zappingInfo = mNowOnTvCache.getZappingInfoAtPosition(position);
        if (zappingInfo == null) {
            // No zapping info. Nothing to show.
            return;
        }

        bindZappingInfo(zappingInfo);
        if (isLongPress() || isZapAnimatorRunning()) {
            //Start the zap animation and schedule a channel tune
            startZapAnimation();
            mHandler.removeCallbacksAndMessages(null);
            scheduleChannelTune(action);
        } else {
            // Start the channel together with the animation.
            startZapAnimation();
            tuneToChannel(action, zappingInfo.getChannel(), true);
        }
    }

    /**
     * Starts the zapping animation on zapping info.
     */
    public AnimatorSet startZapAnimation() {
        // Check if the previous animation is still running.
        if (isZapAnimatorRunning()) {
            stopAnimation();
        }

        mZapAnimator = getPressAnimation();
        mZapAnimator.start();

        return mZapAnimator;
    }

    /**
     * Method used for stop both animations.
     */
    public void stopAnimation() {
        AnimatorUtils.cancelAnimators(mZapAnimator);
    }

    /**
     * @return The position of the channel next to the currently selected one.
     * The channel list is circular, application channels will be skipped.
     */
    private int getNextChannelPosition() {
        List<ZappingInfo> zappingInfoList = mNowOnTvCache.getZappingInfoList();
        for (int i = 1; i < zappingInfoList.size(); i++) {
            int nextPosition = (getCurrentChannelPosition() + i) % zappingInfoList.size();

            // Skip app channels.
            ZappingInfo zappingInfo = zappingInfoList.get(nextPosition);
            if (zappingInfo != null) {
                return nextPosition;
            }
        }
        return -1;
    }

    /**
     * Updates the current channel data.
     */
    private void updateChannelData() {
        mChannelId = mPlaybackDetailsManager.getChannelId();
        isFavorite = mPlaybackDetailsManager.isFavorite();
    }

    /**
     * Gets the position of the current channel.
     */
    private int getCurrentChannelPosition() {
        return mNowOnTvCache.getPositionForChannel(mChannelId, isFavorite);
    }

    /**
     * @return The position of the channel previous to the currently selected one.
     * The channel list is circular, application channel will be skipped.
     */
    private int getPreviousChannelPosition() {
        List<ZappingInfo> zappingInfoList = mNowOnTvCache.getZappingInfoList();
        for (int i = 1; i < zappingInfoList.size(); i++) {
            int prevPosition = getCurrentChannelPosition() - i;
            prevPosition = prevPosition < 0 ? zappingInfoList.size() + prevPosition : prevPosition;

            // Skip app channels.
            ZappingInfo zappingInfo =zappingInfoList.get(prevPosition);
            if (zappingInfo != null) {
                return prevPosition;
            }
        }

        return -1;
    }


    /**
     * @return The zapping info of the currently selected channel.
     * The zapping info holds the channel and broadcast meta info.
     */
    @Nullable
    private ZappingInfo getCurrentZappingInfo() {
        return mNowOnTvCache.getZappingInfoAtPosition(getCurrentChannelPosition());
    }

    /**
     * Creates the animator list, which is needed for fade animation.
     */
    private List<ObjectAnimator> getFadeAnimatorList(final boolean fadeInAnimation) {
        List<ObjectAnimator> animatorList = new ArrayList<>();
        float fromAlpha, toAlpha;
        float logoFromAlpha, logoToAlpha;
        float overlayFromAlpha, overlayToAlpha;

        if (fadeInAnimation) {
            fromAlpha = mBinding.animationOverlay.getAlpha();
            toAlpha = 1f;

            overlayFromAlpha = 0f;
            overlayToAlpha = 0.5f;

            logoFromAlpha = 0f;
            logoToAlpha = 1f;
        } else {
            fromAlpha = logoFromAlpha = 1f;
            toAlpha = logoToAlpha = overlayToAlpha = 0f;
            overlayFromAlpha = 0.5f;
        }

        long animationDuration, logoAnimationDuration, overlayAnimationDuration;
        animationDuration = logoAnimationDuration = overlayAnimationDuration = mZapAnimationDuration;

        if (fadeInAnimation && (isLongPress() || isZapAnimatorRunning())) {
            //calculate the animation duration for channel number, program title and overlay view
            overlayAnimationDuration = animationDuration = (long) ((toAlpha - mBinding.animationOverlay.getAlpha()) * mZapAnimationDuration);

            //calculate the animation duration for logo view
            logoAnimationDuration = mShortZapAnimationDuration;
        } else if (fadeInAnimation) {
            overlayAnimationDuration = mShortZapAnimationDuration;
        }

        animatorList.add(ObjectAnimator.ofFloat(mBinding.number, View.ALPHA, fromAlpha, toAlpha).setDuration(animationDuration));
        animatorList.add(ObjectAnimator.ofFloat(mBinding.title, View.ALPHA, fromAlpha, toAlpha).setDuration(animationDuration));
        animatorList.add(ObjectAnimator.ofFloat(mBinding.animationOverlay, View.ALPHA, overlayFromAlpha, overlayToAlpha).setDuration(overlayAnimationDuration));
        animatorList.add(ObjectAnimator.ofFloat(mBinding.channelLogoLayout, View.ALPHA, logoFromAlpha, logoToAlpha).setDuration(logoAnimationDuration));
        return animatorList;
    }

    /**
     * Logo and title animation.
     *
     * @return The animator to start and subscribe on it.
     */
    private AnimatorSet getPressAnimation() {
        List<Animator> animatorList = new ArrayList<>(getFadeAnimatorList(true));
        AnimatorSet pressAnimator = new AnimatorSet();
        pressAnimator.playTogether(animatorList);

        for (ObjectAnimator objectAnimator : getFadeAnimatorList(false)) {
            objectAnimator.setStartDelay(mZapAnimationFadeOutDelay);
            pressAnimator.play(objectAnimator);
        }

        pressAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation, boolean isReverse) {
                setVisibility(GONE);
            }
        });

        setVisibility(VISIBLE);
        mBinding.title.setVisibility(isLongPress() ? View.GONE : VISIBLE);
        return pressAnimator;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != KeyEvent.KEYCODE_CHANNEL_DOWN
                && keyCode != KeyEvent.KEYCODE_CHANNEL_UP) {
            // Not handled here.
            return super.onKeyDown(keyCode, event);
        }

        if (mFirstEventPressedTime == 0) {
            mFirstEventPressedTime = System.currentTimeMillis();
        }

        // Limit sample time for long press.
        if (System.currentTimeMillis() - mLastProcessedEventTime > mLongPressSampleRate) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_CHANNEL_DOWN:
                case KeyEvent.KEYCODE_CHANNEL_UP:
                    showIfNeededReplayToLiveAlert(keyCode);
                    break;
            }
            mLastProcessedEventTime = System.currentTimeMillis();
        }

        return super.onKeyDown(keyCode, event);
    }

    /**
     * If the program is in replay than the alert bar with back to live message will be shown,
     * otherwise the zapping to previous/next channel is done.
     */
    private void showIfNeededReplayToLiveAlert(int keyCode) {
        //If the current program type is Replay, or the current program is running in the PLTV buffer, the user should see a Back to live dialog.
        if (mPlaybackDetailsManager.inPlaybackType(PlaybackType.Replay) || mPlaybackDetailsManager.isInPLTVBuffer()) {
            mNavigator.showBackToLiveAlert(changeChannel -> zapToChannel(StartAction.TVPageContinue, keyCode));
        } else {
            zapToChannel(keyCode);
        }
    }

    /**
     * Depending on the keyCode pressed will zap to previous/next channel.
     */
    private void zapToChannel(int keyCode) {
        zapToChannel(null, keyCode);
    }

    private void zapToChannel(StartAction action, int keyCode) {
        if (keyCode == KeyEvent.KEYCODE_CHANNEL_DOWN) {
            zapToPreviousChannel(action != null ? action : StartAction.BtnDown);
        } else {
            zapToNextChannel(action != null ? action : StartAction.BtnUp);
        }
    }

    @Override
    public boolean onKeyUp(final int keyCode, KeyEvent event) {
        // Reset long press sample rates.
        mFirstEventPressedTime = 0;
        mLastProcessedEventTime = 0;

        return super.onKeyUp(keyCode, event);
    }

    /**
     * @return True if the channel up / down buttons are longs pressed at the moment.
     */
    private boolean isLongPress() {
        return mFirstEventPressedTime > 0 &&
                System.currentTimeMillis() - mFirstEventPressedTime > mLongPressSampleRate;
    }

    /**
     * Display the meta info about the channel and currently running program on the screen.
     *
     * @param zappingInfo The zapping info which holds all the meta info.
     */
    private void bindZappingInfo(final ZappingInfo zappingInfo) {
        if (zappingInfo == null) {
            // No zapping info. Ignore details bind.
            return;
        }

        mBinding.logoText.setVisibility(View.GONE);
        mBinding.logoText.setText(zappingInfo.getChannel().getName());

        // Display currently running program title.
        TvChannel channel = zappingInfo.getChannel();
        mBinding.title.setText(getBroadcastTitleByChannelType(zappingInfo.getBroadcast(), channel));
        mBinding.number.setText(String.valueOf(zappingInfo.getChannel().getNumber()));
        mChannelId = zappingInfo.getChannel().getId();
        isFavorite = zappingInfo.isFromFavorites();

        // Load channel logo.
        mBinding.logo.setVisibility(View.VISIBLE);
        Glide.with(getContext().getApplicationContext()).clear(mBinding.logo);
        Glide.with(getContext().getApplicationContext())
                .load(zappingInfo.getChannel())
                .dontAnimate()
                .into(new DrawableImageViewTarget(mBinding.logo) {
                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        // Could not load channel logo. Display title.
                        super.onLoadFailed(errorDrawable);
                        mBinding.logoText.setVisibility(View.VISIBLE);
                        mBinding.logo.setVisibility(View.GONE);
                    }

                    @Override
                    public void onResourceReady(@NonNull Drawable resource,
                                                @Nullable Transition<? super Drawable> transition) {
                        super.onResourceReady(resource, transition);
                        mBinding.logo.setVisibility(View.VISIBLE);
                        mBinding.logoText.setVisibility(View.GONE);
                    }
                });
    }

    private String getBroadcastTitleByChannelType(IBroadcast broadcast, TvChannel channel) {
        if (broadcast == null) {
            return "";
        }

        if (channel != null) {
            switch (channel.getType()) {
                case APP:
                case REGIONAL:
                    return "";
            }
        }
        
        return !mParentalControlManager.isRestricted(broadcast) ? broadcast.getTitle() :
                mTranslator.get(TranslationKey.CARDS_TITLE_CONTENT_LOCKED);
    }


    /**
     * Callback to detect when the currently running channel changed.
     */
    IPlaybackCallback mPlaybackCallback = new IPlaybackCallback() {
        @Override
        public void onCommandSuccess(PlaybackCommand cmd, PlaybackDetails details) {
            if (cmd instanceof StartCommand) {
                post(this::onPlaybackChanged);
            }
        }

        @Override
        public void onCommandFailure(PlaybackCommand cmd, PlaybackDetails details, PlaybackError error) {
            if (cmd instanceof StartCommand) {
                post(this::onPlaybackChanged);
            }
        }

        @Override
        public void onPlaybackEvent(PlaybackEvent event, PlaybackDetails details, PlaybackError error) {
        }

        private void onPlaybackChanged() {
            // Update the currently running channel to the selected one.
            if (!isZapAnimatorRunning()) {
                updateChannelData();
            }
        }
    };

    /**
     * Handle channel tuning, based on the channel type.
     * If the channel type is APP then open the application, if the channel type is not APP then
     * let the playback manager handle it.
     *
     * @param channel    The channel on which the playback should be started.
     * @param showPlayer True if we need to show the Player.
     */
    private void tuneToChannel(StartAction action, TvChannel channel, boolean showPlayer) {
        Log.d(TAG, "Finish up zapping with tuning to channel.");

        zapToSelectedChannel(action, channel, showPlayer);
    }

    /**
     * Zaps to the selected channel.
     *
     * @param channel The channel on which the playback should be started.
     * @param showPlayer True if we need to show the Player.
     */
    private void zapToSelectedChannel(StartAction action, TvChannel channel, boolean showPlayer) {
        ZappingInfo zappingInfo  = getCurrentZappingInfo();
        boolean isFavorite = zappingInfo != null && zappingInfo.isFromFavorites();

        // Zap to the selected channel.
        switch (channel.getType()) {
            case APP:
                Log.d(TAG, "Tuning to app channel: " + channel.getName());
                startChannel(action, channel, isFavorite, false);
                break;
            case RADIO:
                Log.d(TAG, "Tuning to radio channel: " + channel.getName());
                mPlaybackDetailsManager.startChannel(action, channel, isFavorite, RestartMode.LiveChannel);
                if (mNavigator.isContentOverlayDisplayed()) {
                    mNavigator.showRadioPlayer(action, channel.getId(), channel.isFavorite());
                }
                break;
            case REGIONAL:
                Log.d(TAG, "Tuning to regional channel: " + channel.getName());
                startChannel(action, channel, isFavorite, false);
                break;
            case TV:
            default:
                Log.d(TAG, "Tuning to TV channel: " + channel.getName());
                startChannel(action, channel, isFavorite, showPlayer);
                break;
        }
    }

    /**
     * starts channel
     *
     * @param channel    data
     * @param isFavorite favorite state
     * @param showPlayer should show player
     */
    private void startChannel(StartAction action, TvChannel channel, boolean isFavorite, boolean showPlayer) {
        Log.d(TAG, "Tuning to channel: " + channel.getName());
        if (!showPlayer) {
            mNavigator.hideContentOverlay();
        } else {
            if (mNavigator.isContentOverlayDisplayed() && !(mNavigator.getContentFragment() instanceof EpgFragment)) {
                // If the player UI is shown, then it should handle the tune
                mNavigator.showLivePlayer(action, channel.getId(), isFavorite);
            }
        }
        // If the player UI is not shown, then we have to tune ourselves
        mPlaybackDetailsManager.startChannel(action, channel, isFavorite, RestartMode.LiveChannel);
    }

    /**
     * @return True if the zapper animator is not null and is running.
     */
    public boolean isZapAnimatorRunning() {
        return AnimatorUtils.isRunning(mZapAnimator);
    }

    /**
     * Method used for skipping the zap animation.
     */
    public void skipZapAnimator(StartAction action) {
        stopAnimation();

        mBinding.channelLogoLayout.setAlpha(0);
        mBinding.title.setAlpha(0);
        mBinding.number.setAlpha(0);
        mBinding.animationOverlay.setAlpha(0);

        ZappingInfo zappingInfo = getCurrentZappingInfo();
        if (zappingInfo == null) {
            // No zapping info. Nothing to show.
            return;
        }

        tuneToChannel(action, zappingInfo.getChannel(), false);
    }

    /**
     * Register a listener which will be notified when the channel zapping animation becomes visible or hidden.
     * To avoid memory leaks all the registered listeners should be removed. {@link #removeStateChangeListener(IZappingInfoStateChangeListener)}
     *
     * @param listener The listeners which should be registered.
     */
    public void addStateChangeListener(IZappingInfoStateChangeListener listener) {
        mZappingInfoStateChangeListeners.add(listener);
    }

    /**
     * Remove a previously registered zapping state change listener. {@link #addStateChangeListener(IZappingInfoStateChangeListener)}
     *
     * @param listener The listeners which needs to be removed.
     */
    public void removeStateChangeListener(IZappingInfoStateChangeListener listener) {
        mZappingInfoStateChangeListeners.remove(listener);
    }

    /**
     * Notify the active listeners about the zapping animation start.
     */
    private void notifyZappingShown() {
        for (IZappingInfoStateChangeListener listener : mZappingInfoStateChangeListeners) {
            listener.onZappingInfoShown();
        }
    }

    /**
     * Notify the active listeners about the zapping animation end.
     */
    private void notifyZappingHidden() {
        for (IZappingInfoStateChangeListener listener : mZappingInfoStateChangeListeners) {
            listener.onZappingInfoHidden();
        }
    }

    /**
     * Schedules a channel tune.
     */
    private void scheduleChannelTune(StartAction startAction) {
        mHandler.postDelayed(() -> {
            ZappingInfo zappingInfo = getCurrentZappingInfo();
            if (zappingInfo == null) {
                return;
            }
            tuneToChannel(startAction, zappingInfo.getChannel(), true);
        }, TUNE_TO_CHANNEL_DELAY);
    }
}
