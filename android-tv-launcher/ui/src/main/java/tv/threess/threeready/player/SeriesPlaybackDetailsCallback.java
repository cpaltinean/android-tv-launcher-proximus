package tv.threess.threeready.player;

/**
 * Callback interface for series playback related events.
 *
 * @author David Bondor
 * @since 19.07.2022
 */
public interface SeriesPlaybackDetailsCallback extends IPlaybackCallback {
    /**
     * Called when the next episode from the series is prepared.
     */
    default void onNextEpisodeLoaded() {
    }
}