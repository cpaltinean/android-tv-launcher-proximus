package tv.threess.threeready.ui.generic.hint;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.databinding.ExploreHintDialogBinding;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Dialog for Explore hint.
 *
 * @author Daniela Toma
 * @since 2021.11.16
 */
public class ExploreHintDialog extends HintsDialog {
    
    private ExploreHintDialogBinding mBinding;

    public static ExploreHintDialog newInstance(String hintId, boolean hintOpenedFromSettings) {
        Bundle args = new Bundle();
        args.putString(EXTRA_HINT_ID, hintId);
        args.putBoolean(EXTRA_HINT_OPENED_FROM_SETTINGS, hintOpenedFromSettings);
        ExploreHintDialog fragment = new ExploreHintDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.description.setText(mTranslator.get(TranslationKey.HINT_TEXT_EXPLORE));
        mBinding.buttonContainer.nextBtn.setText(mTranslator.get(TranslationKey.DISMISS_BUTTON));
        mBinding.buttonContainer.nextBtn.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_HINTS_EPG_BUTTON_DISMISS))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY, mTranslator.get(TranslationKey.HINT_TEXT_EXPLORE))
                        .toString());
    }

    @Override
    protected View inflateView() {
        if (mBinding == null) {
            mBinding = ExploreHintDialogBinding.inflate(LayoutInflater.from(getContext()));
        }

        return mBinding.getRoot();
    }

    @NonNull
    @Override
    protected ImageView getBackgroundImage() {
        return mBinding.background;
    }

    @NonNull
    @Override
    protected TextView getNextButton() {
        return mBinding.buttonContainer.nextBtn;
    }

    @NonNull
    @Override
    protected TextView getPreviousButton() {
        return mBinding.buttonContainer.previousBtn;
    }
}
