package tv.threess.threeready.ui.settings.model;

import android.graphics.drawable.Drawable;

/**
 * Entity class representing a main settings item.
 *
 * @author Andrei Teslovan
 * @since 2018.09.12
 */
public class MainSettingsItem {

    private final MainItemType mType;
    private final String mTitle;
    private final Drawable mIcon;

    public MainSettingsItem(MainItemType type, String title, Drawable icon) {
        mType = type;
        mTitle = title;
        mIcon = icon;
    }

    public MainItemType getType() {
        return mType;
    }

    public String getTitle() {
        return mTitle;
    }

    public Drawable getIcon() {
        return mIcon;
    }

    public enum MainItemType {
        PARENTAL_CONTROL,
        PRIVACY,
        LANGUAGE,
        RENUMBERING,
        EPG_LAYOUT,
        HINTS,
        RECORDINGS,
        SUPPORT,
        VOD_PURCHASE_HISTORY,
        DEVICE_SETTINGS
    }
}
