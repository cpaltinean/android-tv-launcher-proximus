/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.startup.step;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Step to initializeCache the player in the background.
 * This allows the user to play back any type of content after this step.
 * <p>
 * Note that this needs to be called in order for the player to be initialized.
 *
 * @author Andor Lukacs
 * @since 7/4/18
 */

public class InitPlayerStep implements Step {
    private static final String TAG = Log.tag(InitPlayerStep.class);

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);

    Disposable mDisposable;

    @Override
    public void execute(StepCallback callback) {
        mDisposable = mNowOnTvCache.initialize()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(mAppConfig.getQueryTimeout(), TimeUnit.SECONDS)
                .subscribe(
                        () -> mNavigator.showPlayerSurface(callback),
                        throwable -> {
                            Log.e(TAG, "Couldn't load zapping info provers.", throwable);
                            mNavigator.showPlayerSurface(callback);
                        });
    }

    @Override
    public void cancel() {
        RxUtils.disposeSilently(mDisposable);
    }
}
