package tv.threess.threeready.ui.startup.step;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.RestartMode;

/**
 * handles steps for stand by, to tune to last watched channel, has a previous one
 * at @{@link tv.threess.threeready.dagger.StandByStepCreator} to handle screen loading
 *
 * @author hunormadaras
 * @since 11/02/2019
 */
public class TuneToLastChannelStep implements Step, IPlaybackCallback {
    private static final String TAG = Log.tag(TuneToLastChannelStep.class);

    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);

    private StepCallback mCallback;
    private final StartAction mAction;

    public TuneToLastChannelStep(StartAction action) {
        mAction = action;
    }

    @Override
    public void execute(StepCallback callback) {
        mCallback = callback;

        String lastPlayedChannelId = mNowOnTvCache.getLastPlayedChannelId();
        mPlaybackDetailsManager.startChannel(mAction, lastPlayedChannelId, RestartMode.Forced);
        finishStep();
    }

    /**
     * Finish the step anc cleanup all the resources.
     */
    private void finishStep() {
        Log.d(TAG, "finishStep() called");
        mCallback.onFinished();
    }
}
