package tv.threess.threeready.ui.settings.presenter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.SwitchCompat;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.ui.databinding.ParentalControlItemBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.settings.model.ParentalControlItem;
import tv.threess.threeready.ui.settings.model.ParentalControlItem.ParentalControlItemType;

/**
 * Presenter class for the parental control items.
 *
 * @author Daniela Toma
 * @since 2019.05.23
 */
public class ParentalControlItemPresenter extends BasePresenter<ParentalControlItemPresenter.ViewHolder, ParentalControlItem> {
    public static final String TAG = Log.tag(ParentalControlItemPresenter.class);

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);
    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);
    private Disposable mEnablePcDisposable;
    private Disposable mDisablePcDisposable;

    public ParentalControlItemPresenter(Context context) {
        super(context);
    }

    @Override
    public ParentalControlItemPresenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ParentalControlItemPresenter.ViewHolder(
                ParentalControlItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false)
        );
    }

    @Override
    public void onClicked(ViewHolder holder, ParentalControlItem item) {
        super.onClicked(holder, item);
        if (mInternetChecker.isBackendIsolated()) {
            mNavigator.showFailedToSaveSettingsDialog();
            return;
        }
        switch (item.getType()) {
            case PARENTAL_CONTROL:
                holder.mBinding.itemSwitch.setChecked(!holder.mBinding.itemSwitch.isChecked());
                setColor(holder.mBinding.itemSwitch);

                // Enable parental control.
                if(holder.mBinding.itemSwitch.isChecked()) {
                    RxUtils.disposeSilently(mEnablePcDisposable);
                    mEnablePcDisposable = mParentalControlManager
                            .enableParentalControl()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> Log.d(TAG, "Parental control enabling saved successfully!"),
                                        e -> {
                                                Log.d(TAG, "Parental control save enabling failed!", e);
                                                mNavigator.showFailedToSaveSettingsDialog();
                                                holder.mBinding.itemSwitch.setChecked(!holder.mBinding.itemSwitch.isChecked());
                                                setColor(holder.mBinding.itemSwitch);
                                        });
                } else {
                    // Disable parental control.
                    RxUtils.disposeSilently(mDisablePcDisposable);
                    mDisablePcDisposable = mParentalControlManager
                            .disableParentalControl()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> Log.d(TAG, "Parental control disabling saved successfully!"),
                                    e -> {
                                        Log.d(TAG, "Parental control save disabling failed!", e);
                                        mNavigator.showFailedToSaveSettingsDialog();
                                        holder.mBinding.itemSwitch.setChecked(!holder.mBinding.itemSwitch.isChecked());
                                        setColor(holder.mBinding.itemSwitch);
                                    });
                }
                break;

            case CHANGE_PIN_CODE:
                mNavigator.showChangePINCode();
                break;

            case CHANGE_PURCHASE_PIN:
                mNavigator.showPurchasePin();
                break;

            default:
                Log.d(TAG, "Invalid item type: " + item.getType());
        }
    }

    private void setColor(SwitchCompat switchButton) {
        if (switchButton.getTrackDrawable() != null && switchButton.getThumbDrawable() != null) {
            if (switchButton.isChecked()) {
                switchButton.getTrackDrawable().setColorFilter(mLayoutConfig.getSwitchTrackCheckedColor(), PorterDuff.Mode.SRC_IN);
                switchButton.getThumbDrawable().setColorFilter(mLayoutConfig.getSwitchThumbCheckedColor(), PorterDuff.Mode.SRC_IN);
            } else {
                switchButton.getTrackDrawable().setColorFilter(mLayoutConfig.getSwitchTrackUncheckedColor(), PorterDuff.Mode.SRC_IN);
                switchButton.getThumbDrawable().setColorFilter(mLayoutConfig.getSwitchThumbUncheckedColor(), PorterDuff.Mode.SRC_IN);
            }
        }
    }

    @Override
    public void onBindHolder(ParentalControlItemPresenter.ViewHolder holder, ParentalControlItem item) {
        holder.mBinding.title.setText(item.getTitle());
        if(item.getType() == ParentalControlItemType.PARENTAL_CONTROL) {
            holder.mBinding.itemSwitch.setChecked(mParentalControlManager.isParentalControlEnabled());
            setColor(holder.mBinding.itemSwitch);
            holder.mBinding.itemSwitch.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);
        RxUtils.disposeSilently(mEnablePcDisposable, mDisablePcDisposable);
    }

    public static class ViewHolder extends BasePresenter.ViewHolder {

        private final ParentalControlItemBinding mBinding;

        public ViewHolder(ParentalControlItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
