package tv.threess.threeready.ui.generic.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import tv.threess.threeready.ui.databinding.ActionBarDialogBinding;

/**
 * Generic dialog style which is displayed at the bottom of the screen.
 * It can have one or two button, focus will be on the first button by default.
 *
 * @author Barabas Attila
 * @since 2018.09.25
 */
public class ActionBarDialog extends BaseButtonDialog {

    protected ActionBarDialogBinding mBinding;

    protected static ActionBarDialog newInstance(DialogModel model) {
        ActionBarDialog actionBarDialog = new ActionBarDialog();

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_DIALOG_MODEL, model);

        actionBarDialog.setArguments(args);

        return actionBarDialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = ActionBarDialogBinding.inflate(LayoutInflater.from(getContext()), container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBinding.content.setBackgroundColor(mLayoutConfig.getBackgroundColor());
        mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        mBinding.description.setTextColor(mLayoutConfig.getFontColor());

        if (TextUtils.isEmpty(mModel.getTitle())) {
            mBinding.title.setVisibility(View.GONE);
            mBinding.dialogSeparator.setVisibility(View.GONE);
        }

        mBinding.title.setText(mModel.getTitle());
        mBinding.description.setText(mModel.getDescription());
    }

    @Override
    protected ViewGroup getButtonsContainer() {
        return mBinding.buttonsContainer;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        // set fullscreen
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    /**
     * Utility class to build and create generic action bar dialogs.
     */
    public static class Builder {
        private final DialogModel mModel;

        public Builder() {
            mModel = new AlertDialog.DialogModel();
        }

        /**
         * @param dismissOnBtnClick if true it will automatically
         *                         dismiss the dialog after a button clicked.
         */
        public Builder dismissOnBtnClick(boolean dismissOnBtnClick) {
            mModel.mDismissOnBtnClick = dismissOnBtnClick;
            return this;
        }

        public Builder addButton(String text) {
            mModel.mButtons.add(
                    new ButtonModel(text));
            return this;
        }

        public Builder addButton(String text, ButtonClickListener buttonClickListener) {
            mModel.mButtons.add(
                new ButtonModel(text, buttonClickListener));
            return this;
        }

        public Builder title(String title) {
            mModel.mTitle = title;
            return this;
        }

        public Builder description(String description) {
            mModel.mDescription = description;
            return this;
        }

        /**
         * @param cancelable if true the user will be able to cancel the dialog by pressing
         */
        public Builder cancelable(boolean cancelable) {
            mModel.mIsCancelable = cancelable;
            return this;
        }

        public Builder priority(int priority) {
            mModel.mPriority = priority;
            return this;
        }

        /**
         * Blocks all remote keys including global keys, except the OK button.
         */
        public Builder blockKeys(boolean blockKeys) {
            mModel.mBlockKeys = blockKeys;
            return this;
        }

        public DialogModel buildModel() {
            return mModel;
        }

        public ActionBarDialog build() {
            return ActionBarDialog.newInstance(buildModel());
        }
    }
}
