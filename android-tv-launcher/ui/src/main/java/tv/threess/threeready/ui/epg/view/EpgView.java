package tv.threess.threeready.ui.epg.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import androidx.annotation.NonNull;
import java.util.ArrayList;
import java.util.List;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.EpgBinding;

/**
 * This is the base class for EPG. The EPG can have two variants: A and B
 * The EPG consists of 2 parts: {@link ChannelZapperView} and {@link ProgramGuideView}
 * It contains the list of programs of the default channel list.
 * Allows the user to navigate among the tv programs and tune to tv programs
 * 7 days in past and 7 days in future tv program data is available for the available channels
 *
 * @author Barabas Attila
 * @since 2017.12.04
 */

public class EpgView extends SimplifiedEpgView {

    private EpgBinding mBinding;
    private AnimatorSet mFavoriteModeAnimator;

    public EpgView(Context context) {
        this(context, null);
    }

    public EpgView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EpgView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public EpgView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        mBinding.programGuide.setFavoriteModeTriggerListener(mFavoriteModeChangedListener);

        setActivated(true);
    }

    @Override
    protected void inflateLayout() {
        if (mBinding != null) {
            return;
        }
        
        mBinding = EpgBinding.inflate(LayoutInflater.from(getContext()), this);
    }

    @Override
    @NonNull
    public ChannelZapperView getChannelZapperView() {
        return mBinding.channelZapper;
    }

    @Override
    @NonNull
    public ProgramGuideView getProgramGuideView() {
        return mBinding.programGuide;
    }

    public void initialize(String channelId, boolean isFavorite, boolean expandProgramGuide) {
        initialize(channelId, isFavorite);
        mBinding.programGuide.setShouldExpandProgramGuide(expandProgramGuide);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        mBinding.channelZapper.setFavoriteModeTriggerListener(mFavoriteModeChangedListener);
        mBinding.programGuide.setFavoriteModeTriggerListener(mFavoriteModeChangedListener);
    }

    public void onUserInteraction() {
        mBinding.programGuide.onUserInteraction();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        mBinding.programGuide.setFavoriteModeTriggerListener(null);
    }

    public void onDestroy() {
        mBinding.channelZapper.onDestroy();
        mBinding.programGuide.onDestroy();
    }

    private final FavoriteModeTriggerListener mFavoriteModeChangedListener = new FavoriteModeTriggerListener() {
        @Override
        public void onTriggerFavoriteMode(final boolean isFavoriteMode) {
            // Cancel the animation if it is running.
            if (mFavoriteModeAnimator != null && mFavoriteModeAnimator.isRunning()) {
                mFavoriteModeAnimator.cancel();
            }

            List<Animator> favoriteModeAnimators = new ArrayList<>();
            favoriteModeAnimators.addAll(mBinding.channelZapper.getFavoriteModeAnimator(isFavoriteMode));
            favoriteModeAnimators.addAll(mBinding.programGuide.getFavoriteModeAnimator(isFavoriteMode));

            mFavoriteModeAnimator = new AnimatorSet();
            mFavoriteModeAnimator.setDuration(getContext().getResources().getInteger(R.integer.manage_channel_screen_animation_duration));
            mFavoriteModeAnimator.playTogether(favoriteModeAnimators);

            mFavoriteModeAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);

                    bringChildToFront(mBinding.channelZapper);
                    if (isFavoriteMode) {
                        mBinding.channelZapper.onTriggerFavoriteMode();
                        mBinding.programGuide.onTriggerFavoriteMode();
                    } else {
                        mBinding.channelZapper.onTriggerDefaultMode();
                        mBinding.programGuide.onTriggerDefaultMode();
                    }
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);

                    bringChildToFront(mBinding.programGuide);
                    if (isFavoriteMode) {
                        mBinding.channelZapper.onFavoriteMode();
                        mBinding.programGuide.onFavoriteMode();
                    } else {
                        mBinding.channelZapper.onDefaultMode();
                        mBinding.programGuide.onDefaultMode();
                    }
                }
            });

            mFavoriteModeAnimator.start();
        }
    };

    public void setTvGuideStateChangeListener(ExpandableProgramGuideView.TvGuideStateChangeListener guideStateChangeListener) {
        mBinding.programGuide.setTvGuideStateChangeListener(guideStateChangeListener);
    }

    public void setReportListener(ExpandableProgramGuideView.OnReportListener broadcastSelectionListener) {
        mBinding.programGuide.setReportListener(broadcastSelectionListener);
    }

    public void collapseTvGuide() {
        mBinding.programGuide.collapse(false);
    }

    public void openTvGuide(boolean expandTvGuide) {
        mBinding.programGuide.setShouldExpandProgramGuide(expandTvGuide);
    }

    public boolean isTvGuideExpanded() {
        return mBinding.programGuide.isExpanded() || mBinding.programGuide.shouldExpandProgramGuide();
    }

    public interface FavoriteModeTriggerListener {
        void onTriggerFavoriteMode(boolean isFavoriteMode);
    }

    public void onContentShown() {
        mBinding.channelZapper.onContentShown();
        mBinding.programGuide.onContentShown();
    }

    public void onChannelSwitched(String channelId, boolean isFavorite) {
        mBinding.channelZapper.selectChannel(channelId, isFavorite);
    }

    public boolean isProgramGuideExpended(){
        return mBinding.programGuide.shouldExpandProgramGuide();
    }

    public void onCurrentProgramChanged(IBroadcast broadcast) {
        mBinding.programGuide.onCurrentProgramChanged(broadcast);
    }

}
