package tv.threess.threeready.ui.home.presenter.module.collection.nownext;

import android.content.Context;
import android.util.Pair;
import android.view.View;

import androidx.leanback.widget.ClassPresenterSelector;

import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.home.presenter.module.collection.generic.CollectionModulePresenter;
import tv.threess.threeready.ui.home.view.ModularPageView;
import tv.threess.threeready.ui.tv.presenter.nownext.TvNowNextA2CardPresenter;

/**
 * Presenter used to display now next type, A2 variant collections.
 *
 * @author Zsolt Bokor
 * @since 2019.12.16
 */

public class NowNextA2CollectionModulePresenter extends CollectionModulePresenter {
    private static final int NUMBER_OF_COLUMNS = 4;

    public NowNextA2CollectionModulePresenter(Context context, ModularPageView.PageTitleCallback pageTitle, DataSourceLoader dataSourceLoader) {
        super(context, pageTitle, dataSourceLoader);

        // NowNext A2 collection
        mVariantCardPresenterMap.put(ModuleCardVariant.A2, new ClassPresenterSelector()
                .addClassPresenter(Pair.class, new TvNowNextA2CardPresenter(context)));
    }

    @Override
    protected int getColumnNumber() {
        return NUMBER_OF_COLUMNS;
    }

    /**
     * Gets the value for horizontal padding.
     */
    @Override
    protected int getHorizontalPadding() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_now_next_a2_collection_horizontal_padding);
    }

    /**
     * Gets the value for top padding.
     */
    @Override
    protected int getTopPadding(ViewHolder holder) {
        return (holder.mBinding.collection.mBinding.moduleTitle.getVisibility() == View.VISIBLE
                ? mContext.getResources().getDimensionPixelOffset(R.dimen.now_next_collection_grid_view_top_padding)
                : mContext.getResources().getDimensionPixelOffset(R.dimen.tv_now_next_a2_collection_top_padding));
    }

    /**
     * Gets the value for bottom padding.
     */
    @Override
    protected int getBottomPadding() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.collection_vertical_padding);
    }
}
