package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.leanback.widget.VerticalGridView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SafeScrollGridLayoutDelegate;

/**
 * Common base for vertical grid views in the applications.
 *
 * @author Barabas Attila
 * @since 8/23/22
 */
public class BaseVerticalGridView extends FadingEdgeGridLayout {

    private static final float SMOOTH_SCROLL_SPEED_FACTOR = 2f;

    private int mColumnNumber = 1;

    public BaseVerticalGridView(Context context) {
        super(context);
    }

    public BaseVerticalGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseVerticalGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        setSmoothScrollSpeedFactor(SMOOTH_SCROLL_SPEED_FACTOR);
    }

    @Override
    public void setLayoutManager(@Nullable RecyclerView.LayoutManager layout) {
        super.setLayoutManager(layout);
        SafeScrollGridLayoutDelegate.setSafeScrollGridLayoutDelegate(this, layout);
    }

    /**
     * @return the first completely visible item adapter position
     */
    public int findFirstCompletelyVisibleItemPosition() {
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            if (child.getY() > 0 && child.getY() + child.getHeight() < getHeight()) {
                ViewHolder holder = getChildViewHolder(child);
                return holder != null ? holder.getAdapterPosition() : NO_POSITION;
            }
        }
        return NO_POSITION;
    }


    /**
     * This index is used to define the window alignment offset
     */
    public int getRowIndex() {
        int firstVisibleItemPos = findFirstCompletelyVisibleItemPosition();
        int focusedChildAdapterPos = getSelectedPosition();

        return (focusedChildAdapterPos - firstVisibleItemPos) / mColumnNumber;
    }

    @Override
    public void setNumColumns(int numColumns) {
        super.setNumColumns(numColumns);

        mColumnNumber = numColumns;
    }


    @Override
    public View focusSearch(View focused, int direction) {
        View newFocus = super.focusSearch(focused, direction);

        // Workaround to avoid focus search stuck caused by layout/scroll miss alignment.
        if (newFocus == focused
                && getScrollState() == RecyclerView.SCROLL_STATE_IDLE) {
            removeCallbacks(mFinishScroll);
            postOnAnimation(mFinishScroll);
        }
        return newFocus;
    }

    private final Runnable mFinishScroll = () -> scrollToPosition(getSelectedPosition());
}
