package tv.threess.threeready.ui.home.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import tv.threess.threeready.ui.databinding.StripeModuleViewBinding;
import tv.threess.threeready.ui.generic.view.FontTextView;

import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

/**
 * Created by Dalma Medve on 10/3/2017.
 */

public class StripeModuleView extends LinearLayout {

    public StripeModuleViewBinding mBinding;

    public StripeModuleView(Context context) {
       this(context, null);
    }

    public StripeModuleView(Context context, AttributeSet attrs) {
       this(context, attrs,0);
    }

    public StripeModuleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    public void initialize() {
        mBinding = StripeModuleViewBinding.inflate(LayoutInflater.from(getContext()), this);
        setOrientation(VERTICAL);
        mBinding.grid.setHasFixedSize(true);
    }


    public void setRecyclerViewPool(RecyclerView.RecycledViewPool pool) {
        mBinding.grid.setRecycledViewPool(pool);
    }

    public RecyclerView.RecycledViewPool getRecyclerViewPool() {
        return mBinding.grid.getRecycledViewPool();
    }

    @Override
    public void requestChildFocus(View child, View focused) {
        // Active stripe when focus received.
        setActivated(true);
        dispatchSetActivated(true);

        super.requestChildFocus(child, focused);
    }

    @Override
    public View focusSearch(View focused, int direction) {

        View view = super.focusSearch(focused, direction);

        if (mBinding.grid.indexOfChild(view) == NO_POSITION) {
            // Deactivate stripe on focus lost.
            dispatchSetActivated(false);
            setActivated(false);
        }

        return view;
    }

    @NonNull
    public StripeView getStripeGrid() {
        return mBinding.grid;
    }

    @NonNull
    public FontTextView getTitleView() {
        return mBinding.title;
    }

}
