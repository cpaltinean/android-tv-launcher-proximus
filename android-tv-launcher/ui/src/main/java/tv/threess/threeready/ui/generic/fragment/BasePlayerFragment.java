/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.fragment;

import static tv.threess.threeready.player.contract.PlaybackState.Paused;
import static tv.threess.threeready.player.contract.PlaybackState.Started;
import static tv.threess.threeready.player.contract.PlaybackState.TrickPlay;
import static tv.threess.threeready.player.contract.PlaybackState.TrickPlayPaused;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.generic.model.TimeRange;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.player.IPlaybackDetailsCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.command.AbsoluteJumpCommand;
import tv.threess.threeready.player.command.AppendReplayCommand;
import tv.threess.threeready.player.command.PrependReplayCommand;
import tv.threess.threeready.player.command.RelativeJumpCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.contract.SeekDirection;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.notification.NotificationManager;
import tv.threess.threeready.ui.generic.player.view.PlayerButtonContainer;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.generic.view.PlaybackActionView;
import tv.threess.threeready.ui.tv.callback.SeekBarListener;
import tv.threess.threeready.ui.tv.view.SeekBarView;

import static tv.threess.threeready.ui.utils.UiUtils.runOnUiThread;

/**
 * Abstract base class for all player ui fragments in the application.
 * It will handle the basic player functionality like the seek bar and play pause actions.
 *
 * @author Szende Pal
 * @since 2017.06.22
 */
public abstract class BasePlayerFragment extends BaseFragment implements SeekBarListener, IPlaybackDetailsCallback {
    private static final String TAG = Log.tag(BasePlayerFragment.class);

    public static final String EXTRA_ARG_ACTION = "EXTRA_PLAYBACK_EVENT_ACTION";
    public static final String EXTRA_TRIGGER_KEY_EVENT = "EXTRA_TRIGGER_KEY_EVENT";

    protected final DateTimeFormatter PLAYED_TIME_1 = DateTimeFormatter.ofPattern("H:mm:ss", Locale.getDefault());
    protected final DateTimeFormatter PLAYED_TIME_2 = DateTimeFormatter.ofPattern("mm:ss", Locale.getDefault());
    protected final DateTimeFormatter LEFT_TIME_1 = DateTimeFormatter.ofPattern("-H:mm:ss", Locale.getDefault());
    protected final DateTimeFormatter LEFT_TIME_2 = DateTimeFormatter.ofPattern("-mm:ss", Locale.getDefault());

    protected StartAction mStartAction = StartAction.Undefined;

    protected final Navigator mNavigator = Components.get(Navigator.class);
    protected final NotificationManager mNotificationManager = Components.get(NotificationManager.class);
    protected final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);
    protected final AppConfig mAppConfig = Components.get(AppConfig.class);
    protected final Translator mTranslator = Components.get(Translator.class);

    private final long mRemoveUnskippableTimeout = TimeUnit.SECONDS.toMillis(3);

    private final Runnable mRemoveUnskippableMessage = () -> {
        getViewHolder().getUnskippableMessage().setText(null);
        if (mNavigator.isPlayerUIOnTop() && !mPlaybackDetailsManager.inPlaybackState(PlaybackState.Paused)) {
            mNavigator.hideContentOverlay();
        }
    };


    @Override
    public String getPageId() {
        return "PLAYER_UI";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mUserInactivityDetector.reset(mAppConfig.getPlayerUiHideTimeout());
        mPlaybackDetailsManager.registerCallback(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {
        mStartAction = getSerializableArgument(EXTRA_ARG_ACTION);
        mStartAction = mStartAction == null ? StartAction.Undefined : mStartAction;

        return inflateRootView();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SeekBarView seekBar = getViewHolder().getSeekbar();
        seekBar.setProgressBarListener(this);
        seekBar.setContentDescription(mTranslator.get(TranslationKey.TALKBACK_SEEK_PLAYER));
    }

    @Override
    public void onStart() {
        super.onStart();
        if (isForActivePlayback()) {
            updatePlayPauseButton();
        }
        mPlaybackDetailsManager.registerCallback(this);
        mNavigator.hideBackgroundOverlay();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPlaybackDetailsManager.getCurrentStatus().getState() == Paused) {
            mUserInactivityDetector.stop();
            focusSeekBar();
        }
        KeyEvent event = getParcelableArgument(EXTRA_TRIGGER_KEY_EVENT);
        if (event != null) {
            removeArgument(EXTRA_TRIGGER_KEY_EVENT);
            onKeyDown(event);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        removeArgument(EXTRA_ARG_ACTION);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPlaybackDetailsManager.unregisterCallback(this);
        getViewHolder().getUnskippableContainer().removeCallbacks(mRemoveUnskippableMessage);
    }

    /**
     * Handle the default focus on the player UI after it's displayed.
     */
    protected void requestDefaultFocus() {
        getViewHolder().getSeekbar().requestFocus();
    }

    /**
     * @return The resource id of the layout which should be inflated.
     */
    protected abstract View getRootView();

    protected abstract View inflateRootView();

    protected abstract BasePlayerViewHolder getViewHolder();

    public void showUnskippableUI(PlaybackDetails details, String message) {
        int visible = details.isUnskippablePosition() ? View.GONE : View.VISIBLE;
        int gone = details.isUnskippablePosition() ? View.VISIBLE : View.GONE;
        setPlayerDetailItemsVisibility(visible);
        getViewHolder().getSeekBarLayout().setVisibility(visible);
        getViewHolder().getButtonContainer().setVisibility(visible);
        getViewHolder().getUnskippableContainer().setVisibility(gone);
        if (details.isUnskippablePosition()) {
            if (message != null) {
                getViewHolder().getUnskippableContainer().removeCallbacks(mRemoveUnskippableMessage);
                getViewHolder().getUnskippableContainer().postDelayed(mRemoveUnskippableMessage, mRemoveUnskippableTimeout);
                getViewHolder().getUnskippableMessage().setText(message);
            }

            long time = TimeRange.remainingTime(details.getUnskippableRanges(), details.getPosition());
            String text = LocaleTimeUtils.getUnskippableDuration(time, mTranslator);
            getViewHolder().getUnskippableTime().setText(text);
        } else {
            getViewHolder().getUnskippableContainer().removeCallbacks(mRemoveUnskippableMessage);
            getViewHolder().getUnskippableMessage().setText(null);
            getViewHolder().getUnskippableTime ().setText(null);
        }
    }

    /**
     * Method for separate the player detail visibility change.
     */
    protected void setPlayerDetailItemsVisibility(int visible) {
        getViewHolder().getDetailContainer().setVisibility(visible);
        getViewHolder().getLogoView().setVisibility(visible);
    }

    /**
     * User interaction detector,
     * to prevent the auto hiding of the player UI while the user is navigating.
     */
    @Override
    protected void onUserInactivity() {
        reportPageSelectionByTimeout();
        if (this.isResumed()) {
            clearPlayerButtonStates();
            mNavigator.hideContentOverlayWithFadeOut();
        }
    }

    @Override
    public void onContentHide() {
        super.onContentHide();
        clearSeekingStates();
    }

    @Override
    public boolean onBackPressed() {
        // Hide content overlay when visible.
        if (mNavigator.isContentOverlayDisplayed()) {
            mNavigator.hideContentOverlay();
            return true;
        }

        return super.onBackPressed();
    }

    /**
     * Called when the primary position in the seek bar changes.
     *
     * @param position The new primary position of the seek bar.
     */
    @Override
    public void onPrimaryProgressChanged(long position, long start, long end) {
        if (!isAdded() || isRemoving()) {
            return;
        }

        long playedTime = position - start;
        long remainingTime = end - position;

        // Calculate played time.
        FontTextView playedTimeView = getViewHolder().getPlayedTimeView();
        if (TimeUnit.MILLISECONDS.toHours(playedTime) == 0) {
            playedTimeView.setText(TimeUtils.getTimeFormat(PLAYED_TIME_2, playedTime, TimeBuilder.UTC.toZoneId()));
        } else {
            playedTimeView.setText(TimeUtils.getTimeFormat(PLAYED_TIME_1, playedTime, TimeBuilder.UTC.toZoneId()));
        }

        // Calculate left time.
        FontTextView leftTimeView = getViewHolder().getLeftTimeView();
        if (TimeUnit.MILLISECONDS.toHours(remainingTime) == 0) {
            leftTimeView.setText(TimeUtils.getTimeFormat(LEFT_TIME_2, remainingTime, TimeBuilder.UTC.toZoneId()));
        } else {
            leftTimeView.setText(TimeUtils.getTimeFormat(LEFT_TIME_1, remainingTime, TimeBuilder.UTC.toZoneId()));
        }
    }

    @Override
    public void onSeekingOverflow(SeekDirection direction, long position, boolean progressOverflow, boolean bufferOverflow) {
        if (bufferOverflow) {
            if (mPlaybackDetailsManager.inPlaybackType(PlaybackType.LiveTvIpTv) &&
                    !mPlaybackDetailsManager.isRegionalChannelPlaying()) {
                if (direction == SeekDirection.REWIND) {
                    mNavigator.showSeekReachedEnd();
                }
                if (direction == SeekDirection.FORWARD && !mPlaybackDetailsManager.inPlaybackState(PlaybackState.Paused, PlaybackState.TrickPlayPaused)) {
                    mNotificationManager.showLiveNotification();
                }
            }
        }
    }

    /**
     * Called when the user starts seeking on the seek bar.
     *
     * @param direction The direction of the seeking, FORWARD or REWIND.
     */
    @Override
    public void onStartSeeking(SeekDirection direction) {
        mNavigator.hideLoadingIndicator();
        PlaybackActionView actionView = getViewHolder().getPlaybackActionView();
        switch (direction) {
            case FORWARD:
                actionView.setAction(PlaybackActionView.Action.FAST_FORWARD);
                break;
            case REWIND:
                actionView.setAction(PlaybackActionView.Action.FAST_REWIND);
                break;
        }
    }

    /**
     * Called when the user stops the seeking on the seek bar.
     */
    @Override
    public void onStopSeeking(StartAction startAction, long seekPosition) {
        mPlaybackDetailsManager.jumpByPosition(startAction, seekPosition);
        updatePlayPauseButton();
    }

    @Override
    public void onPlaybackEvent(PlaybackEvent event, PlaybackDetails details, PlaybackError error) {
        runOnUiThread(() -> {
            if (event == PlaybackEvent.BufferEndReached) {
                // We should update the play/pause button if buffer end is reached in paused state
                updatePlayPauseButton();
            }
        });
    }

    /**
     * Called after a command was successfully executed in playback manager.
     *
     * @param cmd     The command which was executed.
     * @param details The details about the command.
     */
    @Override
    public void onCommandSuccess(PlaybackCommand cmd, PlaybackDetails details) {
        runOnUiThread(() -> {
            SeekBarView seekbar = getViewHolder().getSeekbar();
            if (cmd instanceof StartCommand) {
                if (cmd.getDomain() == PlaybackDomain.TrickPlay) {
                    seekbar.unblockSeeking();
                } else {
                    seekbar.resetSeeking();
                }

                if (isPlaybackChanged(cmd)
                        && cmd.getDomain() != PlaybackDomain.TrickPlay) {
                    onPlaybackChanged();
                }
                showUnskippableUI(details, "");
            }

            if (cmd.getDomain() == PlaybackDomain.Replay &&
                    (cmd instanceof PrependReplayCommand || cmd instanceof AppendReplayCommand)) {
                // Refresh the seekable positions after a new playlist item is added to the list
                seekbar.setSeekAblePositions(details.getSeekAbleStart(), details.getSeekAbleEnd());
            }

            if (details == null) {
                return;
            }

            switch (details.getState()) {
                case Paused:
                case Started:
                    updatePlayPauseButton();
                    break;
            }
        });
    }

    /**
     * Called when the state of the displayed play/pause button should be updated.
     */
    protected void updatePlayPauseButton() {
        PlaybackState state = mPlaybackDetailsManager.getCurrentStatus().getState();
        if (state == Paused || state == TrickPlayPaused) {
            displayPauseButton();
            mUserInactivityDetector.stop();
        } else if (state == Started || !state.active) {
            displayPlayButton();
            mUserInactivityDetector.start();
        } else if (state == TrickPlay) {
            getViewHolder().getPlaybackActionView().setAction(null);
        }
    }

    /**
     * Returns true if there is no Start command in the queue and none in dispatch
     *
     * @param cmd the current command for which we got a result
     * @return true or false
     */
    protected boolean isPlaybackChanged(PlaybackCommand cmd) {
        //the current command is a Start one and no other Start command is in the queue
        if (!mPlaybackDetailsManager.isCommandQueued(StartCommand.class)) {
            //and there is no other Start command in dispatch (the current one can still be there)
            PlaybackCommand dispatchCommand = mPlaybackDetailsManager.getDispatchingCommand();
            return !(dispatchCommand instanceof StartCommand) || cmd.getId() == dispatchCommand.getId();
        }
        return false;
    }

    /**
     * Called when the maximum duration,
     * and the focusability of the seek bar needs to be updated. It is done if @{@link StartCommand}
     * isn't in the stack or is not executed at the moment
     */
    protected void updateSeekBar(PlaybackDetails playbackDetails) {
        if (mPlaybackDetailsManager.isCommandStarting(StartCommand.class)) {
            // Ignore progress update. Commands still executing.
            return;
        }

        showUnskippableUI(playbackDetails, null);

        Log.v(TAG, "Update seek bar. Details : " + playbackDetails);
        SeekBarView seekBar = getViewHolder().getSeekbar();
        seekBar.setBounds(
                playbackDetails.getStart(), playbackDetails.getEnd(),
                playbackDetails.getSeekAbleStart(), playbackDetails.getSeekAbleEnd());
        seekBar.setPrimaryProgress(playbackDetails.getPosition());
        seekBar.setSecondaryProgress(playbackDetails.getBufferEnd());
        seekBar.setShowBufferStartMarker(
                mPlaybackDetailsManager.inPlaybackDomain(PlaybackDomain.LiveTvIpTv));
        seekBar.setUnskippableMarkers(playbackDetails.getUnskippableRanges());
        seekBar.setFocusable(mPlaybackDetailsManager.canSeek());
    }

    @Override
    public boolean onKeyDown(KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                mNavigator.cancelFadeOutAnimator();
                mNavigator.showPlayerUI();
                if (mPlaybackDetailsManager.canSeek()) {
                    SeekBarView seekBar = getViewHolder().getSeekbar();
                    if (seekBar.isFocusable() && !seekBar.isFocused()) {
                        seekBar.requestFocus();
                        seekBar.onKeyDown(event.getKeyCode(), event);
                        return true;
                    }
                }
        }
        return super.onKeyDown(event);
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        SeekBarView seekBar = getViewHolder().getSeekbar();
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_DPAD_CENTER:
                if (mNavigator.isContentOverlayDisplayed() && seekBar.isFocused()) {
                    mPlaybackDetailsManager.resumePause();
                }
                return false;
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                mNavigator.showPlayerUI();
                if (mPlaybackDetailsManager.canSeek()) {
                    if (seekBar.isFocusable() && !seekBar.isFocused()) {
                        seekBar.requestFocus();
                        seekBar.onKeyUp(event.getKeyCode(), event);
                        return true;
                    }
                }
        }
        return super.onKeyUp(event);
    }

    @Override
    public void onContentShown() {
        super.onContentShown();

        updatePlayPauseButton();
        requestDefaultFocus();
    }

    /**
     * @return True if the player ui is shown for the currently active playback.
     */
    protected abstract boolean isForActivePlayback();

    /**
     * Method used for clear the buttons states after the player is closed.
     */
    protected abstract void clearPlayerButtonStates();

    /**
     * Method used for clear the seeking states after the player is closed.
     */
    protected void clearSeekingStates() {
        SeekBarView seekBar = getViewHolder().getSeekbar();
        if (seekBar != null && seekBar.isSeekingInProgress()) {
            seekBar.resetSeeking();
        }
    }

    /**
     * Display play icon in the action button.
     */
    protected void displayPlayButton() {
        getViewHolder().getPlaybackActionView().setAction(PlaybackActionView.Action.PLAY);
    }

    /**
     * Display pause icon in the action button.
     */
    protected void displayPauseButton() {
        getViewHolder().getPlaybackActionView().setAction(PlaybackActionView.Action.PAUSE);
        mNavigator.showContentOverlay();
    }

    /**
     * Called when the meta info or the status of the active playback changed.
     * The player UI needs to be updated accordingly.
     */
    protected void onPlaybackChanged() {
        if (isForActivePlayback()) {
            updateSeekBar(mPlaybackDetailsManager.getExclusiveDetails());
        } else if (isResumed()) {
            mNavigator.showPlayerUI();
        }
    }

    /**
     * On Pause command focuses the seek bar.
     */
    protected void focusSeekBar() {
        SeekBarView seekBar = getViewHolder().getSeekbar();
        if(mPlaybackDetailsManager.inPlaybackState(PlaybackState.Paused) && seekBar != null) {
            seekBar.requestFocus();
        }
    }

    /**
     * Called when the content of the player UI should be started.
     *
     * @param action The user action which triggered to start of the playback.
     */
    protected abstract void startPlayback(StartAction action);

    public void updatePlaybackDetails(PlaybackDetails details) {
        SeekBarView seekBar = getViewHolder().getSeekbar();

        if (!isForActivePlayback()) {
            Log.d(TAG, "isForActivePlayback - return");
            return;
        }

        if (seekBar.isSeekingInProgress()) {
            seekBar.setSeekAblePositions(details.getSeekAbleStart(), details.getSeekAbleEnd());
            seekBar.setSecondaryProgress(details.getBufferEnd());
            Log.d(TAG, "isSeekingInProgress - return");
            return;
        }

        if (mPlaybackDetailsManager.isCommandStarting(RelativeJumpCommand.class, AbsoluteJumpCommand.class, StartCommand.class)) {
            Log.d(TAG, "isCommandStarting - return");
            return;
        }

        if (!mPlaybackDetailsManager.inPlaybackState(PlaybackState.Started, PlaybackState.Paused) &&
                !mPlaybackDetailsManager.inTrickPlayState()) {
            //HACK: Update secondary progress if the player is stopped
            switch (mPlaybackDetailsManager.getPlayerType()) {
                case LiveTvIpTv:
                case LiveTvOtt:
                case RadioIpTv:
                case RadioOtt:
                    IBroadcast item = mPlaybackDetailsManager.getBroadcastPlayerData();
                    if (item != null) {
                        seekBar.setBounds(item.getStart(), item.getEnd());
                    }
                    seekBar.setSecondaryProgress(System.currentTimeMillis());
                    break;
            }
            Log.d(TAG, "inPlaybackState - return");
            return;
        }

        updateSeekBar(details);
    }

    @Override
    public void handleBackgroundChange(View view) {
        view.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.player_background, null));
    }

    protected interface BasePlayerViewHolder {

        View getUnskippableContainer();

        SeekBarView getSeekbar();

        View getSeekBarLayout();

        ViewGroup getDetailContainer();

        ImageView getLogoView();

        PlayerButtonContainer getButtonContainer();

        TextView getUnskippableMessage();

        TextView getUnskippableTime();

        FontTextView getPlayedTimeView();

        FontTextView getLeftTimeView();

        PlaybackActionView getPlaybackActionView();
    }
}
