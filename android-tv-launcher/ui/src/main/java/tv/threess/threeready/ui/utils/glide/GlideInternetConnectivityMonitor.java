package tv.threess.threeready.ui.utils.glide;

import com.bumptech.glide.manager.ConnectivityMonitor;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.ConnectivityStateChangeReceiver;
import tv.threess.threeready.api.generic.receivers.InternetChecker;

/**
 * Glide internet checker based connectivity monitor to try to reload the images when the internet connection is back.
 *
 * @author Barabas Attila
 * @since 11/26/20
 */
public class GlideInternetConnectivityMonitor implements ConnectivityMonitor, ConnectivityStateChangeReceiver.OnStateChangedListener {

    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);

    private final ConnectivityListener mConnectivityListener;

    public GlideInternetConnectivityMonitor(ConnectivityListener connectivityListener) {
        mConnectivityListener = connectivityListener;
    }

    @Override
    public void onStart() {
        mInternetChecker.addStateChangedListener(this);
    }

    @Override
    public void onStop() {
        mInternetChecker.removeStateChangedListener(this);
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public void onStateChanged(State state) {
        mConnectivityListener.onConnectivityChanged(state.isInternetAvailable());
    }
}
