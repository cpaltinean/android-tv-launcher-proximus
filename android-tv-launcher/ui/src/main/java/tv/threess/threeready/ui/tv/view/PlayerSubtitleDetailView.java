package tv.threess.threeready.ui.tv.view;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.ui.databinding.PlayerSubtitleDetailBinding;

/**
 * A custom view that is responsible to handle the player subtitle
 * It has two text views and they are important because on the UI we need to ellipsize the subtitle
 * if the stream has subtitle and it is too long. Also need to visualizes the side data (Genre, Date, duration)
 * in the second text view. The difference is in the ellipsize. THe first text view need to ellipsize if the text
 * too long, the second text view always needs to be visible.
 */
public class PlayerSubtitleDetailView extends LinearLayout {

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    private final PlayerSubtitleDetailBinding mBinding;

    public PlayerSubtitleDetailView(Context context) {
        this(context, null);
    }

    public PlayerSubtitleDetailView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PlayerSubtitleDetailView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mBinding = PlayerSubtitleDetailBinding.inflate(LayoutInflater.from(getContext()), this);
    }

    public void updateSubtitle(String subtitle, List<String> sideData) {
        //reset views
        mBinding.subtitle.setText(null);
        mBinding.sideInformation.setText(null);

        if (!TextUtils.isEmpty(subtitle)) {
            mBinding.subtitle.setTextColor(mLayoutConfig.getPlaceholderTransparentFontColor());
            mBinding.subtitle.setText(String.format("%s%s", subtitle, ArrayUtils.isEmpty(sideData) ? "" : TimeUtils.DIVIDER));
        }

        if (sideData != null) {
            String details = TextUtils.join(TimeUtils.DIVIDER, sideData);
            SpannableString spannableString = new SpannableString(details);
            int index = details.indexOf(TimeUtils.DIVIDER);
            while (index >= 0) {
                spannableString.setSpan(new ForegroundColorSpan(
                                mLayoutConfig.getPlaceholderTransparentFontColor()),
                        index + 1, index + 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                index = details.indexOf(TimeUtils.DIVIDER, index + 1);
            }

            mBinding.sideInformation.setTextColor(mLayoutConfig.getPlaceholderTransparentFontColor());
            mBinding.sideInformation.setText(spannableString);
        }

    }
}
