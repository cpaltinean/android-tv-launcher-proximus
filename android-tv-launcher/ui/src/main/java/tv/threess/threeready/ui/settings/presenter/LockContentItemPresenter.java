package tv.threess.threeready.ui.settings.presenter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.ui.databinding.LockContentItemBinding;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.settings.listener.LockContentItemClickListener;
import tv.threess.threeready.ui.settings.model.LockContentItem;

public class LockContentItemPresenter extends BasePresenter<LockContentItemPresenter.ViewHolder, LockContentItem> {
    public static final String TAG = Log.tag(LockContentItemPresenter.class);

    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    protected final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);

    private final LockContentItemClickListener mItemClickListener;

    public LockContentItemPresenter(Context context, LockContentItemClickListener itemClickListener) {
        super(context);
        mItemClickListener = itemClickListener;
    }

    @Override
    public LockContentItemPresenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new LockContentItemPresenter.ViewHolder(
                LockContentItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindHolder(LockContentItemPresenter.ViewHolder holder, LockContentItem item) {
        holder.mBinding.description.setText(item.getDescription());

        holder.mBinding.radioButton.setChecked(
                mParentalControlManager.getSelectedParentalRating() == item.getParentalRating());

        holder.mBinding.radioButton.setButtonTintList(holder.mBinding.radioButton.isChecked()
                ? ColorStateList.valueOf(mLayoutConfig.getRadioButtonCheckedColor())
                : ColorStateList.valueOf(mLayoutConfig.getRadioButtonUncheckedColor()));

        showAgeRestrictionIcons(holder, item);
    }

    @Override
    public void onClicked(LockContentItemPresenter.ViewHolder holder, LockContentItem item) {
        super.onClicked(holder, item);
        mItemClickListener.checkLockContentRadioButton(item);
    }

    private void showAgeRestrictionIcons(LockContentItemPresenter.ViewHolder holder, LockContentItem item) {
        switch (item.getParentalRating()) {
            case Rated10:
                holder.mBinding.ageTen.setVisibility(View.VISIBLE);
            case Rated12:
                holder.mBinding.ageTwelve.setVisibility(View.VISIBLE);
            case Rated16:
                holder.mBinding.ageSixteen.setVisibility(View.VISIBLE);
            case Rated18:
                holder.mBinding.ageEightTeen.setVisibility(View.VISIBLE);
        }
    }


    public static class ViewHolder extends BasePresenter.ViewHolder {

        private final LockContentItemBinding mBinding;

        public ViewHolder(LockContentItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
