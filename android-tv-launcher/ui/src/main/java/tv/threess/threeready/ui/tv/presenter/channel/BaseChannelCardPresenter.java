/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.tv.presenter.channel;

import android.content.Context;

import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.netflix.model.SourceType;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.AppChannelInfo;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.RestartMode;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.generic.presenter.BaseModularCardPresenter;

/**
 * Base presenter class for all channel related cards.
 *
 * @author Barabas Attila
 * @since 2017.10.13
 */
public abstract class BaseChannelCardPresenter<THolder extends BaseCardPresenter.ViewHolder>
        extends BaseModularCardPresenter<THolder, TvChannel> {
    private static final String TAG = Log.tag(BaseChannelCardPresenter.class);

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final TvRepository mTvRepository = Components.get(TvRepository.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    private Disposable mChannelInfoDisposable;

    public BaseChannelCardPresenter(Context context) {
        super(context);
    }

    @Override
    public void onUnbindHolder(ModuleData<?> moduleData, THolder holder) {
        super.onUnbindHolder(moduleData, holder);

        cancelChannelInfoRequest();
    }

    @Override
    public void onClicked(final ModuleData<?> moduleData, THolder holder, final TvChannel channel) {
        super.onClicked(moduleData, holder, channel);

        ModuleConfig module = moduleData.getModuleConfig();

        cancelChannelInfoRequest();

        //check if the channel clicked is favorite
        String typeOfChannel;
        if (module.getDataSource() == null || module.getDataSource().getParams() == null) {
            typeOfChannel = "";
        } else {
            typeOfChannel = module.getDataSource().getParams()
                    .getFilter(ModuleFilterOption.Channel.Type.NAME);
        }

        boolean isFavorite = ModuleFilterOption.Channel.Type.Value.FAVORITE.equals(typeOfChannel);

        // Open application for channel
        switch (channel.getType()) {
            case APP:
                mChannelInfoDisposable = mTvRepository.getAppChannelPackageId(channel.getId())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                packageId -> {
                                    TvChannel nextChannel = mNowOnTvCache.getNextChannel(channel.getId());
                                    TvChannel previousChannel = mNowOnTvCache.getPreviousChannel(channel.getId());
                                    mPlaybackDetailsManager.startApplication(StartAction.Ui,
                                            new AppChannelInfo(
                                                    packageId,
                                                    channel.getNumber(),
                                                    nextChannel != null ? nextChannel.getNumber() : -1,
                                                    previousChannel != null ? previousChannel.getNumber() : -1
                                            ),
                                            isFavorite ? SourceType.FavoriteChannel : SourceType.Channel);
                                },
                                throwable -> Log.e(TAG, "Failed to get the app channel info.", throwable)
                        );
                break;
            case REGIONAL:
                mNavigator.openDeepLinkForChannel(mAppConfig.getRegionalChannelLink(), mPlaybackDetailsManager.getChannelData());
                break;
            case RADIO:
            case TV:
                StartAction action = StartAction.Ui;
                if (module.hasSearchTerm()) {
                    action = StartAction.Search;
                }
                mPlaybackDetailsManager.startChannel(action, channel, isFavorite, RestartMode.LiveChannel);
                mNavigator.hideContentOverlay();
                break;
        }
    }

    @Override
    public long getStableId(TvChannel tvChannel) {
        return Objects.hash(tvChannel.getId());
    }

    /**
     * Cancel all currently running channel info requests.
     */
    private void cancelChannelInfoRequest() {
        if (mChannelInfoDisposable != null
                && !mChannelInfoDisposable.isDisposed()) {
            mChannelInfoDisposable.dispose();
        }
    }
}
