package tv.threess.threeready.ui.generic.presenter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Base Card title presenter. This presenter extends the content card presenter with a title view.
 *
 * @author Solyom Zsolt
 * @since 2020.02.24
 */

public abstract class TitleCardPresenter<THolder extends TitleCardPresenter.ViewHolder, TItem extends IBaseContentItem>
        extends ContentMarkerCardPresenter<THolder, TItem> {

    public TitleCardPresenter(Context context) {
        super(context);
    }

    @Override
    protected void onParentalRatingChanged(THolder holder, TItem item) {
        super.onParentalRatingChanged(holder, item);
        updateTitle(holder, item);
    }

    @Override
    public void onBindHolder(ModuleData<?> moduleData, THolder holder, TItem item) {
        super.onBindHolder(moduleData, holder, item);
        updateTitle(holder, item);
    }

    protected void updateTitle(THolder holder, TItem item) {
        holder.getTitleView().setText(mParentalControlManager.isRestricted(item)
                ? mTranslator.get(TranslationKey.CARDS_TITLE_CONTENT_LOCKED) : item.getTitle());
    }

    public abstract static class ViewHolder extends SimpleVodCardPresenter.ViewHolder {

        @NotNull
        public abstract TextView getTitleView();

        protected ViewHolder(View view) {
            super(view);
        }
    }

}
