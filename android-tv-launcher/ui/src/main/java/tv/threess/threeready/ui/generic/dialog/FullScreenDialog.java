package tv.threess.threeready.ui.generic.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import android.view.ViewTreeObserver;
import android.view.Window;

import androidx.annotation.NonNull;

import tv.threess.threeready.api.account.setting.MemoryCache;
import tv.threess.threeready.api.log.Log;

/**
 * Fullscreen dialog with transparent handling and window focus.
 *
 * @author Barabas Attila
 * @since 7/21/21
 */
public class FullScreenDialog extends Dialog {
    private static final String TAG = Log.tag(FullScreenDialog.class);

    private boolean mHasWindowFocus;

    @Override
    public boolean dispatchKeyEvent(@NonNull KeyEvent event) {
        MemoryCache.LastUserActivity.edit().put(System.currentTimeMillis());
        return super.dispatchKeyEvent(event);
    }

    public FullScreenDialog(@NonNull Context context) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);

        Window window = getWindow();
        if (window == null) {
            // No window in dialog.
            return;
        }

        window.getDecorView().getViewTreeObserver()
                .addOnWindowAttachListener(new ViewTreeObserver.OnWindowAttachListener() {
            @Override
            public void onWindowAttached() {
                Log.d(TAG, "onWindowAttached.");

                if (mHasWindowFocus != window.getDecorView().hasWindowFocus()) {
                    // Request window focus again to update decor view.
                    window.setLocalFocus(mHasWindowFocus, false);
                }
            }

            @Override
            public void onWindowDetached() {
                Log.d(TAG, "onWindowDetached.");
            }
        });
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        Log.d(TAG, "onWindowFocusChanged. HasFocus : " + hasFocus);
        super.onWindowFocusChanged(hasFocus);
        mHasWindowFocus = hasFocus;
    }
}
