/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.player.view;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.error.ErrorConfig;
import tv.threess.threeready.api.home.model.generic.ButtonStyle;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.Error;
import tv.threess.threeready.ui.databinding.PlayerErrorViewBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.AnimatorListenerAdapter;
import tv.threess.threeready.ui.generic.utils.ErrorHelper;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Custom view for Playback errors on top of the player surface.
 *
 * @author Lukacs Andor
 * @since 2017.08.11
 */
public class PlayerErrorView extends RelativeLayout {
    private static final String TAG = Log.tag(PlayerErrorView.class);

    private final PlayerErrorViewBinding mBinding;

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final ErrorHelper mErrorHelper = Components.get(ErrorHelper.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final ButtonStyle mButtonStyle = Components.get(ButtonStyle.class);

    int mCurrentErrorPriority = Integer.MIN_VALUE;
    Animator mAnimator;

    public PlayerErrorView(Context context) {
        this(context, null);
    }

    public PlayerErrorView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PlayerErrorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mBinding = PlayerErrorViewBinding.inflate(LayoutInflater.from(getContext()), this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mBinding.errorRetry.setText(mTranslator.get(TranslationKey.RETRY_BUTTON));
        mBinding.errorRetry.setTextColor(UiUtils.createButtonBrandingColorStateList(mLayoutConfig));
        mBinding.errorRetry.setBackground(UiUtils.createButtonBackground(getContext(), mLayoutConfig, mButtonStyle));
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (mAnimator != null && mAnimator.isRunning()) {
            mAnimator.cancel();
        }
    }

    /**
     * Show the given error message.
     * If an error message already shown.
     * Only the error with the higher priority remains displayed
     */
    public void showError(final Error error) {
        ErrorConfig handler = mErrorHelper.getErrorConfig(error);
        String title = error.getErrorTitle();
        String body = mErrorHelper.getErrorMessage(error);
        String errorCode = error.getErrorCode();
        int priority = handler.getPriority();

        if (mCurrentErrorPriority < priority) {
            mCurrentErrorPriority = priority;
            mBinding.errorTitle.setText(title);
            mBinding.errorBody.setText(body);
            if (errorCode == null) {
                mBinding.errorCodeText.setText(null);
            } else {
                String codeText = mTranslator.get(TranslationKey.ERR_CODE) + " " + errorCode;
                mBinding.errorCodeText.setText(codeText);
            }
            Log.error(TAG, "playback error", error);

            fadeErrorIn();
        }
    }

    public void setOnRetryClickListener(OnClickListener listener) {
        mBinding.errorRetry.setOnClickListener(listener);
        showRetryButton(listener != null);
    }

    public void showRetryButton(boolean show) {
        if (show && mNavigator.isContentOverlayDisplayed()) {
            // content is displayed over the player fragment, hide the retry button
            show = false;
        }
        if (show && !mBinding.errorRetry.hasOnClickListeners()) {
            // no click listener is set on the button, hide the retry button
            show = false;
        }

        if (show) {
            mBinding.errorRetry.setVisibility(VISIBLE);
            mBinding.errorRetry.requestFocus();
        } else {
            mBinding.errorRetry.setVisibility(GONE);
        }
    }

    private void clearErrorMessages() {
        mBinding.errorTitle.setText(null);
        mBinding.errorBody.setText(null);
        mBinding.errorCodeText.setText(null);
    }

    /**
     * Hide the displayed error message.
     */
    public void hideError() {
        Log.d(TAG, "Hide player error");
        fadeErrorOut();
        mCurrentErrorPriority = Integer.MIN_VALUE;
    }


    /**
     * Fade error in with a short animation.
     */
    private void fadeErrorIn() {
        if (mAnimator != null
                && mAnimator.isRunning()) {
            mAnimator.cancel();
        }

        setAlpha(0);
        setVisibility(View.VISIBLE);
        mAnimator = ObjectAnimator.ofFloat(this, View.ALPHA, 0f, 1f);
        mAnimator.setDuration(50);
        mAnimator.start();
    }

    /**
     * Fade error out with a short animation.
     */
    private void fadeErrorOut() {
        if (mAnimator != null
                && mAnimator.isRunning()) {
            mAnimator.cancel();
        }

        mAnimator = ObjectAnimator.ofFloat(this, View.ALPHA, 1f, 0f);
        mAnimator.setDuration(50);
        mAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationFinished(Animator animation) {
                super.onAnimationFinished(animation);
                setVisibility(View.GONE);
                clearErrorMessages();
            }
        });
        mAnimator.start();
    }
}
