package tv.threess.threeready.ui.startup.step;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;

/**
 * Startup step to check if there is a system update and start the download.
 * Before we do the update check, we have to make sure we have internet.
 *
 * @author Barabas Attila
 * @since 9/20/20
 */
public class SystemUpdateCheckStep implements Step {
    private static final String TAG = Log.tag(SystemUpdateCheckStep.class);

    @Override
    public void execute(StepCallback callback) {
        try {
            Components.get(MwRepository.class).startCheckSystemUpdate();
        } catch (Exception ex) {
            Log.e(TAG, "Exception during system update checking." + ex);
        }
        callback.onFinished();
    }
}
