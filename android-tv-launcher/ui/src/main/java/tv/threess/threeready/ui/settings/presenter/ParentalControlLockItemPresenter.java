package tv.threess.threeready.ui.settings.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.databinding.ParentalControlLockItemBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.settings.model.ParentalControlLockItem;

/**
 * Presenter class for the parental control lock item.
 *
 * @author Daniela Toma
 * @since 2019.05.28
 */
public class ParentalControlLockItemPresenter extends BasePresenter<ParentalControlLockItemPresenter.ViewHolder, ParentalControlLockItem> {
    public static final String TAG = Log.tag(ParentalControlLockItemPresenter.class);

    protected final Navigator mNavigator = Components.get(Navigator.class);
    protected final Translator mTranslator = Components.get(Translator.class);
    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);

    public ParentalControlLockItemPresenter(Context context) {
        super(context);
    }

    @Override
    public ParentalControlLockItemPresenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(
                ParentalControlLockItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindHolder(ParentalControlLockItemPresenter.ViewHolder holder, ParentalControlLockItem item) {
        holder.mBinding.title.setText(mTranslator.get(TranslationKey.PARENTAL_CONTROL_SETTINGS_LOCKED_CONTENT_TITLE));
        holder.mBinding.subtitle.setText(mTranslator.get(TranslationKey.PARENTAL_CONTROL_SETTINGS_LOCKED_CONTENT));

        // Reset icon visibility.
        for (int i=0; i<holder.mBinding.ageRestriction.getChildCount(); ++i) {
            holder.mBinding.ageRestriction.getChildAt(i).setVisibility(View.GONE);
        }

        // Show the icons for age rating.
        switch (item.getParentalRating()) {
            case Rated10:
                holder.mBinding.ageTen.setVisibility(View.VISIBLE);
            case Rated12:
                holder.mBinding.ageTwelve.setVisibility(View.VISIBLE);
            case Rated16:
                holder.mBinding.ageSixteen.setVisibility(View.VISIBLE);
            case Rated18:
                holder.mBinding.ageEightTeen.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClicked(ViewHolder holder, ParentalControlLockItem item) {
        if (mInternetChecker.isBackendIsolated()) {
            mNavigator.showFailedToSaveSettingsDialog();
            return;
        }
        mNavigator.openLockContentSettings();
    }

    public static class ViewHolder extends BasePresenter.ViewHolder {

        private final ParentalControlLockItemBinding mBinding;

        public ViewHolder(ParentalControlLockItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
