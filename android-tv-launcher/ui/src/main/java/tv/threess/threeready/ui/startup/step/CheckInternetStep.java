/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.startup.step;

/**
 * Startup step to check internet connection
 *
 * @author Albert Antal
 * @since 2018.08.06
 */
public class CheckInternetStep extends BaseConnectionCheckStep {

    @Override
    protected boolean isConnectionAvailable() {
        return mInternetChecker.isInternetAvailable();
    }
}
