/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.home.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.HorizontalGridView;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.ApiConfig;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.MultipleRowViewBinding;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ModularItemBridgeAdapter;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.generic.presenter.EmptyPresenter;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * The cards are displayed in two rows.
 * Two {@link HorizontalGridView} are used and the all card is added to the first horizontal grid view(if needed)
 * Navigation -> moves two cards at once
 *
 * Created by Szilard on 5/5/2017.
 */

public class MultipleRowStripeView extends RelativeLayout {
    private static final int CHANNEL_NUMBER_IN_ONE_ROW = 6;

    private final ApiConfig mApiConfig = Components.get(ApiConfig.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    
    public final MultipleRowViewBinding mBinding;

    private PresenterSelector mPresenterSelector;
    private ModuleData<?> mModuleData;
    private MultiRowAdapterWrapper mAdapterWrapper;

    private final EmptyPresenter mEmptyPresenter;

    public MultipleRowStripeView(Context context) {
        this(context, null);
    }

    public MultipleRowStripeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MultipleRowStripeView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public MultipleRowStripeView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        mBinding = MultipleRowViewBinding.inflate(LayoutInflater.from(getContext()), this);
        mEmptyPresenter = new EmptyPresenter();

        setPadding(getPaddingStart(),
                getPaddingTop(),
                getPaddingEnd(),
                (int) context.getResources().getDimension(R.dimen.tv_channel_stripe_bottom_padding));
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mBinding.moduleTitle.setTextColor(mLayoutConfig.getFontColor());
        mBinding.secondRow.setFocusable(false);
    }

    /**
     * Recycled view pools allow multiple RecyclerViews to share a common pool of scrap views.
     * This can be useful if you have multiple RecyclerViews with adapters that use the same
     * view types, for example if you have several data sets with the same kinds of item views
     * displayed by a {@link ViewPager ViewPager}.
     *
     * @param pool Pool to set. If this parameter is null a new pool will be created and used.
     */
    public void setRecyclerViewPool(RecyclerView.RecycledViewPool pool) {
        mBinding.firstRow.setRecycledViewPool(pool);
        mBinding.secondRow.setRecycledViewPool(pool);
    }

    /**
     * The activated state is needed, because we decide by this state that the card has an overlay or not.
     */
    @Override
    public void requestChildFocus(final View child, View focused) {
        final int[] focusedViewLocation = new int[2];
        focused.getLocationInWindow(focusedViewLocation);
        post(() -> {
            setActivated(true);
            bringChildToFront(child);
            mBinding.firstRow.dispatchSetActivated(true);
            mBinding.secondRow.dispatchSetActivated(true);
            if (focusedViewLocation[1] < 0) {
                mBinding.secondRow.requestFocus();
            }
        });

        super.requestChildFocus(child, focused);
    }

    @Override
    public View focusSearch(View focused, int direction) {
        View view = super.focusSearch(focused, direction);
        setActivated((direction != FOCUS_UP || mBinding.firstRow.indexOfChild(view) != -1)
                && (direction != FOCUS_DOWN || mBinding.secondRow.indexOfChild(view) != -1 || mBinding.firstRow.indexOfChild(view) != -1));
        return view;
    }


    public void setPresenterSelector(PresenterSelector presenterSelector) {
        mPresenterSelector = new ClassPresenterSelector()
                .addClassPresenter(PlaceholderItem.class, mEmptyPresenter)
                // Presenter class for the extra spacing.
                .addClassPresenterSelector(Object.class, presenterSelector);
    }

    public void setModuleData(ModuleData<?> moduleData) {
        mModuleData = moduleData;
    }

    public void setAdapterWrapper(MultiRowAdapterWrapper adapterWrapper) {
        mAdapterWrapper = adapterWrapper;
        initialize();
    }

    public MultiRowAdapterWrapper getAdapterWrapper() {
        return mAdapterWrapper;
    }

    /**
     * Display stripe details and cards.
     */
    private void initialize() {
        if (mAdapterWrapper != null && mPresenterSelector != null) {
            updateStripeTitle(mModuleData.getModuleConfig());
            ArrayObjectAdapter<?> firstRowAdapter = mAdapterWrapper.getFirstRowAdapter();
            ArrayObjectAdapter<?> secondRowAdapter = mAdapterWrapper.getSecondRowAdapter();

            mBinding.firstRow.setAdapter(new ModularItemBridgeAdapter(mModuleData, firstRowAdapter, mPresenterSelector));
            mBinding.secondRow.setAdapter(new ModularItemBridgeAdapter(mModuleData, secondRowAdapter, mPresenterSelector));
            if (firstRowAdapter.size() > 0) {
                Object item = firstRowAdapter.get(firstRowAdapter.size() - 1);

                // Calculate spacing for the end of the second row.
                Presenter presenter = mPresenterSelector.getPresenter(item);
                if (presenter instanceof BaseCardPresenter) {
                    int gapWidth = ((BaseCardPresenter) presenter).getCardWidth(item);
                    mEmptyPresenter.setWidth(gapWidth);
                }
            }

            //set visibility of the second row depending on how much rows we need
            mBinding.secondRow.setVisibility(mAdapterWrapper.isSingleRow() ? GONE : VISIBLE);
        }
    }

    private void updateStripeTitle(ModuleConfig moduleConfig) {
        mBinding.moduleTitle.setText(mTranslator.get(moduleConfig.getTitle()));
    }

    /**
     * Sets the offset percent for window alignment.
     */
    public void setWindowAlignmentOffsetPercent(int percent) {
        mBinding.firstRow.setWindowAlignmentOffsetPercent(percent);
        mBinding.secondRow.setWindowAlignmentOffsetPercent(percent);
    }

    public void setRowHeight(int rowHeight) {
        mBinding.firstRow.setRowHeight(rowHeight);
        mBinding.secondRow.setRowHeight(rowHeight);
    }

    public void setRowPadding(int left, int top, int right, int bottom) {
        mBinding.firstRow.setPadding(left, top, right, bottom);
        mBinding.secondRow.setPadding(left, mBinding.secondRow.getHorizontalSpacing(), right, 0);
    }

    public void setItemSpacing(int itemSpacing) {
        mBinding.firstRow.setItemSpacing(itemSpacing);
        mBinding.secondRow.setItemSpacing(itemSpacing);

        mBinding.secondRow.setPadding(mBinding.secondRow.getLeft(), mBinding.secondRow.getHorizontalSpacing(),
                mBinding.secondRow.getRight(), itemSpacing);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_UP:
                    if (mBinding.secondRow.getFocusedChild() != null) {
                        int position = mBinding.secondRow.getSelectedPosition();
                        mBinding.firstRow.setSelectedPosition(position);
                        mBinding.firstRow.requestFocus();
                        return true;
                    } else {
                        return false;
                    }
                case KeyEvent.KEYCODE_DPAD_DOWN:
                    if (mAdapterWrapper.isSingleRow()) {
                        return false;
                    }
                    int defaultPageSize = mModuleData.getModuleConfig().getSize(mApiConfig.getDefaultPageSize());
                    if (mBinding.firstRow.getFocusedChild() != null) {
                        if (hasPlaceholderItem() && (mBinding.firstRow.getSelectedPosition() == getFirstRowItemCount() - 1)) {
                            if (getFirstRowItemCount() == 1 || (getSecondRowItemCount() > defaultPageSize / 2 && defaultPageSize % 2 == 0)) {
                                return false;
                            }
                            mBinding.firstRow.setSelectedPosition(getSecondRowItemCount() - 2);
                            mBinding.secondRow.setSelectedPosition(getSecondRowItemCount() - 2);
                            mBinding.secondRow.requestFocus();
                            return true;
                        }

                        mBinding.secondRow.setSelectedPosition(mBinding.firstRow.getSelectedPosition());
                        mBinding.secondRow.requestFocus();
                        return true;
                    } else {
                        return false;
                    }
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    if (mBinding.firstRow.getSelectedPosition() != getFirstRowItemCount() - 1
                            || mBinding.secondRow.getSelectedPosition() != getSecondRowItemCount() - 1) {
                        mBinding.firstRow.smoothScrollToPosition(mBinding.firstRow.getSelectedPosition() + 1);
                        mBinding.secondRow.smoothScrollToPosition(mBinding.secondRow.getSelectedPosition() + 1);
                    }
                    return true;
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    if (hasPlaceholderItem() && mBinding.firstRow.getFocusedChild() != null
                            && mBinding.firstRow.getSelectedPosition() == getFirstRowItemCount() - 1) {
                        mBinding.firstRow.setSelectedPosition(getFirstRowItemCount() - 2);
                        mBinding.secondRow.setSelectedPosition(getFirstRowItemCount() - 2);
                        mBinding.firstRow.requestFocus();
                    } else {
                        mBinding.firstRow.smoothScrollToPosition(mBinding.firstRow.getSelectedPosition() - 1);
                        mBinding.secondRow.smoothScrollToPosition(mBinding.secondRow.getSelectedPosition() - 1);
                    }
                    return true;
                default:
                    return super.dispatchKeyEvent(event);
            }
        }
        return super.dispatchKeyEvent(event);
    }

    private int getFirstRowItemCount() {
        if (mBinding.firstRow.getAdapter() != null) {
            return mBinding.firstRow.getAdapter().getItemCount();
        }
        return 0;
    }

    private int getSecondRowItemCount() {
        if (mBinding.secondRow.getAdapter() != null) {
            return mBinding.secondRow.getAdapter().getItemCount();
        }
        return 0;
    }

    /**
     * Return true if a placeholder item is displayed at the end of the second row.
     */
    public boolean hasPlaceholderItem() {
        ArrayObjectAdapter<Object> adapter = mAdapterWrapper.getSecondRowAdapter();
        if (adapter != null && adapter.size() > 0) {
            Object item = adapter.get(adapter.size() - 1);
            return item instanceof PlaceholderItem;
        }

        return false;
    }


    /**
     * In this class, the adapters are created for a multiple row stripe.
     */

    public static class MultiRowAdapterWrapper {
        private Collection<?> mItemList;
        private final ArrayObjectAdapter<Object> mFirstRowAdapter = new ArrayObjectAdapter<>();
        private final ArrayObjectAdapter<Object> mSecondRowAdapter = new ArrayObjectAdapter<>();

        public void setItems(Collection<?> itemList) {
            mItemList = itemList;
            splitItems();
        }

        public boolean isSingleRow() {
            return getItemCount() <= CHANNEL_NUMBER_IN_ONE_ROW;
        }

        private void splitItems() {
            ArrayList<Object> itemList = new ArrayList<>();

            if (mItemList != null) {
                itemList.addAll(mItemList);
            }

            if (isSingleRow()) {
                mFirstRowAdapter.replaceAll(itemList);
                mSecondRowAdapter.replaceAll(Collections.emptyList());
            } else {
                ArrayList<Object> firstRowList = new ArrayList<>();
                ArrayList<Object> secondRowList = new ArrayList<>();

                int count = 0;
                while (itemList.size() > count) {
                    if (count % 2 == 0) {
                        firstRowList.add(itemList.get(count));
                    } else {
                        secondRowList.add(itemList.get(count));
                    }
                    count++;
                }

                if (firstRowList.size() > secondRowList.size()) {
                    secondRowList.add(new PlaceholderItem());
                }

                mFirstRowAdapter.replaceAll(firstRowList);
                mSecondRowAdapter.replaceAll(secondRowList);
            }
        }

        ArrayObjectAdapter<Object> getFirstRowAdapter() {
            return mFirstRowAdapter;
        }

        ArrayObjectAdapter<Object> getSecondRowAdapter() {
            return mSecondRowAdapter;
        }

        public int getItemCount() {
            if (mItemList == null)
                return 0;
            return mItemList.size();
        }
    }

    /**
     * Dummy placeholder class to end spacing to the end of the second row.
     */
    private static class PlaceholderItem {
    }

}
