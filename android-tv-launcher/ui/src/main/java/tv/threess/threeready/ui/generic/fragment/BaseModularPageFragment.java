/*
 *
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.generic.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.home.view.ModularPageView;

/**
 * Abstract base class for modular page fragments.
 * Created by Szilard on 7/28/2017.
 */
public abstract class BaseModularPageFragment extends BaseFragment implements ModularPageView.PageLoadListener {
    public static final String TAG = Log.tag(BaseModularPageFragment.class);

    protected final Translator mTranslator = Components.get(Translator.class);
    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    protected final Navigator mNavigator = Components.get(Navigator.class);
    protected final InternetChecker mInternetChecker = Components.get(InternetChecker.class);

    @Nullable
    protected ModularPageView mModularPageView;

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        restoreFocus();
    }

    protected abstract FrameLayout getPageContainer();

    protected abstract TextView getEmptyPageTextView();

    @SuppressLint("InflateParams")
    protected ModularPageView addModularPageView() {
        clearPageContent();

        mModularPageView = (ModularPageView) LayoutInflater.from(getContext()).inflate(R.layout.modular_page, null);
        mModularPageView.setLayoutParams(new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        hideEmptyPageMessage();
        getPageContainer().addView(mModularPageView);

        return mModularPageView;
    }

    /**
     * Un subscribe all data source observers, need to check if view is created.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        hideEmptyPageMessage();
        clearPageContent();
    }

    @Override
    public void onContentShown() {
        super.onContentShown();
        restoreFocus();
    }

    protected abstract void displayPage(PageConfig pageConfig);

    /**
     * Clear the displayed modules.
     * Cancel the data loadings and remove listeners.
     */
    protected void clearPageContent() {
        if (mModularPageView != null) {
            mModularPageView.setAdapter(null);
            mModularPageView.cancelPageLoading();
        }

        FrameLayout pageContainer = getPageContainer();
        if (pageContainer != null) {
            pageContainer.removeAllViews();
        }
        mModularPageView = null;
    }

    /**
     * Show "Empty page" message for the user after the page loaded,
     * but there is not data available.
     * @param pageConfig THe page which finished the loading
     */
    protected void showEmptyPageMessage(PageConfig pageConfig) {
        TextView emptyTextView = getEmptyPageTextView();
        if (emptyTextView == null) {
            return;
        }

        String emptyPageMessage = pageConfig != null && pageConfig.getLayoutConfig() != null
                ? pageConfig.getLayoutConfig().getEmptyPageMessage() : getEmptyPageText();

        if (mInternetChecker.isBackendIsolated()) {
            emptyPageMessage = mLayoutConfig.getIsolationModeMessage();
        } else if (emptyPageMessage == null) {
            emptyPageMessage = mLayoutConfig.getEmptyPageMessage();
        }

        if (emptyPageMessage == null) {
            return;
        }

        emptyTextView.setText(mTranslator.get(emptyPageMessage));
        emptyTextView.setVisibility(View.VISIBLE);
    }

    /**
     * Hide "Empty page" message after the page is loaded
     * and there is some data to display for the user.
     */
    protected void hideEmptyPageMessage() {
        TextView emptyTextView = getEmptyPageTextView();
        if (emptyTextView != null) {
            emptyTextView.setVisibility(View.GONE);
        }
    }

    /**
     * Returns a custom empty page message or a null.
     */
    protected String getEmptyPageText(){
        return null;
    }

}
