package tv.threess.threeready.ui.update;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.middleware.SystemUpdateReceiver;
import tv.threess.threeready.api.middleware.UpdateUrgency;
import tv.threess.threeready.ui.generic.dialog.BaseDialogFragment;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.notification.NotificationManager;
import tv.threess.threeready.ui.update.dialog.SystemUpdateOptionalDialog;

/**
 * Component which displays the system update dialog.
 *
 * @author Barabas Attila
 * @since 2020.09.14
 */
public class SystemUpdateDialogManager {

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final SystemUpdateReceiver mSystemUpdateReceiver = Components.get(SystemUpdateReceiver.class);
    protected final NotificationManager mNotificationManager = Components.get(NotificationManager.class);
    protected AppConfig mAppConfig = Components.get(AppConfig.class);

    private final Handler mHandler = new Handler(Looper.getMainLooper());

    private Runnable mShowDialogRunnable;
    private boolean mIsInForeground;


    public void onCreate() {
        mSystemUpdateReceiver.addStateChangedListener(mSystemUpdateListener);
    }

    public void onStart() {
        mIsInForeground = true;

        // The app resumed. Show any pending update dialog now.
        if (mShowDialogRunnable != null) {
            mShowDialogRunnable.run();
            mShowDialogRunnable = null;
        }
    }

    public void onStop() {
        mIsInForeground = false;
    }

    public void onDestroy() {
        mSystemUpdateReceiver.removeStateChangedListener(mSystemUpdateListener);
        mHandler.removeCallbacksAndMessages(null);
    }

    private void showUpdateStatus(Runnable runnable) {
        mShowDialogRunnable = runnable;

        // The app is in foreground. Show it immediately.
        if (mIsInForeground) {
            mShowDialogRunnable.run();
            mShowDialogRunnable = null;
        }
    }

    private final SystemUpdateReceiver.Listener mSystemUpdateListener = new SystemUpdateReceiver.Listener() {
        @Override
        public void onUpdateDownloadFailed(UpdateUrgency urgency, boolean manualCheck) {
            if (manualCheck) {
                // Show the error in case the check was initiated by the user.
                showUpdateStatus(mNavigator::showSystemUpdateFailureDialog);
            }
        }

        @Override
        public void onUpdateDownload(UpdateUrgency urgency, boolean manualCheck) {
            showUpdateStatus(() -> {
                switch (urgency) {
                    case Optional:
                    case Mandatory:
                        BaseDialogFragment lastDialog = mNavigator.getDialogFragment();
                        if (lastDialog instanceof SystemUpdateOptionalDialog) {
                            return;
                        }
                        mNavigator.showDialogOnTop(SystemUpdateOptionalDialog.newInstance());
                        break;

                    case Automatic:
                        if (mAppConfig.getForcedRebootDelay(TimeUnit.MILLISECONDS) > 0) {
                            mNotificationManager.showForcedUpdateNotification();
                        }
                        break;
                }
            });
        }
    };
}
