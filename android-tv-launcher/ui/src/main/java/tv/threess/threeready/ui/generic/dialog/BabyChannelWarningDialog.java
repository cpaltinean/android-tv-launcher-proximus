package tv.threess.threeready.ui.generic.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import tv.threess.lib.di.Components;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.player.fragment.PlayerFragment;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Custom action bar dialog for the baby channel warning.
 * It displays a message that it is harmful for the health of babies to watch TV.
 * The user can dismiss the dialog by pressing, all other buttons are blocked.
 *
 * @author Barabas Attila
 * @since 2018.10.01
 */
public class BabyChannelWarningDialog extends ActionBarDialog {

    private final Navigator mNavigator = Components.get(Navigator.class);

    public static BabyChannelWarningDialog newInstance(Translator translator) {
        DialogModel model = new ActionBarDialog.Builder()
                .title(translator.get(TranslationKey.ACTION_BAR_BABY_CHANNEL_TITLE))
                .description(translator.get(TranslationKey.ACTION_BAR_BABY_CHANNEL_BODY))
                .addButton(translator.get(TranslationKey.FERMER_BUTTON))
                .cancelable(false)
                .blockKeys(false)
                .dismissOnBtnClick(true)
                .buildModel();

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_DIALOG_MODEL, model);

        BabyChannelWarningDialog dialog = new BabyChannelWarningDialog();
        dialog.setArguments(args);

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PlayerFragment playerFragment = mNavigator.getPlayerFragment();

        if (playerFragment != null) {
            playerFragment.blockTrickPlay();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        // Add extra margin for the baby channel warning. It has to cover 30% of the screen.;
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) mBinding.title.getLayoutParams();
        lp.height = getResources().getDimensionPixelOffset(R.dimen.baby_channel_action_bar_height);
        mBinding.title.setGravity(Gravity.BOTTOM);

        return view;
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        super.onKeyUp(event);
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_BACK:
            case KeyEvent.KEYCODE_CHANNEL_UP:
            case KeyEvent.KEYCODE_CHANNEL_DOWN:
                dismiss();
                return false;
            case KeyEvent.KEYCODE_TV_RADIO_SERVICE:
                return false;
        }
        return true;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        PlayerFragment playerFragment = mNavigator.getPlayerFragment();

        if (playerFragment != null) {
            playerFragment.unblockTrickPlay();
        }
    }
}
