package tv.threess.threeready.ui.details.utils;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.utils.TransitionTarget;

/**
 * Base class for Glide image target(used to load images on detail pages).
 *
 * @author Daniela Toma
 * @since 2022.04.04
 */
public class ImageTargetView {
    protected int mCurrentTargetIndex = 0;
    protected TransitionTarget mFirstTransitionTarget;
    protected TransitionTarget mSecondTransitionTarget;

    private final ImageView mGradientBottom;
    private final View mFadingStart;
    private final ImageView mBigCoverImage;
    private final ImageView mCoverImage;
    private Context mContext;

    ImageView targetView;

    public ImageTargetView(Context context, ImageView gradientBottom, View fadingStart,ImageView bigCoverImage, ImageView coverImage) {
        mGradientBottom = gradientBottom;
        mFadingStart = fadingStart;
        mBigCoverImage = bigCoverImage;
        mCoverImage = coverImage;
        mContext = context;

        initImageTarget();
    }

    private void initImageTarget() {
        int width = mContext.getResources().getDimensionPixelSize(R.dimen.details_cover_width);
        int height = mContext.getResources().getDimensionPixelSize(R.dimen.details_cover_height);
        mFirstTransitionTarget = new TransitionTarget(width, height, mOnPrepareTargetViewListener);
        mSecondTransitionTarget = new TransitionTarget(width, height, mOnPrepareTargetViewListener);
    }

    public void resetCurrentIndex() {
        mCurrentTargetIndex = 0;
    }

    public void increaseCurrentIndex() {
        mCurrentTargetIndex++;
    }

    public ImageView getGradientBottom() {
        return mGradientBottom;
    }

    public View getFadingStart() {
        return mFadingStart;
    }

    public ImageView getBigCoverImage() {
        return mBigCoverImage;
    }

    public ImageView getCoverImage() {
        return mCoverImage;
    }

    public TransitionTarget getFirstTransitionTarget() {
        return mFirstTransitionTarget;
    }

    public TransitionTarget getSecondTransitionTarget() {
        return mSecondTransitionTarget;
    }

    public int getCurrentIndex() {
        return mCurrentTargetIndex;
    }

    public void clearImage() {
        Glide.with(mContext).clear(getFirstTransitionTarget());
        Glide.with(mContext).clear(getSecondTransitionTarget());
    }

    protected void loadCoverImage() {
        targetView = getCoverImage();
        getGradientBottom().setVisibility(View.GONE);
        getFadingStart().setVisibility(View.GONE);
        getBigCoverImage().setVisibility(View.INVISIBLE);
        getBigCoverImage().setImageDrawable(null);
    }

    protected void loadBigCoverImage() {
        targetView = getBigCoverImage();
        getGradientBottom().setVisibility(View.VISIBLE);
        getFadingStart().setVisibility(View.VISIBLE);
        getCoverImage().setVisibility(View.INVISIBLE);
        getCoverImage().setImageDrawable(null);
    }

    protected void showTargetView() {
        targetView.setVisibility(View.VISIBLE);
    }

    /**
     * Decide if the image can be displayed as background or cover.
     */
    private final TransitionTarget.OnPrepareTargetViewListener mOnPrepareTargetViewListener =
            drawable -> {
                int width = drawable.getIntrinsicWidth();
                int height = drawable.getIntrinsicHeight();
                if (width <=  mContext.getResources().getDimensionPixelSize(R.dimen.details_cover_image_max_width)
                        && height <= mContext.getResources().getDimensionPixelOffset(R.dimen.details_cover_image_max_height)) {
                    loadCoverImage();
                } else {
                    loadBigCoverImage();
                }
                showTargetView();
                return targetView;
            };
}
