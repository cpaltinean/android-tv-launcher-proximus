/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.tv.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.player.RestartMode;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.contract.SeekDirection;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.ui.databinding.PlayerBaseContainerBinding;
import tv.threess.threeready.ui.databinding.TvPlayerBinding;
import tv.threess.threeready.ui.generic.hint.PlayerUIButtonHintsDialog;
import tv.threess.threeready.ui.generic.player.view.PlayerButtonContainer;
import tv.threess.threeready.ui.generic.view.BaseOrderedIconsContainer;
import tv.threess.threeready.ui.generic.view.BroadcastOrderedIconsContainer;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.generic.view.PlaybackActionView;
import tv.threess.threeready.ui.tv.view.ContentMarkersView;
import tv.threess.threeready.ui.tv.view.PlayerButtonView;
import tv.threess.threeready.ui.tv.view.PlayerSubtitleDetailView;
import tv.threess.threeready.ui.tv.view.SeekBarView;

/**
 * The Live player contains the following elements: metadata, progress bar, duration, logos, buttons.
 *
 * @author Szende Pal
 * @since 2017.05.15
 */
public class TvPlayerFragment extends BroadcastPlayerFragment {
    public static final String TAG = Log.tag(TvPlayerFragment.class);

    private static final String EXTRA_CHANNEL_IS_FAVORITE = "EXTRA_CHANNEL_IS_FAVORITE";
    private static final String EXTRA_BROADCAST = "EXTRA_BROADCAST";

    private boolean mIsChannelFavorite;
    private IBroadcast mBroadcast;

    private TvPlayerBinding mBinding;
    private PlayerBaseContainerBinding mBaseContainerBinding;

    // Replay Player
    public static TvPlayerFragment newInstance(IBroadcast broadcast, boolean shouldStartPlayback, boolean isFromFavourite) {
        return newInstance(isFromFavourite, broadcast, shouldStartPlayback);
    }

    public static TvPlayerFragment newInstance(boolean isFavorite, IBroadcast broadcast, boolean shouldStartPlayback) {
        Bundle args = new Bundle();
        args.putBoolean(EXTRA_SHOULD_START_PLAYBACK, shouldStartPlayback);
        args.putBoolean(EXTRA_CHANNEL_IS_FAVORITE, isFavorite);
        args.putSerializable(EXTRA_BROADCAST, broadcast);

        TvPlayerFragment fragment = new TvPlayerFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mIsChannelFavorite = getBooleanArgument(EXTRA_CHANNEL_IS_FAVORITE);
        mBroadcast = getSerializableArgument(EXTRA_BROADCAST);
    }

    @Override
    protected View getRootView() {
        return mBinding != null ? mBinding.getRoot() : null;
    }

    @Override
    public View inflateRootView() {
        if (mBinding == null) {
            mBinding = TvPlayerBinding.inflate(LayoutInflater.from(getContext()));
            mBaseContainerBinding = PlayerBaseContainerBinding.bind(mBinding.getRoot());
        }

        return mBinding.getRoot();
    }

    @Override
    protected BroadcastPlayerViewHolder getViewHolder() {
        return mViewHolder;
    }

    @Nullable
    @Override
    public IBroadcast getPlayerStartedItem() {
        return mBroadcast;
    }

    @Override
    @Nullable
    public IBroadcast getPlaybackData() {
        return mPlaybackDetailsManager.getBroadcastPlayerData();
    }

    @Override
    public void onSeekingOverflow(SeekDirection direction, long position, boolean progressOverflow, boolean bufferOverflow) {
        super.onSeekingOverflow(direction, position, progressOverflow, bufferOverflow);
        IBroadcast broadcast = mPlaybackDetailsManager.getBroadcastPlayerData(position, false);

        Log.d(TAG, "onSeekingOverflow() called with: direction = [" + direction + "], " +
                "position = [" + Log.timestamp(position) + "], " +
                "progressOverflow = [" + progressOverflow + "], " +
                "bufferOverflow = [" + bufferOverflow + "]");

        if (broadcast == null) {
            Log.w(TAG, "No broadcast available to update the UI with.");
            finishTrickPlay();
            return;
        }

        if (mParentalControlManager.isRestricted(broadcast)) {
            Log.w(TAG, "Broadcast is restricted.");
            finishTrickPlay();

            // Lock playback if the broadcast is parental control restricted
            mNavigator.showParentalControlNotification(broadcast);
        }

        if (bufferOverflow) {
            if (mPlaybackDetailsManager.inPlaybackType(PlaybackType.Replay)) {
                finishTrickPlay();
                if (broadcast.isLive() && direction == SeekDirection.FORWARD && !mPlaybackDetailsManager.inPlaybackState(PlaybackState.Paused, PlaybackState.TrickPlayPaused)) {
                    mNotificationManager.showLiveNotification();
                    if (!mPlaybackDetailsManager.inTrickPlayState()) {
                        mPlaybackDetailsManager.startChannel(StartAction.Forward,
                                mPlaybackDetailsManager.getChannelId(), mPlaybackDetailsManager.isFavorite(), RestartMode.Forced);
                    }
                }
            }
        } else {
            SeekBarView seekBar = getViewHolder().getSeekbar();
            updatePlaybackDetails(broadcast);
            PlaybackDetails playbackDetails = mPlaybackDetailsManager.getExclusiveDetails();
            seekBar.setBounds(broadcast.getStart(), broadcast.getEnd(),
                    playbackDetails.getSeekAbleStart(), playbackDetails.getSeekAbleEnd());
            seekBar.setPrimaryProgress(position);
            seekBar.setSecondaryProgress(playbackDetails.getBufferEnd());
            mPlaybackDetailsManager.handleChangeBroadcast(seekBar.getTrickplaySpeed(direction), position);
        }
    }

    @Override
    protected void initializeButtons() {
        super.initializeButtons();

        if (mBroadcast != null && !mBroadcast.isLive()) {
            mBinding.buttonsContainerLayout.recordButton.setVisibility(View.GONE);
            return;
        }

        // Put jump to live button to gone by default on this page.
        mBinding.buttonsContainerLayout.jumpToLive.setVisibility(View.GONE);
    }

    @Override
    protected void startPlayback(StartAction action) {
        if (mBroadcast == null) {
            Log.e(TAG, "Can't start playing. No meta info available");
            return;
        }

        String channelId = mBroadcast.getChannelId();
        Log.d(TAG, "startLivePlayback() called with: channelId = [" + channelId + "], action = [" + action + "]");
        switch (action) {
            case StartOver:
                PlaybackType playbackType = mPlaybackDetailsManager.getPlayerType();
                if (playbackType == PlaybackType.LiveTvDvbC
                        || playbackType == PlaybackType.LiveTvOtt
                        || playbackType == PlaybackType.LiveTvDvbT
                        || playbackType == PlaybackType.LiveTvIpTv) {
                    // check if the playback on which the start over button triggered is live
                    // to set the StartAction to LiveStartOver
                    // LiveStartOver means that the next playback start will change the playback domain
                    // from live playback type. This is important for data collection reporting.
                    action = StartAction.LiveStartOver;
                }

                mPlaybackDetailsManager.startReplay(action, mBroadcast, true, true, mIsChannelFavorite);
                break;
            case JumpLive:
                mPlaybackDetailsManager.startChannel(action, channelId, mIsChannelFavorite, RestartMode.LiveChannel);
                break;
            default:
                if (mBroadcast.isLive()) {
                    mPlaybackDetailsManager.startChannel(action, channelId, mIsChannelFavorite, RestartMode.Channel);
                } else {
                    mPlaybackDetailsManager.startReplay(action, mBroadcast, false, false, mIsChannelFavorite);
                }
                break;
        }
    }

    @Override
    public void onStopSeeking(StartAction startAction, long seekPosition) {
        // Force a jump to the live position by jumping over the buffer end.
        IBroadcast broadcast = mPlaybackDetailsManager.getBroadcastPlayerData(seekPosition);
        if (broadcast != null && broadcast.isLive() && seekPosition >= getViewHolder().getSeekbar().getSeekAbleEnd()) {
            seekPosition = broadcast.getEnd();
            startAction = StartAction.TSTVEnded;
        }

        super.onStopSeeking(startAction, seekPosition);
    }

    @Override
    protected void onPlaybackChanged() {
        // Is for the active playback. Just update the player UI.
        if (isForActivePlayback()) {
            updateSeekBar(mPlaybackDetailsManager.getExclusiveDetails());

            // Always change the player if the user changes to other playback types. Like replay or vod.
        } else if (!mPlaybackDetailsManager.inPlaybackType(PlaybackType.LiveTvIpTv, PlaybackType.RadioIpTv,
                PlaybackType.Replay)) {
            mNavigator.showPlayerUI();

            // Only change the player UI if it's displayed.
        } else if (mNavigator.isContentOverlayDisplayed()) {
            mNavigator.showPlayerUI();
        }
    }

    @Override
    protected void onJumpToLiveClicked() {
        mPlaybackDetailsManager.jumpToLive(StartAction.JumpLive);
    }

    @Override
    protected boolean isForActivePlayback() {
        PlaybackType playerType = mPlaybackDetailsManager.getPlayerType();
        return playerType == PlaybackType.LiveTvIpTv || playerType == PlaybackType.LiveTvOtt ||
                playerType == PlaybackType.Replay;
    }

    @Override
    public void onPlayerDataUpdate(IBroadcast event) {
        mBroadcast = event;
        super.onPlayerDataUpdate(event);
    }

    @Override
    protected void onUserInactivity() {
        if (mNavigator.getDialogFragment() instanceof PlayerUIButtonHintsDialog) {
            // Do not close the player UI if hints are displayed
            mUserInactivityDetector.reset();
            return;
        }
        super.onUserInactivity();
    }

    private final BroadcastPlayerViewHolder mViewHolder = new BroadcastPlayerViewHolder() {
        @Override
        public View getUnskippableContainer() {
            return mBinding.unskippableAdvertisement.unskippableAdvertisement;
        }

        @Override
        public SeekBarView getSeekbar() {
            return mBinding.seekBarLayout.seekBar;
        }

        @Override
        public View getSeekBarLayout() {
            return mBinding.seekBarLayout.seekBarLayout;
        }

        @Override
        public ViewGroup getDetailContainer() {
            return mBinding.detailsLayout.detailContainer;
        }

        @Override
        public PlayerButtonContainer getButtonContainer() {
            return mBinding.buttonsContainerLayout.buttonContainer;
        }

        @Override
        public TextView getUnskippableMessage() {
            return mBinding.unskippableAdvertisement.unskippableMessage;
        }

        @Override
        public TextView getUnskippableTime() {
            return mBinding.unskippableAdvertisement.unskippableTime;
        }

        @Override
        public FontTextView getPlayedTimeView() {
            return mBinding.seekBarLayout.playedTime;
        }

        @Override
        public FontTextView getLeftTimeView() {
            return mBinding.seekBarLayout.leftTime;
        }

        @Override
        public PlaybackActionView getPlaybackActionView() {
            return mBaseContainerBinding.playbackAction;
        }

        @Override
        public PlayerButtonView getStartOverButton() {
            return mBinding.buttonsContainerLayout.startOver;
        }

        @Override
        public PlayerButtonView getOptionsButton() {
            return mBinding.buttonsContainerLayout.optionsButton;
        }

        @Override
        public PlayerButtonView getMoreInfoButton() {
            return mBinding.buttonsContainerLayout.moreInfoButton;
        }

        @Override
        public FontTextView getTitleView() {
            return mBinding.detailsLayout.programTitle;
        }

        @Override
        public ImageView getLogoView() {
            return mBaseContainerBinding.logo;
        }

        @Override
        public BaseOrderedIconsContainer getBaseOrderedIconsContainer() {
            return mBinding.detailsLayout.iconsContainer;
        }

        @Override
        public PlayerSubtitleDetailView getSubtitleView() {
            return mBinding.detailsLayout.subtitleDetails;
        }

        @Override
        public PlayerButtonView getJumpToLive() {
            return mBinding.buttonsContainerLayout.jumpToLive;
        }

        @Override
        public PlayerButtonView getRecordButton() {
            return mBinding.buttonsContainerLayout.recordButton;
        }

        @Override
        public ContentMarkersView getContentMarkerView() {
            return mBinding.detailsLayout.contentMarker;
        }

        @Override
        public AppCompatImageView getFavouriteIcon() {
            return mBinding.detailsLayout.favoriteIcon;
        }

        @Override
        public BroadcastOrderedIconsContainer getBroadcastOrderedContainer() {
            return mBinding.detailsLayout.iconsContainer;
        }
    };
}
