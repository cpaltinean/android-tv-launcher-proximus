package tv.threess.threeready.ui.generic.resolver;

import androidx.annotation.WorkerThread;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.home.model.module.ModuleCondition;
import tv.threess.threeready.api.home.model.module.ModuleConditionMethod;
import tv.threess.threeready.api.log.Log;

/**
 * Resolves the conditions of stripes and tiles
 * for example if the user is subscribed to a given package or not to show or hide the stripe or tile.
 */
public class ConditionResolver implements Component {
    private static final String TAG = Log.tag(ConditionResolver.class);

    private static final String PARAM_PACKAGE = "package";

    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);
    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);

    @WorkerThread
    public boolean resolveConditions(ModuleCondition[] conditions) {
        if (ArrayUtils.isEmpty(conditions)) {
            return true;
        }
        for (ModuleCondition condition : conditions) {
            ModuleConditionMethod method = condition.getMethod();

            if (method == null) {
                Log.i(TAG, "can not resolve condition: " + condition);
                return false;
            }

            switch (method) {
                case SUBSCRIBED:
                    if (!mAccountRepository.isSubscribed(condition.getFilter(PARAM_PACKAGE))) {
                        return false;
                    }
                    break;

                case NOT_SUBSCRIBED:
                    if (mAccountRepository.isSubscribed(condition.getFilter(PARAM_PACKAGE))) {
                        return false;
                    }
                    break;

                case BACKEND_AVAILABLE:
                    if (!mInternetChecker.isBackendAvailable()) {
                        return false;
                    }
                    break;

                case INTERNET_AVAILABLE:
                    if (!mInternetChecker.isInternetAvailable()) {
                        return false;
                    }
                    break;

                case ONLY_BACKEND_AVAILABLE:
                    if (mInternetChecker.isBackendIsolated()) {
                        return false;
                    }
                    break;
            }
        }
        return true;
    }
}
