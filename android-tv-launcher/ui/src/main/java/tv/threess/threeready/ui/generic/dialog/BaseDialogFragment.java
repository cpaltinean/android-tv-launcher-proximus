/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import io.reactivex.disposables.CompositeDisposable;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.fragment.IBaseFragmentUtils;
import tv.threess.threeready.ui.generic.interfaces.IReportablePage;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.receiver.GlobalKeyReceiver;
import tv.threess.threeready.ui.startup.StartupFlow;

/**
 * Base class for all dialogs in the application.
 * All the key events are dispatched from the activity to the dialog.
 *
 * @author Paul
 * @since 2017.07.04
 */
public abstract class BaseDialogFragment extends DialogFragment implements IReportablePage, IBaseFragmentUtils {
    public static final String TAG = Log.tag(BaseDialogFragment.class);

    protected static final int DEFAULT_PRIORITY = 100;
    public static final int BLOCKING_DIALOG_PRIORITY = 101;
    public static final int OFFLINE_MODE_DIALOG_PRIORITY = 1000;

    protected final CompositeDisposable mDisposable = new CompositeDisposable();
    private DialogListener mDialogListener;
    private boolean mIsDismissed;

    protected final StartupFlow mStartupFlow = Components.get(StartupFlow.class);
    protected final Navigator mNavigator = Components.get(Navigator.class);

    @Override
    @NonNull
    public final Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = createDialog();

        if (!blockKeys()) {
            dialog.setOnKeyListener((dialog1, keyCode, event) -> {
                // Dispatch key event to the listener.
                if (mDialogListener != null) {
                    mDialogListener.onKeyEvent(event, this);
                }

                if (event.getAction() == KeyEvent.ACTION_UP) {
                    return onKeyUp(event);
                }
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    return onKeyDown(event);
                }
                return false;
            });
        }

        return dialog;
    }

    protected Dialog createDialog() {
        return new FullScreenDialog(getContext());
    }

    public void setDialogListener(DialogListener listener) {
        mDialogListener = listener;
    }

    /**
     * if reruns true the user will be able to cancel the dialog by pressing
     */
    public boolean isCancelable() {
        return true;
    }

    /**
     * Can be override, it should return true
     * if the dialog has to block all the key events which are not consumed.
     */
    public boolean blockKeys() {
        return false;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mDialogListener != null) {
            mDialogListener.onStart(this);

        }

        BaseFragment contentFragment = mNavigator.getContentFragment();
        if (contentFragment != null) {
            contentFragment.onDialogDisplayed();
        }

        setCancelable(isCancelable());
        if (blockKeys()) {
            GlobalKeyReceiver.disable(getActivity());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mNavigator.reportNavigationIfNeeded(this);
    }

    @Override
    public void onStop() {
        //enable back our Global key receiver in case if it was disabled at onStart()
        GlobalKeyReceiver.enable(getActivity());

        super.onStop();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        mIsDismissed = true;
        if (mDialogListener != null) {
            mDialogListener.onDismiss(this);
        }
        mDialogListener = null;

        BaseFragment contentFragment = mNavigator.getContentFragment();
        BaseDialogFragment dialogFragment = mNavigator.getDialogFragment();
        if (contentFragment != null && dialogFragment == null) {
            contentFragment.onAllDialogDismissed();
        }

        mDisposable.clear();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        IReportablePage reportablePage = mNavigator.getPreviousReportablePage();
        if (reportablePage != null) {
            mNavigator.reportNavigationIfNeeded(reportablePage);
        }
    }

    /**
     * Returns true if the dialog is already dismissed.
     */
    public boolean isDismissed() {
        return mIsDismissed;
    }

    public void onUserInteraction() {
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        return false;
    }

    public boolean onKeyDown(KeyEvent event) {
        return blockKeys();
    }

    public boolean onKeyUp(KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_DPAD_CENTER:
                UILog.logButtonPressFromRemote(UILog.ButtonType.OK);
                break;
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                UILog.logButtonPressFromRemote(UILog.ButtonType.PLAYPAUSE);
                break;
            case KeyEvent.KEYCODE_MEDIA_STOP:
                UILog.logButtonPressFromRemote(UILog.ButtonType.STOP);
                break;
            case KeyEvent.KEYCODE_MEDIA_RECORD:
                UILog.logButtonPressFromRemote(UILog.ButtonType.RECORD);
                break;
            case KeyEvent.KEYCODE_BACK:
                UILog.logButtonPressFromRemote(UILog.ButtonType.BACK);
                break;
            case KeyEvent.KEYCODE_CAPTIONS:
                UILog.logButtonPressFromRemote(UILog.ButtonType.SUB);
        }
        return ChannelSwitchDialog.handleExternalNumericalInput(getActivity(), mStartupFlow, mNavigator, event);
    }

    public boolean onKeyLongPress(KeyEvent event) {
        return false;
    }

    /**
     * Dismissing dialog in case different  keys are triggered
     *
     * @param keyCode key from remote
     * @return true in case was handled
     */
    public boolean handleKeys(int keyCode) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
            case KeyEvent.KEYCODE_MEDIA_STOP:
                dismiss();
                return true;
        }
        return false;
    }

    @Override
    public void dismiss() {
        if (getFragmentManager() == null) {
            // Ignore. Dialog not added or already removed.
            return;
        }
        super.dismiss();
    }

    @Override
    public void dismissAllowingStateLoss() {
        if (getFragmentManager() == null) {
            // Ignore. Dialog not added or already removed.
            return;
        }

        super.dismissAllowingStateLoss();
    }

    /**
     * Returns default priority of the dialog. It can be override.
     * Only one dialog can be displayed in the application at the same time.
     * The dialog with the highest priority will automatically dismiss the others.
     */
    public int getPriority() {
        return DEFAULT_PRIORITY;
    }

    public interface DialogListener {
        default void onStart(BaseDialogFragment dialog) {
        }

        default void onDismiss(BaseDialogFragment dialog) {
        }

        /**
         * Called when the user presses a key on remote while the dialog is displayed.
         *
         * @param event  The event for the key which was pressed.
         * @param dialog The dialog which received the key event.
         */
        default void onKeyEvent(KeyEvent event, BaseDialogFragment dialog) {
        }
    }

    @Override
    public boolean isClearingFromBackstack() {
        return mIsDismissed;
    }

    public void reportNavigation() {
        Event<UILogEvent, UILogEvent.Detail> event = generatePageOpenEvent();
        if (event == null) {
            return;
        }

        Log.event(event);
    }

    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        return null;
    }

}
