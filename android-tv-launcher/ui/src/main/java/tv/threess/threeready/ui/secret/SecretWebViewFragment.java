package tv.threess.threeready.ui.secret;


import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.generic.fragment.BaseWebViewFragment;

/**
 * WebView fragment that is opened from SecretFragment.
 *
 * @author Andrei Teslovan
 * @since 2018.07.25
 */

public class SecretWebViewFragment extends BaseWebViewFragment {
    public static final String TAG = Log.tag(SecretWebViewFragment.class);

    private static final String EXTRA_URL = "URL";

    private String mUrl;


    public static SecretWebViewFragment newInstance(String url) {
        Bundle args = new Bundle();
        args.putString(EXTRA_URL, url);

        SecretWebViewFragment fragment = new SecretWebViewFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mUrl = getStringArgument(EXTRA_URL);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getWebView().loadUrl(mUrl);
    }
}
