package tv.threess.threeready.ui.settings.model;

/**
 * Entity class representing a vod purchase history item.
 *
 * @author Daniela Toma
 * @since 2022.02.07
 */
public class VodPurchaseHistoryItem {

    private final String mTitle;
    private final String mDescription;

    public VodPurchaseHistoryItem(String title, String description/*, VodPurchaseHistoryType type*/) {
        mTitle = title;
        mDescription = description;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }
}
