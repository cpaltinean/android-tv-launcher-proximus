package tv.threess.threeready.ui.startup.step;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IParentalControlSettings;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.generic.navigation.Navigator;

/**
 * Startup step for launching the parental control fragment.
 *
 * @author Barabas Attila
 * @since 2018.07.02
 */
public class ParentalControlFTISetupStep extends ParentalControlSetupStep {

    private final Navigator mNavigator = Components.get(Navigator.class);

    @Override
    protected void onParentalControlSettingsLoaded(StepCallback callback,
                                                   IParentalControlSettings parentalControlSettings) {
        if (mParentalControlManager.isParentalControlEnabled()) {
            mNavigator.showParentalControl(callback, parentalControlSettings.getParentalRating());
        } else {
            callback.onFinished();
        }
    }
}
