package tv.threess.threeready.ui.generic.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.ui.databinding.LoadingDialogBinding;

/**
 * Use it for showing simple loading animation in the middle. Overlaps all the screen. Used for
 * example at the login parts.
 *
 * @author hunormadaras
 * @since 2019-08-23
 */
public class LoadingDialog extends BaseDialogFragment {

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private LoadingDialogBinding mBinding;

    public static LoadingDialog newInstance(LoadingDialog instance) {
        if (instance == null) {
            instance = new LoadingDialog();
        }
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = LoadingDialogBinding.inflate(inflater, container, false);
        mBinding.getRoot().setBackgroundColor(mLayoutConfig.getWelcomeColor());
        return mBinding.getRoot();
    }

}
