package tv.threess.threeready.ui.generic.hint;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.Config;
import tv.threess.threeready.api.home.model.generic.Hint;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.epg.EpgLayoutType;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.ui.epg.fragment.EpgFragment;
import tv.threess.threeready.ui.generic.dialog.BabyChannelWarningDialog;
import tv.threess.threeready.ui.generic.dialog.BaseDialogFragment;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.player.fragment.PlayerFragment;

/**
 * Handles all hints in the application.
 * Hint Manager can display the following type of hints:
 *
 * Full Screen Hint - hint with custom layout, the dialog should be derived from {@link HintsDialog}
 * This type of hint appears when the content overlay is hidden, the playback is playing
 * in full screen. A full screen hint should be displayed in onContentHide() method of {@link #mOnContentChangeListener}
 * with  {@link #scheduleFullScreenHint(String, BaseDialogFragment, long)} and
 * {@link #scheduleFullScreenHint(String, BaseDialogFragment)} methods.
 *
 * Content Fragment Hint - hint with custom layout, the dialog should be derived from {@link HintsDialog}
 * This type of hint should be displayed using the
 * {@link #scheduleContentFragmentHint(String, BaseDialogFragment, long)} and
 * {@link #scheduleContentFragmentHint(String, BaseDialogFragment)} (String, BaseDialogFragment)} methods
 * in content fragments.
 *
 * Image Based Hint - hint with a background image and previous, next buttons.
 * {@link HintsDialog} is used to display this type of hint.
 * {@link Hint} model comes from the config files (config-nl, config-fr) and can be accessed via {@link #mConfig}
 * This type of hint should be displayed using {@link #showImageBasedHint(String)},
 * {@link #showImageBasedHint(String, BaseDialogFragment.DialogListener)} methods.
 *
 * Hint opened from settings - this type of hint does not save the viewed state into the database.
 * We should use {@link #showHintFromSettings(BaseDialogFragment)} method to display hints from settings.
 * We can modify the hints displayed under Settings -> Tips in
 * {@link tv.threess.threeready.ui.settings.fragment.ApplicationHintsFragment} class.
 *
 * Full screen, content fragment and image based hints save their viewed state into the database unless
 * the {@link #EMPTY_HINT_ID} is used for hint id.
 *
 * @author Zsolt Bokor_
 * @since 2020.06.22
 */
public class HintManager implements Component {

    private static final String TUTORIAL_ID = "startUpTutorial";

    // Ids for hints
    // Use the EMPTY_HINT_ID to avoid saving hint state into the database.
    private static final String EMPTY_HINT_ID = "";
    private static final String CHANNEL_ZAPPER_HINT_ID = "channel_zapper_hints";
    private static final String PLAYER_UI_BUTTON_HINT_ID = "player_ui_button_hints";
    private static final String OPEN_MANAGE_CHANNEL_HINT_ID = "open_manage_channel_hints";
    private static final String MANAGE_CHANNEL_HINT_ID = "manage_channels_hints";
    private static final String MINI_EPG_HINT_ID = "mini_epg_hints";
    private static final String MINI_EPG_NEW_LAYOUT_HINT_ID = "mini_epg_new_layout_hints";
    private static final String TRICK_PLAY_HINT_ID = "trickplay_hints";
    public static final String ADD_TO_WATCHLIST_HINT_ID = "add_to_watchlist_hints";
    public static final String EXPLORE_HINT_ID = "explore_hints";
    private static final String TV_GUIDE_HINT = "tv_guide_hint";

    // Delays for hints
    private static final long CHANNEL_ZAPPER_HINTS_DELAY = TimeUnit.SECONDS.toMillis(4);
    private static final long PLAYER_UI_HINTS_DELAY = TimeUnit.SECONDS.toMillis(3);
    private static final long TRICK_PLAY_HINTS_DELAY = TimeUnit.SECONDS.toMillis(3);
    private static final long OPEN_MANAGE_CHANNEL_HINTS_DELAY = TimeUnit.SECONDS.toMillis(2);
    private static final long MINI_EPG_HINTS_DELAY = TimeUnit.SECONDS.toMillis(0);
    private static final long MINI_EPG_HINTS_NEW_LAYOUT_DELAY = TimeUnit.SECONDS.toMillis(3);
    private static final long TV_GUIDE_HINTS_DELAY = TimeUnit.SECONDS.toMillis(3);

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);
    private final Config mConfig = Components.get(Config.class);

    private final Handler mFullScreenHintHandler = new Handler(Looper.getMainLooper());
    private final Handler mContentFragmentHintHandler = new Handler(Looper.getMainLooper());

    private WeakReference<BaseDialogFragment.DialogListener> mEpgFragmentDialogListener;

    private int mEpgOpenedCounter = 0;
    private final Context mContext;

    private enum HintType {
        FullScreenHint,
        ContentFragmentHint,
        ImageBasedHint
    }

    public HintManager(Context context) {
        mContext = context;
    }

    private final Navigator.IContentChangeListener mOnContentChangeListener = new Navigator.IContentChangeListener() {
        @Override
        public void onBackStackChanged(BaseFragment fragment) {
            removeScheduledFullScreenHint();
            if (fragment instanceof EpgFragment) {
                showOpenManageChannelHints();
            }
        }

        @Override
        public void onContentShown(BaseFragment fragment) {
            removeScheduledFullScreenHint();
            if (fragment instanceof EpgFragment) {
                showOpenManageChannelHints();
            }
        }

        @Override
        public void onContentHide(@Nullable BaseFragment fragment) {
            if (mPlaybackDetailsManager.isLivePlaybackType()) {
                showLivePlayerHints();
            } else if (mPlaybackDetailsManager.inPlaybackType(PlaybackType.Vod)) {
                showTrickPlayHints();
            }
        }
    };

    private final IPlaybackCallback mPlaybackCallback = new IPlaybackCallback() {
        @Override
        public void onCommandSuccess(PlaybackCommand cmd, PlaybackDetails details) {
            if (cmd.getDomain() == PlaybackDomain.LiveTvOtt
                    || cmd.getDomain() == PlaybackDomain.LiveTvDvbC
                    || cmd.getDomain() == PlaybackDomain.LiveTvDvbT
                    || cmd.getDomain() == PlaybackDomain.LiveTvIpTv) {
                showLivePlayerHints();
            } else if (cmd.getDomain() == PlaybackDomain.Vod) {
                showTrickPlayHints();
            }
        }
    };

    /**
     * Initialize Hint Manager.
     * This method should be called after the playback manager is initialized.
     */
    public void init() {
        // Register the content change listener
        mNavigator.addContentChangeListener(mOnContentChangeListener);

        // Register the playback callback
        mPlaybackDetailsManager.registerCallback(mPlaybackCallback);
    }

    /**
     * This method is called in PlayerFragment onDestroyView().
     */
    public void destroy() {
        mNavigator.removeContentChangeListener(mOnContentChangeListener);
        mPlaybackDetailsManager.unregisterCallback(mPlaybackCallback);
    }

    /**
     * Set the epg fragment dialog listener.
     */
    public void setEpgFragmentDialogListener(BaseDialogFragment.DialogListener listener) {
        mEpgFragmentDialogListener = new WeakReference<>(listener);
    }

    /**
     * Shows the internet connection hints.
     */
    public void showInternetConnectionHints() {
        if (!mConfig.getInternetCheckHints().isEmpty()) {
            showHintFromSettings(HintsDialog.newInstance(EMPTY_HINT_ID,
                    mConfig.getInternetCheckHints().get(0), false));
        }
    }

    /**
     * Shows the trickplay hints, sequence of 2 hint screens.
     */
    private void showTrickPlayHints() {
        if (isHintViewed(TRICK_PLAY_HINT_ID)) {
            return;
        }
        TrickPlayHintsDialog dialog = TrickPlayHintsDialog.newInstance(TRICK_PLAY_HINT_ID, false);
        scheduleFullScreenHint(TRICK_PLAY_HINT_ID, dialog, TRICK_PLAY_HINTS_DELAY);
    }

    /**
     * Shows the channel zapper or player UI button hints.
     */
    private void showLivePlayerHints() {
        removeScheduledFullScreenHint();
        if(!isHintViewed(CHANNEL_ZAPPER_HINT_ID)) {
            showChannelZapperHints();
        } else {
            showPlayerUIButtonHints();
        }
    }

    /**
     * Shows a sequence of 2 hint screens for channel zapper.
     */
    private void showChannelZapperHints() {
        if (isPancarteChannelPlaying()) {
            return;
        }
        removeScheduledFullScreenHint();
        mFullScreenHintHandler.postDelayed(() ->
                        showFullScreenHint(ChannelZapperHintsDialog.newInstance(CHANNEL_ZAPPER_HINT_ID,
                                false)),
                CHANNEL_ZAPPER_HINTS_DELAY);
    }

    /**
     * Shows a sequence of 5 hint screens for player UI buttons.
     */
    private void showPlayerUIButtonHints() {
        if (isPancarteChannelPlaying()) {
            return;
        }
        if (isHintViewed(PLAYER_UI_BUTTON_HINT_ID)) {
            return;
        }
        PlayerUIButtonHintsDialog dialog =
                PlayerUIButtonHintsDialog.newInstance(PLAYER_UI_BUTTON_HINT_ID, false);
        dialog.setDialogListener(new BaseDialogFragment.DialogListener() {
            @Override
            public void onStart(BaseDialogFragment dialog) {
                mNavigator.showPlayerUI();
            }
        });
        scheduleFullScreenHint(PLAYER_UI_BUTTON_HINT_ID, dialog, PLAYER_UI_HINTS_DELAY);
    }

    /**
     * Shows the hints to open mini epg from channel zapper.
     */
    public void showOpenManageChannelHints() {
        if (mEpgOpenedCounter < 1) {
            // TODO: Find a better solution to detect the first epg open.
            mEpgOpenedCounter++;
            return;
        }
        if (isHintViewed(OPEN_MANAGE_CHANNEL_HINT_ID)) {
            return;
        }

        OpenManageChannelHintsDialog dialog =
                OpenManageChannelHintsDialog.newInstance(OPEN_MANAGE_CHANNEL_HINT_ID, false);
        dialog.setDialogListener(mEpgFragmentDialogListener.get());
        scheduleContentFragmentHint(OPEN_MANAGE_CHANNEL_HINT_ID, dialog, OPEN_MANAGE_CHANNEL_HINTS_DELAY);
    }

    /**
     * Shows the hints for managing channels in the channel zapper.
     */
    public void showManageChannelHints() {
        if (isHintViewed(MANAGE_CHANNEL_HINT_ID)) {
            return;
        }
        // We need to clear the open manage channel hint first.
        mNavigator.clearAllDialogs();
        ManageChannelHintsDialog dialog =
                ManageChannelHintsDialog.newInstance(MANAGE_CHANNEL_HINT_ID, false);
        dialog.setDialogListener(mEpgFragmentDialogListener.get());
        scheduleContentFragmentHint(MANAGE_CHANNEL_HINT_ID, dialog);
    }

    /**
     * Shows watchlist hints when an item is first added in watchlist.
     */
    public void showAddToWatchlistHints() {
        if (isHintViewed(ADD_TO_WATCHLIST_HINT_ID)) {
            return;
        }
        WatchlistHintDialog dialog =
                WatchlistHintDialog.newInstance(ADD_TO_WATCHLIST_HINT_ID, false);
        dialog.setDialogListener(mEpgFragmentDialogListener.get());
        scheduleContentFragmentHint(ADD_TO_WATCHLIST_HINT_ID, dialog);
    }

    /**
     * Hide the manage channel hints if the add or remove button is clicked.
     */
    public void hideManageChannelHints() {
        BaseDialogFragment dialogFragment = mNavigator.getDialogFragment();
        if (dialogFragment instanceof ManageChannelHintsDialog) {
            mNavigator.clearAllDialogs();
        }
    }

    /**
     * Shows the hint for the chosen epg layout.
     */
    public void showEpgLayoutHints() {
        if(Settings.epgLayoutType.getEnum(EpgLayoutType.MiniEpg) == EpgLayoutType.MiniEpg) {
            if (isHintViewed(MINI_EPG_NEW_LAYOUT_HINT_ID)) {
                return;
            }
            MiniEpgNewLayoutHintsDialog dialog = MiniEpgNewLayoutHintsDialog.newInstance(MINI_EPG_NEW_LAYOUT_HINT_ID, false);
            scheduleContentFragmentHint(MINI_EPG_NEW_LAYOUT_HINT_ID, dialog, MINI_EPG_HINTS_NEW_LAYOUT_DELAY);
        } else {
            if (isHintViewed(TV_GUIDE_HINT)) {
                return;
            }
            TvGuideHintDialog dialog = TvGuideHintDialog.newInstance(TV_GUIDE_HINT, false);
            scheduleContentFragmentHint(TV_GUIDE_HINT, dialog, TV_GUIDE_HINTS_DELAY);
        }
    }

    /**
     * Hide the epg layout hints.
     */
    public void hideEpgLayoutHints() {
        BaseDialogFragment dialogFragment = mNavigator.getDialogFragment();
        if (dialogFragment instanceof MiniEpgNewLayoutHintsDialog
                || dialogFragment instanceof MiniEpgHintsDialog
                || dialogFragment instanceof TvGuideHintDialog) {
            dialogFragment.dismiss();
        }
    }

    /**
     * Shows the hints for mini epg.
     */
    public void showMiniEpgHints(BaseDialogFragment.DialogListener listener) {
        if (isHintViewed(MINI_EPG_HINT_ID)) {
            return;
        }
        MiniEpgHintsDialog dialog = MiniEpgHintsDialog.newInstance(MINI_EPG_HINT_ID, false);
        dialog.setDialogListener(listener);
        scheduleContentFragmentHint(MINI_EPG_HINT_ID, dialog, MINI_EPG_HINTS_DELAY);
    }

    /**
     * Shows the startup hints.
     */
    public void showStartupHints(StepCallback callback) {
        if (callback == null) {
            return;
        }
        if (isHintViewed(TUTORIAL_ID)) {
            callback.onFinished();
        } else {
            List<Hint> hints = mConfig.getTutorialHints();
            if (hints.isEmpty()) {
                callback.onFinished();
                return;
            }

            showImageBasedHint(hints.get(0), new BaseDialogFragment.DialogListener() {
                @Override
                public void onDismiss(BaseDialogFragment dialog) {
                    callback.onFinished();
                }
            });
        }
    }

    /**
     * Removes the callbacks from full screen hint handler and cancels the Rx call.
     */
    private void removeScheduledFullScreenHint() {
        mFullScreenHintHandler.removeCallbacksAndMessages(null);
    }

    /**
     * Removes the callbacks from the content fragment hint handler and cancels the Rx call.
     */
    public void removeScheduledContentFragmentHint() {
        mContentFragmentHintHandler.removeCallbacksAndMessages(null);
    }

    /**
     * Shows the explore hint.
     */
    public void showExploreHint(BaseDialogFragment.DialogListener dialogListener) {
        if (isHintViewed(EXPLORE_HINT_ID)) {
            return;
        }
        mNavigator.clearAllDialogs();
        ExploreHintDialog dialog = ExploreHintDialog.newInstance(EXPLORE_HINT_ID, false);
        dialog.setDialogListener(dialogListener);
        scheduleContentFragmentHint(EXPLORE_HINT_ID, dialog);
    }

    /////////////////////////////
    // Hints Opened From Settings
    /////////////////////////////

    public void showInternetConnectionHintsFromSettings() {
        if (!mConfig.getInternetCheckHints().isEmpty()) {
            showHintFromSettings(HintsDialog.newInstance(EMPTY_HINT_ID,
                    mConfig.getInternetCheckHints().get(0), true));
        }
    }

    public void showChannelZapperHintsFromSettings() {
        showHintFromSettings(ChannelZapperHintsDialog.newInstance(EMPTY_HINT_ID, true));
    }

    public void showPlayerUIButtonHintsFromSettings() {
        showHintFromSettings(PlayerUIButtonHintsDialog.newInstance(EMPTY_HINT_ID, true));
    }

    public void showOpenManageChannelHintsFromSettings() {
        showHintFromSettings(OpenManageChannelHintsDialog.newInstance(EMPTY_HINT_ID, true));
    }

    public void showManageChannelHintsFromSettings() {
        showHintFromSettings(ManageChannelHintsDialog.newInstance(EMPTY_HINT_ID, true));
    }

    public void showMiniEpgHintsFromSettings() {
        showHintFromSettings(MiniEpgHintsDialog.newInstance(EMPTY_HINT_ID, true));
    }

    public void showTrickPlayHintsFromSettings() {
        showHintFromSettings(TrickPlayHintsDialog.newInstance(EMPTY_HINT_ID, true));
    }

    ////////////////////
    // Image Based Hints
    ////////////////////

    /**
     * Returns a hint with the given id from config.
     */
    @Nullable
    private Hint getHintById(String id) {
        if (TextUtils.isEmpty(id)
                || mConfig.getHints() == null) {
            return null;
        }

        for (Hint hint : mConfig.getHints()) {
            if (hint != null && Objects.equals(hint.getId(), id)) {
                return hint;
            }
        }
        return null;
    }

    /**
     * Displays an image based hint with given hintId from config.
     *
     * @param hintId Id of the hint from config.
     */
    private void showImageBasedHint(String hintId) {
        showImageBasedHint(hintId, null);
    }

    /**
     * Displays an image based hint with given hintId from config.
     *
     * @param hintId         Id of the hint from config.
     * @param dialogListener Optional dialog listener to handle onStart, onDismiss... events of the dialog.
     */
    private void showImageBasedHint(String hintId, BaseDialogFragment.DialogListener dialogListener) {
        Hint hint = getHintById(hintId);
        showImageBasedHint(hint, dialogListener);
    }

    private void showImageBasedHint(Hint hint, BaseDialogFragment.DialogListener dialogListener) {
        if (hint == null) {
            // Ignore. Hint is null.
            return;
        }
        if (isHintViewed(hint.getId())) {
            return;
        }
        HintsDialog dialog = HintsDialog.newInstance(hint.getId(), hint, false);
        dialog.setDialogListener(dialogListener);
        showHintByType(hint.getId(), HintType.ImageBasedHint, dialog);
    }

    /////////////////
    // Custom hints
    /////////////////

    /**
     * Displays a full screen hint after delayMillis, if there is no hint displayed.
     *
     * @param dialog      Dialog to be displayed.
     * @param delayMillis Delay in millis.
     */
    private void scheduleFullScreenHint(String hintId, BaseDialogFragment dialog, long delayMillis) {
        removeScheduledFullScreenHint();
        final Runnable showHintRunnable = () -> showHintByType(hintId, HintType.FullScreenHint, dialog);
        if (delayMillis != 0) {
            mFullScreenHintHandler.postDelayed(showHintRunnable, delayMillis);
            return;
        }
        mFullScreenHintHandler.post(showHintRunnable);
    }

    private void scheduleFullScreenHint(String hintId, BaseDialogFragment dialog) {
        scheduleFullScreenHint(hintId, dialog, 0);
    }

    /**
     * Displays a content fragment hint after delayMillis, if there is no hint displayed.
     *
     * @param dialog      Dialog to be displayed.
     * @param delayMillis Delay in millis.
     */
    private void scheduleContentFragmentHint(String hintId, BaseDialogFragment dialog, long delayMillis) {
        removeScheduledContentFragmentHint();
        final Runnable showHintRunnable = () -> showHintByType(hintId, HintType.ContentFragmentHint, dialog);
        if (delayMillis != 0) {
            mContentFragmentHintHandler.postDelayed(showHintRunnable, delayMillis);
            return;
        }
        mContentFragmentHintHandler.post(showHintRunnable);
    }

    private void scheduleContentFragmentHint(String hintId, BaseDialogFragment dialog) {
        scheduleContentFragmentHint(hintId, dialog, 0);
    }

    /**
     * Verifies if a hint is viewed already, if not, displays the dialog for the hint.
     *
     * @param hintType Type of the hint which will be shown.
     * @param dialog   Custom dialog for the hint.
     */
    private void showHintByType(String hintId, HintType hintType, BaseDialogFragment dialog) {
        if (!(dialog instanceof HintsDialog) || hintId == null) {
            return;
        }
        switch (hintType) {
            case FullScreenHint:
                showFullScreenHint(dialog);
                break;
            case ContentFragmentHint:
            case ImageBasedHint:
                showHint(dialog);
                break;
        }
    }

    /**
     * Displays a full screen hint, if there is no hint displayed, the content overlay is hidden,
     * and the zap animator is stopped.
     *
     * @param dialog Dialog to be displayed.
     */
    private void showFullScreenHint(BaseDialogFragment dialog) {
        if (mNavigator.isContentOverlayDisplayed() || isZapAnimatorRunning()) {
            return;
        }
        showHint(dialog);
    }

    /**
     * Displays a hint dialog, if there is no hint displayed.
     */
    private void showHint(BaseDialogFragment dialog) {
        if (isHintOpened() || isBabyChannelWarningDialogOpened()) {
            return;
        }
        mNavigator.clearAllDialogs();
        mNavigator.showDialog(dialog);
    }

    /**
     * Displays a hint dialog without closing other dialogs, if there is no hint displayed.
     */
    private void showHintFromSettings(BaseDialogFragment dialog) {
        if (isHintOpened() || isBabyChannelWarningDialogOpened()) {
            return;
        }
        mNavigator.showDialogOnTop(dialog);
    }

    /**
     * @return True if the dialog on top is a hint dialog, otherwise false.
     */
    private boolean isHintOpened() {
        BaseDialogFragment dialogFragment = mNavigator.getDialogFragment();
        return dialogFragment instanceof HintsDialog && !dialogFragment.isDismissed();
    }

    /**
     * @return True if the baby channel warning dialog is opened, otherwise false.
     */
    private boolean isBabyChannelWarningDialogOpened() {
        BaseDialogFragment dialogFragment = mNavigator.getDialogFragment();
        return dialogFragment instanceof BabyChannelWarningDialog;
    }

    /**
     * @return True if the zap animator is running, otherwise false.
     */
    private boolean isZapAnimatorRunning() {
        PlayerFragment playerFragment = mNavigator.getPlayerFragment();
        return playerFragment != null && playerFragment.getChannelInfoView() != null &&
                playerFragment.getChannelInfoView().isZapAnimatorRunning();
    }

    /**
     * @return True if the playing channel is pancarte, otherwise false.
     */
    private boolean isPancarteChannelPlaying() {
        TvChannel channel = mPlaybackDetailsManager.getChannelData();
        return channel != null && (channel.getType() == ChannelType.REGIONAL || channel.getType() == ChannelType.APP);
    }

    /**
     * Check if hint is already viewed
     */
    private boolean isHintViewed(String hintId) {
        Set<String> viewedHints = Settings.viewedHints.getSet();
        return viewedHints != null && viewedHints.contains(hintId);
    }
}
