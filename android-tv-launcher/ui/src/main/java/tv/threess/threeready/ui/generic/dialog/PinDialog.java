package tv.threess.threeready.ui.generic.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.TimeUnit;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.exception.InvalidPinException;
import tv.threess.threeready.api.generic.exception.PinChangedException;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Fragment of the PIN dialog.
 *
 * @author Eugen Guzyk
 * @since 2018.11.29
 */
public class PinDialog extends BasePinDialog {
    private static final String TAG = Log.tag(PinDialog.class);

    private static final int MAX_ATTEMPTS_NUMBER = 3;
    private static final long BLOCK_TIMEOUT_SECONDS = 30;

    private String mPinChangedTitle;
    private String mPinChangedBody;
    private String mPinChangedHint;

    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);

    private final Runnable mCountdownRunnable = new Runnable() {
        @Override
        public void run() {
            long countdownTime = BLOCK_TIMEOUT_SECONDS - TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - mFailedTimestamp);
            if (countdownTime > 0) {
                mBinding.countdown.setText(String.valueOf(countdownTime));
                mBinding.countdown.postDelayed(this, TimeUnit.SECONDS.toMillis(1));
            } else {
                mFailedTimestamp = 0;
                mPinFailedTimestampSetting.asyncEdit().put(mFailedTimestamp);
                setEnterState();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeState();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        RxUtils.disposeSilently(mPinValidateDisposable);
    }

    @Override
    protected void onValidatePin() {
        if (isAllEntered() && !mIsValidatingPin) {
            Log.d(TAG, "Checking pin from backend");
            showLoading();
            mIsValidatingPin = true;
            RxUtils.disposeSilently(mPinValidateDisposable);
            mPinValidator.validatePin(getEnteredPin())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Boolean>() {

                        @Override
                        public void onSubscribe(Disposable d) {
                            mPinValidateDisposable = d;
                        }

                        @Override
                        public void onSuccess(Boolean isValid) {
                            if (isValid) {
                                onPinValid(getEnteredPin());
                                dismiss();
                            } else {
                                if (mInternetChecker.isBackendAvailable()) {
                                    onFail();
                                } else {
                                    mNavigator.showFailedToSaveSettingsDialog();
                                    dismiss();
                                }
                            }

                            mIsValidatingPin = false;
                        }

                        @Override
                        public void onError(Throwable e) {
                            onFail(e);
                            mIsValidatingPin = false;
                        }
                    });
        }

        focusNextDigit();
    }

    /**
     * Initialize the PIN dialog data and state.
     */
    private void initializeState() {
        mFailedTimestamp = mPinFailedTimestampSetting.get(0L);
        if (TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - mFailedTimestamp) > BLOCK_TIMEOUT_SECONDS) {
            mAttemptsNumber = mPinAttemptsSetting.get(0);
            setEnterState();
        } else {
            setBlockState();
        }
    }

    /**
     * Method to be invoked if PIN code entered correctly.
     */
    protected void onPinValid(String pin) {
        mFailedTimestamp = 0;
        mAttemptsNumber = 0;
        mPinAttemptsSetting.asyncEdit().put(mAttemptsNumber);
        mPinFailedTimestampSetting.asyncEdit().put(mFailedTimestamp);
        if (mOnPinSuccessCallback != null) {
            mOnPinSuccessCallback.onSuccess(pin);
        }
    }

    /**
     * Method to be invoked if PIN code enter failed.
     */
    protected void onFail(Throwable pinChangedError) {
        // show validate cannot be done alert dialog
        // if the pin is not synchronized with the BE in offline mode.
        if (pinChangedError instanceof NullPointerException) {
            showGenericMessage();
            return;
        }

        if (checkAttemptsExceeded()) {
            return;
        }

        if (pinChangedError instanceof IllegalStateException) {
            Log.d(TAG, pinChangedError.toString());
            mNavigator.showFailedToSaveSettingsDialog();
            dismiss();
        } else if (pinChangedError instanceof PinChangedException) {
            setPinChangedState();
        } else if (pinChangedError instanceof InvalidPinException) {
            setReenterState();
        } else {
            showGenericMessage();
        }

        resetDigits();
    }

    private void showGenericMessage() {
        mNavigator.showGenericErrorMessageDialog();
        dismiss();
    }

    /**
     * Method to be invoked if PIN code enter failed.
     */
    private void onFail() {
        if (checkAttemptsExceeded())
            return;

        setReenterState();
        resetDigits();
    }

    /**
     * Checks if the user exceeded the maximum number of attempts.
     */
    private boolean checkAttemptsExceeded() {
        hideLoading();
        mAttemptsNumber++;
        mPinAttemptsSetting.asyncEdit().put(mAttemptsNumber);

        if (mAttemptsNumber >= MAX_ATTEMPTS_NUMBER) {
            mFailedTimestamp = System.currentTimeMillis();
            mAttemptsNumber = 0;
            mPinAttemptsSetting.asyncEdit().put(mAttemptsNumber);
            mPinFailedTimestampSetting.asyncEdit().put(mFailedTimestamp);
            setBlockState();
            resetDigits();
            return true;
        }

        return false;
    }

    /**
     * Set "Enter" state of a PIN dialog.
     */
    @Override
    protected void setEnterState() {
        mBinding.title.setText(mTitle);
        mBinding.body.setText(mBody);
        mBinding.body.setTextColor(mLayoutConfig.getFontColor());
        mBinding.hint.setText(null);
        mBinding.digitsContainer.setVisibility(View.VISIBLE);
        mBinding.countdown.setVisibility(View.GONE);
        mBinding.countdown.setText(null);
        stopCountdownUpdate();
        focusFirstDigit();
    }

    /**
     * Set "Reenter" state of a PIN dialog.
     */
    @Override
    protected void setReenterState() {
        super.setReenterState();
        mBinding.hint.setText(mReenterHint);
        mBinding.digitsContainer.setVisibility(View.VISIBLE);
        mBinding.countdown.setVisibility(View.GONE);
        mBinding.countdown.setText(null);
        stopCountdownUpdate();
        focusFirstDigit();
    }

    /**
     * Set "Block" state of a PIN dialog.
     */
    private void setBlockState() {
        mBinding.title.setText(mBlockedTitle);
        mBinding.body.setText(mBlockedBody);
        mBinding.body.setTextColor(mLayoutConfig.getErrorColor());
        mBinding.hint.setText(mBlockedHint);
        mBinding.digitsContainer.setVisibility(View.INVISIBLE);
        mBinding.countdown.setVisibility(View.VISIBLE);
        startCountdownUpdate();
    }

    /**
     * Sets "Purchase Pin Recently changed" state of a PIN dialog.
     */
    private void setPinChangedState() {
        mBinding.title.setText(mPinChangedTitle);
        mBinding.body.setText(mPinChangedBody);
        mBinding.hint.setText(mPinChangedHint);
        mBinding.digitsContainer.setVisibility(View.VISIBLE);
        mBinding.countdown.setVisibility(View.GONE);
        mBinding.countdown.setText(null);
        stopCountdownUpdate();
        focusFirstDigit();
    }

    /**
     * Start countdown update.
     */
    private void startCountdownUpdate() {
        mBinding.countdown.removeCallbacks(mCountdownRunnable);
        mBinding.countdown.post(mCountdownRunnable);
    }

    /**
     * Stop countdown update.
     */
    private void stopCountdownUpdate() {
        mBinding.countdown.removeCallbacks(mCountdownRunnable);
    }

    /**
     * Builder class to build the PIN dialog.
     */
    public static class Builder {
        PinDialog mPinDialog;

        public Builder() {
            mPinDialog = new PinDialog();
        }

        /**
         * Set callback which will be invoked on successful PIN code entering.
         *
         * @param onPinSuccessCallback to be invoked.
         * @return builder.
         */
        public Builder setOnPinSuccessCallback(OnPinSuccessCallback onPinSuccessCallback) {
            mPinDialog.mOnPinSuccessCallback = onPinSuccessCallback;
            return this;
        }

        /**
         * Set callback which will be invoked when loading has to be shown or hidden.
         *
         * @param onLoadingCallback to be invoked.
         * @return builder.
         */
        public Builder setOnLoadingCallback(OnLoadingCallback onLoadingCallback) {
            mPinDialog.mOnLoadingCallback = onLoadingCallback;
            return this;
        }

        /**
         * Set title to the dialog.
         *
         * @param title to be set.
         * @return builder.
         */
        public Builder setTitle(String title) {
            mPinDialog.mTitle = title;
            return this;
        }

        /**
         * Set reenter title to the dialog.
         *
         * @param reenterTitle to be set.
         * @return builder.
         */
        public Builder setReenterTitle(String reenterTitle) {
            mPinDialog.mReenterTitle = reenterTitle;
            return this;
        }

        /**
         * Set blocked title to the dialog.
         *
         * @param blockedTitle to be set.
         * @return builder.
         */
        public Builder setBlockedTitle(String blockedTitle) {
            mPinDialog.mBlockedTitle = blockedTitle;
            return this;
        }

        /**
         * Sets the title to be used in case the Purchase Pin was recently changed.
         *
         * @param changedPinTitle to be set.
         * @return builder.
         */
        public Builder setPurchasePinChangedTitle(String changedPinTitle) {
            mPinDialog.mPinChangedTitle = changedPinTitle;
            return this;
        }

        /**
         * Set body to the dialog.
         *
         * @param body to be set.
         * @return builder.
         */
        public Builder setBody(String body) {
            mPinDialog.mBody = body;
            return this;
        }

        /**
         * Set reenter body to the dialog.
         *
         * @param reenterBody to be set.
         * @return builder.
         */
        public Builder setReenterBody(String reenterBody) {
            mPinDialog.mReenterBody = reenterBody;
            return this;
        }

        /**
         * Set blocked body to the dialog.
         *
         * @param blockedBody to be set.
         * @return builder.
         */
        public Builder setBlockedBody(String blockedBody) {
            mPinDialog.mBlockedBody = blockedBody;
            return this;
        }

        /**
         * Set the body to be used in case the Purchase Pin was recently changed.
         *
         * @param changedPinBody to be set.
         * @return builder.
         */
        public Builder setPurchasePinChangedBody(String changedPinBody) {
            mPinDialog.mPinChangedBody = changedPinBody;
            return this;
        }

        /**
         * Set reenter hint to the dialog.
         *
         * @param reenterHint to be set.
         * @return builder.
         */
        public Builder setReenterHint(String reenterHint) {
            mPinDialog.mReenterHint = reenterHint;
            return this;
        }

        /**
         * Set blocked hint to the dialog.
         *
         * @param blockedHint to be set.
         * @return builder.
         */
        public Builder setBlockedHint(String blockedHint) {
            mPinDialog.mBlockedHint = blockedHint;
            return this;
        }

        /**
         * Set the hint to be used in case the Purchase Pin was recently changed.
         *
         * @param changedPurchasePinHint to be set.
         * @return builder.
         */
        public Builder setPurchasePinChangedHint(String changedPurchasePinHint) {
            mPinDialog.mPinChangedHint = changedPurchasePinHint;
            return this;
        }

        /**
         * Set PIN attempts setting to the dialog.
         *
         * @param setting to be set.
         * @return builder.
         */
        public Builder setPinAttemptsSetting(Settings setting) {
            mPinDialog.mPinAttemptsSetting = setting;
            return this;
        }

        /**
         * Set PIN failed timestamp setting to the dialog.
         *
         * @param setting to be set.
         * @return builder.
         */
        public Builder setPinFailedTimestampSetting(Settings setting) {
            mPinDialog.mPinFailedTimestampSetting = setting;
            return this;
        }

        /**
         * Set get PIN code observable to the dialog.
         *
         * @param pinValidator to be set.
         * @return builder.
         */
        public Builder setPinValidator(PinValidator pinValidator) {
            mPinDialog.mPinValidator = pinValidator;
            return this;
        }

        public Builder setEvent(Event<UILogEvent, UILogEvent.Detail> event) {
            mPinDialog.mEvent = event;
            return this;
        }

        public PinDialog build() {
            return mPinDialog;
        }
    }

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        return getEvent();
    }
}
