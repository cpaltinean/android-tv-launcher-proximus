/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.home.fragment;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.util.List;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.home.model.generic.ContentConfig;
import tv.threess.threeready.api.home.model.menu.MenuItem;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.HomeBinding;
import tv.threess.threeready.ui.generic.activity.MainActivity;
import tv.threess.threeready.ui.generic.dialog.BaseDialogFragment;
import tv.threess.threeready.ui.generic.fragment.BaseModularPageMenuFragment;
import tv.threess.threeready.ui.generic.hint.HintManager;
import tv.threess.threeready.ui.home.view.ModularPageView;
import tv.threess.threeready.ui.pvr.RecordingActionHelper;

/**
 * Main screen of the application.
 * It displayed the main menu and pages based on a configuration file.
 *
 * Created by Barabas Attila on 4/12/2017.
 */

public class HomeFragment extends BaseModularPageMenuFragment {
    public static final String TAG = Log.tag(HomeFragment.class);

    private static final String EXTRA_SELECT_MENU_ITEM_ID = "EXTRA_SELECT_MENU_ITEM_ID";

    private HomeBinding mBinding;
    private MenuItem mCurrentMenuItem;

    public static HomeFragment newInstance() {
        return newInstance(PageConfig.HOME_MENU_ITEM_ID);
    }

    public static HomeFragment newInstance(String selectedMenuId) {
        Bundle args = new Bundle();
        args.putString(EXTRA_SELECT_MENU_ITEM_ID, selectedMenuId);

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static HomeFragment newInstanceMyVideo() {
        return newInstance(PageConfig.MY_VIDEOS_MENU_ITEM_ID);
    }

    private final ContentConfig mContentConfig = Components.get(ContentConfig.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);
    private final RecordingActionHelper mRecordingActionHelper = Components.get(RecordingActionHelper.class);
    private final HintManager mHintManager = Components.get(HintManager.class);

    private ModularPageView mModularPageView;
    private MainActivity mActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String selectedMenuItemId = getStringArgument(EXTRA_SELECT_MENU_ITEM_ID);
        mCurrentMenuItem = mContentConfig.getMenuItem(selectedMenuItemId == null ?  PageConfig.HOME_MENU_ITEM_ID : selectedMenuItemId);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mBinding == null) {
            mBinding = HomeBinding.inflate(inflater, container, false);

            // Setup main menu
            List<MenuItem> menuItems = mContentConfig.getMainMenuItemList();
            mBinding.mainMenu.setMenuItems(menuItems);
            mBinding.mainMenu.setMenuItemClickListener(mItemClickListener);

            // Use the first as default.
            if (mCurrentMenuItem == null && !menuItems.isEmpty()) {
                mCurrentMenuItem = menuItems.get(0);
            }

            displayPage();
            mBinding.mainMenu.setSelectedItem(mCurrentMenuItem);
        }

        // Display hint for explore.
        MenuItem menuItem = mContentConfig.getMenuItem(PageConfig.EXPLORE_MENU_ITEM_ID);
        if (menuItem != null) {
            mHintManager.showExploreHint(exploreHintListener);
        }

        mUserInactivityDetector.reset(mAppConfig.getHomeScreenHideTimeout());

        if (mCurrentMenuItem != null) {
            mBinding.mainMenu.setSelectedItem(mCurrentMenuItem);
        }

        mBinding.mainMenu.requestFocus();
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    protected FrameLayout getPageContainer() {
        return mBinding.pageContainer;
    }

    @Override
    protected TextView getEmptyPageTextView() {
        return mBinding.emptyContentTextLayout.emptyContentText;
    }

    @Override
    public boolean onKeyDown(KeyEvent event) {

        // Clear backstack and show home.
        if (event.getKeyCode() == KeyEvent.KEYCODE_HOME) {
            if (mNavigator.getActivity().getSupportFragmentManager().getBackStackEntryCount() == 0) {
                mNavigator.showHome();
            } else {
                // Clear the opened dialogs
                mNavigator.clearAllDialogs();
                mNavigator.clearBackStack();

                // Hide content overlay when the home page displayed.
                if (mNavigator.isContentOverlayDisplayed()
                        && PageConfig.HOME_MENU_ITEM_ID.equals(mCurrentMenuItem.getId())
                        && mActivity.hasWindowFocus()) {
                    mNavigator.hideContentOverlay();
                } else {
                    mNavigator.showHome();
                }
            }
            return true;
        }

        return super.onKeyDown(event);
    }

    /**
     * User interaction detector,
     * to prevent the auto hiding of the player UI while the user is navigating.
     */
    @Override
    protected void onUserInactivity() {
        reportPageSelectionByTimeout();
        // Time out is applicable only on Home screen.
        if (mCurrentMenuItem != null && PageConfig.HOME_MENU_ITEM_ID.equals(mCurrentMenuItem.getId())) {
            mNavigator.hideContentOverlayWithFadeOut();
            mNavigator.clearAllDialogs();
        }
    }

    @Nullable
    @Override
    protected Event<?, ?> generatePageOpenEvent() {
        return generatePageNavigationEvent(mBinding.mainMenu);
    }

    @Override
    public void onModuleLoaded(ModuleConfig moduleConfig) {
        hideEmptyPageMessage();

        if (mShouldFocusOnContent) {
            getPageContainer().requestFocus();
            mShouldFocusOnContent = false;
        }
    }

    @Override
    public void onPageEmpty(PageConfig pageConfig) {
        mBinding.mainMenu.requestFocus();
        showEmptyPageMessage(pageConfig);
    }

    @Override
    protected void displayPage(@NonNull MenuItem menuItem) {
        PageConfig config = mContentConfig.getPageConfig(menuItem.getId());
        if (config != null) {
            if (!config.hasSubPages()) {
                mCurrentMenuItem = menuItem;
            }
            displayPage(config);
        }
    }

    protected void displayPage(@NonNull PageConfig pageConfig) {
        // Check and show the recording storage full notification for the user when he enters in the recording section.
        if (PageConfig.MY_VIDEOS_MENU_ITEM_ID.equals(pageConfig.getId())) {
            mRecordingActionHelper.showStorageFullNotification();
        }

        if (pageConfig.hasSubPages()) {
            mNavigator.showDynamicPage(pageConfig, true);
        } else {
            mModularPageView = addModularPageView();
            mModularPageView.showTopFading(true);
            mModularPageView.loadPageConfig(pageConfig, this);
            mModularPageView.setHeaderSpacing(getResources().getDimensionPixelOffset(R.dimen.main_menu_modular_page_top));
        }
    }

    /**
     * Display the page content.
     */
    private void displayPage() {
        if (mCurrentMenuItem != null) {
            PageConfig pageConfig = getPageConfigByCurrentMenuItem();
            if (pageConfig != null) {
                displayPage(pageConfig);
            }
        }
    }

    /**
     * Return pageConfig for selected main menu item.
     */
    private PageConfig getPageConfigByCurrentMenuItem() {
        List<PageConfig> pageConfigs = mContentConfig.getPageConfigs();

        for (PageConfig pageConfig : pageConfigs) {
            if (Objects.equals(pageConfig.getId(), mCurrentMenuItem.getId())) {
                if (!TextUtils.isEmpty(pageConfig.getParent())) {
                    for (int i = 0; i < pageConfigs.size(); i++) {
                        if (pageConfigs.get(i).getId().equals(pageConfig.getParent())) {
                            return pageConfigs.get(i);
                        }
                    }
                }
                return pageConfig;
            }
        }
        return null;
    }

    /**
     * Shows the page of the selected menu item.
     *
     * @param selectedMenuItemId The id of the selected menu item.
     */
    private void showSelectedMenuItemPage(String selectedMenuItemId) {
        MenuItem selectedMenuItem = mContentConfig.getMenuItem(selectedMenuItemId);
        if (selectedMenuItem == null) {
            // No such menu item.
            return;
        }

        displayPage(selectedMenuItem);
        mBinding.mainMenu.setSelectedItem(selectedMenuItem);
    }

    /**
     * Shows My Video page.
     */
    public void showMyVideos() {
        showSelectedMenuItemPage(PageConfig.MY_VIDEOS_MENU_ITEM_ID);
    }

    BaseDialogFragment.DialogListener exploreHintListener = new BaseDialogFragment.DialogListener() {

        @Override
        public void onStart(BaseDialogFragment dialog) {
            Context context = getContext();
            if (context != null) {
                mModularPageView.setForeground(new ColorDrawable(ContextCompat.getColor(context, R.color.explore_hint_overlay_color)));
            }
        }

        @Override
        public void onDismiss(BaseDialogFragment dialog) {

            mModularPageView.setForeground(null);
        }
    };
}
