/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.menu.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.ui.R;

/**
 * Custom view for menu selector line.
 * The background color comes from config, the selector is created programmatically.
 *
 * Created by Szilard on 4/18/2017.
 */

public class MenuSelectorLineView extends AppCompatImageView {

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    private ColorStateList mTint;

    public MenuSelectorLineView(Context context) {
        this(context, null);
    }

    public MenuSelectorLineView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MenuSelectorLineView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    private void initialize() {
        int[][] states = new int[][]{
                new int[]{android.R.attr.state_focused, android.R.attr.state_activated},
                new int[]{android.R.attr.state_activated},
                new int[]{}
        };

        int[] colors = new int[]{
                mLayoutConfig.getMenuSelectorFocused(),
                mLayoutConfig.getMenuSelectorActive(),
                Color.TRANSPARENT
        };

        mTint = new ColorStateList(states, colors);
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        // Update the background color, when the state is changed
        if (mTint != null && mTint.isStateful()) {
            updateTintColor();
        }
    }

    private void updateTintColor() {
        int color = mTint.getColorForState(getDrawableState(), 0);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(getContext().getResources().getDimension(R.dimen.rounded_button_border_width_proximus));
        gradientDrawable.setColor(color);
        setBackground(gradientDrawable);
    }
}
