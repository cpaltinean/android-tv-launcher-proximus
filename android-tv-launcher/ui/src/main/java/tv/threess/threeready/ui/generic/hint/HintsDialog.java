/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.hint;

import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.home.model.generic.ButtonStyle;
import tv.threess.threeready.api.home.model.generic.Hint;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.HintsDialogBinding;
import tv.threess.threeready.ui.generic.dialog.BaseDialogFragment;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Dialog for image based hints.
 *
 * @author Daniel Gliga
 * @since 2017.10.27
 */

public class HintsDialog extends BaseDialogFragment {
    public static final String TAG = Log.tag(HintsDialog.class);

    protected static final String EXTRA_HINT_ID = "EXTRA_HINT_ID";
    private static final String EXTRA_HINT = "EXTRA_HINT";
    protected static final String EXTRA_HINT_OPENED_FROM_SETTINGS = "EXTRA_HINT_OPENED_FROM_SETTINGS";

    private static final String DRAWABLE = "drawable";
    private static final int NO_BACKGROUND_IMAGE_ID = -1;

    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    protected final Translator mTranslator = Components.get(Translator.class);
    protected final AccountRepository mAccountRepository = Components.get(AccountRepository.class);
    protected final ButtonStyle mButtonStyle = Components.get(ButtonStyle.class);
    protected final AppConfig mAppConfig = Components.get(AppConfig.class);

    protected String mHintId;
    private Hint mHint;
    protected boolean mHintOpenedFromSettings;
    private long mHintDisplayedTimestamp;

    protected int mImagePosition = 0;
    protected Disposable mDisposable;

    private HintsDialogBinding mBinding;

    public static HintsDialog newInstance(String hintId, Hint hint, boolean hintOpenedFromSettings) {
        Bundle args = new Bundle();
        args.putString(EXTRA_HINT_ID, hintId);
        args.putSerializable(EXTRA_HINT, hint);
        args.putBoolean(EXTRA_HINT_OPENED_FROM_SETTINGS, hintOpenedFromSettings);
        HintsDialog dialog = new HintsDialog();
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHintId = getStringArgument(EXTRA_HINT_ID);
        mHint = getSerializableArgument(EXTRA_HINT);
        mHintOpenedFromSettings = getBooleanArgument(EXTRA_HINT_OPENED_FROM_SETTINGS);

        if (!TextUtils.isEmpty(mHintId) && !mHintOpenedFromSettings) {
            mDisposable = mAccountRepository.setHintViewed(mHintId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> Log.d(TAG, "Hint id: " + mHintId + " set viewed."),
                            e -> Log.e(TAG, "Set viewed for hint id: " + mHintId + " failed", e)
                    );
        }
    }

    @Override
    protected Dialog createDialog() {
        Dialog dialog = super.createDialog();
        Window dialogWindow = dialog.getWindow();
        if (dialogWindow != null) {
            dialogWindow.setWindowAnimations(R.style.HintDialogAnimation);
            dialogWindow.getDecorView().setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_NO);
        }
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflateView();
        initBranding();
        initViews();
        displayHint();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mHintOpenedFromSettings && getBackgroundImageId() != NO_BACKGROUND_IMAGE_ID) {
            ImageView background = getBackgroundImage();
            background.setVisibility(View.VISIBLE);
            background.setImageDrawable(getDrawable(getBackgroundImageId()));
        }
        mHintDisplayedTimestamp = System.currentTimeMillis();
    }

    protected View inflateView() {
        if (mBinding == null) {
            mBinding = HintsDialogBinding.inflate(LayoutInflater.from(getContext()));
        }

        return mBinding.getRoot();
    }

    @NonNull
    protected ImageView getBackgroundImage() {
        return mBinding.background;
    }

    @NonNull
    protected TextView getPreviousButton() {
        return mBinding.buttonContainer.previousBtn;
    }

    @NonNull
    protected TextView getNextButton() {
        return mBinding.buttonContainer.nextBtn;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        RxUtils.disposeSilently(mDisposable);
    }

    /**
     * @return Resource id for background image to display when the hint was opened from settings -> tips.
     */
    protected int getBackgroundImageId() {
        return NO_BACKGROUND_IMAGE_ID;
    }

    /**
     * Set the proximus specific colors.
     */
    private void initBranding() {
        TextView nextButton = getNextButton();
        TextView previousButton = getPreviousButton();
        previousButton.setBackground(UiUtils.createButtonBackground(getActivity(), mLayoutConfig,
                mButtonStyle));
        previousButton.setTextColor(UiUtils.createButtonBrandingColorStateList(mLayoutConfig));

        nextButton.setBackground(UiUtils.createButtonBackground(getActivity(), mLayoutConfig,
                mButtonStyle));
        nextButton.setTextColor(UiUtils.createButtonBrandingColorStateList(mLayoutConfig));
    }

    /**
     * Initialize the views.
     */
    protected void initViews() {
        TextView nextButton = getNextButton();
        TextView previousButton = getPreviousButton();
        previousButton.setText(mTranslator.get(TranslationKey.HINTS_BUTTON_PREVIOUS));
        updateButtons();
        previousButton.setOnClickListener(v -> {
            onPreviousButtonClicked();
            updateButtons();
        });

        nextButton.setOnClickListener(v -> {
            onNextButtonClicked();
            updateButtons();
        });
    }

    /**
     * Handle previous button click event.
     */
    private void onPreviousButtonClicked() {
        mImagePosition--;
        displayHint();
    }

    /**
     * Handle next button click.
     */
    private void onNextButtonClicked() {
        if (isLastImage()) {
            // Dismiss the dialog
            dismiss();
        } else {
            mImagePosition++;
            displayHint();
        }
    }

    /**
     * Update previous and next buttons according to the selected screen.
     */
    protected void updateButtons() {
        TextView nextButton = getNextButton();
        TextView previousButton = getPreviousButton();
        if (isFirstImage()) {
            previousButton.setVisibility(View.GONE);
            nextButton.setText(mTranslator.get(TranslationKey.HINTS_BUTTON_NEXT));
        } else if (isLastImage()) {
            previousButton.setVisibility(View.VISIBLE);
            nextButton.setText(mTranslator.get(TranslationKey.HINTS_BUTTON_DONE));
        } else {
            previousButton.setVisibility(View.VISIBLE);
            nextButton.setText(mTranslator.get(TranslationKey.HINTS_BUTTON_NEXT));
        }
    }

    /**
     * Display the image for the current position.
     */
    protected void displayHint() {
        if (mHint == null) {
            return;
        }
        String image = mHint.getImage(mImagePosition);
        if (TextUtils.isEmpty(image)) {
            return;
        }

        if (getContext() != null) {
            Resources resources = getContext().getResources();
            int resourceId = resources.getIdentifier(image, DRAWABLE, getContext().getPackageName());
            getBackgroundImage().setImageDrawable(ContextCompat.getDrawable(getContext(), resourceId));
        }
    }

    protected boolean isFirstImage() {
        return mImagePosition == 0;
    }

    protected boolean isLastImage() {
        return (mImagePosition + 1 == mHint.getImages().size());
    }

    /**
     * @return Margin value for the given resource ID.
     */
    protected int getMargin(int resourceId) {
        return (int) getResources().getDimension(resourceId);
    }

    /**
     * @return Drawable for the given resource ID.
     */
    protected Drawable getDrawable(int resourceId) {
        return ContextCompat.getDrawable(mNavigator.getActivity(), resourceId);
    }

    /**
     * Gets the number of screens that will be shown for the Hint.
     */
    protected int getScreenNumbers() {
        return 1;
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        if (handlesNavigationButtons(event)) {
            return super.onKeyUp(event);
        }

        if (blockKeys()) {
            return true;
        }

        dismiss();
        return true;
    }

    protected boolean handlesNavigationButtons(KeyEvent event) {
        return ((mHint != null || getScreenNumbers() > 1)
                && (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER
                || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT
                || event.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT));
    }

    @Override
    public boolean blockKeys() {
        if (mHintOpenedFromSettings) {
            return false;
        }
        return System.currentTimeMillis() - mHintDisplayedTimestamp < mAppConfig.getHintsDisplayTime(TimeUnit.MILLISECONDS);
    }
}
