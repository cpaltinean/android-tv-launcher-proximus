package tv.threess.threeready.ui.generic.player.plugin;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.player.contract.PlaybackType;

/**
 * Playback Plugin used to request the next episode when it's needed,
 * store it and update the playback details when the current episode ends.
 *
 * @author Karetka Mezei Zoltan
 * @since 2020.05.18
 */
public class VodBingeWatchingPlugin extends BaseBingeWatchingPlugin {
    private static final String TAG = Log.tag(VodBingeWatchingPlugin.class);

    private final VodRepository mVodRepository = Components.get(VodRepository.class);

    @Override
    protected PlaybackType getPlaybackType() {
        return PlaybackType.Vod;
    }

    @Override
    protected Disposable preloadNextEpisode() {
        IVodItem vodItem = mPlaybackManager.getVodPlayerData();
        IVodSeries vodSeries = mPlaybackManager.getVodSeriesPlayerData();
        if (vodItem == null) {
            Log.d(TAG, "There is no started vod!");
            return null;
        }

        return mVodRepository.getNextEpisode(vodItem, vodSeries)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bingeWatchingInfo -> {
                            IVodItem nextEpisode = bingeWatchingInfo.getNextEpisode();
                            if (nextEpisode != null && (nextEpisode.isSubscribed() || nextEpisode.isRented())) {
                                mPlaybackManager.setNextEpisode(bingeWatchingInfo);
                                Log.d(TAG, "Next Vod episode is loaded: " + nextEpisode.getTitle() +
                                        " | Season: " + nextEpisode.getSeasonNumber() + " | Episode: " +
                                        nextEpisode.getEpisodeNumber());
                            }
                        },
                        throwable -> Log.e(TAG, "Could not load the next Vod episode.", throwable)
                );
    }
}
