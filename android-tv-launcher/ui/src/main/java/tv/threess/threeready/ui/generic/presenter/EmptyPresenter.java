/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.presenter;

import android.view.View;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;

/**
 * Presenter to display a blank view.
 * Used when a proper presenter cannot be found for the given item.
 *
 * @author Barabas Attila
 * @since 2017.06.09
 */
public class EmptyPresenter extends BasePresenter<Presenter.ViewHolder, Object> {

    private int mWidth;
    private int mHeight;

    public EmptyPresenter() {
        super(null);
    }

    public void setHeight(int height) {
        mHeight = height;
    }

    public void setWidth(int width) {
        mWidth = width;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = new View(parent.getContext());

        view.setFocusable(false);
        view.setLayoutParams(new ViewGroup.LayoutParams(mWidth, mHeight));

        return new ViewHolder(view);
    }

    @Override
    public void onBindHolder(ViewHolder holder, Object o) {}

}
