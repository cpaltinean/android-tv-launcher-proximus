/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.utils;

import android.text.TextUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.home.model.error.ErrorConfig;
import tv.threess.threeready.api.log.helper.ErrorType;
import tv.threess.threeready.api.log.model.Error;

/**
 * Utility class which helps to get proper display mode and message for an error.
 *
 * @author Barabas Attila
 * @since 2017.08.28
 */
public class ErrorHelper implements Component {

    // Constant values to build translation keys for errors.
    private static final String ERROR_KEY_SEPARATOR = "_";
    private static final String ERROR_KEY_PREFIX = "ERR";

    // Message constants.
    private static final String ERROR_MESSAGE_KEY_SUFFIX = "MESSAGE";
    private static final String GENERIC_ERROR_MESSAGE_SUFFIX = "GENERIC_MESSAGE";

    // Header constants
    private static final String ERROR_HEADER_KEY_SUFFIX = "HEADER";
    private static final String GENERIC_ERROR_HEADER_SUFFIX = "GENERIC_HEADER";


    private final Map<String, ErrorConfig> mErrorConfigTypeMap = new HashMap<>();
    private final Translator mTranslator;


    public ErrorHelper(Translator translator, List<ErrorConfig> errorConfigList) {
        mTranslator = translator;

        // Build type config map for errors.
        for (ErrorConfig errorConfig : errorConfigList) {
            mErrorConfigTypeMap.put(errorConfig.getType().toLowerCase(), errorConfig);
        }
    }

    /**
     * Returns a display configuration for the given error.
     * If there is no specific configuration for the give error type,
     * a generic configuration for the domain will be returned.
     */
    public ErrorConfig getErrorConfig(Error error) {
        // Return handler for the error code.
        ErrorConfig specificHandler = mErrorConfigTypeMap.get(error.getErrorCode());
        if (specificHandler != null) {
            return specificHandler;
        }

        // Return handler for the domain as fallback.
        return mErrorConfigTypeMap.get(
                error.getDomain().name().toLowerCase());
    }

    /**
     * Return the generic message for the error.
     * In case a custom message provided
     */
    public String getErrorMessage(Error error) {
        // Return custom message directly when available.
        if (!TextUtils.isEmpty(error.getCustomMessage())) {
            return error.getCustomMessage();
        }

        // Get error message for error code.
        String specificErrorKey = buildSpecificErrorKey(error, KeyType.MESSAGE);
        String specificErrorMessage = mTranslator.get(specificErrorKey, "", LocaleUtils.getApplicationLanguage());
        if (!TextUtils.isEmpty(specificErrorMessage)) {
            return specificErrorMessage;
        }

        // Get generic domain error message.
        String genericErrorKey = buildGenericErrorKey(error.getDomain(), KeyType.MESSAGE);
        return mTranslator.get(genericErrorKey);
    }


    /**
     * Return the generic message for the error.
     * In case a custom message provided
     */
    public String getErrorHeader(Error error) {
        // Get error message for error code.
        String specificErrorKey = buildSpecificErrorKey(error, KeyType.HEADER);
        String specificErrorMessage = mTranslator.get(specificErrorKey, "", LocaleUtils.getApplicationLanguage());
        if (!TextUtils.isEmpty(specificErrorMessage)) {
            return specificErrorMessage;
        }

        // Get generic domain error message.
        String genericErrorKey = buildGenericErrorKey(error.getDomain(), KeyType.HEADER);
        return mTranslator.get(genericErrorKey);
    }

    /**
     * Build translation key for error message based on specific error key.
     */
    private String buildSpecificErrorKey(Error error, KeyType keyType) {
        StringBuilder sb = new StringBuilder();
        sb.append(ERROR_KEY_PREFIX);
        sb.append(ERROR_KEY_SEPARATOR);
        sb.append(error.getDomain().name().toUpperCase());

        String errorCode = error.getErrorCode();
        if (!TextUtils.isEmpty(errorCode)) {
            sb.append(ERROR_KEY_SEPARATOR);
            sb.append(errorCode.toUpperCase());
        }

        sb.append(ERROR_KEY_SEPARATOR);
        sb.append(keyType == KeyType.HEADER ?
                ERROR_HEADER_KEY_SUFFIX : ERROR_MESSAGE_KEY_SUFFIX);
        return sb.toString();
    }

    /**
     * Build domain generic error message and header.
     */
    private String buildGenericErrorKey(ErrorType.Domain domain, KeyType keyType) {
        StringBuilder sb = new StringBuilder();
        sb.append(ERROR_KEY_PREFIX);
        sb.append(ERROR_KEY_SEPARATOR);
        sb.append(domain.name().toUpperCase());
        sb.append(ERROR_KEY_SEPARATOR);

        sb.append(keyType == KeyType.HEADER ?
                GENERIC_ERROR_HEADER_SUFFIX : GENERIC_ERROR_MESSAGE_SUFFIX);
        return sb.toString();
    }


    /**
     * Type of the translation key.
     * Used to generate headers and message translations keys for errors.
     */
    enum KeyType {
        HEADER,
        MESSAGE
    }
}
