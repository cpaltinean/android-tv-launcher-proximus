package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Container for the icons displayed in the app.
 * This container displays the items ordered with the following priority:
 * PARENTAL_RATING, REPLAY, RECORDING_STATUS, AUDIO_LANGUAGES, DOLBY, HD. The order of the enums should be preserved.
 * Also, this container provides the possibility to set a limit for the number of icons to display.
 * In this Base class we have just the common icons.
 *
 * @author DenisaTrif
 * @since 01/07/2019
 */
public class BaseOrderedIconsContainer extends LinearLayout {

    private final int mLimit;
    private final TreeMap<Icons, Drawable> mIconDrawableMap = new TreeMap<>();

    private final int mIconSpacing;
    private ParentalRating mParentalRating;
    private final int mEndSpacing;
    private final int mStartSpacing;
    protected final boolean mRevertColorInFocusState;

    private final Matrix mDrawScaleMatrix = new Matrix();

    public BaseOrderedIconsContainer(Context context) {
        this(context, null, 0);
    }

    public BaseOrderedIconsContainer(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseOrderedIconsContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray attr = context.getTheme().obtainStyledAttributes(attrs, R.styleable.OrderedIconsContainer, 0, 0);
        try {
            mLimit = attr.getInteger(R.styleable.OrderedIconsContainer_iconLimit, Icons.values().length);
            mIconSpacing = attr.getDimensionPixelOffset(R.styleable.OrderedIconsContainer_iconSpacing,
                    context.getResources().getDimensionPixelOffset(R.dimen.default_icon_spacing));
            mStartSpacing = attr.getDimensionPixelOffset(R.styleable.OrderedIconsContainer_startSpacing, 0);
            mEndSpacing = attr.getDimensionPixelOffset(R.styleable.OrderedIconsContainer_endSpacing, 0);
            mRevertColorInFocusState = attr.getBoolean(R.styleable.OrderedIconsContainer_revertColorInFocusState, false);
        } finally {
            attr.recycle();
        }
    }

    /**
     * Displays the parental rating icon.
     */
    public void showParentalRating(ParentalRating parentalRating) {
        if (isIconDisplayed(Icons.PARENTAL_RATING)
                && mParentalRating == parentalRating) {
            return;
        }
        showIcon(Icons.PARENTAL_RATING, getDrawable(UiUtils.getParentalRatingIcon(parentalRating)));
        this.mParentalRating = parentalRating;
    }

    /**
     * Hides parental rating
     */
    public void hideParentalRating() {
        hideIcon(Icons.PARENTAL_RATING);
        mParentalRating = null;
    }

    /**
     * Displays the dolby icon.
     */
    public void showDolby() {
        if (isIconDisplayed(Icons.DOLBY)) {
            return;
        }
        showIcon(Icons.DOLBY, getDrawable(R.drawable.ico_dolby));
    }

    /**
     * Hides the dolby icon
     */
    public void hideDolby() {
        hideIcon(Icons.DOLBY);
    }

    /**
     * Displays the HD icon.
     */
    public void showHDIcon() {
        if (isIconDisplayed(Icons.HD)) {
            return;
        }
        showIcon(Icons.HD, getDrawable(R.drawable.ico_hd));
    }

    /**
     * Hides the HD icon.
     */
    public void hideHDIcon() {
        hideIcon(Icons.HD);
    }

    /**
     * Displays descriptive audio icon.
     */
    public void showDescriptiveAudio() {
        if (isIconDisplayed(Icons.DESCRIPTIVE_AUDIO)) {
            return;
        }
        showIcon(Icons.DESCRIPTIVE_AUDIO, getDrawable(R.drawable.ico_descriptive_audio));
    }

    /**
     * Hides descriptive audio icon.
     */
    public void hideDescriptiveAudio() {
        hideIcon(Icons.DESCRIPTIVE_AUDIO);
    }

    /**
     * Displays descriptive subtitle icon according to the UI language.
     */
    public void showDescriptiveSubtitle() {
        switch (LocaleUtils.getApplicationLanguage()) {
            case LocaleUtils.FRENCH_LANGUAGE_CODE:
                showFrenchDescriptiveSubtitleIcon();
                break;
            default:
                showDutchDescriptiveSubtitleIcon();
                break;
        }
    }

    /**
     * Displays descriptive subtitle icon for French language.
     */
    private void showFrenchDescriptiveSubtitleIcon() {
        if (isIconDisplayed(Icons.DESCRIPTIVE_SUBTITLE)) {
            return;
        }
        showIcon(Icons.DESCRIPTIVE_SUBTITLE, getDrawable(R.drawable.ico_descriptive_subtitle_fr));
    }

    /**
     * Displays descriptive subtitle icon for Dutch language.
     */
    private void showDutchDescriptiveSubtitleIcon() {
        if (isIconDisplayed(Icons.DESCRIPTIVE_SUBTITLE)) {
            return;
        }
        showIcon(Icons.DESCRIPTIVE_SUBTITLE, getDrawable(R.drawable.ico_descriptive_subtitle_nl));
    }

    /**
     * Hides descriptive audio icon.
     */
    public void hideDescriptiveSubtitle() {
        hideIcon(Icons.DESCRIPTIVE_SUBTITLE);
    }

    /**
     * Clears all the views in the container.
     */
    public void hideAll() {
        mIconDrawableMap.clear();
        mParentalRating = null;
        requestLayout();
    }

    /**
     * @return True if the container has any icon displayed.
     */
    public boolean hasContent() {
        return !mIconDrawableMap.keySet().isEmpty();
    }

    @NonNull
    public String getIconsDescription(Translator translator) {
        List<String> iconDescriptions = new ArrayList<>();
        int iconCount = 0;
        for (Icons icon : mIconDrawableMap.keySet()) {
            if (iconCount == mLimit) {
                // Icon limit reached.
                break;
            }
            iconCount++;

            iconDescriptions.add(getIconDescription(translator, icon));
        }

        return String.join(", ", iconDescriptions);
    }

    protected String getIconDescription(Translator translator, Icons icons) {
        switch (icons) {
            case AUDIO_LANGUAGE_EN:
                return new TalkBackMessageBuilder(translator.get(TranslationKey.TALKBACK_ICON_LANGUAGE))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_LANGUAGE_NAME, translator.get(TranslationKey.LANGUAGE_ENGLISH))
                        .toString();
            case AUDIO_LANGUAGE_FR:
                return new TalkBackMessageBuilder(translator.get(TranslationKey.TALKBACK_ICON_LANGUAGE))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_LANGUAGE_NAME, translator.get(TranslationKey.LANGUAGE_FRENCH))
                        .toString();
            case AUDIO_LANGUAGE_NL:
                return new TalkBackMessageBuilder(translator.get(TranslationKey.TALKBACK_ICON_LANGUAGE))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_LANGUAGE_NAME, translator.get(TranslationKey.LANGUAGE_DUTCH))
                        .toString();
            case DESCRIPTIVE_AUDIO:
                return translator.get(TranslationKey.TALKBACK_ICON_AD);
            case DESCRIPTIVE_SUBTITLE:
                if (LocaleUtils.FRENCH_LANGUAGE_CODE.equals(LocaleUtils.getApplicationLanguage())) {
                    return translator.get(TranslationKey.TALKBACK_ICON_ST);
                }
                return translator.get(TranslationKey.TALKBACK_ICON_CC);
            case DOLBY:
                return translator.get(TranslationKey.TALKBACK_ICON_DOLBY);
            case HD:
                return translator.get(TranslationKey.TALKBACK_ICON_HD);
            case REPLAY:
                return translator.get(TranslationKey.TALKBACK_ICON_REPLAY);
            case PARENTAL_RATING:
                if (mParentalRating == null || mParentalRating.getMinimumAge() <= 0) {
                    return "";
                }
                return new TalkBackMessageBuilder(translator.get(TranslationKey.TALKBACK_ICON_AGE))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_AGE, String.valueOf(mParentalRating.getMinimumAge()))
                        .toString();
            default:
                return "";
        }
    }

    /**
     * Show the specified drawable for the icon.
     *
     * @param icon     The icon for which the drawable needs to be displayed.
     * @param drawable The drawable to show for the icon.
     */
    protected void showIcon(Icons icon, Drawable drawable) {
        if (drawable == null) {
            hideIcon(icon);
            return;
        }
        mIconDrawableMap.put(icon, drawable);
        setWillNotDraw(mIconDrawableMap.isEmpty());
        requestLayout();
    }

    /**
     * Remove the specified icon from the container.
     *
     * @param icon The enum of which represent the icon.
     */
    protected void hideIcon(Icons icon) {
        if (!mIconDrawableMap.containsKey(icon)) {
            return;
        }
        mIconDrawableMap.remove(icon);
        setWillNotDraw(mIconDrawableMap.isEmpty());
        requestLayout();
    }

    /**
     * @param drawableId The resource id of the drawable which needs the be loaded.
     * @return The drawable with the give id.
     */
    @Nullable
    protected Drawable getDrawable(@DrawableRes int drawableId) {
        if (drawableId == 0) {
            return null;
        }
        Context context = getContext();
        return ResourcesCompat.getDrawable(
                context.getResources(), drawableId, context.getTheme());
    }

    /**
     * @param icon The enum which represents the icon.
     * @return True if the icon is displayed in the container.
     */
    protected boolean isIconDisplayed(Icons icon) {
        return mIconDrawableMap.containsKey(icon);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Calculate container height.
        int layoutHeight = getLayoutParams().height;
        int desiredHeight = layoutHeight > 0 ? layoutHeight : // Optimize for fix height.
                Math.max(getSuggestedMinimumHeight(),
                        getPaddingTop() + getPaddingBottom() + calculateDesiredHeight());
        int measuredHeight = resolveSizeAndState(desiredHeight, heightMeasureSpec, 0);

        // Calculate container width.
        int endSpacing = mIconDrawableMap.isEmpty() ? 0 : mEndSpacing;
        int startSpacing = mIconDrawableMap.isEmpty() ? 0 : mStartSpacing;
        int desiredWidth = Math.max(getSuggestedMinimumWidth(),
                startSpacing + endSpacing + getPaddingStart() + getPaddingEnd()
                        + calculateDesiredWidth(measuredHeight));
        int measuredWidth = resolveSizeAndState(desiredWidth, widthMeasureSpec, 0);

        setMeasuredDimension(measuredWidth, measuredHeight);
    }

    /**
     * @return The needed height of the container to fit all the icons.
     * Used in case of WRAP_CONTENT layout param is defined for the container.
     */
    private int calculateDesiredHeight() {
        int iconCount = 0;
        int iconsHeight = 0;
        for (Drawable drawable : mIconDrawableMap.values()) {
            if (iconCount == mLimit) {
                // Icon limit reached.
                break;
            }
            iconCount++;

            // Calculate the maximum icon height.
            int dHeight = drawable.getIntrinsicHeight();
            if (iconsHeight < dHeight) {
                iconsHeight = dHeight;
            }
        }
        return iconsHeight;
    }

    /**
     * @param height The height of the container.
     * @return The needed width for the container to fit all the icons.
     * Used in case of WRAP_CONTENT layout param is defined for the container.
     */
    private int calculateDesiredWidth(int height) {
        int iconCount = 0;
        float iconsWidth = 0;
        for (Drawable drawable : mIconDrawableMap.values()) {
            if (iconCount == mLimit) {
                // Icon limit reached.
                break;
            }
            iconCount++;

            // Sum of scaled icon widths.
            int dHeight = drawable.getIntrinsicHeight();
            int dWidth = drawable.getIntrinsicWidth();
            float scale = calculateDrawScale(height, dHeight);
            iconsWidth += dWidth * scale;
        }

        return Math.round(iconsWidth) + ((iconCount - 1) * mIconSpacing);
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();

        boolean changed = false;
        int[] states = getDrawableState();
        for (Drawable drawable : mIconDrawableMap.values()) {
            if (drawable.isStateful() && drawable.setState(states)) {
                changed = true;
            }
        }

        if (changed) {
            invalidate();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int vHeight = getHeight();

        int startSpacing = mStartSpacing + getPaddingStart();
        if (!mIconDrawableMap.isEmpty() && startSpacing != 0) {
            canvas.translate(startSpacing, 0);
        }

        canvas.save();

        int displayedIcons = 0;
        for (Drawable drawable : mIconDrawableMap.values()) {
            // Scale icon if needed.
            canvas.save();
            int dHeight = drawable.getIntrinsicHeight();
            int dWidth = drawable.getIntrinsicWidth();
            float scale = calculateDrawScale(vHeight, dHeight);
            float dy = calculateDrawDy(vHeight, dHeight, scale);
            mDrawScaleMatrix.setScale(scale, scale);
            mDrawScaleMatrix.postTranslate(0, dy);
            canvas.concat(mDrawScaleMatrix);

            // Draw icon.
            drawable.setBounds(0, 0, dWidth, dHeight);
            drawable.draw(canvas);
            displayedIcons++;
            canvas.restore();

            if (displayedIcons == mLimit) {
                // Icon limit reached.
                break;
            }

            // Draw spacing.
            canvas.translate(dWidth * scale + mIconSpacing, 0);
        }

        canvas.restore();
    }

    private float calculateDrawScale(int vHeight, int dHeight) {
        if (dHeight <= vHeight) {
            return 1.0f;
        }
        return (float) vHeight / (float) dHeight;
    }

    private float calculateDrawDy(int vHeight, int dHeight, float scale) {
        return Math.round((vHeight - dHeight * scale) * 0.5f);
    }

    /**
     * An enumeration with all the icons supported by the container.
     * The order of the enum represents the priority of the icon.
     */
    protected enum Icons {
        PARENTAL_RATING,
        REPLAY,
        RECORDING_STATUS,
        DESCRIPTIVE_AUDIO,
        DESCRIPTIVE_SUBTITLE,
        AUDIO_LANGUAGE_EN,
        AUDIO_LANGUAGE_FR,
        AUDIO_LANGUAGE_NL,
        DOLBY,
        HD
    }
}