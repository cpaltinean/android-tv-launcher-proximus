package tv.threess.threeready.ui.menu.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.leanback.widget.HorizontalGridView;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.menu.MenuItem;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.menu.adapter.MenuAdapter;
import tv.threess.threeready.ui.menu.callback.MenuClickListener;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Base view for @{@link MainMenuView} and @{@link SubMenuView}
 *
 * @author hunormadaras
 * @since 30/01/2019
 */
public abstract class BaseMenuView extends RelativeLayout {

    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    protected final Translator mTranslator = Components.get(Translator.class);

    protected MenuClickListener mMenuClickListener;
    protected List<View> mMenuIconViewList = new ArrayList<>();
    protected List<MenuItem> mMenuItemList = new ArrayList<>();
    protected MenuAdapter mMenuAdapter;
    protected MenuItem mSelectedMenuItem;

    protected View mSelectedView;

    public BaseMenuView(Context context) {
        this(context, null);
    }

    public BaseMenuView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseMenuView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @NotNull
    protected abstract ImageView getLogo();

    @NotNull
    protected abstract HorizontalGridView getMenuView();

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        loadLogoIcon();

        // Enable fading edge for menu.
        HorizontalGridView menuView = getMenuView();
        menuView.setFadingRightEdge(true);
        menuView.setFadingRightEdgeLength(
                getContext().getResources().getDimensionPixelOffset(R.dimen.menu_fading_edge_length));
        menuView.setFadingLeftEdge(true);
        menuView.setFadingLeftEdgeLength(
                getContext().getResources().getDimensionPixelOffset(R.dimen.menu_fading_edge_length));

        if (menuView.getLayoutManager() != null) {
            menuView.getLayoutManager().setAutoMeasureEnabled(true);
        }

        mMenuAdapter = new MenuAdapter(mMenuItemList, mMenuItemClickListener);
        menuView.setAdapter(mMenuAdapter);
    }


    @Override
    public void requestChildFocus(View child, View focused) {
        // With the following code, the menu will be focused initially
        boolean viewGotFocus = findFocus() == null;
        // findFocus() is null, when the previously focused view wasn't in this view

        super.requestChildFocus(child, focused);

        if (viewGotFocus) {
            if (mSelectedView != null) {
                // If we have selected view we can request the focus for that.
                // If not, we have to search the selected menu item.
                mSelectedView.requestFocus();
                return;
            }

            int position = mMenuItemList.indexOf(mSelectedMenuItem);

            HorizontalGridView menuView = getMenuView();
            RecyclerView.ViewHolder holder = menuView
                    .findViewHolderForAdapterPosition(position);

            if (holder != null) {
                mSelectedView = holder.itemView;
                mSelectedView.requestFocus();
            } else {
                menuView.requestFocus();
            }
        }
    }

    protected void loadLogoIcon() {
        UiUtils.loadImage(mTranslator, mLayoutConfig.getLogo(), getLogo());
    }

    protected MenuAdapter.MenuItemClickListener mMenuItemClickListener = new MenuAdapter.MenuItemClickListener() {
        @Override
        public void onMenuItemClick(MenuItem menuItem) {
            setSelectedItem(menuItem);
            if (mMenuClickListener != null) {
                mMenuClickListener.onMenuItemClick(menuItem);
            }
        }
    };

    @SuppressLint("NotifyDataSetChanged")
    private void updateMenuItemByPosition(MenuItem menuItem, boolean isSelected) {
        if (menuItem == null) {
            return;
        }

        int position = mMenuItemList.indexOf(menuItem);
        if (position != -1) {
            // Update the selected position.
            if (isSelected) {
                HorizontalGridView menuView = getMenuView();
                mMenuAdapter.setSelectedPos(position);
                menuView.setSelectedPosition(position);

                // Save the selected view.
                menuView.post(() -> {
                    RecyclerView.ViewHolder holder = menuView
                            .findViewHolderForAdapterPosition(position);

                    if (holder != null) {
                        mSelectedView = holder.itemView;
                    }
                });
            }

            // Redraw the menu item.
            mMenuAdapter.notifyDataSetChanged();
        }
    }

    protected MenuItem getSelectedMenu(String menuId) {
        if (mMenuItemList != null && !mMenuItemList.isEmpty()) {
            for (int position = 0; position < mMenuItemList.size(); position++) {
                if (mMenuItemList.get(position)
                        .getId().equalsIgnoreCase(menuId)) {
                    return mMenuItemList.get(position);
                }
            }
        }
        return null;
    }

    public void selectLastSelectedMenuItem() {
        updateMenuItemByPosition(mSelectedMenuItem, true);
    }

    public void setSelectedItem(MenuItem menuItem) {
        updateMenuItemByPosition(menuItem, true);
        updateMenuItemByPosition(mSelectedMenuItem, false);
        mSelectedMenuItem = menuItem;
    }

    public void setMenuItemClickListener(MenuClickListener menuClickListener) {
        mMenuClickListener = menuClickListener;
        if (mMenuAdapter != null) {
            mMenuAdapter.setMenuItemClickListener(mMenuItemClickListener);
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    protected void resetOldMenu() {
        mSelectedView = null;
        getMenuView().setSelectedPosition(0);
        mMenuAdapter.setSelectedPos(0);
        mMenuAdapter.updateMenuItems(Collections.emptyList());
        mMenuAdapter.notifyDataSetChanged();
    }

    public UILog.Page getFocusSubPointName() {
        return null;
    }
}
