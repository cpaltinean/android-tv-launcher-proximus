/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.home.presenter.module.stripe.generic;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.MultipleRowStripeModuleBinding;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.generic.utils.DrawableUtils;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.home.presenter.module.BaseStripeModulePresenter;
import tv.threess.threeready.ui.home.view.MultipleRowStripeView;

/**
 * Presenter class for a multiple row stripe module.
 * Responsible to load the data from the given data source and display it in a horizontal gird.
 *
 * Created by Szilard on 5/5/2017.
 */

public abstract class MultipleRowStripeModulePresenter extends BaseStripeModulePresenter<MultipleRowStripeModulePresenter.ViewHolder> {

    protected final Translator mTranslator = Components.get(Translator.class);

    public MultipleRowStripeModulePresenter(Context context, DataSourceLoader dataSourceLoader) {
        super(context, dataSourceLoader);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   RecyclerView.RecycledViewPool pool) {
        MultipleRowStripeModuleBinding binding = MultipleRowStripeModuleBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.multipleRowStripe.setRecyclerViewPool(pool);

        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, ModuleData<?> data) {
        holder.mBinding.multipleRowStripe.setModuleData(data);
        holder.mBinding.multipleRowStripe.setPresenterSelector(getCardPresenterSelector(data.getModuleConfig()));

        updateAdapter(holder, data);
        setIconsForStripe(holder, data.getModuleConfig());
    }


    /**
     * Create a new or update the existing adapter for the module data.
     * @param holder The view holder of the collection.
     * @param moduleData The module data which needs to be displayed
     */
    private void updateAdapter(MultipleRowStripeModulePresenter.ViewHolder holder, ModuleData<?> moduleData) {
        if (holder.mBinding.multipleRowStripe.getAdapterWrapper()!= null) {
            MultipleRowStripeView.MultiRowAdapterWrapper adapterWrapper =
                    holder.mBinding.multipleRowStripe.getAdapterWrapper();
            adapterWrapper.setItems(moduleData.getItems());
            return;
        }

        holder.mBinding.multipleRowStripe.setPresenterSelector(getCardPresenterSelector(moduleData.getModuleConfig()));
        MultipleRowStripeView.MultiRowAdapterWrapper adapterWrapper = new MultipleRowStripeView.MultiRowAdapterWrapper();
        adapterWrapper.setItems(moduleData.getItems());
        holder.mBinding.multipleRowStripe.setAdapterWrapper(adapterWrapper);
    }



    private void setIconsForStripe(ViewHolder holder, ModuleConfig moduleConfig) {
        if (!TextUtils.isEmpty(moduleConfig.getIcon())) {
            Drawable icon = DrawableUtils.getDrawable(mContext, moduleConfig.getIcon());
            if (icon != null) {
                icon.setBounds(0,
                        (int) mContext.getResources().getDimension(R.dimen.stripe_icon_margin_top),
                        (int) mContext.getResources().getDimension(R.dimen.stripe_icon_width),
                        (int) mContext.getResources().getDimension(R.dimen.stripe_icon_height));

                holder.mBinding.multipleRowStripe.mBinding.moduleTitle.setCompoundDrawablePadding((int) mContext.getResources().getDimension(R.dimen.stripe_icon_margin_right));
                holder.mBinding.multipleRowStripe.mBinding.moduleTitle.setCompoundDrawables(icon, null, null, null);
            } else {
                holder.mBinding.multipleRowStripe.mBinding.moduleTitle.setCompoundDrawablePadding(0);
            }
        } else {
            holder.mBinding.multipleRowStripe.mBinding.moduleTitle.setCompoundDrawables(null, null, null, null);
        }
    }

    protected abstract PresenterSelector getCardPresenterSelector(ModuleConfig moduleConfig);

    protected static class ViewHolder extends Presenter.ViewHolder {

        ViewGroup mParentView;
        public final MultipleRowStripeModuleBinding mBinding;

        public ViewHolder(MultipleRowStripeModuleBinding binding) {
            super(binding.getRoot());
            mParentView = (ViewGroup) view;
            mBinding = binding;
        }
    }
}
