/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.search.activity;

/**
 * Define the show stripe in global search.
 *
 * @author Barabas Attila
 * @since 2017.07.12
 */
public class ProgramGlobalSearchHandlerActivity extends BaseGlobalSearchHandlerActivity {
}
