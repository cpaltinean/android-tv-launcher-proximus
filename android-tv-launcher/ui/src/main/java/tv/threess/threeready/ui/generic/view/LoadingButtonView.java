package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Custom view for loading button layout.
 *
 * Created by Noemi Dali on 24.06.2020.
 */
public class LoadingButtonView extends FontTextView {

    private final Translator mTranslator = Components.get(Translator.class);

    private final Runnable mAnimation = new Runnable() {
        int count = 0;

        @Override
        public void run() {

            if (count < 3) {
                append(".");
            }

            if (count == 3) {
                count = 0;
                setText(mTranslator.get(TranslationKey.MODULE_ACTION_BUTTON_LOADING));
            } else {
                ++count;
            }

            mHandler.postDelayed(this, LOADING_ANIMATION_DURATION);
        }
    };

    private static final Handler mHandler = new Handler(Looper.getMainLooper());
    private static final long LOADING_ANIMATION_DURATION = TimeUnit.SECONDS.toMillis(1);

    public LoadingButtonView(Context context) {
        this(context, null);
    }

    public LoadingButtonView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LoadingButtonView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        mHandler.post(mAnimation);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        mHandler.removeCallbacks(mAnimation);
    }
}
