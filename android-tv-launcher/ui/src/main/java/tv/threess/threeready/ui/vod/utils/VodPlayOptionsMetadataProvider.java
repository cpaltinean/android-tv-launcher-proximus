package tv.threess.threeready.ui.vod.utils;

import static tv.threess.threeready.api.generic.helper.TimeUtils.DIVIDER;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Provider class used for provide the common Vod related details for PlayOptions Dialog.
 *
 * @author David
 * @since 25.01.2022
 */
public class VodPlayOptionsMetadataProvider {
    public static String getTitle(Translator translator, IVodVariant vodVariant) {
        return vodVariant.getEpisodeTitleWithSeasonEpisode(translator, " ");
    }
    
    public static String getAudioInfo(Translator translator, String audioProfile) {
        String language = LocaleUtils.getLanguageFromIso(audioProfile);
        return !TextUtils.isEmpty(language) ? translator.get(TranslationKey.DETAIL_AUDIO_LANGUAGE) + " " + language : "";
    }

    public static String getSubtitleInfo(Translator translator, IVodVariant variant) {
        List<String> deepCopyOfSubtitleLanguages = new ArrayList<>(variant.getSubtitleLanguages());

        if (variant.getSubtitleLanguages() != null && !deepCopyOfSubtitleLanguages.isEmpty()) {

            deepCopyOfSubtitleLanguages.replaceAll(LocaleUtils::getLanguageFromIso);
            String subtitles = TextUtils.join(", ", deepCopyOfSubtitleLanguages);

            if (TextUtils.isEmpty(subtitles)) {
                // Sometimes even if subtitleInfo.length > 0 the String "subtitles" is empty ...
                // TODO check video subtitles on CC.
                return translator.get(TranslationKey.DETAIL_SUBTITLES)
                        + " " + translator.get(TranslationKey.SUBTITLES_NONE);
            }

            return translator.get(TranslationKey.DETAIL_SUBTITLES) + " " + subtitles;
        }

        return translator.get(TranslationKey.DETAIL_SUBTITLES)
                + " " + translator.get(TranslationKey.SUBTITLES_NONE);
    }

    public static String getInfoRow(Translator translator, LocaleSettings localeSettings, IVodVariant variant) {
        StringBuilder stringBuilder = new StringBuilder();

        String duration = LocaleTimeUtils.getDuration(variant.getDuration(), translator);
        if (!TextUtils.isEmpty(duration)) {
            stringBuilder.append(duration);
            stringBuilder.append(DIVIDER);
        }

        String availabilityInfo = TimeUtils.formatDate(localeSettings, variant.getLicensePeriodEnd());
        if (!TextUtils.isEmpty(availabilityInfo)) {
            stringBuilder.append(translator.get(TranslationKey.DETAIL_VOD_AVAILABILITY))
                    .append(" ")
                    .append(availabilityInfo);
        } else if (stringBuilder.length() > 0){
            stringBuilder.delete(stringBuilder.length() - TimeUtils.DIVIDER.length(), stringBuilder.length());
        }

        return stringBuilder.toString();
    }
}