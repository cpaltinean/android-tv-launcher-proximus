package tv.threess.threeready.ui.startup.step;

import androidx.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.model.IParentalControlSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Startup step to initializeCache the parental control.
 *
 * @author Barabas Attila
 * @since 2019.07.14
 */
public class ParentalControlSetupStep implements Step {
    private static final String TAG = Log.tag(ParentalControlSetupStep.class);

    protected final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);
    protected final AppConfig mAppConfig = Components.get(AppConfig.class);

    private Disposable mDisposable;

    @Override
    public void execute(StepCallback callback) {
        mParentalControlManager.initialize()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(mAppConfig.getQueryTimeout(), TimeUnit.SECONDS)
                .subscribe(new SingleObserver<IParentalControlSettings>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mDisposable = d;
                    }

                    @Override
                    public void onSuccess(@NonNull IParentalControlSettings settings) {
                        onParentalControlSettingsLoaded(callback, settings);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "Couldn't initialize parental control.", e);
                        callback.onFinished();
                    }
                });
    }

    /**
     * Called after the parental control settings loaded.
     * Should be used to initialize the parental control here.
     * @param callback - Callback for the startup step execution.
     * @param parentalControlSettings - The current parental control settings
     */
    protected void onParentalControlSettingsLoaded(StepCallback callback,
                                                   IParentalControlSettings parentalControlSettings) {
        callback.onFinished();
    }

    @Override
    public void cancel() {
        RxUtils.disposeSilently(mDisposable);
    }
}
