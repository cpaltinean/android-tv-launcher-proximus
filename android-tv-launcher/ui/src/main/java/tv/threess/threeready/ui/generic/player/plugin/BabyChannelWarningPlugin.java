/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.player.plugin;

import android.os.Handler;
import android.os.Looper;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.command.ForceStopCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.plugin.PlaybackPlugin;
import tv.threess.threeready.player.result.Result;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Playback manager plugin to display a warning message every time when a baby tv channel is tuned.
 * The message is shown 2 seconds after tuning (to not distract the zapping experience)
 *
 * @author Barabas Attila
 * @since 2018.10.01
 */
public class BabyChannelWarningPlugin implements PlaybackPlugin {
    private static final String TAG = Log.tag(BabyChannelWarningPlugin.class);

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final PlaybackDetailsManager mPlaybackManager = Components.get(PlaybackDetailsManager.class);
    private final TvRepository mTvRepository = Components.get(TvRepository.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    private Set<String> mBabyChannelIds = new HashSet<>();

    private final Handler mHandler = new Handler(Looper.getMainLooper());

    private Disposable mDisposable;

    private final Runnable mShowWarningRunnable = () -> {
        Log.d(TAG, "mShowWarningRunnable() called");
        if (mNavigator.isStreamDisplayedOnTop() && !mNavigator.isTrickPlayerDisplayedOnTop()) {
            Log.d(TAG, "showing baby channel warning dialog");
            mNavigator.showBabyChannelWarning();
            return;
        }
        scheduleBabyChannelWarning();
    };

    @Override
    public void onCreate() {
        Log.d(TAG, "Create baby channel plugin.");

        // Get baby channel ids.
        mDisposable = mTvRepository.getBabyChannels()
                .subscribeOn(Schedulers.io())
                .map(channels -> {
                    Log.d(TAG, "Baby channel list loaded. " + channels.size());
                    Set<String> babyChannelIds = new HashSet<>();
                    for (TvChannel channel : channels) {
                        babyChannelIds.add(channel.getId());
                    }
                    return babyChannelIds;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(channelIds -> {
                    mBabyChannelIds = channelIds;
                    if (checkDisplayConditions()) {
                        scheduleBabyChannelWarning();
                    }
                });
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy() called");
        RxUtils.disposeSilently(mDisposable);
        mNavigator.hideBabyChannelDialog();
        mHandler.removeCallbacksAndMessages(null);
    }


    @Override
    public void onOfferCommand(PlaybackCommand command) {
        Log.d(TAG, "onOfferCommand() called with: command = [" + command + "]");
        if (shouldProcessCommand(command)) {
            mHandler.removeCallbacksAndMessages(null);
            if (command instanceof ForceStopCommand) {
                mNavigator.hideBabyChannelDialog();
            }
        }
    }

    /**
     * In case of StartCommand we have to show the BabyChannelWarningDialog if the current channel
     * is a BabyChannel. Otherwise if the command is ForceStopCommand, then we have to remove
     * Handler Callbacks.
     *
     * @param command The {@link PlaybackCommand} being dispatched.
     * @param result  The {@link Result} obtained after executing command.
     */
    @Override
    public void onResult(PlaybackCommand command, Result result) {
        Log.d(TAG, "onResult() called with: command = [" + command + "], result = [" + result + "]");

        if (shouldProcessCommand(command)) {
            Log.d(TAG, "command is StartCommand and can be processed.");
            if (result.isFavorable() && checkDisplayConditions()) {
                scheduleBabyChannelWarning();
            }
        }
    }

    /**
     * Schedule a warning in case a baby channel is playing.
     */
    private void scheduleBabyChannelWarning() {
        Log.d(TAG, "Schedule baby channel warning display.");
        mHandler.removeCallbacksAndMessages(null);
        mHandler.postDelayed(mShowWarningRunnable, mAppConfig.getBabyChannelWarningDelay(TimeUnit.MILLISECONDS));
    }

    /**
     * Check if it a Baby channel and fi it was already displayed for this channel.
     */
    private boolean checkDisplayConditions() {
        boolean isBabyChannel = mBabyChannelIds.contains(mPlaybackManager.getChannelId());
        Log.d(TAG, "checkDisplayConditions() called, isBabyChannel: " + isBabyChannel);
        return isBabyChannel;
    }

    /**
     * Check if the received command is a StartCommand and not a TrickPlayStartCommand.
     */
    private boolean shouldProcessCommand(PlaybackCommand command) {
        boolean returnValue = (command instanceof StartCommand && command.getDomain() != PlaybackDomain.TrickPlay);
        Log.d(TAG, "checkCommandConditions() called, result = " + returnValue);
        return returnValue;
    }
}
