/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.tv.presenter.nownext;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.Presenter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.LiveBookmark;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.player.RestartMode;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.TvNowNextA1CardBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.generic.presenter.BaseModularCardPresenter;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.generic.view.BroadcastOrderedIconsContainer;
import tv.threess.threeready.ui.pvr.RecordingActionHelper;
import tv.threess.threeready.ui.utils.ImageTransform;

/**
 * Card presenter class for tv_now_next type and A1 variant card.
 * <p>
 * Created by Szilard on 5/10/2017.
 */

public class TvNowNextA1CardPresenter extends BaseModularCardPresenter<TvNowNextA1CardPresenter.ViewHolder, Pair<IBroadcast, IBroadcast>> {
    private static final String TAG = Log.tag(TvNowNextA1CardPresenter.class);

    private static final int MAX_RECYCLER_VIEW_COUNT = 10;

    private final DateTimeFormatter mTimeFormat = DateTimeFormatter.ofPattern("HH:mm", Locale.getDefault());

    private final Translator mTranslator = Components.get(Translator.class);
    protected final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);
    private final RecordingActionHelper mRecordingActionHelper = Components.get(RecordingActionHelper.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);

    public TvNowNextA1CardPresenter(Context context) {
        super(context);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        TvNowNextA1CardBinding binding = TvNowNextA1CardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.background.setBackgroundColor(mLayoutConfig.getPlaceHolderColor());

        holder.mBinding.nextStartTime.setTextColor(mLayoutConfig.getPlaceholderTransparentFontColor());
        holder.mBinding.nextTitle.setTextColor(mLayoutConfig.getPlaceholderTransparentFontColor());
        holder.mBinding.startTime.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBinding.title.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mPlaceHolderDrawable.setColor(mLayoutConfig.getPlaceHolderColor());
        return holder;
    }

    @Override
    public int getMaxRecyclerViewCount() {
        return MAX_RECYCLER_VIEW_COUNT;
    }

    @Override
    public void onBindHolder(ModuleData<?> moduleData, ViewHolder holder, Pair<IBroadcast, IBroadcast> nowNextBroadcast) {
        super.onBindHolder(moduleData, holder, nowNextBroadcast);

        updateCoverImage(holder, nowNextBroadcast);
        updateChannelLogo(holder, nowNextBroadcast);
        updateTile(holder, nowNextBroadcast);

        TvChannel channel = mNowOnTvCache.getChannel(nowNextBroadcast.first.getChannelId());
        if (nowNextBroadcast.first.canReplay(channel)) {
            holder.mBinding.iconContainer.showReplay();
        } else {
            holder.mBinding.iconContainer.hideReplay();
        }

        holder.mBinding.progressBar.setProgressData(new LiveBookmark(nowNextBroadcast.first));

        invalidateRecordingStatus(holder, nowNextBroadcast);
        updateContentDescription(holder, nowNextBroadcast);
    }

    @Override
    public void onUnbindHolder(ModuleData<?> moduleData, ViewHolder holder) {
        super.onUnbindHolder(moduleData, holder);

        holder.mBinding.iconContainer.hideAll();
        RxUtils.disposeSilently(holder.mRecStatusDisposable);

        Glide.with(mContext).clear(holder.mBinding.cover);
        Glide.with(mContext).clear(holder.mBinding.smallChannelLogo);
        Glide.with(mContext).clear(holder.mBinding.largeChannelLogo);
    }

    @Override
    public void onFocusedState(ModuleData<?> moduleData, ViewHolder holder, Pair<IBroadcast, IBroadcast> nowNextBroadcast) {
        super.onFocusedState(moduleData, holder, nowNextBroadcast);
        if (!TextUtils.isEmpty(nowNextBroadcast.first.getTitle())) {
            // Display indicator icons.
            holder.mBinding.iconContainer.setVisibility(View.VISIBLE);
        }

        // Move the logo to center
        if (holder.mBinding.largeChannelLogo.isShown()) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.mBinding.largeChannelLogo.getLayoutParams();
            params.topMargin = mContext.getResources().getDimensionPixelOffset(R.dimen.focused_now_next_large_channel_logo_margin);
            holder.mBinding.largeChannelLogo.setLayoutParams(params);
        }

        holder.mBinding.title.setSelected(true);
        holder.mBinding.nextTitle.setSelected(true);

        invalidateDolbyLogo(holder.mBinding.iconContainer, nowNextBroadcast.first, true);
    }

    @Override
    public void onDefaultState(ModuleData<?> moduleData, ViewHolder holder, Pair<IBroadcast, IBroadcast> nowNextBroadcast) {
        super.onDefaultState(moduleData, holder, nowNextBroadcast);

        // Hide indicator icons.
        holder.mBinding.iconContainer.setVisibility(View.GONE);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.mBinding.title.getLayoutParams();
        params.setMarginEnd(mContext.getResources().getDimensionPixelOffset(R.dimen.tv_now_next_margin));
        holder.mBinding.title.setLayoutParams(params);

        if (holder.mBinding.largeChannelLogo.isShown()) {
            params = (RelativeLayout.LayoutParams) holder.mBinding.largeChannelLogo.getLayoutParams();
            params.topMargin = mContext.getResources().getDimensionPixelOffset(R.dimen.now_next_large_channel_logo_margin);
            holder.mBinding.largeChannelLogo.setLayoutParams(params);
        }

        holder.mBinding.title.setSelected(false);
        holder.mBinding.nextTitle.setSelected(false);

        invalidateDolbyLogo(holder.mBinding.iconContainer, nowNextBroadcast.first, false);
    }

    protected void updateChannelLogo(ViewHolder holder, Pair<IBroadcast, IBroadcast> nowNextBroadcast) {
        TvChannel channel = mNowOnTvCache.getChannel(nowNextBroadcast.first.getChannelId());
        Glide.with(mContext).clear(holder.mBinding.smallChannelLogo);
        Glide.with(mContext)
                .load(channel)
                .apply(ImageTransform.REQUEST_OPTIONS)
                .transition(new DrawableTransitionOptions().dontTransition())
                .dontAnimate()
                .into(new DrawableImageViewTarget(holder.mBinding.smallChannelLogo) {

                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        super.onResourceReady(resource, transition);
                        holder.mBinding.largeChannelLogo.setVisibility(View.GONE);
                        holder.mBinding.logoGradient.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        view.setVisibility(View.GONE);
                        holder.mBinding.largeChannelLogo.setVisibility(View.GONE);
                    }
                });
    }

    protected void updateCoverImage(ViewHolder holder, Pair<IBroadcast, IBroadcast> nowNextBroadcast) {
        // Load program cover
        Glide.with(mContext).clear(holder.mBinding.cover);
        Glide.with(mContext)
                .load(mParentalControlManager.isRestricted(nowNextBroadcast.first)
                        ? R.drawable.locked_cover_landscape : nowNextBroadcast.first)
                .placeholder(holder.mPlaceHolderDrawable)
                .format(DecodeFormat.PREFER_RGB_565)
                .override(getCardWidth(nowNextBroadcast), getCardHeight(nowNextBroadcast))
                .optionalCenterInside()
                .into(holder.mBinding.cover);
    }

    protected void updateTile(ViewHolder holder, Pair<IBroadcast, IBroadcast> nowNextBroadcast) {
        if (!TextUtils.isEmpty(nowNextBroadcast.first.getTitle())) {
            holder.mBinding.startTime.setText(TimeUtils.getTimeFormat(mTimeFormat, nowNextBroadcast.first.getStart()));
            holder.mBinding.title.setText(buildTitle(nowNextBroadcast.first));
        } else {
            holder.mBinding.startTime.setVisibility(View.GONE);
            holder.mBinding.title.setPadding(mContext.getResources().getDimensionPixelOffset(R.dimen.now_next_program_not_available_text_padding), 0, 0, 0);
            holder.mBinding.title.setText(mTranslator.get(TranslationKey.MODULE_STRIPE_NOW_NEXT_NA));
        }

        if (nowNextBroadcast.second != null && !TextUtils.isEmpty(nowNextBroadcast.second.getTitle())) {
            holder.mBinding.nextStartTime.setVisibility(View.VISIBLE);
            holder.mBinding.nextStartTime.setText(TimeUtils.getTimeFormat(mTimeFormat, nowNextBroadcast.second.getStart()));
            holder.mBinding.nextTitle.setText(buildTitle(nowNextBroadcast.second));
        } else {
            holder.mBinding.nextStartTime.setVisibility(View.GONE);
            holder.mBinding.nextTitle.setPadding(mContext.getResources().getDimensionPixelOffset(R.dimen.now_next_program_not_available_text_padding), 0, 0, 0);
            holder.mBinding.nextTitle.setText(mTranslator.get(TranslationKey.MODULE_STRIPE_NOW_NEXT_NA));
        }
    }

    private void updateContentDescription(ViewHolder holder, Pair<IBroadcast, IBroadcast> nowNextBroadcast) {
        if (!mMwRepository.isTalkBackOn()) {
            return;
        }

        TvChannel channel = mNowOnTvCache.getChannel(nowNextBroadcast.first.getChannelId());
        holder.view.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_TV_COLLECTION))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_CHANNEL_NAME, channel != null ? channel.getName() : "")
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_ASSET_NOW, nowNextBroadcast.first.getTitle())
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_START_TIME, LocaleTimeUtils.getStartDate(nowNextBroadcast.first, mTranslator, mLocaleSettings))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_ASSET_NEXT, nowNextBroadcast.second != null ? nowNextBroadcast.second.getTitle() : mTranslator.get(TranslationKey.MODULE_STRIPE_NOW_NEXT_NA))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BROADCAST_NEXT_START_TIME, nowNextBroadcast.second != null ? LocaleTimeUtils.getStartDate(nowNextBroadcast.second, mTranslator, mLocaleSettings) : mTranslator.get(TranslationKey.MODULE_STRIPE_NOW_NEXT_NA))
                        .toString()
        );
    }

    private String buildTitle(IBroadcast broadcast) {
        return mParentalControlManager.isRestricted(broadcast)
                ? mTranslator.get(TranslationKey.CARDS_TITLE_CONTENT_LOCKED)
                : broadcast.getEpisodeTitleWithSeasonEpisode(mTranslator, " ");
    }

    private void invalidateDolbyLogo(BroadcastOrderedIconsContainer iconsContainer, IBroadcast tvBroadcast, boolean isFocused) {
        if (isFocused && mPlaybackDetailsManager.isLiveChannelPlaying(tvBroadcast.getChannelId())
                && mPlaybackDetailsManager.isDolbyAvailable()) {
            iconsContainer.showDolby();
        } else {
            iconsContainer.hideHDIcon();
        }
    }

    private void invalidateRecordingStatus(ViewHolder holder, Pair<IBroadcast, IBroadcast> nowNext) {
        RxUtils.disposeSilently(holder.mRecStatusDisposable);
        holder.mRecStatusDisposable = mPvrRepository.getRecordingStatus(nowNext.first)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(holder.mBinding.iconContainer::showRecordingStatus
                        , throwable -> {
                            tv.threess.threeready.api.log.Log.e(TAG, "Couldn't get the recording status.", throwable);
                            holder.mBinding.iconContainer.hideRecordingStatus();
                        });
    }

    @Override
    public void onPlaybackChanged(ViewHolder holder, Pair<IBroadcast, IBroadcast> tvBroadcastTvBroadcastPair) {
        super.onPlaybackChanged(holder, tvBroadcastTvBroadcastPair);

        invalidateDolbyLogo(holder.mBinding.iconContainer, tvBroadcastTvBroadcastPair.first, holder.view.isFocused());
    }

    @Override
    public void onClicked(ModuleData<?> moduleData, ViewHolder holder, Pair<IBroadcast, IBroadcast> nowNextBroadcast) {
        super.onClicked(moduleData, holder, nowNextBroadcast);

        if (mParentalControlManager.isRestricted(nowNextBroadcast.first)) {
            // Restricted content. Needs to unblock parental control.
            mNavigator.showParentalControlUnblockDialog();
            return;
        }

        mPlaybackDetailsManager.startChannel(StartAction.Ui, nowNextBroadcast.first.getChannelId(), true, RestartMode.LiveChannel);
        mNavigator.showPlayerUI();
    }

    @Override
    protected void onParentalRatingChanged(ViewHolder holder, Pair<IBroadcast, IBroadcast> nowNextBroadcast) {
        super.onParentalRatingChanged(holder, nowNextBroadcast);

        updateTile(holder, nowNextBroadcast);
        updateCoverImage(holder, nowNextBroadcast);
    }

    @Override
    public boolean onKeyEvent(ModuleData<?> moduleData, ViewHolder holder,
                              Pair<IBroadcast, IBroadcast> nowNext, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP && event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_RECORD) {
            mRecordingActionHelper.handleRecKeyAction(nowNext.first,
                    holder.mBinding.iconContainer.getRecordingStatus());
            return true;
        }

        return super.onKeyEvent(moduleData, holder, nowNext, event);
    }

    public int getCardWidth(ModuleData<?> moduleData, Pair<IBroadcast, IBroadcast> nowNext) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_now_next_a1_card_focused_width);
    }

    @Override
    public int getCardHeight(ModuleData<?> moduleData, Pair<IBroadcast, IBroadcast> nowNext) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_now_next_a1_card_focused_height);
    }

    @Override
    public long getStableId(Pair<IBroadcast, IBroadcast> nowNext) {
        if (nowNext.first == null || nowNext.second == null) {
            return ArrayObjectAdapter.NO_ID;
        }

        return Objects.hash(nowNext.first.getId(), nowNext.second.getId());
    }

    static class ViewHolder extends BaseCardPresenter.ViewHolder {

        private final TvNowNextA1CardBinding mBinding;
        ColorDrawable mPlaceHolderDrawable = new ColorDrawable();

        private Disposable mRecStatusDisposable;

        public ViewHolder(TvNowNextA1CardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

    }
}
