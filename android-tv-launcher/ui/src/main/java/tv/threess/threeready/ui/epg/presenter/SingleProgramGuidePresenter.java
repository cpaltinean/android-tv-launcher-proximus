/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.epg.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.leanback.widget.Presenter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.SingleProgramGuideCardBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Abstract card presenter class for channel types that don't have programs in Mini-PEG.
 * The channels that don't have programs are: {@link AppProgramGuidePresenter} and @{@link RegionalProgramGuidePresenter}
 *
 * @author Barabas Attila
 * @since 2017.07.26
 */
public abstract class SingleProgramGuidePresenter extends BaseCardPresenter<SingleProgramGuidePresenter.ViewHolder, TvChannel> {

    protected final Translator mTranslator = Components.get(Translator.class);
    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    protected final Navigator mNavigator = Components.get(Navigator.class);

    public SingleProgramGuidePresenter(Context context) {
        super(context);
    }

    abstract void setTitle(ViewHolder holder, TvChannel channel);

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        SingleProgramGuideCardBinding binding = SingleProgramGuideCardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.title.setBackgroundColor(mLayoutConfig.getPlaceHolderColor());
        holder.mBinding.title.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBinding.channelNumber.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBinding.background.setBackgroundColor(mLayoutConfig.getPlaceHolderColor());
        return holder;
    }

    @Override
    public void onFocusedState(ViewHolder holder, TvChannel channel) {
        super.onFocusedState(holder, channel);
        updateFavoriteIcon(holder, channel, true);
    }

    @Override
    public void onDefaultState(ViewHolder holder, TvChannel channel) {
        super.onDefaultState(holder, channel);
        updateFavoriteIcon(holder, channel, false);
    }

    @Override
    public void onBindHolder(ViewHolder holder, TvChannel channel) {
        super.onBindHolder(holder, channel);

        // Set the default values
        RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) holder.view.getLayoutParams();
        params.width = mContext.getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_width_focused);
        params.height = mContext.getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_height_focused);
        holder.view.setTranslationX(0);
        holder.view.setTranslationY(0);
        holder.view.setLayoutParams(params);

        String channelNumber = channel != null ? String.valueOf(channel.getNumber()) : "";
        holder.mBinding.channelNumber.setText(channelNumber);

        boolean isFavorite = channel != null && channel.isFavorite();
        holder.mBinding.favoriteIcon.setVisibility(isFavorite ? View.VISIBLE : View.GONE);
        holder.mBinding.logo.setVisibility(View.VISIBLE);

        Glide.with(mContext).clear(holder.mBinding.logo);
        Glide.with(mContext)
                .load(channel)
                .override(
                        mContext.getResources().getDimensionPixelOffset(R.dimen.zapper_netflix_logo_max_width),
                        mContext.getResources().getDimensionPixelOffset(R.dimen.zapper_netflix_logo_max_height))
                .centerInside()
                .into(holder.mBinding.logo);

        setTitle(holder, channel);

    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);

        if (holder.view != null && holder.view.isSelected()) {
            holder.forceFocusedAnimatorState();
        } else {
            holder.forceDefaultAnimatorState();
        }

        holder.mBinding.logo.setVisibility(View.VISIBLE);
        holder.mBinding.favoriteIcon.setVisibility(View.GONE);

        Glide.with(mContext).clear(holder.mBinding.logo);
    }

    @Override
    public int getCardWidth(TvChannel tvChannel) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_width_focused);
    }

    @Override
    public int getCardHeight(TvChannel tvChannel) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_height_focused);
    }

    @Override
    public long getStableId(TvChannel tvChannel) {
        return Objects.hash(tvChannel.getId(), tvChannel.getType());
    }

    private void updateFavoriteIcon(final ViewHolder holder, TvChannel channel, boolean isFocused) {
        holder.mBinding.favoriteIcon.setVisibility(View.GONE);
        holder.mBinding.channelNumber.setVisibility(View.GONE);

        if (isFocused) {
            if (channel.isFavorite()) {
                holder.mBinding.favoriteIcon.setVisibility(View.VISIBLE);
            } else {
                holder.mBinding.channelNumber.setVisibility(View.VISIBLE);
            }
        }
    }

    public static class ViewHolder extends BaseCardPresenter.ViewHolder {
        final SingleProgramGuideCardBinding mBinding;

        public ViewHolder(SingleProgramGuideCardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        public ImageView getLogoView() {
            return mBinding.logo;
        }

        public void forceFocusedAnimatorState() {
            view.getLayoutParams().width = view.getResources()
                    .getDimensionPixelOffset(R.dimen.tv_program_guide_card_width_focused);
            view.getLayoutParams().height = view.getResources()
                    .getDimensionPixelOffset(R.dimen.tv_program_guide_card_height_focused);

            view.setTranslationX(view.getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_translation_x_focused));
            view.setElevation(view.getResources().getDimensionPixelOffset(R.dimen.card_focused_elevation));

            view.requestLayout();
        }

        void forceDefaultAnimatorState() {
            // Set the default values
            view.getStateListAnimator().jumpToCurrentState();
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) view.getLayoutParams();
            params.width = view.getContext().getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_width_focused);
            params.height = view.getContext().getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_height_focused);
            view.setTranslationX(0);
            view.setTranslationY(0);
            view.requestLayout();
        }
    }
}
