package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextClock;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.local.FontConfig;
import tv.threess.threeready.api.home.model.local.FontType;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.FontStylist;

/**
 * Clock view which allows the user to define a font style to use as the view's typeface.
 *
 * @author Barabas Attila
 * @since 2018.08.09
 */
public class FontClockView extends TextClock {

    private final FontStylist mFontStylist = Components.getOrNull(FontStylist.class);

    FontConfig mFontConfig;

    public FontClockView(Context context) {
        this(context, null);
    }

    public FontClockView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FontClockView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public FontClockView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        // Read font type from attributes.
        FontType font = FontType.REGULAR; // default
        if (attrs != null) {
            TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.FontTextView, defStyleRes, 0);
            try {
                int fwAttr = typedArray.getInteger(R.styleable.FontTextView_fontType, font.attrValue);
                font = FontType.forAttributeValue(fwAttr);
            } finally {
                typedArray.recycle();
            }
        }

        setFontType(font);

        // Override the sizes based on font type
        setTextSize(TypedValue.COMPLEX_UNIT_PX, super.getTextSize());
        setLineSpacing(super.getLineSpacingExtra(), super.getLineSpacingMultiplier());
        setLetterSpacing(super.getLetterSpacing());
    }


    public void setFontType(@NonNull final FontType fontStyle) {
        if (isInEditMode() || mFontStylist == null) {
            return;
        }

        mFontConfig = mFontStylist.getFontConfig(fontStyle);
        if (mFontConfig == null) {
            // Not custom font available.
            return;
        }

        // Get font id by name.
        int fontId = getContext().getResources()
                .getIdentifier(mFontConfig.getName(), "font", getContext().getPackageName());

        if (fontId > 0) {
            setTypeface(ResourcesCompat.getFont(getContext(), fontId));
        }
    }

    @Override
    public boolean isImportantForAccessibility() {
        return false;
    }

    @Override
    public void setLineSpacing(float add, float mult) {
        float lineSpacingExtraFactor = mFontConfig == null
                ? 1f : mFontConfig.getLineSpacingExtraFactor();
        super.setLineSpacing(add * lineSpacingExtraFactor, mult);
    }

    @Override
    public void setLetterSpacing(float letterSpacing) {
        float letterSpacingExtraFactor = mFontConfig == null
                ? 1f : mFontConfig.getLetterSpacingFactor();
        super.setLetterSpacing(letterSpacing* letterSpacingExtraFactor);
    }

    @Override
    public void setTextSize(int unit, float size) {
        float textSizeFactor = mFontConfig == null
                ? 1f : mFontConfig.getTextSizeFactor();
        super.setTextSize(unit, size * textSizeFactor);
    }
}
