package tv.threess.threeready.ui.startup.fragment;

import android.animation.Animator;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.model.account.IConsent;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.ui.databinding.MoreInfoFragmentBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.AnimatorUtils;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Implementation of the UI design for the FTI More Info screen regarding the privacy settings
 */
public class ConsentMoreInfoFragment extends StartFragment {
    private static final String TAG = Log.tag(ConsentMoreInfoFragment.class);

    private static final String EXTRA_CONSENT = "extra_consent";

    private MoreInfoFragmentBinding mBinding;

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final VodRepository mVodRepository = Components.get(VodRepository.class);

    private Animator mFadeOutAnimator;
    private IConsent mConsent;
    private IVodItem mTutorialVideo;
    private Disposable mDisposable;

    public static ConsentMoreInfoFragment newInstance(IConsent consent, StepCallback callback) {
        ConsentMoreInfoFragment fragment = new ConsentMoreInfoFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CONSENT, consent);
        fragment.setArguments(args);
        fragment.setStepCallback(callback);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mConsent = getSerializableArgument(EXTRA_CONSENT);
        fetchTutorialVideoDetails();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mBinding = MoreInfoFragmentBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateLayout();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AnimatorUtils.cancelAnimators(mFadeOutAnimator);
        RxUtils.disposeSilently(mDisposable);
    }

    @Override
    public void updateLayout() {
        if (mConsent == null) {
            Log.d(TAG, "Could not update layout. Consent is null.");
            return;
        }

        mBinding.watchButton.setBackground(UiUtils.createFtiButtonBackground(getContext(), mLayoutConfig));
        mBinding.closeButton.setBackground(UiUtils.createFtiButtonBackground(getContext(), mLayoutConfig));

        mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        mBinding.body.setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), mLayoutConfig.ALPHA_PLACEHOLDER_FONT));
        mBinding.watchButton.setTextColor(UiUtils.createButtonBrandingColorStateList(mLayoutConfig));
        mBinding.closeButton.setTextColor(UiUtils.createButtonBrandingColorStateList(mLayoutConfig));

        mFadeOutAnimator = getFadeOutAnimation();
        mBinding.scrollView.setVerticalScrollBarEnabled(false);

        if (!TextUtils.isEmpty(mConsent.getTutorialVideoUrl())) {
            mBinding.watchButton.setVisibility(View.VISIBLE);
            mBinding.watchButton.setOnClickListener(e -> {
                // Start the tutorial video
                if (mTutorialVideo != null) {
                    mNavigator.showGdprPlayer(StartAction.Ui, mTutorialVideo, mTutorialVideo.getCheapestPrice(), mTutorialVideo.getMainVariant());
                }
            });
        }

        mBinding.closeButton.setOnClickListener(button -> {
            mNavigator.popStartFragment();
        });

        mBinding.scrollView.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                v.setVerticalScrollBarEnabled(true);
            }
            v.setScrollbarFadingEnabled(!hasFocus);
        });

        mBinding.scrollView.setScrollListener((scrollPosition) -> {
            switch (scrollPosition) {
                case START:
                case MIDDLE:
                    mBinding.scrollIndicator.setVisibility(View.VISIBLE);
                    break;
                case END:
                    mBinding.scrollIndicator.setVisibility(View.INVISIBLE);
                    break;
                default:
                    break;
            }
        });

        updateScrollViewArrow();
        mBinding.scrollView.addOnLayoutChangeListener(
                (v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> updateScrollViewArrow());

        mBinding.title.setText(mConsent.getTitle());
        mBinding.body.setText(Html.fromHtml(mConsent.getToolTip(), Html.FROM_HTML_MODE_COMPACT));
        mBinding.watchButton.setText(mTranslator.get(TranslationKey.CONSENT_WATCH_VIDEO));
        mBinding.closeButton.setText(mTranslator.get(TranslationKey.CLOSE_BUTTON));

        if (mBinding.watchButton.getVisibility() == View.VISIBLE) {
            mBinding.watchButton.requestFocus();
        } else {
            mBinding.closeButton.requestFocus();
        }
    }

    /**
     * Retrieves the vod item for the tutorial video.
     */
    private void fetchTutorialVideoDetails() {
        if (mConsent == null || TextUtils.isEmpty(mConsent.getTutorialVideoUrl())) {
            return;
        }

        String tutorialVideoIdForCurrentLanguage = mConsent.getTutorialVideoUrl();

        if (tutorialVideoIdForCurrentLanguage == null) {
            return;
        }

        mVodRepository.getVod(tutorialVideoIdForCurrentLanguage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<IVodItem>() {
                    @Override
                    public void onSubscribe(Disposable disposable) {
                        mDisposable = disposable;
                    }

                    @Override
                    public void onNext(IVodItem vodItem) {
                        mTutorialVideo = vodItem;
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "Could not retrieve tutorial video with id: " + tutorialVideoIdForCurrentLanguage, e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void updateScrollViewArrow() {
        if (mBinding.scrollView.isScrollable()) {
            mBinding.scrollIndicator.setVisibility(View.VISIBLE);
            mBinding.scrollView.setFocusable(true);
        } else {
            mBinding.scrollIndicator.setVisibility(View.INVISIBLE);
            mBinding.scrollView.setFocusable(false);
        }
    }
}
