/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.tv.presenter.vod;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.Observable;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.generic.ContentMarkers;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.presenter.ContentCardPresenter;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.VodOrderedIconsContainer;

/**
 * Abstract base presenter of vod type and A1 variant card.
 * <p>
 * Created by Szilard on 6/16/2017.
 */

abstract class VodCardPresenter<THolder extends VodCardPresenter.ViewHolder, TItem extends IBaseVodItem>
        extends ContentCardPresenter<THolder, TItem> {

    VodCardPresenter(Context context) {
        super(context);
    }

    @Override
    public void onBindHolder(ModuleData<?> moduleData, THolder holder, TItem vod) {
        super.onBindHolder(moduleData, holder, vod);
        updateIndicators(holder, vod);
        updateContentDescription(moduleData, holder, vod);
    }

    @Override
    protected void openDetailPage(THolder holder, TItem vod) {
        mNavigator.showVodMovieDetails(vod);
    }

    @Override
    protected void updateInfoRowText(THolder holder, TItem vod) {
        List<String> infoParts = new ArrayList<>();
        String releaseYear = vod.getReleaseYear();
        if (!TextUtils.isEmpty(releaseYear)) {
            infoParts.add(releaseYear);
        }

        String genres = TextUtils.join(", ", vod.getGenres().stream().limit(1).toArray());
        if (!TextUtils.isEmpty(genres)) {
            infoParts.add(genres);
        }

        String duration = LocaleTimeUtils.getDuration(vod.getDuration(), mTranslator);
        if (!TextUtils.isEmpty(duration)) {
            infoParts.add(duration);
        }

        holder.getInfoTextView().setVisibility(View.VISIBLE);
        holder.getInfoTextView().setText(TextUtils.join(TimeUtils.DIVIDER, infoParts));
    }

    protected void updateIndicators(THolder holder, IBaseVodItem vod) {
        holder.getIconContainer().showAudioProfiles(vod.getMainVariant().getAudioLanguages());
        updateParentalRatingIcon(holder, vod);
        updateDescriptiveAudioIcon(holder, vod);
        updateDescriptiveSubtitleIcon(holder, vod);
        updateHdIcon(holder, vod);
    }

    protected void updateParentalRatingIcon(THolder holder, IBaseVodItem vod) {
        if (holder.view.isFocused()) {
            holder.getIconContainer().showParentalRating(vod.getParentalRating());
        } else {
            holder.getIconContainer().hideParentalRating();
        }
    }

    protected void updateDescriptiveAudioIcon(THolder holder, IBaseVodItem vod) {
        if (!vod.getMainVariant().hasDescriptiveAudio()) {
            return;
        }

        if (holder.view.isFocused()) {
            holder.getIconContainer().showDescriptiveAudio();
        } else {
            holder.getIconContainer().hideDescriptiveAudio();
        }
    }

    protected void updateDescriptiveSubtitleIcon(THolder holder, IBaseVodItem vod) {
        if (!vod.getMainVariant().hasDescriptiveSubtitle()) {
            return;
        }

        if (holder.view.isFocused()) {
            holder.getIconContainer().showDescriptiveSubtitle();
        } else {
            holder.getIconContainer().hideDescriptiveSubtitle();
        }
    }

    protected void updateHdIcon(THolder holder, IBaseVodItem vod) {
        if (vod.isHD()) {
            holder.getIconContainer().showHDIcon();
        } else {
            holder.getIconContainer().hideHDIcon();
        }
    }

    protected void updateContentDescription(ModuleData<?> moduleData, THolder holder, TItem vod) {
        if (!mMwRepository.isTalkBackOn()) {
            return;
        }

        if (moduleData == null || ArrayUtils.isEmpty(moduleData.getItems())) {
            return;
        }

        TalkBackMessageBuilder message = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_CARDS_VOD_OTHER_TILE));
        if ((Integer) holder.view.getTag(R.integer.normalized_adapter_position) == 0) {
            message = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_CARDS_VOD_FIRST_TILE))
                    .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_SWIMLANE, mTranslator.get(moduleData.getTitle()));
        }

        holder.view.setContentDescription(message
                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_ASSET, vod.getTitle())
                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_CONTENT_MARKER, holder.getContentMarkerView().getText())
                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_ICONS, holder.getIconContainer().getIconsDescription(mTranslator))
                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_VOD_RELEASE_YEAR, vod.getReleaseYear())
                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_GENRE, vod.getGenres().toString())
                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_DURATION, LocaleTimeUtils.getDuration(vod.getDuration(), mTranslator))
                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_SYNOPSYS, vod.getDescription())
                .toString()
        );
    }

    @Override
    public Observable<IBookmark> getBookmarkObservable(THolder holder, TItem vod) {
        return mVodRepository.getBookmarkForVod(vod);
    }

    @Override
    protected void updateContentMarkers(THolder holder, TItem vod) {
        holder.getContentMarkerView().showMarkers(vod, ContentMarkers.TypeFilter.Cards);
    }

    @Override
    public void onUnbindHolder(ModuleData<?> moduleData, THolder holder) {
        super.onUnbindHolder(moduleData, holder);
        holder.getIconContainer().hideAll();
    }

    @Override
    public long getStableId(TItem contentItem) {
        return Objects.hash(contentItem.getId(), contentItem.isRented());
    }

    protected abstract static class ViewHolder extends ContentCardPresenter.ViewHolder {

        protected ViewHolder(View view) {
            super(view);
        }

        public abstract TextView getInfoTextView();

        @Override
        public abstract VodOrderedIconsContainer getIconContainer();

        ColorDrawable mPlaceHolderDrawable = new ColorDrawable();
    }
}
