/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.pvr.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.pvr.model.IBaseRecordingSeries;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.pvr.model.SeriesRecordingStatus;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.SeriesRecordingA1CardBinding;
import tv.threess.threeready.ui.details.DetailOpenedFrom;
import tv.threess.threeready.ui.generic.presenter.SeriesCardPresenter;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.BroadcastOrderedIconsContainer;
import tv.threess.threeready.ui.pvr.RecordingActionHelper;
import tv.threess.threeready.ui.tv.view.ContentMarkersView;
import tv.threess.threeready.ui.tv.view.ProgressIndicatorView;

/**
 * Card presenter for the A1 variant recording series cards.
 *
 * @author Barabas Attila
 * @since 2018.12.05
 */
public class SeriesRecordingA1CardPresenter extends SeriesCardPresenter<
        SeriesRecordingA1CardPresenter.ViewHolder, IBaseRecordingSeries> {
    private static final String TAG = Log.tag(SeriesRecordingA1CardPresenter.class);

    private static final int MAX_RECYCLER_VIEW_COUNT = 20;

    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);
    private final RecordingActionHelper mRecordingActionHelper = Components.get(RecordingActionHelper.class);

    public SeriesRecordingA1CardPresenter(Context context) {
        super(context);
    }

    @Override
    protected SeriesCardPresenter.ViewHolder createViewHolder(ViewGroup parent) {
        SeriesRecordingA1CardBinding binding = SeriesRecordingA1CardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindHolder(ModuleData<?> moduleData, ViewHolder holder, IBaseRecordingSeries series) {
        super.onBindHolder(moduleData, holder, series);

        updateRecordingStatus(holder, series);
        updateProviderLogo(holder.mBinding.providerLogo, holder.mBinding.providerLogo, series.getChannelId());
    }

    @Override
    public void onUnbindHolder(ModuleData<?> moduleData, ViewHolder holder) {
        super.onUnbindHolder(moduleData, holder);

        RxUtils.disposeSilently(holder.mRecStatusDisposable);

        holder.mCompleteSeries = null;
        holder.mRecordingStatus = null;
        holder.getIconContainer().hideAll();

        // reset Channel Logo and Gradient visibility
        holder.mBinding.logoGradient.setVisibility(View.GONE);
        holder.mBinding.providerLogo.setVisibility(View.GONE);

        Glide.with(mContext).clear(holder.mBinding.providerLogo);
    }

    protected void updateRecordingStatus(ViewHolder holder, IBaseRecordingSeries series) {
        holder.mRecStatusDisposable = mPvrRepository.getSeriesRecordingStatus(series)
                .zipWith(mPvrRepository.getSeriesRecording(series.getId()),
                        (status, completeSeries) -> Pair.create(completeSeries, status))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recording -> {
                    holder.mCompleteSeries = recording.first;
                    holder.mRecordingStatus = recording.second;
                    holder.getIconContainer().showRecordingStatus(holder.mCompleteSeries, holder.mRecordingStatus);
                    updateContentDescription(holder, series);
                    holder.view.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED);
                }, throwable -> {
                    Log.e(TAG, "Couldn't get the recording status.", throwable);
                    holder.getIconContainer().hideRecordingStatus();
                });
    }

    @Override
    public boolean onKeyEvent(ModuleData<?> moduleData, ViewHolder holder,
                              IBaseRecordingSeries series, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP && holder.mCompleteSeries != null) {
            if (event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_RECORD) {
                mRecordingActionHelper.handleRecKeyAction(
                        holder.mCompleteSeries, holder.mRecordingStatus);
                return true;
            }
        }
        return super.onKeyEvent(moduleData, holder, series, event);
    }

    @Override
    protected void updateInfoRowText(ViewHolder holder, IBaseRecordingSeries seriesRecording) {
        super.updateInfoRowText(holder, seriesRecording);
        List<String> infoParts = new ArrayList<>();

        int numberOfEpisodes = seriesRecording.getNumberOfRecordings();
        if (numberOfEpisodes > 0) {
            infoParts.add(numberOfEpisodes + " " + mTranslator.get((numberOfEpisodes > 1) ?
                    TranslationKey.NUMBER_EPISODES : TranslationKey.NUMBER_EPISODE));
        }

        String genres = TextUtils.join(", ",
                seriesRecording.getGenres().stream().limit(1).toArray());

        if (!TextUtils.isEmpty(genres)) {
            infoParts.add(genres);
        }

        holder.mBinding.details.setText(TextUtils.join(TimeUtils.DIVIDER, infoParts));
    }

    @Override
    protected int getNumberOfEpisodes(IBaseRecordingSeries series) {
        return series.getNumberOfRecordings();
    }

    @Override
    protected List<String> getGenres(IBaseRecordingSeries series) {
        return series.getGenres();
    }

    @Override
    public long getStableId(IBaseRecordingSeries series) {
        return Objects.hash(series.getId(), series.getNumberOfRecordings());
    }

    @Override
    protected void openDetailPage(ViewHolder holder, IBaseRecordingSeries seriesRecording) {
        mNavigator.showSeriesRecordingDetailPage(seriesRecording.getId(), DetailOpenedFrom.Recording);
    }

    @Override
    public int getMaxRecyclerViewCount() {
        return MAX_RECYCLER_VIEW_COUNT;
    }

    @Override
    public int getCardWidth(ModuleData<?> moduleData, IBaseRecordingSeries series) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.series_a1_card_width_focused);
    }

    @Override
    public int getCardHeight(ModuleData<?> moduleData, IBaseRecordingSeries series) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.series_a1_card_height_focused);
    }

    @Override
    protected int getCoverHeight(IBaseRecordingSeries seriesRecording) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.series_a1_landscape_cover_height);
    }

    @Override
    protected int getCoverWidth(IBaseRecordingSeries seriesRecording) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.series_a1_landscape_cover_width);
    }

    @Override
    protected int getItemAlignmentOffset() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.series_a1_landscape_card_item_alignment_offset);
    }

    static class ViewHolder extends SeriesCardPresenter.ViewHolder {

        private final SeriesRecordingA1CardBinding mBinding;

        SeriesRecordingStatus mRecordingStatus;
        IRecordingSeries mCompleteSeries;

        Disposable mRecStatusDisposable;

        public ViewHolder(SeriesRecordingA1CardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        @Override
        @NotNull
        public TextView getTitleView() {
            return mBinding.cardTitle;
        }

        @Override
        @NotNull
        public TextView getDetailsView() {
            return mBinding.details;
        }

        @Override
        @NotNull
        public View getLabelView() {
            return mBinding.label;
        }

        @Override
        @NotNull
        public ImageView getCoverView() {
            return mBinding.cover;
        }

        @Override
        @NotNull
        public ContentMarkersView getContentMarkerView() {
            return mBinding.cardMarker;
        }

        @Override
        @NotNull
        public ProgressIndicatorView getProgressIndicatorView() {
            return mBinding.progressIndicator;
        }

        @Override
        protected BroadcastOrderedIconsContainer getIconContainer() {
            return mBinding.iconContainer;
        }
    }
}
