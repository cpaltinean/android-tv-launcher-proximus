package tv.threess.threeready.ui.settings.fragment;

import android.os.Bundle;
import android.view.KeyEvent;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.account.model.account.IConsent;
import tv.threess.threeready.api.account.model.service.ConsentRequestScenario;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.settings.model.ISettingsItem;
import tv.threess.threeready.ui.settings.model.MainSettingsItem;
import tv.threess.threeready.ui.settings.model.SettingsItem;

/**
 * Fragment displaying privacy and GDPR preferences, enabling the user to change the settings done
 * in the FTI
 *
 * @author Denisa Trif
 * @since 29.05.2019
 */
public class PrivacyPreferencesFragment extends BaseSettingsDialogFragment<SettingsItem> {
    private static final String TAG = Log.tag(PrivacyPreferencesFragment.class);

    private static final String EXTRA_CONSENT = "consent";

    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);
    private final Navigator mNavigator = Components.get(Navigator.class);

    private Disposable mDisposable;

    // The currently displayed consent, used to restore the focus on Privacy Settings dialog
    private IConsent mConsentDisplayed;

    private final List<IConsent> mConsents = new ArrayList<>();

    public static PrivacyPreferencesFragment newInstance(IConsent consent) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CONSENT, consent);
        PrivacyPreferencesFragment fragment = new PrivacyPreferencesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mConsentDisplayed = getSerializableArgument(EXTRA_CONSENT);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        RxUtils.disposeSilently(mDisposable);
    }

    @Override
    protected void setTitle() {
        getTitleView().setText(mTranslator.get(TranslationKey.SETTINGS_PRIVACY));
    }

    @Override
    protected void initView() {
        mAccountRepository.getConsents(ConsentRequestScenario.SETTING_SCREEN.name())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<? extends IConsent>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mDisposable = d;
                    }

                    @Override
                    public void onSuccess(List<? extends IConsent> consents) {
                        Log.d(TAG, "Consents loaded for settings screen.");
                        mConsents.addAll(consents);
                        initMenuItems();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "Could not load consents for settings screen.", e);
                        initMenuItems();
                    }
                });
    }

    @Override
    public void onItemClick(ISettingsItem clickedItem) {
        super.onItemClick(clickedItem);
        String identifier = clickedItem.getPreferenceGroup();
        for (IConsent consent : mConsents) {
            if (Objects.equals(consent.getIdentifier(), identifier)) {
                mNavigator.clearAllDialogs();
                mNavigator.showConsents(null, consent);
            }
        }
    }

    /**
     * Initialize the menu.
     */
    private void initMenuItems() {
        List<SettingsItem> menuItems = new ArrayList<>();
        SettingsItem item;
        if (mConsents.isEmpty()) {
            Log.d(TAG, "Could not initialize menu items, consent list is empty.");
            item = new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_GDPR_NOT_AVAILABLE_TITLE), mTranslator.get(TranslationKey.SETTINGS_GDPR_NOT_AVAILABLE_BODY));
            menuItems.add(item);
            setItems(menuItems);
            return;
        }

        for (IConsent consent : mConsents) {
            String subtitle;
            if (consent.isAccepted()) {
                subtitle = mTranslator.get(TranslationKey.CONSENT_ACTIVATED);
            } else {
                subtitle = mTranslator.get(TranslationKey.CONSENT_DEACTIVATED);
            }
            item = new SettingsItem(consent.getTitle(), subtitle);
            item.setPreferenceGroup(consent.getIdentifier());
            menuItems.add(item);
        }
        setItems(menuItems);

        if (mConsentDisplayed == null) {
            // We don't need to restore the focus. Select the first item as default.
            getVerticalGridView().setSelectedPosition(0);
            return;
        }

        for (int i = 0; i < mConsents.size(); i++) {
            if (Objects.equals(mConsents.get(i).getIdentifier(), mConsentDisplayed.getIdentifier())) {
                // Set the focus to the previously opened consent
                getVerticalGridView().setSelectedPosition(i);
            }
        }
    }

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        return new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                .addDetail(UILogEvent.Detail.Page, UILog.Page.SecurityPrivacy);
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            mNavigator.openSettings(MainSettingsItem.MainItemType.PRIVACY);
            return true;
        }
        return super.onKeyUp(event);
    }
}
