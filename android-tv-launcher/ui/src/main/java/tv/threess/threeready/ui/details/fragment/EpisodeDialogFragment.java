/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.details.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;

import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.BroadcastEpisodeDialogBinding;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.dialog.AlertDialog;
import tv.threess.threeready.ui.generic.dialog.BaseDialogFragment;
import tv.threess.threeready.ui.generic.presenter.ActionButtonsPresenter;
import tv.threess.threeready.ui.generic.presenter.ActionModel;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Base dialog to show details and offer possibility of actions
 * for an episode open from the series detail page.
 *
 * @author Barabas Attila
 * @since 2018.12.12
 */
public abstract class EpisodeDialogFragment<TContentItem extends IContentItem> extends BaseDialogFragment {
    private static final String TAG = Log.tag(EpisodeDialogFragment.class);

    protected static final String EXTRA_CONTENT_ITEM = "EXTRA_CONTENT_ITEM";
    protected static final String EXTRA_SERIES_ITEM = "EXTRA_SERIES_ITEM";

    protected final Translator mTranslator = Components.get(Translator.class);
    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    protected InterfacePresenterSelector mPresenterSelector;
    protected ArrayObjectAdapter<ActionModel> mAdapter;
    protected ItemBridgeAdapter mItemBridgeAdapter;

    protected BroadcastEpisodeDialogBinding mBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenterSelector = new InterfacePresenterSelector()
                .addClassPresenter(ActionModel.class, new ActionButtonsPresenter(getContext()));
        mAdapter = new ArrayObjectAdapter<>();
        mItemBridgeAdapter = new ItemBridgeAdapter(mAdapter, mPresenterSelector);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mBinding = BroadcastEpisodeDialogBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    protected TContentItem getContentItem() {
        return getSerializableArgument(EXTRA_CONTENT_ITEM);
    }

    protected IVodSeries getSeriesItem() {
        return getSerializableArgument(EXTRA_SERIES_ITEM);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        mBinding.description.setTextColor(mLayoutConfig.getFontColor());
        mBinding.showMoreBtn.setBackground(UiUtils.createMoreButtonDrawable(view.getContext(), mLayoutConfig));
        mBinding.actionGrid.setAdapter(mItemBridgeAdapter);

        updateProvideLogo(getContentItem());
        updateTitle(getContentItem());
        updateDescription(getContentItem());
        updateButtons(getContentItem(), null);
        updateWatchlist(getContentItem());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Glide.with(mBinding.getRoot().getContext())
                .clear(mBinding.providerLogo);
    }

    @Override
    public boolean onKeyDown(KeyEvent event) {
        Log.d(TAG, "onKeyDown: " + event.toString());
        if (event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE) {
            dismiss();
            return true;
        }

        return super.onKeyDown(event);
    }

    protected void updateButtonDescriptions(List<ActionModel> actionModelList, TContentItem contentItem) {
        for (int i = 0; i < actionModelList.size(); ++i) {
            ActionModel model = actionModelList.get(i);
            TalkBackMessageBuilder description = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_BUTTON_OTHER));

            if (i == 0) {
                description = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_BUTTON_DIALOG))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_DIALOG_BODY, contentItem.getDescription());
            }

            model.setContentDescription(description.replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BUTTON_NAME, model.getText()).toString());
        }
    }

    protected void updateTitle(TContentItem contentItem) {
        mBinding.title.setText(contentItem.getEpisodeTitleWithSeasonEpisode(mTranslator, ""));
    }

    protected void updateDescription(TContentItem contentItem) {
        String description = contentItem.getDescription();
        if (TextUtils.isEmpty(description)) {
            mBinding.description.setVisibility(View.GONE);
            mBinding.showMoreBtn.setVisibility(View.GONE);
            return;
        }

        mBinding.description.setVisibility(View.VISIBLE);

        int moreInfoMarginStart = mBinding.description.getResources().getDimensionPixelOffset(R.dimen.details_more_info_margin_start);
        int maxLines = mBinding.description.getMaxLines();
        float measuredText = mBinding.description.getPaint().measureText(description) + moreInfoMarginStart;
        float viewWidth = mBinding.description.getWidth();

        if (viewWidth > 0 && measuredText / viewWidth > maxLines) {
            int ellipsizePosition = mBinding.description.getPaint().breakText(
                    description, true, maxLines * viewWidth - moreInfoMarginStart, null);
            mBinding.description.setText(String.format("%s...", description.substring(0, ellipsizePosition)));
            mBinding.showMoreBtn.setVisibility(View.VISIBLE);
        } else {
            mBinding.description.setText(description);
            mBinding.showMoreBtn.setVisibility(View.GONE);
        }

        // Calculate show more button again after layout.
        if (mBinding.description.getWidth() == 0) {
            mBinding.description.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v, int left, int top, int right,
                                           int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    mBinding.description.post(() -> updateDescription(contentItem));
                    mBinding.description.removeOnLayoutChangeListener(this);
                }
            });
        }

        // Show description dialog.
        mBinding.showMoreBtn.setOnClickListener(v -> {
            AlertDialog dialog = new AlertDialog.Builder()
                    .title(contentItem.getEpisodeTitleWithSeasonEpisode(mTranslator, ""))
                    .description(contentItem.getDescription())
                    .descriptionFontSize(getResources().getDimensionPixelOffset(R.dimen.alert_description_size_more_info))
                    .cancelable(true)
                    .build();

            mNavigator.showDialogOnTop(dialog);
        });
    }

    protected void updateProvideLogo(TContentItem broadcast) {
    }

    protected void updateButtons(TContentItem contentItem, Boolean inWatchlist) {
    }

    protected void updateWatchlist(TContentItem contentItem) {
    }
}
