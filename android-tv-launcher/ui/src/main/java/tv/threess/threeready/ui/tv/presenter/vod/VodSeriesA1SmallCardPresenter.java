package tv.threess.threeready.ui.tv.presenter.vod;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.vod.model.IBaseVodSeries;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.VodSeriesA1SmallCardBinding;

/**
 * Presenter to display a small landscape card for a VOD series.
 *
 * @author Barabas Attila
 * @since 4/6/22
 */
public class VodSeriesA1SmallCardPresenter extends VodSeriesA1CardPresenter<VodSeriesA1CardPresenter.ViewHolder> {

    public VodSeriesA1SmallCardPresenter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        VodSeriesA1SmallCardBinding binding = VodSeriesA1SmallCardBinding.inflate(
                    LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    protected int getCoverHeight(IBaseVodSeries item) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.vod_series_a1_small_card_height_focused);
    }

    @Override
    protected int getCoverWidth(IBaseVodSeries item) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.vod_series_a1_small_card_width_focused);
    }

    @Override
    public int getCardWidth(@Nullable ModuleData<?> moduleData, IBaseVodSeries iBaseVodSeries) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.vod_series_a1_small_card_width_focused);
    }

    @Override
    public int getCardHeight(@Nullable ModuleData<?> moduleData, IBaseVodSeries iBaseVodSeries) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.vod_series_a1_small_card_height_focused);
    }

    static class ViewHolder extends VodSeriesA1CardPresenter.ViewHolder {
        ColorDrawable mPlaceHolderDrawable =
                new ColorDrawable(Components.get(LayoutConfig.class).getPlaceHolderColor());
        final VodSeriesA1SmallCardBinding mBinding;

        @Override
        public TextView getTitleView() {
            return mBinding.title;
        }

        @Override
        public ImageView getCoverView() {
            return mBinding.cover;
        }

        @Override
        public ColorDrawable getPlaceHolderDrawable() {
            return mPlaceHolderDrawable;
        }

        public ViewHolder(VodSeriesA1SmallCardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
