/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.startup.step;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Startup step to start the data caching services.
 *
 * @author Barabas Attila
 * @since 2018.06.29
 */
public class CachingStep implements Step {
    private static final String TAG = Log.tag(CachingStep.class);

    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    private Disposable mDisposable;

    @Override
    public void execute(StepCallback callback) {
        mDisposable = mAccountRepository.caching()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(mAppConfig.getQueryTimeout(), TimeUnit.SECONDS)
                .subscribe(callback::onFinished, (e) -> {
                    Log.e(TAG, "Could not finish caching.", e);
                    callback.onFinished();
                });
    }

    @Override
    public void cancel() {
        RxUtils.disposeSilently(mDisposable);
    }
}
