/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.home.presenter.module.stripe.nownext;

import android.content.Context;
import android.util.Pair;
import android.view.ViewGroup;

import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.home.presenter.card.app.EditorialA1CardPresenter;
import tv.threess.threeready.ui.home.presenter.module.stripe.generic.StripeModulePresenter;
import tv.threess.threeready.ui.tv.presenter.nownext.TvNowNextA1CardPresenter;

/**
 * Presenter class for a stripe module. (tv_now_next module type, variant a1)
 * Responsible to load the data from the given data source and display it in a horizontal grid.
 * The loaded data will be displayed with cards.
 *
 * Created by Szilard on 5/18/2017.
 */

public class NowNextAStripeModulePresenter extends StripeModulePresenter {

    Map<ModuleCardVariant, PresenterSelector> mVariantCardPresenterMap = new HashMap<>();

    public NowNextAStripeModulePresenter(Context context, DataSourceLoader dataSourceLoader) {
        super(context, dataSourceLoader);
        // NowNext A1 Stripe
        mVariantCardPresenterMap.put(ModuleCardVariant.A1, new ClassPresenterSelector()
                .addClassPresenter(Pair.class, new TvNowNextA1CardPresenter(context))
                .addClassPresenter(EditorialItem.class, new EditorialA1CardPresenter(context)));
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent, RecyclerView.RecycledViewPool pool) {
        ViewHolder holder = (ViewHolder) super.onCreateViewHolder(parent, pool);

        holder.mBinding.stripe.getStripeGrid().setWindowAlignmentOffsetPercent(
                mContext.getResources().getInteger(R.integer.tv_now_next_a_window_alignment_offset));

        return holder;
    }

    @Override
    protected PresenterSelector getCardPresenterSelector(ModuleConfig moduleConfig) {
        return mVariantCardPresenterMap.get(moduleConfig.getCardVariant());
    }

    /**
     * Gets the value for horizontal padding.
     */
    @Override
    protected int getHorizontalPadding() {
        return mContext.getResources().getDimensionPixelSize(R.dimen.stripe_title_left_margin);
    }

    /**
     * Gets the value for bottom padding.
     */
    @Override
    protected int getBottomPadding() {
        return 0;
    }

    /**
     * Gets the value for bottom margin.
     */
    @Override
    protected int getBottomMargin() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_now_next_a1_stripe_module_bottom_margin);
    }

    /**
     * Gets the value for item spacing.
     */
    @Override
    protected int getItemSpacing(ModuleConfig moduleConfig) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_now_next_a_item_spacing);
    }

    /**
     * Gets the value for row height.
     */
    @Override
    protected int getRowHeight(ModuleData<?> moduleData) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_now_next_a_stripe_row_height);
    }

    /**
     * Gets the value for top margin.
     */
    @Override
    protected int getTopMargin() {
        return 0;
    }
}
