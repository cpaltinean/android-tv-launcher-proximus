package tv.threess.threeready.ui.epggrid.listener;

import tv.threess.threeready.api.tv.model.IBroadcast;

/**
 * Listener to get notifications when a user selects a broadcast in the EPG grid.
 *
 * @author Barabas Attila
 * @since 8/26/21
 */
public interface EpgBroadcastSelectedListener {

    /**
     * Called on broadcast selection in the EPG grid..
     * @param broadcast The selected broadcast in the grid.
     */
    void onBroadcastSelected(IBroadcast broadcast);
}
