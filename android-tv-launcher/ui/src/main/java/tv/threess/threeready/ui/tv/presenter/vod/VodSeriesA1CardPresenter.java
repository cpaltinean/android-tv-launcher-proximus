package tv.threess.threeready.ui.tv.presenter.vod;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;

import java.util.Objects;

import tv.threess.threeready.api.vod.model.IBaseVodSeries;
import tv.threess.threeready.ui.generic.presenter.CoverCardPresenter;
import tv.threess.threeready.ui.utils.TitleFallbackImageViewTarget;

/**
 * Presenter to display a landscape card for a VOD series.
 *
 * @author Barabas Attila
 * @since 4/6/22
 */
public abstract class VodSeriesA1CardPresenter<THolder extends VodSeriesA1CardPresenter.ViewHolder>
        extends CoverCardPresenter<THolder, IBaseVodSeries> {

    public VodSeriesA1CardPresenter(Context context) {
        super(context);
    }

    @Override
    protected void openDetailPage(THolder holder, IBaseVodSeries item) {
        mNavigator.showVodSeriesDetails(item.getId());
    }

    @Override
    public long getStableId(IBaseVodSeries series) {
        return Objects.hash(series.getId());
    }

    @Override
    protected void loadCoverImage(THolder holder, IBaseVodSeries item, int coverWidth,
                                  int coverHeight, RequestBuilder<Drawable> requestBuilder) {
        requestBuilder
                .apply(new RequestOptions()
                        .placeholder(holder.getPlaceHolderDrawable())
                        .override(coverWidth, coverHeight)
                        .optionalCenterInside())
                .transition(new DrawableTransitionOptions().dontTransition())
                .into(new TitleFallbackImageViewTarget(holder.getCoverView(),
                        holder.getTitleView(),
                        item.getTitle()));
    }

    static abstract class ViewHolder extends CoverCardPresenter.ViewHolder {

        public ViewHolder(View root) {
            super(root);
        }
    }
}
