package tv.threess.threeready.ui.details.fragment;

import android.os.Bundle;

import java.util.List;

import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.PlayOption;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.details.DetailOpenedFrom;
import tv.threess.threeready.ui.generic.model.RecordingPlayOption;

/**
 * Episode detail dialog for the recording episodes.
 *
 * @author David Bondor
 * @since 19.07.2022
 */
public class RecordingEpisodeDialogFragment extends BroadcastEpisodeDialogFragment {

    protected static final String EXTRA_RECORDING_SERIES = "EXTRA_RECORDING_SERIES";

    public static RecordingEpisodeDialogFragment newInstance(IRecording recording,
                                                             IRecordingSeries recordingSeries,
                                                             RecordingStatus recordingStatus,
                                                             DetailOpenedFrom openedFrom, IBookmark bookmark) {
        RecordingEpisodeDialogFragment fragment = new RecordingEpisodeDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_CONTENT_ITEM, recording);
        bundle.putSerializable(EXTRA_RECORDING_SERIES, recordingSeries);
        bundle.putSerializable(EXTRA_RECORDING_STATUS, recordingStatus);
        bundle.putSerializable(EXTRA_OPENED_FROM, openedFrom);
        bundle.putSerializable(EXTRA_BOOKMARK, bookmark);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected List<PlayOption> generatePlayOptions(TvChannel channel, IBroadcast broadcast, RecordingStatus recordingStatus) {
        return RecordingPlayOption.generatePlayOptions(channel, broadcast, getSerializableArgument(EXTRA_RECORDING_SERIES),
                mBookmark, recordingStatus,
                mOpenedFrom == DetailOpenedFrom.Recording,
                mOpenedFrom == DetailOpenedFrom.ContinueWatching);
    }

    @Override
    protected void showPlayer(PlayOption playOption) {
        if (playOption instanceof RecordingPlayOption) {
            RecordingPlayOption recordingPlayOption = (RecordingPlayOption) playOption;
            mNavigator.showRecordingPlayer(StartAction.Ui, recordingPlayOption.getContentItem(), recordingPlayOption.getRecordingSeries(), null, true);
        } else {
            super.showPlayer(playOption);
        }
    }
}
