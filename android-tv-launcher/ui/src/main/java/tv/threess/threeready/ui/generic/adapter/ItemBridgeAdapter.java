/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.adapter;

import android.annotation.SuppressLint;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.leanback.widget.FacetProvider;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.leanback.widget.SinglePresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.presenter.BasePresenter.RefreshPayload;
import tv.threess.threeready.ui.generic.presenter.EmptyPresenter;


/**
 * Bridge from {@link Presenter} to {@link RecyclerView.Adapter}. Public to allow use by third
 * party Presenters
 *
 * Created by Barabas Attila on 4/12/2017.
 */

public class ItemBridgeAdapter extends RecyclerView.Adapter {
    private static final String TAG = Log.tag(ItemBridgeAdapter.class);

    private ArrayObjectAdapter<?> mAdapter;
    private final PresenterSelector mPresenterSelector;
    private final RecyclerView.RecycledViewPool mRecycledViewPool;
    private boolean mCircular;

    private final ArrayList<BasePresenter> mPresenters = new ArrayList<>();

    public ItemBridgeAdapter(ArrayObjectAdapter<?> adapter) {
        this(adapter, adapter.getPresenterSelector());
    }

    public ItemBridgeAdapter(ArrayObjectAdapter<?> adapter, Presenter presenter,
                             RecyclerView.RecycledViewPool recycledViewPool) {
        this(adapter, new SinglePresenterSelector(presenter), recycledViewPool);
    }

    public ItemBridgeAdapter(ArrayObjectAdapter<?> adapter, PresenterSelector presenterSelector,
                             RecyclerView.RecycledViewPool recycledViewPool) {
        setAdapter(adapter);
        mPresenterSelector = presenterSelector;
        mRecycledViewPool = recycledViewPool;

        initRecyclerViewPool();
    }

    public ItemBridgeAdapter(ArrayObjectAdapter<?> adapter, Presenter presenter) {
        this(adapter, presenter, false);
    }

    public ItemBridgeAdapter(ArrayObjectAdapter<?> adapter, Presenter presenter, boolean circular) {
        this(adapter, new SinglePresenterSelector(presenter), circular);
    }

    public ItemBridgeAdapter(ArrayObjectAdapter<?> adapter, PresenterSelector presenterSelector) {
        this(adapter, presenterSelector, false);
    }

    public ItemBridgeAdapter(ArrayObjectAdapter<?> adapter, PresenterSelector presenterSelector, boolean circular) {
        setAdapter(adapter);
        mPresenterSelector = presenterSelector;
        mRecycledViewPool = new RecyclerView.RecycledViewPool();
        mCircular = circular;

        initRecyclerViewPool();
    }

    private void initRecyclerViewPool() {
        if(mPresenterSelector == null
                || mPresenterSelector.getPresenters() == null) {
            return;
        }

        for (Presenter presenter : mPresenterSelector.getPresenters()) {
            if (presenter instanceof BasePresenter) {
                BasePresenter basePresenter = (BasePresenter) presenter;
                mRecycledViewPool.setMaxRecycledViews(basePresenter.getItemViewType(),
                        basePresenter.getMaxRecyclerViewCount());
            }
        }
    }

    /**
     * @return True if the items from the adapter should start over when the end reached.
     */
    public boolean isCircular() {
        return mCircular;
    }

    public <TObject> ArrayObjectAdapter<TObject> getAdapter() {
        return (ArrayObjectAdapter<TObject>) mAdapter;
    }

    public PresenterSelector getPresenterSelector() {
        return mPresenterSelector;
    }

    /**
     * Sets the {@link ObjectAdapter}.
     */
    public void setAdapter(ArrayObjectAdapter<?> adapter) {
        if (adapter == mAdapter) {
            return;
        }
        if (mAdapter != null) {
            mAdapter.unregisterObserver(mDataObserver);
        }
        mAdapter = adapter;
        if (mAdapter == null) {
            notifyDataSetChanged();
            return;
        }

        mAdapter.registerObserver(mDataObserver);
        if (hasStableIds() != mAdapter.hasStableIds()) {
            setHasStableIds(mAdapter.hasStableIds());
        }
        notifyDataSetChanged();
    }


    private BasePresenter getPresenterForType(int type) {
        for (BasePresenter presenter : mPresenters) {
            if (presenter.getItemViewType() == type) {
                return presenter;
            }
        }

        Log.e(TAG, "No presenter found.");
        return new EmptyPresenter();
    }

    /**
     * Clears the adapter.
     */
    public void clear() {
        setAdapter(null);
    }

    @Override
    public int getItemCount() {
        int itemCount = mAdapter != null ? mAdapter.size() : 0;

        if (mCircular) {
            return itemCount == 0 ? 0 : Integer.MAX_VALUE;
        }

        return itemCount;
    }

    public int getNormalizedPosition(int circularPosition) {
        int itemCount = mAdapter != null ? mAdapter.size() : 0;
        return itemCount == 0 ? 0 : circularPosition % itemCount;
    }

    public int getCircularPosition(int normalizedPosition) {
        int itemCount = mAdapter != null ? mAdapter.size() : 0;
        if (isCircular() && itemCount > 0) {
            int intMid = Integer.MAX_VALUE / 2;
            return intMid - intMid % itemCount + normalizedPosition;
        }

        return normalizedPosition;
    }

    public Object getItem(int position) {
        if (mAdapter == null || mAdapter.size() == 0) {
            return null;
        }

        return mAdapter.get(getNormalizedPosition(position));
    }

    /**
     * @param position The position of the item in the adapter.
     * @return The stable ID for the item at the given position.
     * Used to identify if the item at the position has to be refreshed after an adapter change.
     * If hasStableIds() would return false this method should return NO_ID.
     */
    @Override
    public long getItemId(int position) {
        if (mAdapter == null || mAdapter.size() == 0
                || !mAdapter.hasStableIds()) {
            return ObjectAdapter.NO_ID;
        }

        Object item = mAdapter.get(getNormalizedPosition(position));
        if (item == null) {
            // Item is null. No stable id.
            return ObjectAdapter.NO_ID;
        }

        BasePresenter presenter = (BasePresenter) mPresenterSelector.getPresenter(item);
        if (presenter == null) {
            // Presenter is null. No stable id.
            return ObjectAdapter.NO_ID;
        }

        // Pass the stable id calculation to the presenter.
        return presenter.getStableId(item);
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        BasePresenter presenter = item != null ? (BasePresenter) mPresenterSelector.getPresenter(item) : new EmptyPresenter();
        if (presenter == null) {
            Log.w(TAG, "No presenter found for " + item);
            presenter = new EmptyPresenter();
        }

        int index = mPresenters.indexOf(presenter);
        if (index < 0) {
            mPresenters.add(presenter);
            Log.v(TAG, "getItemViewType added presenter "
                    + presenter + " type " + presenter.getItemViewType());
        }

        return presenter.getItemViewType();
    }


    /**
     * {@link View.OnFocusChangeListener} that assigned in
     * {@link Presenter#onCreateViewHolder(ViewGroup)} may be chained, user should never change
     * {@link View.OnFocusChangeListener} after that.
     */
    @Override
    public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.v(TAG, "onCreateViewHolder viewType " + viewType);
        BasePresenter presenter = getPresenterForType(viewType);
        Presenter.ViewHolder presenterVh = presenter.onCreateViewHolder(parent, mRecycledViewPool);
        View view = presenterVh.view;

        PresenterViewHolder presenterViewHolder = new PresenterViewHolder(presenter, view, presenterVh);
        View presenterView = presenterViewHolder.mHolder.view;
        if (presenterView != null) {
            presenterView.setTag(presenterViewHolder);
            presenterView.setOnFocusChangeListener(presenterViewHolder.mFocusChangeListener);
            presenterView.setOnClickListener(presenterViewHolder.mOnClickListener);
            presenterView.setOnLongClickListener(presenterViewHolder.mOnLongClickListener);
            presenterView.setOnKeyListener(presenterViewHolder.mOnKeyListener);
        }

        return presenterViewHolder;
    }

    @Override
    public final void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onBindViewHolder(holder, position, null);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, List originalPayLoads) {
        Log.v(TAG, "onBindViewHolder position " + position);
        PresenterViewHolder presenterViewHolder = (PresenterViewHolder) holder;

        List<Object> payloads = new ArrayList<>();
        if (originalPayLoads != null) {
            payloads.addAll(originalPayLoads);
        }

        // Check if item refresh payload should be added.
        long newItemId = getItemId(position);
        Object newItem = getItem(position);
        if (newItem == null) {
            Log.w(TAG, "Skipp data bind. The item is null.");
            return;
        }

        if (hasStableIds()
                && newItemId != ObjectAdapter.NO_ID && newItemId == presenterViewHolder.mItemId) {
            payloads.add(RefreshPayload.ITEM_REFRESH);
        }

        presenterViewHolder.mItem = newItem;
        presenterViewHolder.mItemId = newItemId;

        holder.itemView.setTag(R.integer.adapter_size, getItemCount());
        holder.itemView.setTag(R.integer.normalized_adapter_position, getNormalizedPosition(position));

        // Bind data to view holder.
        if (payloads.isEmpty()) {

            resetListeners(presenterViewHolder);

            onBindViewHolder(presenterViewHolder.mPresenter, presenterViewHolder.mHolder, presenterViewHolder.mItem);

            // Set focused state.
            if (holder.itemView.hasFocus()) {
                onFocusedViewHolder(presenterViewHolder.mPresenter, presenterViewHolder.mHolder, presenterViewHolder.mItem);
            } else {
                onDefaultViewHolder(presenterViewHolder.mPresenter, presenterViewHolder.mHolder, presenterViewHolder.mItem);
            }

        // Refresh item with the payloads.
        } else {
            onBindViewHolder(presenterViewHolder.mPresenter, presenterViewHolder.mHolder, presenterViewHolder.mItem, payloads);
        }
    }

    protected void onBindViewHolder(BasePresenter<Presenter.ViewHolder, Object> presenter,
                                    Presenter.ViewHolder holder, Object item, List<?> payloads) {
        presenter.onBindHolder(holder, item, payloads);
    }

    protected void onBindViewHolder(BasePresenter<Presenter.ViewHolder, Object> presenter,
                                    Presenter.ViewHolder holder, Object item) {
        presenter.onBindHolder(holder, item);
    }

    protected void onDefaultViewHolder(BasePresenter<Presenter.ViewHolder, Object> presenter,
                                       Presenter.ViewHolder holder, Object item) {
        presenter.onDefaultState(holder, item);
    }

    protected void onFocusedViewHolder(BasePresenter<Presenter.ViewHolder, Object> presenter,
                                       Presenter.ViewHolder holder, Object item) {
        presenter.onFocusedState(holder, item);
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        PresenterViewHolder presenterViewHolder = (PresenterViewHolder) holder;
        onUnbindHolder(presenterViewHolder.mPresenter, presenterViewHolder.mHolder, presenterViewHolder.mItem);

        presenterViewHolder.mItem = null;
        presenterViewHolder.mItemId = ObjectAdapter.NO_ID;
    }

    protected void onUnbindHolder(BasePresenter<Presenter.ViewHolder, Object> presenter,
                                  Presenter.ViewHolder holder, Object item) {
        presenter.onUnbindHolder(holder);
    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        PresenterViewHolder presenterViewHolder = (PresenterViewHolder) holder;
        presenterViewHolder.mPresenter.onViewAttachedToWindow(presenterViewHolder.mHolder);
    }

    @Override
    public final void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        PresenterViewHolder presenterViewHolder = (PresenterViewHolder) holder;
        presenterViewHolder.mPresenter.onViewDetachedFromWindow(presenterViewHolder.mHolder);
    }

    @Override
    public boolean onFailedToRecycleView(@NonNull RecyclerView.ViewHolder holder) {
        Log.d(TAG, "On failed to recycle view." + holder);
        return true;
    }

    final class OnFocusChangeListener implements View.OnFocusChangeListener {
        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            PresenterViewHolder holder = (PresenterViewHolder)view.getTag();
            if (holder.mItem == null) {
                Log.w(TAG, "Skipp focus change. The item is null.");
                return;
            }

            if (hasFocus) {
                onFocusedViewHolder(holder.mPresenter, holder.mHolder, holder.mItem);
            } else {
                onDefaultViewHolder(holder.mPresenter, holder.mHolder, holder.mItem);
            }
        }
    }

    protected boolean onViewHolderKeyEvent(BasePresenter<Presenter.ViewHolder, Object> presenter, Presenter.ViewHolder holder,
                                        Object item, KeyEvent event) {
        return presenter.onKeyEvent(holder, item, event);
    }

    final class OnKeyListener implements View.OnKeyListener {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            PresenterViewHolder holder = (PresenterViewHolder)v.getTag();
            if (holder.mItem == null) {
                Log.w(TAG, "Skipp key. The item is null.");
                return false;
            }

            return onViewHolderKeyEvent(holder.mPresenter, holder.mHolder, holder.mItem, event);
        }
    }

    protected void onViewHolderClicked(BasePresenter<Presenter.ViewHolder,
            Object> presenter, Presenter.ViewHolder holder, Object item){
        presenter.onClicked(holder, item);
    }

    final class OnClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            Log.v(TAG, "onClicked" + v);

            PresenterViewHolder holder = (PresenterViewHolder)v.getTag();
            if (holder.mItem == null) {
                Log.w(TAG, "Skipp click. The item is null.");
                return;
            }

            onViewHolderClicked(holder.mPresenter, holder.mHolder, holder.mItem);
        }
    }
    protected boolean onViewHolderLongClicked(BasePresenter<Presenter.ViewHolder,
            Object> presenter, Presenter.ViewHolder holder, Object item){
        return presenter.onLongClicked(holder, item);
    }

    final class OnLongClickListener implements View.OnLongClickListener {
        @Override
        public boolean onLongClick(View v) {
            Log.v(TAG, "onLongClicked" + v);

            PresenterViewHolder holder = (PresenterViewHolder)v.getTag();
            if (holder.mItem == null) {
                Log.w(TAG, "Skipp long click. The is null.");
                return false;
            }

            return onViewHolderLongClicked(holder.mPresenter, holder.mHolder, holder.mItem);
        }
    }

    private void resetListeners(PresenterViewHolder presenterViewHolder) {
        presenterViewHolder.mFocusChangeListener = new OnFocusChangeListener();
        presenterViewHolder.mOnKeyListener = new OnKeyListener();
        presenterViewHolder.mOnClickListener = new OnClickListener();
        presenterViewHolder.mOnLongClickListener = new OnLongClickListener();

        presenterViewHolder.itemView.setOnFocusChangeListener(presenterViewHolder.mFocusChangeListener);
        presenterViewHolder.itemView.setOnClickListener(presenterViewHolder.mOnClickListener);
        presenterViewHolder.itemView.setOnLongClickListener(presenterViewHolder.mOnLongClickListener);
        presenterViewHolder.itemView.setOnKeyListener(presenterViewHolder.mOnKeyListener);
    }

    /**
     * ViewRowHolder for the ItemBridgeAdapter.
     */
    public class PresenterViewHolder extends RecyclerView.ViewHolder implements FacetProvider {
        final BasePresenter<Presenter.ViewHolder, Object> mPresenter;
        final Presenter.ViewHolder mHolder;
        OnFocusChangeListener mFocusChangeListener = new OnFocusChangeListener();
        OnKeyListener mOnKeyListener = new OnKeyListener();
        OnClickListener mOnClickListener = new OnClickListener();
        View.OnLongClickListener mOnLongClickListener = new OnLongClickListener();

        Object mItem;
        long mItemId = ObjectAdapter.NO_ID;

        PresenterViewHolder(BasePresenter<Presenter.ViewHolder, Object> presenter, View view, Presenter.ViewHolder holder) {
            super(view);
            mPresenter = presenter;
            mHolder = holder;
        }

        /**
         * Get {@link BasePresenter}.
         */
        public final BasePresenter<?,?> getPresenter() {
            return mPresenter;
        }

        /**
         * Get {@link Presenter.ViewHolder}.
         */
        public final Presenter.ViewHolder getViewHolder() {
            return mHolder;
        }

        /**
         * Get currently bound object.
         */
        public final Object getItem() {
            return mItem;
        }

        @Override
        public Object getFacet(Class<?> facetClass) {
            return mHolder.getFacet(facetClass);
        }
    }

    private final ObjectAdapter.DataObserver mDataObserver = new ObjectAdapter.DataObserver() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onChanged() {
            ItemBridgeAdapter.this.notifyDataSetChanged();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            ItemBridgeAdapter.this.notifyItemRangeChanged(positionStart, itemCount);
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            ItemBridgeAdapter.this.notifyItemRangeInserted(positionStart, itemCount);
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
            ItemBridgeAdapter.this.notifyItemRangeChanged(positionStart, itemCount, payload);
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            ItemBridgeAdapter.this.notifyItemRangeRemoved(positionStart, itemCount);
        }
    };
}
