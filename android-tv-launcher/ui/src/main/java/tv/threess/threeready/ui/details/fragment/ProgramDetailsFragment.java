/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.details.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption.Mixed;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.details.DetailOpenedFrom;
import tv.threess.threeready.ui.details.presenter.ProgramDetailsOverviewPresenter;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.resolver.DataSourceResolver;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Displays selected card program details
 *
 * @author Daniel Gliga
 * @since 2017.05.23
 */
public class ProgramDetailsFragment extends BaseSingleDetailFragment<IBroadcast, ProgramDetailsOverviewPresenter.ViewHolder> {
    public static final String TAG = Log.tag(ProgramDetailsFragment.class);

    private static final String CONFIG_PROGRAM_PAGE_ID = "DETAIL_PROGRAM";
    private static final String CONFIG_RECORDING_PAGE_ID = "DETAIL_RECORDING_SINGLE";

    private static final String EXTRA_BROADCAST_ID = "EXTRA_BROADCAST_ID";
    private static final String EXTRA_BROADCAST = "EXTRA_BROADCAST";
    private static final String EXTRA_OPENED_FROM = "EXTRA_OPENED_FROM";

    private final TvRepository mTvRepository = Components.get(TvRepository.class);

    private DetailOpenedFrom mOpenedFrom;

    public static ProgramDetailsFragment newInstance(String broadcastId) {
        ProgramDetailsFragment fragment = new ProgramDetailsFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_BROADCAST_ID, broadcastId);
        args.putSerializable(EXTRA_OPENED_FROM, DetailOpenedFrom.Default);
        fragment.setArguments(args);
        return fragment;
    }

    public static ProgramDetailsFragment newInstance(IBroadcast broadcast, DetailOpenedFrom openedFrom) {
        ProgramDetailsFragment fragment = new ProgramDetailsFragment();
        Bundle args = new Bundle();

        args.putSerializable(EXTRA_BROADCAST, broadcast);
        args.putString(EXTRA_BROADCAST_ID, broadcast.getId());
        args.putSerializable(EXTRA_OPENED_FROM, openedFrom);

        fragment.setArguments(args);
        return fragment;
    }

    /**
     * @param broadcastId The broadcast id of the program for which the detail page should be verified.
     * @return True if the detail page was opened for the given broadcast.
     */
    public boolean isOpenedFor(@NonNull String broadcastId) {
        return Objects.equals(broadcastId, getStringArgument(EXTRA_BROADCAST_ID));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDataId = getStringArgument(EXTRA_BROADCAST_ID);
        mData = getSerializableArgument(EXTRA_BROADCAST);
        mOpenedFrom = getSerializableArgument(EXTRA_OPENED_FROM);

        Log.d(TAG, "Detail page opened, Program/Episode id: " + mDataId);
    }

    @Override
    protected void addExtraPresenter(InterfacePresenterSelector classPresenterSelector) {
        classPresenterSelector.addClassPresenter(IBroadcast.class,
                new ProgramDetailsOverviewPresenter(getActivity(), mOpenedFrom,
                        () -> displayPage(getConfigPageForRecommendations())));
    }

    @Override
    protected String getRcmPageConfigId() {
        return mOpenedFrom == DetailOpenedFrom.Recording ? CONFIG_RECORDING_PAGE_ID : CONFIG_PROGRAM_PAGE_ID;
    }

    @Override
    protected void loadDataDetails(String dataId) {
        RxUtils.disposeSilently(mDetailsDisposable);
        mDetailsDisposable = mTvRepository.getBroadcast(dataId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::addUIData, throwable -> {
                    Log.e(TAG, "fail to open program: " + dataId, throwable);
                    showErrorMessage();
                });
    }

    @Override
    protected void loadDataDetails(IBroadcast data) {
        RxUtils.disposeSilently(mDetailsDisposable);
        mDetailsDisposable = mTvRepository.getBroadcast(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::addUIData, throwable -> {
                    Log.e(TAG, "fail to open program: " + data, throwable);
                    showErrorMessage();
                });
    }

    @Override
    protected void displayPage(PageConfig pageConfig) {
        if (mModularPageView != null && pageConfig != null) {
            mModularPageView.loadPageConfig(pageConfig, this, mDataSourceResolver);
        }
    }

    @Nullable
    @Override
    protected Event<?, ?> generatePageOpenEvent() {
        if (mData == null) {
            // Page not loaded yet.
            return null;
        }


        return ReportUtils.createEvent((mOpenedFrom == DetailOpenedFrom.Recording ? UILog.Page.RecordActionSubMenuMain : UILog.Page.ProgramDetail), mData);
    }

    @Override
    protected int getOverviewItemOffset(ProgramDetailsOverviewPresenter.ViewHolder viewHolder) {
        return calculateOverviewItemOffset(viewHolder);
    }

    /**
     * Data source resolver to resolve data sources which depends on the current content.
     */
    private final DataSourceResolver mDataSourceResolver = new DataSourceResolver() {
        @Override
        protected Observable<?> resolveDataSources(ModuleDataSourceMethod method, ModuleConfig moduleConfig,
                                                   int start, int count, List<DataSourceSelector> dataSourceSelectors) {
            if (method == ModuleDataSourceMethod.PROGRAMS_BETWEEN) {
                String channelId = mData.getChannelId();

                ModuleDataSource dataSource = moduleConfig.getDataSource();
                if (dataSource == null || dataSource.getParams() == null) {
                    return null;
                }

                long extraTime = dataSource.getParams()
                        .getFilter(Mixed.HoursExtraTime.NAME, 0);
                long currentTime = System.currentTimeMillis();
                long startTime = mOpenedFrom == DetailOpenedFrom.Recording ? currentTime :
                        (mData.getStart() + mData.getDuration() + 1);
                long endTime = startTime + TimeUnit.HOURS.toMillis(extraTime);
                boolean playable = startTime < currentTime;
                return mTvRepository.getUpcomingBroadcastsBetween(channelId, startTime, endTime,
                        playable, moduleConfig);
            }

            return super.resolveDataSources(method, moduleConfig, start, count, dataSourceSelectors);
        }
    };
}