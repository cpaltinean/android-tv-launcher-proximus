package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.leanback.widget.HorizontalGridView;

import tv.threess.threeready.ui.R;

/**
 * Custom view container which holds the detail page {@link tv.threess.threeready.ui.generic.presenter.ActionButtonsPresenter}
 * and the correspond {@link tv.threess.threeready.api.generic.model.IContentItem} data in
 * the inflated view by the correspond xml file (ex. broadcast_details_overlay.xml).
 * <p>
 * Created by Bartalus Csaba - Zsolt, 2021.09.16
 */

public class DetailOverView extends RelativeLayout {

    HorizontalGridView mActionGridView;

    /**
     * Constructors
     */
    public DetailOverView(Context context) {
        this(context, null);
    }

    public DetailOverView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DetailOverView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mActionGridView = findViewById(R.id.action_grid);
    }

    /**
     * Checks if the parent view got focus. If the {@link tv.threess.threeready.ui.details.presenter.BaseDetailsOverviewPresenter}
     * got focus then forces the android to give the focus for the action button view.
     * Two options are existed for the focus on detail page:
     * 1. If it is a VOD or program detail page, single rec detail page the focus set to the action button grid view.
     * 2. If It is a series (series rec, series vod) and has episode stripe the focus set to the episode grid view,
     * which is implemented in the corresponding {@link androidx.leanback.widget.RowPresenter}.
     *
     * @param child   child view.
     * @param focused view which got focus by default from android or programmatically.
     */
    @Override
    public void requestChildFocus(View child, View focused) {
        boolean viewGotFocused = findFocus() == null;

        super.requestChildFocus(child, focused);

        if (mActionGridView != null && viewGotFocused) {
            mActionGridView.requestFocus();
        }
    }
}
