package tv.threess.threeready.ui.settings.presenter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.epg.EpgLayoutType;
import tv.threess.threeready.ui.databinding.EpgItemLayoutBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.settings.listener.EpgLayoutItemClickListener;
import tv.threess.threeready.ui.settings.model.EpgLayoutItem;

/**
 * Presenter used to display EpgLayoutItems.
 *
 * @author Daniela Toma
 * @since 2021.11.08
 */
public class EpgLayoutItemPresenter extends BasePresenter<EpgLayoutItemPresenter.ViewHolder, EpgLayoutItem> {
    public static final String TAG = Log.tag(EpgLayoutItemPresenter.class);

    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    protected final Translator mTranslator = Components.get(Translator.class);

    private final EpgLayoutItemClickListener mItemClickListener;

    public EpgLayoutItemPresenter(Context context, EpgLayoutItemClickListener itemClickListener) {
        super(context);
        mItemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        EpgItemLayoutBinding binding = EpgItemLayoutBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new EpgLayoutItemPresenter.ViewHolder(binding);
    }

    @Override
    public void onBindHolder(ViewHolder holder, EpgLayoutItem item) {
        holder.mBinding.title.setText(mTranslator.get(item.getTitle()));
        holder.mBinding.description.setText(mTranslator.get(item.getDescription()));

        EpgLayoutType selectedEpgLayoutType = Settings.epgLayoutType.getEnum(EpgLayoutType.MiniEpg);
        holder.mBinding.radioButton.setChecked(selectedEpgLayoutType == item.getEpgLayoutType());
        holder.mBinding.radioButton.setButtonTintList(holder.mBinding.radioButton.isChecked()
                ? ColorStateList.valueOf(mLayoutConfig.getRadioButtonCheckedColor())
                : ColorStateList.valueOf(mLayoutConfig.getRadioButtonUncheckedColor()));
    }

    @Override
    public void onClicked(ViewHolder holder, EpgLayoutItem epgLayoutItem) {
        super.onClicked(holder, epgLayoutItem);

        EpgLayoutType selectedEpgLayoutType = Settings.epgLayoutType.getEnum(EpgLayoutType.MiniEpg);
        if (selectedEpgLayoutType != epgLayoutItem.getEpgLayoutType()) {
            mItemClickListener.checkEpgLayoutItemRadioButton(epgLayoutItem);
        }
    }

    public static class ViewHolder extends BasePresenter.ViewHolder {
        private final EpgItemLayoutBinding mBinding;

        public ViewHolder(EpgItemLayoutBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
