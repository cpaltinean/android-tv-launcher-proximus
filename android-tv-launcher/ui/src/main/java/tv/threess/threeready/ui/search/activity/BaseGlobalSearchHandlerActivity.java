package tv.threess.threeready.ui.search.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.util.List;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.search.model.GlobalSearchType;
import tv.threess.threeready.ui.generic.activity.MainActivity;

/**
 * Base class for global search handler activities.
 * Forwards the selected search result to the main activity.
 *
 * @author Barabas Attila
 * @since 2017.07.14
 */
public class BaseGlobalSearchHandlerActivity extends Activity {
    public static final String TAG = Log.tag(BaseGlobalSearchHandlerActivity.class);

    public static final String ACTION_SEARCH_RESULT = "tv.threess.threeready.ui.SEARCH_RESULT";
    public static final String EXTRA_SEARCH_RESULT_ID = "EXTRA_SEARCH_RESULT_ID";
    public static final String EXTRA_PARENTAL_RATING = "EXTRA_PARENTAL_RATING";
    public static final String EXTRA_SEARCH_RESULT_TYPE = "EXTRA_SEARCH_RESULT_TYPE";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        handleIntent(intent);
    }

    public void forwardSearchResult(String resultId, ParentalRating rating, GlobalSearchType resultType) {
        Intent intent = new Intent(this, MainActivity.class);

        intent.setAction(ACTION_SEARCH_RESULT);
        intent.putExtra(EXTRA_SEARCH_RESULT_ID, resultId);
        intent.putExtra(EXTRA_PARENTAL_RATING, rating);
        intent.putExtra(EXTRA_SEARCH_RESULT_TYPE, resultType);

        try {
            startActivity(intent);
        } catch (Exception e) {
            Log.e(TAG, "Could not forward search result to main activity.", e);
        }

        finish();
    }

    protected void handleIntent(Intent intent) {
        if (intent.getData() == null) {
            Log.e(TAG, "Result id is not provided.");
            return;
        }

        List<String> pathSegments = intent.getData().getPathSegments();

        if (pathSegments.size() > 3) {
            try {
                String resultId = pathSegments.get(pathSegments.size() - 3);
                ParentalRating rating = ParentalRating.valueOf(Integer.parseInt(
                        pathSegments.get(pathSegments.size() - 2)));
                GlobalSearchType searchType = GlobalSearchType.valueOf(
                        pathSegments.get(pathSegments.size() - 1));

                if (TextUtils.isEmpty(resultId)) {
                    Log.e(TAG, "Result id is empty.");
                    return;
                }

                forwardSearchResult(resultId, rating, searchType);
            } catch (Exception e) {
                Log.e(TAG, "Error while parsing path segments.", e);
            }
        }
    }
}
