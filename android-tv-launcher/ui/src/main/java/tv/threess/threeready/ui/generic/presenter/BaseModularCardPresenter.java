package tv.threess.threeready.ui.generic.presenter;

import android.content.Context;
import android.view.KeyEvent;

import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;

import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleType;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Base presenter class for cards which are displayed in modules like Stripes and Collections.
 *
 * @author Barabas Attila
 * @since 2018.01.12
 */
public abstract class BaseModularCardPresenter<THolder extends BaseCardPresenter.ViewHolder, TItem>
        extends BaseCardPresenter<THolder, TItem> {

    protected final Translator mTranslator = Components.get(Translator.class);
    protected final Navigator mNavigator = Components.get(Navigator.class);
    protected final VodRepository mVodRepository = Components.get(VodRepository.class);

    public BaseModularCardPresenter(Context context) {
        super(context);
    }


    /**
     * Called when the card is displayed and the data needs to be populated.
     * @param moduleData The module config and data where the item belongs.
     *                      Null if the item doesn't belong to a module.
     * @param holder The view holder of the card.
     * @param item The item which needs to be displayed on the card.
     */

    @CallSuper
    public void onBindHolder(ModuleData<?> moduleData, THolder holder, TItem item) {
        super.onBindHolder(holder, item);
        if (moduleData.getModuleConfig().getType() != ModuleType.COLLECTION) {
            setAlignmentOffset(holder, getItemAlignmentOffset());
        }
    }

    @Override
    public final void onBindHolder(THolder holder, TItem item) {
        super.onBindHolder(holder, item);
    }

    /**
     * Called when some properties changed on the card and it needs to be refreshed.
     * @param moduleData The module config and data where the item belongs
     * @param holder The view holder of the displayed card.
     * @param item The item displayed on the card.
     * @param payLoads The list of properties which changed on the card.
     */
    @CallSuper
    public void onBindHolder(ModuleData<?> moduleData, THolder holder, TItem item, List<?> payLoads) {
        super.onBindHolder(holder, item, payLoads);
    }

    @Override
    public final void onBindHolder(THolder holder, TItem item, List<?> payLoads) {
        super.onBindHolder(holder, item, payLoads);
    }

    /**
     * Called when the displayed card is re-used.
     * @param moduleData The module config and data where the item belongs.
     *                   Null if the item doesn't belong to a module.
     * @param holder The view holder of the re-used card.
     */
    @CallSuper
    public void onUnbindHolder(ModuleData<?> moduleData, THolder holder) {
        super.onUnbindHolder(holder);
    }

    @Override
    public final void onUnbindHolder(THolder holder) {
        super.onUnbindHolder(holder);
    }

    /**
     * @param item The item to calculate the width for.
     * @param moduleData The module config and data where the item belongs.
     *                   Null if the item doesn't belong to a module.
     * @return The focused width of the card displaying the item in pixels.
     */
    public abstract int getCardWidth(@Nullable ModuleData<?> moduleData, TItem item);

    @Override
    public final int getCardWidth(TItem item) {
        return getCardWidth(null, item);
    }

    /**
     * @param item The item to calculate the height for.
     * @param moduleData The module config and data where the item belongs.
     *                         Null if the item doesn't belong to a module.
     * @return The focused height of the card displaying the item in pixels.
     */
    public abstract int getCardHeight(@Nullable ModuleData<?> moduleData, TItem item);

    @Override
    public final int getCardHeight(TItem item) {
        return getCardHeight(null, item);
    }

    /**
     * Called when the displayed card losses focus.
     * @param moduleData The module config and data to which the item belongs.
     *                     Null if the item doesn't belong to a module.
     * @param holder The view holder which lost focus.
     * @param item The item which lost focus.
     */
    @CallSuper
    public void onDefaultState(ModuleData<?> moduleData, THolder holder, TItem item) {
        super.onDefaultState(holder, item);
    }

    @Override
    public final void onDefaultState(THolder holder, TItem item) {
        super.onDefaultState(holder, item);
    }


    /**
     * Called when the displayed card is focused.
     * @param moduleData The module config and data to which the item belongs.
     *                     Null if the item doesn't belong to a module.
     * @param holder The view holder which is focused.
     * @param item The item which is focused.
     */
    public void onFocusedState(@Nullable ModuleData<?> moduleData, THolder holder, TItem item) {
        super.onFocusedState(holder, item);
    }

    @Override
    public final void onFocusedState(THolder holder, TItem item) {
        super.onFocusedState(holder, item);
    }

    /**
     * Called when the displayed card is clicked.
     * @param moduleData The module config and data to which the item belongs.
     *                     Null if the item doesn't belong to a module.
     * @param holder The view holder which is clicked.
     * @param item The item which is clicked.
     */
    @CallSuper
    public void onClicked(ModuleData<?> moduleData, THolder holder, TItem item) {
        ModuleConfig module = moduleData.getModuleConfig();
        if (moduleData.getSelectors().isEmpty() &&
                module.getType() != ModuleType.COLLECTION) {
            Log.event(new Event<>(UILogEvent.PageSelection)
                    .addDetail(UILogEvent.Detail.OpenedBy, UILog.OpenedBy.Ok)
                    .addDetail(UILogEvent.Detail.FocusName, module.getReportName())
                    .addDetail(UILogEvent.Detail.FocusId, module.getReportReference()));
        }
    }

    @Override
    public final void onClicked(THolder holder, TItem item) {
        super.onClicked(holder, item);
    }

    /**
     * Called when a key is pressed while the displayed card is focused.
     * @param moduleData The module config and data to which the item belongs.
     *                     Null if the item doesn't belong to a module.
     * @param holder The view holder which is focused.
     * @param item The item which is focused.
     * @param event The key event pressed by the user.
     * @return True if the key even was consumed by the card,
     * false if it the key event needs to be dispatched forward.
     */
    @CallSuper
    public boolean onKeyEvent(ModuleData<?> moduleData, THolder holder, TItem item, KeyEvent event) {
        return super.onKeyEvent(holder, item, event);
    }

    /**
     * Called when the displayed card is long clicked.
     * @param moduleData The module config and data to which the item belongs.
     *                     Null if the item doesn't belong to a module.
     * @param holder The view holder which is long clicked.
     * @param item The item which is long clicked.
     */
    @CallSuper
    public final boolean onLongClicked(ModuleData<?> moduleData, THolder holder, TItem item) {
        return onLongClicked(holder, item);
    }

    @Override
    public final boolean onLongClicked(THolder holder, TItem item) {
        return super.onLongClicked(holder, item);
    }

    /**
     * @return An offset in pixels based on the middle of the window
     * where the card should be positioned when it's focused.
     */
    protected int getItemAlignmentOffset() {
        return 0;
    }

}
