/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceView;
import android.view.View;

/**
 * Custom TV view to allow screen captures on debug builds.
 *
 * @author Barabas Attila
 * @since 2018.12.20
 */
public class TvView extends android.media.tv.TvView {

    public TvView(Context context) {
        super(context);
    }

    public TvView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TvView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void addView(View child) {
        super.addView(child);

        // Unsecured the surface view on debug builds.
        if (child instanceof SurfaceView) {
            ((SurfaceView)child).setSecure(false);
        }

    }
}
