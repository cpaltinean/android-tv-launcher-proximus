package tv.threess.threeready.ui.utils.glide;

import android.graphics.drawable.Drawable;
import android.util.Size;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

/**
 * Fallback class used to show default details image if needed.
 *
 * @author Daniela Toma
 * @since 2020.01.31
 */
public abstract class FallbackTarget extends CustomTarget<Drawable> {

    private final Size mImageSize;
    private final Drawable mDefaultDetailImage;

    public FallbackTarget(Size imageSize, Drawable defaultImage) {
        mImageSize = imageSize;
        mDefaultDetailImage = defaultImage;
    }

    @Override
    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
        int width = resource.getIntrinsicWidth();
        int height = resource.getIntrinsicHeight();

        if (width <= mImageSize.getWidth() && height <= mImageSize.getHeight()) {
            //show default image
            onImageLoaded(mDefaultDetailImage);
            return;
        }

        onImageLoaded(resource);
    }

    @Override
    public void onLoadFailed(@Nullable Drawable errorDrawable) {
        super.onLoadFailed(errorDrawable);

        if (errorDrawable != null) {
            onImageLoaded(errorDrawable);
        }
    }

    protected abstract void onImageLoaded(Drawable drawable);

}