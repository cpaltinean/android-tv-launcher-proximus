/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.pvr.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.annotations.Nullable;
import tv.threess.threeready.api.generic.model.BingeWatchingInfo;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.player.SeriesPlaybackDetailsCallback;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.contract.SeekDirection;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.databinding.PlayerBaseContainerBinding;
import tv.threess.threeready.ui.databinding.RecordingPlayerBinding;
import tv.threess.threeready.ui.generic.player.view.PlayerButtonContainer;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.BaseOrderedIconsContainer;
import tv.threess.threeready.ui.generic.view.BroadcastOrderedIconsContainer;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.generic.view.PlaybackActionView;
import tv.threess.threeready.ui.tv.fragment.BroadcastPlayerFragment;
import tv.threess.threeready.ui.tv.view.ContentMarkersView;
import tv.threess.threeready.ui.tv.view.PlayerButtonView;
import tv.threess.threeready.ui.tv.view.PlayerSubtitleDetailView;
import tv.threess.threeready.ui.tv.view.SeekBarView;

import static tv.threess.threeready.ui.utils.UiUtils.runOnUiThread;

/**
 * Fragment to display the player UI for a recording.
 * When it's shown it will start the recording if not playing already.
 *
 * @author Barabas Attila
 * @since 2018.11.01
 */
public class RecordingPlayerFragment extends BroadcastPlayerFragment implements SeriesPlaybackDetailsCallback {
    public static final String TAG = Log.tag(RecordingPlayerFragment.class);

    private static final String EXTRA_RECORDING_BROADCAST = "EXTRA_RECORDING_BROADCAST";
    private static final String EXTRA_SERIES_RECORDING = "EXTRA_SERIES_RECORDING";

    private RecordingPlayerBinding mBinding;
    private PlayerBaseContainerBinding mBaseContainerBinding;

    public static RecordingPlayerFragment newInstance(IRecording recording, IRecordingSeries series, boolean shouldStartPlayback) {
        Bundle args = new Bundle();
        args.putBoolean(EXTRA_SHOULD_START_PLAYBACK, shouldStartPlayback);
        args.putSerializable(EXTRA_RECORDING_BROADCAST, recording);
        args.putSerializable(EXTRA_STARTED_ITEM, recording);
        args.putSerializable(EXTRA_SERIES_RECORDING, series);

        RecordingPlayerFragment fragment = new RecordingPlayerFragment();
        fragment.setArguments(args);

        return fragment;

    }

    @Override
    protected View getRootView() {
        return mBinding != null ? mBinding.getRoot() : null;
    }

    @Override
    public View inflateRootView() {
        if (mBinding == null) {
            mBinding = RecordingPlayerBinding.inflate(LayoutInflater.from(getContext()));
            mBaseContainerBinding = PlayerBaseContainerBinding.bind(mBinding.getRoot());
        }

        return mBinding.getRoot();
    }

    @Override
    protected BroadcastPlayerViewHolder getViewHolder() {
        return mViewHolder;
    }

    @Override
    protected void updateRecordingStatus(IBroadcast broadcast) {
        super.updateRecordingStatus(broadcast);
        mBinding.buttonsContainer.recordButton.setVisibility(View.GONE);
    }

    @Override
    protected void initializeButtons() {
        super.initializeButtons();

        PlayerButtonContainer buttonContainer = getViewHolder().getButtonContainer();

        mBinding.buttonsContainer.playNextEpisode.setTitle(mTranslator.get(TranslationKey.SCREEN_PLAYER_PLAY_NEXT_BUTTON));
        mBinding.buttonsContainer.playNextEpisode.setOnClickListener(v -> {
            buttonContainer.clearSelection();
            onPlayNextEpisodeClicked();
        });

        mBinding.buttonsContainer.jumpToLive.setVisibility(View.GONE);
        mBinding.buttonsContainer.recordButton.setVisibility(View.GONE);

        mBinding.buttonsContainer.playNextEpisode.setVisibility(getNextEpisode() == null ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onNextEpisodeLoaded() {
        runOnUiThread(() -> mBinding.buttonsContainer.playNextEpisode.setVisibility(getNextEpisode() == null ? View.GONE : View.VISIBLE));
    }

    @Override
    protected void updateSubtitle(IBroadcast broadcast) {
        String subtitle = null;

        // for Episode: Series number, Episode number and Episode Title
        if (!TextUtils.isEmpty(broadcast.getEpisodeTitle())
                || broadcast.getSeasonNumber() != null
                || broadcast.getEpisodeNumber() != null) {

            subtitle = broadcast.getEpisodeTitleWithSeasonEpisode(mTranslator, "");
        }


        List<String> detailsList = new ArrayList<>();
        // for Movie: Year | Genres | Duration
        // Add release year
        String releaseYear = broadcast.getReleaseYear();
        if (!TextUtils.isEmpty(releaseYear)) {
            detailsList.add(releaseYear);
        }

        // Add genres
        String genres = TextUtils.join(", ",
                broadcast.getGenres().stream().limit(3).toArray());
        if (!TextUtils.isEmpty(genres)) {
            detailsList.add(genres);
        }

        // Add recording duration
        if (broadcast instanceof IRecording) {
            IRecording pvrBroadcast = (IRecording) broadcast;
            detailsList.add(LocaleTimeUtils.getDuration(
                    pvrBroadcast.getRecordingEnd() - pvrBroadcast.getRecordingStart(), mTranslator));
        }
        mBinding.detailsLayout.subtitleDetails.updateSubtitle(subtitle, detailsList);
    }

    @Override
    protected void startPlayback(StartAction action) {
        IRecording recording = getSerializableArgument(EXTRA_RECORDING_BROADCAST);
        if (recording == null) {
            Log.e(TAG, "No recording provided to start the playback.");
            return;
        }

        IRecordingSeries series = getSerializableArgument(EXTRA_SERIES_RECORDING);

        Log.d(TAG, "startPlayback() called with: recording = ["
                + recording.getTitle() + "], action = [" + action + "]");

        if (action == StartAction.StartOver) {
            mPlaybackDetailsManager.startRecording(action, recording, series, true, true);
        } else {
            mPlaybackDetailsManager.startRecording(action, recording, series, false, false);
        }
    }

    @Override
    @Nullable
    public IBroadcast getPlaybackData() {
        // Update broadcast from playback manager.
        return mPlaybackDetailsManager.getRecordingPlayerData();
    }

    @Override
    protected boolean isForActivePlayback() {
        IBroadcast playbackData = mPlaybackDetailsManager.getRecordingPlayerData();
        IRecording playerData = getSerializableArgument(EXTRA_RECORDING_BROADCAST);
        if (playbackData == null || playerData == null) {
            return false;
        }

        return (mPlaybackDetailsManager.getPlayerType() == PlaybackType.Recording)
                && Objects.equals(playbackData.getId(), playerData.getId());
    }

    @Override
    protected void updateContentMarkers(IBroadcast broadcast) {
        mBinding.detailsLayout.contentMarker.setVisibility(View.GONE);
    }

    @Nullable
    @Override
    protected Event<?, ?> generatePageOpenEvent() {
        IBroadcast recording = getPlaybackData();
        if (recording == null) {
            return null;
        }

        return ReportUtils.createEvent(UILog.Page.MediaPlayerPage, recording);
    }

    @Override
    public void onSeekingOverflow(SeekDirection direction, long position, boolean progressOverflow, boolean bufferOverflow) {
        if (bufferOverflow) {
            if (mPlaybackDetailsManager.inPlaybackType(PlaybackType.Recording)) {
                IBroadcast recording = getPlaybackData();
                if (recording.isLive() && direction == SeekDirection.FORWARD && !mPlaybackDetailsManager.inPlaybackState(PlaybackState.Paused, PlaybackState.TrickPlayPaused)) {
                    mNotificationManager.showLiveNotification();
                }
            }
        }
    }

    private void onPlayNextEpisodeClicked() {
        IRecording recording = getNextEpisode();
        if (recording != null) {
            mNavigator.showRecordingPlayer(StartAction.ContinuousPlay, recording, null, true);
        }
    }

    private IRecording getNextEpisode() {
        BingeWatchingInfo<?, ?> bingeEpisode = mPlaybackDetailsManager.getNextEpisode();
        if (bingeEpisode != null && bingeEpisode.getNextEpisode() instanceof IRecording) {
            return (IRecording) bingeEpisode.getNextEpisode();
        }
        return null;
    }

    private final BroadcastPlayerViewHolder mViewHolder = new BroadcastPlayerViewHolder() {
        @Override
        public View getUnskippableContainer() {
            return mBinding.unskippableAdvertisement.unskippableAdvertisement;
        }

        @Override
        public SeekBarView getSeekbar() {
            return mBinding.seekBarLayout.seekBar;
        }

        @Override
        public View getSeekBarLayout() {
            return mBinding.seekBarLayout.seekBarLayout;
        }

        @Override
        public ViewGroup getDetailContainer() {
            return mBinding.detailsLayout.detailContainer;
        }

        @Override
        public PlayerButtonContainer getButtonContainer() {
            return mBinding.buttonsContainer.buttonContainer;
        }

        @Override
        public TextView getUnskippableMessage() {
            return mBinding.unskippableAdvertisement.unskippableMessage;
        }

        @Override
        public TextView getUnskippableTime() {
            return mBinding.unskippableAdvertisement.unskippableTime;
        }

        @Override
        public FontTextView getPlayedTimeView() {
            return mBinding.seekBarLayout.playedTime;
        }

        @Override
        public FontTextView getLeftTimeView() {
            return mBinding.seekBarLayout.leftTime;
        }

        @Override
        public PlaybackActionView getPlaybackActionView() {
            return mBaseContainerBinding.playbackAction;
        }

        @Override
        public PlayerButtonView getStartOverButton() {
            return mBinding.buttonsContainer.startOver;
        }

        @Override
        public PlayerButtonView getOptionsButton() {
            return mBinding.buttonsContainer.optionsButton;
        }

        @Override
        public PlayerButtonView getMoreInfoButton() {
            return mBinding.buttonsContainer.moreInfoButton;
        }

        @Override
        public FontTextView getTitleView() {
            return mBinding.detailsLayout.programTitle;
        }

        @Override
        public ImageView getLogoView() {
            return mBaseContainerBinding.logo;
        }

        @Override
        public BaseOrderedIconsContainer getBaseOrderedIconsContainer() {
            return mBinding.detailsLayout.iconsContainer;
        }

        @Override
        public PlayerSubtitleDetailView getSubtitleView() {
            return mBinding.detailsLayout.subtitleDetails;
        }

        @Override
        public PlayerButtonView getJumpToLive() {
            return mBinding.buttonsContainer.jumpToLive;
        }

        @Override
        public PlayerButtonView getRecordButton() {
            return mBinding.buttonsContainer.recordButton;
        }

        @Override
        public ContentMarkersView getContentMarkerView() {
            return mBinding.detailsLayout.contentMarker;
        }

        @Override
        public AppCompatImageView getFavouriteIcon() {
            return mBinding.detailsLayout.favoriteIcon;
        }

        @Override
        public BroadcastOrderedIconsContainer getBroadcastOrderedContainer() {
            return mBinding.detailsLayout.iconsContainer;
        }
    };
}
