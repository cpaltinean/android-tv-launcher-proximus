/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.home.presenter.module;

import android.content.Context;
import android.util.Pair;

import androidx.leanback.widget.Presenter;

import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleDataSourcePaginationParams;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceParams;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;

/**
 * Base presenter class for a collection module.
 * <p>
 * Created by Szilard on 6/22/2017.
 */

public abstract class BaseCollectionModulePresenter<THolder extends Presenter.ViewHolder> extends BaseModulePresenter<THolder> {
    public static final String TAG = Log.tag(BaseCollectionModulePresenter.class);

    private static final int MINIMUM_THRESHOLD_SIZE = 20;

    public BaseCollectionModulePresenter(Context context, DataSourceLoader dataSourceLoader) {
        super(context, dataSourceLoader);
    }

    /**
     * @return The maximum number of items in a page.
     */
    protected int getPageSize(ModuleConfig moduleConfig) {
        ModuleDataSourcePaginationParams paginationParams = moduleConfig.getPagination();
        if (paginationParams != null && paginationParams.getSize() > 0) {
            return paginationParams.getSize();
        }

        // Disable pagination.
        return Integer.MAX_VALUE;
    }

    /**
     * @return The number of items in each page
     * after which the next page should be requested.
     */
    protected int getThreshold(ModuleConfig moduleConfig) {
        ModuleDataSourcePaginationParams paginationParams = moduleConfig.getPagination();
        if (paginationParams != null && paginationParams.getThreshold() > 0) {
            return paginationParams.getThreshold();
        }

        return Math.max(getPageSize(moduleConfig) / 2, MINIMUM_THRESHOLD_SIZE);
    }


    protected abstract int getNextPageStart(ModuleConfig moduleConfig);

    @Override
    public Pair<Integer, Integer> getLimit(ModuleConfig moduleConfig) {
        int[] size = null;
        if (moduleConfig.getDataSource() != null) {
            ModuleDataSourceParams moduleDataSourceParams = moduleConfig.getDataSource().getParams();
            size = moduleDataSourceParams != null ? moduleDataSourceParams.getSize() : null;
        }

        int start = 0;
        int count;
        if (size != null && size.length > 0) {
            // Define page interval
            if (size.length == 1) {
                count = size[0];
            } else {
                start = size[0];
                count = size[1];
            }
        } else {
            start = getNextPageStart(moduleConfig);
            count = getPageSize(moduleConfig);
        }

        return new Pair<>(start, count);
    }

}
