package tv.threess.threeready.ui.generic.presenter;

import java.util.Map;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.vod.model.CurrencyType;
import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.ui.generic.adapter.DetailPageButtonOrder;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Model used for Rent Action Buttons in @{@ActionButtonsPresenter}.
 *
 * @author Daniela Toma
 * @since 2022.04.01
 */
public class RentActionModel extends ActionModel {

    protected final Translator mTranslator = Components.get(Translator.class);
    protected final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);

    private static final String TRANSLATION_PRICE_EUROS = "%price_euros%";
    private static final String TRANSLATION_MINIMUM_PRICE_EUROS = "%minimum_price_euros%";
    private static final String TRANSLATION_CREDIT_AMOUNT = "%credit%";
    private static final String TRANSLATION_CREDIT_TOTAL_AMOUNT = "%total_credit%";

    int mCredit;
    Map<String, IRentOffer> mVariantOfferMap;

    public RentActionModel(DetailPageButtonOrder detailPageButtonOrder,
                       OnItemClickListener onModelClickListener, int credit, Map<String, IRentOffer> variantOfferMap) {
        this(0, 0, detailPageButtonOrder, onModelClickListener, credit, variantOfferMap );
    }

    public RentActionModel(int leftDrawable, int rightDrawable, DetailPageButtonOrder detailPageButtonOrder,
                           OnItemClickListener onModelClickListener, int credit, Map<String, IRentOffer> variantOfferMap) {
        super(leftDrawable, rightDrawable, "", detailPageButtonOrder, onModelClickListener);
        mCredit = credit;
        mVariantOfferMap = variantOfferMap;
        mText = getRentButtonLabel(mCredit, mVariantOfferMap);
    }

    /**
     * Setup rent button label based on the credits, variants and price
     */
    private String getRentButtonLabel(int credit, Map<String, IRentOffer> variantOfferMap) {
        final IVodPrice minimPriceForCredit = IVodPrice.getMinimumPrice(variantOfferMap, CurrencyType.Credit);
        final IVodPrice minimPriceForEuro = IVodPrice.getMinimumPrice(variantOfferMap, CurrencyType.Euro);

        String rentalText = null;
        if (credit > 0 && minimPriceForCredit != null && minimPriceForCredit.getCurrencyAmount() <= credit) {
            rentalText = mTranslator.get(TranslationKey.RENT_WITH_CREDIT_BUTTON)
                    .replace(TRANSLATION_CREDIT_AMOUNT, String.valueOf((int) minimPriceForCredit.getCurrencyAmount()))
                    .replace(TRANSLATION_CREDIT_TOTAL_AMOUNT, String.valueOf(credit));

        } else if (minimPriceForEuro != null && minimPriceForEuro.getCurrencyAmount() > 0) {
            if (variantOfferMap.size() > 1) {
                rentalText = mTranslator.get(TranslationKey.SCREEN_DETAIL_RENT_FROM_PRICE_BUTTON)
                        .replace(TRANSLATION_MINIMUM_PRICE_EUROS, mLocaleSettings.getCurrency() + " "
                                + StringUtils.formatPrice(minimPriceForEuro.getCurrencyAmount()));
            } else {
                rentalText = mTranslator.get(TranslationKey.SCREEN_DETAIL_RENT_FOR_PRICE_BUTTON)
                        .replace(TRANSLATION_PRICE_EUROS, mLocaleSettings.getCurrency() + " "
                                + StringUtils.formatPrice(minimPriceForEuro.getCurrencyAmount()));
            }
        }

        return rentalText;
    }

}
