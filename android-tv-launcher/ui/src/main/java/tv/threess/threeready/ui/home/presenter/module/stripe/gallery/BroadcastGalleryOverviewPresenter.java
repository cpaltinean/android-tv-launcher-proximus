package tv.threess.threeready.ui.home.presenter.module.stripe.gallery;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.leanback.widget.HorizontalGridView;

import com.bumptech.glide.Glide;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.generic.model.PlayOption;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.pvr.model.SeriesRecordingStatus;
import tv.threess.threeready.api.pvr.model.SingleRecordingStatus;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.ui.databinding.GalleryBroadcastOverviewBinding;
import tv.threess.threeready.ui.generic.adapter.DetailPageButtonOrder;
import tv.threess.threeready.ui.generic.model.BroadcastPlayOption;
import tv.threess.threeready.ui.generic.presenter.ActionModel;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.pvr.RecordingActionHelper;
import tv.threess.threeready.ui.utils.ImageTransform;

/**
 * Gallery overview presenter for {@link IBroadcast} items.
 *
 * @author Andor Lukacs
 * @since 12/5/18
 */
public class BroadcastGalleryOverviewPresenter extends BaseGalleryOverviewPresenter<BroadcastGalleryOverviewPresenter.ViewHolder, IBroadcast> {
    private static final String TAG = Log.tag(BroadcastGalleryOverviewPresenter.class);

    private final Translator mTranslator = Components.get(Translator.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final PlaybackDetailsManager mPlaybackManager = Components.get(PlaybackDetailsManager.class);
    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);
    private final RecordingActionHelper mRecordingActionHelper = Components.get(RecordingActionHelper.class);
    private final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);

    public BroadcastGalleryOverviewPresenter(Context context, ModuleData moduleData) {
        super(context, moduleData);
    }

    @Override
    public ViewHolder createViewHolder(ViewGroup parent) {
        Log.d(TAG, "createViewHolder() called with: parent = [" + parent + "]");
        GalleryBroadcastOverviewBinding binding = GalleryBroadcastOverviewBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindHolder(ViewHolder holder, IBroadcast broadcast) {
        super.onBindHolder(holder, broadcast);
        Log.d(TAG, "onBindHolder() called with: holder = [" + holder + "], broadcast = [" + broadcast + "]");
        updateProviderLogo(holder, broadcast);
        updateMarkersRow(holder, broadcast);
        updateRecordingStatus(holder, broadcast);
        updateInfoRow(holder, broadcast, holder.mBinding.iconsContainer.getRecordingStatus());
        updateButtons(holder, broadcast, null);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        Log.d(TAG, "onUnbindHolder() called with: holder = [" + holder + "]");
        super.onUnbindHolder(holder);

        holder.mBinding.iconsContainer.hideRecordingStatus();

        Glide.with(mContext).clear(holder.mBinding.providerLogo);
        RxUtils.disposeSilently(holder.mRecStatusDisposable);
    }

    @Override
    protected void onParentalRatingChanged(ViewHolder holder, IBroadcast item) {
        super.onParentalRatingChanged(holder, item);

        updateInfoRow(holder, item, holder.mBinding.iconsContainer.getRecordingStatus());
    }

    private void updateProviderLogo(ViewHolder holder, IBroadcast broadcast) {
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        Glide.with(mContext).clear(holder.mBinding.providerLogo);
        Glide.with(mContext)
                .load(channel)
                .apply(ImageTransform.REQUEST_OPTIONS)
                .into(holder.mBinding.providerLogo);
    }

    private void updateMarkersRow(BroadcastGalleryOverviewPresenter.ViewHolder holder, IBroadcast broadcast) {
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());

        // Display content marker
        holder.mBinding.contentMarker.showMarkers(broadcast);
        holder.mBinding.iconsContainer.showParentalRating(broadcast.getParentalRating());
        if (broadcast.canReplay(channel)) {
            holder.mBinding.iconsContainer.showReplay();
        } else {
            holder.mBinding.iconsContainer.hideReplay();
        }

        updateDescriptiveAudioIcon(holder, broadcast);
        updateDescriptiveSubtitleIcon(holder, broadcast);

        Log.d(TAG, "Is dolby available : " + mPlaybackManager.isDolbyAvailable());
        if (mPlaybackManager.isBroadcastPlaying(broadcast) && mPlaybackManager.isDolbyAvailable()) {
            holder.mBinding.iconsContainer.showDolby();
        } else {
            holder.mBinding.iconsContainer.hideDolby();
        }
    }

    /**
     * Displays descriptive audio icon.
     */
    private void updateDescriptiveAudioIcon(ViewHolder holder, IBroadcast broadcast) {
        if (broadcast.hasDescriptiveAudio()) {
            holder.mBinding.iconsContainer.showDescriptiveAudio();
        } else {
            holder.mBinding.iconsContainer.hideDescriptiveAudio();
        }
    }

    /**
     * Displays descriptive subtitle icon.
     */
    private void updateDescriptiveSubtitleIcon(ViewHolder holder, IBroadcast broadcast) {
        if (broadcast.hasDescriptiveSubtitle()) {
            holder.mBinding.iconsContainer.showDescriptiveSubtitle();
        } else {
            holder.mBinding.iconsContainer.hideDescriptiveSubtitle();
        }
    }

    private void updateRecordingStatus(BroadcastGalleryOverviewPresenter.ViewHolder holder, IBroadcast broadcast) {
        holder.mBinding.iconsContainer.hideRecordingStatus();
        RxUtils.disposeSilently(holder.mRecStatusDisposable);
        holder.mRecStatusDisposable = mPvrRepository.getRecordingStatus(broadcast)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recordingStatus -> {
                    holder.mBinding.iconsContainer.showRecordingStatus(recordingStatus);
                    updateButtons(holder, broadcast, recordingStatus);
                    updateInfoRow(holder, broadcast, recordingStatus);
                }, throwable -> {
                    Log.e(TAG, "Couldn't get the recording status.", throwable);
                    holder.mBinding.iconsContainer.hideRecordingStatus();
                });
    }

    private void updateInfoRow(BroadcastGalleryOverviewPresenter.ViewHolder holder, IBroadcast broadcast, RecordingStatus recordingStatus) {
        List<String> infoList = new ArrayList<>();

        infoList.add(LocaleTimeUtils.getDate(broadcast, mTranslator, mLocaleSettings));

        if (!mParentalControlManager.isRestricted(broadcast)) {
            infoList.add(TextUtils.join(", ", broadcast.getGenres().stream().limit(3).toArray()));
        }
        if (broadcast.getReleaseYear() != null && !broadcast.getReleaseYear().isEmpty()) {
            infoList.add(broadcast.getReleaseYear());
        }
        if (recordingStatus != null && recordingStatus.getRecording() != null) {
            TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
            String availability = LocaleTimeUtils.getBroadcastAvailabilityTime(
                    channel, broadcast, recordingStatus.getRecording(), mTranslator);
            infoList.add(availability);

        }
        holder.mBinding.infoRow.setText(TextUtils.join(TimeUtils.DIVIDER, infoList));
    }

    protected void updateButtons(BroadcastGalleryOverviewPresenter.ViewHolder holder, IBroadcast broadcast,
                                 @Nullable RecordingStatus recordingStatus) {

        SingleRecordingStatus singleRecordingStatus = recordingStatus == null ?
                null : recordingStatus.getSingleRecordingStatus();
        SeriesRecordingStatus seriesRecordingStatus = recordingStatus == null ?
                null : recordingStatus.getSeriesRecordingStatus();

        List<ActionModel> actions = new ArrayList<>();

        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        boolean isChannelSubscribed = channel != null && channel.isUserTvSubscribedChannel();

        // Display playback options.
        List<PlayOption> playOptions = BroadcastPlayOption.generatePlayOptions(
                channel, broadcast, null, recordingStatus, false, false);

        if (playOptions.size() > 0) {
            // Start directly the first play option.
            actions.add(new ActionModel(
                    mTranslator.get(TranslationKey.SCREEN_DETAIL_PLAY_BUTTON), DetailPageButtonOrder.Watch, () -> {
                if (!isChannelSubscribed) {
                    mRecordingActionHelper.showChannelNotEntitledDialog();
                    return;
                }

                if (playOptions.size() == 1) {
                    mNavigator.showPlayer(playOptions.get(0), false);
                } else {
                    mNavigator.showPlayOptionsDialog(playOptions, broadcast);
                }
            }
            ));
        }

        if (recordingStatus != null) {
            // Display delete for completed recording.
            if (singleRecordingStatus == SingleRecordingStatus.COMPLETED) {
                actions.add(new ActionModel(
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_DELETE_BUTTON), DetailPageButtonOrder.Delete,
                        () -> mRecordingActionHelper.deleteRecording(recordingStatus.getRecording(), true, false)
                ));

                // Add reschedule possibility for live.
                if (broadcast.isLive() && isChannelSubscribed && mPvrRepository.isPVREnabled(broadcast)) {
                    actions.add(new ActionModel(
                            mTranslator.get(TranslationKey.SCREEN_DETAIL_RECORD_BUTTON), DetailPageButtonOrder.Record,
                            () -> mRecordingActionHelper.scheduleSingleRecording(
                                    broadcast, recordingStatus, true, false)
                    ));
                }

                // Display cancel for scheduled and ongoing recording.
            } else if (singleRecordingStatus == SingleRecordingStatus.SCHEDULED
                    || singleRecordingStatus == SingleRecordingStatus.RECORDING) {
                actions.add(new ActionModel(
                        0, 0,
                        mTranslator.get(singleRecordingStatus == SingleRecordingStatus.RECORDING
                                ? TranslationKey.SCREEN_DETAIL_CANCEL_ONGOING_BUTTON
                                : TranslationKey.SCREEN_DETAIL_CANCEL_BUTTON), DetailPageButtonOrder.Cancel,
                        () -> mRecordingActionHelper.cancelSingleRecording(broadcast, recordingStatus, true, false)));

                // Display record for live and future broadcasts.
            } else if (mPvrRepository.isPVREnabled(broadcast) && isChannelSubscribed && (broadcast.isLive() || broadcast.isFuture())) {
                actions.add(new ActionModel(
                        0, 0,
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_RECORD_BUTTON), DetailPageButtonOrder.Record,
                        () -> mRecordingActionHelper.scheduleSingleRecording(broadcast, recordingStatus, true, false)));
            }

            // Series schedule.  Show 'Cancel Series' button.
            if (seriesRecordingStatus == SeriesRecordingStatus.SCHEDULED) {
                actions.add(new ActionModel(
                        0, 0,
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_CANCEL_SERIES_BUTTON), DetailPageButtonOrder.Cancel,
                        () -> mRecordingActionHelper.cancelSeriesRecording(broadcast, true, false)));

                // Series not scheduled. Show schedule series button.
            } else if (mPvrRepository.isPVREnabled(broadcast) && isChannelSubscribed && !broadcast.isPast()) {
                actions.add(new ActionModel(
                        0, 0,
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_RECORD_SERIES_BUTTON), DetailPageButtonOrder.Record,
                        () -> mRecordingActionHelper.scheduleSeriesRecording(broadcast, recordingStatus, true, false)));
            }
        }

        // Show more info.
        actions.add(new ActionModel(
                0, 0,
                mTranslator.get(TranslationKey.SCREEN_DETAIL_MORE_INFO_BUTTON),
                DetailPageButtonOrder.MoreInfo,
                () -> mNavigator.showProgramDetails(broadcast)));

        for (ActionModel actionModel : actions) {
            actionModel.setContentDescription(
                    new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_GALLERY_TV_ITEM))
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BUTTON_NAME, actionModel.getText())
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_ASSET, broadcast.getTitle())
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_CHANNEL_NAME, channel != null ? channel.getName() : "")
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_AGE_RATING, broadcast.getParentalRating().name())
                            .toString());
        }

        updateActionButtons(holder, broadcast, actions);
    }

    static class ViewHolder extends BaseGalleryOverviewPresenter.ViewHolder {

        private final GalleryBroadcastOverviewBinding mBinding;

        @Override
        @NotNull
        protected FontTextView getTitleView() {
            return mBinding.galleryTitle;
        }

        @Override
        @NotNull
        protected FontTextView getInfoRow() {
            return mBinding.infoRow;
        }

        @Override
        @NotNull
        protected ImageView getCoverImage() {
            return mBinding.imageCover;
        }

        @Override
        @NotNull
        protected HorizontalGridView getActionsGridView() {
            return mBinding.actionGrid;
        }

        Disposable mRecStatusDisposable;

        ViewHolder(GalleryBroadcastOverviewBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
