package tv.threess.threeready.ui.generic.presenter;

import android.content.Context;

import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.home.ModuleData;

/**
 * Card presenter which only allows click actions when the content is unlocked.
 *
 * @author Solyom Zsolt
 * @since 2020.02.26
 */

public abstract class LockedCardPresenter<THolder extends CoverCardPresenter.ViewHolder, TItem extends IBaseContentItem>
        extends BaseModularCardPresenter<THolder, TItem> {

    public LockedCardPresenter(Context context) {
        super(context);
    }

    @Override
    public void onClicked(ModuleData<?> moduleData, THolder holder, TItem item) {
        super.onClicked(moduleData, holder, item);
        if (mParentalControlManager.isRestricted(item)) {
            // Restricted content. Needs to unblock parental control.
            mNavigator.showParentalControlUnblockDialog();
        } else {
            openDetailPage(holder, item);
        }
    }

    /**
     * Called on click event in case if the selected item is not restricted.
     */
    protected abstract void openDetailPage(THolder holder, TItem item);

}
