/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.tv.presenter.broadcast;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.tv.model.IBroadcast;

/**
 * Abstract base presenter of landscape program card.
 *
 * @author Barabas Attila
 * @since 2017.05.15
 */
abstract class BaseBroadcastLandscapeCardPresenter extends BroadcastCardPresenter<BaseBroadcastLandscapeCardPresenter.ViewHolder, IBroadcast> {

    private final Handler mContentMarkerUpdateHandler;

    BaseBroadcastLandscapeCardPresenter(Context context) {
        super(context);
        mContentMarkerUpdateHandler = new Handler();
    }

    @Override
    public void onBindHolder(ModuleData<?> moduleData, ViewHolder holder, IBroadcast broadcast) {
        super.onBindHolder(moduleData, holder, broadcast);
        mContentMarkerUpdateHandler.removeCallbacks(holder.mContentMarkerUpdateRunnable);

        if (broadcast.getEnd() > System.currentTimeMillis()){
            long nextContentMarkerDelay = broadcast.getEnd() - System.currentTimeMillis();
            holder.mContentMarkerUpdateRunnable = () -> updateContentMarkers(holder, broadcast);
            mContentMarkerUpdateHandler.postDelayed(holder.mContentMarkerUpdateRunnable, nextContentMarkerDelay);
        }
    }

    @Override
    public void onUnbindHolder(ModuleData<?> moduleData, ViewHolder holder) {
        super.onUnbindHolder(moduleData, holder);
        mContentMarkerUpdateHandler.removeCallbacks(holder.mContentMarkerUpdateRunnable);
    }

    @Override
    public void onDefaultState(ModuleData<?> moduleData, ViewHolder holder, final IBroadcast broadcast) {
        super.onDefaultState(moduleData, holder, broadcast);

        updateParentalRatingIcon(holder, broadcast);

        // Stop automatic text scrolling.
        holder.getTitleView().setSelected(false);
        holder.getTitleView().setEllipsize(TextUtils.TruncateAt.END);
    }

    @Override
    public void onFocusedState(ModuleData<?> moduleData, ViewHolder holder, IBroadcast broadcast) {
        super.onFocusedState(moduleData, holder, broadcast);

        updateParentalRatingIcon(holder, broadcast);

        // Start automatic text scrolling.
        holder.getTitleView().setSelected(true);
        holder.getTitleView().setEllipsize(TextUtils.TruncateAt.MARQUEE);
    }

    protected static abstract class ViewHolder extends BroadcastCardPresenter.ViewHolder {

        Runnable mContentMarkerUpdateRunnable;

        public ViewHolder(View view) {
            super(view);
        }

    }
}
