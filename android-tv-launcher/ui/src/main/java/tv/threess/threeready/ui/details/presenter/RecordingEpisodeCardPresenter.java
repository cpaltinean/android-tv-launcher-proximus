package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.view.View;

import java.util.List;

import tv.threess.threeready.api.generic.model.PlayOption;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.details.DetailOpenedFrom;
import tv.threess.threeready.ui.generic.model.RecordingPlayOption;

/**
 * Recording episode card presenter on the series details page.
 *
 * @author Barabas Attila
 * @since 2018.11.22
 */
public class RecordingEpisodeCardPresenter extends BroadcastEpisodeCardPresenter<IRecording> {

    private final SeriesProvider<IRecordingSeries> mSeriesProvider;

    public RecordingEpisodeCardPresenter(Context context, SeriesProvider<IRecordingSeries> seriesProvider, DetailOpenedFrom openedFrom) {
        super(context, openedFrom);
        mSeriesProvider = seriesProvider;
    }

    @Override
    public void onBindHolder(ViewHolder holder, IRecording broadcast) {
        super.onBindHolder(holder, broadcast);

        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        List<PlayOption> playOptions = RecordingPlayOption.generateRecordingPlayOptions(
                channel, broadcast, mSeriesProvider.getSeries(), mOpenedFrom == DetailOpenedFrom.Recording);

        boolean iconIsVisible = playOptions.size() == 0 && broadcast.isFailedRecording();

        holder.mBinding.notPlayableIcon.setVisibility(iconIsVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClicked(ViewHolder holder, IRecording broadcast) {
        mNavigator.showRecordingEpisodeDetailsDialog(broadcast, mSeriesProvider.getSeries(),
                holder.mBookmark, holder.mBinding.iconsContainer.getRecordingStatus(), mOpenedFrom);
    }

    @Override
    public void onFocusedState(ViewHolder holder, IRecording contentItem) {
        super.onFocusedState(holder, contentItem);
        holder.mBinding.noFocusOverlay.setVisibility(View.GONE);
    }

    @Override
    public void onDefaultState(ViewHolder holder, IRecording iRecording) {
        super.onDefaultState(holder, iRecording);
        holder.mBinding.noFocusOverlay.setVisibility(View.VISIBLE);
    }
}
