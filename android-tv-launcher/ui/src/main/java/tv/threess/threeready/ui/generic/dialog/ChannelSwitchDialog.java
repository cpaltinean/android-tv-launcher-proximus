package tv.threess.threeready.ui.generic.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.netflix.model.SourceType;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.ChannelSwitchDialogBinding;
import tv.threess.threeready.ui.epg.fragment.EpgFragment;
import tv.threess.threeready.ui.epggrid.EpgGridFragment;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.player.fragment.PlayerFragment;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.startup.StartupFlow;

/**
 * Dialog to manage channel switch number keys, will show on top of all views
 *
 * @author hunormadaras
 * @since 2019-06-06
 */
public class ChannelSwitchDialog extends BaseDialogFragment {
    private static final String TAG = Log.tag(ChannelSwitchDialog.class);

    private static final String EXTRA_DATA_CHANNEL_NUMBER = "EXTRA_DATA_CHANNEL_NUMBER";
    private static final String NETFLIX_CHANNEL_NAME = "Netflix";

    private static final String ZEROS_REGEX = "0*";

    private static final long NEXT_INPUT_WAIT_TIMEOUT = TimeUnit.SECONDS.toMillis(1);

    private final TvRepository mTvRepository = Components.get(TvRepository.class);
    private final Navigator mNavigator = Components.get(Navigator.class);

    private int mMaxLength;

    private Disposable mDisposable;
    private String mChannelNumberEntered;
    private ChannelSwitchDialogBinding mBinding;

    public static ChannelSwitchDialog newInstance(String channelNumber) {
        ChannelSwitchDialog dialog = new ChannelSwitchDialog();

        Bundle args = new Bundle();
        args.putString(EXTRA_DATA_CHANNEL_NUMBER, channelNumber);
        dialog.setArguments(args);

        return dialog;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mChannelNumberEntered = getStringArgument(EXTRA_DATA_CHANNEL_NUMBER);
    }

    @Override
    protected Dialog createDialog() {
        mMaxLength = getResources().getInteger(R.integer.channel_switch_max_length);
        Dialog dialog = super.createDialog();

        // Not focusable. The events will be dispatched from the activity.
        Window window = dialog.getWindow();
        if (window != null) {
            window.addFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        }

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = ChannelSwitchDialogBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mBinding.switchText.setText("");
        mBinding.switchText.setVisibility(View.GONE);
        proceedNumericalInput(mChannelNumberEntered);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (getView() != null) {
            getView().removeCallbacks(mSwitchChannelRunnable);
        }
        RxUtils.disposeSilently(mDisposable);
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        return handleNumericalInput(getActivity(), event);
    }

    /**
     * Handles numerical key presses in side of the dialog, when it is already shown
     *
     * @param activity - activity, not sure that the direct context is activity
     * @param keyEvent - the key event triggered by the user
     */
    public boolean handleNumericalInput(Activity activity, KeyEvent keyEvent) {
        Log.d(TAG, "handleNumericalInput() called with: keyEvent = [" + keyEvent + "]");
        if (shouldProceedNumericInput(activity, mStartupFlow, keyEvent)) {
            proceedNumericalInput(String.valueOf(keyEvent.getNumber()));
            return true;
        }
        return false;
    }

    /**
     * Handles numeric input, channel number action. Can be called from {@link #onResume()} when dialog
     * is shown or from {@link #handleNumericalInput(Activity, KeyEvent)} when the input action
     * is handled from inside of the dialog
     *
     * @param number channel number
     */
    private void proceedNumericalInput(String number) {
        mBinding.switchText.append(number);
        mBinding.switchText.setVisibility(View.VISIBLE);
        mChannelNumberEntered = mBinding.switchText.getText().toString();
        if (getView() != null) {
            getView().removeCallbacks(mSwitchChannelRunnable);
            getView().postDelayed(mSwitchChannelRunnable,
                    mChannelNumberEntered.length() == mMaxLength ? 0 : NEXT_INPUT_WAIT_TIMEOUT);
        }
    }

    /**
     * Method used to get the data from the DB based on the channel number entered by the user and
     * triggers the switching of the channel
     *
     * @param channelNumber to which channel should be switch
     */
    private void switchToChannel(String channelNumber) {
        RxUtils.disposeSilently(mDisposable);

        if (channelNumber.matches(ZEROS_REGEX)) {
            mDisposable = mTvRepository.getFirstFavorite()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(favoriteChannel ->
                    {
                        if (favoriteChannel == null) {
                            mDisposable = mTvRepository.getChannelByNumber(Integer.parseInt(channelNumber))
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(flatChannel ->
                                            switchChannel(flatChannel, false, Integer.parseInt(channelNumber) == flatChannel.getNumber()));
                        } else {
                            boolean isNetflixChannel = (Integer.parseInt(channelNumber) == 0 && favoriteChannel.getId().equalsIgnoreCase(NETFLIX_CHANNEL_NAME));
                            boolean isFavoriteChannel = Integer.parseInt(channelNumber) == favoriteChannel.getNumber();
                            switchChannel(favoriteChannel, true, (isNetflixChannel || isFavoriteChannel));
                        }
                    });
        } else {
            mDisposable = mTvRepository.getChannelByNumber(Integer.parseInt(channelNumber))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(flatChannel -> switchChannel(flatChannel, false, Integer.parseInt(channelNumber) == flatChannel.getNumber()));
        }

        mBinding.switchText.setText("");
        mBinding.switchText.setVisibility(View.GONE);
    }

    private void switchChannel(TvChannel channel, boolean isFavorite, boolean isMatchingChannel) {
        if (channel == null) {
            Log.w(TAG, "No channel to switch to.");
            return;
        }

        dismissDialog();
        Log.d(TAG, "onSwitchChannel() called with: channelId = [" + channel.getId() + "], isFavorite = [" + isFavorite + "]");

        BaseFragment currentFragment = mNavigator.getContentFragment();

        // Dispatch channel selection for EPG.
        if (currentFragment instanceof EpgFragment
                && mNavigator.isContentOverlayDisplayed()) {
            EpgFragment epgFragment = (EpgFragment) currentFragment;
            epgFragment.onChannelSwitch(channel.getId(), isFavorite);
            return;
        }

        // Dispatch channel selection for EPG grid.
        if (currentFragment instanceof EpgGridFragment
                && mNavigator.isContentOverlayDisplayed()) {
            EpgGridFragment epgGridFragment = (EpgGridFragment) currentFragment;
            epgGridFragment.onChannelSwitch(channel.getId(), isFavorite);
            return;
        }

        // Dispatch channel selection for player fragment.
        PlayerFragment playerFragment = mNavigator.getPlayerFragment();
        if (playerFragment != null) {
            switch (channel.getType()) {
                case APP:
                    if(isMatchingChannel) {
                        mNavigator.checkSubscriptionAndOpenApp(StartAction.BtnNumber, channel, SourceType.DirectChannel);
                    } else {
                        playerFragment.goToChannel(StartAction.BtnNumber, channel.getId(), isFavorite);
                    }
                    break;
                case REGIONAL:
                case TV:
                case RADIO:
                default:
                    Log.d(TAG, "Epg is not shown so we can tune to channel " + channel.getId());
                    playerFragment.goToChannel(StartAction.BtnNumber, channel.getId(), isFavorite);
                    break;
            }
        }
    }

    private final Runnable mSwitchChannelRunnable = new Runnable() {
        @Override
        public void run() {
            switchToChannel(mChannelNumberEntered);
        }
    };

    private void dismissDialog() {
        mNavigator.clearAllDialogs();
        dismiss();
    }

    /**
     * checks if numeric input should be handled, used at {@link #handleNumericalInput(Activity, KeyEvent)}
     * and {@link #handleExternalNumericalInput(Activity, StartupFlow, Navigator, KeyEvent)}
     *
     * @param activity     used activity
     * @param mStartupFlow for check if startup finished
     * @param keyEvent     event for numeric input
     * @return true if input should be handled
     */
    private static boolean shouldProceedNumericInput(Activity activity, StartupFlow mStartupFlow, KeyEvent keyEvent) {
        return activity != null
                && !(activity.getCurrentFocus() instanceof EditText)
                && mStartupFlow.isStartupFinished()
                && isNumericalInput(keyEvent)
                && !keyEvent.isCanceled();
    }

    /**
     * Handles numerical key presses from outside of the dialog, in this case dialog will be shown
     * with the corresponding number. In case of the current dialog is @{@link PinDialog} then
     * has to be blocked
     *
     * @param activity         activity, not sure that the direct context is activity
     * @param mStartupFlow     needed to check if startup flow is finished
     * @param navigator        needed to show @{@link ChannelSwitchDialog}
     * @param keyEvent         the key event triggered by the user
     */
    public static boolean handleExternalNumericalInput(Activity activity, StartupFlow mStartupFlow, Navigator navigator, KeyEvent keyEvent) {
        Log.d(TAG, "handleExternalNumericalInput() called with: keyEvent = [" + keyEvent + "]");

        if (shouldProceedNumericInput(activity, mStartupFlow, keyEvent))  {
            BaseDialogFragment topDialog = navigator.getDialogFragment();
            if (isInappropriateDialogDisplayed(topDialog)) {
                navigator.showDialogOnTop(ChannelSwitchDialog.newInstance(String.valueOf(keyEvent.getNumber())));
                return true;
            } else { //check if we should still handle it
                if (topDialog instanceof ChannelSwitchDialog) {
                    ((ChannelSwitchDialog) topDialog).proceedNumericalInput(String.valueOf(keyEvent.getNumber()));
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns true if there are no inappropriate dialogs displayed on top.
     */
    private static boolean isInappropriateDialogDisplayed(BaseDialogFragment topDialog) {
        return !(topDialog instanceof BasePinDialog) &&
                !(topDialog instanceof ReplayPlusOfferDialog) &&
                !(topDialog instanceof ChannelSwitchDialog) &&
                !(topDialog instanceof ReplayBackToLiveDialog);
    }

    /**
     * Checks whether the key event triggered is numerical
     *
     * @param keyEvent the key event to check whether it's numerical or not
     * @return whether the input key is numerical or not
     */
    public static boolean isNumericalInput(KeyEvent keyEvent) {
        return (keyEvent.getKeyCode() >= KeyEvent.KEYCODE_0 && keyEvent.getKeyCode() <= KeyEvent.KEYCODE_9);
    }

}
