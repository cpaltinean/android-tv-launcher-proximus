package tv.threess.threeready.ui.settings.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.settings.model.MainSettingsItem;

/**
 * Fragment to display Proximus settings.
 *
 * @author Andrei Teslovan
 * @since 2018.09.11
 */
public class SettingsFragment extends BaseSettingsDialogFragment<Object> {
    private static final String ITEM_TYPE = "ITEM_TYPE";

    private MainSettingsItem.MainItemType mItemType;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    public static SettingsFragment newInstance(MainSettingsItem.MainItemType type) {
        SettingsFragment settingsFragment = new SettingsFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(ITEM_TYPE, type);
        settingsFragment.setArguments(bundle);

        return settingsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mItemType = getSerializableArgument(ITEM_TYPE);
        return view;
    }

    public void initView() {
        View rootView = getRootView();
        if (rootView == null) {
            return;
        }

        List<Object> items = new ArrayList<>();


        items.add(mTranslator.get(TranslationKey.SETTINGS_MAIN_ITEM_TV_SETTINGS));

        items.add(new MainSettingsItem(MainSettingsItem.MainItemType.PARENTAL_CONTROL,
                mTranslator.get(TranslationKey.SETTINGS_PARENTAL_CONTROL),
                ContextCompat.getDrawable(rootView.getContext(), R.drawable.ico_parental_control)
        ));

        items.add(new MainSettingsItem(MainSettingsItem.MainItemType.VOD_PURCHASE_HISTORY,
                mTranslator.get(TranslationKey.SETTINGS_VOD_PURCHASE),
                ContextCompat.getDrawable(rootView.getContext(), R.drawable.ico_vod_purchase_history)));

        items.add(new MainSettingsItem(MainSettingsItem.MainItemType.PRIVACY,
                mTranslator.get(TranslationKey.SETTINGS_PRIVACY),
                ContextCompat.getDrawable(rootView.getContext(), R.drawable.ico_privacy)));

        items.add(new MainSettingsItem(MainSettingsItem.MainItemType.LANGUAGE,
                mTranslator.get(TranslationKey.SETTINGS_LANGUAGE),
                ContextCompat.getDrawable(rootView.getContext(), R.drawable.ico_language_preferences)
        ));

        items.add(new MainSettingsItem(MainSettingsItem.MainItemType.RENUMBERING,
                mTranslator.get(TranslationKey.SETTINGS_CHANNEL_REORDERING),
                ContextCompat.getDrawable(rootView.getContext(), R.drawable.ico_channel_renumbering)
        ));

        items.add(new MainSettingsItem(MainSettingsItem.MainItemType.EPG_LAYOUT,
                mTranslator.get(TranslationKey.SETTINGS_EPG_LAYOUT),
                ContextCompat.getDrawable(rootView.getContext(), R.drawable.ico_epg_layout)
        ));

        items.add(new MainSettingsItem(MainSettingsItem.MainItemType.HINTS,
                mTranslator.get(TranslationKey.SETTINGS_HINTS),
                ContextCompat.getDrawable(rootView.getContext(), R.drawable.ico_tips)
        ));

        items.add(new MainSettingsItem(MainSettingsItem.MainItemType.RECORDINGS,
                mTranslator.get(TranslationKey.SETTINGS_RECORDINGS),
                ContextCompat.getDrawable(rootView.getContext(), R.drawable.ico_record)
        ));

        items.add(new MainSettingsItem(MainSettingsItem.MainItemType.SUPPORT,
                mTranslator.get(TranslationKey.APP_SETTINGS_SUPPORT),
                ContextCompat.getDrawable(rootView.getContext(), R.drawable.ico_system_information)));

        items.add(mTranslator.get(TranslationKey.SETTINGS_MAIN_ITEM_DEVICE_SETTINGS));

        items.add(new MainSettingsItem(MainSettingsItem.MainItemType.DEVICE_SETTINGS,
                mTranslator.get(TranslationKey.SETTINGS_MAIN_ITEM_DEVICE_SETTINGS),
                ContextCompat.getDrawable(rootView.getContext(), R.drawable.ico_device_settings)
        ));

        setItems(items);
    }

    @Override
    protected void setItems(Collection<Object> values) {
        super.setItems(values);
        if (mItemType == null) {
            return;
        }

        // Default selection.
        int position = 0;
        for (Object item : values) {
            if (item instanceof MainSettingsItem && ((MainSettingsItem) item).getType() == mItemType) {
                getVerticalGridView().setSelectedPosition(position);
                break;
            }
            position++;
        }
    }

    @Override
    protected void setTitle() {
        getTitleView().setText(mTranslator.get(TranslationKey.SETTINGS_SECTION_TITLE));
    }

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        Event<UILogEvent, UILogEvent.Detail> event = new Event<>(UILogEvent.PageOpen);
        event.addDetail(UILogEvent.Detail.Page, UILog.Page.GeneralSettings);

        return event;
    }
}
