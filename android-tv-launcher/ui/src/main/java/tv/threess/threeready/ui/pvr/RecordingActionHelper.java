/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.pvr;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.pvr.model.SeriesRecordingStatus;
import tv.threess.threeready.api.pvr.model.SingleRecordingStatus;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.ITvSeries;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.generic.dialog.ActionBarDialog;
import tv.threess.threeready.ui.generic.dialog.AlertDialog;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.notification.BaseNotification;
import tv.threess.threeready.ui.generic.notification.Notification;
import tv.threess.threeready.ui.generic.notification.NotificationManager;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Utility class to trigger recording related action.
 * It will to choose to correct action based on the recording status
 * and will handle all the confirmation dialogs and notifications.
 *
 * @author Barabas Attila
 * @since 2018.10.26
 */
public class RecordingActionHelper implements Component {
    private static final String TAG = Log.tag(RecordingActionHelper.class);

    private static final String CLEANUP_DATE_PLACEHOLDER = "%recordings_cleanup_date%";
    private static final String TIME_LEFT_PLACEHOLDER = "%time_left%";
    private static final long STORAGE_FULL_DISPLAY_PERIOD = TimeUnit.DAYS.toMillis(2);

    private long mStorageFullLastShownTime;

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final NotificationManager mNotificationManager = Components.get(NotificationManager.class);
    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);

    private final Translator mTranslator = Components.get(Translator.class);

    private final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);
    private final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);

    private final Map<String, Disposable> mAssetDisposableMap = new HashMap<>();
    private Disposable mRecordingStorageDisposable;

    public void onStart() {

    }

    public void onStop() {
        // Dispose all RX observer.
        for (Disposable disposable : mAssetDisposableMap.values()) {
            RxUtils.disposeSilently(disposable);
        }
        mAssetDisposableMap.clear();

        RxUtils.disposeSilently(mRecordingStorageDisposable);
    }

    /**
     * Handle the recording action when the user presses the recording button on UI.
     *
     * @param broadcast              The broadcast for which the recording action should be triggered.
     * @param currentRecordingStatus The current recordings status of the broadcast.
     *                               If null the status will be requested.
     */
    public void handleRecButtonAction(IBroadcast broadcast, @Nullable RecordingStatus currentRecordingStatus) {
        if (broadcast == null) {
            // Nothing to do.
            return;
        }

        Log.d(TAG, "Handle REC button for "
                + broadcast.getId() + " current status " + currentRecordingStatus);

        if (isActionInProgress(broadcast)) {
            Log.w(TAG, "Ignore REC action. A REC action is already in progress.");
            return;
        }

        // Get recording status if not available.
        if (currentRecordingStatus == null) {
            Disposable disposable = mPvrRepository.getRecordingStatus(broadcast)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .firstOrError()
                    .subscribe(recordingStatus -> {
                        mAssetDisposableMap.remove(broadcast.getId());
                        handleRecButtonAction(broadcast, recordingStatus);
                    });
            mAssetDisposableMap.put(broadcast.getId(), disposable);
            return;
        }

        // Episode case.
        if (broadcast.isEpisode()) {
            handleEpisodeRecButtonAction(broadcast, currentRecordingStatus);
            // Movie case.
        } else {
            handleMovieRecButtonAction(broadcast, currentRecordingStatus);
        }
    }

    /**
     * Handle the recording button press on the UI from a movie.
     *
     * @param broadcast              The movie for which a recording action should be triggered.
     * @param currentRecordingStatus The current recording status of the movie.
     */
    private void handleMovieRecButtonAction(IBroadcast broadcast,
                                            @NonNull RecordingStatus currentRecordingStatus) {
        SingleRecordingStatus singleRecordingStatus = currentRecordingStatus.getSingleRecordingStatus();
        // Cancel current recording.
        if (singleRecordingStatus == SingleRecordingStatus.SCHEDULED
                || singleRecordingStatus == SingleRecordingStatus.RECORDING) {
            cancelSingleRecording(broadcast, currentRecordingStatus, true, false);

            // Schedule live/future broadcast.
        } else if (broadcast.isLive() || broadcast.isFuture()) {
            scheduleSingleRecording(broadcast, currentRecordingStatus, true, false);
        }
    }

    /**
     * Handle recording button press from UI for an episode.
     * Displays a dialog for the user will the available actions.
     *
     * @param broadcast              The episode for which the dialog should be displayed.
     * @param currentRecordingStatus The current recording status of the broadcast..
     */
    private void handleEpisodeRecButtonAction(IBroadcast broadcast,
                                              @NonNull RecordingStatus currentRecordingStatus) {

        // Show title and episode info for broadcast.
        String title = broadcast.getTitle();
        String description = broadcast.getEpisodeTitleWithSeasonEpisode(mTranslator, ",");

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder()
                .title(title)
                .description(description)
                .cancelable(true)
                .dismissOnBtnClick(true);

        SingleRecordingStatus singleRecordingStatus = currentRecordingStatus.getSingleRecordingStatus();

        // Cancel episode.
        if (singleRecordingStatus == SingleRecordingStatus.SCHEDULED
                || singleRecordingStatus == SingleRecordingStatus.RECORDING) {
            alertDialogBuilder.addButton(mTranslator.get(broadcast.isLive()
                    ? TranslationKey.BUTTON_CANCEL_ONGOING_EPISODE
                    : TranslationKey.BUTTON_CANCEL_EPISODE), dialog ->
                    cancelSingleRecording(broadcast, currentRecordingStatus, true, false)
            );

            // Record episode.
        } else if (broadcast.isLive() || broadcast.isFuture()) {
            alertDialogBuilder.addButton(mTranslator.get(TranslationKey.BUTTON_RECORD_EPISODE), dialog ->
                    scheduleSingleRecording(broadcast, currentRecordingStatus, true, false)
            );
        }

        SeriesRecordingStatus seriesRecordingStatus = currentRecordingStatus.getSeriesRecordingStatus();

        // Cancel series.
        if (seriesRecordingStatus == SeriesRecordingStatus.SCHEDULED) {
            alertDialogBuilder.addButton(mTranslator.get(TranslationKey.BUTTON_CANCEL_SERIES), dialog ->
                    cancelSeriesRecording(broadcast, true, false)
            );

            // Record series.
        } else {
            alertDialogBuilder.addButton(mTranslator.get(TranslationKey.BUTTON_RECORD_SERIES), dialog ->
                    scheduleSeriesRecording(broadcast, currentRecordingStatus, true, false)
            );
        }

        alertDialogBuilder.setEvent(getPageOpenEvent(broadcast, UILog.Page.RecordActionSubMenuManage));

        mNavigator.showDialog(alertDialogBuilder.build());
    }

    /**
     * Handle recording remote button click on a broadcast.
     * Schedule future  recording if is not already scheduled.
     * Cancel future recording if is already scheduled.
     * Delete past recording if is completed.
     *
     * @param broadcast              The broadcast for which the recording action should be handled.
     * @param currentRecordingStatus The current recording status of the broadcast.
     * @param handleAsSingle         True if the series recording options should be ignored for the broadcast.
     */
    public void handleRecKeyAction(IBroadcast broadcast, @Nullable RecordingStatus currentRecordingStatus, boolean handleAsSingle) {
        if (broadcast == null) {
            // Nothing to do.
            return;
        }

        if (mParentalControlManager.isRestricted(broadcast)) {
            // Cannot record restricted content.
            return;
        }

        Log.d(TAG, "Handle REC key for "
                + broadcast.getId() + " current status " + currentRecordingStatus);

        if (isActionInProgress(broadcast)) {
            Log.w(TAG, "Ignore REC action. A REC action is already in progress.");
            return;
        }

        // Get recording status if not available.
        if (currentRecordingStatus == null) {
            Disposable disposable = mPvrRepository.getRecordingStatus(broadcast)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .firstOrError()
                    .subscribe(recordingStatus -> {
                        mAssetDisposableMap.remove(broadcast.getId());
                        handleRecKeyAction(broadcast, recordingStatus, handleAsSingle);
                    });
            mAssetDisposableMap.put(broadcast.getId(), disposable);
            return;
        }

        SingleRecordingStatus singleRecStatus = currentRecordingStatus.getSingleRecordingStatus();
        SeriesRecordingStatus seriesRecStatus = currentRecordingStatus.getSeriesRecordingStatus();

        // Episode
        if (broadcast.isEpisode() && !handleAsSingle) {
            // Future, not scheduled, series NOT scheduled.
            if (broadcast.isFuture()
                    && singleRecStatus == SingleRecordingStatus.NONE
                    && seriesRecStatus == SeriesRecordingStatus.NONE) {
                scheduleSingleRecording(broadcast, currentRecordingStatus, true, false);

                // Future, not scheduled, series scheduled.
            } else if (broadcast.isFuture()
                    && singleRecStatus == SingleRecordingStatus.NONE
                    && seriesRecStatus == SeriesRecordingStatus.SCHEDULED) {
                scheduleSingleRecording(broadcast, currentRecordingStatus, true, false);

                // Future episode scheduled, Series NOT scheduled.
            } else if (broadcast.isFuture()
                    && singleRecStatus == SingleRecordingStatus.SCHEDULED
                    && seriesRecStatus == SeriesRecordingStatus.NONE) {
                scheduleSeriesRecording(broadcast, currentRecordingStatus, false, false);

                // Future episode scheduled, Series scheduled
            } else if (broadcast.isFuture()
                    && singleRecStatus == SingleRecordingStatus.SCHEDULED
                    && seriesRecStatus == SeriesRecordingStatus.SCHEDULED) {
                cancelSeriesRecording(broadcast, false, false);

                // Episode active, series scheduled:
            } else if (singleRecStatus == SingleRecordingStatus.RECORDING
                    && seriesRecStatus == SeriesRecordingStatus.SCHEDULED) {
                cancelSeriesRecording(broadcast, false, false);

                // Episode active, series NOT scheduled
            } else if (singleRecStatus == SingleRecordingStatus.RECORDING
                    && seriesRecStatus == SeriesRecordingStatus.NONE) {
                scheduleSeriesRecording(broadcast, currentRecordingStatus, false, false);

                // Episode live,  NOT scheduled, NOT recorded.
            } else if (broadcast.isLive()) {
                scheduleSingleRecording(broadcast, currentRecordingStatus, true, false);
            }

            // Movie
        } else {
            // Future, not scheduled.
            if (broadcast.isFuture()
                    && singleRecStatus == SingleRecordingStatus.NONE) {
                scheduleSingleRecording(broadcast, currentRecordingStatus, true, false);

                // Future, scheduled.
            } else if (broadcast.isFuture()
                    && singleRecStatus == SingleRecordingStatus.SCHEDULED) {
                cancelSingleRecording(broadcast, currentRecordingStatus, false, false);

                // Live, scheduled.
            } else if (singleRecStatus == SingleRecordingStatus.RECORDING) {
                cancelSingleRecording(broadcast, currentRecordingStatus, true, false);

                // Live and partially recorded.
            } else if (broadcast.isLive() && singleRecStatus == SingleRecordingStatus.COMPLETED) {
                scheduleSingleRecording(broadcast, currentRecordingStatus, true, false);

                // Live, NOT scheduled, NOT recorded.
            } else if (broadcast.isLive()) {
                scheduleSingleRecording(broadcast, currentRecordingStatus, true, false);
            }
        }
    }

    /**
     * handles recording action @{@link android.view.KeyEvent#KEYCODE_MEDIA_RECORD}
     *
     * @param broadcast              data
     * @param currentRecordingStatus recording status
     */
    public void handleRecKeyAction(IBroadcast broadcast, @Nullable RecordingStatus currentRecordingStatus) {
        if (broadcast == null) {
            return;
        }
        if (mPvrRepository.isPVREnabled(broadcast)) {
            handleRecKeyAction(broadcast, currentRecordingStatus, false);
        }
    }

    /**
     * Handle recording remote button click on a broadcast.
     * Schedule series recording if not already scheduled.
     * Cancel future episodes if the series is already scheduled.
     */
    public void handleRecKeyAction(IRecordingSeries series,
                                   @Nullable SeriesRecordingStatus status) {
        Log.d(TAG, "Handle REC key for "
                + series.getId() + " current status " + status);

        if (isActionInProgress(series)) {
            Log.w(TAG, "Ignore REC action. A REC action is already in progress.");
            return;
        }

        // Get recording status if not available.
        if (status == null) {
            Disposable disposable = mPvrRepository.getSeriesRecordingStatus(series)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .firstOrError()
                    .subscribe(recordingStatus -> {
                        mAssetDisposableMap.remove(series.getId());
                        handleRecKeyAction(series, recordingStatus);
                    });
            mAssetDisposableMap.put(series.getId(), disposable);
            return;
        }

        if (status == SeriesRecordingStatus.SCHEDULED) {
            cancelSeriesRecording(series, true);
        } else {
            scheduleSeriesRecording(series, true);
        }
    }

    /**
     * @param broadcast The broadcast for which the action status should checked.
     * @return True if a recording action already in progress for the broadcast.
     */
    public boolean isActionInProgress(IBroadcast broadcast) {
        Disposable disposable = mAssetDisposableMap.get(broadcast.getId());
        return disposable != null && !disposable.isDisposed();
    }

    /**
     * @param series The series for which the action status should checked.
     * @return True if a recording action already in progress for the series.
     */
    public boolean isActionInProgress(ITvSeries series) {
        Disposable disposable = mAssetDisposableMap.get(series.getId());
        return disposable != null && !disposable.isDisposed();
    }

    /**
     * Schedule the broadcast as a single recording.
     * A notification displayed before and after the action finished.
     *
     * @param broadcast       The broadcast which should be scheduled for single recording.
     * @param confirmation    True to show a confirmation for the user if the partial recording will be override with the schedule.
     * @param closeDetailPage True to close the program detail page when the action triggered.
     */
    public void scheduleSingleRecording(IBroadcast broadcast, RecordingStatus recordingStatus,
                                        boolean confirmation, boolean closeDetailPage) {
        if (isActionInProgress(broadcast)) {
            Log.w(TAG, "Ignore schedule action. A REC action is already in progress.");
            return;
        }

        // TODO : check this with the recording implementation.
//
//        if (!broadcast.canPlayDashRecordingOnDevice() && confirmation) {
//            mNavigator.showRecordingNotPlayableAlert();
//        }

        boolean isPartiallyRecorded = broadcast.isLive() && recordingStatus != null
                && recordingStatus.getSingleRecordingStatus() == SingleRecordingStatus.COMPLETED;

        if (isPartiallyRecorded && confirmation) {
            AlertDialog alertDialog = new AlertDialog.Builder()
                    .title(mTranslator.get(TranslationKey.DELETE_PARTIAL_SINGLE_RECORDING_CONFIRMATION_ALERT_HEADER))
                    .description(mTranslator.get(TranslationKey.DELETE_PARTIAL_SINGLE_RECORDING_CONFIRMATION_ALERT_BODY))
                    .addButton(mTranslator.get(TranslationKey.BUTTON_NO))
                    .addButton(mTranslator.get(TranslationKey.BUTTON_YES), dialog -> scheduleSingleRecording(broadcast, recordingStatus, false, closeDetailPage))
                    .cancelable(true)
                    .dismissOnBtnClick(true)
                    .setEvent(getPageOpenEvent(broadcast, UILog.Page.RecordActionSubMenuConfirm))
                    .build();
            mNavigator.showDialog(alertDialog);
            return;
        }

        Disposable disposable = mPvrRepository.scheduleRecording(broadcast)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                            Log.d(TAG, "Recording scheduled.");
                            UILog.logRecordingEvent(broadcast.isLive() ? UILogEvent.RecordingStarted : UILogEvent.RecordingScheduled, broadcast,
                                    mNowOnTvCache.getChannel(broadcast.getChannelId()));
                            mNotificationManager.showNotification(
                                    new Notification.Builder()
                                            .title(mTranslator.get(broadcast.isLive()
                                                    ? TranslationKey.RECORDING_NOTIFICATION_SCHEDULED_STARTED
                                                    : TranslationKey.RECORDING_NOTIFICATION_SCHEDULED))
                                            .skippDisplayQueue(true)
                                            .build());

                            mAssetDisposableMap.remove(broadcast.getId());
                        }, throwable -> {
                            Log.e(TAG, "Couldn't schedule recording.", throwable);
                            mNotificationManager.showNotification(
                                    new Notification.Builder()
                                            .title(mTranslator.get(TranslationKey.RECORDING_NOTIFICATION_SCHEDULE_ERROR))
                                            .skippDisplayQueue(true)
                                            .build());
                            mAssetDisposableMap.remove(broadcast.getId());
                        }
                );
        mAssetDisposableMap.put(broadcast.getId(), disposable);

        if (closeDetailPage) {
            mNavigator.closeProgramDetailPage(broadcast.getId());
        }
    }

    /**
     * Delete all completed episode of the series to recording.
     * A notification displayed before and after the action finished.
     * May ask for user confirmation before the action.
     *
     * @param series       The series from which all completed episode should be deleted.
     * @param confirmation If true a confirmation dialog displayed before the action.
     */
    public void deleteSeriesRecording(IRecordingSeries series, boolean confirmation) {
        if (isActionInProgress(series)) {
            Log.w(TAG, "Ignore delete action. A REC action is already in progress.");
            return;
        }

        if (confirmation) {
            AlertDialog.Builder builder = new AlertDialog.Builder()
                    .addButton(mTranslator.get(TranslationKey.BUTTON_DELETE_SERIES), dialog -> deleteSeriesRecording(series, false))
                    .cancelable(true)
                    .dismissOnBtnClick(true);

            if (series.hasFutureOrLiveEpisode()) {
                builder.title(mTranslator.get(TranslationKey.DELETE_EPISODES_RECORDING_CONFIRMATION_ALERT_HEADER))
                        .description(mTranslator.get(TranslationKey.DELETE_EPISODES_RECORDING_CONFIRMATION_ALERT_BODY))
                        .addButton(mTranslator.get(TranslationKey.BUTTON_DELETE_AND_CANCEL), dialog -> deleteAndCancelSeries(series));

            } else {
                builder.title(mTranslator.get(TranslationKey.DELETE_EPISODES_NON_ACTIVE_SERIES_RECORDING_HEADER))
                        .description(mTranslator.get(TranslationKey.DELETE_EPISODES_NON_ACTIVE_SERIES_RECORDING_BODY));
            }

            builder.addButton(mTranslator.get(TranslationKey.BACK_BUTTON))
                    .setEvent(getPageOpenEvent(series, UILog.Page.RecordActionSubMenuConfirm));

            AlertDialog alertDialog = builder.build();
            mNavigator.showDialogOnTop(alertDialog);
            return;
        }

        Disposable disposable = mPvrRepository.deleteSeriesRecording(series)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                            Log.d(TAG, "Series recording deleted.");
                            IBroadcast firstEpisode = series.getFirstEpisode();
                            TvChannel channel = firstEpisode == null ? null : mNowOnTvCache.getChannel(firstEpisode.getChannelId());
                            UILog.logRecordingEvent(UILogEvent.SeriesRecDeleted, firstEpisode, channel);
                            mNotificationManager.showNotification(
                                    new Notification.Builder()
                                            .title(mTranslator.get(TranslationKey.RECORDING_SERIES_NOTIFICATION_DELETED))
                                            .skippDisplayQueue(true)
                                            .build());

                            mAssetDisposableMap.remove(series.getId());
                        }, throwable -> {
                            Log.e(TAG, "Couldn't delete series recording.", throwable);
                            mNotificationManager.showNotification(
                                    new Notification.Builder()
                                            .title(mTranslator.get(TranslationKey.RECORDING_NOTIFICATION_DELETE_ERROR))
                                            .skippDisplayQueue(true)
                                            .build());
                            mAssetDisposableMap.remove(series.getId());
                        }
                );
        mAssetDisposableMap.put(series.getId(), disposable);
    }

    /**
     * Delete and cancel all the episodes in a series recording.
     *
     * @param series The series selected that will have their episodes deleted and unscheduled.
     */
    private void deleteAndCancelSeries(IRecordingSeries series) {
        if (isActionInProgress(series)) {
            Log.w(TAG, "Ignore delete action. A REC action is already in progress.");
            return;
        }

        Disposable disposable = mPvrRepository.deleteAndCancelRecording(series)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    Log.d(TAG, "Cancel series recording canceled.");
                    IBroadcast firstEpisode = series.getFirstEpisode();
                    TvChannel channel = firstEpisode == null ? null : mNowOnTvCache.getChannel(firstEpisode.getChannelId());
                    UILog.logRecordingEvent(UILogEvent.SeriesRecCanceled, firstEpisode, channel);
                    UILog.logRecordingEvent(UILogEvent.SeriesRecDeleted, firstEpisode, channel);

                    mNotificationManager.showNotification(new Notification.Builder()
                            .title(mTranslator.get(TranslationKey.RECORDING_SERIES_NOTIFICATION_DELETED))
                            .build());

                    mAssetDisposableMap.remove(series.getId());
                }, throwable -> {
                    Log.e(TAG, "Couldn't cancel recording.", throwable);
                    mNotificationManager.showNotification(new Notification.Builder()
                            .title(mTranslator.get(TranslationKey.RECORDING_NOTIFICATION_DELETE_ERROR))
                            .build());
                    mAssetDisposableMap.remove(series.getId());
                });

        mAssetDisposableMap.put(series.getId(), disposable);
    }

    /**
     * Scheduled the given series to series recording.
     * A notification displayed before and after the action finished.
     * May ask for user confirmation before the action.
     *
     * @param series       The series which should be scheduled for recording.
     * @param confirmation If true a confirmation dialog displayed before the action.
     */
    public void scheduleSeriesRecording(IRecordingSeries series, boolean confirmation) {
        if (isActionInProgress(series)) {
            Log.w(TAG, "Ignore schedule series action. A REC action is already in progress.");
            return;
        }

        // TODO : check this if it's still needed.
//        if (!series.canPlayDashRecordingOnDevice()) {
//            mNavigator.showRecordingNotPlayableAlert();
//        }

        if (series.hasPartiallyRecordedLiveEpisode() && confirmation) {
            AlertDialog alertDialog = new AlertDialog.Builder()
                    .title(mTranslator.get(TranslationKey.DELETE_PARTIAL_SINGLE_RECORDING_CONFIRMATION_ALERT_HEADER))
                    .description(mTranslator.get(TranslationKey.DELETE_PARTIAL_SINGLE_RECORDING_CONFIRMATION_ALERT_BODY))
                    .addButton(mTranslator.get(TranslationKey.BUTTON_NO))
                    .addButton(mTranslator.get(TranslationKey.BUTTON_YES), dialog -> scheduleSeriesRecording(series, false))
                    .cancelable(true)
                    .dismissOnBtnClick(true)
                    .setEvent(getPageOpenEvent(series, UILog.Page.RecordActionSubMenuConfirm))
                    .build();
            mNavigator.showDialogOnTop(alertDialog);
            return;
        }

        Disposable disposable = mPvrRepository.scheduleSeriesRecording(series)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                            Log.d(TAG, "Recording scheduled.");
                            IBroadcast firstEpisode = series.getFirstEpisode();
                            TvChannel channel = firstEpisode == null ? null : mNowOnTvCache.getChannel(firstEpisode.getChannelId());
                            if (firstEpisode != null && firstEpisode.isLive()) {
                                UILog.logRecordingEvent(UILogEvent.RecordingStarted, firstEpisode, channel);
                            }
                            UILog.logRecordingEvent(UILogEvent.SeriesRecScheduled, firstEpisode, channel);
                            mNotificationManager.showNotification(
                                    new Notification.Builder()
                                            .title(mTranslator.get(series.hasLiveEpisode()
                                                    ? TranslationKey.RECORDING_NOTIFICATION_SCHEDULED_STARTED
                                                    : TranslationKey.RECORDING_NOTIFICATION_SCHEDULED))
                                            .skippDisplayQueue(true)
                                            .build());
                        }, throwable -> {
                            Log.e(TAG, "Couldn't schedule recording.", throwable);
                            mNotificationManager.showNotification(
                                    new Notification.Builder()
                                            .title(mTranslator.get(TranslationKey.RECORDING_NOTIFICATION_SCHEDULE_ERROR))
                                            .skippDisplayQueue(true)
                                            .build());
                        }
                );
        mAssetDisposableMap.put(series.getId(), disposable);
    }

    /**
     * Cancel all the future episode of the series from recording.
     * A notification displayed before and after the action finished.
     * May ask for user confirmation before the action.
     *
     * @param series       The series for which all future recording should be canceled from recording.
     * @param confirmation If true a confirmation dialog displayed before the action.
     */
    public void cancelSeriesRecording(IRecordingSeries series, boolean confirmation) {
        if (isActionInProgress(series)) {
            Log.w(TAG, "Ignore cancel series action. A REC action is already in progress.");
            return;
        }

        // Show cancel series recording confirmation dialog.
        if (confirmation) {
            AlertDialog alertDialog = new AlertDialog.Builder()
                    .title(mTranslator.get(TranslationKey.CANCEL_SERIES_RECORDING_CONFIRMATION_ALERT_HEADER))
                    .description(mTranslator.get(TranslationKey.CANCEL_SERIES_RECORDING_CONFIRMATION_ALERT_BODY))
                    .addButton(mTranslator.get(TranslationKey.BUTTON_NO))
                    .addButton(mTranslator.get(TranslationKey.BUTTON_YES), dialog -> cancelSeriesRecording(series, false))
                    .cancelable(true)
                    .dismissOnBtnClick(true)
                    .setEvent(getPageOpenEvent(series, UILog.Page.RecordActionSubMenuConfirm))
                    .build();
            mNavigator.showDialog(alertDialog);
            return;
        }

        Disposable disposable = mPvrRepository.cancelSeriesRecording(series)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    Log.d(TAG, "Cancel series recording canceled.");
                    IBroadcast firstEpisode = series.getFirstEpisode();
                    TvChannel channel = firstEpisode == null ? null : mNowOnTvCache.getChannel(firstEpisode.getChannelId());
                    UILog.logRecordingEvent(UILogEvent.SeriesRecCanceled, firstEpisode, channel);

                    mNotificationManager.showNotification(
                            new Notification.Builder()
                                    .title(mTranslator.get(TranslationKey.RECORDING_NOTIFICATION_SERIES_CANCELLED))
                                    .skippDisplayQueue(true)
                                    .build());
                    mAssetDisposableMap.remove(series.getId());
                }, throwable -> {
                    Log.e(TAG, "Couldn't cancel recording.", throwable);
                    mNotificationManager.showNotification(new Notification.Builder()
                            .title(mTranslator.get(TranslationKey.RECORDING_NOTIFICATION_CANCEL_ERROR))
                            .skippDisplayQueue(true)
                            .build());
                    mAssetDisposableMap.remove(series.getId());
                });

        mAssetDisposableMap.put(series.getId(), disposable);
    }

    /**
     * Schedule broadcast for series recording.
     * A notification displayed before and after the action finished.
     *
     * @param broadcast       The broadcast for which the series should be scheduled for recording.
     * @param recordingStatus The current recording status of the broadcast.
     * @param confirmation    True to show a confirmation for the user if the partial recording will be override with the schedule.
     * @param closeDetailPage True to close the program detail page when the action triggered.
     */
    public void scheduleSeriesRecording(IBroadcast broadcast, RecordingStatus recordingStatus,
                                        boolean confirmation, boolean closeDetailPage) {
        if (isActionInProgress(broadcast)) {
            Log.w(TAG, "Ignore schedule series action. A REC action is already in progress.");
            return;
        }

        // TODO : check this
//        if (!broadcast.canPlayDashRecordingOnDevice()) {
//            mNavigator.showRecordingNotPlayableAlert();
//        }

        boolean isPartiallyRecorded = broadcast.isLive() && recordingStatus != null
                && recordingStatus.getSingleRecordingStatus() == SingleRecordingStatus.COMPLETED;

        if (isPartiallyRecorded && confirmation) {
            AlertDialog alertDialog = new AlertDialog.Builder()
                    .title(mTranslator.get(TranslationKey.DELETE_PARTIAL_SINGLE_RECORDING_CONFIRMATION_ALERT_HEADER))
                    .description(mTranslator.get(TranslationKey.DELETE_PARTIAL_SINGLE_RECORDING_CONFIRMATION_ALERT_BODY))
                    .addButton(mTranslator.get(TranslationKey.BUTTON_NO))
                    .addButton(mTranslator.get(TranslationKey.BUTTON_YES), dialog -> scheduleSeriesRecording(broadcast, recordingStatus, false, closeDetailPage))
                    .cancelable(true)
                    .dismissOnBtnClick(true)
                    .setEvent(getPageOpenEvent(broadcast, UILog.Page.RecordActionSubMenuConfirm))
                    .build();
            mNavigator.showDialog(alertDialog);
            return;
        }

        Disposable disposable = mPvrRepository.scheduleSeriesRecording(broadcast)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    Log.d(TAG, "Series recording scheduled.");
                    TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
                    if (broadcast.isLive()) {
                        UILog.logRecordingEvent(UILogEvent.RecordingStarted, broadcast, channel);
                    }
                    UILog.logRecordingEvent(UILogEvent.SeriesRecScheduled, broadcast, channel);
                    mNotificationManager.showNotification(
                            new Notification.Builder()
                                    .title(mTranslator.get(broadcast.isLive()
                                            ? TranslationKey.RECORDING_NOTIFICATION_SERIES_SCHEDULED_STARTED
                                            : TranslationKey.RECORDING_NOTIFICATION_SERIES_SCHEDULED
                                    ))
                                    .skippDisplayQueue(true)
                                    .build());
                    mAssetDisposableMap.remove(broadcast.getId());
                }, throwable -> {
                    Log.e(TAG, "Couldn't schedule recording.", throwable);
                    mNotificationManager.showNotification(new Notification.Builder()
                            .title(mTranslator.get(TranslationKey.RECORDING_NOTIFICATION_SCHEDULE_ERROR))
                            .skippDisplayQueue(true)
                            .build());
                    mAssetDisposableMap.remove(broadcast.getId());
                });
        mAssetDisposableMap.put(broadcast.getId(), disposable);

        if (closeDetailPage) {
            mNavigator.closeProgramDetailPage(broadcast.getId());
        }
    }

    /**
     * Cancel the ongoing or future recording of the broadcast
     * A notification displayed before and after the action finished.
     * May ask for user confirmation before the action.
     *
     * @param broadcast       The broadcast which should be canceled from receding.
     * @param recordingStatus The current recordings status of the broadcast.
     * @param confirmation    If true display a confirmation dialog for the user.
     * @param closeDetailPage True to close the program detail page when the action triggered.
     */
    public void cancelSingleRecording(IBroadcast broadcast, RecordingStatus recordingStatus,
                                      boolean confirmation, boolean closeDetailPage) {
        if (isActionInProgress(broadcast)) {
            Log.w(TAG, "Ignore cancel action. A REC action is already in progress.");
            return;
        }

        boolean isRecording = recordingStatus != null
                && recordingStatus.getSingleRecordingStatus() == SingleRecordingStatus.RECORDING;

        // Show cancel recording confirmation dialog.
        if (confirmation) {
            String title;
            String body;
            if (broadcast.isEpisode()) {
                title = mTranslator.get(isRecording
                        ? TranslationKey.CANCEL_EPISODE_RECORDING_CONFIRMATION_ALERT_HEADER_ONGOING
                        : TranslationKey.CANCEL_EPISODE_RECORDING_CONFIRMATION_ALERT_HEADER);
                body = mTranslator.get(isRecording
                        ? TranslationKey.CANCEL_EPISODE_RECORDING_CONFIRMATION_ALERT_BODY_ONGOING
                        : TranslationKey.CANCEL_EPISODE_RECORDING_CONFIRMATION_ALERT_BODY);
            } else {
                title = mTranslator.get(isRecording
                        ? TranslationKey.CANCEL_SINGLE_RECORDING_CONFIRMATION_ALERT_HEADER_ONGOING
                        : TranslationKey.CANCEL_SINGLE_RECORDING_CONFIRMATION_ALERT_HEADER);
                body = mTranslator.get(isRecording
                        ? TranslationKey.CANCEL_SINGLE_RECORDING_CONFIRMATION_ALERT_BODY_ONGOING
                        : TranslationKey.CANCEL_SINGLE_RECORDING_CONFIRMATION_ALERT_BODY);
            }

            AlertDialog alertDialog = new AlertDialog.Builder()
                    .title(title)
                    .description(body)
                    .addButton(mTranslator.get(TranslationKey.BUTTON_NO))
                    .addButton(mTranslator.get(TranslationKey.BUTTON_YES), dialog -> cancelSingleRecording(broadcast, recordingStatus, false, closeDetailPage))
                    .cancelable(true)
                    .dismissOnBtnClick(true)
                    .setEvent(getPageOpenEvent(broadcast, UILog.Page.RecordActionSubMenuConfirm))
                    .build();
            mNavigator.showDialog(alertDialog);
            return;
        }

        // Cancel recording.
        Disposable disposable = mPvrRepository.cancelRecording(broadcast)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    Log.d(TAG, "Recording canceled.");
                    UILog.logRecordingEvent(isRecording ? UILogEvent.RecordingStopped : UILogEvent.RecordingCanceled, broadcast, mNowOnTvCache.getChannel(broadcast.getChannelId()));
                    mNotificationManager.showNotification(
                            new Notification.Builder()
                                    .title(mTranslator.get(isRecording
                                            ? TranslationKey.RECORDING_NOTIFICATION_CANCELLED_ONGOING
                                            : TranslationKey.RECORDING_NOTIFICATION_CANCELLED))
                                    .skippDisplayQueue(true)
                                    .build());
                    mAssetDisposableMap.remove(broadcast.getId());
                }, throwable -> {
                    Log.e(TAG, "Couldn't cancel recording.", throwable);
                    mNotificationManager.showNotification(
                            new Notification.Builder()
                                    .title(mTranslator.get(TranslationKey.RECORDING_NOTIFICATION_CANCEL_ERROR))
                                    .skippDisplayQueue(true)
                                    .build());
                    mAssetDisposableMap.remove(broadcast.getId());
                });

        mAssetDisposableMap.put(broadcast.getId(), disposable);


        if (closeDetailPage) {
            mNavigator.closeProgramDetailPage(broadcast.getId());
        }
    }

    /**
     * Cancel all the future episode of the series from recording.
     * A notification displayed before and after the action finished.
     * May ask for user confirmation before the action.
     *
     * @param broadcast       The broadcast of the series for which all future recording should be canceled from recording.
     * @param confirmation    If true a confirmation dialog displayed before the action.
     * @param closeDetailPage True to close the program detail page when the action triggered.
     */
    public void cancelSeriesRecording(IBroadcast broadcast, boolean confirmation, boolean closeDetailPage) {
        if (isActionInProgress(broadcast)) {
            Log.w(TAG, "Ignore cancel series action. A REC action is already in progress.");
            return;
        }

        // Show cancel series recording confirmation dialog.
        if (confirmation) {
            AlertDialog alertDialog = new AlertDialog.Builder()
                    .title(mTranslator.get(TranslationKey.CANCEL_SERIES_RECORDING_CONFIRMATION_ALERT_HEADER))
                    .description(mTranslator.get(TranslationKey.CANCEL_SERIES_RECORDING_CONFIRMATION_ALERT_BODY))
                    .addButton(mTranslator.get(TranslationKey.BUTTON_NO))
                    .addButton(mTranslator.get(TranslationKey.BUTTON_YES), dialog -> cancelSeriesRecording(broadcast, false, closeDetailPage))
                    .cancelable(true)
                    .dismissOnBtnClick(true)
                    .setEvent(getPageOpenEvent(broadcast, UILog.Page.RecordActionSubMenuConfirm))
                    .build();
            mNavigator.showDialog(alertDialog);
            return;
        }

        Disposable disposable = mPvrRepository.cancelSeriesRecording(broadcast)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    Log.d(TAG, "Cancel series recording canceled.");
                    UILog.logRecordingEvent(UILogEvent.SeriesRecCanceled, broadcast, mNowOnTvCache.getChannel(broadcast.getChannelId()));

                    mNotificationManager.showNotification(
                            new Notification.Builder()
                                    .title(mTranslator.get(TranslationKey.RECORDING_NOTIFICATION_SERIES_CANCELLED))
                                    .skippDisplayQueue(true)
                                    .build());
                    mAssetDisposableMap.remove(broadcast.getId());
                }, throwable -> {
                    Log.e(TAG, "Couldn't cancel recording.", throwable);
                    mNotificationManager.showNotification(
                            new Notification.Builder()
                                    .title(mTranslator.get(TranslationKey.RECORDING_NOTIFICATION_CANCEL_ERROR))
                                    .skippDisplayQueue(true)
                                    .build());
                    mAssetDisposableMap.remove(broadcast.getId());
                });

        mAssetDisposableMap.put(broadcast.getId(), disposable);

        if (closeDetailPage) {
            mNavigator.closeProgramDetailPage(broadcast.getId());
        }
    }

    /**
     * Delete the existing recording of the broadcast.
     * A notification displayed before and after the action finished.
     * May ask for user confirmation before the action.
     *
     * @param recording       The recording that should be deleted.
     * @param confirmation    If true a confirmation dialog displayed before the action.
     * @param closeDetailPage True to close the program detail page when the action triggered.
     */
    public void deleteRecording(IRecording recording, boolean confirmation, boolean closeDetailPage) {
        if (isActionInProgress(recording)) {
            Log.w(TAG, "Ignore delete action. A REC action is already in progress.");
            return;
        }

        // Show cancel series recording confirmation dialog.
        if (confirmation) {
            AlertDialog alertDialog = new AlertDialog.Builder()
                    .title(mTranslator.get(recording.isEpisode()
                            ? TranslationKey.DELETE_EPISODE_RECORDING_CONFIRMATION_ALERT_HEADER
                            : TranslationKey.DELETE_SINGLE_RECORDING_CONFIRMATION_ALERT_HEADER))
                    .description(mTranslator.get(recording.isEpisode()
                            ? TranslationKey.DELETE_EPISODE_RECORDING_CONFIRMATION_ALERT_BODY
                            : TranslationKey.DELETE_SINGLE_RECORDING_CONFIRMATION_ALERT_BODY))
                    .addButton(mTranslator.get(TranslationKey.BUTTON_NO))
                    .addButton(mTranslator.get(TranslationKey.BUTTON_YES), dialog -> deleteRecording(recording, false, closeDetailPage))
                    .cancelable(true)
                    .dismissOnBtnClick(true)
                    .setEvent(getPageOpenEvent(recording, UILog.Page.RecordActionSubMenuConfirm))
                    .build();
            mNavigator.showDialog(alertDialog);
            return;
        }

        Disposable disposable = mPvrRepository.deleteRecording(recording)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    Log.d(TAG, "Recording deleted.");
                    UILog.logRecordingEvent(UILogEvent.RecordingDeleted, recording, mNowOnTvCache.getChannel(recording.getChannelId()));
                    mNotificationManager.showNotification(
                            new Notification.Builder()
                                    .title(mTranslator.get(TranslationKey.RECORDING_SINGLE_NOTIFICATION_DELETED))
                                    .skippDisplayQueue(true)
                                    .build());
                    mAssetDisposableMap.remove(recording.getId());
                }, throwable -> {
                    Log.e(TAG, "Couldn't delete recording.", throwable);
                    mNotificationManager.showNotification(
                            new Notification.Builder()
                                    .title(mTranslator.get(TranslationKey.RECORDING_NOTIFICATION_DELETE_ERROR))
                                    .skippDisplayQueue(true)
                                    .build());
                    mAssetDisposableMap.remove(recording.getId());
                });
        mAssetDisposableMap.put(recording.getId(), disposable);

        if (closeDetailPage) {
            mNavigator.closeProgramDetailPage(recording.getId());
        }
    }

    /**
     * Displays an alert dialog stating that the channel is not entitled, with an OK button that dismisses it.
     */
    public void showChannelNotEntitledDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder()
                .title(mTranslator.get(TranslationKey.ERR_PLAY_RECORDING_CHANNEL_NOT_ENTITLED_HEADER))
                .description(mTranslator.get(TranslationKey.ERR_PLAY_RECORDING_CHANNEL_NOT_ENTITLED_BODY))
                .addButton(mTranslator.get(TranslationKey.OK_BUTTON))
                .cancelable(true)
                .dismissOnBtnClick(true)
                .build();
        mNavigator.showDialogOnTop(alertDialog);
    }

    public void showStartReplayWarningNotification(@Nullable RecordingStatus recordingStatus, TvChannel channel) {
        if (recordingStatus == null || recordingStatus.getRecording() == null) {
            return;
        }

        showStartReplayWarningNotification(recordingStatus.getRecording(), channel);
    }


    /**
     * Build a notification to warn user the application started a recording that was failed
     * but can watch in replay until the program doesn't go outside from the replay window.
     */
    public void showStartReplayWarningNotification(@NonNull IRecording recording, TvChannel channel) {
        if (channel == null || !recording.isFailedRecording()) {
            return;
        }

        String timeLeft = LocaleTimeUtils.getBroadcastAvailabilityTime(channel, recording, null, mTranslator);
        if (("").equals(timeLeft)) {
            return;
        }

        String description = mTranslator.get(TranslationKey.NOTIFICATION_RECORDING_FAILED)
                .replace(TIME_LEFT_PLACEHOLDER, timeLeft);

        Notification item = new Notification.Builder()
                .title("")
                .subtext(description)
                .timeout(BaseNotification.Timeout.LENGTH_LONG)
                .build();

        mNotificationManager.showNotification(item);
    }

    /**
     * Show periodically a notification for the user when the recording storage is full.
     */
    public void showStorageFullNotification() {
        if (mStorageFullLastShownTime + STORAGE_FULL_DISPLAY_PERIOD > System.currentTimeMillis()) {
            Log.d(TAG, "Skip recording storage check. Next check at "
                    + new Date(mStorageFullLastShownTime + STORAGE_FULL_DISPLAY_PERIOD));
            return;
        }

        // Request recording storage info.
        RxUtils.disposeSilently(mRecordingStorageDisposable);
        mRecordingStorageDisposable = mPvrRepository.getSTCRecordingQuota()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recordingQuota -> {
                    Log.d(TAG, "User recording storage percentage : " + recordingQuota.getUsedRecordingSpacePercentage());
                    if (recordingQuota.getUsedRecordingSpacePercentage() > 100) {

                        // Generate cleanup date.
                        String cleanUpDate = TimeUtils.getTimeFormat(DateTimeFormatter.ofPattern(mLocaleSettings.getDateFormat(), Locale.getDefault()),
                                recordingQuota.getCleanupDate());

                        // Show storage full dialog.
                        ActionBarDialog actionBarDialog = new ActionBarDialog.Builder()
                                .title(mTranslator.get(TranslationKey.ACTION_BAR_RECORDING_STORAGE_TITLE))
                                .description(mTranslator.get(TranslationKey.ACTION_BAR_RECORDING_STORAGE_BODY)
                                        .replace(CLEANUP_DATE_PLACEHOLDER, cleanUpDate))
                                .priority(AlertDialog.BLOCKING_DIALOG_PRIORITY)
                                .dismissOnBtnClick(true)
                                .blockKeys(true)
                                .cancelable(false)
                                .addButton(mTranslator.get(TranslationKey.OK_BUTTON))
                                .build();
                        mNavigator.showDialogOnTop(actionBarDialog);

                        mStorageFullLastShownTime = System.currentTimeMillis();
                    }
                });
    }

    public Event<UILogEvent, UILogEvent.Detail> getPageOpenEvent(IBroadcast broadcast, UILog.Page page) {
        if (broadcast == null) {
            return null;
        }

        return ReportUtils.createEvent(page, broadcast);
    }

    public Event<UILogEvent, UILogEvent.Detail> getPageOpenEvent(IRecordingSeries series, UILog.Page page) {
        if (series == null) {
            return null;
        }

        return new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                .addDetail(UILogEvent.Detail.Page, page)
                .addDetail(UILogEvent.Detail.PageName, mNowOnTvCache.getCallLetter(series.getChannelId()))
                .addDetail(UILogEvent.Detail.PageId, series.getChannelId())
                .addDetail(UILogEvent.Detail.FocusName, series.getTitle())
                .addDetail(UILogEvent.Detail.FocusId, series.getId());
    }
}
