package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.PlayOptionsCardBinding;
import tv.threess.threeready.ui.generic.model.VodPlayOption;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;

/**
 * Presenter class for a VodVariant card from Play Options dialog.
 *
 * @author Andrei Teslovan
 * @since 2018.11.29
 */
public class VodPlayOptionsCardPresenter extends BaseCardPresenter<VodPlayOptionsCardPresenter.ViewHolder, VodPlayOption> {

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final Navigator mNavigator = Components.get(Navigator.class);

    public VodPlayOptionsCardPresenter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        PlayOptionsCardBinding binding = PlayOptionsCardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.view.setBackgroundColor(mLayoutConfig.getPlaceHolderColor());
        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, VodPlayOption playOption) {
        super.onBindHolder(holder, playOption);
        holder.mBinding.logo.setImageResource(R.drawable.logo_proximus);
    }

    @Override
    public int getCardWidth(VodPlayOption playOption) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.play_options_screen_card_width);
    }

    @Override
    public int getCardHeight(VodPlayOption vodPlayOption) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.play_options_screen_card_height);
    }

    @Override
    public void onClicked(ViewHolder holder, VodPlayOption playOption) {
        super.onClicked(holder, playOption);
        mNavigator.showVodPlayer(StartAction.Ui, playOption.getVodVariant(), playOption.getVod(), playOption.getVodSeries(), playOption.getVodPrice());
        mNavigator.clearAllDialogs();
    }

    @Override
    public long getStableId(VodPlayOption vodPlayOption) {
        return Objects.hash(vodPlayOption);
    }

    public static class ViewHolder extends BaseCardPresenter.ViewHolder {

        private final PlayOptionsCardBinding mBinding;

        public ViewHolder(PlayOptionsCardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
