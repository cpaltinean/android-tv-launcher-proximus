package tv.threess.threeready.ui.startup.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.databinding.ParentalControlFragmentBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Startup step fragment for setting the user's parental control preferences.
 *
 * @author Eugen Guzyk
 * @since 2018.07.02
 */
public class ParentalControlSetupFragment extends StartFragment {
    private static final String TAG = Log.tag(ParentalControlSetupFragment.class);

    private static final String EXTRA_PARENTAL_CONTROL_KEY = "ParentalControl";

    private final Translator mTranslator = Components.get(Translator.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);

    List<Disposable> mDisposables = new ArrayList<>();

    private ParentalControlFragmentBinding mBinding;

    public static ParentalControlSetupFragment newInstance(StepCallback callback, ParentalRating parentalRating) {
        ParentalControlSetupFragment fragment = new ParentalControlSetupFragment();
        fragment.setStepCallback(callback);

        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_PARENTAL_CONTROL_KEY, parentalRating);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mBinding = ParentalControlFragmentBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateLayout();
        mStepCallback.getStartupTimer().pause();
    }

    @Override
    public boolean onBackPressed() {
        //Do nothing on back press
        return true;
    }

    @Override
    public void updateLayout() {
        ParentalRating defaultRating = getParentalRatingFromArguments();

        mBinding.ftiBootCommonLayout.title.setText(mTranslator.get(TranslationKey.FTI_PARENTAL_CONTROL_TITLE));
        mBinding.ftiBootCommonLayout.title.setTextColor(mLayoutConfig.getFontColor());

        String ageRatingKey = "%age_rating%";
        String bodyText = mTranslator.get(TranslationKey.FTI_PARENTAL_CONTROL_BODY)
                .replace(ageRatingKey, String.valueOf(defaultRating.getMinimumAge()));

        mBinding.ftiBootCommonLayout.body.setText(bodyText);
        mBinding.ftiBootCommonLayout.body.setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f));

        mBinding.note.setText(mTranslator.get(TranslationKey.FTI_PARENTAL_CONTROL_NOTE));
        mBinding.note.setTextColor(mLayoutConfig.getFontColor());

        mBinding.deactivateButton.setBackground(UiUtils.createFtiButtonBackground(getContext(), mLayoutConfig));
        mBinding.deactivateButton.setText(mTranslator.get(TranslationKey.FTI_PARENTAL_CONTROL_BUTTON_DEACTIVATE));
        mBinding.deactivateButton.setTextColor(mLayoutConfig.getButtonFocusedFontColor());
        mBinding.deactivateButton.setOnClickListener(v ->
                mParentalControlManager.disableParentalControl()
                        .andThen(mParentalControlManager.selectParentalRating(ParentalRating.Rated18))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(mParentalControlObserver));

        mBinding.deactivateButton.requestFocus();

        mBinding.activateButton.setBackground(UiUtils.createFtiButtonBackground(getContext(), mLayoutConfig));
        mBinding.activateButton.setText(mTranslator.get(TranslationKey.FTI_PARENTAL_CONTROL_BUTTON_ACTIVATE));
        mBinding.activateButton.setTextColor(mLayoutConfig.getButtonFocusedFontColor());
        mBinding.activateButton.setOnClickListener(v ->
                mParentalControlManager.enableParentalControl()
                        .andThen(mParentalControlManager.selectParentalRating(defaultRating))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(mParentalControlObserver));
    }

    private ParentalRating getParentalRatingFromArguments() {
        ParentalRating defaultRating = getSerializableArgument(EXTRA_PARENTAL_CONTROL_KEY, ParentalRating.Rated16);
        if (defaultRating == ParentalRating.RatedAll || defaultRating == ParentalRating.Undefined) {
            return ParentalRating.Rated16;
        }
        return defaultRating;
    }

    private void finish() {
        mNavigator.popStartFragment();
        mStepCallback.getStartupTimer().resume();
        mStepCallback.onFinished();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        RxUtils.disposeSilently(mDisposables);
    }

    private final CompletableObserver mParentalControlObserver = new CompletableObserver() {
        @Override
        public void onSubscribe(Disposable d) {
            mDisposables.add(d);
        }

        @Override
        public void onError(Throwable e) {
            Log.e(TAG, "Could not set the parental control state.", e);
            finish();
        }

        @Override
        public void onComplete() {
            finish();
        }
    };
}
