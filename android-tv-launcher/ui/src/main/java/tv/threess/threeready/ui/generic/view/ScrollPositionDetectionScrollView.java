package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

/**
 * Custom ScrollView which detects the position of the scrollbar.
 *
 * @author Andrei Teslovan
 * @since 2018.07.12
 */

public class ScrollPositionDetectionScrollView extends ScrollView {

    private ScrollListener mScrollListener;

    public interface ScrollListener {
        void onScroll(ScrollPosition scrollPosition);
    }

    public enum ScrollPosition {
        START,
        MIDDLE,
        END
    }

    public ScrollPositionDetectionScrollView(Context context) {
        super(context);
    }

    public ScrollPositionDetectionScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollPositionDetectionScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ScrollPositionDetectionScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setScrollListener(ScrollListener scrollListener) {
        this.mScrollListener = scrollListener;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);

        View view = this.getChildAt(this.getChildCount() - 1);

        if (view != null) {
            int diff = (view.getBottom() - (this.getHeight() + this.getScrollY()));

            if (mScrollListener != null) {
                if (diff <= 0) {
                    mScrollListener.onScroll(ScrollPosition.END);
                } else if (diff >= view.getBottom() - this.getHeight()) {
                    mScrollListener.onScroll(ScrollPosition.START);
                } else {
                    mScrollListener.onScroll(ScrollPosition.MIDDLE);
                }
            }
        }
    }

    public boolean isScrollable() {
        if (getChildCount() > 0) {
            int childHeight = getChildAt(0).getHeight();
            return getHeight() < childHeight + getPaddingTop() + getPaddingBottom();
        }
        return false;
    }
}
