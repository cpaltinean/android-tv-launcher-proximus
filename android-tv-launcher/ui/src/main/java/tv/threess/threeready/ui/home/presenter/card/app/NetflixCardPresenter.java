package tv.threess.threeready.ui.home.presenter.card.app;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.Presenter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.List;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.netflix.INetflixGroup;
import tv.threess.threeready.api.netflix.INetflixTile;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.NetflixCardBinding;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.generic.presenter.BaseModularCardPresenter;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.ResizableCardView;

/**
 * Presenter class for Netflix tile card.
 *
 * @author David
 * @since 28.02.2022
 */
public class NetflixCardPresenter extends BaseModularCardPresenter<NetflixCardPresenter.ViewHolder, INetflixTile> {

    private static final String TAG = Log.tag(NetflixCardPresenter.class);

    private final NetflixCardListener mListener;

    private final int mFocusedCardElevation;
    private final int mDefaultCardElevation;

    public NetflixCardPresenter(Context context, NetflixCardListener listener) {
        super(context);
        mListener = listener;
        mFocusedCardElevation = context.getResources().getDimensionPixelOffset(R.dimen.card_focused_elevation);
        mDefaultCardElevation = context.getResources().getDimensionPixelOffset(R.dimen.card_default_elevation);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent, RecyclerView.RecycledViewPool recycledViewPool) {
        NetflixCardBinding binding = NetflixCardBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindHolder(ModuleData<?> moduleData, ViewHolder holder, INetflixTile item) {
        super.onBindHolder(moduleData, holder, item);

        holder.mCardView.setCardWidth(getCardWidth(moduleData, item));
        holder.mCardView.setCardHeight(getCardHeight(moduleData, item));

        updateCardSize(moduleData, holder, item);
        loadBackground(moduleData, holder, item);
        updateContentDescription(moduleData, holder, item);

        holder.mCardView.getViewTreeObserver().removeOnDrawListener(holder.mOnDrawListener);

        holder.mOnDrawListener = new NetflixCardOnDrawListener(moduleData, holder, item);
        holder.mCardView.getViewTreeObserver().addOnDrawListener(holder.mOnDrawListener);
    }

    @Override
    public int getCardWidth(@Nullable ModuleData<?> moduleData, INetflixTile item) {
        float scaleFactor = mContext.getResources().getFraction(R.fraction.netflix_card_scale_factor, 1, 1);
        if (item.isProfileTile()) {
            return Math.round(mContext.getResources().getDimensionPixelOffset(R.dimen.netflix_profile_tile_card_width) * scaleFactor);
        }
        return Math.round(mContext.getResources().getDimensionPixelOffset(R.dimen.netflix_video_tile_card_width) * scaleFactor);
    }

    @Override
    public int getCardHeight(@Nullable ModuleData<?> moduleData, INetflixTile item) {
        float scaleFactor = mContext.getResources().getFraction(R.fraction.netflix_card_scale_factor, 1, 1);
        if (item.isProfileTile()) {
            return Math.round(mContext.getResources().getDimensionPixelOffset(R.dimen.netflix_profile_tile_card_height) * scaleFactor);
        }
        return Math.round(mContext.getResources().getDimensionPixelOffset(R.dimen.netflix_video_tile_card_height) * scaleFactor);
    }

    @Override
    public void onClicked(ModuleData<?> moduleData, ViewHolder holder, INetflixTile item) {
        super.onClicked(moduleData, holder, item);
        openNetflix(item);
    }

    @Override
    public long getStableId(INetflixTile item) {
        return item.getId().hashCode();
    }

    @Override
    public void onUnbindHolder(ModuleData<?> moduleData, ViewHolder holder) {
        super.onUnbindHolder(moduleData, holder);

        Glide.with(mContext).clear(holder.mBinding.netflixImage);
        holder.mBinding.netflixImage.setImageDrawable(null);

        if (holder.mOnDrawListener != null) {
            holder.mCardView.getViewTreeObserver().removeOnDrawListener(holder.mOnDrawListener);
        }
    }

    private void openNetflix(INetflixTile item) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getDeepLink()));
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        mPlaybackDetailsManager.startApplication(StartAction.Ui, PackageUtils.PACKAGE_NAME_NETFLIX, intent);
    }

    /**
     * Load background.
     */
    private void loadBackground(ModuleData<?> moduleData, ViewHolder holder, INetflixTile item) {
        DrawableImageViewTarget target = new DrawableImageViewTarget(holder.mBinding.netflixImage) {
            @Override
            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                super.onLoadFailed(errorDrawable);
                mListener.failedToLoadImage(item);
            }

            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                super.onResourceReady(resource, transition);
                holder.mBinding.netflixImage.setForeground(null);
            }
        };

        // Load background image.
        holder.mBinding.netflixImage.setBackgroundColor(mLayoutConfig.getPlaceHolderColor());
        if (ArrayUtils.isEmpty(item.getImageSources())) {
            mListener.failedToLoadImage(item);
        } else {
            Glide.with(holder.mBinding.netflixImage)
                    .load(item.getImageSources().get(0))
                    .apply(new RequestOptions()
                            .override(getCardWidth(moduleData, item), getCardHeight(moduleData, item))
                            .optionalCenterInside())
                    .transition(new DrawableTransitionOptions().dontTransition())
                    .into(target);
        }
    }

    /**
     * Calculate and apply the card size and scale animations.
     */
    private void updateCardSize(ModuleData<?> moduleData, ViewHolder holder, INetflixTile item) {
        float scaleFactor = mContext.getResources().getFraction(R.fraction.netflix_card_scale_factor, 1, 1);

        // Width
        int focusedCardWidth = getCardWidth(moduleData, item);
        int defaultCardWidth = Math.round(focusedCardWidth / scaleFactor);
        holder.mCardView.setCardWidth(focusedCardWidth);
        holder.mCardView.setMaxHorizontalPadding((focusedCardWidth - defaultCardWidth) / 2);

        // Height
        int focusedCardHeight = getCardHeight(moduleData, item);
        int defaultCardHeight = Math.round(focusedCardHeight / scaleFactor);
        holder.mCardView.setCardHeight(focusedCardHeight);
        holder.mCardView.setMaxVerticalPadding((focusedCardHeight - defaultCardHeight) / 2);

        // Animation
        holder.view.setStateListAnimator(createScaleStateListAnimator(holder.mCardView));
    }

    /**
     * Create focus state animator.
     */
    private StateListAnimator createScaleStateListAnimator(ResizableCardView cardView) {
        StateListAnimator stateListAnimator = new StateListAnimator();

        // Focused state
        AnimatorSet focusedAnimatorSet = new AnimatorSet();
        focusedAnimatorSet.playTogether(
                ObjectAnimator.ofInt(cardView, ResizableCardView.HORIZONTAL_PADDING, 0),
                ObjectAnimator.ofInt(cardView, ResizableCardView.VERTICAL_PADDING, 0),
                ObjectAnimator.ofFloat(cardView, View.TRANSLATION_Z, mFocusedCardElevation));
        stateListAnimator.addState(new int[]{android.R.attr.state_focused}, focusedAnimatorSet);

        // Default state
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(
                ObjectAnimator.ofInt(cardView, ResizableCardView.HORIZONTAL_PADDING, cardView.getMaxHorizontalPadding()),
                ObjectAnimator.ofInt(cardView, ResizableCardView.VERTICAL_PADDING, cardView.getMaxVerticalPadding()),
                ObjectAnimator.ofFloat(cardView, View.TRANSLATION_Z, mDefaultCardElevation));
        stateListAnimator.addState(new int[0], animatorSet);

        return stateListAnimator;
    }

    /**
     * Updates the content description used for Talkback.
     */
    protected void updateContentDescription(ModuleData<?> moduleData, ViewHolder holder, INetflixTile item) {
        if (!mMwRepository.isTalkBackOn()) {
            return;
        }

        if (moduleData == null || ArrayUtils.isEmpty(moduleData.getItems())) {
            return;
        }

        List<INetflixGroup> netflixGroups = (List<INetflixGroup>) moduleData.getItems();
        boolean isLoggedInOnNetflix = isLoggedInOnNetflix(netflixGroups);

        TalkBackMessageBuilder message;
        if (isLoggedInOnNetflix && item.isProfileTile()) {
            //Key used for Profile card when the user is logged in on Netflix.
            message = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_STRIPE_NETFLIX_PROFILE))
                    .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_PROFILE, getGroupTitle(netflixGroups, item));
        } else if ((isLoggedInOnNetflix && !item.isProfileTile())
                || (!isLoggedInOnNetflix && (Integer) holder.view.getTag(R.integer.normalized_adapter_position) == 0)) {
            //Key used for all the cards except the Profile card when the user is logged in on Netflix.
            //The same key is used for the first card of the stripe when the user is not logged in on Netflix.
            message = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_STRIPE_NETFLIX_FIRST_TILE))
                    .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_SWIMLANE, getGroupTitle(netflixGroups, item))
                    .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_ASSET, item.getTitle());
        } else {
            //Key used for the stripe's cards(except the first one) when the user is not logged in on Netflix.
            message = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_STRIPE_NETFLIX_OTHER_TILE))
                    .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_ASSET, item.getTitle());
        }

        holder.view.setContentDescription(message.toString());
    }

    /**
     * Checks if the user is logged in on Netflix.
     * @param list List of NetflixGroup.
     * @return True if the user is logged in on Netflix, otherwise false.
     */
    private boolean isLoggedInOnNetflix(List<INetflixGroup> list) {
        for (INetflixGroup group : list) {
            for (INetflixTile tile : group.getItems()) {
                if (tile.isProfileTile()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return The title of the group for thr specified item.
     */
    private String getGroupTitle(List<INetflixGroup> groupList, INetflixTile item) {
        for (INetflixGroup group : groupList) {
            List<INetflixTile> tileList = group.getItems();
            for (INetflixTile tile : ArrayUtils.notNull(tileList)) {
                if (tile.getId().equals(item.getId())) {
                    return group.getTitle();
                }
            }
        }
        return "";
    }

    /**
     * Used for measure the percentage of visibility on screen of a netflix tile card.
     * If this percentage is bigger then 50% we have to report a impression event.
     */
    private class NetflixCardOnDrawListener implements ViewTreeObserver.OnDrawListener {

        private final ViewHolder mHolder;
        private final INetflixTile mItem;
        private final int mTotalArea;
        private final Rect mClipRect = new Rect();

        private boolean isSent = false;

        private NetflixCardOnDrawListener(ModuleData<?> moduleData, ViewHolder holder, INetflixTile item) {
            mHolder = holder;
            mItem = item;
            mTotalArea = getCardWidth(moduleData, item) * getCardHeight(moduleData, item);
        }

        @Override
        public void onDraw() {
            if (isSent || !mHolder.mCardView.isShown()) {
                return;
            }

            mHolder.mCardView.getGlobalVisibleRect(mClipRect);
            int visibleArea = mClipRect.width() * mClipRect.height();
            int visiblePercent = (visibleArea * 100) / mTotalArea;

            if (visiblePercent >= 50) {
                mListener.reportImpression(mItem);
                isSent = true;
            }
        }
    }

    public interface NetflixCardListener {

        /**
         * Report a impression to the Netflix ninja when the item is viewed.
         */
        void reportImpression(INetflixTile item);

        /**
         * Remove the item if the image is missing.
         */
        void failedToLoadImage(INetflixTile item);
    }

    public static class ViewHolder extends BaseCardPresenter.ViewHolder {

        ResizableCardView mCardView;

        private final NetflixCardBinding mBinding;

        NetflixCardOnDrawListener mOnDrawListener;

        public ViewHolder(NetflixCardBinding binding) {
            super(binding.getRoot());

            mCardView = binding.getRoot();
            mBinding = binding;
        }
    }
}
