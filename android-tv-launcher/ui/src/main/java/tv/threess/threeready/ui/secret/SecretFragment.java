package tv.threess.threeready.ui.secret;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.config.model.FeatureControl;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.ui.databinding.SecretFragmentBinding;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.utils.SystemUtils;

public class SecretFragment extends BaseFragment {
    public static final String TAG = Log.tag(SecretFragment.class);

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);
    private final MwRepository mMwRepository = Components.get(MwRepository.class);
    protected final AccountRepository mAccountRepository = Components.get(AccountRepository.class);

    List<Disposable> mDisposableList = new ArrayList<>();

    private SecretFragmentBinding mBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mBinding = SecretFragmentBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        mBinding.appVersionTv.setText(String.format("App version : %s", mMwRepository.getAppVersion()));
        mBinding.environmentTv.setText(String.format("Environment : %s", mAccountRepository.getEnvironment()));

        // Extended page config switch.
        mBinding.pageConfigCb.setChecked(Settings.extendedPageConfig.get(
                Components.get(FeatureControl.class).extendedPageConfig())
        );
        mBinding.pageConfigCb.setOnCheckedChangeListener((buttonView, isChecked)
                -> Settings.extendedPageConfig.edit().put(isChecked));

        // Restart action.
        mBinding.restartButton.setOnClickListener(v -> {
            SystemUtils.restartApplication(getActivity());
            Log.i(TAG, "RESTART button clicked.");
        });

        // Open web view.
        mBinding.webviewButton.setOnClickListener(v -> {
            String url = mBinding.webviewEt.getText().toString();
            mNavigator.showSecretWebViewFragment(url);
        });

        mBinding.contentLanguageTv.setText("Content language : " + LocaleUtils.getApplicationLanguage());

        mBinding.standbyDuration.setText(String.valueOf(Settings.activeStandbyDuration.get(mAppConfig.getActiveStandbyTimeout(TimeUnit.MINUTES))));
        mBinding.saveStandby.setOnClickListener((v) -> Settings.activeStandbyDuration.edit().put(Integer.valueOf(mBinding.standbyDuration.getText().toString())));

        mBinding.internetCheckTimeout.setText(String.valueOf(Settings.internetCheckerTimeout.get(mAppConfig.getInternetCheckTimeout(TimeUnit.SECONDS))));
        mBinding.saveInternetCheckTimeoutButton.setOnClickListener(v ->
                Settings.internetCheckerTimeout.edit().put(Integer.valueOf(mBinding.internetCheckTimeout.getText().toString())));

        mBinding.deleteViewedHintsButton.setOnClickListener(v -> {
            Disposable disposable = mAccountRepository.deleteViewedHints()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> Log.d(TAG, "Viewed hints were successfully deleted."),
                            e -> Log.e(TAG, "Deleting viewed hints failed", e)
                    );
            mDisposableList.add(disposable);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        RxUtils.disposeSilently(mDisposableList);
    }
}
