package tv.threess.threeready.ui.settings.model;

import tv.threess.threeready.api.playback.model.TrackInfo;

/**
 * Radio button representing a selectable media track
 *
 * @author Karetka Mezei Zoltan
 */
public class MediaTrackRadioItem extends RadioButtonItem {

    private final TrackInfo mValue;

    public MediaTrackRadioItem(String title, TrackInfo value, String preferenceGroup) {
        super(title, value.getLanguage(), preferenceGroup);
        mValue = value;
    }

    public TrackInfo getTrackInfo() {
        return mValue;
    }

    @Override
    public String toString() {
        return "MediaTracRadioItem{" +
                "language = " + mValue.getLanguage() +
                ", descriptive = " + mValue.isDescriptive() +
                '}';
    }
}
