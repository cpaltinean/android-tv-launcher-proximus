package tv.threess.threeready.ui.generic.presenter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.leanback.widget.HorizontalGridView;
import androidx.recyclerview.widget.RecyclerView;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.databinding.SelectorOptionItemBinding;
import tv.threess.threeready.ui.generic.dialog.SelectorOptionDialog;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Adapter to display filter and sorting options.
 * <p>
 * Created by Noemi Dali on 06.08.2020.
 */
public class SelectorOptionsItemAdapter extends HorizontalGridView.Adapter<SelectorOptionsItemAdapter.ViewHolder> {

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final Translator mTranslator = Components.get(Translator.class);

    private final DataSourceSelector mSelector;

    private final SelectorOptionDialog.OnGridItemClickListener mItemClickListener;


    public SelectorOptionsItemAdapter(DataSourceSelector selector,
                                      SelectorOptionDialog.OnGridItemClickListener itemClickListener) {
        super();
        mSelector = selector;
        mItemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SelectorOptionItemBinding binding = SelectorOptionItemBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.sortOptionName.setBackground(UiUtils.createSortOptionBackground(parent.getContext(), mLayoutConfig));
        holder.mBinding.sortOptionName.setTextColor(UiUtils.createButtonBrandingColorStateList(mLayoutConfig));

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SelectorOption sortOption = mSelector.getOptions().get(position);
        String sortOptionName = mTranslator.get(sortOption.getName());
        holder.mBinding.sortOptionName.setText(sortOptionName);
        holder.itemView.setActivated(sortOption == mSelector.getSelectedOption());

        holder.itemView.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_COLLECTION_SORT_DIALOG))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BUTTON_NAME, sortOptionName)
                        .toString()
        );

        holder.itemView.setOnClickListener(v -> {
            if (mItemClickListener != null) {
                SelectorOption selectedOption = mSelector.getOptions().get(position);
                mItemClickListener.onItemClicked(selectedOption);

                mSelector.selectOption(position);
                notifyItemChanged(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSelector.getOptions().size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        
        private final SelectorOptionItemBinding mBinding;

        public ViewHolder(SelectorOptionItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
