package tv.threess.threeready.ui.epg.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.recyclerview.widget.RecyclerView;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.epg.presenter.TvChannelZapperCardPresenter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;

/**
 * Horizontal channel zapper grid with favorite channel cards.
 *
 * @author Barabas Attila
 * @since 2017.12.05
 */

public class FavoriteModeChannelZapperGridView extends ChannelZapperGridView {

    private boolean mIsFavoriteMode;

    private ViewHolder mSelectedHolder;

    public FavoriteModeChannelZapperGridView(Context context) {
        this(context, null);
    }

    public FavoriteModeChannelZapperGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FavoriteModeChannelZapperGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        addOnChildViewHolderSelectedListener(mOnChildViewHolderSelectedListener);
    }

    @Override
    public void onViewAdded(View child) {
        super.onViewAdded(child);

        setChildFavoriteState(child);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        removeOnChildViewHolderSelectedListener(mOnChildViewHolderSelectedListener);
    }

    @Override
    protected boolean shouldDrawChild(View child) {
        if (isFavoriteMode()) {
            return true;
        }

        return super.shouldDrawChild(child);
    }

    public void setFavoriteMode(boolean favoriteMode) {
        mIsFavoriteMode = favoriteMode;

        // Enable/disable scale animation for the selected item.
        if (mSelectedHolder != null) {
            mSelectedHolder.itemView.setSelected(favoriteMode);
        }

        // The activated is needed, because the selected child has a different background.
        View selectedChild = getSelectedChild();
        if (selectedChild != null) {
            selectedChild.setActivated(true);
            selectedChild.getStateListAnimator().jumpToCurrentState();
        }

        updateFavoriteStates();
    }

    public void updateFavoriteStates() {
        for (int i = 0; i < getChildCount(); i++) {
            setChildFavoriteState(getChildAt(i));
        }
    }


    /**
     * Set the hovered_state for a child view. Based on this state we set the heart icon visibility, which is displayed on cards.
     */
    private void setChildFavoriteState(View view) {
        if (getChildViewHolder(view) != null) {
            ItemBridgeAdapter.PresenterViewHolder presenterHolder = (ItemBridgeAdapter.PresenterViewHolder) getChildViewHolder(view);
            if (presenterHolder.getPresenter() instanceof TvChannelZapperCardPresenter) {
                TvChannelZapperCardPresenter channelZapperCardPresenter =
                        (TvChannelZapperCardPresenter) presenterHolder.getPresenter();

                // Update favorite icon.
                TvChannelZapperCardPresenter.ViewHolder holder = (TvChannelZapperCardPresenter.ViewHolder) presenterHolder.getViewHolder();
                TvChannel channel = (TvChannel) presenterHolder.getItem();
                channelZapperCardPresenter.updateFavoriteIcon(holder, channel);
            }
        }
    }


    public boolean isFavoriteMode() {
        return mIsFavoriteMode;
    }


    private final OnChildViewHolderSelectedListener mOnChildViewHolderSelectedListener = new OnChildViewHolderSelectedListener() {
        @Override
        public void onChildViewHolderSelected(RecyclerView parent, ViewHolder child, int position, int subposition) {
            super.onChildViewHolderSelected(parent, child, position, subposition);

            if (mSelectedHolder != null) {
                setSelectedChannelLogoVisibility(VISIBLE);
                mSelectedHolder.itemView.setSelected(false);
            }

            mSelectedHolder = child;

            if (mSelectedHolder != null) {
                mSelectedHolder.itemView.setSelected(mIsFavoriteMode);
            }

        }
    };

    /**
     * Show or hide the selected channel logo on the selected card.
     */
    public void setSelectedChannelLogoVisibility(int visible) {
        ViewHolder holder = mSelectedHolder;
        if (holder == null) {
            holder = getSelectedViewHolder();
        }
        ItemBridgeAdapter.PresenterViewHolder bridgeHolder = (ItemBridgeAdapter.PresenterViewHolder) holder;
        if (bridgeHolder != null) {
            Presenter.ViewHolder presenterHolder = bridgeHolder.getViewHolder();
            if (presenterHolder instanceof TvChannelZapperCardPresenter.ViewHolder) {
                ((TvChannelZapperCardPresenter.ViewHolder) bridgeHolder.getViewHolder()).getChannelLogoView().setVisibility(visible);
            }
        }
    }

}
