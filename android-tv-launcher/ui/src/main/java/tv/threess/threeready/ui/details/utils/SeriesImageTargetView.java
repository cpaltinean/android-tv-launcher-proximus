package tv.threess.threeready.ui.details.utils;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

/**
 * Base class for Glide image target(used to load images on series detail pages).
 *
 * @author Daniela Toma
 * @since 2022.04.04
 */
public class SeriesImageTargetView extends ImageTargetView {

    boolean mHideImage = false;
    boolean isSmallImage = false;

    public SeriesImageTargetView(Context context, ImageView gradientBottom, View fadingStart, ImageView bigCoverImage, ImageView coverImage) {
        super(context, gradientBottom, fadingStart, bigCoverImage, coverImage);
    }

    @Override
    protected void showTargetView() {
        if(isSmallImage && mHideImage) {
            targetView.setVisibility(View.GONE);
        } else {
            targetView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void loadCoverImage() {
        super.loadCoverImage();
        isSmallImage = true;
    }

    @Override
    protected void loadBigCoverImage() {
        super.loadBigCoverImage();
        isSmallImage = false;
    }

    public void hideImage() {
        mHideImage = true;
        getCoverImage().setVisibility(View.GONE);
    }

    public void showImage() {
        mHideImage = false;
        getCoverImage().setVisibility(View.VISIBLE);
    }
}
