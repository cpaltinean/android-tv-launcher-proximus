/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.tv.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.vectorchildfinder.VectorChildFinder;
import tv.threess.threeready.ui.generic.utils.vectorchildfinder.VectorDrawableCompat;

/**
 * Custom view for the player icons.
 * The selectors are created programmatically, the colors come from config.
 *
 * @author Szende Pal
 * @since 2017.05.16
 */
public class PlayerIconView extends AppCompatImageView {
    private static final String TAG = Log.tag(PlayerIconView.class);

    private ColorStateList mBackgroundTint;
    private ColorStateList mTint;
    protected int mResourceId;

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    public PlayerIconView(Context context) {
        super(context);
        initialize();
    }

    public PlayerIconView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public PlayerIconView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    protected void initialize() {
        //The colors come from config, the selectors will be created programmatically
        int[][] states = new int[][]{
                new int[]{android.R.attr.state_pressed},
                new int[]{android.R.attr.state_focused},
                new int[]{}
        };

        int[] colors = new int[]{
                mLayoutConfig.getFontColor(),
                mLayoutConfig.getFontColor(),
                Color.BLACK
        };

        mTint = new ColorStateList(states, colors);

        int[][] backgroundStates = new int[][]{
                new int[]{android.R.attr.state_pressed},
                new int[]{android.R.attr.state_focused},
                new int[]{}
        };

        int[] backgroundColors = new int[]{
                mLayoutConfig.getButtonPressedColorStart(),
                mLayoutConfig.getButtonFocusedColorStart(),
                getResources().getColor(R.color.player_icon_background, null)
        };

        mBackgroundTint = new ColorStateList(backgroundStates, backgroundColors);
    }

    protected void setResourceId(int resourceId) {
        mResourceId = resourceId;
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();

        //when the state is changed, the colors update
        if (mBackgroundTint != null
                && mBackgroundTint.isStateful() && mTint != null && mTint.isStateful()) {
            updateColor();
        }
    }

    /**
     * Update icon color based on the view state.
     */
    private void updateColor() {
        try {
            VectorChildFinder vector = new VectorChildFinder(getContext(), mResourceId, this);
            int background = mBackgroundTint.getColorForState(getDrawableState(), 0);
            int inner = mTint.getColorForState(getDrawableState(), 0);

            VectorDrawableCompat.VFullPath path = vector.findPathByName(
                    getContext().getString(R.string.icon_background));
            if (path != null) {
                if (background == mLayoutConfig.getButtonFocusedColorStart()) {
                    path.setFillColor(mLayoutConfig.getButtonFocusedColorStart(), mLayoutConfig.getButtonFocusedColorEnd());
                } else if (background == mLayoutConfig.getButtonPressedColorStart()) {
                    path.setFillColor(mLayoutConfig.getButtonPressedColorStart(), mLayoutConfig.getButtonPressedColorEnd());
                } else {
                    path.setFillColor(background);
                }
            }

            path = vector.findPathByName(getContext().getString(R.string.icon_inner));
            if (path != null) {
                path.setFillColor(inner);
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to update color.", e);
        }
    }
}
