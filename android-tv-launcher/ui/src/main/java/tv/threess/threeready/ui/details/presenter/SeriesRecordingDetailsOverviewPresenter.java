/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextSwitcher;

import androidx.annotation.NonNull;
import androidx.leanback.widget.HorizontalGridView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.PlayOption;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.pvr.model.SeriesRecordingStatus;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.databinding.SeriesRecordingDetailsOverlayBinding;
import tv.threess.threeready.ui.details.DetailOpenedFrom;
import tv.threess.threeready.ui.generic.adapter.DetailPageButtonOrder;
import tv.threess.threeready.ui.generic.dialog.AlertDialog;
import tv.threess.threeready.ui.generic.model.RecordingPlayOption;
import tv.threess.threeready.ui.generic.presenter.ActionModel;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.pvr.RecordingActionHelper;
import tv.threess.threeready.ui.utils.ImageTransform;

/**
 * Presenter class for the overview part of a series recording detail page.
 * CompletedSeries information about the selected episode and series.
 *
 * @author Barabas Attila
 * @since 2018.11.25
 */
public class SeriesRecordingDetailsOverviewPresenter
        extends SeriesDetailsOverviewPresenter<SeriesRecordingDetailsOverviewPresenter.ViewHolder, IRecording, IRecordingSeries> {
    private static final String TAG = Log.tag(SeriesRecordingDetailsOverviewPresenter.class);

    private final RecordingActionHelper mRecordingActionHelper = Components.get(RecordingActionHelper.class);
    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);
    private final TvRepository mTvRepository = Components.get(TvRepository.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);

    private final DetailOpenedFrom mOpenedFrom;

    public SeriesRecordingDetailsOverviewPresenter(Context context,
                                                   IRecordingSeries series, DetailOpenedFrom openedFrom) {
        super(context, series);
        mSeries = series;
        mOpenedFrom = openedFrom;
    }

    @Override
    protected BaseDetailsOverviewPresenter.ViewHolder createViewHolder(ViewGroup parent) {
        SeriesRecordingDetailsOverlayBinding binding = SeriesRecordingDetailsOverlayBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, IRecording item) {
        super.onBindHolder(holder, item);
        updateProviderLogo(holder, item);
        updateSeriesRecordingStatus(holder, item);
        updateBookmark(holder, item);
    }

    @Override
    public boolean onKeyEvent(ViewHolder holder, IRecording recording, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP && event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_RECORD) {
            mRecordingActionHelper.handleRecKeyAction(mSeries, holder.mSeriesRecordingStatus);
            return true;
        }

        return super.onKeyEvent(holder, recording, event);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);

        Glide.with(mContext).clear(holder.mBinding.providerLogo);

        RxUtils.disposeSilently(holder.mRecStatusDisposable, holder.mRecordingBookmarkDisposable);
    }

    @Override
    protected String getGenres(IRecording episode) {
        return TextUtils.join(", ",
                episode.getGenres().stream().limit(3).toArray());
    }

    @Override
    protected void updateMarkersRow(ViewHolder holder, IRecording item) {
        holder.mBinding.contentMarker.setVisibility(View.GONE);
    }

    @Override
    public void onBindHolder(ViewHolder holder, IRecording item, List<?> payLoads) {
        super.onBindHolder(holder, item, payLoads);
        updateBookmark(holder, item);
        updateInfoRow(holder, item);
        holder.mBinding.iconContainer.showRecordingStatus(mSeries, holder.mSeriesRecordingStatus);
    }

    private void addLastPlayedEpisodeButton(ViewHolder holder, IRecording broadcast, List<ActionModel> actions) {
        if (broadcast != null) {
            TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
            List<PlayOption> playOptions = RecordingPlayOption.generateRecordingPlayOptions(
                    channel, broadcast, mSeries, mOpenedFrom == DetailOpenedFrom.Recording);

            String buttonPrefix = TranslationKey.SCREEN_DETAIL_PLAY_BUTTON;

            if (mSeries.hasCompletedEpisode() && holder.mRecordingBookmark != null && holder.mRecordingBookmark.getTimestamp() != 0L) {
                buttonPrefix = TranslationKey.SCREEN_DETAIL_RESUME_BUTTON;
            }

            if (playOptions.size() > 0) {
                actions.add(new ActionModel(
                        0, 0,
                        mTranslator.get(buttonPrefix)
                                + " " + broadcast.getTitleWithSeasonEpisode(mTranslator, "", ""),
                        DetailPageButtonOrder.Watch, () -> {
                            if (channel == null || !channel.isUserTvSubscribedChannel()) {
                                mRecordingActionHelper.showChannelNotEntitledDialog();
                                return;
                            }

                            if (playOptions.size() == 1) {
                                if (playOptions.get(0) instanceof RecordingPlayOption) {
                                    RecordingPlayOption recordingPlayOption = (RecordingPlayOption) playOptions.get(0);
                                    mNavigator.showRecordingPlayer(StartAction.Ui, recordingPlayOption.getContentItem(), mSeries, null, true);
                                } else {
                                    mNavigator.showPlayer(playOptions.get(0), mOpenedFrom == DetailOpenedFrom.Favorite);
                                }
                                mRecordingActionHelper.showStartReplayWarningNotification(broadcast, channel);
                            } else {
                                mNavigator.showPlayOptionsDialog(playOptions,
                                        mOpenedFrom == DetailOpenedFrom.Favorite, broadcast);
                            }
                        }
                ));
            }
        }

        holder.mBinding.actionGrid.setVisibility(View.VISIBLE);
    }

    private void updateSeriesRecordingStatus(ViewHolder holder, IRecording broadcast) {
        holder.mBinding.iconContainer.hideRecordingStatus();
        holder.mRecStatusDisposable = mPvrRepository.getSeriesRecordingStatus(mSeries)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recordingStatus -> {
                    holder.mBinding.iconContainer.showRecordingStatus(mSeries, recordingStatus);
                    holder.mSeriesRecordingStatus = recordingStatus;
                    updateButtons(holder, broadcast, recordingStatus);
                }, throwable -> {
                    Log.e(TAG, "Couldn't get the recording status.", throwable);
                    holder.mBinding.iconContainer.hideRecordingStatus();
                });
    }

    private void updateBookmark(ViewHolder holder, IRecording broadcast) {
        holder.mRecordingBookmark = null;
        RxUtils.disposeSilently(holder.mRecordingBookmarkDisposable);
        holder.mRecordingBookmarkDisposable = mTvRepository.getBookmarkForBroadcast(broadcast,
                mOpenedFrom == DetailOpenedFrom.ContinueWatching)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        bookmark -> {
                            holder.mRecordingBookmark = bookmark;
                            updateButtons(holder, broadcast, holder.mSeriesRecordingStatus);
                        },
                        throwable -> {
                            Log.e(TAG, "Couldn't get the recording's bookmark.", throwable);
                            updateButtons(holder, broadcast, holder.mSeriesRecordingStatus);
                        });
    }

    private void updateProviderLogo(ViewHolder holder, IRecording broadcast) {
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        Glide.with(mContext).clear(holder.mBinding.providerLogo);
        Glide.with(mContext)
                .load(channel)
                .apply(ImageTransform.REQUEST_OPTIONS)
                .into(holder.mBinding.providerLogo);
    }

    @Override
    protected void updateButtons(ViewHolder holder, IRecording item) {
        updateButtons(holder, item, SeriesRecordingStatus.NONE);
    }

    private void updateButtons(ViewHolder holder, IRecording broadcast, SeriesRecordingStatus seriesRecordingStatus) {
        // Add play/resume episode option.
        List<ActionModel> actions = new ArrayList<>();

        // Channel not subscribed.
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        boolean isSubscribedChannel = channel != null && channel.isUserTvSubscribedChannel();

        updateWatchButton(holder, broadcast, actions);

        if (seriesRecordingStatus != null) {
            List<ActionModel> seriesActions = new ArrayList<>();

            // Series schedule. Show 'Cancel Series' button.
            if (seriesRecordingStatus == SeriesRecordingStatus.SCHEDULED) {
                ActionModel cancelSeriesAction = new ActionModel(
                        0, 0,
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_CANCEL_SERIES_BUTTON),
                        DetailPageButtonOrder.Cancel,
                        () -> mRecordingActionHelper.cancelSeriesRecording(broadcast, true, false)
                );
                seriesActions.add(cancelSeriesAction);
            }

            // Has completed episodes. Show 'Delete episodes' button.
            if (mSeries.hasCompletedEpisode()) {
                ActionModel deleteSeriesAction = new ActionModel(
                        0, 0,
                        mTranslator.get(TranslationKey.BUTTON_DELETE_SERIES),
                        DetailPageButtonOrder.Delete,
                        () -> mRecordingActionHelper.deleteSeriesRecording(mSeries, true)
                );
                seriesActions.add(deleteSeriesAction);
            }

            // Future or live episode. Show recording series.
            if (seriesRecordingStatus == SeriesRecordingStatus.NONE
                    && isSubscribedChannel && mSeries.hasFutureOrLiveEpisode()) {

                ActionModel recordSeriesAction = new ActionModel(
                        0, 0,
                        mTranslator.get(TranslationKey.BUTTON_RECORD_SERIES),
                        DetailPageButtonOrder.Record,
                        () -> mRecordingActionHelper.scheduleSeriesRecording(mSeries, false)
                );
                seriesActions.add(recordSeriesAction);
            }

            // Show manage recordings.
            if (seriesActions.size() > 1) {
                ActionModel manageRecordingAction = new ActionModel(
                        0, 0,
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_MANAGE_RECORDING_BUTTON),
                        DetailPageButtonOrder.Manage,
                        () -> openManagerRecordings(mSeries, seriesRecordingStatus, isSubscribedChannel)
                );
                actions.add(manageRecordingAction);
            } else {
                actions.addAll(seriesActions);
            }
        }

        List<PlayOption> playOptions = RecordingPlayOption.generateRecordingPlayOptions(
                channel, broadcast, mSeries, mOpenedFrom == DetailOpenedFrom.Recording);

        // show non-playable icon in case the episode is not playable
        updateNonPlayableViewGroupVisibility(holder,  !broadcast.isFailedRecording() || playOptions.size() > 0);

        updateActionModelDescriptions(actions, broadcast);

        holder.mAdapter.replaceAll(actions);

        if (!actions.isEmpty() && holder.view.hasFocus()) {
            holder.mBinding.actionGrid.requestFocus();
        }
    }

    void updateWatchButton(ViewHolder holder, IRecording item, List<ActionModel> actions) {
        if (mSeries.isCompleted()) {
            addLastPlayedEpisodeButton(holder, item, actions);
        } else {
            showLoadingButton(holder);
        }
    }

    private void openManagerRecordings(IRecordingSeries series,
                                       SeriesRecordingStatus seriesRecordingStatus, boolean isChannelSubscribed) {
        AlertDialog.Builder builder = new AlertDialog.Builder()
                .title(mTranslator.get(TranslationKey.MANAGE_RECORDING_ALERT_TITLE))
                .description(mTranslator.get(TranslationKey.MANAGE_RECORDING_ALERT_BODY))
                .dismissOnBtnClick(true)
                .setEvent(mRecordingActionHelper.getPageOpenEvent(series, UILog.Page.RecordActionSubMenuManage));

        // Show schedule series option.
        if (seriesRecordingStatus == SeriesRecordingStatus.NONE && isChannelSubscribed) {
            builder.addButton(mTranslator.get(TranslationKey.BUTTON_RECORD_SERIES), dialog
                    -> mRecordingActionHelper.scheduleSeriesRecording(
                    series, series.hasPartiallyRecordedLiveEpisode()));

            // Show cancel series option.
        } else {
            builder.addButton(mTranslator.get(TranslationKey.BUTTON_CANCEL_SERIES), dialog
                    -> mRecordingActionHelper.cancelSeriesRecording(series, true));
        }

        // Delete all episode option.
        if (series.hasCompletedEpisode()) {
            builder.addButton(mTranslator.get(TranslationKey.BUTTON_DELETE_SERIES),
                    dialog -> mRecordingActionHelper.deleteSeriesRecording(series, true));
        }

        // Show manage recording dialog.
        mNavigator.showDialogOnTop(builder.build());

    }

    public static class ViewHolder extends SeriesDetailsOverviewPresenter.ViewHolder {

        private final SeriesRecordingDetailsOverlayBinding mBinding;

        Disposable mRecStatusDisposable;

        Disposable mRecordingBookmarkDisposable;

        SeriesRecordingStatus mSeriesRecordingStatus;

        IBookmark mRecordingBookmark;

        public ViewHolder(SeriesRecordingDetailsOverlayBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        @NonNull
        @Override
        public HorizontalGridView getActionGridView() {
            return mBinding.actionGrid;
        }

        @NonNull
        @Override
        protected ImageView getNonPlayableIcon() {
            return mBinding.nonPlayableIcon;
        }

        @NonNull
        @Override
        protected FontTextView getNonPlayableTextView() {
            return mBinding.nonPlayableTextView;
        }

        @NonNull
        @Override
        protected View getNonPlayableOverlay() {
            return mBinding.nonPlayableOverview;
        }

        @NonNull
        @Override
        protected FontTextView getTitleView() {
            return mBinding.title;
        }

        @NonNull
        @Override
        protected FontTextView getInfoRow() {
            return mBinding.infoRow;
        }

        @NonNull
        @Override
        protected TextSwitcher getDescriptionView() {
            return mBinding.description;
        }

        @NonNull
        @Override
        protected ImageView getShowMoreBtn() {
            return mBinding.showMoreBtn;
        }

        @NonNull
        @Override
        protected FontTextView getCastView() {
            return mBinding.cast;
        }

        @NonNull
        @Override
        protected FontTextView getDirectorsView() {
            return mBinding.directors;
        }
    }
}
