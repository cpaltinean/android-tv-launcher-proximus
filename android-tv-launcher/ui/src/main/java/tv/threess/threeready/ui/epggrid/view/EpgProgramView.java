package tv.threess.threeready.ui.epggrid.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.view.BaseOrderedIconsContainer;
import tv.threess.threeready.ui.generic.view.FontTextView;

/**
 * Custom view where the display information about a program in the EPG grid
 * and keep the information on the visible area of the screen.
 *
 * @author Barabas Attila
 * @since 8/27/21
 */
public class EpgProgramView extends LinearLayout {

    private int mDefaultStartPadding;
    private int mDefaultEndPadding;

    private IBroadcast mBroadcast;
    protected FontTextView mTitleView;
    protected BaseOrderedIconsContainer mIconsContainer;

    public EpgProgramView(Context context) {
        this(context, null);
    }

    public EpgProgramView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EpgProgramView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public EpgProgramView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mIconsContainer = findViewById(R.id.icon_container);
        mTitleView = findViewById(R.id.title);

        mDefaultStartPadding = getPaddingStart();
        mDefaultEndPadding = getPaddingEnd();
    }

    public void setBroadcast(IBroadcast broadcast) {
        mBroadcast = broadcast;
    }

    @Nullable
    public IBroadcast getBroadcast() {
        return mBroadcast;
    }


    /**
     * Set start and end padding to the views
     * so the content of it remains on the visible area of the timeline.
     */
    void layoutVisibleArea(EpgTimeLineView timeLineView) {
        if (mBroadcast == null) {
            // Not bonded yet.
            return;
        }

        // Calculate start padding
        int scrolledStartPadding = timeLineView.convertMillisToPixel(timeLineView.getDurationInWindow(mBroadcast.getStart(), timeLineView.getFrom()));
        int startPadding = Math.max(0, scrolledStartPadding);
        mTitleView.setPaddingRelative(startPadding, 0, 0, 0);

        // Calculate end padding
        int scrolledEndPadding = timeLineView.convertMillisToPixel(timeLineView.getDurationInWindow(timeLineView.getTo(), mBroadcast.getEnd()));
        int endPadding = Math.max(mDefaultEndPadding, scrolledEndPadding);
        mIconsContainer.setPaddingRelative(0, 0, endPadding, 0);

    }
}
