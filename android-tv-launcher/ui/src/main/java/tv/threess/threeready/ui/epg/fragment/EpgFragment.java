/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.epg.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.IPlaybackDetailsCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.ui.epg.view.EpgView;
import tv.threess.threeready.ui.epg.view.ExpandableProgramGuideView;
import tv.threess.threeready.ui.generic.dialog.BaseDialogFragment;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.hint.HintManager;
import tv.threess.threeready.ui.generic.navigation.Navigator;

/**
 * Epg fragment which holds and dispatches the user events to the {@link EpgView}.
 *
 * @author Barabas Attila
 * @since 2017.04.26
 */

public class EpgFragment extends BaseFragment {
    public static final String TAG = Log.tag(EpgFragment.class);

    protected static final String EXTRA_CHANNEL_ID = "EXTRA_CHANNEL_ID";
    protected static final String EXTRA_CHANNEL_IS_FAVORITE = "EXTRA_CHANNEL_IS_FAVORITE";
    protected static final String EXTRA_EXPAND_PROGRAM_GUIDE = "EXTRA_EXPAND_PROGRAM_GUIDE";

    /**
     * Open miniEPG for a specific channel, and tune to it if needed.
     */
    public static EpgFragment newInstance(String channelId, boolean isFavorite, boolean expandProgramGuide) {
        Bundle args = new Bundle();
        args.putString(EXTRA_CHANNEL_ID, channelId);
        args.putBoolean(EXTRA_CHANNEL_IS_FAVORITE, isFavorite);
        args.putBoolean(EXTRA_EXPAND_PROGRAM_GUIDE, expandProgramGuide);

        EpgFragment fragment = new EpgFragment();
        fragment.setArguments(args);
        return fragment;
    }


    private final Navigator mNavigator = Components.get(Navigator.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);
    private final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final HintManager mHintManager = Components.get(HintManager.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    private EpgView mEpgView;
    private boolean mEnabledReporting = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserInactivityDetector.reset(mAppConfig.getZapperUiHideTimeout());
        mPlaybackDetailsManager.registerCallback(playbackDetailsCallback);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (mEpgView == null) {
            String selectedChannelId = getStringArgument(EXTRA_CHANNEL_ID);
            if (TextUtils.isEmpty(selectedChannelId)) {
                if (TextUtils.isEmpty(mPlaybackDetailsManager.getChannelId())) {
                    selectedChannelId = mNowOnTvCache.getLastPlayedChannelId();
                } else {
                    selectedChannelId = mPlaybackDetailsManager.getChannelId();
                }
            }

            boolean isFavoriteChannel = getBooleanArgument(EXTRA_CHANNEL_IS_FAVORITE);
            boolean expandProgramGuide = getBooleanArgument(EXTRA_EXPAND_PROGRAM_GUIDE);
            removeArgument(EXTRA_EXPAND_PROGRAM_GUIDE);

            mEpgView = new EpgView(getContext());
            mEpgView.initialize(selectedChannelId, isFavoriteChannel, expandProgramGuide);
            mEpgView.setTvGuideStateChangeListener(mTvGuideStateChangeListener);
            mEpgView.setReportListener(mOnReportListener);
            mHintManager.setEpgFragmentDialogListener(mDialogListener);
        }
        return mEpgView;
    }

    /**
     * the check is needed in case program guide is expended, otherwise when fragment appears will
     * close start inactivity again, no matter that #onTvGuideOpened was called
     */
    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
        if (mEpgView.isProgramGuideExpended()) {
            mUserInactivityDetector.stop();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mEnabledReporting = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mEpgView != null) {
            mEpgView.onDestroy();
        }
        mPlaybackDetailsManager.unregisterCallback(playbackDetailsCallback);
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (mEpgView != null) {
            mEpgView.onUserInteraction();
        }
    }

    protected void onUserInactivity() {
        reportPageSelectionByTimeout();
        // If hint doesn't show, hide EPG on user inactivity for 10 minutes
        if (mNavigator.getContentFragment() instanceof EpgFragment) {
            mNavigator.hideContentOverlayWithFadeOut();
        }
    }

    private final ExpandableProgramGuideView.TvGuideStateChangeListener mTvGuideStateChangeListener = new ExpandableProgramGuideView.TvGuideStateChangeListener() {
        @Override
        public void onTvGuideClosed() {
            mUserInactivityDetector.start();
            mHintManager.hideEpgLayoutHints();
            mEnabledReporting = true;
            reportNavigation();
        }

        @Override
        public void onTvGuideOpened() {
            mUserInactivityDetector.stop();
            mEnabledReporting = true;
            reportNavigation();
        }
    };

    private final ExpandableProgramGuideView.OnReportListener mOnReportListener = this::reportNavigation;

    private final BaseDialogFragment.DialogListener mDialogListener = new BaseDialogFragment.DialogListener() {
        @Override
        public void onStart(BaseDialogFragment dialog) {
            mUserInactivityDetector.stop();
        }

        @Override
        public void onDismiss(BaseDialogFragment dialog) {
            mUserInactivityDetector.start();
        }
    };

    @Override
    public void onContentShown() {
        super.onContentShown();
        if (mEpgView != null) {
            mEpgView.onContentShown();
        }

        restoreFocus();
    }

    @Override
    public void onContentHide() {
        super.onContentHide();
        mEnabledReporting = true;
    }

    @Override
    public String getPageId() {
        return "MINI_EPG";
    }

    public void onChannelSwitch(String channelId, boolean isFavorite) {
        mEpgView.onChannelSwitched(channelId, isFavorite);
    }

    public void collapseTvGuide() {
        mEpgView.collapseTvGuide();
    }

    public void openTvGuide(boolean expandTvGuide) {
        mEpgView.openTvGuide(expandTvGuide);
    }

    private final IPlaybackDetailsCallback playbackDetailsCallback = new IPlaybackDetailsCallback() {
        @Override
        public void onPlayerDataUpdate(IBroadcast broadcast) {
            mEpgView.onCurrentProgramChanged(broadcast);
        }
    };

    @Override
    public void handleBackgroundChange(View view) {
        view.setBackgroundColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getBackgroundColor(), 0.8f));
    }

    @Override
    public void onAllDialogDismissed() {
        // Two inactivity detector exist, first in the base fragment, second in the program guide view.
        // These two detector are set for different timeout. When one is working the other one is stopped.
        if (mEpgView != null && !mEpgView.isProgramGuideExpended()) {
            mUserInactivityDetector.start();
        }
    }

    @Override
    public void reportNavigation() {
        if (!mEnabledReporting) {
            return;
        }

        Event<?, ?> event = generatePageOpenEvent();
        if (event == null) {
            // Nothing to report.
            return;
        }

        Log.event(event);
        mEnabledReporting = false;
    }

    @Nullable
    @Override
    protected Event<?, ?> generatePageOpenEvent() {
        if (mEpgView == null) {
            Log.d(TAG, "Reporting failed because epg view is not exist.");
            return null;
        }

        TvChannel selectedChannel = mEpgView.getChannelZapperView().getSelectedChannel();
        IContentItem selectedContentItem = mEpgView.getProgramGuideView().getLastSelectedProgram();

        if (selectedChannel == null || selectedChannel.getId() == null) {
            Log.d(TAG, "Log page open event unsuccessful. Channel is null or channel identifier is null!");
            return null;
        }

        if (selectedContentItem == null) {
            Log.d(TAG, "Log page open event unsuccessful. " +
                    "Ongoing content item on channel is null.");
            return null;
        }

        return new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                .addDetail(UILogEvent.Detail.Page, mEpgView.isTvGuideExpanded() ? UILog.Page.MiniEPGPage : UILog.Page.ZapListPage)
                .addDetail(UILogEvent.Detail.PageName, selectedChannel.getCallLetter())
                .addDetail(UILogEvent.Detail.PageId, selectedChannel.getId())
                .addDetail(UILogEvent.Detail.FocusName, selectedContentItem.getTitle())
                .addDetail(UILogEvent.Detail.FocusId, selectedContentItem.getId());
    }
}
