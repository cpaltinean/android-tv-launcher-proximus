package tv.threess.threeready.ui.generic.notification;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import tv.threess.threeready.ui.databinding.LiveNotificationBinding;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Class for displaying a live notification in the bottom right corner of the screen.
 *
 * @author Zsolt Bokor
 * @since 2020.03.13
 */
public class LiveNotification extends BaseNotification {

    private LiveNotificationBinding mBinding;

    public LiveNotification() {
        mTimeOut = Timeout.LENGTH_SHORT;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = LiveNotificationBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        mBinding.title.setText(mTranslator.get(TranslationKey.SCREEN_PLAYER_LIVE));
    }

    @Override
    public boolean isDisplayed(BaseNotification current) {
        return current instanceof LiveNotification;
    }

    @Override
    public boolean skippDisplayQueue() {
        return true;
    }

    @Override
    public long getPriority() {
        return DEFAULT_PRIORITY;
    }
}
