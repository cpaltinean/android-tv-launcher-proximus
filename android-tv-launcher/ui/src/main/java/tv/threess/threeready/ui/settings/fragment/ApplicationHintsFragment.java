package tv.threess.threeready.ui.settings.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.hint.HintManager;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.settings.model.ISettingsItem;
import tv.threess.threeready.ui.settings.model.SettingsItem;

/**
 * Fragment to display tips and hints
 *
 * @author Solyom Zsolt
 * @since 2020.05.25
 */
public class ApplicationHintsFragment extends BaseSettingsDialogFragment<SettingsItem> {
    private static final String TAG = Log.tag(ApplicationHintsFragment.class);

    private final HintManager mHintManager = Components.get(HintManager.class);

    public static ApplicationHintsFragment newInstance() {
        return new ApplicationHintsFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.settings_left_frame_color));
    }

    protected void initView() {
        List<SettingsItem> items = new ArrayList<>();

        SettingsItem internetHints = new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_HINTS_CONNECTIVITY_ISSUES));
        internetHints.setPreferenceGroup(TranslationKey.SETTINGS_HINTS_CONNECTIVITY_ISSUES);
        items.add(internetHints);

        SettingsItem channelZapperHints =
                new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_HINTS_DISPLAY_PLAYER_UI_CHANNEL_ZAPPER));
        channelZapperHints.setPreferenceGroup(TranslationKey.SETTINGS_HINTS_DISPLAY_PLAYER_UI_CHANNEL_ZAPPER);
        items.add(channelZapperHints);

        SettingsItem openManageChannelHints =
                new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_HINTS_OPEN_MANAGE_CHANNELS));
        openManageChannelHints.setPreferenceGroup(TranslationKey.SETTINGS_HINTS_OPEN_MANAGE_CHANNELS);
        items.add(openManageChannelHints);

        SettingsItem manageChannelHints =
                new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_HINTS_ADD_REMOVE_FAV_CHANNELS));
        manageChannelHints.setPreferenceGroup(TranslationKey.SETTINGS_HINTS_ADD_REMOVE_FAV_CHANNELS);
        items.add(manageChannelHints);

        SettingsItem miniEpgHints =
                new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_HINTS_JUMP_DAY_MINI_EPG));
        miniEpgHints.setPreferenceGroup(TranslationKey.SETTINGS_HINTS_JUMP_DAY_MINI_EPG);
        items.add(miniEpgHints);

        SettingsItem playerUIButtonHints =
                new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_HINTS_PLAYER_UI_BUTTONS));
        playerUIButtonHints.setPreferenceGroup(TranslationKey.SETTINGS_HINTS_PLAYER_UI_BUTTONS);
        items.add(playerUIButtonHints);

        SettingsItem trickPlayHints =
                new SettingsItem(mTranslator.get(TranslationKey.SETTINGS_HINTS_TRICKPLAY));
        trickPlayHints.setPreferenceGroup(TranslationKey.SETTINGS_HINTS_TRICKPLAY);
        items.add(trickPlayHints);
        setItems(items);
    }

    @Override
    protected void setTitle() {
        getTitleView().setText(mTranslator.get(TranslationKey.SETTINGS_HINTS));
    }

    @Override
    public void onItemClick(ISettingsItem clickedItem) {
        super.onItemClick(clickedItem);
        switch (clickedItem.getPreferenceGroup()) {
            case TranslationKey.SETTINGS_HINTS_CONNECTIVITY_ISSUES:
                mHintManager.showInternetConnectionHintsFromSettings();
                break;
            case TranslationKey.SETTINGS_HINTS_DISPLAY_PLAYER_UI_CHANNEL_ZAPPER:
                mHintManager.showChannelZapperHintsFromSettings();
                break;
            case TranslationKey.SETTINGS_HINTS_OPEN_MANAGE_CHANNELS:
                mHintManager.showOpenManageChannelHintsFromSettings();
                break;
            case TranslationKey.SETTINGS_HINTS_ADD_REMOVE_FAV_CHANNELS:
                mHintManager.showManageChannelHintsFromSettings();
                break;
            case TranslationKey.SETTINGS_HINTS_JUMP_DAY_MINI_EPG:
                mHintManager.showMiniEpgHintsFromSettings();
                break;
            case TranslationKey.SETTINGS_HINTS_PLAYER_UI_BUTTONS:
                mHintManager.showPlayerUIButtonHintsFromSettings();
                break;
            case TranslationKey.SETTINGS_HINTS_TRICKPLAY:
                mHintManager.showTrickPlayHintsFromSettings();
                break;
            default:
                Log.d(TAG, "Invalid hint type: " + clickedItem.getPreferenceGroup());
        }
    }

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        return new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                .addDetail(UILogEvent.Detail.Page, UILog.Page.Tips);
    }

}
