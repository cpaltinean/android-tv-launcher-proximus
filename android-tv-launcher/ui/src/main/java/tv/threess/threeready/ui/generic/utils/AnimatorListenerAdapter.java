/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.utils;

import android.animation.Animator;

/**
 * This adapter class provides empty implementations of the methods from {@link android.animation.Animator.AnimatorListener}.
 * Any custom listener that cares only about a subset of the methods of this listener can
 * simply subclass this adapter class instead of implementing the interface directly.
 *
 * @author Barabas Attila
 * @since 2017.10.11
 */
public class AnimatorListenerAdapter implements Animator.AnimatorListener, Animator.AnimatorPauseListener {

    private boolean mCanceled;


    @Override
    public void onAnimationCancel(Animator animation) {
        mCanceled = true;
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        if (!mCanceled) {
            onAnimationFinished(animation);
        }
    }

    /**
     * Called when the animation normally finished, not by a cancel.
     */
    public void onAnimationFinished(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {
    }


    @Override
    public void onAnimationStart(Animator animation) {
        mCanceled = false;
    }

    @Override
    public void onAnimationPause(Animator animation) {
    }

    @Override
    public void onAnimationResume(Animator animation) {
    }
}
