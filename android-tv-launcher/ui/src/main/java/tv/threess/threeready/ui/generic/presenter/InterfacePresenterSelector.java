package tv.threess.threeready.ui.generic.presenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;

/**
 * Presenter selector that supports both selection by class and selection by interfaces.
 *
 * @author Andor Lukacs
 * @since 10/4/18
 */
public class InterfacePresenterSelector extends PresenterSelector {

    private final List<Presenter> mPresenters = new ArrayList<>();
    private final HashMap<Class<?>, Object> mClassMap = new HashMap<>();

    public InterfacePresenterSelector() {
    }

    public InterfacePresenterSelector addClassPresenter(Class<?> cls, Presenter presenter) {
        mClassMap.put(cls, presenter);
        if (!this.mPresenters.contains(presenter)) {
            this.mPresenters.add(presenter);
        }

        return this;
    }

    public InterfacePresenterSelector addClassPresenterSelector(Class<?> cls, PresenterSelector presenterSelector) {
        mClassMap.put(cls, presenterSelector);
        Presenter[] innerPresenters = presenterSelector.getPresenters();

        for (int i = 0; i < innerPresenters.length; ++i) {
            if (!this.mPresenters.contains(innerPresenters[i])) {
                this.mPresenters.add(innerPresenters[i]);
            }
        }

        return this;
    }

    public Presenter getPresenter(Object item) {
        List<Class> classList = new ArrayList<>();


        Class<?> cls = item.getClass();
        Object presenter = null;

        // Set up a list with the class hierarchy
        do {
            if(cls != null) {
                classList.add((cls));
                cls = cls.getSuperclass();
            }
        } while (cls != null);

        // Set up a list with the interface hierarchy
        int i = 0;
        do {
            if(classList.get(i) !=null) {
                classList.addAll(Arrays.asList(classList.get(i).getInterfaces()));
            }
            ++i;
        } while (classList.size() > i);

        for (Class classInterface : classList) {
            if(presenter == null) {
                presenter = mClassMap.get(classInterface);
                if (presenter instanceof PresenterSelector) {
                    Presenter innerPresenter = ((PresenterSelector) presenter).getPresenter(item);
                    if (innerPresenter != null) {
                        return innerPresenter;
                    }
                }
            } else {
                return (Presenter) presenter;
            }
        }

        return (Presenter) presenter;
    }

    public Presenter[] getPresenters() {
        return mPresenters.toArray(new Presenter[this.mPresenters.size()]);
    }
}
