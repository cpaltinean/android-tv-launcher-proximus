package tv.threess.threeready.ui.generic.notification;

import android.os.Bundle;

import java.util.Queue;

/**
 * Notification that has to be shown only once.
 * @author Daniela Toma
 * @since 27.09.2022
 */
public class SingleNotification extends Notification {

    public static SingleNotification newInstance(NotificationModel model) {
        SingleNotification notification = new SingleNotification();
        notification.mModel = model;

        Bundle arguments = new Bundle();
        arguments.putSerializable(NOTIFICATION_MODEL_EXTRA, model);

        notification.setArguments(arguments);
        return notification;
    }

    @Override
    public boolean showIfEnqueued(Queue<BaseNotification> queued) {
        for (BaseNotification notification : queued) {
            if (notification instanceof Notification) {
                if (mModel.equals(((Notification) notification).getModel())) {
                    return false;
                }
            }
        }
        return super.showIfEnqueued(queued);
    }

    public static class Builder extends Notification.Builder {

        @Override
        public SingleNotification build() {
            return SingleNotification.newInstance(new NotificationModel(mTitle, mSubtext, mIconId));
        }
    }
}
