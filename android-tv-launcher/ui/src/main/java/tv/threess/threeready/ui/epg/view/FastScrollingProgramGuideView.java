package tv.threess.threeready.ui.epg.view;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import java.util.concurrent.TimeUnit;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.AnimatorListenerAdapter;

/**
 * Expandable program guide view which enters in fast scrolling mode
 * when the user scrolls the EPG for more then 2 seconds.
 *
 * @author Barabas Attila
 * @since 2017.12.04
 */

public abstract class FastScrollingProgramGuideView extends ExpandableProgramGuideView {

    private boolean mIsFastScrolling;

    private long mFirstEventPressedTime;

    public FastScrollingProgramGuideView(Context context) {
        this(context, null);
    }

    public FastScrollingProgramGuideView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FastScrollingProgramGuideView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public FastScrollingProgramGuideView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @NonNull
    protected abstract FastScrollingView getFastScrollingView();

    @Override
    public void onSelectedChannelChanged(@NonNull TvChannel channel, boolean isFromFavoriteSection) {
        super.onSelectedChannelChanged(channel, isFromFavoriteSection);
        getFastScrollingView().displayChannelLogo(channel);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        getGridView().addOnScrollListener(mOnScrollListener);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        getGridView().removeOnScrollListener(mOnScrollListener);
    }

    @Override
    protected void onSelectedTimeChanged(long time, IBroadcast program) {
        super.onSelectedTimeChanged(time, program);
        getFastScrollingView().setTime(time == LIVE_PROGRAM_TIMESTAMP
                ? System.currentTimeMillis() : time);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_UP:
                case KeyEvent.KEYCODE_DPAD_DOWN:
                    if (mFirstEventPressedTime == 0) {
                        mFirstEventPressedTime = System.currentTimeMillis();
                    }

                    if (System.currentTimeMillis()
                            - TimeUnit.SECONDS.toMillis(2) > mFirstEventPressedTime) {
                        onFastScrollingState();
                    }
                    break;
            }

        } else {
            mFirstEventPressedTime = 0;
            onDefaultScrollingState();
        }

        return super.dispatchKeyEvent(event);
    }

    private void onFastScrollingState() {
        if (mIsFastScrolling) {
            // Nothing to do. Already in fast scrolling mode.
            return;
        }

        mIsFastScrolling = true;
        Animator fadeAnimator = getProgramGuideGridFadeOutAnimator();
        fadeAnimator.setDuration(getContext().getResources().getInteger(R.integer.content_frame_fade_duration));
        fadeAnimator.start();

        FastScrollingView fastScrollingView = getFastScrollingView();
        fastScrollingView.setVisibility(View.VISIBLE);
        fastScrollingView.requestFocus();
    }

    private void onDefaultScrollingState() {
        if (!mIsFastScrolling) {
            // Nothing to do. Already in default scrolling state.
            return;
        }

        onSelectedTimeChanged(getFastScrollingView().getTime(), null);
        selectCurrentProgram();
        getGridView().requestFocus();

        mIsFastScrolling = false;
        Animator fadeAnimator = getProgramGuideGridFadeInAnimator();
        fadeAnimator.setDuration(getContext().getResources().getInteger(R.integer.content_frame_fade_duration));
        fadeAnimator.start();

        getFastScrollingView().setVisibility(View.GONE);
    }

    protected Animator getProgramGuideGridFadeOutAnimator() {
        // Cancel previous animation.
        ObjectAnimator animator = (ObjectAnimator) getTag(R.integer.animation_info);
        if (animator != null
                && animator.isRunning()) {
            animator.cancel();
        }

        ExpandableProgramGuideGridView gridView = getGridView();
        gridView.setChannelLogoVisibility(GONE);
        ObjectAnimator fadeAnimator = ObjectAnimator.ofFloat(gridView, View.ALPHA, 1f, 0f);
        setTag(R.integer.animation_info, fadeAnimator);
        return fadeAnimator;
    }

    protected Animator getProgramGuideGridFadeInAnimator() {
        // Cancel previous animation.
        ObjectAnimator animator = (ObjectAnimator) getTag(R.integer.animation_info);
        if (animator != null
                && animator.isRunning()) {
            animator.cancel();
        }

        ExpandableProgramGuideGridView gridView = getGridView();
        gridView.setChannelLogoVisibility(GONE);
        ObjectAnimator fadeAnimator = ObjectAnimator.ofFloat(gridView, View.ALPHA, 0f, 1f);
        setTag(R.integer.animation_info, fadeAnimator);
        fadeAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                gridView.setChannelLogoVisibility(VISIBLE);
            }
        });

        return fadeAnimator;
    }

    // Scroll listener to update the time on the fast scrolling view.
    private final RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            IBroadcast selectedProgram = getSelectedProgram();
            if (selectedProgram != null && mIsFastScrolling) {
                long time = selectedProgram.getStart();
                getFastScrollingView().setTime(time == LIVE_PROGRAM_TIMESTAMP
                        ? System.currentTimeMillis() : time);
            }
        }
    };

}
