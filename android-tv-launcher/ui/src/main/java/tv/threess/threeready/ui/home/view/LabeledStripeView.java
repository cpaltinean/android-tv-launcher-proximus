/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.home.view;

import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.HorizontalGridView;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.LabeledStripeLayoutBinding;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeItemSpaceDecorator;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeLabel;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeSectionSeparatorDecorator;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeSeparatorProvider;
import tv.threess.threeready.ui.generic.view.FontTextView;

/**
 * Horizontal grid view which can display a label above a specified area in the grid.
 *
 * @author Barabas Attila
 * @since 2017.05.17
 */
public class LabeledStripeView extends FrameLayout {
    private static final String TAG = Log.tag(LabeledStripeView.class);

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    private TreeSet<LabelView> mLabelViews = new TreeSet<>(LabelView.POSITION_COMPARATOR);
    private ItemBridgeAdapter mAdapter;
    private StripeSeparatorProvider<StripeLabel> mLabelProvider;

    public LabeledStripeLayoutBinding mBinding;
    private List<StripeLabel> mLabels = new ArrayList<>();

    StripeSectionSeparatorDecorator mSectionSeparatorDecorator;

    private final int mLabelLayout;

    private boolean mHideSelected;
    private int mLabelMarginStart;

    public LabeledStripeView(Context context) {
        this(context, null);
    }

    public LabeledStripeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LabeledStripeView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public LabeledStripeView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mLabelLayout = R.layout.stripe_label_view;

        if (attrs != null) {
            TypedArray attr = context.getTheme().obtainStyledAttributes(attrs, R.styleable.LabeledStripeView, 0, 0);
            try {
                mHideSelected = attr.getBoolean(R.styleable.LabeledStripeView_hide_selected, false);
                mLabelMarginStart = attr.getDimensionPixelOffset(R.styleable.LabeledStripeView_label_margin_start, 0);
            } finally {
                attr.recycle();
            }
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mBinding = LabeledStripeLayoutBinding.inflate(LayoutInflater.from(getContext()), this);

        mBinding.itemGrid.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                invalidateLabelViews();
            }
        });

        mBinding.itemGrid.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> postInvalidateLabelViews());
        mBinding.itemGrid.setOnChildLaidOutListener((parent, view, position, id) -> postInvalidateLabelViews());
        mBinding.itemGrid.addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(@NonNull View view) {
                view.setActivated(isActivated());
                invalidateLabelViews();
            }

            @Override
            public void onChildViewDetachedFromWindow(@NonNull View view) {
                invalidateLabelViews();
            }
        });

        if (!mLabels.isEmpty()) {
            mSectionSeparatorDecorator = new StripeSectionSeparatorDecorator();
            mBinding.itemGrid.addItemDecoration(mSectionSeparatorDecorator);
        }

        mBinding.itemGrid.addItemDecoration(new StripeItemSpaceDecorator());
    }

    /**
     * Recycled view pools allow multiple RecyclerViews to share a common pool of scrap views.
     * This can be useful if you have multiple RecyclerViews with adapters that use the same
     * view types, for example if you have several data sets with the same kinds of item views
     * displayed by a {@link ViewPager ViewPager}.
     *
     * @param pool Pool to set. If this parameter is null a new pool will be created and used.
     */
    public void setRecyclerViewPool(RecyclerView.RecycledViewPool pool) {
        mBinding.itemGrid.setRecycledViewPool(pool);
    }

    /**
     * The activated state is needed, because we decide by this state that the card has an overlay or not.
     */
    @Override
    public void requestChildFocus(View child, View focused) {
        setActivated(true);
        super.requestChildFocus(child, focused);
    }

    @Override
    public View focusSearch(View focused, int direction) {
        View view = super.focusSearch(focused, direction);
        if (mBinding.itemGrid.indexOfChild(view) == NO_POSITION) {
            setActivated(false);
        }
        return view;
    }

    @Override
    public void setActivated(boolean activated) {
        super.setActivated(activated);

        int tintColor = getResources().getColor(activated ? R.color.label_stripe_view_separator_activated : R.color.label_stripe_view_separator_default, null);

        for (StripeLabel label : mLabels) {
            if (label.getSeparatorDrawable() != null) {
                label.getSeparatorDrawable().setTint(tintColor);
            }
        }
    }

    /**
     * Add OnChildViewHolderSelectedListener to the grid view.
     */
    public void addOnChildViewHolderSelectedListener(OnChildViewHolderSelectedListener onChildViewHolderSelectedListener) {
        mBinding.itemGrid.setOnChildViewHolderSelectedListener(onChildViewHolderSelectedListener);
    }

    public ItemBridgeAdapter getAdapter() {
        return mAdapter;
    }

    /**
     * Set adapter for the grid view.
     */
    public void setAdapter(ItemBridgeAdapter adapter, StripeSeparatorProvider<StripeLabel> labelProvider) {
        // Unregister previous data source observer.
        if (mAdapter != null) {
            mAdapter.unregisterAdapterDataObserver(mAdapterDataObserver);
        }

        mAdapter = adapter;
        mLabelProvider = labelProvider;

        // Observe data source changes
        if (adapter != null) {
            adapter.registerAdapterDataObserver(mAdapterDataObserver);
        }

        mBinding.itemGrid.setAdapter(adapter);

        if (mSectionSeparatorDecorator != null) {
            mSectionSeparatorDecorator.setAdapter(adapter);
        }

        if (adapter != null) {
            updateLabels();
        }
    }

    public void updateLabelProvider(StripeSeparatorProvider<StripeLabel> labelProvider) {
        mLabelProvider = labelProvider;
        updateLabels();
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);

        if (hasWindowFocus) {
            invalidateLabelViews();
        }
    }

    // Receive notification about data set changes.
    private final RecyclerView.AdapterDataObserver mAdapterDataObserver = new RecyclerView.AdapterDataObserver() {

        @Override
        public void onChanged() {
            super.onChanged();
            updateLabels();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
            updateLabels();
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            super.onItemRangeMoved(fromPosition, toPosition, itemCount);
            updateLabels();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            updateLabels();
        }
    };

    /**
     * Set the window alignment offset percent for grid.
     */
    public void setWindowAlignmentOffsetPercent(float windowAlignment) {
        mBinding.itemGrid.setWindowAlignmentOffsetPercent(windowAlignment);
    }

    public void setWindowAlignment(int windowAlignment) {
        mBinding.itemGrid.setWindowAlignment(windowAlignment);
    }

    public void setWindowAlignmentOffset(int offset) {
        mBinding.itemGrid.setWindowAlignmentOffset(offset);
    }

    /**
     * Set the item spacing for the gird.
     */
    public void setItemSpacing(int cardSpacing) {
        mBinding.itemGrid.setItemSpacing(cardSpacing);
    }

    /**
     * Set padding for the gird.
     */
    public void setGridPadding(int left, int top, int right, int bottom) {
        mBinding.itemGrid.setPadding(left, top, right, bottom);
    }

    public HorizontalGridView getGridView() {
        return mBinding.itemGrid;
    }

    /**
     * Custom text view which displays a label.
     */
    public static class LabelView extends FontTextView {
        int mStartPosition;
        int mEndPosition;

        public LabelView(Context context) {
            super(context);
        }

        public LabelView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public LabelView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }

        public int getStartPosition() {
            return mStartPosition;
        }

        public static Comparator<LabelView> POSITION_COMPARATOR =
                Comparator.comparingInt(LabelView::getStartPosition);
    }

    /**
     * Remove and redisplay all the labels.
     */
    private void updateLabels() {
        clearLabelViews();
        mLabels = mLabelProvider.getSeparators();
        if (mSectionSeparatorDecorator != null) {
            mSectionSeparatorDecorator.setSeparators(mLabels);
        }
        postInvalidateLabelViews();
    }

    /**
     * Remove all displayed label.
     */
    private void clearLabelViews() {
        for (LabelView labelView : mLabelViews) {
            removeView(labelView);
        }
        mLabelViews.clear();
        Log.d(TAG, "Clear label views.");
    }

    private void postInvalidateLabelViews() {
        post(this::invalidateLabelViews);
    }

    /**
     * Recalculate the position for all label.
     */
    public void invalidateLabelViews() {
        if (mAdapter == null) {
            return;
        }

        int childCount = getGridView().getChildCount();
        for (int i = 0; i < childCount; ++i) {
            View child = getGridView().getChildAt(i);
            if (child == null) {
                continue;
            }

            RecyclerView.ViewHolder holder = getGridView().getChildViewHolder(child);
            if (holder == null) {
                continue;
            }

            LabelView labelView = getLabelViewForPosition(holder.getBindingAdapterPosition());
            if (labelView == null) {
                continue;
            }

            invalidateLabel(labelView);
        }

        clearHiddenLabels();
    }

    /**
     * Set the activated state of the selected label view.
     */
    public void setSelectedLabel(int position) {
        LabelView[] labelArray = mLabelViews.toArray(new LabelView[0]);
        for (int i = 0; i < labelArray.length; ++i) {
            labelArray[i].setActivated(i == position);
        }
    }

    /**
     * Avoid too much attached views.
     * Remove the hidden ones.
     */
    private void clearHiddenLabels() {
        if (!mAdapter.isCircular() || isInLayout()) {
            // Limit label count. No need for cleanup.
            return;
        }

        // Remove hidden label views.
        TreeSet<LabelView> labelViews = new TreeSet<>(LabelView.POSITION_COMPARATOR);
        for (final LabelView labelView : mLabelViews) {
            invalidateLabel(labelView);
            if (labelView.getVisibility() == VISIBLE) {
                labelViews.add(labelView);
            } else {
                removeView(labelView);
            }
        }
        mLabelViews = labelViews;
    }


    /**
     * Return and existing or create a new label view for the given adapter position.
     */
    @Nullable
    private LabelView getLabelViewForPosition(int position) {
        // Check if already exists.
        for (LabelView labelView : mLabelViews) {
            if (labelView.mStartPosition <= position
                    && labelView.mEndPosition > position) {

                return labelView;
            }
        }

        // Get label for portion.
        StripeLabel label = getLabelForPosition(position);
        if (label == null) {
            return null;
        }

        // Generate view for the label.
        LabelView labelView = (LabelView) LayoutInflater.from(getContext())
                .inflate(mLabelLayout, this, false);
        labelView.setTextColor(mLayoutConfig.getFontColor());
        labelView.setText(label.getTitle());
        labelView.setCompoundDrawablesWithIntrinsicBounds(label.getLeftDrawable(), null, null, null);
        labelView.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_NO);

        if (label.shouldTruncate()) {
            labelView.setEllipsize(TextUtils.TruncateAt.END);
            labelView.setMaxLines(1);
        }

        int itemSize = mAdapter.getAdapter().size();
        if (mLabels.size() > 1 && itemSize > 0) {
            // Convert label position to adapter position.
            int positionCircleCount = position / itemSize;

            labelView.mStartPosition = itemSize * positionCircleCount + label.getStartPosition();
            labelView.mEndPosition = itemSize * positionCircleCount + label.getEndPosition();
        } else {
            labelView.mStartPosition = 0;
            labelView.mEndPosition = mAdapter.getItemCount();
        }

        mLabelViews.add(labelView);
        addView(labelView);

        return labelView;
    }


    /**
     * Returns the label for the given adapter position.
     */
    @Nullable
    private StripeLabel getLabelForPosition(int position) {
        int normalizedPosition = mAdapter.getNormalizedPosition(position);
        for (StripeLabel label : mLabels) {
            if (label.getStartPosition() <= normalizedPosition
                    && label.getEndPosition() > normalizedPosition) {
                return label;
            }
        }

        return null;
    }

    /**
     * Recalculate label position and visibility.
     */
    private void invalidateLabel(LabelView labelView) {
        if (shouldLabelBeVisible(labelView)) {
            // Calculate label position.
            int left = getVisibleLabelLeft(labelView);
            int right = getVisibleLabelRight(labelView);

            if (left < getWidth() - right) {
                labelView.setVisibility(VISIBLE);
                labelView.setPadding(left, labelView.getPaddingTop(), right, labelView.getPaddingBottom());
            } else {
                labelView.setVisibility(View.INVISIBLE);
                labelView.setPadding(-getWidth(), labelView.getPaddingTop(),
                        0, labelView.getPaddingBottom());
            }

        // Not visible, hide the label.
        } else {
            labelView.setVisibility(INVISIBLE);
            labelView.setPadding(-getWidth(), labelView.getPaddingTop(),
                    0, labelView.getPaddingBottom());
        }

        labelView.requestLayout();
    }

    /**
     * Returns true when the given label is currently visible.
     */
    private boolean shouldLabelBeVisible(LabelView labelView) {
        if (mHideSelected &&
                labelView.mStartPosition == getGridView().getSelectedPosition()) {
            return false;
        }

        return getFirsVisibleChildForLabel(labelView) != null;
    }

    /**
     * Returns the first visible view holder from the label.
     */
    @Nullable
    private View getFirsVisibleChildForLabel(LabelView labelView) {
        for (int i = 0; i < mBinding.itemGrid.getChildCount(); ++i) {
            View child = mBinding.itemGrid.getChildAt(i);
            if (child != null && child.getLeft() < getWidth()
                    && child.getRight() > 0 && isChildInLabel(child, labelView)) {
                return child;
            }
        }

        return null;
    }

    /**
     * Returns the last visible view holder from the label.
     */
    @Nullable
    private View getLastVisibleViewForLabel(LabelView label) {
        for (int i = mBinding.itemGrid.getChildCount() - 1; i >= 0; --i) {
            View child = mBinding.itemGrid.getChildAt(i);
            if (child != null && isChildInLabel(child, label)) {
                return child;
            }
        }

        return null;
    }

    /**
     * Return the left position for the given label in relative to its parent.
     */
    private int getVisibleLabelLeft(LabelView labelView) {
        View firstView = getFirsVisibleChildForLabel(labelView);
        if (firstView == null) {
            return mBinding.itemGrid.getPaddingStart() + mBinding.itemGrid.getLeft();
        }

        int marginStart = mLabelMarginStart;
        if (labelView == mLabelViews.first()) {
            marginStart = 0;
        }

        return Math.max(mBinding.itemGrid.getPaddingStart(),
                firstView.getLeft() + marginStart + mBinding.itemGrid.getLeft());
    }

    /**
     * Return the right position for the given label in relative to its parent.
     */
    private int getVisibleLabelRight(LabelView labelView) {
        if (mAdapter == null) {
            return 0;
        }

        View lastView = getLastVisibleViewForLabel(labelView);
        LabelView lastLabelView = null;
        if (mLabelViews != null) {
            lastLabelView = mLabelViews.last();
        }

        if (lastView == null) {
            return getWidth() - mBinding.itemGrid.getPaddingEnd();
        }

        if (lastLabelView != null && lastLabelView.equals(labelView) && !mAdapter.isCircular()) {
            return mBinding.itemGrid.getPaddingEnd();
        }

        return Math.min(getWidth() - mBinding.itemGrid.getPaddingEnd(),
                getWidth() - (lastView.getRight() + mBinding.itemGrid.getLeft()));

    }

    /**
     * Returns true when the child view is inside of the label.
     */
    private boolean isChildInLabel(View child, LabelView labelView) {
        RecyclerView.ViewHolder holder = mBinding.itemGrid.getChildViewHolder(child);
        if (holder == null) {
            return false;
        }

        int position = holder.getBindingAdapterPosition();
        return labelView.mStartPosition <= position && labelView.mEndPosition > position;
    }

    @NonNull
    public HorizontalGridView getItemGrid() {
        return mBinding.itemGrid;
    }


}
