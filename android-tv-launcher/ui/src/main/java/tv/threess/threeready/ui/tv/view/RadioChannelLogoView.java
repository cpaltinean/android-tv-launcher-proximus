package tv.threess.threeready.ui.tv.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.RadioChannelLogoViewBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.settings.fragment.BaseSettingsDialogFragment;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Radio channel logo animator class
 *
 * @author Solyom Zsolt
 * @since 2019.12.04
 */

public class RadioChannelLogoView extends RelativeLayout {
    private static final String TAG = Log.tag(RadioChannelLogoView.class);

    private final Navigator mNavigator = Components.get(Navigator.class);

    private final RadioChannelLogoViewBinding mBinding;

    // Time variables
    private final Handler mAnimationHandler;
    private static final int mAnimationDuration = 2500;
    private static final int mTranslationDuration = 3500;
    private static final int mLogoCrossFadeDuration = 600;
    private final long mNoInteractionTime = TimeUnit.HOURS.toMillis(1);

    // Animation variables
    private int originalX;
    private int originalY;
    private final int mScreenWidth;
    private final int mScreenHeight;
    private final int defaultMessage = 1;
    private ObjectAnimator mScaleAnimator;
    private ObjectAnimator mTranslationAnimator;
    private AnimatorListenerAdapter mAnimatorListener;
    private final Runnable mTranslationRunnable = this::startTranslationAnimation;
    private String mChannelId = "";

    public RadioChannelLogoView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mBinding = RadioChannelLogoViewBinding.inflate(LayoutInflater.from(getContext()), this);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        mNavigator.getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        mScreenWidth = displaymetrics.widthPixels;
        mScreenHeight = displaymetrics.heightPixels;
        mAnimationHandler = new Handler();

        generateRandomCoordinate();
        initScaleAnimator();
    }

    /**
     * Stops all animation and stops the timer handler. This method also resets the original
     * position of the imageView.
     */
    public void stopAnimation() {
        Log.d(TAG, "stopAnimation() called");
        resetPosition();
        clearData();
        mBinding.channelLogo.setVisibility(View.GONE);
        Glide.with(getContext().getApplicationContext()).clear(mBinding.channelLogo);

    }

    /**
     * Pauses the animation and stops the time timer Handler.
     */
    public void pauseAnimation() {
        Log.d(TAG, "pauseAnimation() called");
        resetPosition();
        mScaleAnimator.pause();
        mTranslationAnimator.cancel();
        mAnimationHandler.removeCallbacksAndMessages(null);
    }

    public void hideAndPauseAnimation() {
        hideAnimation();
        pauseAnimation();
    }

    /**
     * Restarts the animation and reinitialize the time timer Handler.
     */
    public void resumeAnimation() {
        Log.d(TAG, "resumeAnimation() called");
        mBinding.channelLogo.setVisibility(View.VISIBLE);
        mScaleAnimator.start();
        initTimerHandler();
    }

    /**
     * Returns True, if at least one animator is running or if the timer Handler is running.
     */
    public boolean isRunning() {
        return mScaleAnimator.isRunning() || mTranslationAnimator.isRunning()
                || mBinding.channelLogo.getHandler().hasMessages(defaultMessage);
    }

    /**
     * Returns True, if at least one animator is paused.
     */
    public boolean isPaused() {
        return mTranslationAnimator.isPaused() || mScaleAnimator.isPaused();
    }

    public void startChannelLogoAnimation(TvChannel channel) {
        if (channel != null && shouldLoadImage(channel)) {
            loadImageAndStartAnimation(channel);
            mChannelId = channel.getId();
        }
    }

    /**
     * Resets the channel id.
     */
    public void resetChannelId() {
        mChannelId = "";
    }

    /**
     * Hides imageView. Important: This method will not stops the animation!
     */
    private void hideAnimation() {
        mBinding.channelLogo.setVisibility(View.INVISIBLE);
    }

    /**
     * Returns true if the channel was changed or if RadioPlayer is open when Settings dialog is shown.
     */
    private boolean shouldLoadImage(TvChannel channel) {
        return !mChannelId.equals(channel.getId()) || (mNavigator.getDialogFragment() instanceof BaseSettingsDialogFragment);
    }

    /**
     * Start Channel Logo Animation. If the Broadcast is null, or if the content overlay is displayed, then we don't start the animation.
     */
    private void loadImageAndStartAnimation(TvChannel channel) {
        Log.d(TAG, "loadImageAndStartAnimation() called with: channel = [" + channel.getName() + "]");

        Glide.with(getContext().getApplicationContext())
                .load(channel)
                .transition(withCrossFade(mLogoCrossFadeDuration))
                .dontAnimate()
                .into(new DrawableImageViewTarget(mBinding.channelLogo) {
                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        mBinding.channelLogo.setVisibility(View.GONE);
                        Log.e(TAG, "onLoadFailed()  showing error drawable");
                    }

                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        super.onResourceReady(resource, transition);
                        onImageLoaded();
                    }
                });

    }

    private void onImageLoaded() {
        mBinding.channelLogo.setVisibility(View.VISIBLE);
        mScaleAnimator.addListener(mAnimatorListener);
        originalX = (int) mBinding.channelLogo.getX();
        originalY = (int) mBinding.channelLogo.getY();
        mScaleAnimator.start();
        initTimerHandler();
    }

    /**
     * Initialize the scale animator, responsible for pulsating animation
     */
    private void initScaleAnimator() {
        mAnimatorListener = new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mBinding.channelLogo.setVisibility(View.VISIBLE);
            }
        };

        mScaleAnimator = ObjectAnimator.ofPropertyValuesHolder(
                mBinding.channelLogo,
                PropertyValuesHolder.ofFloat("scaleX", 0.9f),
                PropertyValuesHolder.ofFloat("scaleY", 0.9f));

        mScaleAnimator.setDuration(mAnimationDuration).setStartDelay(getResources().getInteger(R.integer.channel_switch_animation_duration));
        mScaleAnimator.setInterpolator(new FastOutSlowInInterpolator());
        mScaleAnimator.setRepeatCount(ObjectAnimator.INFINITE);
        mScaleAnimator.setRepeatMode(ObjectAnimator.REVERSE);
        mScaleAnimator.addListener(mAnimatorListener);

        mScaleAnimator.start();
    }

    /**
     * Starts translation animation to a random position
     */
    private void startTranslationAnimation() {
        generateRandomCoordinate();
        mTranslationAnimator.start();
        initTimerHandler();
    }

    /**
     * Initialize the translation animation with a random coordinates. This animator will be used
     * only after one hour of inactivity.
     */
    private void generateRandomCoordinate() {

        //Max x,y coordinates are the last x and y coordinate on the screen where the
        //provider logo is still inside the screen.
        int possibleMaxX = mScreenWidth - originalX - mBinding.channelLogo.getWidth();
        int possibleMaxY = mScreenHeight - originalY - mBinding.channelLogo.getHeight();

        //-originalX and -originalY are the smallest possible coordinates on the screen where
        //the provider logo is still inside the screen and not outside.
        int newX = getRandomCoordinateBetween(-originalX, possibleMaxX);
        int newY = getRandomCoordinateBetween(-originalY, possibleMaxY);

        mTranslationAnimator = ObjectAnimator.ofPropertyValuesHolder(
                mBinding.channelLogo,
                PropertyValuesHolder.ofFloat("translationX", newX),
                PropertyValuesHolder.ofFloat("translationY", newY))
                .setDuration(mTranslationDuration);
    }

    /**
     * @param min coordinate
     * @param max coordinate
     * @return a random coordinate between Min and Max
     */
    private int getRandomCoordinateBetween(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    /**
     * Initialize the translation animation with a random coordinates. This animator will be used
     * only after one hour of inactivity.
     */
    private void initTimerHandler() {
        mAnimationHandler.removeCallbacksAndMessages(null);
        mAnimationHandler.postDelayed(mTranslationRunnable, mNoInteractionTime);
        mAnimationHandler.sendEmptyMessage(defaultMessage);
    }

    /**
     * This method resets the default position of the imageView.
     */
    private void resetPosition() {
        ObjectAnimator.ofPropertyValuesHolder(
                mBinding.channelLogo,
                PropertyValuesHolder.ofFloat("scaleX", 1),
                PropertyValuesHolder.ofFloat("scaleY", 1),
                PropertyValuesHolder.ofFloat("translationX", 0),
                PropertyValuesHolder.ofFloat("translationY", 0)).start();
    }

    /**
     * This method resets everything in to the default settings.
     * It also stops the animations and stops the timer Handler.
     */
    private void clearData() {
        Log.d(TAG, "clearData() called");
        mScaleAnimator.cancel();
        mTranslationAnimator.cancel();
        mBinding.channelLogo.clearAnimation();
        mScaleAnimator.removeAllListeners();
        mAnimationHandler.removeCallbacksAndMessages(null);
    }
}
