package tv.threess.threeready.ui.startup.step;

/**
 * Trigger check and wait for backend availability.
 *
 * @author Barabas Attila
 * @since 4/26/21
 */
public class CheckBackendStep extends BaseConnectionCheckStep {

    protected boolean isConnectionAvailable() {
        return mInternetChecker.isBackendAvailable();
    }
}
