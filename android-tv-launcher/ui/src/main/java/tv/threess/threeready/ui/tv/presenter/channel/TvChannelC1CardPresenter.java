/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.tv.presenter.channel;

import android.content.Context;
import android.view.View;

import com.bumptech.glide.Glide;

import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.utils.TitleFallbackImageViewTarget;

/**
 * Card presenter class for channel type and C1 variant card.
 * <p>
 * Created by Szilard on 5/2/2017.
 */

public class TvChannelC1CardPresenter extends TvChannelCCardPresenter {

    public TvChannelC1CardPresenter(Context context) {
        super(context);
    }

    @Override
    public void onBindHolder(ModuleData<?> moduleData,
                             final ViewHolder holder, final TvChannel channel) {
        super.onBindHolder(moduleData, holder, channel);

        Glide.with(mContext).clear(holder.mBinding.logo);
        Glide.with(mContext)
                .load(channel)
                .into(new TitleFallbackImageViewTarget(
                        holder.mBinding.logo,
                        holder.mBinding.logoText,
                        channel.getName()));

        holder.mBinding.number.setText(String.valueOf(channel.getNumber()));
        updateContentDescription(moduleData, holder, channel);
    }

    /**
     * Updates the content description used for Talkback.
     */
    private void updateContentDescription(ModuleData<?> moduleData, ViewHolder holder, TvChannel channel) {
        if (!mMwRepository.isTalkBackOn()) {
            return;
        }

        TalkBackMessageBuilder message = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_CARDS_CHANNEL_OTHER_TILE));
        if ((Integer) holder.view.getTag(R.integer.normalized_adapter_position) == 0) {
            message = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_CARDS_CHANNEL_FIRST_TILE))
                    .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_SWIMLANE, mTranslator.get(moduleData.getTitle()));
        }
        holder.view.setContentDescription(message
                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_CHANNEL_NAME, channel.getName())
                .toString()
        );
    }

    @Override
    public void onUnbindHolder(ModuleData<?> moduleData, ViewHolder holder) {
        super.onUnbindHolder(moduleData, holder);

        holder.mBinding.logoText.setVisibility(View.GONE);
        Glide.with(mContext).clear(holder.mBinding.logo);
    }
}
