/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.tv.view;

import static tv.threess.threeready.api.generic.helper.TimeUtils.CONTENT_MARKET_DELETE_SOON_LESS_THAN_DAYS;
import static tv.threess.threeready.api.generic.helper.TimeUtils.CONTENT_MARKET_DELETE_SOON_RECORDING_AVAILABILITY_DAYS;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.util.Pair;
import android.util.TypedValue;
import android.view.View;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.model.generic.ContentMarkerDetails;
import tv.threess.threeready.api.home.model.generic.ContentMarkerStyle;
import tv.threess.threeready.api.home.model.generic.ContentMarkers;
import tv.threess.threeready.api.home.model.generic.GradientStyle;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.home.model.local.FontType;
import tv.threess.threeready.api.home.model.module.MarkerType;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.api.vod.model.CurrencyType;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.generic.view.FontTextView;

/**
 * Custom view for content markers.
 *
 * @author Szende Pal
 * @since 2017.08.04
 */
public class ContentMarkersView extends FontTextView {
    private final ContentMarkers mContentMarkers = Components.get(ContentMarkers.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final ContentMarkerStyle mContentMarkerStyle = Components.get(ContentMarkerStyle.class);
    private final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);

    public ContentMarkersView(Context context) {
        this(context, null);
    }

    public ContentMarkersView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ContentMarkersView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setIncludeFontPadding(false);
    }

    public boolean hasContent(){
        return getVisibility() == View.VISIBLE;
    }

    public void hideMarkers() {
        setVisibility(View.GONE);
    }

    /**
     * Shows content markers for a content item. Will show all possible markers, is not filtered.
     * @param contentItem The content item.
     */
    public void showMarkers(IContentItem contentItem) {
        showMarkers(contentItem, ContentMarkers.TypeFilter.All);
    }

    /**
     * Shows the filtered content markers for a content item.
     * @param contentItem The content item.
     * @param typeFilter Type filter to know which markers can be shown.
     */
    public void showMarkers(IContentItem contentItem, ContentMarkers.TypeFilter typeFilter) {
        showMarkers(contentItem, typeFilter, true);
    }

    /**
     * Shows the filtered markers for a content item based on channel and focused state. It is used for broadcast markers.
     * @param contentItem The content item.
     * @param typeFilter Type filter to know which markers can be shown.
     * @param isFocused  Focused state of the broadcast item. (if we need to show detail page markers, focused state will be always true)
     */
    public void showMarkers(IContentItem contentItem, ContentMarkers.TypeFilter typeFilter, boolean isFocused) {
        Map<MarkerType, String> markerTypes = provideContentMarkers(contentItem, isFocused);
        Map<MarkerType, String> filteredMarkers = new HashMap<>();
        for (Map.Entry<MarkerType, String> markerTypeEntry: markerTypes.entrySet()) {
            if (typeFilter.getMarkers().contains(markerTypeEntry.getKey())) {
                filteredMarkers.put(markerTypeEntry.getKey(), markerTypeEntry.getValue());
            }
        }

        showMarkers(filteredMarkers);
    }

    /**
     * Provides a list of content markers for the content item.
     * @param contentItem The content item.
     * @param isFocused Focused state.
     * @return A map with calculated markers.
     */
    private  Map<MarkerType, String> provideContentMarkers(IContentItem contentItem, boolean isFocused) {
        Map<MarkerType, String> markerTypes = new HashMap<>();
        if (contentItem instanceof IBroadcast) {
            markerTypes = getBroadcastMarkers((IBroadcast) contentItem, isFocused);
        } else if (contentItem instanceof IBaseVodItem) {
            markerTypes = getVodMarkers((IBaseVodItem) contentItem);
        } else if (contentItem instanceof IVodVariant) {
            markerTypes = getVariantMarkers((IVodVariant) contentItem);
        }

        return markerTypes;
    }

    /**
     * Provides a list of content markers for a vod item.
     * @param vodItem Vod content item.
     * @return A map with calculated markers.
     */
    private  Map<MarkerType, String> getVodMarkers(IBaseVodItem vodItem) {
        Map<MarkerType, String> markerTypes = new HashMap<>();

        if (mPlaybackDetailsManager.isContentItemPlaying(vodItem)) {
            markerTypes.put(MarkerType.NOW_PLAYING, "");
        }

        if (vodItem.isTVod()) {
            if (vodItem.isRented()) {
                markerTypes.put(MarkerType.RENTED,
                        LocaleTimeUtils.getRentalRemainingTime(
                                vodItem.getRentalRemainingTime(), mTranslator));
            } else if (vodItem.isRentable()) {
                IVodPrice price = vodItem.getCheapestPrice();
                if (price != null) {
                    Pair<MarkerType, String> pair = getPriceMarker(vodItem.getCheapestPrice(), mTranslator);
                    markerTypes.put(pair.first, pair.second);
                }
            }
        } else if (vodItem.isSVod()) {
            if (vodItem.isSubscribed()) {
                markerTypes.put(MarkerType.INCLUDED, "");
            } else {
                markerTypes.put(MarkerType.SUBSCRIBE, "");
            }
        }

        if(vodItem.isLastChance()) {
            markerTypes.put(MarkerType.LAST_CHANCE, "");
        }

        if (vodItem.isNew()) {
            markerTypes.put(MarkerType.NEW, "");
        }
        return markerTypes;
    }

    /**
     * Provides a list of content markers for a vod variant item.
     * @param variant Vod variant.
     * @return A map with calculated markers.
     */
    private Map<MarkerType, String> getVariantMarkers(IVodVariant variant) {
        Map<MarkerType, String> markerTypes = new HashMap<>();

        if (mPlaybackDetailsManager.isContentItemPlaying(variant)) {
            markerTypes.put(MarkerType.NOW_PLAYING, "");
        }

        if (variant.isTVod()) {
            if (variant.isRented()) {
                markerTypes.put(MarkerType.RENTED,
                        LocaleTimeUtils.getRentalRemainingTime(
                                variant.getRentalRemainingTime(), mTranslator));
            }
        } else if (variant.isSVod()) {
            if (variant.isSubscribed()) {
                markerTypes.put(MarkerType.INCLUDED, "");
            }
        }

        return markerTypes;
    }

    /**
     * Provides a list with calculated markers for a broadcast item.
     * @param broadcast The content item.
     * @param isFocused Focused state.
     * @return A map with calculated markers.
     */
    private Map<MarkerType, String> getBroadcastMarkers(IBroadcast broadcast,boolean isFocused) {
        Map<MarkerType, String> markerTypes = new HashMap<>();
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());

        if (mPlaybackDetailsManager.isBroadcastPlaying(broadcast)) {
            markerTypes.put(MarkerType.NOW_PLAYING, "");
        }

        if (broadcast.isLive()) {
            markerTypes.put(MarkerType.NOW, "");
        }

        if (channel != null && channel.getType() == ChannelType.RADIO) {
            if (isFocused) {
                markerTypes.put(MarkerType.RADIO, "");
            }
            return markerTypes;
        }

        if (broadcast instanceof IRecording) {
            if (broadcast.isFuture()) {
                markerTypes.put(MarkerType.PLANNED, "");
            }

            //Check if the recording will be deleted in less than CONTENT_MARKET_DELETE_SOON_LESS_THAN_DAYS(5) days.
            if (broadcast.getStart() + TimeUnit.DAYS.toMillis(CONTENT_MARKET_DELETE_SOON_RECORDING_AVAILABILITY_DAYS) - System.currentTimeMillis()
                    < TimeUnit.DAYS.toMillis(CONTENT_MARKET_DELETE_SOON_LESS_THAN_DAYS)) {
                markerTypes.put(MarkerType.DELETE_SOON, "");
            }
        }

        if (broadcast.isFuture() && isFocused) {
            if (broadcast.isNextToLive()) {
                markerTypes.put(MarkerType.NEXT, "");
            } else if (broadcast.isFuture()) {
                markerTypes.put(MarkerType.LATER, "");
            }
        }

        return markerTypes;
    }

    /**
     * @param markers Key: markerType, Value: an extra string ( ex: price, remaining time )
     */
    private void showMarkers(Map<MarkerType, String> markers) {
        ContentMarkerDetails showContentMarker = mContentMarkers.getHighestPriorityContentMarker(markers.keySet());

        String extraString = showContentMarker == null ? "" : markers.get(showContentMarker.getMarkerType());
        updateViewDetails(showContentMarker, extraString);
    }

    /**
     * @param markerDetails content markers properties: text color, bg, etc
     * @param extraText     text added to the translation of content marker type
     */
    private void updateViewDetails(ContentMarkerDetails markerDetails, String extraText) {
        if (markerDetails == null) {
            setVisibility(GONE);
            return;
        }

        ContentMarkerDetails defaultMarker = mContentMarkers.getContentMarkerDetails(MarkerType.DEFAULT);
        if (defaultMarker == null) {
            // No marker available.
            setVisibility(GONE);
            return;
        }

        MarkerType markerType = markerDetails.getMarkerType();
        if (markerDetails.isEmpty()) {
            markerDetails = defaultMarker;
        }

        String text = "";
        switch (markerType) {
            case NOW:
                text = mTranslator.get(TranslationKey.CONTENT_MARKER_NOW);
                break;
            case LATER:
                text = mTranslator.get(TranslationKey.CONTENT_MARKER_LATER);
                break;
            case NEXT:
                text = mTranslator.get(TranslationKey.CONTENT_MARKER_NEXT);
                break;
            case NOW_PLAYING:
                text = mTranslator.get(TranslationKey.CONTENT_MARKER_NOW_PLAYING);
                break;
            case FREE:
                text = mTranslator.get(TranslationKey.CONTENT_MARKER_FREE);
                break;
            case PLANNED:
                text = mTranslator.get(TranslationKey.CONTENT_MARKER_PLANNED);
                break;
            case RENTED:
            case PRICE:
                text = extraText;
                break;
            case RADIO:
                text = mTranslator.get(TranslationKey.CONTENT_MARKER_RADIO);
                break;
            case INCLUDED:
                text = mTranslator.get(TranslationKey.CONTENT_MARKER_INCLUDED);
                break;
            case SUBSCRIBE:
                text = mTranslator.get(TranslationKey.CONTENT_MARKER_SUBSCRIBE);
                break;
            case DELETE_SOON:
                text = mTranslator.get(TranslationKey.CONTENT_MARKER_DELETE_SOON);
                break;
            case LAST_CHANCE:
                text = mTranslator.get(TranslationKey.CONTENT_MARKER_LAST_CHANCE);
                break;
            case NEW:
                text = mTranslator.get(TranslationKey.CONTENT_MARKER_NEW);
                break;
        }

        setText(text);

        applyMarkerStyle(markerDetails);

        setVisibility(VISIBLE);
    }

    /**
     * Apply content marker styles based on the config.
     */
    private void applyMarkerStyle(ContentMarkerDetails markerDetails) {
        if (mContentMarkerStyle == ContentMarkerStyle.PROXIMUS) {
            applyProximusMarkerStyle(markerDetails);
            return;
        }

        applyDefaultMarkerStyle(markerDetails);
    }

    /**
     * Default rectangular content marker.
     */
    private void applyDefaultMarkerStyle(ContentMarkerDetails markerDetails) {
        Resources res = getContext().getResources();
        setPadding(
                res.getDimensionPixelOffset(R.dimen.content_marker_horizontal_padding),
                res.getDimensionPixelOffset(R.dimen.content_marker_top_padding),
                res.getDimensionPixelOffset(R.dimen.content_marker_horizontal_padding),
                res.getDimensionPixelOffset(R.dimen.content_marker_bottom_padding));

        setAlpha(markerDetails.getTransparency());
        setTextColor(markerDetails.getColor());

        if (markerDetails.hasGradient()) {
            setBackground(createGradientDrawable(markerDetails.getGradient()));
        } else {
            setBackgroundColor(markerDetails.getBackground());
        }

        setFontType(FontType.BOLD);
        setTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.content_marker_text_size));
        setLineSpacing(res.getDimension(R.dimen.content_marker_line_spacing_extra), 1f);
    }

    /**
     * Proximus style rounded content markers.
     */
    private void applyProximusMarkerStyle(ContentMarkerDetails markerDetails) {
        Resources res = getContext().getResources();
        setPadding(
                res.getDimensionPixelOffset(R.dimen.proximus_content_marker_horizontal_padding),
                getPaddingTop(),
                res.getDimensionPixelOffset(R.dimen.proximus_content_marker_horizontal_padding),
                getPaddingBottom());

        setAlpha(markerDetails.getTransparency());
        setTextColor(markerDetails.getColor());

        if (markerDetails.hasGradient()) {
            float cornerRadius = getContext().getResources().getDimension(R.dimen.proximus_content_marker_radius);
            GradientDrawable gradientDrawable = createGradientDrawable(markerDetails.getGradient());
            gradientDrawable.setCornerRadii(new float[] {cornerRadius, cornerRadius,
                    cornerRadius, cornerRadius, 0f, 0f, cornerRadius, cornerRadius});
            setBackground(gradientDrawable);
        } else {
            setBackgroundColor(markerDetails.getBackground());
        }

        setFontType(FontType.BOLD);
        setTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.proximus_content_marker_text_size));
        setLineSpacing(res.getDimension(R.dimen.proximus_content_marker_line_spacing_extra), 1f);
    }

    private GradientDrawable createGradientDrawable(GradientStyle markerGradient) {
        int[] color = new int[] {markerGradient.getFromColor(), markerGradient.getToColor()};
        return new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, color);
    }

    /**
     * Method to return a content marker based on VOD price(with price or free)
     * @param price The VOD`s price
     * @return The content marker.
     */
    private Pair<MarkerType, String> getPriceMarker(IVodPrice price, Translator translator) {
        if (price.getCurrencyAmount() == 0d) {
            return new Pair<>(MarkerType.FREE, "");
        }
        return new Pair<>(MarkerType.PRICE, formatPrice(price, translator));
    }

    /**
     * Method to format the VOD price.
     * @param price VOD`s price.
     * @return The formatted price as a string.
     */
    private String formatPrice(IVodPrice price, Translator translator) {
        if (price.getCurrencyType() == CurrencyType.Credit) {
            return translator.get(TranslationKey.CONTENT_MARKER_CREDIT);
        }
        return mLocaleSettings.getCurrency() + " " + StringUtils.formatPrice(price.getCurrencyAmount());
    }
}
