/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.home.presenter.module.stripe.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.AppStripeModuleBinding;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ModularItemBridgeAdapter;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeLabel;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeSeparatorProvider;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.home.presenter.module.BaseStripeModulePresenter;

/**
 * Presenter class for an application stripe module.
 * Responsible to load the data from the given data source and display it in a horizontal gird.
 * The loaded data will be displayed with cards. The type of the card is based on the module type and variant.
 *
 * @author Barabas Attila
 * @since 2017.05.17
 */
public abstract class AppStripeModulePresenter extends BaseStripeModulePresenter<AppStripeModulePresenter.ViewHolder> {
    protected final Translator mTranslator = Components.get(Translator.class);

    public AppStripeModulePresenter(Context context, DataSourceLoader dataSourceLoader) {
        super(context, dataSourceLoader);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent, RecyclerView.RecycledViewPool pool) {
        AppStripeModuleBinding binding = AppStripeModuleBinding
                .inflate(LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) holder.mBinding.stripeGrid.getItemGrid().getLayoutParams();
        params.topMargin = mContext.getResources().getDimensionPixelOffset(R.dimen.stripe_grid_top_margin);

        holder.mBinding.stripeGrid.setRecyclerViewPool(pool);
        setAlignmentOffsetPercent(holder, APP_SCROLL_ALIGNMENT_PERCENTAGE_OFFSET);

        int horizontalPadding =  mContext.getResources().getDimensionPixelSize(R.dimen.stripe_title_left_margin);
        int topPadding = mContext.getResources().getDimensionPixelSize(R.dimen.app_a_stripe_top_padding);
        holder.mBinding.stripeGrid.getGridView().setPadding(horizontalPadding, topPadding, horizontalPadding, 0);

        holder.mBinding.stripeGrid.getGridView().setRowHeight(mContext.getResources().getDimensionPixelOffset(R.dimen.app_a_stripe_row_height));
        holder.mBinding.stripeGrid.getGridView().getLayoutParams().height = mContext.getResources().getDimensionPixelOffset(R.dimen.app_a_stripe_row_height);
        holder.mBinding.stripeGrid.setItemSpacing(mContext.getResources().getDimensionPixelOffset(R.dimen.app_a1_stripe_card_spacing));

        holder.mBinding.stripeGrid.setWindowAlignmentOffsetPercent(
            mContext.getResources().getInteger(R.integer.app_a_stripe_window_alignment));

        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, ModuleData<?> moduleData, List<?> payLoads) {
        super.onBindHolder(holder, moduleData, payLoads);

        updateAdapter(holder, moduleData);
    }

    @Override
    public void onBindHolder(ViewHolder holder, ModuleData<?> moduleData) {
        updateAdapter(holder, moduleData);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);

        holder.mBinding.stripeGrid.setAdapter(null, null);
    }


    /**
     * Create a new or update the existing adapter for the module data.
     * @param holder The view holder of the collection.
     * @param moduleData The module data which needs to be displayed
     */
    private void updateAdapter(AppStripeModulePresenter.ViewHolder holder, ModuleData<?> moduleData) {
        if (holder.mBinding.stripeGrid.getAdapter() != null) {
            ModularItemBridgeAdapter itemBridgeAdapter =
                    (ModularItemBridgeAdapter) holder.mBinding.stripeGrid.getAdapter();
            ModuleConfig adapterModule = itemBridgeAdapter.getModuleData().getModuleConfig();
            if (Objects.equals(adapterModule, moduleData.getModuleConfig())) {
                ArrayObjectAdapter<Object> objectAdapter = itemBridgeAdapter.getAdapter();
                objectAdapter.replaceAll(moduleData.getItems());
                return;
            }
        }

        ArrayObjectAdapter<Object> objectAdapter = new ArrayObjectAdapter<>(moduleData.getItems());
        objectAdapter.setHasStableIds(true);

        PresenterSelector cardPresenterSelector = getCardPresenterSelector(moduleData.getModuleConfig());
        ModularItemBridgeAdapter itemBridgeAdapter = new ModularItemBridgeAdapter(
                moduleData, objectAdapter, cardPresenterSelector);
        holder.mBinding.stripeGrid.setAdapter(itemBridgeAdapter, getLabelProvider(moduleData, itemBridgeAdapter));
    }

    protected abstract StripeSeparatorProvider<StripeLabel> getLabelProvider(ModuleData<?> moduleData, ModularItemBridgeAdapter adapter);

    protected abstract PresenterSelector getCardPresenterSelector(ModuleConfig moduleConfig);

    static class ViewHolder extends Presenter.ViewHolder {

        ViewGroup mParentView;
        private final AppStripeModuleBinding mBinding;

        public ViewHolder(AppStripeModuleBinding binding) {
            super(binding.getRoot());
            mParentView = (ViewGroup) view;
            mBinding = binding;
        }
    }

}
