package tv.threess.threeready.ui.home.presenter.module.stripe.notification;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.notification.model.NotificationItem;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.home.presenter.card.app.EditorialA1CardPresenter;
import tv.threess.threeready.ui.home.presenter.module.stripe.generic.StripeModulePresenter;
import tv.threess.threeready.ui.home.presenter.notification.NotificationD1CardPresenter;

/**
 * Presenter class for the D1 system notifications stripe.
 *
 * @author Eugen Guzyk
 * @since 2018.08.23
 */
public class NotificationStripeModulePresenter extends StripeModulePresenter {

    private final Map<ModuleCardVariant, PresenterSelector> mVariantCardPresenterMap = new HashMap<>();

    public NotificationStripeModulePresenter(Context context, DataSourceLoader dataSourceLoader) {
        super(context, dataSourceLoader);
        mVariantCardPresenterMap.put(ModuleCardVariant.D1, new ClassPresenterSelector()
                .addClassPresenter(NotificationItem.class, new NotificationD1CardPresenter(context))
                .addClassPresenter(EditorialItem.class, new EditorialA1CardPresenter(context)));
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent, RecyclerView.RecycledViewPool pool) {
        ViewHolder holder = (ViewHolder) super.onCreateViewHolder(parent, pool);

        holder.mBinding.stripe.getTitleView().setVisibility(View.GONE);

        holder.mBinding.stripe.getStripeGrid().setHorizontalSpacing(mContext.getResources().getDimensionPixelOffset(R.dimen.notification_stripe_horizontal_spacing));

        return holder;
    }

    @Override
    protected PresenterSelector getCardPresenterSelector(ModuleConfig moduleConfig) {
        return mVariantCardPresenterMap.get(moduleConfig.getCardVariant());
    }

    /**
     * Gets the value for horizontal padding.
     */
    @Override
    protected int getHorizontalPadding() {
        return mContext.getResources().getDimensionPixelSize(R.dimen.notification_stripe_horizontal_padding);
    }

    /**
     * Gets the value for bottom padding.
     */
    @Override
    protected int getBottomPadding() {
        return mContext.getResources().getDimensionPixelSize(R.dimen.notification_stripe_bottom_padding);
    }

    /**
     * Gets the value for bottom margin.
     */
    @Override
    protected int getBottomMargin() {
        return 0;
    }

    /**
     * Gets the value for item spacing.
     */
    @Override
    protected int getItemSpacing(ModuleConfig moduleConfig) {
        return 0;
    }

    /**
     * Gets value for row height.
     */
    @Override
    protected int getRowHeight(ModuleData<?> moduleData) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.notification_d1_card_height);
    }

    /**
     * Gets the value for top margin.
     */
    @Override
    protected int getTopMargin() {
        return 0;
    }
}
