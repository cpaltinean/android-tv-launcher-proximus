package tv.threess.threeready.ui.settings.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.ui.databinding.SettingsMainItemBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.settings.model.MainSettingsItem;

/**
 * Presenter class for the main settings items.
 *
 * @author Andrei Teslovan
 * @since 2018.09.13
 */
public class MainItemPresenter extends BasePresenter<MainItemPresenter.ViewHolder, MainSettingsItem> {
    public static final String TAG = Log.tag(MainItemPresenter.class);

    protected final Navigator mNavigator = Components.get(Navigator.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    public MainItemPresenter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(
                SettingsMainItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onClicked(ViewHolder holder, MainSettingsItem item) {
        super.onClicked(holder, item);
        switch (item.getType()) {
            case PARENTAL_CONTROL:
                mNavigator.showParentalControlPINDialog((pin) -> mNavigator.openParentalControlSettings());
                break;

            case VOD_PURCHASE_HISTORY:
                mNavigator.openVodPurchaseHistorySettings();
                break;

            case PRIVACY:
                mNavigator.openPrivacyInfoFragment();
                break;

            case LANGUAGE:
                mNavigator.openLanguagePreferencesSettings();
                break;

            case RENUMBERING:
                mNavigator.openDeepLink(mAppConfig.getChannelRenumberingLink());
                mNavigator.clearAllDialogs();
                Log.event(new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                    .addDetail(UILogEvent.Detail.Page, UILog.Page.ChannelRenumbering));
                break;

            case EPG_LAYOUT:
                mNavigator.openEpgLayoutSettings();
                break;

            case RECORDINGS:
                mNavigator.openRecordingsSettings();
                break;

            case SUPPORT:
                mNavigator.openSupportSettings();
                break;

            case HINTS:
                mNavigator.openApplicationHintPage();
                break;

            case DEVICE_SETTINGS:
                mNavigator.openAndroidSettings(false);
                Log.event(new Event<>(UILogEvent.PageOpen)
                    .addDetail(UILogEvent.Detail.Page, UILog.Page.DevicePreferencesPage));

                break;

            default:
                Log.d(TAG, "Invalid item type: " + item.getType());
                break;
        }
    }

    @Override
    public void onBindHolder(ViewHolder holder, MainSettingsItem item) {
        holder.mBinding.title.setText(item.getTitle());
        holder.mBinding.image.setBackground(item.getIcon());
    }

    public static class ViewHolder extends BasePresenter.ViewHolder {

        private final SettingsMainItemBinding mBinding;

        public ViewHolder(SettingsMainItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
