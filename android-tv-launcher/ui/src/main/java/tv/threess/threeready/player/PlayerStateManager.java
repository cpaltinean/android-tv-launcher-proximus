package tv.threess.threeready.player;

import androidx.annotation.UiThread;
import androidx.fragment.app.FragmentManager;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.ui.details.fragment.ProgramDetailsFragment;
import tv.threess.threeready.ui.details.fragment.SeriesRecordingDetailsFragment;
import tv.threess.threeready.ui.details.fragment.VodMovieDetailsFragment;
import tv.threess.threeready.ui.details.fragment.VodSeriesDetailsFragment;
import tv.threess.threeready.ui.generic.dialog.BaseDialogFragment;
import tv.threess.threeready.ui.generic.dialog.BingeWatchingDialog;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.fragment.BasePlayerFragment;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.notification.NotificationManager;
import tv.threess.threeready.ui.home.fragment.HomeFragment;
import tv.threess.threeready.ui.pvr.fragment.RecordingPlayerFragment;
import tv.threess.threeready.ui.tv.fragment.TvPlayerFragment;
import tv.threess.threeready.ui.vod.fragment.GdprPlayerFragment;
import tv.threess.threeready.ui.vod.fragment.TrailerPlayerFragment;
import tv.threess.threeready.ui.vod.fragment.VodPlayerFragment;

/**
 * handles different state actions for player (like stop button, stream end reached etc.)
 *
 * @author hunormadaras
 * @since 29/03/2019
 */
public class PlayerStateManager implements Component {
    private static final String TAG = Log.tag(PlayerStateManager.class);

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final NotificationManager mNotificationManager = Components.get(NotificationManager.class);
    private final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);

    public PlayerStateManager() {
    }


    /**
     * Tune to the last played channel
     *
     * @param action The action which triggered the playback change.
     */
    private void tuneToLastChannel(StartAction action, boolean showDetail) {
        if (showDetail) {
            goBackToDetail();
        } else {
            closePlayerUI();
        }
        mNavigator.tuneToLastChannel(action, false, false);
    }

    /**
     * Close the currently visible player UI and go back to previous page.
     */
    private void closePlayerUI() {
        BaseFragment topFragment = mNavigator.getContentFragment();

        // Display playback in full screen.
        if (topFragment instanceof TvPlayerFragment) {
            mNavigator.clearBackStack();
            mNavigator.hideContentOverlay();

            // Check if the player UI is displayed.
        } else if (topFragment instanceof BasePlayerFragment) {
            mNavigator.getActivity().getSupportFragmentManager().popBackStackImmediate();
            mNavigator.showContentOverlay();
        }
    }

    /**
     * Handles reached end at stream at VOD, Trailer and Recording types, in case the stack is empty
     * or the penultimate element isn't the corresponding detail page in the stack then opens a new detail page.
     */
    private void goBackToDetail() {

        if (mNavigator.getActivity() == null || mNavigator.getActivity().isInstanceStateSaved()) {
            return;
        }

        BaseFragment top = mNavigator.getContentFragment();
        if (top == null) {
            mNavigator.showNewDetailPageByPlayer();
            return;
        }

        Map<String, List<String>> possibleDetailPages = new HashMap<>();
        possibleDetailPages.put(VodPlayerFragment.TAG, Arrays.asList(VodMovieDetailsFragment.TAG, VodSeriesDetailsFragment.TAG));
        possibleDetailPages.put(RecordingPlayerFragment.TAG, Arrays.asList(SeriesRecordingDetailsFragment.TAG, ProgramDetailsFragment.TAG));
        possibleDetailPages.put(TrailerPlayerFragment.TAG, Arrays.asList(VodMovieDetailsFragment.TAG, VodSeriesDetailsFragment.TAG, HomeFragment.TAG));
        possibleDetailPages.put(TvPlayerFragment.TAG, Collections.singletonList(ProgramDetailsFragment.TAG));

        if (possibleDetailPages.containsKey(top.getTag())) {
            List<String> detailPages = possibleDetailPages.get(top.getTag());

            FragmentManager fragmentManager = mNavigator.getActivity().getSupportFragmentManager();

            int backStackSize = fragmentManager.getBackStackEntryCount();
            int detailFrgPosition = backStackSize - 2;

            if (backStackSize >= 2) {
                BaseFragment penultimateFragment = (BaseFragment) fragmentManager.findFragmentByTag(fragmentManager.getBackStackEntryAt(detailFrgPosition).getName());
                if (penultimateFragment != null && penultimateFragment.getTag() != null && detailPages != null && detailPages.contains(penultimateFragment.getTag())) {
                    fragmentManager.popBackStackImmediate(fragmentManager.getBackStackEntryAt(detailFrgPosition).getId(), 0);
                    mNavigator.showContentOverlay();
                    return;
                }
            }

            mNavigator.showNewDetailPageByPlayer();
        }
    }

    /**
     * called when playback event is: @{@link PlaybackEvent#EndReached} to handle cases
     */
    @UiThread
    public void handleEndReachedAction() {
        PlaybackType playerType = mPlaybackDetailsManager.getPlayerType();

        switch (playerType) {
            case LiveTvIpTv:
            case LiveTvOtt:
            case RadioIpTv:
            case RadioOtt:
                // Nothing to do. Live and radio playback never ends.
                break;
            case Replay:
                String channelId = mPlaybackDetailsManager.getChannelId();
                mPlaybackDetailsManager.startChannel(StartAction.ReplayEnded,
                        channelId, false, RestartMode.LiveChannel);
                mNotificationManager.showLiveNotification();
                break;
            case Recording:
                IRecording recording = mPlaybackDetailsManager.getRecordingPlayerData();
                StartAction action = recording == null || recording.getSeriesId() == null ?
                        StartAction.RecordingEnded : StartAction.SeriesRecordingEnded;
                tuneToLastChannel(action, true);
                break;

            case Vod:
                BaseDialogFragment dialog = mNavigator.getDialogFragment();
                if (dialog instanceof BingeWatchingDialog && !dialog.isDismissed()) {
                    if (((BingeWatchingDialog) dialog).startNextEpisode()) {
                        return;
                    }
                }

                tuneToLastChannel(StartAction.VodEnded, true);
                break;
            case VodTrailer:
                tuneToLastChannel(StartAction.TrailerEnded, true);
                break;

            case Gdpr:
                handleGdprStopAndEndActions();
                break;

            case Default:
            default:
                Log.w(TAG, "Unknown playback type finished. " + mPlaybackDetailsManager.getPlayerType());
                break;
        }
    }

    public void handleStopButton() {
        if (!mNavigator.isPlayerUIOnTop() && mNavigator.isContentOverlayDisplayed()) {
            mNavigator.hideContentOverlay();
        }
        switch (mPlaybackDetailsManager.getPlayerType()) {
            case LiveTvIpTv:
            case LiveTvOtt:
                mNavigator.showPlayerUI();
                mPlaybackDetailsManager.jumpToLive(StartAction.BtnStop);
                if (!mPlaybackDetailsManager.isInPLTVBuffer()
                        && !mPlaybackDetailsManager.isRegionalChannelPlaying()
                        && !mPlaybackDetailsManager.isAppChannelPlaying()) {
                    mNotificationManager.showLiveNotification();
                }
                break;
            case VodTrailer:
            case Vod:
            case Recording:
                tuneToLastChannel(StartAction.BtnStop, true);
                break;

            case Replay:
                IBroadcast broadcast = mPlaybackDetailsManager.getReplayPlayerData();
                if (broadcast != null && broadcast.isLive()) {
                    mNavigator.showPlayerUI();
                    mNavigator.tuneToLastChannel(StartAction.BtnStop, false, false);
                    mNotificationManager.showLiveNotification();
                } else {
                    tuneToLastChannel(StartAction.BtnStop, true);
                }
                break;

            case Gdpr:
                handleGdprStopAndEndActions();
                break;

            case Default:
            default:
                Log.w(TAG, "Unknown playback type finished. " + mPlaybackDetailsManager.getPlayerType());
                break;
        }
    }

    /**
     * Handles the stop and end reached actions for Gdpr playback.
     */
    private void handleGdprStopAndEndActions() {
        if (mNavigator.isStartupFinished()) {
            tuneToLastChannel(StartAction.Ui, false);
            return;
        }

        if (mNavigator.getContentFragment() instanceof GdprPlayerFragment) {
            mNavigator.popBackStackImmediate();
            mNavigator.showContentOverlay();
            mPlaybackDetailsManager.forceStop();
        }
    }
}
