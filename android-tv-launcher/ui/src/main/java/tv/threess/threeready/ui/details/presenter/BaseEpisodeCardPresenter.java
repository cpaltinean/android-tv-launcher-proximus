package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IBaseSeries;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.generic.view.BaseOrderedIconsContainer;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.tv.view.ContentMarkersView;

/**
 * Base card presenter for an episode on the series detail page.
 *
 * @author Barabas Attila
 * @since 2018.11.21
 */
public abstract class BaseEpisodeCardPresenter<THolder extends BaseEpisodeCardPresenter.ViewHolder, TContentItem extends IContentItem>
        extends BaseCardPresenter<THolder, TContentItem> {

    protected final Translator mTranslator = Components.get(Translator.class);
    protected final Navigator mNavigator = Components.get(Navigator.class);
    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    protected BaseEpisodeCardPresenter(Context context) {
        super(context);
    }

    protected abstract void updateMarkers(ContentMarkersView textView, TContentItem episode, boolean isFocused);

    @Override
    public void onBindHolder(THolder holder, TContentItem episode) {
        super.onBindHolder(holder, episode);

        updateCoverImage(holder, episode);
        updateTitle(holder, episode);
    }

    @Override
    protected void onParentalRatingChanged(THolder holder, TContentItem episode) {
        super.onParentalRatingChanged(holder, episode);

        updateCoverImage(holder, episode);
        updateTitle(holder, episode);
    }

    protected void updateCoverImage(THolder holder, TContentItem episode) {
        ImageView coverView = holder.getCoverView();
        Glide.with(mContext).clear(coverView);
        Glide.with(mContext)
                .load(episode)
                .placeholder(holder.mPlaceHolderDrawable)
                .fallback(R.drawable.content_fallback_image_landscape)
                .error(R.drawable.content_fallback_image_landscape)
                .format(DecodeFormat.PREFER_RGB_565)
                .override(getCardWidth(episode), getCardHeight(episode))
                .optionalCenterInside()
                .transition(new DrawableTransitionOptions().dontTransition())
                .into(coverView);
    }

    protected void updateTitle(THolder holder, TContentItem episode) {
        String title = episode.getEpisodeTitle();
        if (!TextUtils.isEmpty(title)) {
            holder.getTitleView().setText(title);
            holder.getTitleView().setVisibility(View.VISIBLE);
        } else {
            holder.getTitleView().setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(episode.getSeasonEpisodeNumber(mTranslator))) {
            holder.getSeriesInfoView().setText(episode.getSeasonEpisodeNumber(mTranslator));
            holder.getSeriesInfoView().setVisibility(View.VISIBLE);
        } else {
            holder.getSeriesInfoView().setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(episode.getDescription())) {
            holder.getDescriptionView().setText(episode.getDescription());
            holder.getDescriptionView().setVisibility(View.VISIBLE);
        } else {
            holder.getDescriptionView().setVisibility(View.GONE);
        }
    }

    protected void updateParentalControlIcon(THolder holder, TContentItem episode) {
        holder.getIconsContainer().showParentalRating(episode.getParentalRating());
    }

    protected void updateDescriptiveAudioIcon(THolder holder, TContentItem episode) {
        if (!episode.hasDescriptiveAudio()) {
            return;
        }
        holder.getIconsContainer().showDescriptiveAudio();
    }

    protected void updateDescriptiveSubtitleIcon(THolder holder, TContentItem episode) {
        if (!episode.hasDescriptiveSubtitle()) {
            return;
        }
        holder.getIconsContainer().showDescriptiveSubtitle();
    }

    @Override
    protected void onPlaybackChanged(THolder holder, TContentItem contentItem) {
        updateMarkers(holder.getContentMarkersView(), contentItem, holder.view.isFocused());
    }


    @Override
    public void onUnbindHolder(THolder holder) {
        super.onUnbindHolder(holder);

        Glide.with(mContext).clear(holder.getCoverView());
    }

    @Override
    public int getCardWidth(TContentItem contentItem) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.episode_card_width);
    }

    @Override
    public int getCardHeight(TContentItem tContentItem) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.episode_card_height);
    }

    @Override
    public long getStableId(TContentItem contentItem) {
        return Objects.hash(contentItem.getId());
    }

    public static abstract class ViewHolder extends BaseCardPresenter.ViewHolder {

        @NotNull
        protected abstract BaseOrderedIconsContainer getIconsContainer();

        @NotNull
        protected abstract ImageView getCoverView();

        @NotNull
        protected abstract FontTextView getTitleView();

        @NotNull
        protected abstract ContentMarkersView getContentMarkersView();

        @NotNull
        protected abstract FontTextView getSeriesInfoView();

        @NotNull
        protected abstract FontTextView getDescriptionView();

        ColorDrawable mPlaceHolderDrawable = new ColorDrawable();

        public ViewHolder(View view) {
            super(view);
        }
    }


    /**
     * Provider used for get always the recent series from the row presenter.
     */
    public interface SeriesProvider<T extends IBaseSeries> {

        @Nullable
        T getSeries();
    }
}
