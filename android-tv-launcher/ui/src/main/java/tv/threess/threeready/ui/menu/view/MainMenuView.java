/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.menu.view;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.widget.ImageView;

import androidx.leanback.widget.HorizontalGridView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import tv.threess.threeready.api.home.model.menu.MenuItem;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.MainMenuBinding;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * A custom view, where the menu item list will be created using the data from config.
 * Created by Szilard on 4/12/2017.
 */

public class MainMenuView extends BaseMenuView {

    private final MainMenuBinding mBinding;

    public MainMenuView(Context context) {
        this(context, null);
    }

    public MainMenuView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainMenuView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mBinding = MainMenuBinding.inflate(LayoutInflater.from(getContext()), this);
    }

    @Override
    @NotNull
    protected ImageView getLogo() {
        return mBinding.menuLogo;
    }

    @Override
    @NotNull
    protected HorizontalGridView getMenuView() {
        return mBinding.menuItemList;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mBinding.menuItemList.setPadding(Math.round(getResources().getDimension(R.dimen.menu_padding_left)),
                0, Math.round(getResources().getDimension(R.dimen.menu_padding_right)), 0);
        setUpMenuRightSide();

        for (int i = 0; i < mMenuIconViewList.size(); ++i) {
            mMenuIconViewList.get(i).setContentDescription(
                    new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_MAIN_NAVIGATION_ICONS))
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_ICON_NAME, mMenuIconViewList.get(i).getTransitionName())
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_POSITION, String.valueOf(i + 1))
                            .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TOTAL, String.valueOf(mMenuIconViewList.size()))
                            .toString());
        }

        mBinding.background.setBackgroundColor(Color.BLACK);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    if (findFocus() != null
                            && !mMenuIconViewList.isEmpty() && findFocus() == mMenuIconViewList.get(0) && mBinding.menuItemList.getAdapter() != null) {
                        mSelectedView = null;
                        mBinding.menuItemList.setSelectedPosition(mBinding.menuItemList.getAdapter().getItemCount() - 1);
                        mBinding.menuItemList.requestFocus();
                        return true;
                    } else {
                        return false;
                    }
                default:
                    return super.dispatchKeyEvent(event);
            }
        }
        return super.dispatchKeyEvent(event);
    }

    /**
     * adding to the list search, settings and notifications
     */
    private void setUpMenuRightSide() {
        // Search icon.
        mMenuIconViewList.add(mBinding.search);
        mBinding.search.setTransitionName(mTranslator.get(TranslationKey.TALKBACK_ICON_SEARCH));
        mBinding.search.setOnClickListener(v -> {
            if (mMenuClickListener != null) {
                reportMenuIconSelection(UILog.Page.Search.name());
                mSelectedView = mBinding.search;
                mMenuClickListener.onSearchIconClick();
            }
        });

        // Settings icon.
        mMenuIconViewList.add(mBinding.settings);
        mBinding.settings.setTransitionName(mTranslator.get(TranslationKey.TALKBACK_ICON_SETTINGS));
        mBinding.settings.setOnClickListener(v -> {
            if (mMenuClickListener != null) {
                reportMenuIconSelection(UILog.Page.Settings.name());
                mSelectedView = mBinding.settings;
                mMenuClickListener.onSettingsIconClick();
            }
        });

        // Notification icon.
        mMenuIconViewList.add(mBinding.notification);
        mBinding.notification.setTransitionName(mTranslator.get(TranslationKey.TALKBACK_ICON_NOTIFICATIONS));
        mBinding.notification.setOnClickListener(v -> {
            if (mMenuClickListener != null) {
                reportMenuIconSelection(UILog.Page.Notifications.name());
                mSelectedView = mBinding.notification;
                mMenuClickListener.onNotificationIconClick();
            }
        });
    }

    public void setMenuItems(List<MenuItem> menuItemList) {
        mMenuItemList.clear();
        mMenuItemList.addAll(menuItemList);
        if (mMenuAdapter != null) {
            mMenuAdapter.notifyDataSetChanged();
        }
    }

    private void reportMenuIconSelection(String page) {
        Log.event(new Event<>(UILogEvent.PageSelection)
                .addDetail(UILogEvent.Detail.PageName, page));
    }

    @Override
    public UILog.Page getFocusSubPointName() {
        if (mBinding.search.hasFocus()) {
            return UILog.Page.Search;
        }

        if (mBinding.settings.hasFocus()) {
            return UILog.Page.Settings;
        }

        if (mBinding.notification.hasFocus()) {
            return UILog.Page.Notifications;
        }
        return super.getFocusSubPointName();
    }
}
