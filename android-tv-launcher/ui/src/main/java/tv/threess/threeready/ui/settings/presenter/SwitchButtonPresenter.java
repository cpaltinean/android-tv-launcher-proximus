package tv.threess.threeready.ui.settings.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.leanback.widget.Presenter;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.SettingsSwitchButtonBinding;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.settings.listener.ItemClickListener;
import tv.threess.threeready.ui.settings.model.SwitchButtonItem;

/**
 * Presenter of the {@link SwitchButtonItem} into the layout
 *
 * @author Denisa Trif
 * @since 29.05.2019
 */
public class SwitchButtonPresenter extends BasePresenter<SwitchButtonPresenter.ViewHolder, SwitchButtonItem> {

    private final ItemClickListener mItemClickListener;

    public SwitchButtonPresenter(Context mContext, ItemClickListener itemClickListener) {
        super(mContext);
        mItemClickListener = itemClickListener;
    }

    @Override
    public SwitchButtonPresenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new SwitchButtonPresenter.
                ViewHolder(SettingsSwitchButtonBinding.inflate(LayoutInflater.from(parent.getContext())));
    }

    @Override
    public void onBindHolder(ViewHolder holder, SwitchButtonItem switchButtonItem) {
        holder.mBinding.title.setText(switchButtonItem.getTitle());
        holder.mBinding.title.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0,
                switchButtonItem.isChecked() ? R.drawable.ico_on : R.drawable.ico_off, 0);
        holder.mBinding.subtitle.setText(switchButtonItem.getSubtitle());
    }

    @Override
    public void onClicked(ViewHolder holder, SwitchButtonItem item) {
        mItemClickListener.onItemClick(item);
    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private final SettingsSwitchButtonBinding mBinding;

        public ViewHolder(SettingsSwitchButtonBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
