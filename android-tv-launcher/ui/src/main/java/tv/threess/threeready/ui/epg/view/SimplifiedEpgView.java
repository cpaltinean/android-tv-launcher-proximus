package tv.threess.threeready.ui.epg.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.FrameLayout;

/**
 * Epg view which displays the available channels and programs.
 * Synchronizes the {@link ChannelZapperView} and {@link ProgramGuideView} components.
 *
 * @author Barabas Attila
 * @since 2017.11.16
 */

public abstract class SimplifiedEpgView extends FrameLayout {

    public SimplifiedEpgView(Context context) {
        this(context, null);
    }

    public SimplifiedEpgView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SimplifiedEpgView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public SimplifiedEpgView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflateLayout();

        ChannelZapperView.OnChannelSelectedListener onChannelSelectedListener = (channel, isFromFavoriteSection, changed) -> {
            if (changed) {
                getProgramGuideView().onSelectedChannelChanged(channel, isFromFavoriteSection);
            }
        };
        getChannelZapperView().setOnChannelSelectedListener(onChannelSelectedListener);
    }

    protected abstract ChannelZapperView getChannelZapperView();

    protected abstract ProgramGuideView getProgramGuideView();

    protected abstract void inflateLayout();

    public void initialize(String channelId, boolean isFavorite) {
        getChannelZapperView().setInitialChannel(channelId, isFavorite);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (!isShown()) {
            return false;
        }

        ProgramGuideView programGuideView = getProgramGuideView();
        if (programGuideView.dispatchKeyEvent(event)) {
            return true;
        }

        ChannelZapperView channelZapperView = getChannelZapperView();
        if (channelZapperView.dispatchKeyEvent(event)) {
            return true;
        }

        return super.dispatchKeyEvent(event);
    }

}
