/*
 *  Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.utils;

import android.os.Handler;
import android.os.Looper;

/**
 *  Utility class to detect user inactivity.
 *  A {@link InteractionListener} will be notified
 *  when there is no user interaction in the given period of time.
 *
 * @author Barabas Attila
 * @since 2017.04.30
 */
public class UserInactivityDetector {

    private long mTimeout;
    private boolean mIsStarted;
    private final Handler mHandler;
    private final InteractionListener mInteractionListener;

    public UserInactivityDetector(long timeout, InteractionListener userInteractionListener) {
        mIsStarted = false;
        mTimeout = timeout;
        mHandler = new Handler(Looper.getMainLooper());
        mInteractionListener = userInteractionListener;
    }

    public UserInactivityDetector(InteractionListener userInteractionListener) {
        this(0, userInteractionListener);
    }

    /**
     * Start the user interaction detection.
     */
    public void start() {
        mIsStarted = true;
        mHandler.removeCallbacks(mNotifyRunnable);
        if (mTimeout > 0) {
            mHandler.postDelayed(mNotifyRunnable, mTimeout);
        }
    }

    /**
     * Start the user interaction detection.
     */
    public void start(long timeout) {
        mTimeout = timeout;
        this.start();
    }

    /**
     * This method should be called on user interaction.
     */
    public void reset() {
        mHandler.removeCallbacks(mNotifyRunnable);
        if (mIsStarted && mTimeout > 0) {
            mHandler.postDelayed(mNotifyRunnable, mTimeout);
        }
    }

    /**
     * This method should be called on user interaction.
     */
    public void reset(long timeout) {
        mTimeout = timeout;
        this.reset();
    }

    /**
     * Stop the user interaction detection.
     */
    public void stop() {
        mIsStarted = false;
        mHandler.removeCallbacks(mNotifyRunnable);
    }

    private final Runnable mNotifyRunnable = new Runnable() {
        @Override
        public void run() {
            if (mInteractionListener != null) {
                mInteractionListener.onNoInteraction();
            }
        }
    };

    /**
     * Lister which will be notified when there is no user interaction in the defined time.
     */
    public interface InteractionListener {
        void onNoInteraction();
    }
}
