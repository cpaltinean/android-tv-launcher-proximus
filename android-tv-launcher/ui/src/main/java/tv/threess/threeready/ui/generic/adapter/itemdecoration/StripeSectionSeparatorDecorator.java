package tv.threess.threeready.ui.generic.adapter.itemdecoration;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;

import java.util.List;

import androidx.leanback.widget.HorizontalGridView;
import androidx.recyclerview.widget.RecyclerView;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;

/**
 * Item decorator which draws separator line between label sections.
 *
 * @author Barabas Attila
 * @since 2018.08.06
 */
public class StripeSectionSeparatorDecorator extends RecyclerView.ItemDecoration{
    private static final String TAG = Log.tag(StripeSectionSeparatorDecorator.class);

    List<? extends StripeSeparator> mSeparators;
    ItemBridgeAdapter mAdapter;
    ExtraOffsetProvider mExtraOffsetProvider;
    int mTranslationY;


    public StripeSectionSeparatorDecorator() {
    }

    public StripeSectionSeparatorDecorator(int translationY, ExtraOffsetProvider extraOffsetProvider) {

        mTranslationY = translationY;
        mExtraOffsetProvider = extraOffsetProvider;
    }

    public void setAdapter(ItemBridgeAdapter adapter) {
        mAdapter = adapter;
    }

    public void setSeparators(List<? extends StripeSeparator> separators) {
        mSeparators = separators;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        if (!(parent instanceof HorizontalGridView)
                || mSeparators == null || mAdapter == null) {
            Log.w(TAG, "Separator could not be applied.");
            return;
        }

        HorizontalGridView gridView = (HorizontalGridView) parent;

        if (mSeparators.size() <= 1) {
            return;
        }

        RecyclerView.ViewHolder holder = parent.getChildViewHolder(view);
        if (holder == null) {
            return;
        }

        int normalizedPosition = mAdapter.getNormalizedPosition(holder.getAdapterPosition());

        // Set spacing for section separator.
        for (StripeSeparator separator : mSeparators) {
            if ((mAdapter.isCircular() || separator.getStartPosition() > 0)
                    && normalizedPosition == separator.getStartPosition()) {
                outRect.left = separator.getSeparatorDrawable().getIntrinsicWidth()
                        + gridView.getHorizontalSpacing() + getSeparatingSpacingExtra();
                return;
            }
        }
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDraw(c, parent, state);

        if (!(parent instanceof HorizontalGridView)
                || mSeparators == null || mAdapter == null) {
            Log.w(TAG, "Separator could not be applied.");
            return;
        }

        HorizontalGridView gridView = (HorizontalGridView) parent;

        if (mSeparators.size() <= 1) {
            return;
        }

        int top = parent.getPaddingTop() + mTranslationY;


        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            RecyclerView.ViewHolder holder = parent.getChildViewHolder(child);

            if (holder == null) {
                continue;
            }

            int normalizedPosition = mAdapter.getNormalizedPosition(holder.getAdapterPosition());

            // Draw separator line.
            for (StripeSeparator separator : mSeparators) {

                if ((mAdapter.isCircular() || separator.getStartPosition() > 0)
                        && normalizedPosition == separator.getStartPosition()) {

                    // Calculate position for separator line.
                    int bottom = parent.getPaddingTop() + child.getHeight() + mTranslationY;
                    int offsetExtra = getOffsetExtra(child);

                    int left = child.getLeft() - separator.getSeparatorDrawable().getIntrinsicWidth()
                                - gridView.getHorizontalSpacing() - getSeparatingSpacingExtra() + offsetExtra ;
                    int right = child.getLeft() - gridView.getHorizontalSpacing() + offsetExtra;

                    separator.getSeparatorDrawable().setBounds(left, top, right, bottom);
                    separator.getSeparatorDrawable().draw(c);
                }
            }
        }
    }

    private int getOffsetExtra(View child) {
        if (mExtraOffsetProvider != null) {
            return mExtraOffsetProvider.calculateOffsetExtraForChild(child);
        }

        return 0;
    }

    private int getSeparatingSpacingExtra() {
        return 22;
    }

    /**
     * Utility class use to set extra horizontal offset for separators.
     */
    public interface ExtraOffsetProvider {
        int calculateOffsetExtraForChild(View child);
    }
}
