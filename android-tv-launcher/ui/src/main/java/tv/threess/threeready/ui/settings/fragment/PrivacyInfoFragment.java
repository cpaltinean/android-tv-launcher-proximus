package tv.threess.threeready.ui.settings.fragment;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.PrivacyInfoFragmentBinding;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.settings.model.MainSettingsItem;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Class for GDPR page which holds QR code.
 *
 * Created by Bartalus Csaba - Zsolt, since  18.02.2022.
 */
public class PrivacyInfoFragment extends BaseFragment {

    private static final String DOT_CHARACTER = "  \u2022 ";

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);

    private PrivacyInfoFragmentBinding mBinding;

    private Disposable mQrCodeUrlDisposable;
    private Handler mDestroyHandler;

    public static PrivacyInfoFragment newInstance() {
        return new PrivacyInfoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (mBinding == null) {
            mBinding = PrivacyInfoFragmentBinding.inflate(inflater, container, false);

            mBinding.title.setTextColor(mLayoutConfig.getFontColor());
            mBinding.description.setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f));

            updateLogo(mBinding.getRoot());
            updateTitle();
            updateDescription();
            updateButtons();
            updateQrCode();

            mBinding.getRoot().setVisibility(View.GONE);
        }


        return mBinding.getRoot();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mDestroyHandler = new Handler();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mDestroyHandler.removeCallbacks(mDestroyViewRunnable);
        mDestroyHandler = null;
    }

    private void updateLogo(View view) {
        mBinding.logo.setImageDrawable(ContextCompat.getDrawable(view.getContext(), R.drawable.proximus_logo_title));
    }

    private void updateTitle() {
        mBinding.title.setText(mTranslator.get(TranslationKey.SETTINGS_PRIVACY));
    }

    private void updateDescription() {
        String description = mTranslator.get(TranslationKey.SETTINGS_PRIVACY_INFO_LINE1)
                + "\n"+ DOT_CHARACTER + mTranslator.get(TranslationKey.SETTINGS_PRIVACY_INFO_LINE2)
                + "\n"+ DOT_CHARACTER + mTranslator.get(TranslationKey.SETTINGS_PRIVACY_INFO_LINE3)
                + "\n"+ DOT_CHARACTER + mTranslator.get(TranslationKey.SETTINGS_PRIVACY_INFO_LINE4);
        mBinding.description.setText(description);
    }

    private void updateButtons() {
        mBinding.continueButton.setText(mTranslator.get(TranslationKey.BUTTON_CONTINUE_ON_TV));
        mBinding.continueButton.setBackground(UiUtils.createFtiButtonBackground(getContext(), mLayoutConfig));
        mBinding.continueButton.setOnClickListener(view -> {
            mNavigator.goBack();
            mNavigator.openPrivacySettings();
        });
        mBinding.continueButton.requestFocus();

        mBinding.cancelButton.setText(mTranslator.get(TranslationKey.CANCEL_BUTTON));
        mBinding.cancelButton.setBackground(UiUtils.createFtiButtonBackground(getContext(), mLayoutConfig));
        mBinding.cancelButton.setOnClickListener(view -> {
            mNavigator.goBack();
            mNavigator.openSettings(MainSettingsItem.MainItemType.PRIVACY);
        });
    }

    private void updateQrCode() {
        RxUtils.disposeSilently(mQrCodeUrlDisposable);
        mQrCodeUrlDisposable = mAccountRepository.getConsentQrCode()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::loadQrCode,
                        throwable -> {
                            Log.v(TAG, "Qr code download failed!", throwable);
                            mNavigator.goBack();
                            mNavigator.openPrivacySettings();
                        });
    }

    private void loadQrCode(IImageSource source) {
        Context context = getContext();
        if (context == null) {
            return;
        }

        Glide.with(context.getApplicationContext()).clear(mBinding.qrCode);
        Glide.with(context.getApplicationContext())
                .load(source)
                .addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                Target<Drawable> target, boolean isFirstResource) {
                        mDestroyHandler.removeCallbacks(mDestroyViewRunnable);
                        mDestroyHandler.post(mDestroyViewRunnable);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target,
                                                   DataSource dataSource, boolean isFirstResource) {
                        mBinding.getRoot().setVisibility(View.VISIBLE);
                        return false;
                    }
                })
                .into(mBinding.qrCode);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RxUtils.disposeSilently(mQrCodeUrlDisposable);
        Context context = getContext();
        if (context != null) {
            Glide.with(context.getApplicationContext()).clear(mBinding.qrCode);
        }
    }

    @Override
    public boolean onBackPressed() {
        mNavigator.openSettings(MainSettingsItem.MainItemType.PRIVACY);
        return super.onBackPressed();
    }

    private final Runnable mDestroyViewRunnable = () -> {
        mNavigator.goBack();
        mNavigator.openPrivacySettings();
    };
}
