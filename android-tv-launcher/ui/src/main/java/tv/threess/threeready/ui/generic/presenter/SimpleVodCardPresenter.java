package tv.threess.threeready.ui.generic.presenter;

import android.content.Context;

import java.util.Objects;

import tv.threess.threeready.api.home.model.generic.ContentMarkers;
import tv.threess.threeready.api.vod.model.IBaseVodItem;

/**
 * Base presenter for a simple Vod card. Contains only a cover View, a progress indicator and a content Marker.
 *
 * @author Solyom Zsolt
 * @since 2020.02.24
 */

public abstract class SimpleVodCardPresenter<THolder extends SimpleVodCardPresenter.ViewHolder, TItem extends IBaseVodItem>
        extends ContentMarkerCardPresenter<THolder, TItem> {

    public SimpleVodCardPresenter(Context context) {
        super(context);
    }

    @Override
    protected void openDetailPage(THolder holder, TItem vod) {
        mNavigator.showVodMovieDetails(vod);
    }

    @Override
    protected void updateContentMarkers(THolder holder, TItem vod) {
        holder.getContentMarkerView().showMarkers(vod, ContentMarkers.TypeFilter.Cards);
    }

    @Override
    public long getStableId(IBaseVodItem vod) {
        return Objects.hash(vod.getId(), vod.isSubscribed(), vod.isRented());
    }
}