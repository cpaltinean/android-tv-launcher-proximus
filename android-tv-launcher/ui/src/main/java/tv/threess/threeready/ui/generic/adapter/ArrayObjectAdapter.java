/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.adapter;

import androidx.leanback.widget.ObjectAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Stores a collection of the data objects and notify the recycler view adapter about the data changes.
 * @see ItemBridgeAdapter
 *
 * Created by Barabas Attila on 4/12/2017.
 */

public class ArrayObjectAdapter<TObject> extends ObjectAdapter implements Serializable{

    private List<TObject> mItems;

    /**
     * Constructs an adapter.
     */
    public ArrayObjectAdapter() {
        super();
        mItems = new ArrayList<>();
    }

    public ArrayObjectAdapter(Collection<? extends TObject> items) {
        super();
        mItems = new ArrayList<>(items);
    }

    public List<TObject> getItems() {
        return mItems;
    }

    @Override
    public int size() {
        return mItems.size();
    }

    @Override
    public TObject get(int index) {
        return mItems.get(index);
    }

    @Override
    public final long getId(int position) {
        // Avoid stable id override here.
        // Will be handled by the ItemBridgeAdapter
        return super.getId(position);
    }

    /**
     * Returns the index for the first occurrence of item in the adapter, or -1 if
     * not found.
     *
     * @param item The item to find in the list.
     * @return Index of the first occurrence of the item in the adapter, or -1
     * if not found.
     */
    public int indexOf(TObject item) {
        return mItems.indexOf(item);
    }

    public boolean contains(TObject item) {
        return mItems.contains(item);
    }

    /**
     * Notify that the content of a range of items changed. Note that this is
     * not same as items being added or removed.
     *
     * @param positionStart The position of first item that has changed.
     * @param itemCount     The count of how many items have changed.
     */
    public void notifyArrayItemRangeChanged(int positionStart, int itemCount) {
        notifyItemRangeChanged(positionStart, itemCount);
    }

    /**
     * Adds an item to the end of the adapter.
     *
     * @param item The item to add to the end of the adapter.
     */
    public void add(TObject item) {
        add(mItems.size(), item);
    }

    /**
     * Inserts an item into this adapter at the specified index.
     * If the index is >= {@link #size} an exception will be thrown.
     *
     * @param index The index at which the item should be inserted.
     * @param item  The item to insert into the adapter.
     */
    public void add(int index, TObject item) {
        mItems.add(index, item);
        notifyItemRangeInserted(index, 1);
    }

    /**
     * Adds the objects in the given collection to the adapter, starting at the
     * given index.  If the index is >= {@link #size} an exception will be thrown.
     *
     * @param index The index at which the items should be inserted.
     * @param items A {@link Collection} of items to insert.
     */
    public void addAll(int index, Collection<TObject> items) {
        int itemsCount = items.size();
        if (itemsCount == 0) {
            return;
        }
        mItems.addAll(index, items);
        notifyItemRangeInserted(index, itemsCount);
    }

    /**
     * Remove the existent items and replace it
     */
    public void replaceAll(Collection<? extends TObject> newItems){
        int itemsCount = newItems.size();
        if (itemsCount == 0) {
            clear();
        }
        mItems = new ArrayList<>(newItems);
        notifyDataSourceChanged();
    }

    /**
     * Removes the first occurrence of the given item from the adapter.
     *
     * @param item The item to remove from the adapter.
     * @return True if the item was found and thus removed from the adapter.
     */
    public boolean remove(TObject item) {
        int index = mItems.indexOf(item);
        if (index >= 0) {
            mItems.remove(index);
            notifyItemRangeRemoved(index, 1);
        }
        return index >= 0;
    }

    /**
     * Replaces item at position with a new item and calls notifyItemRangeChanged()
     * at the given position.  Note that this method does not compare new item to
     * existing item.
     *
     * @param position The index of item to replace.
     * @param item     The new item to be placed at given position.
     */
    public void replace(int position, TObject item) {
        mItems.set(position, item);
        notifyItemRangeChanged(position, 1);
    }

    /**
     * Removes a range of items from the adapter. The range is specified by giving
     * the starting position and the number of elements to remove.
     *
     * @param position The index of the first item to remove.
     * @param count    The number of items to remove.
     * @return The number of items removed.
     */
    public int removeItems(int position, int count) {
        int itemsToRemove = Math.min(count, mItems.size() - position);
        if (itemsToRemove <= 0) {
            return 0;
        }

        for (int i = 0; i < itemsToRemove; i++) {
            mItems.remove(position);
        }
        notifyItemRangeRemoved(position, itemsToRemove);
        return itemsToRemove;
    }

    /**
     * Removes all items from this adapter, leaving it empty.
     */
    public void clear() {
        int itemCount = mItems.size();
        if (itemCount == 0) {
            return;
        }
        mItems.clear();
        notifyItemRangeRemoved(0, itemCount);
    }

    public void notifyDataSourceChanged() {
        super.notifyChanged();
    }
}
