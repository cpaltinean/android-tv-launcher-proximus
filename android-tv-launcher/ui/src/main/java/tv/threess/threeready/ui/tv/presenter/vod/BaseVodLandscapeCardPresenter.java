/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.tv.presenter.vod;

import android.content.Context;
import android.text.TextUtils;

import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.vod.model.IBaseVodItem;

/**
 * Abstract base presenter of landscape vod card.
 * <p>
 * Created by Szilard on 6/6/2017.
 */

abstract class BaseVodLandscapeCardPresenter<THolder extends BaseVodLandscapeCardPresenter.ViewHolder>
        extends VodCardPresenter<THolder, IBaseVodItem> {

    BaseVodLandscapeCardPresenter(Context context) {
        super(context);
    }

    @Override
    public void onFocusedState(ModuleData<?> moduleData, THolder holder, IBaseVodItem vod) {
        super.onFocusedState(moduleData, holder, vod);

        updateParentalRatingIcon(holder, vod);

        // Start automatic text scrolling.
        holder.getTitleView().setSelected(true);
        holder.getTitleView().setEllipsize(TextUtils.TruncateAt.MARQUEE);
    }

    @Override
    public void onDefaultState(ModuleData<?> moduleData, THolder holder, IBaseVodItem vod) {
        super.onDefaultState(moduleData, holder, vod);

        updateParentalRatingIcon(holder, vod);

        // Stop automatic text scrolling.
        holder.getTitleView().setSelected(false);
        holder.getTitleView().setEllipsize(TextUtils.TruncateAt.END);
    }

}
