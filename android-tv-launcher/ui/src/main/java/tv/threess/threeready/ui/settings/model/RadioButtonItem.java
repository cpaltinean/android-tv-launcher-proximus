/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.settings.model;

/**
 * Model class for settings pages
 * @author Paul
 * @since 2017.05.30
 */

public class RadioButtonItem implements ISettingsItem {
    /**
     * The text will be displayed in the list
     */
    private final String mTitle;
    private boolean mChecked;
    /**
     * Item tag object
     */
    private final String mPreferenceGroup;

    /**
     * Extra information. Currently will be set in the database for the settings name provided.
     */
    private final String mValue;

    public RadioButtonItem(String title, String value, String preferenceGroup) {
        mTitle = title;
        mValue = value;
        mPreferenceGroup = preferenceGroup;
    }

    public String getTitle() {
        return mTitle;
    }

    public boolean isChecked() {
        return mChecked;
    }

    public void setChecked(boolean checked) {
        mChecked = checked;
    }

    public String getPreferenceGroup() {
        return mPreferenceGroup;
    }

    public String getValue() {
        return mValue;
    }
}
