package tv.threess.threeready.ui.generic.player.plugin;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.command.StopCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.plugin.PlaybackPlugin;
import tv.threess.threeready.player.result.EventResult;
import tv.threess.threeready.player.result.Result;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Plugin class used for handle the changes between the programs on IpTv.
 *
 * @author David Bondor
 * @since 2020.2.18
 */
public class IpTvPlayerPlugin implements PlaybackPlugin {
    private static final String TAG = Log.tag(IpTvPlayerPlugin.class);

    // maximum time buffer to get the epg data
    private static final long MAX_TIME_BUFFER = TimeUnit.HOURS.toMillis(6);

    // pad one hour with no info available if epg is missing from database
    private static final long WINDOW_LENGTH = TimeUnit.HOURS.toMillis(1);

    // default duration to re-fetch currently playing broadcast list
    private static final long RETRY_SCHEDULE = TimeUnit.SECONDS.toMillis(30);

    private final Handler mHandler = new Handler(Looper.getMainLooper());

    // A separated runnable only for program expiration
    private final Runnable mProgramExpirationRunnable = this::onCurrentProgramExpired;

    private final PlaybackDetailsManager mPlaybackManager;
    private Disposable mCurrentProgramDisposable;

    private final TvRepository mTvRepository = Components.get(TvRepository.class);
    private final Translator mTranslator = Components.get(Translator.class);

    public IpTvPlayerPlugin(PlaybackDetailsManager playbackManager) {
        mPlaybackManager = playbackManager;
    }

    @Override
    public boolean isSubscribed(PlaybackDomain domain) {
        return domain == PlaybackDomain.LiveTvIpTv || domain == PlaybackDomain.RadioIpTv;
    }

    @Override
    public void onDestroy() {
        RxUtils.disposeSilently(mCurrentProgramDisposable);
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onEvent(EventResult result, Bundle details) {
        PlaybackPlugin.super.onEvent(result, details);
        if (result.event == PlaybackEvent.PlaybackItemChanged) {
            mHandler.post(() -> mPlaybackManager.switchCurrentProgram());
        }
    }

    @Override
    public void onResult(PlaybackCommand command, Result result) {
        PlaybackPlugin.super.onResult(command, result);
        if (command instanceof StopCommand) {
            mHandler.removeCallbacksAndMessages(null);
            RxUtils.disposeSilently(mCurrentProgramDisposable);
        }
        else if (command instanceof StartCommand) {
            // Update audio/subtitle button after start finished or failed.
            onCurrentProgramExpired();
        }
    }

    /**
     * Load and display the current live program on the given channel.
     */
    private void onCurrentProgramExpired() {
        // Dispose current program request
        RxUtils.disposeSilently(mCurrentProgramDisposable);
        if (!mPlaybackManager.inPlaybackDomain(PlaybackDomain.LiveTvIpTv, PlaybackDomain.RadioIpTv)) {
            Log.d(TAG, "Current program expired, but playback is on other domain");
            return;
        }

        TvChannel channel = mPlaybackManager.getChannelData();
        if (channel == null) {
            Log.e(TAG, "Cannot get current program on null channel.", new Exception());
            return;
        }

        long from = mPlaybackManager.getExclusiveDetails().getBufferStart();
        long to = System.currentTimeMillis();
        if (from <= 0) {
            from = to;
        }
        if (to - from > MAX_TIME_BUFFER) {
            Log.d(TAG, "time buffer is too large: " + Log.formatDuration(to - from));
            from = to - MAX_TIME_BUFFER;
        }

        Observable.create(mTvRepository.getEpgBroadcastsBetweenTimestamps(channel, from, to,
                mTranslator.get(TranslationKey.EPG_NO_INFO),
                mTranslator.get(TranslationKey.EPG_NO_INFO)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<IBroadcast>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        mCurrentProgramDisposable = d;
                    }

                    @Override
                    public void onNext(@NonNull List<IBroadcast> broadcasts) {
                        Log.d(TAG, "On program list updated.");

                        // Update current play list and active playback.
                        mPlaybackManager.updatePlayerData(broadcasts);

                        // Select the live broadcast from the list for sending the program end notification
                        sendProgramEndNotification(broadcasts);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e(TAG, "Could not load program list.", e);
                    }

                    @Override
                    public void onComplete() {
                        Log.e(TAG, "Program list updates finished.");
                    }
                });
    }

    /**
     * Notify other components when the current program expires.
     */
    private void sendProgramEndNotification(Collection<IBroadcast> broadcasts) {
        long now = System.currentTimeMillis();
        long delay = RETRY_SCHEDULE;

        for (IBroadcast broadcast : broadcasts) {
            if (broadcast.getStart() <= now && now <= broadcast.getEnd()) {
                delay = broadcast.getEnd() - now;
                break;
            }
        }

        // Cancel & Remove only the program expiration runnable. Do not remove all callbacks!
        mHandler.removeCallbacks(mProgramExpirationRunnable);
        mHandler.postDelayed(mProgramExpirationRunnable, delay);
    }
}