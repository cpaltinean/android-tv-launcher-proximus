package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;

import java.util.List;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.model.ISeason;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.local.FontType;
import tv.threess.threeready.ui.databinding.SeasonLabelItemBinding;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Presenter for the season labels on series detail page.
 *
 * @author Barabas Attila
 * @since 2018.11.21
 */
public class SeasonLabelPresenter<TContentItem extends IContentItem> extends BasePresenter<SeasonLabelPresenter.ViewHolder, ISeason<TContentItem>> {

    private final Translator mTranslator = Components.get(Translator.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    private final ISeasonSelectionListener<TContentItem> mSeasonSelectionListener;
    private final ISelectedSeason<TContentItem> mSelectedSeason;

    public SeasonLabelPresenter(Context context, ISeasonSelectionListener<TContentItem> seasonSelectionListener, ISelectedSeason<TContentItem> selectedSeason) {
        super(context);
        mSeasonSelectionListener = seasonSelectionListener;
        mSelectedSeason = selectedSeason;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        SeasonLabelItemBinding binding = SeasonLabelItemBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.title.setTextColor(createTextColor());
        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, ISeason<TContentItem> season, List<?> payLoads) {
        holder.view.setActivated(isActivated(season));
        updateFontType(holder);
    }

    @Override
    public void onBindHolder(ViewHolder holder, ISeason<TContentItem> season) {
        holder.view.setActivated(isActivated(season));
        updateTitle(holder, season);
        updateFontType(holder);
    }

    private void updateTitle(ViewHolder holder, ISeason<TContentItem> season) {
        if (season.getSeasonNumber() != null) {
            String seasonText = mTranslator.get(TranslationKey.SCREEN_DETAIL_PROGRAM_SEASON) + " " + season.getSeasonNumber();
            holder.mBinding.title.setText(seasonText);
        } else if (!TextUtils.isEmpty(season.getSeasonTitle())) {
            holder.mBinding.title.setText(season.getSeasonTitle());
        } else {
            holder.mBinding.title.setText(mTranslator.get(
                    TranslationKey.SCREEN_DETAIL_PROGRAM_SEASON_TAB_OTHER_EPISODE));
        }
    }

    /**
     * @return True if season is activated, otherwise false.
     */
    private boolean isActivated(ISeason<TContentItem> season) {
        if (mSelectedSeason == null) {
            return false;
        }
        ISeason<TContentItem> selectedSeason = mSelectedSeason.getSelectedSeason();
        return Objects.equals(season.getSeasonNumber(), selectedSeason.getSeasonNumber());
    }

    @Override
    public void onClicked(ViewHolder holder, ISeason<TContentItem> season) {
        super.onClicked(holder, season);

        if (mSeasonSelectionListener != null) {
            mSeasonSelectionListener.onSeasonSelected(holder, season);
            updateFontType(holder);
        }
    }

    private void updateFontType(ViewHolder holder) {
        holder.mBinding.title.setFontType((holder.view.isActivated()) ? FontType.BOLD : FontType.REGULAR);
    }

    /**
     * Creates a selector for the color that should be used for the text.
     */
    public ColorStateList createTextColor() {
        int[] focusedStateList = new int[]{android.R.attr.state_focused};
        int[] activatedStateList = new int[]{android.R.attr.state_activated};
        int[] defaultStateList = new int[]{};

        return new ColorStateList(
                new int[][]{focusedStateList, activatedStateList, defaultStateList},
                new int[]{mLayoutConfig.getMenuSelectorFocused(),
                        mLayoutConfig.getMenuSelectorActive(),
                        ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f)});
    }

    @Override
    public long getStableId(ISeason<TContentItem> season) {
        return Objects.hash(season.getSeasonTitle(), season.getSeasonNumber());
    }

    static class ViewHolder extends Presenter.ViewHolder {

        private final SeasonLabelItemBinding mBinding;

        public ViewHolder(SeasonLabelItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }

    public interface ISeasonSelectionListener<TContentItem extends IContentItem> {
        void onSeasonSelected(ViewHolder viewHolder, ISeason<TContentItem> season);
    }

    public interface ISelectedSeason<TContentItem extends IContentItem> {
        ISeason<TContentItem> getSelectedSeason();
    }

}
