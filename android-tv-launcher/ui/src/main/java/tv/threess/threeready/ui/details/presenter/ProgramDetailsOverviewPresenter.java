/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Pair;
import android.util.Size;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextSwitcher;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.leanback.widget.HorizontalGridView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.model.PlayOption;
import tv.threess.threeready.api.generic.model.WatchlistType;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.pvr.model.SeriesRecordingStatus;
import tv.threess.threeready.api.pvr.model.SingleRecordingStatus;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.BroadcastDetailsOverlayBinding;
import tv.threess.threeready.ui.details.DetailOpenedFrom;
import tv.threess.threeready.ui.generic.adapter.DetailPageButtonOrder;
import tv.threess.threeready.ui.generic.hint.HintManager;
import tv.threess.threeready.ui.generic.model.BroadcastPlayOption;
import tv.threess.threeready.ui.generic.presenter.ActionModel;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.pvr.RecordingActionHelper;
import tv.threess.threeready.ui.tv.view.ProgressIndicatorView;
import tv.threess.threeready.ui.utils.ImageTransform;
import tv.threess.threeready.ui.utils.glide.FallbackTarget;

/**
 * Presenter class for a program detail overview.
 *
 * @author Barabas Attila
 * @since 2018.07.26
 */
public class ProgramDetailsOverviewPresenter extends BaseSingleDetailOverviewPresenter<ProgramDetailsOverviewPresenter.ViewHolder, IBroadcast> {
    private static final String TAG = Log.tag(ProgramDetailsOverviewPresenter.class);

    private final TvRepository mTvRepository = Components.get(TvRepository.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);
    private final RecordingActionHelper mRecordingActionHelper = Components.get(RecordingActionHelper.class);
    private final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);
    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);

    private final DetailOpenedFrom mOpenedFrom;

    public ProgramDetailsOverviewPresenter(Context context, DetailOpenedFrom openedFrom, IDetailLoaded callback) {
        super(context, callback);
        mOpenedFrom = openedFrom;
    }

    @Override
    protected BaseDetailsOverviewPresenter.ViewHolder createViewHolder(ViewGroup parent) {
        BroadcastDetailsOverlayBinding binding = BroadcastDetailsOverlayBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        
        ViewHolder holder = new ViewHolder(binding);
        holder.mGlideTarget = new FallbackTarget(new Size(
                mContext.getResources().getDimensionPixelSize(R.dimen.details_cover_image_max_width),
                mContext.getResources().getDimensionPixelOffset(R.dimen.details_cover_image_max_height)),
                ContextCompat.getDrawable(parent.getContext(), R.drawable.default_detail_image_background)) {
            @Override
            protected void onImageLoaded(Drawable drawable) {
                holder.getBigCoverImage().setImageDrawable(drawable);
                holder.getGradientBottom().setVisibility(View.VISIBLE);
                holder.getFadingStart().setVisibility(View.VISIBLE);
                holder.getCoverImage().setVisibility(View.INVISIBLE);
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {
                holder.getBigCoverImage().setImageDrawable(null);
                holder.getCoverImage().setImageDrawable(null);
            }
        };

        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, IBroadcast broadcast) {
        super.onBindHolder(holder, broadcast);
        updateProviderLogo(holder, broadcast);
        updateProgramStatus(holder, broadcast);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);
        holder.mBinding.iconContainer.hideRecordingStatus();

        Glide.with(mContext).clear(holder.mBinding.providerLogo);
        Glide.with(mContext).clear(holder.mGlideTarget);

        RxUtils.disposeSilently(holder.mButtonStatsDisposable);
        RxUtils.disposeSilently(holder.mAddWatchlistDisposable);
        RxUtils.disposeSilently(holder.mRemoveWatchlistDisposable);
    }

    @Override
    protected void updateTitle(ViewHolder holder, IBroadcast broadcast) {
        if (!TextUtils.isEmpty(broadcast.getTitle())) {
            holder.mBinding.title.setText(broadcast.getTitle());

            String subtitle = buildSubtitle(broadcast);
            if (!TextUtils.isEmpty(subtitle)) {
                holder.mBinding.title.setTextColor(mLayoutConfig.getFontColor());
                holder.mBinding.title.setText(subtitle);
                holder.mBinding.title.setVisibility(View.VISIBLE);
            }
        } else {
            String subtitle = buildSubtitle(broadcast);
            if (!TextUtils.isEmpty(subtitle)) {
                holder.mBinding.title.setText(subtitle);
            }
        }
    }

    private String buildSubtitle(IContentItem contentItem) {
        return contentItem.getTitleWithSeasonEpisode(mTranslator, contentItem.getEpisodeTitle(), " ");
    }

    @Override
    protected Observable<IBookmark> getBookmark(ViewHolder holder, IBroadcast item) {
        return mTvRepository.getBookmarkForBroadcast(item,
                mOpenedFrom == DetailOpenedFrom.ContinueWatching);
    }

    @Override
    protected void updateProgressState(ViewHolder holder, IBroadcast item) {
        super.updateProgressState(holder, item);

        holder.mBinding.progressBar.setProgressListener(() -> holder.mBinding.progressBar.setVisibility(View.GONE));

        updateMarkersRow(holder, item);
    }

    private void updateProgramStatus(ViewHolder holder, IBroadcast broadcast) {
        holder.mBinding.iconContainer.hideRecordingStatus();

        RxUtils.disposeSilently(holder.mButtonStatsDisposable);
        holder.mButtonStatsDisposable = Observable.combineLatest(
                mPvrRepository.getRecordingStatus(broadcast),
                mAccountRepository.isInWatchlist(broadcast.getId()),
                Pair::new)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recordingStatusBooleanPair -> {
                    RecordingStatus recordingStatus = recordingStatusBooleanPair.first;
                    Boolean inWatchlist = recordingStatusBooleanPair.second;
                    Log.d(TAG, "Recording status and watchlist info retrieved, recording status = [" + recordingStatus + "] inWatchlist = [" + inWatchlist + "]");
                    holder.mBinding.iconContainer.showRecordingStatus(recordingStatus);
                    updateButtons(holder, broadcast, recordingStatus, inWatchlist);
                    updateInfoRow(holder, broadcast, recordingStatus); // Update availability.

                    if (!mIsPageLoaded) {
                        callbackDetail.loadPage();
                        mIsPageLoaded = true;
                    }
                }, throwable -> {
                    Log.e(TAG, "Couldn't get the recording status and watchlist information.", throwable);
                    updateButtons(holder, broadcast, null, null);
                    holder.mBinding.iconContainer.hideRecordingStatus();
                });
    }

    private void updateProviderLogo(ViewHolder holder, IBroadcast broadcast) {
        Glide.with(mContext).clear(holder.mBinding.providerLogo);
        Glide.with(mContext)
                .load(mNowOnTvCache.getChannel(broadcast.getChannelId()))
                .apply(ImageTransform.REQUEST_OPTIONS)
                .into(holder.mBinding.providerLogo);
    }

    @Override
    protected void updateMarkersRow(ViewHolder holder, IBroadcast broadcast) {
        // Display content marker
        holder.mBinding.contentMarker.showMarkers(broadcast);
        holder.mBinding.iconContainer.showParentalRating(broadcast.getParentalRating());
        holder.mBinding.iconContainer.hideHDIcon();
        updateReplayIconVisibility(holder, broadcast);
        updateDescriptiveAudioIcon(holder, broadcast);
        updateDescriptiveSubtitleIcon(holder, broadcast);

        Log.d(TAG, "Is dolby available : " + mPlaybackManager.isDolbyAvailable());
        if (mPlaybackManager.isBroadcastPlaying(broadcast) && mPlaybackManager.isDolbyAvailable()) {
            holder.mBinding.iconContainer.showDolby();
        } else {
            holder.mBinding.iconContainer.hideDolby();
        }
    }

    private void updateReplayIconVisibility(ViewHolder holder, IBroadcast broadcast) {
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        if (broadcast.canReplay(channel)) {
            holder.mBinding.iconContainer.showReplay();
        } else {
            holder.mBinding.iconContainer.hideReplay();
        }
    }

    private void updateDescriptiveAudioIcon(ViewHolder holder, IBroadcast broadcast) {
        if (broadcast.hasDescriptiveAudio()) {
            holder.mBinding.iconContainer.showDescriptiveAudio();
        } else {
            holder.mBinding.iconContainer.hideDescriptiveAudio();
        }
    }

    private void updateDescriptiveSubtitleIcon(ViewHolder holder, IBroadcast broadcast) {
        if (broadcast.hasDescriptiveSubtitle()) {
            holder.mBinding.iconContainer.showDescriptiveSubtitle();
        } else {
            holder.mBinding.iconContainer.hideDescriptiveSubtitle();
        }
    }

    @Override
    protected void updateInfoRow(ViewHolder holder, IBroadcast broadcast) {
        updateInfoRow(holder, broadcast, null);
    }

    protected void updateInfoRow(ViewHolder holder, IBroadcast broadcast, @Nullable RecordingStatus recordingStatus) {
        StringBuilder stringBuilder = new StringBuilder();

        String date = LocaleTimeUtils.getDate(broadcast, mTranslator, mLocaleSettings);
        if (!TextUtils.isEmpty(date)) {
            stringBuilder.append(date);
            stringBuilder.append(TimeUtils.DIVIDER);
        }

        String genres = TextUtils.join(", ",
                broadcast.getGenres().stream().limit(3).toArray());
        if (!TextUtils.isEmpty(genres)) {
            stringBuilder.append(genres);
            stringBuilder.append(TimeUtils.DIVIDER);
        }

        String releaseYear = broadcast.getReleaseYear();
        if (!TextUtils.isEmpty(releaseYear)) {
            stringBuilder.append(releaseYear);
            stringBuilder.append(TimeUtils.DIVIDER);
        }

        // Add broadcast availability.
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        String availability = LocaleTimeUtils.getBroadcastAvailabilityTime(channel, broadcast,
                recordingStatus != null ? recordingStatus.getRecording() : null, mTranslator);
        if (TextUtils.isEmpty(availability)) {
            stringBuilder.delete(stringBuilder.length() - TimeUtils.DIVIDER.length(), stringBuilder.length());
        } else {
            stringBuilder.append(availability);
        }

        holder.mBinding.infoRow.setText(stringBuilder.toString());
        holder.mBinding.infoRow.setVisibility(View.VISIBLE);
    }

    @Override
    protected void updateImage(ViewHolder holder, IBroadcast item) {
        Glide.with(mContext).clear(holder.mGlideTarget);
        Glide.with(mContext).load(item)
                .fallback(R.drawable.content_fallback_image_landscape)
                .error(R.drawable.content_fallback_image_landscape)
                .format(DecodeFormat.PREFER_RGB_565)
                .override(
                        mContext.getResources().getDimensionPixelSize(R.dimen.details_cover_width),
                        mContext.getResources().getDimensionPixelSize(R.dimen.details_cover_height))
                .optionalCenterInside()
                .into(holder.mGlideTarget);
    }

    protected void updateButtons(ViewHolder holder, IBroadcast broadcast, @Nullable RecordingStatus recordingStatus, Boolean inWatchlist) {
        SingleRecordingStatus singleRecordingStatus = recordingStatus == null ?
                null : recordingStatus.getSingleRecordingStatus();
        SeriesRecordingStatus seriesRecordingStatus = recordingStatus == null ?
                null : recordingStatus.getSeriesRecordingStatus();

        List<ActionModel> actions = new ArrayList<>();

        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        boolean isChannelSubscribed = channel != null && channel.isUserTvSubscribedChannel();

        // Display playback options.
        List<PlayOption> playOptions = BroadcastPlayOption.generatePlayOptions(
                channel, broadcast, holder.mBookmark, recordingStatus,
                mOpenedFrom == DetailOpenedFrom.Recording,
                mOpenedFrom == DetailOpenedFrom.ContinueWatching);

        updateNonPlayableViewGroupVisibility(holder, recordingStatus == null || recordingStatus.getRecording() == null
                || !recordingStatus.getRecording().isFailedRecording() || playOptions.size() > 0);

        if (playOptions.size() > 0) {
            // Start directly the first play option.
            actions.add(new ActionModel(
                    mTranslator.get(TranslationKey.SCREEN_DETAIL_PLAY_BUTTON), DetailPageButtonOrder.Watch, () -> {
                if (!isChannelSubscribed) {
                    mRecordingActionHelper.showChannelNotEntitledDialog();
                    return;
                }

                if (playOptions.size() == 1) {
                    mNavigator.showPlayer(playOptions.get(0), false);
                    mRecordingActionHelper.showStartReplayWarningNotification(recordingStatus, channel);
                } else {
                    mNavigator.showPlayOptionsDialog(playOptions, broadcast);
                }
            }));
        }

        if (recordingStatus != null) {
            // Display delete for completed recording.
            if (singleRecordingStatus == SingleRecordingStatus.COMPLETED) {
                actions.add(new ActionModel(
                        0, 0,
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_DELETE_BUTTON),
                        DetailPageButtonOrder.Delete,
                        () ->
                                mRecordingActionHelper.deleteRecording(
                                        recordingStatus.getRecording(), true, true)
                ));

                // Add reschedule possibility for live.
                if (broadcast.isLive() && isChannelSubscribed && mPvrRepository.isPVREnabled(broadcast)) {
                    actions.add(new ActionModel(
                            0, 0,
                            mTranslator.get(TranslationKey.SCREEN_DETAIL_RECORD_BUTTON),
                            DetailPageButtonOrder.Record,
                            () ->
                                    mRecordingActionHelper.scheduleSingleRecording(
                                            broadcast, recordingStatus, true, true)
                    ));
                }

           // Display cancel for scheduled and ongoing recording.
            } else if (singleRecordingStatus == SingleRecordingStatus.SCHEDULED
                    || singleRecordingStatus == SingleRecordingStatus.RECORDING) {
                actions.add(new ActionModel(
                        0, 0,
                        mTranslator.get(singleRecordingStatus == SingleRecordingStatus.RECORDING
                                ? TranslationKey.SCREEN_DETAIL_CANCEL_ONGOING_BUTTON
                                : TranslationKey.SCREEN_DETAIL_CANCEL_BUTTON),
                        DetailPageButtonOrder.Cancel,
                        () ->
                                mRecordingActionHelper.cancelSingleRecording(
                                        broadcast, recordingStatus,
                                        true, true)
                ));

                // Display record for live and future broadcasts.
            } else if (mPvrRepository.isPVREnabled(broadcast) && isChannelSubscribed &&
                    (broadcast.isLive() || broadcast.isFuture())) {
                actions.add(new ActionModel(
                        0, 0,
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_RECORD_BUTTON),
                        DetailPageButtonOrder.Record,
                        () ->
                                mRecordingActionHelper.scheduleSingleRecording(
                                        broadcast, recordingStatus, true, true
                                )));
            }

            // Series schedule.  Show 'Cancel Series' button.
            if (seriesRecordingStatus == SeriesRecordingStatus.SCHEDULED) {
                actions.add(new ActionModel(
                        0, 0,
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_CANCEL_SERIES_BUTTON),
                        DetailPageButtonOrder.Cancel,
                        () -> mRecordingActionHelper.cancelSeriesRecording(broadcast, true, true)));

                // Series not scheduled. Show schedule series button.
            } else if (mPvrRepository.isPVREnabled(broadcast) && isChannelSubscribed && !broadcast.isPast() && broadcast.isEpisode()) {
                actions.add(new ActionModel(
                        0, 0,
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_RECORD_SERIES_BUTTON),
                        DetailPageButtonOrder.Record,
                        () -> mRecordingActionHelper.scheduleSeriesRecording(broadcast, recordingStatus, true, true)));
            }
        }

        // Add watchlist button
        if (inWatchlist != null) {
            if (inWatchlist) {
                actions.add(new ActionModel(
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_REMOVEFROMWATCHLIST_BUTTON), DetailPageButtonOrder.RemoveFromWatchList,
                        () -> {
                            RxUtils.disposeSilently(holder.mRemoveWatchlistDisposable);
                            holder.mRemoveWatchlistDisposable = mAccountRepository.removeFromWatchlist(broadcast.getId(), WatchlistType.Broadcast)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(() ->
                                                    Log.d(TAG, "Successfully removed the item from watchlist"),
                                            throwable -> Log.e(TAG, "Could not remove item from watchlist.", throwable));
                        }
                ));
            } else {
                if (broadcast.shouldShowAddToWatchListButton(channel, singleRecordingStatus)) {
                    actions.add(new ActionModel(
                            mTranslator.get(TranslationKey.SCREEN_DETAIL_ADDTOWATCHLIST_BUTTON), DetailPageButtonOrder.AddToWatchList,
                            () -> {
                                Log.d(TAG, "add to watchlist called with: holder = [" + holder + "], broadcast = [" + broadcast + "], recordingStatus = [" + recordingStatus + "], inWatchlist = [" + inWatchlist + "]");
                                RxUtils.disposeSilently(holder.mAddWatchlistDisposable);
                                holder.mAddWatchlistDisposable = mAccountRepository.addToWatchlist(WatchlistType.Broadcast, broadcast.getId())
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(() -> {
                                                    Log.d(TAG, "Successfully added item to watchlist");
                                                    Components.get(HintManager.class).showAddToWatchlistHints();
                                                },
                                                throwable -> Log.e(TAG, "Could not add item to watchlist", throwable));
                            }
                    ));
                }
            }
        }

        updateActionModelDescriptions(actions, broadcast);

        holder.mAdapter.replaceAll(actions);
        holder.mBinding.actionGrid.requestFocus();
    }

    @Override
    protected void updateButtons(ViewHolder holder, IBroadcast broadcast) {
        updateButtons(holder, broadcast, null, null);
    }

    @Override
    public boolean onKeyEvent(ViewHolder holder, IBroadcast broadcast, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP && event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_RECORD) {
            mRecordingActionHelper.handleRecKeyAction(broadcast,
                    holder.mBinding.iconContainer.getRecordingStatus());
            return true;
        }

        return super.onKeyEvent(holder, broadcast, event);
    }

    public static class ViewHolder extends BaseSingleDetailOverviewPresenter.ViewHolder {

        private final BroadcastDetailsOverlayBinding mBinding;

        FallbackTarget mGlideTarget;

        Disposable mButtonStatsDisposable;
        Disposable mAddWatchlistDisposable;
        Disposable mRemoveWatchlistDisposable;

        public ViewHolder(BroadcastDetailsOverlayBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        @NonNull
        @Override
        public HorizontalGridView getActionGridView() {
            return mBinding.actionGrid;
        }

        @NonNull
        @Override
        ProgressIndicatorView getProgressBar() {
            return mBinding.progressBar;
        }

        @NonNull
        @Override
        protected FontTextView getSubtitle() {
            return mBinding.subtitle;
        }

        @NonNull
        @Override
        protected ImageView getBigCoverImage() {
            return mBinding.coverBig;
        }

        @NonNull
        @Override
        protected ImageView getNonPlayableIcon() {
            return mBinding.nonPlayableIcon;
        }

        @NonNull
        @Override
        protected FontTextView getNonPlayableTextView() {
            return mBinding.nonPlayableTextView;
        }

        @NonNull
        @Override
        protected View getNonPlayableOverlay() {
            return mBinding.nonPlayableOverview;
        }

        @NonNull
        @Override
        protected View getFadingStart() {
            return mBinding.fadingStart;
        }

        @NonNull
        @Override
        protected ImageView getGradientBottom() {
            return mBinding.gradientBottom;
        }

        @NonNull
        @Override
        protected FontTextView getTitleView() {
            return mBinding.title;
        }

        @NonNull
        @Override
        protected FontTextView getInfoRow() {
            return mBinding.infoRow;
        }

        @NonNull
        @Override
        protected TextSwitcher getDescriptionView() {
            return mBinding.description;
        }

        @NonNull
        @Override
        protected ImageView getShowMoreBtn() {
            return mBinding.showMoreBtn;
        }

        @NonNull
        @Override
        protected FontTextView getCastView() {
            return mBinding.cast;
        }

        @NonNull
        @Override
        protected FontTextView getDirectorsView() {
            return mBinding.directors;
        }

        @NonNull
        @Override
        protected ImageView getCoverImage() {
            return mBinding.cover;
        }
    }
}
