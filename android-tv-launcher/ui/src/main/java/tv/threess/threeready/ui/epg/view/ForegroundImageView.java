package tv.threess.threeready.ui.epg.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

import androidx.annotation.Nullable;

/**
 * @author Vlad
 * @since 2018.02.23
 */

public class ForegroundImageView extends ImageView {
    public ForegroundImageView(Context context) {
        super(context);
    }

    public ForegroundImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ForegroundImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ForegroundImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void setForeground(Drawable foreground) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.setForeground(foreground);
        }
    }
}
