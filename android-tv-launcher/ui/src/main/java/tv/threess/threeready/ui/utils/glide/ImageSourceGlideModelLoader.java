package tv.threess.threeready.ui.utils.glide;

import android.content.Context;
import android.webkit.URLUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ResourceLoader;
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader;

import java.io.InputStream;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.ImageRepository;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Custom glide model loader to load image for module items.
 *
 * @author Barabas Attila
 * @since 12/11/20
 */
public class ImageSourceGlideModelLoader extends BaseGlideUrlLoader<IImageSource> {
    private final ImageRepository mImageRepository = Components.get(ImageRepository.class);

    private final Context mContext;
    private final ResourceLoader<InputStream> mResourceLoader;

    public ImageSourceGlideModelLoader(Context context, ModelLoader<GlideUrl, InputStream> concreteLoader,
                                       ResourceLoader<InputStream> resourceLoader) {
        super(concreteLoader);
        mContext = context;
        mResourceLoader = resourceLoader;
    }

    @Nullable
    @Override
    public LoadData<InputStream> buildLoadData(@NonNull IImageSource imageSource, int width, int height, @NonNull Options options) {
        LoadData<InputStream> dataLoader = super.buildLoadData(imageSource, width, height, options);

        // Try to load as local resource.
        if (dataLoader.sourceKey instanceof GlideUrl) {
            GlideUrl glideUrl = (GlideUrl) dataLoader.sourceKey;
            if (!URLUtil.isValidUrl(glideUrl.toStringUrl())) {
                return mResourceLoader.buildLoadData(
                        UiUtils.getDrawableFromString(glideUrl.toStringUrl(), mContext), width, height, options);
            }
        }

        return dataLoader;
    }

    @Override
    protected String getUrl(IImageSource imageSource, int width, int height, Options options) {
        return mImageRepository.getImageSourceUrl(imageSource, width, height);
    }

    @Override
    public boolean handles(@NonNull IImageSource imageSource) {
        return true;
    }
}
