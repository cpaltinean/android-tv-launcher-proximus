package tv.threess.threeready.player.plugin;

import android.content.Context;
import android.text.TextUtils;

import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.MassStorage;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.model.VideoQuality;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.PlaybackItemType;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.log.model.VodItemType;
import tv.threess.threeready.api.middleware.BluetoothUtils;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.command.start.IptvLiveStartCommand;
import tv.threess.threeready.player.command.start.RecordingStartCommand;
import tv.threess.threeready.player.command.start.ReplayStartCommand;
import tv.threess.threeready.player.command.start.VodStartCommand;

/**
 * Class to get {@link IContentItem} data for broadcasts and vods as well.
 * <p>
 * Created by Bartalus Csaba - Zsolt
 * since 2021. December 2.
 */

public class ReportUtils {

    public static void reportPowerCycle(Context context) {
        long lastKnownHeartBeat = MassStorage.lastKnownHeartBeatEpoch.get(0L);

        //Checks if the remote was connected to the device when it stopped.
        boolean isRemoteConnected = MassStorage.lastKnownRemoteConnection.get(false);
        Log.event(new Event<>(UILogEvent.Stopped, lastKnownHeartBeat)
                .addDetail(UILogEvent.Detail.RemoteConnected, isRemoteConnected)
                .addDetail(UILogEvent.Detail.LastKnownHeartBeat, lastKnownHeartBeat)
        );

        //Checks at start if the remote connected to the device.
        isRemoteConnected = BluetoothUtils.isRemoteConnected(context);
        Log.event(new Event<>(UILogEvent.Started)
                .addDetail(UILogEvent.Detail.RemoteConnected, isRemoteConnected)
                .addDetail(UILogEvent.Detail.LastKnownHeartBeat, lastKnownHeartBeat)
        );
    }

    public static Event<UILogEvent, UILogEvent.Detail> createEvent(UILog.Page page, IContentItem contentItem) {
        return new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                .addDetail(UILogEvent.Detail.Page, page)
                .addDetail(UILogEvent.Detail.PageName, getPageName(contentItem))
                .addDetail(UILogEvent.Detail.PageId, getPageId(contentItem))
                .addDetail(UILogEvent.Detail.FocusName, getFocusName(contentItem))
                .addDetail(UILogEvent.Detail.FocusId, getFocusId(contentItem));
    }

    public static Event<UILogEvent, UILogEvent.Detail> createSelectionSelection(IContentItem contentItem) {
        return new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageSelection)
                .addDetail(UILogEvent.Detail.PageName, getPageName(contentItem))
                .addDetail(UILogEvent.Detail.PageId, getPageId(contentItem))
                .addDetail(UILogEvent.Detail.FocusName, getFocusName(contentItem))
                .addDetail(UILogEvent.Detail.FocusId, getFocusId(contentItem));
    }

    public static UILog.Page getPage(IContentItem item) {
        if (item instanceof IBroadcast) {
            return UILog.Page.WatchFromBroadcaster;
        }

        return UILog.Page.WatchFromVOD;
    }

    public static String getPageName(IContentItem item) {
        if (item instanceof IBroadcast) {
            return Components.get(NowOnTvCache.class).getCallLetter(((IBroadcast) item).getChannelId());
        }

        return  getGroupTitle(item);
    }

    public static String getPageId(IContentItem item) {
        return getContentItemId(item);
    }

    public static String getContentItemId(IContentItem item) {
        if (item instanceof IBroadcast) {
            return ((IBroadcast) item).getChannelId();
        }

        if (item instanceof IBaseVodItem) {
            return ((IBaseVodItem) item).getGroupId();
        }

        if (item instanceof IVodVariant) {
            return ((IVodVariant) item).getGroupId();
        }

        return null;
    }

    public static String getFocusName(IContentItem item) {
        if (item instanceof IBaseVodItem) {
            return ((IBaseVodItem) item).getVariantIds().size() > 1 ? getTitle(item) : null;
        }

        return getTitle(item);
    }

    public static String getFocusId(IContentItem item) {
        if (item instanceof IBaseVodItem) {
            return ((IBaseVodItem) item).getVariantIds().size() > 1 ? getItemId(item) : null;
        }

        return getItemId(item);
    }

    public static String getProgramId(IContentItem item) {
        if (item instanceof IBroadcast) {
            return ((IBroadcast) item).getProgramId();
        }
        return null;
    }

    public static String getItemId(IContentItem item) {
        return item != null ? item.getId() : null;
    }

    public static String getItemInternalId(IContentItem item) {
        if (item instanceof IVodVariant) {
            return String.valueOf(((IVodVariant) item).getInternalId());
        }

        if (item instanceof IVodItem) {
            return String.valueOf(((IVodItem) item).getInternalId());
        }
        return null;
    }

    public static String getItemTitle(IContentItem item) {
        if (item == null) {
            return null;
        }

        if (item instanceof IVodItem) {
            return ((IBaseVodItem) item).getItemTitle();
        }

        if (item instanceof IVodVariant) {
            return ((IVodVariant) item).getItemTitle();
        }

        return item.getTitle();
    }

    public static String getGroupTitle(IContentItem item) {
        if (item == null) {
            return null;
        }

        if (item instanceof IBaseVodItem) {
            return ((IBaseVodItem) item).getGroupTitle();
        }

        if (item instanceof IVodVariant) {
            return ((IVodVariant) item).getGroupTitle();
        }

        return null;
    }

    public static String getTitle(IContentItem item) {
        return item != null ? item.getTitle() : null;
    }

    public static String getItemGenre(IContentItem item) {
        if (item == null) {
            return null;
        }
        List<String> genres = item.getGenres();
        if (genres == null || genres.size() < 1) {
            return null;
        }
        return genres.get(0);
    }

    public static String getItemSubGenre(IContentItem item) {
        if (item == null) {
            return null;
        }
        List<String> subGenres = item.getSubGenres();
        if (subGenres == null || subGenres.size() < 1) {
            return null;
        }
        return subGenres.get(0);
    }

    public static String getItemSuperGenre(IContentItem item) {
        if (item instanceof IVodVariant) {
            return ((IVodVariant) item).getSuperGenre();
        }
        if (item instanceof IVodItem) {
            return ((IVodItem) item).getSuperGenre();
        }
        return null;
    }

    public static PlaybackItemType getItemType(IContentItem item) {
        if (item == null) {
            return null;
        }
        return TextUtils.isEmpty(item.getSeriesId()) ? PlaybackItemType.SINGLE : PlaybackItemType.SERIES;
    }

    public static VodItemType getVodType(IContentItem item) {
        if (item == null) {
            return null;
        }

        if (item instanceof IVodVariant) {
            IVodVariant vodVariant = (IVodVariant) item;
            if (vodVariant.isTVod()) {
                return vodVariant.isFree() ? VodItemType.FVOD : VodItemType.TVOD;
            } else if (vodVariant.isSVod()) {
                return VodItemType.SVOD;
            }
        }

        if (item instanceof IVodItem) {
            IVodItem vodItem = (IVodItem) item;
            if (vodItem.isTVod()) {
                return vodItem.isFree() ? VodItemType.FVOD : VodItemType.TVOD;
            } else if (vodItem.isSVod()) {
                return VodItemType.SVOD;
            }
        }
        return null;
    }

    public static String getItemResolution(StartCommand startedCommand, NowOnTvCache nowOnTvCache) {
        if (startedCommand instanceof VodStartCommand) {
            IVodVariant vodVariant = ((VodStartCommand) startedCommand).getContentItem();
            VideoQuality vodVariantQuality = vodVariant != null ? vodVariant.getHighestQuality() : null;
            if (vodVariantQuality != null) {
                return vodVariant.getHighestQuality().toString();
            }
        }

        if (startedCommand instanceof IptvLiveStartCommand) {
            TvChannel channel = ((IptvLiveStartCommand) startedCommand).getChannel();
            VideoQuality liveQuality = channel != null ? channel.isHDEnabled() ? VideoQuality.HD : VideoQuality.SD : null;
            if (liveQuality != null) {
                return liveQuality.toString();
            }
        }

        if (startedCommand instanceof ReplayStartCommand) {
            IBroadcast replay = ((ReplayStartCommand) startedCommand).getContentItem();
            VideoQuality replayQuality = replay != null ? replay.getVideoQuality(nowOnTvCache.getChannel(replay.getChannelId())) : null;
            if (replayQuality != null) {
                return replayQuality.toString();
            }
        }

        if (startedCommand instanceof RecordingStartCommand) {
            IBroadcast recording = ((RecordingStartCommand) startedCommand).getContentItem();
            VideoQuality recordingQuality = recording != null ? recording.getVideoQuality(nowOnTvCache.getChannel(recording.getChannelId())) : null;
            if (recordingQuality != null) {
                return recordingQuality.toString();
            }
        }

        return null;
    }

    public static Number getItemSeasonNr(IContentItem item) {
        return item != null ? item.getSeasonNumber() : null;
    }

    public static Number getItemEpisodeNr(IContentItem item) {
        return item != null ? item.getEpisodeNumber() : null;
    }

    public static String getItemEpisodeTitle(IContentItem item) {
        return item != null ? item.getEpisodeTitle() : null;
    }

    public static long getDuration(IContentItem item) {
        if (item instanceof IBroadcast && ((IBroadcast) item).isGenerated()) {
            return 0;
        }
        return item != null ? item.getDuration() : 0;
    }
}
