package tv.threess.threeready.ui.home.presenter.module.stripe.channel;

import android.content.Context;
import android.view.Gravity;
import android.view.ViewGroup;

import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.home.presenter.card.app.EditorialA1CardPresenter;
import tv.threess.threeready.ui.home.presenter.module.stripe.generic.StripeModulePresenter;
import tv.threess.threeready.ui.tv.presenter.channel.TvChannelC1CardPresenter;

/**
 * Presenter used to display channel type, A variant stripes.
 *
 * @author Barabas Attila
 * @since 2017.06.08
 */
public class TvChannelAStripePresenter extends StripeModulePresenter {

    Map<ModuleCardVariant, PresenterSelector> mVariantCardPresenterMap = new HashMap<>();

    public TvChannelAStripePresenter(Context context, DataSourceLoader dataSourceLoader) {
        super(context, dataSourceLoader);
        // TvChannel A1 Stripe
        mVariantCardPresenterMap.put(ModuleCardVariant.A1, new ClassPresenterSelector()
                .addClassPresenter(TvChannel.class, new TvChannelC1CardPresenter(context))
                .addClassPresenter(EditorialItem.class, new EditorialA1CardPresenter(context))
        );
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent, RecyclerView.RecycledViewPool pool) {
        ViewHolder holder = (ViewHolder) super.onCreateViewHolder(parent, pool);

        holder.mBinding.stripe.getStripeGrid().setWindowAlignmentOffsetPercent(
                mContext.getResources().getInteger(R.integer.tv_channel_stripe_window_alignment_percent));
        holder.mBinding.stripe.getStripeGrid().setGravity(Gravity.CENTER_VERTICAL);

        return holder;
    }

    @Override
    protected PresenterSelector getCardPresenterSelector(final ModuleConfig moduleConfig) {
        return mVariantCardPresenterMap.get(moduleConfig.getCardVariant());
    }

    /**
     * Gets the value for horizontal padding.
     */
    @Override
    protected int getHorizontalPadding() {
        return mContext.getResources().getDimensionPixelSize(R.dimen.tv_channel_a_stripe_left_margin);
    }

    /**
     * Gets the value for bottom padding.
     */
    @Override
    protected int getBottomPadding() {
        return 0;
    }

    /**
     * Gets the value for bottom margin.
     */
    @Override
    protected int getBottomMargin() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_channel_a_stripe_module_bottom_margin);
    }

    /**
     * Gets the value for item spacing.
     */
    @Override
    protected int getItemSpacing(ModuleConfig moduleConfig) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_channel_a_stripe_card_item_spacing);
    }

    /**
     * Gets the value for row height.
     */
    @Override
    protected int getRowHeight(ModuleData<?> moduleData) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_channel_card_height);
    }

    /**
     * Gets the value for the top margin.
     */
    @Override
    protected int getTopMargin() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_channel_a_stripe_top_margin);
    }
}
