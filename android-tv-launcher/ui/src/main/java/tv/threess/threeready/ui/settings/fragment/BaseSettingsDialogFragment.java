/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.settings.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.VerticalGridView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.reactivex.disposables.Disposable;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;

import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.SettingsLayoutBinding;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.dialog.BaseDialogFragment;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.utils.AnimatorUtils;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.settings.listener.EpgLayoutItemClickListener;
import tv.threess.threeready.ui.settings.listener.ItemClickListener;
import tv.threess.threeready.ui.settings.listener.LockContentItemClickListener;
import tv.threess.threeready.ui.settings.listener.ParentalItemClickListener;
import tv.threess.threeready.ui.settings.model.EpgLayoutItem;
import tv.threess.threeready.ui.settings.model.ISettingsItem;
import tv.threess.threeready.ui.settings.model.LockContentItem;
import tv.threess.threeready.ui.settings.model.MainSettingsItem;
import tv.threess.threeready.ui.settings.model.ParentalControlItem;
import tv.threess.threeready.ui.settings.model.ParentalControlLockItem;
import tv.threess.threeready.ui.settings.model.RadioButtonItem;
import tv.threess.threeready.ui.settings.model.SettingsItem;
import tv.threess.threeready.ui.settings.model.SupportItem;
import tv.threess.threeready.ui.settings.model.SwitchButtonItem;
import tv.threess.threeready.ui.settings.model.VodPurchaseHistoryItem;
import tv.threess.threeready.ui.settings.presenter.EpgLayoutItemPresenter;
import tv.threess.threeready.ui.settings.presenter.LockContentItemPresenter;
import tv.threess.threeready.ui.settings.presenter.MainItemPresenter;
import tv.threess.threeready.ui.settings.presenter.ParentalControlItemPresenter;
import tv.threess.threeready.ui.settings.presenter.ParentalControlLockItemPresenter;
import tv.threess.threeready.ui.settings.presenter.RadioButtonPresenter;
import tv.threess.threeready.ui.settings.presenter.SettingsSubMenuPresenter;
import tv.threess.threeready.ui.settings.presenter.SettingsTextItemPresenter;
import tv.threess.threeready.ui.settings.presenter.SupportItemPresenter;
import tv.threess.threeready.ui.settings.presenter.SwitchButtonPresenter;
import tv.threess.threeready.ui.settings.presenter.VodPurchaseSettingsItemPresenter;

/**
 * This class will display options radio buttons, and will handle checked change events on it.
 *
 * @author Paul
 * @since 2017.06.27
 */

public abstract class BaseSettingsDialogFragment<TItem> extends BaseDialogFragment implements ItemClickListener, ParentalItemClickListener,
    LockContentItemClickListener, EpgLayoutItemClickListener {
    public static final String TAG = Log.tag(BaseSettingsDialogFragment.class);

    private static final long ANIMATION_SLIDE_TIME = 300;

    protected final Translator mTranslator = Components.get(Translator.class);

    private Animator mSlideAnimator;

    List<Disposable> mDisposableList = new ArrayList<>();

    protected ArrayObjectAdapter<TItem> mAdapter;

    private SettingsLayoutBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflateLayout();
    }

    @NonNull
    protected View inflateLayout(){
        if (mBinding == null) {
            mBinding = SettingsLayoutBinding.inflate(LayoutInflater.from(getContext()));
        }

        return mBinding.getRoot();
    }

    protected View getRootView() {
        return mBinding != null ? mBinding.getRoot() : null;
    }

    @NonNull
    protected View getRightFrame() {
        return mBinding.rightFrame;
    }

    protected VerticalGridView getVerticalGridView() {
        return mBinding.grid;
    }

    protected TextView getTitleView() {
        return mBinding.title;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        startSlideInAnimation();
        setTitle();
        initView();
    }

    /**
     * Sets the title for the dialog fragment
     */
    protected abstract void setTitle();

    /**
     * Initializes views on the fragment
     */
    protected abstract void initView();

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        AnimatorUtils.cancelAnimators(mSlideAnimator);
    }

    protected void startSlideInAnimation() {
        // Cancel the animation, if it is running
        AnimatorUtils.cancelAnimators(mSlideAnimator);

        mSlideAnimator = ObjectAnimator.ofFloat(getRightFrame(),
                View.TRANSLATION_X, getResources().getDimensionPixelOffset(R.dimen.settings_width), 0);
        mSlideAnimator.setDuration(ANIMATION_SLIDE_TIME);

        mSlideAnimator.start();
    }

    protected void startSlideOutAnimation() {
        AnimatorUtils.cancelAnimators(mSlideAnimator);

        mSlideAnimator = ObjectAnimator.ofFloat(getRightFrame(),
                View.TRANSLATION_X, 0, getResources().getDimensionPixelOffset(R.dimen.settings_width));
        mSlideAnimator.setDuration(ANIMATION_SLIDE_TIME);
        mSlideAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                BaseSettingsDialogFragment.super.dismissAllowingStateLoss();
            }
        });

        mSlideAnimator.start();
    }

    protected void setItems(Collection<TItem> items) {
        VerticalGridView gridView = getVerticalGridView();
        if (gridView == null) {
            throw new IllegalStateException("You must call setItems after onCreateView!");
        }

        InterfacePresenterSelector presenterSelector = new InterfacePresenterSelector()
                .addClassPresenter(SwitchButtonItem.class, new SwitchButtonPresenter(getContext(), this))
                .addClassPresenter(RadioButtonItem.class, new RadioButtonPresenter(getContext(), this))
                .addClassPresenter(MainSettingsItem.class, new MainItemPresenter(getContext()))
                .addClassPresenter(SupportItem.class, new SupportItemPresenter(getContext()))
                .addClassPresenter(String.class, new SettingsSubMenuPresenter(getContext()))
                .addClassPresenter(SettingsItem.class, new SettingsTextItemPresenter(getContext(), this))
                .addClassPresenter(ParentalControlItem.class, new ParentalControlItemPresenter(getContext()))
                .addClassPresenter(ParentalControlLockItem.class, new ParentalControlLockItemPresenter(getContext()))
                .addClassPresenter(LockContentItem.class, new LockContentItemPresenter(getContext(), this))
                .addClassPresenter(EpgLayoutItem.class, new EpgLayoutItemPresenter(getContext(), this))
                .addClassPresenter(VodPurchaseHistoryItem.class, new VodPurchaseSettingsItemPresenter(getContext()));

        mAdapter = new ArrayObjectAdapter<>(items);
        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(mAdapter, presenterSelector);
        gridView.setAdapter(itemBridgeAdapter);
    }

    /**
     * @param item the item that was now checked
     * @return false if we should set the settings value with it's text, true if it's logic is
     * implemented in the inherited class.
     */
    protected boolean onOptionsSelected(RadioButtonItem item) {
        return false;
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onItemClick(ISettingsItem clickedItem) {
        for (Object item : mAdapter.getItems()) {
            if (item instanceof RadioButtonItem) {
                RadioButtonItem radioButtonItem = (RadioButtonItem) item;
                if (!item.equals(clickedItem)) {
                    if (radioButtonItem.getPreferenceGroup() != null
                            && radioButtonItem.getPreferenceGroup().equals(clickedItem.getPreferenceGroup())) {
                        radioButtonItem.setChecked(false);
                    }
                } else {
                    radioButtonItem.setChecked(!(item instanceof SwitchButtonItem) || !radioButtonItem.isChecked());
                    onOptionsSelected(radioButtonItem);
                }
            }
        }

        VerticalGridView gridView = getVerticalGridView();
        if (gridView.getAdapter() != null) {
            gridView.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void dismiss() {
        startSlideOutAnimation();
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        super.onKeyUp(event);
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            Log.event(new Event<>(UILogEvent.PageSelection)
                    .addDetail(UILogEvent.Detail.OpenedBy, UILog.OpenedBy.Back));
            dismiss();
            return true;
        }
        return false;
    }
}
