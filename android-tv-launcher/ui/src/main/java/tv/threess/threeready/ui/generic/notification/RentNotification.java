package tv.threess.threeready.ui.generic.notification;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

import static tv.threess.threeready.ui.utils.UiUtils.runOnUiThread;

/**
 * Rent notification shown while rent request
 * This notification blocks all key events except Home btn ( on home btn click UI is unblocked )
 *
 * @author Daniel Gliga
 * @since 2017.08.10
 */

public class RentNotification extends Notification {
    private static final String TAG = Log.tag(RentNotification.class);

    private static final String EXTRA_VOD = "EXTRA_VOD";
    private static final String EXTRA_VARIANT = "EXTRA_VARIANT";

    private boolean mBlockUI = true;

    private final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);
    private final NotificationManager mNotificationManager = Components.get(NotificationManager.class);

    private IVodItem mVod;
    private IVodVariant mVodVariant;

    public static RentNotification newInstance(IVodItem vod, IVodVariant variant) {
        RentNotification rentNotification = new RentNotification();

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_VOD, vod);
        args.putSerializable(EXTRA_VARIANT, variant);
        rentNotification.setArguments(args);
        return rentNotification;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mVod = getSerializableArgument(EXTRA_VOD);
        mVodVariant = getSerializableArgument(EXTRA_VARIANT);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String title = mTranslator.get(TranslationKey.NOTIFICATION_PURCHASE_SUCCESS);
        String subtext = mTranslator.get(TranslationKey.NOTIFICATION_PURCHASE_SUCCESS_SUBTEXT);

        mBinding.title.setText(title);

        if (!TextUtils.isEmpty(subtext)) {
            mBinding.subtext.setText(subtext);
            mBinding.subtext.setVisibility(View.VISIBLE);
        } else {
            mBinding.subtext.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        mPlaybackDetailsManager.registerCallback(mPlaybackCallback);
    }

    public void onStop() {
        super.onStop();

        mPlaybackDetailsManager.unregisterCallback(mPlaybackCallback);
    }

    @Override
    public boolean onKeyDown(KeyEvent event) {
        // block UI until user press home button
        if (event.getKeyCode() == KeyEvent.KEYCODE_HOME) {
            mBlockUI = false;
        }
        return mBlockUI;
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        return mBlockUI;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        return mBlockUI;
    }

    @Override
    public Timeout getTimeOut() {
        return Timeout.LENGTH_RENT_NOTIFICATION;
    }

    @Override
    public boolean onBackPressed() {
        return mBlockUI;
    }

    private final IPlaybackCallback mPlaybackCallback = new IPlaybackCallback() {
        @Override
        public void onCommandSuccess(PlaybackCommand cmd, PlaybackDetails details) {
            if (details.getState() == PlaybackState.Started &&
                    mPlaybackDetailsManager.isVodPlaying(mVodVariant)) {
                runOnUiThread(mNotificationManager::hideNotification);
            }
        }

        @Override
        public void onCommandFailure(PlaybackCommand command, PlaybackDetails details, PlaybackError error) {
            if (!TextUtils.isEmpty(mVod.getId()) && mPlaybackDetailsManager.getVodPlayerData() != null
                    && mVod.getId().equals(mPlaybackDetailsManager.getVodPlayerData().getId())) {
                runOnUiThread(mNotificationManager::hideNotification);
                Log.d(TAG, "Purchase commit failed." + details);
            }
        }
    };
}
