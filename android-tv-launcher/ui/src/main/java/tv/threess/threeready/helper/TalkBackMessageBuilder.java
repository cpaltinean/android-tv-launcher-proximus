package tv.threess.threeready.helper;

import androidx.annotation.NonNull;

/**
 * Builder used for build the talk-back service messages.
 *
 * @author David Bondor
 * @since 23.09.2022
 */
public class TalkBackMessageBuilder {

    String message;

    public TalkBackMessageBuilder(@NonNull String message) {
        this.message = message;
    }

    /**
     * Remove the given key if the replacement is null, replace it if not.
     */
    public TalkBackMessageBuilder replaceOrRemove(String key, CharSequence replacement) {
        message = message.replace(key, replacement == null ? "" : replacement);
        return this;
    }

    @NonNull
    @Override
    public String toString() {
        return message;
    }
}
