package tv.threess.threeready.ui.tv.presenter.vod;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.vod.model.IBaseVodSeries;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.SeriesA1LandscapeCardBinding;

/**
 * Presenter to display a large landscape card for a VOD series.
 *
 * @author Barabas Attila
 * @since 4/6/22
 */
public class VodSeriesA1BigCardPresenter
        extends VodSeriesA1CardPresenter<VodSeriesA1BigCardPresenter.ViewHolder> {

    public VodSeriesA1BigCardPresenter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        SeriesA1LandscapeCardBinding binding = SeriesA1LandscapeCardBinding.inflate(
                    LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public int getCardWidth(@Nullable ModuleData<?> moduleData, IBaseVodSeries series) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.vod_series_a1_landscape_card_width_focused);
    }

    @Override
    public int getCardHeight(@Nullable ModuleData<?> moduleData, IBaseVodSeries series) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.vod_series_a1_landscape_card_height_focused);
    }

    @Override
    protected int getCoverHeight(IBaseVodSeries series) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.vod_series_a1_landscape_cover_width);
    }

    @Override
    protected int getCoverWidth(IBaseVodSeries series) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.vod_series_a1_landscape_cover_height);
    }

    static class ViewHolder extends VodSeriesA1CardPresenter.ViewHolder {

        ColorDrawable mPlaceHolderDrawable = new ColorDrawable();
        private final SeriesA1LandscapeCardBinding mBinding;

        @Override
        public TextView getTitleView() {
            return mBinding.title;
        }

        @Override
        public ImageView getCoverView() {
            return mBinding.cover;
        }

        @Override
        public ColorDrawable getPlaceHolderDrawable() {
            return mPlaceHolderDrawable;
        }

        public ViewHolder(SeriesA1LandscapeCardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
