package tv.threess.threeready.ui.generic.presenter;

/**
 * Model used for Loading Button in @{@LoadingButtonPresenter}.
 *
 * @author Daniela Toma
 * @since 2020.08.05
 */
public class LoadingModel {
    private final String mText;

    public LoadingModel(String text) {
        mText = text;
    }

    public String getText() {
        return mText;
    }
}