package tv.threess.threeready.ui.update;

import android.content.Context;
import android.content.Intent;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.middleware.CasUpdateReceiver;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.notification.NotificationManager;
import tv.threess.threeready.ui.utils.SystemUtils;

import java.util.concurrent.TimeUnit;

/**
 * Components which displays the cas update dialog.
 *
 * @author Karetka Mezei Zoltan
 * @since 2022.10.26
 */
public class CasUpdateDialogManager {

    private static final String REBOOT_REASON_CAS_UPDATE = "userrequested,vcas";

    private final CasUpdateReceiver mCasUpdateReceiver = Components.get(CasUpdateReceiver.class);
    private final NotificationManager mNotificationManager = Components.get(NotificationManager.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    public void onCreate() {
        mCasUpdateReceiver.addStateChangedListener(mCasUpdateListener);
     }

    public void onDestroy() {
        mCasUpdateReceiver.removeStateChangedListener(mCasUpdateListener);
    }

    private final CasUpdateReceiver.Listener mCasUpdateListener = new CasUpdateReceiver.Listener() {
        private void reboot() {
            SystemUtils.reboot(mNavigator.getActivity(), REBOOT_REASON_CAS_UPDATE);
        }

        @Override
        public void onStateChanged() {
            mNotificationManager.showForcedRebootNotification();
            long timeout = mAppConfig.getForcedRebootDelay(TimeUnit.MILLISECONDS);
            Schedulers.computation().scheduleDirect(this::reboot, timeout, TimeUnit.MILLISECONDS);
        }
    };
}
