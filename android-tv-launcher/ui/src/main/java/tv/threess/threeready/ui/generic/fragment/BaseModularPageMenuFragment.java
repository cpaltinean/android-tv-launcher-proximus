/*
 *
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.generic.fragment;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.api.home.model.generic.ContentConfig;
import tv.threess.threeready.api.home.model.menu.MenuItem;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.ui.home.view.ModularPageView;
import tv.threess.threeready.ui.menu.callback.MenuClickListener;
import tv.threess.threeready.ui.menu.view.BaseMenuView;

/**
 * Abstract base class for modular page fragments with menu.
 * <p>
 * Created by Szilard on 7/28/2017.
 */

public abstract class BaseModularPageMenuFragment extends BaseModularPageFragment {
    public static final String TAG = Log.tag(BaseModularPageMenuFragment.class);

    protected final ContentConfig mContentConfig = Components.get(ContentConfig.class);

    protected boolean mShouldFocusOnContent = false;

    protected MenuClickListener mItemClickListener =
        new MenuClickListener() {

            @Override
            public void onNotificationIconClick() {
                mNavigator.openAndroidNotifications();
            }

            @Override
            public void onSearchIconClick() {
                mNavigator.openSearch();
            }

            @Override
            public void onSettingsIconClick() {
                mNavigator.openSettings();
            }

            @Override
            public void onMenuItemClick(MenuItem menuItem) {
                // Display page of the selected meu item
                displayPageAtMenuItemClick(menuItem);
            }
        };

    /**
     * use when you want to focus on the content, it will set the flag to true and call
     * {@link #displayPage(MenuItem)}
     *
     * @param menuItem clicked menu item data
     */
    protected void displayPageAtMenuItemClick(@NonNull MenuItem menuItem) {
        mShouldFocusOnContent = true;
        displayPage(menuItem);
    }

    protected void displayPage(@NonNull MenuItem menuItem) {
        PageConfig config = mContentConfig.getPageConfig(menuItem.getId());
        if (config != null) {
            displayPage(config);
        }
    }

    @Override
    public void onPageLoaded(PageConfig pageConfig, ModuleData<?> data) {
        reportNavigation();
    }

    protected Event<UILogEvent, UILogEvent.Detail> generatePageNavigationEvent(BaseMenuView menuView) {
        if (mModularPageView == null
                || mModularPageView.getPageConfig() == null) {
            // Not loaded yet.
            return null;
        }

        if (menuView.hasFocus()) {
            UILog.Page keyPointName = menuView.getFocusSubPointName();
            if (keyPointName != null) {
                return generateMenuIconNavigationEvent(keyPointName.name());
            }

            return generateMenuNavigationEvent(mModularPageView);
        }

        return createPageEvent(mModularPageView);
    }

    private Event<UILogEvent, UILogEvent.Detail> generateMenuNavigationEvent(ModularPageView modularPageView) {
        PageConfig page = modularPageView.getPageConfig();
        Event<UILogEvent, UILogEvent.Detail> event = new Event<>(UILogEvent.PageOpen);
        event.addDetail(UILogEvent.Detail.Page, page.getPageName())
                .addDetail(UILogEvent.Detail.PageName, page.getReportName())
                .addDetail(UILogEvent.Detail.PageId, page.getReportReference());
        return event;
    }

    private Event<UILogEvent, UILogEvent.Detail> generateMenuIconNavigationEvent(String keyPointName) {
        Event<UILogEvent, UILogEvent.Detail> event = new Event<>(UILogEvent.PageOpen);
        event.addDetail(UILogEvent.Detail.Page, UILog.Page.Home)
                .addDetail(UILogEvent.Detail.PageName, keyPointName);
        return event;
    }

    private Event<UILogEvent, UILogEvent.Detail> createPageEvent(ModularPageView modularPageView) {
        PageConfig page = modularPageView.getPageConfig();
        PageConfig parentPage = mContentConfig.getPageConfig(page.getParent());

        if (parentPage != null && parentPage.hasSubPages()) {
            return createEvent(parentPage, modularPageView);
        }

        return createEvent(page, modularPageView);
    }


    private Event<UILogEvent, UILogEvent.Detail> createEvent(PageConfig page, ModularPageView modularPageView) {
        Event<UILogEvent, UILogEvent.Detail> event = new Event<>(UILogEvent.PageOpen);
        event.addDetail(UILogEvent.Detail.Page, page.getPageName())
                .addDetail(UILogEvent.Detail.PageName, page.getReportName())
                .addDetail(UILogEvent.Detail.PageId, page.getReportReference());

        RecyclerView.Adapter<?> adapter = modularPageView.getAdapter();

        if (adapter == null) {
            return null;
        }

        if (adapter.getItemCount() == 0) {
            return null;
        }

        ModuleData<?> data = modularPageView.getSelectedModuleData();
        if (data == null) {
            return event;
        }

        List<DataSourceSelector> selectorList = data.getSelectors();

        switch (page.getPageName()) {
            case StoreSubcategoryBrowsing:
                return event;
            case StoreFullContentFilterPage:
                if (selectorList.size() == 1) {
                    SelectorOption selectedOption = selectorList.get(0).getSelectedOption();
                    if (selectedOption != null) {
                        event.addDetail(UILogEvent.Detail.FocusName, selectedOption.getReportReference());
                    }
                }
                return event;
            case ContentWallFullContentPage:
                if (selectorList.size() == 1) {
                    SelectorOption selectedOption = selectorList.get(0).getSelectedOption();
                    if (selectedOption != null) {
                        event.addDetail(UILogEvent.Detail.FocusId, selectedOption.getReportReference());
                    }
                }
                return event;
            case ContentWallFullContentFilterPage:
                for (DataSourceSelector selector : selectorList) {
                    if (selector.getSelectedOption() == null) {
                        continue;
                    }
                    if (selector.getDisplayType() == DataSourceSelector.DisplayType.Menu) {
                        event.addDetail(UILogEvent.Detail.FocusName, selector.getSelectedOption().getReportName());
                    } else if (selector.getDisplayType() == DataSourceSelector.DisplayType.DropDown) {
                        event.addDetail(UILogEvent.Detail.FocusId, selector.getSelectedOption().getReportName());
                    }
                }
                return event;
            default:
                event.addDetail(UILogEvent.Detail.FocusName, data.getModuleConfig().getReportName())
                        .addDetail(UILogEvent.Detail.FocusId, data.getModuleConfig().getReportReference());
                return event;
        }
    }
}
