package tv.threess.threeready.ui.details;

/**
 * Enum indicates from where the detail page was opened.
 *
 * @author Barabas Attila
 * @since 2020.12.04
 */
public enum DetailOpenedFrom {
    Default,
    Player,
    Recording,
    Favorite,
    ContinueWatching
}
