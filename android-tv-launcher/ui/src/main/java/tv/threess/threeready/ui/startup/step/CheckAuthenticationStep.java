package tv.threess.threeready.ui.startup.step;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.generic.navigation.Navigator;

/**
 * In case there is internet connection and backed is available but we could not authenticate -> we show an alert dialog.
 *
 * @author Bondor David
 * @since 2019.08.13
 */
public class CheckAuthenticationStep implements Step {
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);

    @Override
    public void execute(StepCallback callback) {
        if (!Settings.authenticated.get(false)
                && mInternetChecker.isInternetAvailable() && mInternetChecker.isBackendAvailable()) {
            mNavigator.showNoBackendAlertDialog();
        } else {
            callback.onFinished();
        }
    }
}