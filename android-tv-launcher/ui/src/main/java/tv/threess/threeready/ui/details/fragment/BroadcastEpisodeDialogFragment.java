/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.details.fragment;

import android.os.Bundle;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.PlayOption;
import tv.threess.threeready.api.generic.model.WatchlistType;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.pvr.model.SingleRecordingStatus;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.details.DetailOpenedFrom;
import tv.threess.threeready.ui.generic.adapter.DetailPageButtonOrder;
import tv.threess.threeready.ui.generic.hint.HintManager;
import tv.threess.threeready.ui.generic.model.BroadcastPlayOption;
import tv.threess.threeready.ui.generic.presenter.ActionModel;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.pvr.RecordingActionHelper;
import tv.threess.threeready.ui.utils.ImageTransform;

/**
 * Episode detail dialog for a tv broadcasts and recordings.
 *
 * @author Barabas Attila
 * @since 2018.12.12
 */
public class BroadcastEpisodeDialogFragment extends EpisodeDialogFragment<IBroadcast> {
    private static final String TAG = Log.tag(BroadcastEpisodeDialogFragment.class);

    protected static final String EXTRA_RECORDING_STATUS = "EXTRA_RECORDING_STATUS";
    protected static final String EXTRA_BOOKMARK = "EXTRA_BOOKMARK";
    protected static final String EXTRA_OPENED_FROM = "EXTRA_OPENED_FROM";

    protected final RecordingActionHelper mRecordingActionHelper = Components.get(RecordingActionHelper.class);

    protected IBookmark mBookmark;
    protected DetailOpenedFrom mOpenedFrom;

    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);

    private Disposable mInWatchlistDisposable;
    private Disposable mAddWatchlistDisposable;
    private Disposable mRemoveWatchlistDisposable;

    public static BroadcastEpisodeDialogFragment newInstance(IBroadcast broadcast, RecordingStatus recordingStatus,
                                                             DetailOpenedFrom openedFrom, IBookmark bookmark) {
        BroadcastEpisodeDialogFragment fragment = new BroadcastEpisodeDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_CONTENT_ITEM, broadcast);
        bundle.putSerializable(EXTRA_RECORDING_STATUS, recordingStatus);
        bundle.putSerializable(EXTRA_OPENED_FROM, openedFrom);
        bundle.putSerializable(EXTRA_BOOKMARK, bookmark);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBookmark = getSerializableArgument(EXTRA_BOOKMARK);
        mOpenedFrom = getSerializableArgument(EXTRA_OPENED_FROM);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        RxUtils.disposeSilently(mInWatchlistDisposable, mAddWatchlistDisposable, mRemoveWatchlistDisposable);
    }

    @Override
    protected void updateButtons(IBroadcast broadcast, Boolean inWatchlist) {
        RecordingStatus recordingStatus = getSerializableArgument(EXTRA_RECORDING_STATUS);

        SingleRecordingStatus singleRecordingStatus = recordingStatus == null ?
                null : recordingStatus.getSingleRecordingStatus();

        List<ActionModel> actions = new ArrayList<>();

        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        boolean isChannelSubscribed = channel != null && channel.isUserTvSubscribedChannel();

        // Display playback options.
        List<PlayOption> playOptions = generatePlayOptions(channel, broadcast, recordingStatus);

        if (playOptions.size() > 0) {
            // Start directly the first play option.
            actions.add(new ActionModel(
                    mTranslator.get(TranslationKey.SCREEN_DETAIL_PLAY_BUTTON), DetailPageButtonOrder.Watch, () -> {
                if (!isChannelSubscribed) {
                    mRecordingActionHelper.showChannelNotEntitledDialog();
                    return;
                }

                if (playOptions.size() == 1) {
                    showPlayer(playOptions.get(0));
                    mRecordingActionHelper.showStartReplayWarningNotification(recordingStatus, channel);
                } else {
                    mNavigator.showPlayOptionsDialog(playOptions, broadcast);
                }
                dismiss();
            }
            ));
        }

        if (recordingStatus != null) {
            // Display delete for completed recording.
            if (singleRecordingStatus == SingleRecordingStatus.COMPLETED) {
                actions.add(new ActionModel(
                        0, 0,
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_DELETE_BUTTON),
                        DetailPageButtonOrder.Delete, () -> {
                    mRecordingActionHelper.deleteRecording(recordingStatus.getRecording(), true, false);
                    dismiss();
                }));

                // Add reschedule possibility for live.
                if (broadcast.isLive() && isChannelSubscribed && mPvrRepository.isPVREnabled(broadcast)) {
                    actions.add(new ActionModel(
                            0, 0,
                            mTranslator.get(TranslationKey.SCREEN_DETAIL_RECORD_BUTTON),
                            DetailPageButtonOrder.Record, () -> {
                        mRecordingActionHelper.scheduleSingleRecording(broadcast, recordingStatus, true, false);
                        dismiss();
                    }));
                }

                // Display cancel for scheduled and ongoing recording.
            } else if (singleRecordingStatus == SingleRecordingStatus.SCHEDULED
                    || singleRecordingStatus == SingleRecordingStatus.RECORDING) {
                actions.add(new ActionModel(
                        0, 0,
                        mTranslator.get(singleRecordingStatus == SingleRecordingStatus.RECORDING
                                ? TranslationKey.SCREEN_DETAIL_CANCEL_ONGOING_BUTTON
                                : TranslationKey.SCREEN_DETAIL_CANCEL_BUTTON),
                        DetailPageButtonOrder.Cancel, () -> {
                    mRecordingActionHelper.cancelSingleRecording(broadcast, recordingStatus, true, false);
                    dismiss();
                }));

                // Display record for live and future broadcasts.
            } else if (mPvrRepository.isPVREnabled(broadcast) && isChannelSubscribed
                    && (broadcast.isLive() || broadcast.isFuture())) {
                actions.add(new ActionModel(
                        0, 0,
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_RECORD_BUTTON),
                        DetailPageButtonOrder.Record, () -> {
                    mRecordingActionHelper.scheduleSingleRecording(broadcast, recordingStatus, true, false);
                    dismiss();
                }));
            }
        }

        if (inWatchlist != null) {
            if (inWatchlist) {
                actions.add(new ActionModel(
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_REMOVEFROMWATCHLIST_BUTTON), DetailPageButtonOrder.RemoveFromWatchList,
                        () -> {
                            RxUtils.disposeSilently(mRemoveWatchlistDisposable);
                            mRemoveWatchlistDisposable = mAccountRepository.removeFromWatchlist(broadcast.getId(), WatchlistType.Broadcast)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(() -> Log.d(TAG, "Successfully removed the item from watchlist"),
                                            throwable -> Log.e(TAG, "Could not remove item from watchlist.", throwable));
                        }
                ));
            } else {
                if (broadcast.shouldShowAddToWatchListButton(channel, singleRecordingStatus)) {
                    actions.add(new ActionModel(
                            mTranslator.get(TranslationKey.SCREEN_DETAIL_ADDTOWATCHLIST_BUTTON), DetailPageButtonOrder.AddToWatchList,
                            () -> {
                                RxUtils.disposeSilently(mAddWatchlistDisposable);
                                mAddWatchlistDisposable = mAccountRepository.addToWatchlist(WatchlistType.Broadcast, broadcast.getId())
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(() -> {
                                            Log.d(TAG, "Successfully added item to watchlist");
                                            Components.get(HintManager.class).showAddToWatchlistHints();
                                        }, throwable -> Log.e(TAG, "Could not add item to watchlist", throwable));
                            }
                    ));
                }
            }
        }

        DetailPageButtonOrder.sort(actions);
        updateButtonDescriptions(actions, broadcast);
        mAdapter.replaceAll(actions);
    }

    /**
     * Update the buttons with watchlist information
     */
    @Override
    protected void updateWatchlist(IBroadcast broadcast) {
        RxUtils.disposeSilently(mInWatchlistDisposable);
        mInWatchlistDisposable = mAccountRepository.isInWatchlist(broadcast.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((inWatchlist) -> {
                    Log.d(TAG, "Watchlist state retrieved, inWatchlist = [" + inWatchlist + "]");
                    updateButtons(broadcast, inWatchlist);
                }, throwable -> {
                    Log.e(TAG, "Could not get watchlist state");
                    updateButtons(broadcast, null);
                });
    }

    @Override
    protected void updateProvideLogo(IBroadcast broadcast) {
        super.updateProvideLogo(broadcast);

        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        Glide.with(mBinding.getRoot().getContext())
                .load(channel)
                .apply(ImageTransform.REQUEST_OPTIONS)
                .into(mBinding.providerLogo);
    }

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        IBroadcast broadcast = getContentItem();
        if (broadcast == null) {
            // Page not loaded yet.
            return null;
        }

        return ReportUtils.createEvent(UILog.Page.RecordActionSubMenuDialog, broadcast);
    }

    protected List<PlayOption> generatePlayOptions(TvChannel channel, IBroadcast broadcast, RecordingStatus recordingStatus) {
        return BroadcastPlayOption.generatePlayOptions(channel, broadcast,
                mBookmark, recordingStatus,
                mOpenedFrom == DetailOpenedFrom.Recording,
                mOpenedFrom == DetailOpenedFrom.ContinueWatching);
    }


    protected void showPlayer(PlayOption playOption) {
        mNavigator.showPlayer(playOption, false);
    }
}
