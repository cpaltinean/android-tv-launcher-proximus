package tv.threess.threeready.ui.utils;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;
import java.util.List;

/**
 * Glide target which decided to display the loaded image as cover or background image based on it's size,
 * and changes between the loaded images with a cross fade animation.
 */
public class TransitionTarget extends CustomTarget<Drawable> {
    private static final int TRANSITION_DURATION = 500;
    private Drawable mLoadedDrawable;
    private final OnPrepareTargetViewListener mOnPrepareTargetViewListener;

    public TransitionTarget(int width, int height, OnPrepareTargetViewListener onPrepareTargetViewListener) {
        super(width, height);
        mOnPrepareTargetViewListener = onPrepareTargetViewListener;
    }

    @Override
    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
        mLoadedDrawable = resource;
        onImageLoaded(resource);
    }

    @Override
    public void onLoadCleared(@Nullable Drawable placeholder) {
        onClearImage(mLoadedDrawable);
        mLoadedDrawable = null;
    }

    @Override
    public void onLoadFailed(@Nullable Drawable errorDrawable) {
        super.onLoadFailed(errorDrawable);

        if (errorDrawable != null) {
            onImageLoaded(errorDrawable);
        }
    }

    /**
     * Called when the load is canceled and the resource needs to be cleared.
     *
     * @param drawable The previously loaded image view if exists.
     */
    private void onClearImage(@Nullable Drawable drawable) {
        if (drawable == null) {
            // Nothing to do. Not loaded yet.
            return;
        }

        ImageView targetView = prepareTargetView(drawable);

        if (targetView.getDrawable() == drawable) {
            //
            targetView.setImageDrawable(null);
        }

        if (targetView.getDrawable() instanceof TransitionDrawable) {
            TransitionDrawable transitionDrawable = (TransitionDrawable) targetView.getDrawable();

            List<Drawable> remainingLayers = new ArrayList<>();
            for (int i = 0; i < transitionDrawable.getNumberOfLayers(); ++i) {
                Drawable layer = transitionDrawable.getDrawable(i);
                if (layer != drawable) {
                    remainingLayers.add(layer);
                }
            }

            if (remainingLayers.size() == transitionDrawable.getNumberOfLayers()) {
                // Already removed.
                return;
            }

            if (remainingLayers.isEmpty()) {
                // Nothing to show. Clear image view.
                targetView.setImageDrawable(null);
            } else if (remainingLayers.size() == 1) {
                // Only single drawable. Display directly.
                targetView.setImageDrawable(remainingLayers.get(0));
            } else {
                // Multiple drawable create layer list.
                targetView.setImageDrawable(
                        new TransitionDrawable(remainingLayers.toArray(new Drawable[0])));
            }
        }
    }

    /**
     * Select the image view for the given image based on it's size.
     *
     * @param drawable The drawable to select image view for.
     * @return The selected small or background image view.
     */
    private ImageView prepareTargetView(Drawable drawable) {
        return mOnPrepareTargetViewListener.onPrepareTargetView(drawable);
    }

    /**
     * Called when a new drawable is loaded and needs to be displayed.
     *
     * @param drawable The drawable which needs to be displayed.
     */
    private void onImageLoaded(Drawable drawable) {
        ImageView targetView = prepareTargetView(drawable);

        // Animate drawable change.
        if (targetView.getDrawable() == null) {
            targetView.setImageDrawable(drawable);
        } else {
            Drawable prevDrawable = targetView.getDrawable();
            if (prevDrawable instanceof TransitionDrawable) {
                TransitionDrawable prevTransitionDrawable = (TransitionDrawable) prevDrawable;
                prevDrawable = prevTransitionDrawable.getDrawable(prevTransitionDrawable.getNumberOfLayers() - 1);
            }

            Drawable[] drawables = new Drawable[]{prevDrawable.getCurrent(), drawable};
            TransitionDrawable transitionDrawable = new TransitionDrawable(drawables);

            targetView.setImageDrawable(transitionDrawable);
            transitionDrawable.startTransition(TRANSITION_DURATION);
        }
    }

    public interface OnPrepareTargetViewListener {
        ImageView onPrepareTargetView(Drawable drawable);
    }
}