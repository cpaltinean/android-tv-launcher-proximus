package tv.threess.threeready.ui.home.presenter.module.collection.generic;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleType;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.CollectionPresenterBinding;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.adapter.ModularItemBridgeAdapter;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.home.presenter.module.BaseCollectionModulePresenter;
import tv.threess.threeready.ui.home.view.ModularPageView;


/**
 * Presenter class for a collection module.
 * Responsible to load the data from the given data source and display it in a multi column vertical grid.
 * The loaded data will be displayed with cards. The type of the card is based on the module type and variant.
 *
 * @author Barabas Attila
 * @since 2017.06.12
 */

public abstract class CollectionModulePresenter extends BaseCollectionModulePresenter<CollectionModulePresenter.ViewHolder> {

    protected final Translator mTranslator = Components.get(Translator.class);
    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    protected final Navigator mNavigator = Components.get(Navigator.class);

    protected final Map<ModuleCardVariant, PresenterSelector> mVariantCardPresenterMap = new HashMap<>();

    private final ModularPageView.PageTitleCallback mPageTitle;

    /**
     * returns the number of columns that should be displayed
     *
     * @return the corresponding value
     */
    protected abstract int getColumnNumber();

    public CollectionModulePresenter(Context context, ModularPageView.PageTitleCallback pageTitle, DataSourceLoader dataSourceLoader) {
        super(context, dataSourceLoader);
        mPageTitle = pageTitle;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, RecyclerView.RecycledViewPool pool) {
        CollectionPresenterBinding binding = CollectionPresenterBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);

        final ViewHolder holder = new ViewHolder(binding, parent);
        holder.mBinding.collection.getTitleView().setTextColor(mLayoutConfig.getFontColor());
        holder.mBinding.collection.getEmptyTextView().setTextColor(mLayoutConfig.getFontColor());

        CollectionSelectorButtonPresenter.SelectorChangeListener selectorChangeListener = () -> {
            // Reload the collection after a selector change.
            mDataSourceLoader.reloadModule(holder.mModuleData.getModuleConfig(),
                    holder.mModuleData.getSelectors());
            holder.mButtonSelectorAdapter.notifyDataSourceChanged();
            holder.mMenuSelectorAdapter.notifyDataSourceChanged();
            holder.mBinding.collection.getCollectionGrid().setSelectedPosition(0);
            holder.mNeedFocusOnGrid = true;
        };

        // Selector buttons.
        holder.mButtonSelectorAdapter.setHasStableIds(true);
        holder.mBinding.collection.getSelectorButtonGrid().getLayoutManager().setAutoMeasureEnabled(true);
        holder.mBinding.collection.getSelectorButtonGrid().setAdapter(new ItemBridgeAdapter(holder.mButtonSelectorAdapter,
                new CollectionSelectorButtonPresenter(mContext, mPageTitle, selectorChangeListener)));

        // Selector menu.
        holder.mBinding.collection.getSelectorMenuGrid().setFadingRightEdge(true);
        holder.mBinding.collection.getSelectorMenuGrid().setFadingRightEdgeLength(
                mContext.getResources().getDimensionPixelOffset(R.dimen.menu_fading_edge_length));
        holder.mBinding.collection.getSelectorMenuGrid().setFadingLeftEdge(true);
        holder.mBinding.collection.getSelectorMenuGrid().setFadingLeftEdgeLength(
                mContext.getResources().getDimensionPixelOffset(R.dimen.menu_fading_edge_length));
        holder.mBinding.collection.getSelectorMenuGrid().requestFocus();
        holder.mMenuSelectorAdapter.setHasStableIds(true);
        holder.mBinding.collection.getSelectorMenuGrid().setHasFixedSize(true);
        holder.mSelectorMenuPresenter = new CollectionSelectorMenuPresenter(mContext, mPageTitle, selectorChangeListener);

        if (holder.mMenuSelectorAdapter.getItems().size() == 0) {
            holder.mIsFirstTimeOpened = true;
            holder.mNeedFocusOnGrid = true;
        }
        holder.mBinding.collection.getSelectorMenuGrid().setAdapter(
                new ItemBridgeAdapter(holder.mMenuSelectorAdapter, holder.mSelectorMenuPresenter));

        holder.mBinding.collection.getCollectionGrid().setRecycledViewPool(pool);
        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, ModuleData<?> data) {
        holder.mModuleData = data;
        ModuleConfig moduleConfig = data.getModuleConfig();

        holder.mBinding.collection.getEmptyTextView().setText(mTranslator.get(getEmptyPageMessage(data)));

        if (moduleConfig.getType() == ModuleType.COLLECTION && !TextUtils.isEmpty(moduleConfig.getTitle())
                && mDataSourceLoader.getModuleConfigs().indexOf(moduleConfig) > 0) { // Don't show title for the first item.
            holder.mBinding.collection.getTitleView().setText(mTranslator.get(moduleConfig.getTitle()));
            holder.mBinding.collection.getTitleView().setVisibility(View.VISIBLE);
        } else {
            holder.mBinding.collection.getTitleView().setVisibility(View.GONE);
        }

        updateSelectorOptions(holder, data);

        holder.mBinding.collection.getCollectionGrid().setOnChildSelectedListener(position -> {
            if (isNeedToRequestNextPage(holder, moduleConfig, position)) {
                requestNextPage(moduleConfig);
            }
        });

        holder.mBinding.collection.getCollectionGrid().setOnKeyInterceptListener(event -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                return onKeyDown(event, holder);
            }
            return false;
        });


        updateAdapter(holder, data);
        restoreFocus(holder, data);

        holder.mBinding.collection.setNumColumns(getColumnNumber());
        holder.mBinding.collection.getCollectionGrid().setPadding(getHorizontalPadding(),
                getTopPadding(holder), getHorizontalPadding(), getBottomPadding());
        holder.mBinding.collection.getCollectionGrid().setItemSpacing(getItemSpacing());

        int position = data.getItemCount() > 0 ?
                holder.mBinding.collection.getCollectionGrid().getSelectedPosition() :
                0;
        if (isNeedToRequestNextPage(holder, data.getModuleConfig(), position)) {
            requestNextPage(moduleConfig);
        }
    }

    protected String getEmptyPageMessage(ModuleData<?> moduleData) {
        for (DataSourceSelector selector : ArrayUtils.notNull(moduleData.getSelectors())) {
            SelectorOption selectedOption = selector.getSelectedOption();
            if (selectedOption != null) {
                return TextUtils.isEmpty(selectedOption.getEmptyPageMessage()) ?
                        mLayoutConfig.getEmptyPageMessage() : selectedOption.getEmptyPageMessage();
            }
        }

        return mLayoutConfig.getEmptyPageMessage();
    }

    /**
     * @param position adapter position (it can be -1 if there is no focused card in the grid view).
     * @return check if user arrived to threshold and needs to get the next page
     */
    private boolean isNeedToRequestNextPage(ViewHolder holder, ModuleConfig moduleConfig, int position) {
        RecyclerView.Adapter<?> adapter = holder.mBinding.collection.getCollectionGrid().getAdapter();
        if (position == -1 || adapter == null) {
            return false;
        }

        return position > (adapter.getItemCount() - getThreshold(moduleConfig));
    }

    private void requestNextPage(ModuleConfig moduleConfig) {
        // Resubscribe to data source
        Pair<Integer, Integer> limit = getLimit(moduleConfig);
        mDataSourceLoader.requestPage(moduleConfig, limit);
    }

    /**
     * Restore the focus on the collection grid after the data loads.
     */
    private void restoreFocus(ViewHolder holder, ModuleData<?> data) {
        boolean hasMoreItems = data.hasMoreItems();
        boolean hasContent = !data.getItems().isEmpty();

        // Display empty page message when needed.
        holder.mBinding.collection.getEmptyTextView().setVisibility(
                hasMoreItems || hasContent ? View.GONE : View.VISIBLE);

        if (hasContent && holder.mBinding.collection.hasFocus()
                && holder.mNeedFocusOnGrid) {
            holder.mBinding.collection.getCollectionGrid().requestFocus();
            holder.mNeedFocusOnGrid = false;
        }
    }

    /**
     * Update the sort option button for the collection.
     *
     * @param holder The view holder which holds the collection view
     * @param data   The collection module data source and adapter
     */
    private void updateSelectorOptions(ViewHolder holder, ModuleData<?> data) {
        // Display selector options.
        DataSourceSelector menuSelector = null;
        List<DataSourceSelector> buttonSelectors = new ArrayList<>();

        for (DataSourceSelector selector : data.getSelectors()) {
            if (!selector.hasMultipleOption()) {
                continue;
            }

            if (menuSelector == null && selector.getDisplayType() == DataSourceSelector.DisplayType.Menu) {
                menuSelector = selector;
                continue;
            }

            buttonSelectors.add(selector);
        }

        // Selector menu.
        if (menuSelector != null) {
            holder.mSelectorMenuPresenter.setSelector(menuSelector);
            holder.mBinding.collection.setSelector(menuSelector);
            holder.mMenuSelectorAdapter.replaceAll(menuSelector.getOptions());
            holder.mBinding.collection.getSelectorMenuGrid().setVisibility(View.VISIBLE);
            if (holder.mIsFirstTimeOpened) {
                holder.mBinding.collection.getSelectorMenuGrid().setSelectedPosition(getSelectedMenuItemPosition(menuSelector));
                holder.mIsFirstTimeOpened = false;
            }
        } else {
            holder.mBinding.collection.getSelectorMenuGrid().setVisibility(View.GONE);
            holder.mMenuSelectorAdapter.clear();
        }

        // Selector buttons
        if (!buttonSelectors.isEmpty()) {
            holder.mButtonSelectorAdapter.replaceAll(buttonSelectors);
            holder.mBinding.collection.getSelectorButtonGrid().setVisibility(View.VISIBLE);

            ViewGroup.MarginLayoutParams buttonLayoutParams = (ViewGroup.MarginLayoutParams)
                    holder.mBinding.collection.getSelectorButtonGrid().getLayoutParams();
            buttonLayoutParams.topMargin = mContext.getResources().getDimensionPixelOffset(
                    holder.mBinding.collection.getSelectorMenuGrid().getVisibility() == View.VISIBLE
                            ? R.dimen.collection_buttons_margin_top_large : R.dimen.collection_buttons_margin_top_small);
        } else {
            holder.mBinding.collection.getSelectorButtonGrid().setVisibility(View.GONE);
            holder.mButtonSelectorAdapter.clear();
        }

        updateCollectionHeight(holder);
    }

    /**
     * Gets the position of the selected menu item.
     */
    private int getSelectedMenuItemPosition(DataSourceSelector menuSelector) {
        for (int i = 0; i < menuSelector.getOptions().size(); i++) {
            if (menuSelector.getOptions().get(i).isDefault()) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);

        holder.mBinding.collection.getCollectionGrid().setAdapter(null);
        holder.mIsFirstTimeOpened = false;
        holder.mNeedFocusOnGrid = false;
        holder.mSelectorMenuPresenter.setSelector(null);
    }

    @Override
    protected int getNextPageStart(ModuleConfig moduleConfig) {
        ModuleData<?> data = mDataSourceLoader.getContentData(moduleConfig);
        return data == null ? 0 : data.getOffset() + data.getPageSize();
    }

    /**
     * Calculate and set the maximum height possible for the collection.
     *
     * @param holder The view holder which holds the collection.
     */
    private void updateCollectionHeight(ViewHolder holder) {
        if (holder.mParent instanceof ModularPageView) {
            int headerSpacing = ((ModularPageView) holder.mParent).getHeaderSpacing();
            int menuOffset = holder.mBinding.collection.getSelectorMenuGrid().getVisibility() == View.VISIBLE ?
                    mContext.getResources().getDimensionPixelOffset(R.dimen.collection_meu_vertical_offset) : 0;
            holder.mBinding.collection.getLayoutParams().height = holder.mParent.getHeight() - headerSpacing + menuOffset;
        }
    }

    /**
     * Create a new or update the existing adapter for the module data.
     *
     * @param holder     The view holder of the collection.
     * @param moduleData The module data which needs to be displayed
     */
    private void updateAdapter(ViewHolder holder, ModuleData<?> moduleData) {
        if (holder.mBinding.collection.getCollectionGrid().getAdapter() != null) {
            ModularItemBridgeAdapter itemBridgeAdapter =
                    (ModularItemBridgeAdapter) holder.mBinding.collection.getCollectionGrid().getAdapter();
            ModuleConfig adapterModule = itemBridgeAdapter.getModuleData().getModuleConfig();
            if (Objects.equals(adapterModule, moduleData.getModuleConfig())) {
                itemBridgeAdapter.getAdapter().replaceAll(moduleData.getItems());
                return;
            }
        }

        ArrayObjectAdapter<Object> objectAdapter = new ArrayObjectAdapter<>(moduleData.getItems());
        objectAdapter.setHasStableIds(true);

        PresenterSelector cardPresenterSelector = getCardPresenterSelector(moduleData.getModuleConfig());
        ModularItemBridgeAdapter itemBridgeAdapter = new ModularItemBridgeAdapter(
                moduleData, objectAdapter, cardPresenterSelector);
        holder.mBinding.collection.getCollectionGrid().setAdapter(itemBridgeAdapter);
    }

    protected boolean onKeyDown(KeyEvent event, CollectionModulePresenter.ViewHolder holder) {

        int focusedViewPosition = holder.mBinding.collection.getCollectionGrid().getChildAdapterPosition(holder.mBinding.collection.getCollectionGrid().getFocusedChild());
        int focusedViewColumnNumber = focusedViewPosition % getColumnNumber();

        int lastRowIndex = 0;
        if (holder.mBinding.collection.getCollectionGrid().getAdapter() != null) {
            lastRowIndex = (holder.mBinding.collection.getCollectionGrid().getAdapter().getItemCount() - 1) / getColumnNumber();
        }

        int focusedViewRowNumber = focusedViewPosition / getColumnNumber();

        // deactivate right RC button, when the focus is on the rightmost item
        if (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT &&
                (focusedViewColumnNumber == getColumnNumber() - 1 || focusedViewPosition == holder.mBinding.collection.getCollectionGrid().getAdapter().getItemCount() - 1)) {
            return true;
        }

        // deactivate left RC button, when the focus is on the leftmost item
        if (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT && focusedViewColumnNumber == 0) {
            return true;
        }

        if (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN && focusedViewRowNumber + 1 == lastRowIndex
                && focusedViewColumnNumber > (holder.mBinding.collection.getCollectionGrid().getAdapter().getItemCount() - 1) % getColumnNumber()) {
            holder.mBinding.collection.getCollectionGrid().getFocusedChild().setActivated(false);
            holder.mBinding.collection.getCollectionGrid().setSelectedPosition(holder.mBinding.collection.getCollectionGrid().getAdapter().getItemCount() - 1);
            holder.mBinding.collection.getCollectionGrid().requestFocus();
            return true;
        }

        return false;

    }

    protected PresenterSelector getCardPresenterSelector(ModuleConfig moduleConfig) {
        return mVariantCardPresenterMap.get(moduleConfig.getCardVariant());
    }

    /**
     * Gets the value for horizontal padding.
     */
    protected abstract int getHorizontalPadding();

    /**
     * Gets the value for top padding.
     */
    protected abstract int getTopPadding(ViewHolder holder);

    /**
     * Gets the value for bottom padding.
     */
    protected abstract int getBottomPadding();

    /**
     * Gets the value for item spacing.
     */
    protected int getItemSpacing() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.collection_item_spacing);
    }

    protected static class ViewHolder extends Presenter.ViewHolder {

        private ModuleData<?> mModuleData;
        private CollectionSelectorMenuPresenter mSelectorMenuPresenter;

        private final View mParent;
        private final ArrayObjectAdapter<DataSourceSelector> mButtonSelectorAdapter = new ArrayObjectAdapter<>();
        private final ArrayObjectAdapter<SelectorOption> mMenuSelectorAdapter = new ArrayObjectAdapter<>();

        private boolean mIsFirstTimeOpened = false;
        private boolean mNeedFocusOnGrid = false;

        public final CollectionPresenterBinding mBinding;

        public ViewHolder(CollectionPresenterBinding binding, View parent) {
            super(binding.getRoot());
            mParent = parent;
            mBinding = binding;
        }
    }

}
