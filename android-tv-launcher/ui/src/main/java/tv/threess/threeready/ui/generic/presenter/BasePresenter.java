/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.presenter;

import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;

/**
 * A Presenter is used to generate {@link View}s and bind Objects to them on
 * demand. It is closely related to the concept of an {@link ItemBridgeAdapter RecyclerView.Adapter}, but is
 * not position-based.  The leanback framework implements the adapter concept using
 * {@link ArrayObjectAdapter} which refers to a Presenter (or {@link PresenterSelector}) instance.
 *
 * Created by Barabas Attila on 4/12/2017.
 */

public abstract class BasePresenter<THolder extends Presenter.ViewHolder, TItem> extends Presenter {
    private static final int MAX_RECYCLER_VIEW_COUNT = 5;

    protected Context mContext;

    public BasePresenter(Context context) {
        // Use application context to can register and cleanup
        // global resources which are bind to the application lifecycle, not to the activity.
        mContext = context == null ? null : context.getApplicationContext();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return null;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         RecyclerView.RecycledViewPool recycledViewPool) {
        return onCreateViewHolder(parent);
    }

    public int getItemViewType() {
        return hashCode();
    }

    public int getMaxRecyclerViewCount() {
        return MAX_RECYCLER_VIEW_COUNT;
    }

    @Override
    public final void onBindViewHolder(ViewHolder viewHolder, Object item) {
        onBindHolder((THolder) viewHolder, (TItem) item);
    }

    @Override
    public final void onUnbindViewHolder(ViewHolder viewHolder) {
        onUnbindHolder((THolder) viewHolder);
    }

    @Override
    public final void onViewAttachedToWindow(ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        onAttachedToWindow((THolder) holder);
    }

    @Override
    public final void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        onDetachedFromWindow((THolder) holder);
    }


    /**
     * Binds data to view holder.
     */
    public abstract void onBindHolder(THolder holder, TItem item);

    /**
     * Binds data to view holder. Used to update the already bound data with the new pay load.
     */
    public void onBindHolder(THolder holder, TItem item, List<?> payLoads){}

    /**
     * Called when a view created by this presenter has been attached to a window.
     *
     * <p>This can be used as a reasonable signal that the view is about to be seen
     * by the user. If the adapter previously freed any resources in
     * {@link #onViewDetachedFromWindow(ViewHolder)}
     * those resources should be restored here.</p>
     *
     * @param holder Holder of the view being attached
     */
    public void onAttachedToWindow(THolder holder) { }

    /**
     * Called when the view is not focused.
     */
    public void onDefaultState(THolder holder, TItem item) {
    }

    /**
     * Called when the view gains focus.
     */
    public void onFocusedState(THolder holder, TItem item) {
    }

    /**
     * Called when the tile is clicked.
     */
    public void onClicked(THolder holder, TItem item) {}

    /**
     * Called when a key pressed on the focused cards.
     */
    public boolean onKeyEvent(THolder holder, TItem item, KeyEvent event){
        return false;
    }

    /**
     * Called when the tile is long clicked clicked.
     */
    public boolean onLongClicked(THolder holder, TItem item) {
        return false;
    }

    /**
     * Called when a view created by this presenter has been detached from its window.
     *
     * <p>Becoming detached from the window is not necessarily a permanent condition;
     * the consumer of an presenter's views may choose to cache views offscreen while they
     * are not visible, attaching and detaching them as appropriate.</p>
     *
     * Any view property animations should be cancelled here or the view may fail
     * to be recycled.
     *
     * @param holder Holder of the view being detached
     */
    public void onDetachedFromWindow(THolder holder) { }


    /**
     * Unbinds a {@link View} from an item. Any expensive references may be
     * released here, and any fields that are not bound for every item should be
     * cleared here.
     */
    public void onUnbindHolder(THolder holder) { }

    /**
     * @param item The item for which the stable should be calculated.
     * @return The stable ID for the item at the given position.
     * Used to identify if the item has to be refreshed after an adapter change.
     */
    public long getStableId(TItem item) {
        return ObjectAdapter.NO_ID;
    }

    public enum RefreshPayload {
        ITEM_REFRESH
    }

}