package tv.threess.threeready.ui.menu.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

import tv.threess.threeready.ui.R;

/**
 * Custom icon view to display the current android notification existence.
 *
 * @author Barabas Attila
 * @since 2017.12.09
 */

public class NotificationIconView extends MenuIconView {

    private final Paint mNotificationBackgroundPaint;

    public NotificationIconView(Context context) {
        this(context, null);
    }

    public NotificationIconView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NotificationIconView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mNotificationBackgroundPaint = new Paint();
        mNotificationBackgroundPaint.setStyle(Paint.Style.FILL);
        mNotificationBackgroundPaint.setAntiAlias(true);
    }

    @Override
    public void draw(Canvas canvas) {
        if (isFocused() || isActivated() || isPressed()
                || (getParent() instanceof View && ((View) getParent()).hasFocus())) {
            int colorStart = isPressed() ? mLayoutConfig.getButtonPressedColorStart() : mLayoutConfig.getButtonFocusedColorStart();
            int colorEnd = isPressed() ? mLayoutConfig.getButtonPressedColorEnd() : mLayoutConfig.getButtonFocusedColorEnd();

            mNotificationBackgroundPaint.setShader(new LinearGradient(0, 0, getWidth(), getHeight(),
                    colorStart, colorEnd, Shader.TileMode.MIRROR));

            float right = getResources().getDimensionPixelOffset(R.dimen.menu_notification_background_right);
            float bottom = getResources().getDimensionPixelOffset(R.dimen.menu_notification_background_bottom);
            canvas.drawOval(0, 0, right, bottom, mNotificationBackgroundPaint);
        }

        super.draw(canvas);
    }
}
