/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.dialog;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.BingeWatchingInfo;
import tv.threess.threeready.api.generic.model.IBaseSeries;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.model.generic.ButtonStyle;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.ui.databinding.BingeWatchingDialogBinding;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Dialog for displaying the next available episode for a series.
 * Binge Watching functionality.
 *
 * @author Zsolt Bokor_
 * @since 2020.04.14
 */

public class BingeWatchingDialog extends BaseDialogFragment {
    private static final String TAG = Log.tag(BingeWatchingDialog.class);

    private static final long UPDATE_PERIOD_MS = TimeUnit.SECONDS.toMillis(1);
    private static final String EXTRA_SERIES = "EXTRA_SERIES";
    private static final String EXTRA_CONTENT_ITEM = "EXTRA_CONTENT_ITEM";
    private static final String STARTS_IN_SECONDS_PLACEHOLDER = "%starts_in_seconds%";

    private BingeWatchingDialogBinding mBinding;

    private final Translator mTranslator = Components.get(Translator.class);
    private final PlaybackDetailsManager mPlaybackManager = Components.get(PlaybackDetailsManager.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    private final Handler mHandler = new Handler(Looper.getMainLooper());

    /**
     * Runnable used to display the countdown timer.
     */
    private final Runnable mCountDownRunnable = new Runnable() {
        @Override
        public void run() {
            PlaybackDetails details = mPlaybackManager.getExclusiveDetails();
            long timer = TimeUnit.MILLISECONDS.toSeconds(details.getBufferEnd() - details.getPosition());
            if (!details.getState().active || details.getState() == PlaybackState.Paused) {
                dismiss();
                Log.d(TAG, "Playback is stopped or paused, dismissing the dialog!");
            } else if (timer > 0) {
                updateDescription(timer);
                mHandler.postDelayed(this, UPDATE_PERIOD_MS);
                Log.d(TAG, "Updating the description, seconds left: " + timer);
            } else {
                Log.d(TAG, "Countdown ended. Starting the next episode");
                startNextEpisode();
            }
        }
    };

    private IBaseSeries mSeries;
    private IContentItem mNextEpisode;

    public static BingeWatchingDialog newInstance(BingeWatchingInfo<?, ?> bingeWatchingInfo) {
        BingeWatchingDialog dialog = new BingeWatchingDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_SERIES, bingeWatchingInfo.getSeries());
        bundle.putSerializable(EXTRA_CONTENT_ITEM, bingeWatchingInfo.getNextEpisode());
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSeries = getSerializableArgument(EXTRA_SERIES);
        mNextEpisode = getSerializableArgument(EXTRA_CONTENT_ITEM);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = BingeWatchingDialogBinding.inflate(inflater, container, false);
        initViews();
        initBranding();

        mBinding.nextEpisodeBtn.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_BUTTON_DIALOG))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_DIALOG_BODY, mBinding.description.getText())
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BUTTON_NAME, mTranslator.get(TranslationKey.BUTTON_NEXT_EPISODE))
                        .toString()
        );

        return mBinding.getRoot();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (!isDismissed()) {
            Log.d(TAG, "Dismiss the dialog.");
            dismissAllowingStateLoss();
        }
        mHandler.removeCallbacks(mCountDownRunnable);
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_0:
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_2:
            case KeyEvent.KEYCODE_3:
            case KeyEvent.KEYCODE_4:
            case KeyEvent.KEYCODE_5:
            case KeyEvent.KEYCODE_6:
            case KeyEvent.KEYCODE_7:
            case KeyEvent.KEYCODE_8:
            case KeyEvent.KEYCODE_9:
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                dismiss();
                break;
        }
        return super.onKeyUp(event);
    }

    private void initViews() {
        if (mNextEpisode == null) {
            Log.d(TAG, "Could not get the next episode from bundle.");
            dismiss();
            return;
        }

        mBinding.title.setText(mNextEpisode.getSeriesTitleWithSeasonEpisode(mTranslator, " "));
        mBinding.nextEpisodeBtn.setText(mTranslator.get(TranslationKey.BUTTON_NEXT_EPISODE));
        mBinding.nextEpisodeBtn.setOnClickListener(v -> startNextEpisode());

        // Start the countdown
        mHandler.post(mCountDownRunnable);
    }

    /**
     * Set the proximus specific styles for views.
     */
    private void initBranding() {
        mBinding.nextEpisodeBtn.setBackground(UiUtils.createButtonBackground(getActivity(),
                mLayoutConfig, ButtonStyle.PROXIMUS));
        mBinding.nextEpisodeBtn.setTextColor(UiUtils.createButtonBrandingColorStateList(mLayoutConfig));
    }

    /**
     * Update the description with the remaining seconds.
     */
    private void updateDescription(long seconds) {
        mBinding.description.setText(mTranslator.get(TranslationKey.SCREEN_PLAYER_BINGE_WATCHING_STARTS_IN)
                .replace(STARTS_IN_SECONDS_PLACEHOLDER, String.valueOf(seconds)));
    }

    /**
     * Starts the next episode if the next episode button was clicked or the countdown ended.
     */
    public boolean startNextEpisode() {
        if (isDismissed()) {
            Log.d(TAG, "Binge watching dialog is dismissed.");
            return false;
        }

        switch (mPlaybackManager.getPlayerType()) {
            case Recording:
                if (!(mNextEpisode instanceof IRecording) || !(mSeries instanceof IRecordingSeries)) {
                    Log.d(TAG, "Next episode is not a recording.");
                    dismiss();
                    return false;
                }
                IRecording recording = (IRecording) mNextEpisode;
                IRecordingSeries recordingSeries = (IRecordingSeries) mSeries;
                mNavigator.showRecordingPlayer(StartAction.ContinuousPlay, recording, recordingSeries, null, true);
                break;
            case Vod:
                if (!(mNextEpisode instanceof IVodItem) || !(mSeries instanceof IVodSeries)) {
                    Log.d(TAG, "Invalid vod episode or series.");
                    dismiss();
                    return false;
                }

                IVodItem vod = (IVodItem) mNextEpisode;
                IVodSeries vodSeries = (IVodSeries) mSeries;
                IVodVariant playingVodVariant = mPlaybackManager.getVodVariantPlayerData();
                IVodPrice price = mPlaybackManager.getVodPricePlayerData();
                // Set the default vod variant for the next episode
                IVodVariant variantToStart = vod.getMainVariant();

                for (IVodVariant variant : vod.getSubscribedVariants()) {
                    if (playingVodVariant != null && playingVodVariant.formatEquals(variant)) {
                        variantToStart = variant;
                        break;
                    }
                }
                mNavigator.showVodPlayer(StartAction.ContinuousPlay, variantToStart, vod, vodSeries, price);
                break;
        }
        dismiss();
        return true;
    }
}