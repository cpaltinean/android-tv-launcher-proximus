package tv.threess.threeready.ui.pc.view;

import android.content.Context;
import android.graphics.Color;
import android.media.tv.TvView;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.player.IPlaybackDetailsCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.command.ForceStopCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.plugin.PlaybackPlugin;
import tv.threess.threeready.player.result.Result;
import tv.threess.threeready.ui.databinding.PlaybackLockBinding;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.fragment.BasePlayerFragment;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.player.view.IZappingInfoStateChangeListener;
import tv.threess.threeready.ui.generic.player.view.ZappingInfoView;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.api.pc.ParentalControlManager;

/**
 * Custom view to hide playback surface and mute the volume when
 * the currently running content is restricted by the parental control.
 *
 * @author Barabas Attila
 * @since 2019.06.12
 */
public class PlaybackLockView extends LinearLayout {
    private final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);
    private final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private ZappingInfoView mZappingInfoView;

    private final PlaybackLockBinding binding;

    protected TvView mTvView;

    public PlaybackLockView(Context context) {
        this(context, null);
    }

    public PlaybackLockView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PlaybackLockView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public PlaybackLockView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        setBackgroundColor(Color.BLACK);
        setGravity(Gravity.CENTER);
        setOrientation(VERTICAL);

        binding = PlaybackLockBinding.inflate(LayoutInflater.from(context), this);
    }


    public void setTvView(TvView tvView) {
        this.mTvView = tvView;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        mZappingInfoView = Components.get(ZappingInfoView.class);
        binding.hint.setText(mTranslator.get(TranslationKey.CONTENT_LOCKED_PLAYER_HINT_TEXT));

        mPlaybackDetailsManager.registerPlugin(mPlaybackPlugin);
        mPlaybackDetailsManager.registerCallback(mPlaybackCallback);
        mParentalControlManager.addListener(mParentalControlListener);
        mNavigator.addContentChangeListener(mContentChangeListener);
        mZappingInfoView.addStateChangeListener(mZappingInfoStateChangeListener);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        mPlaybackDetailsManager.unregisterPlugin(mPlaybackPlugin);
        mPlaybackDetailsManager.unregisterCallback(mPlaybackCallback);
        mParentalControlManager.removeListener(mParentalControlListener);
        mNavigator.removeContentChangeListener(mContentChangeListener);
        mZappingInfoView.removeStateChangeListener(mZappingInfoStateChangeListener);
    }

    /**
     * Called when the parental control or the currently running content changed.
     * Should be used to validate the restriction and block/unblock the playback here.
     */
    public void updateParentalControlRestriction() {
        IContentItem contentItem = mPlaybackDetailsManager.getPlayingItem();
        if (mParentalControlManager.isRestricted(contentItem, true)) {
            lockPlayback();
        } else {
            unlockPlayback(contentItem);
        }
    }

    /**
     * Hide the playback surface.
     * Show the lock icon and the hint.
     */
    private void lockPlayback() {
        mParentalControlManager.setCurrentlyPlaying(null);
        setVisibility(View.VISIBLE);
        binding.icon.setVisibility(View.VISIBLE);
        binding.hint.setVisibility(mNavigator.isContentOverlayDisplayed() ? INVISIBLE : VISIBLE);
        mPlaybackDetailsManager.mute();

        // TODO: Resize the surface to 3px x 3px. Workaround for PROX-7051, remove after BCM fix.
        if (mTvView != null) {
            ViewGroup.LayoutParams layoutParams = mTvView.getLayoutParams();
            layoutParams.width = 3;
            layoutParams.height = 3;
            mTvView.setLayoutParams(layoutParams);
            mTvView.forceLayout();
        }
    }

    /**
     * Show the playback surface.
     * @param contentItem The currently playing item
     *                    for which the parental control is unblocked.
     */
    private void unlockPlayback(IContentItem contentItem) {
        if (mNavigator.isStartupFinished() || mPlaybackDetailsManager.inPlaybackDomain(PlaybackDomain.Gdpr)) {
            mParentalControlManager.setCurrentlyPlaying(contentItem);
            setVisibility(View.INVISIBLE);
            mPlaybackDetailsManager.unMute();

            // TODO: Resize the surface to parent. Workaround for PROX-7051, remove after BCM fix.
            if (mTvView != null) {
                ViewGroup.LayoutParams layoutParams = mTvView.getLayoutParams();
                layoutParams.width = LayoutParams.MATCH_PARENT;
                layoutParams.height = LayoutParams.MATCH_PARENT;
                mTvView.setLayoutParams(layoutParams);
                mTvView.forceLayout();
            }
        }
    }

    /**
     * @return True if the parental control lock is currently visible.
     */
    public boolean isLocked() {
        return getVisibility() == View.VISIBLE;
    }

    // Listener to detect the parental control changes.
    private final ParentalControlManager.IParentalControlListener mParentalControlListener =
            parentalRating -> updateParentalControlRestriction();

    // Listener to detect the content frame visibility changes.
    private final Navigator.IContentChangeListener mContentChangeListener = new Navigator.IContentChangeListener() {

        @Override
        public void onBackStackChanged(BaseFragment fragment) {
            if (fragment instanceof BasePlayerFragment
                    && mNavigator.isContentOverlayDisplayed()) {
                binding.hint.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onContentShown(BaseFragment fragment) {
            if (fragment instanceof BasePlayerFragment) {
                binding.hint.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onContentHide(BaseFragment fragment) {
            binding.hint.setVisibility(View.VISIBLE);
        }
    };


    PlaybackPlugin mPlaybackPlugin = new PlaybackPlugin() {
        @Override
        public void onResult(PlaybackCommand command, Result result) {
            post(() -> {
                if (command instanceof ForceStopCommand) {
                    mParentalControlManager.setCurrentlyPlaying(null);
                    return;
                }

                if (command instanceof StartCommand && command.getDomain() != PlaybackDomain.TrickPlay) {
                    IContentItem contentItem = mPlaybackDetailsManager.getPlayingItem();
                    if (!mParentalControlManager.isRestricted(contentItem, true)) {
                        unlockPlayback(contentItem);
                    }
                }
            });
        }

        @Override
        public void onBeforeExecute(PlaybackCommand command, PlaybackEntireState state, PlaybackDetailsBuilder details) {
            post(() -> {
                if (command instanceof StartCommand && command.getDomain() != PlaybackDomain.TrickPlay) {
                    IContentItem contentItem = mPlaybackDetailsManager.getPlayingItem();
                    if (mParentalControlManager.isRestricted(contentItem, true)) {
                        lockPlayback();
                    } else if (isLocked()) {
                        binding.hint.setVisibility(View.GONE);
                        binding.icon.setVisibility(View.GONE);
                    }
                }
            });
        }
    };

    private final IPlaybackDetailsCallback mPlaybackCallback = new IPlaybackDetailsCallback() {
        @Override
        public void onPlayerDataUpdate(IBroadcast event) {
            if (!mPlaybackDetailsManager.isCommandStarting(StartCommand.class)) {
                mTvView.post(() -> updateParentalControlRestriction());
            }
        }
    };

    // Hide the lock and text during channel switch animations.
    private final IZappingInfoStateChangeListener mZappingInfoStateChangeListener =
            new IZappingInfoStateChangeListener() {
        @Override
        public void onZappingInfoShown() {
            binding.hint.setAlpha(0f);
            binding.icon.setAlpha(0f);
        }

        @Override
        public void onZappingInfoHidden() {
            binding.hint.setAlpha(1f);
            binding.icon.setAlpha(1f);
        }
    };


}
