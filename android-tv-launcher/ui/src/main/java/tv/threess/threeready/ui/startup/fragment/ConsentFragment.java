package tv.threess.threeready.ui.startup.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.account.model.account.IConsent;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.ConsentFragmentBinding;
import tv.threess.threeready.ui.generic.dialog.AlertDialog;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Fragment for consent screens.
 *
 * @author Daniela Toma
 * @since 2020.09.01
 */
public class ConsentFragment extends StartFragment {
    private static final String TAG = Log.tag(ConsentFragment.class);

    private static final String EXTRA_CONSENTS = "consents";

    public static ConsentFragment newInstance(StepCallback stepCallback, IConsent... consents) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CONSENTS, consents);
        ConsentFragment fragment = new ConsentFragment();
        fragment.setArguments(args);
        fragment.setStepCallback(stepCallback);
        return fragment;
    }
    
    private ConsentFragmentBinding mBinding;
    private Disposable mActionDisposable = null;

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final Translator mTranslator = Components.get(Translator.class);

    private final List<IConsent> mConsents = new ArrayList<>();
    private int mSelectedPosition = 0;

    private final Action onComplete = this::getNextLayout;
    private final Consumer<Throwable> onError = throwable -> {
        Log.e(TAG, "Could not sent the consent.", throwable);

        if (mStepCallback != null) {
            // After error, need to appear the next consent page if there any in the list
            getNextLayout();
            return;
        }

        mNavigator.showFailedToSaveSettingsDialog();
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IConsent[] consents = getSerializableArgument(EXTRA_CONSENTS);

        if (consents != null) {
            Collections.addAll(mConsents, consents);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (mBinding == null) {
            mBinding = ConsentFragmentBinding.inflate(inflater, container, false);
            updateLayout();
        }

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mStepCallback != null) {
            mStepCallback.getStartupTimer().pause();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        RxUtils.disposeSilently(mActionDisposable);
    }

    @Override
    public boolean onBackPressed() {
        if (mStepCallback == null) {
            // Fragment is opened from settings.
            mNavigator.openPrivacySettings(getConsentAtPosition(mSelectedPosition));
            return super.onBackPressed();
        }

        // Do nothing on back press.
        return true;
    }

    public void getNextLayout(){
        if (mSelectedPosition < mConsents.size() - 1) {
            mSelectedPosition++;
            updateLayout();
            return;
        }
        // We don't have more consents, close the screen
        closeScreen();
    }

    @Override
    public void updateLayout() {
        Context context = mBinding.getRoot().getContext();
        IConsent currentConsent = getConsentAtPosition(mSelectedPosition);

        if (currentConsent == null || context == null) {
            Log.d(TAG, "Could not update layout.");
            return;
        }

        mBinding.leftContainer.showMoreButton.setBackground(UiUtils.createMoreButtonDrawable(context, mLayoutConfig));
        mBinding.agreeButton.setBackground(UiUtils.createFtiButtonBackground(context, mLayoutConfig));
        mBinding.declineButton.setBackground(UiUtils.createFtiButtonBackground(context, mLayoutConfig));
        mBinding.moreInfoButton.setBackground(UiUtils.createFtiButtonBackground(context, mLayoutConfig));
        mBinding.leftContainer.title.setTextColor(mLayoutConfig.getFontColor());
        mBinding.leftContainer.description.setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), mLayoutConfig.ALPHA_PLACEHOLDER_FONT));
        mBinding.leftContainer.footNote.setTextColor(mLayoutConfig.getFontColor());

        mBinding.leftContainer.title.setText(currentConsent.getTitle());
        mBinding.leftContainer.description.setText(Html.fromHtml(currentConsent.getDescription(), Html.FROM_HTML_MODE_COMPACT));
        mBinding.leftContainer.footNote.setText(mTranslator.get(TranslationKey.CONSENT_SCREEN_FOOTNOTE_TEXT));

        mBinding.agreeButton.setText(mTranslator.get(TranslationKey.BUTTON_AGREE_CONSENT_SCREENS));
        mBinding.declineButton.setText(mTranslator.get(TranslationKey.BUTTON_DECLINE_CONSENT_SCREENS));
        mBinding.moreInfoButton.setText(mTranslator.get(TranslationKey.BUTTON_MORE_INFO_CONSENT_SCREENS));

        float maxLines = mBinding.leftContainer.description.getMaxLines();
        float descriptionWidth = context.getResources().getDimension(R.dimen.consent_fragment_description_width);
        float textWidth = mBinding.leftContainer.description.getPaint().measureText(currentConsent.getDescription());

        if (descriptionWidth > 0 && textWidth / descriptionWidth > maxLines) {
            // Display the + button
            mBinding.leftContainer.showMoreButton.setVisibility(View.VISIBLE);
            mBinding.leftContainer.showMoreButton.setOnClickListener(e -> {
                AlertDialog dialog = new AlertDialog.Builder()
                        .title(currentConsent.getTitle())
                        .description(currentConsent.getDescription())
                        .descriptionFontSize(context.getResources().getDimensionPixelOffset(tv.threess.threeready.ui.R.dimen.alert_description_size_more_info))
                        .formatHtml(true)
                        .cancelable(true)
                        .build();
                mNavigator.showDialog(dialog);
            });
        }

        mBinding.agreeButton.setOnClickListener(e -> {
            RxUtils.disposeSilently(mActionDisposable);
            mActionDisposable = mAccountRepository.setConsent(currentConsent.getIdentifier(), true)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(onComplete, onError);
        });

        mBinding.declineButton.setOnClickListener(e -> {
            RxUtils.disposeSilently(mActionDisposable);
            mActionDisposable = mAccountRepository.setConsent(currentConsent.getIdentifier(), false)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(onComplete, onError);
        });


        if (!TextUtils.isEmpty(currentConsent.getDescription())) {
            mBinding.moreInfoButton.setVisibility(View.VISIBLE);
            mBinding.moreInfoButton.setOnClickListener(e -> mNavigator.showConsentsMoreInfoFragment(currentConsent, mStepCallback));
        } else {
            mBinding.moreInfoButton.setVisibility(View.GONE);
        }
        mBinding.agreeButton.requestFocus();
    }

    /**
     * Close the consent screen.
     */
    private void closeScreen() {
        if (mStepCallback != null) {
            mNavigator.popStartFragment();
            mStepCallback.getStartupTimer().resume();
            mStepCallback.onFinished();
            return;
        }
        mNavigator.popBackStack();
        mNavigator.openPrivacySettings(getConsentAtPosition(mSelectedPosition));
    }

    /**
     * Checks if the given position is valid and returns the consent for that position.
     * @param position Position from the list of consents.
     * @return Consent with the given position or null.
     */
    private IConsent getConsentAtPosition(int position) {
        if (mConsents.isEmpty() || position < 0 || position >= mConsents.size()) {
            return null;
        }
        return mConsents.get(position);
    }
}
