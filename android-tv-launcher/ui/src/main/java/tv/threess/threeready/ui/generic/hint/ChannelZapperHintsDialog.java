package tv.threess.threeready.ui.generic.hint;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;

import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.ChannelZapperHintsDialogBinding;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Dialog for channel zapper hints.
 * Shows a sequence of 2 hint screens for channel zapper.
 *
 * @author Zsolt Bokor_
 * @since 2020.06.15
 */

public class ChannelZapperHintsDialog extends HintsDialog {

    private ChannelZapperHintsDialogBinding mBinding;

    private enum Type {
        OPEN_PLAYER,
        OPEN_CHANNEL_ZAPPER
    }

    public static ChannelZapperHintsDialog newInstance(String hintId, boolean hintOpenedFromSettings) {
        Bundle args = new Bundle();
        args.putString(EXTRA_HINT_ID, hintId);
        args.putBoolean(EXTRA_HINT_OPENED_FROM_SETTINGS, hintOpenedFromSettings);
        ChannelZapperHintsDialog fragment = new ChannelZapperHintsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected View inflateView() {
        if (mBinding == null) {
            mBinding =  ChannelZapperHintsDialogBinding.inflate(LayoutInflater.from(getContext()));
        }
        return mBinding.getRoot();

    }

    @NonNull
    @Override
    protected ImageView getBackgroundImage() {
        return mBinding.background;
    }

    @NonNull
    @Override
    protected TextView getNextButton() {
        return mBinding.buttonContainer.nextBtn;
    }

    @NonNull
    @Override
    protected TextView getPreviousButton() {
        return mBinding.buttonContainer.previousBtn;
    }

    @Override
    protected int getBackgroundImageId() {
        return R.drawable.trickplay_hints_background;
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        if (!mHintOpenedFromSettings) {
            if (blockKeys()) {
                return true;
            }
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_UP:
                case KeyEvent.KEYCODE_DPAD_DOWN:
                    mNavigator.showEpgForPlayback();
                    dismiss();
                    return true;
            }
        }
        return super.onKeyUp(event);
    }

    @Override
    protected void initViews() {
        super.initViews();
        mBinding.descriptionSmall.setText(mTranslator.get(TranslationKey.HINT_REVIEW_ALL_YOUR_TIPS));
    }

    @Override
    protected void displayHint() {
        switch (Type.values()[mImagePosition]) {
            case OPEN_PLAYER:
                mBinding.remoteControl.setImageDrawable(getDrawable(R.drawable.remote_control_ok));
                setDescriptionTopMargin(getMargin(R.dimen.channel_zapper_hints_dialog_description_top_margin_open_player_screen));
                mBinding.description.setText(mTranslator.get(TranslationKey.HINT_TEXT_DISPLAY_PLAYER_UI));
                mBinding.descriptionSmall.setVisibility(View.GONE);
                mBinding.buttonContainer.nextBtn.setContentDescription(
                        new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_HINTS_BUTTON_NEXT))
                                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY, mTranslator.get(TranslationKey.HINT_TEXT_DISPLAY_PLAYER_UI))
                                .toString());
                break;

            case OPEN_CHANNEL_ZAPPER:
                mBinding.remoteControl.setImageDrawable(getDrawable(R.drawable.remote_control_up_down));
                setDescriptionTopMargin(getMargin(R.dimen.channel_zapper_hints_dialog_description_top_margin_open_zapper_screen));
                mBinding.description.setText(mTranslator.get(TranslationKey.HINT_TEXT_OPEN_CHANNEL_ZAPPER));
                mBinding.descriptionSmall.setVisibility(View.VISIBLE);
                mBinding.buttonContainer.nextBtn.setContentDescription(
                        new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_HINTS_BUTTON_DONE))
                                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BODY,
                                        mTranslator.get(TranslationKey.HINT_TEXT_OPEN_CHANNEL_ZAPPER) + " " + mTranslator.get(TranslationKey.HINT_REVIEW_ALL_YOUR_TIPS))
                                .toString());
                mBinding.buttonContainer.previousBtn.setContentDescription(mTranslator.get(TranslationKey.TALKBACK_HINTS_BUTTON_PREVIOUS));
                break;
        }
    }

    @Override
    protected boolean isLastImage() {
        return mImagePosition + 1 == Type.values().length;
    }

    /**
     * Sets the top margin for description.
     */
    private void setDescriptionTopMargin(int topMargin) {
        RelativeLayout.LayoutParams layoutParams =
                (RelativeLayout.LayoutParams) mBinding.description.getLayoutParams();
        layoutParams.topMargin = topMargin;
        mBinding.description.setLayoutParams(layoutParams);
    }

    @Override
    protected int getScreenNumbers() {
        return Type.values().length;
    }
}
