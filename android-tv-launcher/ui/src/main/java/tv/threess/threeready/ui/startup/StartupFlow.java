/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.startup;

import android.content.Context;

import java.util.Queue;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.helper.TimerLog;
import tv.threess.threeready.api.generic.receivers.ConnectivityStateChangeReceiver;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.startup.IStepCreator;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.generic.dialog.AlertDialog;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.startup.types.FlowTypes;
import tv.threess.threeready.ui.utils.SystemUtils;

/**
 * Manager class to handle the start up types(@{@link FlowTypes}) flows
 *
 * @author Barabas Attila
 * @since 2018.06.27
 */
public class StartupFlow implements Component {
    private static final String TAG = Log.tag(StartupFlow.class);

    private final IStepCreator mStepCreator = Components.get(IStepCreator.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    private final InternetChecker.DelayedStateChangedListener mOnInternetStateChangedListener;

    private final TimerLog mStartupTimerLog = new TimerLog(TAG);
    private final TimerLog mStepTimerLog = new TimerLog(TAG);

    private Queue<Step> mStepQueue;
    private Step mCurrentStep;

    private FlowTypes mFlowType = FlowTypes.START_UP;

    protected Context mContext;

    public StartupFlow(Context context) {
        mContext = context;

        //Alert the user when the internet or backend connectivity drops.
        long delayMiliseconds = TimeUnit.SECONDS.toMillis(Settings.internetCheckerTimeout.get(mAppConfig.getInternetCheckTimeout(TimeUnit.SECONDS)));
        mOnInternetStateChangedListener = new ConnectivityStateChangeReceiver.DelayedStateChangedListener(delayMiliseconds) {
            @Override
            public void onStateChanged(State state) {
                if (!isStartupStarted() || isStartupFinished() || mNavigator.isInBackground()) {
                    return;
                }

                Log.d(TAG, "Internet state changed. " + state);
                if (state.isInternetAvailable() && state.isBackendAvailable()) {
                    // Retry startup.
                    mNavigator.clearAllDialogs(true);
                    startStartupFlow();
                } else {
                    if (!state.isInternetAvailable()) {
                        mNavigator.showNoInternetAlertDialog();
                    } else if (!state.isBackendAvailable()) {
                        mNavigator.showNoBackendAlertDialog();
                    }
                }
            }
        };
        mInternetChecker.addStateChangedListener(mOnInternetStateChangedListener);
    }

    public boolean isStartupStarted() {
        return mStepQueue != null;
    }

    public boolean isStartupFinished() {
        return mStepQueue != null && mStepQueue.isEmpty();
    }

    /**
     * continues flow or start new one
     */
    public void onStart() {
        Log.d(TAG, "onStart");
        if (isStartupStarted()) {
            mStartupTimerLog.resume();
            executeSteps();
        } else {
            startStartupFlow();
        }
    }

    public void onStop() {
        Log.d(TAG, "onStop");
        stopStartupFlow();
    }

    public void startStartupFlow() {
        Log.d(TAG, "Start startup flow.");
        mOnInternetStateChangedListener.resetState();
        mStartupTimerLog.reset();
        mNavigator.clearAllDialogs();
        stopStartupFlow();
        buildSteps();
        executeSteps();
    }

    private void stopStartupFlow() {
        Log.d(TAG, "Stop startup flow.");
        if (mCurrentStep != null) {
            mCurrentStep.cancel();
        }
        mStartupTimerLog.pause();
    }

    /**
     * Build FTI or startup flow.
     */
    private void buildSteps() {
        switch (mFlowType) {
            case OFFLINE:
                Log.d(TAG, "Build Offline startup flow.");
                mStepQueue = mStepCreator.getOfflineSteps();
                break;
            case FTI:
                Log.d(TAG, "Build FTI startup flow.");
                mStepQueue = mStepCreator.getFTISteps();
                break;
            case STAND_BY:
                Log.d(TAG, "Build Stand By startup flow.");
                mStepQueue = mStepCreator.getStandbySteps();
                break;
            case START_UP:
                Log.d(TAG, "Build Startup flow.");
                mStepQueue = mStepCreator.getStartupSteps();
                break;
        }
    }

    /**
     * Execute the given steps queue sequentially.
     */
    private void executeSteps() {
        Queue<Step> stepQueue = mStepQueue;
        if (stepQueue == null) {
            return;
        }

        // Cancel the previous step.
        if (mCurrentStep != null) {
            mCurrentStep.cancel();
        }

        // Get the next step.
        mCurrentStep = stepQueue.peek();
        if (mCurrentStep == null) {
            mStartupTimerLog.d("Startup finished");
            return;
        }

        Log.d(TAG, "Start step. "
                + mCurrentStep.getClass().getSimpleName());

        mStepTimerLog.reset();
        mCurrentStep.execute(new StepCallback() {
            @Override
            public void onFinished() {
                mStepTimerLog.d("Step finished. "
                        + mCurrentStep.getClass().getSimpleName());

                if (!stepQueue.isEmpty()) {
                    // Remove finished step from the queue.
                    stepQueue.remove();
                }
                executeSteps();
            }

            @Override
            public void onFailed() {
                mStepTimerLog.d("Step failed. "
                        + mCurrentStep.getClass().getSimpleName());
                showErrorDialog();
            }

            @Override
            public TimerLog getStartupTimer() {
                return mStartupTimerLog;
            }
        });
    }

    /**
     * Whenever the app cannot start due to some MW or other error an alert shall be displayed to the user.
     */
    private void showErrorDialog() {
        stopStartupFlow();
        AlertDialog errorDialog = new AlertDialog.Builder()
                .title(mTranslator.get(TranslationKey.ERR_APP_STARTUP_NOT_POSSIBLE))
                .description(mTranslator.get(TranslationKey.ERR_APP_STARTUP_NOT_POSSIBLE_BODY))
                .cancelable(false)
                .addButton(mTranslator.get(TranslationKey.ERR_APP_STARTUP_NOT_POSSIBLE_RESTART_BUTTON),
                        dialog -> SystemUtils.reboot(mNavigator.getActivity(), null))
                .build();
        mNavigator.showDialog(errorDialog);
    }

    /**
     * Sets the type of the startup flow.
     * The flow needs to be started later.
     *
     * @param type {@see FlowTypes}
     */
    public void setStartupFlowType(FlowTypes type) {
        stopStartupFlow();
        mStepQueue = null;
        mFlowType = type;
    }

    /**
     * resets current flow and starts flow type passed, stack clear needed to be sure that different
     * callbacks don't influence flow
     *
     * @param type flow type
     */
    public void startStartupFlow(FlowTypes type) {
        mNavigator.clearBackStack();
        stopStartupFlow();
        mStepQueue = null;
        mFlowType = type;
        startStartupFlow();
    }

}
