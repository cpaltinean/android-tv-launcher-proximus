package tv.threess.threeready.ui.details.presenter;

import android.content.Context;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.ui.generic.model.RecordingPlayOption;

/**
 * Presenter class for recording actions play options screen
 *
 * @author David Bondor
 * @since 19.07.2022
 */
public class RecordingPlayOptionsCardPresenter extends BroadcastPlayOptionsCardPresenter<RecordingPlayOption> {

    public RecordingPlayOptionsCardPresenter(Context context, boolean isFromFavourite) {
        super(context, isFromFavourite);
    }

    @Override
    public void onClicked(ViewHolder holder, RecordingPlayOption playOption) {
        mNavigator.showRecordingPlayer(StartAction.Ui, playOption.getContentItem(), playOption.getRecordingSeries(), null, true);
        mNavigator.clearAllDialogs();
    }
}