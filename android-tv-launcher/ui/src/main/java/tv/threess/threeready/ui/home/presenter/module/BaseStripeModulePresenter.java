/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.home.presenter.module;

import android.content.Context;
import android.util.Pair;

import androidx.leanback.widget.Presenter;

import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceParams;
import tv.threess.threeready.api.home.model.module.ModuleDataType;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Base presenter class for a stripe module.
 *
 * Created by Szilard on 6/22/2017.
 */

public abstract class BaseStripeModulePresenter<THolder extends Presenter.ViewHolder> extends BaseModulePresenter<THolder> {

    private static final int Z_INDEX = 1;

    protected final Translator mTranslator = Components.get(Translator.class);

    public BaseStripeModulePresenter(Context context, DataSourceLoader dataSourceLoader) {
        super(context, dataSourceLoader);
    }

    @Override
    public void onBindHolder(THolder holder, ModuleData<?> moduleData, List<?> payLoads) {
        super.onBindHolder(holder, moduleData, payLoads);
        holder.view.setZ(getZIndex());
    }

    @Override
    public Pair<Integer, Integer> getLimit(ModuleConfig moduleConfig) {
        int[] size = null;
        if (moduleConfig.getDataSource() != null) {
            ModuleDataSourceParams moduleDataSourceParams = moduleConfig.getDataSource().getParams();
            size = moduleDataSourceParams != null ? moduleDataSourceParams.getSize() : null;
        }

        int start = 0;
        int count;
        if (moduleConfig.getDataType() == ModuleDataType.START_WATCHING) {
            // there is no configured limit for this stripe
            count = Integer.MAX_VALUE;
        } else {
            if (size != null && size.length > 0) {
                // Define page interval
                if (size.length == 1) {
                    count = size[0];
                } else {
                    start = size[0];
                    count = size[1];
                }
            } else {
                count = mApiConfig.getDefaultPageSize();
            }
        }

        return Pair.create(start, count);
    }

    /*
     * Method for getting Z-index of a module in a grid.
     * It could be overridden by the child classes.
     */
    public int getZIndex() {
        return Z_INDEX;
    }

}
