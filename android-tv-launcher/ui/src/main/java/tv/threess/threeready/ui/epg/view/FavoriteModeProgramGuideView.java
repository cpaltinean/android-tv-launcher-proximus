package tv.threess.threeready.ui.epg.view;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.databinding.FavoriteModeProgramGuideBinding;
import tv.threess.threeready.ui.generic.hint.HintManager;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.FontTextView;

/**
 * Fast scrolling program guide view which has also favorite management capability.
 *
 * @author Barabas Attila
 * @since 2017.12.04
 */

public class FavoriteModeProgramGuideView extends FastScrollingProgramGuideView {

    private final HintManager mHintManager = Components.get(HintManager.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    private boolean mIsFavoriteMode;
    private FavoriteModeProgramGuideBinding mBinding;

    private EpgView.FavoriteModeTriggerListener mFavoriteModeChangedListener;

    public FavoriteModeProgramGuideView(Context context) {
        this(context, null);
    }

    public FavoriteModeProgramGuideView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FavoriteModeProgramGuideView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        inflateLayout();

        mBinding.manageChannels.setTextColor(createButtonBrandingColorStateList());
        mBinding.manageChannels.setText(mTranslator.get(TranslationKey.BUTTON_EPG_MANAGE_CHANNELS));
    }

    public void setFavoriteModeTriggerListener(EpgView.FavoriteModeTriggerListener favoriteModeChangedListener) {
        mFavoriteModeChangedListener = favoriteModeChangedListener;
    }

    @Override
    protected void inflateLayout() {
        if (mBinding != null) {
            return;
        }
        
        mBinding = FavoriteModeProgramGuideBinding.inflate(LayoutInflater.from(getContext()), this);
    }

    @NonNull
    @Override
    protected ProgramGuideGridView getProgramGuideGridView() {
        return getGridView();
    }

    @Override
    @NonNull
    protected ExpandableProgramGuideGridView getGridView() {
        return mBinding.programGuideGrid;
    }

    @Override
    @NonNull
    protected FastScrollingView getFastScrollingView() {
        return mBinding.fastScrollingProgramGuide;
    }

    @Override
    @NonNull
    protected FontTextView getMoreDownView() {
        return mBinding.moreDown;
    }

    @Override
    public void expand(boolean animated) {
        super.expand(animated);

        if (mBinding.programGuideGrid.isExpanded()) {
            mBinding.manageChannels.setVisibility(View.GONE);
        }
    }

    @Override
    public void collapse(boolean animated) {
        super.collapse(animated);
        mBinding.manageChannels.setVisibility(View.VISIBLE);
    }

    /**
     * Display or hide the more button based on the channel type and grid state.
     */
    @Override
    protected void invalidateMoreButtonVisibility() {
        if (mIsFavoriteMode) {
            mBinding.moreDown.setVisibility(View.GONE);
        } else {
            super.invalidateMoreButtonVisibility();
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (mIsFavoriteMode) {
            return false;
        }

        if (event.getAction() == KeyEvent.ACTION_DOWN && !mBinding.programGuideGrid.isExpanded()) {

            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_UP:
                    triggerFavoriteMode();
                    return true;
                case KeyEvent.KEYCODE_DPAD_CENTER:
                case KeyEvent.KEYCODE_ENTER:
                    if (mBinding.manageChannels.isHovered()) {
                        triggerFavoriteMode();
                    }
                    break;
            }
        }

        return super.dispatchKeyEvent(event);
    }

    private void triggerFavoriteMode() {
        if (mFavoriteModeChangedListener != null) {
            mFavoriteModeChangedListener.onTriggerFavoriteMode(true);
        }
    }

    public void onTriggerDefaultMode() {
        mIsFavoriteMode = false;
        mBinding.manageChannels.setVisibility(View.VISIBLE);
        mBinding.manageChannels.setHovered(false);

        invalidateMoreButtonVisibility();
    }

    public void onDefaultMode() {
    }

    public void onTriggerFavoriteMode() {
        mIsFavoriteMode = true;
        mHintManager.showManageChannelHints();
    }


    public void onFavoriteMode() {
        mBinding.moreDown.setVisibility(View.GONE);
        mBinding.manageChannels.setVisibility(View.GONE);
    }

    public boolean isExpanded() {
        return mBinding.programGuideGrid.isExpanded();
    }

    public List<Animator> getFavoriteModeAnimator(boolean isFavoriteMode) {
        List<Animator> animatorList = new ArrayList<>();
        animatorList.add(isFavoriteMode ? getProgramGuideGridFadeOutAnimator() : getProgramGuideGridFadeInAnimator());
        animatorList.addAll(mBinding.programGuideGrid.getSizeAnimatorList(isFavoriteMode));
        animatorList.add(ObjectAnimator.ofFloat(mBinding.moreDown, View.ALPHA,
                mBinding.moreDown.getAlpha(), isFavoriteMode ? 0f : 1f));
        animatorList.add(ObjectAnimator.ofFloat(mBinding.manageChannels, View.ALPHA,
                mBinding.manageChannels.getAlpha(), isFavoriteMode ? 0f : 1f));
        return animatorList;
    }

    private ColorStateList createButtonBrandingColorStateList() {
        return new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_hovered},
                        new int[]{}
                },
                new int[]{
                        mLayoutConfig.getButtonFocusedFontColor(),
                        mLayoutConfig.getFontColor()
                }
        );
    }

    public void onContentShown() {
        if (shouldExpandProgramGuide()) {
            expand(false);
        } else {
            collapse(false);
            mBinding.manageChannels.setHovered(false);
        }
    }

    @Override
    public void onSelectedChannelChanged(@NonNull TvChannel channel, boolean isFromFavoriteSection) {
        super.onSelectedChannelChanged(channel, isFromFavoriteSection);
        if (shouldExpandProgramGuide()) {
            expand(false);
        } else {
            collapse(false);
        }
    }
}
