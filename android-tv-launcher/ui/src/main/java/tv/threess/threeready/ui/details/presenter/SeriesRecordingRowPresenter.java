/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.details.presenter;

import android.content.Context;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import tv.threess.threeready.api.generic.model.ISeason;
import tv.threess.threeready.api.generic.model.ISeries;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.ui.details.DetailOpenedFrom;
import tv.threess.threeready.ui.details.utils.SeriesFilterType;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;

/**
 * Presenter for the season/episode grid on a series recording detail page.
 *
 * @author Barabas Attila
 * @since 2018.11.25
 */
public class SeriesRecordingRowPresenter extends SeriesRowPresenter<IRecording> {

    private IRecordingSeries mSeries;

    public SeriesRecordingRowPresenter(Context context, @Nullable IRecording selectedEpisode,
                                       DetailOpenedFrom openedFrom,
                                       EpisodeSelectionListener<IRecording> episodeSelectionListener) {
        super(context, selectedEpisode, episodeSelectionListener);
        mEpisodePresenterSelector = new InterfacePresenterSelector().addClassPresenter(
                IRecording.class, new RecordingEpisodeCardPresenter(context, () -> mSeries, openedFrom));
    }

    @Override
    public void onBindHolder(ViewHolder<IRecording> holder, ISeries<IRecording> series) {
        super.onBindHolder(holder, series);

        mSeries = (IRecordingSeries) series;
    }

    @Override
    protected List<SeriesFilterType> getFilters(ViewHolder<IRecording> holder) {
        List<SeriesFilterType> filters = new ArrayList<>();
        if (!shouldHideTab(holder, SeriesFilterType.Episode)) {
            filters.add(SeriesFilterType.Episode);
        }
        if (!shouldHideTab(holder, SeriesFilterType.MostRecent)) {
            filters.add(SeriesFilterType.MostRecent);
        }
        if (!shouldHideTab(holder, SeriesFilterType.Planned)) {
            filters.add(SeriesFilterType.Planned);
        }

        return filters;
    }

    @Override
    protected List<ISeason<IRecording>> getSeasons(ViewHolder<IRecording> holder) {
        List<ISeason<IRecording>> seasonsList = new ArrayList<>();
        for (ISeason<IRecording> season : holder.mSeries.getSeasons()) {
            for (IRecording recording : season.getEpisodes()) {
                if (!recording.isFuture()) {
                    seasonsList.add(season);
                    break;
                }
            }
        }

        return seasonsList;
    }

    @Override
    protected List<IRecording> filterEpisodes(ViewHolder<IRecording> holder, SeriesFilterType selectedFilter) {
        List<IRecording> filterEpisodes = new ArrayList<>();
        switch (selectedFilter) {
            case Episode:
                filterEpisodes = holder.mSeries.getEpisodesSorted();
                break;
            case MostRecent:
                filterEpisodes = getMostRecentEpisodes(holder.mSeries);
                break;
            case Planned:
                filterEpisodes = getPlannedEpisodes(holder.mSeries);
                break;
        }
        return filterEpisodes;

    }

    private List<IRecording> getMostRecentEpisodes(ISeries<IRecording> series) {
        List<IRecording> episodes = new ArrayList<>();
        for (ISeason<IRecording> season : series.getSeasons()) {
            List<IRecording> seasonEpisodes = season.getEpisodes();
            for (IRecording episodeItem : seasonEpisodes) {
                if (episodeItem.isLive() || episodeItem.isPast()) {
                    episodes.add(episodeItem);
                }
            }
        }

        // Sort list by season/episode ascending.
        episodes.sort(Comparator.comparingLong(IRecording::getStart).reversed());

        return episodes;
    }

    private List<IRecording> getPlannedEpisodes(ISeries<IRecording> series) {
        List<IRecording> episodes = new ArrayList<>();
        for (ISeason<IRecording> season : series.getSeasons()) {
            List<IRecording> seasonEpisodes = season.getEpisodes();
            for (IRecording episodeItem : seasonEpisodes) {
                if (episodeItem.isFuture()) {
                    episodes.add(episodeItem);
                }
            }
        }

        // Sort list by season/episode ascending.
        episodes.sort(Comparator.comparingLong(IRecording::getStart));

        return episodes;
    }

    /**
     * Checks if the specified filter tab should be hidden.
     */
    private boolean shouldHideTab(ViewHolder<IRecording> holder, SeriesFilterType selectedFilter) {
        switch (selectedFilter) {
            case Episode:
                return holder.mSeries.getEpisodesSorted().size() == 0;
            case MostRecent:
                return getMostRecentEpisodes(holder.mSeries).size() == 0;
            case Planned:
                return getPlannedEpisodes(holder.mSeries).size() == 0;
        }
        return false;
    }
}
