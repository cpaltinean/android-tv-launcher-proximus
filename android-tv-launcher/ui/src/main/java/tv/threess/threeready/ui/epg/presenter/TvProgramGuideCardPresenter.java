/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.epg.presenter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.home.model.generic.ContentMarkers;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.pvr.model.SeriesRecordingStatus;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.LiveBookmark;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.ProgramGuideCardBinding;
import tv.threess.threeready.ui.details.DetailOpenedFrom;
import tv.threess.threeready.ui.epg.view.ForegroundImageView;
import tv.threess.threeready.ui.epg.view.ProgramGuideGridView;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.pvr.RecordingActionHelper;
import tv.threess.threeready.ui.utils.ImageTransform;
import tv.threess.threeready.ui.utils.UiUtils;
import tv.threess.threeready.ui.utils.WeakHandler;

/**
 * Presenter class for rendering a tv program card inside of the program guide grid. {@link ProgramGuideGridView}
 *
 * @author Barabas Attila
 * @since 2017.04.27
 */
public class TvProgramGuideCardPresenter extends BaseCardPresenter<TvProgramGuideCardPresenter.ViewHolder, IBroadcast> {
    private static final String TAG = Log.tag(TvProgramGuideCardPresenter.class);

    private static final int MAX_RECYCLER_VIEW_COUNT = 10;

    private final Translator mTranslator = Components.get(Translator.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final TvRepository mTvRepository = Components.get(TvRepository.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final RecordingActionHelper mRecordingActionHelper = Components.get(RecordingActionHelper.class);
    private final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);

    private final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);

    private final WeakHandler mHandler = new WeakHandler();

    //indicates that the card is from the favorite section
    private boolean mIsFromFavoriteSection;

    //indicates if the guide is expanded
    private boolean mIsProgramGuideExpanded;

    public void setFromFavoriteSection(boolean isFromFavoriteSection) {
        mIsFromFavoriteSection = isFromFavoriteSection;
    }

    public void setIsProgramGuideExpanded(boolean isProgramGuideExpanded) {
        mIsProgramGuideExpanded = isProgramGuideExpanded;
    }

    public TvProgramGuideCardPresenter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        ProgramGuideCardBinding binding = ProgramGuideCardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.mBinding.background.setBackgroundColor(mLayoutConfig.getPlaceHolderColor());
        holder.mBinding.title.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBinding.time.setTextColor(mLayoutConfig.getPlaceholderTransparentFontColor());
        holder.mBinding.channelNumber.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBinding.details.setTextColor(mLayoutConfig.getPlaceholderFontColor());

        holder.mBinding.notAvailableTitle.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBinding.notAvailableTitle.setText(mTranslator.get(TranslationKey.MODULE_CHANNEL_NOT_AVAILABLE_TITLE));
        holder.mBinding.notAvailableMessage.setTextColor(mLayoutConfig.getPlaceholderTransparentFontColor());
        holder.mBinding.notAvailableMessage.setText(mTranslator.get(TranslationKey.MODULE_CHANNEL_NOT_AVAILABLE_BODY_INTERNET));

        return holder;
    }

    @Override
    public int getMaxRecyclerViewCount() {
        return MAX_RECYCLER_VIEW_COUNT;
    }

    @Override
    public void onBindHolder(final ViewHolder holder, IBroadcast broadcast) {
        super.onBindHolder(holder, broadcast);
        Log.d(TAG, "onBindHolder() called with: broadcast = [" + broadcast + "]");

        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId(), mIsFromFavoriteSection);
        String channelNumber = channel != null ? String.valueOf(channel.getNumber()) : "";
        holder.mBinding.channelNumber.setText(channelNumber);

        updateCoverImage(holder, broadcast, channel);
        updateChannelLogo(holder, channel);

        if (broadcast.isPast()) {
            updateBroadcastProgress(holder, broadcast);
        }

        updateProgressIndicators(holder, broadcast);
        updateReplayIndicator(holder, broadcast, channel);
        updateDolbyIndicator(holder, broadcast);
        updateParentalRatingIndicator(holder, broadcast);
        updateRecordingIndicator(holder, broadcast);
        updateDescriptiveAudioIndicator(holder, broadcast);
        updateDescriptiveSubtitleIndicator(holder, broadcast);
        updateContentMarkers(holder, broadcast, holder.view.isFocused());

        scheduleAutomaticProgressIndicatorUpdate(holder, broadcast);

        updateTitle(holder, broadcast);
        updateInfoRow(holder, broadcast, channel);
        updateTime(holder, broadcast);
        updateContentDescription(holder, broadcast);
    }

    /**
     * Display the broadcast start and end time on the card.
     *
     * @param holder    The holder which contains the view for time.
     * @param broadcast The broadcast for which the time needs to be displayed.
     */
    private void updateTime(ViewHolder holder, IBroadcast broadcast) {
        // Display date and time.
        String aux = LocaleTimeUtils.getDate(broadcast, mTranslator, mLocaleSettings);

        SpannableString spannableString = new SpannableString(aux);
        int index = aux.indexOf(TimeUtils.DIVIDER);
        while (index >= 0) {
            spannableString.setSpan(new ForegroundColorSpan(mLayoutConfig.getPlaceholderTransparentFontColor()),
                    index + 1, index + 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            index = aux.indexOf(TimeUtils.DIVIDER, index + 1);
        }
        holder.mBinding.time.setText(spannableString);
    }

    /**
     * Display the episode title, genre and release year on the card.
     *
     * @param holder    The holder which contains the view for the subtitle.
     * @param broadcast The broadcast for which the information needs to be displayed.
     * @param channel   The channel on which the broadcast is available or null if there is no such.
     */
    private void updateInfoRow(ViewHolder holder, IBroadcast broadcast, @Nullable TvChannel channel) {
        if (channel != null && channel.getType() == ChannelType.RADIO) {
            holder.mBinding.details.setVisibility(View.GONE);
            return;
        }

        if (mParentalControlManager.isRestricted(broadcast)) {
            holder.mBinding.details.setVisibility(View.GONE);
            return;
        }

        // Display broadcast detail.
        StringBuilder detailBuilder = new StringBuilder();
        if (!TextUtils.isEmpty(broadcast.getEpisodeTitle())
                || broadcast.getSeasonNumber() != null
                || broadcast.getEpisodeNumber() != null) {

            detailBuilder.append(broadcast.getEpisodeTitleWithSeasonEpisode(mTranslator, ""));

        } else {
            // Display release year.
            if (!TextUtils.isEmpty(broadcast.getReleaseYear())) {
                detailBuilder.append(broadcast.getReleaseYear());
            }

            // Display genres.
            if (broadcast.getGenres().size() > 0) {
                if (detailBuilder.length() > 0) {
                    detailBuilder.append(TimeUtils.DIVIDER);
                }
                detailBuilder.append(TextUtils.join(", ",
                        broadcast.getGenres().stream().limit(3).toArray()));
            }
        }
        holder.mBinding.details.setText(UiUtils.addDividerColor(detailBuilder.toString(),
                TimeUtils.DIVIDER, mLayoutConfig.getPlaceholderTransparentFontColor()));
        holder.mBinding.details.setVisibility(View.VISIBLE);

    }

    private void updateTitle(ViewHolder holder, IBroadcast broadcast) {
        holder.mBinding.title.setText(mParentalControlManager.isRestricted(broadcast)
                ? mTranslator.get(TranslationKey.CARDS_TITLE_CONTENT_LOCKED) : broadcast.getTitle());
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);

        // Reset last viewed position
        holder.mBookmark = null;

        if (holder.view != null && holder.view.isSelected()) {
            holder.forceFocusedAnimatorState();
        } else {
            holder.forceDefaultAnimatorState();
        }

        holder.mBinding.notAvailableLayout.setVisibility(View.GONE);
        holder.mIsGreyedOut = false;

        Glide.with(mContext).clear(holder.mBinding.cover);
        Glide.with(mContext).clear(holder.mBinding.channelLogo);

        holder.mBinding.contentMarker.setVisibility(View.GONE);
        holder.mBinding.channelLogo.setVisibility(View.GONE);
        holder.mBinding.channelLogoGradient.setVisibility(View.GONE);
        holder.mBinding.iconContainer.hideAll();

        // Remove automatic indicator updates.
        cancelAutomaticIndicatorUpdate(holder);

        RxUtils.disposeSilently(
                holder.mPlaybackOptionDisposable,
                holder.mRecStatusDisposable,
                holder.mBookmarkDisposable);

    }

    @Override
    public void onDefaultState(ViewHolder holder, IBroadcast broadcast) {
        super.onDefaultState(holder, broadcast);
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId(), mIsFromFavoriteSection);

        holder.mBinding.title.setMaxLines(1);

        updateChannelLogoVisibility(holder, channel, false);
        updateFavoriteIcon(holder, false, channel);
        updateContentMarkers(holder, broadcast, false);
        updateProgressIndicators(holder, broadcast);
        updateAvailability(holder, broadcast, false, channel);
        updateDolbyIndicator(holder, broadcast);
    }

    @Override
    public void onFocusedState(final ViewHolder holder, final IBroadcast broadcast) {
        super.onFocusedState(holder, broadcast);
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId(), mIsFromFavoriteSection);

        holder.mBinding.title.setMaxLines(2);

        updateChannelLogoVisibility(holder, channel, true);
        updateFavoriteIcon(holder, true, channel);
        updateContentMarkers(holder, broadcast, true);
        updateProgressIndicators(holder, broadcast);
        updateAvailability(holder, broadcast, true, channel);
        updateDolbyIndicator(holder, broadcast);
    }

    /**
     * Updates the content description used for Talkback.
     */
    private void updateContentDescription(ViewHolder holder, IBroadcast broadcast) {
        if (!mMwRepository.isTalkBackOn()) {
            return;
        }

        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        holder.view.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_CARDS_MINI_EPG))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_CHANNEL_NAME, channel != null ? channel.getName() : "")
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_ASSET,
                                mParentalControlManager.isRestricted(broadcast) ? mTranslator.get(TranslationKey.CARDS_TITLE_CONTENT_LOCKED) : broadcast.getTitle())
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_ICONS, holder.mBinding.iconContainer.getIconsDescription(mTranslator))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_START_TIME, LocaleTimeUtils.getStartDate(broadcast, mTranslator, mLocaleSettings))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_END_TIME, LocaleTimeUtils.getEndTime(broadcast, mLocaleSettings))
                        .toString()
        );
    }

    @Override
    public void onInternetStateChanged(ViewHolder holder, IBroadcast broadcast, boolean available) {
        super.onInternetStateChanged(holder, broadcast, available);
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId(), mIsFromFavoriteSection);

        // Reload data fhe internet is back.
        if (available) {
            onBindHolder(holder, broadcast);
        }

        updateAvailability(holder, broadcast, holder.view.isFocused(), channel);
    }

    @Override
    public void onPlaybackChanged(ViewHolder holder, IBroadcast broadcast) {
        super.onPlaybackChanged(holder, broadcast);

        updateContentMarkers(holder, broadcast, holder.view.isFocused());
    }

    @Override
    protected void onParentalRatingChanged(ViewHolder holder, IBroadcast broadcast) {
        super.onParentalRatingChanged(holder, broadcast);
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId(), mIsFromFavoriteSection);

        updateCoverImage(holder, broadcast, channel);
        updateTitle(holder, broadcast);
        updateInfoRow(holder, broadcast, channel);
    }

    @Override
    public void onClicked(ViewHolder holder, final IBroadcast broadcast) {
        super.onClicked(holder, broadcast);

        if (holder.isGreyedOut()) {
            // Nothing to do grout channel.
            return;
        }

        Log.event(ReportUtils.createSelectionSelection(broadcast));

        if (mParentalControlManager.isRestricted(broadcast)) {
            mNavigator.showParentalControlUnblockDialog();
            return;
        }

        if (broadcast.isLive()) {
            TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId(), mIsFromFavoriteSection);
            if (channel == null) {
                Log.w(TAG, "No channel available.");
                return;
            }

            if (channel.getType() == ChannelType.RADIO) {
                Log.d(TAG, "Broadcast is live Radio, starting live playback!");
                mNavigator.showRadioPlayer(StartAction.Ui, broadcast.getChannelId(), mIsFromFavoriteSection, null, true);
                return;
            } else if (channel.getType() == ChannelType.TV) {
                if (mPlaybackDetailsManager.isBroadcastPlaying(broadcast) || mIsProgramGuideExpanded) {
                    Log.d(TAG, "Broadcast is live (already playing or guide expanded), opening detail page.");
                    openDetailPage(holder, broadcast);
                } else {
                    Log.d(TAG, "Broadcast is live, starting live playback!");
                    mNavigator.showLivePlayer(StartAction.Ui, broadcast.getChannelId(), mIsFromFavoriteSection);
                }
                return;
            }

            mNavigator.hideContentOverlay();
        } else {
            Log.d(TAG, "Opening detail page.");
            openDetailPage(holder, broadcast);
        }
    }

    /**
     * @return The width of the cover image in pixels.
     */
    protected int getCoverWidth(IBroadcast broadcast) {
        return mContext.getResources().getDimensionPixelSize(R.dimen.tv_program_guide_cover_width_focused);
    }

    /**
     * @return The height of the cover image in pixels.
     */
    protected int getCoverHeight(IBroadcast broadcast) {
        return getCardHeight(broadcast);
    }

    /**
     * Open single or series recording detail for the broadcast based on the recording status.
     */
    private void openDetailPage(ViewHolder holder, IBroadcast broadcast) {
        if (broadcast != null && broadcast.isGenerated()) {
            // do not open detail page for dummy broadcast
            return;
        }

        DetailOpenedFrom openedFrom = mIsFromFavoriteSection ? DetailOpenedFrom.Favorite : DetailOpenedFrom.Default;

        RecordingStatus recordingStatus = holder.mBinding.iconContainer.getRecordingStatus();
        if (recordingStatus != null && recordingStatus.getRecording() != null
                && recordingStatus.getSeriesRecordingStatus() == SeriesRecordingStatus.SCHEDULED) {
            mNavigator.showSeriesRecordingDetailPage(recordingStatus.getRecording(), openedFrom);
        } else {
            mNavigator.showProgramDetails(broadcast, openedFrom);
        }
    }

    @Override
    public boolean onKeyEvent(ViewHolder holder, IBroadcast broadcast, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    // ToDo: Sometimes the pressed state is false and in that case the onClick() method is not called.
                    // ToDo: So the Ok button functionality is not working. Find a better solution to handle that situation.
                    holder.view.setPressed(true);
                    break;
                case KeyEvent.KEYCODE_MEDIA_RECORD:
                    mRecordingActionHelper.handleRecKeyAction(broadcast,
                            holder.mBinding.iconContainer.getRecordingStatus());
                    return true;
            }
        }

        return super.onKeyEvent(holder, broadcast, event);
    }

    private void updateFavoriteIcon(final ViewHolder holder, boolean isFocused, TvChannel channel) {
        holder.mBinding.favoriteIcon.setVisibility(View.GONE);
        holder.mBinding.channelNumber.setVisibility(View.GONE);

        if (isFocused) {
            if (channel != null && channel.isFavorite()) {
                holder.mBinding.favoriteIcon.setVisibility(View.VISIBLE);
            } else {
                holder.mBinding.channelNumber.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Load and display the cover image for the broadcast.
     *
     * @param holder    The holder which contains cover image view.
     * @param broadcast The broadcast for which the image needs to be displayed.
     * @param channel   The channel on which the broadcast is available or null if there is no channel.
     */
    private void updateCoverImage(ViewHolder holder, IBroadcast broadcast, @Nullable TvChannel channel) {
        Glide.with(mContext).clear(holder.mBinding.programCardLayout);

        // Load blocked content cover.
        if (mParentalControlManager.isRestricted(broadcast)) {
            holder.mBinding.cover.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Glide.with(mContext)
                    .load(R.drawable.locked_epg_cover)
                    .format(DecodeFormat.PREFER_RGB_565)
                    .override(
                            mContext.getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_cover_width_default),
                            getCardHeight(broadcast))
                    .centerInside()
                    .into(holder.mBinding.cover);


        // Load radio channel logo
        } else if (channel != null && channel.getType() == ChannelType.RADIO) {
            holder.mBinding.cover.setScaleType(ImageView.ScaleType.CENTER);
            Glide.with(mContext)
                    .load(channel)
                    .format(DecodeFormat.PREFER_RGB_565)
                    .override(
                            mContext.getResources().getDimensionPixelOffset(R.dimen.proximus_radio_card_width),
                            mContext.getResources().getDimensionPixelOffset(R.dimen.proximus_radio_card_height))
                    .centerInside()
                    .into(holder.mBinding.cover);

        // Load program cover.
        } else {
            holder.mBinding.cover.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Glide.with(mContext)
                    .load(broadcast)
                    .format(DecodeFormat.PREFER_RGB_565)
                    .override(
                            getCoverWidth(broadcast),
                            getCoverHeight(broadcast))
                    .centerInside()
                    .into(holder.mBinding.cover);
        }
    }

    /**
     * Display the channel logo on the card.
     *
     * @param holder  The holder which contains the image view for the logo.
     * @param channel The channel for which the logo needs to be displayed
     *                or null if there is no channel available.
     */
    private void updateChannelLogo(ViewHolder holder, @Nullable TvChannel channel) {
        Glide.with(mContext).clear(holder.mBinding.channelLogo);

        // No channel. Nothing to display.
        if (channel == null) {
            holder.mBinding.channelLogoGradient.setVisibility(View.GONE);
            holder.mBinding.channelLogo.setVisibility(View.GONE);
            return;
        }

        // Load channel logo
        if (channel.getType() != ChannelType.RADIO) {
            Glide.with(mContext)
                    .load(channel)
                    .apply(ImageTransform.REQUEST_OPTIONS)
                    .transition(new DrawableTransitionOptions().dontTransition())
                    .dontAnimate()
                    .into(new DrawableImageViewTarget(holder.mBinding.channelLogo) {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            super.onResourceReady(resource, transition);
                            if (holder.view.isFocused()) {
                                holder.mBinding.channelLogoGradient.setVisibility(View.VISIBLE);
                                holder.mBinding.channelLogo.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        }
    }

    /**
     * Show and hide the channel logo visibility based on the channel type and focus state.
     *
     * @param holder  The holder which contains the view for the channel logo.
     * @param channel The channel for which the logo visibility needs to be updated.
     * @param focused True if the card is currently focused.
     */
    private void updateChannelLogoVisibility(ViewHolder holder, @Nullable TvChannel channel, boolean focused) {
        holder.mBinding.channelLogo.setVisibility(focused && channel != null
                && channel.getType() != ChannelType.RADIO ? View.VISIBLE : View.GONE);
        holder.mBinding.channelLogoGradient.setVisibility(focused && channel != null
                && channel.getType() != ChannelType.RADIO ? View.VISIBLE : View.GONE);
    }


    /**
     * Grey out programs based on the channel playback options and connectivity state,.
     *
     * @param holder    The holder which contains the views on the card.
     * @param broadcast The broadcast for which the playability needs to be verified.
     * @param isFocused True if the card is currently in focus.
     * @param channel   The channel on which the broadcast is available.
     */
    private void updateAvailability(final ViewHolder holder, final IBroadcast broadcast, boolean isFocused, @Nullable TvChannel channel) {

        // Blacklisted program
        if (broadcast.isBlackListed()) {
            holder.mBinding.notAvailableLayout.setVisibility(View.GONE);
            holder.mBinding.iconContainer.hideReplay();
            setForegroundImage(holder.mBinding.cover, channel, broadcast.isBlackListed(), isFocused);

            // Live Program
        } else if (broadcast.isLive()) {
            setForegroundImage(holder.mBinding.cover, channel, broadcast.isBlackListed(), isFocused);
            holder.mBinding.notAvailableLayout.setVisibility(View.GONE);

            if (isFocused) {
                // Check if playback option available.
                checkChannelAvailability(holder);
            }

            // Past program
        } else if (broadcast.isPast()) {
            holder.mIsGreyedOut = false;
            holder.mBinding.notAvailableLayout.setVisibility(View.GONE);
            setForegroundImage(holder.mBinding.cover, channel, broadcast.isBlackListed(), isFocused);

            // Future program
        } else {
            holder.mIsGreyedOut = false;
            holder.mBinding.notAvailableLayout.setVisibility(View.GONE);
            setForegroundImage(holder.mBinding.cover, channel, broadcast.isBlackListed(), isFocused);
        }

        if (!mInternetChecker.isInternetAvailable()) {
            holder.mBinding.notAvailableLayout.setVisibility(View.VISIBLE);
            holder.mBinding.contentMarker.setVisibility(View.GONE);
            holder.mBinding.progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Sets the foreground image cover.
     *
     * @param coverView     ImageCover that will be set.
     * @param channel       The channel on which the card is displayed.
     * @param isBlackListed Flag used to know if the program is blacklisted or not.
     * @param isFocused     Flag used to know if the card is in focused state or not.
     */
    private void setForegroundImage(ForegroundImageView coverView, @Nullable TvChannel channel, boolean isBlackListed, boolean isFocused) {
        int drawableId = R.color.program_guide_offline_and_radio_card_overlay;

        if (channel == null || channel.getType() != ChannelType.RADIO) {
            if (isBlackListed) {
                drawableId = R.color.program_guide_offline_and_radio_card_overlay;
            } else {
                drawableId = isFocused ? R.color.program_guide_active_card_overlay : R.color.program_guide_card_overlay;
            }
        }

        coverView.setForeground(ContextCompat.getDrawable(mContext, drawableId));
    }

    private void checkChannelAvailability(final ViewHolder holder) {
        if (!mInternetChecker.isInternetAvailable()) {
            holder.mBinding.notAvailableMessage.setText(mTranslator.get(TranslationKey.MODULE_CHANNEL_NOT_AVAILABLE_BODY_INTERNET));
        }
    }

    /**
     * Show dolby logo when available and the currently running card focused.
     */
    private void updateDolbyIndicator(ViewHolder holder, IBroadcast broadcast) {
        if (holder.view.isFocused() && mPlaybackDetailsManager.isBroadcastPlaying(broadcast)
                && mPlaybackDetailsManager.isDolbyAvailable()) {
            holder.mBinding.iconContainer.showDolby();
        } else {
            holder.mBinding.iconContainer.hideDolby();
        }
    }

    /**
     * Update recording indicator for the broadcast.
     */
    private void updateRecordingIndicator(final ViewHolder holder, final IBroadcast broadcast) {
        RxUtils.disposeSilently(holder.mRecStatusDisposable);
        holder.mRecStatusDisposable = mPvrRepository.getRecordingStatus(broadcast)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recordingStatus -> {
                            holder.mBinding.iconContainer.showRecordingStatus(recordingStatus);
                            updateContentDescription(holder, broadcast);
                            holder.view.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED);
                        }
                        , throwable -> {
                            Log.e(TAG, "Couldn't get the recording status.", throwable);
                            holder.mBinding.iconContainer.hideRecordingStatus();
                        });
    }

    private void updateBroadcastProgress(final ViewHolder holder, final IBroadcast broadcast) {
        RxUtils.disposeSilently(holder.mBookmarkDisposable);
        holder.mBookmarkDisposable = mTvRepository.getBookmarkForBroadcast(broadcast, false)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        bookmark -> {
                            Log.d(TAG, "Bookmark : " + bookmark);
                            holder.mBookmark = bookmark;
                            updateProgressIndicators(holder, broadcast);
                        },
                        throwable -> Log.e(TAG, "Couldn't get the recording last position.", throwable));
    }

    /**
     * Calculate and display the content marker based on the broadcast and focus state.
     *
     * @param holder    Recycler view holder, which contains the view references.
     * @param broadcast The broadcast for which the marker should be displayed.
     * @param isFocused True if the the card is currently focused, false otherwise.
     */
    private void updateContentMarkers(ViewHolder holder, IBroadcast broadcast, boolean isFocused) {
        holder.mBinding.contentMarker.showMarkers(broadcast, ContentMarkers.TypeFilter.MiniEpg, isFocused);
    }

    /**
     * Schedule an update for live and catchup indicators when the show starts or ends.
     */
    private void scheduleAutomaticProgressIndicatorUpdate(ViewHolder holder, IBroadcast broadcast) {
        cancelAutomaticIndicatorUpdate(holder);

        // Set new indicator updater.
        IndicatorUpdaterRunnable updateIndicatorsRunnable = new IndicatorUpdaterRunnable(holder, broadcast);
        holder.mUpdateIndicatorsRunnable = updateIndicatorsRunnable;

        // Schedule update for live program.
        if (broadcast.isLive()) {
            mHandler.postDelayed(updateIndicatorsRunnable,
                    broadcast.getEnd() - System.currentTimeMillis());

            // Schedule update for future program.
        } else if (broadcast.isFuture()) {
            mHandler.postDelayed(updateIndicatorsRunnable,
                    broadcast.getStart() - System.currentTimeMillis());
        }
    }

    /**
     * Cancel the previously scheduled automatic indicator update.
     */
    private void cancelAutomaticIndicatorUpdate(ViewHolder holder) {
        IndicatorUpdaterRunnable updateIndicatorsRunnable = holder.mUpdateIndicatorsRunnable;
        if (updateIndicatorsRunnable != null) {
            mHandler.removeCallbacks(updateIndicatorsRunnable);
        }
    }

    /**
     * Display live logos and progress bar.
     */
    private void updateProgressIndicators(final ViewHolder holder, IBroadcast broadcast) {
        // Past program
        if (broadcast.isPast() && holder.mBookmark != null && holder.mBookmark.getPosition() > 0) {
            holder.mBinding.progressBar.setProgressData(holder.mBookmark);
            holder.mBinding.progressBar.setVisibility(View.VISIBLE);
            // Live program
        } else if (broadcast.isLive()) {
            holder.mBinding.progressBar.setVisibility(View.VISIBLE);
            holder.mBinding.progressBar.setProgressData(new LiveBookmark(broadcast));
        } else {
            holder.mBinding.progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public int getCardWidth(IBroadcast broadcast) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_width_focused);
    }

    @Override
    public int getCardHeight(IBroadcast broadcast) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_height_focused);
    }

    /**
     * Display the replay icon for past replay capable programs.
     *
     * @param holder    The view holder which contains the replay icon.
     * @param broadcast The broadcast for which the replay icon needs to be displayed.
     * @param channel   The channel on which the broadcast is available, or null if there is no channel for the broadcast.
     */
    private void updateReplayIndicator(final ViewHolder holder, IBroadcast broadcast, @Nullable TvChannel channel) {
        if (broadcast.canReplay(channel)) {
            holder.mBinding.iconContainer.showReplay();
        } else {
            holder.mBinding.iconContainer.hideReplay();
        }
    }

    /**
     * Display the parental ration code on the card.
     *
     * @param holder    The holder which contains the parental rating icons.
     * @param broadcast The broadcast for which the rating needs to be displayed.
     */
    private void updateParentalRatingIndicator(ViewHolder holder, IBroadcast broadcast) {
        holder.mBinding.iconContainer.showParentalRating(broadcast.getParentalRating());
    }

    /**
     * Display the descriptive audio icon on the card.
     *
     * @param holder    The holder which contains the parental rating icons.
     * @param broadcast The broadcast for which the rating needs to be displayed.
     */
    private void updateDescriptiveAudioIndicator(final ViewHolder holder, IBroadcast broadcast) {
        if (broadcast.hasDescriptiveAudio()) {
            holder.mBinding.iconContainer.showDescriptiveAudio();
        } else {
            holder.mBinding.iconContainer.hideDescriptiveAudio();
        }
    }

    /**
     * Display the descriptive subtitle icon on the card.
     *
     * @param holder    The holder which contains the parental rating icons.
     * @param broadcast The broadcast for which the rating needs to be displayed.
     */
    private void updateDescriptiveSubtitleIndicator(final ViewHolder holder, IBroadcast broadcast) {
        if (broadcast.hasDescriptiveSubtitle()) {
            holder.mBinding.iconContainer.showDescriptiveSubtitle();
        } else {
            holder.mBinding.iconContainer.hideDescriptiveSubtitle();
        }
    }

    /**
     * Runnable to schedule content marker and progress bar visibility update.
     */
    private class IndicatorUpdaterRunnable implements Runnable {
        IBroadcast mProgram;
        ViewHolder mHolder;

        private IndicatorUpdaterRunnable(ViewHolder holder, IBroadcast program) {
            mProgram = program;
            mHolder = holder;
        }

        @Override
        public void run() {
            updateProgressIndicators(mHolder, mProgram);
            updateContentMarkers(mHolder, mProgram, mHolder.view.isFocused());
        }
    }

    @Override
    public long getStableId(IBroadcast broadcast) {
        return Objects.hash(broadcast.getId());
    }

    public static class ViewHolder extends BaseCardPresenter.ViewHolder {
        private final ProgramGuideCardBinding mBinding;

        private boolean mIsGreyedOut;

        private IBookmark mBookmark;

        private IndicatorUpdaterRunnable mUpdateIndicatorsRunnable;
        private Disposable mPlaybackOptionDisposable;
        private Disposable mRecStatusDisposable;
        private Disposable mBookmarkDisposable;

        public ViewHolder(ProgramGuideCardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        public ImageView getChannelLogoView() {
            return mBinding.channelLogo;
        }

        boolean isGreyedOut() {
            return mIsGreyedOut;
        }


        void forceDefaultAnimatorState() {
            // Set the default values
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            params.width = view.getContext().getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_width_default);
            params.height = view.getContext().getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_height_default);
            view.setTranslationX(view.getContext().getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_translation_x_default));
            view.setTranslationY(0f);

            mBinding.coverContainer.getLayoutParams().width = view.getResources()
                    .getDimensionPixelOffset(R.dimen.tv_program_guide_cover_width_default);

            view.requestLayout();
        }

        public void forceFocusedAnimatorState() {
            view.getStateListAnimator().jumpToCurrentState();

            mBinding.programCardLayout.getLayoutParams().width = view.getResources()
                    .getDimensionPixelOffset(R.dimen.tv_program_guide_card_width_focused);
            mBinding.programCardLayout.getLayoutParams().height = view.getResources()
                    .getDimensionPixelOffset(R.dimen.tv_program_guide_card_height_focused);
            view.setTranslationX(view.getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_translation_x_focused));
            view.setElevation(view.getResources().getDimensionPixelOffset(R.dimen.card_focused_elevation));

            mBinding.coverContainer.getStateListAnimator().jumpToCurrentState();
            mBinding.coverContainer.getLayoutParams().width = view.getResources()
                    .getDimensionPixelOffset(R.dimen.tv_program_guide_cover_width_focused);

            mBinding.programCardLayout.requestLayout();
        }

    }
}
