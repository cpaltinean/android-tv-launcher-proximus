/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.search.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.core.content.res.ResourcesCompat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.local.FontConfig;
import tv.threess.threeready.api.home.model.local.FontType;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.search.model.SearchSuggestion;
import tv.threess.threeready.api.search.model.SearchTerm;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.utils.FontStylist;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Used for displaying suggestions, only above keyboard, and will trigger a text changed listener
 * when some time passes after the user stops editing the text.
 * To update the suggestions just use setSuggestions.
 *
 * @author Paul
 * @since 2017.07.03
 */

public class SuggestionsAutoCompleteTextView extends AppCompatAutoCompleteTextView {
    private static final String TAG = Log.tag(SuggestionsAutoCompleteTextView.class);

    private static final long WAIT_TEXT_INTERVAL = 500;

    private final FontStylist mFontStylist = Components.getOrNull(FontStylist.class);

    private InputListener mInputListener;

    private final Map<String, SearchSuggestion> mSuggestionMap = new HashMap<>();

    private Disposable mSearchInputDisposable;

    private FontConfig mFontConfig;

    public SuggestionsAutoCompleteTextView(Context context) {
        this(context, null);
    }

    public SuggestionsAutoCompleteTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SuggestionsAutoCompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        if (isInEditMode()) {
            return;
        }

        // Read font type from attributes.
        FontType font = FontType.REGULAR; // default
        if (attrs != null) {
            TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.FontTextView, defStyleAttr, 0);
            try {
                int fwAttr = typedArray.getInteger(R.styleable.FontTextView_fontType, font.attrValue);
                font = FontType.forAttributeValue(fwAttr);
            } finally {
                typedArray.recycle();
            }
        }

        setFontType(font);

        // Override the sizes based on font type
        setTextSize(TypedValue.COMPLEX_UNIT_PX, super.getTextSize());
        setLineSpacing(super.getLineSpacingExtra(), super.getLineSpacingMultiplier());
        setLetterSpacing(super.getLetterSpacing());

        setDropDownHeight(0);

        RxUtils.disposeSilently(mSearchInputDisposable);

        getTextChangeObservable()
                .filter(s -> mInputListener != null)
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mSearchInputDisposable = d;
                    }

                    @Override
                    public void onNext(String text) {
                        Log.d(TAG, "onNext() called with: text = [" + text + "]");
                        if (mSuggestionMap.containsKey(text)) {
                            mInputListener.onTextChanged(mSuggestionMap.get(text));

                            // Direct text search.
                        } else {
                            mInputListener.onTextChanged(new SearchTerm(text));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "Error while subscribing to the text changed observable ", e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        RxUtils.disposeSilently(mSearchInputDisposable);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mInputListener != null) {
                mInputListener.onKeyboardCanceled();
            }
            return true;
        }

        return super.onKeyPreIme(keyCode, event);
    }

    /**
     * Create text change observable for the {@code searchInput}
     *
     * @return An observable that will emmit the debounced input term
     */
    private Observable<String> getTextChangeObservable() {
        return Observable.create((ObservableOnSubscribe<String>) emitter -> addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                emitter.onNext(s.toString());
            }
        }))
                .debounce(WAIT_TEXT_INTERVAL, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void setFontType(@NonNull final FontType fontStyle) {
        if (isInEditMode() || mFontStylist == null) {
            return;
        }

        mFontConfig = mFontStylist.getFontConfig(fontStyle);
        if (mFontConfig == null) {
            // Not custom font available.
            return;
        }

        // Get font id by name.
        int fontId = getContext().getResources()
                .getIdentifier(mFontConfig.getName(), "font", getContext().getPackageName());

        if (fontId > 0) {
            setTypeface(ResourcesCompat.getFont(getContext(), fontId));
        }
    }

    @Override
    public void setLineSpacing(float add, float mult) {
        float lineSpacingExtraFactor = mFontConfig == null
                ? 1f : mFontConfig.getLineSpacingExtraFactor();
        super.setLineSpacing(add * lineSpacingExtraFactor, mult);
    }

    @Override
    public void setLetterSpacing(float letterSpacing) {
        float letterSpacingExtraFactor = mFontConfig == null
                ? 1f : mFontConfig.getLetterSpacingFactor();
        super.setLetterSpacing(letterSpacing * letterSpacingExtraFactor);
    }

    @Override
    public void setTextSize(int unit, float size) {
        float textSizeFactor = mFontConfig == null
                ? 1f : mFontConfig.getTextSizeFactor();
        super.setTextSize(unit, size * textSizeFactor);
    }

    public void setInputListener(InputListener suggestionListener) {
        mInputListener = suggestionListener;
    }

    /**
     * @param suggestions the list of strings that will be the suggestions
     */
    public void setSuggestions(List<SearchSuggestion> suggestions) {
        mSuggestionMap.clear();

        int count = suggestions.size();

        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            CompletionInfo[] completions = new CompletionInfo[count];

            for (int i = 0; i < count; i++) {
                SearchSuggestion suggestion = suggestions.get(i);
                mSuggestionMap.put(suggestion.getTerm(), suggestion);
                completions[i] = new CompletionInfo(0, i, suggestion.getTerm());
            }

            imm.displayCompletions(this, completions);
        }
    }

    public interface InputListener {
        void onKeyboardCanceled();

        void onTextChanged(SearchTerm term);
    }
}
