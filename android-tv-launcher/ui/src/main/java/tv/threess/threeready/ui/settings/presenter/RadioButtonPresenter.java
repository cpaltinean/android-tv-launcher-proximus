/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.settings.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.leanback.widget.Presenter;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.SettingsRadioButtonBinding;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.settings.listener.ItemClickListener;
import tv.threess.threeready.ui.settings.model.RadioButtonItem;

/**
 * This class will bind the layout to the model of radio button items.
 *
 * @author Paul
 * @since 2017.05.30
 */

public class RadioButtonPresenter extends BasePresenter<RadioButtonPresenter.ViewHolder, RadioButtonItem> {

    private final ItemClickListener mItemClickListener;

    public RadioButtonPresenter(Context context, ItemClickListener itemClickListener) {
        super(context);
        mItemClickListener = itemClickListener;
    }

    @Override
    public void onBindHolder(ViewHolder holder, RadioButtonItem item) {
        holder.mBinding.text.setText(item.getTitle());
        holder.mBinding.text.setCompoundDrawablesRelativeWithIntrinsicBounds(
                item.isChecked() ? R.drawable.ico_check_active : R.drawable.ico_check_default, 0, 0, 0);
    }

    @Override
    public RadioButtonPresenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(SettingsRadioButtonBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    public static class ViewHolder extends Presenter.ViewHolder {

        private final SettingsRadioButtonBinding mBinding;

        public ViewHolder(SettingsRadioButtonBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }

    @Override
    public void onClicked(ViewHolder holder, RadioButtonItem item) {
        mItemClickListener.onItemClick(item);
    }
}
