package tv.threess.threeready.ui.settings.model;

import tv.threess.threeready.api.pc.model.ParentalRating;

/**
 * Entity class representing a parental control item.
 *
 * @author Daniela Toma
 * @since 2019.05.30
 */
public class LockContentItem {

    private final String mDescription;

    private final ParentalRating mParentalRating;

    public LockContentItem(String description, ParentalRating type) {
        mDescription = description;
        mParentalRating = type;
    }

    public String getDescription() {
        return mDescription;
    }

    public ParentalRating getParentalRating() {
        return mParentalRating;
    }


}
