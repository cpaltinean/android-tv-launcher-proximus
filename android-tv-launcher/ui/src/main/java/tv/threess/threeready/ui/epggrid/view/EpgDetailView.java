package tv.threess.threeready.ui.epggrid.view;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.EpgDataLayoutBinding;
import tv.threess.threeready.ui.details.utils.ImageTargetView;
import tv.threess.threeready.ui.generic.activity.BaseActivity;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.TransitionTarget;

/**
 * View which displays the details about the selected broadcast in the EPG grid.
 *
 * @author Andor Lukacs
 * @since 28/07/2021
 */
public class EpgDetailView extends RelativeLayout {
    private static final String TAG = Log.tag(EpgDetailView.class);

    private IBroadcast mSelectedBroadcast;

    private final Handler mUpdateContentMarkerHandler;
    private Disposable mRecStatusDisposable;

    private final Translator mTranslator = Components.get(Translator.class);
    private final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);
    private final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);
    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);

    protected ImageTargetView mImageTargetView;

    private final Context mAppContext;
    private final EpgDataLayoutBinding mBinding;

    public EpgDetailView(Context context) {
        this(context, null);
    }

    public EpgDetailView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EpgDetailView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mAppContext = context.getApplicationContext();

        mBinding = EpgDataLayoutBinding.inflate(LayoutInflater.from(context), this);

        LayoutConfig layoutConfig = Components.get(LayoutConfig.class);
        mBinding.title.setTextColor(layoutConfig.getFontColor());
        mBinding.infoRow.setTextColor(layoutConfig.getFontColor());
        mBinding.description.setTextColor(layoutConfig.getFontColor());

        // Glide image target.
        mImageTargetView = new ImageTargetView(mAppContext,
                mBinding.bottomFadeGradient,
                mBinding.startFadeGradient,
                mBinding.coverBig,
                mBinding.cover);

        mUpdateContentMarkerHandler = new Handler();

        mParentalControlManager.addListener(mParentalControlListener);
        mPlaybackDetailsManager.registerCallback(mIPlaybackCallback);
    }

    public void updateDetails(IBroadcast broadcast) {
        mSelectedBroadcast = broadcast;

        if (!isAttachedToWindow()) {
            // Not attached. Update once it's attached.
            return;
        }

        updateTitle(broadcast);
        updateContentMarkers(broadcast);
        updateInfoRow(broadcast);
        updateIconContainer(broadcast);
        updateDescription(broadcast);
        updateProgramCover(broadcast);
    }

    private void updateContentMarkers(IBroadcast broadcast) {
        mUpdateContentMarkerHandler.removeCallbacks(mUpdateContentMarkerRunnable);
        if (broadcast.isGenerated()) {
            mBinding.contentMarker.hideMarkers();
            return;
        }

        mBinding.contentMarker.showMarkers(broadcast);

        if (broadcast.isLive()) {
            mUpdateContentMarkerHandler.postDelayed(mUpdateContentMarkerRunnable,
                    broadcast.getEnd() - System.currentTimeMillis());
            return;
        }

        if (broadcast.isFuture()) {
            mUpdateContentMarkerHandler.postDelayed(mUpdateContentMarkerRunnable,
                    broadcast.getStart() - System.currentTimeMillis());
        }
    }

    public void updateProgramCover(IBroadcast broadcast) {
        TransitionTarget target = mImageTargetView.getCurrentIndex() % 2 == 0
                ? mImageTargetView.getFirstTransitionTarget() : mImageTargetView.getSecondTransitionTarget();

        int width = mAppContext.getResources().getDimensionPixelSize(R.dimen.details_cover_width);
        int height = mAppContext.getResources().getDimensionPixelSize(R.dimen.details_cover_height);

        if (mParentalControlManager.isRestricted(broadcast)) {
            mImageTargetView.clearImage();
            Glide.with(mAppContext)
                    .load(R.drawable.locked_background)
                    .format(DecodeFormat.PREFER_RGB_565)
                    .override(width, height)
                    .into(target);
            mImageTargetView.increaseCurrentIndex();

            return;
        }

        mImageTargetView.clearImage();
        Glide.with(mAppContext).load(broadcast)
                .fallback(R.drawable.content_fallback_image_landscape)
                .error(R.drawable.content_fallback_image_landscape)
                .format(DecodeFormat.PREFER_RGB_565)
                .override(width, height)
                .optionalCenterInside()
                .into(target);

        mImageTargetView.increaseCurrentIndex();
    }

    private void updateIconContainer(IBroadcast broadcast) {
        updateParentalRatingIcon(broadcast);
        updateReplayIcon(broadcast);
        updateRecordingIcon(broadcast);
        updateDescriptiveAudioIcon(broadcast);
        updateDescriptiveSubtitleIcon(broadcast);
    }

    /**
     * Display the recording icon when available.
     */
    private void updateRecordingIcon(IBroadcast broadcast) {
        RxUtils.disposeSilently(mRecStatusDisposable);
        mRecStatusDisposable = mPvrRepository.getRecordingStatus(broadcast)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mBinding.iconContainer::showRecordingStatus
                        , throwable -> {
                            Log.e(TAG, "Couldn't get the recording status.", throwable);
                            mBinding.iconContainer.hideRecordingStatus();
                        });
    }

    /**
     * Display the replay icon when capable.
     */
    private void updateParentalRatingIcon(IBroadcast broadcast) {
        mBinding.iconContainer.showParentalRating(broadcast.getParentalRating());
    }

    private void updateReplayIcon(IBroadcast broadcast) {
        TvChannel channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
        if (broadcast.canReplay(channel)) {
            mBinding.iconContainer.showReplay();
        } else {
            mBinding.iconContainer.hideReplay();
        }
    }

    /**
     * Displays descriptive audio icon when is available.
     */
    private void updateDescriptiveAudioIcon(IBroadcast broadcast) {
        if(broadcast.hasDescriptiveAudio()) {
            mBinding.iconContainer.showDescriptiveAudio();
        } else {
            mBinding.iconContainer.hideDescriptiveAudio();
        }
    }

    /**
     * Displays descriptive subtitle audio when is available.
     */
    private void updateDescriptiveSubtitleIcon(IBroadcast broadcast) {
        if(broadcast.hasDescriptiveSubtitle()) {
            mBinding.iconContainer.showDescriptiveSubtitle();
        } else {
            mBinding.iconContainer.hideDescriptiveSubtitle();
        }
    }

    /**
     * Display the broadcast airing information and genres.
     */
    private void updateInfoRow(IBroadcast broadcast) {
        if (broadcast.isGenerated()) {
            mBinding.infoRow.setText(null);
            return;
        }

        StringBuilder stringBuilder = new StringBuilder();

        String date = LocaleTimeUtils.getDate(broadcast, mTranslator, mLocaleSettings);
        if (!TextUtils.isEmpty(date)) {
            stringBuilder.append(date);
        }

        String genres = TextUtils.join(", ",
                broadcast.getGenres().stream().limit(3).toArray());
        if (!TextUtils.isEmpty(genres)) {
            if(stringBuilder.length() > 0) {
                stringBuilder.append(TimeUtils.DIVIDER);
            }
            stringBuilder.append(genres);
        }

        String releaseYear = broadcast.getReleaseYear();
        if (!TextUtils.isEmpty(releaseYear)) {
            if(stringBuilder.length() > 0) {
                stringBuilder.append(TimeUtils.DIVIDER);
            }
            stringBuilder.append(releaseYear);
        }

        mBinding.infoRow.setText(stringBuilder);
    }

    /**
     * Display the program title.
     */
    private void updateTitle(IBroadcast broadcast) {
        if (mParentalControlManager.isRestricted(broadcast)) {
            String title = mTranslator.get(TranslationKey.CARDS_TITLE_CONTENT_LOCKED);
            mBinding.title.setText(title);
            return;
        }
        mBinding.title.setText(broadcast.getTitle());
    }

    /**
     * Display the description of the broadcast.
     */
    private void updateDescription(IBroadcast broadcast) {
        if (mParentalControlManager.isRestricted(broadcast)) {
            mBinding.description.setText(null);
            return;
        }
        mBinding.description.setText(broadcast.getDescription());
    }

    /**
     * Call this method when the onDestroy of EpgGridFragment is called.
     */
    public void destroy() {
        mParentalControlManager.removeListener(mParentalControlListener);
        mUpdateContentMarkerHandler.removeCallbacks(mUpdateContentMarkerRunnable);
        mPlaybackDetailsManager.unregisterCallback(mIPlaybackCallback);

        mImageTargetView.clearImage();
        RxUtils.disposeSilently(mRecStatusDisposable);

        mImageTargetView.resetCurrentIndex();
    }

    /**
     * Called when the parental control state changes.
     */
    private final ParentalControlManager.IParentalControlListener mParentalControlListener = new ParentalControlManager.IParentalControlListener() {
        @Override
        public void onParentalRatingChanged(ParentalRating parentalRating) {
            if (getContext() instanceof BaseActivity) {
                BaseActivity activity = (BaseActivity) getContext();
                if (activity == null || activity.isInstanceStateSaved()) {
                    // The activity is already destroyed. This listener will be removed soon. (WeakReference)
                    return;
                }
            }

            if (mSelectedBroadcast != null) {
                updateDetails(mSelectedBroadcast);
            }
        }
    };

    /**
     * Called when the content marker needs to be updated after a corresponding timestamp.
     */
    private final Runnable mUpdateContentMarkerRunnable = new Runnable() {
        @Override
        public void run() {
            if (mSelectedBroadcast != null) {
                updateContentMarkers(mSelectedBroadcast);
            }
        }
    };

    private final IPlaybackCallback mIPlaybackCallback = new IPlaybackCallback() {
        @Override
        public void onPlaybackEvent(PlaybackEvent event, PlaybackDetails details, PlaybackError error) {
            mUpdateContentMarkerHandler.removeCallbacks(mUpdateContentMarkerRunnable);
            mUpdateContentMarkerHandler.post(mUpdateContentMarkerRunnable);
        }
    };

}
