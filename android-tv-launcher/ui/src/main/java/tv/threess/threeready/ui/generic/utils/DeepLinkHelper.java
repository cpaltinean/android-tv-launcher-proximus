package tv.threess.threeready.ui.generic.utils;

import android.net.Uri;
import android.text.TextUtils;
import java.net.URI;
import java.net.URISyntaxException;
import tv.threess.threeready.api.log.Log;

/**
 * Class used for the deeplink schema implementations used at the HTML pages.
 *
 * @author Daniela Toma
 * @since 2019.07.23
 */
public class DeepLinkHelper {
    private static final String TAG = Log.tag(DeepLinkHelper.class);

    // Constant values for the name of query string parameters.
    public static final String DEEPLINK_CLOSE_APPLICATION_URL = "http://be.px.stbtvclient.deeplink";

    private static final String DEEPLINK_BASE_URL = "http://be.px.stbtvclient.deeplink";
    private static final String DEEPLINK_QUERY_PARAMETER_TYPE = "type";
    private static final String DEEPLINK_QUERY_PARAMETER_ACTION = "action";
    private static final String DEEPLINK_QUERY_PARAMETER_ID = "id";

    public static final String APP_TYPE = "app";
    public static final String ACTION_OPEN = "open";

    private String mType;
    private String mAction;
    private String mId;
    private final String mUrl;

    public DeepLinkHelper(String url) {
        mUrl = url;
    }

    /**
     * @return Type from the query string parameters.
     */
    public String getType() {
        return mType;
    }

    /**
     * @return Action from the query string parameters.
     */
    public String getAction() {
        return mAction;
    }

    /**
     * @return Id from the query string parameters.
     */
    public String getId() {
        return mId;
    }

    /**
     * Verifies if the URL could be processed to open the player.
     *
     * @return True if the URL can be used to open the player, otherwise false.
     */
    public boolean canProcessUrl() {
        try {
            Uri uri = Uri.parse(mUrl);

            String baseUrl = getBaseUrl(uri);
            if (baseUrl.contains(DEEPLINK_BASE_URL)) {
                setQueryParameters(uri);
                if (existsQueryParameters()) {
                    return true;
                } else {
                    Log.d(TAG, "Missing parameters from URL: " + mUrl + " Type: " + mType + " Action: " + mAction + " Id: " + mId);
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, "Error when processing the url " + ex);
        }
        return false;
    }

    /**
     * Gets the base Url from the specified URI.
     *
     * @param uri The URI from which will get the base Url.
     * @return The base Url.
     * @throws URISyntaxException if the given URI contains illegal characters.
     */
    private String getBaseUrl(Uri uri) throws URISyntaxException {
        return new URI(uri.getScheme(),
            uri.getAuthority(),
            uri.getPath(),
            null, // Ignore the query part of the input url
            uri.getFragment()).toString();
    }

    /**
     * Sets/assigns the values of query string parameters to variables.
     *
     * @param uri The URI from which will get the values for each query string.
     */
    private void setQueryParameters(Uri uri) {
        try {
            mType = uri.getQueryParameter(DEEPLINK_QUERY_PARAMETER_TYPE);
            mAction = uri.getQueryParameter(DEEPLINK_QUERY_PARAMETER_ACTION);
            mId = uri.getQueryParameter(DEEPLINK_QUERY_PARAMETER_ID);
        } catch (Exception ex) {
            Log.d(TAG, "Exception when getting Query String Parameters" + ex);
        }
    }

    /**
     * Checks if all query string parameters have values.
     *
     * @return True if all query string parameters have values, otherwise false.
     */
    private boolean existsQueryParameters() {
        return !TextUtils.isEmpty(mType) && !TextUtils.isEmpty(mAction) && !TextUtils.isEmpty(mId);
    }
}