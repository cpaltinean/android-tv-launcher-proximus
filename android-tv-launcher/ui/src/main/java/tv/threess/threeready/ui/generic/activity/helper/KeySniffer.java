/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.activity.helper;

import android.util.SparseArray;
import android.view.KeyEvent;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Key event sniffer that is capable of recognizing a combination of keys pressed
 *
 * @author Dan Titiu
 * @since 27.11.2014
 */
public class KeySniffer {

    private int maxLength;
    private int minLength;
    private int idx = -1;
    private final SparseArray<List<Sequence>> sequences = new SparseArray<>(4);
    private int[] buffer;

    public KeySniffer() {
        this.buffer = new int[0];
        this.maxLength = 0;
        this.minLength = Integer.MAX_VALUE;
    }

    public boolean consumeEvent(KeyEvent event) {
        if (KeyEvent.ACTION_DOWN == event.getAction()) {
            int keyCode = event.getKeyCode();
            if (KeyEvent.KEYCODE_UNKNOWN == keyCode) {
                keyCode = event.getScanCode();// Fallback to scan code for unknown keycodes
            }
            this.scanAndTrigger(keyCode);
        }
        return false;
    }

    /**
     * Should only be called from one thread (Main) because its not thread-safe.
     *
     * @param id       The unique ID of the sequence.
     * @param callback Callback to be called on match (may be the same for several sequences/IDs).
     * @param codes    The key code combination to match.
     */
    public void registerSequence(int id, Callback callback, int... codes) {
        if (codes == null || codes.length == 0) {
            throw new IllegalArgumentException("Invalid sequence codes");
        }
        final int k = codes.length;
        List<Sequence> sqs = this.sequences.get(k);
        if (sqs == null) {
            sqs = new LinkedList<>();
            this.sequences.put(k, sqs);
        }

        sqs.add(new Sequence(id, callback, codes));
        // Adjust buffer for new sequence if needed
        if (k > this.buffer.length) {
            int[] oldBuf = this.buffer;
            this.buffer = new int[k];
            System.arraycopy(oldBuf, 0, this.buffer, 0, oldBuf.length);
        }
        // Adjust bounds
        if (this.minLength > k) {
            this.minLength = k;
        }
        if (this.maxLength < k) {
            this.maxLength = k;
        }
    }

    private void dispatchMatch(Sequence sequence) {
        sequence.callback.onMatch(sequence.id);
    }

    private boolean scanAndTrigger(int keyCode) {
        // Shortcut for no registered Sequence
        if (this.sequences.size() == 0) {
            return false;
        }
        this.buffer[++this.idx] = keyCode;
        final int s = this.idx + 1;
        if (s < this.minLength) {
            return false;// Not enough keys (yet)
        }

        // Match only current length as it has to be an exact match
        List<Sequence> sqs = this.sequences.get(s);
        if (sqs != null && !sqs.isEmpty()) {
            for (Sequence sq : sqs) {
                boolean match = true;
                for (int x = 0; x < s; x++) {
                    if (sq.keys[x] != this.buffer[x]) {
                        match = false;
                        break;
                    }
                }

                if (match) {
                    // Dispatch the match
                    this.dispatchMatch(sq);
                    // Reset the buffer
                    Arrays.fill(this.buffer, 0, this.idx, 0);
                    this.idx = -1;
                    // Never consume the KeyEvent
                    return false;
                }
            }
        }

        // No exact match, discard oldest key when maxLength reached
        if (s >= this.maxLength) {
            System.arraycopy(this.buffer, 1, this.buffer, 0, s - 1);
            this.buffer[this.idx] = 0;// Remove newest key copy
            --this.idx;
        }
        return false;
    }

    private static class Sequence {
        public final int id;
        final Callback callback;
        final int[] keys;

        Sequence(int id, Callback callback, int[] keys) {
            this.id = id;
            this.callback = callback;
            this.keys = keys;
        }
    }

    public interface Callback {
        void onMatch(int id);
    }
}
