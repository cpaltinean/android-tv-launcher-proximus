/*
 *  Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.IntProperty;
import android.util.Property;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import tv.threess.threeready.ui.R;

/**
 * Custom relative layout to animate layout size changes.
 * The layout size can be animate only between the defined min. and max value.
 *
 * @author Barabas Attila
 * @since 2017.04.29
 */
public class ResizableCardView extends RelativeLayout {

    private int mMaxHorizontalPadding;
    private int mMaxVerticalPadding;

    private int mCardWidth;
    private int mCardHeight;
    private int mHorizontalPadding;
    private int mVerticalPadding;

    public ResizableCardView(Context context) {
        this(context, null);
    }

    public ResizableCardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ResizableCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ResizableCardView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        // Read custom attributes
        TypedArray cardTypeArray = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.ResizableCardView, defStyleAttr, defStyleRes);

        // Read android attributes
        int[] viewAttrsArray = new int[]{
                android.R.attr.layout_width, // 0
                android.R.attr.layout_height // 1
        };
        TypedArray viewTypeArray = context.obtainStyledAttributes(attrs, viewAttrsArray);


        try {
            mCardWidth = viewTypeArray.getDimensionPixelOffset(0, ViewGroup.LayoutParams.WRAP_CONTENT);
            mCardHeight = viewTypeArray.getDimensionPixelOffset(1, ViewGroup.LayoutParams.WRAP_CONTENT);
            mMaxHorizontalPadding = cardTypeArray.getDimensionPixelOffset(R.styleable.ResizableCardView_maxHorizontalPadding, 0);
            mMaxVerticalPadding = cardTypeArray.getDimensionPixelOffset(R.styleable.ResizableCardView_maxVerticalPadding, 0);
        } finally {
            cardTypeArray.recycle();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        setVerticalPadding(getMaxVerticalPadding());
        setHorizontalPadding(getMaxHorizontalPadding());
    }

    /**
     * @param maxHorizontalPadding The horizontal padding of the card when it's not scaled.
     */
    public void setMaxHorizontalPadding(int maxHorizontalPadding) {
        mMaxHorizontalPadding = maxHorizontalPadding;
    }

    /**
     * @param maxVerticalPadding The vertical padding of the card when it's not scaled.
     */
    public void setMaxVerticalPadding(int maxVerticalPadding) {
        mMaxVerticalPadding = maxVerticalPadding;
    }

    /**
     * Get the current card height.
     */
    public int getCardHeight() {
        return mCardHeight;
    }

    /**
     * Set height for the card between the min. and max. values.
     */
    public void setCardHeight(int height) {
        if (mCardHeight != height
                && getLayoutParams() != null) {
            getLayoutParams().height = height;
            requestLayout();
            mCardHeight = height;
        }
    }

    /**
     * Get the current card width.
     */
    public int getCardWidth() {
        return mCardWidth;
    }

    /**
     * Set width for the card between the min. and max. values.
     */
    public void setCardWidth(int width) {
        if (mCardWidth != width
                && getLayoutParams() != null) {
            getLayoutParams().width = width;
            mCardWidth = width;
            requestLayout();
        }
    }

    /**
     * Get the horizontal padding of card.
     */
    public int getHorizontalPadding() {
        return mHorizontalPadding;
    }

    /**
     * Set horizontal padding for the card.
     */
    public void setHorizontalPadding(final int horizontalPadding) {
        if (mHorizontalPadding != horizontalPadding) {
            mHorizontalPadding = horizontalPadding;
            setPadding(horizontalPadding, getPaddingTop(), horizontalPadding, getPaddingBottom());
        }
    }

    /**
     * Get the vertical padding of card.
     */
    public int getVerticalPadding() {
        return mVerticalPadding;
    }

    /**
     * Set vertical padding for the card.
     */
    public void setVerticalPadding(final int verticalPadding) {
        if (mVerticalPadding != verticalPadding) {
            mVerticalPadding = verticalPadding;
            setPadding(getPaddingLeft(), verticalPadding, getPaddingRight(), verticalPadding);
        }
    }

    /**
     * Get the maximum horizontal padding of card.
     */
    public int getMaxHorizontalPadding() {
        return mMaxHorizontalPadding;
    }

    /**
     * Get the maximum vertical padding of card.
     */
    public int getMaxVerticalPadding() {
        return mMaxVerticalPadding;
    }


    /**
     * A Property wrapper around the vertical padding functionality.
     * Used to animate vertical padding changes.
     */
    public static final Property<ResizableCardView, Integer> VERTICAL_PADDING =
            new IntProperty<ResizableCardView>("vertical_padding") {

                @Override
                public Integer get(ResizableCardView resizableCardView) {
                    return resizableCardView.getVerticalPadding();
                }

                @Override
                public void setValue(ResizableCardView resizableCardView, int verticalPadding) {
                    resizableCardView.setVerticalPadding(verticalPadding);
                }
            };

    /**
     * A Property wrapper around the horizontal padding functionality.
     * Used to animate horizontal padding changes.
     */
    public static final Property<ResizableCardView, Integer> HORIZONTAL_PADDING =
            new IntProperty<ResizableCardView>("horizontal_padding") {

                @Override
                public Integer get(ResizableCardView resizableCardView) {
                    return resizableCardView.getHorizontalPadding();
                }

                @Override
                public void setValue(ResizableCardView resizableCardView, int horizontalPadding) {
                    resizableCardView.setHorizontalPadding(horizontalPadding);
                }
            };


    /**
     * A Property wrapper around the width functionality.
     * Used to animate layout height changes.
     */
    public static final Property<ResizableCardView, Integer> CARD_HEIGHT =
            new IntProperty<ResizableCardView>("card_height") {

                @Override
                public Integer get(ResizableCardView resizableCardView) {
                    return resizableCardView.getCardHeight();
                }

                @Override
                public void setValue(ResizableCardView resizableCardView, int height) {
                    resizableCardView.setCardHeight(height);
                }
            };

    /**
     * A Property wrapper around the width functionality.
     * Used to animate layout width changes.
     */
    public static final Property<ResizableCardView, Integer> CARD_WIDTH =
            new IntProperty<ResizableCardView>("card_width") {

                @Override
                public Integer get(ResizableCardView resizableCardView) {
                    return resizableCardView.getCardWidth();
                }

                @Override
                public void setValue(ResizableCardView resizableCardView, int width) {
                    resizableCardView.setCardWidth(width);
                }
            };
}
