package tv.threess.threeready.ui.generic.player.plugin;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.command.StopCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.plugin.PlaybackPlugin;
import tv.threess.threeready.player.result.EventResult;
import tv.threess.threeready.ui.generic.utils.RxUtils;


/**
 * Playback Plugin used for request the next replay when it's needed, store it and update the playback details when the current replay program ends.
 *
 * @author David Bondor
 * @since 2020.02.03
 */
public class ReplayPlaylistPlugin implements PlaybackPlugin {
    private static final String TAG = Log.tag(ReplayPlaylistPlugin.class);

    private final Handler mHandler = new Handler(Looper.getMainLooper());

    private Disposable mNextBroadcastDisposable;
    private Disposable mPreviousBroadcastDisposable;

    private final List<IBroadcast> mBroadcastList = new CopyOnWriteArrayList<>();

    private final PlaybackDetailsManager mPlaybackManager = Components.get(PlaybackDetailsManager.class);
    private final TvRepository mTvRepository = Components.get(TvRepository.class);

    @Override
    public boolean isSubscribed(PlaybackDomain domain) {
        return domain == PlaybackDomain.Replay;
    }

    @Override
    public void onOfferCommand(PlaybackCommand command) {
        // Cancel replay append.
        if (command instanceof StartCommand || command instanceof StopCommand) {
            RxUtils.disposeSilently(mNextBroadcastDisposable, mPreviousBroadcastDisposable);
            mBroadcastList.clear();
            mHandler.removeCallbacksAndMessages(null);
        }

        // Update broadcast list.
        if (command instanceof StartCommand && command.getType() == PlaybackType.Replay) {
            IBroadcast currentBroadcast = mPlaybackManager.getReplayPlayerData();
            if (currentBroadcast != null) {
                mBroadcastList.add(currentBroadcast);
                mPlaybackManager.updatePlayerData(mBroadcastList);
            }
        }
    }

    @Override
    public void onEvent(EventResult result, Bundle details) {
        switch (result.event) {
            default:
                return;

            case PrependPlaylist:
                offerPreviousReplay();
                break;

            case UpdatePlayList:
                mPlaybackManager.updateReplayPlaylist();
                break;

            case AppendPlaylist:
                offerNextReplay();
                break;

            case PlaybackItemChanged:
                IBroadcast broadcast = mPlaybackManager.getBroadcastPlayerData(
                        mPlaybackManager.getPosition(System.currentTimeMillis()));

                Log.d(TAG, "PlaybackItemChanged. New item : " + broadcast);

                // Filter blacklisted items.
                mPlaybackManager.updatePlayerData(mBroadcastList);
                mPlaybackManager.switchCurrentProgram();
                break;
        }
    }

    @Override
    public void onDestroy() {
        RxUtils.disposeSilently(mNextBroadcastDisposable, mPreviousBroadcastDisposable);
        mHandler.removeCallbacksAndMessages(null);
    }

    /**
     * Method used for get and append the next replay to the playlist.
     */
    private void offerNextReplay() {
        Log.d(TAG, "Next replay");
        if (mBroadcastList.isEmpty()) {
            Log.e(TAG, "Cannot append replay playlist. No current broadcast available.");
            return;
        }

        IBroadcast lastBroadcast = mBroadcastList.get(mBroadcastList.size() - 1);
        RxUtils.disposeSilently(mNextBroadcastDisposable);
        mNextBroadcastDisposable = mTvRepository.getNextBroadcast(lastBroadcast)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(nextBroadcast -> {
                            Log.d(TAG, "The next broadcast: " + nextBroadcast);
                            for (int i = 0; i < mBroadcastList.size(); ++i) {
                                // Check if we should update the Dummy Broadcast
                                if (mBroadcastList.get(i).getStart() == nextBroadcast.getStart()) {
                                    mBroadcastList.set(i, nextBroadcast);
                                    break;
                                }

                                if (i == mBroadcastList.size() - 1) {
                                    mBroadcastList.add(nextBroadcast);
                                    break;
                                }
                            }
                            mPlaybackManager.updatePlayerData(mBroadcastList);
                            mPlaybackManager.appendNextReplay(nextBroadcast);
                        },
                        throwable -> Log.e(TAG, "Couldn't load the next replay.", throwable));
    }

    /**
     * Method used for get and prepend the previous replay to the playlist.
     */
    private void offerPreviousReplay() {
        if (mBroadcastList.isEmpty()) {
            Log.e(TAG, "Cannot prepend replay playlist. No current broadcast available.");
            return;
        }

        IBroadcast firstBroadcast = mBroadcastList.get(0);
        RxUtils.disposeSilently(mPreviousBroadcastDisposable);
        mPreviousBroadcastDisposable = mTvRepository.getPreviousBroadcast(firstBroadcast)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(previousBroadcast -> {
                            Log.d(TAG, "The previous broadcast: " + previousBroadcast);
                            for (int i = 0; i < mBroadcastList.size(); ++i) {
                                // Check if we should update the Dummy Broadcast
                                if (mBroadcastList.get(i).getEnd() == previousBroadcast.getEnd()) {
                                    mBroadcastList.set(i, previousBroadcast);
                                    break;
                                }

                                if (i == mBroadcastList.size() - 1) {
                                    mBroadcastList.add(0, previousBroadcast);
                                    break;
                                }
                            }

                            mPlaybackManager.updatePlayerData(mBroadcastList);
                            mPlaybackManager.prependPreviousReplay(previousBroadcast);
                        },
                        throwable -> Log.e(TAG, "Couldn't load the previous replay.", throwable));
    }
}