/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.resolver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.gms.GmsRepository;
import tv.threess.threeready.api.home.HomeRepository;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.vod.VodRepository;

/**
 * Map the data source of the module to an RX observable which will emit the data.
 *
 * Created by Barabas Attila on 4/12/2017.
 */
public class DataSourceResolver implements Component {
    private static final String TAG = Log.tag(DataSourceResolver.class);

    private final ConditionResolver mConditionResolver = Components.get(ConditionResolver.class);
    private final HomeRepository mHomeRepository = Components.get(HomeRepository.class);
    private final VodRepository mVodRepository = Components.get(VodRepository.class);
    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);
    private final GmsRepository mGmsRepository = Components.get(GmsRepository.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    public Observable<ModuleData<Object>> resolve(ModuleConfig moduleConfig, int start, int count,
                                             List<DataSourceSelector> dataSourceSelectors) {
        return Observable.defer(() -> {
            if (mConditionResolver.resolveConditions(moduleConfig.getConditions())) {
                // filtered by module conditions
                if (moduleConfig.getDataSource() != null) {
                    return (ObservableSource<ModuleData<Object>>) resolveDataSources(
                            moduleConfig.getDataSource().getMethod(),
                            moduleConfig, start, count, dataSourceSelectors);
                }
            }
            return Observable.just(new ModuleData<>(moduleConfig, new ArrayList<>()));
        }).map(resultData -> {
            if (!mConditionResolver.resolveConditions(moduleConfig.getConditions())) {
                return resultData;
            }

            List<Object> retList = new ArrayList<>(resultData.getItems());

            // Trim the list.
            int limit = resultData.getPageSize(count);
            if (limit < retList.size()) {
                retList = retList.subList(0, limit);
            }

            for (EditorialItem item : ArrayUtils.notNull(moduleConfig.getEditorialItems())) {
                if (!mConditionResolver.resolveConditions(item.getConditions())) {
                    // filtered by item conditions
                    continue;
                }

                // No data source, display all editorial.
                if (moduleConfig.getDataSource() == null) {
                    addEditorialItem(retList, item, start);

                } else if (item.getMinItemCount() == null && item.getPosition() == null) {
                    if (count <= retList.size() && resultData.hasMoreItems()) {
                        // We have more items than the limit, put the view all card to the end
                        retList.add(item);
                    }
                } else {
                    if (item.getMinItemCount() != null
                            && item.getMinItemCount() > retList.size()) {
                        // Minimum item count not reached.
                        continue;
                    }

                    addEditorialItem(retList, item, start);
                }
            }

            return new ModuleData<>(moduleConfig, retList, resultData);
        }).timeout(Observable.empty().delay(mAppConfig.getModuleDataSourceTimeout(TimeUnit.SECONDS), TimeUnit.SECONDS), o -> Observable.never());
    }

    private void addEditorialItem(List<Object> items, EditorialItem item, int start) {
        if (item.getPosition() != null) {
            // Put the editorial to the expected position.
            addEditorialToPosition(items, item, start, item.getPosition());
        } else {
            // Add it to the end.
            items.add(item);
        }
    }

    /**
     * Add the editorial item to the expected position in the result list.
     */
    private void addEditorialToPosition(List<Object> retList,
                                        EditorialItem item, int start, int position) {
        // Item is in the page. Add it to the correct position.
        if (position >= start
                && position < start + retList.size()) {
            retList.add(position - start, item);
        }
    }

    protected Observable<?> resolveDataSources(ModuleDataSourceMethod method,
                                               ModuleConfig moduleConfig, int start, int count,
                                               List<DataSourceSelector> dataSourceSelectors) {
        switch (method) {
            default:
                Log.e(TAG, "Can't resolve data source. Module : "
                        + moduleConfig.getTitle() + ", data source : " + moduleConfig.getDataSource());
                return Observable.just(new ModuleData<>(moduleConfig, new ArrayList<>()));

            case TV_PROGRAM_INDEX:
                return mHomeRepository.getCategoryContent(moduleConfig, start, count, dataSourceSelectors);

            case TV_PROGRAM_SEARCH:
                return mHomeRepository.getProgramSearchResults(moduleConfig, start, count);

            case TV_CHANNEL_INDEX:
                return mHomeRepository.getChannels(moduleConfig, count);

            case TV_CHANNEL_SEARCH:
                return mHomeRepository.getChannelSearchResults(moduleConfig, start, count);

            case REPLAY_CHANNEL_PAGE_ITEMS:
                return mHomeRepository.getReplayChannelPageItems(moduleConfig, start, count, dataSourceSelectors);

            case TV_PROGRAM_NOW_NEXT:
                return mHomeRepository.getNowNext(moduleConfig, count, dataSourceSelectors);

            case TV_PROGRAM_NOW:
                return mHomeRepository.getNowOnTV(moduleConfig, count);

            case CONTINUE_WATCHING:
                return mHomeRepository.getContinueWatching(moduleConfig, start, count);

            case WATCHLIST:
                return mHomeRepository.getWatchlist(moduleConfig, start, count);

            case USER_RENTALS:
                return mVodRepository.getUserRentals(moduleConfig, start, count);

            case COMPLETED_RECORDING:
                return mPvrRepository.getRecordedCategoryContent(moduleConfig, start, count);

            case SCHEDULED_RECORDING:
                return mPvrRepository.getScheduledCategoryContent(moduleConfig, start, count);

            case NETFLIX:
                return mHomeRepository.getNetflixGroups(moduleConfig, count);

            case ANDROID_TV_APPS:
                return mGmsRepository.getApps(moduleConfig);

            case ANDROID_TV_NOTIFICATIONS:
                return mGmsRepository.getSystemNotifications(moduleConfig);
        }
    }

}
