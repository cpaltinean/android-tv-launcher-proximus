package tv.threess.threeready.ui.generic.player.plugin;

import android.os.Handler;
import android.os.Looper;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.ScanReceiver;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.command.StopCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.plugin.PlaybackPlugin;
import tv.threess.threeready.player.result.Result;

import java.util.concurrent.TimeUnit;

/**
 * Plugin class used for retry the tune in case it failed while a scan was in progress.
 */
public class IpTvScanRetryPlugin implements PlaybackPlugin, ScanReceiver.Listener {
    private static final String TAG = Log.tag(IpTvScanRetryPlugin.class);

    private static final long SCAN_RETRY_TIMEOUT = TimeUnit.SECONDS.toMillis(30);

    private volatile boolean mTimeoutEnabled = true;
    private final Handler mTimeoutHandler = new Handler(Looper.getMainLooper());
    private final Runnable onTimeoutRunnable = this::executeScanRetry;

    private final ScanReceiver mScanReceiver = Components.get(ScanReceiver.class);
    private final PlaybackDetailsManager mPlaybackManager;

    public IpTvScanRetryPlugin(PlaybackDetailsManager playbackManager) {
        mPlaybackManager = playbackManager;
    }

    @Override
    public boolean isSubscribed(PlaybackDomain domain) {
        return true;
    }

    @Override
    public void onDestroy() {
        disableScanRetry();
    }

    @Override
    public void onOfferCommand(PlaybackCommand command) throws PlaybackError {
        PlaybackPlugin.super.onOfferCommand(command);
        if (command instanceof StartCommand) {
            disableScanRetry();
        }
    }

    @Override
    public void onResult(PlaybackCommand command, Result result) {
        PlaybackPlugin.super.onResult(command, result);
        if (command instanceof StopCommand) {
            disableScanRetry();
        }
        else if (command instanceof StartCommand) {
            if (PlaybackError.Type.TUNING_WHILE_SCANNING.matches(result)) {
                // tuned while scanning, enable the retry when scan finishes or 30 seconds
                enableScanRetry();
            }
            else if (result.isFavorable()) {
                // playback started, enable next timeout
                mTimeoutEnabled = true;
            }
        }
    }

    @Override
    public void onScanStateChanged(boolean inProgress) {
        if (inProgress) {
            Log.d(TAG, "Start scan do not re-tune");
            return;
        }
        executeScanRetry();
    }

    /**
     * enable the tune retry when the channel scan finishes
     */
    private void enableScanRetry() {
        mScanReceiver.addListener(this);
        mTimeoutHandler.removeCallbacks(onTimeoutRunnable);
        if (mTimeoutEnabled) {
            mTimeoutHandler.postDelayed(onTimeoutRunnable, SCAN_RETRY_TIMEOUT);
        }
    }

    /**
     * disable the tune retry when the channel scan finishes
     */
    private void disableScanRetry() {
        mScanReceiver.removeListener(this);
        mTimeoutHandler.removeCallbacks(onTimeoutRunnable);
    }

    /**
     * execute the tune retry when the channel scan finishes or timeouts
     */
    public void executeScanRetry() {
        Log.d(TAG, "Scan finished or timeouts, re-tune");
        disableScanRetry();
        mTimeoutEnabled = false;
        mPlaybackManager.reTryLastTuned(false);
    }
}
