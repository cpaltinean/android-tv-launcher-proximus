package tv.threess.threeready.ui.details.fragment;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.HorizontalGridView;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.model.PlayOption;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.OptionsDialogBinding;
import tv.threess.threeready.ui.details.presenter.BroadcastPlayOptionsCardPresenter;
import tv.threess.threeready.ui.details.presenter.BroadcastPlayOptionsMetadataPresenter;
import tv.threess.threeready.ui.details.presenter.RecordingPlayOptionsCardPresenter;
import tv.threess.threeready.ui.details.presenter.VodPlayOptionsCardPresenter;
import tv.threess.threeready.ui.details.presenter.VodPlayOptionsMetadataPresenter;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeLabel;
import tv.threess.threeready.ui.generic.dialog.BaseDialogFragment;
import tv.threess.threeready.ui.generic.model.BroadcastPlayOption;
import tv.threess.threeready.ui.generic.model.RecordingPlayOption;
import tv.threess.threeready.ui.generic.model.VodPlayOption;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Class for the play options dialog.
 *
 * @author Andrei Teslovan
 * @since 2018.11.28
 */
public class PlayOptionsDialog extends BaseDialogFragment {
    private static final String TAG = Log.tag(PlayOptionsDialog.class);

    private static final String EXTRA_PLAY_OPTIONS = "EXTRA_PLAY_OPTIONS";
    private static final String EXTRA_CONTINUE_TO_WATCH = "EXTRA_CONTINUE_TO_WATCH";
    private static final String EXTRA_CHANNEL_IS_FAVORITE = "EXTRA_CHANNEL_IS_FAVORITE";
    public static final String EXTRA_CONTENT_ITEM = "EXTRA_CONTENT_ITEM";

    private OptionsDialogBinding mBinding;

    private final Translator mTranslator = Components.get(Translator.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    private PresenterSelector mMetadataPresenterSelector;
    private PresenterSelector mCardPresenterSelector;

    private final ArrayObjectAdapter<PlayOption> mCardAdapter = new ArrayObjectAdapter<>();

    private ArrayObjectAdapter<PlayOption> mMetadataAdapter;

    private List<PlayOption> mPlayOptions = new ArrayList<>();

    private IContentItem mContentItem;

    public static PlayOptionsDialog newInstance(List<? extends PlayOption> playOptions) {

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_PLAY_OPTIONS, new ArrayList<>(playOptions));

        PlayOptionsDialog fragment = new PlayOptionsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public static PlayOptionsDialog newInstance(List<? extends PlayOption> playOptions,
                                                boolean continueWatch, boolean isFromFavourite,
                                                IContentItem contentItem) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CONTENT_ITEM, contentItem);
        args.putBoolean(EXTRA_CONTINUE_TO_WATCH, continueWatch);
        args.putSerializable(EXTRA_PLAY_OPTIONS, new ArrayList<>(playOptions));
        args.putBoolean(EXTRA_CHANNEL_IS_FAVORITE, isFromFavourite);

        PlayOptionsDialog fragment = new PlayOptionsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPlayOptions = getSerializableArgument(EXTRA_PLAY_OPTIONS);
        mContentItem = getSerializableArgument(EXTRA_CONTENT_ITEM);

        boolean continueToWatch = getBooleanArgument(EXTRA_CONTINUE_TO_WATCH);
        boolean fromFavourite = getBooleanArgument(EXTRA_CHANNEL_IS_FAVORITE);

        // use this consumer for updating the play options card content description (for TalkBack)
        Consumer<String> playOptionProvider = description -> {
            View view = mBinding.labeledStripeGrid.findFocus();
            if (view != null) {
                view.setContentDescription(description);
                view.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED);
            }
        };

        // Presenter selector for the playback option meta info.
        mCardPresenterSelector = new InterfacePresenterSelector()
                .addClassPresenter(BroadcastPlayOption.class, new BroadcastPlayOptionsCardPresenter<>(getContext(), fromFavourite))
                .addClassPresenter(RecordingPlayOption.class, new RecordingPlayOptionsCardPresenter(getContext(), fromFavourite))
                .addClassPresenter(VodPlayOption.class, new VodPlayOptionsCardPresenter(getContext()));
        mMetadataPresenterSelector = new InterfacePresenterSelector()
                .addClassPresenter(BroadcastPlayOption.class, new BroadcastPlayOptionsMetadataPresenter(getContext(), playOptionProvider, continueToWatch))
                .addClassPresenter(VodPlayOption.class, new VodPlayOptionsMetadataPresenter(getContext(), playOptionProvider));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = OptionsDialogBinding.inflate(inflater, container, false);

        setCardAdapter();

        int horizontalPadding = getResources().getDimensionPixelSize(R.dimen.stripe_title_left_margin);
        int topPadding = getResources().getDimensionPixelSize(R.dimen.play_options_screen_stripe_top_padding);
        mBinding.labeledStripeGrid.setGridPadding(horizontalPadding, topPadding, horizontalPadding, 0);

        mBinding.labeledStripeGrid.setWindowAlignmentOffsetPercent(0);
        mBinding.labeledStripeGrid.setWindowAlignmentOffset(getResources().getDimensionPixelOffset(R.dimen.play_options_screen_window_alignment_offset));
        mBinding.labeledStripeGrid.setWindowAlignment(HorizontalGridView.WINDOW_ALIGN_HIGH_EDGE);
        mBinding.labeledStripeGrid.setItemSpacing(getResources().getDimensionPixelSize(R.dimen.play_options_screen_card_spacing));
        mBinding.labeledStripeGrid.addOnChildViewHolderSelectedListener(mOnChildViewHolderSelectedListener);

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateHeaderTitle();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mBinding.labeledStripeGrid.setAdapter(null, null);
        mBinding.metadataGridView.setAdapter(null);
    }

    /**
     * Block navigation on meta data.
     *
     * @param event key event
     * @return in case of up or down of dpad true, otherwise super call
     */
    @Override
    public boolean onKeyDown(KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_DPAD_DOWN:
            case KeyEvent.KEYCODE_DPAD_UP:
                return true;
        }
        return super.onKeyDown(event);
    }

    protected void updateHeaderTitle() {
        mBinding.headerTitle.setText(mTranslator.get(TranslationKey.SCREEN_CHOOSE_PLAYBACK_OPTION));
        mBinding.headerTitle.setTextColor(mLayoutConfig.getFontColor());
    }

    /**
     * Method called to set the adapter for the LabeledStripeView.
     */
    private void setCardAdapter() {
        Log.d(TAG, "setCardAdapter() called");

        int subscriptionItemsSize = 0;
        List<StripeLabel> labels = new ArrayList<>();

        // Create label adapter.
        for (PlayOption playOption : mPlayOptions) {
            if (playOption instanceof VodPlayOption) {
                mCardAdapter.add(playOption);
                if (playOption.getType() == PlayOption.Type.INCLUDED) {
                    subscriptionItemsSize++;
                } else {
                    labels.add(new StripeLabel(labels.size(), mCardAdapter.size(),
                            getLabelForBroadcast(playOption), null));
                }
            } else {
                mCardAdapter.add(playOption);
                labels.add(new StripeLabel(labels.size(), mCardAdapter.size(),
                        getLabelForBroadcast(playOption), null));
            }
        }

        // Add labels.
        if (subscriptionItemsSize != 0) {
            labels.add(new StripeLabel(0,
                    subscriptionItemsSize, mTranslator.get(
                    TranslationKey.SCREEN_CHOOSE_PLAYBACK_OPTION_SECTION_HEADER_INCLUDED), null));
            labels.add(new StripeLabel(subscriptionItemsSize,
                    mCardAdapter.size(), mTranslator.get(
                    TranslationKey.SCREEN_CHOOSE_PLAYBACK_OPTION_SECTION_HEADER_RENTED), null));
        }

        ItemBridgeAdapter adapter = new ItemBridgeAdapter(mCardAdapter, mCardPresenterSelector);
        mBinding.labeledStripeGrid.setAdapter(adapter, () -> labels);
    }

    /**
     * Method called to set the adapter for the HorizontalGridView.
     */
    private void updateMetaInfo(PlayOption playOption) {
        Log.d(TAG, "updateMetaInfo() called");

        if (mMetadataAdapter == null) {
            mMetadataAdapter = new ArrayObjectAdapter<>();
            mMetadataAdapter.setHasStableIds(true);
            mMetadataAdapter.add(playOption);
            ItemBridgeAdapter adapter = new ItemBridgeAdapter(mMetadataAdapter, mMetadataPresenterSelector);
            mBinding.metadataGridView.setAdapter(adapter);
        } else {
            mMetadataAdapter.replaceAll(Collections.singletonList(playOption));
        }
    }

    private String getLabelForBroadcast(PlayOption playOption) {
        switch (playOption.getType()) {
            case LIVE:
                return mTranslator.get(TranslationKey.SCREEN_CHOOSE_PLAYBACK_OPTION_SECTION_HEADER_LIVE);
            case RECORDING:
                return mTranslator.get(TranslationKey.SCREEN_CHOOSE_PLAYBACK_OPTION_SECTION_HEADER_RECORDING);
            case REPLAY:
                return mTranslator.get(TranslationKey.SCREEN_CHOOSE_PLAYBACK_OPTION_SECTION_HEADER_REPLAY);
            case RENTED:
                return mTranslator.get(TranslationKey.SCREEN_CHOOSE_PLAYBACK_OPTION_SECTION_HEADER_RENTED);
            default:
                return "";
        }
    }

    private final OnChildViewHolderSelectedListener mOnChildViewHolderSelectedListener = new OnChildViewHolderSelectedListener() {
        @Override
        public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
            super.onChildViewHolderSelected(parent, child, position, subposition);
            Log.d(TAG, "Selected position changed : " + position);
            updateMetaInfo(mCardAdapter.get(position));
            mBinding.labeledStripeGrid.setSelectedLabel(position);
        }
    };

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        if (mBinding.labeledStripeGrid.getAdapter() == null || mContentItem == null) {
            return null;
        }

        return ReportUtils.createEvent(ReportUtils.getPage(mContentItem), mContentItem);
    }
}
