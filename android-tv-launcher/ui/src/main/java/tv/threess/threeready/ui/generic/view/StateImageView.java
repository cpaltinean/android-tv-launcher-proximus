package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;


/**
 * Base custom image view which can show different images for states.
 *
 * @author Eugen Guzyk
 * @since 2018.07.11
 */
public abstract class StateImageView extends androidx.appcompat.widget.AppCompatImageView {

    public StateImageView(Context context) {
        super(context);
        initView();
    }

    public StateImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public StateImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        setVisibility(GONE);
    }

    public void setImage(Drawable imageDrawable) {
        setImageDrawable(imageDrawable);
        setVisibility(VISIBLE);
    }

    public void setImage(int imageResource) {
        setImageResource(imageResource);
        setVisibility(VISIBLE);
    }

    public void resetImage() {
        setImageResource(0);
        setVisibility(GONE);
    }

    public void hide() {
        setVisibility(GONE);
    }

}
