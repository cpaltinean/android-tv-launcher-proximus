package tv.threess.threeready.ui.settings.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.leanback.widget.VerticalGridView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.playback.model.TrackInfo;
import tv.threess.threeready.ui.databinding.LanguageSubscreenLayoutBinding;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.settings.model.ISettingsItem;
import tv.threess.threeready.ui.settings.model.MediaTrackRadioItem;
import tv.threess.threeready.ui.settings.model.RadioButtonItem;

/**
 * Fragment displaying the language preferences screen: Audio & Subtitles screen and Shop language screen.
 * Based on the argument given through intent we display the settings for both Audio & settings or for the Shop languages settings.
 * The Audio & Subtitles settings are shown based on the {@link Settings} audioLanguage & subtitlesLanguage persisted preferences.
 * The available options are French, Dutch, German and English languages for both audio & subtitles options.
 * <p>
 * The shop language is persisted in cc and we get/set it in order to display/send it further to cc.
 * The available shop languages are French, Dutch and French & Dutch
 *
 * @author Denisa Trif
 * @since 30.04.2019
 */
public class LanguagePreferencesSubscreensFragment extends BaseSettingsDialogFragment<Object> {
    private static final String TAG = Log.tag(LanguagePreferencesSubscreensFragment.class);

    private static final String ARG_LANG_PREFERENCE_SUBSCREEN = "ARG_LANG_PREFERENCE_SUBSCREEN";

    private SubScreen mSubScreen;

    private Disposable mGetLanguageDisposable;

    private Disposable mSetLanguageDisposable;

    private Disposable mSetSubLanguageDisposable;
    private Disposable mSetAudioLanguageDisposable;
    private RadioButtonItem defaultShopLanguage;

    List<Object> mItems = new ArrayList<>();

    private LanguageSubscreenLayoutBinding mBinding;

    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);

    public enum SubScreen {
        AUDIO_SUBTITLES,
        SHOP_LANGUAGE
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubScreen = SubScreen.valueOf(getStringArgument(ARG_LANG_PREFERENCE_SUBSCREEN));
    }

    public LanguagePreferencesSubscreensFragment newInstance(SubScreen screen) {
        Bundle args = new Bundle();
        args.putString(ARG_LANG_PREFERENCE_SUBSCREEN, screen.name());

        LanguagePreferencesSubscreensFragment fragment = new LanguagePreferencesSubscreensFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    protected View inflateLayout() {
        if (mBinding == null) {
            mBinding = LanguageSubscreenLayoutBinding.inflate(LayoutInflater.from(getContext()));
        }

        return mBinding.getRoot();
    }

    @NonNull
    protected View getRightFrame() {
        return mBinding.rightFrame;
    }

    protected VerticalGridView getVerticalGridView() {
        return mBinding.settingsGrid;
    }

    protected TextView getTitleView() {
        return mBinding.title;
    }

    @Override
    protected void setTitle() {
        switch (mSubScreen) {
            case AUDIO_SUBTITLES:
                mBinding.title.setText(mTranslator.get(TranslationKey.SETTINGS_LANGUAGE_AUDIO_SUBTITLES));
                mBinding.description.setVisibility(View.VISIBLE);
                mBinding.description.setText(mTranslator.get(TranslationKey.SETTINGS_LANGUAGE_AUDIO_SUBTITLES_SUBTITLE));
                break;
            case SHOP_LANGUAGE:
                mBinding.title.setText(mTranslator.get(TranslationKey.SETTINGS_LANGUAGE_SHOP_LANGUAGE));
                break;
        }
    }

    @Override
    protected void initView() {
        switch (mSubScreen) {
            case AUDIO_SUBTITLES:
                setAudioAndSubtitlesScreen();
                break;
            case SHOP_LANGUAGE:
                setShopLanguageScreen();
                break;
        }
    }

    private void setShopLanguageScreen() {
        RxUtils.disposeSilently(mGetLanguageDisposable);
        mGetLanguageDisposable = mAccountRepository.getShopLanguage()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(languages -> {
                            RadioButtonItem french = new RadioButtonItem(mTranslator.get(TranslationKey.SETTINGS_FRENCH),
                                    LocaleUtils.FRENCH_SHOP_LANGUAGE, TranslationKey.SETTINGS_LANGUAGE_SHOP_LANGUAGE);
                            RadioButtonItem dutch = new RadioButtonItem(mTranslator.get(TranslationKey.SETTINGS_DUTCH),
                                    LocaleUtils.DUTCH_SHOP_LANGUAGE, TranslationKey.SETTINGS_LANGUAGE_SHOP_LANGUAGE);
                            RadioButtonItem dutchAndFrench = new RadioButtonItem(mTranslator.get(TranslationKey.SETTINGS_DUTCH_AND_FRENCH),
                                    LocaleUtils.FRENCH_AND_DUTCH_SHOP_LANGUAGE, TranslationKey.SETTINGS_LANGUAGE_SHOP_LANGUAGE);

                            mItems.addAll(Arrays.asList(french, dutch, dutchAndFrench));

                            if (languages.contains(",")) {
                                dutchAndFrench.setChecked(true);
                                defaultShopLanguage = dutchAndFrench;
                            } else {
                                if (languages.contains(LocaleUtils.DUTCH_SHOP_LANGUAGE)) {
                                    dutch.setChecked(true);
                                    defaultShopLanguage = dutch;
                                } else if (languages.contains(LocaleUtils.FRENCH_SHOP_LANGUAGE)) {
                                    french.setChecked(true);
                                    defaultShopLanguage = french;
                                }
                            }
                            setItems(mItems);
                        }
                );
    }

    private void setAudioAndSubtitlesScreen() {
        //add audio language options
        mItems.add(mTranslator.get(TranslationKey.SCREEN_AUDIO_SUBTITLES_AUDIO_LANGUAGE));
        mItems.addAll(generateAudioTrackItems(TranslationKey.SCREEN_AUDIO_SUBTITLES_AUDIO_LANGUAGE));

        // add subtitles language options
        mItems.add(mTranslator.get(TranslationKey.SCREEN_AUDIO_SUBTITLES_SUBTITLES));
        mItems.add(new MediaTrackRadioItem(mTranslator.get(TranslationKey.SCREEN_AUDIO_SUBTITLES_SUBTITLES_OFF),
                new TrackInfo("", false), TranslationKey.SCREEN_AUDIO_SUBTITLES_SUBTITLES));
        mItems.addAll(generateSubtitleTrackItems(TranslationKey.SCREEN_AUDIO_SUBTITLES_SUBTITLES));

        setItems(mItems);

        setDefaultsLanguageItem();
    }

    /**
     * @return list with descriptive audio.
     */
    private List<MediaTrackRadioItem> generateAudioTrackItems(String preferenceGroup) {
        return Arrays.asList(
                new MediaTrackRadioItem(mTranslator.get(TranslationKey.SETTINGS_FRENCH),
                        new TrackInfo(LocaleUtils.FRENCH_LANGUAGE_CODE, false), preferenceGroup),
                new MediaTrackRadioItem(mTranslator.get(TranslationKey.SETTINGS_DUTCH),
                        new TrackInfo(LocaleUtils.DUTCH_LANGUAGE_CODE, false), preferenceGroup),
                new MediaTrackRadioItem(mTranslator.get(TranslationKey.SETTINGS_GERMAN),
                        new TrackInfo(LocaleUtils.GERMAN_LANGUAGE_CODE, false), preferenceGroup),
                new MediaTrackRadioItem(mTranslator.get(TranslationKey.LANGUAGE_ENGLISH),
                        new TrackInfo(LocaleUtils.ENGLISH_LANGUAGE_CODE, false), preferenceGroup),

                new MediaTrackRadioItem(mTranslator.get(TranslationKey.SETTINGS_FRENCH_AD),
                        new TrackInfo(LocaleUtils.FRENCH_LANGUAGE_CODE, true), preferenceGroup),
                new MediaTrackRadioItem(mTranslator.get(TranslationKey.SETTINGS_DUTCH_AD),
                        new TrackInfo(LocaleUtils.DUTCH_LANGUAGE_CODE, true), preferenceGroup),
                new MediaTrackRadioItem(mTranslator.get(TranslationKey.SETTINGS_GERMAN_AD),
                        new TrackInfo(LocaleUtils.GERMAN_LANGUAGE_CODE, true), preferenceGroup),
                new MediaTrackRadioItem(mTranslator.get(TranslationKey.SETTINGS_ENGLISH_AD),
                        new TrackInfo(LocaleUtils.ENGLISH_LANGUAGE_CODE, true), preferenceGroup)
        );
    }

    /**
     * @return list with descriptive subtitles.
     */
    private List<MediaTrackRadioItem> generateSubtitleTrackItems(String preferenceGroup) {
        return Arrays.asList(
                new MediaTrackRadioItem(mTranslator.get(TranslationKey.SETTINGS_FRENCH),
                        new TrackInfo(LocaleUtils.FRENCH_LANGUAGE_CODE, false), preferenceGroup),
                new MediaTrackRadioItem(mTranslator.get(TranslationKey.SETTINGS_DUTCH),
                        new TrackInfo(LocaleUtils.DUTCH_LANGUAGE_CODE, false), preferenceGroup),
                new MediaTrackRadioItem(mTranslator.get(TranslationKey.SETTINGS_GERMAN),
                        new TrackInfo(LocaleUtils.GERMAN_LANGUAGE_CODE, false), preferenceGroup),
                new MediaTrackRadioItem(mTranslator.get(TranslationKey.SETTINGS_ENGLISH),
                        new TrackInfo(LocaleUtils.ENGLISH_LANGUAGE_CODE, false), preferenceGroup),

                new MediaTrackRadioItem(mTranslator.get(TranslationKey.SETTINGS_FRENCH_ST),
                        new TrackInfo(LocaleUtils.FRENCH_LANGUAGE_CODE, true), preferenceGroup),
                new MediaTrackRadioItem(mTranslator.get(TranslationKey.SETTINGS_DUTCH_ST),
                        new TrackInfo(LocaleUtils.DUTCH_LANGUAGE_CODE, true), preferenceGroup),
                new MediaTrackRadioItem(mTranslator.get(TranslationKey.SETTINGS_GERMAN_ST),
                        new TrackInfo(LocaleUtils.GERMAN_LANGUAGE_CODE, true), preferenceGroup),
                new MediaTrackRadioItem(mTranslator.get(TranslationKey.SETTINGS_ENGLISH_ST),
                        new TrackInfo(LocaleUtils.ENGLISH_LANGUAGE_CODE, true), preferenceGroup)
        );
    }

    @Override
    public void onItemClick(ISettingsItem clickedItem) {
        super.onItemClick(clickedItem);

        switch (clickedItem.getPreferenceGroup()) {
            case TranslationKey.SETTINGS_LANGUAGE_SHOP_LANGUAGE:
                RadioButtonItem radioButtonItem = (RadioButtonItem) clickedItem;
                RxUtils.disposeSilently(mSetLanguageDisposable);
                mSetLanguageDisposable =
                        mAccountRepository.setShopLanguages(radioButtonItem.getValue())
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(() -> {
                                            Log.d(TAG, "Set shopping language completed.");
                                            // Restart the activity to update the UI.
                                            mNavigator.getActivity().recreate();
                                        },
                                        e -> {
                                            Log.e(TAG, "Set shopping language failed", e);
                                            mNavigator.showFailedToSaveSettingsDialog();

                                            if (defaultShopLanguage == null) {
                                                return;
                                            }

                                            setDefaultsLanguageItem();
                                            mAdapter.notifyDataSourceChanged();
                                        });
                break;

            case TranslationKey.SCREEN_AUDIO_SUBTITLES_AUDIO_LANGUAGE:
                MediaTrackRadioItem audioRadioItem = (MediaTrackRadioItem) clickedItem;
                RxUtils.disposeSilently(mSetAudioLanguageDisposable);
                mSetAudioLanguageDisposable = mAccountRepository.setPreferredAudioLanguage(audioRadioItem.getTrackInfo())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> Log.d(TAG, "Successfully set the global audio language in the TCH settings."),
                                e -> {
                                    Log.e(TAG, "Could not set the audio language in the TCH settings.", e);
                                    mNavigator.showFailedToSaveSettingsDialog();
                                    setDefaultsLanguageItem();
                                    mAdapter.notifyDataSourceChanged();
                                }
                        );
                break;

            case TranslationKey.SCREEN_AUDIO_SUBTITLES_SUBTITLES:
                MediaTrackRadioItem subtitleRadioItem = (MediaTrackRadioItem) clickedItem;
                RxUtils.disposeSilently(mSetSubLanguageDisposable);
                mSetSubLanguageDisposable = mAccountRepository.setPreferredSubtitleLanguages(subtitleRadioItem.getTrackInfo())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> Log.d(TAG, "Successfully set the global subtitle language in the TCH settings."),
                                e -> {
                                    Log.e(TAG, "Could not set the subtitle language in the TCH settings.", e);
                                    mNavigator.showFailedToSaveSettingsDialog();
                                    setDefaultsLanguageItem();
                                    mAdapter.notifyDataSourceChanged();
                                }
                        );
                break;
        }
    }

    private void setDefaultsLanguageItem() {
        TrackInfo selectedLanguageAudio = Settings.audioLanguage.getObject(TrackInfo.class, TrackInfo::fromLanguage);
        if (selectedLanguageAudio == null) {
            selectedLanguageAudio = new TrackInfo(LocaleUtils.getApplicationLanguage(), false);
        }

        TrackInfo selectedSubtitle = Settings.subtitlesLanguage.getObject(TrackInfo.class, TrackInfo::fromLanguage);

        for (Object item : mAdapter.getItems()) {
            if (item instanceof MediaTrackRadioItem) {
                MediaTrackRadioItem mediaTrackRadioItem = (MediaTrackRadioItem) item;
                if (TranslationKey.SCREEN_AUDIO_SUBTITLES_SUBTITLES.equals(mediaTrackRadioItem.getPreferenceGroup())) {
                    mediaTrackRadioItem.setChecked(selectedSubtitle != null && selectedSubtitle.sameAs(mediaTrackRadioItem.getTrackInfo()));
                }

                if (TranslationKey.SCREEN_AUDIO_SUBTITLES_AUDIO_LANGUAGE.equals(mediaTrackRadioItem.getPreferenceGroup())) {
                    mediaTrackRadioItem.setChecked(selectedLanguageAudio.sameAs(mediaTrackRadioItem.getTrackInfo()));
                }
            }

            if (item instanceof RadioButtonItem) {
                RadioButtonItem radioButtonItem = (RadioButtonItem) item;
                if (TranslationKey.SETTINGS_LANGUAGE_SHOP_LANGUAGE.equals(radioButtonItem.getPreferenceGroup())) {
                    radioButtonItem.setChecked(defaultShopLanguage.getValue().equals(radioButtonItem.getValue()));
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RxUtils.disposeSilently(mGetLanguageDisposable, mSetLanguageDisposable, mSetAudioLanguageDisposable, mSetSubLanguageDisposable);
    }

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        UILog.Page page = UILog.Page.CatalogueLanguage;
        if (mSubScreen != null && mSubScreen.equals(SubScreen.AUDIO_SUBTITLES)) {
            page = UILog.Page.GeneralLanguageSubtitleListPage;
        }

        Event<UILogEvent, UILogEvent.Detail> event = new Event<>(UILogEvent.PageOpen);
        event.addDetail(UILogEvent.Detail.Page, page);

        return event;
    }
}
