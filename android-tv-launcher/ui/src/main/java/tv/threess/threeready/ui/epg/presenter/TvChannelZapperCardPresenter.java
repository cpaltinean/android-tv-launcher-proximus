/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.epg.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.leanback.widget.Presenter;

import com.bumptech.glide.Glide;

import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.ChannelZapperCardBinding;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.utils.TitleFallbackImageViewTarget;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Presenter used to display channels in Mini-EPG.
 *
 * @author Barabas Attila
 * @since 2017.04.26
 */
public class TvChannelZapperCardPresenter extends BaseCardPresenter<TvChannelZapperCardPresenter.ViewHolder, TvChannel> {
    private static final String TAG = Log.tag(TvChannelZapperCardPresenter.class);

    private static final int MAX_RECYCLER_VIEW_COUNT = 10;

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    private final TvRepository mTvRepository = Components.get(TvRepository.class);

    public TvChannelZapperCardPresenter(Context context) {
        super(context);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        ChannelZapperCardBinding binding = ChannelZapperCardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        ViewHolder holder = new ViewHolder(binding);
        holder.view.setBackgroundColor(mLayoutConfig.getPlaceHolderColor());
        holder.mBinding.number.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBinding.logoText.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBinding.overlay.setForeground(UiUtils.createFavoriteCardDrawable(
                parent.getContext().getResources().getColor(R.color.fast_scrolling_card_enabled, null),
                parent.getContext().getResources().getColor(R.color.fast_scrolling_card_default, null)));
        return holder;
    }

    @Override
    public int getMaxRecyclerViewCount() {
        return MAX_RECYCLER_VIEW_COUNT;
    }

    @Override
    public void onBindHolder(final ViewHolder holder, final TvChannel channel) {
        super.onBindHolder(holder, channel);

        // Load channel logo
        Glide.with(mContext).clear(holder.mBinding.logo);
        Glide.with(mContext)
                .load(channel)
                .into(new TitleFallbackImageViewTarget(
                        holder.mBinding.logo,
                        holder.mBinding.logoText,
                        channel.getName()));

        updateFavoriteIcon(holder, channel);
        invalidateAvailability(holder, channel);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        holder.view.setPadding(0, 0, 0, 0);
        holder.mBinding.logoText.setVisibility(View.GONE);
        holder.view.setEnabled(true);
        holder.view.setHovered(false);

        super.onUnbindHolder(holder);
        Glide.with(mContext).clear(holder.mBinding.logo);
        holder.dispose();
    }

    public void updateFavoriteIcon(final ViewHolder holder, final TvChannel channel) {
        if (channel.isFavorite()) {
            holder.mBinding.favoriteIcon.setVisibility(View.VISIBLE);
            holder.mBinding.number.setVisibility(View.GONE);
        } else {
            holder.mBinding.number.setText(String.valueOf(channel.getNumber()));
            holder.mBinding.number.setVisibility(View.VISIBLE);
            holder.mBinding.favoriteIcon.setVisibility(View.GONE);
        }
    }

    /**
     * Grey out programs based on the channel playback options and connectivity state,.
     */
    private void invalidateAvailability(final ViewHolder holder, final TvChannel channel) {
        holder.view.setEnabled(true);
        holder.dispose();
        holder.mPlaybackOptionDisposable = mTvRepository.getPlaybackOptions(channel.getId(), false)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(playbackOptions ->
                        holder.view.setEnabled(playbackOptions != null && !playbackOptions.isEmpty()));
    }

    @Override
    public void onInternetStateChanged(ViewHolder holder, TvChannel tvChannel, boolean available) {
        super.onInternetStateChanged(holder, tvChannel, available);

        Log.d(TAG, "Internet connection is back.");
        if (available) {
            onBindHolder(holder, tvChannel);
        } else {
            invalidateAvailability(holder, tvChannel);
        }
    }

    @Override
    public int getCardWidth(TvChannel tvChannel) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_card_width);
    }

    @Override
    public int getCardHeight(TvChannel tvChannel) {
        return  mContext.getResources().getDimensionPixelOffset(R.dimen.tv_channel_zapper_card_height);
    }

    @Override
    public long getStableId(TvChannel tvChannel) {
        return Objects.hash(tvChannel.getId(), tvChannel.getType());
    }

    public static class ViewHolder extends BaseCardPresenter.ViewHolder {
        
        private final ChannelZapperCardBinding mBinding;

        Disposable mPlaybackOptionDisposable;

        public ViewHolder(ChannelZapperCardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        public void dispose() {
            if (mPlaybackOptionDisposable != null) {
                mPlaybackOptionDisposable.dispose();
            }
        }

        public ImageView getChannelLogoView() {
            return mBinding.logo;
        }
    }
}
