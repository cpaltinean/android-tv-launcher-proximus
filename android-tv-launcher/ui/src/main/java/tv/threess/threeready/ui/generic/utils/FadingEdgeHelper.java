package tv.threess.threeready.ui.generic.utils;

import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;

/**
 * Interface with helper methods used in FadingEdgeGridLayout and FadingEdgeLinearLayout.
 *
 * @author Daniela Toma
 * @since 12.09.2022
 */
public interface FadingEdgeHelper {

    int FADE_EDGE_TOP = 1;
    int FADE_EDGE_BOTTOM = 2;
    int FADE_EDGE_LEFT = 4;
    int FADE_EDGE_RIGHT = 8;

    int DIRTY_FLAG_TOP = 1;
    int DIRTY_FLAG_BOTTOM = 2;
    int DIRTY_FLAG_LEFT = 4;
    int DIRTY_FLAG_RIGHT = 8;

    int[] FADE_COLORS = new int[]{Color.TRANSPARENT,  Color.BLACK};
    int[] FADE_COLORS_REVERSE = new int[]{Color.BLACK, Color.TRANSPARENT};

    default void initBottomGradient(int height, int width, int paddingTop, int paddingLeft, int paddingBottom, int paddingRight,
                                    int gradientSizeBottom, Rect gradientRectBottom, Paint gradientPaintBottom) {
        int actualHeight = height - paddingTop - paddingBottom;
        int size = Math.min(gradientSizeBottom, actualHeight);
        int l = paddingLeft;
        int t = paddingTop + actualHeight - size;
        int r = width - paddingRight;
        int b = t + size;
        gradientRectBottom.set(l, t, r, b);
        LinearGradient gradient = new LinearGradient(l, t, l, b, FADE_COLORS_REVERSE, null, Shader.TileMode.CLAMP);
        gradientPaintBottom.setShader(gradient);
    }

    default void initLeftGradient(int height, int width, int paddingTop,int paddingBottom,
                                  int gradientSizeLeft, Rect gradientRectLeft, Paint gradientPaintLeft) {
        int actualWidth = width;
        int size = Math.min(gradientSizeLeft, actualWidth);
        int l = 0;
        int t = paddingTop;
        int r = l + size;
        int b = height - paddingBottom;
        gradientRectLeft.set(l, t, r, b);
        LinearGradient gradient = new LinearGradient(l, t, r, t, FADE_COLORS, null, Shader.TileMode.CLAMP);
        gradientPaintLeft.setShader(gradient);
    }

    default void initTopGradient(int height, int width, int gradientSizeTop, Rect gradientRectTop, Paint gradientPaintTop) {
        int actualHeight = height;
        int size = Math.min(gradientSizeTop, actualHeight);
        int l = 0;
        int t = 0;
        int r = width;
        int b = t + size;
        gradientRectTop.set(l, t, r, b);
        gradientPaintTop.setShader(getLinearGradient(l,t, b));
    }

    default void initRightGradient(int height, int width, int paddingTop, int paddingBottom,
                                   int gradientSizeRight, Rect gradientRectRight, Paint gradientPaintRight) {
        int actualWidth = width;
        int size = Math.min(gradientSizeRight, actualWidth);
        int l = 0;
        int t = paddingTop;
        int r = l + size;
        int b = height - paddingBottom;
        gradientRectRight.set(l, t, r, b);
        LinearGradient gradient = new LinearGradient(l, t, r, t, FADE_COLORS_REVERSE, null, Shader.TileMode.CLAMP);
        gradientPaintRight.setShader(gradient);
    }

    LinearGradient getLinearGradient(int left, int top, int bottom);
}
