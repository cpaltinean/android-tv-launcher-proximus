/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.vod.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.ui.generic.notification.Notification;

/**
 * Player for trailer.
 *
 * @author Szende Pal
 * @since 2017.08.22
 */
public class TrailerPlayerFragment extends VodPlayerFragment {
    public static final String TAG = Log.tag(TrailerPlayerFragment.class);

    private static final String EXTRA_TRAILER_VOD = "EXTRA_TRAILER_VOD";
    private static final String EXTRA_TRAILER_VOD_VARIANT = "EXTRA_TRAILER_VOD_VARIANT";

    private Notification mAgeRestrictionNotification;

    public static TrailerPlayerFragment newInstance(IVodVariant vodVariant, IVodItem vod, boolean shouldStartPlayback) {
        Bundle args = new Bundle();
        args.putBoolean(EXTRA_SHOULD_START_PLAYBACK, shouldStartPlayback);
        args.putSerializable(EXTRA_TRAILER_VOD, vod);
        args.putSerializable(EXTRA_TRAILER_VOD_VARIANT, vodVariant);
        args.putSerializable(EXTRA_STARTED_ITEM, vodVariant);

        TrailerPlayerFragment fragment = new TrailerPlayerFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mAgeRestrictionNotification != null) {
            mNotificationManager.hideNotification(mAgeRestrictionNotification);
        }
    }

    @Override
    protected boolean isForActivePlayback() {
        IVodItem playerData = mPlaybackDetailsManager.getVodPlayerData();
        IVodItem currentVod = getSerializableArgument(EXTRA_TRAILER_VOD);
        if (playerData == null || currentVod == null) {
            return false;
        }

        return mPlaybackDetailsManager.getPlayerType() == PlaybackType.VodTrailer
                && currentVod.getId().equals(playerData.getId());
    }

    @Override
    protected void updateMoreInfoButton(IVodVariant variant) {
        mBinding.buttonsContainer.moreInfoButton.setVisibility(View.GONE);
    }

    @Override
    protected void startPlayback(StartAction action) {
        IVodVariant vodVariant = getSerializableArgument(EXTRA_TRAILER_VOD_VARIANT);
        IVodItem vod = getSerializableArgument(EXTRA_TRAILER_VOD);
        if (vod == null || vodVariant == null) {
            Log.e(TAG, "No VOD provided to start the playback.");
            return;
        }

        if (!mPlaybackDetailsManager.isTrailerPlaying(vodVariant)) {
            // Show age restriction notification when the playback starts
            mAgeRestrictionNotification = mNotificationManager
                    .showAgeRestrictionLegalNotificationIfNeeded(vod.getParentalRating());
        }

        Log.d(TAG, "startPlayback() called with: vod = [" + vod.getTitle() + " "
                + vod.getId() + "], vod variant = [" + vodVariant.getTitle() + " " + vodVariant.getId() + "], action = [" + action + "]");

        mPlaybackDetailsManager.startTrailer(action, vod, vodVariant, action == StartAction.StartOver);
    }

    @Override
    protected void updateContentMarkers(IVodVariant contentItem) {
        mBinding.detailsLayout.contentMarker.setVisibility(View.GONE);
    }

    @Override
    protected void updateSubtitle(IVodVariant contentItem) {
        List<String> detailsList = new ArrayList<>();
        detailsList.add(contentItem.getReleaseYear());
        String genres = TextUtils.join(", ", contentItem.getGenres().stream().limit(1).toArray());
        if (!TextUtils.isEmpty(genres)) {
            detailsList.add(genres);
        }
        mBinding.detailsLayout.subtitleDetails.updateSubtitle(null, detailsList);
    }
}
