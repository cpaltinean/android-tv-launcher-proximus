package tv.threess.threeready.ui.generic.model;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.PlayerConfig;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.PlayOption;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.pvr.model.SingleRecordingStatus;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;

/**
 * Broadcast wrapper class which holds all the necessary information to start the playback.
 *
 * @author Barabas Attila
 * @since 2018.12.17
 */
public class BroadcastPlayOption<TItem extends IBroadcast> extends PlayOption {

    private final TItem mContentItem;

    public BroadcastPlayOption(PlayOption.Type type, TItem broadcast) {
        super(type);
        mContentItem = broadcast;
    }

    public TItem getContentItem() {
        return mContentItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BroadcastPlayOption<TItem> that = (BroadcastPlayOption<TItem>) o;
        return Objects.equals(mContentItem, that.mContentItem);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mContentItem);
    }

    /**
     * Generate available playback option for a broadcast.
     *
     * @param channel           The channel on which the broadcast is available.
     * @param broadcast         the broadcast for which the playback options should be generated.
     * @param bookmark          the bookmark corresponding to the given IBroadcast
     * @param recordingStatus   the current recording status of the broadcast
     * @param recordingPriority if true ignores all the other playback options, returns only the recording if available.
     * @param replayPriority    if the replay playback options should be prioritized over live.
     * @return Returns all available playback option.
     */
    public static List<PlayOption> generatePlayOptions(@Nullable TvChannel channel, IBroadcast broadcast,
                                                       IBookmark bookmark,
                                                       @Nullable RecordingStatus recordingStatus,
                                                       boolean recordingPriority, boolean replayPriority) {
        PlayOption livePlayOption = getLivePlayOption(broadcast, channel, bookmark, replayPriority);
        PlayOption recordingPlayOption = getRecordingPlayOption(recordingStatus);
        PlayOption replayPlayOption = getReplayPlayOption(broadcast, channel);

        return generateFinalOptions(livePlayOption, recordingPlayOption, replayPlayOption, recordingPriority);
    }

    /**
     * Prioritize recording play option if available.
     */
    protected static List<PlayOption> generateFinalOptions(PlayOption livePlayOption, PlayOption recordingPlayOption, PlayOption replayPlayOption, boolean recordingPriority) {
        if (recordingPriority && recordingPlayOption != null) {
            return Collections.singletonList(recordingPlayOption);
        }

        List<PlayOption> playOptions = new ArrayList<>(
                Arrays.asList(recordingPlayOption, livePlayOption, replayPlayOption));
        playOptions.removeIf(Objects::isNull);
        return playOptions;
    }

    /**
     * Add play button for live.
     */
    protected static PlayOption getLivePlayOption(IBroadcast broadcast, TvChannel channel, IBookmark bookmark, boolean replayPriority) {
        if (broadcast.isLive()) {
            if (replayPriority && bookmark != null && bookmark.getTimestamp() > 0) {
                return new BroadcastPlayOption<>(Type.REPLAY, broadcast);
            } else if (channel != null && channel.getType() == ChannelType.RADIO) {
                return new BroadcastPlayOption<>(Type.RADIO, broadcast);
            } else {
                return new BroadcastPlayOption<>(Type.LIVE, broadcast);
            }
        }
        return null;
    }

    /**
     * Add play button for recording.
     */
    private static PlayOption getRecordingPlayOption(RecordingStatus recordingStatus) {
        SingleRecordingStatus singleRecordingStatus = recordingStatus == null ?
                null : recordingStatus.getSingleRecordingStatus();

        if (singleRecordingStatus == SingleRecordingStatus.RECORDING) {
            long startOverAppearingDelay = Components.get(PlayerConfig.class)
                    .getStartOverAppearingDelay(TimeUnit.MILLISECONDS);
            // If the recording only started recently, we need to wait 25 until we are able to show the stream
            IRecording recording = recordingStatus.getRecording();
            if (recording != null && !recording.isFailedRecording()) {
                if (System.currentTimeMillis() - recording.getRecordingStart() > startOverAppearingDelay) {
                    return new RecordingPlayOption(Type.RECORDING, recording, null);
                }
            }
        } else if (singleRecordingStatus == SingleRecordingStatus.COMPLETED) {
            IRecording recording = recordingStatus.getRecording();
            if (recording != null && !recording.isFailedRecording()) {
                return new RecordingPlayOption(Type.RECORDING, recording, null);
            }
        }

        return null;
    }

    /**
     * Add play button for replay.
     */
    protected static PlayOption getReplayPlayOption(IBroadcast broadcast, TvChannel channel) {
        if (broadcast.isPast() && broadcast.canReplay(channel)) {
            return new BroadcastPlayOption<>(Type.REPLAY, broadcast);
        }
        return null;
    }
}
