package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.leanback.widget.HorizontalGridView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Objects;

import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.CollectionModuleViewBinding;
import tv.threess.threeready.ui.home.view.CollectionGridView;

/**
 * Responsible of presenting a collection's elements: title, grid view
 *
 * Created by Dalma Medve on 9/11/2017.
 */

public class CollectionView extends LinearLayout {

    private final static int NO_POSITION = -1;
    public CollectionModuleViewBinding mBinding;

    private int mColumnNumber;
    private DataSourceSelector mSelector;

    public CollectionView(Context context) {
        this(context, null);
    }

    public CollectionView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CollectionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize();
    }

    @Override
    public View focusSearch(View focused, int direction) {
        View view = null;
        switch (direction) {
            case FOCUS_UP: {
                if (mBinding.collectionGridView.hasFocus() && mBinding.selectorButtonGrid.isShown()) {
                    view = mBinding.selectorButtonGrid;
                    break;
                }

                if (mBinding.selectorButtonGrid.hasFocus() && mBinding.selectorMenuGrid.isShown()){
                    // need to force focus to go back on the last focused item in the menu grid view
                    int position = getLastActivatedMenuViewPosition();
                    if (position != NO_POSITION) {
                        mBinding.selectorMenuGrid.setSelectedPosition(position);
                        view = mBinding.selectorMenuGrid;
                    }
                }
                break;
            }
            case FOCUS_DOWN: {
                if (mBinding.selectorMenuGrid.hasFocus() && mBinding.selectorButtonGrid.isShown()) {
                    // Focus selector buttons from menu.
                    view = mBinding.selectorButtonGrid;
                }
                break;
            }
        }

        if (view == null) {
            view = super.focusSearch(focused, direction);
        }

        // Focus is not on gird. Deactivate title.
        if (mBinding.collectionGridView.indexOfChild(view) == -1) {
            mBinding.moduleTitle.setActivated(false);
        }

        return view;
    }

    /**
     * @return the last focused menu item position from the menu grid. From {@link DataSourceSelector}
     *         class we decide which {@link SelectorOption} was selected last time.
     */
    private int getLastActivatedMenuViewPosition() {
        // if there is no selector return fallback
        if (mSelector == null) {
            return NO_POSITION;
        }

        SelectorOption selectedOption = mSelector.getSelectedOption();
        List<SelectorOption> options = mSelector.getOptions();
        for (int i = 0; i< options.size(); ++i) {
            SelectorOption option = options.get(i);
            if (Objects.equals(option, selectedOption)) {
                return i;
            }
        }

        // no selected option is exist in the selector option list
        // return fallback
        return NO_POSITION;
    }

    @Override
    public void requestChildFocus(View child, View focused) {
        mBinding.moduleTitle.setActivated(true);

        super.requestChildFocus(child, focused);
    }

    private void initialize() {
        mBinding = CollectionModuleViewBinding.inflate(LayoutInflater.from(getContext()), this);

        mBinding.collectionGridView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int scrolledY = 0;

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                scrolledY += dy;
                if (mBinding.collectionGridView.getY() > 0) {
                    mBinding.moduleTitle.setVisibility(View.GONE);
                }

                View firstChild = mBinding.collectionGridView.getChildAt(0);
                if (firstChild != null) {
                    if(firstChild.getY() >= getResources().getDimension(R.dimen.scroll_cut_threshold)) {
                        hideFading();
                    } else {
                        showFading();
                    }
                }

                if (mBinding.collectionGridView.getChildAdapterPosition(mBinding.collectionGridView.getFocusedChild()) < mColumnNumber) {
                    hideFading();
                }
                if (mBinding.collectionGridView.findFirstCompletelyVisibleItemPosition() == 0
                        && !TextUtils.isEmpty(mBinding.moduleTitle.getText())) {
                    mBinding.moduleTitle.setVisibility(View.VISIBLE);
                }

                // Sync grid scrolling with button selectors.
                int selectorTranslationY = 0;
                RecyclerView.Adapter<?> adapter = mBinding.collectionGridView.getAdapter();
                if (firstChild != null && adapter != null && adapter.getItemCount() > 0) {
                    int firstAdapterPosition = mBinding.collectionGridView.getChildAdapterPosition(firstChild);
                    selectorTranslationY =  firstAdapterPosition == 0 ? Math.round(firstChild.getY()) : getHeight();
                }
                mBinding.selectorButtonGrid.setTranslationY(selectorTranslationY);
            }
        });
    }

    private void showFading() {
        mBinding.fadingEdgeLayout.setFadeEdges(true, false, false, false);
        mBinding.fadingEdgeLayout.setFadeSizes((int) getResources().getDimension(R.dimen.fading_edge_visible_size), 0, 0, 0);
    }

    private void hideFading() {
        mBinding.fadingEdgeLayout.setFadeSizes((int) getResources().getDimension(R.dimen.fading_edge_invisible_size), 0, 0, 0);
        mBinding.fadingEdgeLayout.setFadeEdges(true, false, false, false);
    }

    public void setNumColumns(int columnNumber) {
        mBinding.collectionGridView.setNumColumns(columnNumber);
        mColumnNumber = columnNumber;
    }

    public void setSelector(DataSourceSelector menuSelector) {
        mSelector = menuSelector;
    }

    @NonNull
    public HorizontalGridView getSelectorMenuGrid() {
        return mBinding.selectorMenuGrid;
    }

    @NonNull
    public HorizontalGridView getSelectorButtonGrid() {
        return mBinding.selectorButtonGrid;
    }

    @NonNull
    public CollectionGridView getCollectionGrid() {
        return mBinding.collectionGridView;
    }

    @NonNull
    public FontTextView getEmptyTextView() {
        return mBinding.emptyContentText;
    }

    @NonNull
    public FontTextView getTitleView() {
        return mBinding.moduleTitle;
    }
}
