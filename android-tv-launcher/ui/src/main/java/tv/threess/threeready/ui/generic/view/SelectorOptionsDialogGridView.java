package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.SelectorOptionsGridViewBinding;
import tv.threess.threeready.ui.generic.dialog.SelectorOptionDialog;
import tv.threess.threeready.ui.generic.presenter.SelectorOptionsItemAdapter;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Custom grid view used for sort option selection. This layout contains two arrows, above and below the list, which will be visible only if there are more than 5 items, through navigation in the list.
 * Grid width will be calculated by the largest item from the list.
 *
 * Created by Noemi Dali on 07.08.2020.
 */
public class SelectorOptionsDialogGridView extends LinearLayout {

    private final Translator mTranslator = Components.get(Translator.class);
    private final SelectorOptionsGridViewBinding mBinding;
    private final TextPaint mTextPaint;

    public SelectorOptionsDialogGridView(Context context) {
        this(context, null);
    }

    public SelectorOptionsDialogGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SelectorOptionsDialogGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        mBinding = SelectorOptionsGridViewBinding.inflate(LayoutInflater.from(context), this, true);

        mTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTypeface(ResourcesCompat.getFont(getContext(), R.font.proximus_bold));
        mTextPaint.setTextSize(getResources().getDimension(R.dimen.sort_option_item_text_size));
        mTextPaint.setStyle(Paint.Style.FILL);

        mBinding.selectorGrid.setOnScrollChangeListener((v, scrollX, scrollY, oldScrollX, oldScrollY) -> updateArrowsVisibility());
        mBinding.selectorGrid.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                if (parent.getAdapter() == null) {
                    return;
                }

                if (parent.getChildAdapterPosition(view) != 0) {
                    outRect.top += getResources().getDimensionPixelOffset(R.dimen.sort_options_item_vertical_offset);
                }
                if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1) {
                    outRect.bottom += getResources().getDimensionPixelOffset(R.dimen.sort_options_item_vertical_offset);
                }
            }
        });
    }

    public void setupGridItems(DataSourceSelector sorting, SelectorOptionDialog.OnGridItemClickListener onGridItemClickListener) {
        SelectorOptionsItemAdapter sortOptionsItemPresenter = new SelectorOptionsItemAdapter(sorting, clickedOption -> {
            if (onGridItemClickListener != null) {
                onGridItemClickListener.onItemClicked(clickedOption);
            }
        });

        mBinding.selectorGrid.setAdapter(sortOptionsItemPresenter);
        mBinding.selectorGrid.setSelectedPosition(
                sorting.getOptions().indexOf(sorting.getSelectedOption()));

        updateArrowsVisibility();

        int width = calculateWidth(sorting.getOptions());
        int minWidth = (int) getResources().getDimension(R.dimen.sort_option_grid_min_width);
        MarginLayoutParams lp = (MarginLayoutParams) mBinding.selectorGrid.getLayoutParams();
        lp.width = Math.max(minWidth, width);
        mBinding.selectorGrid.setLayoutParams(lp);
    }

    private void updateArrowsVisibility() {
        RecyclerView.Adapter<?> adapter = mBinding.selectorGrid.getAdapter();
        if (adapter == null || adapter.getItemCount() == 0) {
            mBinding.arrowUp.setVisibility(INVISIBLE);
            mBinding.arrowDown.setVisibility(INVISIBLE);
            return;
        }

        int firstVisible = findFirstCompletelyVisibleItemPosition();
        int lastVisible = findLastCompletelyVisibleItemPosition();

        if (firstVisible == 0) {
            mBinding.arrowUp.setVisibility(INVISIBLE);
        } else {
            mBinding.arrowUp.setVisibility(VISIBLE);
        }

        if (lastVisible == adapter.getItemCount() - 1) {
            mBinding.arrowDown.setVisibility(INVISIBLE);
        } else {
            mBinding.arrowDown.setVisibility(VISIBLE);
        }
    }

    /**
     * @return the first completely visible item adapter position
     */
    public int findFirstCompletelyVisibleItemPosition() {
        for (int i = 0; i < mBinding.selectorGrid.getChildCount(); i++) {
            View child = mBinding.selectorGrid.getChildAt(i);
            if (child.getY() >= 0 && child.getY() + child.getHeight() < mBinding.selectorGrid.getHeight()) {
                RecyclerView.ViewHolder holder = mBinding.selectorGrid.getChildViewHolder(child);
                return holder != null ? holder.getBindingAdapterPosition() : RecyclerView.NO_POSITION;
            }
        }
        return RecyclerView.NO_POSITION;
    }

    /**
     * @return the last completely visible item adapter position
     */
    public int findLastCompletelyVisibleItemPosition() {
        for (int i = mBinding.selectorGrid.getChildCount() - 1; i >= 0; --i) {
            View child = mBinding.selectorGrid.getChildAt(i);
            if (child.getY() > 0 && child.getY() + child.getHeight() <= mBinding.selectorGrid.getHeight()) {
                RecyclerView.ViewHolder holder = mBinding.selectorGrid.getChildViewHolder(child);
                return holder != null ? holder.getBindingAdapterPosition() : RecyclerView.NO_POSITION;
            }
        }
        return RecyclerView.NO_POSITION;
    }

    /**
     * Calculate maximum width in pixels for the title of the sort option when it will be displayed.
     * @param sortOptions The list of sort option to calculate maximum from.
     * @return The maximum width in pixels.
     */
    private int calculateWidth(List<? extends SelectorOption> sortOptions) {
        int maxWidth = 0;

        for (SelectorOption sortOption : sortOptions) {
            int width = (int) mTextPaint.measureText(
                    mTranslator.get(sortOption.getName()));
            if (width > maxWidth) {
                maxWidth = width;
            }
        }

        int itemPadding = 2 * getContext().getResources().getDimensionPixelOffset(R.dimen.action_btn_horizontal_padding);
        return maxWidth + itemPadding;
    }
}