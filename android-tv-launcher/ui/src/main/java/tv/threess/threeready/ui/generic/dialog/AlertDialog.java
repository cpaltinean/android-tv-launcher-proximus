/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.dialog;

import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.io.Serializable;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.AlertDialogBinding;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 *  Generic dialog style which is displayed at the middle of the screen.
 *  It can have one or two button, focus will be on the first button by default.
 *
 * @author Daniel Gliga
 * @since 2017.06.13
 */

public class AlertDialog extends BaseButtonDialog {
    public static final String TAG = Log.tag(AlertDialog.class);

    protected DialogModel mModel;
    private AlertDialogBinding mBinding;

    static AlertDialog newInstance(DialogModel model) {
        AlertDialog alertDialog = new AlertDialog();

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_DIALOG_MODEL, model);

        alertDialog.setArguments(args);
        return alertDialog;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);

        // Read dialog model immediately.
        mModel = (DialogModel) args.getSerializable(EXTRA_DIALOG_MODEL);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mModel = getSerializableArgument(EXTRA_DIALOG_MODEL);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = AlertDialogBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (TextUtils.isEmpty(mModel.getTitle())) {
            mBinding.title.setVisibility(View.GONE);
            mBinding.dialogSeparator.setVisibility(View.GONE);
        } else {
            mBinding.title.setVisibility(View.VISIBLE);
            mBinding.title.setText(mModel.getTitle());
            mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        }

        if (!mModel.isHtmlFormattingEnabled()) {
            mBinding.description.setText(mModel.getDescription());
        } else {
            mBinding.description.setText(Html.fromHtml(mModel.getDescription(), Html.FROM_HTML_MODE_COMPACT));
        }

        mBinding.description.setTextColor(mLayoutConfig.getFontColor());

        showFading();
        mBinding.scroll.getViewTreeObserver().addOnScrollChangedListener(() -> {
            View childView = mBinding.scroll.getChildAt(mBinding.scroll.getChildCount() - 1);
            int bottomDetector = childView.getBottom() - (mBinding.scroll.getHeight() + mBinding.scroll.getScrollY());
            if(bottomDetector == 0 ) {
                hideBottomFading();
            } else {
                showFading();
            }
        });

        changeTitleMargin();

        if (!TextUtils.isEmpty(mModel.getSecondaryDesc())) {
            mBinding.secondaryDescription.setVisibility(View.VISIBLE);
            mBinding.secondaryDescription.setText(mModel.getSecondaryDesc());
            mBinding.secondaryDescription.setTextColor(mLayoutConfig.getFontColor());
        }

        if (mModel.getButtons() == null || mModel.getButtons().isEmpty()) {
            mBinding.btnsLayout.setVisibility(View.GONE);
            mBinding.scroll.setContentDescription(mModel.getDescription());
        }

        if (!TextUtils.isEmpty(mModel.getErrorCode())) {
            mBinding.errorCode.setText(String.format("%s %s", mTranslator.get(TranslationKey.ERR_CODE), mModel.getErrorCode()));
        }

        if(mModel.getDescriptionTextSize() > 0) {
            mBinding.description.setTextSize(TypedValue.COMPLEX_UNIT_PX, mModel.getDescriptionTextSize());
            mBinding.secondaryDescription.setTextSize(TypedValue.COMPLEX_UNIT_PX, mModel.getDescriptionTextSize());
        }
    }

    /**
     * Change the title's margin.
     */
    private void changeTitleMargin() {
        int screenHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
        int paddingTop = (int) getResources().getDimension(R.dimen.alert_title_padding_top);

        float titleHeight = getTextHeight(mBinding.title, mModel.getTitle(), true) + getResources().getDimension(R.dimen.alert_title_margin_bottom);
        float descriptionHeight = getTextHeight(mBinding.description, mModel.getDescription(), false) + getResources().getDimension(R.dimen.alert_description_margin_bottom);
        float separatorHeight = mBinding.dialogSeparator.getHeight() + getResources().getDimension(R.dimen.alert_title_margin_bottom);

        if(paddingTop + titleHeight + descriptionHeight + separatorHeight > screenHeight) {
            int marginBottom = (int) getResources().getDimension(R.dimen.alert_title_margin_bottom);
            int marginTop = (int) getResources().getDimension(R.dimen.alert_title_margin_top);
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams)mBinding.title.getLayoutParams();
            layoutParams.setMargins(0, marginTop, 0, marginBottom);
        } else {
            //Move the focus from scroll to buttons.
            setFocusToButtons();
        }
    }

    /**
     * Sets the focus to buttons.
     */
    private void setFocusToButtons() {
        if(mModel.getButtons() != null && mModel.getButtons().size() > 0) {
            mBinding.btnsLayout.requestFocus();
            mBinding.scroll.setFocusable(false);
        }
    }

    /**
     * Gets the height that will be occupy by the text view before rendering.
     */
    private float getTextHeight(TextView view, String textToMeasure, boolean isTitle) {
        float lineWidth;
        float textHeight;
        int numberOfLines;

        Rect bounds = new Rect();
        Paint textPaint = view.getPaint();
        textPaint.getTextBounds(textToMeasure, 0, textToMeasure.length(), bounds);
        if (isTitle) {
            lineWidth = Resources.getSystem().getDisplayMetrics().widthPixels - 2 * getResources().getDimension(R.dimen.alert_side_padding);
        } else {
            lineWidth = getResources().getDimension(R.dimen.alert_desc_width_for_calculation);
        }
        numberOfLines = (int) Math.ceil(bounds.width() / lineWidth);
        textHeight = numberOfLines * (view.getLineHeight() + view.getLineSpacingExtra());

        return textHeight;
    }

    @Override
    protected ViewGroup getButtonsContainer() {
        return mBinding.btnsLayout;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        // set fullscreen
        if (dialog != null && dialog.getWindow() != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        return mModel.getEvent();
    }

    /**
     * Model class which holds all the necessary information to display the alert dialog.
     */
    protected static class DialogModel extends BaseButtonDialog.DialogModel implements Serializable {
        String mErrorCode;

        float mDescriptionTextSize;

        boolean mFormatHtml;

        transient Event<UILogEvent, UILogEvent.Detail> event;

        public String getErrorCode() {
            return mErrorCode;
        }

        public float getDescriptionTextSize() {
            return mDescriptionTextSize;
        }

        public boolean isHtmlFormattingEnabled() {
            return mFormatHtml;
        }

        public Event<UILogEvent, UILogEvent.Detail> getEvent() {
            return event;
        }
    }

    /**
     * Utility class to build and create generic alert dialogs.
     */
    public static class Builder {
        private final DialogModel mModel;

        public Builder() {
            mModel = new DialogModel();
        }

        public Builder errorCode(String errorCode) {
            mModel.mErrorCode = errorCode;
            return this;
        }

        /**
         * @param dismissOnBtnClick if true it will automatically
         *                         dismiss the dialog after a button clicked.
         */
        public Builder dismissOnBtnClick(boolean dismissOnBtnClick) {
            mModel.mDismissOnBtnClick = dismissOnBtnClick;
            return this;
        }

        public Builder addButton(String text, ButtonClickListener buttonClickListener) {
            mModel.mButtons.add(new ButtonModel(text, buttonClickListener));
            return this;
        }

        public Builder addButton(String text) {
            mModel.mButtons.add(
                    new ButtonModel(text));
            return this;
        }

        public Builder title(String title) {
            mModel.mTitle = title;
            return this;
        }

        public Builder description(String description) {
            mModel.mDescription = description;
            return this;
        }

        public Builder secondaryDescription(String secondaryDesc) {
            mModel.mSecondaryDesc = secondaryDesc;
            return this;
        }

        public Builder formatHtml(boolean formatHtml) {
            mModel.mFormatHtml = formatHtml;
            return this;
        }

        /**
         * @param cancelable if true the user will be able to cancel the dialog by pressing
         */
        public Builder cancelable(boolean cancelable) {
            mModel.mIsCancelable = cancelable;
            return this;
        }

        public Builder priority(int priority) {
            mModel.mPriority = priority;
            return this;
        }

        /**
         * Blocks all remote keys including global keys, except the OK button.
         */
        public Builder blockKeys(boolean blockKeys) {
            mModel.mBlockKeys = blockKeys;
            return this;
        }

        /**
         * Sets the font size that will be used for the description.
         */
        public Builder descriptionFontSize(float textSize) {
            mModel.mDescriptionTextSize = textSize;
            return this;
        }

        public Builder setEvent(Event<UILogEvent, UILogEvent.Detail>  event) {
            mModel.event = event;
            return this;
        }

        public DialogModel buildModel() {
            return mModel;
        }

        public String getDescription() {
            return mModel.mDescription;
        }

        public String getErrorCode() {
            return mModel.getErrorCode();
        }

        public String getTitle() {
            return mModel.getTitle();
        }

        public AlertDialog build() {
            return AlertDialog.newInstance(buildModel());
        }
    }


    /**
     * Shows the gradient at the top and at the bottom.
     */
    private void showFading() {
        int paddingTop = (int) getResources().getDimension(R.dimen.alert_title_padding_top);
        mBinding.dialogLayout.setFadeEdges(true, false, true, false);
        mBinding.dialogLayout.setFadeSizes(paddingTop, 0, paddingTop, 0);

    }

    /**
     * Hides the gradient from the bottom.
     */
    private void hideBottomFading() {
        mBinding.dialogLayout.setFadeSizes((int) getResources().getDimension(R.dimen.alert_title_padding_top), 0, (int) getResources().getDimension(R.dimen.fading_edge_invisible_size), 0);
        mBinding.dialogLayout.setFadeEdges(true, false, true, false);
    }
}
