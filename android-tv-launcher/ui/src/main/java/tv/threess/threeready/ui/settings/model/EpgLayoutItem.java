package tv.threess.threeready.ui.settings.model;

import tv.threess.threeready.api.tv.epg.EpgLayoutType;

/**
 * Entity class representing an epg layout item.
 *
 * @author Daniela Toma
 * @since 2021.11.08
 */
public class EpgLayoutItem {

    private final String mTitle;
    private final String mDescription;
    private final EpgLayoutType mEpgLayoutType;

    public EpgLayoutItem(String title, String description, EpgLayoutType type) {
        mTitle = title;
        mDescription = description;
        mEpgLayoutType = type;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public EpgLayoutType getEpgLayoutType() {
        return mEpgLayoutType;
    }
}
