package tv.threess.threeready.ui.generic.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.ButtonStyle;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.ActionItemBinding;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Presenter used for Action Buttons.
 *
 * @author Daniela Toma
 * @since 2020.07.31
 */
public class ActionButtonsPresenter extends BasePresenter<ActionButtonsPresenter.ViewHolder, ActionModel> {

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final ButtonStyle mButtonStyle = Components.get(ButtonStyle.class);

    private GlobalItemClickListener mGlobalItemClickListener;

    public ActionButtonsPresenter(Context context){
        super(context);
    }

    public ActionButtonsPresenter(Context context, GlobalItemClickListener globalItemClickListener) {
        super(context);
        mGlobalItemClickListener = globalItemClickListener;
    }

    @Override
    public ActionButtonsPresenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        ActionItemBinding binding = ActionItemBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);

        ActionButtonsPresenter.ViewHolder holder = new ActionButtonsPresenter.ViewHolder(binding);
        holder.mBinding.btnText.setBackground(UiUtils.createButtonBackground(mContext, mLayoutConfig, mButtonStyle));
        holder.mBinding.btnText.setTextColor(UiUtils.createButtonBrandingColorStateList(mLayoutConfig));
        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, ActionModel actionModel) {
        holder.mBinding.btnText.setText(actionModel.getText());
        if (actionModel.getRightDrawable() > 0 || actionModel.getLeftDrawable() > 0) {
            holder.mBinding.btnText.setCompoundDrawablesWithIntrinsicBounds(actionModel.getLeftDrawable(), 0, actionModel.getRightDrawable(), 0);
            holder.mBinding.btnText.setCompoundDrawablePadding(holder.mBinding.btnText.getResources().getDimensionPixelOffset(R.dimen.action_btn_vertical_padding));
        }

        // Dispatch focus changes on button.
        holder.view.setOnFocusChangeListener(actionModel.getOnFocusChangeListener());
        holder.view.setContentDescription(actionModel.getContentDescription());
    }

    @Override
    public void onClicked(ViewHolder holder, ActionModel actionModel) {
        super.onClicked(holder, actionModel);

        ActionModel.OnItemClickListener onModelClickListener = actionModel.getOnModelClickListener();
        if (mGlobalItemClickListener != null
            && mGlobalItemClickListener.onClick(actionModel)) {
            // The click was handled by the global listener.
            return;
        }

        if (onModelClickListener != null) {
            onModelClickListener.onClick();
        }
    }

    /**
     * Interface to get notified when the user clicks on any item in the adapter.
     */
    public interface GlobalItemClickListener {

        /**
         * Called when the user click on an item.
         * @param model The item on which the user clicked.
         * @return True if the action was fully handled by the global listener
         * and doesn't needs to dispatched for the item click listeners.
         */
        boolean onClick(ActionModel model);
    }

    public static class ViewHolder extends BasePresenter.ViewHolder {
        
        private final ActionItemBinding mBinding;

        public ViewHolder(ActionItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}