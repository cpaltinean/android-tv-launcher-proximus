/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ItemAlignmentFacet;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.leanback.widget.VerticalGridView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.NumberUtils;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.model.ISeason;
import tv.threess.threeready.api.generic.model.ISeries;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.SeasonRowBinding;
import tv.threess.threeready.ui.details.utils.SeriesFilterType;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Presenter for a season/episode grid on the detail page.
 *
 * @author Barabas Attila
 * @since 2018.12.03
 */
public abstract class SeriesRowPresenter<TContentItem extends IContentItem>
        extends BasePresenter<SeriesRowPresenter.ViewHolder<TContentItem>, ISeries<TContentItem>> {

    private static final long EPISODE_SELECTION_DISPATCH_TIMEOUT = 200;

    protected final Translator mTranslator = Components.get(Translator.class);
    protected final PlaybackDetailsManager mPlaybackManager = Components.get(PlaybackDetailsManager.class);
    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    protected InterfacePresenterSelector mEpisodePresenterSelector;

    /**
     * The episode which should be focused when the list is displayed.
     */
    private IContentItem mDefaultSelectedEpisode;

    /**
     * Listener used for episode grid view.
     */
    private OnChildViewHolderSelectedListener episodeChildHolderSelectedListener;

    EpisodeSelectionListener<TContentItem> mEpisodeSelectionListener;

    public SeriesRowPresenter(Context context, @Nullable IContentItem selectedEpisode,
                              EpisodeSelectionListener<TContentItem> episodeSelectionListener) {
        super(context);
        mDefaultSelectedEpisode = selectedEpisode;
        mEpisodeSelectionListener = episodeSelectionListener;
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        SeasonRowBinding binding = SeasonRowBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);

        ViewHolder<TContentItem> holder = new ViewHolder<>(binding);

        holder.mBinding.selectedEpisodeFromTotal.setTextColor(mLayoutConfig.getFontColor());

        //Focus item should be on top.
        holder.mBinding.episodeGrid.setItemAlignmentOffset(0);
        holder.mBinding.episodeGrid.setItemAlignmentOffsetPercent(VerticalGridView.ITEM_ALIGN_OFFSET_PERCENT_DISABLED);
        holder.mBinding.episodeGrid.setWindowAlignmentOffset(parent.getResources().getDimensionPixelOffset(R.dimen.details_episode_alignment_offset));
        holder.mBinding.episodeGrid.setWindowAlignmentOffsetPercent(VerticalGridView.WINDOW_ALIGN_OFFSET_PERCENT_DISABLED);
        holder.mBinding.episodeGrid.setWindowAlignment(VerticalGridView.WINDOW_ALIGN_NO_EDGE);
        holder.mBinding.episodeGrid.setVerticalSpacing(parent.getResources().getDimensionPixelSize(R.dimen.details_season_episode_space));
        holder.mBinding.episodeGrid.setVerticalFadingEdgeEnabled(true);
        holder.mBinding.episodeGrid.setFadingEdgeLength(R.dimen.details_fading_edge_length);
        holder.mBinding.episodeGrid.setHasFixedSize(true);
        holder.mBinding.episodeGrid.setItemViewCacheSize(6);

        holder.mBinding.filterGrid.setHorizontalSpacing(
                parent.getResources().getDimensionPixelOffset(R.dimen.details_tab_space));
        holder.mBinding.seasonsGrid.setVerticalSpacing(parent.getResources().getDimensionPixelSize(R.dimen.details_season_items_space));
        holder.mBinding.seasonsGrid.setHasFixedSize(true);

        setAlignmentOffset(holder);
        addSeasonListener(holder);
        setFilterAdapter(holder);
        setSeasonAdapter(holder);
        setEpisodesAdapter(holder);
        return holder;
    }

    private void setAlignmentOffset(ViewHolder<TContentItem> holder) {
        ItemAlignmentFacet.ItemAlignmentDef facetDef = new ItemAlignmentFacet.ItemAlignmentDef();
        facetDef.setItemAlignmentOffset(mContext.getResources().getDimensionPixelOffset(R.dimen.details_series_row_alignment_offset));
        ItemAlignmentFacet facet = new ItemAlignmentFacet();
        facet.setAlignmentDefs(new ItemAlignmentFacet.ItemAlignmentDef[]{facetDef});
        holder.setFacet(ItemAlignmentFacet.class, facet);
    }

    protected abstract List<SeriesFilterType> getFilters(ViewHolder<TContentItem> holder);

    @Override
    public void onBindHolder(ViewHolder<TContentItem> holder, ISeries<TContentItem> series, List<?> payLoads) {
        onBindHolder(holder, series);
    }

    @Override
    public void onBindHolder(ViewHolder<TContentItem> holder, ISeries<TContentItem> series) {
        holder.mSeries = series;

        updateFilters(holder);
        updateSeasons(holder);
        updateEpisodes(holder);

        holder.mPlayerCallback = new SeriesRowPresenter<?>.PlayerCallback(holder);
        mPlaybackManager.registerCallback(holder.mPlayerCallback);
    }

    private void setFilterAdapter(ViewHolder<TContentItem> holder) {
        holder.mFilterAdapter.setHasStableIds(true);
        holder.mBinding.filterGrid.setAdapter(new ItemBridgeAdapter(holder.mFilterAdapter,
                new SeriesFilterTypePresenter(mContext, (viewHolder, selectedFilter) -> {
                    if (holder.mActiveFilter != selectedFilter) {
                        holder.mActiveFilter = selectedFilter;
                        holder.mFilterAdapter.notifyDataSourceChanged();

                        List<TContentItem> episodes = filterEpisodes(holder, selectedFilter);
                        holder.setTotalEpisodes(episodes.size());
                        holder.mEpisodeAdapter.replaceAll(episodes);
                        holder.mBinding.episodeGrid.setSelectedPosition(0);

                        updateSeasonGridVisibility(holder, selectedFilter);
                    }
                },
                        () -> holder.mActiveFilter
                )));
    }

    private void updateFilters(ViewHolder<TContentItem> holder) {
        holder.mFilters = getFilters(holder);

        // Select first filter by default.
        if (holder.mActiveFilter == null
                || !holder.mFilters.contains(holder.mActiveFilter)) {
            holder.mActiveFilter = holder.mFilters.get(0);
        }

        holder.mFilterAdapter.replaceAll(holder.mFilters);
    }

    /**
     * Gets the list of seasons that will be shown in seasons grid.
     */
    protected abstract List<ISeason<TContentItem>> getSeasons(ViewHolder<TContentItem> holder);

    private void setSeasonAdapter(ViewHolder<TContentItem> holder) {
        holder.mSeasonAdapter.setHasStableIds(true);

        holder.mBinding.seasonsGrid.setAdapter(new ItemBridgeAdapter(holder.mSeasonAdapter,
                new SeasonLabelPresenter<>(mContext,
                        (viewHolder, season) -> {
                            int seasonIndex = holder.mSeasonAdapter.indexOf(season);
                            updateSeasonSelection(holder, seasonIndex);
                            holder.mBinding.episodeGrid.setSelectedPosition(
                                    getPositionForSeason(holder.mSeasonAdapter.get(seasonIndex),
                                            holder.mEpisodeAdapter));
                        },
                        () -> holder.mSeasonAdapter.get(holder.mSelectedSeasonPosition)
                )));
    }

    private void updateSeasons(ViewHolder<TContentItem> holder) {
        // Show seasons.
        holder.mSeasons = getSeasons(holder);

        // Update season selection.
        holder.mSelectedSeasonPosition = NumberUtils.clamp(
                holder.mSelectedSeasonPosition, 0, holder.mSeasons.size() - 1);

        holder.mSeasonAdapter.replaceAll(holder.mSeasons);

        updateSeasonGridVisibility(holder, holder.mActiveFilter);
    }

    private void setEpisodesAdapter(ViewHolder<TContentItem> holder) {
        holder.mEpisodeAdapter.setHasStableIds(true);
        holder.mBinding.episodeGrid.setAdapter(new ItemBridgeAdapter(
                holder.mEpisodeAdapter, mEpisodePresenterSelector));

        // Select season based on the focused episode.
        addViewHolderSelectedListener(holder);

        holder.mBinding.episodeGrid.setOnChildSelectedListener((parent, view, position, id) -> {
            int selectedPosition = position + 1;
            updateSelectedEpisodeFromTotalText(holder, selectedPosition);
        });
    }

    private void updateEpisodes(ViewHolder<TContentItem> holder) {
        // Episode grid.
        holder.mEpisodes = filterEpisodes(holder, holder.mActiveFilter);
        holder.mEpisodeAdapter.replaceAll(holder.mEpisodes);
        changeDefaultEpisode(holder,  holder.mEpisodes);
        updateEpisodeSelection(holder);
        holder.setTotalEpisodes(holder.mEpisodes.size());
        int selectedPosition = holder.mBinding.episodeGrid.getSelectedPosition();
        holder.mBinding.episodeGrid.setSelectedPosition(
                NumberUtils.clamp(selectedPosition, 0,  holder.mEpisodes.size() - 1));
    }

    /**
     * Changes episode's selected position when Most recent filter is selected.
     */
    private void changeDefaultEpisode(ViewHolder<TContentItem> holder, List<TContentItem> episodeList) {
        if (holder.mActiveFilter == SeriesFilterType.MostRecent && mDefaultSelectedEpisode == null) {
            mDefaultSelectedEpisode = episodeList.get(0);
        }
    }

    /**
     * Adds scroll listener on season grid used to show/hide gradient on top/bottom of season grid.
     */
    private void addSeasonListener(ViewHolder<TContentItem> holder) {
        holder.mBinding.seasonsGrid.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                updateSeasonShadow(holder);
            }
        });

        holder.mBinding.seasonsGrid.addOnChildAttachStateChangeListener(
                new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(@NonNull View view) {
                updateSeasonShadow(holder);
            }

            @Override
            public void onChildViewDetachedFromWindow(@NonNull View view) {
                updateSeasonShadow(holder);
            }
        });
    }

    private void updateSeasonShadow(ViewHolder<TContentItem> holder) {
        RecyclerView recyclerView = holder.mBinding.seasonsGrid;

        boolean showTopShadow = true;
        boolean showBottomShadow = true;

        for (int i=0; i< recyclerView.getChildCount(); ++i) {
            View view = recyclerView.getChildAt(i);
            if (view == null) {
                continue;
            }

            int adapterPosition = recyclerView.getChildAdapterPosition(view);

            // Check if the first view fully visible.
            if (adapterPosition == 0 && view.getTop() == 0) {
                showTopShadow = false;
            }

            // Check if the last view fully visible.
            if (adapterPosition ==  holder.mSeasonAdapter.size() - 1
                    && view.getBottom() <= recyclerView.getHeight()) {
                showBottomShadow = false;
            }

        }

        holder.mBinding.seasonGridTopGradient.setVisibility(showTopShadow ? View.VISIBLE : View.GONE);
        holder.mBinding.seasonGridBottomGradient.setVisibility(showBottomShadow ? View.VISIBLE : View.GONE);
    }

    /**
     * Updates seasons grid visibility(should be visible only on Episode tab if there are seasons).
     */
    protected void updateSeasonGridVisibility(ViewHolder<TContentItem> holder, SeriesFilterType selectedFilter) {
        ViewGroup.MarginLayoutParams layout = (ViewGroup.MarginLayoutParams) holder.mBinding.episodeGrid.getLayoutParams();

        if (shouldHideSeasonGrid(selectedFilter, holder.mSeasons)) {
            holder.mBinding.seasonsGrid.setVisibility(View.GONE);
            holder.mBinding.seasonGridTopGradient.setVisibility(View.GONE);
            holder.mBinding.seasonGridBottomGradient.setVisibility(View.GONE);
            layout.setMargins(mContext.getResources().getDimensionPixelOffset(R.dimen.details_episode_margin_left_without_season),
                    mContext.getResources().getDimensionPixelOffset(R.dimen.details_episode_margin_top),
                    0, 0);
            holder.mBinding.seasonsGrid.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        } else {
            layout.setMargins(mContext.getResources().getDimensionPixelOffset(R.dimen.details_episode_margin_left),
                    mContext.getResources().getDimensionPixelOffset(R.dimen.details_episode_margin_top),
                    0, 0);
            holder.mBinding.seasonsGrid.setVisibility(View.VISIBLE);
            holder.mBinding.seasonsGrid.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
        }
        holder.mBinding.episodeGrid.requestLayout();
    }

    /**
     * @return Returns true if season grid should be hidden, otherwise false.
     */
    protected boolean shouldHideSeasonGrid(SeriesFilterType selectedFilter, List<ISeason<TContentItem>> seasons) {
        if (selectedFilter == SeriesFilterType.MostRecent
                || selectedFilter == SeriesFilterType.Planned) {
            return true;
        }
        //If seasons contains only "Others", the grid will not be shown.
        return noSeasons(seasons);
    }

    protected boolean noSeasons(List<ISeason<TContentItem>> seasons) {
        return seasons.size() == 1 && seasons.get(0).getSeasonNumber() == null && TextUtils.isEmpty(seasons.get(0).getSeasonTitle());
    }

    /**
     * Filters the episodes by type.
     */
    protected abstract List<TContentItem> filterEpisodes(ViewHolder<TContentItem> holder, SeriesFilterType selectedFilter);

    /**
     * Method used for add the episode grid view listener.
     * After changing the episode list we have to refresh this listener.
     */
    private void addViewHolderSelectedListener(ViewHolder<TContentItem> holder) {
        if (episodeChildHolderSelectedListener != null) {
            holder.mBinding.episodeGrid.removeOnChildViewHolderSelectedListener(episodeChildHolderSelectedListener);
        }

        episodeChildHolderSelectedListener = new OnChildViewHolderSelectedListener() {
            int oldPosition = -1;

            @Override
            public void onChildViewHolderSelectedAndPositioned(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
                super.onChildViewHolderSelectedAndPositioned(parent, child, position, subposition);

                if (position < 0 || position >= holder.mEpisodeAdapter.size() || oldPosition == position) {
                    // Ignore. The data changed, adapter will be notified.
                    return;
                }

                oldPosition = position;
                // Select season grid together with the episodes.
                TContentItem selectedEpisode = holder.mEpisodeAdapter.get(position);
                int selectedSeasonPosition = getEpisodeSeasonPosition(selectedEpisode, holder.mSeasons);
                updateSeasonSelection(holder, selectedSeasonPosition);

                holder.mBinding.seasonsGrid.removeCallbacks(mSelectionDispatcherRunnable);
                mSelectionDispatcherRunnable = () -> {
                    if (mEpisodeSelectionListener != null) {
                        mEpisodeSelectionListener.onEpisodeSelected(selectedEpisode);
                    }
                };
                holder.mBinding.seasonsGrid.postDelayed(mSelectionDispatcherRunnable, EPISODE_SELECTION_DISPATCH_TIMEOUT);
            }

            private Runnable mSelectionDispatcherRunnable;
        };

       holder.mBinding.episodeGrid.addOnChildViewHolderSelectedListener(episodeChildHolderSelectedListener);
    }

    private void updateEpisodeSelection(ViewHolder<TContentItem> holder) {
        if (mDefaultSelectedEpisode != null) {
            int defaultPosition = getPositionForEpisode(mDefaultSelectedEpisode, holder.mEpisodeAdapter);
            holder.mBinding.episodeGrid.setSelectedPosition(defaultPosition);
        }
    }

    private void updateSeasonSelection(ViewHolder<TContentItem> holder, int selectedSeasonPosition) {
        int oldPosition = holder.mSelectedSeasonPosition;

        if(selectedSeasonPosition != oldPosition) {
            holder.mSelectedSeasonPosition = selectedSeasonPosition;
            holder.mSeasonAdapter.notifyItemRangeChanged(oldPosition, 1, selectedSeasonPosition);
            holder.mSeasonAdapter.notifyItemRangeChanged(selectedSeasonPosition, 1, selectedSeasonPosition);
            holder.mBinding.seasonsGrid.setSelectedPosition(selectedSeasonPosition);
        }
    }

    private int getPositionForSeason(ISeason<TContentItem> season, ArrayObjectAdapter<TContentItem> episodeAdapter) {
        // Search for season by the first episode
        if (season.getSeasonNumber() == null && !season.getEpisodes().isEmpty()) {
            return getPositionForEpisode(season.getEpisodes().get(0), episodeAdapter);
        }

        // Search for season by season number.
        for (int i = 0; i < episodeAdapter.getItems().size(); ++i) {
            IContentItem item = episodeAdapter.getItems().get(i);
            if (item != null) {
                if (Objects.equals(item.getSeasonNumber(), season.getSeasonNumber())) {
                    return i;
                }
            }
        }
        return 0;
    }

    @Override
    public void onUnbindHolder(ViewHolder<TContentItem> holder) {
        super.onUnbindHolder(holder);

        holder.mBinding.episodeGrid.setAdapter(null);
        holder.mBinding.seasonsGrid.setAdapter(null);
        holder.mBinding.filterGrid.setAdapter(null);

        mPlaybackManager.unregisterCallback(holder.mPlayerCallback);
        holder.mPlayerCallback = null;
    }

    /**
     * Sets the text selected episodes/total episodes.
     */
    private void updateSelectedEpisodeFromTotalText(ViewHolder<TContentItem> holder, int selectedEpisode) {
        holder.mBinding.selectedEpisodeFromTotal.setText(String.format("%s/%s", selectedEpisode, holder.mTotalEpisodes));
    }

    private int getEpisodeSeasonPosition(IContentItem episode, List<ISeason<TContentItem>> seasons) {
        for (int i = 0; i < seasons.size(); ++i) {
            ISeason<?> season = seasons.get(i);

            // Find by season number.
            if (season.getSeasonNumber() != null) {
                if (season.getSeasonNumber() != null
                        && Objects.equals(episode.getSeasonNumber(), season.getSeasonNumber())) {
                    return i;
                }

            // Find by episode.
            } else {
                if (season.getEpisodes().contains(episode)) {
                    return i;
                }
            }
        }
        return 0;
    }

    private int getPositionForEpisode(IContentItem selectedEpisode, ArrayObjectAdapter<TContentItem> episodeAdapter) {
        for (int i = 0; i < episodeAdapter.getItems().size(); ++i) {
            IContentItem item = episodeAdapter.getItems().get(i);
            if (item != null) {
                if (Objects.equals(selectedEpisode.getId(), item.getId())) {
                    return i;
                }
            }
        }
        return 0;
    }

    @Override
    public boolean onKeyEvent(ViewHolder<TContentItem> holder, ISeries<TContentItem> tContentItemISeries, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP && event.getKeyCode() == KeyEvent.KEYCODE_BACK
                && (holder.mBinding.seasonsGrid.hasFocus() || holder.mBinding.episodeGrid.hasFocus())) {
            focusFirstFilterTab(holder, tContentItemISeries);
            return true;
        }

        if(event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT &&
                holder.mBinding.seasonsGrid.hasFocus()) {
            holder.mBinding.episodeGrid.requestFocus();
            return true;
        }

        if(holder.mBinding.episodeGrid.hasFocus() && event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (event.getKeyCode()) {
                //When LEFT is pressed the focus needs to be moved from Episode grid to Seasons grid.
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    holder.mBinding.seasonsGrid.requestFocus();
                    return true;
                //If the focus is on the first episode card, when UP is pressed the focus should be moved to Filters grid.
                case KeyEvent.KEYCODE_DPAD_UP:
                    int selectedPosition = holder.mBinding.episodeGrid.getSelectedPosition();
                    if (selectedPosition == 0) {
                        holder.mBinding.filterGrid.setSelectedPosition(holder.mFilterAdapter.indexOf(holder.mActiveFilter));
                        holder.mBinding.filterGrid.requestFocus();
                        return true;
                    }
            }
        }

        return super.onKeyEvent(holder, tContentItemISeries, event);
    }

    /**
     * Focuses the first filter tab.
     */
    private void focusFirstFilterTab(ViewHolder<TContentItem> holder, ISeries<TContentItem> series) {
        holder.mActiveFilter = holder.mFilterAdapter.get(0);
        updateSeasonGridVisibility(holder, holder.mActiveFilter);
        holder.mFilterAdapter.notifyDataSourceChanged();
        filterEpisodes(holder, holder.mActiveFilter);
        holder.mBinding.filterGrid.setSelectedPosition(0);
        holder.mBinding.filterGrid.requestFocus();
    }

    static class ViewHolder<TContentItem extends IContentItem> extends Presenter.ViewHolder {
        protected final SeasonRowBinding mBinding;

        ISeries<TContentItem> mSeries;
        List<ISeason<TContentItem>> mSeasons;
        List<SeriesFilterType> mFilters = new ArrayList<>();
        List<TContentItem> mEpisodes = new ArrayList<>();

        ArrayObjectAdapter<ISeason<TContentItem>> mSeasonAdapter = new ArrayObjectAdapter<>();
        ArrayObjectAdapter<TContentItem> mEpisodeAdapter = new ArrayObjectAdapter<>();
        ArrayObjectAdapter<SeriesFilterType> mFilterAdapter = new ArrayObjectAdapter<>();

        IPlaybackCallback mPlayerCallback;

        SeriesFilterType mActiveFilter;
        int mSelectedSeasonPosition;

        int mTotalEpisodes = 0;

        public ViewHolder(SeasonRowBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        private void setTotalEpisodes(int totalEpisodes) {
            mTotalEpisodes = totalEpisodes;
        }
    }

    public interface EpisodeSelectionListener<TContentItem> {

        void onEpisodeSelected(TContentItem contentItem);
    }

    /**
     * Used to notify ui components about background playback changes.
     */
    private class PlayerCallback implements IPlaybackCallback {
        SeriesRowPresenter.ViewHolder<TContentItem> mHolder;

        PlayerCallback(SeriesRowPresenter.ViewHolder<TContentItem> holder) {
            mHolder = holder;
        }

        @Override
        public void onCommandSuccess(PlaybackCommand cmd, PlaybackDetails details) {
            IVodItem item = mPlaybackManager.getVodPlayerData();
            if (item != null && mDefaultSelectedEpisode != null
                    && !Objects.equals(item.getId(), mDefaultSelectedEpisode.getId()) && Objects.equals(item.getSeriesId(), mDefaultSelectedEpisode.getSeriesId())) {
                int position = getPositionForEpisode(item, mHolder.mEpisodeAdapter);
                mHolder.mBinding.episodeGrid.post(() -> mHolder.mBinding.episodeGrid.setSelectedPosition(position));
            }
        }
    }
}

