package tv.threess.threeready.ui.generic.player.view;

/**
 * interface that gets updates when state is changed for the @{@link ZappingInfoView}, states
 * could be the visibility change of the view
 * @author hunormadaras
 * @since 2019-06-13
 */
public interface IZappingInfoStateChangeListener {

    void onZappingInfoShown();

    void onZappingInfoHidden();

}
