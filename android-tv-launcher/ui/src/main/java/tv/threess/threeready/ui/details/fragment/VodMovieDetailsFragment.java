/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.details.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.details.presenter.VodMovieDetailsOverviewPresenter;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.vod.fragment.VodPlayerFragment;

/**
 * Displays movie details
 *
 * @author Andrei Teslovan
 * @since 2018.10.12
 */

public class VodMovieDetailsFragment extends BaseSingleDetailFragment<IBaseVodItem, VodMovieDetailsOverviewPresenter.ViewHolder> {
    public static final String TAG = Log.tag(VodMovieDetailsFragment.class);

    private static final String CONFIG_PAGE_ID = "DETAIL_VOD_MOVIE";

    private static final String EXTRA_VOD_MOVIE_ID = "movieID";
    private static final String EXTRA_VOD_MOVIE = "movie";

    private final VodRepository mVodRepository = Components.get(VodRepository.class);
    private final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);

    public static VodMovieDetailsFragment newInstance(String movieId) {
        VodMovieDetailsFragment fragment = new VodMovieDetailsFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_VOD_MOVIE_ID, movieId);
        fragment.setArguments(args);
        return fragment;
    }

    public static VodMovieDetailsFragment newInstance(IBaseVodItem movie) {
        VodMovieDetailsFragment fragment = new VodMovieDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_VOD_MOVIE, movie);
        args.putString(EXTRA_VOD_MOVIE_ID, movie.getId());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDataId = getStringArgument(EXTRA_VOD_MOVIE_ID);
        mData = getSerializableArgument(EXTRA_VOD_MOVIE);

        Log.d(TAG, "Detail page opened. Movie id: " + mDataId);
    }

    @Override
    protected void addExtraPresenter(InterfacePresenterSelector classPresenterSelector) {
        classPresenterSelector
                .addClassPresenter(IVodItem.class, new VodMovieDetailsOverviewPresenter(getContext(), () -> displayPage(getConfigPageForRecommendations())));
    }

    @Override
    protected String getRcmPageConfigId() {
        return CONFIG_PAGE_ID;
    }

    /**
     * Load and display movie details
     */
    @Override
    protected void loadDataDetails(final String movieId) {
        // Dispose channel list request
        RxUtils.disposeSilently(mDetailsDisposable);

        mDetailsDisposable = mVodRepository.getVod(movieId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::addUIData, throwable -> {
                    Log.e(TAG, "fail to open movie: " + movieId, throwable);
                    showErrorMessage();
                });
    }

    /**
     * Load and display movie details
     */
    @Override
    protected void loadDataDetails(IBaseVodItem vod) {
        // Dispose channel list request
        RxUtils.disposeSilently(mDetailsDisposable);

        mDetailsDisposable = mVodRepository.getVod(vod)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::addUIData, throwable -> {
                    Log.e(TAG, "fail to open movie: " + vod, throwable);
                    showErrorMessage();
                });
    }

    @Override
    protected int getOverviewItemOffset(VodMovieDetailsOverviewPresenter.ViewHolder viewHolder) {
        return calculateOverviewItemOffset(viewHolder);
    }

    @Override
    public boolean onBackPressed() {
        FragmentManager fragmentManager = mNavigator.getActivity().getSupportFragmentManager();
        int size = fragmentManager.getBackStackEntryCount();
        if (size >= 2) {
            //Pop PlayerFragment from backstack.
            FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(size - 2);
            if (Objects.equals(backStackEntry.getName(), VodPlayerFragment.TAG) && !mPlaybackDetailsManager.isContentItemPlaying(mData)) {
                fragmentManager.popBackStack(backStackEntry.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
                return true;
            }
        }
        return super.onBackPressed();
    }

    @Nullable
    @Override
    protected Event<?, ?> generatePageOpenEvent() {
        if (mData == null) {
            return null;
        }

        return ReportUtils.createEvent(UILog.Page.ActionMenuVod, mData);
    }
}
