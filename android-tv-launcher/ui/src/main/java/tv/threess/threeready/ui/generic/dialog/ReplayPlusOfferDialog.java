package tv.threess.threeready.ui.generic.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import androidx.annotation.NonNull;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.player.fragment.PlayerFragment;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Custom dialog fragment for replay plus.
 * This dialog shows up whether we need to block the forward seeking in case the user's account does not allow it.
 *
 * @author Andor Lukacs
 * @since 5/13/19
 */
public class ReplayPlusOfferDialog extends ActionBarDialog {
    private static final String TAG = Log.tag(ReplayPlusOfferDialog.class);

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);
    private final AppConfig appConfig = Components.get(AppConfig.class);

    private Disposable mURLDisposable;

    public static ReplayPlusOfferDialog newInstance(Translator translator) {
        ReplayPlusOfferDialog replayDialog = new ReplayPlusOfferDialog();

        DialogModel upSellingDialog = new ActionBarDialog.Builder()
                .title(translator.get(TranslationKey.ACTION_BAR_REPLAY_UPSELL_TITLE))
                .description(translator.get(TranslationKey.ACTION_BAR_REPLAY_UPSELL_BODY))
                .addButton(translator.get(TranslationKey.GET_REPLAY_PLUS_BUTTON), dialog -> replayDialog.openDeepLink())
                .addButton(translator.get(TranslationKey.DISMISS_BUTTON), dialog -> replayDialog.dismiss())
                .cancelable(false)
                .buildModel();

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_DIALOG_MODEL, upSellingDialog);

        replayDialog.setArguments(args);
        return replayDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PlayerFragment playerFragment = mNavigator.getPlayerFragment();

        if (playerFragment != null) {
            playerFragment.blockTrickPlay();
        }
    }

    private void openDeepLink() {
        String url = appConfig.getReplayPlusLink();
        RxUtils.disposeSilently(mURLDisposable);
        mURLDisposable = mAccountRepository.buildURL(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(formattedURL -> {
                            Log.d(TAG, "Formatted URL = " + formattedURL);
                            mNavigator.showWebView(formattedURL, false);
                            dismiss();
                        },
                        throwable -> Log.d(TAG, "Error getting parameters from CC. " + throwable));
    }

    /**
     * We have to override the default android behavior in case of
     * - KeyEvent.KEYCODE_DPAD_RIGHT
     * - KeyEvent.KEYCODE_DPAD_LEFT
     * - KeyEvent.KEYCODE_DPAD_CENTER
     *
     * Since Android sometimes steals key down events when a long press is already being
     * processed while displaying the dialog.
     */
    @Override
    public boolean onKeyDown(KeyEvent event) {
        Log.d(TAG, "onKeyDown() called with: event = [" + event + "]");
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_DPAD_RIGHT:
            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
            case KeyEvent.KEYCODE_MEDIA_REWIND:
            case KeyEvent.KEYCODE_BACK:
            case KeyEvent.KEYCODE_HOME:
            case KeyEvent.KEYCODE_GUIDE:
            case KeyEvent.KEYCODE_TV_RADIO_SERVICE:
                return true;
        }
        return super.onKeyDown(event);
    }

    /**
     * We have to override the default android behavior in case of
     * - KeyEvent.KEYCODE_DPAD_RIGHT
     * - KeyEvent.KEYCODE_DPAD_LEFT
     * - KeyEvent.KEYCODE_DPAD_CENTER
     *
     * Since Android sometimes steals key down events when a long press is already being
     * processed while displaying the dialog.
     */
    @Override
    public boolean onKeyUp(KeyEvent event) {
        Log.d(TAG, "onKeyUp() called with: event = [" + event + "]");
        super.onKeyUp(event);
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                return focusNextButton(View.FOCUS_RIGHT);
            case KeyEvent.KEYCODE_DPAD_LEFT:
                return focusNextButton(View.FOCUS_LEFT);
            case KeyEvent.KEYCODE_DPAD_CENTER:
                View focusedView = mBinding.buttonsContainer.findFocus();
                if (focusedView != null) {
                    focusedView.performClick();
                }
                return true;
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
            case KeyEvent.KEYCODE_MEDIA_REWIND:
            case KeyEvent.KEYCODE_BACK:
            case KeyEvent.KEYCODE_HOME:
            case KeyEvent.KEYCODE_GUIDE:
            case KeyEvent.KEYCODE_TV_RADIO_SERVICE:
                return true;
        }
        return false;
    }

    /**
     * Grab the current focused item, see if there is a next focusable item
     * based on {@param direction}, if there is any view to be focused, then focus it.
     *
     * @param direction View direction for the focus.
     * @return          Whether the view was focused or not.
     */
    private boolean focusNextButton(int direction) {
        View focusedView = mBinding.buttonsContainer.findFocus();
        View viewToFocus = mBinding.buttonsContainer.focusSearch(focusedView, direction);
        if (viewToFocus != null) {
            return viewToFocus.requestFocus();
        }

        return false;
    }



    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        PlayerFragment playerFragment = mNavigator.getPlayerFragment();
        RxUtils.disposeSilently(mURLDisposable);

        if (playerFragment != null) {
            playerFragment.unblockTrickPlay();
        }
    }
}
