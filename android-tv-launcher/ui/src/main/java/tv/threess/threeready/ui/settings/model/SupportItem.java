package tv.threess.threeready.ui.settings.model;

import android.graphics.drawable.Drawable;

/**
 * Entity class representing a support setting item.
 *
 * @author Karetka Mezei Zoltan
 * @since 2020.10.04
 */
public class SupportItem {

    private final Type mType;
    private final String mTitle;
    private final Drawable mIcon;

    public SupportItem(Type type, String title, Drawable icon) {
        mTitle = title;
        mType = type;
        mIcon = icon;
    }

    public String getTitle() {
        return mTitle;
    }

    public Type getType() {
        return mType;
    }

    public Drawable getIcon() {
        return mIcon;
    }

    public enum Type {
        SYSTEM_INFORMATION,
        CHECK_FOR_UPDATES,
        CHANNEL_SCAN,
        CLEAR_DATA,
        APP_RESTART,
    }
}
