package tv.threess.threeready.ui.startup.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.receivers.StandByStateChangeReceiver;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.WelcomeFragmentBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.UiUtils;

public class WelcomeFragment extends StartFragment {

    protected final Translator mTranslator = Components.get(Translator.class);
    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    protected final StandByStateChangeReceiver mStandByStateChangeReceiver = Components.get(StandByStateChangeReceiver.class);
    protected final Navigator mNavigator = Components.get(Navigator.class);

    private WelcomeFragmentBinding mBinding;

    public static WelcomeFragment newInstance() {
        return new WelcomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (mBinding == null) {
            mBinding = WelcomeFragmentBinding.inflate(inflater, container, false);
            mBinding.welcomeMessage.setTextColor(mLayoutConfig.getFontColor());
            mBinding.welcomeMessage.setText(mTranslator.get(TranslationKey.SCREEN_SPLASH_ENERGY_SAVE));
        }

        mStandByStateChangeReceiver.addStateChangedListener(mStandbyChangeListener);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateLayout();
    }

    @Override
    public void updateLayout() {
        if (mBinding.logo.getDrawable() == null) {
            UiUtils.loadImage(mTranslator, mLayoutConfig.getWelcomeLogo(), mBinding.logo);
        }

        mStandbyChangeListener.onStateChanged(mStandByStateChangeReceiver.isScreenOn());
    }

    @Override
    public boolean onBackPressed() {
        //Do nothing on back press
        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mStandByStateChangeReceiver.removeStateChangedListener(mStandbyChangeListener);
    }

    private final StandByStateChangeReceiver.Listener mStandbyChangeListener = new StandByStateChangeReceiver.Listener() {
        @Override
        public void onStateChanged(boolean screenOn) {
            if (mBinding == null) {
                return;
            }

            Glide.with(mBinding.getRoot()).clear(mBinding.gif);

            long intoStandby = Settings.intoStandbyTime.get(0L);
            long startupFlowFinished = Settings.startupFlowFinished.get(0L);

            if (intoStandby <= startupFlowFinished) {
                mBinding.gif.setVisibility(View.GONE);
                mBinding.welcomeMessage.setVisibility(View.GONE);
                mBinding.progressBar.setVisibility(View.VISIBLE);
            } else {
                mBinding.progressBar.setVisibility(View.GONE);
                Glide.with(mBinding.getRoot())
                        .load(R.drawable.splash_screen_animation)
                        .into(mBinding.gif);
                mBinding.gif.setVisibility(View.VISIBLE);
                mBinding.welcomeMessage.setVisibility(View.VISIBLE);
            }
        }
    };
}
