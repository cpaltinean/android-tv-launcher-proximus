package tv.threess.threeready.ui.generic.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.ui.databinding.LoadingItemBinding;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Presenter used for Loading Button.
 *
 * @author Daniela Toma
 * @since 2020.08.05
 */
public class LoadingButtonPresenter extends BasePresenter<LoadingButtonPresenter.ViewHolder, LoadingModel> {

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    public LoadingButtonPresenter(Context context) {
        super(context);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        LoadingItemBinding binding = LoadingItemBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        LoadingButtonPresenter.ViewHolder holder = new LoadingButtonPresenter.ViewHolder(binding);
        holder.mBinding.loadingButton.setTextColor(UiUtils.createButtonBrandingColorStateList(mLayoutConfig));
        holder.mBinding.loadingButton.setBackgroundColor(mLayoutConfig.getLoadingButtonColor());
        holder.mBinding.loadingButton.setBackground(UiUtils.createLoadingButtonBackground(parent.getContext(), mLayoutConfig));

        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, LoadingModel loadingModel) {
        holder.mBinding.loadingButton.setText(loadingModel.getText());
    }

    public static class ViewHolder extends BasePresenter.ViewHolder {

        private final LoadingItemBinding mBinding;

        public ViewHolder(LoadingItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}