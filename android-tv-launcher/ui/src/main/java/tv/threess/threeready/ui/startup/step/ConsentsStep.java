package tv.threess.threeready.ui.startup.step;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.account.model.account.IConsent;
import tv.threess.threeready.api.account.model.service.ConsentRequestScenario;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Startup step for launching the privacy settings.
 *
 * @author Barabas Attila
 * @since 2018.07.02
 */
public class ConsentsStep implements Step {
    private static final String TAG = Log.tag(ConsentsStep.class);

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);

    private Disposable mDisposable;
    private final ConsentRequestScenario mConsentRequestScenario;


    public ConsentsStep() {
        mConsentRequestScenario = ConsentRequestScenario.REGULAR_BOOT;
    }

    public ConsentsStep(@NonNull ConsentRequestScenario consentRequestScenario) {
        mConsentRequestScenario = consentRequestScenario;
    }

    @Override
    public void execute(StepCallback callback) {
        long consentUpdateLimit = Components.get(AppConfig.class).getCacheSettings().getConsentUpdateLimit(TimeUnit.MILLISECONDS);
        long calculatedConsentUpdateBound = System.currentTimeMillis() - consentUpdateLimit;
        long lastSavedUpdateTime = Settings.consentLastUpdateTime.get(calculatedConsentUpdateBound);

        // if lastSavedUpdateTime is in [calculatedConsentUpdateBound, System.currentTimeMillis()]
        // interval then don't need to update the consents
        if (lastSavedUpdateTime > calculatedConsentUpdateBound) {
            Log.d(TAG, "Consent update not needed because last update " +
                    Log.timestamp(lastSavedUpdateTime) + " is in the responsible bound.");
            callback.onFinished();
            return;
        }

        mAccountRepository.getConsents(mConsentRequestScenario.name())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<? extends IConsent>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable disposable) {
                        mDisposable = disposable;
                    }

                    @Override
                    public void onSuccess(@NonNull List<? extends IConsent> consents) {
                        Settings.consentLastUpdateTime.asyncEdit().put(System.currentTimeMillis());
                        Log.d(TAG, "Consents loaded for scenario: " + mConsentRequestScenario);
                        if (!consents.isEmpty()) {
                            mNavigator.showConsents(callback, consents.toArray(new IConsent[0]));
                            return;
                        }
                        callback.onFinished();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e(TAG, "Could not retrieve consents for boot screen.", e);
                        callback.onFinished();
                    }
                });
    }

    @Override
    public void cancel() {
        RxUtils.disposeSilently(mDisposable);
    }
}
