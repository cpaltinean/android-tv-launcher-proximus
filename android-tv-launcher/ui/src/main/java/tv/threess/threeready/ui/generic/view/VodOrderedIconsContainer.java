package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.util.AttributeSet;

import androidx.core.content.res.ResourcesCompat;

import java.util.List;

import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.ui.R;

/**
 * Container for the Vod related icons.
 *
 * @author Bondor David
 * @since 06/17/2019
 */
public class VodOrderedIconsContainer extends BaseOrderedIconsContainer {

    public VodOrderedIconsContainer(Context context) {
        this(context, null, 0);
    }

    public VodOrderedIconsContainer(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VodOrderedIconsContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Displays audio profiles given as param.
     * @param audioProfiles  The list of language
     *                       for which the icons needs to be displayed.
     */
    public void showAudioProfiles(List<String> audioProfiles) {
        if (audioProfiles == null) {
            return;
        }

        for (String profile : audioProfiles) {
            switch (profile) {
                case LocaleUtils.ENGLISH_LANGUAGE_CODE:
                    showEnglishLanguageIcon();
                    break;
                case LocaleUtils.DUTCH_LANGUAGE_CODE:
                    showDutchLanguageIcon();
                    break;
                case LocaleUtils.FRENCH_LANGUAGE_CODE:
                    showFrenchLanguageIcon();
                    break;
                // Support for other language can be added here.
            }
        }
    }

    /**
     * Show english language icon.
     */
    public void showEnglishLanguageIcon() {
        if (isIconDisplayed(Icons.AUDIO_LANGUAGE_EN)) {
            return;
        }
        Context context = getContext();
        showIcon(Icons.AUDIO_LANGUAGE_EN, ResourcesCompat.getDrawable(
                context.getResources(), R.drawable.ico_lang_en, context.getTheme()));
    }

    /**
     * Show french language icon.
     */
    public void showFrenchLanguageIcon() {
        if (isIconDisplayed(Icons.AUDIO_LANGUAGE_FR)) {
            return;
        }
        Context context = getContext();
        showIcon(Icons.AUDIO_LANGUAGE_FR, ResourcesCompat.getDrawable(
                context.getResources(), R.drawable.ico_lang_fr, context.getTheme()));
    }

    /**
     * Show dutch language icon.
     */
    public void showDutchLanguageIcon() {
        if (isIconDisplayed(Icons.AUDIO_LANGUAGE_NL)) {
            return;
        }
        Context context = getContext();
        showIcon(Icons.AUDIO_LANGUAGE_NL, ResourcesCompat.getDrawable(
                context.getResources(), R.drawable.ico_lang_nl, context.getTheme()));
    }
}