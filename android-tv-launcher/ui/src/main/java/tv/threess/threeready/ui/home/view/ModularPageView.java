package tv.threess.threeready.ui.home.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.resolver.DataSourceResolver;
import tv.threess.threeready.ui.generic.view.BaseVerticalGridView;
import tv.threess.threeready.ui.home.presenter.module.BaseModulePresenter;
import tv.threess.threeready.ui.home.presenter.module.ModulePresenterSelector;

/**
 * Custom vertical grid for a modular page.
 * <p>
 * Created by Barabas Attila on 4/18/2017.
 */
public class ModularPageView extends BaseVerticalGridView {
    private static final String TAG = Log.tag(ModularPageView.class);

    /**
     * for handling different presenters for extension
     */
    private final InterfacePresenterSelector mClassPresenterSelector;

    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    private ModulePresenterSelector mModulePresenterSelector;
    private DataSourceLoader mDataSourceLoader;

    private PageConfig mPageConfig;

    private final ArrayObjectAdapter<Object> mAdapter;

    private int mHeaderSpacing;

    private PageLoadListener mPageLoadListener;
    private PageTitleCallback mPageTitleCallback;
    private int mFadingSize = getResources().getDimensionPixelOffset(R.dimen.grid_fading_edge_default_height);
    private boolean mShowTopFading = false;

    public ModularPageView(Context context) {
        this(context, null);
    }

    public ModularPageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ModularPageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        addItemDecoration(new MarginItemDecoration());
        addOnChildViewHolderSelectedListener(mPrefetchListener);

        setFocusScrollStrategy(FOCUS_SCROLL_ALIGNED);
        setDescendantFocusability(FOCUS_AFTER_DESCENDANTS);
        setHasFixedSize(true);

        mAdapter = new ArrayObjectAdapter<>();
        mClassPresenterSelector = new InterfacePresenterSelector();
        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(mAdapter, mClassPresenterSelector);
        setAdapter(itemBridgeAdapter);
    }

    public ArrayObjectAdapter<Object> getObjectAdapter() {
        return mAdapter;
    }

    public InterfacePresenterSelector getClassPresenterSelector() {
        return mClassPresenterSelector;
    }

    /**
     * @param headerSpacing Extra header spacing for the page.
     */
    public void setHeaderSpacing(int headerSpacing) {
        mHeaderSpacing = headerSpacing;
    }

    public int getHeaderSpacing() {
        return mHeaderSpacing;
    }

    public void setFadingSize(int fadingSize) {
        mFadingSize =  fadingSize;
    }

    public void showTopFading(boolean showTopFading) {
        mShowTopFading = showTopFading;
        this.removeOnScrollListener(mOnScrollListener);
        if (mShowTopFading) {
            this.addOnScrollListener(mOnScrollListener);
        }
    }

    /**
     * Load and display the modules from the page config.
     * Only the modules with data available will be displayed.
     * @param pageConfig The page config which holds the module configuration.
     * @param listener Listener to get notification about the page load progress and status.
     * @param dataSourceResolver Resolver to map to module data sources to RX observables.
     */
    public void loadPageConfig(PageConfig pageConfig, PageLoadListener listener, DataSourceResolver dataSourceResolver) {
        if (pageConfig == null || pageConfig.getModules() == null) {
            Log.w(TAG, "Empty page. Nothing to display. " + pageConfig);
            return;
        }

        // Unsubscribe from all previous observables.
        cancelPageLoading();

        mPageConfig = pageConfig;
        mPageLoadListener = listener;

        // Initialize data source loaders.
        setWindowAlignment(WINDOW_ALIGN_LOW_EDGE);
        mDataSourceLoader = new DataSourceLoader(dataSourceResolver, mDataSourceCallback, pageConfig.getModules());
        mModulePresenterSelector = new ModulePresenterSelector(getContext(), mPageTitleCallback, mDataSourceLoader);
        mClassPresenterSelector.addClassPresenterSelector(ModuleData.class, mModulePresenterSelector);

        mDataSourceLoader.loadVisibleModules(mAppConfig.getPageInitModuleCount());

        initializeFading(mFadingSize);
    }

    public void loadPageConfig(PageConfig pageConfig, PageLoadListener listener) {
        loadPageConfig(pageConfig, listener, Components.get(DataSourceResolver.class));
    }

    /**
     * @return The currently loaded page config.
     */
    public PageConfig getPageConfig() {
        return mPageConfig;
    }

    /**
     * @return The config and data of the selected module.
     */
    @Nullable
    public ModuleData<?> getSelectedModuleData() {
        int selectedPosition = getSelectedPosition();
        if (selectedPosition < 0 || selectedPosition >= mAdapter.size()) {
            // No module selected.
            return null;
        }

        Object selectedObject = mAdapter.get(selectedPosition);
        if (selectedObject instanceof ModuleData) {
            return (ModuleData<?>) selectedObject;
        }

        // Not a module selected.
        return null;
    }

    /**
     * Cancel a previously started page loading.
     * All the module data source observers will be unsubscribed.
     */
    public void cancelPageLoading() {
        if (mDataSourceLoader != null) {
            mDataSourceLoader.unSubscribeAll();
        }
        mPageLoadListener = null;
    }

    /**
     * Find the position in the adapter for the given module.
     *
     * @param moduleConfig The module for which the position should be returned.
     * @return The adapter position of the module config.
     */
    private int findModuleExpectedPositionInAdapter(ModuleConfig moduleConfig) {
        List<ModuleConfig> moduleConfigs = mPageConfig.getModules();
        if (moduleConfigs == null) {
            return NO_POSITION;
        }

        int modulePosition = mPageConfig.getModules().indexOf(moduleConfig);

        for (int i = 0; i < mAdapter.size(); ++i) {
            if (!(mAdapter.get(i) instanceof ModuleData)) {
                continue;
            }

            ModuleData<?> moduleData = (ModuleData<?>) mAdapter.get(i);
            int adapterModulePosition = moduleConfigs.indexOf(moduleData.getModuleConfig());
            if (adapterModulePosition > modulePosition) {
                return i;
            }
        }

        // Not found.  Add it to the end.
        return mAdapter.size();
    }


    private int findModuleAdapterPosition(ModuleConfig moduleConfig) {
        for (int i = 0; i < mAdapter.size(); ++i) {
            if (!(mAdapter.get(i) instanceof ModuleData)) {
                continue;
            }

            ModuleData<?> moduleData = (ModuleData<?>) mAdapter.get(i);
            if (Objects.equals(moduleData.getModuleConfig(), moduleConfig)) {
                return i;
            }
        }

        return NO_POSITION;
    }

    private void notifyModuleLoaded(ModuleConfig moduleConfig) {
        if (mPageLoadListener != null) {
            mPageLoadListener.onModuleLoaded(moduleConfig);
        }
    }

    private void notifyModuleUpdated() {
        if (mPageLoadListener != null) {
            mPageLoadListener.onModuleUpdated();
        }
    }

    private void notifyPageLoaded(ModuleData<?> data) {
        RecyclerView.Adapter<?> adapter = getAdapter();

        if (adapter == null) {
            Log.d(TAG, "notifyPageLoaded: adpter null");
            return;
        }

        if (adapter.getItemCount() == 0) {
            Log.d(TAG, "notifyPageLoaded: adapter is goal");
        }

        setSelectedPosition(0);
        if (mPageLoadListener != null) {
            mPageLoadListener.onPageLoaded(mPageConfig, data);
        }
    }

    private void notifyPageEmpty() {
        if (mPageLoadListener != null) {
            mPageLoadListener.onPageEmpty(mPageConfig);
        }
    }

    public void setPageTitleCallback(PageTitleCallback pageTitleCallback) {
        mPageTitleCallback = pageTitleCallback;
    }

    private final OnChildViewHolderSelectedListener mPrefetchListener = new OnChildViewHolderSelectedListener() {

        @Override
        public void onChildViewHolderSelected(RecyclerView parent, ViewHolder child, int position, int subposition) {
            super.onChildViewHolderSelected(parent, child, position, subposition);

            if (!parent.hasFocus()) {
                // No focused. Skip perfecting.
                return;
            }

            if (mDataSourceLoader == null) {
                // Loading didn't start yet.
                return;
            }

            View lastChild = parent.getChildAt(parent.getChildCount() - 1);
            if (lastChild == null) {
                return;
            }

            // Calculate prefetch size.
            int lastVisiblePosition = parent.getChildAdapterPosition(lastChild);
            int prefetchUntil = lastVisiblePosition + mAppConfig.getPagePrefetchModuleCount();

            Log.d(TAG, "Selected position : " + position + ", prefetch until : " + prefetchUntil);
            mDataSourceLoader.loadVisibleModules(prefetchUntil);
        }
    };

    private final DataSourceLoader.DataSourceCallback mDataSourceCallback = new DataSourceLoader.DataSourceCallback() {

        @Override
        public Pair<Integer, Integer> getDefaultLimit(ModuleConfig moduleConfig) {
            BaseModulePresenter<?> modulePresenter = mModulePresenterSelector.getModulePresenter(moduleConfig);
            if (modulePresenter == null) {
                return Pair.create(0, 0);
            }
            return modulePresenter.getLimit(moduleConfig);
        }

        @Override
        public void onDataSourceLoaded(ModuleData<?> moduleData) {
            ModuleConfig moduleConfig = moduleData.getModuleConfig();

            Log.d(TAG, "Module displayed title: " + moduleConfig.getTitle());
            int moduleAdapterPosition = findModuleAdapterPosition(moduleConfig);

            boolean shouldDisplay = moduleData.isVisible();

            // Module not displayed yet.
            if (moduleAdapterPosition == NO_POSITION) {
                if (shouldDisplay) {
                    int moduleExpectedPosition = findModuleExpectedPositionInAdapter(moduleConfig);
                    mAdapter.add(moduleExpectedPosition, moduleData);

                    notifyModuleLoaded(moduleConfig);

                    if (mAdapter.size() == 1) {
                        notifyPageLoaded(moduleData);
                    }
                }

            // Module already displayed
            } else {
                if (shouldDisplay) {
                    mAdapter.replace(moduleAdapterPosition, moduleData);
                    notifyModuleUpdated();
                } else {
                    mAdapter.removeItems(moduleAdapterPosition, 1);
                }
            }

            // Block focusability on empty strips.
            if (mAdapter.size() == 0) {
                setDescendantFocusability(FOCUS_BLOCK_DESCENDANTS);
            } else {
                setDescendantFocusability(FOCUS_AFTER_DESCENDANTS);
            }

            ModuleConfig lastModule = mPageConfig.getLastModule();
            if (moduleData.getModuleConfig().equals(lastModule)) {
                if (getWindowAlignment() != WINDOW_ALIGN_BOTH_EDGE) {
                    setWindowAlignment(WINDOW_ALIGN_BOTH_EDGE);
                }

                if (mAdapter.size() == 0) {
                    notifyPageEmpty();
                }
            }

            if(mAdapter.size() == 0 && mDataSourceLoader.allModulesLoaded()) {
                notifyPageEmpty();
            }
        }
    };

    /**
     * these are default, the different implementation of @{@link tv.threess.threeready.ui.generic.fragment.BaseModularPageFragment}
     * will overwrite them if needed, along with other implementations
     */
    public interface PageLoadListener {

        /**
         * Called when a module in the page is loaded.
         * @param moduleConfig The module for which the loading completed.
         */
        default void onModuleLoaded(ModuleConfig moduleConfig) {
        }

        /**
         * Called when the module is updated/changed.
         */
        default void onModuleUpdated() {
        }

        /**
         * Called when the first module is loaded and displayed on the page.
         * @param pageConfig The config of the page on which the module loaded.
         * @param data The data of the first loaded module.
         */
        default void onPageLoaded(PageConfig pageConfig, ModuleData<?> data) {
        }

        /**
         * Called when all the modules on the page are loaded, but there is no data available.
         * @param pageConfig The page with no data available.
         */
        default void onPageEmpty(PageConfig pageConfig) {
        }
    }

    public interface PageTitleCallback {
        default String getPageTitle() {
            return null;
        }
    }

    private class MarginItemDecoration extends RecyclerView.ItemDecoration {

        @Override
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull State state) {
            super.getItemOffsets(outRect, view, parent, state);

            // First item.
            if (parent.getChildAdapterPosition(view) == 0) {
                // Gallery spacing.
                if (view instanceof GalleryModuleView) {
                    view.setTranslationZ(-1);
                    outRect.bottom = getResources().getDimensionPixelOffset(R.dimen.gallery_a1_module_bottom_margin);

                // Header spacing.
                } else {
                    outRect.top = mHeaderSpacing;
                }
            }
        }
    }

    private final RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
        int scrolledY = 0;

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            scrolledY += dy;
            if (mAdapter.size() == 0) {
                hideFading();
            }

            View focusChild = getFocusedChild();
            if (focusChild != null) {
                if (getChildAdapterPosition(focusChild) == 0) {
                    hideFading();
                } else {
                    showFading(mFadingSize);
                }
            }
        }
    };
}
