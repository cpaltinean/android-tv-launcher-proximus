/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.player.fragment;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.MemoryCache;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.config.model.FeatureControl;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.model.TimeRange;
import tv.threess.threeready.api.generic.receivers.HdmiConnectivityReceiver;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.helper.ErrorType;
import tv.threess.threeready.api.log.model.Error;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.netflix.model.SourceType;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.assistent.PlaybackAssistantController;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.PlayerStateManager;
import tv.threess.threeready.player.RestartMode;
import tv.threess.threeready.player.command.PauseCommand;
import tv.threess.threeready.player.command.ResumeCommand;
import tv.threess.threeready.player.command.base.JumpCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.ControlFactory;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.plugin.PlaybackPlugin;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.player.result.EventResult;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.PlayerFragmentBinding;
import tv.threess.threeready.ui.epg.fragment.EpgFragment;
import tv.threess.threeready.ui.generic.activity.BaseActivity;
import tv.threess.threeready.ui.generic.activity.MainActivity;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.fragment.BasePlayerFragment;
import tv.threess.threeready.ui.generic.hint.HintManager;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.notification.NotificationManager;
import tv.threess.threeready.ui.generic.player.plugin.BabyChannelWarningPlugin;
import tv.threess.threeready.ui.generic.player.plugin.BaseBingeWatchingPlugin;
import tv.threess.threeready.ui.generic.player.plugin.RecordingBingeWatchingPlugin;
import tv.threess.threeready.ui.generic.player.plugin.ReplayPlaylistPlugin;
import tv.threess.threeready.ui.generic.player.plugin.ReplayPlusOfferPlugin;
import tv.threess.threeready.ui.generic.player.plugin.VodBingeWatchingPlugin;
import tv.threess.threeready.ui.generic.player.view.IZappingInfoStateChangeListener;
import tv.threess.threeready.ui.generic.player.view.ZappingInfoView;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.pvr.RecordingActionHelper;
import tv.threess.threeready.ui.startup.StartupFlow;
import tv.threess.threeready.ui.utils.SystemUtils;
import static tv.threess.threeready.ui.utils.UiUtils.runOnUiThread;

/**
 * Player ui component which should be always displayed in the background.
 * <p>
 * Created by Barabas Attila on 4/24/2017.
 */
public class PlayerFragment extends BaseFragment implements IPlaybackCallback {
    public static final String TAG = Log.tag(PlayerFragment.class);

    public static PlayerFragment newInstance(StepCallback stepCallback) {
        Bundle args = new Bundle();

        PlayerFragment fragment = new PlayerFragment();
        fragment.setArguments(args);
        fragment.mStepCallback = stepCallback;
        return fragment;
    }

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final NotificationManager mNotificationManager = Components.get(NotificationManager.class);
    private final HintManager mHintManager = Components.get(HintManager.class);
    private final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);
    private final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);
    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final ControlFactory mControlFactory = Components.get(ControlFactory.class);
    private final RecordingActionHelper mRecordingActionHelper = Components.get(RecordingActionHelper.class);
    private final StartupFlow mStartupFlow = Components.get(StartupFlow.class);
    private final PlayerStateManager mPlayerStateManager = Components.get(PlayerStateManager.class);
    private final TvRepository mTvRepository = Components.get(TvRepository.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);
    private final HdmiConnectivityReceiver mHdmiReceiver = Components.get(HdmiConnectivityReceiver.class);

    StepCallback mStepCallback;

    protected PlayerFragmentBinding mBinding;

    private volatile boolean mTriggerChannelScan = true;
    private BabyChannelWarningPlugin mBabyChannelPlugin;
    private ReplayPlusOfferPlugin mReplayPlusOfferPlugin;
    private ReplayPlaylistPlugin mReplayPlaylistPlugin;
    private BaseBingeWatchingPlugin mVodBingeWatchingPlugin;
    private BaseBingeWatchingPlugin mRecordingBingeWatchingPlugin;

    private PlaybackAssistantController mPlaybackAssistantController;

    private Disposable mLastPlayedChannelIdDisposable;

    private final Runnable mLoadingIndicatorRunnable = () -> {
        if (!mBinding.loadingSpinner.isShown() && mBinding.loadingSpinner.getAlpha() != 0) {
            reportLoadingSpinnerState(PlaybackEvent.StartLoadingSpinner);
        }
        mBinding.loadingSpinner.setVisibility(View.VISIBLE);
    };

    private final HdmiConnectivityReceiver.Listener mHdmiStateListener = new HdmiConnectivityReceiver.Listener() {
        @Override
        public void onConnectionChanged(boolean isConnected) {
            Log.d(TAG, "onConnectionChanged() called with: isConnected = [" + isConnected + "] on: " + Thread.currentThread().getName());
            if (!isConnected) {
                Log.d(TAG, "Hiding player error view");
                runOnUiThread(mBinding.errorView::hideError);
            }
        }

        @Override
        public void onHdcpStateChanged(boolean active) {
            Log.d(TAG, "HDCP state changed active : " + active);
            if (!active) {
                // Nothing to do. HDCP not active.
                return;
            }

            if (!mStartupFlow.isStartupFinished()) {
                Log.d(TAG, "Startup is not finished, ignoring HDCP change");
                return;
            }

            MemoryCache.LastUserActivity.edit().put(System.currentTimeMillis());
            restartLastCommand(StartAction.Hdmi);
        }
    };

    private final InternetChecker.OnStateChangedListener mOnStateChangedListener = (state) -> {
        if (!state.isInternetStateChanged()) {
            // internet availability not changed, do not re-tune
            return;
        }

        if (!mStartupFlow.isStartupFinished()) {
            // startup is not finished, do not tune
            return;
        }

        if (!mInternetChecker.isInternetAvailable()) {
            // Internet is not available, do not tune
            return;
        }
        restartLastCommand(StartAction.Internet);
    };

    private void restartLastCommand(StartAction action) {
        Log.d(TAG, "Restart the last command, Hdcp or internet is back.");
        // Retry the last start command in the player.
        // If there is no last successful command then tune to the last tuned live channel
        if (!mPlaybackDetailsManager.isCommandStarting(StartCommand.class)) {
            if (mPlaybackDetailsManager.reTryLastTuned(action, false) < 0) {
                Log.d(TAG, "No last tuned command, tune to last channel.");
                mNavigator.tuneToLastChannel(action, true, false);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = PlayerFragmentBinding.inflate(inflater, container, false);

        Components.registerInstance(mBinding.channelInfo);

        mControlFactory.setTvView(mBinding.playerView);
        mControlFactory.setTrickPlaySurface(mBinding.trickplayView, mBinding.trickplayBackground);

        mPlaybackDetailsManager.registerPlugin(mPlaybackPlugin);

        // Playback plugin to display baby content warning.
        mBabyChannelPlugin = new BabyChannelWarningPlugin();
        mPlaybackDetailsManager.registerPlugin(mBabyChannelPlugin);

        // Playback plugin to append the next replay to the playlist.
        mReplayPlaylistPlugin = new ReplayPlaylistPlugin();
        mPlaybackDetailsManager.registerPlugin(mReplayPlaylistPlugin);

        // Register plugin to enable binge watching functionality
        mVodBingeWatchingPlugin = new VodBingeWatchingPlugin();
        mPlaybackDetailsManager.registerPlugin(mVodBingeWatchingPlugin);

        mRecordingBingeWatchingPlugin = new RecordingBingeWatchingPlugin();
        mPlaybackDetailsManager.registerPlugin(mRecordingBingeWatchingPlugin);

        mPlaybackAssistantController = new PlaybackAssistantController(getActivity(), mPlaybackDetailsManager);
        mPlaybackAssistantController.createMediaSession();

        // Playback plugin to offer replay plus upselling.
        mReplayPlusOfferPlugin = new ReplayPlusOfferPlugin();
        mPlaybackDetailsManager.registerPlugin(mReplayPlusOfferPlugin);

        mPlaybackDetailsManager.onCreate();
        mPlaybackDetailsManager.registerCallback(this);

        // Initialize Hint Manager
        mHintManager.init();

        mBinding.jumpIndicator.setPlaybackManager(mPlaybackDetailsManager);

        mBinding.channelInfo.addStateChangeListener(mZappingInfoStateChangeListener);

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Workaround for the surface drawing issue.
        // Recalculate the transparent area when the views are added.
        mBinding.playerView.getParent()
                .requestTransparentRegion(mBinding.playerView);

        mBinding.playbackLockView.setTvView(mBinding.playerView);

        if (mStepCallback != null) {
            mStepCallback.onFinished();
        }

        mNavigator.addContentChangeListener(mContentFragmentChangeListener);
    }

    public ZappingInfoView getChannelInfoView() {
        return mBinding.channelInfo;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");

        MainActivity activity = mNavigator.getActivity();
        if (!activity.wasStartedForSearchResult() && activity.isStartupFinished()
                && !mPlaybackDetailsManager.isCommandStarting(StartCommand.class)) {
            if (activity.isStartedWithKey(KeyEvent.KEYCODE_GUIDE)) {
                mNavigator.tuneToLastChannelAndShowEpg();
            } else if (!activity.isStartedWithKey(KeyEvent.KEYCODE_TV_RADIO_SERVICE)) {
                mNavigator.tuneToLastChannel(StartAction.ActivityStart, true, false);
            }
        }

        SystemUtils.requestAudioFocus(activity);
        SystemUtils.resetVolume(activity);

        mInternetChecker.addStateChangedListener(mOnStateChangedListener);
        mHdmiReceiver.addStateChangedListener(mHdmiStateListener);

        //re-act in case the last played channel changed
        RxUtils.disposeSilently(mLastPlayedChannelIdDisposable);
        mTvRepository.getLastPlayedChannelIdChange()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        mLastPlayedChannelIdDisposable = d;
                    }

                    @Override
                    public void onNext(@NonNull String lastPlayedChannelId) {
                        Log.i(TAG, "Last played channel id change detected: " + lastPlayedChannelId);
                        if (shouldReTune(lastPlayedChannelId)) {
                            mPlaybackDetailsManager.startChannel(StartAction.Undefined, lastPlayedChannelId, RestartMode.Forced);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e(TAG, "Could not get the last changed played channel id!", e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        mAutoUpdateRunnable.start();
    }

    /**
     * Checks if a re-tune is needed in case the last played channel changed (line-up changed based on triggering).
     *
     * @param newChannelId the new channel id
     * @return true if the current played back channel is not in the lineup anymore or the channel id changed under the saved channel number
     */
    private boolean shouldReTune(String newChannelId) {
        String currentChannelId = mPlaybackDetailsManager.getChannelId();
        //check if the current channel is still in the line-up (Replay, PVR, IPTV)
        if (!TextUtils.isEmpty(currentChannelId) && mNowOnTvCache.getChannel(currentChannelId) == null) { //there is a channel playing
            Log.d(TAG, "Currently tuned channel not in the lineup anymore: re-tune!");
            return true;
        }

        //check if we need to re-tune because the channel id changed (based on the last saved number) - only in case od IPTV playback
        TvChannel currentChannel = mPlaybackDetailsManager.getChannelData();
        if (currentChannel != null && !currentChannel.getId().equals(newChannelId)) {
            Log.d(TAG, "Currently tuned channel number changed: re-tune!");
            return true;
        }

        return false;
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop()");

        // do not show the previous player ui when exiting third party app

        if (mNavigator.isPlayerUIOnTop()) {
            mNavigator.hideContentOverlay();
        }

        if (!mPlaybackDetailsManager.inPlaybackDomain(PlaybackDomain.App)) {
            mPlaybackDetailsManager.forceStop();
        }
        SystemUtils.abandonAudioFocus(getActivity());

        mInternetChecker.removeStateChangedListener(mOnStateChangedListener);
        mHdmiReceiver.removeStateChangedListener(mHdmiStateListener);

        RxUtils.disposeSilently(mLastPlayedChannelIdDisposable);
        mAutoUpdateRunnable.stop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mBinding.channelInfo.removeStateChangeListener(mZappingInfoStateChangeListener);

        mPlaybackDetailsManager.unregisterCallback(this);

        mControlFactory.setTrickPlaySurface(null, mBinding.trickplayBackground);
        if (mBinding.trickplayView.getHolder().getSurface() != null) {
            mBinding.trickplayView.getHolder().getSurface().release();
        }
        mPlaybackAssistantController.releaseSession();

        mHintManager.destroy();

        mPlaybackDetailsManager.onDestroy();

        mPlaybackDetailsManager.unregisterPlugin(mPlaybackPlugin);
        mPlaybackDetailsManager.unregisterPlugin(mBabyChannelPlugin);
        mPlaybackDetailsManager.unregisterPlugin(mReplayPlaylistPlugin);
        mPlaybackDetailsManager.unregisterPlugin(mVodBingeWatchingPlugin);
        mPlaybackDetailsManager.unregisterPlugin(mRecordingBingeWatchingPlugin);
        mPlaybackDetailsManager.unregisterPlugin(mReplayPlusOfferPlugin);

        mNavigator.removeContentChangeListener(mContentFragmentChangeListener);
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        if (isUnskippableBlocked(event)) {
            return true;
        }

        PlaybackType playerType = mPlaybackDetailsManager.getPlayerType();
        switch (event.getKeyCode()) {

            case KeyEvent.KEYCODE_TV:
                boolean hideContentOverlayWithAnimation = false;

                if (mNavigator.isInBackground()) {
                    runOnUiThread(() -> {
                        Log.d(TAG, "TV button pressed. Hide content overlay. Show playback.");
                        mNavigator.hideContentOverlay();
                        mNavigator.clearAllDialogs();
                    });
                    return true;
                }

                BaseFragment currentFragment = mNavigator.getContentFragment();

                mNavigator.clearAllDialogs();

                // Content overlay is not displayed.
                if (!mNavigator.isContentOverlayDisplayed()) {

                    if (mPlaybackDetailsManager.isLiveBroadcastPlaying()) {

                        // Check if current channel is at live position.
                        if (!mPlaybackDetailsManager.isLiveChannelPlaying(mPlaybackDetailsManager.getChannelId())) {
                            Log.d(TAG, "TV button pressed. Show start live playback for current channel.");
                            mPlaybackDetailsManager.startChannel(StartAction.BtnTv, mPlaybackDetailsManager.getChannelId(),
                                    mPlaybackDetailsManager.isFavorite(), RestartMode.Forced);
                        } else {
                            Log.d(TAG, "TV button pressed. Show start live playback for current channel.");
                            mNavigator.showEpgForPlayback();
                        }
                        return true;
                    }

                    // Content overlay is displayed.
                } else if (currentFragment != null) {
                    if (EpgFragment.TAG.equals(currentFragment.getTag())) {
                        hideContentOverlayWithAnimation = true;
                    }

                    if (mPlaybackDetailsManager.isLiveBroadcastPlaying()) {

                        // Check if current channel is at live position.
                        if (!mPlaybackDetailsManager.isLiveChannelPlaying(mPlaybackDetailsManager.getChannelId())) {
                            Log.d(TAG, "TV button pressed. Show start live playback for current channel.");
                            mPlaybackDetailsManager.startChannel(StartAction.BtnTv, mPlaybackDetailsManager.getChannelId(),
                                    mPlaybackDetailsManager.isFavorite(), RestartMode.Forced);
                        }
                    }
                }

                // Not live player is playing.
                if (!(mNavigator.isContentOverlayDisplayed()
                        && mPlaybackDetailsManager.isLiveBroadcastPlaying())) {
                    Log.d(TAG, "TV button pressed. Tune to last played channel.");
                    mNavigator.tuneToLastChannel(StartAction.BtnTv);
                }

                // Hide content overlay.
                if (hideContentOverlayWithAnimation) {
                    mNavigator.hideContentOverlayWithFadeOut();
                } else {
                    mNavigator.hideContentOverlay();
                    mNavigator.clearAllDialogs();
                }

                return true;

            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                if (!mNavigator.canHandleMediaKeys()) {
                    if (!mNavigator.clearAllDialogsByKey(KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE)) {
                        mNavigator.showPlayerUI(StartAction.Resume, null);
                    }
                    return false;
                }

                handlePlayPauseButton();
                return true;
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                if (mPlaybackDetailsManager.getPlayerType() == PlaybackType.RadioIpTv) {
                    mBinding.radioChannelLogo.pauseAnimation();
                }
                IContentItem contentItem = mPlaybackDetailsManager.getPlayingItem();
                if (mParentalControlManager.isRestricted(contentItem, true)) {
                    // Show PIN. Restricted content.
                    mNavigator.showParentalControlUnblockDialog();
                    return true;
                }

                if (mPlaybackDetailsManager.isAppChannelPlaying()) {
                    mNavigator.checkSubscriptionAndOpenApp(StartAction.RemoteBtn,
                            mPlaybackDetailsManager.getChannelData(), SourceType.DirectChannel);
                    return true;
                }

                if (mPlaybackDetailsManager.isRegionalChannelPlaying()) {
                    return true;
                }

                if (!mNavigator.isContentOverlayDisplayed() ||
                        (mNavigator.isContentOverlayDisplayed() && mNavigator.getContentFragment() == null)) {
                    mNavigator.showPlayerUI();
                    return true;
                }
                break;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
            case KeyEvent.KEYCODE_DPAD_LEFT:
                if (onMediaKeyUp(event)) {
                    return true;
                }
                break;
            case KeyEvent.KEYCODE_CHANNEL_UP:
            case KeyEvent.KEYCODE_CHANNEL_DOWN:
                if (mNavigator.isStreamDisplayedOnTop()
                        && (playerType == PlaybackType.LiveTvIpTv
                        || playerType == PlaybackType.RadioIpTv
                        || playerType == PlaybackType.Replay)) {
                    mBinding.channelInfo.onKeyUp(event.getKeyCode(), event);
                    return true;
                }
                break;
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                if (!mNavigator.canHandleMediaKeys()) {
                    return false;
                }

                if (!onMediaKeyUp(event)) {
                    mNavigator.showPlayerUI();
                }

                return true;
            case KeyEvent.KEYCODE_GUIDE:
                mNavigator.showEpgLayoutFromGuideButton();
                break;
            case KeyEvent.KEYCODE_TV_RADIO_SERVICE:
                goToLastRadioChannel();
                mNavigator.clearAllDialogs();
                break;
            case KeyEvent.KEYCODE_MEDIA_STOP:
                return true;
            case KeyEvent.KEYCODE_CAPTIONS:
                mNavigator.showPlayerOptionsDialog();
                return true;
            case KeyEvent.KEYCODE_DPAD_UP:
            case KeyEvent.KEYCODE_DPAD_DOWN:
                if (!mNavigator.isContentOverlayDisplayed()) {
                    switch (mPlaybackDetailsManager.getPlayerType()) {
                        case RadioIpTv:
                            mBinding.radioChannelLogo.hideAndPauseAnimation();
                            mNavigator.showEpgForPlayback();
                            return true;
                        case Recording:
                            IRecording recording = mPlaybackDetailsManager.getRecordingPlayerData();
                            if (recording != null && recording.isLiveRecording()) {
                                mNavigator.showEpgForPlayback();
                            } else {
                                mNavigator.showPlayerUI();
                            }
                            return true;
                        case Vod:
                        case VodTrailer:
                            mNavigator.showPlayerUI();
                            return true;
                        default:
                            mNavigator.showEpgForPlayback();
                            return true;
                    }
                }
            case KeyEvent.KEYCODE_MEDIA_RECORD:
                if (!mNavigator.isContentOverlayDisplayed()
                        && mPlaybackDetailsManager.isLivePlaybackType()) {
                    mRecordingActionHelper
                            .handleRecKeyAction(mPlaybackDetailsManager.getLivePlayerData(), null);
                    return true;
                }
                break;
        }
        return super.onKeyUp(event);
    }

    private void handlePlayPauseButton() {
        if (mPlaybackDetailsManager.resumePause()) {
            mNavigator.showPlayerUI();
        }
    }

    @Override
    public boolean onKeyDown(KeyEvent event) {
        if (isUnskippableBlocked(event)) {
            return true;
        }

        PlaybackType playerType = mPlaybackDetailsManager.getPlayerType();
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                if (onMediaKeyDown(event)) {
                    return true;
                }
                break;
            case KeyEvent.KEYCODE_CHANNEL_UP:
            case KeyEvent.KEYCODE_CHANNEL_DOWN:
                if (mBinding.radioChannelLogo.isRunning()) {
                    mBinding.radioChannelLogo.stopAnimation();
                }
                if (mNavigator.isStreamDisplayedOnTop() &&
                        (playerType == PlaybackType.LiveTvIpTv ||
                                playerType == PlaybackType.RadioIpTv ||
                                playerType == PlaybackType.Replay)) {
                    mBinding.channelInfo.onKeyDown(event.getKeyCode(), event);
                    return true;
                }
                break;
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                if (!mNavigator.canHandleMediaKeys()) {
                    return false;
                }

                if (!onMediaKeyDown(event)) {
                    StartAction startAction = event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_REWIND ?
                            StartAction.Rewind : StartAction.Forward;

                    mNavigator.showPlayerUI(startAction, event);
                }
                return true;

            case KeyEvent.KEYCODE_DPAD_CENTER:
                if (mPlaybackDetailsManager.isRegionalChannelPlaying()) {
                    if (!mNavigator.isContentOverlayDisplayed()) {
                        Log.d(TAG, "Regional channel clicked, open webpage.");
                        mNavigator.openDeepLinkForChannel(mAppConfig.getRegionalChannelLink(), mPlaybackDetailsManager.getChannelData());
                    }
                }
                return true;
        }
        return super.onKeyDown(event);
    }

    /**
     * Remote Control blocked events while unskippable advertisement is playing
     *
     * Fast Forward FFW	Action not allowed, cannot FW on the ad stream	"Fast forwarding not allowed during commercial break"
     * Fast Rewind RW  	Action not allowed, cannot RW on the ad stream	"Rewinding is not allowed during commercial break"
     * Pause/Play      	Pause: Pause on the video ad + Show "Pause" icon in the middle, until press Play
     * Stop            	global behavior (RCU STOP button)
     * Record          	no action
     * OK              	no action
     * UP/DOWN         	no action
     * RIGHT           	Action not allowed, cannot jump forward on the ad stream	same message as on FW
     * LEFT            	Action not allowed, cannot jump back on the ad stream	same message as on RW
     * Back            	global behavior, Back to previous screen
     * Home            	global behavior, go to Home page
     * Guide           	global behavior, open TV Guide
     * Volume +/-      	global behavior, increase/decrease volume
     * Channel +/-     	no action (behavior in the recordings stream)
     * Mute            	mute / un-mute
     * Mic             	Launch GA
     * Subtitles       	no action
     * 0-9 keys        	global behavior, zap to channel
     * Netlix          	global behavior, launch Netflix
    */
    public boolean isUnskippableBlocked(KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_MEDIA_RECORD:
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_DPAD_UP:
            case KeyEvent.KEYCODE_DPAD_DOWN:
            case KeyEvent.KEYCODE_CAPTIONS:
                return mNavigator.isStreamDisplayedOnTop() && mPlaybackDetailsManager.isUnskippablePosition();
        }
        return false;
    }

    @Override
    public void onCommandSuccess(PlaybackCommand cmd, PlaybackDetails details) {
        if (cmd instanceof ResumeCommand || cmd instanceof JumpCommand) {
            if (details.isUnskippablePosition()) {
                postOnUiThread(mNavigator::hideContentOverlay);
            }
        }
        if (cmd instanceof PauseCommand) {
            if (details.isUnskippablePosition()) {
                postOnUiThread(mNavigator::showPlayerUI);
            }
            hideLoadingIndicator();
        }

        // Hide error message when the streaming started.
        if (details.getState() == PlaybackState.Started) {
            runOnUiThread(mBinding.errorView::hideError);
        }
        if (cmd instanceof StartCommand) {
            postOnUiThread(mAutoUpdateRunnable);
            hideLoadingIndicator();
        }
    }

    @Override
    public void onCommandFailure(PlaybackCommand cmd, PlaybackDetails details, PlaybackError error) {
        postOnUiThread(() -> {
            PlaybackError.Type reason = error.getReason();
            Log.d(TAG, "onCommandFailure() called with: cmd = [" + cmd + "], details = ["
                    + details + "], error = [" + error + "]");

            BaseActivity activity = mNavigator.getActivity();
            if (activity == null || activity.isInBackground()) {
                // Activity is in background. Nothing to do.
                return;
            }

            // Current program is blacklisted. Show notification and jump to live.
            if (reason == PlaybackError.Type.BLACKLISTED) {
                mNotificationManager.showBlacklistedNotification();
                mNavigator.tuneToLastChannel(StartAction.ReplayEnded);
                return;
            }

            if (cmd instanceof PauseCommand && cmd.getDomain() == PlaybackDomain.LiveTvIpTv) {
                mNotificationManager.showCantPauseNotification();
                return;
            }

            if (!(cmd instanceof StartCommand) || cmd.getDomain() == PlaybackDomain.TrickPlay) {
                // ignore non start and trickplay related commands
                return;
            }

            if (reason == PlaybackError.Type.CANCELED) {
                // canceled start command, probably by a stop or another start command
                return;
            }

            // Failed because of no playback option. Try to do a re-scan.
            StartCommand startCommand = (StartCommand) cmd;
            if (mTriggerChannelScan && reason == PlaybackError.Type.NO_PLAYBACK_OPTION) {
                StartAction action = startCommand.getStartAction();
                mNavigator.showChannelScanDialog(action != StartAction.Restart);
                mTriggerChannelScan = false;
                hideLoadingIndicator();
                return;
            }

            if (startCommand.getRetryCount() < reason.getAutomaticRetryCount()) {
                mPlaybackDetailsManager.reTryLastTuned(true);
                return;
            }

            showErrorMessage(error);
        });
    }

    @Override
    public void onPlaybackEvent(PlaybackEvent event, PlaybackDetails details, PlaybackError error) {
        if (!details.getDomain().exclusive) {
            return;
        }
        postOnUiThread(() -> {
            switch (event) {
                case PlaybackUnavailable:
                    onCommandFailure(mPlaybackDetailsManager.getLastCommand(), details, error);
                    break;

                // The next blacklisted slot reached. Display notification and tune to live.
                case NextContentBlacklisted:
                    mNotificationManager.showBlacklistedJumpLiveNotification();
                    mNavigator.tuneToLastChannel(StartAction.ReplayEnded);
                    break;

                // The previous blacklisted slot reached. Display notification.
                case PreviousContentBlacklisted:
                    mNotificationManager.showBlacklistedNotification();
                    break;

                case BufferEndReached:
                    mNotificationManager.showCantPauseNotification();
                    break;

                case EndReached:
                    mPlayerStateManager.handleEndReachedAction();
                    break;

                case StartInitiated:
                    showLoadingIndicator();
                    mBinding.errorView.hideError();
                    break;

                case PlaybackFailed:
                case BackendSessionInvalid:
                case PlaybackDisconnected:
                    showErrorMessage(error);
                    break;

                case PlaybackBuffering:
                    if (mPlaybackDetailsManager.getExclusiveDetails().getState() == PlaybackState.Started) {
                        showLoadingIndicator();
                    }
                    break;
                case TracksChanged:
                    handlePlayerTypeChangeEvent();
                    break;

                case PlaybackStarted:
                case PlaybackItemChanged:
                    hideLoadingIndicator();
                    if (mBinding.errorView.getVisibility() == View.VISIBLE) {
                        mBinding.errorView.hideError();
                    }
                    break;
                case StartLoadingSpinner:
                    mBinding.radioChannelLogo.hideAndPauseAnimation();
                    break;
                case StopLoadingSpinner:
                    if (mPlaybackDetailsManager.getPlayerType() == PlaybackType.RadioIpTv) {
                        mBinding.radioChannelLogo.resumeAnimation();
                    }
                    break;
            }
            Log.d(TAG, "PlaybackEvent[" + event + "]");
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mBinding.radioChannelLogo.isRunning()) {
            mBinding.radioChannelLogo.hideAndPauseAnimation();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPlaybackDetailsManager.getPlayerType() == PlaybackType.RadioIpTv) {
            mBinding.radioChannelLogo.startChannelLogoAnimation(mPlaybackDetailsManager.getChannelData());
        }
    }

    private boolean onMediaKeyUp(KeyEvent event) {
        if (mNavigator.isStreamDisplayedOnTop() && mPlaybackDetailsManager.isUnskippablePosition()) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_LEFT:
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                case KeyEvent.KEYCODE_MEDIA_REWIND:
                case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                    mNavigator.showPlayerUI(StartAction.Undefined, event);
                    return false;
            }
        }

        if (!mNavigator.isContentOverlayDisplayed() && !mPlaybackDetailsManager.isAppChannelPlaying()
                && !mPlaybackDetailsManager.isRegionalChannelPlaying() && !Components.get(FeatureControl.class).isOffline()) {
            return mBinding.jumpIndicator.onKeyUp(event.getKeyCode(), event);
        }
        return false;
    }

    private boolean onMediaKeyDown(KeyEvent event) {
        if (mNavigator.isStreamDisplayedOnTop() && mPlaybackDetailsManager.isUnskippablePosition()) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_LEFT:
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                case KeyEvent.KEYCODE_MEDIA_REWIND:
                case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                    mNavigator.showPlayerUI(StartAction.Undefined, event);
                    return false;
            }
        }

        if (!mNavigator.isContentOverlayDisplayed() && !mPlaybackDetailsManager.isAppChannelPlaying()
                && !mPlaybackDetailsManager.isRegionalChannelPlaying()) {
            return mBinding.jumpIndicator.onKeyDown(event.getKeyCode(), event);
        }
        return false;
    }

    public void triggerParentalControl() {
        mBinding.playbackLockView.updateParentalControlRestriction();
    }

    /**
     * Display error message in UI
     *
     * @param error the failure reason (base on this reason a message is showed)
     */
    private void showErrorMessage(PlaybackError error) {
        Log.d(TAG, "showErrorMessage: " + error.getErrorCode(), error);

        runOnUiThread(() -> {
            if (getView() == null || mPlaybackDetailsManager.isCommandStarting(StartCommand.class)) {
                return;
            }

            hideLoadingIndicator();
            mBinding.errorView.showError(getPlayerErrorFromThrowable(error));
            if (error.getReason().isManualRetry()) {
                mBinding.errorView.setOnRetryClickListener(v -> {
                    mPlaybackDetailsManager.reTryLastTuned(true);
                });
            } else {
                mBinding.errorView.setOnRetryClickListener(null);
            }
            mBinding.radioChannelLogo.stopAnimation();
        });
    }

    /**
     * Playback manager plugin to avoid initiate of playback start while the activity is in background.
     */
    private final PlaybackPlugin mPlaybackPlugin = new PlaybackPlugin() {

        @Override
        public void onBeforeExecute(PlaybackCommand command, PlaybackEntireState state, PlaybackDetailsBuilder details) throws PlaybackError {
            if (command instanceof StartCommand && command.getDomain() != PlaybackDomain.App && mNavigator.isInBackground()) {
                throw new PlaybackError(PlaybackError.Type.TUNE_IN_BACKGROUND, "Cannot start playback while the activity is in background.");
            }
        }
    };

    /**
     * @return True if the playback lock view is visible, otherwise false.
     */
    public boolean isPlaybackLocked() {
        return mBinding.playbackLockView.isLocked();
    }

    public void blockTrickPlay() {
        mPlaybackDetailsManager.setTrickPlayBlocked(true);
    }

    public void unblockTrickPlay() {
        mPlaybackDetailsManager.setTrickPlayBlocked(false);
    }

    public Error getPlayerErrorFromThrowable(PlaybackError error) {
        return new Error.Builder(ErrorType.PLAYER)
                .customCode(error.getErrorCode())
                .titleTranslationKey(error.getTitleTranslationKey())
                .bodyTranslationKey(error.getBodyTranslationKey())
                .customTitle(mTranslator.get(error.getTitleTranslationKey()))
                .build();
    }

    private void handlePlayerTypeChangeEvent() {
        mBinding.radioChannelLogo.resetChannelId();
        if (mPlaybackDetailsManager.getPlayerType() == PlaybackType.RadioIpTv) {
            mBinding.radioChannelLogo.startChannelLogoAnimation(mPlaybackDetailsManager.getChannelData());
            return;
        }
        mBinding.radioChannelLogo.stopAnimation();
    }

    private void showLoadingIndicator() {
        mBinding.loadingSpinner.postDelayed(mLoadingIndicatorRunnable,
                mAppConfig.getPlaybackSettings().getSpinnerDelayTimeout(TimeUnit.MILLISECONDS));
    }

    public void hideLoadingIndicator() {
        mBinding.loadingSpinner.removeCallbacks(mLoadingIndicatorRunnable);
        mBinding.loadingSpinner.post(() -> {
            if (mBinding.loadingSpinner.isShown() && mBinding.loadingSpinner.getAlpha() != 0) {
                reportLoadingSpinnerState(PlaybackEvent.StopLoadingSpinner);
            }
            mBinding.loadingSpinner.setVisibility(View.INVISIBLE);
        });
    }

    public void setLoadingIndicatorColor(boolean playerUiIsShown) {
        int resId = playerUiIsShown ? R.color.loading_spinner_color_on_player_ui : R.color.loading_spinner_color_on_player;
        mBinding.loadingSpinner.getIndeterminateDrawable().setColorFilter(getResources().getColor(resId, null), PorterDuff.Mode.SRC_IN);
    }

    public void goToChannel(StartAction action, String channelId, boolean isFavourite) {
        Log.d(TAG, "go to channel: " + channelId);

        if (mPlaybackDetailsManager.isChannelPlaying(channelId) && mPlaybackDetailsManager.isInPLTVBuffer()) {
            mNavigator.showBackToLiveAlert(changeChannel -> mPlaybackDetailsManager.jumpToLive(StartAction.TVPageContinue));
            return;
        }

        // If the current program type is Replay, or the current program is running in the PLTV buffer, the user should see a Back to live dialog.
        if (mPlaybackDetailsManager.inPlaybackType(PlaybackType.Replay) || mPlaybackDetailsManager.isInPLTVBuffer()) {
            mNavigator.showBackToLiveAlert(changeChannel -> mBinding.channelInfo.zapToChannel(StartAction.TVPageContinue, channelId, isFavourite));
            return;
        }

        mBinding.channelInfo.zapToChannel(action, channelId, isFavourite);
    }

    /**
     * Opens the last played radio channel. If this is not available, opens the first radio channel in the list.
     * If radio channels are not available at all, opens the first channel in the list.
     */
    private void goToLastRadioChannel() {
        String lastPlayedRadioChannelId = mNowOnTvCache.getLastPlayedRadioChannelId();

        Log.d(TAG, "Last tuned radio channel: " + lastPlayedRadioChannelId);
        if (mPlaybackDetailsManager.isChannelPlaying(lastPlayedRadioChannelId)) {
            if (mNavigator.getContentFragment() instanceof EpgFragment) {
                if (mNavigator.isContentOverlayDisplayed()) {
                    mNavigator.hideContentOverlay();
                } else {
                    mNavigator.showContentOverlay();
                }
            } else {
                if (mNavigator.isPlayerUIOnTop()) {
                    mNavigator.showEpgForPlayback();
                } else {
                    mNavigator.showPlayerUI();
                }
            }
        } else {
            goToChannel(StartAction.BtnRadio, lastPlayedRadioChannelId, false);
        }
    }

    private void reportLoadingSpinnerState(PlaybackEvent event) {
        PlaybackDomain domain = mPlaybackDetailsManager.getActiveExclusiveDomain();
        if (domain == null) {
            domain = PlaybackDomain.None;
        }
        mPlaybackDetailsManager.offerEvent(new EventResult(event, domain), Bundle.EMPTY);
    }

    @Nullable
    @Override
    protected Event<?, ?> generatePageOpenEvent() {
        if (mNavigator.isContentOverlayDisplayed()) {
            return null;
        }

        IRecording recording = mPlaybackDetailsManager.getRecordingPlayerData();
        if (recording != null) {
            return ReportUtils.createEvent(UILog.Page.MediaPlayerPage, recording)
                    .addDetail(UILogEvent.Detail.NeedReset, true);
        }

        IBroadcast broadcast = mPlaybackDetailsManager.getBroadcastPlayerData();
        if (broadcast != null) {
            return ReportUtils.createEvent(UILog.Page.TvPlayer, broadcast)
                    .addDetail(UILogEvent.Detail.NeedReset, true);
        }

        IContentItem contentItem = mPlaybackDetailsManager.getPlayingItem();
        if (contentItem != null) {
            return ReportUtils.createEvent(UILog.Page.MediaPlayerPage, contentItem)
                    .addDetail(UILogEvent.Detail.NeedReset, true);
        }

        return null;
    }

    /**
     * Set "dummy" visual only focus on the More info button when there is no content fragment displayed.
     */
    private final Navigator.IContentChangeListener mContentFragmentChangeListener = new Navigator.IContentChangeListener() {
        @Override
        public void onContentShown(BaseFragment fragment) {
            if (mPlaybackDetailsManager.getPlayerType() == PlaybackType.RadioIpTv) {
                mBinding.radioChannelLogo.pauseAnimation();
            }
            mBinding.errorView.showRetryButton(false);
        }

        @Override
        public void onContentHide(@Nullable BaseFragment fragment) {
            if (mPlaybackDetailsManager.getPlayerType() == PlaybackType.RadioIpTv) {
                if (mBinding.radioChannelLogo.isPaused()) {
                    mBinding.radioChannelLogo.resumeAnimation();
                } else {
                    mBinding.radioChannelLogo.startChannelLogoAnimation(mPlaybackDetailsManager.getChannelData());
                }
            }
            mBinding.errorView.showRetryButton(mBinding.errorView.isShown());
            reportNavigation();
        }
    };

    private final IZappingInfoStateChangeListener mZappingInfoStateChangeListener = new IZappingInfoStateChangeListener() {
        @Override
        public void onZappingInfoShown() {
            if (mBinding.loadingSpinner.isShown()) {
                reportLoadingSpinnerState(PlaybackEvent.StopLoadingSpinner);
            }
            mBinding.loadingSpinner.setAlpha(0);
        }

        @Override
        public void onZappingInfoHidden() {
            if (mBinding.loadingSpinner.isShown()) {
                reportLoadingSpinnerState(PlaybackEvent.StartLoadingSpinner);
            }
            mBinding.loadingSpinner.setAlpha(1);
        }
    };

    protected final AutoUpdateRunnable mAutoUpdateRunnable = new AutoUpdateRunnable();

    private class AutoUpdateRunnable implements Runnable {
        public final long AUTO_UPDATE_PERIOD = TimeUnit.SECONDS.toMillis(1);

        @Override
        public void run() {
            mBinding.playerView.postDelayed(this, AUTO_UPDATE_PERIOD);

            PlaybackDetails details = mPlaybackDetailsManager.getExclusiveDetails();

            BaseFragment contentFragment = mNavigator.getContentFragment();
            if (contentFragment instanceof BasePlayerFragment) {
                ((BasePlayerFragment) contentFragment).updatePlaybackDetails(details);
            }

            mBinding.unskippableAdvertisement.unskippableMessage.setText(null);

            if (!details.isUnskippablePosition()) {
                Log.v(TAG, "isForActivePlayback - return");
                mBinding.unskippableAdvertisement.unskippableTime.setText(null);
                return;
            }

            long time = TimeRange.remainingTime(details.getUnskippableRanges(), details.getPosition());
            String text = LocaleTimeUtils.getUnskippableDuration(time, mTranslator);
            mBinding.unskippableAdvertisement.unskippableTime.setText(text);
        }

        public void start() {
            mBinding.unskippableAdvertisement.unskippableTime.setVisibility(View.VISIBLE);
            mBinding.playerView.removeCallbacks(this);
            mBinding.playerView.post(this);
        }

        public void stop() {
            mBinding.unskippableAdvertisement.unskippableTime.setVisibility(View.GONE);
            mBinding.playerView.removeCallbacks(this);
        }
    }
}
