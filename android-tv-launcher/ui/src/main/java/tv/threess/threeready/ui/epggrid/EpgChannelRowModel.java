package tv.threess.threeready.ui.epggrid;

import java.util.List;

import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;

/**
 * Model for a channel row in the EPG grid.
 * Wraps the channel and the program list for it.
 *
 * @author Barabas Attila
 * @since 8/20/21
 */
public class EpgChannelRowModel {
    public final TvChannel channel;
    public final List<IBroadcast> programs;
    public final boolean isInFavouriteChannelList;

    public EpgChannelRowModel(TvChannel channel, List<IBroadcast> programs, boolean isInFavouriteChannelList) {
        this.channel = channel;
        this.programs = programs;
        this.isInFavouriteChannelList = isInFavouriteChannelList;
    }

}
