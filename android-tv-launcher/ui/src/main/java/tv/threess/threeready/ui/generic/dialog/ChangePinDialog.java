package tv.threess.threeready.ui.generic.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Fragment of the Change PIN dialog.
 *
 * @author Daniela Toma
 * @since 2019.06.11
 */
public class ChangePinDialog extends BasePinDialog {
    private static final String TAG = Log.tag(ChangePinDialog.class);

    private Disposable mChangePinDisposable;

    private String mMissMatchHint;

    private String mErrorHint;

    private String mPin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setEnterState();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        RxUtils.disposeSilently(mPinValidateDisposable, mChangePinDisposable);
    }

    /**
     * Set "Enter" state of a PIN dialog.
     */
    @Override
    protected void setEnterState() {
        RxUtils.disposeSilently(mChangePinDisposable, mPinValidateDisposable);
        mBinding.title.setText(mTitle);
        mBinding.body.setText(mBody);
        mBinding.body.setTextColor(mLayoutConfig.getFontColor());
        mBinding.digitsContainer.setVisibility(View.VISIBLE);
        focusFirstDigit();
    }

    @Override
    protected void onValidatePin(){
        if (isAllEntered() && !mIsValidatingPin) {
            if (!TextUtils.isEmpty(mPin) && mPin.equalsIgnoreCase(getEnteredPin())) {
                mIsValidatingPin = true;
                mPinValidator.validatePin(mPin)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Boolean>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            mChangePinDisposable = d;
                        }

                        @Override
                        public void onSuccess(Boolean changed) {
                            if (changed) {
                                dismiss();
                            } else {
                                onFail();
                            }
                            mIsValidatingPin = false;
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, "Error on changing the Pin ", e);
                            mNavigator.showFailedToSaveSettingsDialog();
                            dismiss();
                            mIsValidatingPin = false;
                        }
                    });
                return;

            // Check if the two pin is not the same.
            } else if(!TextUtils.isEmpty(mPin) && !mPin.equalsIgnoreCase(getEnteredPin())) {
                onDifferentPINs();
                return;

            } else {
                mPin = getEnteredPin();
                setReenterState();
                return;
            }
        }

        focusNextDigit();
    }

    @Override
    protected void setReenterState() {
        super.setReenterState();
        resetDigits();
        focusFirstDigit();

        mBinding.hint.setVisibility(View.GONE);
    }

    protected void onFail() {
        setEnterState();
        mPin = "";
        resetDigits();

        mBinding.hint.setText(mErrorHint);
        mBinding.hint.setVisibility(View.VISIBLE);
    }

    /**
     * Called when the user enters different PINs for the first and second time.
     */
    protected void onDifferentPINs() {
        setEnterState();
        mPin = "";
        resetDigits();

        mBinding.hint.setText(mMissMatchHint);
        mBinding.hint.setVisibility(View.VISIBLE);
    }

    public static class Builder {
        ChangePinDialog mChangePinDialog;

        public Builder() {
            mChangePinDialog = new ChangePinDialog();
        }

        /**
         * Set title to the dialog.
         *
         * @param title to be set.
         * @return builder.
         */
        public ChangePinDialog.Builder setTitle(String title) {
            mChangePinDialog.mTitle = title;
            return this;
        }

        /**
         * Set reenter title to the dialog.
         *
         * @param reenterTitle to be set.
         * @return builder.
         */
        public ChangePinDialog.Builder setReenterTitle(String reenterTitle) {
            mChangePinDialog.mReenterTitle = reenterTitle;
            return this;
        }

        /**
         * Set body to the dialog.
         *
         * @param body to be set.
         * @return builder.
         */
        public ChangePinDialog.Builder setBody(String body) {
            mChangePinDialog.mBody = body;
            return this;
        }

        /**
         * Set reenter body to the dialog.
         *
         * @param reenterBody to be set.
         * @return builder.
         */
        public ChangePinDialog.Builder setReenterBody(String reenterBody) {
            mChangePinDialog.mReenterBody = reenterBody;
            return this;
        }

        /**
         * Set get PIN code observable to the dialog.
         *
         * @param pinValidator to be set.
         * @return builder.
         */
        public ChangePinDialog.Builder setPinValidator(PinValidator pinValidator) {
            mChangePinDialog.mPinValidator = pinValidator;
            return this;
        }

        /**
         * Sets a hint message which will be displayed
         * when the user enters two different pins and the firs and second time.
         * @return builder.
         */
        public ChangePinDialog.Builder setMissMatchHint(String missMatchHint) {
             mChangePinDialog.mMissMatchHint = missMatchHint;
             return this;
        }

        /**
         * Sets a hint message which will be displayed
         * when the pin code could not be saved.
         * @return builder.
         */
        public ChangePinDialog.Builder setErrorHint(String errorHint) {
             mChangePinDialog.mErrorHint = errorHint;
             return this;
        }

        public ChangePinDialog build() {
            return mChangePinDialog;
        }
    }
}
