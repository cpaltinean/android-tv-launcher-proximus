package tv.threess.threeready.ui.home.presenter.module.collection.mixed;

import android.content.Context;

import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;

import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.home.view.ModularPageView;
import tv.threess.threeready.ui.tv.presenter.vod.VodA2CollectionCardPresenter;

/**
 * Presenter used to display mixed type, A2 variant portrait cards, for collections.
 * <p>
 * Created by Solyom Zsolt on 2020.01.29.
 */

public class MixedA2PortraitCollectionModulePresenter extends BaseMixedCollectionModulePresenter {

    private static final int NUMBER_OF_COLUMNS = 5;

    public MixedA2PortraitCollectionModulePresenter(Context context, ModularPageView.PageTitleCallback pageTitle, DataSourceLoader dataSourceLoader) {
        super(context, pageTitle, dataSourceLoader);
        mVariantCardPresenterMap.put(ModuleCardVariant.A2, new InterfacePresenterSelector()
                .addClassPresenterSelector(IContentItem.class,
                        new MixedA2PortraitCollectionModulePresenter.CardPresenterSelector(
                                new VodA2CollectionCardPresenter(mContext))));
    }

    @Override
    public void onBindHolder(ViewHolder holder, ModuleData<?> data) {
        super.onBindHolder(holder, data);
        int horizontalLeftPadding = mContext.getResources().getDimensionPixelOffset(R.dimen.mixed_a2_collection_horizontal_padding_left);
        int horizontalRightPadding = mContext.getResources().getDimensionPixelOffset(R.dimen.mixed_a2_collection_horizontal_padding_right);
        int verticalPadding = mContext.getResources().getDimensionPixelOffset(R.dimen.mixed_a2_collection_vertical_padding);
        holder.mBinding.collection.getCollectionGrid().setPadding(horizontalLeftPadding, verticalPadding, horizontalRightPadding, verticalPadding);
    }

    @Override
    protected int getColumnNumber() {
        return NUMBER_OF_COLUMNS;
    }

    private static class CardPresenterSelector extends PresenterSelector {

        private final BaseCardPresenter<?,?> cardPresenter;

        CardPresenterSelector(BaseCardPresenter<?,?> vodPresenter) {
            cardPresenter = vodPresenter;
        }

        @Override
        public Presenter[] getPresenters() {
            return new Presenter[]{cardPresenter};
        }

        @Override
        public Presenter getPresenter(Object obj) {
            return cardPresenter;
        }

    }

}
