/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.player.plugin;

import android.os.Bundle;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.MemoryCache;
import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.PlaybackEventDetails;
import tv.threess.threeready.api.log.model.PlaybackLogEvent;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.api.middleware.model.Scte35Info;
import tv.threess.threeready.api.middleware.model.ScteState;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.TvRepository;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvAdReplacement;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.player.IPlaybackDetailsCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.command.AdStitchCommand;
import tv.threess.threeready.player.command.ClearCommand;
import tv.threess.threeready.player.command.ForceStopCommand;
import tv.threess.threeready.player.command.PauseCommand;
import tv.threess.threeready.player.command.ResumeCommand;
import tv.threess.threeready.player.command.StopCommand;
import tv.threess.threeready.player.command.base.ImplicitCommand;
import tv.threess.threeready.player.command.base.JumpCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.command.start.AppStartCommand;
import tv.threess.threeready.player.command.start.ContentItemStartCommand;
import tv.threess.threeready.player.command.start.IptvLiveStartCommand;
import tv.threess.threeready.player.command.start.TrickPlayStartCommand;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.control.DtvControlProximus;
import tv.threess.threeready.player.exception.AdStitchError;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.player.model.PlaybackDetailsBuilder;
import tv.threess.threeready.player.model.PlaybackEntireState;
import tv.threess.threeready.player.plugin.PlaybackPlugin;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.player.result.EventResult;
import tv.threess.threeready.player.result.Failure;
import tv.threess.threeready.player.result.Result;

/**
 * Playback Plugin reporting the playback events.
 *
 * @author zkaretka
 * @since 2017.09.11
 */
public class ReportPlugin implements PlaybackPlugin, IPlaybackDetailsCallback {
    private static final String TAG = Log.tag(ReportPlugin.class);

    private static final long MIN_LOG_DURATION = 20;
    private static final Event<PlaybackLogEvent, PlaybackEventDetails> EMPTY = new Event<>(PlaybackLogEvent.Unknown);

    private final PlaybackDetailsManager mPlaybackManager;
    private volatile boolean mIsPlaybackStarted = false;
    private final AtomicInteger mBitRate = new AtomicInteger(0);
    private volatile long mRelativePosition;
    private volatile boolean mIsLive;
    private volatile IBroadcast mCurrentBroadcast;
    private volatile TvAdReplacement mAdReplacement = null;

    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);
    private final TvRepository mTvRepository = Components.get(TvRepository.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);

    public ReportPlugin(PlaybackDetailsManager playbackManager) {
        mPlaybackManager = playbackManager;
    }

    @Override
    public void onOfferCommand(PlaybackCommand command) {
        Log.v(TAG, "onOfferCommand: " + command);
        if (command.isSynthetic()) {
            return;
        }

        if (!isReportingEnabledForDomain(command)) {
            Log.d(TAG, "onOfferCommand playbackDomain is not supported: " + command.getDomain());
            return;
        }

        if (command instanceof StartCommand) {
            mIsLive = ((StartCommand) command).isLive();
            Log.event(getEvent(PlaybackLogEvent.StartInitiated, command));
        }
    }

    @Override
    public void onBeforeExecute(PlaybackCommand command, PlaybackEntireState state, PlaybackDetailsBuilder details) {
        Log.v(TAG, "onBeforeExecute: " + command);
        if (!isReportingEnabledForDomain(command)) {
            Log.d(TAG, "onBeforeExecute playbackDomain is not supported: " + command.getDomain());
            return;
        }

        if (command instanceof ClearCommand || command instanceof StopCommand) {
            mIsLive = details.isLive();
            mRelativePosition = Math.max(0, details.getRelativePosition());
        }

        if (command instanceof StartCommand) {
            mIsLive = ((StartCommand) command).isLive();
            Log.event(getEvent(PlaybackLogEvent.StartResolved, command));
        }

        if (command instanceof JumpCommand) {
            Log.event(getEvent(PlaybackLogEvent.ExecuteJump, command));
        }
    }

    @Override
    public void onAfterExecute(PlaybackCommand command, PlaybackEntireState state, PlaybackDetailsBuilder details) {
        Log.v(TAG, "onAfterExecute: " + command);
        if (!isReportingEnabledForDomain(command)) {
            Log.d(TAG, "onAfterExecute playbackDomain is not supported: " + command.getDomain());
            return;
        }

        if (command.isSynthetic() || !(command instanceof StartCommand)) {
            return;
        }

        mIsLive = ((StartCommand) command).isLive();
        mBitRate.set(((StartCommand) command).getBitrate());
        Log.event(getEvent(PlaybackLogEvent.StartResolved, command));
        consoleLogTime(command,
                "queue delay (waiting previous commands to be completed)", command.getQueueTime());
    }

    @Override
    public void onResult(PlaybackCommand command, Result result) {
        Log.v(TAG, "onResult(command: " + command + ", result: " + result);

        // Do not report trick-play related events
        if (!isReportingEnabledForDomain(command)) {
            Log.d(TAG, "onResult playbackDomain is not supported: " + command.getDomain());
            return;
        }

        // Report application start event
        if (command instanceof AppStartCommand) {
            if (result instanceof Failure) {
                return;
            }
            AppStartCommand appStartCommand = (AppStartCommand) command;
            Log.event(new Event<>(UILogEvent.AppStarted)
                    .putDetail(UILogEvent.Detail.PackageName, appStartCommand.getPackageName())
                    .putDetail(UILogEvent.Detail.StartAction, appStartCommand.getStartAction())
            );
            return;
        }

        if (command instanceof StartCommand) {
            consoleLogTime(command, "tuning " + command.getDomain().getName(), command.getProcessingTime());
        }

        if (command instanceof AdStitchCommand) {
            AdStitchCommand adStitchCommand = (AdStitchCommand) command;
            long timestamp = command.getTriggerTime();
            IptvLiveStartCommand startCommand = (IptvLiveStartCommand) mPlaybackManager.getStartedCommand();
            Scte35Info marker = adStitchCommand.getMarker();
            String spliceId = marker == null ? "" : marker.get(Scte35Info.Field.splice_event_id);
            Log.event(getAdEvent(PlaybackLogEvent.AdTrigger, timestamp)
                .addDetail(PlaybackEventDetails.SPLICE_ID, spliceId)
                .addDetail(PlaybackEventDetails.TAP_REQUEST_SENT, adStitchCommand.getVastResponse() != null)
                .addDetail(PlaybackEventDetails.CHANNEL_INACTIVITY_THRESHOLD, startCommand.getChannel().getTargetAdInactivity(TimeUnit.MILLISECONDS))
                .addDetail(PlaybackEventDetails.LAST_ACTIVITY, MemoryCache.LastUserActivity.get(System.currentTimeMillis()))
            );

            if (result instanceof Failure) {
                TvAdReplacement adReplacement = adStitchCommand.getVastResponse();
                TvAdReplacement.Error error = TvAdReplacement.Error.Unidentified;
                if (((Failure) result).getError() instanceof AdStitchError) {
                    error = ((AdStitchError) ((Failure) result).getError()).getError();
                }
                mTvRepository.trackAdReplacement(adReplacement, error);
                mAdReplacement = null;
            } else {
                mAdReplacement = adStitchCommand.getVastResponse();
            }
            return;
        }

        // Command execution failed
        if (result instanceof Failure) {
            if (command instanceof StartCommand) {
                mBitRate.set(0);
                mIsPlaybackStarted = false;
                Failure failure = (Failure) result;
                // Don't report cancel failure reasons. Since those will not be visible for the user anyway.
                if (failure.getReason() != PlaybackError.Type.CANCELED) {
                    logPlaybackFailed((StartCommand) command, failure);
                    consoleLogTime(command, "tune failed [ "
                            + failure.getReason().name() + "]", command.getCmdTotalTime());
                }
            }
            return;
        }

        if (!(command instanceof ClearCommand) && !(command instanceof StopCommand)) {
            mRelativePosition = Math.max(0, result.getDetails().getRelativePosition());
        }

        // Command execution succeeded
        if (command instanceof StartCommand) {
            mIsLive = result.getDetails().isLive();
            logPlaybackStarted((StartCommand) command, result);
            consoleLogTime(command, "tune success", command.getCmdTotalTime());
            if (command instanceof TrickPlayStartCommand) {
                TrickPlayStartCommand trickPlayCommand = (TrickPlayStartCommand) command;
                mTvRepository.trackAdReplacement(mAdReplacement, trickPlayCommand.getPlaybackSpeed() < 0 ?
                        TvAdReplacement.Event.rewind : TvAdReplacement.Event.skip);
            } else {
                mAdReplacement = null;
            }
        } else if (command instanceof StopCommand || command instanceof ClearCommand) {
            mTvRepository.trackAdReplacement(mAdReplacement, TvAdReplacement.Event.closeLinear);
            mAdReplacement = null;
            logPlaybackStopped(command);
        } else if (command instanceof PauseCommand) {
            mTvRepository.trackAdReplacement(mAdReplacement, TvAdReplacement.Event.pause);
            Log.event(getEvent(PlaybackLogEvent.PlaybackPaused, command)
                .putDetail(PlaybackEventDetails.ACTION, StartAction.Pause)
            );
        } else if (command instanceof ResumeCommand) {
            mTvRepository.trackAdReplacement(mAdReplacement, TvAdReplacement.Event.resume);
            Log.event(getEvent(PlaybackLogEvent.PlaybackResumed, command)
                .putDetail(PlaybackEventDetails.ACTION, ((ResumeCommand) command).getStartAction())
            );
        } else if (command instanceof JumpCommand) {
            JumpCommand jumpCommand = (JumpCommand) command;
            mIsLive = result.getDetails().isLive();
            mTvRepository.trackAdReplacement(mAdReplacement, jumpCommand.isRewind() ?
                    TvAdReplacement.Event.rewind : TvAdReplacement.Event.skip
            );
            Log.event(getEvent(PlaybackLogEvent.JumpExecuted, command));
        }

        mIsLive = result.getDetails().isLive();
    }

    @Override
    public void onEvent(EventResult result, Bundle details) {
        Log.v(TAG, "onEvent: " + result);
        // translate the playback events to PlaybackLogEvents
        switch (result.event) {
            default:
                Log.d(TAG, "Skipping event: " + result.event);
                return;

            case StartLoadingSpinner:
                Log.event(getEvent(PlaybackLogEvent.StartLoadingSpinner));
                break;

            case StopLoadingSpinner:
                Log.event(getEvent(PlaybackLogEvent.StopLoadingSpinner));
                break;

            case PlaybackStarted:
                Log.event(getEvent(PlaybackLogEvent.PlaybackBuffered));
                break;

            case PlaybackDisconnected:
                Log.event(getEvent(PlaybackLogEvent.PlaybackDisconnected));
                break;

            case PlaybackFailed:
                Log.event(getEvent(PlaybackLogEvent.PlaybackFailed));
                break;

            case PlaybackUnavailable:
                Log.event(getEvent(PlaybackLogEvent.PlaybackUnavailable));
                break;

            case EndReached:
                Log.event(getEvent(PlaybackLogEvent.PlaybackEnded));
                break;

            case PlayerStateChanged:
                Log.event(getEvent(PlaybackLogEvent.StateChanged)
                    .copyDetail(PlaybackEventDetails.PLAYER_STATE, details)
                    .copyDetail(PlaybackEventDetails.STATE_CHANGED_TIMESTAMP, details)
                );
                break;

            case BitrateChanged:
                Log.event(getEvent(PlaybackLogEvent.BitrateChanged)
                    .copyDetail(PlaybackEventDetails.BITRATE, details)
                );
                mBitRate.set(details.getInt(PlaybackEventDetails.BITRATE.name(), 0));
                break;

            case SegmentDownloaded:
                Log.event(getEvent(PlaybackLogEvent.SegmentDownloaded)
                    .copyDetail(PlaybackEventDetails.PLAYBACK_URL, details)
                    .copyDetail(PlaybackEventDetails.SEGMENT_SIZE, details)
                    .copyDetail(PlaybackEventDetails.SEGMENT_TIME, details)
                );
                break;

            case AdReplacement:
                TvAdReplacement adReplacement = mAdReplacement;
                ScteState state = (ScteState) details.getSerializable(DtvControlProximus.TAD_EXTRA_STATE);
                int progress = -1;

                switch (state.getState()) {
                    case READY:
                        progress = (int) state.get(ScteState.Field.progress, -1);
                        break;

                    case START:
                        if (adReplacement != null) {
                            Log.event(getAdEvent(PlaybackLogEvent.AdReplacement)
                                    .addDetail(PlaybackEventDetails.ADVERTISEMENT_REFERENCE, adReplacement.getFileName())
                            );
                        }
                        // report also 0% complete percentage
                        progress = (int) state.get(ScteState.Field.progress, -1);
                        break;

                    case COMPLETE:
                        mAdReplacement = null;
                        // report also 100% complete percentage
                        progress = (int) state.get(ScteState.Field.progress, -1);
                        break;

                    case CANCEL:
                    case FAIL:
                        mAdReplacement = null;
                        switch (state.get(ScteState.Field.fail_reason, "")) {
                            case ScteState.FAIL_REASON_UNKNOWN:
                            case ScteState.FAIL_REASON_INVALID_AD_FILE:
                            case ScteState.FAIL_REASON_INVALID_START_PTS:
                            case ScteState.FAIL_REASON_INVALID_AD_DURATION:
                            case ScteState.FAIL_REASON_RESERVED_POSITION:
                            case ScteState.FAIL_REASON_AV_PID_MISMATCH:
                                mTvRepository.trackAdReplacement(adReplacement, TvAdReplacement.Error.PlaybackFailed);
                                return;
                        }
                        mTvRepository.trackAdReplacement(adReplacement, TvAdReplacement.Error.Unidentified);
                        return;
                }

                switch (progress) {
                    case 0:
                        mTvRepository.trackAdReplacement(adReplacement, TvAdReplacement.Event.start);
                        break;

                    case 25:
                        mTvRepository.trackAdReplacement(adReplacement, TvAdReplacement.Event.firstQuartile);
                        break;

                    case 50:
                        mTvRepository.trackAdReplacement(adReplacement, TvAdReplacement.Event.midpoint);
                        break;

                    case 75:
                        mTvRepository.trackAdReplacement(adReplacement, TvAdReplacement.Event.thirdQuartile);
                        break;

                    case 100:
                        mTvRepository.trackAdReplacement(adReplacement, TvAdReplacement.Event.complete);
                        break;
                }
                break;
        }
    }

    @Override
    public void onPlayerDataUpdate(IBroadcast event) {
        IBroadcast oldEvent = mCurrentBroadcast;
        mCurrentBroadcast = event;
        if (oldEvent == null || event == null) {
            return;
        }
        if (Objects.equals(oldEvent.getId(), event.getId())) {
            // if the channel is force restarted do not report the change from the same event to the same event
            Log.d(TAG, "Ignoring broadcast change from: " + oldEvent.getId() + " to: " + event.getId());
            return;
        }
        if (Objects.equals(oldEvent.getChannelId(), event.getChannelId()) && shouldSendProgramChangeEvents()) {
            mRelativePosition = oldEvent.getDuration();
            logProgramChangedStop(oldEvent);

            mRelativePosition = 0;
            logProgramChangedStart(event);
        }
    }

    private void logProgramChangedStart(IBroadcast event) {
        Log.event(getEvent(PlaybackLogEvent.PlaybackStarted, null, event)
            .putDetail(PlaybackEventDetails.ACTION, StartAction.ProgramChanged)
        );
        mIsPlaybackStarted = true;
    }

    private void logProgramChangedStop(IBroadcast event) {
        // If stop called without a start(or start failed), do not log the stop event
        if (!mIsPlaybackStarted) {
            return;
        }
        mIsPlaybackStarted = false;
        Log.event(getEvent(PlaybackLogEvent.PlaybackStopped, null, event)
            .putDetail(PlaybackEventDetails.ACTION, StartAction.ProgramChanged)
        );
    }

    private void logPlaybackStarted(StartCommand cmd, Result result) {
        Event<PlaybackLogEvent, PlaybackEventDetails> event = getEvent(PlaybackLogEvent.PlaybackStarted, cmd);
        mIsPlaybackStarted = true;

        if (result != null && result.getDetails() != null) {
            event.addDetail(PlaybackEventDetails.RESUME_BOOKMARK, result.getDetails().isResumeBookmark());
        }
        Log.event(event);
    }

    private void logPlaybackFailed(StartCommand cmd, Failure failure) {
        Event<PlaybackLogEvent, PlaybackEventDetails> event = getEvent(PlaybackLogEvent.PlaybackFailed, cmd);

        if (!mInternetChecker.checkInternetAvailability()) {
            event.putDetail(PlaybackEventDetails.ERROR_NO_INTERNET, true);
        }
        Log.event(event);
    }

    /**
     * Send stop event with the details from the last start event
     */
    private void logPlaybackStopped(PlaybackCommand cmd) {
        // if stop called without a start(or start failed), do not log the stop event
        if (!mIsPlaybackStarted) {
            return;
        }
        mIsPlaybackStarted = false;
        Log.event(getEvent(PlaybackLogEvent.PlaybackStopped, cmd));
    }

    /**
     * Get an event filled with all the attributes available in the PlaybackManager.
     */
    private Event<PlaybackLogEvent, PlaybackEventDetails> getEvent(PlaybackLogEvent kind, PlaybackCommand command, IContentItem item) {
        if (command == null) {
            // Fallback to last offered command
            command = mPlaybackManager.getStartedCommand();

            if (command == null) {
                return EMPTY;
            }
        }

        if (command.getDomain() == PlaybackDomain.TrickPlay) {
            // Ignore trick play start commands
            return EMPTY;
        }

        final long playbackInitiatedTimestamp = command.getCommandInitiatedTimestamp();
        PlaybackType currentType = command.getType();
        PlaybackDomain currentDomain = currentType.getDomain();
        PlaybackDetails currentDetails = mPlaybackManager.getDetails(currentType);

        Event<PlaybackLogEvent, PlaybackEventDetails> event = new Event<>(kind);
        // We need to update the following details for all events
        event.addDetail(PlaybackEventDetails.IS_LIVE, mIsLive);
        event.addDetail(PlaybackEventDetails.IS_PLAYING, currentDetails.getState() == PlaybackState.Started);
        event.addDetail(PlaybackEventDetails.POSITION, mRelativePosition);
        event.addDetail(PlaybackEventDetails.BITRATE, mBitRate.get());
        addStartAction(event, command);

        if (item == null && command instanceof ContentItemStartCommand) {
            item = ((ContentItemStartCommand<?>) command).getContentItem();
        }

        if (item == null) {
            return event;
        }

        // Get audio tracks
        if (currentDetails.getCurrentAudioTrack() != null) {
            event.addDetail(PlaybackEventDetails.AUDIO_LANGUAGE,
                    currentDetails.getCurrentAudioTrack().getLanguage());
        }
        // Get subtitle tracks
        if (currentDetails.getCurrentSubtitleTrack() != null) {
            event.addDetail(PlaybackEventDetails.SUBTITLE_LANGUAGE,
                    currentDetails.getCurrentSubtitleTrack().getLanguage());
        }

        event.addDetail(PlaybackEventDetails.STREAM_TYPE, currentDomain);
        event.addDetail(PlaybackEventDetails.PLAYBACK_URL, currentDetails.getPlaybackUrl());

        TvChannel channel = mNowOnTvCache.getChannel(mPlaybackManager.getChannelId());
        if (channel != null) {
            event.addDetail(PlaybackEventDetails.CHANNEL_ID, channel.getId());
            event.addDetail(PlaybackEventDetails.CHANNEL_INTERNAL_ID, channel.getInternalId());
            event.addDetail(PlaybackEventDetails.CHANNEL_CALL_LETTER, channel.getCallLetter());
        }

        event.addDetail(PlaybackEventDetails.ITEM_RESOLUTION, ReportUtils.getItemResolution((StartCommand) command, mNowOnTvCache));
        event.addDetail(PlaybackEventDetails.PLAYBACK_INITIATED_TIMESTAMP, playbackInitiatedTimestamp);

        switch (currentDomain) {
            case Vod:
            case Trailer:
                event.addDetail(PlaybackEventDetails.ITEM_TITLE, ReportUtils.getItemTitle(item));
                event.addDetail(PlaybackEventDetails.VOD_TITLE, ReportUtils.getTitle(item));
                event.addDetail(PlaybackEventDetails.GROUP_TITLE, ReportUtils.getGroupTitle(item));
                event.addDetail(PlaybackEventDetails.VOD_ITEM_IDENTIFIER, ReportUtils.getItemInternalId(item));
                event.addDetail(PlaybackEventDetails.ITEM_ID, ReportUtils.getItemInternalId(item));
                event.addDetail(PlaybackEventDetails.VOD_EXTERNAL_IDENTIFIER, ReportUtils.getItemId(item));
                event.addDetail(PlaybackEventDetails.ITEM_EXTERNAL_ID, ReportUtils.getItemId(item));
                event.addDetail(PlaybackEventDetails.ITEM_GENRE, ReportUtils.getItemGenre(item));
                event.addDetail(PlaybackEventDetails.ITEM_SUPER_GENRE, ReportUtils.getItemSuperGenre(item));
                event.addDetail(PlaybackEventDetails.VOD_TYPE, ReportUtils.getVodType(item));
                event.addDetail(PlaybackEventDetails.EPISODE_TITLE, ReportUtils.getItemEpisodeTitle(item));
                event.addDetail(PlaybackEventDetails.SEASON_NR, ReportUtils.getItemSeasonNr(item));
                event.addDetail(PlaybackEventDetails.EPISODE_NR, ReportUtils.getItemEpisodeNr(item));
                break;

            case App:
                String packageName = item.getId();
                event.addDetail(PlaybackEventDetails.ITEM_TITLE,
                        PackageUtils.getAppNameByPackageName(mPlaybackManager.getContext(), packageName));
                event.addDetail(PlaybackEventDetails.ITEM_ID, packageName);
                break;

            default:
                event.addDetail(PlaybackEventDetails.PROGRAM_TITLE, ReportUtils.getItemTitle(item));
                event.addDetail(PlaybackEventDetails.ITEM_TITLE, ReportUtils.getItemTitle(item));
                event.addDetail(PlaybackEventDetails.PROGRAM_ID, ReportUtils.getProgramId(item));
                event.addDetail(PlaybackEventDetails.ITEM_ID, ReportUtils.getProgramId(item));
                event.addDetail(PlaybackEventDetails.ITEM_TYPE, ReportUtils.getItemType(item));
                event.addDetail(PlaybackEventDetails.PROGRAM_ITEM_ID, ReportUtils.getItemId(item));
                event.addDetail(PlaybackEventDetails.ITEM_START, mPlaybackManager.getStart());
                event.addDetail(PlaybackEventDetails.ITEM_DURATION, ReportUtils.getDuration(item));
                event.addDetail(PlaybackEventDetails.EPISODE_TITLE, ReportUtils.getItemEpisodeTitle(item));
                event.addDetail(PlaybackEventDetails.SEASON_NR, ReportUtils.getItemSeasonNr(item));
                event.addDetail(PlaybackEventDetails.EPISODE_NR, ReportUtils.getItemEpisodeNr(item));
                event.addDetail(PlaybackEventDetails.PROGRAM_CATEGORY, ReportUtils.getItemGenre(item));
                event.addDetail(PlaybackEventDetails.PROGRAM_SUB_CATEGORY, ReportUtils.getItemSubGenre(item));
                break;
        }
        return event;
    }

    private Event<PlaybackLogEvent, PlaybackEventDetails> getEvent(PlaybackLogEvent event, PlaybackCommand command) {
        return getEvent(event, command, null);
    }

    private Event<PlaybackLogEvent, PlaybackEventDetails> getEvent(PlaybackLogEvent event) {
        return getEvent(event, null, null);
    }

    /**
     * Add AD event details for trigger and stitching
     */
    private Event<PlaybackLogEvent, PlaybackEventDetails> getAdEvent(PlaybackLogEvent kind, long timestamp) {
        Event<PlaybackLogEvent, PlaybackEventDetails> event = new Event<>(kind, timestamp);
        IptvLiveStartCommand startCommand = (IptvLiveStartCommand) mPlaybackManager.getStartedCommand();
        event.addDetail(PlaybackEventDetails.STREAM_TYPE, startCommand.getDomain());
        event.addDetail(PlaybackEventDetails.CHANNEL_CALL_LETTER, startCommand.getChannel().getCallLetter());
        event.addDetail(PlaybackEventDetails.CHANNEL_ID, startCommand.getChannelId());
        IBroadcast currentProgram = startCommand.getBroadcast(mPlaybackManager.getExclusiveDetails().getPosition(), false);
        event.addDetail(PlaybackEventDetails.PROGRAM_ITEM_ID, currentProgram != null ? currentProgram.getId() : "");
        event.addDetail(PlaybackEventDetails.PROGRAM_ID, currentProgram != null ? currentProgram.getProgramId() : "");
        event.addDetail(PlaybackEventDetails.PROGRAM_TITLE, currentProgram != null ? currentProgram.getTitle() : "");
        return event;
    }

    private Event<PlaybackLogEvent, PlaybackEventDetails> getAdEvent(PlaybackLogEvent kind) {
        return getAdEvent(kind, System.currentTimeMillis());
    }

    /**
     * Log tuning duration messages in console
     */
    private void consoleLogTime(PlaybackCommand cmd, String message, long ms) {
        // no need to log short duration actions
        if (ms < MIN_LOG_DURATION) {
            return;
        }

        float sec = ms / 1000f;
        Log.i(TAG, "[Tune log] CMD_ID: " + cmd.getId() + " | Domain: "
                + cmd.getDomain().getName() + " | Message: " + message + " | Duration: " + sec + " sec ");
    }

    ///////////////////
    // Helper methods /
    ///////////////////

    private void addStartAction(Event<PlaybackLogEvent, PlaybackEventDetails> event, PlaybackCommand cmd) {
        if (event.getDetail(PlaybackEventDetails.ACTION, StartAction.Undefined) != StartAction.Undefined) {
            // Start Action already added to the event.
            return;
        }

        StartAction action = StartAction.Undefined;
        if (cmd instanceof ForceStopCommand) {
            if (!Components.get(MwRepository.class).isScreenOn()) {
                // Force stop command with screen off is triggered when the activity is stopped entering sleep
                action = StartAction.StandBy;
            }
        } else if (cmd instanceof StopCommand || cmd instanceof ClearCommand) {
            // Replace the action of the command with the action that caused the stop command
            PlaybackCommand injector = ((ImplicitCommand) cmd).getInjectedBy();
            if (injector instanceof StartCommand) {
                action = ((StartCommand) injector).getStartAction();
            }
        } else if (cmd instanceof StartCommand) {
            action = ((StartCommand) cmd).getStartAction();
        } else if (cmd instanceof JumpCommand) {
            action = ((JumpCommand) cmd).getStartAction();
        }
        event.putDetail(PlaybackEventDetails.ACTION, action);
    }

    /**
     * @return True if reporting is enabled for the given playback domain, otherwise false.
     */
    private boolean isReportingEnabledForDomain(PlaybackCommand cmd) {
        PlaybackDomain domain = cmd.getDomain();
        return domain != PlaybackDomain.TrickPlay && domain != PlaybackDomain.None;
    }

    /**
     * @return True if we should send the program change events, otherwise false.
     */
    private boolean shouldSendProgramChangeEvents() {
        return mPlaybackManager.inPlaybackDomain(PlaybackDomain.LiveTvIpTv, PlaybackDomain.RadioIpTv,
                PlaybackDomain.Replay);
    }
}
