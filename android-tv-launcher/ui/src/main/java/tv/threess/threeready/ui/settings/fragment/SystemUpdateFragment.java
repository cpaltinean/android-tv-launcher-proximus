package tv.threess.threeready.ui.settings.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.ButtonStyle;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.api.middleware.SystemUpdateReceiver;
import tv.threess.threeready.api.middleware.UpdateUrgency;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.SystemUpdateBinding;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.settings.model.MainSettingsItem;
import tv.threess.threeready.ui.settings.model.SupportItem;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Fragment to display system update content
 *
 * @author Diana Toma
 * @since 18.08.2020
 */
public class SystemUpdateFragment extends BaseFragment {
    private static final String TAG = Log.tag(SystemUpdateFragment.class);

    private static final String OPENED_FROM_SETTINGS_EXTRA = "OPENED_FROM_SETTINGS_EXTRA";

    private final Translator mTranslator = Components.get(Translator.class);
    private final MwRepository mMwRepository = Components.get(MwRepository.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final ButtonStyle mButtonStyle = Components.get(ButtonStyle.class);

    private String mUpdateStat;

    private SystemUpdateBinding mBinding;

    private final SystemUpdateReceiver mSystemUpdateReceiver = Components.get(SystemUpdateReceiver.class);

    private boolean mOpenedFromSettings;

    private Disposable mDisposable;

    public static SystemUpdateFragment newInstance(boolean openedFromSettings) {
        SystemUpdateFragment fragment = new SystemUpdateFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(OPENED_FROM_SETTINGS_EXTRA, openedFromSettings);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mOpenedFromSettings = getBooleanArgument(OPENED_FROM_SETTINGS_EXTRA);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = SystemUpdateBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUpdateStat = TranslationKey.UPGRADE_PROGRESS_PAGE_TITLE;
        mBinding.title.setText(mTranslator.get(TranslationKey.UPGRADE_PROGRESS_PAGE_TITLE));
        mBinding.okButton.setText(mTranslator.get(TranslationKey.OK_BUTTON));
        mBinding.okButton.setTextColor(UiUtils.createButtonBrandingColorStateList(mLayoutConfig));
        mBinding.okButton.setBackground(UiUtils.createButtonBackground(getContext(), mLayoutConfig, mButtonStyle));

        view.announceForAccessibility(mTranslator.get(TranslationKey.TALKBACK_SCREEN_UPDATES));
    }

    @Override
    public void onStart() {
        super.onStart();
        checkForUpdate();
        mSystemUpdateReceiver.addStateChangedListener(mUpdateListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        mSystemUpdateReceiver.removeStateChangedListener(mUpdateListener);
    }

    /**
     * Trigger the system update check and show the progress bar for it.
     */
    public void checkForUpdate() {
        RxUtils.disposeSilently(mDisposable);
        mDisposable = mMwRepository.checkSystemUpdates(false, true, true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        () -> Log.d(TAG, "Check for system update successfully done."),
                        throwable -> Log.e(TAG, "Error while checking for system update.", throwable)
                );
        showProgress();
    }

    /**
     * Close the page and go back to the settings if the page was opened from it.
     */
    public void goBack() {
        mNavigator.popBackStack();

        // Go back to settings.
        if (mOpenedFromSettings) {
            mNavigator.openSupport(SupportItem.Type.CHECK_FOR_UPDATES);
        }
    }

    private void showProgress() {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        mBinding.description.setVisibility(View.GONE);
        mBinding.okButton.setVisibility(View.GONE);
        mBinding.title.setText(mTranslator.get(TranslationKey.UPGRADE_PROGRESS_PAGE_TITLE));
    }

    @Override
    public boolean onBackPressed() {
        if (mOpenedFromSettings) {
            mNavigator.openSettings(MainSettingsItem.MainItemType.SUPPORT);
        }
        return super.onBackPressed();
    }

    /**
     * Show system update available/not available screen content.
     */
    private void showSystemUpdateAvailableContent(final boolean available) {
        mBinding.progressBar.setVisibility(View.GONE);

        // adapt title view bottom margin according to available/not available status
        final ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) mBinding.title.getLayoutParams();
        params.bottomMargin = (int) getResources().getDimension(available ? R.dimen.system_update_available_title_margin_bottom : R.dimen.system_update_not_available_title_margin_bottom);

        String title = mTranslator.get(available ? TranslationKey.ALERT_MANUAL_OTA_UPGRADE_AVAILABLE_TITLE : TranslationKey.ALERT_MANUAL_OTA_UPGRADE_NOT_AVAILABLE_TITLE);
        mBinding.title.setText(title);

        mBinding.description.setText(mTranslator.get(available ? TranslationKey.ALERT_MANUAL_OTA_UPGRADE_AVAILABLE_BODY : TranslationKey.ALERT_MANUAL_OTA_UPGRADE_NOT_AVAILABLE_BODY));
        mBinding.description.setVisibility(View.VISIBLE);

        mBinding.okButton.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_SCREEN_UPDATES_COMPLETE))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_DIALOG_TITLE, title)
                        .toString()
        );

        mBinding.okButton.setOnClickListener(v -> goBack());
        mBinding.okButton.setVisibility(View.VISIBLE);
        mBinding.okButton.requestFocus();
    }

    private final SystemUpdateReceiver.Listener mUpdateListener = new SystemUpdateReceiver.Listener() {
        @Override
        public void onUpdateAvailable(UpdateUrgency urgency, boolean manualCheck) {
            showSystemUpdateAvailableContent(true);
            mUpdateStat = TranslationKey.ALERT_MANUAL_OTA_UPGRADE_AVAILABLE_TITLE;
            reportNavigation();
        }

        @Override
        public void onNoUpdateAvailable(boolean manualCheck) {
            showSystemUpdateAvailableContent(false);
            mUpdateStat = TranslationKey.ALERT_MANUAL_OTA_UPGRADE_NOT_AVAILABLE_TITLE;
            reportNavigation();
        }

        @Override
        public void onUpdateCheckFailed(boolean manualCheck) {
            mNavigator.showSystemUpdateFailureDialog();
            mUpdateStat = TranslationKey.UPGRADE_FAILED_ALERT_TITLE;
            reportNavigation();
        }

        @Override
        public void onUpdateDownload(UpdateUrgency urgency, boolean manualCheck) {
            mNavigator.popBackStack();
        }
    };

    @Nullable
    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        return new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                .addDetail(UILogEvent.Detail.Page, UILog.Page.CheckUpdatesPage)
                .addDetail(UILogEvent.Detail.PageName, mUpdateStat);
    }
}
