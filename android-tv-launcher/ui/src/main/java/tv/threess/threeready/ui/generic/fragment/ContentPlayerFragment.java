/*
 *
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */
package tv.threess.threeready.ui.generic.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.command.PauseCommand;
import tv.threess.threeready.player.command.ResumeCommand;
import tv.threess.threeready.player.command.base.JumpCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.MediaTrackInfo;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.ui.generic.player.view.PlayerButtonContainer;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.generic.view.BaseOrderedIconsContainer;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.startup.StartupFlow;
import tv.threess.threeready.ui.tv.view.PlayerButtonView;

import static tv.threess.threeready.ui.utils.UiUtils.runOnUiThread;

/**
 * Abstract base class for all player user interface which is displayed for a content item.
 *
 * @author Barabas Attila
 * @since 2018.11.06
 */
public abstract class ContentPlayerFragment<TContentItem extends IContentItem> extends BasePlayerFragment {
    protected static final String EXTRA_STARTED_ITEM = "EXTRA_STARTED_ITEM";
    protected static final String EXTRA_SHOULD_START_PLAYBACK = "EXTRA_SHOULD_START_PLAYBACK";

    protected final Translator mTranslator = Components.get(Translator.class);

    protected final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);
    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    protected final StartupFlow mStartupFlow = Components.get(StartupFlow.class);

    @Override
    public String getPageId() {
        return "PLAYER_UI";
    }

    @Override
    protected abstract ContentPlayerViewHolder getViewHolder();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (getRootView() != null && !isForActivePlayback()) {
            mNavigator.popBackStack();
            return null;
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeButtons();

        // Start playback if not running.
        if (shouldStartPlayback()) {
            startPlayback(mStartAction);
        }

        TContentItem contentItem = getPlaybackData();
        if (contentItem != null) {
            updatePlaybackDetails(contentItem);
        }

        getViewHolder().getSeekbar().setFocusable(mPlaybackDetailsManager.canSeek());
        mParentalControlManager.addListener(mParentalControlListener);
    }

    protected boolean shouldStartPlayback() {
        return getBooleanArgument(EXTRA_SHOULD_START_PLAYBACK, true) && mStartupFlow.isStartupFinished();
    }

    @Override
    public void onContentShown() {
        super.onContentShown();
        updateDolbyIconVisible();
    }

    protected void initializeButtons() {
        PlayerButtonView startOverButton = getViewHolder().getStartOverButton();
        PlayerButtonContainer buttonContainer = getViewHolder().getButtonContainer();
        if (getViewHolder().getStartOverButton() == null) {
            return;
        }

        startOverButton.setTitle(mTranslator.get(TranslationKey.SCREEN_PLAYER_STARTOVER_BUTTON));
        startOverButton.setOnClickListener(v -> {
            buttonContainer.setSelectedChild(startOverButton);
            onStartOverClicked();
        });

        PlayerButtonView playerOptionsButton = getViewHolder().getOptionsButton();
        playerOptionsButton.setVisibility(View.GONE);
        playerOptionsButton.setTitle(mTranslator.get(TranslationKey.SCREEN_PLAYER_SUBTITLES_BUTTON));
        playerOptionsButton.setOnClickListener(v -> {
            buttonContainer.setSelectedChild(playerOptionsButton);
            onOptionsClicked();
        });

        PlayerButtonView moreInfoButton = getViewHolder().getMoreInfoButton();
        moreInfoButton.setTitle(mTranslator.get(TranslationKey.SCREEN_PLAYER_MORE_INFO_BUTTON));
        moreInfoButton.setOnClickListener(view -> {
            buttonContainer.setSelectedChild(moreInfoButton);
            TContentItem contentItem = getPlaybackData();
            if (contentItem != null) {
                onMoreInfoButtonClicked(contentItem);
            }
        });

        requestDefaultFocus();
    }

    protected abstract void reportDetailsFragmentSelection();

    @Override
    protected void requestDefaultFocus() {
        PlayerButtonContainer buttonContainer = getViewHolder().getButtonContainer();
        if (buttonContainer != null && buttonContainer.isShown() && mNavigator.isContentOverlayDisplayed() && !getViewHolder().getSeekbar().isFocused()) {
            buttonContainer.requestFocus();
        }
    }

    /**
     * Display the start over button if the current playback supports it.
     */
    protected void updatePlaybackButtons(TContentItem contentItem) {
        boolean restricted = mParentalControlManager.isRestricted(contentItem, true);

        boolean startOverChanged = changeButtonVisibility(getViewHolder().getStartOverButton(),
                !restricted && mPlaybackDetailsManager.canStartOver());

        if (startOverChanged) {
            requestDefaultFocus();
        }
    }

    /**
     * Display the dolby icon if the currently active playback has it.
     */
    protected void updateDolbyIconVisible() {
        BaseOrderedIconsContainer iconsContainer = getViewHolder().getBaseOrderedIconsContainer();
        if (mPlaybackDetailsManager.isCommandStarting(StartCommand.class)) {
            // Start not finished yet.
            iconsContainer.hideDolby();
            return;
        }

        if (mPlaybackDetailsManager.isDolbyAvailable()) {
            iconsContainer.showDolby();
        } else {
            iconsContainer.hideDolby();
        }
    }

    /**
     * Display or hide the more info button.
     * It depends on the player ui type. It's displayed by default.
     */
    protected void updateMoreInfoButton(TContentItem contentItem) {
        PlayerButtonView moreInfoButton = getViewHolder().getMoreInfoButton();
        if (mParentalControlManager.isRestricted(contentItem, true)) {
            // Restricted content.
            moreInfoButton.setVisibility(View.GONE);
            return;
        }

        moreInfoButton.setVisibility(View.VISIBLE);
    }

    /**
     * Called when the user clicks on the start over button.
     * The playback needs to start from the beginning.
     */
    private void onStartOverClicked() {
        if (getViewHolder().getSeekbar().getProgress() > 0 ||
                mPlaybackDetailsManager.getCurrentStatus().getState() == PlaybackState.Stopped) {
            startPlayback(StartAction.StartOver);
        }
    }

    /**
     * Called when the user clicks on the more info button.
     * Opens the audio/video option chooser dialog.
     */
    protected void onOptionsClicked() {
        mNavigator.showPlayerOptionsDialog();
    }

    /**
     * Called when the user clicks on the more info button.
     * Needs to be override to display the detail page for the content item.
     *
     * @param contentItem The content item for which the detail page should be opened.
     */
    protected abstract void onMoreInfoButtonClicked(TContentItem contentItem);

    /**
     * @return The content item for which the player was displayed.
     */
    @Nullable
    public TContentItem getPlayerStartedItem() {
        return getSerializableArgument(EXTRA_STARTED_ITEM);
    }

    /**
     * @return The content item for which the player was displayed.
     */
    @Nullable
    public abstract TContentItem getPlaybackData();

    /**
     * Update all the displayed information on the player ui.
     *
     * @param contentItem The content item for which the information should be displayed.
     */
    protected void updatePlaybackDetails(TContentItem contentItem) {
        updateMoreInfoButton(contentItem);

        updateProviderLogo(contentItem);
        updateTitle(contentItem);
        updateSubtitle(contentItem);

        updateParentalRating(contentItem);
        updateDescriptiveAudioIcon(contentItem);
        updateDescriptiveSubtitleIcon(contentItem);
        updateQualityIndicator(contentItem);
        updateContentMarkers(contentItem);

        updateSeekBar(mPlaybackDetailsManager.getExclusiveDetails());

        updateDolbyIconVisible();
        updatePlaybackButtons(contentItem);
        updateOptionsButton(contentItem);
    }

    /**
     * Update the displayed provider logo for the content item.
     *
     * @param contentItem The content item which contains the logos.
     */
    protected void updateProviderLogo(TContentItem contentItem) {
    }


    /**
     * Update the displayed title for the content item.
     *
     * @param contentItem The content item which contains the title.
     */
    protected void updateTitle(TContentItem contentItem) {
        getViewHolder().getTitleView().setText(mParentalControlManager.isRestricted(contentItem, true)
                ? mTranslator.get(TranslationKey.CARDS_TITLE_CONTENT_LOCKED) : contentItem.getTitle());
    }

    /**
     * Update displayed subtitle/description for the content item.
     *
     * @param contentItem The content item which contains all the information for description.
     */
    protected abstract void updateSubtitle(TContentItem contentItem);

    /**
     * Update the displayed parental rating for the content item.
     *
     * @param contentItem The content item which contains the parental rating.
     */
    protected void updateParentalRating(TContentItem contentItem) {
        getViewHolder().getBaseOrderedIconsContainer().showParentalRating(contentItem.getParentalRating());
    }

    /**
     * Displays descriptive audio icon for the content item.
     */
    protected void updateDescriptiveAudioIcon(TContentItem contentItem) {
        BaseOrderedIconsContainer iconsContainer = getViewHolder().getBaseOrderedIconsContainer();
        if (contentItem.hasDescriptiveAudio()) {
            iconsContainer.showDescriptiveAudio();
        } else {
            iconsContainer.hideDescriptiveAudio();
        }
    }

    /**
     * Displays descriptive subtitle icon for the content item.
     */
    protected void updateDescriptiveSubtitleIcon(TContentItem contentItem) {
        BaseOrderedIconsContainer iconsContainer = getViewHolder().getBaseOrderedIconsContainer();
        if (contentItem.hasDescriptiveSubtitle()) {
            iconsContainer.showDescriptiveSubtitle();
        } else {
            iconsContainer.hideDescriptiveSubtitle();
        }
    }

    /**
     * Update the displayed video quality indicator for the content item.
     *
     * @param contentItem The content item which contains the video quality.
     */
    protected abstract void updateQualityIndicator(TContentItem contentItem);

    /**
     * Update the displayed content marker for the content item.
     *
     * @param contentItem The content item on based the content marker should be displayed.
     */
    protected abstract void updateContentMarkers(TContentItem contentItem);

    @Override
    public void onSecondaryProgressChanged(long position, long start, long end) {
        if (position == end) {
            // Invalidate the `now` content marker visibility.
            TContentItem contentItem = getPlaybackData();
            if (contentItem != null) {
                updateContentMarkers(contentItem);
            }
        }
        // Invalidate the `start over` button visibility.
        updatePlaybackButtons(getPlaybackData());
    }

    @Override
    public boolean onBackPressed() {
        clearPlayerButtonStates();
        return super.onBackPressed();
    }

    @Override
    protected void clearPlayerButtonStates() {
        PlayerButtonContainer buttonContainer = getViewHolder().getButtonContainer();
        if (buttonContainer != null) {
            buttonContainer.clearSelection();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mParentalControlManager.removeListener(mParentalControlListener);

        Context context = getContext();
        if (context != null) {
            Glide.with(context.getApplicationContext()).clear(getViewHolder().getLogoView());
        }
    }

    /**
     * Display the more options button if there is more then one playback option available.
     * The active playback needs to have at leas 2 audio option or a subtitle to display the button.
     */
    public void updateOptionsButton(IContentItem contentItem) {
        PlayerButtonView playerOptions = getViewHolder().getOptionsButton();
        if (mParentalControlManager.isRestricted(contentItem, true)) {
            // Restricted content.
            playerOptions.setVisibility(View.GONE);
            return;
        }

        if (mPlaybackDetailsManager.isCommandStarting(StartCommand.class)) {
            // Start not finished yet.
            playerOptions.setVisibility(View.GONE);
            return;
        }

        boolean showOptions;
        // We need at least 2 audio tracks to choose from.
        ArrayList<MediaTrackInfo> audioTrackList = mPlaybackDetailsManager.getAvailableAudioTracks(2);

        // We need at least one subtitle track, because we'll also have an off option.
        ArrayList<MediaTrackInfo> subtitleTrackList = mPlaybackDetailsManager.getAvailableSubtitlesTracks(1);

        showOptions = !subtitleTrackList.isEmpty() || !audioTrackList.isEmpty();
        playerOptions.setVisibility(showOptions ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onCommandSuccess(PlaybackCommand cmd, PlaybackDetails details) {
        super.onCommandSuccess(cmd, details);

        runOnUiThread(() -> {
            if (cmd instanceof StartCommand && cmd.getDomain() != PlaybackDomain.TrickPlay) {
                // Update audio/subtitle button after start finished.
                updatePlaybackButtons(getPlaybackData());
                updateOptionsButton(getPlaybackData());

                // Update seekBar after start finished.
                updateSeekBar(details);

                // Hide player ui if unskippable advertisement period is playing
                if (mNavigator.isPlayerUIOnTop() && details.isUnskippablePosition()) {
                    mNavigator.hideContentOverlay();
                }
            }

            if (cmd instanceof JumpCommand) {
                updatePlaybackButtons(getPlaybackData());
            }

            if (mNavigator.isContentOverlayDisplayed()) {
                if (cmd instanceof PauseCommand) {
                    focusSeekBar();
                }

                if (cmd instanceof ResumeCommand
                        && !getViewHolder().getSeekbar().isSeekingInProgress()) {
                    focusButtonContainer();
                }
            }
        });
    }

    @Override
    public void onCommandFailure(PlaybackCommand cmd, PlaybackDetails details, PlaybackError error) {
        super.onCommandFailure(cmd, details, error);
        if (cmd instanceof StartCommand && cmd.getDomain() != PlaybackDomain.TrickPlay) {
            runOnUiThread(() -> {
                if (isPlaybackChanged(cmd)) {
                    onPlaybackChanged();
                }

                updatePlaybackButtons(getPlaybackData());
                getViewHolder().getPlayedTimeView().setText(null);
                getViewHolder().getLeftTimeView().setText(null);
            });
        }

        if (error.getReason() == PlaybackError.Type.UNSKIPPABLE_REGION) {
            runOnUiThread(() -> mNavigator.showPlayerUI(StartAction.Undefined, null));
        }
    }

    @Override
    public void onPlaybackEvent(PlaybackEvent event, PlaybackDetails details, PlaybackError error) {
        super.onPlaybackEvent(event, details, error);
        switch (event) {
            case TracksChanged:
                // Update audio/subtitle button.
                runOnUiThread(() -> updateOptionsButton(getPlaybackData()));
                break;
            case PlaybackUnavailable:
            case PlaybackFailed:
            case PlaybackDisconnected:
                runOnUiThread(() -> updatePlaybackButtons(getPlaybackData()));
                break;
        }
    }

    private final ParentalControlManager.IParentalControlListener mParentalControlListener = parentalRating -> {
        TContentItem playerData = getPlaybackData();
        if (playerData != null) {
            updatePlaybackDetails(playerData);
        }
    };

    /**
     * This method will change the button's visibility by the condition and returns
     * true if the visibility has changed.
     */
    protected boolean changeButtonVisibility(View buttonView, boolean condition) {
        int newVisibility = condition ? View.VISIBLE : View.GONE;
        boolean hasVisibilityChanged = newVisibility != buttonView.getVisibility();
        buttonView.setVisibility(newVisibility);
        return hasVisibilityChanged;
    }

    /**
     * On Resume command focuses the button's container.
     */
    private void focusButtonContainer() {
        PlayerButtonContainer buttonContainer = getViewHolder().getButtonContainer();
        if (mPlaybackDetailsManager.inPlaybackState(PlaybackState.Started) && buttonContainer != null) {
            buttonContainer.requestFocus();
        }
    }

    protected interface ContentPlayerViewHolder extends BasePlayerViewHolder {

        PlayerButtonView getStartOverButton();

        PlayerButtonView getOptionsButton();

        PlayerButtonView getMoreInfoButton();

        FontTextView getTitleView();

        BaseOrderedIconsContainer getBaseOrderedIconsContainer();
    }
}
