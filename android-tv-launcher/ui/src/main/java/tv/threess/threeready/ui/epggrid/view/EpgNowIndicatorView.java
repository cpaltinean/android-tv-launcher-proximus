package tv.threess.threeready.ui.epggrid.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;

/**
 * Custom view to handle the vertical live line positioning.
 * <p>
 * Created by Bartalus Csaba - Zsolt since 2021.09.27.
 */

public class EpgNowIndicatorView extends View {

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    private EpgTimeLineView mTimeLineView;

    public EpgNowIndicatorView(Context context) {
        this(context, null);
    }

    public EpgNowIndicatorView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EpgNowIndicatorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public EpgNowIndicatorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setBackgroundColor(mLayoutConfig.getFontColor());
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mAutoPositionUpdateRunnable.run();
        if (mTimeLineView != null) {
            mTimeLineView.removeTimeLineChangeListener(mTimeLineChangeListener);
            mTimeLineView.addTimeLineChangeListener(mTimeLineChangeListener);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(mAutoPositionUpdateRunnable);
        mTimeLineView.removeTimeLineChangeListener(mTimeLineChangeListener);
    }

    public void setTimeLineView(EpgTimeLineView timeLineView) {
        mTimeLineView = timeLineView;
        removeCallbacks(mAutoPositionUpdateRunnable);
        mAutoPositionUpdateRunnable.run();
        mTimeLineView.addTimeLineChangeListener(mTimeLineChangeListener);
    }

    /**
     * Handle view position changes.
     */
    private void syncVerticalLine() {
        if (mTimeLineView != null && mTimeLineView.isLiveTimeVisible()) {
            setVisibility(VISIBLE);
            long now = TimeBuilder.utc().floor(TimeUnit.MINUTES).get();
            long differenceTime = now - mTimeLineView.getFrom();
            long translationX = mTimeLineView.convertMillisToPixel(differenceTime) + getWidth();
            setTranslationX(translationX);
            return;
        }
        setVisibility(View.GONE);
    }

    /**
     * Runnable for automatic position update after every minutes.
     */
    private final Runnable mAutoPositionUpdateRunnable = new Runnable() {

        private final long AUTO_UPDATE_PERIOD = TimeUnit.MINUTES.toMillis(1);

        @Override
        public void run() {
            postDelayed(this, AUTO_UPDATE_PERIOD);
            syncVerticalLine();
        }
    };

    private final EpgTimeLineView.TimeLineChangeListener mTimeLineChangeListener = new EpgTimeLineView.TimeLineChangeListener() {
        @Override
        public void onTimeLineChanged(long from, long to) {
            syncVerticalLine();
        }
    };

}
