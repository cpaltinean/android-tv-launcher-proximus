package tv.threess.threeready.ui.startup.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

public class BackButtonDisabledEditText extends EditText {

    public BackButtonDisabledEditText(Context context) {
        super(context);
    }

    public BackButtonDisabledEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BackButtonDisabledEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public BackButtonDisabledEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_UP) {
            return true;
        }
        return super.onKeyPreIme(keyCode, event);
    }
}
