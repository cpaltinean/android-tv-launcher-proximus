package tv.threess.threeready.ui.tv.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;

import java.util.Objects;

import io.reactivex.annotations.Nullable;
import tv.threess.threeready.api.home.model.generic.ContentMarkers;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.player.RestartMode;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.ui.databinding.PlayerBaseContainerBinding;
import tv.threess.threeready.ui.databinding.RadioPlayerBinding;
import tv.threess.threeready.ui.generic.player.view.PlayerButtonContainer;
import tv.threess.threeready.ui.generic.view.BaseOrderedIconsContainer;
import tv.threess.threeready.ui.generic.view.BroadcastOrderedIconsContainer;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.generic.view.PlaybackActionView;
import tv.threess.threeready.ui.tv.view.ContentMarkersView;
import tv.threess.threeready.ui.tv.view.PlayerButtonView;
import tv.threess.threeready.ui.tv.view.PlayerSubtitleDetailView;
import tv.threess.threeready.ui.tv.view.SeekBarView;

/**
 * Fragment used to display the radio player UI
 *
 * @author Denisa Trif
 * @since 2018.08.03
 */
public class RadioPlayerFragment extends BroadcastPlayerFragment {
    public static final String TAG = Log.tag(RadioPlayerFragment.class);

    private static final String EXTRA_CHANNEL_ID = "EXTRA_CHANNEL_ID";
    private static final String EXTRA_CHANNEL_IS_FAVORITE = "EXTRA_CHANNEL_IS_FAVORITE";

    private String mChannelId;

    private RadioPlayerBinding mBinding;
    private PlayerBaseContainerBinding mBaseContainerBinding;

    public static RadioPlayerFragment newInstance(String channelId, boolean isFavorite, boolean shouldStartPlayback) {
        Bundle args = new Bundle();
        args.putBoolean(EXTRA_SHOULD_START_PLAYBACK, shouldStartPlayback);
        args.putString(EXTRA_CHANNEL_ID, channelId);
        args.putBoolean(EXTRA_CHANNEL_IS_FAVORITE, isFavorite);

        RadioPlayerFragment fragment = new RadioPlayerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mChannelId = getStringArgument(EXTRA_CHANNEL_ID);
    }

    @Override
    protected void initializeButtons() {
        super.initializeButtons();

        // Put jump to live button to gone by default on this page.
        mBinding.buttonsContainer.jumpToLive.setVisibility(View.GONE);
        mBinding.buttonsContainer.recordButton.setVisibility(View.GONE);
    }

    @Override
    protected void updateRecordingButton(IBroadcast broadcast, @Nullable RecordingStatus recordingStatus) {
        // Ignore. We don't have to display the recording button in radio player.
    }

    @Override
    protected View getRootView() {
        return mBinding != null ? mBinding.getRoot() : null;
    }

    @Override
    public View inflateRootView() {
        if (mBinding == null) {
            mBinding = RadioPlayerBinding.inflate(LayoutInflater.from(getContext()));
            mBaseContainerBinding = PlayerBaseContainerBinding.bind(mBinding.getRoot());
        }

        return mBinding.getRoot();
    }

    @Override
    protected BroadcastPlayerViewHolder getViewHolder() {
        return mViewHolder;
    }

    @Override
    protected void startPlayback(StartAction action) {
        String channelId = getStringArgument(EXTRA_CHANNEL_ID);
        boolean isFavorite = getBooleanArgument(EXTRA_CHANNEL_IS_FAVORITE);

        if (TextUtils.isEmpty(channelId)) {
            Log.e(TAG, "No channel provided to start the playback.");
            return;
        }

        Log.d(TAG, "startPlayback() called with: channel = [" + channelId
                + "], favorite = [" + isFavorite + "], action = [" + action  + "]");
        mPlaybackDetailsManager.startChannel(action, channelId, isFavorite, RestartMode.Channel);
    }

    @Override
    protected void onPlaybackChanged() {
        // Is for the active playback. Just update the player UI.
        if (isForActivePlayback()) {
            updateSeekBar(mPlaybackDetailsManager.getExclusiveDetails());

            // Always change the player if the user changes to other playback types. Like replay or vod.
        } else if (!mPlaybackDetailsManager.inPlaybackType(PlaybackType.LiveTvIpTv, PlaybackType.RadioIpTv)) {
            mNavigator.showPlayerUI();

            // Only change the player UI if it's displayed.
        } else if (mNavigator.isContentOverlayDisplayed()) {
            mNavigator.showPlayerUI();
        }
    }

    public String getChannelId() {
        return mChannelId;
    }

    @Override
    @Nullable
    public IBroadcast getPlaybackData() {
        return mPlaybackDetailsManager.getLivePlayerData();
    }

    @Override
    protected boolean isForActivePlayback() {
        PlaybackType playerType = mPlaybackDetailsManager.getPlayerType();
        return playerType == PlaybackType.RadioIpTv
                && mPlaybackDetailsManager.getLivePlayerData() != null
                && Objects.equals(getStringArgument(EXTRA_CHANNEL_ID), mPlaybackDetailsManager.getChannelId());
    }

    @Override
    protected void updateMoreInfoButton(IBroadcast contentItem) {
        mBinding.buttonsContainer.moreInfoButton.setVisibility(View.GONE);
    }

    @Override
    protected void updateContentMarkers(IBroadcast broadcast) {
        mBinding.detailsLayout.contentMarker.showMarkers(broadcast, ContentMarkers.TypeFilter.Player);
    }

    @Override
    protected void updateProviderLogo(IBroadcast broadcast) {
        // Ignore. We don't have to display the logo.
        if (mPlaybackDetailsManager.getExclusiveDetails().getCurrentVideoTrack() != null) {
            super.updateProviderLogo(broadcast);
        }
    }

    private final BroadcastPlayerViewHolder mViewHolder = new BroadcastPlayerViewHolder() {
        @Override
        public View getUnskippableContainer() {
            return mBinding.unskippableAdvertisement.unskippableAdvertisement;
        }

        @Override
        public SeekBarView getSeekbar() {
            return mBinding.seekBarLayout.seekBar;
        }

        @Override
        public View getSeekBarLayout() {
            return mBinding.seekBarLayout.seekBarLayout;
        }

        @Override
        public ViewGroup getDetailContainer() {
            return mBinding.detailsLayout.detailContainer;
        }

        @Override
        public PlayerButtonContainer getButtonContainer() {
            return mBinding.buttonsContainer.buttonContainer;
        }

        @Override
        public TextView getUnskippableMessage() {
            return mBinding.unskippableAdvertisement.unskippableMessage;
        }

        @Override
        public TextView getUnskippableTime() {
            return mBinding.unskippableAdvertisement.unskippableTime;
        }

        @Override
        public FontTextView getPlayedTimeView() {
            return mBinding.seekBarLayout.playedTime;
        }

        @Override
        public FontTextView getLeftTimeView() {
            return mBinding.seekBarLayout.leftTime;
        }

        @Override
        public PlaybackActionView getPlaybackActionView() {
            return mBaseContainerBinding.playbackAction;
        }

        @Override
        public PlayerButtonView getStartOverButton() {
            return mBinding.buttonsContainer.startOver;
        }

        @Override
        public PlayerButtonView getOptionsButton() {
            return mBinding.buttonsContainer.optionsButton;
        }

        @Override
        public PlayerButtonView getMoreInfoButton() {
            return mBinding.buttonsContainer.moreInfoButton;
        }

        @Override
        public FontTextView getTitleView() {
            return mBinding.detailsLayout.programTitle;
        }

        @Override
        public ImageView getLogoView() {
            return mBaseContainerBinding.logo;
        }

        @Override
        public BaseOrderedIconsContainer getBaseOrderedIconsContainer() {
            return mBinding.detailsLayout.iconsContainer;
        }

        @Override
        public PlayerSubtitleDetailView getSubtitleView() {
            return mBinding.detailsLayout.subtitleDetails;
        }

        @Override
        public PlayerButtonView getJumpToLive() {
            return mBinding.buttonsContainer.jumpToLive;
        }

        @Override
        public PlayerButtonView getRecordButton() {
            return mBinding.buttonsContainer.recordButton;
        }

        @Override
        public ContentMarkersView getContentMarkerView() {
            return mBinding.detailsLayout.contentMarker;
        }

        @Override
        public AppCompatImageView getFavouriteIcon() {
            return mBinding.detailsLayout.favoriteIcon;
        }

        @Override
        public BroadcastOrderedIconsContainer getBroadcastOrderedContainer() {
            return mBinding.detailsLayout.iconsContainer;
        }
    };
}
