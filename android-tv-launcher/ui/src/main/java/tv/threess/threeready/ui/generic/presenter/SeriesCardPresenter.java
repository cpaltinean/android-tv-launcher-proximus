/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.generic.presenter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import androidx.leanback.widget.Presenter;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import java.util.Objects;
import io.reactivex.Observable;
import tv.threess.threeready.api.generic.model.IBaseSeries;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Base card presenter for TV and VOD series cards.
 *
 * @author Barabas Attila
 * @since 2018.12.04
 */
public abstract class SeriesCardPresenter<THolder extends SeriesCardPresenter.ViewHolder, TSeries extends IBaseSeries>
        extends ContentCardPresenter<THolder, TSeries> {

    protected SeriesCardPresenter(Context context) {
        super(context);
    }

    @Override
    public void onBindHolder(ModuleData<?> moduleData, THolder holder, TSeries item) {
        super.onBindHolder(moduleData, holder, item);
        updateContentDescription(holder, item);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        ViewHolder holder = createViewHolder(parent);
        holder.getTitleView().setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.getDetailsView().setTextColor(mLayoutConfig.getPlaceholderTransparentFontColor());
        holder.getLabelView().setBackgroundColor(mLayoutConfig.getPlaceHolderColor());
        holder.mPlaceHolderDrawable.setColor(mLayoutConfig.getPlaceHolderColor());
        return holder;
    }

    protected abstract ViewHolder createViewHolder(ViewGroup parent);

    @Override
    public long getStableId(TSeries series) {
        return Objects.hash(series.getId());
    }

    @Override
    public Observable<IBookmark> getBookmarkObservable(THolder holder, TSeries series) {
        //We don't have bookmark here
        return null;
    }

    @Override
    protected void updateInfoRowText(THolder holder, TSeries series) {
        holder.getDetailsView().setVisibility(View.VISIBLE);
    }

    protected void updateContentDescription(THolder holder, TSeries series) {
        if (!mMwRepository.isTalkBackOn()) {
            return;
        }

        holder.view.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_CARDS_SERIES))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_ASSET, series.getTitle())
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_CONTENT_MARKER, "")
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_ICONS, holder.getIconContainer().getIconsDescription(mTranslator))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_SERIES_EPISODE_NUMBER, String.valueOf(getNumberOfEpisodes(series)))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_GENRE, getGenres(series).toString())
                        .toString()
        );
    }

    protected abstract int getNumberOfEpisodes(TSeries series);

    protected abstract List<String> getGenres(TSeries series);

    public abstract static class ViewHolder extends ContentCardPresenter.ViewHolder {

        ColorDrawable mPlaceHolderDrawable = new ColorDrawable();

        public ViewHolder(View view) {
            super(view);
        }

        @Override
        public ColorDrawable getPlaceHolderDrawable() {
            return mPlaceHolderDrawable;
        }

        @NotNull
        public abstract View getLabelView();
    }
}
