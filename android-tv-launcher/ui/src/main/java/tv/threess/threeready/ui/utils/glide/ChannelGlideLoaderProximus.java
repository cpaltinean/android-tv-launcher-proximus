package tv.threess.threeready.ui.utils.glide;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader;

import java.io.InputStream;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.ImageRepository;
import tv.threess.threeready.api.tv.model.TvChannel;

/**
 * Custom glide loader for channel items.
 *
 * Created by Noemi Dali on 07.04.2020.
 */
public class ChannelGlideLoaderProximus extends BaseGlideUrlLoader<TvChannel> {

    private final ImageRepository mImageRepository = Components.get(ImageRepository.class);

    public ChannelGlideLoaderProximus(ModelLoader<GlideUrl, InputStream> concreteLoader) {
        super(concreteLoader);
    }

    @Override
    protected String getUrl(TvChannel tvChannel, int width, int height, Options options) {
        return mImageRepository.getLogoUrl(tvChannel, width, height);
    }

    @Override
    public boolean handles(@NonNull TvChannel tvChannel) {
        return true;
    }
}
