package tv.threess.threeready.ui.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import java.security.MessageDigest;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.request.RequestOptions;

import tv.threess.threeready.api.log.Log;

/**
 * Resize the image to fit the width and height, taking into consideration the aspect ratio of the view
 * in case the aspect ratio of the view is 1:2 but the image is 1:1
 * the image will be scaled to occupy half of the view and centered
 *
 * @author Oltean Ionut
 * @since 2018.01.10
 */
public class ImageTransform extends BitmapTransformation {
    private static final String TAG = Log.tag(ImageTransform.class);

    private static final byte[] TAG_BYTES = TAG.getBytes(CHARSET);

    public static final RequestOptions REQUEST_OPTIONS = new RequestOptions()
            .downsample(new SmoothDownSampleStrategy())
            .transform(new ImageTransform());

    private static final Paint PAINT = new Paint(Paint.FILTER_BITMAP_FLAG | Paint.DITHER_FLAG | Paint.ANTI_ALIAS_FLAG);
    private final double mMaxScale;

    public ImageTransform(double maxScale) {
        mMaxScale = maxScale;
    }

    public ImageTransform() {
        this(1);
    }

    @Override
    protected Bitmap transform(@NonNull BitmapPool bitmapPool, @NonNull Bitmap original, int outWidth, int outHeight) {
        if (original.getWidth() == outWidth && original.getHeight() == outHeight) {
            return original;
        }

        double widthScale = outWidth / (double) original.getWidth();
        double heightScale = outHeight / (double) original.getHeight();
        double scale = Math.min(widthScale, heightScale);
        if (scale > mMaxScale) {
            // allow only upscaling
            scale = mMaxScale;
        }

        double outRatio = outWidth / (double) outHeight;
        if (outRatio > 1) {
            outRatio = 1 / outRatio;
        }

        double ratio = original.getWidth() / (double) original.getHeight();
        if (ratio > 1) {
            ratio = 1 / ratio;
        }

        if (ratio > outRatio) {
            scale *= Math.sqrt(outRatio / ratio);
        }

        double dx = (outWidth - original.getWidth() * scale) / 2;
        double dy = (outHeight - original.getHeight() * scale) / 2;

        Matrix matrix = new Matrix();
        matrix.setTranslate((float) dx, (float) dy);
        matrix.preScale((float) scale, (float) scale);

        Bitmap result = bitmapPool.get(outWidth, outHeight, original.getConfig());
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(original, matrix, PAINT);
        canvas.setBitmap(null);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ImageTransform;
    }

    @Override
    public int hashCode() {
        return TAG.hashCode();
    }

    @Override
    public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {
        messageDigest.update(TAG_BYTES);
    }

    private static class SmoothDownSampleStrategy extends DownsampleStrategy {

        @Override
        public float getScaleFactor(int sourceWidth, int sourceHeight, int requestedWidth, int requestedHeight) {
            float widthScale = requestedWidth / (float) sourceWidth;
            float heightScale = requestedHeight / (float) sourceHeight;
            float scaleFactor = Math.min(widthScale, heightScale);
            if (scaleFactor > .75) {
                // skip scaling if the resulting image is not scaled too much
                // this improves performance without losing quality
                scaleFactor = 1;
            }
            return scaleFactor;
        }

        @Override
        public SampleSizeRounding getSampleSizeRounding(int sourceWidth, int sourceHeight, int requestedWidth, int requestedHeight) {
            return DownsampleStrategy.SampleSizeRounding.QUALITY;
        }
    }
}
