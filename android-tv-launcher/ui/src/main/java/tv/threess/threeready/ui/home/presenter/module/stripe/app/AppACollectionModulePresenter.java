package tv.threess.threeready.ui.home.presenter.module.stripe.app;

import android.content.Context;
import android.view.ViewGroup;

import androidx.leanback.widget.ClassPresenterSelector;

import tv.threess.threeready.api.gms.model.InstalledAppInfo;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.home.presenter.card.app.AppA1CardPresenter;
import tv.threess.threeready.ui.home.presenter.card.app.EditorialA1CardPresenter;
import tv.threess.threeready.ui.home.presenter.module.collection.generic.CollectionModulePresenter;
import tv.threess.threeready.ui.home.view.ModularPageView;

/**
 * presenter used to display application type, A variant collections
 *
 * @author hunormadaras
 * @since 18/02/2019
 */
public class AppACollectionModulePresenter extends CollectionModulePresenter {

    public AppACollectionModulePresenter(Context context, ModularPageView.PageTitleCallback pageTitle, DataSourceLoader dataSourceLoader) {
        super(context, pageTitle, dataSourceLoader);
        mVariantCardPresenterMap.put(ModuleCardVariant.A1, new ClassPresenterSelector()
                .addClassPresenter(InstalledAppInfo.class, new AppA1CardPresenter(context))
                .addClassPresenter(EditorialItem.class, new EditorialA1CardPresenter(context))
        );
    }

    @Override
    protected int getColumnNumber() {
        return 6;
    }

    @Override
    public void onBindHolder(ViewHolder holder, ModuleData<?> data) {
        super.onBindHolder(holder, data);

        ((ViewGroup.MarginLayoutParams) holder.mBinding.collection.getTitleView().getLayoutParams()).leftMargin = mContext.getResources().getDimensionPixelOffset(R.dimen.a1_collection_module_title_left_margin);
        ((ViewGroup.MarginLayoutParams) holder.mBinding.collection.getTitleView().getLayoutParams()).topMargin = mContext.getResources().getDimensionPixelOffset(R.dimen.a1_collection_module_title_top_margin);

    }

    /**
     * Gets the value for horizontal padding.
     */
    @Override
    protected int getHorizontalPadding() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.stripe_title_left_margin);
    }

    /**
     * Gets the value for top padding.
     */
    @Override
    protected int getTopPadding(ViewHolder viewHolder) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.a1_collection_grid_view_top_padding);
    }

    /**
     * Gets the value for bottom padding.
     */
    @Override
    protected int getBottomPadding() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.channel_c_collection_grid_view_top_padding);
    }

    /**
     * Gets the value for item spacing.
     */
    @Override
    protected int getItemSpacing() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.app_a1_stripe_card_spacing);
    }
}
