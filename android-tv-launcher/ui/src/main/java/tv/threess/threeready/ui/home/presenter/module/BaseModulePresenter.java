/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.home.presenter.module;

import android.content.Context;
import android.util.Pair;

import androidx.leanback.widget.ItemAlignmentFacet;
import androidx.leanback.widget.Presenter;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.ApiConfig;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;

/**
 * Presenter class for a module
 * Responsible to load the data from the given data source and display it.
 * The loaded data will be displayed with cards. The type of the card is based on the module type and variant.
 *
 * @author Barabas Attila
 * @since 2017.06.07
 */
public abstract class BaseModulePresenter<THolder extends Presenter.ViewHolder>
        extends BasePresenter<THolder, ModuleData<?>> {
    public static final String TAG = Log.tag(BaseModulePresenter.class);

    public static final int APP_SCROLL_ALIGNMENT_PERCENTAGE_OFFSET = 40;

    protected DataSourceLoader mDataSourceLoader;

    protected final ApiConfig mApiConfig = Components.get(ApiConfig.class);

    public BaseModulePresenter(Context context, DataSourceLoader dataSourceLoader) {
        super(context);
        mDataSourceLoader = dataSourceLoader;
    }

    public abstract Pair<Integer, Integer> getLimit(ModuleConfig moduleConfig);

    /**
     * to handle offset, pass 0 to remain at the corresponding position
     *
     * @param holder        for which holder
     * @param offsetPercent value of offset in percentage
     */
    protected void setAlignmentOffsetPercent(Presenter.ViewHolder holder, float offsetPercent) {
        ItemAlignmentFacet.ItemAlignmentDef facetDef = new ItemAlignmentFacet.ItemAlignmentDef();
        facetDef.setItemAlignmentOffsetPercent(offsetPercent);
        ItemAlignmentFacet facet = new ItemAlignmentFacet();
        facet.setAlignmentDefs(new ItemAlignmentFacet.ItemAlignmentDef[]{facetDef});
        holder.setFacet(ItemAlignmentFacet.class, facet);
    }
}
