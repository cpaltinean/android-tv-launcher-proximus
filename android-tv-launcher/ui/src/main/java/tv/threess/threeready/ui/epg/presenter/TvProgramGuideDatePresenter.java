/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.epg.presenter;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.TvProgramGuideDateBinding;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Display timestamp divider for a day change.
 *
 * Created by Szilard on 10/2/2017.
 */

public class TvProgramGuideDatePresenter extends BaseCardPresenter<TvProgramGuideDatePresenter.ViewHolder, Long> {

    private final DateTimeFormatter mDateFormat = DateTimeFormatter.ofPattern("dd", Locale.getDefault());

    private final Translator mTranslator = Components.get(Translator.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    public TvProgramGuideDatePresenter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        TvProgramGuideDateBinding binding = TvProgramGuideDateBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindHolder(ViewHolder holder, Long date) {
        super.onBindHolder(holder, date);

        // Display date
        String day = LocaleTimeUtils.getDayNameLocalized(date, mTranslator, false);

        String dayOfMonth = TimeUtils.getTimeFormat(mDateFormat, date);
        String month = LocaleTimeUtils.getMonthNameLocalized(date, mTranslator, false);

        String aux = String.format(TimeUtils.PROGRAM_GUIDE_TIME_STAMP_FORMAT, day, dayOfMonth, month);
        SpannableString spannableString = new SpannableString(aux);

        int index = aux.indexOf(TimeUtils.DIVIDER);
        while (index >= 0) {
            spannableString.setSpan(new ForegroundColorSpan(mLayoutConfig.getPlaceholderTransparentFontColor()), index+1, index+2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            index = aux.indexOf(TimeUtils.DIVIDER, index + 1);
        }

        holder.mBinding.guideDate.setTextColor(mLayoutConfig.getPlaceholderTransparentFontColor());
        holder.mBinding.guideDate.setText(spannableString);
    }

    @Override
    public int getCardWidth(Long date) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_width_focused);
    }

    @Override
    public int getCardHeight(Long aLong) {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.tv_program_guide_card_height_focused);
    }

    @Override
    public long getStableId(Long date) {
        return Objects.hash(date);
    }

    static class ViewHolder extends BaseCardPresenter.ViewHolder {

        private final TvProgramGuideDateBinding mBinding;

        public ViewHolder(TvProgramGuideDateBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
