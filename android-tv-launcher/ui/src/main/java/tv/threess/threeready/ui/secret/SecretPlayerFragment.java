package tv.threess.threeready.ui.secret;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.config.model.PlaybackSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.databinding.SecretPlayerFragmentBinding;
import tv.threess.threeready.ui.generic.fragment.BaseFragment;
import tv.threess.threeready.ui.utils.SystemUtils;

/**
 * Secret fragment for displaying player related settings.
 * Here you can set the player configuration to custom values.
 *
 * Configuration that can be set:
 *  - ABR mechanism and configuration
 *  - Buffer configuration
 *  - Live playback configuration
 *  - Retry configuration
 *  - Debug overlay
 *
 * @author Andor Lukacs
 * @since 2019-11-12
 */
public class SecretPlayerFragment extends BaseFragment {

    private SecretPlayerFragmentBinding mBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mBinding = SecretPlayerFragmentBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        mBinding.playerVersionText.setText("Castlabs version: " + Settings.playerVersion.get(""));

        PlaybackSettings settings = Components.get(AppConfig.class).getPlaybackSettings();

        mBinding.minPlaybackStart.setText(String.valueOf(Settings.minimumPlaybackStartSeconds.get(settings.getMinimumPlaybackStartSeconds())));
        mBinding.bufferSize.setText(String.valueOf(Settings.bufferSize.get(settings.getBufferSize())));
        mBinding.lowMediaTimeTrickplay.setText(String.valueOf(Settings.lowMediaTimeTrickplay.get(settings.getLowMediaTimeTrickPlay())));
        mBinding.highMediaTimeTrickplay.setText(String.valueOf(Settings.highMediaTimeTrickplay.get(settings.getHighMediaTimeTrickPlay())));
        mBinding.lowMediaTime.setText(String.valueOf(Settings.lowMediaTime.get(settings.getLowMediaTime())));
        mBinding.highMediaTime.setText(String.valueOf(Settings.highMediaTime.get(settings.getHighMediaTime())));
        mBinding.toggleDrain.setChecked(Settings.drainWhileCharging.get(settings.isDrainWhileCharging()));

        mBinding.saveBuffer.setOnClickListener(v -> Settings.batchEdit()
                .put(Settings.minimumPlaybackStartSeconds, Long.valueOf(mBinding.minPlaybackStart.getText().toString()))
                .put(Settings.bufferSize, Long.valueOf(mBinding.bufferSize.getText().toString()))
                .put(Settings.lowMediaTime, Integer.valueOf(mBinding.lowMediaTime.getText().toString()))
                .put(Settings.highMediaTime, Integer.valueOf(mBinding.highMediaTime.getText().toString()))
                .put(Settings.lowMediaTimeTrickplay, Integer.valueOf(mBinding.lowMediaTimeTrickplay.getText().toString()))
                .put(Settings.highMediaTimeTrickplay, Integer.valueOf(mBinding.highMediaTimeTrickplay.getText().toString()))
                .put(Settings.drainWhileCharging, mBinding.toggleDrain.isChecked())
                .persist());

        mBinding.initQuality.setChecked(Settings.lowestInitTrack.get(settings.isLowestInitTrack()));
        mBinding.keepQuality.setChecked(Settings.abrOn.get(settings.isAbrOn()));
        mBinding.minDurationQualityIncrease.setText(String.valueOf(Settings.minDurationQualityIncrease.get(settings.getMinDurationQualityIncrease())));
        mBinding.maxDurationQualityDecrease.setText(String.valueOf(Settings.maxDurationQualityDecrease.get(settings.getMaxDurationQualityDecrease())));
        mBinding.minDurationDiscard.setText(String.valueOf(Settings.minDurationRetainAfterDiscard.get(settings.getMinDurationRetainAfterDiscard())));
        mBinding.minSamples.setText(String.valueOf(Settings.minDegradationSamples.get(settings.getMinDegradationSamples())));

        mBinding.saveAbr.setOnClickListener(v -> Settings.batchEdit()
                .put(Settings.lowestInitTrack,  mBinding.initQuality.isChecked())
                .put(Settings.abrOn, mBinding.keepQuality.isChecked())
                .put(Settings.minDurationQualityIncrease, Long.valueOf(mBinding.minDurationQualityIncrease.getText().toString()))
                .put(Settings.maxDurationQualityDecrease, Long.valueOf(mBinding.maxDurationQualityDecrease.getText().toString()))
                .put(Settings.minDurationRetainAfterDiscard, Long.valueOf(mBinding.minDurationDiscard.getText().toString()))
                .put(Settings.minDegradationSamples, Long.valueOf(mBinding.minSamples.getText().toString()))
                .persist());

        mBinding.manifestRetryCount.setText(String.valueOf(Settings.manifestRetryCount.get(settings.getManifestRetryCount())));
        mBinding.manifestRetryDelay.setText(String.valueOf(Settings.manifestRetryDelay.get(settings.getManifestRetryDelay())));
        mBinding.segmentRetryCount.setText(String.valueOf(Settings.segmentRetryCount.get(settings.getSegmentRetryCount())));
        mBinding.segmentRetryDelay.setText(String.valueOf(Settings.segmentRetryDelay.get(settings.getSegmentRetryDelay())));
        mBinding.manifestConnectTimeout.setText(String.valueOf(Settings.manifestConnectTimeout.get(settings.getManifestConnectTimeout())));
        mBinding.manifestReadTimeout.setText(String.valueOf(Settings.manifestReadTimeout.get(settings.getManifestReadTimeout())));
        mBinding.segmentConnectTimeout.setText(String.valueOf(Settings.segmentConnectTimeout.get(settings.getSegmentConnectTimeout())));
        mBinding.segmentReadTimeout.setText(String.valueOf(Settings.segmentReadTimeout.get(settings.getSegmentReadTimeout())));

        mBinding.saveRetry.setOnClickListener(v -> Settings.batchEdit()
                .put(Settings.manifestRetryCount, Long.valueOf(mBinding.manifestRetryCount.getText().toString()))
                .put(Settings.manifestRetryDelay, Long.valueOf(mBinding.manifestRetryDelay.getText().toString()))
                .put(Settings.segmentRetryCount, Long.valueOf(mBinding.segmentRetryCount.getText().toString()))
                .put(Settings.segmentRetryDelay, Long.valueOf(mBinding.segmentRetryDelay.getText().toString()))
                .put(Settings.manifestConnectTimeout, Long.valueOf(mBinding.manifestConnectTimeout.getText().toString()))
                .put(Settings.manifestReadTimeout, Long.valueOf(mBinding.manifestReadTimeout.getText().toString()))
                .put(Settings.segmentConnectTimeout, Long.valueOf(mBinding.segmentConnectTimeout.getText().toString()))
                .put(Settings.segmentReadTimeout, Long.valueOf(mBinding.segmentReadTimeout.getText().toString()))
                .persist());

        mBinding.drmConnectTimeout.setText(String.valueOf(Settings.drmConnectionTimeout.get(settings.getDrmConnectionTimeout())));
        mBinding.drmReadTimeout.setText(String.valueOf(Settings.drmReadTimeout.get(settings.getDrmReadTimeout())));
        mBinding.drmAcquisitionTimeout.setText(String.valueOf(Settings.drmAcquisitionTimeout.get(settings.getDrmAcquisitionTimeout())));

        mBinding.saveDrm.setOnClickListener(v -> Settings.batchEdit()
                .put(Settings.drmConnectionTimeout, Long.valueOf(mBinding.drmConnectTimeout.getText().toString()))
                .put(Settings.drmReadTimeout, Long.valueOf(mBinding.drmReadTimeout.getText().toString()))
                .put(Settings.drmAcquisitionTimeout, Long.valueOf(mBinding.drmAcquisitionTimeout.getText().toString()))
                .persist());

        mBinding.liveEdgeText.setText(String.valueOf(Settings.ottPlayerLiveEdgeMs.get(settings.getLiveEdgeLatencyMs())));

        mBinding.saveLiveEdge.setOnClickListener(v -> Settings.batchEdit()
                .put(Settings.ottPlayerLiveEdgeMs, Long.valueOf(mBinding.liveEdgeText.getText().toString()))
                .persist());

        mBinding.toggleDebugOverlay.setChecked(Settings.playerDebugOverlayEnabled.get(false));

        mBinding.toggleDebugOverlay.setOnCheckedChangeListener((buttonView, isChecked)
                -> Settings.playerDebugOverlayEnabled.edit().put(isChecked));

        mBinding.maxBandwidth.setText(String.valueOf(Settings.bandwidthMinimum.get(settings.getControlMinimum())));
        mBinding.filterValue.setText(String.valueOf(Settings.maximumLimitedBitrate.get(settings.getMaximumLimitedBitrate())));

        mBinding.saveBandwidth.setOnClickListener(v -> Settings.batchEdit()
                .put(Settings.bandwidthMinimum, Long.valueOf(mBinding.maxBandwidth.getText().toString()))
                .put(Settings.maximumLimitedBitrate, Long.valueOf(mBinding.filterValue.getText().toString()))
                .persist());

        // Restart action.
        mBinding.restartButton.setOnClickListener(v -> {
            Log.i(TAG, "RESTART button clicked.");
            SystemUtils.restartApplication(getActivity());
        });
    }
}
