package tv.threess.threeready.ui.epggrid.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.PlaybackSettings;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.EpgGridBinding;
import tv.threess.threeready.ui.epggrid.EpgChannelRowModel;
import tv.threess.threeready.ui.epggrid.EpgDataLoader;
import tv.threess.threeready.ui.epggrid.listener.EpgBroadcastSelectedListener;
import tv.threess.threeready.ui.epggrid.presenter.EpgChannelRowPresenter;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * EPG grid which displays the channel list and the programs on each channel.
 *
 * @author Barabas Attila
 * @since 8/26/21
 */
public class EpgGridView extends RelativeLayout {
    private static final String TAG = Log.tag(EpgGridView.class);

    private static final float CHANNEL_GRID_SMOOTH_SCROLL_SPEED_FACTOR = 12f;

    private final PlaybackSettings mPlaybackSettings = Components.get(PlaybackSettings.class);
    private final Translator mTranslator = Components.get(Translator.class);

    private final ArrayObjectAdapter<EpgChannelRowModel> mEpgAdapter = new ArrayObjectAdapter<>();
    private final ItemBridgeAdapter mBridgeAdapter;

    private final EpgDataLoader mEpgDataLoader;
    private IBroadcast mSelectedBroadcast;

    @Nullable
    private EpgBroadcastSelectedListener mBroadcastSelectedListener;

    // The channel to select after the grid loads.
    private String mChannelIdToSelect;

    private final EpgGridBinding mBinding;

    public EpgGridView(Context context) {
        this(context, null);
    }

    public EpgGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EpgGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        mBinding = EpgGridBinding.inflate(LayoutInflater.from(context), this);

        LayoutConfig layoutConfig = Components.get(LayoutConfig.class);
        mBinding.timelineDate.setTextColor(layoutConfig.getFontColor());

        mBridgeAdapter = new ItemBridgeAdapter(mEpgAdapter,
                new EpgChannelRowPresenter(getContext(), mBinding.timeline, broadcast -> {
                    mSelectedBroadcast = broadcast;
                    if (mBroadcastSelectedListener != null) {
                        mBroadcastSelectedListener.onBroadcastSelected(broadcast);
                    }
                }), true);
        mEpgAdapter.setHasStableIds(true);
        mBinding.channelGrid.setAdapter(mBridgeAdapter);
        mBinding.channelGrid.setSmoothScrollMaxPendingMoves(2);
        mBinding.channelGrid.setWindowAlignmentOffsetPercent(0);
        mBinding.channelGrid.setSmoothScrollSpeedFactor(CHANNEL_GRID_SMOOTH_SCROLL_SPEED_FACTOR);
        mBinding.channelGrid.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull @io.reactivex.annotations.NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mEpgDataLoader.onPositionSelected(mBinding.channelGrid.getSelectedPosition());
            }
        });

        // Build timeline.
        mBinding.timeline.addOnScrollListener(mScrollListener);
        mBinding.timeline.addTimeLineChangeListener(new EpgTimeLineView.TimeLineChangeListener() {
            @Override
            public void onSelectedDateChanged(long selectedDate) {
                updateSelectedDate(selectedDate);
            }
        });
        mBinding.nowIndicator.setTimeLineView(mBinding.timeline);

        // Data loading
        mEpgDataLoader = new EpgDataLoader(mBinding.timeline, mEpgDataLoadedListener);
        mBinding.timeline.addTimeLineChangeListener(mEpgDataLoader);
    }

    /**
     * Needs to be called to initiate data loading in the EPG.
     * @param channelId The id of the channel to select after the EPG loads.
     * @param initTime The time to focus after the EPG loads.
     */
    public void initiate(String channelId, long initTime) {
        long now = System.currentTimeMillis();
        mBinding.timeline.setTimeWindow(initTime,
                now - Settings.epgPastWindowLength.get(TimeUnit.DAYS.toMillis(7)),
                now + Settings.epgFutureWindowLength.get(TimeUnit.DAYS.toMillis(7)));
        updateSelectedDate(mBinding.timeline.getFrom());
        selectChannel(channelId);
        mEpgDataLoader.initiate();
    }


    /**
     * Cancel the data loading and clear the EPG grid.
     * Should be called when the grid no longer needed to avoid memory leaks.
     */
    public void destroy() {
        mEpgDataLoader.dispose();
        mBinding.channelGrid.setAdapter(null);
        mEpgAdapter.clear();
    }

    /**
     * @param listener Listener to get notifies when the selected program changes.
     */
    public void setBroadcastSelectionListener(EpgBroadcastSelectedListener listener) {
        mBroadcastSelectedListener = listener;
    }

    /**
     * Focus the row of the given channel in the EPG grid.
     * @param channelId The id of the channel to select.
     */
    public void selectChannel(String channelId) {
        ItemBridgeAdapter adapter = (ItemBridgeAdapter) mBinding.channelGrid.getAdapter();
        if (adapter == null || adapter.getItemCount() == 0) {
            mChannelIdToSelect = channelId;
            return;
        }

        for (int i = 0; i < adapter.getItemCount(); ++i) {
            EpgChannelRowModel row = mEpgAdapter.get(i);
            if (Objects.equals(channelId, row.channel.getId())) {
                mBinding.channelGrid.setSelectedPosition(mBridgeAdapter.getCircularPosition(i));
                return;
            }
        }
    }

    /**
     * Focus the program at the give time.
     * @param time The time in millis to focus program at.
     */
    public void selectTime(long time) {
        mBinding.timeline.scrollByTime(time- mBinding.timeline.getFrom()
                - EpgTimeLineView.INIT_TIME_DISPLAY_OFFSET);
        mBinding.timeline.setFocusReferenceTime(time, time);
    }

    /**
     * Focus the given channel at the given time in the EPG grid.
     * @param channelId The channel to focus on.
     * @param time The time to focus on the timeline.
     */
    public void select(String channelId, long time) {
        selectChannel(channelId);
        selectTime(time);
    }

    private void updateSelectedDate(long selectedDate) {
        mBinding.timelineDate.setText(LocaleTimeUtils.getDate(selectedDate, mTranslator));
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD: {
                    goToNextPreviousDayPrimeTime(true);
                    return true;
                }
                case KeyEvent.KEYCODE_MEDIA_REWIND: {
                    goToNextPreviousDayPrimeTime(false);
                    return true;
                }
            }
        }

        return super.dispatchKeyEvent(event);
    }

    private void goToNextPreviousDayPrimeTime(boolean isFuture) {
        if (mSelectedBroadcast == null) {
            return;
        }

        long referenceTime;
        if (isFuture) {
            referenceTime = Math.min(mSelectedBroadcast.getEnd(), mBinding.timeline.getTo());
        } else {
            referenceTime = Math.max(mSelectedBroadcast.getStart(), mBinding.timeline.getFrom());
        }

        long now = System.currentTimeMillis();
        long primeTime = isFuture ?
                TimeUtils.nextPrimeTime(referenceTime, mBinding.timeline.getWindowEnd() - now, mPlaybackSettings.getPrimeTime()) :
                TimeUtils.prevPrimeTime(referenceTime, now - mBinding.timeline.getWindowStart(), mPlaybackSettings.getPrimeTime());

        if (primeTime == -1) {
            primeTime = System.currentTimeMillis();
        }

        selectTime(primeTime);
        mBinding.channelGrid.requestFocus();
    }

    /**
     * Synchronize scrolling between timeline and program rows.
     */
    private final RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(@NonNull @NotNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            for (int i = 0; i < mBinding.channelGrid.getChildCount(); ++i) {
                EpgProgramRowGridView programRow = mBinding.channelGrid.getChildAt(i)
                        .findViewById(R.id.program_grid);
                if (programRow != null) {
                    programRow.scrollBy(dx, 0);
                }
            }
        }
    };

    private final EpgDataLoader.EpgDataLoadedListener mEpgDataLoadedListener = new EpgDataLoader.EpgDataLoadedListener() {
        @Override
        public void onChannelListLoaded(List<EpgChannelRowModel> rowsModels) {
            Log.d(TAG, "Channel list loaded. Size : " + rowsModels.size());
            mEpgAdapter.replaceAll(rowsModels);

            // Select the required channel.
            if (mChannelIdToSelect != null) {
                selectChannel(mChannelIdToSelect);
                mChannelIdToSelect = null;
            }
        }

        @Override
        public void onProgramUpdate(int position, EpgChannelRowModel rowModel) {
            Log.d(TAG, "Program list update. Channel : " + rowModel.channel.getId());
            mBridgeAdapter.notifyItemRangeChanged(position, 1, rowModel);
        }
    };
}
