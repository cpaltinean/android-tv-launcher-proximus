package tv.threess.threeready.ui.home.presenter.module.stripe.app;

import android.content.Context;

import androidx.core.content.ContextCompat;
import androidx.leanback.widget.PresenterSelector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tv.threess.threeready.api.gms.model.InstalledAppInfo;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.adapter.ModularItemBridgeAdapter;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeLabel;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeSeparatorProvider;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.home.presenter.card.app.AppA1CardPresenter;
import tv.threess.threeready.ui.home.presenter.card.app.EditorialA1CardPresenter;

/**
 * Presenter class for the B variant of the app stripe, which displays only one label.
 *
 * @author Barabas Attila
 * @since 2017.12.15
 */

public class AppBStripeModulePresenter extends AppStripeModulePresenter {

    Map<ModuleCardVariant, PresenterSelector> mVariantCardPresenterMap = new HashMap<>();

    public AppBStripeModulePresenter(Context context, DataSourceLoader dataSourceLoader) {
        super(context, dataSourceLoader);

        InterfacePresenterSelector presenterSelector = new InterfacePresenterSelector();
        presenterSelector.addClassPresenter(InstalledAppInfo.class, new AppA1CardPresenter(context))
                .addClassPresenter(EditorialItem.class, new EditorialA1CardPresenter(context, presenterSelector));
        mVariantCardPresenterMap.put(ModuleCardVariant.B1, presenterSelector);
    }

    @Override
    protected StripeSeparatorProvider<StripeLabel> getLabelProvider(ModuleData<?> moduleData, ModularItemBridgeAdapter adapter) {
        return () -> {
            List<StripeLabel> labels = new ArrayList<>();
            labels.add(new StripeLabel(0,
                    adapter.getItemCount(), mTranslator.get(moduleData.getModuleConfig().getTitle()),
                    ContextCompat.getDrawable(mContext, R.drawable.section_separator)));
            return labels;
        };
    }


    @Override
    protected PresenterSelector getCardPresenterSelector(ModuleConfig moduleConfig) {
        return mVariantCardPresenterMap.get(moduleConfig.getCardVariant());
    }
}
