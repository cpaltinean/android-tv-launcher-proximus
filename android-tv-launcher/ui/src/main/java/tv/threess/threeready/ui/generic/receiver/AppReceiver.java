/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.receiver;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;

import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.generic.receivers.BaseBroadcastReceiver;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;


/**
 * Receive package added and removed intents.
 *
 * @author Karetka Mezei Zoltan.
 * @since 2019.08.21
 */
public class AppReceiver extends BaseBroadcastReceiver {
    private static final String TAG = Log.tag(AppReceiver.class);

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Received package change intent", intent);
        if (intent == null || intent.getAction() == null) {
            return;
        }
        Uri uri = intent.getData();
        if (uri == null) {
            return;
        }

        if (Intent.ACTION_PACKAGE_ADDED.equals(intent.getAction())) {
            if (!intent.getBooleanExtra(Intent.EXTRA_REPLACING, false)) {
                Log.event(new Event<>(UILogEvent.AppInstalled)
                        .addDetail(UILogEvent.Detail.PackageName, uri.getSchemeSpecificPart())
                );
            } else {
                Log.event(new Event<>(UILogEvent.AppUpdated)
                        .addDetail(UILogEvent.Detail.PackageName, uri.getSchemeSpecificPart())
                );
            }
        }

        if (Intent.ACTION_PACKAGE_REMOVED.equals(intent.getAction())) {
            if (!intent.getBooleanExtra(Intent.EXTRA_REPLACING, false)) {
                Log.event(new Event<>(UILogEvent.AppUninstalled)
                        .addDetail(UILogEvent.Detail.PackageName, uri.getSchemeSpecificPart())
                );
            }
        }
    }

    @Override
    public IntentFilter getIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        intentFilter.addDataScheme(PackageUtils.SCHEMA_PACKAGE);
        return intentFilter;
    }
}
