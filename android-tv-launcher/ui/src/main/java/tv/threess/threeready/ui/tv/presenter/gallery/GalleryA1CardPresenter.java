/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.tv.presenter.gallery;

import android.content.Context;

import androidx.leanback.widget.Presenter;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;

/**
 * Card presenter class for gallery type and A1 variant card.
 *
 * @author Eugen Guzyk
 * @since 2018.09.19
 */
public class GalleryA1CardPresenter extends BaseCardPresenter {

    public GalleryA1CardPresenter(Context context) {
        super(context);
    }

    @Override
    public int getCardWidth(Object o) {
        return 0;
    }

    @Override
    public int getCardHeight(Object o) {
        return 0;
    }

    @Override
    public void onBindHolder(Presenter.ViewHolder holder, Object o) {

    }

}
