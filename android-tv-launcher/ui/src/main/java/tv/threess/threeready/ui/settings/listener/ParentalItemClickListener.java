package tv.threess.threeready.ui.settings.listener;

/**
 * This class will be used to notify if parental control is enabled or not.
 *
 * @author Daniela Toma
 * @since 2019.06.28
 */
public interface ParentalItemClickListener extends ItemClickListener {
}
