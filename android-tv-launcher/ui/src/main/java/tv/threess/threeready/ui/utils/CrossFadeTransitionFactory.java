package tv.threess.threeready.ui.utils;

import android.graphics.drawable.Drawable;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.request.transition.DrawableCrossFadeTransition;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.request.transition.TransitionFactory;

/**
 * @author Tatiana Buha
 * @since 2019.10.10
 */
public class CrossFadeTransitionFactory implements TransitionFactory<Drawable> {
    private static final int TRANSITION_DURATION  = 300;

    @Override
    public Transition<Drawable> build(DataSource dataSource, boolean isFirstResource) {
        return new DrawableCrossFadeTransition(TRANSITION_DURATION, true);
    }
}
