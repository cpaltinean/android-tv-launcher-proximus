package tv.threess.threeready.ui.generic.presenter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;

import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.ui.R;

/**
 * Base Card cover presenter. This presenter contains only a image cover view.
 *
 * @author Solyom Zsolt
 * @since 2020.02.24
 */
public abstract class CoverCardPresenter<THolder extends CoverCardPresenter.ViewHolder, TItem extends IBaseContentItem>
        extends LockedCardPresenter<THolder, TItem> {

    public CoverCardPresenter(Context context) {
        super(context);
    }

    @Override
    public void onBindHolder(ModuleData<?> moduleData, THolder holder, TItem item) {
        super.onBindHolder(moduleData, holder, item);
        updateCoverImage(holder, item);
    }

    @Override
    protected void onParentalRatingChanged(THolder holder, TItem item) {
        super.onParentalRatingChanged(holder, item);
        updateCoverImage(holder, item);
    }

    @Override
    public void onUnbindHolder(ModuleData<?> moduleData, THolder holder) {
        super.onUnbindHolder(moduleData, holder);
        Glide.with(mContext).clear(holder.getCoverView());
    }

    /**
     * Update the cover image on the card.
     * It can be landscape or portrait based on the content type.
     */
    protected void updateCoverImage(THolder holder, TItem item) {
        Glide.with(mContext).clear(holder.getCoverView());

        // Get the size of cover image.
        int coverWidth = getCoverWidth(item);
        int coverHeight = getCoverHeight(item);

        RequestBuilder<Drawable> requestBuilder;
        if (mParentalControlManager.isRestricted(item)) {
            // Loaded restricted cover.
            requestBuilder = Glide.with(mContext)
                    .load(coverWidth > coverHeight ?
                            R.drawable.locked_cover_landscape : R.drawable.locked_cover_portrait);
        } else {
            requestBuilder = Glide.with(mContext).load(item)
                    .fallback(coverWidth > coverHeight ? R.drawable.content_fallback_image_landscape : R.drawable.content_fallback_image_portrait)
                    .error(coverWidth > coverHeight ? R.drawable.content_fallback_image_landscape : R.drawable.content_fallback_image_portrait);
        }

        // Load the cover image.
        loadCoverImage(holder, item, coverWidth, coverHeight, requestBuilder);
    }

    /**
     * Loads the cover image.
     */
    protected void loadCoverImage(THolder holder, TItem item, int coverWidth, int coverHeight, RequestBuilder<Drawable> requestBuilder) {
        requestBuilder
                .apply(new RequestOptions()
                        .placeholder(holder.getPlaceHolderDrawable())
                        .override(coverWidth, coverHeight)
                        .optionalCenterInside())
                .transition(new DrawableTransitionOptions().dontTransition())
                .into(holder.getCoverView());
    }

    /**
     * @return The height of the cover image in pixels.
     */
    protected abstract int getCoverHeight(TItem item);

    /**
     * @return The width of the cover image in pixels.
     */
    protected abstract int getCoverWidth(TItem item);

    /**
     * View holder which binds and holds the views from the layout.
     */
    public abstract static class ViewHolder extends BaseCardPresenter.ViewHolder {

        public abstract TextView getTitleView();

        public abstract ImageView getCoverView();

        public abstract ColorDrawable getPlaceHolderDrawable();

        public ViewHolder(View view) {
            super(view);
        }
    }

}
