/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.menu.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.HorizontalGridView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.menu.MenuItem;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.MainMenuItemBinding;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * The {@link MenuAdapter} is subclass of {@link HorizontalGridView.Adapter} to show a list of menu items.
 * <p>
 * Created by Szilard on 4/12/2017.
 */

public class MenuAdapter extends HorizontalGridView.Adapter<MenuAdapter.MenuItemHolder> {

    private final Translator mTranslator = Components.get(Translator.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    private List<MenuItem> mMenuItemList;
    private MenuItemClickListener mMenuItemClickListener;

    private int mSelectedPos = -1;

    public MenuAdapter(@NonNull List<MenuItem> menuItemList, @Nullable MenuItemClickListener menuItemClickListener) {
        mMenuItemList = menuItemList;
        mMenuItemClickListener = menuItemClickListener;
    }

    public void setMenuItemClickListener(MenuItemClickListener menuItemClickListener) {
        mMenuItemClickListener = menuItemClickListener;
    }

    public void setSelectedPos(int mSelectedPos) {
        this.mSelectedPos = mSelectedPos;
    }

    @NonNull
    @Override
    public MenuAdapter.MenuItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        MainMenuItemBinding binding = MainMenuItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        View view = binding.getRoot();
        view.setOnClickListener(mOnItemClickListener);
        return new MenuItemHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuAdapter.MenuItemHolder holder, int position) {
        MenuItem menuItem = mMenuItemList.get(position);
        holder.itemView.setTag(R.id.menu_item_position, position);
        Context context = holder.itemView.getContext();
        holder.mBinding.title.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.menu_text_size));
        holder.mBinding.title.setText(mTranslator.get(menuItem.getTitle()));
        holder.mBinding.title.setContentDescription(
                new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_MAIN_NAVIGATION))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_NAME, mTranslator.get(menuItem.getTitle()))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_POSITION, String.valueOf(position + 1))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TOTAL, String.valueOf(getItemCount()))
                        .toString());

        holder.itemView.setActivated(position == mSelectedPos);
    }

    @Override
    public int getItemCount() {
        return mMenuItemList.size();
    }

    private final View.OnClickListener mOnItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int position = (int) view.getTag(R.id.menu_item_position);
            if (mMenuItemClickListener != null) {
                mMenuItemClickListener.onMenuItemClick(mMenuItemList.get(position));
            }
        }
    };

    @SuppressLint("NotifyDataSetChanged")
    public void updateMenuItems(List<MenuItem> menuItemList) {
        mMenuItemList = menuItemList;
        notifyDataSetChanged();
    }

    class MenuItemHolder extends RecyclerView.ViewHolder {

        public final MainMenuItemBinding mBinding;

        MenuItemHolder(MainMenuItemBinding binding) {
            super(binding.getRoot());
            binding.title.setTextColor(UiUtils.createFontBrandingColorStateList(mLayoutConfig));
            mBinding = binding;
        }
    }

    public interface MenuItemClickListener {
        void onMenuItemClick(MenuItem menuItem);
    }

}
