/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.tv.callback;

import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.player.contract.SeekDirection;

/**
 * Contains the Progressbar states
 *
 * @author Szende Pal
 * @since 2017.05.30
 */
public interface SeekBarListener {

    /**
     * Called when the primary position changes in the seek bar.
     *
     * @param position The new primary position.
     * @param start    The start of the seek bar.
     * @param end      The end of the seek bar
     */
    void onPrimaryProgressChanged(long position, long start, long end);

    /**
     * Called when the secondary buffer position changes in the seek bar.
     *
     * @param position The new secondary position changes in the seek bar.
     * @param start    The start of the seek bar.
     * @param end      The end of the seek bar,
     */
    void onSecondaryProgressChanged(long position, long start, long end);

    /**
     * Called when the user starts seeking on the seek bar.
     *
     * @param direction The direction of the seeking,
     */
    void onStartSeeking(SeekDirection direction);

    /**
     * Called when the user finished the seeking on the seek bar.
     *
     * @param startAction     The action to be reported when jump is executed.
     * @param seekPosition    The position where the seeking was finished.
     */
    void onStopSeeking(StartAction startAction, long seekPosition);

    /**
     * Called when the secondary buffer position is reached during the seeking or
     * when the start or the end of the seek bar was reached during the seeking.
     *
     * @param direction The direction of the seeking.
     * @param position  The new primary position which is outside of the boundaries.
     * @param bufferOverflow True in case the position is outside of the buffer
     * @param progressOverflow True in case the position is outside the seekbar's bounds
     */
    void onSeekingOverflow(SeekDirection direction, long position, boolean progressOverflow, boolean bufferOverflow);
}
