package tv.threess.threeready.ui.utils.glide;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader;

import java.io.InputStream;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.ImageRepository;
import tv.threess.threeready.api.generic.model.IBaseContentItem;

/**
 * Custom glide model loader for content items(broadcasts, vods).
 *
 * Created by Noemi Dali on 06.04.2020.
 */
public class ContentItemGlideModelLoader extends BaseGlideUrlLoader<IBaseContentItem> {

    private final ImageRepository mImageRepository = Components.get(ImageRepository.class);

    public ContentItemGlideModelLoader(ModelLoader<GlideUrl, InputStream> concreteLoader) {
        super(concreteLoader);
    }

    @Override
    protected String getUrl(IBaseContentItem iBaseContentItem, int width, int height, Options options) {
        return mImageRepository.getCoverUrl(iBaseContentItem, width, height);
    }

    @Override
    public boolean handles(@NonNull IBaseContentItem iBaseContentItem) {
        return true;
    }
}
