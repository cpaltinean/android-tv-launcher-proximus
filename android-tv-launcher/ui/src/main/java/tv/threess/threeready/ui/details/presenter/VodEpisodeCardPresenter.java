package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.home.model.generic.ContentMarkers;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.ui.databinding.VodSeriesEpisodeCardBinding;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.view.BaseOrderedIconsContainer;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.tv.view.ContentMarkersView;

/**
 * Vod series episode card presenter on the series details page.
 *
 * @author Zsolt Bokor
 * @since 2019.01.08
 */
public class VodEpisodeCardPresenter extends BaseEpisodeCardPresenter<VodEpisodeCardPresenter.ViewHolder, IBaseVodItem> {

    private final VodRepository mVodRepository = Components.get(VodRepository.class);
    private final SeriesProvider<IVodSeries> mSeriesProvider;

    public VodEpisodeCardPresenter(Context context, SeriesProvider<IVodSeries> seriesProvider) {
        super(context);
        mSeriesProvider = seriesProvider;
    }

    @Override
    public BaseEpisodeCardPresenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        VodSeriesEpisodeCardBinding binding = VodSeriesEpisodeCardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        VodEpisodeCardPresenter.ViewHolder holder = new VodEpisodeCardPresenter.ViewHolder(binding);
        holder.mBinding.title.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBinding.details.setTextColor(mLayoutConfig.getPlaceholderTransparentFontColor());
        holder.mPlaceHolderDrawable.setColor(mLayoutConfig.getPlaceHolderColor());
        holder.mBinding.seriesInfo.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        holder.mBinding.description.setTextColor(mLayoutConfig.getPlaceholderFontColor());
        return holder;
    }

    @Override
    public void onBindHolder(VodEpisodeCardPresenter.ViewHolder holder, IBaseVodItem episode) {
        super.onBindHolder(holder, episode);

        // Update details.
        updateTitle(holder, episode);
        holder.mBinding.details.setText(LocaleTimeUtils.getDuration(episode.getDuration(), mTranslator));

        // Update content markers.
        updateMarkers(holder.mBinding.contentMarker, episode, false);
        updateParentalControlIcon(holder, episode);
        updateDescriptiveAudioIcon(holder, episode);
        updateDescriptiveSubtitleIcon(holder, episode);
        updateLanguageIcon(holder, episode);
        updateVodVideoQualityIcon(holder, episode);
        updateDolbyLogo(holder, episode);
        invalidateProgressBar(holder, episode);
    }

    private void invalidateProgressBar(ViewHolder holder, IBaseVodItem vod) {
        RxUtils.disposeSilently(holder.mVodBookmarkDisposable);

        holder.mVodBookmark = null;
        holder.mBinding.progressBar.setVisibility(View.GONE);

        holder.mVodBookmarkDisposable = mVodRepository.getBookmarkForVod(vod)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bookmark -> {
                            holder.mVodBookmark = bookmark;
                            invalidateProgressIndicatorVisibility(holder);
                        }
                );
    }

    private void invalidateProgressIndicatorVisibility(ViewHolder holder) {
        if (holder.mVodBookmark != null && holder.mVodBookmark.getPosition() > 0) {
            holder.mBinding.progressBar.setVisibility(View.VISIBLE);
            holder.mBinding.progressBar.setProgressData(holder.mVodBookmark);
        } else {
            holder.mBinding.progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFocusedState(ViewHolder holder, IBaseVodItem contentItem) {
        super.onFocusedState(holder, contentItem);
        invalidateProgressIndicatorVisibility(holder);
        holder.mBinding.noFocusOverlay.setVisibility(View.GONE);
    }

    @Override
    public void onDefaultState(ViewHolder holder, IBaseVodItem vod) {
        super.onDefaultState(holder, vod);
        invalidateProgressIndicatorVisibility(holder);
        holder.mBinding.noFocusOverlay.setVisibility(View.VISIBLE);
    }

    /**
     * Invalidate the displayed content markers.
     */
    @Override
    public void updateMarkers(ContentMarkersView textView, IBaseVodItem episode, boolean isFocused) {
        textView.showMarkers(episode, ContentMarkers.TypeFilter.Cards);
    }

    private void updateDolbyLogo(ViewHolder holder, IBaseVodItem episode) {
        if (isDolbyAvailable(episode.getMainVariant())) {
            holder.mBinding.iconsContainer.showDolby();
        }
    }

    private boolean isDolbyAvailable(IVodVariant iVodVariant) {
        return mPlaybackDetailsManager.isVodPlaying(iVodVariant) &&
                mPlaybackDetailsManager.isDolbyAvailable();
    }

    private void updateLanguageIcon(ViewHolder holder, IBaseVodItem episode) {
        holder.mBinding.iconsContainer.showAudioProfiles(episode.getMainVariant().getAudioLanguages());
    }

    protected void updateVodVideoQualityIcon(ViewHolder holder, IBaseVodItem episode) {
        if (episode.getMainVariant().isHD()) {
            holder.mBinding.iconsContainer.showHDIcon();
        }
    }

    @Override
    public void onClicked(VodEpisodeCardPresenter.ViewHolder holder, IBaseVodItem episode) {
        super.onClicked(holder, episode);

        mNavigator.showVodEpisodeDetailsDialog(mSeriesProvider.getSeries(), episode);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);

        holder.mBinding.iconsContainer.hideHDIcon();
        holder.mBinding.iconsContainer.hideDolby();
        RxUtils.disposeSilently(holder.mVodBookmarkDisposable);
    }

    @Override
    public long getStableId(IBaseVodItem vod) {
        // Check the content and availability status for stable id.
        return Objects.hash(vod.getId(), vod.isSubscribed(), vod.isRented());
    }

    protected static class ViewHolder extends BaseEpisodeCardPresenter.ViewHolder {

        private final VodSeriesEpisodeCardBinding mBinding;
        IBookmark mVodBookmark;
        Disposable mVodBookmarkDisposable;
        
        public ViewHolder(VodSeriesEpisodeCardBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        @NonNull
        @Override
        protected BaseOrderedIconsContainer getIconsContainer() {
            return mBinding.iconsContainer;
        }

        @NonNull
        @Override
        protected ImageView getCoverView() {
            return mBinding.cover;
        }

        @NonNull
        @Override
        protected FontTextView getTitleView() {
            return mBinding.title;
        }

        @NonNull
        @Override
        protected ContentMarkersView getContentMarkersView() {
            return mBinding.contentMarker;
        }

        @NonNull
        @Override
        protected FontTextView getSeriesInfoView() {
            return mBinding.seriesInfo;
        }

        @NonNull
        @Override
        protected FontTextView getDescriptionView() {
            return mBinding.description;
        }
    }
}
