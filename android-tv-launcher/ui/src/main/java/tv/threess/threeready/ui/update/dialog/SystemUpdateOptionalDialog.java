package tv.threess.threeready.ui.update.dialog;

import android.os.Bundle;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.api.middleware.UpdateUrgency;
import tv.threess.threeready.ui.generic.dialog.ActionBarDialog;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Dialog displayed in case there is a system update available with optional urgency {@link UpdateUrgency}
 *
 * @author Diana Toma
 * @since 20.08.2020
 */
public class SystemUpdateOptionalDialog extends ActionBarDialog {
    private static final String TAG = Log.tag(SystemUpdateOptionalDialog.class);

    public static final int SYSTEM_UPDATE_DIALOG_PRIORITY = 1000;

    private final MwRepository mMwRepository = Components.get(MwRepository.class);

    public static SystemUpdateOptionalDialog newInstance() {
        final SystemUpdateOptionalDialog dialog = new SystemUpdateOptionalDialog();

        final DialogModel dialogModel = new Builder()
                .title(dialog.mTranslator.get(TranslationKey.ALERT_OTA_UPGRADE_TITLE))
                .description(dialog.mTranslator.get(TranslationKey.ALERT_OTA_UPGRADE_BODY))
                .priority(SYSTEM_UPDATE_DIALOG_PRIORITY)
                .addButton(dialog.mTranslator.get(TranslationKey.ALERT_OTA_UPGRADE_BUTTON_UPGRADE), d -> dialog.applySystemUpdate())
                .addButton(dialog.mTranslator.get(TranslationKey.ALERT_OTA_UPGRADE_BUTTON_SKIP), d -> dialog.dismiss())
                .cancelable(true)
                .blockKeys(false)
                .buildModel();

        final Bundle args = new Bundle();
        args.putSerializable(EXTRA_DIALOG_MODEL, dialogModel);
        dialog.setArguments(args);

        return dialog;
    }

    private void applySystemUpdate() {
        mDisposable.add(mMwRepository.applySystemUpdate()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> Log.d(TAG, "System update successfully applied."),
                        throwable -> Log.e(TAG, "Failed to apply system update.", throwable)));
    }

}