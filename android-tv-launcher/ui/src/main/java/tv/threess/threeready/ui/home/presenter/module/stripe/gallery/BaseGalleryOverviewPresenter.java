package tv.threess.threeready.ui.home.presenter.module.stripe.gallery;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Size;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.leanback.widget.HorizontalGridView;
import androidx.leanback.widget.Presenter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.DecodeFormat;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleEditorialStyle;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.activity.BaseActivity;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.DetailPageButtonOrder;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.ActionButtonsPresenter;
import tv.threess.threeready.ui.generic.presenter.ActionModel;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.utils.glide.FallbackTarget;

/**
 * Base Overview presenter for the Gallery view, to bind the data to to the item.
 * This Presenter works for {@link IContentItem} types.
 *
 * @author Andor Lukacs
 * @since 12/5/18
 */
public abstract class BaseGalleryOverviewPresenter<THolder extends BaseGalleryOverviewPresenter.ViewHolder, TItem extends IContentItem> extends BasePresenter<THolder, TItem> {

    protected final ModuleData mModuleData;
    protected final ModuleConfig mModuleConfig;

    protected final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);
    protected final Translator mTranslator = Components.get(Translator.class);
    protected final Navigator mNavigator = Components.get(Navigator.class);
    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    public BaseGalleryOverviewPresenter(Context context, ModuleData moduleData) {
        super(context);
        mModuleData = moduleData;
        mModuleConfig = mModuleData.getModuleConfig();
    }

    @Override
    public final Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        THolder holder = createViewHolder(parent);

        setupColors(holder, mModuleConfig);

        holder.mGlideTarget = new FallbackTarget(new Size(mContext.getResources().getDimensionPixelSize(R.dimen.details_cover_image_max_width),
                mContext.getResources().getDimensionPixelOffset(R.dimen.details_cover_image_max_height)),
                ContextCompat.getDrawable(parent.getContext(), R.drawable.default_detail_image_background)) {
            @Override
            protected void onImageLoaded(Drawable drawable) {
                holder.getCoverImage().setImageDrawable(drawable);
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {
                holder.getCoverImage().setImageDrawable(null);
            }
        };

        return holder;
    }

    public abstract THolder createViewHolder(ViewGroup parent);

    @Override
    public void onBindHolder(THolder holder, TItem item) {
        updateTitle(holder, item);
        updateImage(holder, item, mModuleConfig);

        // Register parental control listener.
        holder.mParentalControlListener = new ParentalControlListener(holder, item);
        mParentalControlManager.addListener(holder.mParentalControlListener);
    }

    @Override
    public void onUnbindHolder(THolder holder) {
        super.onUnbindHolder(holder);

        Glide.with(mContext).clear(holder.mGlideTarget);

        mParentalControlManager.removeListener(holder.mParentalControlListener);
    }

    protected void setupColors(THolder holder, ModuleConfig moduleConfig) {
        ModuleEditorialStyle moduleStyle = moduleConfig.getModuleStyle();
        int titleColor = moduleStyle != null && moduleStyle.hasTitleColor()
                ? moduleStyle.getTitleColor() : mLayoutConfig.getFontColor();
        holder.getTitleView().setTextColor(titleColor);
        holder.getInfoRow().setTextColor(
                ColorUtils.applyTransparencyToColor(titleColor, 0.7f));
    }

    private void updateTitle(THolder holder, TItem contentItem) {
        holder.getTitleView().setText(mParentalControlManager.isRestricted(contentItem)
                ? mTranslator.get(TranslationKey.CARDS_TITLE_CONTENT_LOCKED) : contentItem.getTitle());
    }

    private void updateImage(THolder holder, TItem contentItem, ModuleConfig moduleConfig) {
        Glide.with(mContext).clear(holder.mGlideTarget);

        String url = null;
        if (moduleConfig.getModuleStyle() != null && moduleConfig.getModuleStyle().hasBackgroundImage()) {
            url = moduleConfig.getModuleStyle().getBackgroundImage();
        }

        holder.getCoverImage().setVisibility(View.VISIBLE);
        RequestBuilder<Drawable> requestBuilder;

        // Load the restricted background
        if (mParentalControlManager.isRestricted(contentItem)) {
            requestBuilder = Glide.with(mContext).load(R.drawable.locked_background);

            // Load the background of the content.
        } else if (TextUtils.isEmpty(url)) {
            requestBuilder = Glide.with(mContext).load(contentItem);

            // Load the background from the config.
        } else {
            requestBuilder = Glide.with(mContext).load(url);
        }

        requestBuilder
                .fallback(R.drawable.content_fallback_image_landscape)
                .error(R.drawable.content_fallback_image_landscape)
                .format(DecodeFormat.PREFER_RGB_565)
                .override(
                        mContext.getResources().getDimensionPixelSize(R.dimen.gallery_a1_image_width),
                        mContext.getResources().getDimensionPixelSize(R.dimen.gallery_a1_image_height))
                .optionalCenterInside()
                .into(holder.mGlideTarget);
    }

    protected List<ActionModel> getFirstAndMoreActions(List<ActionModel> actions) {
        //Gets the first action shown on Details Page and More Info action.
        List<ActionModel> actionModelList = new ArrayList<>();
        for (int i = 0; i < actions.size(); i++) {
            if (i == 0 || actions.get(i).getDetailPageButtonOrder().getValue() == DetailPageButtonOrder.MoreInfo.getValue()) {
                actionModelList.add(actions.get(i));
            }
        }
        return actionModelList;
    }

    /**
     * Update the action buttons on the detail page based on the priority. (max 2.)
     *
     * @param holder  The holder which holds the view references.
     * @param item    The content item for which the buttons should be shown.
     * @param actions All the possible actions. Will be filtered based on priority.
     */
    protected void updateActionButtons(THolder holder, TItem item, List<ActionModel> actions) {
        DetailPageButtonOrder.sort(actions);

        InterfacePresenterSelector presenterSelector = new InterfacePresenterSelector()
                .addClassPresenter(ActionModel.class, new ActionButtonsPresenter(mContext,
                        model -> {
                            Log.event(new Event<>(UILogEvent.PageSelection)
                                    .addDetail(UILogEvent.Detail.OpenedBy, UILog.OpenedBy.Ok)
                                    .addDetail(UILogEvent.Detail.FocusName, mModuleConfig.getReportName())
                                    .addDetail(UILogEvent.Detail.FocusId, mModuleConfig.getReportReference()));

                            if (mParentalControlManager.isRestricted(item)) {
                                mNavigator.showParentalControlUnblockDialog();
                                return true;
                            }
                            return false;
                        }));
        ArrayObjectAdapter<ActionModel> adapter = new ArrayObjectAdapter<>(getFirstAndMoreActions(actions));
        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(adapter, presenterSelector);
        holder.getActionsGridView().setAdapter(itemBridgeAdapter);
    }

    protected void onParentalRatingChanged(THolder holder, TItem item) {
        updateTitle(holder, item);
        updateImage(holder, item, mModuleConfig);
    }

    private class ParentalControlListener implements ParentalControlManager.IParentalControlListener {
        TItem mItem;
        THolder mHolder;

        ParentalControlListener(THolder holder, TItem item) {
            mItem = item;
            mHolder = holder;
        }

        @Override
        public void onParentalRatingChanged(ParentalRating parentalRating) {
            if (mHolder.view.getContext() instanceof BaseActivity) {
                BaseActivity activity = (BaseActivity) mHolder.view.getContext();
                if (activity == null || activity.isInstanceStateSaved()) {
                    // The activity is already destroyed. This listener will be removed soon. (WeakReference)
                    return;
                }
            }

            BaseGalleryOverviewPresenter.this.onParentalRatingChanged(mHolder, mItem);
        }
    }

    static abstract class ViewHolder extends Presenter.ViewHolder {

        @NotNull
        protected abstract FontTextView getTitleView();

        @NotNull
        protected abstract FontTextView getInfoRow();

        @NotNull
        protected abstract ImageView getCoverImage();

        @NotNull
        protected abstract HorizontalGridView getActionsGridView();

        FallbackTarget mGlideTarget;

        ParentalControlManager.IParentalControlListener mParentalControlListener;

        public ViewHolder(View view) {
            super(view);
        }
    }
}
