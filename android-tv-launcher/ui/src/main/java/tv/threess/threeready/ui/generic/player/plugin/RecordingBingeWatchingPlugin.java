package tv.threess.threeready.ui.generic.player.plugin;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pvr.PvrRepository;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.player.contract.PlaybackType;

/**
 * Playback Plugin used to request the next episode when it's needed,
 * store it and update the playback details when the current episode ends.
 *
 * @author David Bondor
 * @since 19.07.2022
 */
public class RecordingBingeWatchingPlugin extends BaseBingeWatchingPlugin {

    private static final String TAG = Log.tag(RecordingBingeWatchingPlugin.class);

    private final PvrRepository mPvrRepository = Components.get(PvrRepository.class);

    @Override
    protected PlaybackType getPlaybackType() {
        return PlaybackType.Recording;
    }

    @Override
    protected Disposable preloadNextEpisode() {
        IRecording recording = mPlaybackManager.getRecordingPlayerData();
        IRecordingSeries series = mPlaybackManager.getRecordingSeriesPlayerData();
        if (recording == null) {
            Log.d(TAG, "There is no started recording!");
            return null;
        }

        return mPvrRepository.getNextEpisode(recording, series)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bingeWatchingInfo -> {
                            IRecording nextEpisode = bingeWatchingInfo.getNextEpisode();
                            if (nextEpisode != null) {
                                mPlaybackManager.setNextEpisode(bingeWatchingInfo);
                                Log.d(TAG, "Next Recording episode is loaded: " + nextEpisode.getTitle() +
                                        " | Season: " + nextEpisode.getSeasonNumber() + " | Episode: " +
                                        nextEpisode.getEpisodeNumber());
                            }
                        },
                        throwable -> Log.e(TAG, "Could not load the next Recording episode.", throwable)
                );
    }
}
