package tv.threess.threeready.ui.generic.interfaces;

/**
 * Interface for page reporting.
 *
 * Created by Bartalus Csaba-Zsolt since 26.01.2022
 */
public interface IReportablePage {

    /**
     * Reports the opened page
     */
    void reportNavigation();

    /**
     * @return true if the fragment is currently removing from the backstack, else false.
     */
    boolean isClearingFromBackstack();
}
