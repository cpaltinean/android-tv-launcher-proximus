package tv.threess.threeready.ui.startup.step;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Startup step to update endpoints and backend config options.
 *
 * @author Barabas Attila
 * @since 2020.03.20
 */
public class UpdateBootConfigStep implements Step {
    private static final String TAG = Log.tag(UpdateBootConfigStep.class);

    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    private Disposable mDisposable;

    @Override
    public void execute(StepCallback callback) {
        mDisposable = mAccountRepository.updateBootConfig()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(mAppConfig.getQueryTimeout(), TimeUnit.SECONDS)
                .subscribe(callback::onFinished, (e) -> {
                    Log.e(TAG, "Could not update the boot config.", e);
                    callback.onFinished();
                });
    }

    @Override
    public void cancel() {
        RxUtils.disposeSilently(mDisposable);
    }
}
