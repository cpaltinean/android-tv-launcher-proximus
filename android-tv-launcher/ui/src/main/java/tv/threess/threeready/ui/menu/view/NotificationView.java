package tv.threess.threeready.ui.menu.view;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.gms.GmsRepository;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.notification.model.NotificationItem;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.NotificationIconViewBinding;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * View for notification icon in menu with notification indicator.
 *
 * @author Eugen Guzyk
 * @since 2018.08.21
 */
public class NotificationView extends RelativeLayout {
    private static final String TAG = Log.tag(NotificationView.class);

    private List<NotificationItem> mStoredNotificationsList = Collections.emptyList();

    private Disposable mGetUpdatedNotificationsDisposable;
    private Disposable mSaveNotificationsDisposable;

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final GmsRepository mGmsRepository = Components.get(GmsRepository.class);

    private final NotificationIconViewBinding mBinding;

    public NotificationView(Context context) {
        this(context, null);
    }

    public NotificationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NotificationView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public NotificationView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mBinding = NotificationIconViewBinding.inflate(LayoutInflater.from(getContext()), this);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
            setAllRead();
            saveNotifications();
            updateExistenceState(false);
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
            updateExistenceState(true);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onFocusChanged(boolean gainFocus, int direction, @Nullable Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
        updateExistenceState(false);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        updateStoredNotifications();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        RxUtils.disposeSilently(mGetUpdatedNotificationsDisposable,
                mSaveNotificationsDisposable);
    }

    private void updateStoredNotifications() {
        RxUtils.disposeSilently(mGetUpdatedNotificationsDisposable);
        mGetUpdatedNotificationsDisposable = mGmsRepository.getUpdatedNotifications()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(updatedNotifications -> {
                    mStoredNotificationsList = updatedNotifications;
                    saveNotifications();
                    updateExistenceState(isPressed());
                });
    }

    private void saveNotifications() {
        RxUtils.disposeSilently(mSaveNotificationsDisposable);
        mSaveNotificationsDisposable = mGmsRepository.saveNotifications(mStoredNotificationsList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        () -> Log.d(TAG, "Save notification succeed."),
                        throwable -> Log.e(TAG, "Failed to save the notifications.", throwable)
                );
    }

    private void setAllRead() {
        mStoredNotificationsList.forEach(NotificationItem::setRead);
    }

    private boolean isAnyUnread() {
        for (NotificationItem notification : mStoredNotificationsList) {
            if (!notification.isRead()) {
                return true;
            }
        }
        return false;
    }

    private void updateExistenceState(boolean isPressed) {
        if (!isPressed) {
            if (mStoredNotificationsList.isEmpty()) {
                mBinding.notificationExistence.setVisibility(INVISIBLE);
                mBinding.icon.setImageResource(R.drawable.ico_notification);
            } else {
                mBinding.notificationExistence.setVisibility(VISIBLE);
                mBinding.icon.setImageResource(R.drawable.ico_notification_badge);
            }
        }
        GradientDrawable drawable = (GradientDrawable) mBinding.notificationExistence.getBackground();
        if (isFocused()) {
            drawable.setColor(mLayoutConfig.getFontColor());
        } else if (isAnyUnread()) {
            drawable.setColor(mLayoutConfig.getErrorColor());
        } else {
            drawable.setColor(mLayoutConfig.getFontColor());
        }
        mBinding.notificationExistence.setBackground(drawable);
    }
}
