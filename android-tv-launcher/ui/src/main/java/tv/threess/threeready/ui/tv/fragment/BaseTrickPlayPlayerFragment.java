package tv.threess.threeready.ui.tv.fragment;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.command.start.TrickPlayStartCommand;
import tv.threess.threeready.player.contract.PlaybackEvent;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.contract.SeekDirection;
import tv.threess.threeready.player.control.ControlFactory;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.ui.generic.fragment.ContentPlayerFragment;
import tv.threess.threeready.ui.generic.utils.AnimatorUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.tv.view.SeekBarView;

import static tv.threess.threeready.ui.utils.UiUtils.runOnUiThread;

/**
 * Base fragment for the content that supports trickplay.
 *
 * @author Andor Lukacs
 * @since 10/24/18
 */
public abstract class BaseTrickPlayPlayerFragment<TContentItem extends IContentItem> extends ContentPlayerFragment<TContentItem> {
    private final String TAG = Log.tag(BaseTrickPlayPlayerFragment.class);

    @Nullable
    View mTrickPlayBackground;

    @Nullable
    SurfaceView mTrickPlayView;

    private final ControlFactory mControlFactory = Components.get(ControlFactory.class);

    private volatile boolean mIsUserInteracting;
    private ObjectAnimator mTrickPlayAnimator;

    private Handler mHandler = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mHandler = new Handler(Looper.getMainLooper());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mTrickPlayView = mControlFactory.getTrickPlaySurface();
        mTrickPlayBackground = mControlFactory.getTrickplayBackground();

        if (mTrickPlayView != null) {
            mTrickPlayView.setVisibility(View.VISIBLE);
            mTrickPlayView.setZOrderOnTop(true);
            mTrickPlayView.setAlpha(0F);
        }

        if (mTrickPlayBackground != null) {
            mTrickPlayBackground.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        AnimatorUtils.cancelAnimators(mTrickPlayAnimator);
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
    }

    @Override
    public boolean onKeyDown(KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_MEDIA_REWIND:
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                if (getViewHolder().getSeekbar().allowTrickPlay(event)) {
                    if (doTrickPlay(getSeekDirection(event))) {
                        mIsUserInteracting = true;
                        return true;
                    }
                }
        }
        return super.onKeyDown(event);
    }

    @Override
    public boolean onKeyUp(KeyEvent event) {
        if (mNavigator.isStreamDisplayedOnTop()) {
            boolean forward = true;
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_MEDIA_REWIND:
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    forward = false;
                    // fall-trough
                case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    PlaybackDetails details = mPlaybackDetailsManager.getExclusiveDetails();
                    if (details.isUnskippablePosition()) {
                        String message = forward
                                ? mTranslator.get(TranslationKey.NOTIFICATION_UNSKIPPABLE_ADS_FORWARD)
                                : mTranslator.get(TranslationKey.NOTIFICATION_UNSKIPPABLE_ADS_REWIND);
                        showUnskippableUI(details, message);
                        return true;
                    }
                    break;
            }
        }

        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                if (finishTrickPlay()) {
                    mIsUserInteracting = false;
                    return true;
                }
                break;
        }
        return super.onKeyUp(event);
    }

    @Override
    protected void setPlayerDetailItemsVisibility(int visible) {
        getViewHolder().getDetailContainer().setVisibility(isTrickPlayShown() ? View.GONE : visible);
        getViewHolder().getLogoView().setVisibility(isTrickPlayShown() ? View.GONE : visible);
    }

    @Override
    public void onCommandSuccess(PlaybackCommand cmd, PlaybackDetails details) {
        super.onCommandSuccess(cmd, details);
        Log.d(TAG, "onCommandSuccess() called with: cmd = ["
                + cmd + "], details = [" + details + "]");

        if (mTrickPlayBackground == null || mTrickPlayView == null) {
            Log.w(TAG, "Trick play view is not initialized yet. ");
            return;
        }

        if (cmd instanceof TrickPlayStartCommand) {
            TrickPlayStartCommand command = (TrickPlayStartCommand) cmd;
            if (command.getPlaybackSpeed() > 0) {
                UILog.logButtonPressFromRemote(UILog.ButtonType.FASTFORWARD);
            } else if (command.getPlaybackSpeed() < 0) {
                UILog.logButtonPressFromRemote(UILog.ButtonType.REWIND);
            }
        }

        runOnUiThread(() -> {
            if (cmd.getDomain() == PlaybackDomain.TrickPlay && cmd instanceof StartCommand) {
                Log.d(TAG, "Trick play domain, command executed successfully: " + cmd);

                AnimatorUtils.cancelAnimators(mTrickPlayAnimator);

                mTrickPlayView.clearAnimation();
                mTrickPlayAnimator = ObjectAnimator.ofFloat(mTrickPlayView, "alpha", 1F);
                mTrickPlayAnimator.setDuration(200);
                mTrickPlayAnimator.start();
            }
        });
    }

    @Override
    public void onPlaybackEvent(PlaybackEvent event, PlaybackDetails details, PlaybackError error) {
        super.onPlaybackEvent(event, details, error);
        switch (event) {
            case PlaybackFailed:
            case PlaybackUnavailable:
            case PlaybackDisconnected:
                postOnHandler(() -> {
                    if (finishTrickPlay()) {
                        mIsUserInteracting = false;
                    }
                    updatePlayPauseButton();
                    updateSeekBar(details);
                });
                break;
        }
    }

    public boolean isTrickPlayShown() {
        if (mTrickPlayBackground == null || mTrickPlayView == null) {
            // Trick play view is not initialized yet.
            return false;
        }
        // return true if the trick-play background is displayed
        return mTrickPlayBackground.getVisibility() == View.VISIBLE;
    }

    protected void displayTrickPlay() {
        if (mTrickPlayBackground == null || mTrickPlayView == null) {
            Log.w(TAG, "Trick play view is not initialized yet. ");
            return;
        }


        Log.d(TAG, "Display the trickplay player. State: " + mPlaybackDetailsManager.getNonExclusiveDetails().getState());
        if (mTrickPlayBackground.getVisibility() != View.VISIBLE) {
            mNavigator.showContentOverlay();

            getViewHolder().getDetailContainer().setVisibility(View.GONE);
            getViewHolder().getLogoView().setVisibility(View.GONE);
            getViewHolder().getPlaybackActionView().setVisibility(View.GONE);
            mTrickPlayView.setVisibility(View.VISIBLE);
            mTrickPlayView.setZOrderOnTop(true);
            mTrickPlayBackground.setVisibility(View.VISIBLE);
            getViewHolder().getSeekbar().requestFocus();
        }
    }

    protected void hideTrickPlay() {
        if (mTrickPlayBackground == null || mTrickPlayView == null) {
            Log.w(TAG, "Trick play view is not initialized yet. ");
            return;
        }

        if (mTrickPlayBackground.getVisibility() == View.VISIBLE) {
            getViewHolder().getDetailContainer().setVisibility(View.VISIBLE);
            getViewHolder().getLogoView().setVisibility(View.VISIBLE);
            getViewHolder().getPlaybackActionView().setVisibility(View.VISIBLE);

            mTrickPlayView.setVisibility(View.INVISIBLE);
            mTrickPlayBackground.setVisibility(View.GONE);

            AnimatorUtils.cancelAnimators(mTrickPlayAnimator);

            mTrickPlayView.clearAnimation();
            mTrickPlayAnimator = ObjectAnimator.ofFloat(mTrickPlayView, View.ALPHA, 0F);
            mTrickPlayAnimator.setDuration(200);
            mTrickPlayAnimator.start();
        }
    }

    @Override
    public void onStopSeeking(StartAction startAction, long seekPosition) {
        // Close the trick play surface.
        finishTrickPlay();
        super.onStopSeeking(startAction, seekPosition);
    }

    @Override
    public void onSeekingOverflow(SeekDirection direction, long position, boolean progressOverflow, boolean bufferOverflow) {
        super.onSeekingOverflow(direction, position, progressOverflow, bufferOverflow);
        if (bufferOverflow) {
            if (mPlaybackDetailsManager.inPlaybackType(PlaybackType.LiveTvIpTv)) {
                StartAction action = direction == SeekDirection.REWIND ? StartAction.BufferRestart : StartAction.TSTVEnded;
                mPlaybackDetailsManager.resume(action);
            }
        }
    }

    private boolean doTrickPlay(SeekDirection seekDirection) {
        if (!mIsUserInteracting && mPlaybackDetailsManager.isTrickPlayAvailable()) {
            Log.d(TAG, "doTrickPlay() called with: seekDirection = [" + seekDirection + "] mIsUserInteracting = ["
                    + mIsUserInteracting + "] TrickplayAvailable = [" + mPlaybackDetailsManager.isTrickPlayAvailable() + "]");

            SeekBarView seekBarView = getViewHolder().getSeekbar();
            mNavigator.cancelFadeOutAnimator();
            seekBarView.requestFocus();

            //TODO: Remove if when implementing IPTV PIP trickplay
            if (!mPlaybackDetailsManager.inPlaybackDomain(PlaybackDomain.LiveTvIpTv)) {
                seekBarView.blockSeeking();
                displayTrickPlay();
            }

            mPlaybackDetailsManager.startTrickPlay(seekBarView.getTrickplaySpeed(seekDirection));
            return true;
        }
        return false;
    }


    protected boolean finishTrickPlay() {
        runOnUiThread(this::hideTrickPlay);
        if (!mPlaybackDetailsManager.isTrickPlayAvailable()) {
            return false;
        }
        if (!mIsUserInteracting) {
            return false;
        }
        mPlaybackDetailsManager.stopTrickPlay();
        return true;
    }

    private void postOnHandler(Runnable task) {
        Handler handler = mHandler;
        if (handler == null) {
            return;
        }
        handler.post(() -> {
            try {
                task.run();
            } catch (Exception e) {
                Log.e(TAG, "Failed to execute task", e);
            }
        });
    }

    /**
     * @return Seek direction calculated from the key event.
     */
    private static SeekDirection getSeekDirection(KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                return SeekDirection.REWIND;
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
            default:
                return SeekDirection.FORWARD;
        }
    }
}
