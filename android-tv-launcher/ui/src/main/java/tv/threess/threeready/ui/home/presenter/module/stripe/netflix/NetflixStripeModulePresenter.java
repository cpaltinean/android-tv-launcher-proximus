package tv.threess.threeready.ui.home.presenter.module.stripe.netflix;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.netflix.INetflixGroup;
import tv.threess.threeready.api.netflix.INetflixTile;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.NetflixStripeModuleBinding;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ModularItemBridgeAdapter;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeLabel;
import tv.threess.threeready.ui.generic.adapter.itemdecoration.StripeSeparatorProvider;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.home.presenter.card.app.NetflixCardPresenter;
import tv.threess.threeready.ui.home.presenter.module.BaseStripeModulePresenter;

/**
 * Presenter class for the netflix stripe module.
 * Responsible to load the data from the given data source and display it in a horizontal gird.
 * The loaded data will be displayed with cards.
 *
 * @author David
 * @since 04.03.2022
 */
public class NetflixStripeModulePresenter extends BaseStripeModulePresenter<NetflixStripeModulePresenter.ViewHolder> {

    private final String TAG = Log.tag(NetflixStripeModulePresenter.class);
    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);

    public NetflixStripeModulePresenter(Context context, DataSourceLoader dataSourceLoader) {
        super(context, dataSourceLoader);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent, RecyclerView.RecycledViewPool pool) {
        NetflixStripeModuleBinding binding = NetflixStripeModuleBinding.inflate(LayoutInflater.from(mContext), parent, false);

        ViewHolder holder = new ViewHolder(binding);

        holder.mBinding.netflixStripeModuleGrid.setRecyclerViewPool(pool);

        holder.mBinding.netflixStripeModuleGrid.setGridPadding(getVerticalMargin(), 0, getVerticalMargin(), 0);

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) holder.mBinding.netflixStripeModuleGrid.getGridView().getLayoutParams();
        params.setMargins(0, getTopMargin(), 0, getBottomMargin());

        holder.mBinding.netflixStripeModuleGrid.setItemSpacing(getItemSpacing());
        holder.mBinding.netflixStripeModuleGrid.getGridView().setRowHeight(getRowHeight());

        return holder;
    }

    @Override
    public void onBindHolder(ViewHolder holder, ModuleData<?> data) {
        super.onBindHolder(holder, data, null);

        holder.mBinding.netflixStripeModuleGrid.getGridView().setHasFixedSize(true);
        updateAdapter(holder, data);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);

        holder.mBinding.netflixStripeModuleGrid.setAdapter(null, null);
    }

    private int getRowHeight() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.netflix_stripe_row_height);
    }

    private int getItemSpacing() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.netflix_stripe_item_spacing);
    }

    private int getTopMargin() {
        return mContext.getResources().getDimensionPixelSize(R.dimen.netflix_stripe_top_padding);
    }

    private int getBottomMargin() {
        return mContext.getResources().getDimensionPixelSize(R.dimen.netflix_stripe_bottom_padding);
    }

    /**
     * Gets the value for horizontal padding.
     */
    private int getVerticalMargin() {
        return mContext.getResources().getDimensionPixelSize(R.dimen.stripe_title_left_margin);
    }

    /**
     * Create a new or update the existing adapter for the module data.
     *
     * @param holder     The view holder of the collection.
     * @param moduleData The module data which needs to be displayed
     */
    private void updateAdapter(ViewHolder holder, ModuleData<?> moduleData) {
        List<INetflixTile> itemList = new ArrayList<>();
        List<INetflixGroup> groupItemList = new ArrayList<>();
        for (Object item : moduleData.getItems()) {
            if (item instanceof INetflixGroup) {
                INetflixGroup groupItem = (INetflixGroup) item;
                groupItemList.add(groupItem);
                itemList.addAll(groupItem.getItems());
            }
        }

        if (holder.mBinding.netflixStripeModuleGrid.getAdapter() != null) {
            ModularItemBridgeAdapter itemBridgeAdapter =
                    (ModularItemBridgeAdapter) holder.mBinding.netflixStripeModuleGrid.getAdapter();
            ModuleConfig adapterModule = itemBridgeAdapter.getModuleData().getModuleConfig();
            if (Objects.equals(adapterModule, moduleData.getModuleConfig())) {
                ArrayObjectAdapter<INetflixTile> objectAdapter = itemBridgeAdapter.getAdapter();
                objectAdapter.replaceAll(itemList);
                holder.mBinding.netflixStripeModuleGrid.updateLabelProvider(getLabelProvider(groupItemList));
                return;
            }
        }

        ArrayObjectAdapter<INetflixTile> objectAdapter = new ArrayObjectAdapter<>(itemList);
        objectAdapter.setHasStableIds(true);

        RecyclerView.RecycledViewPool recycledViewPool = holder.mBinding.netflixStripeModuleGrid.getGridView().getRecycledViewPool();
        ModularItemBridgeAdapter itemBridgeAdapter = new ModularItemBridgeAdapter(
                moduleData, objectAdapter, getCardPresenterSelector(holder, moduleData), recycledViewPool);
        holder.mBinding.netflixStripeModuleGrid.setAdapter(itemBridgeAdapter, getLabelProvider(groupItemList));
    }

    private PresenterSelector getCardPresenterSelector(ViewHolder holder, ModuleData<?> moduleData) {
        return new InterfacePresenterSelector()
                .addClassPresenter(INetflixTile.class, new NetflixCardPresenter(mContext, new NetflixCardPresenter.NetflixCardListener() {
                    @Override
                    public void reportImpression(INetflixTile item) {
                        mAccountRepository.sendImpression(item.getDeepLink());
                    }

                    @Override
                    public void failedToLoadImage(INetflixTile tile) {
                        for (Object item : moduleData.getItems()) {
                            if (item instanceof INetflixGroup) {
                                List<INetflixTile> tileList = ((INetflixGroup) item).getItems();
                                if (ArrayUtils.notNull(tileList).contains(tile)) {
                                    Log.d(TAG, "Failed to load the image for: " + tile.getTitle() + " -> remove it!");
                                    tileList.remove(tile);
                                    break;
                                }
                            }
                        }
                        updateAdapter(holder, moduleData);
                    }
                }));
    }

    private StripeSeparatorProvider<StripeLabel> getLabelProvider(List<INetflixGroup> groupItemList) {
        return () -> {
            if (ArrayUtils.isEmpty(groupItemList)) {
                return Collections.emptyList();
            }

            int startPos;
            int endPos = 0;
            String continueWatchingTitle = mTranslator.get(TranslationKey.STRIPE_NETFLIX_CONTINUE_WATCHING);

            List<StripeLabel> labels = new ArrayList<>();
            for (int i = 0; i < groupItemList.size(); ++i) {
                startPos = endPos;
                endPos = i + groupItemList.get(i).getItems().size();
                String labelTitle = groupItemList.get(i).getTitle();
                labels.add(new StripeLabel(startPos, endPos,
                        StringUtils.containsIgnoreCase(labelTitle, continueWatchingTitle) ? continueWatchingTitle : labelTitle, true));
            }

            return labels;
        };
    }

    static class ViewHolder extends Presenter.ViewHolder {

        private final NetflixStripeModuleBinding mBinding;
        public ViewGroup mParentView;

        public ViewHolder(NetflixStripeModuleBinding binding) {
            super(binding.getRoot());
            mParentView = (ViewGroup) view;
            mBinding = binding;
        }
    }
}