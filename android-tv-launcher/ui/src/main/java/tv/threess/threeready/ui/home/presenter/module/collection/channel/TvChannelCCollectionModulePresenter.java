/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.home.presenter.module.collection.channel;

import android.content.Context;
import android.view.KeyEvent;
import android.view.View;

import androidx.leanback.widget.ClassPresenterSelector;

import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.tv.model.ReplayChannelPageItem;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.home.presenter.module.collection.generic.CollectionModulePresenter;
import tv.threess.threeready.ui.home.view.ModularPageView;
import tv.threess.threeready.ui.tv.presenter.channel.ReplayChannelPageCardPresenter;
import tv.threess.threeready.ui.tv.presenter.channel.TvChannelC1CardPresenter;

/**
 * Presenter used to display channel type, C variant collections.
 * <p>
 * Created by Szilard on 6/15/2017.
 */
public class TvChannelCCollectionModulePresenter extends CollectionModulePresenter {

    public TvChannelCCollectionModulePresenter(Context context, ModularPageView.PageTitleCallback pageTitle, DataSourceLoader dataSourceLoader) {
        super(context, pageTitle, dataSourceLoader);
        mVariantCardPresenterMap.put(ModuleCardVariant.C1, new ClassPresenterSelector()
                .addClassPresenter(TvChannel.class, new TvChannelC1CardPresenter(context))
                .addClassPresenter(ReplayChannelPageItem.class, new ReplayChannelPageCardPresenter(context)));
    }

    @Override
    protected int getColumnNumber() {
        return 6;
    }

    @Override
    public void onBindHolder(ViewHolder holder, ModuleData<?> data) {
        super.onBindHolder(holder, data);
        holder.mBinding.collection.getCollectionGrid().setOnKeyInterceptListener(event -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                return onKeyDown(event, holder);
            }
            return false;
        });
    }

    @Override
    protected boolean onKeyDown(KeyEvent event, ViewHolder holder) {
        if ((event.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN ||
                event.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP)) {
            switch (holder.mBinding.collection.getCollectionGrid().getRowIndex()) {
                case 0:
                    if (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN) {
                        holder.mBinding.collection.getCollectionGrid().setWindowAlignmentOffsetPercent(
                                mContext.getResources().getInteger(R.integer.channel_c_collection_window_alignment_second_row));
                        break;
                    }
                case 1:
                    if (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN) {
                        holder.mBinding.collection.getCollectionGrid().setWindowAlignmentOffsetPercent(
                                mContext.getResources().getInteger(R.integer.channel_c_collection_window_alignment_third_row));
                    } else {
                        holder.mBinding.collection.getCollectionGrid().setWindowAlignmentOffsetPercent(
                                mContext.getResources().getInteger(R.integer.channel_c_collection_window_alignment_first_row));
                    }
                    break;
                case 2:
                    if (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN) {
                        holder.mBinding.collection.getCollectionGrid().setWindowAlignmentOffsetPercent(
                                mContext.getResources().getInteger(R.integer.channel_c_collection_window_alignment_fourth_row));
                    } else {
                        holder.mBinding.collection.getCollectionGrid().setWindowAlignmentOffsetPercent(
                                mContext.getResources().getInteger(R.integer.channel_c_collection_window_alignment_second_row));
                    }
                    break;
                case 3:
                    if (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP) {
                        holder.mBinding.collection.getCollectionGrid().setWindowAlignmentOffsetPercent(
                                mContext.getResources().getInteger(R.integer.channel_c_collection_window_alignment_third_row));
                        break;
                    }
            }
        }
        return super.onKeyDown(event, holder);
    }

    /**
     * Gets the value for horizontal padding.
     */
    @Override
    protected int getHorizontalPadding() {
        return mContext.getResources().getDimensionPixelOffset(R.dimen.channel_collection_horizontal_padding);
    }

    /**
     * Gets the value for top padding.
     */
    @Override
    protected int getTopPadding(ViewHolder holder) {
        return (holder.mBinding.collection.mBinding.moduleTitle.getVisibility() == View.VISIBLE
            ? mContext.getResources().getDimensionPixelOffset(R.dimen.channel_c_collection_grid_view_top_padding)
            : mContext.getResources().getDimensionPixelOffset(R.dimen.collection_vertical_padding));
    }

    /**
     * Gets the value for bottom padding.
     */
    @Override
    protected int getBottomPadding() {
        return  mContext.getResources().getDimensionPixelOffset(R.dimen.collection_vertical_padding);
    }
}
