package tv.threess.threeready.ui.generic.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.helper.StringUtils;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.generic.model.VideoQuality;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.model.CurrencyType;
import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.api.vod.wrapper.LanguageRentOffer;
import tv.threess.threeready.api.vod.wrapper.VariantRentOffer;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.RentConfirmationDialogBinding;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.DetailPageButtonOrder;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.ActionButtonsPresenter;
import tv.threess.threeready.ui.generic.presenter.ActionModel;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Confirm rent dialog class
 *
 * @author Eugen Guzyk
 * @since 2018.11.07
 */
public class RentConfirmationDialog extends BaseDialogFragment {
    private static final String TAG = Log.tag(RentConfirmationDialog.class);

    private static final String EXTRA_VOD = "EXTRA_VOD";

    private static final String TRANSLATION_PRICE = "%price%";
    private static final String TRANSLATION_QUALITY = "%quality%";
    private static final String TRANSLATION_HOURS = "%rental_time%";
    private static final String TRANSLATION_RENTAL_DURATION = "%RENTAL_DURATION%";
    private static final String TRANSLATION_CREDIT = "%credit%";
    private static final String TRANSLATION_REMAINING_CREDITS = "%remaining_credits%";
    private static final String TRANSLATION_LANGUAGE = "%language%";

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final Translator mTranslator = Components.get(Translator.class);
    private final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);
    private final VodRepository mVodRepository = Components.get(VodRepository.class);
    private final Navigator mNavigator = Components.get(Navigator.class);  // Do not move this is parent class (AlertDialog), can cause crashes during FTI

    List<Disposable> mDisposables = new ArrayList<>();
    private IVodItem mVod;
    private String mLanguageChosen;

    private ArrayObjectAdapter<ActionModel> mAdapter;
    private ItemBridgeAdapter itemBridgeAdapter;
    
    private RentConfirmationDialogBinding mBinding;

    /**
     * Model of available variant-offer pairs. Mapped to the available languages.
     */
    private LanguageRentOffer mLanguageRentOfferModel;

    public static RentConfirmationDialog newInstance(IVodItem vod) {
        RentConfirmationDialog dialog = new RentConfirmationDialog();

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_VOD, vod);
        dialog.setArguments(args);

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVod = getSerializableArgument(EXTRA_VOD);
        InterfacePresenterSelector presenterSelector = new InterfacePresenterSelector()
                .addClassPresenter(ActionModel.class, new ActionButtonsPresenter(getContext()));
        mAdapter = new ArrayObjectAdapter<>();
        itemBridgeAdapter = new ItemBridgeAdapter(mAdapter, presenterSelector);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = RentConfirmationDialogBinding.inflate(inflater, container, false);

        mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        mBinding.audioInfo.setTextColor(mLayoutConfig.getFontColor());
        mBinding.subtitlesInfo.setTextColor(mLayoutConfig.getFontColor());
        mBinding.infoRow.setTextColor(mLayoutConfig.getFontColor());
        mBinding.rentInfo.setTextColor(mLayoutConfig.getFontColor());
        mBinding.qualityTv.setTextColor(mLayoutConfig.getFontColor());

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBinding.actionGrid.setAdapter(itemBridgeAdapter);
        if (mVod != null) {
            processRentOffers(mVod);
        }
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        RxUtils.disposeSilently(mDisposables);

    }

    private void loadLogo() {
        //No logo will be shown until the provider logos will be available.
        mBinding.titleSeparator.setVisibility(View.VISIBLE);
    }

    private void processRentOffers(IVodItem movie) {
        mDisposables.add(mVodRepository.getLanguageRentOffers(movie)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            mLanguageRentOfferModel = result;
                            initializeLanguage();
                            loadLogo();
                            updateButtons(getActions());
                            updatePriceInfo();
                        },
                        throwable -> Log.d(TAG, "Error getting credits. ", throwable)));
    }

    /**
     * Initialize the language when open the dialog.
     */
    private void initializeLanguage() {
        mLanguageChosen = mLanguageRentOfferModel.getFirstLanguage();
        updateAudioInfo();
    }

    /**
     * Switches the language to the given one.
     *
     * @param language to be changed to.
     */
    private void switchToLanguage(String language) {
        mLanguageChosen = language;
        updateAudioInfo();
        updateButtons(getActions());
        updatePriceInfo();
    }

    /**
     * Updates the grid of action buttons with the given actions.
     *
     * @param actions to be updated.
     */
    private void updateButtons(List<ActionModel> actions) {
        if (!actions.isEmpty()) {
            mAdapter.replaceAll(actions);
            if (mBinding.actionGrid.getAdapter() != null && mBinding.actionGrid.getAdapter().getItemCount() > 0) {
                mBinding.actionGrid.smoothScrollToPosition(0);
            }
            mBinding.actionGrid.setVisibility(View.VISIBLE);
        } else {
            mBinding.actionGrid.setVisibility(View.GONE);
        }
    }

    /**
     * Gets actions for the buttons action grid.
     *
     * @return list of actions.
     */
    private List<ActionModel> getActions() {
        List<ActionModel> actionsList = new ArrayList<>();
        List<VariantRentOffer> variantRentOffer = mLanguageRentOfferModel.getVariantRentOfferByLanguage(mLanguageChosen);
        for (int i = 0; i < variantRentOffer.size(); ++i) {
            VariantRentOffer model = variantRentOffer.get(i);
            IRentOffer offer = model.getOffer();
            if (offer != null) {
                String description = getRentButtonDescription(model, i == 0);
                ActionModel actionModel = new ActionModel(
                        0, 0,
                        getButtonText(model),
                        DetailPageButtonOrder.Rent,
                        () -> rentVod(model.getVodVariant(), model.getOffer(),
                                offer.getPriceForCurrency(
                                        offer.canPurchaseForCredit(mLanguageRentOfferModel.getCredit())
                                                ? CurrencyType.Credit : CurrencyType.Euro)),
                        (v, hasFocus) -> onButtonFocusChange(model, hasFocus)
                );
                actionModel.setContentDescription(description);
                actionsList.add(actionModel);
            }
        }

        for (String language : mLanguageRentOfferModel.getLanguages()) {
            // no need to add chosen language
            if (!language.equals(mLanguageChosen)) {
                ActionModel actionModel = new ActionModel(
                        0, 0,
                        getLanguageButtonText(language),
                        DetailPageButtonOrder.ChosenLanguage,
                        () -> switchToLanguage(language));
                actionModel.setContentDescription(
                        new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_BUTTON_OTHER))
                                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BUTTON_NAME, getLanguageButtonText(language))
                                .toString());
                actionsList.add(actionModel);
            }
        }
        return actionsList;
    }

    private String getRentButtonDescription(VariantRentOffer model, boolean isFirst) {
        TalkBackMessageBuilder details = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_BUTTON_OTHER));
        if (isFirst) {
            details = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_BUTTON_RENT_OPTIONS))
                    .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE, getTitle(model.getVodVariant()))
                    .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_ICONS, mBinding.iconContainer.getIconsDescription(mTranslator))
                    .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_AUDIO, mTranslator.get(TranslationKey.DETAIL_AUDIO_LANGUAGE) + " " + LocaleUtils.getLanguageFromIso(mLanguageChosen))
                    .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_SUBTITLE, getSubtitleInfo(model.getVodVariant()))
                    .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_INFO, getInfoDetails(model.getVodVariant()))
                    .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_RENTAL_PERIOD, getRentInfo(model.getOffer()));
        }

        return details
                .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BUTTON_NAME, getButtonText(model))
                .toString();
    }

    /**
     * Gets button text for a given language ISO code.
     *
     * @param languageIsoCode ISO code of the language.
     * @return button text.
     */
    private String getLanguageButtonText(String languageIsoCode) {
        if (LocaleUtils.ORIGINAL_VERSION_LANGUAGE_CODE.equals(languageIsoCode)) {
            return mTranslator.get(TranslationKey.SCREEN_RENT_CONFIRMATION_CHANGE_ORIGINAL_VERSION);
        }

        return mTranslator.get(TranslationKey.SCREEN_RENT_CONFIRMATION_CHANGE_AUDIO_VERSION)
                .replace(TRANSLATION_LANGUAGE, LocaleUtils.getLanguageFromIso(languageIsoCode));
    }

    /**
     * Gets button text for the given pair.
     *
     * @param variantRentOffer to get the text for.
     * @return text of the button.
     */
    private String getButtonText(VariantRentOffer variantRentOffer) {
        VideoQuality quality = variantRentOffer.getVodVariant().getHighestQuality();
        if (variantRentOffer.getOffer().canPurchaseForCredit(mLanguageRentOfferModel.getCredit())) {
            // case for renting with credits
            return mTranslator.get(TranslationKey.SCREEN_RENT_CONFIRMATION_BUTTON_QUALITY_CREDIT)
                    .replace(TRANSLATION_QUALITY, quality.toString())
                    .replace(TRANSLATION_CREDIT, String.valueOf(getCreditPrice(variantRentOffer.getOffer())));
        }

        // case for renting with currency
        IVodPrice priceForCurrency = variantRentOffer.getOffer().getPriceForCurrency(CurrencyType.Euro);
        double currencyAmount = 0;
        if (priceForCurrency != null) {
            currencyAmount = priceForCurrency.getCurrencyAmount();
        }

        String priceText = mLocaleSettings.getCurrency() + " " + StringUtils.formatPrice(currencyAmount);
        return mTranslator.get(TranslationKey.SCREEN_RENT_CONFIRMATION_BUTTON_QUALITY_CURRENCY)
                .replace(TRANSLATION_QUALITY, quality.toString())
                .replace(TRANSLATION_PRICE, priceText);
    }

    /**
     * Actions performed on buttons focus change.
     *
     * @param variantRentOffer     of a button with changed focus.
     * @param hasFocus shows if button has focus.
     */
    private void onButtonFocusChange(VariantRentOffer variantRentOffer, boolean hasFocus) {
        if (hasFocus) {
            updateTitle(variantRentOffer.getVodVariant());
            updateParentalRating(mVod);
            updateDescriptiveAudioIcon(mVod);
            updateDescriptiveSubtitleIcon(mVod);
            updateSubtitleInfo(variantRentOffer.getVodVariant());
            updateInfoRow(variantRentOffer.getVodVariant());
            updateRentInfo(variantRentOffer.getOffer());
            updateChooseQualityView(variantRentOffer.getOffer());
        }
    }

    /**
     * Rents VOD variant from given pair for the offer's price
     */
    private void rentVod(IVodVariant variant, IRentOffer offer, IVodPrice price) {
        mNavigator.showPurchasePinDialog(mVod, variant, offer, price);
    }

    private void updateTitle(IVodVariant variant) {
        String title = getTitle(variant);
        mBinding.title.setText(title);
        mBinding.title.setVisibility(TextUtils.isEmpty(title) ? View.GONE : View.VISIBLE);
    }

    private void updateParentalRating(IVodItem vod) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) mBinding.audioInfo.getLayoutParams();
        ParentalRating rating = vod.getParentalRating();
        if (rating == null || rating == ParentalRating.RatedAll) {
            mBinding.iconContainer.setVisibility(View.GONE);
            params.topMargin = getResources().getDimensionPixelOffset(R.dimen.audio_info_marin_top_without_qualifiers);
            return;
        }

        mBinding.iconContainer.showParentalRating(rating);
        params.topMargin = getResources().getDimensionPixelOffset(R.dimen.audio_info_marin_top_with_qualifiers);
        mBinding.iconContainer.setVisibility(View.VISIBLE);
    }

    /**
     * Displays descriptive audio icon.
     */
    private void updateDescriptiveAudioIcon(IVodItem vod) {
        if (vod.hasDescriptiveAudio()) {
            mBinding.iconContainer.showDescriptiveAudio();
        } else {
            mBinding.iconContainer.hideDescriptiveAudio();
        }
    }

    /**
     * Displays descriptive subtitle icon.
     */
    private void updateDescriptiveSubtitleIcon(IVodItem vod) {
        if (vod.hasDescriptiveSubtitle()) {
            mBinding.iconContainer.showDescriptiveSubtitle();
        } else {
            mBinding.iconContainer.hideDescriptiveSubtitle();
        }
    }

    private void updateAudioInfo() {
        if (TextUtils.isEmpty(mLanguageChosen)) {
            return;
        }

        String audioInfo = mTranslator.get(TranslationKey.DETAIL_AUDIO_LANGUAGE) + " " + LocaleUtils.getLanguageFromIso(mLanguageChosen);
        mBinding.audioInfo.setText(audioInfo);
        mBinding.audioInfo.setVisibility(TextUtils.isEmpty(audioInfo) ? View.GONE : View.VISIBLE);
    }

    private void updateSubtitleInfo(IVodVariant variant) {
        String info = getSubtitleInfo(variant);
        mBinding.subtitlesInfo.setText(info);
        mBinding.subtitlesInfo.setVisibility(TextUtils.isEmpty(info) ? View.GONE : View.VISIBLE);
    }

    private String getSubtitleInfo(IVodVariant variant) {
        List<String> deepCopyOfSubtitleLanguages = new ArrayList<>(variant.getSubtitleLanguages());

        if (variant.getSubtitleLanguages() != null && !deepCopyOfSubtitleLanguages.isEmpty()) {

            deepCopyOfSubtitleLanguages.replaceAll(LocaleUtils::getLanguageFromIso);
            String subtitles = TextUtils.join(", ", deepCopyOfSubtitleLanguages);

            if (TextUtils.isEmpty(subtitles)) {
                // Sometimes even if subtitleInfo.length > 0 the String "subtitles" is empty ...
                // TODO check video subtitles on CC.
                return mTranslator.get(TranslationKey.DETAIL_SUBTITLES)
                        + " " + mTranslator.get(TranslationKey.SUBTITLES_NONE);
            }

            return mTranslator.get(TranslationKey.DETAIL_SUBTITLES) + " " + subtitles;
        }

        return mTranslator.get(TranslationKey.DETAIL_SUBTITLES)
                + " " + mTranslator.get(TranslationKey.SUBTITLES_NONE);
    }

    private void updateInfoRow(IVodVariant variant) {
        String infoRowText = getInfoDetails(variant);
        mBinding.infoRow.setText(infoRowText);
        mBinding.infoRow.setVisibility(TextUtils.isEmpty(infoRowText) ? View.GONE : View.VISIBLE);
    }

    private String getInfoDetails(IVodVariant variant) {
        StringBuilder infoRowText = new StringBuilder();
        String durationText = LocaleTimeUtils.getDuration(variant.getDuration(), mTranslator);
        if (!TextUtils.isEmpty(durationText)) {
            infoRowText.append(durationText);
            infoRowText.append(TimeUtils.DIVIDER);
        }

        String availabilityText = getAvailabilityInfo(variant);
        if (!TextUtils.isEmpty(availabilityText)) {
            infoRowText.append(availabilityText);
        } else if (infoRowText.length() > 0) {
            infoRowText.delete(infoRowText.length() - TimeUtils.DIVIDER.length(), infoRowText.length());
        }

        return infoRowText.toString();
    }

    private String getAvailabilityInfo(IVodVariant variant) {
        String availabilityInfo = TimeUtils.formatDate(mLocaleSettings, variant.getLicensePeriodEnd());
        if (!TextUtils.isEmpty(availabilityInfo)) {
            return mTranslator.get(TranslationKey.DETAIL_VOD_AVAILABILITY) + " " + availabilityInfo;
        }
        return null;
    }

    private void updateRentInfo(IRentOffer offer) {
        String info = getRentInfo(offer);
        mBinding.rentInfo.setText(info);
        mBinding.rentInfo.setVisibility(TextUtils.isEmpty(info) ? View.GONE : View.VISIBLE);
    }

    private String getRentInfo(IRentOffer offer) {
        String info = null;

        long time = TimeUnit.MILLISECONDS.toHours(offer.getRentalTime());
        int credit = mLanguageRentOfferModel.getCredit();
        if (offer.canPurchaseForCredit(credit)) {
            info = mTranslator.get(TranslationKey.SCREEN_RENT_CONFIRMATION_CREDITS)
                    .replace(TRANSLATION_CREDIT, String.valueOf(getCreditPrice(offer)))
                    .replace(TRANSLATION_HOURS, String.valueOf(time))
                    .replace(TRANSLATION_REMAINING_CREDITS, String.valueOf(credit));
        }
        return info;
    }

    private void updatePriceInfo() {
        List<String> priceList = new ArrayList<>();
        List<VariantRentOffer> variantRentOffer = mLanguageRentOfferModel.getVariantRentOfferByLanguage(mLanguageChosen);
        if (!variantRentOffer.isEmpty()) {
            for (VariantRentOffer vodVariantRentOffer : variantRentOffer) {
                if (!vodVariantRentOffer.getOffer().canPurchaseForCredit(mLanguageRentOfferModel.getCredit())) {
                    continue;
                }

                IVodPrice priceForCurrency = vodVariantRentOffer.getOffer().getPriceForCurrency(CurrencyType.Euro);
                double currencyAmount = 0;
                if (priceForCurrency != null) {
                    currencyAmount = priceForCurrency.getCurrencyAmount();
                }
                
                String crossedOutPrice = StringUtils.addHTMLStrikeTag(
                        mLocaleSettings.getCurrency() + StringUtils.formatPrice(
                                currencyAmount));
                priceList.add(mTranslator.get(TranslationKey.SCREEN_RENT_CONFIRMATION_QUALITY_PRICE_STRIKETHROUGH)
                                .replace(TRANSLATION_PRICE, crossedOutPrice)
                                .replace(TRANSLATION_QUALITY, vodVariantRentOffer.getVodVariant().getHighestQuality().toString()));
            }

            // Update price info text.
            if (priceList.isEmpty()) {
                mBinding.priceInfo.setVisibility(View.GONE);
            } else {
                mBinding.priceInfo.setText(Html.fromHtml(TextUtils.join(" / ", priceList),
                        Html.FROM_HTML_MODE_LEGACY));
                mBinding.priceInfo.setVisibility(View.VISIBLE);
            }
        } else {
            mBinding.priceInfo.setVisibility(View.GONE);
        }
    }

    private void updateChooseQualityView(IRentOffer offer) {
        long time = TimeUnit.MILLISECONDS.toHours(offer.getRentalTime());
        String qualityText = mTranslator.get(TranslationKey.DETAIL_RENTAL_TIME_TEMPLATE)
                .replace(TRANSLATION_RENTAL_DURATION, String.valueOf(time));
        mBinding.qualityTv.setText(qualityText);
        mBinding.qualityTv.setVisibility(TextUtils.isEmpty(qualityText) ? View.GONE : View.VISIBLE);
    }

    /**
     * Build the title - > Display episode title if it is available
     */
    protected String getTitle(IVodVariant variant) {
        // Show title and episode info for vod.
        return mVod.getTitleWithSeasonEpisode(mTranslator, variant.getTitle(), " ");
    }

    private int getCreditPrice(IRentOffer offer) {
        if (offer.isFree()) {
            return 0;
        }
        IVodPrice price = offer.getPriceForCurrency(CurrencyType.Credit);
        if (price == null) {
            return Integer.MAX_VALUE;
        }
        return (int) price.getCurrencyAmount();
    }

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        if (mVod == null) {
            return null;
        }

        return ReportUtils.createEvent(UILog.Page.ChooseVersionPage, mVod);
    }
}
