package tv.threess.threeready.ui.startup.step;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Startup step to display the trigger registration when needed.
 *
 * @author Barabas Attila
 * @since 2019.03.14
 */
public class DeviceRegistrationStep implements Step {
    private static final String TAG = Log.tag(DeviceRegistrationStep.class);

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    private Disposable mDisposable;

    @Override
    public void execute(StepCallback callback) {
        mDisposable = mAccountRepository.isDeviceRegistrationRequired()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(mAppConfig.getQueryTimeout(), TimeUnit.SECONDS)
                .subscribe(required -> {
                    Log.d(TAG, "DeviceRegistrationStep successful," +
                            " need to show line number fragment: [" + required + "]");
                    if (required) {
                        mNavigator.showLineNumberFragment();
                    } else {
                        mAccountRepository.startUpdateDeviceRegistration();
                        callback.onFinished();
                    }
                }, throwable -> {
                    Log.e(TAG, "Device registration status check failed.", throwable);
                    callback.onFinished();
                });
    }

    @Override
    public void cancel() {
        RxUtils.disposeSilently(mDisposable);
    }
}
