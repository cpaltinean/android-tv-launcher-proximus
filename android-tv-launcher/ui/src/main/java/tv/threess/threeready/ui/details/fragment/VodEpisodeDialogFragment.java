/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.details.fragment;

import android.os.Bundle;
import android.util.Pair;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.generic.model.WatchlistType;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.wrapper.VodRentOffers;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.generic.adapter.DetailPageButtonOrder;
import tv.threess.threeready.ui.generic.hint.HintManager;
import tv.threess.threeready.ui.generic.model.VodPlayOption;
import tv.threess.threeready.ui.generic.presenter.ActionModel;
import tv.threess.threeready.ui.generic.presenter.RentActionModel;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Episode detail dialog for a VOD
 * <p>
 *
 * @author Zsolt Bokor
 * @since 2019.01.11
 */
public class VodEpisodeDialogFragment extends EpisodeDialogFragment<IBaseVodItem> {
    private static final String TAG = Log.tag(VodEpisodeDialogFragment.class);

    private final VodRepository mVodRepository = Components.get(VodRepository.class);
    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);

    private final List<Disposable> mDisposables = new ArrayList<>();
    private Disposable mAddWatchlistDisposable;
    private Disposable mRemoveWatchlistDisposable;

    public static VodEpisodeDialogFragment newInstance(IBaseVodItem vod, IVodSeries series) {
        VodEpisodeDialogFragment fragment = new VodEpisodeDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_CONTENT_ITEM, vod);
        bundle.putSerializable(EXTRA_SERIES_ITEM, series);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getButtonStates(getSeriesItem(), getContentItem());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        RxUtils.disposeSilently(mDisposables);
    }

    @Override
    protected void updateProvideLogo(IBaseVodItem episode) {
        super.updateProvideLogo(episode);

        //No logo will be shown until the provider logos will be available.
    }

    protected void updateButtons(IVodSeries series, VodRentOffers rentOffers, Boolean inWatchlist) {
        if (rentOffers == null) {
            return;
        }

        Map<String, IRentOffer> variantOfferMap = rentOffers.getVariantOfferMap();
        IVodItem episode = rentOffers.getVod();
        super.updateButtons(episode, null);

        List<ActionModel> actions = new ArrayList<>();

        // Show watch button
        List<VodPlayOption> vodPlayOptions = VodPlayOption.generatePlayOptions(episode, series, variantOfferMap);
        if (!vodPlayOptions.isEmpty()) {
            actions.add(new ActionModel(0, 0,
                    mTranslator.get(TranslationKey.SCREEN_DETAIL_PLAY_BUTTON),
                    DetailPageButtonOrder.Watch,
                    () -> {
                        if (vodPlayOptions.size() > 1) {
                            mNavigator.showPlayOptionsDialog(vodPlayOptions, episode);
                        } else {
                            VodPlayOption playOption = vodPlayOptions.get(0);
                            mNavigator.showVodPlayer(StartAction.Ui, playOption.getVodVariant(), episode, series, playOption.getVodPrice());
                            mNavigator.clearAllDialogs();
                        }
                    }));
        }

        // Show rent button.
        int credit = rentOffers.getCredit();
        if (episode.isRentable(credit, variantOfferMap)) {
            actions.add(new RentActionModel(0, 0,
                    DetailPageButtonOrder.Rent,
                    () -> mNavigator.showRentConfirmationDialog(episode),
                    credit, variantOfferMap
            ));
        }

        // Add more info.
        if (episode.isSVod() && !episode.isSubscribed()) {
            actions.add(new ActionModel(0, 0,
                    mTranslator.get(TranslationKey.SCREEN_DETAIL_MORE_INFO_BUTTON),
                    DetailPageButtonOrder.MoreInfo,
                    () -> {
                        // TODO: Subscribe - opens webview with upselling page - to be finalized in Upselling story
                    }));
        }

        // Add watchlist button
        if (inWatchlist != null) {
            if (inWatchlist) {
                actions.add(new ActionModel(
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_REMOVEFROMWATCHLIST_BUTTON), DetailPageButtonOrder.RemoveFromWatchList,
                        () -> {
                            RxUtils.disposeSilently(mRemoveWatchlistDisposable);
                            mRemoveWatchlistDisposable = mAccountRepository.removeFromWatchlist(episode.getId(), WatchlistType.Vod)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(() -> Log.d(TAG, "Successfully removed the item from watchlist"),
                                            throwable -> Log.e(TAG, "Could not remove item from watchlist.", throwable));
                        }
                ));
            } else {
                actions.add(new ActionModel(
                        mTranslator.get(TranslationKey.SCREEN_DETAIL_ADDTOWATCHLIST_BUTTON), DetailPageButtonOrder.AddToWatchList,
                        () -> {
                            RxUtils.disposeSilently(mAddWatchlistDisposable);
                            mAddWatchlistDisposable = mAccountRepository.addToWatchlist(WatchlistType.Vod, episode.getId())
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(() -> {
                                        Log.d(TAG, "Successfully added item to watchlist");
                                        Components.get(HintManager.class).showAddToWatchlistHints();
                                    }, throwable -> Log.e(TAG, "Could not add item to watchlist", throwable));
                        }
                ));
            }
        }

        if (actions.isEmpty()) {
            mBinding.actionGrid.setVisibility(View.GONE);
            if (mBinding.showMoreBtn.getVisibility() == View.VISIBLE) {
                mBinding.showMoreBtn.requestFocus();
            }
        } else {
            updateButtonDescriptions(actions, episode);
            mAdapter.replaceAll(actions);
        }
    }

    /**
     * Get the rent offers for each VodVariant. Check for cheapest price / biggest rental time and update the UI with this info.
     *
     * @param movie the vod for which we check this
     */
    private void getButtonStates(IVodSeries vodSeries, IBaseVodItem movie) {
        RxUtils.disposeSilently(mDisposables);
        mDisposables.add(Observable.combineLatest(
                mVodRepository.getVariantRentOffers(movie),
                mAccountRepository.isInWatchlist(movie.getId()),
                Pair::new)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(vodVariantRentOffersBooleanPair -> {
                    VodRentOffers rentOffers = vodVariantRentOffersBooleanPair.first;
                    Boolean inWatchlist = vodVariantRentOffersBooleanPair.second;
                    Log.d(TAG, "Rent offers and watchlist state retrieved, inWatchlist = [" + inWatchlist + "]");
                    updateButtons(vodSeries, rentOffers, inWatchlist);
                }, throwable -> {
                    Log.e(TAG, "Could not get watchlist state");
                    updateButtons(vodSeries, null, null);
                }));
    }

    @Override
    protected Event<UILogEvent, UILogEvent.Detail> generatePageOpenEvent() {
        IBaseVodItem vod = getContentItem();

        if (vod == null) {
            return null;
        }

        return new Event<UILogEvent, UILogEvent.Detail>(UILogEvent.PageOpen)
                .addDetail(UILogEvent.Detail.Page, UILog.Page.ActionMenuVodDialog)
                .addDetail(UILogEvent.Detail.PageName, ReportUtils.getPageName(vod))
                .addDetail(UILogEvent.Detail.PageId, ReportUtils.getPageId(vod))
                .addDetail(UILogEvent.Detail.FocusName, vod.getVariantIds().size() > 1 ? vod.getItemTitle() : null)
                .addDetail(UILogEvent.Detail.FocusId, vod.getVariantIds().size() > 1 ? vod.getId() : null);
    }
}
