package tv.threess.threeready.ui.details.utils;

import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Enum used for series filter item.
 *
 * @author Daniela Toma
 * @since 2022.03.08
 */
public enum SeriesFilterType {
    Episode(TranslationKey.PAGE_SERIES_TAB_EPISODES),
    MostRecent(TranslationKey.PAGE_SERIES_TAB_RECENT),
    Planned(TranslationKey.PAGE_SERIES_TAB_PLANNED);

    SeriesFilterType(String text) {
        this.mText = text;
    }

    private final String mText;

    public String getText() {
        return mText;
    }
}


