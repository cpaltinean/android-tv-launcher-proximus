package tv.threess.threeready.ui.generic.hint;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.OpenManageChannelHintsDialogBinding;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.view.FontTextView;

/**
 * Dialog for open manage channel hints.
 *
 * @author Zsolt Bokor_
 * @since 2020.06.22
 */
public class OpenManageChannelHintsDialog extends EpgHintsDialog {

    private OpenManageChannelHintsDialogBinding mBinding;

    public static OpenManageChannelHintsDialog newInstance(String hintId, boolean hintOpenedFromSettings) {
        Bundle args = new Bundle();
        args.putString(EXTRA_HINT_ID, hintId);
        args.putBoolean(EXTRA_HINT_OPENED_FROM_SETTINGS, hintOpenedFromSettings);
        OpenManageChannelHintsDialog fragment = new OpenManageChannelHintsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected View inflateView() {
        if (mBinding == null) {
            mBinding = OpenManageChannelHintsDialogBinding.inflate(LayoutInflater.from(getContext()));
        }

        return mBinding.getRoot();
    }

    @Override
    protected FontTextView getDescriptionSmall() {
        return mBinding.descriptionSmall;
    }

    @NonNull
    @Override
    protected ImageView getBackgroundImage() {
        return mBinding.background;
    }

    @NonNull
    @Override
    protected TextView getPreviousButton() {
        return mBinding.buttonContainer.previousBtn;
    }

    @NonNull
    @Override
    protected TextView getNextButton() {
        return mBinding.buttonContainer.nextBtn;
    }

    @Override
    protected int getBackgroundImageId() {
        switch (LocaleUtils.getApplicationLanguage()) {
            case LocaleUtils.DUTCH_LANGUAGE_CODE:
                return R.drawable.open_manage_channel_hints_background_nl;
            case LocaleUtils.FRENCH_LANGUAGE_CODE:
                return R.drawable.open_manage_channel_hints_background_fr;
            default:
                return R.drawable.open_manage_channel_hints_background_en;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.description.setText(mTranslator.get(TranslationKey.HINT_TEXT_OPEN_MANAGE_CHANNELS_MINI_EPG));
        view.setContentDescription(mTranslator.get(TranslationKey.HINT_TEXT_OPEN_MANAGE_CHANNELS_MINI_EPG)
                + " " + mTranslator.get(TranslationKey.HINT_REVIEW_ALL_YOUR_TIPS));
    }
}
