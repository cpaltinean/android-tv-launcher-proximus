package tv.threess.threeready.ui.generic.adapter.itemdecoration;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import tv.threess.threeready.ui.generic.view.ResizableCardView;

/**
 * Adjust the spacing between the items in order to place the card near to each other in minimized state.
 * In maximized state the cards can overlap each other.
 */
public class StripeItemSpaceDecorator extends RecyclerView.ItemDecoration {

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        // Set spacing for card based on the card
        if (view instanceof ResizableCardView) {
            ResizableCardView resizableCardView = (ResizableCardView) view;

            // Calculate spacing based on min and maximum card size.
            int horizontalSpacing = resizableCardView.getMaxHorizontalPadding();
            outRect.left -= horizontalSpacing;
            outRect.right -= horizontalSpacing;

            // Center the card vertically.
            int verticalPadding = parent.getHeight()
                    - resizableCardView.getCardHeight();
            outRect.top += verticalPadding / 2;
        } else {
            // Center the card in parent.
            int verticalPadding = parent.getHeight() - view.getLayoutParams().height;
            outRect.top += verticalPadding / 2;
        }
    }
}