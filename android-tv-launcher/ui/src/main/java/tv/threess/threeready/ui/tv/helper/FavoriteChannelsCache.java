/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.tv.helper;

import android.os.Handler;
import android.os.Looper;

import java.util.LinkedHashSet;
import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.TvRepository;

/**
 * This class will save the channels that we need to add or remove, and after the user stops adding or removing,
 * it will send a batch of inserts / deletes to sync with the database
 *
 * @author Paul
 * @since 2018.03.12
 */
public class FavoriteChannelsCache implements Component {
    private static final String TAG = Log.tag(FavoriteChannelsCache.class);

    private final TvRepository mTvRepository = Components.get(TvRepository.class);

    private static final int SYNC_DELAY = 2000;

    private final Set<String> mAddFavoriteChannels = new LinkedHashSet<>();
    private final Set<String> mRemoveFavoriteChannels = new LinkedHashSet<>();
    private final Handler mHandler;
    private final Runnable mSyncFavoritesRunnable;

    public FavoriteChannelsCache() {
        mHandler = new Handler(Looper.getMainLooper());
        mSyncFavoritesRunnable = () -> Observable.create((ObservableOnSubscribe<Boolean>) e -> {
            mTvRepository.synchronizeFavoriteChannels(mAddFavoriteChannels, mRemoveFavoriteChannels);
            e.onNext(true);
            e.onComplete();
        }).subscribeOn(Schedulers.io()).observeOn(Schedulers.io())
                .subscribe(aBoolean -> Log.d(TAG, "Successfully synced favorites with database"), throwable -> Log.e(TAG, "Error syncing favorites", throwable));
    }

    public void addFavoriteChannel(final String channelId) {
        Log.d(TAG, "Want to add favorite channel id " + channelId);
        if (mRemoveFavoriteChannels.contains(channelId)) {
            mRemoveFavoriteChannels.remove(channelId);
        } else {
            mAddFavoriteChannels.add(channelId);
        }
        mHandler.removeCallbacksAndMessages(null);
        mHandler.postDelayed(mSyncFavoritesRunnable, SYNC_DELAY);
    }

    public void removeFavoriteChannel(final String channelId) {
        Log.d(TAG, "Want to remove favorite channel id " + channelId);
        if (mAddFavoriteChannels.contains(channelId)) {
            mAddFavoriteChannels.remove(channelId);
        } else {
            mRemoveFavoriteChannels.add(channelId);
        }
        mHandler.removeCallbacksAndMessages(null);
        mHandler.postDelayed(mSyncFavoritesRunnable, SYNC_DELAY);
    }

    public void saveChanges() {
        mHandler.removeCallbacksAndMessages(null);
        mSyncFavoritesRunnable.run();
    }
}
