package tv.threess.threeready.ui.details.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.leanback.widget.HorizontalGridView;
import androidx.leanback.widget.Presenter;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.helper.TalkBackMessageBuilder;
import tv.threess.threeready.player.IPlaybackCallback;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.exception.PlaybackError;
import tv.threess.threeready.player.model.PlaybackDetails;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.dialog.AlertDialog;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.presenter.ActionModel;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.utils.LocaleTimeUtils;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.generic.view.FontTextView;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Presenter class for a program detail overview.
 *
 * @author Eugen Guzyk
 * @since 2018.09.25
 */
public abstract class BaseDetailsOverviewPresenter<THolder extends BaseDetailsOverviewPresenter.ViewHolder, TItem extends IContentItem>
        extends BasePresenter<THolder, TItem> {

    protected final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    protected final Translator mTranslator = Components.get(Translator.class);
    protected final LocaleSettings mLocaleSettings = Components.get(LocaleSettings.class);
    protected final Navigator mNavigator = Components.get(Navigator.class);
    protected final PlaybackDetailsManager mPlaybackManager = Components.get(PlaybackDetailsManager.class);

    public BaseDetailsOverviewPresenter(Context context) {
        super(context);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        ViewHolder holder = createViewHolder(parent);

        holder.getTitleView().setTextColor(mLayoutConfig.getFontColor());
        holder.getInfoRow().setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f));
        holder.getCastView().setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f));
        holder.getDirectorsView().setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), 0.7f));

        ImageView showMoreBtn = holder.getShowMoreBtn();
        showMoreBtn.setBackground(UiUtils.createMoreButtonDrawable(parent.getContext(), mLayoutConfig));
        showMoreBtn.setContentDescription(mTranslator.get(TranslationKey.TALKBACK_BUTTON_MORE_INFO));

        FontTextView nonPlayableText = holder.getNonPlayableTextView();
        nonPlayableText.setTextColor(mLayoutConfig.getFontColor());
        nonPlayableText.setText(mTranslator.get(TranslationKey.DETAIL_RECORDING_NOT_PLAYABLE));

        TextSwitcher descriptionView = holder.getDescriptionView();
        descriptionView.setFactory(() -> {
            FontTextView fontTextView = (FontTextView) LayoutInflater.from(parent.getContext())
                    .inflate(getDescriptionLayout(), descriptionView, false);
            fontTextView.setTextColor(mLayoutConfig.getFontColor());
            return fontTextView;
        });
        descriptionView.setInAnimation(AnimationUtils.loadAnimation(parent.getContext(), android.R.anim.fade_in));
        descriptionView.setOutAnimation(AnimationUtils.loadAnimation(parent.getContext(), android.R.anim.fade_out));

        return holder;
    }

    /**
     * Gets the layout that should be used to show description.
     */
    protected abstract int getDescriptionLayout();

    /**
     * Gets the width for description view.
     */
    protected abstract float getDescriptionWidth();

    /**
     * Gets the start margin that is used for showing Show More button.
     */
    protected  abstract int getMarginStartForShowMore(View view);

    protected abstract BaseDetailsOverviewPresenter.ViewHolder createViewHolder(ViewGroup parent);

    @CallSuper
    @Override
    public void onBindHolder(THolder holder, TItem item) {
        // Register playback state change listener.
        holder.mPlayerCallback = new PlayerCallback(holder, item);
        mPlaybackManager.registerCallback(holder.mPlayerCallback);

        updateTitle(holder, item);
        updateMarkersRow(holder, item);
        updateInfoRow(holder, item);
        updateDescription(holder, item);
        updateCast(holder, item);
        updateDirectors(holder, item);
        updateButtons(holder, item);
    }

    protected void updateNonPlayableViewGroupVisibility(THolder holder, boolean isPlayable) {
        int visibility = isPlayable ? View.GONE : View.VISIBLE;
        holder.getNonPlayableTextView().setVisibility(visibility);
        holder.getNonPlayableIcon().setVisibility(visibility);
        holder.getNonPlayableOverlay().setVisibility(visibility);
    }

    @Override
    public void onUnbindHolder(THolder holder) {
        super.onUnbindHolder(holder);

        mPlaybackManager.unregisterCallback(holder.mPlayerCallback);
        holder.mPlayerCallback = null;
    }

    protected abstract void updateTitle(THolder holder, TItem item);

    protected abstract void updateMarkersRow(THolder holder, TItem item);

    protected abstract void updateInfoRow(THolder holder, TItem item);

    protected abstract void updateButtons(THolder holder, TItem item);

    protected void updateDescription(THolder holder, TItem item) {
        String description = item.getDescription();
        if (TextUtils.isEmpty(description)) {
            holder.getDescriptionView().setVisibility(View.GONE);
            return;
        }
        showDescription(holder, item, item.getDescription(), null);
    }

    /**
     * shows description view, manages lines and + more button
     *
     * @param holder        which view
     * @param description   dialog description at for more button
     * @param secondaryDesc secondary data
     */
    protected void showDescription(THolder holder, TItem item, String description, String secondaryDesc) {
        TextSwitcher descriptionView = holder.getDescriptionView();
        ImageView showMoreBtn = holder.getShowMoreBtn();
        descriptionView.setVisibility(View.VISIBLE);

        TextView textView = (TextView) descriptionView.getCurrentView();

        int moreInfoMarginStart = getMarginStartForShowMore(holder.view);
        int maxLines = textView.getMaxLines();
        float measuredText = textView.getPaint().measureText(description) + moreInfoMarginStart;
        float viewWidth = getDescriptionWidth();

        String text;
        if (viewWidth > 0 && measuredText / viewWidth > maxLines) {
            int ellipsizePosition = textView.getPaint().breakText(
                    description, true, maxLines * viewWidth - moreInfoMarginStart, null);
            text = String.format("%s...", description.substring(0, ellipsizePosition));
            showMoreBtn.setVisibility(View.VISIBLE);
        } else {
            text = description;
            showMoreBtn.setVisibility(View.GONE);
        }

        String displayedText = textView.getText() + "";

        // Display immediately.
        if (TextUtils.isEmpty(displayedText)) {
            descriptionView.setCurrentText(text);
            // Animate text change.
        } else if (!text.equals(displayedText)) {
            descriptionView.setText(text);
        }

        // Calculate show more button again after layout.
        if (descriptionView.getHeight() == 0) {
            descriptionView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v, int left, int top, int right,
                                           int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    descriptionView.post(() -> updateDescription(holder, item));
                    descriptionView.removeOnLayoutChangeListener(this);
                }
            });
        }

        // Show description dialog.
        showMoreBtn.setOnClickListener(v -> {
            AlertDialog dialog = new AlertDialog.Builder()
                    .title(item.getTitle())
                    .description(description)
                    .secondaryDescription(secondaryDesc)
                    .descriptionFontSize(mContext.getResources().getDimensionPixelOffset(R.dimen.alert_description_size_more_info))
                    .cancelable(true)
                    .build();

            mNavigator.showDialog(dialog);
        });

    }

    protected void updateCast(THolder holder, TItem item) {
        FontTextView castView = holder.getCastView();
        String actors = TextUtils.join(", ", item.getActors());
        if (TextUtils.isEmpty(actors)) {
            castView.setVisibility(View.GONE);
        } else {
            castView.setVisibility(View.VISIBLE);
            String actorsStr = mTranslator.get(TranslationKey.SCREEN_DETAIL_CAST_CREW) + ": " + actors;
            castView.setText(actorsStr);
        }
    }

    protected void updateDirectors(THolder holder, TItem item) {
        FontTextView directorsView = holder.getDirectorsView();
        String directors = TextUtils.join(", ", item.getDirectors());
        if (TextUtils.isEmpty(directors)) {
            directorsView.setVisibility(View.GONE);
        } else {
            directorsView.setVisibility(View.VISIBLE);
            String directorsStr = mTranslator.get(TranslationKey.SCREEN_DETAIL_CAST_CREW_ONE) + ": " + directors;
            directorsView.setText(directorsStr);
        }
    }

    /**
     * Method used for update the detail page button content descriptions
     * used by the TalkBack service.
     */
    protected void updateActionModelDescriptions(List<ActionModel> modelList, IContentItem contentItem) {
        for (int i = 0; i < modelList.size(); ++i) {
            ActionModel actionModel = modelList.get(i);

            TalkBackMessageBuilder description = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_BUTTON_OTHER));
            if (i == 0) {
                description = new TalkBackMessageBuilder(mTranslator.get(TranslationKey.TALKBACK_BUTTON_DETAIL_PAGE))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_TITLE_OF_ASSET, contentItem.getTitle())
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_AGE_RATING, contentItem.getParentalRating().name())
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_DATE, contentItem.getReleaseYear())
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_GENRE, contentItem.getGenres().toString())
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_DURATION, LocaleTimeUtils.getDuration(contentItem.getDuration(), mTranslator))
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_SYNOPSYS, contentItem.getDescription())
                        .replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_CAST, contentItem.getActors().toString());
            }
            actionModel.setContentDescription(description.replaceOrRemove(PlaceholderKey.TALKBACK_DETAIL_BUTTON_NAME, actionModel.getText()).toString());
        }
    }

    @Override
    public long getStableId(TItem item) {
        return Objects.hash(item.getId());
    }

    /**
     * Called when the playback changed in background player.
     */
    public void onPlaybackChanged(THolder holder, TItem item) {
        updateMarkersRow(holder, item);
    }

    /**
     * Used to notify ui components about background playback changes.
     */
    private class PlayerCallback implements IPlaybackCallback {
        TItem mItem;
        THolder mHolder;

        PlayerCallback(THolder holder, TItem item) {
            mItem = item;
            mHolder = holder;
        }

        @Override
        public void onCommandSuccess(PlaybackCommand cmd, PlaybackDetails details) {
            updateViewOnPlaybackChange();
        }

        @Override
        public void onCommandFailure(PlaybackCommand cmd, PlaybackDetails details, PlaybackError error) {
            updateViewOnPlaybackChange();
        }

        private void updateViewOnPlaybackChange() {
            mHolder.view.post(() -> onPlaybackChanged(mHolder, mItem));
        }
    }

    public abstract static class ViewHolder extends Presenter.ViewHolder {
        IPlaybackCallback mPlayerCallback;

        public ViewHolder(View view) {
            super(view);
        }

        @NotNull
        public abstract HorizontalGridView getActionGridView();

        @NotNull
        protected abstract ImageView getNonPlayableIcon();

        @NotNull
        protected abstract FontTextView getNonPlayableTextView();

        @NonNull
        protected abstract View getNonPlayableOverlay();

        @NotNull
        protected abstract FontTextView getTitleView();

        @NotNull
        protected abstract FontTextView getInfoRow();

        @NotNull
        protected abstract TextSwitcher getDescriptionView();

        @NotNull
        protected abstract ImageView getShowMoreBtn();

        @NotNull
        protected abstract FontTextView getCastView();

        @NotNull
        protected abstract FontTextView getDirectorsView();
    }

    public interface IDetailLoaded {
        void loadPage();
    }
}
