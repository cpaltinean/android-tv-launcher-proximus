package tv.threess.threeready.ui.generic.notification;

import android.os.Handler;
import android.os.Looper;

import androidx.fragment.app.FragmentTransaction;

import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.gms.GmsRepository;
import tv.threess.threeready.api.home.model.generic.NotificationSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.notification.NotificationPriority;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.activity.MainActivity;
import tv.threess.threeready.ui.generic.notification.BaseNotification.Timeout;
import tv.threess.threeready.ui.generic.utils.PlaceholderKey;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Handles all the notification visibility, shows, hides them.
 * Note: in case of system notifications are visible, it is dismissing others ones
 * automatically.
 *
 * @author hunormadaras
 * @since 2019-08-19
 */
public class NotificationManager implements Component {
    public static final String TAG = Log.tag(NotificationManager.class);

    private final Translator mTranslator = Components.get(Translator.class);
    private final GmsRepository mGmsRepository = Components.get(GmsRepository.class);
    private final NotificationSettings mNotificationSettings = Components.get(NotificationSettings.class);
    protected AppConfig mAppConfig = Components.get(AppConfig.class);

    private Disposable mGetSystemNotificationsDisposable;

    protected MainActivity mActivity;

    private final Handler mTimeoutHandler = new Handler(Looper.getMainLooper());

    private final Comparator<BaseNotification> notificationComparator = (BaseNotification o1, BaseNotification o2) -> (int)(o2.getPriority() - o1.getPriority());
    private final PriorityQueue<BaseNotification> mNotificationQueue = new PriorityQueue<>(notificationComparator);

    private BaseNotification mCurrentNotification;

    private static final long NOTIFICATION_MINIMUM_DISPLAY_TIME = TimeUnit.SECONDS.toMillis(2);
    private static final long RENT_NOTIFICATION_TIMEOUT = TimeUnit.SECONDS.toMillis(30);

    /**
     * Method that subscribes to system notification updates, in case of a new system notification arrives then automatically closes other ones if they are
     * visible
     */
    private void subscribeToSystemNotifications() {
        RxUtils.disposeSilently(mGetSystemNotificationsDisposable);
        mGetSystemNotificationsDisposable = mGmsRepository.getSystemNotifications(NotificationPriority.MAX)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(systemNotification -> {
                if (!ArrayUtils.isEmpty(systemNotification)) {
                    hideNotification();
                }
            });
    }

    /**
     * Hides the current notification.
     */
    void hideNotification() {
        hideNotification(getNotification());
    }

    /**
     * Hide the specified fragment right away.
     * This method removes the notification fragment in the moment it is called.
     *
     */
    public void hideNotification(BaseNotification notification) {
        if (notification == null || notification.isHidden() || !notification.isVisible()) {
            return;
        }

        notification.hideNotificationAnimation();
        FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
        transaction.remove(mActivity.getSupportFragmentManager().findFragmentById(R.id.notification_container));
        transaction.commitAllowingStateLoss();
        mCurrentNotification = null;
    }

    /**
     * When the param activity is created then we subscribe to the system notifications
     *
     * @param activity main activity
     */
    public void init(MainActivity activity) {
        mActivity = activity;
        subscribeToSystemNotifications();

        mCurrentNotification = getNotification();
        if (mCurrentNotification != null && getTimeOutSeconds(mCurrentNotification) > 0) {
            hideNotification(mCurrentNotification);
        }
    }

    public void destroy() {
        if (mTimeoutHandler != null && mTimeoutRunnable != null) {
            mTimeoutHandler.removeCallbacks(mTimeoutRunnable);
        }

        RxUtils.disposeSilently(mGetSystemNotificationsDisposable);
    }

    public BaseNotification getNotification() {
        return (BaseNotification) mActivity.getSupportFragmentManager()
                .findFragmentById(R.id.notification_container);
    }

    /**
     * Adds the notification in the queue and shows the first one.
     */
    public void showNotification(BaseNotification notification) {
        if (notification == null || !notification.showIfEnqueued(mNotificationQueue)) {
            // Nothing to do.
            return;
        }

        try {
            // if the notification is already displayed we don`t have to add again to the queue
            if (notification.isDisplayed(mCurrentNotification)) {
                // we have to reset the timer
                mTimeoutHandler.removeCallbacks(mTimeoutRunnable);
                mTimeoutHandler.postDelayed(mTimeoutRunnable, getTimeOutSeconds(mCurrentNotification));
                return;
            }

            // Skipp the queue display the notification immediately.
            if (notification.skippDisplayQueue()) {
                mCurrentNotification = notification;
                showCurrentNotification();
                mTimeoutHandler.removeCallbacks(mTimeoutRunnable);
                mTimeoutHandler.postDelayed(mTimeoutRunnable, getTimeOutSeconds(mCurrentNotification));
                return;
            }

            mNotificationQueue.add(notification);

            if (mNotificationQueue.size() >= 1 && mCurrentNotification != null
                    && getTimeOutSeconds(mCurrentNotification) != NOTIFICATION_MINIMUM_DISPLAY_TIME) {
                // If in the queue are more than one notifications,
                // the first notification has to be shown for two seconds, and not for the timeout that was previously set.
                resolveNotificationTimeout();
                return;
            }

            // Not other notification. Display immediately.
            if (mCurrentNotification == null) {
                showNextNotification();
                resolveNotificationTimeout();
            }
        } catch (Exception e) {
            Log.e(TAG, "An exception occurred on showing the notification: " + e);
        }
    }

    /**
     * Start renting process and displays a rent notification which blocks the UI
     */
    public void showRentNotification(IVodItem vod, IVodVariant variant) {
        showNotification(RentNotification.newInstance(vod, variant));
    }

    /**
     * Series notification about the rent failure.
     */
    public void showRentFailedNotification() {
        Notification notification = new Notification.Builder()
            .title(mTranslator.get(TranslationKey.ERR_PLAYER_PAYMENT_NOT_CHARGED_TITLE))
            .subtext(mTranslator.get(TranslationKey.ERR_PLAYER_PAYMENT_NOT_CHARGED))
            .build();
        showNotification(notification);
    }

    /**
     * Use this function to display age restriction legal message.
     * The function receives a parental rating value and decides is should create notification or not
     *
     * @param parentalRating - The parental rating value of the vod
     */
    public Notification showAgeRestrictionLegalNotificationIfNeeded(ParentalRating parentalRating) {
        if (!parentalRating.isRestricted()) {
            return null;
        }

        Notification notification = new Notification.Builder()
                .title(mTranslator.get(TranslationKey.NOTIFICATION_PLAYER_AGE_RATING_TITLE))
                .subtext(mTranslator.get(TranslationKey.NOTIFICATION_PLAYER_AGE_RATING_BODY,
                        Collections.singletonMap(PlaceholderKey.PARENTAL_CONTROL_RATING,
                                String.valueOf(parentalRating.getMinimumAge()))))
                .iconId(UiUtils.getParentalRatingIcon(parentalRating))
                .timeout(Timeout.LENGTH_LONG)
                .build();

        showNotification(notification);

        return notification;
    }


    /**
     * Displays a live notification dialog in the bottom right corner of the screen.
     */
    public void showLiveNotification() {
        showNotification(new LiveNotification());
    }

    // TODO: use this notifications for accurate blacklisting.
    /**
     * Notification shown when a blacklisted playback slot is reached by backward seeking.
     */
    public void showBlacklistedNotification() {
        showNotification(new SingleNotification.Builder()
                .title(mTranslator.get(TranslationKey.BLACKLISTED_NOTIFICATION_TITLE))
                .subtext(mTranslator.get(TranslationKey.BLACKLISTED_NOTIFICATION_BODY))
                .build());
    }

    /**
     * Notification shown when a blacklisted playback slot is reached by forward seeking.
     */
    public void showBlacklistedJumpLiveNotification() {
        showNotification(new Notification.Builder()
                .title(mTranslator.get(TranslationKey.BLACKLISTED_LIVE_NOTIFICATION_TITLE))
                .subtext(mTranslator.get(TranslationKey.BLACKLISTED_LIVE_NOTIFICATION_BODY))
                .build());
    }

    /**
     * Displays a notification when the buffer is over and the player can't be paused
     * or can't be pause anymore.
     */
    public void showCantPauseNotification() {
        showNotification(new Notification.Builder()
                .title(mTranslator.get(TranslationKey.TIMESHIFT_NOTIFICATION_PAUSE_TITLE))
                .subtext(mTranslator.get(TranslationKey.TIMESHIFT_NOTIFICATION_PAUSE_BODY))
                .build());
    }

    /**
     * Displays a notification before an update is forced on the stb.
     */
    public void showForcedUpdateNotification() {
        showNotification(new Notification.Builder()
                .title(mTranslator.get(TranslationKey.NOTIFICATION_RESTART_TITLE))
                .subtext(mTranslator.get(TranslationKey.NOTIFICATION_RESTART_BODY))
                .timeout(Timeout.LENGTH_FORCED_REBOOT_NOTIFICATION)
                .build());
    }

    /**
     * Displays a notification before a reboot is forced on the stb.
     */
    public void showForcedRebootNotification() {
        showNotification(new Notification.Builder()
                .title(mTranslator.get(TranslationKey.NOTIFICATION_RESTART_GENERIC_TITLE))
                .subtext(mTranslator.get(TranslationKey.NOTIFICATION_RESTART_GENERIC_BODY))
                .timeout(Timeout.LENGTH_FORCED_REBOOT_NOTIFICATION)
                .build());
    }

    /**
     * Runnable used to display and hide notifications from queue.
     */
    private final Runnable mTimeoutRunnable = new Runnable() {
        @Override
        public void run() {

            if (mNotificationQueue.size() > 0) {
                showNextNotification();
                resolveNotificationTimeout();
            } else {
                hideNotification(mCurrentNotification);
                mCurrentNotification = null;
            }
        }
    };

    /**
     * Shows the next notification form the queue.
     */
    private void showNextNotification() {
        if (mNotificationQueue.size() > 0) {
            //Get the first notification from the queue.
            mCurrentNotification = mNotificationQueue.poll();
            showCurrentNotification();
        }
    }

    /**
     * Show the current notification on the screen.
     */
    private void showCurrentNotification() {
        try {
            BaseNotification displayedNotification = getNotification();
            BaseNotification currentNotification = mCurrentNotification;
            // If a notification already shown, only the notification with the highest priority remain displayed.
            if (displayedNotification == null
                    || currentNotification.getPriority() >= displayedNotification.getPriority()) {

                final FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.animator.notification_fade_in, R.animator.notification_fade_out);
                transaction.replace(R.id.notification_container, currentNotification);
                transaction.commitAllowingStateLoss();
            }
        } catch (Exception e) {
            Log.e(TAG, "Could not show notification.", e);
        }
    }

    /**
     * Gets the detraction of the timeout in seconds for the specified notification.
     */
    private long getTimeOutSeconds(BaseNotification notification) {
        if (notification != null) {
            switch (notification.getTimeOut()) {
                case LENGTH_MINIMUM:
                    return TimeUnit.SECONDS.toMillis((mNotificationSettings.getMinimumDisplayDuration()));

                case LENGTH_SHORT:
                    return TimeUnit.SECONDS.toMillis(mNotificationSettings.getLowDuration());

                case LENGTH_MEDIUM:
                    return TimeUnit.SECONDS.toMillis(mNotificationSettings.getNormalDuration());

                case LENGTH_LONG:
                    return TimeUnit.SECONDS.toMillis(mNotificationSettings.getHighDuration());

                case LENGTH_RENT_NOTIFICATION:
                    return RENT_NOTIFICATION_TIMEOUT;

                case LENGTH_FORCED_REBOOT_NOTIFICATION:
                    return mAppConfig.getForcedRebootDelay(TimeUnit.MILLISECONDS);

                case LENGTH_PERMANENT:
                default:
                    break;
            }
        }
        return TimeUnit.SECONDS.toMillis(mNotificationSettings.getUrgentDuration());
    }

    /**
     * Resolves and sets timeout for the notification.
     */
    private void resolveNotificationTimeout() {
        // Set timeout for the current notification.
        mTimeoutHandler.removeCallbacks(mTimeoutRunnable);
        if (mNotificationQueue.size() > 0) {
            // If one or more notifications are in the queue, than show the notification for 2 seconds.
            mCurrentNotification.setTimeOut(Timeout.LENGTH_MINIMUM);
            mTimeoutHandler.postDelayed(mTimeoutRunnable, getTimeOutSeconds(mCurrentNotification));
        } else {
            // No other notification. Display for the configured time.
            mTimeoutHandler.postDelayed(mTimeoutRunnable, getTimeOutSeconds(mCurrentNotification));
        }
    }

    public Handler getTimeoutHandler() {
        return mTimeoutHandler;
    }
}
