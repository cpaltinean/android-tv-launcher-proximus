package tv.threess.threeready.ui.settings.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.VerticalGridView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.ui.databinding.SettingsLockContentLayoutBinding;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.ui.settings.model.LockContentItem;

/**
 * Parental control dialog showing parental control options
 *
 * @author Daniela Toma
 * @since 2019.05.30
 */
public class LockContentFragment extends BaseSettingsDialogFragment<LockContentItem> {

    private SettingsLockContentLayoutBinding mBinding;

    private final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);

    public static LockContentFragment newInstance() {
        return new LockContentFragment();
    }

    @NonNull
    protected View inflateLayout(){
        if (mBinding == null) {
            mBinding = SettingsLockContentLayoutBinding.inflate(LayoutInflater.from(getContext()));
        }

        return mBinding.getRoot();
    }

    @NonNull
    protected View getRightFrame() {
        return mBinding.rightFrame;
    }

    protected VerticalGridView getVerticalGridView() {
        return mBinding.grid;
    }

    protected TextView getTitleView() {
        return mBinding.title;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.description.setText(mTranslator.get(TranslationKey.PARENTAL_CONTROL_SETTINGS_LOCKED_CONTENT));
    }

    @Override
    protected void setTitle() {
        mBinding.title.setText(mTranslator.get(TranslationKey.PARENTAL_CONTROL_SETTINGS_LOCKED_CONTENT_TITLE));
    }

    @Override
    protected void initView() {
        List<LockContentItem> items = new ArrayList<>();
        items.add(new LockContentItem(mTranslator.get(
                TranslationKey.PARENTAL_CONTROL_SETTINGS_AGE_RATING_18_INFO), ParentalRating.Rated18));
        items.add(new LockContentItem(mTranslator.get(
                TranslationKey.PARENTAL_CONTROL_SETTINGS_AGE_RATING_16_INFO), ParentalRating.Rated16));
        items.add(new LockContentItem(mTranslator.get(
                TranslationKey.PARENTAL_CONTROL_SETTINGS_AGE_RATING_12_INFO), ParentalRating.Rated12));
        items.add(new LockContentItem(mTranslator.get(
                TranslationKey.PARENTAL_CONTROL_SETTINGS_AGE_RATING_10_INFO), ParentalRating.Rated10));
        setItems(items);
    }

    @Override
    @SuppressLint("NotifyDataSetChanged")
    public void checkLockContentRadioButton(LockContentItem checkedItem) {
         Disposable disposable = mParentalControlManager
                .selectParentalRating(checkedItem.getParentalRating())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    // Notify change.
                    RecyclerView.Adapter<?> adapter = mBinding.grid.getAdapter();
                    if (adapter != null) {
                        adapter.notifyDataSetChanged();
                    }

                    // Close the dialog.
                    dismissAllowingStateLoss();
                },
                e -> {
                    Log.d(TAG, "Age rating save failed!");
                    mNavigator.showFailedToSaveSettingsDialog();
                });
        mDisposableList.add(disposable);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        RxUtils.disposeSilently(mDisposableList);
    }
}
