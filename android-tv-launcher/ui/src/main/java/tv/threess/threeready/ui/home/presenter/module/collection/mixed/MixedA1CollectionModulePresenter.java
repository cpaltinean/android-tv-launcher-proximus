/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.home.presenter.module.collection.mixed;

import android.content.Context;

import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;

import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.pvr.model.IBaseRecordingSeries;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.vod.model.IBaseVodSeries;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.generic.presenter.BaseCardPresenter;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.home.presenter.card.app.EditorialA1CardPresenter;
import tv.threess.threeready.ui.home.view.ModularPageView;
import tv.threess.threeready.ui.pvr.presenter.SeriesRecordingA1CardPresenter;
import tv.threess.threeready.ui.tv.presenter.broadcast.BroadcastA1LandscapeCardPresenter;
import tv.threess.threeready.ui.tv.presenter.vod.VodA1LandscapeCardPresenter;
import tv.threess.threeready.ui.tv.presenter.vod.VodSeriesA1BigCardPresenter;

/**
 * Presenter used to display mixed type, A variant collections.
 * <p>
 * Created by Szilard on 6/21/2017.
 */
public class MixedA1CollectionModulePresenter extends BaseMixedCollectionModulePresenter {

    public MixedA1CollectionModulePresenter(Context context, ModularPageView.PageTitleCallback pageTitle, DataSourceLoader dataSourceLoader) {
        super(context, pageTitle, dataSourceLoader);

        InterfacePresenterSelector selector = new InterfacePresenterSelector()
                .addClassPresenterSelector(IContentItem.class, new MixedA1CollectionModulePresenter.CardPresenterSelector(
                        new BroadcastA1LandscapeCardPresenter(mContext),
                        new VodA1LandscapeCardPresenter(mContext)))
                .addClassPresenter(IBaseVodSeries.class, new VodSeriesA1BigCardPresenter(mContext))
                .addClassPresenter(IBaseRecordingSeries.class, new SeriesRecordingA1CardPresenter(mContext));
        selector.addClassPresenter(EditorialItem.class, new EditorialA1CardPresenter(context, selector));

        // Mixed A1 Collection
        mVariantCardPresenterMap.put(ModuleCardVariant.A1, selector);
    }

    @Override
    protected int getColumnNumber() {
        return 3;
    }

    @Override
    public void onBindHolder(ViewHolder holder, ModuleData<?> data) {
        super.onBindHolder(holder, data);
        int horizontalPadding = mContext.getResources().getDimensionPixelOffset(R.dimen.collection_mixed_horizontal_padding);
        int verticalPadding = mContext.getResources().getDimensionPixelOffset(R.dimen.mixed_collection_vertical_padding);
        holder.mBinding.collection.getCollectionGrid().setPadding(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);
    }

    private static class CardPresenterSelector extends PresenterSelector {

        private final BaseCardPresenter<?,?> mTvProgramLandscapeCardPresenter;
        private final BaseCardPresenter<?,?> mVodLandscapeCardPresenter;

        CardPresenterSelector(BaseCardPresenter<?,?> tvProgramLandscapeCardPresenter,
                              BaseCardPresenter<?,?> vodLandscapeCardPresenter) {
            mTvProgramLandscapeCardPresenter = tvProgramLandscapeCardPresenter;
            mVodLandscapeCardPresenter = vodLandscapeCardPresenter;
        }

        @Override
        public Presenter[] getPresenters() {
            return new Presenter[]{mTvProgramLandscapeCardPresenter, mVodLandscapeCardPresenter};
        }

        @Override
        public Presenter getPresenter(Object obj) {
            IContentItem item = (IContentItem) obj;

            if (item instanceof IBroadcast) {
                return mTvProgramLandscapeCardPresenter;
            } else {
                return mVodLandscapeCardPresenter;
            }
        }
    }
}
