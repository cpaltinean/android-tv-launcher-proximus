/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.ui.details.fragment;

import android.os.Bundle;
import android.view.KeyEvent;

import androidx.annotation.Nullable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.vod.VodRepository;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.player.plugin.ReportUtils;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.details.presenter.SeriesDetailsOverviewPresenter;
import tv.threess.threeready.ui.details.presenter.SeriesRowPresenter;
import tv.threess.threeready.ui.details.presenter.SeriesVodRowPresenter;
import tv.threess.threeready.ui.details.presenter.VodSeriesDetailsOverviewPresenter;
import tv.threess.threeready.ui.generic.dialog.BaseDialogFragment;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Displays vod series details
 *
 * @author Zsolt Bokor
 * @since 2019.01.08
 */

public class VodSeriesDetailsFragment extends BaseSeriesDetailFragment<IBaseVodItem, IVodSeries, VodSeriesDetailsOverviewPresenter.ViewHolder> {
    public static final String TAG = Log.tag(VodSeriesDetailsFragment.class);

    private static final String EXTRA_VOD_EPISODE = "episode";
    private static final String EXTRA_VOD_SERIES_ID = "series_id";

    private final VodRepository mVodRepository = Components.get(VodRepository.class);

    public static VodSeriesDetailsFragment newInstance(String seriesId) {
        VodSeriesDetailsFragment fragment = new VodSeriesDetailsFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_VOD_SERIES_ID, seriesId);
        fragment.setArguments(args);
        return fragment;
    }

    public static VodSeriesDetailsFragment newInstance(IBaseVodItem episode) {
        VodSeriesDetailsFragment fragment = new VodSeriesDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_VOD_EPISODE, episode);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSeriesId = getStringArgument(EXTRA_VOD_SERIES_ID);
        mEpisode = getSerializableArgument(EXTRA_VOD_EPISODE);
    }

    /**
     * Load and display series details for the episode.
     *
     * @param episode The episode for which the series details should be loaded.
     */
    @Override
    protected void loadSeriesDetails(final IBaseVodItem episode) {
        // Dispose channel list request
        RxUtils.disposeSilently(mDetailsDisposable);
        mDetailsDisposable = mVodRepository.getVodSeries(episode.getSeriesId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(model -> addUIData(model, episode),
                        throwable -> {
                            Log.e(TAG, "Failed to open series details for " + episode.getSeriesId(), throwable);
                            showErrorMessage();
                        });
    }

    /**
     * Load and display series details
     *
     * @param seriesId The id of the series for which the details should be loaded.
     */
    @Override
    protected void loadSeriesDetails(final String seriesId) {
        // Dispose channel list request
        RxUtils.disposeSilently(mDetailsDisposable);
        mDetailsDisposable = mVodRepository.getVodSeries(seriesId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(model -> addUIData(model, model.getLastPlayedEpisode()),
                        throwable -> {
                            Log.e(TAG, "Failed to open series details for " + seriesId, throwable);
                            showErrorMessage();
                        });
    }

    @Override
    protected void addUIData(IVodSeries series, IBaseVodItem selectedEpisode) {
        if (mParentalControlManager.isRestricted(series)) {
            // Locked content. Show parental control unblock dialog.
            mNavigator.showParentalControlUnblockDialog(new BaseDialogFragment.DialogListener() {
                @Override
                public void onKeyEvent(KeyEvent event, BaseDialogFragment dialog) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN
                            && event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                        // Close detail page on back press.
                        mNavigator.popBackStack();
                    }
                }

                @Override
                public void onDismiss(BaseDialogFragment dialog) {
                    // Reset the black foreground
                    resetModularPageViewForeground();
                }
            });
            super.addUIData(series, selectedEpisode);
            return;
        }
        // Reset the black foreground
        resetModularPageViewForeground();
        super.addUIData(series, selectedEpisode);
    }

    @Override
    protected SeriesDetailsOverviewPresenter<VodSeriesDetailsOverviewPresenter.ViewHolder, IBaseVodItem, IVodSeries> getOverviewPresenter(IVodSeries series) {
        return new VodSeriesDetailsOverviewPresenter(getContext(), series);
    }

    @Override
    protected SeriesRowPresenter<IBaseVodItem> getSeriesRowPresenter(IVodSeries series, IBaseVodItem selectedEpisode) {
        return new SeriesVodRowPresenter(
                getContext(),
                selectedEpisode == null ? series.getFirstEpisode() : selectedEpisode,
                this
        );
    }

    @Override
    protected void addExtraPresenter(InterfacePresenterSelector classPresenterSelector, IVodSeries series, IBaseVodItem selectedEpisode) {
        classPresenterSelector.addClassPresenter(IBaseVodItem.class, getOverviewPresenter(series));
        classPresenterSelector.addClassPresenter(IVodSeries.class, getSeriesRowPresenter(series, selectedEpisode));
    }

    @Override
    protected int getOverviewItemOffset(VodSeriesDetailsOverviewPresenter.ViewHolder viewHolder) {
        return getResources().getDimensionPixelOffset(R.dimen.series_detail_page_grid_row_top_margin);
    }

    /**
     * Reset the foreground of the modular page view.
     */
    private void resetModularPageViewForeground() {
        if (mModularPageView == null) {
            return;
        }
        mModularPageView.setForeground(null);
    }

    @Nullable
    @Override
    protected Event<?, ?> generatePageOpenEvent() {
        IBaseVodItem vod = mEpisode;

        if (mEpisode == null && mSeries != null) {
            vod = mSeries.getFirstEpisode();
        }

        if (vod == null) {
            return null;
        }

        return new Event<>(UILogEvent.PageOpen)
                .addDetail(UILogEvent.Detail.Page, UILog.Page.StoreSeriesFullContentPage)
                .addDetail(UILogEvent.Detail.PageName, ReportUtils.getPageName(vod))
                .addDetail(UILogEvent.Detail.PageId, ReportUtils.getPageId(vod));
    }
}
