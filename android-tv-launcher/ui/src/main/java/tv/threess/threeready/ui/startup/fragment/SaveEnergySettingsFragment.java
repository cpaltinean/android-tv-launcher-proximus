package tv.threess.threeready.ui.startup.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.ui.databinding.SaveEnergySettingsFragmentBinding;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Fragment for notifying the user about the energy saver features of the box.
 *
 * This fragment is a part of the First Time Install.
 *
 * @author Andor Lukacs
 * @since 7/6/18
 */

public class SaveEnergySettingsFragment extends StartFragment {

    private SaveEnergySettingsFragmentBinding mBinding;

    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);
    private final Navigator mNavigator = Components.get(Navigator.class);
    private final Translator mTranslator = Components.get(Translator.class);

    public static SaveEnergySettingsFragment newInstance(StepCallback stepCallback) {
        SaveEnergySettingsFragment fragment = new SaveEnergySettingsFragment();
        fragment.setStepCallback(stepCallback);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mBinding = SaveEnergySettingsFragmentBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateLayout();
        mStepCallback.getStartupTimer().pause();
    }

    @Override
    public boolean onBackPressed() {
        //Do nothing on back press
        return true;
    }

    @Override
    public void updateLayout() {
        mBinding.continueButton.setOnClickListener(button -> {
            mNavigator.popStartFragment();
            mStepCallback.getStartupTimer().resume();
            mStepCallback.onFinished();
        });

        mBinding.continueButton.setBackground(UiUtils.createFtiButtonBackground(getContext(), mLayoutConfig));
        mBinding.continueButton.setTextColor(mLayoutConfig.getButtonFocusedFontColor());

        mBinding.title.setTextColor(mLayoutConfig.getFontColor());
        mBinding.description.setTextColor(ColorUtils.applyTransparencyToColor(mLayoutConfig.getFontColor(), mLayoutConfig.ALPHA_PLACEHOLDER_FONT));
        mBinding.continueButton.setTextColor(mLayoutConfig.getButtonFocusedFontColor());


        mBinding.title.setText(mTranslator.get(TranslationKey.FTI_SAVE_ENERGY_TITLE));
        mBinding.description.setText(mTranslator.get(TranslationKey.FTI_SAVE_ENERGY_BODY));
        mBinding.continueButton.setText(mTranslator.get(TranslationKey.FTI_SAVE_ENERGY_BUTTON_CONTINUE));
    }
}
