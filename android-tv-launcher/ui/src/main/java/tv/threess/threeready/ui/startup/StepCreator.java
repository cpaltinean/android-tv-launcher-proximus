package tv.threess.threeready.ui.startup;

import java.util.Queue;

import tv.threess.threeready.api.account.model.service.ConsentRequestScenario;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.startup.IStepCreator;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepQueueBuilder;
import tv.threess.threeready.ui.startup.step.BootAnimationFinishStep;
import tv.threess.threeready.ui.startup.step.CachingStep;
import tv.threess.threeready.ui.startup.step.CheckAuthenticationStep;
import tv.threess.threeready.ui.startup.step.CheckBackendStep;
import tv.threess.threeready.ui.startup.step.CheckInternetStep;
import tv.threess.threeready.ui.startup.step.ConsentsStep;
import tv.threess.threeready.ui.startup.step.DeviceRegistrationStep;
import tv.threess.threeready.ui.startup.step.FtiCompletedStep;
import tv.threess.threeready.ui.startup.step.InitPlayerStep;
import tv.threess.threeready.ui.startup.step.LoadFontConfigStep;
import tv.threess.threeready.ui.startup.step.ParentalControlFTISetupStep;
import tv.threess.threeready.ui.startup.step.ParentalControlSetupStep;
import tv.threess.threeready.ui.startup.step.SaveEnergyStep;
import tv.threess.threeready.ui.startup.step.StandByCompletedStep;
import tv.threess.threeready.ui.startup.step.StartupCompletedStep;
import tv.threess.threeready.ui.startup.step.SystemUpdateCheckStep;
import tv.threess.threeready.ui.startup.step.TuneToLastChannelStep;
import tv.threess.threeready.ui.startup.step.UpdateBootConfigStep;
import tv.threess.threeready.ui.startup.step.WaitForHDCPStep;
import tv.threess.threeready.ui.startup.step.WaitStartupStep;
import tv.threess.threeready.ui.startup.step.WelcomeScreenStep;

/**
 * Create proximus specific startup steps.
 *
 * @author Barabas Attila
 * @since 2020.09.08
 */
public class StepCreator implements IStepCreator {

    @Override
    public Queue<Step> getStartupSteps() {
        return new StepQueueBuilder()
                .addStep(new LoadFontConfigStep())      // cached
                .addStep(new BootAnimationFinishStep())
                .addStep(new WelcomeScreenStep())
                .addStep(new WaitForHDCPStep())
                .addStep(new CheckInternetStep())
                .addStep(new SystemUpdateCheckStep())
                .addStep(new UpdateBootConfigStep())
                .addStep(new CheckBackendStep())
                .addStep(new DeviceRegistrationStep())
                .addStep(new CachingStep())
                .addStep(new CheckAuthenticationStep())
                .addStep(new ParentalControlSetupStep())
                .addStep(new WaitStartupStep())
                .addStep(new InitPlayerStep())
                .addStep(new ConsentsStep())
                .addStep(new StartupCompletedStep())
                .build();
    }

    @Override
    public Queue<Step> getFTISteps() {
        return new StepQueueBuilder()
                .addStep(new LoadFontConfigStep())
                .addStep(new BootAnimationFinishStep())
                .addStep(new WelcomeScreenStep())
                .addStep(new WaitForHDCPStep())
                .addStep(new CheckInternetStep())
                .addStep(new SystemUpdateCheckStep())
                .addStep(new UpdateBootConfigStep())
                .addStep(new CheckBackendStep())
                .addStep(new DeviceRegistrationStep())
                .addStep(new CachingStep())
                .addStep(new CheckAuthenticationStep())
                .addStep(new ParentalControlFTISetupStep())
                .addStep(new WaitStartupStep())
                .addStep(new InitPlayerStep())
                .addStep(new ConsentsStep(ConsentRequestScenario.FIRST_TIME_INSTALL))
                .addStep(new SaveEnergyStep())
                .addStep(new WelcomeScreenStep())
                .addStep(new FtiCompletedStep())
                .build();
    }

    @Override
    public Queue<Step> getStandbySteps() {
        return new StepQueueBuilder()
                .addStep(new WelcomeScreenStep())
                .addStep(new CheckInternetStep())
                .addStep(new CheckBackendStep())
                .addStep(new WaitForHDCPStep())
                .addStep(new CachingStep())
                .addStep(new ConsentsStep())
                .addStep(new WaitStartupStep())
                .addStep(new TuneToLastChannelStep(StartAction.StandBy))
                .addStep(new StandByCompletedStep())
                .build();
    }

    @Override
    public Queue<Step> getOfflineSteps() {
        return new StepQueueBuilder()
                .addStep(new LoadFontConfigStep())
                .addStep(new WelcomeScreenStep())
                .addStep(new InitPlayerStep())
                .addStep(new ParentalControlSetupStep())
                .addStep(new StartupCompletedStep())
                .build();
    }
}
