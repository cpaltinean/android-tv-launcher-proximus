package tv.threess.threeready.ui.generic.view;

import android.content.Context;
import android.util.AttributeSet;

import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.ui.utils.UiUtils;

/**
 * Custom view to display the icons for a parental rating.
 *
 * @author Eugen Guzyk
 * @since 2018.07.11
 */
public class ParentalRatingView extends StateImageView {

    public ParentalRatingView(Context context) {
        super(context);
    }

    public ParentalRatingView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ParentalRatingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setRating(ParentalRating rating) {
        if (rating == null) {
            resetImage();
            return;
        }

        int parentalRatingIconRef = UiUtils.getParentalRatingIcon(rating);

        if (parentalRatingIconRef != 0) {
            setImage(parentalRatingIconRef);
        } else {
            resetImage();
        }
    }
}
