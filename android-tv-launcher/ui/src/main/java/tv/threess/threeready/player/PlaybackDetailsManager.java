/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.player;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.FeatureControl;
import tv.threess.threeready.api.config.model.PlayerConfig;
import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.generic.model.BingeWatchingInfo;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.netflix.model.CategoryType;
import tv.threess.threeready.api.netflix.model.SourceType;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.playback.player.PlaybackDomain;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.AppChannelInfo;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.api.tv.model.ZappingInfo;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodSeries;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.player.command.InvalidStartCommand;
import tv.threess.threeready.player.command.base.PlaybackCommand;
import tv.threess.threeready.player.command.base.StartCommand;
import tv.threess.threeready.player.command.start.BroadcastStartCommand;
import tv.threess.threeready.player.command.start.ContentItemStartCommand;
import tv.threess.threeready.player.command.start.GdprStartCommand;
import tv.threess.threeready.player.command.start.IptvLiveStartCommand;
import tv.threess.threeready.player.command.start.RecordingStartCommand;
import tv.threess.threeready.player.command.start.ReplayStartCommand;
import tv.threess.threeready.player.command.start.TrailerStartCommand;
import tv.threess.threeready.player.command.start.VodStartCommand;
import tv.threess.threeready.player.contract.PlaybackState;
import tv.threess.threeready.player.contract.PlaybackType;
import tv.threess.threeready.player.contract.SeekDirection;
import tv.threess.threeready.player.control.ControlFactory;
import tv.threess.threeready.player.control.base.PlaybackControl;
import tv.threess.threeready.player.model.MediaTrackInfo;
import tv.threess.threeready.player.model.PlaybackDetails;

/**
 * The manager class for all playback management components.
 * This class creates bundle for PlaybackManager, and only in this class we can add/get data from bundle.
 *
 * @author Szende Pal
 * @since 2017.09.12
 */
public class PlaybackDetailsManager extends PlaybackManager implements IPlaybackDetailsCallback, SeriesPlaybackDetailsCallback, Component {
    private static final String TAG = Log.tag(PlaybackDetailsManager.class);

    /*
     * Number of seconds which are ignored while seeking to live.
     * If number of seconds to live stream is less than this value - the live stream will start.
     */
    private static final int SECONDS_TO_LIVE = 10;

    //The start command of the current playback.
    //NOTE: we modify this command, so it's not the exact one that was started
    @Nullable
    private StartCommand mStartCommand;

    // This flag is used to block the trick-play if a warning dialog is displayed.
    private boolean mIsTrickPlayBlocked = false;

    /**
     * Next episode for binge-watching.
     */
    private BingeWatchingInfo<?, ?> mNextEpisode;

    private final ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);

    /**
     * Default constructor
     */
    public PlaybackDetailsManager(WeakReference<Context> context,
                                  @NonNull ControlFactory controlFactory, @NonNull CommandFactory commandFactory) {
        super(context, controlFactory, commandFactory);
    }

    public PlaybackType getPlayerType() {
        StartCommand startCommand = mStartCommand;
        if (startCommand == null) {
            return PlaybackType.Default;
        }

        return startCommand.getType();
    }


    ///////////////////
    /// Live playback
    //////////////////


    public void startChannel(StartAction action, String channelId, RestartMode restartMode) {
        startChannel(action, channelId, true, restartMode);
    }

    /***
     * Start the live playback on the given channel.
     *
     * @param action The place from where the playback start was triggered. E.g. EPG, Detail page etc.
     * @param channelId The id of the channel on which should be started.
     * @param isFavorite True if the playback was started from favorites.
     * @param restartMode The restart strategy if the playback on the given channel is already playing.
     */
    public void startChannel(StartAction action, String channelId, boolean isFavorite, RestartMode restartMode) {
        ZappingInfo zappingInfo = mNowOnTvCache.getZappingInfo(channelId, isFavorite);

        if (zappingInfo == null) {
            long cid = nextCommandId();
            PlaybackControl<?> playbackControl = controller.getControl(PlaybackDomain.None);
            offerCommand(new InvalidStartCommand(cid, PlaybackType.Default, playbackControl, action, channelId));
            return;
        }

        startChannel(action, zappingInfo, restartMode);
    }

    /***
     * Start the live playback on the given channel.
     *
     * @param action The place from where the playback start was triggered. E.g. EPG, Detail page etc.
     * @param channel The channel on which the playback should be started.
     * @param isFavorite True if the playback was started from favorites.
     * @param restartMode The restart strategy if the playback on the given channel is already playing.
     */
    public void startChannel(StartAction action, TvChannel channel, boolean isFavorite, RestartMode restartMode) {
        ZappingInfo zappingInfo = mNowOnTvCache.getZappingInfo(channel.getId(), isFavorite);

        if (zappingInfo == null) {
            long cid = nextCommandId();
            PlaybackControl<?> playbackControl = controller.getControl(PlaybackDomain.None);
            offerCommand(new InvalidStartCommand(cid, PlaybackType.Default, playbackControl, action, channel.getId()));
            return;
        }

        startChannel(action, zappingInfo, restartMode);
    }

    /***
     * Start the live playback on the given channel.
     *
     * @param action The place from where the playback start was triggered. E.g. EPG, Detail page etc.
     * @param zappingInfo The zapping info which holds the channel and the currently running program on it.
     * @param restartMode The restart strategy if the playback on the given channel is already playing.
     */
    public void startChannel(StartAction action, @NonNull ZappingInfo zappingInfo, RestartMode restartMode) {
        Log.d(TAG, "startChannel() called with: zappingInfo = ["
                + zappingInfo + "], restartMode= [" + restartMode + "]");

        if (!needsRestart(restartMode, zappingInfo.getChannel().getId())) {
            // Channel already playing. Nothing to do.
            if (mStartCommand instanceof IptvLiveStartCommand) {
                ((IptvLiveStartCommand) mStartCommand).setIsFromFavorites(zappingInfo.isFromFavorites());
            }

            Log.d(TAG, "Skipp channel start, already playing. Zapping info : " + zappingInfo);
            return;
        }

        saveLastPlayedChannelNumber(zappingInfo.getChannel().getId(), zappingInfo.getChannel().getType());
        StartCommand command = super.startChannel(action, zappingInfo.getChannel(), zappingInfo.getBroadcast());
        onPlayerDataUpdate(zappingInfo.getBroadcast());

        ((IptvLiveStartCommand) command).setIsFromFavorites(zappingInfo.isFromFavorites());
    }

    ////////////////////////
    /// Start replay.
    ///////////////////////

    /**
     * Start replay stream on the given broadcast.
     *
     * @param action       The place from where the playback start was triggered. E.g. EPG, Detail page etc.
     * @param broadcast    The broadcast on which the replay stream should be started.
     * @param startOver    True if the stream should start from the beginning,
     *                     otherwise the last bookmark position will be used.
     * @param forceRestart True if the playback should be restarted even if it is already playing.
     * @param isFromFavourite Flag used to know if the channel id is from favourite channels list.
     */
    public void startReplay(StartAction action, IBroadcast broadcast, boolean forceRestart, boolean startOver, boolean isFromFavourite) {
        startReplay(action, broadcast, forceRestart, startOver ? 0L : null, isFromFavourite);
    }

    /**
     * Start replay stream on the given broadcast.
     *
     * @param action       The place from where the playback start was triggered. E.g. EPG, Detail page etc.
     * @param broadcast    The broadcast on which the replay stream should be started,
     *                     otherwise the last bookmark position will be used.
     * @param forceRestart True if the playback should be restarted even if it is already playing.
     * @param position     The position in milliseconds relative to the beginning of the stream
     *                     from where the playback should be started.
     */
    public void startReplay(StartAction action, IBroadcast broadcast, boolean forceRestart, Long position) {
        startReplay(action, broadcast, forceRestart, position, false);
    }

    /**
     * Start replay stream on the given broadcast.
     *
     * @param action       The place from where the playback start was triggered. E.g. EPG, Detail page etc.
     * @param broadcast    The broadcast on which the replay stream should be started,
     *                     otherwise the last bookmark position will be used.
     * @param forceRestart True if the playback should be restarted even if it is already playing.
     * @param position     The position in milliseconds relative to the beginning of the stream
     *                     from where the playback should be started.
     * @param isFromFavourite Flag used to know if the channel id is from favourite channels list.
     */
    public void startReplay(StartAction action, IBroadcast broadcast, boolean forceRestart, Long position, boolean isFromFavourite) {
        Log.d(TAG, "startReplay() called with: broadcast = [" + broadcast + "], " +
                "forceRestart = [" + forceRestart + "], position = [" + position + "]");

        if (!forceRestart && isReplayPlaying(broadcast.getId())) {
            return;
        }

        saveLastPlayedChannelNumber(broadcast.getChannelId(), ChannelType.TV);
        super.startReplay(action, broadcast, position, isFromFavourite);
    }

    //////////////////////
    /// Start recording
    /////////////////////

    /**
     * Start recording playback for the broadcast.
     * The playback will fail if the broadcast doesn't have a completed or ongoing recording.
     *
     * @param action          The place from where the playback start was triggered. E.g. EPG, Detail page etc.
     * @param recording       The recording which should be started.
     * @param recording       The series of the started recording.
     * @param startOver       True if the playback should start from the beginning,
     *                        false to start the playback from the bookmarked position.
     * @param forceRestart    True if the playback should be started even if the recording is currently playing.
     */
    public void startRecording(StartAction action, IRecording recording, IRecordingSeries series,
                               boolean startOver, boolean forceRestart) {
        if (!forceRestart && isRecordingPlaying(recording.getId())) {
            return;
        }

        Long position = null;
        if (startOver) {
            // Start of the stream for completed recordings: 0, for live recordings: 1
            position = recording.isLiveRecording() ? 1L : 0L;
        }
        super.startRecording(action, recording, series, position);
    }

    /////////////////
    // Start VOD playback.
    /////////////////


    /**
     * Start the playback for the VOD.
     *
     * @param action       The place from where the playback start was triggered. E.g. EPG, Detail page etc.
     * @param vod          The VOD for which the playback should be started.
     * @param vodPrice     The VOD price.
     * @param vodVariant   The VOD variant for which the playback should be started.
     * @param vodSeries   The VOD series of the started vod item.
     * @param startOver    True if the stream should start from the beginning,
     *                     otherwise the last bookmark position will be used.
     * @param forceRestart True if the playback should be restarted even if it is already playing.
     */
    public void startVod(StartAction action, IVodItem vod, IVodPrice vodPrice, IVodVariant vodVariant, IVodSeries vodSeries, boolean startOver, boolean forceRestart) {
        if (!forceRestart && isVodPlaying(vodVariant)) {
            // Already playing.
            return;
        }

        super.startVod(action, vod, vodPrice, vodVariant, vodSeries, startOver ? 0L : null);
    }

    /**
     * Start the playback for the VOD.
     *
     * @param action       The place from where the playback start was triggered. E.g. EPG, Detail page etc.
     * @param vod          The vod for which the trailer playback should be started.
     * @param vodVariant   The vod variant for which the trailer playback should be started.
     * @param forceRestart True if the playback should be restarted even if it is already playing.
     */
    public void startTrailer(StartAction action, IVodItem vod, IVodVariant vodVariant, boolean forceRestart) {
        if (!forceRestart && isTrailerPlaying(vodVariant)) {
            return;
        }

        super.startTrailer(action, vod, vodVariant);
    }

    public void startGpdr(StartAction action, IVodItem vod, IVodPrice vodPrice, IVodVariant vodVariant, boolean forceRestart) {
        if (!forceRestart && isGdprVideoPlaying(vodVariant)) {
            return;
        }
        super.startGdpr(action, vod, vodPrice, vodVariant);
    }

    ////////////
    // Start application
    ///////////

    /**
     * @param action      The place from where the application start was triggered. E.g. EPG, Detail page etc.
     * @param packageName The package name of the application which should be opened.
     * @param args        Extra arguments which will be included in the start intent.
     */
    public void startApplication(StartAction action, String packageName, Bundle args) {
        super.startApplication(action, packageName, "", args);
    }

    /**
     * @param action      The place from where the application start was triggered. E.g. EPG, Detail page etc.
     * @param packageName The package name of the application which should be opened.
     * @param intent      The intent which will be start the application
     */
    public void startApplication(StartAction action, String packageName, Intent intent) {
        super.startApplication(action, packageName, intent, Bundle.EMPTY);
    }

    /**
     * @param action      The place from where the application start was triggered. E.g. EPG, Detail page etc.
     * @param packageName The package name of the application which should be opened.
     *                    <p>
     *                    // TODO : check if we can map it with the {@link StartAction} and use only one.
     * @param sourceType  The place from where the application start was triggered. Used only for Netflix.
     */
    public void startApplication(StartAction action, String packageName, SourceType sourceType) {
        startApplication(action, packageName, "", sourceType);
    }

    /**
     * @param action       The place from where the application start was triggered. E.g. EPG, Detail page etc.
     * @param packageName  The package name of the application which should be opened.
     * @param activityName The name of the activity which will handle the start intent.
     *                     <p>
     *                     // TODO : check if we can map it with the {@link StartAction} and use only one.
     * @param sourceType   The place from where the application start was triggered. Used only for Netflix.
     */
    public void startApplication(StartAction action, String packageName, String activityName, SourceType sourceType) {
        // Open netflix.
        if (PackageUtils.PACKAGE_NAME_NETFLIX.equals(packageName)) {
            Uri deepLinkUri = Uri.parse(PackageUtils.NETFLIX_BROWSE_URL).buildUpon()
                    .appendQueryParameter(PackageUtils.NETFLIX_SOURCE_TYPE_PARAM, String.valueOf(sourceType.getCode()))
                    .build();

            Intent intent = new Intent(Intent.ACTION_VIEW, deepLinkUri);
            super.startApplication(action, packageName, intent, Bundle.EMPTY);
        } else {
            super.startApplication(action, packageName, activityName, Bundle.EMPTY);
        }
    }

    /**
     * @param action         The place from where the application start was triggered. E.g. EPG, Detail page etc.
     * @param appChannelInfo The object which holds the package name and other extra information about the app channel.
     *                       <p>
     *                       // TODO : check if we can map it with the {@link StartAction} and use only one.
     * @param sourceType     The place from where the application start was triggered. Used only for Netflix.
     */

    public void startApplication(StartAction action, AppChannelInfo appChannelInfo, SourceType sourceType) {
        if (!PackageUtils.PACKAGE_NAME_NETFLIX.equals(appChannelInfo.packageName)) {
            startApplication(action, appChannelInfo.packageName, sourceType);
            return;
        }

        Uri payloadUri = Uri.EMPTY.buildUpon()
                // Send 0 as channel number, since it's not displayed in app.
                .appendQueryParameter(PackageUtils.NETFLIX_CHANNEL_NUMBER_PARAM, String.valueOf(appChannelInfo.channelNumber))
                .appendQueryParameter(PackageUtils.NETFLIX_PREV_CHANNEL_NUMBER_PARAM, String.valueOf(appChannelInfo.prevChannelNumber))
                .appendQueryParameter(PackageUtils.NETFLIX_NEXT_CHANNEL_NUMBER_PARAM, String.valueOf(appChannelInfo.nextChannelNumber))

                // At the moment set other category for all channel.
                .appendQueryParameter(PackageUtils.NETFLIX_CHANNEL_CATEGORY_TYPE_PARAM, String.valueOf(CategoryType.OTHER))
                .build();

        // Add source type.
        Uri.Builder deepLinkBuilder = Uri.parse(PackageUtils.NETFLIX_BROWSE_URL).buildUpon()
                .appendQueryParameter(PackageUtils.NETFLIX_SOURCE_TYPE_PARAM, String.valueOf(sourceType.getCode()));

        // Append with payload params
        String payload = payloadUri.getEncodedQuery();
        if (!TextUtils.isEmpty(payload)) {
            deepLinkBuilder.appendQueryParameter(PackageUtils.NETFLIX_SOURCE_TYPE_PLAY_LOAD_PARAM, payload);
        }


        Intent intent = new Intent(Intent.ACTION_VIEW, deepLinkBuilder.build());
        intent.putExtra(PackageUtils.NETFLIX_MINI_EPG_EXTRA, true);
        startApplication(action, PackageUtils.PACKAGE_NAME_NETFLIX, intent);
    }

    @Override
    public void offerCommand(PlaybackCommand... commands) {
        // Save last content start command to read meta info from.
        for (PlaybackCommand command : commands) {
            if (command instanceof ContentItemStartCommand) {
                mStartCommand = (StartCommand) command;
            }
        }

        super.offerCommand(commands);
    }

    public void setTrickPlayBlocked(boolean isBlocked) {
        Log.d(TAG, "setTrickPlayBlocked() called with value: " + isBlocked);
        mIsTrickPlayBlocked = isBlocked;
    }

    public boolean isTrickPlayAvailable() {
        return Components.get(FeatureControl.class).isTrickplayEnabled() && !mIsTrickPlayBlocked
                && !mParentalControlManager.isRestricted(getPlayingItem(), true)
                && getActiveExclusiveDomain() != null &&
                !inPlaybackDomain(PlaybackDomain.Trailer, PlaybackDomain.Gdpr);
    }
    public void setNextEpisode(BingeWatchingInfo<?, ?> nextEpisode) {
        mNextEpisode = nextEpisode;
        onNextEpisodeLoaded();
    }

    @Nullable
    public BingeWatchingInfo<?, ?> getNextEpisode() {
        return mNextEpisode;
    }

    /**
     * Starts the trick play with a replay transaction, if needed.
     * If the user is in live playback, in order to start trick play you need to first start replay, then start trick play.
     */
    public void startTrickPlay(float playbackSpeed) {
        PlaybackDomain activeDomain = mStartCommand != null ? mStartCommand.getDomain() : null;
        if (activeDomain == null) {
            Log.d(TAG, "There is no active domain currently, trick play is not possible.");
            return;
        }

        switch (activeDomain) {
            case Trailer:
                // Trick play is disabled for trailer.
                break;
            case LiveTvOtt:
                startReplay(StartAction.TrickPlay, getLivePlayerData(), false,
                        getExclusiveDetails().getRelativePosition(), isFavorite());
                super.startTrickPlay(StartAction.TrickPlay, playbackSpeed, PlaybackDomain.Replay);
                break;
            default:
                super.startTrickPlay(StartAction.TrickPlay, playbackSpeed, activeDomain);
                break;
        }
    }

    /**
     * Stops the trick play.
     */
    public void stopTrickPlay() {
        PlaybackDomain activeDomain = mStartCommand != null ? mStartCommand.getDomain() : null;
        if (activeDomain == null) {
            Log.d(TAG, "There is no active domain currently, could not stop trick play.");
            return;
        }
        super.stopTrickPlay(activeDomain);
    }

    /**
     * Handle seeking between broadcast in the EPG.
     * The playback speed and position needs to be adjusted.
     *
     * @param playbackSpeed The playback speed of the new broadcast.
     * @param jumpPosition  The initial position in the broadcast.
     */
    public void handleChangeBroadcast(float playbackSpeed, long jumpPosition) {
        validatePosition(jumpPosition);

        PlaybackControl<?> trickPlayControl = controller.getControl(PlaybackDomain.TrickPlay);
        if (inTrickPlayState() || trickPlayControl.getCurrentState().active) {
            startTrickPlay(playbackSpeed);
        }
    }

    @Override
    public long pause() {
        PlaybackDetails details = getExclusiveDetails();
        IBroadcast livePlayerData = getLivePlayerData();
        if (details.getDomain() == PlaybackDomain.LiveTvOtt && livePlayerData != null) {
            startReplay(StartAction.Pause, livePlayerData, false, details.getRelativePosition(), isFavorite());
        }
        return super.pause();
    }

    public boolean resumePause() {
        if (!canPause()) {
            return false;
        }

        if(inPlaybackState(PlaybackState.Started)){
            pause();
        }else{
            resume();
        }
        return true;
    }

    @Override
    public void jumpByDuration(StartAction startAction, long duration) {
        if (isRegionalChannelPlaying()) {
            return;
        }

        if (!isLivePlaybackType() && !inPlaybackType(PlaybackType.Replay)) {
            super.jumpByDuration(startAction, duration);
            return;
        }

        IBroadcast livePlayerData = getLivePlayerData();
        PlaybackDetails playbackDetails = getExclusiveDetails();
        if (livePlayerData == null) {
            Log.d(TAG, "no live player data, jumping millis: [" + duration + "]");
            super.jumpByDuration(startAction, duration);
            return;
        }

        // Switch to replay stream.
        if (inPlaybackType(PlaybackType.LiveTvOtt)) {
            if (duration >= 0) {
                // do not allow jump into the future
                return;
            }
            startReplay(StartAction.Rewind, livePlayerData, true,
                    Math.max(0, playbackDetails.getRelativePosition() + duration), isFavorite());

            // Switch to live stream
        } else if (inPlaybackType(PlaybackType.Replay)) {
            final long currentPosition = playbackDetails.getPosition();

            // Check if seeking is out of bounds
            if (currentPosition + duration >= playbackDetails.getSeekAbleEnd() - TimeUnit.SECONDS.toMillis(SECONDS_TO_LIVE)
                    && playbackDetails.getState() != PlaybackState.Paused) {
                // Switch to live stream.
                startChannel(StartAction.Forward, livePlayerData.getChannelId(), isFavorite(), RestartMode.Forced);
            } else {
                super.jumpByDuration(startAction, duration);
            }

            // Normalize jump position for the time-shift buffer.
        } else if (inPlaybackType(PlaybackType.LiveTvIpTv)) {
            super.jumpByDuration(startAction, duration);
        }
    }

    @Override
    public void jumpByPosition(StartAction startAction, long position) {
        if (isRegionalChannelPlaying() || isAppChannelPlaying()) {
            return;
        }

        if (!isLivePlaybackType() && !inPlaybackType(PlaybackType.Replay)) {
            super.jumpByPosition(startAction, position);
            return;
        }

        IBroadcast livePlayerData = getLivePlayerData();
        PlaybackDetails playbackDetails = getExclusiveDetails();

        if (livePlayerData == null) {
            Log.d(TAG, "no live player data, jumping millis: [" + position + "]");
            super.jumpByPosition(startAction, position);
            return;
        }

        // Switch to replay stream.
        if (inPlaybackType(PlaybackType.LiveTvOtt)) {
            if (position >= playbackDetails.getSeekAbleEnd()) {
                // do not allow jump into the future
                return;
            }
            startReplay(StartAction.Rewind, livePlayerData, true, Math.max(0,
                    position - livePlayerData.getStart()), isFavorite());

            // Switch to live stream
        } else if (inPlaybackType(PlaybackType.Replay)) {
            // Check if seeking is out of bounds
            if (position >= System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(SECONDS_TO_LIVE)
                    && playbackDetails.getState() != PlaybackState.Paused) {
                // Switch to live stream.
                startChannel(StartAction.Forward, livePlayerData.getChannelId(), isFavorite(), RestartMode.Forced);
            } else {
                super.jumpByPosition(startAction, position);
            }

            // Normalize jump position for the time-shift buffer.
        } else if (inPlaybackType(PlaybackType.LiveTvIpTv)) {
            super.jumpByPosition(startAction, position);
        }
    }

    /**
     * Jump to live and resume the playback if the state is paused
     */
    public void jumpToLive(StartAction action) {
        // absolute jump to live to prevent the loss of time-shift buffer
        if (inPlaybackType(PlaybackType.LiveTvIpTv)) {
            if (inPlaybackState(PlaybackState.Paused)) {
                resume();
            }
            jumpByPosition(action, Long.MAX_VALUE);
            return;
        }
        startChannel(action, getChannelId(), isFavorite(), RestartMode.LiveChannel);
    }

    /**
     * Returns true if current playback is live playback type
     */
    public boolean isLivePlaybackType() {
        return inPlaybackType(
                PlaybackType.LiveTvDvbC,
                PlaybackType.LiveTvDvbT,
                PlaybackType.LiveTvIpTv,
                PlaybackType.LiveTvOtt,
                PlaybackType.RadioIpTv,
                PlaybackType.RadioOtt);
    }

    /**
     * Returns true if the currently running program is in the PLTV buffer.
     */
    public boolean isInPLTVBuffer() {
        PlaybackDetails details = getExclusiveDetails();
        return details.getType() == PlaybackType.LiveTvIpTv && !details.isLive() && details.getBufferEnd() != details.getBufferStart();
    }

    /**
     * Returns true if current playback is app playback type
     */
    public boolean isAppPlaybackType() {
        return inPlaybackType(PlaybackType.App);
    }

    private boolean needsRestart(RestartMode restartMode, String channelId) {
        switch (restartMode) {
            case LiveChannel:
                return !isLiveChannelPlaying(channelId);

            case Channel:
                return !isChannelPlaying(channelId);

            case Forced:
            default:
                return true;
        }
    }

    /**
     * Returns true if the channel is currently playing at the live position.
     */
    public boolean isLiveChannelPlaying(String channelId) {
        Log.d(TAG, "isLiveChannelPlaying() called with: channelId = [" + channelId + "], player type : " + getPlayerType());

        if (!isCommandStarting(StartCommand.class)
                && mStartCommand == getLastFailedCommand()) {
            // Last command failed. Try again.
            return false;
        }

        PlaybackType playbackType = getPlayerType();
        if (playbackType == PlaybackType.LiveTvOtt
                || playbackType == PlaybackType.LiveTvIpTv
                || playbackType == PlaybackType.LiveTvDvbC
                || playbackType == PlaybackType.LiveTvDvbT
                || playbackType == PlaybackType.RadioIpTv
                || playbackType == PlaybackType.RadioOtt) {

            return Objects.equals(channelId, getChannelId());
        }

        return false;
    }

    /**
     * Returns true if the channel is currently playing.
     */
    public boolean isChannelPlaying(String channelId) {
        Log.d(TAG, "isChannelPlaying() called with: channelId = [" + channelId + "], player type : " + getPlayerType() );

        if (!isCommandStarting(StartCommand.class)
                && mStartCommand == getLastFailedCommand()) {
            // Last command failed. Try again.
            return false;
        }

        PlaybackType playbackType = getPlayerType();
        if (playbackType == PlaybackType.LiveTvOtt
                || playbackType == PlaybackType.LiveTvIpTv
                || playbackType == PlaybackType.LiveTvDvbC
                || playbackType == PlaybackType.LiveTvDvbT
                || playbackType == PlaybackType.RadioIpTv
                || playbackType == PlaybackType.RadioOtt
                || playbackType == PlaybackType.Replay) {

            IBroadcast broadcast = getBroadcastPlayerData();
            return Objects.equals(channelId, getChannelId())
                    && broadcast != null && broadcast.isLive() ;
        }

        return false;
    }

    /**
     * Returns true if the live or catchup broadcast is currently playing.
     */
    public boolean isBroadcastPlaying(IBroadcast broadcast) {
        if (broadcast == null) {
            return false;
        }

        switch (getPlayerType()) {
            case LiveTvOtt:
            case LiveTvIpTv:
            case LiveTvDvbC:
            case LiveTvDvbT:
            case RadioIpTv:
                return isLivePlaying(broadcast);

            case Replay:
                return isReplayPlaying(broadcast.getId());

            case Recording:
                return isRecordingPlaying(broadcast.getId());

            default:
                return false;
        }
    }

    /**
     * Returns true if the tv or vod event is currently playing from the category.
     */
    public boolean isContentItemPlaying(IContentItem contentItem) {
        switch (getPlayerType()) {
            case RadioIpTv:
            case RadioOtt:
            case LiveTvOtt:
            case LiveTvIpTv:
            case LiveTvDvbC:
            case LiveTvDvbT:
                if (contentItem instanceof IBroadcast) {
                    IBroadcast liveBroadcast = (IBroadcast) contentItem;
                    return isLivePlaying(liveBroadcast);
                }
                return false;


            case Replay:
                IBroadcast replayBroadcast = getReplayPlayerData();

                if (replayBroadcast != null && contentItem instanceof IBroadcast) {
                    IBroadcast linearCategoryItem = (IBroadcast) contentItem;
                    return replayBroadcast.getId() != null
                            && replayBroadcast.getId().equals(linearCategoryItem.getId());

                }
                return false;

            case Recording:
                IBroadcast recordingBroadcast = getRecordingPlayerData();
                if (recordingBroadcast != null && contentItem instanceof IBroadcast) {
                    IBroadcast linearCategoryItem = (IBroadcast) contentItem;
                    return recordingBroadcast.getId() != null
                            && recordingBroadcast.getId().equals(linearCategoryItem.getId());

                }
                return false;

            case Vod:
            case VodTrailer:
                if (contentItem instanceof IBaseVodItem) {
                    IBaseVodItem item = (IBaseVodItem) contentItem;

                    IVodVariant variant = getVodVariantPlayerData();
                    if (variant != null) {
                        // a vod is currently playing, if one of its variants is playing
                        return item.getVariantIds().contains(variant.getId());
                    }
                }

                if (contentItem instanceof IVodVariant) {
                    IVodVariant variant = getVodVariantPlayerData();
                    if (variant != null) {
                        // a vod variant is currently playing, if that variants is playing
                        return contentItem.getId().equals(variant.getId());
                    }
                }

                IVodItem vod = getVodPlayerData();
                return vod != null && vod.getId() != null
                        && vod.getId().equals(contentItem.getId());
        }
        return false;
    }

    /**
     * Returns true if the given broadcast is currently playing.
     */
    public boolean isLivePlaying(IBroadcast broadcast) {
        PlaybackState state = getExclusiveDetails().getState();
        if (state != null && !state.active) {
            return false;
        }

        IBroadcast playerData = getLivePlayerData();
        return playerData != null && broadcast != null
                && Objects.equals(playerData.getId(), broadcast.getId());
    }

    /**
     * @return True if live broadcast is playing, otherwise false.
     */
    public boolean isLiveBroadcastPlaying() {
        IBroadcast broadcast = getBroadcastPlayerData();
        PlaybackType playerType = getPlayerType();
        return (playerType == PlaybackType.LiveTvIpTv || playerType == PlaybackType.LiveTvOtt ||
                playerType == PlaybackType.Replay) && broadcast != null && broadcast.isLive();
    }

    /**
     * Returns true if a replay with the given broadcast id is currently playing.
     */
    public boolean isReplayPlaying(String broadcastId) {
        PlaybackState state = getExclusiveDetails().getState();
        if ((state != null && !state.active) || getPlayerType() != PlaybackType.Replay) {
            return false;
        }

        IBroadcast broadcast = getBroadcastPlayerData();
        return broadcast != null && !TextUtils.isEmpty(broadcast.getId())
                && broadcast.getId().equals(broadcastId);
    }

    public boolean isRecordingPlaying(String broadcastId) {
        PlaybackState state = getExclusiveDetails().getState();
        if (state != null && !state.active) {
            return false;
        }

        IBroadcast broadcast = getRecordingPlayerData();
        return broadcast != null && !TextUtils.isEmpty(broadcast.getId())
                && broadcast.getId().equals(broadcastId);
    }

    /**
     * @return True if the given VOD variant is currently playing.
     */
    public boolean isVodPlaying(IVodVariant vodVariant) {
        PlaybackState state = getExclusiveDetails().getState();
        if (state != null && !state.active || getPlayerType() != PlaybackType.Vod) {
            return false;
        }

        IVodVariant vodVariantPlayerData = getVodVariantPlayerData();
        return vodVariantPlayerData != null
                && Objects.equals(vodVariantPlayerData.getId(), vodVariant.getId());
    }

    /**
     * Returns true is the vod trailer with the given playback uri is currently playing.
     */
    public boolean isTrailerPlaying(IVodVariant vodVariant) {
        PlaybackState state = getExclusiveDetails().getState();
        if (state != null && !state.active || getPlayerType() != PlaybackType.VodTrailer) {
            return false;
        }

        IVodVariant vodVariantPlayerData = getVodVariantPlayerData();
        return vodVariantPlayerData != null
                && Objects.equals(vodVariantPlayerData.getId(), vodVariant.getId());
    }

    /**
     * @return True if the given vod variant is currently playing, otherwise false.
     */
    public boolean isGdprVideoPlaying(IVodVariant vodVariant) {
        PlaybackState state = getExclusiveDetails().getState();
        if (state != null && !state.active || getPlayerType() != PlaybackType.Gdpr) {
            return false;
        }

        IVodVariant gdprVodVariant = getGdprVariantPlayerData();
        return gdprVodVariant != null
                && Objects.equals(gdprVodVariant.getId(), vodVariant.getId());
    }

    public StartCommand getStartedCommand() {
        return mStartCommand;
    }

    /**
     * Get the channel ID (if any) of the currently running program
     */
    public String getChannelId() {
        if (mStartCommand instanceof IptvLiveStartCommand) {
            return ((IptvLiveStartCommand) mStartCommand).getChannel().getId();
        }
        if (mStartCommand instanceof ReplayStartCommand) {
            return ((ReplayStartCommand) mStartCommand).getContentItem().getChannelId();
        }
        if (mStartCommand instanceof RecordingStartCommand) {
            return ((RecordingStartCommand) mStartCommand).getContentItem().getChannelId();
        }
        return null;
    }

    public boolean isFavorite() {
        if (mStartCommand instanceof IptvLiveStartCommand) {
            return ((IptvLiveStartCommand) mStartCommand).isFromFavorites();
        }
        if(mStartCommand instanceof  ReplayStartCommand) {
            return ((ReplayStartCommand) mStartCommand).isFromFavourite();
        }
        return false;
    }

    public boolean isAppChannelPlaying() {
        TvChannel channel = getChannelData();
        return channel != null && channel.getType() == ChannelType.APP;
    }

    public boolean isAppChannelPlaying(String nextChannelId) {
        TvChannel channel = getChannelData();
        return channel != null && !TextUtils.isEmpty(channel.getId())
                && channel.getId().equals(nextChannelId) && channel.getType() == ChannelType.APP;
    }

    public boolean isRegionalChannelPlaying(String nextChannelId) {
        TvChannel channel = getChannelData();
        return channel != null && !TextUtils.isEmpty(channel.getId())
                && channel.getId().equals(nextChannelId) && channel.getType() == ChannelType.REGIONAL;
    }

    public boolean isRegionalChannelPlaying() {
        TvChannel channel = getChannelData();
        return channel != null && channel.getType() == ChannelType.REGIONAL;
    }

    /**
     * @return The live (or radio) channel which is currently playing or null if there is no such.
     */
    public TvChannel getChannelData() {
        if (mStartCommand instanceof IptvLiveStartCommand) {
            return ((IptvLiveStartCommand) mStartCommand).getChannel();
        }
        return null;
    }

    /**
     * @return The currently running broadcast or null if there is no such.
     */
    @Nullable
    public IBroadcast getBroadcastPlayerData() {
        return getBroadcastPlayerData(getPosition(System.currentTimeMillis()));
    }

    /**
     * @param timeStamp The timestamp from where the broadcast needs to be returned.
     * @return The broadcast which is playing at the given timestamp or null if there is no such.
     */
    @Nullable
    public IBroadcast getBroadcastPlayerData(long timeStamp) {
        return getBroadcastPlayerData(timeStamp, true);
    }

    /**
     * @param timeStamp    The timestamp from where the broadcast needs to be returned.
     * @param defaultFirst First broadcast as default when there is no broadcast at the given timestamp.
     * @return The broadcast which is playing at the given timestamp or null if there is no such.
     */
    @Nullable
    public IBroadcast getBroadcastPlayerData(long timeStamp, boolean defaultFirst) {
        if (mStartCommand instanceof BroadcastStartCommand) {
            return ((BroadcastStartCommand) mStartCommand).getBroadcast(timeStamp, defaultFirst);
        }
        return null;
    }

    /**
     * @return The replay broadcast which is currently playing or null if there is no such.
     */
    @Nullable
    public IBroadcast getReplayPlayerData() {
        if (mStartCommand instanceof ReplayStartCommand) {
            // Broadcasts are always updated one by one
            return ((ReplayStartCommand) mStartCommand).getContentItem();
        }
        return null;
    }

    /**
     * @return The currently running live broadcast.
     * If the currently running playback is not live (or live replay) it will return null.
     */
    @Nullable
    public IBroadcast getLivePlayerData() {
        if (mStartCommand != null) {
            switch (mStartCommand.getType()) {
                case LiveTvIpTv:
                case RadioIpTv:
                    return getBroadcastPlayerData(getPosition(System.currentTimeMillis()));
                case Replay:
                    return getReplayPlayerData();
            }
        }
        return null;
    }

    /**
     * @return The VOD which is currently playing or null if there is no such.
     */
    @Nullable
    public IVodItem getVodPlayerData() {
        if (mStartCommand instanceof VodStartCommand) {
            return ((VodStartCommand) mStartCommand).getVodItem();
        }
        if (mStartCommand instanceof TrailerStartCommand) {
            return ((TrailerStartCommand) mStartCommand).getVodItem();
        }
        return null;
    }

    /**
     * @return The series of the currently running vod episode.
     */
    @Nullable
    public IVodSeries getVodSeriesPlayerData() {
        if (mStartCommand instanceof VodStartCommand) {
            return ((VodStartCommand) mStartCommand).getVodSeries();
        }
        return null;
    }

    /**
     * @return The VOD variant which is currently playing or null if there is no such.
     */
    @Nullable
    public IVodVariant getVodVariantPlayerData() {
        if (mStartCommand instanceof VodStartCommand) {
            return ((VodStartCommand) mStartCommand).getContentItem();
        }
        if (mStartCommand instanceof TrailerStartCommand) {
            return ((TrailerStartCommand) mStartCommand).getContentItem();
        }
        return null;
    }

    @Nullable
    public IVodPrice getVodPricePlayerData() {
        if (mStartCommand instanceof VodStartCommand) {
            return ((VodStartCommand) mStartCommand).getVodPrice();
        }
        return null;
    }

    /**
     * @return The VOD variant for GDPR video which is currently playing or null if there is no such.
     */
    @Nullable
    public IVodVariant getGdprVariantPlayerData() {
        if (mStartCommand instanceof GdprStartCommand) {
            return ((GdprStartCommand) mStartCommand).getContentItem();
        }
        return null;
    }

    @Nullable
    public IVodPrice getGdprVodPrice() {
        if (mStartCommand instanceof GdprStartCommand) {
            return ((GdprStartCommand) mStartCommand).getVodPrice();
        }
        return null;
    }

    /**
     * @return The VOD for GDPR video which is currently playing or null if there is no such.
     */
    @Nullable
    public IVodItem getGdprPlayerData() {
        if (mStartCommand instanceof GdprStartCommand) {
            return ((GdprStartCommand) mStartCommand).getVodItem();
        }
        return null;
    }


    /**
     * @return The recording which is currently playing or null if there is no such.
     */
    @Nullable
    public IRecording getRecordingPlayerData() {
        if (mStartCommand instanceof RecordingStartCommand) {
            return ((RecordingStartCommand) mStartCommand).getContentItem();
        }
        return null;
    }

    /**
     * @return The series of the currently running recording episode.
     */
    @Nullable
    public IRecordingSeries getRecordingSeriesPlayerData() {
        if (mStartCommand instanceof RecordingStartCommand) {
            return ((RecordingStartCommand) mStartCommand).getRecordingSeries();
        }
        return null;
    }

    @Override
    public void onCommandSuccess(PlaybackCommand cmd, PlaybackDetails details) {
        super.onCommandSuccess(cmd, details);
        Log.d(TAG, "PlaybackCommand[" + cmd.getId() + "] was successful");
    }

    @Override
    public void onPlayerDataUpdate(IBroadcast event) {
        if (event == null) {
            // There is nothing to update.
            return;
        }

        for (WeakReference<IPlaybackCallback> cw : callbacks) {
            mMainHandler.post(() -> {
                try {
                    IPlaybackCallback callback = cw.get();
                    if (callback instanceof IPlaybackDetailsCallback) {
                        ((IPlaybackDetailsCallback) callback).onPlayerDataUpdate(event);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Failed to dispatch data update.", e);
                }
            });
        }
    }

    @Override
    public void onNextEpisodeLoaded() {
        for (WeakReference<IPlaybackCallback> cw : callbacks) {
            mMainHandler.post(() -> {
                try {
                    IPlaybackCallback callback = cw.get();
                    if (callback instanceof SeriesPlaybackDetailsCallback) {
                        ((SeriesPlaybackDetailsCallback) callback).onNextEpisodeLoaded();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Failed to dispatch data update.", e);
                }
            });
        }
    }

    public long getStart() {
        if (mStartCommand != null) {
            switch (mStartCommand.getType()) {
                case LiveTvIpTv:
                case RadioIpTv:
                    IBroadcast broadcast = getBroadcastPlayerData(getPosition(System.currentTimeMillis()));
                    return broadcast != null ? broadcast.getStart() : System.currentTimeMillis();
                case Replay:
                    broadcast = getReplayPlayerData();
                    if (broadcast != null) {
                        return broadcast.getStart();
                    }
                    break;
                case Recording:
                    return ((RecordingStartCommand) mStartCommand).getContentItem().getRecordingStart();
            }
        }

        return 0;
    }

    public IContentItem getPlayingItem() {
        if (mStartCommand instanceof ContentItemStartCommand) {
            return ((ContentItemStartCommand<?>) mStartCommand).getContentItem();
        }
        return null;
    }

    /**
     * @return True if the currently active playback can be paused.
     */
    public boolean canPause() {
        PlaybackDetails details = getExclusiveDetails();
        if (!details.canPause()) {
            return false;
        }

        IBroadcast broadcast;
        TvChannel channel;
        switch (getPlayerType()) {
            case RadioIpTv:
            case RadioOtt:
                return false;
            case LiveTvOtt:
            case LiveTvIpTv:
            case LiveTvDvbC:
            case LiveTvDvbT:
                if (isRegionalChannelPlaying()) {
                    return false;
                }
                if (details.getDomain() != PlaybackDomain.LiveTvIpTv || isAppChannelPlaying()) {
                    broadcast = getLivePlayerData();
                    channel = broadcast != null ? mNowOnTvCache.getChannel(broadcast.getChannelId()) : null;
                    return broadcast != null && channel != null
                            && channel.isReplayEnabled() && broadcast.isInReplayWindow(channel);
                }
                break;
            case Replay:
                broadcast = getReplayPlayerData();

                if (broadcast == null) {
                    return false;
                }

                channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
                return channel != null && channel.isReplayEnabled() && broadcast.isInReplayWindow(channel);
            case Recording:
            case Vod:
            case VodTrailer:
            default:
                break;
        }
        return true;
    }

    /**
     * @return True if the user can start over the currently playing stream.
     */
    public boolean canStartOver() {
        IBroadcast broadcast;
        TvChannel channel;
        PlaybackDetails details;
        long appearingDelay = Components.get(PlayerConfig.class)
                .getStartOverAppearingDelay(TimeUnit.MILLISECONDS);

        long playbackStartTime = getLastStartInitiatedTime(System.currentTimeMillis());
        switch (getPlayerType()) {
            case LiveTvOtt:
            case LiveTvIpTv:
            case LiveTvDvbC:
            case LiveTvDvbT:
                // Seek enable on replay.
                broadcast = getLivePlayerData();
                if (broadcast == null) {
                    return false;
                }
                if (broadcast.isGenerated()) {
                    // no info broadcast can not be replayed
                    return false;
                }

                channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
                if (!broadcast.canStartOver(channel, playbackStartTime)) {
                    return false;
                }

                if (controller.getActiveControl() == null && !isCommandStarting(StartCommand.class)) {
                    return true;
                }

                details = getExclusiveDetails();
                return (System.currentTimeMillis() - details.getStart() > appearingDelay);

            case Replay:
                broadcast = getReplayPlayerData();
                if (broadcast == null) {
                    return false;
                }

                if (broadcast.isGenerated()) {
                    // no info broadcast can not be replayed
                    return false;
                }

                channel = mNowOnTvCache.getChannel(broadcast.getChannelId());

                if (!broadcast.canStartOver(channel, playbackStartTime)) {
                    return false;
                }

                // Enable start over in case of playback errors
                if (controller.getActiveControl() == null && !isCommandStarting(StartCommand.class)) {
                    return true;
                }

                details = getExclusiveDetails();
                return System.currentTimeMillis() - details.getStart() > appearingDelay;

            case RadioIpTv:
            case RadioOtt:
                return false;

            case Recording:
            case Vod:
            case VodTrailer:
            default:
                return true;
        }
    }

    /**
     * @return True if the user can jump to live in the currently playing replay stream.
     */
    public boolean canJumpToLive() {
        boolean noActiveControl = controller.getActiveControl() == null;
        boolean isCommandStarting = isCommandStarting(StartCommand.class);

        switch (getPlayerType()) {
            case LiveTvIpTv:
                return (noActiveControl || !getExclusiveDetails().isLive()) && !isCommandStarting;

            case Replay:
                return true;

            case RadioIpTv:
            case RadioOtt:
            case LiveTvOtt:
                return noActiveControl && !isCommandStarting;

            case Recording:
            case Vod:
            case VodTrailer:
                return false;

        }

        return false;
    }

    /**
     * Method used for jump and resume the playback if we have IpTv playback in the background.
     */
    public void jumpAndResume(long positionToJump) {
        PlaybackDetails details = getExclusiveDetails();
        SeekDirection direction = details.getPosition() < positionToJump
                ? SeekDirection.FORWARD : SeekDirection.REWIND;

        StartAction startAction;

        if (positionToJump == Long.MAX_VALUE) {
            // Jump to live
            startAction = StartAction.TSTVEnded;
        } else if (positionToJump == Long.MIN_VALUE) {
            // Jump to the beginning of the buffer
            startAction = StartAction.BufferRestart;
        } else {
            startAction = direction == SeekDirection.REWIND ? StartAction.Rewind : StartAction.Forward;
        }

        if (inPlaybackState(PlaybackState.Paused)
                && positionToJump >= details.getSeekAbleEnd()
                && inPlaybackType(PlaybackType.LiveTvIpTv)) {
            // Live point reached, resume playback
            resume();
        }
        jumpByPosition(startAction, positionToJump);
    }

    /**
     * @return True if the user can seek in the currently playing stream.
     */
    public boolean canSeek() {
        PlaybackDetails details = getExclusiveDetails();
        if (details.getSeekAbleEnd() <= details.getSeekAbleStart()) {
            return false;
        }
        if (details.isUnskippablePosition()) {
            return false;
        }

        IBroadcast broadcast;
        TvChannel channel;
        long playbackStartTime = getLastStartInitiatedTime(System.currentTimeMillis());
        switch (getPlayerType()) {
            case LiveTvOtt:
            case LiveTvIpTv:
            case LiveTvDvbC:
            case LiveTvDvbT:
                if (isRegionalChannelPlaying()) {
                    return false;
                }
                if (details.getDomain() != PlaybackDomain.LiveTvIpTv || isAppChannelPlaying()) {
                    broadcast = getLivePlayerData();
                    if (broadcast == null) {
                        return false;
                    }

                    channel = mNowOnTvCache.getChannel(broadcast.getChannelId());
                    return broadcast.canStartOver(channel, playbackStartTime);
                }
                break;
            case Replay:
                if (isRegionalChannelPlaying() || isAppChannelPlaying()) {
                    return false;
                }
                break;
            case RadioIpTv:
            case RadioOtt:
                return false;

            case Recording:
            case Vod:
            case VodTrailer:
            default:
                break;
        }
        return true;
    }

    /**
     * @param minimumValid the minimum valid number of subtitles track to return
     * @return a list with the available subtitles tracks
     */
    public ArrayList<MediaTrackInfo> getAvailableSubtitlesTracks(int minimumValid) {
        return getAvailableTracks(getExclusiveDetails().getSubtitleTracks(), minimumValid);
    }

    /**
     * @param minimumValid the minimum valid number of audio track to return
     * @return a list with the available audio tracks
     */
    public ArrayList<MediaTrackInfo> getAvailableAudioTracks(int minimumValid) {
        return getAvailableTracks(getExclusiveDetails().getAudioTracks(), minimumValid);
    }

    /**
     * @param originalTracks the tracks from the playback manager.
     * @return an array list without any null items that can be empty if at least <minimumValid> tracks are inside the array.
     * otherwise, an empty array list.
     */
    private ArrayList<MediaTrackInfo> getAvailableTracks(MediaTrackInfo[] originalTracks,
                                                         int minimumValid) {
        ArrayList<MediaTrackInfo> tracks = new ArrayList<>();
        if (originalTracks.length < minimumValid) {
            return tracks;
        }
        for (MediaTrackInfo tvTrackInfo : originalTracks) {
            if (tvTrackInfo != null && tvTrackInfo.getLanguage() != null) {
                tracks.add(tvTrackInfo);
            }
        }
        if (tracks.size() < minimumValid) {
            tracks.clear();
        }
        return tracks;
    }

    /**
     * Stores in DB the last played channel number.
     *
     * @param channelId   The selected channel id which needs to be saved.
     * @param channelType The selected channelType.
     */
    private void saveLastPlayedChannelNumber(String channelId, ChannelType channelType) {
        TvChannel channel;
        if (channelId == null || channelType == null || (channel = mNowOnTvCache.getChannel(channelId)) == null) {
            return;
        }

        Log.d(TAG, "Save the last tuned channel (by number): " + channelId + ", number: " + channel.getNumber());
        switch (channelType) {
            case RADIO:
                Settings.lastPlayedRadioChannelNumber.asyncEdit().put(channel.getNumber());
                // Fall through
            case TV:
                Settings.lastPlayedChannelNumber.asyncEdit().put(channel.getNumber());
                break;
        }
    }

    /**
     * Update the broadcast list for the current playback.
     *
     * @param broadcasts    The list of broadcasts that are in the buffer
     */
    public void updatePlayerData(List<IBroadcast> broadcasts) {
        Log.d(TAG, "updatePlayerData() called with: broadcasts = [" + broadcasts + "]");
        if (mStartCommand instanceof BroadcastStartCommand) {
            BroadcastStartCommand broadcastStartCommand = (BroadcastStartCommand) mStartCommand;
            IBroadcast prevPlaying = broadcastStartCommand.getContentItem();
            broadcastStartCommand.setBroadcastList(broadcasts);
            IBroadcast nowPlaying = broadcastStartCommand.getContentItem();

            // Current broadcast changed.
            boolean changed = prevPlaying == null || nowPlaying == null
                    || !Objects.equals(prevPlaying.getId(), nowPlaying.getId());

            if (changed) {
                switchCurrentProgram();
            }
        }
    }

    /**
     * Switch the currently running program bounds for the next one that's coming.
     */
    public void switchCurrentProgram() {
        Log.d(TAG, "switchCurrentProgram() called.");
        IBroadcast nextBroadcast = getBroadcastPlayerData(getPosition(System.currentTimeMillis()), false);
        if (nextBroadcast != null) {
            onPlayerDataUpdate(nextBroadcast);
        }
    }

    /**
     * @return True if the current playback state is a trick play state, otherwise false.
     */
    public boolean inTrickPlayState() {
        PlaybackControl<?> control = controller.getActiveControl();
        if (control == null) {
            return false;
        }
        PlaybackState state = control.getCurrentState();
        return state == PlaybackState.TrickPlay || state == PlaybackState.TrickPlayPaused;
    }

    public boolean isUnskippablePosition() {
        PlaybackControl<?> control = controller.getActiveControl();
        if (control == null) {
            return false;
        }

        long position = control.getCurrentPosition();
        return control.isUnskippablePosition(position);
    }
}
