package tv.threess.threeready.ui.generic.fragment;

import static tv.threess.threeready.ui.generic.utils.DeepLinkHelper.DEEPLINK_CLOSE_APPLICATION_URL;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.Nullable;

import io.reactivex.annotations.NonNull;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.generic.navigation.Navigator;

/**
 * Base class for WebViewFragments.
 *
 * @author Andrei Teslovan
 * @since 2018.07.25
 */

public abstract class BaseWebViewFragment extends BaseFragment {
    public static final String TAG = Log.tag(BaseWebViewFragment.class);

    public static final String CONSOLE_TAG = "WebViewConsole";

    protected WebView mWebView = null;

    private final Navigator mNavigator = Components.get(Navigator.class);
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        mWebView = new WebView(getContext());
        WebSettings settings = mWebView.getSettings();
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setJavaScriptEnabled(true);

        setWebClient();
        setWebChromeClient();
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.d(CONSOLE_TAG, consoleMessage.message() + " -- From line "
                        + consoleMessage.lineNumber() + " of "
                        + consoleMessage.sourceId());
                return true;
            }
        });
        mWebView.requestFocus();

        return mWebView;
    }

    private void setWebClient() {
        mWebView.setWebViewClient(new WebViewClient() {

            /**
             * In case of error we check if the url is the close trigger one and we close
             * the whole page by calling @{@link Navigator#goBack()}
             *
             * @param view    WebView
             * @param request what was the request
             * @param error   the error which was triggered
             */
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                if (request.getUrl().toString().contains(DEEPLINK_CLOSE_APPLICATION_URL)
                        && request.getUrl().getQueryParameterNames().isEmpty()) {
                    mNavigator.goBack();
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mNavigator.doDeepLinkActionFromWebView(url);
            }
        });
    }

    private void setWebChromeClient() {
        mWebView.setWebChromeClient(new WebChromeClient());
    }

    /**
     * Called when the fragment is visible to the user and actively running. Resumes the WebView.
     */
    @Override
    public void onPause() {
        super.onPause();
        mWebView.onPause();
    }

    /**
     * Called when the fragment is no longer resumed. Pauses the WebView.
     */
    @Override
    public void onResume() {
        mWebView.onResume();
        super.onResume();
    }

    /**
     * Called when the WebView has been detached from the fragment.
     * The WebView is no longer available after this time.
     */
    @Override
    public void onDestroyView() {
        if (mWebView != null) {
            mWebView.destroy();
            mWebView = null;
        }
        super.onDestroyView();
    }

    @Override
    public boolean onKeyDown(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_BACK:
                    if (mWebView.canGoBack()) {
                        mWebView.goBack();
                        return true;
                    }
            }
        }

        return super.onKeyDown(event);
    }

    /**
     * Gets the WebView.
     */
    public WebView getWebView() {
        return mWebView;
    }
}
