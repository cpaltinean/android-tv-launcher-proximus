package tv.threess.threeready.ui.tv.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.TimeRange;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.ui.R;

/**
 * Base SeekBarView used to draw 2 overlapping seek bars and a buffer start indicator.
 * Responsible only for drawing primary and secondary progress, the thumb image and the buffer
 * start indicator. The primary seek bar shows the progress of the playback, and the secondary
 * progress shows the total duration of the available part of the running playback.
 *
 * @author Solyom Zsolt
 * @since 2020.03.24
 */

public abstract class BaseSeekBarView extends View {
    private final LayoutConfig mLayoutConfig = Components.get(LayoutConfig.class);

    protected long mSeekAbleStart;
    protected long mSeekAbleEnd;

    protected long mProgressStart;
    protected long mProgressEnd;

    protected long mPrimaryStart;
    protected long mPrimaryEnd;

    protected long mSecondaryStart;
    protected long mSecondaryEnd;

    protected TimeRange[] mUnskipableRanges;

    private final float mSeekBarHeight = 12;
    protected boolean mIsThumbVisible = false;
    protected boolean mShowBufferStartMarker = false;

    private final SeekBarDrawable mThumbImage;
    private final SeekBarDrawable mBufferStartImage;

    private final RectF mRectF;
    private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public BaseSeekBarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mThumbImage = new SeekBarDrawable(getContext(), R.drawable.player_thumb);
        mBufferStartImage = new SeekBarDrawable(getContext(), R.drawable.player_buffer_start);

        setFocusable(true);
        setFocusableInTouchMode(true);

        mRectF = new RectF(0f,
                0.5f * (getHeight() - mSeekBarHeight), getWidth(),
                0.5f * (getHeight() + mSeekBarHeight));

        setOnFocusChangeListener((v, hasFocus) -> {
            mIsThumbVisible = hasFocus;
            if (mIsThumbVisible) {
                invalidate();
            }
        });
    }

    public long getSeekAbleEnd() {
        return mSeekAbleEnd;
    }

    /**
     * @param showBufferStartMarker True if the vertical marker line
     *                              at the start of the buffer start should be displayed.
     */
    public void setShowBufferStartMarker(boolean showBufferStartMarker) {
        mShowBufferStartMarker = showBufferStartMarker;
    }

    public void setUnskippableMarkers(TimeRange[] unskippableRanges) {
        mUnskipableRanges = unskippableRanges;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        initRectF();

        drawBackgroundSeekBar(canvas, mRectF);
        if (mSecondaryStart < mSecondaryEnd) {
            drawSecondaryProgress(canvas, mRectF);
        }

        if (mPrimaryStart < mPrimaryEnd) {
            drawPrimaryProgress(canvas, mRectF);
        }

        if (mIsThumbVisible && getDuration() > 0) {
            drawImageToSeekBar(mThumbImage, normalizedToScreen(mPrimaryEnd), canvas);
        }

        if (mShowBufferStartMarker
                && mSeekAbleStart > mProgressStart
                && mSeekAbleStart < mSeekAbleEnd) {
            drawImageToSeekBar(mBufferStartImage, normalizedToScreen(mSeekAbleStart), canvas);
        }
    }

    /**
     * Returns the duration of the current playback
     */
    protected long getDuration() {
        return mProgressEnd - mProgressStart;
    }

    private void initRectF() {
        mRectF.left = 0f;
        mRectF.top = 0.5f * (getHeight() - mSeekBarHeight);
        mRectF.right = getWidth();
        mRectF.bottom = 0.5f * (getHeight() + mSeekBarHeight);
    }

    private void drawBackgroundSeekBar(Canvas canvas, RectF rect) {
        mPaint.reset();
        mPaint.setColor(getResources().getColor(R.color.seekbar_progress_indicator_background, null));
        canvas.drawRoundRect(rect, mSeekBarHeight / 2, mSeekBarHeight / 2, mPaint);
    }

    private void drawSecondaryProgress(Canvas canvas, RectF rect) {
        mPaint.reset();
        rect.left = normalizedToScreen(mSecondaryStart);
        rect.right = normalizedToScreen(mSecondaryEnd);
        mPaint.setColor(getResources().getColor(R.color.progress_bar_buffered_background, null));
        canvas.drawRoundRect(rect, mSeekBarHeight / 2, mSeekBarHeight / 2, mPaint);
    }

    private void drawPrimaryProgress(Canvas canvas, RectF rect) {
        mPaint.reset();
        if (mPrimaryEnd < mSecondaryStart || mPrimaryStart < mSecondaryStart) {
            return;
        }
        rect.left = normalizedToScreen(mPrimaryStart);
        rect.right = normalizedToScreen(mPrimaryEnd);

        mPaint.setColor(mLayoutConfig.getPrimaryColorStart());
        mPaint.setShader(new LinearGradient(0, 0, getWidth(),
                getHeight(), mLayoutConfig.getPrimaryColorStart(), mLayoutConfig.getPrimaryColorEnd(), Shader.TileMode.MIRROR));

        //This calculation is used for keep the rounded corner of the progress bar.
        if ((rect.right - rect.left) < mSeekBarHeight) {
            float w = rect.right - rect.left;
            float h = (mSeekBarHeight - w) / 4;

            rect.top = rect.top + h;
            rect.bottom = rect.bottom - h;
            rect.right = rect.left + w;
        }

        canvas.drawRoundRect(rect, mSeekBarHeight / 2, mSeekBarHeight / 2, mPaint);
    }

    /**
     * Draws the "normal" resp. "pressed" drawable image on specified x-coordinate.
     *
     * @param screenCoord The x-coordinate in screen space where to draw the image.
     * @param canvas      The canvas to draw upon.
     */
    private void drawImageToSeekBar(SeekBarDrawable drawableImage, float screenCoord, Canvas canvas) {
        mPaint.reset();
        drawableImage.calculateBounds(screenCoord, getHeight());
        drawableImage.draw(canvas);
    }

    /**
     * Converts a position value into screen space.
     *
     * @param coord The value to convert.
     * @return The converted value in screen space.
     */
    private float normalizedToScreen(long coord) {
        float duration = getDuration();
        if (duration == 0) {
            // prevent division by zero, simply return 0.
            return 0;
        }
        float normalizedCoord = (coord - mProgressStart) / duration;
        return normalizedCoord * getWidth();
    }
}
