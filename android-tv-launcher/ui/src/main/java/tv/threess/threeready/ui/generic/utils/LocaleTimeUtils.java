/*
 *  Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.ui.generic.utils;

import android.annotation.SuppressLint;

import androidx.annotation.Nullable;

import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;

/**
 * Date and time related utility methods.
 *
 * @author Barabas Attila
 * @since 2017.05.04
 */
public class LocaleTimeUtils {
    private static final int AVAILABILITY_TIME_60_DAYS = 60;
    private static final int AVAILABILITY_TIME_48H = 48;
    private static final int AVAILABILITY_TIME_24H = 24;
    private static final int AVAILABILITY_TIME_60MIN = 60;
    private static final String TRANSLATION_DAYS = "%DAYS%";
    private static final String TRANSLATION_HOURS = "%HOURS%";
    private static final String TRANSLATION_DATE = "%DATE%";
    private static final String TRANSLATION_MINUTES = "%MINUTES%";

    private static final String DAY_DATE_FORMAT = "dd";
    private static final String MONTH_DATE_FORMAT = "MM";
    private static final String DAY_MONTH_YEAR_FORMAT = "dd/MM/YYYY";

    /**
     * @param channel The tv channel on which the broadcast is available.
     * @param broadcast  The broadcast for which the availability should be calculated.
     * @param recording  The recording event of the broadcast or null if not recorded.
     * @param translator Translator to localize the generated text.
     * @return Return the remaining days/hours until the broadcast can be watched.
     */
    public static String getBroadcastAvailabilityTime(TvChannel channel, IBroadcast broadcast,
                                                      @Nullable IRecording recording, Translator translator) {
        // Calculate availability time for replay.
        long replayAvailabilityTime = 0;
        if (!broadcast.isFuture() && broadcast.canReplay(channel)) {
            replayAvailabilityTime = broadcast.getStart() - System.currentTimeMillis() + channel.getReplayWindow();
        }

        // Calculate availability time for recordings.
        long recordingAvailabilityTime = 0;
        if (recording != null) {
            recordingAvailabilityTime = recording.getAvailabilityTime() - System.currentTimeMillis();
        }

        // Pick the max from replay and recording availability time.
        long availabilityTime = Math.max(replayAvailabilityTime, recordingAvailabilityTime);

        // Available. Decide on display format.
        if (availabilityTime > 0) {
            long availableHours = TimeUnit.MILLISECONDS.toHours(availabilityTime);
            if (availableHours >= AVAILABILITY_TIME_48H) {
                return translator.get(TranslationKey.DETAIL_AVAILABILITY_DAYS)
                        .replace(TRANSLATION_DAYS, Long.toString(TimeUnit.HOURS.toDays(availableHours)));
            } else if (availableHours >= AVAILABILITY_TIME_24H) {
                return translator.get(TranslationKey.DETAIL_AVAILABILITY_1_DAY)
                        .replace(TRANSLATION_DAYS, Long.toString(TimeUnit.HOURS.toDays(availableHours)));
            } else {
                long availableMinutes = TimeUnit.MILLISECONDS.toMinutes(availabilityTime);
                if (availableMinutes < AVAILABILITY_TIME_60MIN) {
                    return translator.get(TranslationKey.DETAIL_AVAILABILITY_LESS_THAN_1_HOUR);
                } else {
                    return translator.get(TranslationKey.DETAIL_AVAILABILITY_HOURS)
                            .replace(TRANSLATION_HOURS, Long.toString(availableHours));
                }
            }
        }

        // Not available. Hide text.
        return "";
    }


    /**
     * Format and return the time available until the VOD is removed from the catalog.
     * @param remainingTime The remaining available time of the VOD in millis.
     * @param translator The locales for the selected language.
     * @return The formatted text.
     */
    public static String getVodAvailabilityText(long remainingTime, Translator translator) {
        if (remainingTime > 0) {
            long availableDays = TimeUnit.MILLISECONDS.toDays(remainingTime);
            if (availableDays > AVAILABILITY_TIME_60_DAYS) {
                String date = TimeUtils.getTimeFormat(DateTimeFormatter.ofPattern(DAY_MONTH_YEAR_FORMAT, Locale.getDefault()), (remainingTime + System.currentTimeMillis()));
                return translator.get(TranslationKey.DETAIL_AVAILABILITY_UNITL).replace(TRANSLATION_DATE, date);
            } else {
                long availableHours = TimeUnit.MILLISECONDS.toHours(remainingTime);
                if (availableHours >= AVAILABILITY_TIME_48H) {
                    return translator.get(TranslationKey.DETAIL_AVAILABILITY_DAYS)
                            .replace(TRANSLATION_DAYS, Long.toString(TimeUnit.HOURS.toDays(availableHours)));
                } else if (availableHours >= AVAILABILITY_TIME_24H) {
                    return translator.get(TranslationKey.DETAIL_AVAILABILITY_1_DAY)
                            .replace(TRANSLATION_DAYS, Long.toString(TimeUnit.HOURS.toDays(availableHours)));
                } else {
                    long availableMinutes = TimeUnit.MILLISECONDS.toMinutes(remainingTime);
                    if (availableMinutes < AVAILABILITY_TIME_60MIN) {
                        return translator.get(TranslationKey.DETAIL_AVAILABILITY_LESS_THAN_1_HOUR);
                    } else {
                        return translator.get(TranslationKey.DETAIL_AVAILABILITY_HOURS)
                                .replace(TRANSLATION_HOURS, Long.toString(availableHours));
                    }
                }
            }
        }
        return "";
    }

    /**
     * Format and return the time available until the purchased VOD is available for playback..
     * @param remainingTime The remaining available time of the VOD in millis.
     * @param translator The locales for the selected language.
     * @return The formatted text.
     */
    public static String getRentalRemainingTime(long remainingTime, Translator translator) {
        long minutes = TimeUnit.MILLISECONDS.toMinutes(remainingTime);
        long hours = TimeUnit.MILLISECONDS.toHours(remainingTime);

        if (hours > 0) {
            return translator.get(TranslationKey.DETAIL_RENTAL_TIME_REMAINING_HOURS)
                    .replace(TRANSLATION_HOURS, String.valueOf(hours));
        } else {
            return translator.get(TranslationKey.DETAIL_RENTAL_TIME_REMAINING_MINUTES)
                    .replace(TRANSLATION_MINUTES, String.valueOf(minutes));
        }
    }

    /**
     * Gets date for a given broadcast.
     *
     * @param broadcast      to get date for.
     * @param translator     instance.
     * @param localeSettings instance.
     * @return formatted date string.
     */
    public static String getDate(IBroadcast broadcast, Translator translator, LocaleSettings localeSettings) {
        String month = getMonthNameLocalized(broadcast.getStart(), translator, true);
        String day = getDayNameLocalized(broadcast.getStart(), translator, true);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(localeSettings.getTimeFormat(), Locale.getDefault());
        DateTimeFormatter dayFormatter = DateTimeFormatter.ofPattern(DAY_DATE_FORMAT, Locale.getDefault());

        String date = TimeUtils.getTimeFormat(dayFormatter, broadcast.getStart());
        String startTime = TimeUtils.getTimeFormat(formatter, broadcast.getStart());
        String endTime = TimeUtils.getTimeFormat(formatter, broadcast.getEnd());

        if (getShouldIncludeDate(broadcast.getStart())) {
            return String.format(TimeUtils.LONG_TIME_STAMP_FORMAT, day, date, month, startTime, endTime);
        } else {
            return String.format(TimeUtils.SHORT_TIME_STAMP_FORMAT, day, startTime, endTime);
        }
    }

    public static String getStartDate(IBroadcast broadcast, Translator translator, LocaleSettings localeSettings) {
        String day = getDayNameLocalized(broadcast.getStart(), translator, true);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(localeSettings.getTimeFormat(), Locale.getDefault());
        String startTime = TimeUtils.getTimeFormat(formatter, broadcast.getStart());

        return String.format(TimeUtils.SHORT_START_TIME_STAMP_FORMAT, day, startTime);
    }

    public static String getEndTime(IBroadcast broadcast, LocaleSettings localeSettings) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(localeSettings.getTimeFormat(), Locale.getDefault());
        String endTime = TimeUtils.getTimeFormat(formatter, broadcast.getEnd());

        return endTime;
    }

    public static String getDate(long time, Translator translator) {
        String month = getMonthNameLocalized(time, translator, true);
        String day = getDayNameLocalized(time, translator, true);

        DateTimeFormatter dayFormatter = DateTimeFormatter.ofPattern(DAY_DATE_FORMAT, Locale.getDefault());
        String date = TimeUtils.getTimeFormat(dayFormatter, time);

        if (getShouldIncludeDate(time)) {
            return String.format(TimeUtils.EPG_LONG_TIME_STAMP_FORMAT, day, date, month);
        } else {
            return String.format(TimeUtils.EPG_SHORT_TIME_STAMP_FORMAT, day);
        }
    }

    /**
     * Returns the name of the name localized with Today, Yesterday and Tomorrow.
     */
    public static String getDayNameLocalized(long time, Translator translator, boolean useShortName) {
        int dayDiff = getDayDifference(time);

        switch (dayDiff) {
            case 0:
                return translator.get(useShortName ? TranslationKey.DAY_NAME_TODAY : TranslationKey.DAY_NAME_DIVIDER_TODAY);
            case -1:
                return translator.get(useShortName ? TranslationKey.DAY_NAME_YESTERDAY : TranslationKey.DAY_NAME_DIVIDER_YESTERDAY);
            case +1:
                return translator.get(useShortName ? TranslationKey.DAY_NAME_TOMORROW : TranslationKey.DAY_NAME_DIVIDER_TOMORROW);
            default:
                return getDayOfWeekLocalized(time, translator, useShortName);
        }
    }

    private static int getDayDifference(long time) {
        long currentTime = System.currentTimeMillis();
        TimeZone timeZone = TimeZone.getDefault();

        // Get the given a current time with timezone offset.
        long timeLocalized = time + timeZone.getOffset(time);
        long currentTimeLocalized = currentTime + timeZone.getOffset(currentTime);

        return (int) (timeLocalized / TimeUnit.DAYS.toMillis(1)
                - currentTimeLocalized / TimeUnit.DAYS.toMillis(1));
    }

    public static boolean getIsToday(long time) {
        return getDayDifference(time) != 0;
    }

    public static boolean getShouldIncludeDate(long time) {
        return Math.abs(getDayDifference(time)) > 1;
    }

    /**
     * Return the name of the day localized.
     */
    private static String getDayOfWeekLocalized(long time, Translator translator, boolean useShortName) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);

        // Past days
        if (time < System.currentTimeMillis()) {
            switch (calendar.get(Calendar.DAY_OF_WEEK)) {
                case Calendar.MONDAY:
                    return translator.get(useShortName ? TranslationKey.DAY_NAME_LAST_MONDAY : TranslationKey.DAY_NAME_DIVIDER_LAST_MONDAY);
                case Calendar.TUESDAY:
                    return translator.get(useShortName ? TranslationKey.DAY_NAME_LAST_TUESDAY : TranslationKey.DAY_NAME_DIVIDER_LAST_TUESDAY);
                case Calendar.WEDNESDAY:
                    return translator.get(useShortName ? TranslationKey.DAY_NAME_LAST_WEDNESDAY : TranslationKey.DAY_NAME_DIVIDER_LAST_WEDNESDAY);
                case Calendar.THURSDAY:
                    return translator.get(useShortName ? TranslationKey.DAY_NAME_LAST_THURSDAY : TranslationKey.DAY_NAME_DIVIDER_LAST_THURSDAY);
                case Calendar.FRIDAY:
                    return translator.get(useShortName ? TranslationKey.DAY_NAME_LAST_FRIDAY : TranslationKey.DAY_NAME_DIVIDER_LAST_FRIDAY);
                case Calendar.SATURDAY:
                    return translator.get(useShortName ? TranslationKey.DAY_NAME_LAST_SATURDAY : TranslationKey.DAY_NAME_DIVIDER_LAST_SATURDAY);
                case Calendar.SUNDAY:
                    return translator.get(useShortName ? TranslationKey.DAY_NAME_LAST_SUNDAY : TranslationKey.DAY_NAME_DIVIDER_LAST_SUNDAY);
            }
            // Future days
        } else {
            switch (calendar.get(Calendar.DAY_OF_WEEK)) {
                case Calendar.MONDAY:
                    return translator.get(useShortName ? TranslationKey.DAY_NAME_MONDAY : TranslationKey.DAY_NAME_DIVIDER_MONDAY);
                case Calendar.TUESDAY:
                    return translator.get(useShortName ? TranslationKey.DAY_NAME_TUESDAY : TranslationKey.DAY_NAME_DIVIDER_TUESDAY);
                case Calendar.WEDNESDAY:
                    return translator.get(useShortName ? TranslationKey.DAY_NAME_WEDNESDAY : TranslationKey.DAY_NAME_DIVIDER_WEDNESDAY);
                case Calendar.THURSDAY:
                    return translator.get(useShortName ? TranslationKey.DAY_NAME_THURSDAY : TranslationKey.DAY_NAME_DIVIDER_THURSDAY);
                case Calendar.FRIDAY:
                    return translator.get(useShortName ? TranslationKey.DAY_NAME_FRIDAY : TranslationKey.DAY_NAME_DIVIDER_FRIDAY);
                case Calendar.SATURDAY:
                    return translator.get(useShortName ? TranslationKey.DAY_NAME_SATURDAY : TranslationKey.DAY_NAME_DIVIDER_SATURDAY);
                case Calendar.SUNDAY:
                    return translator.get(useShortName ? TranslationKey.DAY_NAME_SUNDAY : TranslationKey.DAY_NAME_DIVIDER_SUNDAY);
            }
        }

        return "";
    }

    /**
     * Returns  the name of the month localized.
     */
    @SuppressLint("SwitchIntDef")
    public static String getMonthNameLocalized(long time, Translator translator, boolean useShortName) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);

        switch (calendar.get(Calendar.MONTH)) {
            case Calendar.JANUARY:
                return translator.get(useShortName ? TranslationKey.MONTH_NAME_JANUARY : TranslationKey.MONTH_NAME_DIVIDER_JANUARY);
            case Calendar.FEBRUARY:
                return translator.get(useShortName ? TranslationKey.MONTH_NAME_FEBRUARY : TranslationKey.MONTH_NAME_DIVIDER_FEBRUARY);
            case Calendar.MARCH:
                return translator.get(useShortName ? TranslationKey.MONTH_NAME_MARCH : TranslationKey.MONTH_NAME_DIVIDER_MARCH);
            case Calendar.APRIL:
                return translator.get(useShortName ? TranslationKey.MONTH_NAME_APRIL : TranslationKey.MONTH_NAME_DIVIDER_APRIL);
            case Calendar.MAY:
                return translator.get(useShortName ? TranslationKey.MONTH_NAME_MAY : TranslationKey.MONTH_NAME_DIVIDER_MAY);
            case Calendar.JUNE:
                return translator.get(useShortName ? TranslationKey.MONTH_NAME_JUNE : TranslationKey.MONTH_NAME_DIVIDER_JUNE);
            case Calendar.JULY:
                return translator.get(useShortName ? TranslationKey.MONTH_NAME_JULY : TranslationKey.MONTH_NAME_DIVIDER_JULY);
            case Calendar.AUGUST:
                return translator.get(useShortName ? TranslationKey.MONTH_NAME_AUGUST : TranslationKey.MONTH_NAME_DIVIDER_AUGUST);
            case Calendar.SEPTEMBER:
                return translator.get(useShortName ? TranslationKey.MONTH_NAME_SEPTEMBER : TranslationKey.MONTH_NAME_DIVIDER_SEPTEMBER);
            case Calendar.OCTOBER:
                return translator.get(useShortName ? TranslationKey.MONTH_NAME_OCTOBER : TranslationKey.MONTH_NAME_DIVIDER_OCTOBER);
            case Calendar.NOVEMBER:
                return translator.get(useShortName ? TranslationKey.MONTH_NAME_NOVEMBER : TranslationKey.MONTH_NAME_DIVIDER_NOVEMBER);
            case Calendar.DECEMBER:
                return translator.get(useShortName ? TranslationKey.MONTH_NAME_DECEMBER : TranslationKey.MONTH_NAME_DIVIDER_DECEMBER);
        }

        return "";
    }

    /**
     * Return formatted duration for unskippable advertisements e.g 10:45
     */
    public static String getUnskippableDuration(long time, Translator translator) {
        final DateTimeFormatter playedTime = DateTimeFormatter.ofPattern(" mm:ss", Locale.getDefault());
        return translator.get(TranslationKey.UNSKIPPABLE_ADS_ADVERTISEMENT) +
                TimeUtils.getTimeFormat(playedTime, time, TimeBuilder.UTC.toZoneId());
    }

    /**
     * Return duration e.g 1h 45min
     */
    public static String getDuration(long time, Translator translator) {
        int hour = (int) TimeUnit.MILLISECONDS.toHours(time);

        long timeLeft = time - TimeUnit.HOURS.toMillis(hour);
        int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(timeLeft);
        long seconds = (int) TimeUnit.MILLISECONDS.toSeconds(timeLeft - TimeUnit.MINUTES.toMillis(minutes));

        return (hour > 0 ? hour + translator.get(TranslationKey.SCREEN_DETAIL_TIME_HOURS) + " " : "")
                + minutes + translator.get(TranslationKey.SCREEN_DETAIL_TIME_MINUTES) + " "
                + (seconds > 0 ?  seconds + translator.get(TranslationKey.SCREEN_DETAIL_TIME_SEC) + " " : "");
    }
}
