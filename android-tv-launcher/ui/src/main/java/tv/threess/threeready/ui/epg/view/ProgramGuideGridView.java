package tv.threess.threeready.ui.epg.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.leanback.widget.VerticalGridView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SafeScrollGridLayoutDelegate;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.epg.presenter.SingleProgramGuidePresenter;
import tv.threess.threeready.ui.epg.presenter.TvProgramGuideCardPresenter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.view.ResizableCardView;

/**
 * Vertical program guide grid with program cards and day divider.
 *
 * @author Barabas Attila
 * @since 2017.12.04
 */

public class ProgramGuideGridView extends VerticalGridView {
    private static final String TAG = Log.tag(ProgramGuideGridView.class);

    protected ViewHolder mSelectedHolder;

    private Runnable mSelectPositionRunnable;

    public ProgramGuideGridView(Context context) {
        this(context, null);
    }

    public ProgramGuideGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProgramGuideGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        // Align the selected item at the center-bottom
        setWindowAlignment(VerticalGridView.WINDOW_ALIGN_NO_EDGE);
        setItemSpacing(getResources().getDimensionPixelSize(R.dimen.tv_program_guide_item_spacing));
    }


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        addOnChildViewHolderSelectedListener(mOnChildViewHolderSelectedListener);
    }

    @Override
    public void setLayoutManager(@Nullable  RecyclerView.LayoutManager layout) {
        super.setLayoutManager(layout);
        SafeScrollGridLayoutDelegate.setSafeScrollGridLayoutDelegate(this, layout);
    }

    @Override
    public void onViewAdded(View child) {
        super.onViewAdded(child);

        child.setActivated(isActivated());

        // Set maximum size for the selected card.
        ViewHolder holder = getChildViewHolder(child);
        if (getScrollState() == SCROLL_STATE_IDLE && holder != null
                && holder.getAdapterPosition() == getSelectedPosition()
                && holder.itemView instanceof ResizableCardView) {
            child.setSelected(true);
            forceFocusedHolderState(holder);
        }
    }


    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        removeOnChildViewHolderSelectedListener(mOnChildViewHolderSelectedListener);
    }


    /**
     * Returns the currently selected view holder.
     */
    @Nullable
    protected ViewHolder getSelectedViewHolder() {
        return mSelectedHolder;
    }


    @Override
    public void setSelectedPosition(int position) {
        setSelectedPosition(position, 0);
    }

    @Override
    public void setSelectedPosition(int position, int scrollExtra) {
        // Cancel previous selection
        if (mSelectPositionRunnable != null) {
            removeCallbacks(mSelectPositionRunnable);
            mSelectPositionRunnable = null;
        }

        // Not attached. Wait for layout and then select position.
        if (!isAttachedToWindow()) {
            mSelectPositionRunnable = () -> setSelectedPosition(position, scrollExtra);
            post(mSelectPositionRunnable);
            return;
        }

        super.setSelectedPosition(position, scrollExtra);
    }

    protected void onChildViewHolderSelected(ViewHolder child, int position) {
        Log.d(TAG, "Selected position changed : " + position);

        if (mSelectedHolder != null) {
            mSelectedHolder.itemView.setSelected(false);
        }

        mSelectedHolder = child;

        if (mSelectedHolder != null) {
            mSelectedHolder.itemView.setSelected(true);
        }
    }

    /**
     * Returns the currently selected view child.
     */
    @Nullable
    protected View getSelectedChild() {
        ViewHolder holder = getSelectedViewHolder();
        if (holder != null) {
            return holder.itemView;
        }

        return null;
    }


    @Override
    public void requestChildFocus(View child, View focused) {
        super.requestChildFocus(child, focused);

        // Set maximum size for the selected card.
        ViewHolder holder = getChildViewHolder(child);
        if (getScrollState() == SCROLL_STATE_IDLE && holder != null
                && holder.getAdapterPosition() == getSelectedPosition()
                && holder.itemView instanceof ResizableCardView) {
            forceFocusedHolderState(holder);
            child.setSelected(true);
        }
    }

    /**
     * Set focused state for the given holder without animation.
     */
    private void forceFocusedHolderState(final RecyclerView.ViewHolder holder) {
        ItemBridgeAdapter.PresenterViewHolder presenterHolder = (ItemBridgeAdapter.PresenterViewHolder) holder;

        // Force focused animator state for program cards.
        if (presenterHolder.getViewHolder() instanceof TvProgramGuideCardPresenter.ViewHolder) {
            ((TvProgramGuideCardPresenter.ViewHolder)
                    presenterHolder.getViewHolder()).forceFocusedAnimatorState();

        } else if (presenterHolder.getViewHolder() instanceof SingleProgramGuidePresenter.ViewHolder) {
            ((SingleProgramGuidePresenter.ViewHolder)
                    presenterHolder.getViewHolder()).forceFocusedAnimatorState();
        }
    }

    /**
     * Set selected drawing state for the view and save it for future use.
     */
    OnChildViewHolderSelectedListener mOnChildViewHolderSelectedListener = new OnChildViewHolderSelectedListener() {
        @Override
        public void onChildViewHolderSelected(RecyclerView parent, ViewHolder child, int position, int subposition) {
            super.onChildViewHolderSelected(parent, child, position, subposition);

            ProgramGuideGridView.this.onChildViewHolderSelected(child, position);
        }
    };


}
