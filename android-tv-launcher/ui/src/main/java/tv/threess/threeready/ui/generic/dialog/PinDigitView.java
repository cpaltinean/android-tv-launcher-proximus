package tv.threess.threeready.ui.generic.dialog;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.PinDigitViewBinding;

/**
 * Class for representing the digit in PIN dialog.
 *
 * @author Eugen Guzyk
 * @since 2018.12.04
 */
public class PinDigitView extends FrameLayout {
    private static final String TAG = Log.tag(PinDigitView.class);

    private static final String EMPTY_SYMBOL = "-";
    private static final String ENTERED_SYMBOL = "*";

    private String mDigit;

    private OnEnterDigitCallback mOnEnterDigitCallback;

    private final PinDigitViewBinding mBinding;

    public PinDigitView(Context context) {
        this(context, null);
    }

    public PinDigitView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PinDigitView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mBinding = PinDigitViewBinding.inflate(LayoutInflater.from(getContext()), this);

        setFocusable(true);

        LayoutConfig layoutConfig = Components.get(LayoutConfig.class);
        setBackground(createDigitBackground(layoutConfig));
        mBinding.digit.setTextColor(layoutConfig.getFontColor());

        reset();
    }

    /**
     * Set the callback which invoked on digit enter.
     *
     * @param onEnterDigitCallback callback.
     */
    public void setOnEnterDigitCallback(OnEnterDigitCallback onEnterDigitCallback) {
        mOnEnterDigitCallback = onEnterDigitCallback;
    }

    /**
     * Reset the digit.
     */
    public void reset() {
        mDigit = null;
        mBinding.digit.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.pin_dialog_digit_empty_text_size));
        mBinding.digit.setText(EMPTY_SYMBOL);
    }

    /**
     * Set digit.
     *
     * @param digit to be set.
     */
    private void setDigit(String digit) {
        mDigit = digit;
        mBinding.digit.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.pin_dialog_digit_entered_text_size));
        mBinding.digit.setText(ENTERED_SYMBOL);
    }

    public String getDigit() {
        return mDigit;
    }

    /**
     * @return true if current digit is empty.
     */
    public boolean isEmpty() {
        return TextUtils.isEmpty(mDigit);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_0:
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_2:
            case KeyEvent.KEYCODE_3:
            case KeyEvent.KEYCODE_4:
            case KeyEvent.KEYCODE_5:
            case KeyEvent.KEYCODE_6:
            case KeyEvent.KEYCODE_7:
            case KeyEvent.KEYCODE_8:
            case KeyEvent.KEYCODE_9:
                setDigit(String.valueOf((char) event.getUnicodeChar()));
                break;
            default:
                return super.onKeyDown(keyCode, event);
        }
        Log.d(TAG, "pin entered digit: " + mDigit);
        return true;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_0:
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_2:
            case KeyEvent.KEYCODE_3:
            case KeyEvent.KEYCODE_4:
            case KeyEvent.KEYCODE_5:
            case KeyEvent.KEYCODE_6:
            case KeyEvent.KEYCODE_7:
            case KeyEvent.KEYCODE_8:
            case KeyEvent.KEYCODE_9:
                if (!TextUtils.isEmpty(mDigit)) {
                    requestCheck();
                }
                return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    /**
     * Requests the PIN code check.
     */
    private void requestCheck() {
        if (mOnEnterDigitCallback != null) {
            mOnEnterDigitCallback.onCheck();
        }
    }

    /**
     * Create background for digit based on layout config.
     */
    public Drawable createDigitBackground(LayoutConfig layoutConfig) {
        int[] focusedStateList = new int[]{android.R.attr.state_focused};
        int[] defaultStateList = new int[]{};

        int[] focusedColors = { layoutConfig.getButtonFocusedColorStart(), layoutConfig.getButtonFocusedColorEnd() };
        GradientDrawable focusedDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, focusedColors);

        GradientDrawable noStateDrawable = new GradientDrawable();

        StateListDrawable stateListDrawable = new StateListDrawable();
        // Focused
        stateListDrawable.addState(focusedStateList, focusedDrawable);
        // No state
        stateListDrawable.addState(defaultStateList, noStateDrawable);

        return stateListDrawable;
    }

    /**
     * Callback to be set on digit enter.
     */
    public interface OnEnterDigitCallback {
        void onCheck();
    }
}
