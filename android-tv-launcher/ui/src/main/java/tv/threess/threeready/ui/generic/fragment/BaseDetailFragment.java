package tv.threess.threeready.ui.generic.fragment;

import android.graphics.Rect;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.reactivex.disposables.Disposable;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.ContentConfig;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.DetailsBinding;
import tv.threess.threeready.ui.details.presenter.BaseDetailsOverviewPresenter;
import tv.threess.threeready.ui.generic.adapter.ItemBridgeAdapter;
import tv.threess.threeready.ui.generic.dialog.AlertDialog;
import tv.threess.threeready.ui.generic.dialog.BaseDialogFragment;
import tv.threess.threeready.ui.generic.dialog.ErrorCode;
import tv.threess.threeready.ui.generic.presenter.BasePresenter;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;

/**
 * Base fragment for detail fragments
 *
 * @author Andrei Teslovan
 * @since 2018.10.18
 */

public abstract class BaseDetailFragment<TOverviewHolder extends BaseDetailsOverviewPresenter.ViewHolder> extends BaseModularPageFragment {

    protected final ContentConfig mContentConfig = Components.get(ContentConfig.class);
    protected ParentalControlManager mParentalControlManager = Components.get(ParentalControlManager.class);

    protected Disposable mDetailsDisposable;

    protected DetailsBinding mBinding;

    /**
     * returns recommendations page config id
     *
     * @return id
     */
    protected abstract String getRcmPageConfigId();

    protected abstract int getOverviewItemOffset(TOverviewHolder viewHolder);

    @Override
    public void onDestroy() {
        super.onDestroy();
        RxUtils.disposeSilently(mDetailsDisposable);
    }

    @Override
    public void onContentShown() {
        super.onContentShown();
        restoreFocus();
    }

    @Override
    public boolean onBackPressed() {
        if (!mNavigator.isContentOverlayDisplayed()) {
            mNavigator.showContentOverlay();
            return true;
        }

        return super.onBackPressed();
    }

    /**
     * Dispatch key event to the presenter.
     *
     * @param event key event
     * @return had been performed
     */
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (mModularPageView == null) {
            return true;
        }

        int selectedPosition = mModularPageView.getSelectedPosition();
        ItemBridgeAdapter.PresenterViewHolder holder = (ItemBridgeAdapter.PresenterViewHolder)
            mModularPageView.findViewHolderForAdapterPosition(selectedPosition);
        if (holder != null) {
            BasePresenter presenter = holder.getPresenter();
            BasePresenter.ViewHolder presenterHolder = holder.getViewHolder();
            Object item = holder.getItem();
            if (presenter.onKeyEvent(presenterHolder, item, event)) {
                return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    /**
     * Should be called to display the modules from the page.
     *
     * @param pageConfig config data
     */
    @Override
    protected void displayPage(PageConfig pageConfig) {
        if (mModularPageView != null && pageConfig != null) {
            mModularPageView.loadPageConfig(pageConfig, this);
        }
    }

    protected void initModularPage() {
        addModularPageView();
        manageTopMarin();

        if (mModularPageView != null) {
            mModularPageView.setVerticalSpacing(
                    getResources().getDimensionPixelSize(R.dimen.details_row_distance));
        }
    }

    /**
     * needed to be reset for 0
     */
    protected void manageTopMarin() {
        if (mModularPageView == null) {
            return;
        }

        mModularPageView.addItemDecoration(new MarginTopResetItemDecoration());
    }

    protected PageConfig getConfigPageForRecommendations() {
        String rcmPageConfigId = getRcmPageConfigId();
        List<PageConfig> pageConfigs = mContentConfig.getPageConfigs();
        for (int i = 0; i < pageConfigs.size(); i++) {
            if (pageConfigs.get(i).getId().equals(rcmPageConfigId)) {
                return pageConfigs.get(i);
            }
        }
        return null;
    }

    /**
     * creates view and sets up it
     *
     * @param inflater for creating view
     * @param container container for fragment create view
     */
    protected void createView(LayoutInflater inflater, ViewGroup container) {
        mBinding = DetailsBinding.inflate(inflater, container, false);
        initModularPage();
    }

    @Override
    @NonNull
    protected FrameLayout getPageContainer() {
        return mBinding.pageContainer;
    }

    @Override
    @Nullable
    protected TextView getEmptyPageTextView() {
        return null;
    }

    /**
     * Show error message if fail to get data from backend
     */
    public void showErrorMessage() {
        AlertDialog alertDialog = new AlertDialog.Builder()
            .title(mTranslator.get(TranslationKey.ERR_A012_DETAIL_DISPLAY_ERROR))
            .description(mTranslator.get(TranslationKey.ERR_A012_DETAIL_DISPLAY_ERROR_DESC))
            .errorCode(ErrorCode.ERROR_CODE_LOAD_DETAIL_PAGE)
            .addButton(mTranslator.get(TranslationKey.CLOSE_BUTTON))
            .dismissOnBtnClick(true)
            .build();

        alertDialog.setDialogListener(new AlertDialog.DialogListener() {
            @Override
            public void onDismiss(BaseDialogFragment dialog) {
                mNavigator.goBack();
            }
        });

        mNavigator.showDialog(alertDialog);
    }

    /**
     * needed to align to top the view, in other cases {@link tv.threess.threeready.ui.home.view.ModularPageView} has a bigger padding on the top
     */
    private class MarginTopResetItemDecoration extends RecyclerView.ItemDecoration {

        @Override
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            if (parent.getChildAdapterPosition(view) != 0) {
                return;
            }

            ItemBridgeAdapter.PresenterViewHolder presenterViewHolder = (ItemBridgeAdapter.PresenterViewHolder) parent.getChildViewHolder(view);
            if (presenterViewHolder != null && presenterViewHolder.getViewHolder() != null
                && presenterViewHolder.getViewHolder() instanceof BaseDetailsOverviewPresenter.ViewHolder) {
                outRect.bottom = getOverviewItemOffset((TOverviewHolder) presenterViewHolder.getViewHolder());
            }
        }
    }

}
