package tv.threess.threeready.ui.details.fragment;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.VerticalGridView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;

import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.generic.model.ISeries;
import tv.threess.threeready.ui.R;
import tv.threess.threeready.ui.databinding.SeriesDetailsBinding;
import tv.threess.threeready.ui.details.presenter.SeriesDetailsOverviewPresenter;
import tv.threess.threeready.ui.details.presenter.SeriesRowPresenter;
import tv.threess.threeready.ui.details.utils.SeriesImageTargetView;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.fragment.BaseDetailFragment;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.home.view.ModularPageView;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.ui.utils.TransitionTarget;

/**
 * Basic class for series detail pages. Inflates the layout and set's up the common functionality
 * of a series detail page.
 *
 * @author Zsolt Bokor
 * @since 2019.01.10
 */
public abstract class BaseSeriesDetailFragment<TEpisode extends IContentItem, TSeries extends ISeries<TEpisode>, TOverviewHolder extends SeriesDetailsOverviewPresenter.ViewHolder>
        extends BaseDetailFragment<TOverviewHolder>
        implements SeriesRowPresenter.EpisodeSelectionListener<TEpisode> {

    private static final String CONFIG_PAGE_ID = "DETAIL_VOD_SERIES";

    protected String mSeriesId;
    protected TSeries mSeries;
    protected TEpisode mEpisode;

    protected SeriesImageTargetView mImageTargetView;

    protected SeriesDetailsBinding mBinding;

    protected abstract SeriesDetailsOverviewPresenter<TOverviewHolder, TEpisode, TSeries> getOverviewPresenter(TSeries series);

    protected abstract SeriesRowPresenter<TEpisode> getSeriesRowPresenter(TSeries series, TEpisode selectedEpisode);

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (mBinding == null) {
            createView(inflater, container);

            if (mEpisode == null) {
                loadSeriesDetails(mSeriesId);
            } else {
                mBinding.getRoot().setForeground(ContextCompat.getDrawable(mBinding.getRoot().getContext(), R.drawable.detail_skeleton));
                loadSeriesDetails(mEpisode);
            }

        // Skipp detail page from back stack when it's restricted.
        } else if (mParentalControlManager.isRestricted(mSeries)
                || mParentalControlManager.isRestricted(mEpisode)) {
            mNavigator.popBackStack();
        }

        mParentalControlManager.addListener(mParentalControlListener);

        return mBinding.getRoot();
    }

    @Override
    protected ModularPageView addModularPageView() {
        if (mModularPageView == null) {
            mModularPageView = (ModularPageView) LayoutInflater.from(mApp).inflate(R.layout.modular_page, null);
            mModularPageView.setLayoutParams(new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            hideEmptyPageMessage();
            getPageContainer().addView(mModularPageView);
        }

        return mModularPageView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mParentalControlManager.removeListener(mParentalControlListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mImageTargetView.clearImage();
        mImageTargetView.resetCurrentIndex();
    }

    /**
     * Load and display series details for the episode.
     *
     * @param episode The episode for which the series details should be loaded.
     */
    protected abstract void loadSeriesDetails(final TEpisode episode);

    /**
     * Load and display series details
     *
     * @param seriesId The series for which the details should be loaded.
     */
    protected abstract void loadSeriesDetails(final String seriesId);

    @Override
    protected void createView(LayoutInflater inflater, ViewGroup container) {
        // Inflate the layout for this fragment
        mBinding = SeriesDetailsBinding.inflate(inflater, container, false);
        mImageTargetView = new SeriesImageTargetView(getContext(), mBinding.gradientBottom, mBinding.fadingStart, mBinding.coverBig, mBinding.cover);
        initModularPage();

        if (mModularPageView != null) {
            mModularPageView.addItemDecoration(mItemDecoration);
            // Set focused row alignment.
            mModularPageView.setWindowAlignment(VerticalGridView.WINDOW_ALIGN_BOTH_EDGE);
            mModularPageView.setItemAlignmentOffsetPercent(0f);

            mBinding.coverOverlay.setBackgroundColor(mLayoutConfig.getBackgroundColor());
            mModularPageView.addOnChildViewHolderSelectedListener(new OnChildViewHolderSelectedListener() {
                @Override
                public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
                    super.onChildViewHolderSelected(parent, child, position, subposition);
                    //Changes big cover's opacity when you navigate from details area to filter/season/episodes area. Small image will be hidden.
                    if (position == 1) {
                        mImageTargetView.hideImage();
                        mBinding.coverOverlay.setAlpha(0.85f);
                    } else {
                        mImageTargetView.showImage();
                        mBinding.coverOverlay.setAlpha(0f);
                    }
                }

                @Override
                public void onChildViewHolderSelectedAndPositioned(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
                    super.onChildViewHolderSelectedAndPositioned(parent, child, position, subposition);
                    if (position == 0) {
                        mBinding.gradientEpisodeSeason.setVisibility(View.VISIBLE);
                    } else {
                        mBinding.gradientEpisodeSeason.setVisibility(View.GONE);
                    }
                }
            });
        }
    }

    /**
     * adds the corresponding presenter to the selector
     *
     * @param classPresenterSelector selector from the @{@link tv.threess.threeready.ui.home.view.ModularPageView}
     * @param series                 data for series
     * @param selectedEpisode        data for selected episode
     */
    protected abstract void addExtraPresenter(InterfacePresenterSelector classPresenterSelector, TSeries series, TEpisode selectedEpisode);


    @Override
    protected String getRcmPageConfigId() {
        return CONFIG_PAGE_ID;
    }

    /**
     * adds or replaces data for UI to @{@link tv.threess.threeready.ui.home.view.ModularPageView}
     * also handles position selection in case series row has to be selected (ex: case series was
     * opened from EPG)
     *
     * @param series         series list
     * @param focusedEpisode episode item which will be in focus
     */
    protected void addReplaceExtraDataToAdapter(TSeries series, TEpisode focusedEpisode) {
        if (mModularPageView == null
                || mModularPageView.getObjectAdapter() == null) {
            return;
        }

        ArrayObjectAdapter<Object> modularAdapter = mModularPageView.getObjectAdapter();

        if (focusedEpisode != null && !series.isEpisodeIncluded(focusedEpisode) && !series.isCompleted()) {
            return;
        }

        mBinding.getRoot().setForeground(null);

        if (modularAdapter.size() == 0) {
            //We use focusedEpisode value NULL in SeriesRowPresenter to select the correct episode on Most Recent tab.
            addExtraPresenter(mModularPageView.getClassPresenterSelector(), series, focusedEpisode);

            focusedEpisode = focusedEpisode== null ? series.getFirstEpisode() : focusedEpisode;
            modularAdapter.add(0, focusedEpisode);
            modularAdapter.add(1, series);

            displayPage(getConfigPageForRecommendations());
        } else {
            Presenter presenter = mModularPageView.getClassPresenterSelector()
                    .getPresenter(modularAdapter.get(0));
            if (presenter instanceof SeriesDetailsOverviewPresenter) {
                ((SeriesDetailsOverviewPresenter) presenter).updateSeries(series);
            }

            modularAdapter.getItems().remove(1);
            modularAdapter.getItems().add(series);
            modularAdapter.notifyItemRangeChanged(0, 2, series);

        }
    }

    /**
     * replaces data for UI, in case of episode has to change at firs position
     *
     * @param selectedEpisode data
     */
    private void replaceExtraDataToAdapter(IContentItem selectedEpisode) {
        if (mModularPageView == null || mModularPageView.getObjectAdapter() == null
                || mModularPageView.getObjectAdapter().size() == 0) {
            return;
        }

        if (!(mModularPageView.getObjectAdapter().get(0) instanceof IContentItem) ||
                (mModularPageView.getObjectAdapter().get(0) instanceof IContentItem
                        && !((IContentItem) mModularPageView.getObjectAdapter().get(0)).getId().equals(selectedEpisode.getId()))) {

            mModularPageView.getObjectAdapter().getItems().remove(0);
            mModularPageView.getObjectAdapter().getItems().add(0, selectedEpisode);
            mModularPageView.getObjectAdapter().notifyItemRangeChanged(0, 1, selectedEpisode);
        }
    }

    protected void updateImage(TEpisode item) {
        if(item == null) {
            return;
        }

        Context context = getContext();
        if (context == null) {
            return;
        }

        int width = context.getResources().getDimensionPixelSize(R.dimen.details_cover_width);
        int height = context.getResources().getDimensionPixelSize(R.dimen.details_cover_height);

        TransitionTarget target = mImageTargetView.getCurrentIndex() % 2 == 0
                ? mImageTargetView.getFirstTransitionTarget() : mImageTargetView.getSecondTransitionTarget();

        Glide.with(context.getApplicationContext()).clear(target);
        Glide.with(context.getApplicationContext()).load(item)
                .error(R.drawable.content_fallback_image_series)
                .format(DecodeFormat.PREFER_RGB_565)
                .override(width, height)
                .optionalCenterInside()
                .into(target);

        mImageTargetView.increaseCurrentIndex();
    }

    /**
     * adds the data to the adapter, which will updates its self thanks
     * for @{@link tv.threess.threeready.ui.home.view.ModularPageView} not, you should add
     * the presenter as wellf
     *
     * @param series          series list
     * @param selectedEpisode episode data
     */
    protected void addUIData(TSeries series, TEpisode selectedEpisode) {
        // Close the detail page when there is no available episode.
        if (series.getNumberOfEpisodes() == 0) {
            mNavigator.goBack();
            return;
        }

        mSeries = series;
        mEpisode = selectedEpisode;

        addReplaceExtraDataToAdapter(series, mEpisode);
        mNavigator.reportNavigationIfNeeded(this);
    }

    protected void addUIData(TSeries series) {
        addUIData(series, null);
    }

    @NonNull
    @Override
    protected FrameLayout getPageContainer() {
        return mBinding.pageContainer;
    }

    @Override
    public void onEpisodeSelected(TEpisode episode) {
        if (mModularPageView != null) {
            mModularPageView.post(() -> replaceExtraDataToAdapter(episode));
            updateImage(episode);
        }
    }

    /**
     * Item decoration the set overlapping margin for the overview and episode grid.
     */
    protected RecyclerView.ItemDecoration mItemDecoration = new RecyclerView.ItemDecoration() {
        @Override
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            RecyclerView.ViewHolder holder = parent.findContainingViewHolder(view);
            if (holder != null && holder.getBindingAdapterPosition() == 1) {
                outRect.top = getResources().getDimensionPixelOffset(R.dimen.details_overlapping_margin);
                holder.itemView.setZ(1f);
            }
        }
    };

    // Close the detail page when is restricted by the parental control.
    private final ParentalControlManager.IParentalControlListener mParentalControlListener =
            parentalRating -> {
                if (mParentalControlManager.isRestricted(mSeries)
                        || mParentalControlManager.isRestricted(mEpisode)) {
                    mNavigator.clearAllDialogs();
                    mNavigator.getActivity().getSupportFragmentManager().popBackStack();
                }
            };
}
