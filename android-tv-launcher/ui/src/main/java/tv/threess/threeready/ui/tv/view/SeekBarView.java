package tv.threess.threeready.ui.tv.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.ViewConfiguration;

import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.PlaybackSettings;
import tv.threess.threeready.api.config.model.PlayerConfig;
import tv.threess.threeready.api.generic.helper.NumberUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.contract.SeekDirection;
import tv.threess.threeready.ui.tv.callback.SeekBarListener;

/**
 * SeekBarView extended from BaseSeekBarView. Responsible for some progress calculation and event generating.
 *
 * @author Denisa Trif
 * @since 2018.10.29
 */
public class SeekBarView extends BaseSeekBarView {
    public static final String TAG = Log.tag(SeekBarView.class);

    private final PlayerConfig mPlayerConfig = Components.get(PlayerConfig.class);
    private final PlaybackDetailsManager mPlaybackDetailsManager = Components.get(PlaybackDetailsManager.class);
    private final PlaybackSettings mPlaybackSettings = Components.get(PlaybackSettings.class);

    private final long mMinimumSeekingSpeed = mPlaybackSettings.getMinimumSeekingSpeed(TimeUnit.MILLISECONDS);
    private final long mMaximumSeekingSpeed = mPlaybackSettings.getMaximumSeekingSpeed(TimeUnit.MILLISECONDS);
    private final long mMaximumSeekingSpeedRewind = mPlaybackSettings.getMaximumSeekingSpeedRewind(TimeUnit.MILLISECONDS);
    private final long mSeekToEndDuration = mPlayerConfig.getSeekToEndDuration(TimeUnit.MILLISECONDS);
    private final int mSinglePressJumpFactor = mPlaybackSettings.getSinglePressJumpFactorPercentage();

    private volatile boolean mSeekingBlocked = false;
    private volatile boolean mBufferAlreadyOverflow = false;

    protected volatile long mLongPressTimestamp = 0;
    private volatile long mLongPressPosition = Long.MIN_VALUE;

    protected SeekBarListener mSeekBarListener;

    public SeekBarView(Context context) {
        this(context, null);
    }

    public SeekBarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SeekBarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean isMediaButton = true;
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_MEDIA_REWIND:
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                isMediaButton = false;
            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                if (isSeekingBlocked() || !isEnabled() || !mPlaybackDetailsManager.canSeek()) {
                    if (isSeekingBlocked()) {
                        mLongPressTimestamp = System.currentTimeMillis();
                    }
                    return true;
                }

                processSeeking(event);
                return isMediaButton;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        boolean isMediaButton = true;
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                isMediaButton = false;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
            case KeyEvent.KEYCODE_DPAD_LEFT:
                if (!isEnabled() || !mPlaybackDetailsManager.canSeek()) {
                    return true;
                }

                Log.d(TAG, "Finish seeking.");
                finishSeeking(event);
                return isMediaButton;
        }
        return super.onKeyUp(keyCode, event);
    }

    public boolean isSeekingBlocked() {
        return mSeekingBlocked;
    }

    public void blockSeeking() {
        mSeekingBlocked = true;
    }

    public void unblockSeeking() {
        mSeekingBlocked = false;
    }

    public boolean isSeekingInProgress() {
        return mLongPressTimestamp != 0;
    }

    /**
     * Returns the relative progress, 0 ... duration
     */
    public long getProgress() {
        return mPrimaryEnd - mProgressStart;
    }

    public void setProgressBarListener(SeekBarListener progressBarListener) {
        mSeekBarListener = progressBarListener;
    }

    /**
     * Set the bounds of the seek bar minimum and maximum values for display and seeking
     *
     * @param progressStart the start position of the seek bar
     * @param progressEnd the end position of the seek bar
     * @param seekAbleStart disable seeking below this value
     * @param seekAbleEnd   disable seeking above this value
     */
    public void setBounds(long progressStart, long progressEnd,
                          long seekAbleStart, long seekAbleEnd) {
        mProgressStart = progressStart;
        mProgressEnd = progressEnd;
        mSeekAbleStart = seekAbleStart;
        mSeekAbleEnd = seekAbleEnd;

        // Validate primary position with the new bounds.
        mPrimaryStart = NumberUtils.clamp(mPrimaryStart, progressStart, progressEnd);
        mPrimaryEnd = NumberUtils.clamp(mPrimaryEnd, progressStart, progressEnd);

        // Validate secondary position with the new bounds.
        mSecondaryStart = NumberUtils.clamp(mSecondaryStart, progressStart, progressEnd);
        mSecondaryEnd = NumberUtils.clamp(mSecondaryEnd, progressStart, progressEnd);

        if (mLongPressTimestamp != 0) {
            // reset long press position and timestamp to disable a big jump
            // in a short broadcast when switching from a very long one
            mLongPressTimestamp = System.currentTimeMillis();
            if (mLongPressPosition < mPrimaryEnd) {
                mLongPressPosition = mProgressStart;
            } else {
                mLongPressPosition = mProgressEnd;
            }
        }

        invalidate();
    }

    /**
     * Set the bounds of the seek bar minimum and maximum values for display and seeking
     * * @param progressStart the start position of the seek bar
     *
     * @param progressEnd the end position of the seek bar
     */
    public void setBounds(long progressStart, long progressEnd) {
        setBounds(progressStart, progressEnd, progressStart, progressEnd);
    }

    /**
     * Set the progress bar values
     *
     * @param primaryStart   seek bar primary start value
     * @param primaryEnd     seek bar primary end value (usually stream position)
     * @param secondaryStart seek bar secondary start value
     * @param secondaryEnd   seek bar secondary end value (usually current time)
     */
    private void setProgress(long primaryStart, long primaryEnd, long secondaryStart, long secondaryEnd) {
        long oldPrimaryEnd = mPrimaryEnd;
        mPrimaryStart = NumberUtils.clamp(primaryStart, mProgressStart, mProgressEnd);
        mPrimaryEnd = NumberUtils.clamp(primaryEnd, mProgressStart, mProgressEnd);
        if (mSeekBarListener != null && oldPrimaryEnd != mPrimaryEnd) {
            mSeekBarListener.onPrimaryProgressChanged(mPrimaryEnd, mProgressStart, mProgressEnd);
        }

        long oldSecondaryEnd = mSecondaryEnd;
        mSecondaryStart = NumberUtils.clamp(secondaryStart, mProgressStart, mProgressEnd);
        mSecondaryEnd = NumberUtils.clamp(secondaryEnd, mProgressStart, mProgressEnd);
        if (mSeekBarListener != null && oldSecondaryEnd != mSecondaryEnd) {
            mSeekBarListener.onSecondaryProgressChanged(mSecondaryEnd, mProgressStart, mProgressEnd);
        }
        invalidate();
    }

    /**
     * Sets end position of the primary seek bar
     *
     * @param value the end of the progress
     */
    public void setPrimaryProgress(long value) {
        setProgress(mProgressStart, value, mSecondaryStart, mSecondaryEnd);
    }

    /**
     * Sets end position of the secondary seek bar
     *
     * @param value the end of the progress
     */
    public void setSecondaryProgress(long value) {
        setProgress(mPrimaryStart, mPrimaryEnd, mProgressStart, value);
    }

    /**
     * Sets the seekable positions of the seek bar.
     */
    public void setSeekAblePositions(long seekAbleStart, long seekAbleEnd) {
        mSeekAbleStart = seekAbleStart;
        mSeekAbleEnd = seekAbleEnd;
    }

    /**
     * Calculates the duration of the jump for a key event while seeking.
     *
     * @param isLongPress True if the key event is coming from a long press.
     * @param direction   The direction of the seeking.
     */
    private long calculateSeekDuration(boolean isLongPress, SeekDirection direction) {
        long duration = getDuration();
        if (duration == 0) {
            return 0;
        }

        if (isLongPress) {
            // Long press: x sec through the whole content
            long seekDuration = System.currentTimeMillis() - mLongPressTimestamp;
            return (long) (seekDuration * getTrickplaySpeed(direction));
        } else {
            // Short press: seek by x% of the video
            long seekDuration = (duration * mSinglePressJumpFactor) / 100;
            if (direction == SeekDirection.REWIND) {
                return -NumberUtils.clamp(seekDuration, mMinimumSeekingSpeed, mMaximumSeekingSpeedRewind);
            }
            return NumberUtils.clamp(seekDuration, mMinimumSeekingSpeed, mMaximumSeekingSpeed);
        }
    }

    /**
     * Process the key event, calculate and set the next position on the seekbar.
     *
     * @param event The key event which needs ot be processed.
     */
    private void processSeeking(KeyEvent event) {
        long now = System.currentTimeMillis();
        SeekDirection direction = getSeekDirection(event);
        if (mLongPressTimestamp == 0 || event.getRepeatCount() == 1) {
            mLongPressTimestamp = now;
            if (mSeekBarListener != null) {
                mSeekBarListener.onStartSeeking(direction);
            }
            return;
        }

        if (mLongPressPosition == Long.MIN_VALUE) {
            // save the current position of the seek bar
            mLongPressPosition = mPrimaryEnd;
        }

        long seekDuration = calculateSeekDuration(isLongPress(event), direction);
        long position = mLongPressPosition + seekDuration;
        long newPrimaryEnd = NumberUtils.clamp(position,
                Math.max(mSeekAbleStart, mProgressStart),
                Math.min(mSeekAbleEnd, mProgressEnd));


        setPrimaryProgress(newPrimaryEnd);

        if (mBufferAlreadyOverflow) {
            return;
        }

        if (mSeekBarListener == null) {
            // Nothing to notify.
            return;
        }

        boolean progressOverflow = position < mProgressStart || position > mProgressEnd;
        if (position < mSeekAbleStart || position > mSeekAbleEnd) {
            mBufferAlreadyOverflow = true;
        }

        if (mBufferAlreadyOverflow || progressOverflow) {
            mSeekBarListener.onSeekingOverflow(direction, position, progressOverflow, mBufferAlreadyOverflow);
        }
    }

    private void finishSeeking(KeyEvent event) {
        if (mLongPressTimestamp == 0) {
            resetSeeking();
            return;
        }

        processSeeking(event);
        if (mSeekBarListener != null) {
            StartAction startAction = isRewind(event) ? StartAction.Rewind : StartAction.Forward;
            mSeekBarListener.onStopSeeking(startAction, mPrimaryEnd);
        }
        resetSeeking();
    }

    private static boolean isLongPress(KeyEvent event) {
        return event.getRepeatCount() > 0 ||
                event.getEventTime() - event.getDownTime() > ViewConfiguration.getLongPressTimeout();
    }

    public float getTrickplaySpeed(SeekDirection seekDirection) {
        float playbackSpeed = (float) getDuration() / mSeekToEndDuration;
        return seekDirection == SeekDirection.FORWARD ? playbackSpeed : -playbackSpeed;
    }

    public void resetSeeking() {
        mBufferAlreadyOverflow = false;
        mSeekingBlocked = false;
        mLongPressTimestamp = 0;
        mLongPressPosition = Long.MIN_VALUE;
    }

    /**
     * Determines the direction is rewind or forward from the key event
     */
    private static boolean isRewind(KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_MEDIA_REWIND:
            case KeyEvent.KEYCODE_DPAD_LEFT:
                return true;
        }
        return false;
    }

    /**
     * Determines the seek direction from the key event
     */
    private static SeekDirection getSeekDirection(KeyEvent event) {
        if (isRewind(event)) {
            return SeekDirection.REWIND;
        }
        return SeekDirection.FORWARD;
    }

    public boolean allowTrickPlay(KeyEvent event) {
        if (event.getRepeatCount() <= 0) {
            return false;
        }

        long position = mPrimaryEnd;
        long seekDuration = event.getEventTime() - event.getDownTime();
        position += (seekDuration * getTrickplaySpeed(getSeekDirection(event)));

        if (position < mSeekAbleStart || position > mSeekAbleEnd) {
            // trick-play is not allowed outside the buffer
            return false;
        }

        return this.isFocusable();
    }
}
