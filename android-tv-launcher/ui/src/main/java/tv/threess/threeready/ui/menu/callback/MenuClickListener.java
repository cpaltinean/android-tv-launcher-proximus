/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.ui.menu.callback;

import tv.threess.threeready.api.home.model.menu.MenuItem;

/**
 * Contains the item click methods for Main Menu and Submenu.
 *
 * Created by Szilard on 4/28/2017.
 */

public interface MenuClickListener {

    void onNotificationIconClick();

    void onSearchIconClick();

    void onSettingsIconClick();

    void onMenuItemClick(MenuItem menuItem);
}
