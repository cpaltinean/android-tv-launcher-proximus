package tv.threess.threeready.ui.startup.step;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.startup.Step;
import tv.threess.threeready.api.startup.StepCallback;
import tv.threess.threeready.api.tv.TvRepository;

/**
 * Startup step to start the EPG cache sync.
 *
 * @author Barabas Attila
 * @since 2019.06.07
 */
public class EpgSyncStep implements Step {

    @Override
    public void execute(StepCallback callback) {
        Components.get(TvRepository.class).startBroadcastUpdate();
        callback.onFinished();
    }

    @Override
    public void cancel() {
    }
}
