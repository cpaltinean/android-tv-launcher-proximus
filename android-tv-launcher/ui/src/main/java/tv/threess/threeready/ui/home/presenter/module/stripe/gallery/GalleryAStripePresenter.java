package tv.threess.threeready.ui.home.presenter.module.stripe.gallery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleEditorialStyle;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.ui.databinding.GalleryModulePresenterBinding;
import tv.threess.threeready.ui.generic.adapter.ArrayObjectAdapter;
import tv.threess.threeready.ui.generic.adapter.ModularItemBridgeAdapter;
import tv.threess.threeready.ui.generic.loader.DataSourceLoader;
import tv.threess.threeready.ui.generic.presenter.InterfacePresenterSelector;
import tv.threess.threeready.ui.home.presenter.module.BaseStripeModulePresenter;
import tv.threess.threeready.ui.tv.presenter.gallery.GalleryA1CardPresenter;

/**
 * Presenter used to display gallery type, A variant stripes.
 *
 * @author Eugen Guzyk
 * @since 2018.09.19
 */
public class GalleryAStripePresenter extends BaseStripeModulePresenter<GalleryAStripePresenter.ViewHolder> {
    private static final int Z_INDEX = 0;

    private final Map<ModuleCardVariant, PresenterSelector> mVariantCardPresenterMap = new HashMap<>();

    public GalleryAStripePresenter(Context context, DataSourceLoader dataSourceLoader) {
        super(context, dataSourceLoader);
        mVariantCardPresenterMap.put(ModuleCardVariant.A1, new InterfacePresenterSelector()
                .addClassPresenter(IContentItem.class, new GalleryA1CardPresenter(context))
        );
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent, RecyclerView.RecycledViewPool pool) {
        GalleryModulePresenterBinding binding = GalleryModulePresenterBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindHolder(ViewHolder holder, ModuleData<?> data) {
        super.onBindHolder(holder, data, null);
        Log.d(TAG, "onBindHolder() called with: holder = [" + holder
                + "], moduleConfig = [" + data.getModuleConfig() + "]");

        ModuleEditorialStyle moduleStyle = data.getModuleConfig().getModuleStyle();
        if (moduleStyle != null) {
            holder.mBinding.galleryStripeView.setBackgroundColor(moduleStyle.getBackgroundColor());
        }

        updateAdapter(holder, data);
    }

    @Override
    public void onUnbindHolder(ViewHolder holder) {
        super.onUnbindHolder(holder);

        holder.mBinding.galleryStripeView.mBinding.overviewGridView.setAdapter(null);
        holder.mBinding.galleryStripeView.mBinding.moduleGridView.setAdapter(null);
    }


    /**
     * Create a new or update the existing adapter for the module data.
     * @param holder The view holder of the collection.
     * @param moduleData The module data which needs to be displayed
     */
    private void updateAdapter(GalleryAStripePresenter.ViewHolder holder, ModuleData<?> moduleData) {
        // We only display the first item on gallery.
        List<IContentItem> items = (List) moduleData.getItems();
        if (!items.isEmpty()) {
            items = items.subList(0, 1);
        }

        if (holder.mBinding.galleryStripeView.mBinding.overviewGridView.getAdapter() != null) {
            ModularItemBridgeAdapter itemBridgeAdapter =
                    (ModularItemBridgeAdapter) holder.mBinding.galleryStripeView.mBinding.overviewGridView.getAdapter();
            ModuleConfig adapterModule = itemBridgeAdapter.getModuleData().getModuleConfig();
            if (Objects.equals(adapterModule, moduleData.getModuleConfig())) {
                ArrayObjectAdapter<IContentItem> objectAdapter = itemBridgeAdapter.getAdapter();
                objectAdapter.replaceAll(items);
                return;
            }
        }

        ArrayObjectAdapter<IContentItem> objectAdapter = new ArrayObjectAdapter<>(items);
        objectAdapter.setHasStableIds(true);

        PresenterSelector cardPresenterSelector = new InterfacePresenterSelector()
                .addClassPresenter(IBroadcast.class, new BroadcastGalleryOverviewPresenter(mContext, moduleData))
                .addClassPresenter(IBaseVodItem.class, new VodGalleryOverviewPresenter(mContext, moduleData));

        ModularItemBridgeAdapter itemBridgeAdapter = new ModularItemBridgeAdapter(
                moduleData, objectAdapter, cardPresenterSelector);
        holder.mBinding.galleryStripeView.mBinding.overviewGridView.setAdapter(itemBridgeAdapter);
    }


    @Override
    public int getZIndex() {
        return Z_INDEX;
    }

    protected PresenterSelector getCardPresenterSelector(final ModuleConfig moduleConfig) {
        return mVariantCardPresenterMap.get(moduleConfig.getCardVariant());
    }

    static class ViewHolder extends Presenter.ViewHolder {

        private final GalleryModulePresenterBinding mBinding;

        ViewHolder(GalleryModulePresenterBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }

}
