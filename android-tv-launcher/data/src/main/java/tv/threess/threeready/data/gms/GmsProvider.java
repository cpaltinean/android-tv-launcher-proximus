/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.gms;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.helper.UriMatcher;
import tv.threess.threeready.data.generic.BaseProvider;

/**
 * Content provider to access persisted data.
 *
 * @author Eugen Guzyk
 * @since 2018.12.19
 */
public class GmsProvider extends BaseProvider {

    enum UriIDs {
        Notification(GmsContract.StoredNotification.TABLE_NAME),
        Apps(GmsContract.Apps.TABLE_NAME);

        UriIDs(String table) {
            mTable = table;
        }

        final String mTable;
    }

    // prepare the uri matcher
    private static final UriMatcher<UriIDs> matcher = new UriMatcher<>(GmsContract.AUTHORITY, UriIDs.class)
            .add(UriIDs.Notification, GmsContract.StoredNotification.CONTENT_URI)
            .add(GmsProvider.UriIDs.Apps, GmsContract.Apps.CONTENT_URI);

    private GmsDatabase mDatabase;

    @Override
    public boolean onCreate() {
        super.onCreate();
        // prepare the database helper
        mDatabase = new GmsDatabase(this.getContext());
        return true;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        UriIDs match = matcher.matchEnum(uri);
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);

            case Notification:
                return GmsContract.StoredNotification.MIME_TYPE;
            case Apps:
                return GmsContract.Apps.MIME_TYPE;

        }
    }

    @Override
    public Bundle call(String method, String arg, Bundle extras) {
        if (DATABASE_VERSION_CHECK.equals(method)) {
            mDatabase.getReadableDatabase();
        }
        return super.call(method, arg, extras);
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        UriIDs match = matcher.matchEnum(uri);
        SQLiteDatabase db = mDatabase.getReadableDatabase();
        return super.query(uri, db, match.mTable, projection, selection, selectionArgs, sortOrder);
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        UriIDs match = matcher.matchEnum(uri);
        int onConflict = SQLiteDatabase.CONFLICT_NONE;
        switch (match) {
            case Apps:
            case Notification:
                onConflict = SQLiteDatabase.CONFLICT_REPLACE;
                break;
        }

        long rowId = super.insert(uri, mDatabase.getWritableDatabase(), match.mTable, values, onConflict);
        return rowId < 0 ? null : uri;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        UriIDs match = matcher.matchEnum(uri);

        return super.delete(uri,
                mDatabase.getWritableDatabase(), match.mTable, selection, selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        UriIDs match = matcher.matchEnum(uri);

        return super.update(uri, mDatabase.getWritableDatabase(), match.mTable, values, selection, selectionArgs);
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        UriIDs match = matcher.matchEnum(uri);

        String replace = uri.getQueryParameter(BaseContract.BULK_REPLACE);
        return bulkInsertReplacePurge(uri,
                mDatabase.getWritableDatabase(), match.mTable, replace, values);
    }

    @NonNull
    @Override
    public ContentProviderResult[] applyBatch(@NonNull ArrayList<ContentProviderOperation> operations) throws OperationApplicationException {
        return super.applyBatch(mDatabase.getWritableDatabase(), operations);
    }
}
