package tv.threess.threeready.data.config.observable;

import android.content.Context;
import android.net.Uri;

import java.io.IOException;

import io.reactivex.ObservableEmitter;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.model.IParentalControlSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.account.model.ParentalControlSettingsDBModel;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;

/**
 * Rx observable which emits the parental control settings.
 *
 * @author Barabas Attila
 * @since 12/7/20
 */
public class ParentalControlSettingsObservable extends BaseContentObservable<IParentalControlSettings> {
    private static final String TAG = Log.tag(ParentalControlSettingsObservable.class);

    public ParentalControlSettingsObservable(Context context) {
        super(context);
    }

    @Override
    public void subscribe(ObservableEmitter<IParentalControlSettings> emitter) throws Exception {
        super.subscribe(emitter);
        publishParentalSettings(emitter);

        registerObserver(
                ConfigContract.buildSettingsUriForKey(Settings.enabledParentalControl.getName()),
                ConfigContract.buildSettingsUriForKey(Settings.minAgeRating.getName()));
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<IParentalControlSettings> emitter) throws IOException {
        Log.d(TAG, "OnChange : " + uri);
        publishParentalSettings(emitter);
    }

    private void publishParentalSettings(ObservableEmitter<IParentalControlSettings> emitter) throws IOException {
        IParentalControlSettings settings = new ParentalControlSettingsDBModel(
                Settings.enabledParentalControl.get(false),
                Settings.minAgeRating.getParentalRating(ParentalRating.RatedAll));

        Log.d(TAG, "New parental control settings : " + settings.isEnabled());
        emitter.onNext(settings);
    }
}
