package tv.threess.threeready.data.account.adapter;

import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.home.model.local.FontType;

/**
 * Json type adapter to read font type as enum.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public class FontTypeAdapter3Ready extends NameTypeMappingTypeAdapter<FontType> {

    @Override
    protected Map<String, FontType> createNameTypeMap() {
        return new HashMap<String, FontType>() {
            {
                put("regular", FontType.REGULAR);
                put("regular_italic", FontType.REGULAR_ITALIC);
                put("bold", FontType.BOLD);
                put("extra_bold", FontType.EXTRA_BOLD);
                put("bold_italic", FontType.BOLD_ITALIC);
                put("light", FontType.LIGHT);
                put("light_italic", FontType.LIGHT_ITALIC);
            }
        };
    }

    @Nullable
    @Override
    public FontType getDefaultValue() {
        return FontType.REGULAR;
    }
}
