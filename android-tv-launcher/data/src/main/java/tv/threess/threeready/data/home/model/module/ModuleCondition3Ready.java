/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.home.model.module;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import tv.threess.threeready.api.home.model.module.ModuleCondition;
import tv.threess.threeready.api.home.model.module.ModuleConditionMethod;
import tv.threess.threeready.data.account.adapter.ModuleConditionMethodTypeAdapter3Ready;
import tv.threess.threeready.data.account.adapter.ModuleDataSourceFilterJsonAdapter3Ready;
import tv.threess.threeready.data.home.model.generic.ModuleFilter3Ready;

/**
 * Define the conditions to show or not the stripe / tile
 */
public class ModuleCondition3Ready implements ModuleCondition {

    @SerializedName("method")
    @JsonAdapter(ModuleConditionMethodTypeAdapter3Ready.class)
    private ModuleConditionMethod mMethod;

    @SerializedName("filter")
    @JsonAdapter(ModuleDataSourceFilterJsonAdapter3Ready.class)
    private ModuleFilter3Ready mFilter;

    @Override
    public ModuleConditionMethod getMethod() {
        return mMethod;
    }

    @Override
    public String getFilter(String key) {
        return mFilter == null ? null : mFilter.getFilter(key, String.class);
    }

    @Override
    public String toString() {
        return "ModuleDataCondition{" +
                "mMethod='" + mMethod + '\'' +
                ", mFilter=" + mFilter +
                '}';
    }
}
