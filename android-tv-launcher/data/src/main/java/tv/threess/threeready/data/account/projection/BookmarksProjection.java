package tv.threess.threeready.data.account.projection;

import android.database.Cursor;

import tv.threess.threeready.api.generic.model.BookmarkType;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.account.model.BookmarkDBModel;
import tv.threess.threeready.data.generic.IProjection;

/**
 * Projection to get the bookmark.
 *
 * @author Daniela Toma
 * @since 2020.04.28
 */
public enum BookmarksProjection implements IProjection {
    CONTENT_ID(ConfigContract.Bookmarks.CONTENT_ID),
    TYPE(ConfigContract.Bookmarks.TYPE),
    POSITION(ConfigContract.Bookmarks.POSITION),
    DURATION(ConfigContract.Bookmarks.DURATION),
    LAST_UPDATED(ConfigContract.Bookmarks.LAST_UPDATED);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    BookmarksProjection(String column) {
        mColumn = column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    public static IBookmark readRow(Cursor cursor) {
        return BookmarkDBModel.Builder.builder()
            .setContentId(cursor.getString(CONTENT_ID.ordinal()))
            .setType(BookmarkType.valueOf(cursor.getString(TYPE.ordinal())))
            .setPosition(cursor.getLong(POSITION.ordinal()))
            .setDuration(cursor.getLong(DURATION.ordinal()))
            .setLastUpdatedDate(cursor.getLong(LAST_UPDATED.ordinal()))
            .build();
    }
}