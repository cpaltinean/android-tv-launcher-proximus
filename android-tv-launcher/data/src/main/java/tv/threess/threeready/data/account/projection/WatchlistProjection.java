package tv.threess.threeready.data.account.projection;

import android.database.Cursor;

import tv.threess.threeready.api.generic.model.IWatchlistItem;
import tv.threess.threeready.api.generic.model.WatchlistType;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.account.model.WatchlistItemDBModel;
import tv.threess.threeready.data.generic.IProjection;

/**
 * Projection for watchlist content
 *
 * @author Andor Lukacs
 * @since 04/11/2020
 */
public enum WatchlistProjection implements IProjection {
    ID(ConfigContract.Watchlist.WATCHLIST_ID),
    CONTENT_ID(ConfigContract.Watchlist.CONTENT_ID),
    TYPE(ConfigContract.Watchlist.TYPE),
    ADDED_AT(ConfigContract.Watchlist.ADDED_AT);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    WatchlistProjection(String column) {
        mColumn = column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    public static IWatchlistItem readRow(Cursor cursor) {
        WatchlistItemDBModel.Builder builder = WatchlistItemDBModel.Builder.builder()
                .setId(cursor.getString(ID.ordinal()))
                .setContentId(cursor.getString(CONTENT_ID.ordinal()))
                .setType(WatchlistType.valueOf(cursor.getString(TYPE.ordinal())))
                .setAddedAt(cursor.getLong(ADDED_AT.ordinal()));

        return builder.build();
    }
}
