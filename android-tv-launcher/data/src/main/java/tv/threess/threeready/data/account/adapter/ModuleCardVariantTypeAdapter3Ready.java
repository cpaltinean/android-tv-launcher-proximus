package tv.threess.threeready.data.account.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import tv.threess.threeready.api.home.model.module.ModuleCardVariant;

/**
 * Json type adapter to read the module card variants as enum.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public class ModuleCardVariantTypeAdapter3Ready extends TypeAdapter<ModuleCardVariant> {

    @Override
    public void write(JsonWriter out, ModuleCardVariant value) throws IOException {
        out.value(value.name().toLowerCase());
    }

    @Override
    public ModuleCardVariant read(JsonReader in) throws IOException {
        String value = in.nextString();
        for (ModuleCardVariant variant : ModuleCardVariant.values()) {
            if (variant.name().equalsIgnoreCase(value)) {
                return variant;
            }
        }

        return ModuleCardVariant.UNDEFINED;
    }
}
