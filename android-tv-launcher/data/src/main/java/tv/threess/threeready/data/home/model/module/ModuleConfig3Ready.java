/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.home.model.module;

import androidx.annotation.Nullable;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.api.home.model.module.ModuleCardVariant;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleDataSourcePaginationParams;
import tv.threess.threeready.api.home.model.module.ModuleDataType;
import tv.threess.threeready.api.home.model.module.ModuleEditorialStyle;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.api.home.model.module.ModuleType;
import tv.threess.threeready.data.account.adapter.ModuleCardVariantTypeAdapter3Ready;
import tv.threess.threeready.data.account.adapter.ModuleDataTypeAdapter3Ready;
import tv.threess.threeready.data.account.adapter.ModuleTypeAdapter3Ready;

/**
 * Configuration class for a user interface module.
 * A module is an independent element which can be placed on a page.
 *
 * Created by Barabas Attila on 4/12/2017.
 */

public class ModuleConfig3Ready implements ModuleConfig {

    @SerializedName("title")
    private String mTitle;

    @JsonAdapter(ModuleTypeAdapter3Ready.class)
    @SerializedName("module")
    private ModuleType mType;

    @SerializedName("icon")
    private String mIcon;

    @JsonAdapter(ModuleDataTypeAdapter3Ready.class)
    @SerializedName("type")
    private ModuleDataType mDataType;

    @JsonAdapter(ModuleCardVariantTypeAdapter3Ready.class)
    @SerializedName("variant")
    private ModuleCardVariant mCardVariant;

    @SerializedName("style")
    private ModuleEditorialStyle3Ready mModuleStyle;

    @SerializedName("action")
    private EditorialItemAction3Ready mItemAction;

    @SerializedName("items")
    private final List<EditorialItem3Ready> mItems = new ArrayList<>();

    @SerializedName("datasource")
    private ModuleDataSource3Ready mDataSource;

    @SerializedName("conditions")
    private ModuleCondition3Ready[] mConditions;

    @SerializedName("min_elements")
    private int mMinElements;

    @SerializedName("selector")
    private List<ModuleDataSourceSelector3Ready> mSelectors;

    private ModuleConfig3Ready() {
    }

    @Nullable
    @Override
    public String getId() {
        if (mDataSource != null && mDataSource.getParams() != null) {
            return mDataSource.getParams().getFilter(ModuleFilterOption.Mixed.SwimLaneId.NAME);
        }
        return null;
    }

    @Override
    public String getReportName() {
        return mTitle;
    }

    @Override
    public String getReportReference() {
        return mDataType.name();
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public ModuleType getType() {
        if (mType == null) {
            return ModuleType.COLLECTION;
        }
        return mType;
    }

    @Override
    public String getIcon() {
        return mIcon;
    }

    @Override
    public ModuleDataType getDataType() {
        return mDataType;
    }

    @Override
    public ModuleCardVariant getCardVariant() {
        return mCardVariant;
    }

    @Override
    public List<EditorialItem> getEditorialItems() {
        return (List) mItems;
    }

    @Override
    public ModuleEditorialStyle getModuleStyle() {
        return mModuleStyle;
    }

    @Override
    public EditorialItemAction getEditorialItemAction() {
        return mItemAction;
    }

    @Override
    public ModuleDataSource3Ready getDataSource() {
        return mDataSource;
    }

    @Override
    public ModuleCondition3Ready[] getConditions() {
        return mConditions;
    }

    @Override
    public List<SelectorOption> getSelectorList() {
        return (List) mSelectors;
    }

    /**
     * Return the number of cards.
     */
    @Override
    public int getSize(int defaultSize) {
        int size = defaultSize;
        if (getDataSource() != null && getDataSource().getParams() != null &&
                getDataSource().getParams().getSize() != null && getDataSource().getParams().getSize().length > 0) {
            size = getDataSource().getParams().getSize()[0];
        }
        return size;
    }

    @Override
    public int getMinElements() {
        if (!ArrayUtils.isEmpty(mSelectors)) {
            return 0;
        }
        return mMinElements > 0 ? mMinElements : 1;
    }

    @Override
    public ModuleDataSourcePaginationParams getPagination() {
        if (mDataSource != null && mDataSource.getParams() != null) {
            return mDataSource.getParams().getPagination();
        }
        return null;
    }

    @Override
    public String toString() {
        return "ModuleConfig{" +
                " title='" + mTitle + '\'' +
                ", type=" + mDataType +
                ", variant=" + mType +
                ", datasource=" + mDataSource +
                ", conditions=" + ArrayUtils.toString(mConditions) +
                '}';
    }
}
