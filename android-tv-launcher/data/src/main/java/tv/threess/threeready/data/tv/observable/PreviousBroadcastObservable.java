package tv.threess.threeready.data.tv.observable;

import android.content.Context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.ObservableEmitter;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.data.tv.model.NoInfoBroadcast;
import tv.threess.threeready.data.tv.model.TvBroadcastDBModel;

/**
 * Rx observable which will emit the broadcast previous to the give one.
 *
 * @author Barabas Attila
 * @since 2020.08.08
 */
public class PreviousBroadcastObservable extends BroadcastsBetweenObservable<IBroadcast> {
    private final IBroadcast mCurrentBroadcast;

    public PreviousBroadcastObservable(Context context, IBroadcast broadcast, boolean isAdultChannel) {
        super(context,  broadcast.getChannelId(),
                broadcast.getStart() - TimeUnit.HOURS.toMillis(1), broadcast.getStart(), isAdultChannel);
        mCurrentBroadcast = broadcast;
    }

    @Override
    protected void publishPrograms(ObservableEmitter<IBroadcast> emitter, IBroadcast programs, boolean fromNotify) {
        if (programs != null) {
            emitter.onNext(programs);
            if (!(programs instanceof NoInfoBroadcast)) {
                emitter.onComplete();
            }
        }
    }

    @Override
    IBroadcast getPublishValue(List<IBroadcast> broadcasts) {
        IBroadcast broadcast = ArrayUtils.first(broadcasts);
        if (broadcast != null && broadcast.getEnd() < mCurrentBroadcast.getStart()) {
            if (broadcast instanceof TvBroadcastDBModel) {
                // broadcast is obtained from database, there will be a next publish with downloaded data
                return null;
            }
            broadcast = createDummyProgram(Math.max(broadcast.getEnd(), mFrom), mTo, mDummyNoInfoProgramTitle);
        }
        return broadcast;
    }

    @Override
    List<IBroadcast> getBroadcastsListFromDatabase() {
        return mTvCache.getPreviousBroadcasts(mChannelId, mTo);
    }

    @Override
    protected List<IBroadcast> getBroadcastsListFromBackend() throws IOException {
        // getPublishValue checks the first broadcast, so use 1 millisecond in the past
        return new ArrayList<>(mTvProxy.getBroadcasts(mChannelId, mTo - 1, mTo).getBroadcasts());
    }
}
