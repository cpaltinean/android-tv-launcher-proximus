/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.account;


import java.io.IOException;
import java.util.List;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.account.model.account.IConsent;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.tv.epg.EpgLayoutType;

/**
 * Proxy (pass-through) interface to access config related backend data.
 *
 * @author Barabas Attila
 * @since 2017.04.20
 */
public interface AccountProxy extends Component {

    /**
     * Active or deactivate the parental control.
     *
     * @param enable True if the parental control should be activated.
     */
    void enableParentalControl(boolean enable) throws IOException;

    /**
     * Register the current device to the account.
     *
     * @throws IOException In case of errors.
     */
    void registerDevice(String linenumber, String ipAddress,
                        String serialNum, String accountPIN,
                        String hardwareVersion, String softwareVersion,
                        String androidSerialNumber, String ethernetMACAddress) throws IOException;

    /**
     * @param requestScenario Starting point of the request. E.g. boot screen / setting screen...
     * @return All consents from the microservice for the given scenario.
     * @throws IOException In case of errors.
     */
    List<? extends IConsent> getConsents(String requestScenario) throws IOException;

    /**
     * @param consentId Id of the consent to be returned.
     * @return Consent with the given ID.
     * @throws IOException In case of errors.
     */
    IConsent getConsent(String consentId) throws IOException;

    /**
     * @return The image for the image QR code.
     * @throws IOException In case of errors.
     */
    IImageSource getConsentQrCode() throws IOException;

    /**
     * @param consentId  Id of the consent to be updated.
     * @param isAccepted New state of the consent which will be sent to backend.
     * @throws IOException In case of errors.
     */
    void setConsent(String consentId, boolean isAccepted) throws IOException;

    /**
     * @return token that uniquely identifies device used for Netflix authentication.
     */
    String getNetflixAuthenticationToken(String hardwareSerialNumber) throws IOException;

    /**
     * Changes parental control pin with the specified value.
     *
     * @param pin The value that is set as parental control pin.
     */
    boolean changeParentalControlPIN(String pin) throws IOException;

    /**
     * Set a new purchase PIN which will be used for VOD renting.
     *
     * @param purchasePIN The new purchase Pin.
     */
    boolean changePurchasePIN(String purchasePIN) throws IOException;

    /**
     * Sets parental age rating.
     *
     * @param parentalRating The value that will be set as parental age rating.
     */
    void setParentalRating(ParentalRating parentalRating) throws IOException;

    /**
     * @param shopLanguage The preferred shop language
     * @throws IOException in case of errors (BTA/HTTP)
     */
    void setShopLanguage(String shopLanguage) throws IOException;

    /**
     * @param audioLanguages The preferred audio language
     * @throws IOException in case of errors (BTA/HTTP)
     */
    void setAudioLanguage(String audioLanguages) throws IOException;

    /**
     * @param subtitleLanguages The preferred subtitle language
     * @throws IOException in case of errors (BTA/HTTP)
     */
    void setSubtitleLanguage(String subtitleLanguages) throws IOException;

    /**
     * @param favoriteChannels A String with all favorite channel id's joined by ','.
     * @throws IOException in case of errors (BTA/HTTP)
     */
    void updateFavoriteChannels(String favoriteChannels) throws IOException;

    /**
     * @throws IOException in case of errors (BTA/HTTP)
     */
    void setPowerConsumptionStatus(boolean status) throws IOException;

    /**
     * @param epgLayout The preferred epg layout.
     * @throws IOException in case of errors (BTA/HTTP)
     */
    void setEpgLayout(EpgLayoutType epgLayout) throws IOException;

    /**
     * @param hintIds the ids of the hints that were viewed by user. The ids are separately by ",".
     * @throws IOException in case of errors (BTA/HTTP)
     */
    void setViewedHints(String hintIds) throws IOException;

    /**
     * @throws IOException in case of errors (BTA/HTTP)
     */
    void deleteViewedHints() throws IOException;
}
