/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.generic.observable;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Cancellable;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Rx observable which can be notified about data source changes via broadcast.
 * The broadcast receiver is automatically unregistered when the subscriber unsubscribes.
 *
 * @author Barabas Attila
 * @since 2017.05.23
 */
public abstract class BaseBroadcastObservable<T> implements ObservableOnSubscribe<T> {
    private static final String TAG = Log.tag(BaseBroadcastObservable.class);

    protected final Context mContext;
    protected ObservableEmitter<T> mEmitter;
    private boolean mIsRegistered;

    private static final Handler mHandler = BaseContentObservable.sWorkerHandler;

    public BaseBroadcastObservable(Context context) {
        mContext = context;
    }

    @Override
    public void subscribe(ObservableEmitter<T> emitter) throws Exception {
        mEmitter = emitter;

        // Unregister broadcast receiver when the subscription disposed or canceled.
        mEmitter.setCancellable(mCancellable);
    }


    protected abstract void onBroadcastReceived(Intent intent);

    /**
     * Register a broadcast receiver with the given intent filter.
     */
    protected void registerReceiver(IntentFilter intentFilter) {
        synchronized (this) {
            if (!mIsRegistered && mEmitter != null && !mEmitter.isDisposed()) {
                Log.d(TAG, "Register broadcast receiver.");
                mContext.registerReceiver(mBroadcastReceiver, intentFilter, null, mHandler);
                mIsRegistered = true;
            }
        }
    }

    /**
     * Unregister the previously registered broadcast receiver.
     */
    protected void unregisterReceiver() {
        synchronized (this) {
            if (mIsRegistered) {
                Log.d(TAG, "Unregister broadcast receiver.");
                mContext.unregisterReceiver(mBroadcastReceiver);
                mIsRegistered = false;
            }
        }
    }

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (RxUtils.isDisposed(mEmitter)) {
                Log.e(TAG, "Couldn't handle broadcast. Emitter already disposed. " + intent);
                return;
            }

            try {
                BaseBroadcastObservable.this.onBroadcastReceived(intent);
            } catch (Exception e) {
                Log.e(TAG, "Failed to handle the received broadcast.", e);
                mEmitter.onError(e);
            }
        }
    };

    private final Cancellable mCancellable = new Cancellable() {
        @Override
        public void cancel() {
            synchronized (this) {
                unregisterReceiver();
                mEmitter = null;
            }
        }
    };
}
