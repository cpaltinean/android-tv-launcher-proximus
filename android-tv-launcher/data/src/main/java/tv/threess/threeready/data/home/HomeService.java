package tv.threess.threeready.data.home;

import android.content.Context;

import androidx.work.Data;
import androidx.work.WorkerParameters;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.HomeServiceRepository;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.generic.BaseWorker;
import tv.threess.threeready.data.generic.JobIntent;

/**
 * Intent service for translation and client configuration background updates.
 *
 * @author Barabas Attila
 * @since 4/20/22
 */
public class HomeService {

    public static JobIntent buildUpdateConfigIntent() {
        return new JobIntent(ClientConfigUpdater.class, Data.EMPTY);
    }

    public static class ClientConfigUpdater extends BaseWorker {

        public ClientConfigUpdater(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            HomeServiceRepository repository = Components.get(HomeServiceRepository.class);
            Exception error = null;

            try {
                repository.updateClientConfig();
            } catch (Exception e) {
                error = e;
            }

            repository.updateCachedTranslations();

            if (error != null) {
                throw error;
            }
        }
    }
}
