package tv.threess.threeready.data.pvr;

import android.app.Application;
import android.content.ContentResolver;
import android.content.ContentValues;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.TimerLog;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.pvr.model.ISeriesRecordingSchedule;
import tv.threess.threeready.api.pvr.model.SeriesRecordingStatus;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.data.pvr.model.RecordingSeriesDbModel;
import tv.threess.threeready.data.pvr.projections.GroupedRecordingProjection;
import tv.threess.threeready.data.pvr.projections.RecordingsProjection;
import tv.threess.threeready.data.pvr.projections.SeriesRecordingScheduleProjection;
import tv.threess.threeready.data.utils.SqlUtils;

/**
 * Cache implementation for reading/writing PVR domain related data from/in the locale data base.
 *
 * @author David
 * @since 16.11.2021
 */
public class PvrCache implements Component {
    private static final String TAG = Log.tag(PvrCache.class);

    private static final long REC_AVAILABILITY_MILLISECONDS = TimeUnit.DAYS.toMillis(60);

    private final Application mApp;

    public PvrCache(Application application) {
        mApp = application;
    }

    /**
     * @param broadcastId The identifier of the broadcast for which the recording should be returned.
     * @return The recording for the broadcast or throws exception the recording doesn't exists.
     */
    @Nullable
    public IRecording getRecording(String broadcastId) {
        String groupBySelection = PvrContract.Recording.BROADCAST_ID + " =? " +
                " AND " + PvrContract.Recording.RECORDING_END + " + " + REC_AVAILABILITY_MILLISECONDS + " > " + System.currentTimeMillis() +
                " GROUP BY " + PvrContract.Recording.BROADCAST_ID +
                " HAVING (" + PvrContract.Recording.RECORDING_END + " - " +
                PvrContract.Recording.RECORDING_START + ") = MAX (" +
                PvrContract.Recording.RECORDING_END + "-" + PvrContract.Recording.RECORDING_START + ");";

        return getRecordingByQueryParams(groupBySelection, new String[]{broadcastId});
    }

    /**
     * @param broadcastId The unique identifier of the broadcast for which the recording needs to be returned.
     * @return The continue watching recording for the given broadcast id.
     */
    @Nullable
    public IRecording getContinueWatchingRecording(String broadcastId) {
        String selection = PvrContract.Recording.BROADCAST_ID + " =?";

        return getRecordingByQueryParams(selection, new String[]{broadcastId});
    }

    /**
     * Gets the recordings for the specified broadcast id.
     *
     * @param broadcastId The identifier of the broadcast for which the recording should be returned.
     * @return The recording for the broadcast or throws exception the recording doesn't exists.
     */
    public List<IRecording> getRecordings(String broadcastId) {
        String selection = PvrContract.Recording.BROADCAST_ID + " =? " +
                " AND " + PvrContract.Recording.RECORDING_END + " + " + REC_AVAILABILITY_MILLISECONDS + " > " + System.currentTimeMillis();

        return getRecordingsByQueryParams(selection, new String[]{broadcastId}, null);
    }

    /**
     * @param seriesId The id of teh series for which the recording should be returned.
     * @return Series with all the available recording episode for the broadcast.
     * If exist more than one recordings from the same broadcast (multiple items, recordings)
     * than will be returned a list of all available recordings and with the item that has the
     * longest duration from the multiple items.
     */
    @Nullable
    public IRecordingSeries getSeriesRecording(String seriesId) {
        String[] selectionArgs = {seriesId};

        String groupBySelection = PvrContract.Recording.SERIES_ID + " =? " +
                " AND " + PvrContract.Recording.RECORDING_END + " + " + REC_AVAILABILITY_MILLISECONDS + " > " + System.currentTimeMillis() +
                " GROUP BY " + PvrContract.Recording.BROADCAST_ID +
                " HAVING (" + PvrContract.Recording.RECORDING_END + " - " +
                PvrContract.Recording.RECORDING_START + ") = MAX (" +
                PvrContract.Recording.RECORDING_END + "-" + PvrContract.Recording.RECORDING_START + ")";

        String sortOrder = PvrContract.Recording.BROADCAST_START + " ASC";

        List<IRecording> episodes = getRecordingsByQueryParams(groupBySelection, selectionArgs, sortOrder);
        return !ArrayUtils.isEmpty(episodes) ? new RecordingSeriesDbModel(episodes) : null;
    }

    /**
     * @param seriesId Series id.
     * @return The series recording state for given series id.
     */
    public SeriesRecordingStatus getSeriesStatusById(String seriesId) {
        String selection = PvrContract.SeriesRecording.SERIES_ID + " =? ";
        SeriesRecordingStatus seriesRecordingStatus = SqlUtils.queryFirst(mApp,
                PvrContract.SeriesRecording.CONTENT_URI,
                SeriesRecordingScheduleProjection.PROJECTION,
                selection,
                new String[]{seriesId},
                cursor -> SeriesRecordingStatus.SCHEDULED
        );

        return seriesRecordingStatus != null ? seriesRecordingStatus : SeriesRecordingStatus.NONE;
    }

    /**
     * @return recordings on the same broadcast.
     */
    public List<String> getRecordingsOnSameBroadcast(IRecording recording) {
        String selection = PvrContract.Recording.BROADCAST_ID + " =? " +
                " AND " + PvrContract.Recording.RECORDING_END + " + " + REC_AVAILABILITY_MILLISECONDS + " > " + System.currentTimeMillis();
        String[] projection = {PvrContract.Recording.RECORDING_ID};

        return SqlUtils.queryFirst(mApp,
                PvrContract.Recording.CONTENT_URI,
                projection,
                selection,
                new String[]{recording.getId()},
                RecordingsProjection::getRecIdsFromCursor
        );
    }

    /**
     * Get all the series recordings by a series id.
     */
    public List<IRecording> getRecordingsBySeriesId(String seriesId) {
        String selection = PvrContract.Recording.SERIES_ID + " =? " +
                " AND " + PvrContract.Recording.RECORDING_END + " + " + REC_AVAILABILITY_MILLISECONDS
                + " > " + System.currentTimeMillis();

        return getRecordingsByQueryParams(selection, new String[]{seriesId}, null);
    }

    /**
     * @return the user series id.
     */
    @Nullable
    public String getUserSeriesIdBySeriesId(String seriesId) {
        String selection = PvrContract.SeriesRecording.SERIES_ID + "=?";
        return SqlUtils.queryFirst(mApp,
                PvrContract.SeriesRecording.CONTENT_URI,
                SeriesRecordingScheduleProjection.PROJECTION,
                selection,
                new String[]{seriesId},
                cursor -> {
                    ISeriesRecordingSchedule series = SeriesRecordingScheduleProjection.readRow(cursor);
                    return series.getUserSeriesId();
                }
        );
    }

    /**
     * Group and sort the scheduled recordings and create a module data source from them.
     *
     * @param start The pagination start position of of the data source.
     * @param end   The pagination end position of the data source.
     * @return The data source which holds the grouped recordings.
     */
    public List<IBaseContentItem> getScheduledData(int start, int end) {
        // Group series by series id when available.
        String selection = PvrContract.Recording.RECORDING_START + " > " + System.currentTimeMillis()
                + " GROUP BY IFNULL(" + PvrContract.Recording.SERIES_ID + ", " + PvrContract.Recording.BROADCAST_ID + ")";
        // Sort the group recordings so the near feature are first.
        String sortOrder = GroupedRecordingProjection.MIN_RECORDING_START.getAlias() + " ASC LIMIT " + start + ", " + (end - start);

        return SqlUtils.queryAll(mApp,
                PvrContract.Recording.CONTENT_URI,
                GroupedRecordingProjection.PROJECTION,
                selection,
                null,
                sortOrder,
                GroupedRecordingProjection::readRow
        );
    }

    /**
     * Group and sort the ongoing and completed recordings and create a module data source from them.
     *
     * @param start The pagination start position of of the data source.
     * @param end   The pagination end position of the data source.
     * @return The data source which holds the grouped recordings.
     */
    public List<IBaseContentItem> getRecordedData(int start, int end) {
        // Group series by series id when available.
        String selection = PvrContract.Recording.RECORDING_START + " < " + System.currentTimeMillis()
                + " AND " + PvrContract.Recording.RECORDING_END + " + " + REC_AVAILABILITY_MILLISECONDS + " > " + System.currentTimeMillis()
                + " GROUP BY IFNULL(" + PvrContract.Recording.SERIES_ID + ", " + PvrContract.Recording.BROADCAST_ID + ")";
        // Sort the group recordings so the most recants are first.
        String sortOrder = GroupedRecordingProjection.MAX_RECORDING_START.getAlias() + " DESC LIMIT " + start + ", " + (end - start);

        return SqlUtils.queryAll(mApp,
                PvrContract.Recording.CONTENT_URI,
                GroupedRecordingProjection.PROJECTION, selection, null, sortOrder,
                GroupedRecordingProjection::readRow
        );
    }

    /**
     * @param recording Recording item, which needs to be updated
     * @param end       The end of the recording in millis.
     */
    public void updateRecordingEnd(IRecording recording, long end) {
        ContentValues contentValues = RecordingsProjection.toContentValues(recording);
        contentValues.put(PvrContract.Recording.RECORDING_END, end);
        String selection = PvrContract.Recording.RECORDING_ID + " =? ";

        mApp.getContentResolver().update(BaseContract.withBatchUpdate(PvrContract.Recording.CONTENT_URI),
                contentValues, selection, new String[]{recording.getRecordingId()});

        notifyByRecordingBroadcast(recording);
    }

    /**
     * Get the recordings from backend and store them in the local cache.
     */
    public void updateRecordingList(List<? extends IRecording> recordingList) {
        TimerLog timerLog = new TimerLog(TAG);

        long now = System.currentTimeMillis();
        timerLog.d("Recording list updated.");

        List<ContentValues> recordingValues = new ArrayList<>();

        if (recordingList != null && !recordingList.isEmpty()) {
            for (IRecording recording : recordingList) {

                if (recording.getAvailabilityTime() < System.currentTimeMillis()) {
                    // Filter out recordings which are not available.
                    continue;
                }

                recordingValues.add(RecordingsProjection.toContentValues(recording));
            }

            timerLog.d("Recording list updated.");
        } else {
            Log.d(TAG, "Recording list is null or empty");
        }

        ContentResolver cr = mApp.getContentResolver();
        SqlUtils.bulkReplace(cr, PvrContract.Recording.CONTENT_URI, now, recordingValues);
    }

    /**
     * Get the schedule series recordings from backend and store them in the local cache.
     */
    public void updateSeriesRecording(List<? extends ISeriesRecordingSchedule> seriesRecordingList) {
        TimerLog timerLog = new TimerLog(TAG);

        long now = System.currentTimeMillis();

        timerLog.d("Series Recording list loaded");

        List<ContentValues> seriesRecordingCV = new ArrayList<>();

        for (ISeriesRecordingSchedule series : seriesRecordingList) {
            //Add series detail.
            ContentValues seriesContentValues = new ContentValues();

            seriesContentValues.put(PvrContract.SeriesRecording.SERIES_ID, series.getSeriesRefNo());
            seriesContentValues.put(PvrContract.SeriesRecording.USER_SERIES_ID, series.getUserSeriesId());
            seriesContentValues.put(PvrContract.SeriesRecording.CHANNEL_ID, series.getChannelId());
            seriesContentValues.put(PvrContract.SeriesRecording.TITLE, series.getTitle());
            seriesContentValues.put(PvrContract.SeriesRecording.LAST_UPDATED, now);

            seriesRecordingCV.add(seriesContentValues);
        }

        // Insert series recording details in database.
        ContentResolver cr = mApp.getContentResolver();
        long seriesRecording = SqlUtils.bulkReplace(cr,
                PvrContract.SeriesRecording.CONTENT_URI, now, seriesRecordingCV);

        timerLog.d("SeriesRecordingSchedule list updated. " + seriesRecording);
    }

    /**
     * Schedule the given broadcast for recoding.
     *
     * @param broadcast the broadcast which should be scheduled recording.
     */
    public void scheduleRecording(IBroadcast broadcast, IRecording recording) {
        TimerLog timerLog = new TimerLog(TAG);

        ContentResolver contentResolver = mApp.getContentResolver();
        final ContentValues values = RecordingsProjection.toContentValues(recording, broadcast);

        //First check if recording already exists in db, delete before insert.
        IRecording recordingFromDb = getRecording(broadcast.getId());
        if (recordingFromDb != null) {
            deleteRecordingsByIds(recordingFromDb.getRecordingId());
        }

        //Insert new recording item.
        contentResolver.insert(BaseContract.withBatchUpdate(PvrContract.Recording.CONTENT_URI), values);

        timerLog.d("Schedule recording");

        // Notify observers about the recording status change.
        notifyByRecordingBroadcast(broadcast);
    }

    /**
     * Schedule the given broadcast for series recoding.
     *
     * @param broadcast the broadcast which should be scheduled series recording.
     */
    public void scheduleSeriesRecording(IBroadcast broadcast, String seriesId, List<? extends IRecording> recordingList) {
        final long now = System.currentTimeMillis();
        final ContentResolver contentResolver = mApp.getContentResolver();

        // Insert series
        final ContentValues seriesValues = new ContentValues();
        seriesValues.put(PvrContract.SeriesRecording.SERIES_ID, broadcast.getSeriesId());
        seriesValues.put(PvrContract.SeriesRecording.USER_SERIES_ID, seriesId);
        seriesValues.put(PvrContract.SeriesRecording.CHANNEL_ID, broadcast.getChannelId());
        seriesValues.put(PvrContract.SeriesRecording.TITLE, broadcast.getTitle());
        seriesValues.put(PvrContract.SeriesRecording.LAST_UPDATED, now);
        contentResolver.insert(BaseContract.withBatchUpdate(PvrContract.SeriesRecording.CONTENT_URI), seriesValues);

        // Insert episodes
        ContentValues[] recordingContentValues = new ContentValues[recordingList.size()];
        for (int i = 0; i < recordingList.size(); ++i) {
            recordingContentValues[i] = RecordingsProjection.toContentValues(recordingList.get(i));
        }
        contentResolver.bulkInsert(BaseContract.withBatchUpdate(PvrContract.Recording.CONTENT_URI), recordingContentValues);

        // Notify observers about the recording status change.
        notifyBySeriesId(broadcast.getSeriesId());
    }

    /**
     * Deletes one or more recording items from db by system recording ids..
     *
     * @param recordingIds System recording ids which needs to be deleted.
     */
    public void deleteRecordingsByIds(String... recordingIds) {
        String selection = SqlUtils.buildBinding(
                PvrContract.Recording.RECORDING_ID, "IN", recordingIds.length);
        mApp.getContentResolver()
                .delete(BaseContract.withBatchUpdate(PvrContract.Recording.CONTENT_URI), selection, recordingIds);
    }

    /**
     * Deletes one or more recording items from db by system recording ids.
     */
    public void deleteRecordings(IBroadcast broadcast, String... recordingIds) {
        deleteRecordingsByIds(recordingIds);
        notifyByRecordingBroadcast(broadcast);
    }

    /**
     * Deletes one or more recording items from db by system recording ids and series id.
     */
    public void deleteRecordings(String seriesId, String... recordingIds) {
        deleteRecordingsByIds(recordingIds);
        notifyBySeriesId(seriesId);
    }

    /**
     * Deletes one or more recording items from db by user recording ids.
     *
     * @param recordingSystemIds User recording ids which needs to be deleted.
     */
    public void deleteRecordingsFromDbBySystemId(String... recordingSystemIds) {
        String selection = SqlUtils.buildBinding(
                PvrContract.Recording.SYSTEM_RECORD_ID, "IN", recordingSystemIds.length);
        mApp.getContentResolver()
                .delete(BaseContract.withBatchUpdate(PvrContract.Recording.CONTENT_URI), selection, recordingSystemIds);
    }

    /**
     * Deletes a series from db.
     *
     * @param seriesId Series id which needs to be deleted.
     */
    public void deleteSeriesFromDb(String seriesId, boolean notifyChange) {
        String selection = PvrContract.SeriesRecording.SERIES_ID + " =? ";
        mApp.getContentResolver().delete(BaseContract.withBatchUpdate(PvrContract.SeriesRecording.CONTENT_URI), selection, new String[]{seriesId});

        if (notifyChange) {
            notifyBySeriesId(seriesId);
        }
    }

    private void notifyByRecordingBroadcast(IBroadcast broadcast) {
        mApp.getContentResolver().notifyChange(
                PvrContract.buildRecordingUriForBroadcast(broadcast), null);
    }

    private void notifyBySeriesId(String seriesId) {
        mApp.getContentResolver().notifyChange(
                PvrContract.buildRecordingUriForSeries(seriesId), null);
    }

    private IRecording getRecordingByQueryParams(String selection, String[] selectionArgs) {
        return SqlUtils.queryFirst(mApp,
                PvrContract.Recording.CONTENT_URI,
                RecordingsProjection.PROJECTION,
                selection,
                selectionArgs,
                RecordingsProjection::readRow);
    }

    private List<IRecording> getRecordingsByQueryParams(String selection, String[] selectionArgs, String sortOrder) {
        return SqlUtils.queryAll(mApp,
                PvrContract.Recording.CONTENT_URI,
                RecordingsProjection.PROJECTION,
                selection,
                selectionArgs,
                sortOrder,
                RecordingsProjection::readRow);
    }
}
