package tv.threess.threeready.data.account.adapter;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.home.model.module.ModuleConditionMethod;

/**
 * JSON type adapter to read the value as module condition method enum.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public class ModuleConditionMethodTypeAdapter3Ready extends NameTypeMappingTypeAdapter<ModuleConditionMethod> {
    @Override
    protected Map<String, ModuleConditionMethod> createNameTypeMap() {
        return new HashMap<String, ModuleConditionMethod>() {
            {
                put("subscribed", ModuleConditionMethod.SUBSCRIBED);
                put("not_subscribed", ModuleConditionMethod.NOT_SUBSCRIBED);
                put("backend_available", ModuleConditionMethod.BACKEND_AVAILABLE);
                put("internet_available", ModuleConditionMethod.INTERNET_AVAILABLE);
                put("only_backend_available", ModuleConditionMethod.ONLY_BACKEND_AVAILABLE);
            }
        };
    }
}
