/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.gms;

import java.util.List;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.gms.model.InstalledAppInfo;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceParams;

/**
 * Proxy (pass-through) interface to access android and google services.
 *
 * @author Barabas Attila
 * @since 2017.05.17
 */
public interface GmsProxy extends Component {

    /**
     * Return a list of installed application.
     * Filtered and sorted by the given data source parameters.
     */
    List<InstalledAppInfo> getApps(ModuleDataSourceParams dataSource);
}
