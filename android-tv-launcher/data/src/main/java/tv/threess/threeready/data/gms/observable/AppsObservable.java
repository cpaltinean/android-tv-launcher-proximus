/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.gms.observable;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import io.reactivex.ObservableEmitter;
import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.gms.model.InstalledAppInfo;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.generic.observable.BaseBroadcastObservable;

/**
 * Rx observables which returns a list of installed application.
 * It emits a new application list avery time when a new package added or removed.
 *
 * @author Barabas Attila
 * @since 2017.05.23
 */
public abstract class AppsObservable extends BaseBroadcastObservable<ModuleData<InstalledAppInfo>> {
    private static final String TAG = Log.tag(AppsObservable.class);

    // Intent filter to detect application installs and uninstalls.
    private static final IntentFilter PACKAGE_CHANGE_INTENT_FILER = new IntentFilter();
    static {
        PACKAGE_CHANGE_INTENT_FILER.addAction(Intent.ACTION_PACKAGE_ADDED);
        PACKAGE_CHANGE_INTENT_FILER.addAction(Intent.ACTION_PACKAGE_REMOVED);
        PACKAGE_CHANGE_INTENT_FILER.addDataScheme(PackageUtils.SCHEMA_PACKAGE);
    }

    public AppsObservable(Context context) {
        super(context);
    }

    @Override
    public void subscribe(ObservableEmitter<ModuleData<InstalledAppInfo>> emitter) throws Exception {
        super.subscribe(emitter);
        mEmitter.onNext(onLoadApps());
        registerReceiver(PACKAGE_CHANGE_INTENT_FILER);
    }
    
    @Override
    protected void onBroadcastReceived(Intent intent) {
        Log.d(TAG, "On package list changed.");
        mEmitter.onNext(onLoadApps());
    }

    /**
     * This method should be overridden to return the requested list of application.
     */
    public abstract ModuleData<InstalledAppInfo> onLoadApps();

}
