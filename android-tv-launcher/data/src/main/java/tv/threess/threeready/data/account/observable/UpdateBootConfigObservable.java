package tv.threess.threeready.data.account.observable;

import android.content.Context;

import tv.threess.threeready.data.account.AccountService;
import tv.threess.threeready.data.generic.observable.BaseWorkManagerObservable;

/**
 * Rx observable to start the boot config update and wait to finish.
 *
 * @author Barabas Attila
 * @since 2020.03.20
 */
public class UpdateBootConfigObservable extends BaseWorkManagerObservable<Void> {

    public UpdateBootConfigObservable(Context context) {
        super(context, AccountService.buildUpdateBootConfigIntent());
    }
}
