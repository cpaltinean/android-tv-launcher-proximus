/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.pvr;


import java.io.IOException;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.pvr.model.IRecordingQuota;
import tv.threess.threeready.api.tv.model.IRecordingListResponse;

/**
 * Proxy (pass-through) interface to access pvr related backend data.
 *
 * @author Denisa Trif
 * @since 2018.09.14
 */
public interface PvrProxy extends Component {

    /**
     * @return a list with all available recordings.
     */
    IRecordingListResponse getAllRecordings() throws IOException;

    /**
     * @return The available recording space.
     */
    IRecordingQuota getSTCRecordingQuota() throws IOException;
}
