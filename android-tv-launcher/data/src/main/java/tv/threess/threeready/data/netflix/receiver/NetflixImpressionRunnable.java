package tv.threess.threeready.data.netflix.receiver;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import java.util.List;
import java.util.function.Consumer;

import tv.threess.threeready.api.log.Log;

/**
 * Used for sending th impression event.
 *
 * @author David Bondor
 * @since 6.5.2022
 */
public class NetflixImpressionRunnable extends BaseNetflixDETRunnable {

    private static final String DEEP_LINK_KEY = "%DEEP_LINK_KEY%";
    private static final String sImpressionPayload = "{\"query\":\"mutation SendImpressionEvent($deepLinks:[String!]!){sendImpressionEvent(deepLinks:$deepLinks,type:PRESENT){status}}\",\"variables\":{\"deepLinks\":[" + DEEP_LINK_KEY + "]}, \"version\":\"" + EXTRA_DET_APP_VERSION + "\"}";

    private final Consumer<Boolean> mIsSent;
    private String mPayload;

    public NetflixImpressionRunnable(Context context, List<String> deepLinks, Consumer<Boolean> isSent) {
        super(context);

        mPayload = sImpressionPayload.replace(DEEP_LINK_KEY, TextUtils.join(", ", deepLinks));

        // Set the app version
        mPayload = mPayload.replace(EXTRA_DET_APP_VERSION, mwRepository.getAppVersion());

        mIsSent = isSent;
    }

    @Override
    public void run() {
        Log.d(TAG, "Send Netflix Impression intent!");
        super.run();
        mIsSent.accept(true);
    }

    @Override
    protected String getPayload() {
        return mPayload;
    }

    @Override
    protected void addIntentExtras(Intent intent) {
        intent.putExtra(BaseNetflixDETReceiver.EXTRA_DET_GRAPHQL_PAYLOAD, mPayload);
        intent.putExtra(EXTRA_DET_QUERY_COMMAND, EXTRA_DET_IMPRESSION_QUERY_COMMAND);
    }
}
