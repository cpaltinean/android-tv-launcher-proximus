package tv.threess.threeready.data.netflix.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.concurrent.TimeUnit;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountRepository;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.receivers.StandByStateChangeReceiver;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Broadcast receiver to catch Netflix authentication request and provider the token.
 *
 * @author Barabas Attila
 * @since 2019.01.22
 */
public class NetflixAuthenticationReceiver extends BroadcastReceiver {
    private static final String TAG = Log.tag(NetflixAuthenticationReceiver.class);

    private static final String TOKEN_RESPONSE_ACTION = "com.netflix.partner.intent.action.TOKEN_RESPONSE";
    private static final String NETFLIX_TOKEN_PERMISSION = "com.netflix.ninja.permission.TOKEN";

    private static final String EXTRA_TOKEN = "token";
    private static final String EXTRA_ERROR_CODE = "error_code";
    private static final String PARTNER_ERROR = "PARTNER_ERROR";

    private final AccountRepository mAccountRepository = Components.get(AccountRepository.class);
    private final StandByStateChangeReceiver mStandByStateChangeReceiver = Components.get(StandByStateChangeReceiver.class);
    private final AppConfig mAppConfig = Components.get(AppConfig.class);

    private Disposable mTokenDisposable;


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Netflix token request received."  + intent);

        if (!mStandByStateChangeReceiver.isScreenOn()) {
            Log.w(TAG, "Ignore Netflix token request. Device is in standby.");
            return;
        }

        RxUtils.disposeSilently(mTokenDisposable);
        mAccountRepository.getNetflixAuthenticationToken()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(mAppConfig.getQueryTimeout(), TimeUnit.SECONDS)
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable disposable) {
                        mTokenDisposable = disposable;
                    }

                    @Override
                    public void onSuccess(String token) {
                        sendAuthenticationToken(context, token);
                    }

                    @Override
                    public void onError(Throwable error) {
                        Log.e(TAG, "Could not get the Netflix authentication token.", error);
                        sendErrorCode(context);
                    }
                });
    }

    private void sendAuthenticationToken(Context context, String token) {
        Intent intent = new Intent(TOKEN_RESPONSE_ACTION);
        intent.putExtra(EXTRA_TOKEN, token);
        Log.d(TAG, "Send Netflix authentication token. " +  intent);
        context.sendBroadcast(intent, NETFLIX_TOKEN_PERMISSION);
    }

    private void sendErrorCode(Context context) {
        Intent intent = new Intent(TOKEN_RESPONSE_ACTION);
        intent.putExtra(EXTRA_ERROR_CODE, PARTNER_ERROR);
        context.sendBroadcast(intent, NETFLIX_TOKEN_PERMISSION);
    }
}
