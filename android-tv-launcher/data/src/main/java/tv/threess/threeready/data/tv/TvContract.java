/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.tv;

import android.content.ContentResolver;
import android.net.Uri;

import tv.threess.threeready.api.BuildConfig;
import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.model.IImageSource;

/**
 * Contract class to access data in tv database.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.13
 */
public class TvContract {

    public static final String AUTHORITY = BuildConfig.TV_PROVIDER;

    static final Uri BASE_URI = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT).authority(AUTHORITY).build();

    /**
     * Builds a URI that points to all programs on a given channel.
     *
     * @param channelId The ID of the channel to return programs for.
     */
    public static Uri buildBroadcastUriForChannel(String channelId) {
        return Uri.withAppendedPath(Broadcast.CONTENT_URI_WITH_CHANNEL, channelId);
    }

    /**
     * Contract class for TV channels.
     */
    public interface Channel {

        /**
         * Used to identify the netflix channel.
         */
        String NETFLIX_CHANNEL_NAME = "Netflix";

        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Channel.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(Channel.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(TvContract.BASE_URI, PATH);

        /**
         * Content uri to listen on the channel memory cache refresh
         */
        Uri CHANNEL_MEMORY_CACHE_CONTENT_URI = Uri.withAppendedPath(BASE_URI, "channel_memory_cache");

        //
        // Columns
        //
        /**
         * Unique identifier for a channel
         * <P>Type: String</P>
         */
        String ID = BaseContract._ID;

        /**
         * Internal unique identifier of the channel.
         */
        String INTERNAL_ID = "internal_id";

        /**
         * Global unique identifier of the channel.
         */
        String CALL_LETTER = "call_letter";

        /**
         * Unique identifier for a channel qualified with table name
         * <P>Type: String</P>
         */
        String QUALIFIED_ID = TABLE_NAME + "." + BaseContract._ID;

        /**
         * Position of the channel in the json file.
         * <P>Type: Number</P>
         */
        String POSITION = "position";

        /**
         * Used to sort channels, order should be maintained by clients when presenting channels.
         * <P>Type: String</P>
         */
        String NUMBER = "number";

        /**
         * Channel name
         * <P>Type: String</P>
         */
        String NAME = "name";

        /**
         * Language of the channel.
         * <P>Type: String</P>
         */
        String LANGUAGE = "language";

        /**
         * URL to channel image.
         * <P>Type: String</P>
         */
        String IMAGE_URL = "channel_image_url";

        /**
         * Type of the channel : channel, app
         * <P>Type: Number</P>
         */
        String TYPE = "type";

        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;

        /**
         * Warning is required regarding baby content.
         * <P>Type: Boolean</P>
         */
        String IS_BABY_CONTENT_WARNING_REQUIRED = "is_baby_content_warning_required";

        /**
         * Flag that states if the channel is broadcasted in HD
         * <P>Type: Boolean</P>
         */
        String IS_HD_ENABLED = "is_hd_enabled";

        /**
         * Flag that states if the channel is NPVR ready.
         * <P>Type: Boolean</P>
         */
        String IS_NPVR_ENABLED = "is_npvr_enabled";

        /**
         * Flag that states if the channel is Replay ready.
         * <P>Type: Boolean</P>
         */
        String IS_REPLAY_ENABLED = "is_replay_enabled";

        /**
         * Flag that states if the channel can play targeted ads in live mode.
         * <P>Type: Boolean</P>
         */
        String IS_LIVE_TAD_ENABLED = "is_live_tad_enabled";

        /**
         * Flag that states if the channel can play targeted ads in time-shift mode.
         * <P>Type: Boolean</P>
         */
        String IS_TIME_SHIFT_TAD_ENABLED = "is_time_shift_tad_enabled";

        /**
         * Targeted ad inactivity value for inactivity detection for the channel.
         * <P>Type: Long</P>
         */
        String TARGET_AD_INACTIVITY = "target_ad_inactivity";

        /**
         * Time replays could be played starting from "now" back to "now - replay window".
         */
        String REPLAY_WINDOW = "replayWindow";

        /**
         * Flag that states if the channel is entitled to the user
         * * <P>Type: Boolean</P>
         */
        String IS_USER_TV_SUBSCRIBED_CHANNEL = "isUserTvSubscribedChannel";
    }

    /**
     * Contract class for TV channels linkage ids.
     */
    public interface PlaybackOption {

        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(PlaybackOption.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(PlaybackOption.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(TvContract.BASE_URI, PATH);

        Uri CONTENT_URI_WITH_CHANNEL = Uri.withAppendedPath(TvContract.BASE_URI, "channel_playback_options");

        //
        // Columns
        //

        /**
         * Primary key.
         * <P>Type: Number</P>
         */
        String OPTION_ID = BaseContract._ID;

        /**
         * Foreign key to channel id.
         * <P>Type: Number</P>
         */
        String CHANNEL_ID = "channel_id";

        /**
         * type of playback: DVBC, DVBT, IPTV, OTT
         * <P>Type: Number</P>
         */
        String PLAYBACK_TYPE = "playback_type";

        /**
         * playback option order, ex: 1 will be first
         * <P>Type: Number</P>
         */
        String PLAYBACK_ORDER = "playback_order";

        /**
         * playback data: linkage id, mapping id, multi-cast url, package id
         * <P>Type: String</P>
         */
        String PLAYBACK_DATA = "playback_data";

        /**
         * playback extra data
         * <P>Type: String</P>
         */
        String PLAYBACK_EXTRA = "playback_extra";

        /**
         * the id of the channel in tif database
         * <P>Type: Number</P>
         */
        String TIF_CHANNEL_ID = "tif_channel_id";

        /**
         * the input id of the channel in tif database
         * <P>Type: String</P>
         */
        String TIF_INPUT_ID = "tif_input_id";

        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;
    }

    /**
     * Contract class for TV channels.
     */
    public interface Broadcast {

        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Broadcast.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(Broadcast.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(TvContract.BASE_URI, PATH);

        /**
         * Content uri to read data from the channel table joined with broadcast table.
         */
        Uri CONTENT_URI_WITH_CHANNEL = Uri.withAppendedPath(TvContract.BASE_URI, "channel_broadcast");

        //
        // Columns
        //

        /**
         * Unique identifier for the event.
         * <P>Type: String</P>
         */
        String EVENT_ID = "event_id";

        /**
         * Unique identifier for a channel.
         * <P>Type: Number</P>
         */
        String CHANNEL_ID = "channel_id";

        /**
         * Program start time UTC.
         * <P>Type: Number</P>
         */
        String START = "start";

        /**
         * Program end time UTC.
         * <P>Type: Number</P>
         */
        String END = "end";

        /**
         * Program title.
         * <P>Type: String</P>
         */
        String TITLE = "title";

        /**
         * Program description.
         * <P>Type: String</P>
         */
        String DESCRIPTION = "description";

        /**
         * Genres, separated with ','.
         * <P>Type: String </>
         */
        String GENRES = "genres";

        /**
         * Sub genres, separated with ','.
         * <P>Type: String </>
         */
        String SUB_GENRES = "sub_genres";

        /**
         * Release year of the broadcast.
         * <P>Type: String</>
         */
        String RELEASE_YEAR = "release_year";

        /**
         * Season.
         * <P>Type: String</P>
         */
        String SEASON = "season";

        /**
         * Episode.
         * <P>Type: String</P>
         */
        String EPISODE = "episode";

        /**
         * A specific title for this program's episode.
         * <P>Type: String</P>
         */
        String EPISODE_TITLE = "episode_title";

        /**
         * Unique identifier for the program.
         * <P>Type: String</P>
         */
        String PROGRAM_ID = "program_id";

        /**
         * Unique identifier for the program.
         * <P>Type: String</P>
         */
        String SERIES_PROGRAM_ID = "series_id";

        /**
         * Unique identifier for the unskipable advertisement.
         * <P>Type: String</P>
         */
        String UNSKIPABLE_ASSET_ID = "unskipable_asset_id";

        /**
         * Actor list joined by ','.
         * <P>Type: String</P>
         */
        String ACTORS = "actors";

        /**
         * Director list joined by ','.
         * <P>Type: String</P>
         */
        String DIRECTORS = "director";

        /**
         * Parental rating value of the broadcast.
         * <P>Type: String</></>
         */
        String PARENTAL_RATING = "parental_rating";

        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;

        /**
         * Marks if the broadcast is blacklisted or not.
         * <P>Type: Integer</>
         */
        String IS_BLACKLISTED = "is_blacklisted";

        /**
         * The image urls of the broadcast joined by ','.
         * <P>Type: String</P>
         */
        String IMAGE_URL = "image_url";

        /**
         * The type of each image url joined by ','.
         * E.g TCover_W320_H240,E.g TBackground_W120_H240
         * <p>
         * The type consist of 3 part :
         * T - The name of the image source type. {@link IImageSource.Type}
         * W - The width of the image in pixel when defined.
         * H - The height of the image in pixel when defined.
         *
         * <P>Type: String</P>
         */
        String IMAGE_TYPE = "image_type";

        /**
         * True if the variant has descriptive audio, otherwise false.
         * <P>Type: Numeric</P>
         */
        String HAS_DESCRIPTIVE_AUDIO = "has_descriptive_audio";

        /**
         * True if the variant has descriptive subtitles, otherwise false.
         * <P>Type: Numeric</P>
         */
        String HAS_DESCRIPTIVE_SUBTITLE = "has_descriptive_subtitle";

        /**
         * Marks if the broadcast is npvr enable or not.
         * <P>Type: Boolean</P>
         */
        String IS_NPVR_ENABLE = "is_npvr_enable";

        /**
         * Qualified channel id containing also the table name (in form: table_name.column_name)
         * <P>Type: String</P>
         */
        String QUALIFIED_CHANNEL_ID = TABLE_NAME + '.' + CHANNEL_ID;
    }

    public interface ContinueWatchingBroadcast extends Broadcast {
        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(ContinueWatchingBroadcast.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(ContinueWatchingBroadcast.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(TvContract.BASE_URI, PATH);


        /**
         * Qualified channel id containing also the table name (in form: table_name.column_name)
         * <P>Type: String</P>
         */
        String QUALIFIED_CHANNEL_ID = TABLE_NAME + '.' + CHANNEL_ID;
    }

    /**
     * Contract class for favorites
     */
    public interface Favorites {
        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Favorites.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(Favorites.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(TvContract.BASE_URI, PATH);

        //
        // Columns
        //

        /**
         * Unique identifier for a favorite item
         * <P>Type: Number</P>
         */
        String CHANNEL_ID = "channel_id";

        /**
         * Favorite item position
         * <P>Type: Number</P>
         */
        String POSITION = "position";

        /**
         * The timestamp when the data was saved as favorite
         * <P>Type: Timestamp(long)</P>
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;

        /**
         * Qualified channel id containing also the table name (in form: table_name.column_name)
         * <P>Type: String</P>
         */
        String QUALIFIED_CHANNEL_ID = TABLE_NAME + '.' + CHANNEL_ID;
    }

    /**
     * Contract class for advertisements
     */
    public interface Advertisement {
        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Advertisement.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(Advertisement.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(TvContract.BASE_URI, PATH);

        /**
         * Method for increasing the download attempt counter for advertisements
         */
        String CALL_INCREMENT_ATTEMPTS = "increment_download_attempt";

        //
        // Columns
        //

        /**
         * Unique identifier of the advertisement
         * <P>Type: String</P>
         */
        String CREATIVE_ID = BaseContract._ID;

        /**
         * name of the advertisement
         * <P>Type: String</P>
         */
        String NAME = "name";

        /**
         * Broadcaster of the advertisement
         * <P>Type: String</P>
         */
        String BROADCASTER = "broadcaster";

        /**
         * Filename of the the advertisement
         * <P>Type: String</P>
         */
        String FILENAME = "filename";

        /**
         * Size of the file
         * <P>Type: Number</P>
         */
        String SIZE = "size";

        /**
         * duration of the advertisement
         * <P>Type: Number</P>
         */
        String DURATION = "duration";

        /**
         * expiration date of the advertisement
         * <P>Type: Number</P>
         */
        String EXPIRES = "expires";

        /**
         * expiration date of the advertisement
         * <P>Type: Number</P>
         */
        String ON_MARKER = "on_marker";

        /**
         * indicator for how many times did the download fail
         * <P>Type: Number</P>
         */
        String ATTEMPT_COUNT = "attempt_count";

        /**
         * The timestamp when the data was saved
         * <P>Type: Timestamp(long)</P>
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;
    }
}
