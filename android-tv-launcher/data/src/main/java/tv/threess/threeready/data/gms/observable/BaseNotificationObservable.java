package tv.threess.threeready.data.gms.observable;

import android.content.Context;
import android.net.Uri;
import android.os.Build;

import java.util.List;

import io.reactivex.ObservableEmitter;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.data.gms.GmsContract;
import tv.threess.threeready.api.notification.model.NotificationItem;
import tv.threess.threeready.data.generic.observable.BaseCursorObservable;
import tv.threess.threeready.data.gms.projection.SystemNotificationProjection;

/**
 * observable for @{@link SystemNotificationsObservable} and @{@link UpdatedNotificationsObservable}
 *
 * @author hunormadaras
 * @since 04/03/2019
 */
public abstract class BaseNotificationObservable extends BaseCursorObservable<List<NotificationItem>> {

    public BaseNotificationObservable(Context context) {
        super(context);
    }

    /**
     * subscribe if OS bigger or equal with O, because notifications are available on above android O.
     *
     * @param emitter emitter
     * @throws Exception what kind of exception
     */
    @Override
    public void subscribe(ObservableEmitter<List<NotificationItem>> emitter) throws Exception {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            emitter.onComplete();
            FileUtils.closeSafe(mCursor);
            return;
        }
        super.subscribe(emitter);
    }

    @Override
    protected Uri getUri() {
        return GmsContract.SystemNotification.CONTENT_URI;
    }

    @Override
    protected String[] getProjection() {
        return SystemNotificationProjection.PROJECTION;
    }

}
