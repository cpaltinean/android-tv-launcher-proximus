package tv.threess.threeready.data.generic.observable;

import android.content.Context;
import android.net.Uri;

import io.reactivex.ObservableEmitter;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.account.model.BookmarkDBModel;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Base RX observable to get the last views position for playable assets.
 *
 * @author Barabas Attila
 * @since 2019.08.14
 */
public abstract class BaseBookmarkObservable<TContentItem extends IContentItem> extends BaseContentObservable<IBookmark> {
    private static final String TAG = Log.tag(BaseBookmarkObservable.class);

    private IBookmark mBookmark;
    protected TContentItem mContentItem;

    public BaseBookmarkObservable(Context context, TContentItem contentItem) {
        super(context);
        mContentItem = contentItem;
    }

    @Override
    public void subscribe(ObservableEmitter<IBookmark> e) throws Exception {
        super.subscribe(e);
        registerObserver(ConfigContract.buildUriForBookmark(mContentItem.getId()));
        publishBookmark();
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<IBookmark> observableEmitter) {
        publishBookmark();
    }

    protected void publishBookmark() {
        if (RxUtils.isDisposed(mEmitter)) {
            // Already disposed;
            return;
        }

        IBookmark bookmark = getBookmark();

        // No bookmark. Report empty bookmark.
        if (bookmark == null) {
            mEmitter.onNext(EMPTY_BOOKMARK);
            return;
        }

        if (!bookmark.equals(mBookmark)) {
            mEmitter.onNext(bookmark);
            mBookmark = bookmark;
            Log.d(TAG, "Publish bookmark." + bookmark);
        }
    }

    protected abstract IBookmark getBookmark();

    private static final IBookmark EMPTY_BOOKMARK = new BookmarkDBModel.Builder().build();
}
