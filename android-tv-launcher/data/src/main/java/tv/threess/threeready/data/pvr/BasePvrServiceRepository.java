package tv.threess.threeready.data.pvr;

import android.app.Application;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.StandByStateChangeReceiver;
import tv.threess.threeready.api.pvr.PvrServiceRepository;
import tv.threess.threeready.data.generic.BaseWorkManager;

/**
 * Base class for a PVR service handler. {@link PvrService}
 * All the common PVR related service action should be handled here.
 *
 * @author Denisa Trif
 * @since 2018.09.14
 */
public abstract class BasePvrServiceRepository extends BasePvrRepository implements PvrServiceRepository {

    public BasePvrServiceRepository(Application application) {
        super(application);

        // Detect standby state changes.
        Components.get(StandByStateChangeReceiver.class)
                .addStateChangedListener(mStandByStateChangeListener);
    }

    // Get notification about the standby state changes.
    private final StandByStateChangeReceiver.Listener mStandByStateChangeListener = screenOn -> {
        // Cancel the periodic recording update during standby. Will be loaded during the standby flow.
        if (!screenOn) {
            BaseWorkManager.cancelAlarm(mApp, PvrService.buildUpdateRecordingIntent());
        }
    };
}
