package tv.threess.threeready.data.home.model.generic;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import tv.threess.threeready.api.home.model.generic.ButtonStyle;

/**
 * Json type adapter to read button style as enum.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public class ButtonStyleTypeAdapter3Ready extends TypeAdapter<ButtonStyle> {
    private static final String DEFAULT = "default";
    private static final String PROXIMUS = "proximus";

    @Override
    public void write(JsonWriter out, ButtonStyle value) throws IOException {
        if (value == ButtonStyle.PROXIMUS) {
            out.value(PROXIMUS);
        } else {
            out.value(DEFAULT);
        }
    }

    @Override
    public ButtonStyle read(JsonReader in) throws IOException {
        String value = in.nextString();
        if (PROXIMUS.equals(value)) {
            return ButtonStyle.PROXIMUS;
        }
        return ButtonStyle.DEFAULT;
    }
}
