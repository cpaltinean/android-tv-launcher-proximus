package tv.threess.threeready.data.netflix.receiver;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.netflix.model.NetflixDETCategory;
import tv.threess.threeready.data.netflix.model.NetflixDETResponse;

/**
 * Rx observables which returns the list of the netflix recommendations.
 *
 * @author Barabas Attila
 * @since 2017.07.20
 */
public class NetflixDETResponseReceiver extends BaseNetflixDETReceiver implements Component {

    private static final String INTERNAL_ERROR = "INTERNAL_ERROR";
    private static final String STATUS_OK = "OK";

    private static final String EXTRA_DET_RESPONSE_COMMAND = "command";
    private static final String EXTRA_DET_RESPONSE_DATA = "data";
    private static final String EXTRA_DET_RESPONSE_STATUS = "status";

    // Time that indicates how long (in seconds) to wait before you can submit an identical request (int)
    private static final String EXTRA_DET_RESPONSE_MIN_REFRESH_WAIT = "minRefreshWait";

    // The timestamp that indicates when partner must remove any tiles from its UI (long)
    private static final String EXTRA_DET_RESPONSE_MIN_EXPIRE_AT = "expireAt";

    private static final int MAX_RETRY_COUNT = 10;

    private static final List<String> mDeepLinks = new CopyOnWriteArrayList<>();
    private static final Map<String, NetflixDiscoveryRunnable> mDiscoveryRunnableMap = new HashMap<>();

    @Override
    public void handleIntent(Context context, Intent intent) {
        String data = intent.getStringExtra(EXTRA_DET_RESPONSE_DATA);
        Log.d(TAG, "Data: " + data);
        String command = intent.getStringExtra(EXTRA_DET_RESPONSE_COMMAND);
        Log.d(TAG, "Command: " + command);
        String graphQLPayload = intent.getStringExtra(EXTRA_DET_GRAPHQL_PAYLOAD);
        Log.d(TAG, "GraphQLPayload: " + graphQLPayload);

        //this comes in seconds
        long minRefreshWait = intent.getIntExtra(EXTRA_DET_RESPONSE_MIN_REFRESH_WAIT, 0);
        Log.d(TAG, "minRefreshWait: " + minRefreshWait + " in seconds");

        // this comes in seconds
        long expireAt = intent.getLongExtra(EXTRA_DET_RESPONSE_MIN_EXPIRE_AT, 0);
        Log.d(TAG, "expireAt: " + expireAt + " - epoc in seconds");

        String status = intent.getStringExtra(EXTRA_DET_RESPONSE_STATUS);
        Log.d(TAG, "status: " + status);

        // We need the task logic just for the "discovery" response
        // We can receive other responses too, exp: "impression"
        if (EXTRA_DET_DISCOVERY_QUERY_COMMAND.equals(command)) {
            long now = System.currentTimeMillis();

            // Based on the payload find the needed task
            NetflixDiscoveryRunnable runnable = mDiscoveryRunnableMap.get(graphQLPayload);
            if (runnable == null) {
                Log.d(TAG, "There is no runnable for the received payload");
                return;
            }

            switch (status) {
                case STATUS_OK:
                    NetflixDETResponse response = new NetflixDETResponse(
                            runnable.getCategory(),
                            LocaleUtils.getApplicationLanguage(),
                            data,
                            (now + TimeUnit.SECONDS.toMillis(minRefreshWait)),
                            TimeUnit.SECONDS.toMillis(expireAt)
                    );

                    // Cache the response in the local data base
                    sWorkerHandler.post(() -> mCache.updateNetflixStripe(response));

                    // After completing, we need to remove it from the map.
                    mDiscoveryRunnableMap.remove(graphQLPayload);
                    break;

                case INTERNAL_ERROR:
                    // In the case of Err 5xx we will use a "noise factor" and will re-try the request.
                    if (runnable.getRetryCount() < MAX_RETRY_COUNT) {
                        retryNetflixDiscoveryRequest(runnable);
                        break;
                    }

                default:
                    // Even if we have an error, we need to remove it from the map.
                    mDiscoveryRunnableMap.remove(graphQLPayload);
                    break;
            }
        }
    }

    /**
     * This method will send the request broadcast to the netflix ninja.
     */
    public void sendNetflixDiscoveryRequest(Context context, NetflixDETCategory category, String iid, int count) {
        sMainHandler.post(() -> {
            NetflixDiscoveryRunnable runnable = new NetflixDiscoveryRunnable(context, category, iid, count);
            String payload = runnable.getPayload();

            NetflixDiscoveryRunnable prevRunnable = mDiscoveryRunnableMap.get(payload);
            if (prevRunnable != null) {
                sMainHandler.removeCallbacks(prevRunnable);
                prevRunnable.resetRetryCount();
                prevRunnable.run();
            } else {
                mDiscoveryRunnableMap.put(payload, runnable);
                runnable.run();
            }
        });
    }

    /**
     * Method used for send the impression event for the ninja apk.
     * We use the main handler for collecting the impression events and send in bucket.
     */
    public void sendImpression(Context context, String deepLink) {
        mDeepLinks.add("\"" + deepLink + "\"");
        sMainHandler.removeCallbacksAndMessages(NetflixImpressionRunnable.class);
        sMainHandler.postDelayed(new NetflixImpressionRunnable(
                context,
                mDeepLinks,
                isSent -> {
                    if (isSent) {
                        mDeepLinks.clear();
                    }
                }
        ), NetflixImpressionRunnable.class, 500);
    }

    /**
     * Method used for re-try the Netflix Discovery request
     */
    private void retryNetflixDiscoveryRequest(@NonNull NetflixDiscoveryRunnable runnable) {
        runnable.increaseRetryCount();
        long delay = (long) (Math.pow(2, runnable.getRetryCount()) * 100);

        Log.d(TAG, "Try to send the DET request again! Retry count: " + runnable.getRetryCount() + ", delay: " + delay);

        // Remove the previous runnable from the handler
        sMainHandler.removeCallbacks(runnable);
        sMainHandler.postDelayed(runnable, delay);
    }
}