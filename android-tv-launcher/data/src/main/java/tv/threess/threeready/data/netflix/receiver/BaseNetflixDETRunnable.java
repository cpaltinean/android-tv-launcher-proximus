package tv.threess.threeready.data.netflix.receiver;

import android.content.Context;
import android.content.Intent;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.MwRepository;

/**
 * Base class used by the Netflix Impression and Discovery runnable.
 *
 * @author David Bondor
 * @since 6.5.2022
 */
public abstract class BaseNetflixDETRunnable implements Runnable {

    protected static final String TAG = Log.tag(BaseNetflixDETRunnable.class);

    protected static final String ACTION_DET_REQUEST = "com.netflix.ninja.intent.action.DET_REQUEST";
    protected static final String NETFLIX_DET_PERMISSION = "com.netflix.ninja.permission.DET";

    protected static final String EXTRA_DET_IMPRESSION_QUERY_COMMAND = "impression";
    protected static final String EXTRA_DET_REQUEST_PARTNER_ID = "partnerID";
    protected static final String EXTRA_DET_QUERY_COMMAND = "queryCommand";
    protected static final String EXTRA_DET_APP_VERSION = "%APP_VERSION%";

    protected final MwRepository mwRepository = Components.get(MwRepository.class);

    private final Context mContext;

    public BaseNetflixDETRunnable(Context context) {
        mContext = context;
    }

    @Override
    public void run() {
        Intent intent = new Intent(ACTION_DET_REQUEST);
        addIntentExtras(intent);

        intent.setPackage(PackageUtils.PACKAGE_NAME_NETFLIX);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);

        mContext.sendBroadcast(intent, NETFLIX_DET_PERMISSION);
    }

    protected abstract String getPayload();

    /**
     * Method used for add extra values to the given intent.
     */
    protected abstract void addIntentExtras(Intent intent);
}