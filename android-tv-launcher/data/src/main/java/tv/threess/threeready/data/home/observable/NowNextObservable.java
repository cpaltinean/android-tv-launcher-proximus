/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.home.observable;

import android.content.Context;
import android.net.Uri;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.ObservableEmitter;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.DataSourceSelector;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.ZappingInfo;
import tv.threess.threeready.data.home.model.module.ModuleDataSourceSelector3Ready;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Rx observable class to return the current and previous program based on the module config.
 * Automatically refresh and emits a new program list when a program expires.
 *
 * @author Barabas Attila
 * @since 2017.09.18
 */
public class NowNextObservable extends BaseTvBroadcastObservable<ModuleData<Pair<IBroadcast, IBroadcast>>> {

    private final String TAG = Log.tag(NowNextObservable.class);

    private final TvCache mTvCache = Components.get(TvCache.class);
    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);

    private final List<DataSourceSelector> mDataSourceSelectors;
    private ModuleData<Pair<IBroadcast, IBroadcast>> mNowNextBroadcasts;

    private Disposable mRescheduleDisposable;

    public NowNextObservable(Context context, ModuleConfig moduleConfig, int count, List<DataSourceSelector> dataSourceSelectors) {
        super(context, moduleConfig, count);
        if (dataSourceSelectors == null) {
            mDataSourceSelectors = getSelectors(mModuleConfig);
            setMenuSelectorItemsCount();
        } else {
            mDataSourceSelectors = dataSourceSelectors;
        }
    }

    @Override
    public void subscribe(ObservableEmitter<ModuleData<Pair<IBroadcast, IBroadcast>>> emitter) throws Exception {
        super.subscribe(emitter);

        registerObserver(TvContract.Favorites.CONTENT_URI);
        registerObserver(TvContract.Channel.CHANNEL_MEMORY_CACHE_CONTENT_URI);
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<ModuleData<Pair<IBroadcast, IBroadcast>>> observableEmitter) {
        Log.d(TAG, "onChange: " + uri);
        if (TvContract.Channel.CHANNEL_MEMORY_CACHE_CONTENT_URI.equals(uri)) {
            setMenuSelectorItemsCount();
            if (mNowNextBroadcasts != null) {
                observableEmitter.onNext(mNowNextBroadcasts);
            }
        } else {
            super.onChange(uri, observableEmitter);
        }
    }

    @Override
    protected void publishBroadcasts(ObservableEmitter<ModuleData<Pair<IBroadcast, IBroadcast>>> emitter) {
        mNowNextBroadcasts = new ModuleData<>(mModuleConfig, mModuleConfig.getTitle(), getNowNextBroadcasts(), mDataSourceSelectors);
        emitter.onNext(mNowNextBroadcasts);

        // Calculate the remaining time until a show expires.
        long minEndDate = Long.MAX_VALUE;
        for (Pair<IBroadcast, IBroadcast> nowNext : mNowNextBroadcasts.getItems()) {
            if (nowNext.first.getEnd() > System.currentTimeMillis()
                    && nowNext.first.getEnd() < minEndDate) {
                minEndDate = nowNext.first.getEnd();
            }
        }

        // Calculate the remaining time until the first show expires.
        final long delay = minEndDate - System.currentTimeMillis()
                // Add a few seconds threshold in case there are small gaps between programs.
                + TimeUnit.SECONDS.toMillis(3);

        if (delay > 0 && !emitter.isDisposed()) {
            Log.d(TAG, "Reschedule now next update after." + TimeUnit.MILLISECONDS.toSeconds(delay) + " sec.");
            RxUtils.disposeSilently(mRescheduleDisposable);
            mRescheduleDisposable = Completable.fromAction(() -> publishBroadcasts(emitter))
                    .delaySubscription(delay, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe();

        }
    }

    private List<Pair<IBroadcast, IBroadcast>> getNowNextBroadcasts() {
        List<String> genreList = new ArrayList<>();
        String favorite = "";
        for (DataSourceSelector selector : ArrayUtils.notNull(mDataSourceSelectors)) {
            SelectorOption selectorOption = selector.getSelectedOption();
            if (selectorOption instanceof ModuleDataSourceSelector3Ready) {
                genreList = ((ModuleDataSourceSelector3Ready) selectorOption).getFilterList(ModuleFilterOption.Mixed.Genre.NAME);
                favorite = ((ModuleDataSourceSelector3Ready) selectorOption).getFilter(ModuleFilterOption.Mixed.Favorite.NAME);
                break;
            }
        }

        List<IBroadcast> nowBroadcasts = mTvCache.getNowBroadcasts(genreList, favorite, mCount);
        List<IBroadcast> nextBroadcasts = mTvCache.getNextBroadcasts(favorite, mCount);

        // Build next broadcast map.
        Map<String, IBroadcast> nextBroadcastMap = new HashMap<>();
        for (IBroadcast broadcast : nextBroadcasts) {
            nextBroadcastMap.put(broadcast.getChannelId(), broadcast);
        }

        // Build new/next results.
        List<Pair<IBroadcast, IBroadcast>> result = new ArrayList<>();
        for (IBroadcast broadcast : nowBroadcasts) {
            String channelId = broadcast.getChannelId();
            result.add(Pair.create(broadcast, nextBroadcastMap.get(channelId)));
        }

        return result;
    }

    /**
     * Cancel reschedule after observable cancelled.
     */
    @Override
    protected void unregisterObserver() {
        super.unregisterObserver();
        RxUtils.disposeSilently(mRescheduleDisposable);
    }

    /**
     * @return Filtering options for the module config.
     */
    private List<DataSourceSelector> getSelectors(ModuleConfig moduleConfig) {
        List<SelectorOption> selectorOptions = moduleConfig.getSelectorList();
        if (!ArrayUtils.isEmpty(selectorOptions)) {
            return Collections.singletonList(new DataSourceSelector(selectorOptions, DataSourceSelector.DisplayType.Menu));
        }
        return Collections.emptyList();
    }

    /**
     * Sets the menu selectors item count.
     */
    private void setMenuSelectorItemsCount() {
        List<ZappingInfo> zappingInfoList = mNowOnTvCache.getZappingInfoList();

        for (DataSourceSelector selector : ArrayUtils.notNull(mDataSourceSelectors)) {
            for (SelectorOption selectorOption : selector.getOptions()) {
                if (!(selectorOption instanceof ModuleDataSourceSelector3Ready)) {
                    continue;
                }

                ModuleDataSourceSelector3Ready option = (ModuleDataSourceSelector3Ready) selectorOption;

                int itemCount = 0;


                Set<String> genreList = new HashSet<>(ArrayUtils.notNull(option.getFilterList(ModuleFilterOption.Mixed.Genre.NAME)));
                boolean isFavorite = Boolean.parseBoolean(option.getFilter(ModuleFilterOption.Mixed.Favorite.NAME));

                for (ZappingInfo zappingInfo : zappingInfoList) {
                    if (zappingInfo.isFromFavorites() || zappingInfo.getBroadcast().isGenerated()
                            || zappingInfo.getChannel().getType() == ChannelType.REGIONAL
                            || zappingInfo.getChannel().getType() == ChannelType.APP) {
                        // Ignore favorite section and app, regional and dummy broadcasts.
                        continue;
                    }

                    // Favorites.
                    if (isFavorite && zappingInfo.getChannel().isFavorite()) {
                        itemCount++;
                    }

                    // Genre filter
                    for (String broadcastGenre : zappingInfo.getBroadcast().getGenres()) {
                        if (genreList.contains(broadcastGenre)) {
                            itemCount++;
                        }
                    }
                }

                option.setItemCount(itemCount);
            }
        }
    }
}
