package tv.threess.threeready.data.mw;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import io.reactivex.annotations.Nullable;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.SystemUpdateReceiver;
import tv.threess.threeready.api.middleware.UpdateUrgency;

/**
 * Task to trigger a system update check and wait for the system image download when available.
 *
 * @author Barabas Attila
 * @since 2020.09.17
 */
public class SystemUpdateTask {
    private static final String TAG = Log.tag(SystemUpdateTask.class);

    private final SystemUpdateReceiver mSystemUpdateReceiver = Components.get(SystemUpdateReceiver.class);
    private final MwProxy mMwProxy = Components.get(MwProxy.class);

    private UpdateUrgency mUpdateUrgency;

    private final FutureTask<UpdateUrgency> mFutureTask = new FutureTask<>(() -> {
        Log.d(TAG, "System update task finished.");
        return mUpdateUrgency;
    });

    /**
     * Check the system update check and image download.
     */
    public void start() {
        mSystemUpdateReceiver.addStateChangedListener(mInternalListener);
        mMwProxy.checkSystemUpdates(false, false, false);
    }

    /**
     * @param millis The maximum time to wait for the result.
     *               If the task will not finish during this time it will throw a TimeoutException.
     * @return the urgency of the downloaded system update or null if there is no update available.
     */
    @Nullable
    public UpdateUrgency waitResult(long millis) throws InterruptedException, ExecutionException, TimeoutException {
        try {
            return mFutureTask.get(millis, TimeUnit.MILLISECONDS);
        } finally {
            mSystemUpdateReceiver.removeStateChangedListener(mInternalListener);
        }
    }

    private final SystemUpdateReceiver.Listener mInternalListener = new SystemUpdateReceiver.Listener() {
        @Override
        public void onNoUpdateAvailable(boolean manualCheck) {
            mUpdateUrgency = null;
            mFutureTask.run();
        }

        @Override
        public void onUpdateCheckFailed(boolean manualCheck) {
            mUpdateUrgency = null;
            mFutureTask.run();
        }

        @Override
        public void onUpdateDownloadFailed(UpdateUrgency urgency, boolean manualCheck) {
            mUpdateUrgency = null;
            mFutureTask.run();
        }

        @Override
        public void onUpdateDownload(UpdateUrgency urgency, boolean manualCheck) {
            mUpdateUrgency = urgency;
            mFutureTask.run();
        }
    };
}
