package tv.threess.threeready.data.tv.observable;

import android.content.Context;
import android.net.Uri;

import java.util.List;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.tv.model.PlaybackOption;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.tv.TvDatabase;

/**
 * Query all playback options.
 * If internet not connected excludes IPTV and OTT playback options
 * If cable not connected excludes DVB playback options
 *
 * @author Daniel Gliga
 * @since 2017.10.19
 */

public class PlaybackOptionsObservable extends BaseContentObservable<List<PlaybackOption>> {

    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);

    private final boolean mOnlyAvailable;
    private final String mChannelId;

    /**
     * @param context
     * @param onlyAvailable if true excludes IPTV and OTT options if no internet and DVB options if cable not connected
     */
    public PlaybackOptionsObservable(Context context, String channelId, boolean onlyAvailable) {
        super(context);
        mOnlyAvailable = onlyAvailable;
        mChannelId = channelId;
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter observableEmitter) {

    }

    @Override
    public void subscribe(ObservableEmitter<List<PlaybackOption>> e) throws Exception {
        super.subscribe(e);
        e.onNext(queryPlaybackOptions());
        e.onComplete();
    }

    public List<PlaybackOption> queryPlaybackOptions() {
        return TvDatabase.queryPlaybackOptions(mContext, mChannelId,
            mInternetChecker.isInternetAvailable(), false, false, mOnlyAvailable
        );
    }
}
