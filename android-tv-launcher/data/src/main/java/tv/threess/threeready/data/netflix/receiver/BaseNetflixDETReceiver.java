package tv.threess.threeready.data.netflix.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.netflix.NetflixDETCache;

/**
 * Base runnable class used for the common parts of {@link NetflixDETResponseReceiver} and {@link NetflixDETTokenReceiver}
 *
 * @author David Bondor
 * @since 6.5.2022
 */
public abstract class BaseNetflixDETReceiver extends BroadcastReceiver {

    protected static final String TAG = Log.tag(BaseNetflixDETReceiver.class);

    protected static final String NETFLIX_DET_PERMISSION = "com.netflix.ninja.permission.DET";
    protected static final String EXTRA_DET_DISCOVERY_QUERY_COMMAND = "discovery";
    protected static final String EXTRA_DET_GRAPHQL_PAYLOAD = "graphQLPayload";

    protected final NetflixDETCache mCache = Components.get(NetflixDETCache.class);

    protected static final Handler sMainHandler = new Handler(Looper.getMainLooper());
    protected static final Handler sWorkerHandler = new Handler(new HandlerThread(":BaseNetflixDETReceiver") {{
        this.start();
    }}.getLooper());

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Netflix DET response received: " + intent);
        handleIntent(context, intent);
    }

    protected abstract void handleIntent(Context context, Intent intent);
}