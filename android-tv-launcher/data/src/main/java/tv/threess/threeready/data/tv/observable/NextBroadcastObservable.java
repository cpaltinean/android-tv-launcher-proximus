/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.tv.observable;

import android.content.Context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.ObservableEmitter;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.data.tv.model.NoInfoBroadcast;
import tv.threess.threeready.data.tv.model.TvBroadcastDBModel;

/**
 * Rx observable which emits the broadcast next to the given one.
 *
 * @author Eugen Guzyk
 * @since 2018.08.02
 */
public class NextBroadcastObservable extends BroadcastsBetweenObservable<IBroadcast> {
    private final IBroadcast mCurrentBroadcast;

    public NextBroadcastObservable(Context context, IBroadcast broadcast, boolean isAdultChannel) {
        super(context, broadcast.getChannelId(), broadcast.getEnd(),
                broadcast.getEnd() + TimeUnit.HOURS.toMillis(1), isAdultChannel);
        mCurrentBroadcast = broadcast;
    }

    @Override
    protected void publishPrograms(ObservableEmitter<IBroadcast> emitter, IBroadcast programs, boolean fromNotify) {
        if (programs != null) {
            emitter.onNext(programs);
            if (!(programs instanceof NoInfoBroadcast)) {
                emitter.onComplete();
            }
        }
    }

    @Override
    IBroadcast getPublishValue(List<IBroadcast> broadcasts) {
        IBroadcast broadcast = ArrayUtils.first(broadcasts);
        if (broadcast != null && broadcast.getStart() > mCurrentBroadcast.getEnd()) {
            if (broadcast instanceof TvBroadcastDBModel) {
                // broadcast is obtained from database, there will be a next publish with downloaded data
                return null;
            }
            broadcast = createDummyProgram(mFrom, Math.min(mTo, broadcast.getStart()), mDummyNoInfoProgramTitle);
        }
        return broadcast;
    }

    @Override
    List<IBroadcast> getBroadcastsListFromDatabase() {
        return mTvCache.getNextBroadcasts(mChannelId, mFrom);
    }

    @Override
    protected List<IBroadcast> getBroadcastsListFromBackend() throws IOException {
        return new ArrayList<>(mTvProxy.getBroadcasts(mChannelId, mFrom, mFrom + 1).getBroadcasts());
    }
}
