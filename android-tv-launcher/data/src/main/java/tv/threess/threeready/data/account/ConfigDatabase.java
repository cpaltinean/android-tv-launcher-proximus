/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.account;

import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.data.generic.helper.BaseSQLiteOpenHelper;
import tv.threess.threeready.data.utils.SqlUtils;

/**
 * Helper class to manage database creation and version management.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.27
 */
public class ConfigDatabase extends BaseSQLiteOpenHelper {

    private static ConfigDatabase mInstance = null;

    // database file name
    private static final String NAME = "config.db";

    // database version number
    private static final int VERSION = 63;

    public static ConfigDatabase getInstance(Context ctx) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        if (mInstance == null) {
            mInstance = new ConfigDatabase(ctx.getApplicationContext());
        }
        return mInstance;
    }

    private ConfigDatabase(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + ConfigContract.Settings.TABLE_NAME + " ("
                + ConfigContract.Settings.KEY + " TEXT PRIMARY KEY NOT NULL,"
                + ConfigContract.Settings.VALUE + " TEXT,"
                + ConfigContract.Settings.LAST_UPDATED + " INTEGER NOT NULL)"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS " + ConfigContract.Session.TABLE_NAME + " ("
                + ConfigContract.Session.KEY + " TEXT PRIMARY KEY NOT NULL,"
                + ConfigContract.Session.VALUE + " TEXT,"
                + ConfigContract.Session.LAST_UPDATED + " INTEGER NOT NULL)"
        );

        StringBuilder columns = new StringBuilder();
        for (String column : PublicSettings.PUBLIC_VALUES) {
            if (columns.length() > 0) {
                columns.append(",");
            }
            columns.append(DatabaseUtils.sqlEscapeString(column));
        }
        db.execSQL("CREATE VIEW IF NOT EXISTS " + ConfigContract.Settings.PUBLIC_VIEW
                + " AS SELECT " + ConfigContract.Settings.KEY + ", " + ConfigContract.Settings.VALUE
                + " FROM " + ConfigContract.Settings.TABLE_NAME
                + " WHERE " + ConfigContract.Settings.KEY + " IN(" + columns + ")"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS " + ConfigContract.Packages.TABLE_NAME + " ("
                + ConfigContract.Packages.PACKAGE_ID + " TEXT PRIMARY KEY NOT NULL,"
                + ConfigContract.Packages.EXTERNAL_ID + " TEXT,"
                + ConfigContract.Packages.PACKAGE_START + " TEXT,"
                + ConfigContract.Packages.PACKAGE_END + " TEXT,"
                + ConfigContract.Packages.IS_LOCKED + " TEXT,"
                + ConfigContract.Packages.PACKAGE_TYPES + " TEXT,"
                + ConfigContract.Packages.LAST_UPDATED + " INTEGER NOT NULL"
                + ")"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS " + ConfigContract.PosterServers.TABLE_NAME + " ("
                + ConfigContract.PosterServers.NAME + " TEXT,"
                + ConfigContract.PosterServers.HEIGHT + " REAL,"
                + ConfigContract.PosterServers.WIDTH + " REAL,"
                + ConfigContract.PosterServers.URL + " TEXT,"
                + ConfigContract.PosterServers.IS_RESIZABLE + " INTEGER,"
                + ConfigContract.PosterServers.LAST_UPDATED + " INTEGER NOT NULL"
                + ")"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS " + ConfigContract.Bookmarks.TABLE_NAME + " ("
            + ConfigContract.Bookmarks.CONTENT_ID + " TEXT NOT NULL,"
            + ConfigContract.Bookmarks.TYPE + " TEXT NOT NULL,"
            + ConfigContract.Bookmarks.POSITION + " INTEGER NOT NULL,"
            + ConfigContract.Bookmarks.DURATION + " INTEGER NOT NULL,"
            + ConfigContract.Bookmarks.LAST_UPDATED + " INTEGER NOT NULL,"
            + " UNIQUE (" + ConfigContract.Bookmarks.CONTENT_ID + "," + ConfigContract.Bookmarks.TYPE + "))"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS " + ConfigContract.Watchlist.TABLE_NAME + " ("
                + ConfigContract.Watchlist.WATCHLIST_ID + " TEXT NOT NULL,"
                + ConfigContract.Watchlist.CONTENT_ID + " TEXT NOT NULL,"
                + ConfigContract.Watchlist.TYPE + " TEXT NOT NULL,"
                + ConfigContract.Watchlist.ADDED_AT + " INTEGER NOT NULL,"
                + ConfigContract.Watchlist.LAST_UPDATED + " INTEGER NOT NULL,"
                + " UNIQUE (" + ConfigContract.Watchlist.WATCHLIST_ID + "))"
        );
    }

    @Override
    public boolean onUpgrade(SQLiteDatabase db, int toVersion) {
        switch (toVersion) {
            default:
                db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.Settings.TABLE_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.Session.TABLE_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.Hints.TABLE_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.Packages.TABLE_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.PosterServers.TABLE_NAME);

                cleanCreate(db);
                return true;

            case 47:
            case 48:
            case 49:
                deleteSettings(db,
                        Settings.lastAccountInfoSyncTime.getName(),
                        Settings.lastBootConfigSyncTime.getName(),
                        Settings.lastChannelsSyncTime.getName(),
                        Settings.lastBroadcastSyncTime.getName(),
                        Settings.lastRentedVodSyncTime.getName(),
                        Settings.lastRecordingSyncTime.getName());

                db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.Session.TABLE_NAME);
                onCreate(db);
                return false;
            case 50:
                deleteSettings(db, Settings.lastAccountInfoSyncTime.getName());
                db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.Packages.TABLE_NAME);
                onCreate(db);
                return false;
            case 51:
                db.execSQL("CREATE TABLE IF NOT EXISTS " + ConfigContract.Watchlist.TABLE_NAME + " ("
                        + ConfigContract.Watchlist.WATCHLIST_ID + " TEXT NOT NULL,"
                        + ConfigContract.Watchlist.CONTENT_ID + " TEXT NOT NULL,"
                        + ConfigContract.Watchlist.TYPE + " TEXT NOT NULL,"
                        + ConfigContract.Watchlist.LAST_UPDATED + " INTEGER NOT NULL,"
                        + " UNIQUE (" + ConfigContract.Watchlist.WATCHLIST_ID + "))"
                );
                return false;
            case 52:
                deleteSettings(db, Settings.ottPlaybackTimeout.getName(),
                        Settings.ottPlayerLiveEdgeMs.getName(),
                        Settings.minimumPlaybackStartSeconds.getName(),
                        Settings.bufferSize.getName(),
                        Settings.lowestInitTrack.getName(),
                        Settings.abrOn.getName(),
                        Settings.minDurationQualityIncrease.getName(),
                        Settings.maxDurationQualityDecrease.getName(),
                        Settings.minDurationRetainAfterDiscard.getName(),
                        Settings.lowMediaTime.getName(),
                        Settings.highMediaTime.getName(),
                        Settings.lowMediaTimeTrickplay.getName(),
                        Settings.highMediaTimeTrickplay.getName(),
                        Settings.drainWhileCharging.getName(),
                        Settings.minDegradationSamples.getName(),
                        Settings.manifestRetryCount.getName(),
                        Settings.manifestRetryDelay.getName(),
                        Settings.segmentRetryCount.getName(),
                        Settings.segmentRetryDelay.getName(),
                        Settings.manifestConnectTimeout.getName(),
                        Settings.manifestReadTimeout.getName(),
                        Settings.segmentConnectTimeout.getName(),
                        Settings.segmentReadTimeout.getName(),
                        Settings.drmConnectionTimeout.getName(),
                        Settings.drmReadTimeout.getName(),
                        Settings.drmAcquisitionTimeout.getName()
                );
                return false;

            case 53:
            case 57:
                deleteSettings(db, Settings.lastBootConfigSyncTime.getName(),
                        Settings.lastAccountInfoSyncTime.getName(),
                        Settings.lastWatchlistSyncTime.getName());
                db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.PosterServers.TABLE_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.Packages.TABLE_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.Watchlist.TABLE_NAME);
                onCreate(db);
                return false;

            case 54:
            case 55:
            case 56:
            case 59:
                deleteSettings(db, Settings.lastBootConfigSyncTime.getName());
                return false;
            case 58:
                deleteSettings(db, Settings.lastAccountInfoSyncTime.getName());
                db.execSQL("DROP TABLE IF EXISTS translations");
                return false;
            case 60:
                db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.Hints.TABLE_NAME);
                return false;
            case 61:
                deleteSettings(db,
                        Settings.lastAccountInfoSyncTime.getName(),
                        Settings.lastChannelsSyncTime.getName()
                );
                return false;
            case 62:
                deleteSettings(db,
                        Settings.lastAccountInfoSyncTime.getName(),
                        Settings.lastChannelsSyncTime.getName(),
                        Settings.iptvPlaybackOptionsHash.getName());
                return false;
            case 63:
                deleteSettings(db,
                        Settings.lastAccountInfoSyncTime.getName());
                return false;
        }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.Settings.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.Session.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.Hints.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.Packages.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.PosterServers.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.Bookmarks.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ConfigContract.Watchlist.TABLE_NAME);
        cleanCreate(db);
    }

    private void cleanCreate(SQLiteDatabase db) {
        db.execSQL("DROP VIEW IF EXISTS " + ConfigContract.Settings.PUBLIC_VIEW);
        onCreate(db);
    }

    private void deleteSettings(SQLiteDatabase db, String... settingsItems) {
        // Remove only the EPG/Channel related settings (has dependencies on the TV database)
        int count = settingsItems.length;
        String selection = SqlUtils.buildBinding(ConfigContract.Settings.KEY, "IN", count);
        db.delete(ConfigContract.Settings.TABLE_NAME, selection, settingsItems);
    }
}
