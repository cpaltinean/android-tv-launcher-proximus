package tv.threess.threeready.data.vod.projections;

import android.database.Cursor;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.data.vod.VodContract;
import tv.threess.threeready.api.vod.model.IRentalExpiration;
import tv.threess.threeready.data.generic.IProjection;
import tv.threess.threeready.data.vod.model.RentalExpirationDbModel;

/**
 * Proximus projection for rental expiration data.
 *
 * Created by Noemi Dali on 27.04.2020.
 */
public enum RentalExpirationProjection implements IProjection {
    VARIANT_ID(VodContract.RentalExpiration.VARIANT_ID),
    EXPIRATION_TIME(VodContract.RentalExpiration.EXPIRATION),
    RENTAL_TIME(VodContract.RentalExpiration.RENTAL_TIME);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    RentalExpirationProjection(String column) {
        mColumn = column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    public static IRentalExpiration readRow(Cursor cursor) {
        return RentalExpirationDbModel.Builder.builder()
                .setVariantId(cursor.getString(VARIANT_ID.ordinal()))
                .setExpirationTime(cursor.getLong(EXPIRATION_TIME.ordinal()))
                .setRentalTime(cursor.getLong(RENTAL_TIME.ordinal()))
                .build();
    }

    public static Map<String, IRentalExpiration> mapFromCursor(Cursor cursor) {
        Map<String, IRentalExpiration> rentalExpirationMap = new HashMap<>();
        if (!cursor.moveToFirst()) {
            return rentalExpirationMap;
        }

        do {
            rentalExpirationMap.put(cursor.getString(VARIANT_ID.ordinal()), readRow(cursor));
        } while (cursor.moveToNext());

        return rentalExpirationMap;
    }
}
