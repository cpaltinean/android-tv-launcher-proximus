package tv.threess.threeready.data.generic.model;

import java.io.Serializable;

/**
 * Base backend response for both json and xml responses.
 */
public interface BaseResponse extends Serializable {

    default Error getError() {
        return null;
    }

    interface Error extends Serializable {

        String getErrorCode();

        String getErrorDescription();
    }
}
