/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.tv.projections;

import static tv.threess.threeready.data.tv.TvContract.Channel;
import static tv.threess.threeready.data.tv.TvContract.Favorites;

import android.database.Cursor;

import tv.threess.threeready.data.generic.IProjection;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.tv.model.ChannelDBModel;

/**
 * Created by Boldijar Paul on 4/25/17.
 */

public enum ChannelProjection implements IProjection {
    CHANNEL_ID(Channel.TABLE_NAME, Channel.ID),
    INTERNAL_ID(Channel.TABLE_NAME, Channel.INTERNAL_ID),
    CALL_LETTER(Channel.TABLE_NAME, Channel.CALL_LETTER),
    NUMBER(Channel.TABLE_NAME, Channel.NUMBER),
    NAME(Channel.TABLE_NAME, Channel.NAME),
    LANGUAGE(Channel.TABLE_NAME, Channel.LANGUAGE),
    IMAGE_URL(Channel.TABLE_NAME, Channel.IMAGE_URL),
    TYPE(Channel.TABLE_NAME, Channel.TYPE),
    FAVORITE_ID(Favorites.TABLE_NAME, Favorites.CHANNEL_ID),
    IS_BABY_CONTENT_WARNING_REQUIRED(Channel.TABLE_NAME, Channel.IS_BABY_CONTENT_WARNING_REQUIRED),
    IS_HD_ENABLED(Channel.TABLE_NAME, Channel.IS_HD_ENABLED),
    IS_NPVR_ENABLED(Channel.TABLE_NAME, Channel.IS_NPVR_ENABLED),
    IS_REPLAY_ENABLED(Channel.TABLE_NAME, Channel.IS_REPLAY_ENABLED),
    IS_LIVE_TAD_ENABLED(Channel.TABLE_NAME, Channel.IS_LIVE_TAD_ENABLED),
    IS_TIME_SHIFT_TAD_ENABLED(Channel.TABLE_NAME, Channel.IS_TIME_SHIFT_TAD_ENABLED),
    TARGET_AD_INACTIVITY(Channel.TABLE_NAME, Channel.TARGET_AD_INACTIVITY),
    REPLAY_WINDOW(Channel.TABLE_NAME, Channel.REPLAY_WINDOW),
    IS_USER_TV_SUBSCRIBED_CHANNEL(Channel.TABLE_NAME, Channel.IS_USER_TV_SUBSCRIBED_CHANNEL);

    public static final String[] PROJECTION = IProjection.build(values());
    public static final String[] COLUMN_CHANNEL_ID = IProjection.build(CHANNEL_ID);

    private final String mColumn;

    ChannelProjection(String tableName, String column) {
        mColumn = tableName + "." + column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    public static TvChannel readRow(Cursor cursor) {
        return new ChannelDBModel.Builder()
                .setId(cursor.getString(CHANNEL_ID.ordinal()))
                .setInternalId(cursor.getString(INTERNAL_ID.ordinal()))
                .setCallLetter(cursor.getString(CALL_LETTER.ordinal()))
                .setNumber(cursor.getInt(NUMBER.ordinal()))
                .setName(cursor.getString(NAME.ordinal()))
                .setLanguage(cursor.getString(LANGUAGE.ordinal()))
                .setImageUrl(cursor.getString(IMAGE_URL.ordinal()))
                .setType(ChannelType.values()[cursor.getInt(TYPE.ordinal())])
                .setFavorite(!cursor.isNull(FAVORITE_ID.ordinal()))
                .setIsUserTvSubscribedChannel(cursor.getInt(IS_USER_TV_SUBSCRIBED_CHANNEL.ordinal()) != 0)
                .setIsBabyContentWarningRequired(cursor.getInt(IS_BABY_CONTENT_WARNING_REQUIRED.ordinal()) != 0)
                .setHdEnabled(cursor.getInt(IS_HD_ENABLED.ordinal()) != 0)
                .setNPVREnabled(cursor.getInt(IS_NPVR_ENABLED.ordinal()) != 0)
                .setReplayEnabled(cursor.getInt(IS_REPLAY_ENABLED.ordinal()) != 0)
                .setLiveTadEnabled(cursor.getInt(IS_LIVE_TAD_ENABLED.ordinal()) != 0)
                .setTimeShiftTadEnabled(cursor.getInt(IS_TIME_SHIFT_TAD_ENABLED.ordinal()) != 0)
                .setTargetAdInactivity(cursor.getLong(TARGET_AD_INACTIVITY.ordinal()))
                .setReplayWindow(cursor.getLong(REPLAY_WINDOW.ordinal()))
                .build();
    }
}
