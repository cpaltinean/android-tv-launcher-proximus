/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.tv;

import android.content.Context;

import androidx.work.Data;
import androidx.work.WorkerParameters;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.tv.TvServiceRepository;
import tv.threess.threeready.data.generic.BaseWorker;
import tv.threess.threeready.data.generic.JobIntent;

/**
 * Download data from the backend and persist it in the database.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.13
 */
public class TvService {

    public static JobIntent buildChannelUpdateIntent() {
        return new JobIntent(ChannelListUpdater.class, BaseTvRepository.channelUpdater, Data.EMPTY);
    }

    public static JobIntent buildPlaybackConfigUpdateIntent() {
        return new JobIntent(PlaybackConfigUpdater.class, Data.EMPTY);
    }

    public static JobIntent buildChannelScanIntent() {
        return new JobIntent(ChannelScanner.class, Data.EMPTY);
    }

    /**
     * Build intent to start the TV service with broadcast update action.
     *
     * @return The intent to start the service with.
     */
    public static JobIntent buildBroadcastUpdateIntent() {
        return new JobIntent(BroadcastUpdater.class, BaseTvRepository.broadcastUpdater, Data.EMPTY);
    }

    public static class ChannelListUpdater extends BaseWorker {

        public ChannelListUpdater(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            Components.get(TvServiceRepository.class).updateChannels();
        }
    }

    public static class PlaybackConfigUpdater extends BaseWorker {

        public PlaybackConfigUpdater(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            Components.get(TvServiceRepository.class).updatePlaybackConfig();
        }
    }

    public static class ChannelScanner extends BaseWorker {

        public ChannelScanner(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            Settings.batchEdit()
                    .remove(Settings.iptvPlaybackOptionsHash)
                    .remove(Settings.lastChannelsSyncTime)
                    .persistNow();

            TvServiceRepository repository = Components.get(TvServiceRepository.class);
            repository.updateChannels();
            repository.checkTIFPlaybackOptions();
        }
    }

    public static class BroadcastUpdater extends BaseWorker {
        public BroadcastUpdater(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            Components.get(TvServiceRepository.class).updateBroadcasts();
        }
    }
}
