package tv.threess.threeready.data.pvr.observable;

import android.content.Context;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountCache;
import tv.threess.threeready.api.generic.model.BookmarkType;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.data.generic.observable.BaseBookmarkObservable;
import tv.threess.threeready.data.pvr.PvrContract;

/**
 * RX observable that is used to react to the last viewed position of a Recording broadcast
 *
 * @author Tatiana Buha
 * @since 2019.07.24
 */
public class RecordingBookmarkObservable extends BaseBookmarkObservable<IBroadcast> {

    private final AccountCache.Bookmarks mAccountCache = Components.get(AccountCache.Bookmarks.class);

    public RecordingBookmarkObservable(Context context, IBroadcast broadcast) {
        super(context, broadcast);
    }

    @Override
    public void subscribe(ObservableEmitter<IBookmark> emitter) throws Exception {
        super.subscribe(emitter);
        registerObserver(PvrContract.buildRecordingUriForBroadcast(mContentItem));
    }

    @Override
    protected IBookmark getBookmark() {
        return mAccountCache.getBookmark(mContentItem.getId(), BookmarkType.Recording);
    }
}
