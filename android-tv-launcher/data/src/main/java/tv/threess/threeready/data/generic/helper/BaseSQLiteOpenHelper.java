/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.generic.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Base class for databases, where write-ahead logging will be enabled by default
 *
 * @author Paul
 * @since 2017.06.21
 */

public abstract class BaseSQLiteOpenHelper extends SQLiteOpenHelper {
    protected final Context mContext;

    public BaseSQLiteOpenHelper(Context context, String name,
                                SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        mContext = context;
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);

        setWriteAheadLoggingEnabled(true);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Avoid complications with missing or invalid values
        onUpgrade(db, oldVersion, newVersion);
    }

    public abstract boolean onUpgrade(SQLiteDatabase db, int toVersion);

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (int i = oldVersion + 1; i <= newVersion; ++i) {
            if (onUpgrade(db, i)) {
                break;
            }
        }
        // TODO: add exception handling and recreate the database
    }
}
