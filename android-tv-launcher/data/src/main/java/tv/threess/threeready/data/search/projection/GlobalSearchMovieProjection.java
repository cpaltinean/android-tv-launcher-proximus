/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.search.projection;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.media.tv.TvContract;

import java.util.Arrays;
import java.util.List;

import tv.threess.threeready.data.generic.IProjection;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.data.search.GlobalSearchContract;
import tv.threess.threeready.data.search.GlobalSearchContract.Movie;
import tv.threess.threeready.api.search.model.GlobalSearchType;
import tv.threess.threeready.data.R;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Projection to display movies in global search.
 *
 * @author Barabas Attila
 * @since 2017.07.13
 */
public enum GlobalSearchMovieProjection implements IProjection {
    ID(Movie.ID),
    TITLE(Movie.TITLE),
    DESCRIPTION(Movie.DESCRIPTION),
    CARD_IMAGE(Movie.CARD_IMAGE),
    INTENT_DATA(Movie.INTENT_DATA),
    DURATION(Movie.DURATION),
    RENTAL_PRICE(Movie.RENTAL_PRICE),
    PURCHASE_PRICE(Movie.PURCHASE_PRICE),
    RELEASE_YEAR(Movie.RELEASE_YEAR),
    INTENT_DATA_ID(Movie.INTENT_DATA_ID);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    GlobalSearchMovieProjection(String column) {
        mColumn = column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    public static Cursor buildCursor(ParentalControlManager parentalControlManager,
                                     Translator translator, IContentItem... resultArray) {
        return buildCursor(Arrays.asList(resultArray), parentalControlManager, translator);
    }

    public static Cursor buildCursor(List<IContentItem> resultList,
                                     ParentalControlManager parentalControlManager,
                                     Translator translator) {
        MatrixCursor cursor = new MatrixCursor(PROJECTION);

        for (IContentItem result : resultList) {
            boolean contentLocked = parentalControlManager.isRestricted(result);
            Object[] row = new Object[PROJECTION.length];
            row[ID.ordinal()] = result.getId();
            row[TITLE.ordinal()] = contentLocked ? translator.get(TranslationKey.CARDS_TITLE_CONTENT_LOCKED) : result.getTitle();
            row[DESCRIPTION.ordinal()] = contentLocked ? null : result.getReleaseYear();
            row[INTENT_DATA.ordinal()] = result.getId();
            row[DURATION.ordinal()] = contentLocked ? null : result.getDuration();
            row[RELEASE_YEAR.ordinal()] = contentLocked ? null : result.getReleaseYear();
            row[INTENT_DATA.ordinal()] = TvContract.buildChannelUri(0); // Dummy TIF channel id.
            row[INTENT_DATA_ID.ordinal()] = result.getId() + "/" + result.getParentalRating().getMinimumAge() + "/" + GlobalSearchType.Movie.name();
            if (contentLocked) {
                row[CARD_IMAGE.ordinal()] = R.drawable.locked_cover_portrait;
            } else {
                if (result.getImageSources() != null && !result.getImageSources().isEmpty()) {
                    row[CARD_IMAGE.ordinal()] = GlobalSearchContract.Movie.CONTENT_URI + "/" + result.getImageSources().get(0).getUrl();
                }
            }

            // TODO : add price when available in GA.
            cursor.addRow(row);
        }

        return cursor;
    }
}
