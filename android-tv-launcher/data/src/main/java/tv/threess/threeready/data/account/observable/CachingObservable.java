/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.account.observable;

import android.content.Context;

import tv.threess.threeready.data.account.AccountService;
import tv.threess.threeready.data.generic.observable.BaseWorkManagerObservable;

/**
 * Observable which perform caching and notifies the subscribes when finished.
 *
 * @author Barabas Attila
 * @since 2017.08.20
 */
public class CachingObservable extends BaseWorkManagerObservable<Void> {

    public CachingObservable(Context context) {
        super(context, AccountService.buildPerformCachingIntent());
    }
}
