package tv.threess.threeready.data.netflix.projection;

import android.database.Cursor;

import tv.threess.threeready.api.netflix.model.NetflixDETCategory;
import tv.threess.threeready.data.generic.IProjection;
import tv.threess.threeready.data.netflix.NetflixDETContract;
import tv.threess.threeready.data.netflix.model.NetflixDETResponse;

/**
 * Database projection to read a NetflixDET response base item from database.
 *
 * @author David
 * @since 25.02.2022
 */
public enum NetflixDETProjection implements IProjection {
    CATEGORY(NetflixDETContract.Netflix.CATEGORY),
    UI_LANGUAGE(NetflixDETContract.Netflix.UI_LANGUAGE),
    RESPONSE_DATA(NetflixDETContract.Netflix.RESPONSE_DATA),
    MIN_REFRESH_WAIT(NetflixDETContract.Netflix.MIN_REFRESH_AT),
    EXPIRE_AT(NetflixDETContract.Netflix.EXPIRE_AT);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    NetflixDETProjection(String column) {
        mColumn = column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    public static NetflixDETResponse readRow(Cursor cursor) {
        return new NetflixDETResponse(
                NetflixDETCategory.findByName(cursor.getString(CATEGORY.ordinal())),
                cursor.getString(UI_LANGUAGE.ordinal()),
                cursor.getString(RESPONSE_DATA.ordinal()),
                cursor.getLong(MIN_REFRESH_WAIT.ordinal()),
                cursor.getLong(EXPIRE_AT.ordinal())
        );
    }
}
