/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.account;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Binder;

import androidx.annotation.NonNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.generic.helper.UriMatcher;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.BuildConfig;
import tv.threess.threeready.data.generic.BaseProvider;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.data.tv.TvProxy;
import tv.threess.threeready.data.utils.SqlUtils;

/**
 * Content provider to access persisted data.
 * <p>
 * to query data from the provider use:
 * adb shell content query --uri content://tv.threess.threeready.proximus.settings
 * <p>
 * to insert or update data use:
 * adb shell content insert --uri content://tv.threess.threeready.proximus.settings --bind quiescentRebootUptime:i:0
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.27
 */
public class PublicSettings extends BaseProvider {

    private static final String TAG = Log.tag(PublicSettings.class);

    enum UriIDs {
        PublicSettings,
        TadPartition,
        TadMetadata
    }

    private static final UriMatcher<UriIDs> matcher = new UriMatcher<>(tv.threess.threeready.api.BuildConfig.PUBLIC_SETTINGS, UriIDs.class)
            .add(UriIDs.PublicSettings, "")
            .add(UriIDs.TadPartition, UriIDs.TadPartition.name())
            .add(UriIDs.TadMetadata, UriIDs.TadMetadata.name());

    /**
     * Keys that can be exported to other applications.
     * Changing requires modifying also the config database version
     */
    public static final Set<String> PUBLIC_VALUES = new HashSet<>(Arrays.asList(
            Settings.quiescentRebootUptime.getName(),
            Settings.playerDebugOverlayEnabled.getName(),
            Settings.minimumPlaybackStartSeconds.getName(),
            Settings.bufferSize.getName(),
            Settings.lowestInitTrack.getName(),
            Settings.abrOn.getName(),
            Settings.minDurationQualityIncrease.getName(),
            Settings.maxDurationQualityDecrease.getName(),
            Settings.minDurationRetainAfterDiscard.getName(),
            Settings.lowMediaTimeTrickplay.getName(),
            Settings.highMediaTimeTrickplay.getName(),
            Settings.lowMediaTime.getName(),
            Settings.highMediaTime.getName(),
            Settings.drainWhileCharging.getName(),
            Settings.minDegradationSamples.getName(),
            Settings.manifestRetryCount.getName(),
            Settings.manifestRetryDelay.getName(),
            Settings.segmentRetryCount.getName(),
            Settings.segmentRetryDelay.getName(),
            Settings.manifestConnectTimeout.getName(),
            Settings.manifestReadTimeout.getName(),
            Settings.segmentConnectTimeout.getName(),
            Settings.segmentReadTimeout.getName(),
            Settings.ottPlayerLiveEdgeMs.getName(),
            Settings.drmConnectionTimeout.getName(),
            Settings.drmReadTimeout.getName(),
            Settings.drmAcquisitionTimeout.getName(),
            Settings.ottPlaybackTimeout.getName(),
            Settings.playerVersion.getName()
    ));

    @Override
    public boolean onCreate() {
        super.onCreate();
        return true;
    }

    @Override
    public boolean getNotify(Uri uri) {
        return false;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        return ConfigContract.Settings.MIME_TYPE;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (!BuildConfig.DEBUG && matcher.matchEnum(uri) != UriIDs.PublicSettings) {
            throw new UnsupportedOperationException("Query is not available");
        }

        long identity = Binder.clearCallingIdentity();
        try {
            switch (matcher.matchEnum(uri)) {
                case PublicSettings:
                    return getContext().getContentResolver().query(
                            Uri.withAppendedPath(ConfigContract.Settings.CONTENT_URI, ConfigContract.Settings.PUBLIC_VIEW),
                            projection, selection, selectionArgs, sortOrder);

                case TadMetadata:
                    return getContext().getContentResolver().query(TvContract.Advertisement.CONTENT_URI,
                            projection, selection, selectionArgs, sortOrder);

                case TadPartition:
                    return listTadPartitionFiles();
            }
        } finally {
            Binder.restoreCallingIdentity(identity);
        }
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if (!BuildConfig.DEBUG && matcher.matchEnum(uri) != UriIDs.PublicSettings) {
            throw new UnsupportedOperationException("Insert is not available");
        }

        long id = Binder.clearCallingIdentity();
        try {
            ArrayList<ContentValues> toUpdate = new ArrayList<>(values.size());
            long now = System.currentTimeMillis();
            for (String key : values.keySet()) {
                if (!PUBLIC_VALUES.contains(key)) {
                    throw new UnsupportedOperationException("Insert is not available for: " + key);
                }
                ContentValues value = new ContentValues(3);
                value.put(ConfigContract.Settings.KEY, key);
                value.put(ConfigContract.Settings.VALUE, values.getAsString(key));
                value.put(ConfigContract.Settings.LAST_UPDATED, now);
                toUpdate.add(value);
            }
            if (SqlUtils.bulkReplace(getContext().getContentResolver(), ConfigContract.Settings.CONTENT_URI, now, toUpdate) < 0) {
                return null;
            }
            return uri;
        } finally {
            Binder.restoreCallingIdentity(id);
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("Update is not available");
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if (!BuildConfig.DEBUG) {
            throw new UnsupportedOperationException("Delete is not available");
        }

        long identity = Binder.clearCallingIdentity();
        try {
            switch (matcher.matchEnum(uri)) {
                case TadMetadata:
                    return getContext().getContentResolver().delete(TvContract.Advertisement.CONTENT_URI, null, null);

                case TadPartition:
                    return deleteTadPartitionFiles();

            }
        } finally {
            Binder.restoreCallingIdentity(identity);
        }

        throw new UnsupportedOperationException("Delete is not available");
    }

    private static Cursor listTadPartitionFiles() {
        MatrixCursor result = new MatrixCursor(new String[]{"LastModified", "LastAccessed", "Size", "Name"});
        File path = new File(TvProxy.TAD_DOWNLOAD_DIR);
        for (File file : FileUtils.list(path)) {
            result.addRow(new Object[]{
                    TimeBuilder.local(file.lastModified()).toString(TimeUnit.SECONDS),
                    TimeBuilder.local(FileUtils.lastAccessed(file)).toString(TimeUnit.SECONDS),
                    file.length(),
                    file.getName()
            });
        }
        return result;
    }

    private static int deleteTadPartitionFiles() {
        int result = 0;
        File tadDir = new File(TvProxy.TAD_DOWNLOAD_DIR);
        for (File tad : FileUtils.list(tadDir)) {
            boolean deleted = tad.delete();
            result += deleted ? 1 : 0;
            Log.d(TAG, "ad file deleted[" + deleted + "]: " + tad.getAbsolutePath());
        }

        boolean deleted = tadDir.delete();
        result += deleted ? 1 : 0;
        Log.d(TAG, "ad folder deleted[" + deleted + "]: " + tadDir.getAbsolutePath());
        return result;
    }
}
