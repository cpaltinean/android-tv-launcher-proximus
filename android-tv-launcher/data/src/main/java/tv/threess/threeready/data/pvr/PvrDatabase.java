package tv.threess.threeready.data.pvr;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.data.generic.helper.BaseSQLiteOpenHelper;

/**
 * Helper class to manage pvr database creation and version management.
 *
 * @author Denisa Trif
 * @since 2018.09.14
 */
public class PvrDatabase extends BaseSQLiteOpenHelper {

    // database file name
    private static final String NAME = "pvr.db";

    // database version number
    private static final int VERSION = 11;

    public PvrDatabase(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + PvrContract.SeriesRecording.TABLE_NAME + " ("
                + PvrContract.SeriesRecording.SERIES_ID + " TEXT NOT NULL UNIQUE,"
                + PvrContract.SeriesRecording.USER_SERIES_ID + " TEXT NOT NULL,"
                + PvrContract.SeriesRecording.CHANNEL_ID + " TEXT,"
                + PvrContract.SeriesRecording.TITLE + " TEXT,"
                + PvrContract.SeriesRecording.LAST_UPDATED + " INTEGER NOT NULL)"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS " + PvrContract.Recording.TABLE_NAME + " ("
                + PvrContract.Recording.RECORDING_ID + " TEXT PRIMARY KEY NOT NULL,"
                + PvrContract.Recording.BROADCAST_ID + " TEXT,"
                + PvrContract.Recording.SYSTEM_RECORD_ID + " TEXT,"
                + PvrContract.Recording.CHANNEL_ID + " TEXT,"
                + PvrContract.Recording.RECORDING_START + " INTEGER,"
                + PvrContract.Recording.RECORDING_END + " INTEGER,"
                + PvrContract.Recording.BROADCAST_START + " INTEGER,"
                + PvrContract.Recording.BROADCAST_END + " INTEGER,"
                + PvrContract.Recording.TITLE + " TEXT,"
                + PvrContract.Recording.DESCRIPTION + " TEXT,"
                + PvrContract.Recording.GENRES + " TEXT,"
                + PvrContract.Recording.RELEASE_YEAR + " TEXT,"
                + PvrContract.Recording.SEASON + " TEXT,"
                + PvrContract.Recording.EPISODE + " TEXT,"
                + PvrContract.Recording.EPISODE_TITLE + " TEXT,"
                + PvrContract.Recording.PROGRAM_ID + " TEXT,"
                + PvrContract.Recording.SERIES_ID + " TEXT,"
                + PvrContract.Recording.UNSKIPABLE_ASSET_ID + " TEXT,"
                + PvrContract.Recording.ACTORS + " TEXT,"
                + PvrContract.Recording.DIRECTOR + " TEXT,"
                + PvrContract.Recording.PARENTAL_RATING + " TEXT,"
                + PvrContract.Recording.IS_BLACKLISTED + " INTEGER,"
                + PvrContract.Recording.IS_HD + " INTEGER,"
                + PvrContract.Recording.IS_NPVR_ENABLED + " INTEGER,"
                + PvrContract.Recording.IMAGE_URL + " TEXT,"
                + PvrContract.Recording.IMAGE_TYPE + " TEXT,"
                + PvrContract.Recording.FAILED_RECORDING + " BOOLEAN,"
                + PvrContract.Recording.HAS_DESCRIPTIVE_AUDIO + " INTEGER,"
                + PvrContract.Recording.HAS_DESCRIPTIVE_SUBTITLE + " INTEGER,"
                + PvrContract.Recording.LAST_UPDATED + " INTEGER NOT NULL)"
        );
    }

    @Override
    public boolean onUpgrade(SQLiteDatabase db, int toVersion) {

        switch (toVersion) {
            default:
                db.execSQL("DROP TABLE IF EXISTS " + PvrContract.SeriesRecording.TABLE_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + PvrContract.Recording.TABLE_NAME);

                Settings.batchEdit()
                        .remove(Settings.lastSeriesRecordingSyncTime)
                        .remove(Settings.lastRecordingSyncTime)
                        .persistNow();

                onCreate(db);
                return true;

            case 6:
            case 8: // descriptive audio and video
            case 9: // non playable icon
            case 10: // unskipable asset id
                db.execSQL("DROP TABLE IF EXISTS " + PvrContract.Recording.TABLE_NAME);
                Settings.lastRecordingSyncTime.edit().remove();
                onCreate(db);
                return false;
            case 11:
                db.execSQL("DROP TABLE IF EXISTS " + PvrContract.Recording.TABLE_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + PvrContract.SeriesRecording.TABLE_NAME);
                Settings.lastRecordingSyncTime.edit().remove();
                onCreate(db);
                return false;
        }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + PvrContract.SeriesRecording.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PvrContract.Recording.TABLE_NAME);
        Settings.batchEdit()
                .remove(Settings.lastSeriesRecordingSyncTime)
                .remove(Settings.lastRecordingSyncTime)
                .persistNow();
        onCreate(db);
    }
}
