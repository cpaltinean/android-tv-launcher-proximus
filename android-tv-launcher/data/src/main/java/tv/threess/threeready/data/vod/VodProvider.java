/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.vod;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import android.os.Bundle;
import androidx.annotation.NonNull;
import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.helper.UriMatcher;
import tv.threess.threeready.data.generic.BaseProvider;

/**
 * Content provider to access VOD related persisted data.
 *
 * @author Barabas Attila
 * @since 2017.09.04
 */
public class VodProvider extends BaseProvider {

    enum UriIDs {
        ContinueWatchingVod,
        ContinueWatchingVodVariant,
        RentedVod
    }

    private VodDatabase mDatabase;

    // prepare the uri matcher
    private static final UriMatcher<VodProvider.UriIDs> matcher = new UriMatcher<>(VodContract.AUTHORITY, VodProvider.UriIDs.class)
            .add(UriIDs.RentedVod, VodContract.RentalExpiration.CONTENT_URI)
            .add(UriIDs.ContinueWatchingVod, VodContract.ContinueWatchingVod.CONTENT_URI)
            .add(UriIDs.ContinueWatchingVodVariant, VodContract.ContinueWatchingVodVariant.CONTENT_URI);


    @Override
    public Bundle call(String method, String arg, Bundle extras) {
        if (DATABASE_VERSION_CHECK.equals(method)) {
            mDatabase.getReadableDatabase();
        }
        return super.call(method, arg, extras);
    }

    @Override
    public boolean onCreate() {
        super.onCreate();
        // prepare the database helper
        mDatabase = new VodDatabase(this.getContext());

        return true;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        VodProvider.UriIDs match = matcher.matchEnum(uri);
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);
            case RentedVod:
                return VodContract.RentalExpiration.MIME_TYPE;
            case ContinueWatchingVod:
                return VodContract.ContinueWatchingVod.MIME_TYPE;
            case ContinueWatchingVodVariant:
                return VodContract.ContinueWatchingVodVariant.MIME_TYPE;
        }
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);
            case RentedVod:
                table = VodContract.RentalExpiration.TABLE_NAME;
                break;
            case ContinueWatchingVodVariant:
                table = VodContract.ContinueWatchingVodVariant.TABLE_NAME;
                break;
            case ContinueWatchingVod:
                table = VodContract.ContinueWatchingVod.TABLE_NAME;
                break;
        }

        SQLiteDatabase db = mDatabase.getReadableDatabase();
        return super.query(uri, db, table, projection, selection, selectionArgs, sortOrder);
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);
            case RentedVod:
                table = VodContract.RentalExpiration.TABLE_NAME;
                break;
            case ContinueWatchingVodVariant:
                table = VodContract.ContinueWatchingVodVariant.TABLE_NAME;
                break;
            case ContinueWatchingVod:
                table = VodContract.ContinueWatchingVod.TABLE_NAME;
                break;
        }

        long rowId = super.insert(uri, mDatabase.getWritableDatabase(), table, values);
        return rowId < 0 ? null : uri;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);
            case RentedVod:
                table = VodContract.RentalExpiration.TABLE_NAME;
                break;
            case ContinueWatchingVodVariant:
                table = VodContract.ContinueWatchingVodVariant.TABLE_NAME;
                break;
            case ContinueWatchingVod:
                table = VodContract.ContinueWatchingVod.TABLE_NAME;
                break;
        }
        String replace = uri.getQueryParameter(BaseContract.BULK_REPLACE);
        return bulkInsertReplacePurge(uri,
                mDatabase.getWritableDatabase(), table, replace, values);
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);
            case RentedVod:
                table = VodContract.RentalExpiration.TABLE_NAME;
                break;
            case ContinueWatchingVodVariant:
                table = VodContract.ContinueWatchingVodVariant.TABLE_NAME;
                break;
            case ContinueWatchingVod:
                table = VodContract.ContinueWatchingVod.TABLE_NAME;
                break;
        }
        return super.delete(uri,
                mDatabase.getWritableDatabase(), table, selection, selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);
            case RentedVod:
                table = VodContract.RentalExpiration.TABLE_NAME;
                break;
            case ContinueWatchingVodVariant:
                table = VodContract.ContinueWatchingVodVariant.TABLE_NAME;
                break;
            case ContinueWatchingVod:
                table = VodContract.ContinueWatchingVod.TABLE_NAME;
                break;
        }
        return super.update(uri, mDatabase.getWritableDatabase(), table, values, selection, selectionArgs);
    }
}
