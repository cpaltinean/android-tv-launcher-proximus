package tv.threess.threeready.data.vod.model;

import androidx.annotation.Nullable;

import java.util.List;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.vod.model.IVodPrice;
import tv.threess.threeready.api.vod.model.IVodVariant;

/**
 * Base vod item read from the local database.
 *
 * @author Barabas Attila
 * @since 2020.05.18
 */
public class BaseVodItemDbModel extends BaseVodItem<VodVariantDbModel> {

    private String mId;
    private String mMainVariantId;
    private List<String> mVariantIds;

    private BaseVodItemDbModel() {
        super();
    }

    public void setMainVariant(VodVariantDbModel mainVariant) {
        mMainVariant = mainVariant;
    }

    public String getMainVariantId() {
        return mMainVariantId;
    }

    @Override
    public List<String> getVariantIds() {
        return mVariantIds;
    }

    @Override
    public IVodVariant getMainVariant() {
        return mMainVariant;
    }

    @Override
    public boolean isTVod() {
        return mMainVariant.isTVod();
    }

    @Override
    public boolean isSVod() {
        return mMainVariant.isSVod();
    }

    @Override
    public boolean isSubscribed() {
        return mMainVariant.isSVod();
    }

    @Override
    public boolean isRentable() {
        return mMainVariant.isRentable();
    }

    @Override
    public boolean isRented() {
        return mRentExpiration > System.currentTimeMillis();
    }

    @Override
    public boolean isFree() {
        return mMainVariant.isFree();
    }

    @Override
    public long getRentalPeriodEnd() {
        return mRentExpiration;
    }

    @Override
    public String getItemTitle() {
        return mMainVariant.getItemTitle();
    }

    @Override
    public String getGroupTitle() {
        return mMainVariant.getGroupTitle();
    }

    @Override
    public String getGroupId() {
        return mMainVariant.getGroupId();
    }

    @Override
    public String getSeriesTitle() {
        return mMainVariant.getSeriesTitle();
    }

    @Override
    public String getSuperGenre() {
        return mMainVariant.getSuperGenre();
    }

    @Override
    public int getInternalId() {
        return mMainVariant.getInternalId();
    }

    @Override
    public boolean isHD() {
        return mMainVariant.isHD();
    }

    @Override
    public IVodPrice getCheapestPrice() {
        return null;
    }

    @Override
    public boolean isLastChance() {
       return mMainVariant.getLicensePeriodEnd() <= (System.currentTimeMillis() + TimeUnit.DAYS.toMillis(LAST_CHANCE_DAYS));
    }

    @Override
    public boolean isNew() {
        return mMainVariant.isNew();
    }

    @Override
    public String getDescription() {
        return mMainVariant.getDescription();
    }

    @Nullable
    @Override
    public Integer getSeasonNumber() {
        return mMainVariant.getSeasonNumber();
    }

    @Nullable
    @Override
    public Integer getEpisodeNumber() {
        return mMainVariant.getEpisodeNumber();
    }

    @Override
    public String getEpisodeTitle() {
        return mMainVariant.getEpisodeTitle();
    }

    @Override
    public String getReleaseYear() {
        return mMainVariant.getReleaseYear();
    }

    @Override
    public long getDuration() {
        return mMainVariant.getDuration();
    }

    @Nullable
    @Override
    public String getSeriesId() {
        return mMainVariant.getSeriesId();
    }

    @Override
    public List<String> getActors() {
        return mMainVariant.getActors();
    }

    @Override
    public List<String> getDirectors() {
        return mMainVariant.getDirectors();
    }

    @Override
    public List<String> getGenres() {
        return mMainVariant.getGenres();
    }

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public String getTitle() {
        return mMainVariant.getTitle();
    }

    @Override
    public ParentalRating getParentalRating() {
        return mMainVariant.getParentalRating();
    }

    @Override
    public List<IImageSource> getImageSources() {
        return mMainVariant.getImageSources();
    }

    @Override
    public boolean hasDescriptiveAudio() {
        return mMainVariant.hasDescriptiveAudio();
    }

    @Override
    public boolean hasDescriptiveSubtitle() {
        return mMainVariant.hasDescriptiveSubtitle();
    }

    public static class Builder {
        BaseVodItemDbModel mVod = new BaseVodItemDbModel();


        public static BaseVodItemDbModel.Builder builder() {
            return new BaseVodItemDbModel.Builder();
        }

        public Builder id(String id) {
            mVod.mId = id;
            return this;
        }

        public Builder mainVariantId(String id) {
            mVod.mMainVariantId = id;
            return this;
        }

        public Builder variantIds(List<String> variantIds) {
            mVod.mVariantIds = variantIds;
            return this;
        }

        public BaseVodItemDbModel build() {
            return mVod;
        }
    }
}
