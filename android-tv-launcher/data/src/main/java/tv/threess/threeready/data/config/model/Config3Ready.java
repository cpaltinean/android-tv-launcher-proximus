package tv.threess.threeready.data.config.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tv.threess.threeready.api.config.model.ApiConfig;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.config.model.Config;
import tv.threess.threeready.api.config.model.FeatureControl;
import tv.threess.threeready.api.home.model.generic.ContentConfig;
import tv.threess.threeready.api.home.model.generic.Hint;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.data.home.model.generic.ContentConfig3Ready;
import tv.threess.threeready.data.home.model.generic.FeatureControl3Ready;
import tv.threess.threeready.data.home.model.generic.Hint3Ready;
import tv.threess.threeready.data.home.model.generic.LayoutConfig3Ready;

/**
 * 3Ready client configuration options.
 *
 * @author Barabas Attila
 * @since 5/12/21
 */
public class Config3Ready implements Config {

    @SerializedName("api_config")
    private ApiConfig3Ready mApiConfig;

    @SerializedName("app_config")
    private AppConfig3Ready mAppConfig;

    @SerializedName("content_config")
    private ContentConfig3Ready mContentConfig;

    @SerializedName("layout_config")
    private LayoutConfig3Ready mLayoutConfig;

    @SerializedName("feature_control")
    private FeatureControl3Ready mFeatureControl;

    @SerializedName("hints")
    private List<Hint3Ready> mHints;

    @SerializedName("internet_check_hints")
    private List<Hint3Ready> mInternetCheckHints;

    @Override
    public ApiConfig getApiConfig() {
        return mApiConfig;
    }

    @Override
    public AppConfig getAppConfig() {
        return mAppConfig;
    }

    @Override
    public ContentConfig getContentConfig() {
        return mContentConfig;
    }

    @Override
    public LayoutConfig getLayoutConfig() {
        return mLayoutConfig;
    }

    @Override
    public FeatureControl getFeatureControl() {
        return mFeatureControl;
    }

    @Override
    public List<Hint> getHints() {
        return (List<Hint>) (List<?>) mHints;
    }

    @Override
    public List<Hint> getTutorialHints() {
        List<Hint> startUpHintList = new ArrayList<>();
        if (mHints == null) {
            return startUpHintList;
        }
        for (Hint hint : mHints) {
            if (hint.getEnabledTutorial()) {
                startUpHintList.add(hint);
            }
        }
        return startUpHintList;
    }

    @Override
    public List<Hint> getInternetCheckHints() {
        return mInternetCheckHints == null
                ? Collections.emptyList() : (List<Hint>) (List<?>) mInternetCheckHints;
    }
}
