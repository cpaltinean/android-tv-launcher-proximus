package tv.threess.threeready.data.account.adapter;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;

/**
 * Json type adapter the read the data source method as enum.
 *
 * @author Barabas Attila
 * @since 5/16/21
 */
public class ModuleDataSourceMethodTypeAdapter extends NameTypeMappingTypeAdapter<ModuleDataSourceMethod> {
    @Override
    protected Map<String, ModuleDataSourceMethod> createNameTypeMap() {
        return new HashMap<String, ModuleDataSourceMethod>() {
            {
                put("tv_program_index", ModuleDataSourceMethod.TV_PROGRAM_INDEX);
                put("tv_program_search", ModuleDataSourceMethod.TV_PROGRAM_SEARCH);
                put("tv_program_now", ModuleDataSourceMethod.TV_PROGRAM_NOW);
                put("continue_watching", ModuleDataSourceMethod.CONTINUE_WATCHING);
                put("tv_channel_index", ModuleDataSourceMethod.TV_CHANNEL_INDEX);
                put("tv_channel_search", ModuleDataSourceMethod.TV_CHANNEL_SEARCH);
                put("programs_between", ModuleDataSourceMethod.PROGRAMS_BETWEEN);
                put("tv_program_now_next", ModuleDataSourceMethod.TV_PROGRAM_NOW_NEXT);

                put("watchlist", ModuleDataSourceMethod.WATCHLIST);
                put("editorial_items", ModuleDataSourceMethod.EDITORIAL_ITEMS);
                put("user_rentals", ModuleDataSourceMethod.USER_RENTALS);

                put("netflix", ModuleDataSourceMethod.NETFLIX);

                put("android_tv_apps", ModuleDataSourceMethod.ANDROID_TV_APPS);
                put("android_tv_notifications", ModuleDataSourceMethod.ANDROID_TV_NOTIFICATIONS);

                put("scheduled_recording", ModuleDataSourceMethod.SCHEDULED_RECORDING);
                put("completed_recording", ModuleDataSourceMethod.COMPLETED_RECORDING);
            }
        };
    }
}
