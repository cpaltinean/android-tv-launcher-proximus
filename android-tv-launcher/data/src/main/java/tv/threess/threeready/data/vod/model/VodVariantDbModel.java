package tv.threess.threeready.data.vod.model;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.util.List;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.generic.model.VideoQuality;
import tv.threess.threeready.api.pc.model.ParentalRating;

/**
 * VOD variant model from local database.
 *
 * @author Barabas Attila
 * @since 2020.05.15
 */
public class VodVariantDbModel extends BaseVodVariantModel {

    private boolean mHasTrailer;
    private List<String> mAudioLanguages;
    private List<String> mSubtitleLanguages;
    private boolean mIsTVod;
    private boolean mIsSVod;
    private boolean mIsNew;
    private boolean mSubscribed;
    private boolean mIsRentable;
    private VideoQuality mQuality;
    private long mLicencePeriodEnd;
    private boolean mIsFree;
    private String mSuperGenre;
    private int mInternalId;
    private boolean mIsHd;
    private String mDescription;
    private String mSeriesTitle;
    private Integer mSeasonNumber;
    private Integer mEpisodeNumber;
    private String mEpisodeTitle;
    private String mReleaseYear;
    private long mDuration;
    private String mSeriesId;
    private List<String> mActors;
    private List<String> mDirectors;
    private List<String> mGenres;
    private String mId;
    private String mItemTitle;
    private String mGroupTitle;
    private String mGroupId;
    private ParentalRating mParentalRating;
    private List<IImageSource> mImageSources;
    private boolean mHasDescriptiveAudio;
    private boolean mHasDescriptiveSubtitles;

    @Override
    public boolean hasTrailer() {
        return mHasTrailer;
    }

    @Override
    public String getItemTitle() {
        return mItemTitle;
    }

    @Override
    public String getGroupTitle() {
        return mGroupTitle;
    }

    @Override
    public String getGroupId() {
        return mGroupId;
    }

    @Override
    public String getSeriesTitle() {
        return mSeriesTitle;
    }

    @Override
    public List<String> getAudioLanguages() {
        return mAudioLanguages;
    }

    @Override
    public List<String> getSubtitleLanguages() {
        return mSubtitleLanguages;
    }

    @Override
    public boolean isTVod() {
        return mIsTVod;
    }

    @Override
    public boolean isSVod() {
        return mIsSVod;
    }

    @Override
    public boolean isSubscribed() {
        return mSubscribed;
    }

    @Override
    public boolean isRentable() {
        if (isFree() || isRented()) {
            return false;
        }
        return mIsRentable;
    }

    @Override
    public VideoQuality getHighestQuality() {
        return mQuality;
    }

    @Override
    public long getLicensePeriodEnd() {
        return mLicencePeriodEnd;
    }

    @Override
    public boolean isFree() {
        return mIsFree;
    }

    @Override
    public String getSuperGenre() {
        return mSuperGenre;
    }

    @Override
    public int getInternalId() {
        return mInternalId;
    }

    @Override
    public boolean isHD() {
        return mIsHd;
    }

    @Override
    public boolean isNew() {
        return mIsNew;
    }

    @Override
    public String getDescription() {
        return mDescription;
    }

    @Nullable
    @Override
    public Integer getSeasonNumber() {
        return mSeasonNumber;
    }

    @Nullable
    @Override
    public Integer getEpisodeNumber() {
        return mEpisodeNumber;
    }

    @Override
    public String getEpisodeTitle() {
        return mEpisodeTitle;
    }

    @Override
    public String getReleaseYear() {
        return mReleaseYear;
    }

    @Override
    public long getDuration() {
        return mDuration;
    }

    @Nullable
    @Override
    public String getSeriesId() {
        return mSeriesId;
    }

    @Override
    public List<String> getActors() {
        return mActors;
    }

    @Override
    public List<String> getDirectors() {
        return mDirectors;
    }

    @Override
    public List<String> getGenres() {
        return mGenres;
    }

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public String getTitle() {
        String groupTitle = getGroupTitle();
        return !TextUtils.isEmpty(groupTitle) ? groupTitle : getItemTitle();
    }

    @Override
    public ParentalRating getParentalRating() {
        return mParentalRating;
    }

    @Override
    public List<IImageSource> getImageSources() {
        return mImageSources;
    }

    @Override
    public boolean hasDescriptiveAudio() {
        return mHasDescriptiveAudio;
    }

    @Override
    public boolean hasDescriptiveSubtitle() {
        return mHasDescriptiveSubtitles;
    }

    public static class Builder {
        VodVariantDbModel mVariant = new VodVariantDbModel();

        public static VodVariantDbModel.Builder builder() {
            return new VodVariantDbModel.Builder();
        }

        public Builder hasTrailer(boolean hasTrailer) {
            mVariant.mHasTrailer = hasTrailer;
            return this;
        }

        public Builder audioLanguages(List<String> audioLanguages) {
            mVariant.mAudioLanguages = audioLanguages;
            return this;
        }

        public Builder subtitleLanguages(List<String> subtitleLanguages) {
            mVariant.mSubtitleLanguages = subtitleLanguages;
            return this;
        }

        public Builder isNew(boolean isNew) {
            mVariant.mIsNew = isNew;
            return this;
        }

        public Builder isTVod(boolean tVod) {
            mVariant.mIsTVod = tVod;
            return this;
        }

        public Builder isSVod(boolean sVod) {
            mVariant.mIsSVod = sVod;
            return this;
        }

        public Builder isSubscribed(boolean subscribed) {
            mVariant.mSubscribed = subscribed;
            return this;
        }

        public Builder isRentable(boolean isRentable) {
            mVariant.mIsRentable = isRentable;
            return this;
        }

        public Builder quality(VideoQuality quality) {
            mVariant.mQuality = quality;
            return this;
        }

        public Builder licencePeriodEnd(long periodEnd) {
            mVariant.mLicencePeriodEnd = periodEnd;
            return this;
        }

        public Builder isFree(boolean isFree) {
            mVariant.mIsFree = isFree;
            return this;
        }

        public Builder superGenre(String superGenre) {
            mVariant.mSuperGenre = superGenre;
            return this;
        }

        public Builder internalId(int internalId) {
            mVariant.mInternalId = internalId;
            return this;
        }

        public Builder isHd(boolean isHd) {
            mVariant.mIsHd = isHd;
            return this;
        }

        public Builder description(String description) {
            mVariant.mDescription = description;
            return this;
        }

        public Builder seasonNumber(Integer seasonNumber) {
            mVariant.mSeasonNumber = seasonNumber;
            return this;
        }

        public Builder episodeNumber(Integer seasonNumber) {
            mVariant.mEpisodeNumber = seasonNumber;
            return this;
        }

        public Builder episodeTitle(String episodeTitle) {
            mVariant.mEpisodeTitle = episodeTitle;
            return this;
        }

        public Builder releaseYear(String releaseYear) {
            mVariant.mReleaseYear = releaseYear;
            return this;
        }

        public Builder duration(long duration) {
            mVariant.mDuration = duration;
            return this;
        }

        public Builder seriesId(String seriesId) {
            mVariant.mSeriesId = seriesId;
            return this;
        }

        public Builder actors(List<String> actors) {
            mVariant.mActors = actors;
            return this;
        }

        public Builder directors(List<String> directors) {
            mVariant.mDirectors = directors;
            return this;
        }

        public Builder genres(List<String> genres) {
            mVariant.mGenres = genres;
            return this;
        }

        public Builder id(String id) {
            mVariant.mId = id;
            return this;
        }

        public Builder itemTitle(String title) {
            mVariant.mItemTitle = title;
            return this;
        }

        public Builder groupTitle(String title) {
            mVariant.mGroupTitle = title;
            return this;
        }

        public Builder groupId(String groupId) {
            mVariant.mGroupId = groupId;
            return this;
        }

        public Builder parentalRating(ParentalRating parentalRating) {
            mVariant.mParentalRating = parentalRating;
            return this;
        }

        public Builder imageSources(List<IImageSource> imageSources) {
            mVariant.mImageSources = imageSources;
            return this;
        }

        public Builder hasDescriptiveAudio(boolean hasDescriptiveAudio) {
            mVariant.mHasDescriptiveAudio = hasDescriptiveAudio;
            return this;
        }

        public Builder hasDescriptiveSubtitles(boolean hasDescriptiveSubtitles) {
            mVariant.mHasDescriptiveSubtitles = hasDescriptiveSubtitles;
            return this;
        }

        public VodVariantDbModel build() {
            return mVariant;
        }
    }
}
