package tv.threess.threeready.data.home.model.generic;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import tv.threess.threeready.api.home.model.generic.ContentMarkerStyle;

/**
 * Json type adapter to read content marker style as enum.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public class ContentMarkerStyleTypeAdapter3Ready extends TypeAdapter<ContentMarkerStyle> {
    private static final String DEFAULT = "default";
    private static final String PROXIMUS = "proximus";

    @Override
    public void write(JsonWriter out, ContentMarkerStyle value) throws IOException {
        if (value == ContentMarkerStyle.PROXIMUS) {
            out.value(PROXIMUS);
        } else {
            out.value(DEFAULT);
        }
    }

    @Override
    public ContentMarkerStyle read(JsonReader in) throws IOException {
        String value = in.nextString();
        if (PROXIMUS.equals(value)) {
            return ContentMarkerStyle.PROXIMUS;
        }
        return ContentMarkerStyle.DEFAULT;
    }
}