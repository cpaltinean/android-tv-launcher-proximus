package tv.threess.threeready.data.mw;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.accessibility.AccessibilityManager;

import androidx.annotation.Nullable;

import java.io.RandomAccessFile;
import java.lang.reflect.Method;
import java.net.Inet4Address;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.account.setting.MassStorage;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.StartAction;
import tv.threess.threeready.api.log.model.UILogEvent;

public abstract class BaseMwProxy implements MwProxy {
    private static final String TAG = Log.tag(BaseMwProxy.class);

    private static final String LAST_PACKAGE_NAME = "LAST_PACKAGE_NAME";

    private static final String ANDROID_PROPERTY_CLASS = "android.os.SystemProperties";
    private static final String PROPERTY_READ_METHOD = "get";

    private static final String HEX = "0123456789ABCDEF";

    private static long lastStartTimestamp = 0;
    private static String lastPackageName = LAST_PACKAGE_NAME;
    private static long lastStopTimestamp = 0;

    // Cached values.
    protected String mMacAddress;

    protected abstract Context getContext();

    @Override
    public String getOperationSystemVersion() {
        return "Android " + Build.VERSION.RELEASE;
    }

    @Override
    public String getHardwareType() {
        return Build.DEVICE;
    }

    /**
     * @return The ip address of the device from the currently connected network.
     * Null if there is no active network connection.
     */
    @Nullable
    @Override
    public String getIpAddress() {
        try {
            NetworkInterface networkInterface = getNetworkInterface();
            if (networkInterface == null) {
                return null;
            }
            List<InterfaceAddress> interfaceAddresses = networkInterface.getInterfaceAddresses();
            if (interfaceAddresses != null) {
                for (InterfaceAddress interfaceAddress : interfaceAddresses) {
                    if (interfaceAddress.getAddress() instanceof Inet4Address) {
                        return interfaceAddress.getAddress().getHostAddress();
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Could not get IP address!", e);
        }

        return null;
    }

    /**
     * @param componentName The name of the component to check the process for.
     * @return The name of the process in which the given components running.
     */
    @Nullable
    @Override
    public String getProcessName(ComponentName componentName) {
        Context context = getContext();
        try {
            PackageManager packageManager = context.getPackageManager();
            ServiceInfo serviceInfo = packageManager.getServiceInfo(componentName, 0);
            return serviceInfo == null ? null : serviceInfo.processName;
        } catch (Exception e) {
            Log.d(TAG, "Could not get process name for: " + context.getPackageName());
        }
        return null;
    }

    /**
     * @param propertyName The name of the system property.
     * @return The value of the given system property or null if there is no such.
     */
    @Override
    public String getSystemProperties(String propertyName) {
        try {
            Class systemProp = Class.forName(ANDROID_PROPERTY_CLASS);

            Method method = systemProp.getDeclaredMethod(PROPERTY_READ_METHOD, String.class);
            String prop = (String) method.invoke(null, propertyName);

            if (prop != null) {
                prop = prop.trim();
                if (!prop.isEmpty()) {
                    return prop;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Cannot get system property: " + propertyName, e);
        }
        return null;
    }

    /**
     * @return The currently installed TV App version
     */
    @Override
    public String getAppVersion() {
        Context context = getContext();
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.w(TAG, "Could not get the package info.");
        }
        return pInfo == null ? "" : pInfo.versionName;
    }

    /**
     * @return The software version of the middleware.
     */
    @Override
    public String getMWVersion() {
        return Build.VERSION.INCREMENTAL;
    }

    /**
     * Get the content of the boot_id file, This file provides a UUID that changes on each boot of the machine.
     * @return content of the boot_id file.
     */
    @Override
    public String getBootId() {
        RandomAccessFile bootIdFile = null;
        try {
            bootIdFile = new RandomAccessFile("/proc/sys/kernel/random/boot_id", "r");
            return bootIdFile.readLine();
        } catch (Exception e) {
            Log.e(TAG, "Error reading boot id", e);
        } finally {
            FileUtils.closeSafe(bootIdFile);
        }
        return "";
    }

    /**
     * Reboot the device with the given reason.
     * @param reason The reason of the reboot. E.g recovery, quiescent etc.
     */
    @Override
    public void reboot(String reason) {
        try {
            Log.d(TAG, "Reboot device. Reason " + reason);
            PowerManager powerManager = (PowerManager) getContext().getSystemService(Context.POWER_SERVICE);
            powerManager.reboot(reason);
        } catch (Exception e) {
            Log.e(TAG, "Failed to reboot the device.", e);
        }
    }

    /**
     * @return True if the box is in active state, false if the box is in stand by state.
     */
    @Override
    public boolean isScreenOn() {
        PowerManager powerManager = (PowerManager) getContext().getSystemService(Context.POWER_SERVICE);
        if (powerManager != null) {
            return powerManager.isInteractive();
        }

        return false;
    }

    /**
     * @return True if the box is in active state, false if the box is in stand by state.
     */
    @Override
    public boolean isTalkBackOn() {
        try {
            AccessibilityManager manager = (AccessibilityManager) getContext().getSystemService(Context.ACCESSIBILITY_SERVICE);
            List<AccessibilityServiceInfo> serviceList = manager.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_SPOKEN);
            for (AccessibilityServiceInfo serviceInfo : serviceList) {
                String name = serviceInfo.getSettingsActivityName();
                if (!TextUtils.isEmpty(name) && name.equals(PackageUtils.TALKBACK_SETTING_ACTIVITY_NAME)) {
                    return true;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "failed to get TalkBack status", e);
        }
        return false;
    }

    /**
     * @return True if the application is the default launcher on the device.
     */
    @Override
    public boolean isDefaultLauncher() {
        Context context = getContext();
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        final ResolveInfo res = context.getPackageManager().resolveActivity(intent, 0);
        return res.activityInfo != null && context.getPackageName().equals(res.activityInfo.packageName);
    }

    /**
     * @return The width of the screen in pixels.
     */
    public int getWindowWidth() {
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        return metrics.widthPixels;
    }

    /**
     * @return The height of the screen in pixels.
     */
    public int getWindowHeight() {
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        return metrics.heightPixels;
    }

    /**
     * @return The hardware serial number of the device.
     */
    @Override
    public String getHardwareSerialNumber() {
        return getSystemProperties("persist.vendor.serialno");
    }

    /**
     * @return The hardware version of the device.
     */
    @Override
    public String getHardwareVersion() {
        return getSystemProperties("ro.hardware");
    }


    @Override
    public void reportUsageEvents() {
        Context context = getContext();
        long now = System.currentTimeMillis();

        //Get the last app usage events report time from the MassStorage
        long startTime = MassStorage.lastAppUsageEventsReportTime.get(now - TimeUnit.DAYS.toMillis(7));
        MassStorage.lastAppUsageEventsReportTime.edit().put(Math.max(now, startTime));

        //Get from android the usages between now (System.currentTimeMillis())
        // and the last app usage events report time (startTime)
        UsageStatsManager statsManager = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
        UsageEvents usageEvents = statsManager.queryEvents(startTime, now);

        final String ownPackageName = context.getApplicationContext().getPackageName();


        //Dynamic instance for the events
        ApplicationEvents usages = new ApplicationEvents() {
            final UsageEvents.Event event = new UsageEvents.Event();

            @Override
            public boolean isForeground() {
                return event.getEventType() == UsageEvents.Event.MOVE_TO_FOREGROUND;
            }

            @Override
            public boolean isBackground() {
                return event.getEventType() == UsageEvents.Event.MOVE_TO_BACKGROUND;
            }

            @Override
            public long getTimeStamp() {
                return event.getTimeStamp();
            }

            @Override
            public String getPackageName() {
                return event.getPackageName();
            }

            @Override
            public boolean hasNext() {
                return usageEvents.hasNextEvent();
            }

            @Override
            public ApplicationEvents next() {
                usageEvents.getNextEvent(event);
                return this;
            }
        };

        // Lambda function for reporting.
        // As the logging happens with pair, lambda has a start time, a stop time,
        // the application package name, and boolean if the application entered active state or not.
        // if these two duration is equals it means that the app has no start or stop event
        // isActiveState is for make difference between start and stop event.
        reportUsageEvents(usages, ownPackageName, (start, stop, packageName, isInActiveState) -> {
            if (start == stop) {
                if (isInActiveState) {
                    Log.event(new Event<>(UILogEvent.AppStarted, start)
                            .putDetail(UILogEvent.Detail.PackageName, packageName)
                            .putDetail(UILogEvent.Detail.StartAction, StartAction.UsageStats)
                    );
                    return;
                }

                Log.event(new Event<>(UILogEvent.AppStopped, stop)
                        .putDetail(UILogEvent.Detail.PackageName, packageName)
                        .putDetail(UILogEvent.Detail.Duration, 0)
                );
                return;
            }

            Log.event(new Event<>(UILogEvent.AppStarted, start)
                    .putDetail(UILogEvent.Detail.PackageName, packageName)
                    .putDetail(UILogEvent.Detail.StartAction, StartAction.UsageStats)
            );
            Log.event(new Event<>(UILogEvent.AppStopped, stop)
                    .putDetail(UILogEvent.Detail.PackageName, packageName)
                    .putDetail(UILogEvent.Detail.Duration, stop - start)
            );
        });
    }

    /**
     * Saves the start or stop events data and logs it.
     * If has a stop after a start and application has same package name then it logs these
     * data in pair.
     * If exist multiple start stop and same app package name then merge the first start and
     * last stop event.
     * If comes two event and not same application then we log the previous event.
     * Normally in the list the app events comes normally first start and next is a stop, but
     * is case when not exist the pair events. (Ex. exist start event of an app but stop is not
     * or inversely ) than has to log the event with same timestamp because has no both timestamp (start and stop timestamp).
     * That gives for the event a 0 duration when we send to the BE.
     * @param usageEvents the variable that hold the events from the android os. (They can come randomized)
     * @param ownPackageName our application package name,
     * @param logger interface for events logging.
     */
    public static void reportUsageEvents(ApplicationEvents usageEvents, String ownPackageName, ApplicationUsageLogger logger) {
        while (usageEvents.hasNext()) {
            ApplicationEvents event = usageEvents.next();
            String packageName = event.getPackageName();
            long timestamp = event.getTimeStamp();

            // The event is not background or foreground then skip the iteration.
            if (!event.isBackground() && !event.isForeground()) {
                continue;
            }

            //The app is our app then log last events and skip the iteration.
            if (packageName.equals(ownPackageName)) {
                logLastEvents(logger);
                continue;
            }

            //if on the current iteration the package name is not equal to last package name
            //than it means that we have to log last events
            if(!lastPackageName.equals(LAST_PACKAGE_NAME) && !packageName.equals(lastPackageName)){
                logLastEvents(logger);
            }

            //At background event saves the package name and its timestamp when occurred the event.
            if(event.isBackground()){
                lastPackageName = packageName;
                lastStopTimestamp = timestamp;
            }

            // At foreground event
            // Last package not equals with the package name of the current iteration
            // or there is no saved start event then logs last events if are saved events
            // and saves the current package name and the timestamp.
            // if in the list exist same application start->stop->start->stop we have to merge
            // the first start and the last stop.
            if(event.isForeground() && (!lastPackageName.equals(packageName) || lastStartTimestamp == 0)){
                logLastEvents(logger);
                lastStartTimestamp = timestamp;
                lastPackageName = packageName;
            }
        }

        //cases start, stop, start->stop
        //These cases are saved but not logged. Has to log after the loop.
        logLastEvents(logger);

        lastPackageName = LAST_PACKAGE_NAME;
        lastStopTimestamp = 0;
        lastStartTimestamp = 0;
    }

    /**
     * Logs the last events and reset the variables.
     * Here checks if an app has start and stop event and reports as a pair.
     * Or if an app has just one stop or just one start event reports with the same timestamp.
     * @param logger interface for reporting the events.
     */
    private static void logLastEvents(ApplicationUsageLogger logger){
        if(lastStartTimestamp != 0 && lastStopTimestamp != 0){
            logger.onUsageLog(lastStartTimestamp, lastStopTimestamp, lastPackageName, false);
            lastPackageName = LAST_PACKAGE_NAME;
            lastStopTimestamp = 0;
            lastStartTimestamp = 0;
        }else{
            if(lastStopTimestamp != 0){
                logger.onUsageLog(lastStopTimestamp, lastStopTimestamp, lastPackageName, false);
                lastPackageName = LAST_PACKAGE_NAME;
                lastStopTimestamp = 0;
            }
            if(lastStartTimestamp != 0){
                logger.onUsageLog(lastStartTimestamp, lastStartTimestamp, lastPackageName, true);
                lastPackageName = LAST_PACKAGE_NAME;
                lastStartTimestamp = 0;
            }
        }
    }

    /**
     * Finds the network interface for which the hardware address is set
     *
     * @return the found interface
     */
    private NetworkInterface getNetworkInterface() throws SocketException {
        Enumeration<NetworkInterface> intfs = NetworkInterface.getNetworkInterfaces();
        if (intfs != null) {
            while (intfs.hasMoreElements()) {
                NetworkInterface networkInterface = intfs.nextElement();
                if (networkInterface != null && networkInterface.getHardwareAddress() != null) {
                    return networkInterface;
                }
            }
        } else {
            Log.e(TAG, "NetworkInterface.getNetworkInterfaces() failed");
        }
        return null;
    }

    public interface ApplicationUsageLogger{
        void onUsageLog(long start, long end, String packageName, boolean isInActiveState);
    }

    public interface ApplicationEvents extends Iterator<ApplicationEvents>{
        boolean isForeground();
        boolean isBackground();
        long getTimeStamp();
        String getPackageName();

    }
}
