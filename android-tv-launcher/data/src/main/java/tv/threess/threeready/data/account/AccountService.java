/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.account;

import android.content.Context;

import androidx.work.Data;
import androidx.work.WorkerParameters;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountServiceRepository;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.generic.BaseWorker;
import tv.threess.threeready.data.generic.JobIntent;

/**
 * Download data from the backend and persist it in the database.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.27
 */
public class AccountService {
    private static final String TAG = Log.tag(AccountService.class);

    private static final String INTENT_EXTRA_NETFLIX_ESN = "intent.extra.NETFLIX_ESN";

    public static JobIntent buildPerformCachingIntent() {
        return new JobIntent(PerformCaching.class, Data.EMPTY);
    }

    public static JobIntent buildUpdateBootConfigIntent() {
        return new JobIntent(UpdateBootConfig.class, BaseAccountRepository.bootUpdater, Data.EMPTY);
    }

    public static JobIntent buildUpdateGlobalInstallIntent() {
        return new JobIntent(UpdateGlobalInstall.class, BaseAccountRepository.globalInstallUpdater, Data.EMPTY);
    }

    public static JobIntent buildUpdateAccountInfoIntent() {
        return new JobIntent(UpdateAccountInfo.class, BaseAccountRepository.accountUpdater, Data.EMPTY);
    }

    public static JobIntent buildUpdateAccountPackagesInfo() {
        return new JobIntent(UpdateAccountPackagesInfo.class, Data.EMPTY);
    }

    public static JobIntent buildUpdateDeviceRegistration() {
        return new JobIntent(UpdateDeviceRegistration.class, Data.EMPTY);
    }

    public static JobIntent buildUpdateDeviceRegistration(String netflixESN) {
        return new JobIntent(UpdateDeviceRegistration.class, new Data.Builder()
                .putString(INTENT_EXTRA_NETFLIX_ESN, netflixESN)
                .build()
        );
    }

    public static JobIntent buildUpdateWatchlist() {
        return new JobIntent(UpdateWatchlist.class, BaseAccountRepository.watchlistUpdater, Data.EMPTY);
    }

    public static class PerformCaching extends BaseWorker {
        public PerformCaching(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            Components.get(AccountServiceRepository.class).performCaching();
        }
    }

    public static class UpdateBootConfig extends BaseWorker {
        public UpdateBootConfig(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            Components.get(AccountServiceRepository.class).updateBootConfig();
        }
    }

    public static class UpdateGlobalInstall extends BaseWorker {
        public UpdateGlobalInstall(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            Components.get(AccountServiceRepository.class).updateGlobalInstall();
        }
    }

    public static class UpdateAccountInfo extends BaseWorker {
        public UpdateAccountInfo(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            Components.get(AccountServiceRepository.class).updateAccountInfo();
        }
    }

    public static class UpdateAccountPackagesInfo extends BaseWorker {
        public UpdateAccountPackagesInfo(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            Components.get(AccountServiceRepository.class).updateAccountPackagesInfo();
        }
    }

    public static class UpdateDeviceRegistration extends BaseWorker {
        public UpdateDeviceRegistration(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            String netflixESN = getStringInputData(INTENT_EXTRA_NETFLIX_ESN);
            if (netflixESN == null) {
                Components.get(AccountServiceRepository.class).updateDeviceRegistration();
            } else {
                Components.get(AccountServiceRepository.class).updateDeviceRegistration(netflixESN);
            }

        }
    }

    public static class UpdateWatchlist extends BaseWorker {
        public UpdateWatchlist(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            Components.get(AccountServiceRepository.class).updateWatchlist();
        }
    }
}
