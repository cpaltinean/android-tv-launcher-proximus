package tv.threess.threeready.data.pvr;

import android.content.Context;

import androidx.work.Data;
import androidx.work.WorkerParameters;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.pvr.PvrServiceRepository;
import tv.threess.threeready.data.generic.BaseWorker;
import tv.threess.threeready.data.generic.JobIntent;

/**
 * Download PVR data from the backend and persist it in the database.
 *
 * @author Denisa Trif
 * @since 2018.09.14
 */
public class PvrService {

    public static JobIntent buildUpdateRecordingIntent() {
        return new JobIntent(UpdateRecording.class, BasePvrRepository.pvrUpdater, Data.EMPTY);
    }

    public static class UpdateRecording extends BaseWorker {

        public UpdateRecording(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            Components.get(PvrServiceRepository.class).updateRecordings();
        }
    }
}
