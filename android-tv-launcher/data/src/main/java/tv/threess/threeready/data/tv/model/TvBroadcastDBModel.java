/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.tv.model;

import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.tv.model.IBroadcast;

/**
 * TvBroadcast entity created from database
 *
 * @author Daniel Gliga
 * @since 2017.11.01
 */

public class TvBroadcastDBModel implements IBroadcast {

    private String mID;
    private String mChannelID;
    private long mStart;
    private long mEnd;
    private String mTitle;
    private String mDescription;
    private List<String> mGenres;
    private List<String> mSubGenres;
    private Integer mSeason;
    private Integer mEpisode;
    private String mEpisodeTitle;
    private String mProgramID;
    private String mSeriesProgramID;
    private String mUnskipableAssetID;
    private String mReleaseYear;
    private List<String> mActors;
    private List<String> mDirectors;
    private ParentalRating mParentalRating;
    private boolean mIsNPVREnabled;
    private boolean mIsBlackListed;
    private boolean mIsNextToLive;
    private boolean mHasDescriptiveAudio;
    private boolean mHasDescriptiveSubtitle;
    private List<IImageSource> mImageSources;

    @Override
    public String getId() {
        return mID;
    }

    @Override
    public String getProgramId() {
        return mProgramID;
    }

    @Override
    public String getChannelId() {
        return mChannelID;
    }

    @Override
    public long getStart() {
        return mStart;
    }

    @Override
    public long getEnd() {
        return mEnd;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getDescription() {
        return mDescription;
    }

    @Override
    public List<String> getGenres() {
        if (mGenres == null) {
            return Collections.emptyList();
        }

        return mGenres;
    }

    @Override
    public List<String> getSubGenres() {
        if (mSubGenres == null) {
            return Collections.emptyList();
        }
        return mSubGenres;
    }

    @Override
    public List<IImageSource> getImageSources() {
        return mImageSources;
    }

    @Override
    public Integer getSeasonNumber() {
        return mSeason;
    }

    @Override
    public Integer getEpisodeNumber() {
        return mEpisode;
    }

    @Override
    public String getEpisodeTitle() {
        return mEpisodeTitle;
    }

    @Override
    public String getSeriesId() {
        return mSeriesProgramID;
    }

    @Override
    public String getAssetIdentifier() {
        return mUnskipableAssetID;
    }

    @Override
    public String getReleaseYear() {
        return mReleaseYear;
    }

    @Override
    public long getDuration() {
        return mEnd - mStart;
    }

    @Override
    public List<String> getActors() {
        if (mActors == null) {
            return Collections.emptyList();
        }

        return mActors;
    }

    @Override
    public List<String> getDirectors() {
        if (mDirectors == null) {
            return Collections.emptyList();
        }

        return mDirectors;
    }

    @Override
    @NonNull
    public ParentalRating getParentalRating() {
        if (mParentalRating == null) {
            return ParentalRating.Undefined;
        }
        
        return mParentalRating;
    }

    @Override
    public boolean isNPVREnabled() {
        return mIsNPVREnabled;
    }

    @Override
    public boolean isBlackListed() {
        return mIsBlackListed;
    }

    @Override
    public boolean isNextToLive() {
        return mIsNextToLive;
    }

    @Override
    public boolean hasDescriptiveAudio() {
        return mHasDescriptiveAudio;
    }

    @Override
    public boolean hasDescriptiveSubtitle() {
        return mHasDescriptiveSubtitle;
    }

    @Override
    public String toString() {
        return "TvBroadcastDBModel{Id='" + getId() + '\'' +
                ", Start='" + Log.timestamp(getStart()) + '\'' +
                ", End='" + Log.timestamp(getEnd()) + '\'' +
                ", Title='" + getTitle() + "'}";
    }

    public static class Builder {
        TvBroadcastDBModel mBroadcast = new TvBroadcastDBModel();

        public static Builder builder() {
            return new Builder();
        }

        public Builder setId(String id) {
            mBroadcast.mID = id;
            return this;
        }

        public Builder setProgramID(String id) {
            mBroadcast.mProgramID = id;
            return this;
        }

        public Builder setChannelID(String id) {
            mBroadcast.mChannelID = id;
            return this;
        }

        public Builder setStart(long start) {
            mBroadcast.mStart = start;
            return this;
        }

        public Builder setEnd(long end) {
            mBroadcast.mEnd = end;
            return this;
        }

        public Builder setTitle(String title) {
            mBroadcast.mTitle = title;
            return this;
        }

        public Builder setDescription(String description) {
            mBroadcast.mDescription = description;
            return this;
        }

        public Builder setGenres(List<String> genres) {
            mBroadcast.mGenres = genres;
            return this;
        }

        public Builder setSubGenres(List<String> subGenres) {
            mBroadcast.mSubGenres = subGenres;
            return this;
        }

        public Builder setSeason(Integer season) {
            mBroadcast.mSeason = season;
            return this;
        }

        public Builder setEpisode(Integer episode) {
            mBroadcast.mEpisode = episode;
            return this;
        }

        public Builder setEpisodeTitle(String episodeTitle) {
            mBroadcast.mEpisodeTitle = episodeTitle;
            return this;
        }

        public Builder setSeriesProgramID(String seriesProgramID) {
            mBroadcast.mSeriesProgramID = seriesProgramID;
            return this;
        }

        public Builder setUnskipableAssetID(String value) {
            mBroadcast.mUnskipableAssetID = value;
            return this;
        }

        public Builder setParentalRating(ParentalRating parentalRating) {
            mBroadcast.mParentalRating = parentalRating;
            return this;
        }

        public Builder setReleaseYear(String releaseYear) {
            mBroadcast.mReleaseYear = releaseYear;
            return this;
        }

        public Builder setActors(List<String> actors) {
            mBroadcast.mActors = actors;
            return this;
        }

        public Builder setDirectors(List<String> directors) {
            mBroadcast.mDirectors = directors;
            return this;
        }

        public Builder setIsBlackListed(boolean isBlackListed) {
            mBroadcast.mIsBlackListed = isBlackListed;
            return this;
        }

        public Builder setNPVREnabled(boolean isNPVREnabled) {
            mBroadcast.mIsNPVREnabled = isNPVREnabled;
            return this;
        }

        public Builder setImageSources(List<IImageSource> imageSources) {
            mBroadcast.mImageSources = imageSources;
            return this;
        }

        public Builder setNextToLive(boolean isNextToLive) {
            mBroadcast.mIsNextToLive = isNextToLive;
            return this;
        }

        public Builder setHasDescriptiveAudio(boolean hasDescriptiveAudio) {
            mBroadcast.mHasDescriptiveAudio = hasDescriptiveAudio;
            return this;
        }

        public Builder setHasDescriptiveSubtitle(boolean hasDescriptiveSubtitle) {
            mBroadcast.mHasDescriptiveSubtitle = hasDescriptiveSubtitle;
            return this;
        }

        public IBroadcast build() {
            return mBroadcast;
        }
    }
}
