package tv.threess.threeready.data.pvr.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.pvr.model.IRecording;

/**
 * Database model class for recording item.
 * <p>
 * Created by Noemi Dali on 17.04.2020.
 */

public class RecordingDBModel implements IRecording {

    private static final int REC_AVAILABILITY_DAYS = 60;

    private String mId;
    private String mRecordingId;
    private String mSystemRecordId;
    private String mChannelId;
    private long mRecordingStart;
    private long mRecordingEnd;
    private long mProgramStart;
    private long mProgramEnd;
    private String mTitle;
    private String mDescription;
    private List<String> mGenres;
    private String mReleaseYear;
    private Integer mSeasonNumber;
    private Integer mEpisodeNumber;
    private String mEpisodeTitle;
    private String mProgramId;
    private String mSeriesId;
    private String mUnskipableAssetID;
    private List<String> mActors;
    private List<String> mDirectors;
    private ParentalRating mParentalRating;
    private boolean isBlacklisted;
    private List<IImageSource> mImageSources;
    private boolean mIsNPVREnabled;
    private boolean mIsFailedRecording;
    private boolean mHasDescriptiveAudio;
    private boolean mHasDescriptiveSubtitle;

    public String getId() {
        return mId;
    }

    public String getRecordingId() {
        return mRecordingId;
    }

    @Override
    public String getSystemRecordId() {
        return mSystemRecordId;
    }

    @Override
    public boolean isRecordingFinished() {
        return mRecordingEnd < System.currentTimeMillis();
    }

    @Override
    public String getProgramId() {
        return mProgramId;
    }

    public String getChannelId() {
        return mChannelId;
    }

    @Override
    public long getStart() {
        return mProgramStart;
    }

    @Override
    public long getEnd() {
        return mProgramEnd;
    }

    @Override
    public boolean isBlackListed() {
        return isBlacklisted;
    }

    @Override
    public boolean isNextToLive() {
        return false;
    }

    @Override
    public boolean isNPVREnabled() {
        return mIsNPVREnabled;
    }

    public long getRecordingStart() {
        return mRecordingStart;
    }

    public long getRecordingEnd() {
        return mRecordingEnd;
    }

    @Override
    public long getAvailabilityTime() {
        return TimeUnit.DAYS.toMillis(REC_AVAILABILITY_DAYS) + mRecordingEnd;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public List<String> getGenres() {
        return mGenres;
    }

    public String getReleaseYear() {
        return mReleaseYear;
    }

    public Integer getSeasonNumber() {
        return mSeasonNumber;
    }

    public Integer getEpisodeNumber() {
        return mEpisodeNumber;
    }

    public String getEpisodeTitle() {
        return mEpisodeTitle;
    }

    public String getSeriesId() {
        return mSeriesId;
    }

    @Override
    public String getAssetIdentifier() {
        return mUnskipableAssetID;
    }

    public List<String> getActors() {
        return mActors;
    }

    public List<String> getDirectors() {
        return mDirectors;
    }

    public ParentalRating getParentalRating() {
        return mParentalRating;
    }

    public List<IImageSource> getImageSources() {
        return mImageSources;
    }

    @Override
    public boolean hasDescriptiveAudio() {
        return mHasDescriptiveAudio;
    }

    @Override
    public boolean hasDescriptiveSubtitle() {
        return mHasDescriptiveSubtitle;
    }

    @Override
    public boolean isFailedRecording() {
        return mIsFailedRecording;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        return Objects.equals(this.getId(), ((RecordingDBModel) obj).getId());
    }

    public static class Builder {
        RecordingDBModel recordingDBModel = new RecordingDBModel();

        public static Builder builder() {
            return new Builder();
        }

        public Builder setId(String id) {
            recordingDBModel.mId = id;
            return this;
        }

        public Builder setRecordingId(String recordingId) {
            recordingDBModel.mRecordingId = recordingId;
            return this;
        }

        public Builder setSystemRecordId(String systemRecordId) {
            recordingDBModel.mSystemRecordId = systemRecordId;
            return this;
        }

        public Builder setProgramID(String id) {
            recordingDBModel.mProgramId = id;
            return this;
        }

        public Builder setChannelID(String id) {
            recordingDBModel.mChannelId = id;
            return this;
        }

        public Builder setStart(long start) {
            recordingDBModel.mProgramStart = start;
            return this;
        }

        public Builder setEnd(long end) {
            recordingDBModel.mProgramEnd = end;
            return this;
        }

        public Builder setRecordingStart(long start) {
            recordingDBModel.mRecordingStart = start;
            return this;
        }

        public Builder setRecordingEnd(long end) {
            recordingDBModel.mRecordingEnd = end;
            return this;
        }

        public Builder setTitle(String title) {
            recordingDBModel.mTitle = title;
            return this;
        }

        public Builder setDescription(String description) {
            recordingDBModel.mDescription = description;
            return this;
        }

        public Builder setGenres(List<String> genres) {
            recordingDBModel.mGenres = genres;
            return this;
        }

        public Builder setSeason(Integer season) {
            recordingDBModel.mSeasonNumber = season;
            return this;
        }

        public Builder setEpisode(Integer episode) {
            recordingDBModel.mEpisodeNumber = episode;
            return this;
        }

        public Builder setEpisodeTitle(String episodeTitle) {
            recordingDBModel.mEpisodeTitle = episodeTitle;
            return this;
        }

        public Builder setSeriesID(String seriesProgramID) {
            recordingDBModel.mSeriesId = seriesProgramID;
            return this;
        }

        public Builder setUnskipableAssetID(String value) {
            recordingDBModel.mUnskipableAssetID = value;
            return this;
        }

        public Builder setParentalRating(ParentalRating parentalRating) {
            recordingDBModel.mParentalRating = parentalRating;
            return this;
        }

        public Builder setReleaseYear(String releaseYear) {
            recordingDBModel.mReleaseYear = releaseYear;
            return this;
        }

        public Builder setActors(List<String> actors) {
            recordingDBModel.mActors = actors;
            return this;
        }

        public Builder setDirectors(List<String> directors) {
            recordingDBModel.mDirectors = directors;
            return this;
        }

        public Builder setIsBlackListed(boolean isBlackListed) {
            recordingDBModel.isBlacklisted = isBlackListed;
            return this;
        }

        public Builder setNPVREnabled(boolean isNPVREnabled) {
            recordingDBModel.mIsNPVREnabled = isNPVREnabled;
            return this;
        }

        public Builder setFailedRecording(boolean isFailed) {
            recordingDBModel.mIsFailedRecording = isFailed;
            return this;
        }

        public Builder setImageSources(List<IImageSource> imageSources) {
            recordingDBModel.mImageSources = imageSources;
            return this;
        }

        public Builder setHasDescriptiveAudio(boolean hasDescriptiveAudio) {
            recordingDBModel.mHasDescriptiveAudio = hasDescriptiveAudio;
            return this;
        }

        public Builder setHasDescriptiveSubtitle(boolean hasDescriptiveSubtitle) {
            recordingDBModel.mHasDescriptiveSubtitle = hasDescriptiveSubtitle;
            return this;
        }

        public IRecording build() {
            return recordingDBModel;
        }
    }

    @NonNull
    @Override
    public String toString() {
        return "RecordingDBModel{" +
                "mId='" + mId + '\'' +
                ", mRecordingId='" + mRecordingId + '\'' +
                ", mSystemRecordId='" + mSystemRecordId + '\'' +
                ", mChannelId='" + mChannelId + '\'' +
                ", mRecordingStart=" + mRecordingStart +
                ", mRecordingEnd=" + mRecordingEnd +
                ", mProgramStart=" + mProgramStart +
                ", mProgramEnd=" + mProgramEnd +
                ", mTitle='" + mTitle + '\'' +
                ", mDescription='" + mDescription + '\'' +
                ", mGenres=" + mGenres +
                ", mReleaseYear='" + mReleaseYear + '\'' +
                ", mSeasonNumber=" + mSeasonNumber +
                ", mEpisodeNumber=" + mEpisodeNumber +
                ", mEpisodeTitle='" + mEpisodeTitle + '\'' +
                ", mProgramId='" + mProgramId + '\'' +
                ", mSeriesId='" + mSeriesId + '\'' +
                ", mActors=" + mActors +
                ", mDirectors=" + mDirectors +
                ", mParentalRating=" + mParentalRating +
                ", isBlacklisted=" + isBlacklisted +
                ", mImageSources=" + mImageSources +
                ", mIsNPVREnabled=" + mIsNPVREnabled +
                ", mIsFailedRecording=" + mIsFailedRecording +
                '}';
    }
}
