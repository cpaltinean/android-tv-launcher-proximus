package tv.threess.threeready.data.account.adapter;

import androidx.annotation.Nullable;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Json type adapter to map the value to an enum.
 *
 * @author Barabas Attila
 * @since 5/18/21
 */
public abstract class NameTypeMappingTypeAdapter<T> extends TypeAdapter<T> {
    private Map<T, String> mTypeNameMap;
    private Map<String, T> mNameTypeMap;

    private void buildTypeMap() {
        mNameTypeMap = createNameTypeMap();
        mTypeNameMap = mNameTypeMap.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
    }

    protected abstract Map<String, T> createNameTypeMap();

    @Nullable
    public T getDefaultValue() {
        return null;
    }

    @Override
    public void write(JsonWriter out, T value) throws IOException {
        if (mTypeNameMap == null) {
            buildTypeMap();
        }
        out.value(mTypeNameMap.get(value));
    }

    @Override
    public T read(JsonReader in) throws IOException {
        if (mNameTypeMap == null) {
            buildTypeMap();
        }
        T type = mNameTypeMap.get(in.nextString());
        if (type == null) {
            return getDefaultValue();
        }
        return type;
    }
}
