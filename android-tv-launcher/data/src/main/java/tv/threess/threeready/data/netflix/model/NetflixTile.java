package tv.threess.threeready.data.netflix.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.netflix.INetflixTile;

/**
 * Configuration for a netflix tile response.
 *
 * @author David
 * @since 25.02.2022
 */
public class NetflixTile implements INetflixTile {
    @SerializedName("title")
    String mTitle;

    @SerializedName("deepLink")
    String mDeepLink;

    @SerializedName("description")
    String mDescription;

    @SerializedName("images")
    TileImages mImages;

    @SerializedName("pvid")
    String mPvid;

    @Override
    public String getId() {
        return String.valueOf(hashCode());
    }

    @Override
    public List<IImageSource> getImageSources() {
        return Collections.singletonList(mImages.getTile());
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getDeepLink() {
        return mDeepLink;
    }

    @Override
    public String getDescription() {
        return mDescription;
    }

    @Override
    public boolean isProfileTile() {
        // Only the VideoTiles have pvid.
        return TextUtils.isEmpty(mPvid);
    }
}
