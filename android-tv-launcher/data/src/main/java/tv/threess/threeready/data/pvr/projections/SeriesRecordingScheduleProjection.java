package tv.threess.threeready.data.pvr.projections;

import android.database.Cursor;

import tv.threess.threeready.data.pvr.PvrContract;
import tv.threess.threeready.api.pvr.model.ISeriesRecordingSchedule;
import tv.threess.threeready.data.generic.IProjection;
import tv.threess.threeready.data.pvr.model.SeriesRecordingScheduleDBModel;

/**
 * Projection to get scheduled series entity.
 *
 * Created by Noemi Dali on 17.04.2020.
 */
public enum SeriesRecordingScheduleProjection implements IProjection {
    SERIES_ID(PvrContract.SeriesRecording.SERIES_ID),
    USER_SERIES_ID(PvrContract.SeriesRecording.USER_SERIES_ID),
    CHANNEL_ID(PvrContract.SeriesRecording.CHANNEL_ID),
    TITLE(PvrContract.SeriesRecording.TITLE);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    SeriesRecordingScheduleProjection(String column) {
        mColumn = column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    public static ISeriesRecordingSchedule readRow(Cursor cursor) {
        return SeriesRecordingScheduleDBModel.Builder.builder()
                .setSeriesId(cursor.getString(SERIES_ID.ordinal()))
                .setUserSeriesId(cursor.getString(USER_SERIES_ID.ordinal()))
                .setChannelId(cursor.getString(CHANNEL_ID.ordinal()))
                .setSeriesTitle(cursor.getString(TITLE.ordinal()))
                .build();
    }
}
