/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.home.observable;

import android.content.Context;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceParams;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.tv.TvContract;

/**
 * Observable for getting the channel list from the database cache.
 * This observable checks if there are data inserted in the database and if not then it reacts on
 * cursor notifications.
 * <p>
 * This observable needs to be a hot one so it emits data to whoever is subscribed after every update in the database.
 *
 * @author Lukacs Andor
 * @since 2017.05.25
 */
public class TvChannelObservable extends BaseContentObservable<ModuleData<TvChannel>> {
    public static final String TAG = Log.tag(TvChannelObservable.class);

    private final TvCache mTvCache = Components.get(TvCache.class);

    private final ModuleConfig mModuleConfig;
    private final int mCount;

    public TvChannelObservable(Context context, ModuleConfig moduleConfig, int count) {
        super(context);
        mModuleConfig = moduleConfig;
        mCount = count;
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<ModuleData<TvChannel>> observableEmitter) {
        if (uri.equals(TvContract.Channel.CONTENT_URI)) {
            emitData(observableEmitter);
            //in case of hot observable, we don't need this step
            unregisterObserver();
        }
    }

    @Override
    public void subscribe(ObservableEmitter<ModuleData<TvChannel>> emitter) throws Exception {
        super.subscribe(emitter);
        List<TvChannel> channels = getTvChannels();
        if (channels == null || channels.isEmpty()) {
            registerObserver(TvContract.Channel.CONTENT_URI);
        }
        emitter.onNext(new ModuleData<>(mModuleConfig, channels));
    }

    /**
     * Emit the data through the given {@link ObservableEmitter} so the subscribed user can react on it.
     *
     * @param emitter The emitter used to emit the list of {@link TvChannel}
     */
    private void emitData(ObservableEmitter<ModuleData<TvChannel>> emitter) {
        emitter.onNext(new ModuleData<>(mModuleConfig, getTvChannels()));
        //do not call on Complete in case of hot observable
        emitter.onComplete();
    }

    private List<TvChannel> getTvChannels() {
        if (mModuleConfig.getDataSource() != null && mModuleConfig.getDataSource().getParams() != null) {
            ModuleDataSourceParams params = mModuleConfig.getDataSource().getParams();
            String typeFilter = params.getFilter(ModuleFilterOption.Channel.Type.NAME);
            String languageFilter = params.getFilter(ModuleFilterOption.Channel.Language.NAME);
            String idFilter = params.getFilter(ModuleFilterOption.Channel.ChannelId.NAME);
            String[] sortOptions = mModuleConfig.getDataSource().getParams().getSort();

            return mTvCache.getTvChannels(typeFilter, languageFilter, idFilter, sortOptions, mCount);

        }
        return new ArrayList<>();
    }
}
