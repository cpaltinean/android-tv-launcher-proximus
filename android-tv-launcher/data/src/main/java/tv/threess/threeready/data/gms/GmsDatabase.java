/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.gms;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import tv.threess.threeready.data.generic.helper.BaseSQLiteOpenHelper;

/**
 * Helper class to manage database creation and version management.
 *
 * @author Eugen Guzyk
 * @since 2018.12.19
 */
public class GmsDatabase extends BaseSQLiteOpenHelper {

    // database file name
    private static final String NAME = "gms.db";

    // database version number
    private static final int VERSION = 2;

    public GmsDatabase(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + GmsContract.StoredNotification.TABLE_NAME + " ("
                + GmsContract.StoredNotification.ID + " INTEGER PRIMARY KEY NOT NULL,"
                + GmsContract.StoredNotification.COLUMN_SBN_KEY + " TEXT NOT NULL UNIQUE,"
                + GmsContract.StoredNotification.COLUMN_PACKAGE_NAME + " TEXT,"
                + GmsContract.StoredNotification.COLUMN_NOTIF_TITLE + " TEXT,"
                + GmsContract.StoredNotification.COLUMN_NOTIF_TEXT + " TEXT,"
                + GmsContract.StoredNotification.COLUMN_AUTODISMISS + " INTEGER,"
                + GmsContract.StoredNotification.COLUMN_DISMISSIBLE + " INTEGER,"
                + GmsContract.StoredNotification.COLUMN_ONGOING + " INTEGER,"
                + GmsContract.StoredNotification.COLUMN_SMALL_ICON + " BLOB,"
                + GmsContract.StoredNotification.COLUMN_IMPORTANCE + " INTEGER,"
                + GmsContract.StoredNotification.COLUMN_PROGRESS + " INTEGER,"
                + GmsContract.StoredNotification.COLUMN_PROGRESS_MAX + " INTEGER,"
                + GmsContract.StoredNotification.COLUMN_FLAGS + " INTEGER,"
                + GmsContract.StoredNotification.COLUMN_HAS_CONTENT_INTENT + " INTEGER,"
                + GmsContract.StoredNotification.COLUMN_BIG_PICTURE + " BLOB,"
                + GmsContract.StoredNotification.COLUMN_CONTENT_BUTTON_LABEL + " TEXT,"
                + GmsContract.StoredNotification.COLUMN_DISMISS_BUTTON_LABEL + " TEXT,"
                + GmsContract.StoredNotification.COLUMN_TAG + " TEXT,"
                + GmsContract.StoredNotification.COLUMN_READ + " INTEGER )"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS " + GmsContract.Apps.TABLE_NAME + " ("
                + GmsContract.Apps.PACKAGE_NAME + " TEXT PRIMARY KEY NOT NULL,"
                + GmsContract.Apps.LAST_OPENED + " INTEGER)"
        );

    }

    @Override
    public boolean onUpgrade(SQLiteDatabase db, int toVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + GmsContract.StoredNotification.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + GmsContract.Apps.TABLE_NAME);
        onCreate(db);
        return true;
    }
}
