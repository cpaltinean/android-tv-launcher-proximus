package tv.threess.threeready.data.pvr;

import android.app.Application;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.model.CacheSettings;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.data.utils.CacheUpdater;
import tv.threess.threeready.data.utils.PeriodicCacheUpdater;

/**
 * Base common implementation class for the pvr handler.
 * All generic pvr related action should be implemented here.
 *
 * @author David
 * @since 22.11.2021
 */
public class BasePvrRepository {
    protected final Application mApp;

    public BasePvrRepository(Application application) {
        mApp = application;
    }

    public static final PeriodicCacheUpdater pvrUpdater = new PeriodicCacheUpdater(Settings.lastRecordingSyncTime,
            Components.get(CacheSettings.class)::getRecordingUpdatePeriod);
}
