package tv.threess.threeready.data.account.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import tv.threess.threeready.api.home.model.page.PageType;

/**
 * Json type adapter to read the value as page type enum.
 *
 * @author Barabas Attila
 * @since 5/14/21
 */
public class PageTypeAdapter3Ready extends TypeAdapter<PageType> {
    private static final String MODULAR_PAGE = "modular_page";
    private static final String STATIC_PAGE = "static_page";

    @Override
    public void write(JsonWriter out, PageType value) throws IOException {
        if (value == PageType.MODULAR_PAGE) {
            out.value(MODULAR_PAGE);
        } else {
            out.value(STATIC_PAGE);
        }
    }

    @Override
    public PageType read(JsonReader in) throws IOException {
        String value = in.nextString();
        if (MODULAR_PAGE.equals(value)) {
            return PageType.MODULAR_PAGE;
        }
        return PageType.STATIC_PAGE;
    }
}
