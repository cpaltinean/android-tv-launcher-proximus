/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.tv;

import android.app.Application;
import android.util.Range;

import androidx.annotation.Nullable;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.generic.helper.TimeUtils;
import tv.threess.threeready.api.generic.receivers.StandByStateChangeReceiver;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.model.TifChannelList;
import tv.threess.threeready.api.tv.TvServiceRepository;
import tv.threess.threeready.api.tv.model.ITvChannelResponse;
import tv.threess.threeready.api.tv.model.TifScanParameters;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.generic.MwScanReceiver;
import tv.threess.threeready.data.mw.MwProxy;

/**
 * Base class for a TV service handler. {@link TvService}
 * All the common TV related service action should be handled here.
 *
 * @author Barabas Attila
 * @since 2018.04.30
 */
public abstract class BaseTvServiceRepository extends BaseTvRepository implements TvServiceRepository {
    private static final String TAG = Log.tag(BaseTvServiceRepository.class);

    private static final long PROGRAM_GUIDE_WINDOW_START_LENGTH = TimeUnit.DAYS.toMillis(7);

    protected final AppConfig mAppConfig = Components.get(AppConfig.class);
    protected final MwProxy mMwProxy = Components.get(MwProxy.class);
    protected final TvCache mTvCache = Components.get(TvCache.class);

    public BaseTvServiceRepository(Application app) {
        super(app);

        // Detect standby state changes.
        Components.get(StandByStateChangeReceiver.class)
                .addStateChangedListener(mStandByStateChangeListener);
    }

    @Override
    public void checkTIFPlaybackOptions() throws Exception {
        mTvCache.checkTIFPlaybackOptions();
    }

    /**
     * Update the cached channels and validate all the playback options.
     */
    protected <Channel extends TvChannel> void updateChannels(ITvChannelResponse<Channel> response) throws Exception {
        TifScanParameters scanOptions = response.getScanParameters();
        MwScanReceiver scanNotifier = null;
        Exception error = null;
        try {
            if (scanOptions != null && scanOptions.canScan()) {
                Log.d(TAG, "Update channels - performing scan: " + scanOptions);
                scanNotifier = Components.get(MwScanReceiver.class);
                scanNotifier.setInProgress(true);

                boolean scanSuccess = mMwProxy.startScan(scanOptions).waitForResult(MwProxy.SCAN_TIMEOUT);
                Log.d(TAG, "Update channels - scan finished with success: " + scanSuccess);
            } else {
                Log.d(TAG, "Update channels - scan skipped with option: " + scanOptions);
            }
        } catch (Exception e) {
            error = e;
        }

        try {
            // get tif channels to be merged with the playback options
            TifChannelList tifChannels = mMwProxy.getTifChannels();
            Log.d(TAG, "Update channels - retrieved " + tifChannels.size() + " tif channels");
            mTvCache.updateChannels(response, tifChannels);
        } catch (Exception e) {
            if (error == null) {
                error = e;
            }
        }

        if (scanNotifier != null) {
            scanNotifier.setInProgress(false);
        }

        if (error != null) {
            throw error;
        }
    }

    /**
     * Calculate the time range of the missing broadcast in the cache.
     *
     * @param wantedFrom The start of the time range from where the cache needs to checked.
     * @param wantedTo   The end of the time range until the cache needs to checked.
     * @return The time range which is missing from the cache or null if everything is there.
     */
    @Nullable
    protected Range<Long> calculateMissingBroadcastsInterval(long wantedFrom, long wantedTo) {
        Log.d(TAG, "Calculate Missing Broadcasts Interval: from : " + Log.timestamp(wantedFrom) + " ,to : " + Log.timestamp(wantedTo));

        Range<Long> wantedRange = Range.create(wantedFrom, wantedTo);
        Range<Long> cachedRange = mTvCache.getCachedBroadcastInterval();

        // Cache empty.
        if (ArrayUtils.isEmpty(cachedRange)) {
            Log.d(TAG, "Database empty, will get from the server everything ["
                    + wantedFrom + "],[" + wantedTo + "]");
            return wantedRange;
        }

        // Everything is in the cache.
        if (cachedRange.contains(wantedRange)) {
            Log.d(TAG, "Everything is in cache.");
            return null;
        }

        long missingStart;
        long missingEnd;

        // Cache inside.
        // TODO : Needs improvement. We get everything from backend even if a part is already in the cache.
        if (wantedRange.contains(cachedRange)) {
            missingStart = wantedFrom;
            missingEnd = wantedTo;
            Log.d(TAG, "Cache inside, server call is  ["
                    + new Date(missingStart) + "],[" + new Date(missingEnd) + "]");

            // Gap
        } else if (TimeUtils.gapBetween(cachedRange.getLower(), cachedRange.getUpper(), wantedFrom, wantedTo)) {
            missingStart = Math.min(wantedFrom, cachedRange.getUpper());
            missingEnd = Math.max(wantedTo, cachedRange.getLower());
            Log.d(TAG, "Gap, server call is  ["
                    + new Date(missingStart) + "],[" + new Date(missingEnd) + "]");

            // Missing start.
        } else if (wantedFrom < cachedRange.getLower()) {
            missingStart = wantedFrom;
            missingEnd = cachedRange.getLower();
            Log.d(TAG, "Start intersection, server call ["
                    + new Date(missingStart) + "],[" + new Date(missingEnd) + "]");

            // Missing end.
        } else {
            missingStart = cachedRange.getUpper();
            missingEnd = wantedTo;
            Log.d(TAG, "End intersection, server call ["
                    + new Date(missingStart) + "],[" + new Date(missingEnd) + "]");
        }

        // Check the EPG window length.
        long windowStart = System.currentTimeMillis() - PROGRAM_GUIDE_WINDOW_START_LENGTH;
        if (missingStart < windowStart) {
            missingStart = windowStart;
        }

        return Range.create(missingStart, missingEnd);
    }


    // Get notification about the standby state changes.
    private final StandByStateChangeReceiver.Listener mStandByStateChangeListener = screenOn -> {
        // Cancel the periodic EPG update during standby. Will be loaded during the standby flow.
        if (!screenOn) {
            BaseWorkManager.cancelAlarm(mApp, TvService.buildChannelUpdateIntent());
            BaseWorkManager.cancelAlarm(mApp, TvService.buildBroadcastUpdateIntent());
        }
    };
}
