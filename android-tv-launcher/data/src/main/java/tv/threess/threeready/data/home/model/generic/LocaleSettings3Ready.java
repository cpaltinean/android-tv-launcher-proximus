 /*
  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
  */
package tv.threess.threeready.data.home.model.generic;

 import com.google.gson.annotations.SerializedName;

 import java.io.Serializable;

 import tv.threess.threeready.api.home.model.generic.LocaleSettings;

 /**
 * Configuration for currency and date/time format displayed in app.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public class LocaleSettings3Ready implements Serializable, LocaleSettings {

    @SerializedName("date_format")
    protected String mDateFormat;

    @SerializedName("time_format")
    protected String mTimeFormat;

    @SerializedName("currency")
    protected String mCurrency;

    @Override
    public String getDateFormat() {
        return mDateFormat;
    }

    @Override
    public String getTimeFormat() {
        return mTimeFormat;
    }

     @Override
     public String getCurrency() {
         return mCurrency;
     }
 }
