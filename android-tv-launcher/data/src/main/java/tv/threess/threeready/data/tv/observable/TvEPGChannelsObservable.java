/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.tv.observable;

import android.content.Context;
import android.net.Uri;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.FeatureControl;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.TvChannelCacheType;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.home.observable.TvChannelObservable;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.tv.TvContract;

/**
 * Returns both favorite and not favorite channels but in two separate lists.
 * Favorites are order by favorite position and not favorites by channel position
 *
 * @author Daniel Gliga
 * @since 2017.07.24
 */

public class TvEPGChannelsObservable extends BaseContentObservable<Map<TvChannelCacheType, List<TvChannel>>> {
    public static final String TAG = Log.tag(TvChannelObservable.class);

    private final TvChannelCacheType channelCacheType;

    private final FeatureControl mFeatureControl = Components.get(FeatureControl.class);
    private final TvCache mTvCache = Components.get(TvCache.class);

    public TvEPGChannelsObservable(Context context) {
        super(context);
        channelCacheType = mFeatureControl.displayAllChannels() ? TvChannelCacheType.ALL : TvChannelCacheType.NOT_FAVORITE;
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<Map<TvChannelCacheType, List<TvChannel>>> observableEmitter) {
        emitData(observableEmitter);
    }

    @Override
    public void subscribe(ObservableEmitter<Map<TvChannelCacheType, List<TvChannel>>> emitter) throws Exception {
        super.subscribe(emitter);

        emitData(emitter);

        registerObserver(TvContract.Channel.CONTENT_URI);
    }

    /**
     * Emit the data through the given {@link ObservableEmitter} so the subscribed user can react on it.
     *
     * @param emitter The emitter used to emit the list of {@link TvChannel}
     */
    private void emitData(ObservableEmitter<Map<TvChannelCacheType, List<TvChannel>>> emitter) {
        Map<TvChannelCacheType, List<TvChannel>> channels = new HashMap<>();
        List<TvChannel> favChannels = mTvCache.getChannels(TvChannelCacheType.FAVORITE, true);
        List<TvChannel> allChannels = mTvCache.getChannels(channelCacheType, true);

        channels.put(TvChannelCacheType.FAVORITE, favChannels);
        channels.put(channelCacheType, allChannels);

        emitter.onNext(channels);
    }
}
