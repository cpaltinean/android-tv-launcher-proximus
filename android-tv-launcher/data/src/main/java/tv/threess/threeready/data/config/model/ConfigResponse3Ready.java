/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.config.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import tv.threess.threeready.api.config.model.Config;

/**
 * Created by Barabas Attila on 4/20/2017.
 */

public class ConfigResponse3Ready implements Serializable {

    @SerializedName("success")
    protected boolean mSuccess;

    @SerializedName("err")
    protected Object mError;

    @SerializedName("data")
    protected Config3Ready mConfig;

    public boolean isSuccess() {
        return mSuccess;
    }

    public Object getError() {
        return mError;
    }

    public Config getConfig() {
        return mConfig;
    }

}
