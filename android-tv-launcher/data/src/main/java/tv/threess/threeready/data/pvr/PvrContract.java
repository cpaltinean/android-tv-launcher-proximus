/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.pvr;

import android.content.ContentResolver;
import android.net.Uri;

import androidx.annotation.Nullable;

import java.util.List;
import java.util.Objects;

import tv.threess.threeready.api.BuildConfig;
import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.tv.model.IBroadcast;

/**
 * Contract class to access data in the pvr database
 *
 * @author Denisa Trif
 * @since 2018.09.14
 */
public class PvrContract {

    public static final String AUTHORITY = BuildConfig.PVR_PROVIDER;

    private static final String EPISODE_PATH = "episode";
    private static final String SERIES_PATH = "series";
    private static final String PROGRAM_PATH = "program";

    private static final Uri BASE_URI = new Uri.Builder()
            .scheme(ContentResolver.SCHEME_CONTENT).authority(AUTHORITY).build();

    /**
     * Builds a URI that points to a recording with the given broadcast.
     *
     * @param broadcast The broadcast to generate recording content uri for.
     */
    public static Uri buildRecordingUriForBroadcast(IBroadcast broadcast) {
        if (broadcast.isEpisode()) {
            return Recording.CONTENT_URI.buildUpon()
                    .appendEncodedPath(SERIES_PATH)
                    .appendEncodedPath(broadcast.getSeriesId())
                    .appendEncodedPath(EPISODE_PATH)
                    .appendEncodedPath(broadcast.getId())
                    .build();
        }

        return Recording.CONTENT_URI.buildUpon()
                .appendEncodedPath(PROGRAM_PATH)
                .appendEncodedPath(broadcast.getId())
                .build();
    }

    /**
     * @param uri The uri to get the broadcast id from.
     * @return The broadcast id from the recording URI path when available.
     * Or null if there when there is no broadcast id in the URI.
     */
    @Nullable
    public static String getBroadcastIdFromUri(Uri uri) {
        List<String> pathSegments = uri.getPathSegments();
        for (int i = 0; i < pathSegments.size() - 1; ++i) {
            String segment = pathSegments.get(i);
            if (Objects.equals(segment, PROGRAM_PATH)
                    || Objects.equals(segment, EPISODE_PATH)) {
                return pathSegments.get(i + 1);
            }
        }
        return null;
    }

    /**
     * Builds a URI that points to a recordings with the given series id.
     *
     * @param seriesId The id of the series.
     */
    public static Uri buildRecordingUriForSeries(String seriesId) {
        return Recording.CONTENT_URI.buildUpon()
                .appendEncodedPath(SERIES_PATH)
                .appendEncodedPath(seriesId)
                .build();
    }

    /**
     * @param uri THe uri to get the series id from.
     * @return The series id from the recording uri when available.
     * Or null when there is no series id in the uri.
     */
    @Nullable
    public static String getSeriesIdFromUri(Uri uri) {
        List<String> pathSegments = uri.getPathSegments();
        for (int i = 0; i < pathSegments.size() - 1; ++i) {
            String segment = pathSegments.get(i);
            if (Objects.equals(segment, SERIES_PATH)) {
                return pathSegments.get(i + 1);
            }
        }
        return null;
    }

    public interface Recording {
        /**
         * Table name
         */
        String TABLE_NAME = BaseContract.tableName(Recording.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(BASE_URI, PATH);

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(Recording.class);

        /**
         * Unique identifier for the recording item.
         */
        String RECORDING_ID = "recording_id";

        /**
         * Identifier of the broadcast.
         */
        String BROADCAST_ID = "broadcast_id";

        /**
         * Identifier for system recording id.
         */
        String SYSTEM_RECORD_ID = "system_id";

        /**
         * Unique identifier for a channel.
         * <P>Type: Number</P>
         */
        String CHANNEL_ID = "channel_id";

        /**
         * Recording start time UTC.
         * <P>Type: Number</P>
         */
        String RECORDING_START = "rec_start";

        /**
         * Recording end time UTC.
         * <P>Type: Number</P>
         */
        String RECORDING_END = "rec_end";

        /**
         * Program start time UTC.
         * <P>Type: Number</P>
         */
        String BROADCAST_START = "br_start";

        /**
         * Program end time UTC.
         * <P>Type: Number</P>
         */
        String BROADCAST_END = "br_end";

        /**
         * Recording title.
         * <P>Type: String</P>
         */
        String TITLE = "title";

        /**
         * Program description.
         * <P>Type: String</P>
         */
        String DESCRIPTION = "description";

        /**
         * Genres, separated with ','.
         * <P>Type: String </>
         */
        String GENRES = "genres";

        /**
         * Release year of the broadcast.
         * <P>Type: String</>
         */
        String RELEASE_YEAR = "release_year";

        /**
         * Season.
         * <P>Type: String</P>
         */
        String SEASON = "season";

        /**
         * Episode.
         * <P>Type: String</P>
         */
        String EPISODE = "episode";

        /**
         * A specific title for this program's episode.
         * <P>Type: String</P>
         */
        String EPISODE_TITLE = "episode_title";

        /**
         * Unique identifier for the program.
         * <P>Type: String</P>
         */
        String PROGRAM_ID = "program_id";

        /**
         * Unique identifier of the series.
         * Only available for episodes.
         */
        String SERIES_ID = "series_id";

        /**
         * Unique identifier for the unskipable advertisement.
         * <P>Type: String</P>
         */
        String UNSKIPABLE_ASSET_ID = "unskipable_asset_id";

        /**
         * Actor list joined by ','.
         * <P>Type: String</P>
         */
        String ACTORS = "actors";

        /**
         * Director list joined by ','.
         * <P>Type: String</P>
         */
        String DIRECTOR = "director";

        /**
         * Parental rating value of the broadcast.
         * <P>Type: String</></>
         */
        String PARENTAL_RATING = "parental_rating";

        /**
         * Marks if the recording is blacklisted or not.
         * <P>Type: Integer</>
         */
        String IS_BLACKLISTED = "is_blacklisted";

        /**
         * Marks if the stream is high definition or not.
         */
        String IS_HD = "is_hd";

        /**
         * Marks if the npvr is enabled for the recording.
         */
        String IS_NPVR_ENABLED = "npvr_enabled";
        /**
         * The image urls of the broadcast joined by ','.
         * <P>Type: String</P>
         */
        String IMAGE_URL = "image_url";

        /**
         * Recorded status value of the recording.
         * <P>Type: Boolean</></>
         */
        String FAILED_RECORDING = "failed_recording";

        /**
         * The type of each image url joined by ','.
         * E.g TCover_W320_H240,E.g TBackground_W120_H240
         * <p>
         * The type consist of 3 part :
         * T - The name of the image source type. {@link IImageSource.Type}
         * W - The width of the image in pixel when defined.
         * H - The height of the image in pixel when defined.
         *
         * <P>Type: String</P>
         */
        String IMAGE_TYPE = "image_type";

        /**
         * Marks if the descriptive audio is enabled for the recording.
         */
        String HAS_DESCRIPTIVE_AUDIO = "has_descriptive_audio";

        /**
         * Marks if the descriptive subtitle is enabled for the recording.
         */
        String HAS_DESCRIPTIVE_SUBTITLE = "has_descriptive_subtitle";

        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;
    }


    public interface SeriesRecording {

        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(SeriesRecording.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(SeriesRecording.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(PvrContract.BASE_URI, PATH);

        //
        // Columns
        //
        /**
         * Unique identifier for the serie.
         * <P>Type: String</P>
         */
        String SERIES_ID = "series_id";

        /**
         * Unique identifier for the series.
         * <P>Type: String</P>
         */
        String USER_SERIES_ID = "user_series_id";

        /**
         * Unique identifier for a channel item
         * <P>Type: Number</P>
         */
        String CHANNEL_ID = "channel_id";

        /**
         * Series title.
         * <P>Type: String</P>
         */
        String TITLE = "title";

        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;

    }
}
