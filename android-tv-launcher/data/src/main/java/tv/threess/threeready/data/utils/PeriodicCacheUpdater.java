package tv.threess.threeready.data.utils;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.Nullable;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.generic.JobIntent;

/**
 * Cache updater which decides the cache validity based on the last sync time
 * and schedules automatic update after a given period
 *
 * @author Barabas Attila
 * @since 4/21/22
 */
public class PeriodicCacheUpdater extends CacheUpdater {

    protected final Settings mSettings;
    protected final Function<TimeUnit, Long> mUpdatePeriodProvider;

    public PeriodicCacheUpdater(Settings settings) {
        mSettings = settings;
        mUpdatePeriodProvider = null; // No update period.
    }

    public PeriodicCacheUpdater(Settings settings, Function<TimeUnit, Long> updatePeriodProvider) {
        mSettings = settings;
        mUpdatePeriodProvider = updatePeriodProvider;
    }

    @Override
    public boolean isCacheValid() {
        if (mUpdatePeriodProvider == null) {
            // No update period. If has some cache it's valid.
            return hasCache();
        }

        // Check if update needed based on period.
        long period = mUpdatePeriodProvider.apply(TimeUnit.MILLISECONDS);
        return mSettings.get(0L) + period > System.currentTimeMillis();
    }

    @Override
    public boolean hasCache() {
        return mSettings.get(0L) > 0;
    }

    @Override
    public void validateCache() {
        mSettings.edit().put(System.currentTimeMillis());
    }

    @Nullable
    @Override
    public Uri getUpdateUri() {
        return ConfigContract.buildSettingsUriForKey(mSettings.name());
    }

    @Override
    public String getName() {
        return mSettings.name();
    }

    protected long getNextScheduleTime(long period) {
        return BaseWorkManager.getNextUpdateScheduleTime(mSettings.get(0L), period);
    }

    public long getSchedulePeriod() {
        if (mUpdatePeriodProvider == null) {
            // No update schedule available.
            return 0;
        }

        return mUpdatePeriodProvider.apply(TimeUnit.MILLISECONDS);
    }
}
