/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.vod;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.vod.model.IRentOffer;
import tv.threess.threeready.api.vod.model.IRentalExpiration;
import tv.threess.threeready.api.vod.model.IRentalExpirationResponse;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;

/**
 * Proxy (pass-through) interface to access vod related backend data.
 *
 * @author Boldijar Paul
 * @since 2017.05.5
 */
public interface VodProxy extends Component {

    /**
     * Get a VOD object by it's identified.
     *
     * @param id ID of VOD item.
     * @return The VOD item.
     */
    IVodItem getVod(String id) throws IOException;

    /**
     * Request multiple VODs in a batch.
     * @param ids The id of the VODs which needs to be returned.
     * @return A list with the requested VODs
     */
    List<? extends IVodItem> getVod(List<String> ids) throws IOException;

    /**
     * @return List of episodes for the given series, found by series Id.
     */
    List<IBaseVodItem> getEpisodes(String seriesId, String seasonId, Map<String, IRentalExpiration> rentalExpirationMap) throws IOException;

    /**
     * Get all the credits that are available for the user.
     *
     * @return A list of credits that are available for the user.
     */
    int getAvailableCreditCount();

    /**
     * Get the rent offer for a specific vod variant.
     *
     * @param variant A variant to request offer for.
     * @return The rent offer for the variant or null if there is no offer available.
     */
    @Nullable
    IRentOffer getRentOffer(IVodVariant variant);

    /**
     * @return A list with rented VODs.
     * @throws IOException in case of errors.
     */
    IRentalExpirationResponse getRentedVodList() throws IOException;

}
