package tv.threess.threeready.data.home.model.page;

import androidx.annotation.Nullable;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.home.model.generic.ContentConfig;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.home.model.menu.MenuItem;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleType;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.api.home.model.page.PageType;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.data.account.adapter.PageTypeAdapter3Ready;
import tv.threess.threeready.data.home.model.generic.LayoutConfig3Ready;
import tv.threess.threeready.data.home.model.module.ModuleConfig3Ready;

/**
 * Created by Barabas Attila on 4/18/2017.
 */

public class PageConfig3Ready implements PageConfig {

    @SerializedName("id")
    private String mId;

    @SerializedName("title")
    private String mTitle;

    @JsonAdapter(PageTypeAdapter3Ready.class)
    @SerializedName("type")
    private PageType mType;

    @SerializedName("parent")
    private String mParent;

    @SerializedName("modules")
    private List<ModuleConfig3Ready> mModules;

    @SerializedName("subpages")
    private List<String> mSubPages;

    @SerializedName("hint")
    private String mHint;

    @SerializedName("layout_config")
    private LayoutConfig3Ready mLayoutConfig;

    private List<MenuItem> mSubMenus = new ArrayList<>();

    @Override
    public UILog.Page getPageName() {
        if (mModules != null && mModules.size() == 1
                && mModules.get(0).getType() == ModuleType.COLLECTION) {
            return UILog.Page.ContentWallFullContentPage;
        }
        return PageConfig.super.getPageName();
    }

    @Override
    public String getReportName() {
        return mTitle;
    }

    @Override
    public String getReportReference() {
        return mId;
    }

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public PageType getType() {
        return mType;
    }

    @Override
    @Nullable
    public String getParent() {
        return mParent;
    }

    @Override
    @Nullable
    public List<ModuleConfig> getModules() {
        return (List) mModules;
    }

    @Override
    public boolean hasSubPages() {
        for (ModuleConfig3Ready module : ArrayUtils.notNull(mModules)) {
            if (!ArrayUtils.isEmpty(module.getSelectorList())) {
                return true;
            }
        }

        return !ArrayUtils.isEmpty(mSubPages);
    }

    @Override
    public List<MenuItem> getSubMenus() {
        if (ArrayUtils.isEmpty(mSubMenus)) {
            mSubMenus = prepareSubMenus();
        }
        return mSubMenus;
    }

    @Override
    @Nullable
    public String getHint() {
        return mHint;
    }

    @Override
    public LayoutConfig getLayoutConfig() {
        return mLayoutConfig;
    }

    private List<MenuItem> prepareSubMenus() {
        List<MenuItem> subMenus = new ArrayList<>();

        if (mSubPages == null) {
            return subMenus;
        }

        ContentConfig contentConfig = Components.get(ContentConfig.class);

        // Calculate menu items from content config.
        for (String pageId : mSubPages) {
            for (PageConfig subPage : contentConfig.getPageConfigs()) {
                if (pageId.equals(subPage.getId())) {
                    subMenus.add(new MenuItem3Ready(pageId, subPage.getTitle()));
                }
            }
        }

        return subMenus;
    }
}
