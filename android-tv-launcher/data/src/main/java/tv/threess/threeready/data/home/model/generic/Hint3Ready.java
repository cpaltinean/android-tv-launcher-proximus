/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.home.model.generic;


import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.home.model.generic.Hint;
import tv.threess.threeready.api.log.Log;

/**
 * Config hints model class
 *
 * @author Daniel Gliga
 * @since 2017.10.27
 */
public class Hint3Ready implements Serializable, Hint {
    private static final String TAG = Log.tag(Hint3Ready.class);

    @SerializedName("id")
    private String mId;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("images")
    private Images mImages;

    @SerializedName("enabled_tutorial")
    private boolean mEnabledTutorial;

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public boolean getEnabledTutorial() {
        return mEnabledTutorial;
    }

    /**
     * @return Image for the given position, if the position is correct, otherwise null.
     */
    @Override
    @Nullable
    public String getImage(int position) {
        try {
            return getImages().get(position);
        } catch (ArrayIndexOutOfBoundsException e) {
            Log.e(TAG, "Cannot retrieve image from position: " + position + ", language: " +
                    LocaleUtils.getApplicationLanguage(), e);
        }
        return null;
    }

    /**
     * @return List of images for the current language.
     */
    @Override
    public List<String> getImages() {
        switch (LocaleUtils.getApplicationLanguage()) {
            case LocaleUtils.DUTCH_LANGUAGE_CODE:
                return mImages.getNl();
            case LocaleUtils.FRENCH_LANGUAGE_CODE:
                return mImages.getFr();
            case LocaleUtils.ENGLISH_LANGUAGE_CODE:
            default:
                return mImages.getEn();
        }
    }

    private static class Images implements Serializable {
        @SerializedName("en")
        List<String> mEn = null;

        @SerializedName("nl")
        List<String> mNl = null;

        @SerializedName("fr")
        List<String> mFr = null;

        public List<String> getEn() {
            return mEn;
        }

        public List<String> getFr() {
            return mFr;
        }

        public List<String> getNl() {
            return mNl;
        }
    }
}
