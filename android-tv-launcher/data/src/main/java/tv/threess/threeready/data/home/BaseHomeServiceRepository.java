package tv.threess.threeready.data.home;

import android.app.Application;
import android.content.Context;

import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.Config;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.home.HomeServiceRepository;
import tv.threess.threeready.api.home.model.error.ErrorConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.config.model.ConfigResponse3Ready;
import tv.threess.threeready.data.home.model.error.ErrorConfig3Ready;
import tv.threess.threeready.data.home.model.generic.TranslationsResponse3Ready;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.utils.GsonUtils;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Base for home service related logic.
 *
 * @author Barabas Attila
 * @since 4/20/22
 */
public abstract class BaseHomeServiceRepository implements HomeServiceRepository {
    private static final String TAG = Log.tag(BaseHomeServiceRepository.class);

    private static final String LOCAL_CONFIG_FILE_NAME = "config-%1$s.json";
    private static final String CACHED_CONFIG_PREFIX = "cached_config";
    private static final String CACHED_CONFIG_FILE_NAME = CACHED_CONFIG_PREFIX + "-%1$s-%2$s.json";

    private static final String OFFLINE_CONFIG_FILE_NAME = "offline_config.json";
    private static final String ERROR_CONFIG_FILE_NAME = "error_config.json";

    private static final String LOCAL_TRANSLATIONS_FILE_NAME = "strings-%1$s.json";
    private static final String CACHED_TRANSLATIONS_PREFIX = "cached_strings";
    private static final String CACHED_TRANSLATIONS_FILE_NAME = CACHED_TRANSLATIONS_PREFIX + "-%1$s-%2$s.json";

    protected final Application mApp;

    protected final MwProxy mMwProxy = Components.get(MwProxy.class);

    public BaseHomeServiceRepository(Application application) {
        mApp = application;
    }


    /**
     * Loads the currently active 3Ready configuration file.
     *
     * @return A wrapper which holds the configuration and the place from where it was loaded.
     */
    public static Config loadConfig(Context context, boolean isUserAuthenticated, boolean forceLocalConfig) {
        ConfigResponse3Ready config = null;

        // User is not authenticated yet. Use offline config.
        if (!isUserAuthenticated) {
            config = loadOfflineConfig(context);
        }

        // Load cached config.
        if (config == null && !forceLocalConfig) {
            config = loadCachedConfig(context);
        }

        if (config == null) {
            config = loadLocalConfig(context);
        }

        // Load offline config.
        if (config == null) {
            config = loadOfflineConfig(context);
        }

        return config.getConfig();
    }

    /**
     * @return Offline configuration file from assets.
     */
    private static ConfigResponse3Ready loadOfflineConfig(Context context) {
        ConfigResponse3Ready configResponse = null;
        try {
            configResponse = GsonUtils.readJsonFromAssets(context,
                    OFFLINE_CONFIG_FILE_NAME, ConfigResponse3Ready.class);
            Log.d(TAG, "Offline config loaded. " + OFFLINE_CONFIG_FILE_NAME);
        } catch (Exception ex) {
            Log.e(TAG, "Could not load offline config.", ex);
        }
        return configResponse;
    }

    /**
     * @return Cached configuration file from file system.
     */
    private static ConfigResponse3Ready loadCachedConfig(Context context) {
        ConfigResponse3Ready configResponse = null;
        try {
            String configFileName = getCachedConfigFileName();
            configResponse = GsonUtils.readFromFile(context, configFileName, ConfigResponse3Ready.class);
            Log.d(TAG, "Cached config loaded: " + configFileName);
        } catch (Exception ex) {
            Log.e(TAG, "Could not load cached config file. ", ex);
        }
        return configResponse;
    }

    /**
     * @return Configuration file from assets.
     */
    private static ConfigResponse3Ready loadLocalConfig(Context context) {
        ConfigResponse3Ready configResponse = null;
        try {
            String configFileName = getLocalConfigFileName();
            configResponse = GsonUtils.readJsonFromAssets(context, configFileName,
                    ConfigResponse3Ready.class);
            Log.d(TAG, "Local config file loaded from assets: " + configFileName);
        } catch (Exception ex) {
            Log.e(TAG, "Could not load local config file from assets.", ex);
        }
        return configResponse;
    }

    /**
     * Save the give config input in local cache.
     */
    protected void updateCachedConfig(InputStream configStream) {
        try {
            // Save the new config file.
            String cachedConfigPath = getCachedConfigFileName();
            FileUtils.writeToFile(mApp, configStream, cachedConfigPath);
            Log.d(TAG, "Cached client config was updated. " + cachedConfigPath);
        } catch (Exception e) {
            Log.e(TAG, "Could not update the cached configuration file.", e);
        } finally {
            // Remove all previously cached config file.
            removeCachedConfigs();
        }
    }

    /**
     * Removes all previously cached config file.
     */
    private void removeCachedConfigs() {
        File[] cachedConfigFiles = mApp.getFilesDir()
                .listFiles((dir, name) -> name.startsWith(CACHED_CONFIG_PREFIX));
        if (cachedConfigFiles == null) {
            Log.e(TAG, "Could not access cached config files..");
            return;
        }

        for (File file : cachedConfigFiles) {
            String fileVersion = getVersionName(file.getName());
            String appVersion = mMwProxy.getAppVersion();

            if (!appVersion.equalsIgnoreCase(fileVersion)) {
                try {
                    boolean deleted = file.delete();
                    Log.d(TAG, "Remove cached config file. "
                            + file.getName() + ", removed : " + deleted);
                } catch (Exception e) {
                    Log.e(TAG, "Could not clear the config cache.");
                }
            } else {
                Log.d(TAG, "Did not remove cached config because versions match. [" + appVersion + "]");
            }
        }
    }

    /**
     * Get the translations that are saved locally in assets.
     */
    private static Map<String, Map<String, String>> loadLocalTranslations(Context context) {
        Map<String, Map<String, String>> translations = new HashMap<>();

        String translationPath = String.format(LOCAL_TRANSLATIONS_FILE_NAME, LocaleUtils.getApplicationLanguage());
        Log.d(TAG, "Load local translations " + translationPath);

        Map<String, String> currentLanguageTranslations = readTranslation(context, translationPath);
        translations.put(LocaleUtils.getApplicationLanguage(), currentLanguageTranslations);

        if (!LocaleUtils.ENGLISH_LANGUAGE_CODE.equalsIgnoreCase(LocaleUtils.getApplicationLanguage())) {
            String englishTranslationPath = String.format(LOCAL_TRANSLATIONS_FILE_NAME, LocaleUtils.ENGLISH_LANGUAGE_CODE);
            Map<String, String> englishTranslations = readTranslation(context, englishTranslationPath);
            translations.put(LocaleUtils.ENGLISH_LANGUAGE_CODE, englishTranslations);
        }

        return translations;
    }

    /**
     * Read the local translation file from the assets.
     *
     * @param context Context for reading.
     * @param path    Path of translation file.
     * @return A map of key-values, containing the translations.
     */
    private static Map<String, String> readTranslation(Context context, String path) {
        try {
            return GsonUtils.readJsonFromAssets(context, path, TranslationsResponse3Ready.class).getTranslations();
        } catch (Exception e) {
            Log.e(TAG, "Could not find translations in " + path, e);
        }

        return new HashMap<>();
    }

    /**
     * Get the cached translations that were cached from the CDN.
     */
    private static Map<String, Map<String, String>> loadCachedTranslations(Context context) {
        Map<String, Map<String, String>> translations = new HashMap<>();

        try {
            String translationPath = getCachedTranslationsFileName();
            Log.d(TAG, "Load cached translations " + translationPath);

            TranslationsResponse3Ready translationsResponse = GsonUtils.readFromFile(context, translationPath, TranslationsResponse3Ready.class);
            if (translationsResponse != null) {
                Map<String, String> currentLanguageTranslations = translationsResponse.getTranslations();
                if (!currentLanguageTranslations.isEmpty()) {
                    translations.put(LocaleUtils.getApplicationLanguage(), currentLanguageTranslations);
                    Log.d(TAG, "Cached translations loaded successfully.");
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Could not read cached translations.", e);
        }
        return translations;
    }

    /**
     * @return Error handlers from error config file.
     */
    public static List<ErrorConfig> getErrorHandlers(Context context) {
        try {
            return Arrays.asList(GsonUtils.readJsonFromAssets(context, ERROR_CONFIG_FILE_NAME,
                    ErrorConfig3Ready[].class));
        } catch (Exception e) {
            Log.e(TAG, "Could not read error handlers.", e);
        }
        return Collections.emptyList();
    }

    /**
     * @return The name of the cached 3Ready configuration file.
     * Name changes based on the UI language and app version.
     */
    private static String getCachedConfigFileName() {
        return String.format(CACHED_CONFIG_FILE_NAME,
                getConfigLanguage(), Components.get(MwProxy.class).getAppVersion());
    }

    /**
     * @return The name of the cached 3Ready translations file.
     * Name changes based on the UI language and app version.
     */
    private static String getCachedTranslationsFileName() {
        return String.format(CACHED_TRANSLATIONS_FILE_NAME,
                getTranslationsLanguage(),
                Components.get(MwProxy.class).getAppVersion());
    }

    /**
     * Get the version name for a file that matches either
     * {@link #CACHED_TRANSLATIONS_FILE_NAME or tv.threess.threeready.api.config.helper.ConfigUtils#CACHED_CONFIG_FILE_NAME}
     */
    private static String getVersionName(String fileName) {
        String[] segments = fileName.split("-");
        return segments[segments.length - 1].replace(".json", "");
    }

    /**
     * @return The name of the configuration file from assets.
     * Name changes based on the UI language.
     */
    private static String getLocalConfigFileName() {
        return String.format(LOCAL_CONFIG_FILE_NAME,
                getConfigLanguage());
    }

    protected static String getTranslationsLanguage() {
        if (LocaleUtils.FRENCH_LANGUAGE_CODE
                .equals(LocaleUtils.getApplicationLanguage())) {
            return LocaleUtils.FRENCH_LANGUAGE_CODE;
        }
        if (LocaleUtils.DUTCH_LANGUAGE_CODE
                .equals(LocaleUtils.getApplicationLanguage())) {
            return LocaleUtils.DUTCH_LANGUAGE_CODE;
        }

        return LocaleUtils.ENGLISH_LANGUAGE_CODE;
    }

    protected static String getConfigLanguage() {
        if (LocaleUtils.FRENCH_LANGUAGE_CODE
                .equals(LocaleUtils.getApplicationLanguage())) {
            return LocaleUtils.FRENCH_LANGUAGE_CODE;
        }

        return LocaleUtils.DUTCH_LANGUAGE_CODE;
    }

    /**
     * Load the translation files.
     * First, try to load it from the translation file that was saved from the CDN
     * and if that is not there, we load the one we have locally.
     */
    public static Map<String, Map<String, String>> loadTranslations(Context context) {
        Map<String, Map<String, String>> translations;

        translations = loadCachedTranslations(context);
        Log.d(TAG, "loadTranslations() called with: translations = [" + translations + "]");

        if (translations.isEmpty()) {
            translations = loadLocalTranslations(context);
        }

        return translations;
    }

    /**
     * Save the given translations file in local cache.
     */
    protected void updateCachedTranslations(InputStream translations) {
        try {
            // Save the new config file.
            String cachedTranslationsPath = getCachedTranslationsFileName();
            FileUtils.writeToFile(mApp, translations, cachedTranslationsPath);
            Log.d(TAG, "Cached translations was updated. " + cachedTranslationsPath);
            Components.get(Translator.class).setTranslations(loadTranslations(mApp));
        } catch (Exception e) {
            Log.e(TAG, "Could not update the cached translations file.", e);
        } finally {
            // Remove all previously cached config file.
            removeCachedTranslations();
        }
    }

    /**
     * Removes all previously cached translations files.
     */
    private void removeCachedTranslations() {
        File[] cachedTranslations = mApp.getFilesDir()
                .listFiles((dir, name) -> name.startsWith(CACHED_TRANSLATIONS_PREFIX));
        if (cachedTranslations == null) {
            Log.e(TAG, "Couldn't access caches translation files.");
            return;
        }

        for (File file : cachedTranslations) {
            String fileVersion = getVersionName(file.getName());
            String appVersion = mMwProxy.getAppVersion();

            if (!appVersion.equalsIgnoreCase(fileVersion)) {
                try {
                    boolean deleted = file.delete();
                    Log.d(TAG, "Remove cached config file. "
                            + file.getName() + ", removed : " + deleted);
                } catch (Exception e) {
                    Log.e(TAG, "Could not clear the config cache.");
                }
            } else {
                Log.d(TAG, "Did not remove cached translations because versions match. [" + appVersion + "]");
            }
        }
    }
}
