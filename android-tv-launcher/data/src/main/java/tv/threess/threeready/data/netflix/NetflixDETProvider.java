package tv.threess.threeready.data.netflix;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.helper.UriMatcher;
import tv.threess.threeready.data.generic.BaseProvider;

/**
 * Provider class used for handle the Netflix DET response related SQL methods.
 *
 * @author David
 * @since 25.02.2022
 */
public class NetflixDETProvider extends BaseProvider {

    enum UriIDs {
        NetflixDETByCategory,
        NetflixDET
    }

    // prepare the uri matcher
    private static final UriMatcher<NetflixDETProvider.UriIDs> matcher = new UriMatcher<>(NetflixDETContract.AUTHORITY, NetflixDETProvider.UriIDs.class)
            .add(UriIDs.NetflixDET, NetflixDETContract.Netflix.CONTENT_URI)
            .add(UriIDs.NetflixDETByCategory, NetflixDETContract.Netflix.CONTENT_URI_WITH_CATEGORY, "*");

    private NetflixDETDatabase mDatabase;

    @Override
    public boolean onCreate() {
        super.onCreate();
        // prepare the database helper
        mDatabase = new NetflixDETDatabase(this.getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        if (match == UriIDs.NetflixDETByCategory) {
            table = NetflixDETContract.Netflix.TABLE_NAME;
        } else {
            throw new UnsupportedOperationException("Unimplemented match: " + match);
        }

        SQLiteDatabase db = mDatabase.getReadableDatabase();

        // Filter for the given category.
        final String category = uri.getLastPathSegment();
        selection = DatabaseUtils.concatenateWhere(selection,
                NetflixDETContract.Netflix.CATEGORY + " = ? AND " + NetflixDETContract.Netflix.UI_LANGUAGE + " = ?");
        selectionArgs = DatabaseUtils.appendSelectionArgs(selectionArgs, new String[]{category, LocaleUtils.getApplicationLanguage()});

        return super.query(uri, db, table, projection, selection, selectionArgs, sortOrder);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        UriIDs match = matcher.matchEnum(uri);
        if (match == UriIDs.NetflixDETByCategory) {
            return NetflixDETContract.Netflix.MIME_TYPE;
        }
        throw new UnsupportedOperationException("Unimplemented match: " + match);
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        if (match == UriIDs.NetflixDETByCategory) {
            table = NetflixDETContract.Netflix.TABLE_NAME;
        } else {
            throw new UnsupportedOperationException("Unimplemented match: " + match);
        }
        long rowId = super.insert(uri, mDatabase.getWritableDatabase(), table, values);
        return rowId < 0 ? null : uri;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        if (match == UriIDs.NetflixDETByCategory) {
            table = NetflixDETContract.Netflix.TABLE_NAME;
        } else {
            throw new UnsupportedOperationException("Unimplemented match: " + match);
        }

        String replaceTimeStamp = uri.getQueryParameter(BaseContract.BULK_REPLACE);
        String replaceWhereCase = null;
        String[] replaceWhereArgs = null;

        if (!TextUtils.isEmpty(replaceTimeStamp)) {
            replaceWhereCase = BaseContract.LAST_UPDATED + "<" + replaceTimeStamp
                    + " AND " + NetflixDETContract.Netflix.CATEGORY + " =? ";
            replaceWhereArgs = new String[]{uri.getLastPathSegment()};
        }

        return bulkInsertReplacePurge(uri,
                mDatabase.getWritableDatabase(), table, replaceWhereCase, replaceWhereArgs, values);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        if (match == UriIDs.NetflixDETByCategory) {
            table = NetflixDETContract.Netflix.TABLE_NAME;
        } else {
            throw new UnsupportedOperationException("Unimplemented match: " + match);
        }

        // Filter for the given category.
        final String category = uri.getLastPathSegment();
        selection = DatabaseUtils.concatenateWhere(selection, NetflixDETContract.Netflix.CATEGORY + " = ?");
        selectionArgs = DatabaseUtils.appendSelectionArgs(selectionArgs, new String[]{category});

        return super.delete(uri,
                mDatabase.getWritableDatabase(), table, selection, selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            case NetflixDETByCategory:
                table = NetflixDETContract.Netflix.TABLE_NAME;

                final String category = uri.getLastPathSegment();
                selection = DatabaseUtils.concatenateWhere(selection, NetflixDETContract.Netflix.CATEGORY + " = ?");
                selectionArgs = DatabaseUtils.appendSelectionArgs(selectionArgs, new String[]{category});
                break;
            case NetflixDET:
                table = NetflixDETContract.Netflix.TABLE_NAME;
                break;
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);
        }

        return super.update(uri, mDatabase.getWritableDatabase(), table, values, selection, selectionArgs);
    }
}
