/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.account;

import android.app.Application;
import android.text.TextUtils;

import java.io.IOException;
import java.util.Objects;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.model.CacheSettings;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.utils.CacheUpdater;
import tv.threess.threeready.data.utils.PeriodicCacheUpdater;
import tv.threess.threeready.data.vod.VodCategoryCache;

/**
 * Base class for a config service handler. {@link AccountService}
 * All the common config related service action should be handled here.
 *
 * @author Barabas Attila
 * @since 2018.02.10
 */

public abstract class BaseAccountRepository {
    private static final String TAG = Log.tag(BaseAccountRepository.class);

    protected final Application mApp;

    public BaseAccountRepository(Application application) {
        mApp = application;

    }

    protected abstract String getSavedInstance();

    protected abstract void registerInstance(String instanceId) throws IOException;

    /**
     * Register the new app instance.
     */
    public void registerAppInstance(String instanceId) {
        String savedInstance = getSavedInstance();

        if (!Settings.authenticated.get(false)) {
            Log.d(TAG, "Device not registered yet. Ignore firebase registration.");
            return;
        }

        if (Objects.equals(instanceId, savedInstance)) {
            Log.d(TAG, "Registration not needed, token is already registered.");
            return;
        }

        try {
            registerInstance(instanceId);
            Log.d(TAG, "Firebase token registered : " + instanceId);
        } catch (IOException e) {
            Log.e(TAG, "Registration failed: " + e);
        }
    }

    protected static final PeriodicCacheUpdater bootUpdater = new PeriodicCacheUpdater(Settings.lastBootConfigSyncTime,
            Components.get(CacheSettings.class)::getBootConfigUpdatePeriod);

    protected static final PeriodicCacheUpdater accountUpdater = new PeriodicCacheUpdater(Settings.lastAccountInfoSyncTime,
            Components.get(CacheSettings.class)::getAccountInfoUpdatePeriod);

    protected static final PeriodicCacheUpdater watchlistUpdater = new PeriodicCacheUpdater(Settings.lastWatchlistSyncTime,
            Components.get(CacheSettings.class)::getWatchlistUpdatePeriod);

    protected static final PeriodicCacheUpdater globalInstallUpdater = new PeriodicCacheUpdater(Settings.lastGlobalInstallSyncTime,
            Components.get(CacheSettings.class)::getGlobalInstallUpdatePeriod) {
        @Override
        public boolean isCacheValid() {
            // Check if the category memory cache is loaded.
            return Components.get(VodCategoryCache.class)
                    .isCategoryTreeInitialized() && super.isCacheValid();
        }
    };

    protected static final CacheUpdater stbPropertyUpdater = new PeriodicCacheUpdater(Settings.lastSTBPropertiesSyncTime) {
        @Override
        public boolean hasCache() {
            // The cache is optional. Has default values in app.
            return true;
        }

        @Override
        public boolean isCacheValid() {
            return mSettings.get(0L) > 0;
        }
    };

    protected static final CacheUpdater drmRegistrationUpdater = new PeriodicCacheUpdater(Settings.lastDRMClientSyncTime) {
        @Override
        public boolean isCacheValid() {
            // Consider valid if it's cached.
            String ottDRMClientId = Settings.ottDRMClientId.get("");
            String iptvDRMClientId = Settings.iptvDRMClientId.get("");
            return hasCache()  &&
                    !TextUtils.isEmpty(ottDRMClientId) && !TextUtils.isEmpty(iptvDRMClientId);
        }
    };


}
