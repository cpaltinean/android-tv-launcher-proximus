package tv.threess.threeready.data.account;

import android.app.Application;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import tv.threess.lib.di.Component;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountCache;
import tv.threess.threeready.api.account.model.account.IPackage;
import tv.threess.threeready.api.account.model.server.IPosterServerInfo;
import tv.threess.threeready.api.account.model.server.IPosterServerInfoField;
import tv.threess.threeready.api.account.watchlist.IWatchListResponseItem;
import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.model.BookmarkType;
import tv.threess.threeready.api.generic.model.ContinueWatchingConfig;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.IWatchlistItem;
import tv.threess.threeready.api.generic.model.WatchlistType;
import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.data.account.model.BookmarkDBModel;
import tv.threess.threeready.data.account.projection.BookmarksProjection;
import tv.threess.threeready.data.account.projection.PackageProjection;
import tv.threess.threeready.data.account.projection.PosterServerInfoFieldProjection;
import tv.threess.threeready.data.account.projection.WatchlistProjection;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.data.utils.SqlUtils;

/**
 * Implementation of the Config Cache interface methods.
 *
 * @author David
 * @since 16.11.2021
 */
public class AccountLocalCache implements AccountCache.Bookmarks,
        AccountCache.WatchList, AccountCache.Packages, AccountCache.PosterServers, Component {
    private final String TAG = Log.tag(AccountLocalCache.class);

    private static final String SLASH_SEPARATOR = "/";
    private static final String SEPARATOR = "-";
    private static final String IMAGE_TYPE_PART_SEPARATOR = "_";
    private static final String IMAGE_WIDTH_PREFIX = "w";
    private static final String IMAGE_HEIGHT_PREFIX = "h";

    private final Application mApp;

    public AccountLocalCache(Application application) {
        mApp = application;
    }

    // ----------------- Bookmark

    @Override
    @Nullable
    public IBookmark getBookmark(String id, BookmarkType bookmarkType) {
        String selection = ConfigContract.Bookmarks.CONTENT_ID + " = ? AND "
                + ConfigContract.Bookmarks.TYPE + " = ?";

        IBookmark bookmark = SqlUtils.queryFirst(mApp, ConfigContract.Bookmarks.CONTENT_URI,
                BookmarksProjection.PROJECTION,
                selection,
                new String[]{id, bookmarkType.name()},
                BookmarksProjection::readRow
        );

        return bookmark != null ?
                bookmark :
                new BookmarkDBModel.Builder()
                        .setContentId(id)
                        .setType(bookmarkType)
                        .build();
    }

    @Override
    public IBookmark getVodBookmark(IBaseVodItem vod) {
        List<String> variantIds = vod.getVariantIds();
        IBookmark lastUpdated = null;
        if (variantIds == null)
            return null;

        for (String id : variantIds) {
            IBookmark temp = getBookmark(id, BookmarkType.VodVariant);
            if (temp == null) {
                continue;
            }

            if (lastUpdated == null || lastUpdated.getTimestamp() < temp.getTimestamp()) {
                lastUpdated = temp;
            }
        }
        return lastUpdated;
    }

    @Override
    public List<IBookmark> getContinueWatchingBookmarks() {
        String[] bookmarkTypes = new String[]{BookmarkType.Recording.name(), BookmarkType.Replay.name(), BookmarkType.VodVariant.name()};
        String selection = SqlUtils.buildBinding(TvContract.Channel.TYPE, "IN", bookmarkTypes.length);

        ContinueWatchingConfig cwConfig = Components.get(ContinueWatchingConfig.class);
        String sort = ConfigContract.Bookmarks.LAST_UPDATED + " DESC LIMIT " + cwConfig.getContinueWatchingStripeMaxSize();

        return SqlUtils.queryAll(mApp,
                ConfigContract.Bookmarks.CONTENT_URI,
                BookmarksProjection.PROJECTION,
                selection,
                bookmarkTypes,
                sort,
                BookmarksProjection::readRow
        );
    }

    @Override
    public void addBookmark(String contentId, BookmarkType type, long position, long duration) {
        long now = System.currentTimeMillis();
        ContentValues bookmarkContentValues = new ContentValues();
        bookmarkContentValues.put(ConfigContract.Bookmarks.CONTENT_ID, contentId);
        bookmarkContentValues.put(ConfigContract.Bookmarks.TYPE, type.name());
        bookmarkContentValues.put(ConfigContract.Bookmarks.POSITION, position);
        bookmarkContentValues.put(ConfigContract.Bookmarks.DURATION, duration);
        bookmarkContentValues.put(ConfigContract.Bookmarks.LAST_UPDATED, now);

        ContentResolver contentResolver = mApp.getContentResolver();
        SqlUtils.bulkReplace(contentResolver, BaseContract.withBatchUpdate(ConfigContract.Bookmarks.CONTENT_URI), bookmarkContentValues);
        Log.d(TAG, "Bookmark added. Content id : " + contentId + ", type : " + type
                + ", position : " + position + ", duration : " + duration);

        // Notify bookmark added.
        contentResolver.notifyChange(ConfigContract.buildUriForBookmark(contentId), null);
    }

    @Override
    public void deleteBookmarks(BookmarkType bookmarkType, String... contentIds) {
        String selection = ConfigContract.Bookmarks.TYPE + " = ? AND " +
                SqlUtils.buildBinding(ConfigContract.Bookmarks.CONTENT_ID, "IN", contentIds.length);

        List<String> selectionArguments = new ArrayList<>();
        selectionArguments.add(bookmarkType.toString());
        selectionArguments.addAll(java.util.Arrays.asList(contentIds));

        ContentResolver contentResolver = mApp.getContentResolver();
        contentResolver.delete(BaseContract.withBatchUpdate(ConfigContract.Bookmarks.CONTENT_URI),
                selection, selectionArguments.toArray(new String[0]));

        // Notify bookmark for contents deleted.
        for (String contentId : contentIds) {
            contentResolver.notifyChange(ConfigContract.buildUriForBookmark(contentId), null);
            Log.d(TAG, "Bookmark deleted. " + contentId);
        }
    }

    @Override
    public void deleteOlderBookmarks(long olderThan) {
        String selection = BaseContract.LAST_UPDATED + " <= ? ";

        mApp.getContentResolver().delete(
                BaseContract.withBatchUpdate(ConfigContract.Bookmarks.CONTENT_URI), selection,
                new String[]{String.valueOf(olderThan)});
    }

    // ----------------- WatchList

    @Override
    public List<IWatchlistItem> getWatchlistItems(ModuleDataSource dataSourceConfig, int start, int end) {
        return SqlUtils.queryAll(mApp,
                ConfigContract.Watchlist.CONTENT_URI,
                WatchlistProjection.PROJECTION,
                null,
                null,
                ConfigContract.Watchlist.ADDED_AT + " DESC",
                WatchlistProjection::readRow
        );
    }

    @Override
    public String getWatchlistId(String contentId) {
        String selection = ConfigContract.Watchlist.TABLE_NAME + "." + ConfigContract.Watchlist.CONTENT_ID + "=?";
        return SqlUtils.queryFirst(mApp,
                ConfigContract.Watchlist.CONTENT_URI,
                new String[]{ConfigContract.Watchlist.WATCHLIST_ID},
                selection,
                new String[]{contentId},
                cursor -> cursor.getString(cursor.getColumnIndex(ConfigContract.Watchlist.WATCHLIST_ID))
        );
    }

    @Override
    public Boolean isInWatchlist(String contentId) {
        String selection = ConfigContract.Watchlist.TABLE_NAME + "." + ConfigContract.Watchlist.CONTENT_ID + "=?";

        return Optional.ofNullable(SqlUtils.queryFirst(mApp,
                ConfigContract.Watchlist.CONTENT_URI,
                new String[]{ConfigContract.Watchlist.CONTENT_ID},
                selection,
                new String[]{contentId},
                cursor -> true
        )).orElse(false);
    }

    @Override
    public void addToWatchlist(WatchlistType type, String watchListId, String contentId, String timeStamp) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ConfigContract.Watchlist.WATCHLIST_ID, watchListId);
        contentValues.put(ConfigContract.Watchlist.CONTENT_ID, contentId);
        contentValues.put(ConfigContract.Watchlist.TYPE, type.name());
        contentValues.put(ConfigContract.Watchlist.ADDED_AT, timeStamp);
        contentValues.put(ConfigContract.Watchlist.LAST_UPDATED, System.currentTimeMillis());

        mApp.getContentResolver().insert(BaseContract.withBatchUpdate(ConfigContract.Watchlist.CONTENT_URI), contentValues);

        // Notify observers about the watchlist status change.
        mApp.getContentResolver().notifyChange(
                ConfigContract.buildUriForWatchlist(contentId), null);
    }

    @Override
    public void removeFromWatchlist(WatchlistType type, String id, String contentId) {
        String where = ConfigContract.Watchlist.WATCHLIST_ID + "=?";
        mApp.getContentResolver().delete(BaseContract.withBatchUpdate(ConfigContract.Watchlist.CONTENT_URI), where, new String[]{id});

        // Notify observers about the watchlist status change.
        mApp.getContentResolver().notifyChange(
                ConfigContract.buildUriForWatchlist(contentId), null);
    }

    @Override
    public void updateWatchlist(List<? extends IWatchListResponseItem> watchListResponseItems) {
        long now = System.currentTimeMillis();
        List<ContentValues> contentValues = new ArrayList<>();
        for (IWatchListResponseItem item : watchListResponseItems) {
            ContentValues values = new ContentValues();
            values.put(ConfigContract.Watchlist.WATCHLIST_ID, item.getId());
            values.put(ConfigContract.Watchlist.CONTENT_ID, item.getType() == WatchlistType.Broadcast
                    ? buildBroadcastId(item.getChannelId(), item.getItemId()) : item.getItemId());
            values.put(ConfigContract.Watchlist.TYPE, item.getType().name());
            values.put(ConfigContract.Watchlist.ADDED_AT, item.getTimestamp());
            values.put(ConfigContract.Watchlist.LAST_UPDATED, now);
            contentValues.add(values);
        }

        ContentResolver cr = mApp.getContentResolver();
        SqlUtils.bulkReplace(cr, ConfigContract.Watchlist.CONTENT_URI, now, contentValues);
    }

    // ----------------- Packages

    @Override
    public List<IPackage> getSubscriberPackages(String toPackage) {
        String selection = null;
        String[] selectionArgs = null;

        if (toPackage != null) {
            selection = ConfigContract.Packages.EXTERNAL_ID + " =?";
            selectionArgs = new String[]{toPackage};
        }

        return SqlUtils.queryAll(mApp,
                ConfigContract.Packages.CONTENT_URI,
                PackageProjection.PROJECTION,
                selection,
                selectionArgs,
                PackageProjection::readRow
        );
    }

    @Override
    public void updatePackages(Collection<? extends IPackage> packages, long now) {
        List<ContentValues> packageValues = new ArrayList<>();
        for (IPackage packageInfo : packages) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ConfigContract.Packages.PACKAGE_ID, packageInfo.getPackageId());
            contentValues.put(ConfigContract.Packages.EXTERNAL_ID, packageInfo.getExternalId());
            contentValues.put(ConfigContract.Packages.PACKAGE_START, packageInfo.getPackageStartDate());
            contentValues.put(ConfigContract.Packages.PACKAGE_END, packageInfo.getPackageEndDate());
            contentValues.put(ConfigContract.Packages.IS_LOCKED, packageInfo.isLocked());
            contentValues.put(ConfigContract.Packages.PACKAGE_TYPES, packageInfo.getPackageTypes() != null
                    ? String.join(",", packageInfo.getPackageTypes()) : "");
            contentValues.put(ConfigContract.Packages.LAST_UPDATED, now);
            packageValues.add(contentValues);
        }

        ContentResolver cr = mApp.getContentResolver();
        SqlUtils.bulkReplace(cr, ConfigContract.Packages.CONTENT_URI, now, packageValues);
    }

    @Override
    public boolean isPackageSubscribed(String toPackage) {
        if (TextUtils.isEmpty(toPackage)) {
            // not subscribed to nothing
            return false;
        }

        List<IPackage> packages = getSubscriberPackages(toPackage);
        boolean subscribed = packages != null && packages.size() > 0 && packages.get(0).isSubscribed();
        Log.d(TAG, "isSubscribed(toPackage: " + toPackage + "): " + subscribed);
        return subscribed;
    }

    // ----------------- PosterServer

    @Override
    public void updatePosterServers(List<? extends IPosterServerInfo> posterServerList) {
        long now = System.currentTimeMillis();
        ContentResolver cr = mApp.getContentResolver();

        List<ContentValues> posterServerValues = new ArrayList<>();

        for (IPosterServerInfo posterServer : posterServerList) {
            //for each poster server fist we need to add the default image url
            ContentValues contentValues = new ContentValues();
            contentValues.put(ConfigContract.PosterServers.NAME, posterServer.getName());
            contentValues.put(ConfigContract.PosterServers.WIDTH, 0);
            contentValues.put(ConfigContract.PosterServers.HEIGHT, 0);
            contentValues.put(ConfigContract.PosterServers.URL, posterServer.getDefaultUrl());
            contentValues.put(ConfigContract.PosterServers.IS_RESIZABLE, posterServer.isImageResizable());
            contentValues.put(ConfigContract.PosterServers.LAST_UPDATED, now);

            posterServerValues.add(contentValues);

            //if server has different allowed sizes, need to add all of them to database.
            if (posterServer.getAllowedSizes() != null && posterServer.getAllowedSizes().size() > 0) {
                for (IPosterServerInfoField allowedSize : posterServer.getAllowedSizes()) {
                    if ((allowedSize.getWidth() != null && allowedSize.getWidth() != 0f)
                            || (allowedSize.getHeight() != null && allowedSize.getHeight() != 0f)) {
                        contentValues = new ContentValues();
                        contentValues.put(ConfigContract.PosterServers.NAME, posterServer.getName());
                        contentValues.put(ConfigContract.PosterServers.HEIGHT, allowedSize.getHeight());
                        contentValues.put(ConfigContract.PosterServers.WIDTH, allowedSize.getWidth());
                        contentValues.put(ConfigContract.PosterServers.URL, allowedSize.getUri());
                        contentValues.put(ConfigContract.PosterServers.IS_RESIZABLE, false);
                        contentValues.put(ConfigContract.PosterServers.LAST_UPDATED, now);

                        posterServerValues.add(contentValues);
                    }
                }
            }
        }

        SqlUtils.bulkReplace(cr, ConfigContract.PosterServers.CONTENT_URI, now, posterServerValues);
    }

    @Override
    public String getBestPossibleUrl(String filteredImageUrl, int width, int height, String serverName) {
        String selection = ConfigContract.PosterServers.NAME + " =? ";

        List<IPosterServerInfoField> posterServerInfoFields = SqlUtils.queryAll(mApp,
                ConfigContract.PosterServers.CONTENT_URI,
                PosterServerInfoFieldProjection.PROJECTION,
                selection,
                new String[]{serverName},
                PosterServerInfoFieldProjection::readRow
        );

        if (!posterServerInfoFields.isEmpty()) {
            String url = getResizableUrl(posterServerInfoFields, filteredImageUrl, width, height);
            if (url != null && width != 0 && height != 0) {
                return url;
            }
            url = getSizedUrl(posterServerInfoFields, filteredImageUrl, width, height);
            if (url != null) {
                return url;
            }
            return posterServerInfoFields.get(0).getUri() + SLASH_SEPARATOR + filteredImageUrl;
        }
        return "";
    }

    // ---------- private methods

    /**
     * Build unique id for the broadcast.
     *
     * @param channelId The channel id on which the broadcast available.
     * @param trailId   The trail id of the broadcast.
     * @return The generated broadcast id.
     */
    private String buildBroadcastId(String channelId, String trailId) {
        return channelId + "-" + trailId;
    }

    /**
     * @param serverList Server list from database.
     * @param imageUrl   Image url which needs to be loaded by server url.
     * @param width      Requested image width.
     * @param height     Requested image height.
     * @return The built string from server url and image url. Server url is the best possible url, based on requested dimensions.
     */
    @Nullable
    private String getSizedUrl(List<IPosterServerInfoField> serverList, String imageUrl, int width, int height) {
        int widthAbs = 0;
        int heightAbs = 0;

        IPosterServerInfoField possibleItem = null;

        for (IPosterServerInfoField serverField : serverList) {
            if (width > height) {
                if (Math.abs(serverField.getWidth() - width) < widthAbs) {
                    widthAbs = (int) Math.abs(serverField.getWidth() - width);
                    possibleItem = serverField;
                }
            } else {
                if (Math.abs(serverField.getHeight() - height) < heightAbs) {
                    heightAbs = (int) Math.abs(serverField.getHeight() - height);
                    possibleItem = serverField;
                }
            }
        }
        if (possibleItem == null) {
            return null;
        }

        return possibleItem.getUri() + SLASH_SEPARATOR + imageUrl;
    }

    /**
     * @param serverList Server list from database.
     * @param imageUrl   Image url which needs to be loaded by server url.
     * @param width      Requested image width.
     * @param height     Requested image height.
     * @return The built string from server url and image url or null if image can not be resized.
     */
    @Nullable
    private String getResizableUrl(List<IPosterServerInfoField> serverList, String imageUrl, int width, int height) {
        for (IPosterServerInfoField serverField : serverList) {
            if (serverField.isResizable()) {
                StringBuilder sb = new StringBuilder(serverField.getUri());
                sb.append(SLASH_SEPARATOR);
                sb.append(IMAGE_WIDTH_PREFIX);
                sb.append(SEPARATOR);
                sb.append(width);
                sb.append(IMAGE_TYPE_PART_SEPARATOR);
                sb.append(IMAGE_HEIGHT_PREFIX);
                sb.append(SEPARATOR);
                sb.append(height);
                sb.append(SLASH_SEPARATOR);
                sb.append(imageUrl);

                return sb.toString();
            }
        }
        return null;
    }
}