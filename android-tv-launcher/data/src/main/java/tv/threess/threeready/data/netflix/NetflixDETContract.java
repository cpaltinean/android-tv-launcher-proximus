package tv.threess.threeready.data.netflix;

import android.content.ContentResolver;
import android.net.Uri;

import tv.threess.threeready.api.BuildConfig;
import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.netflix.model.NetflixDETCategory;

/**
 * Contract class to access data in netflix database.
 *
 * @author David
 * @since 25.02.2022
 */
public class NetflixDETContract {
    public static final String AUTHORITY = BuildConfig.NETFLIX_PROVIDER;

    static final Uri BASE_URI = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT).authority(AUTHORITY).build();

    public static Uri buildForCategory(String category) {
        return Uri.withAppendedPath(Netflix.CONTENT_URI_WITH_CATEGORY, category);
    }

    public static Uri buildForCategory(NetflixDETCategory category) {
        if (category == null) {
            return Netflix.CONTENT_URI_WITH_CATEGORY;
        }
        return buildForCategory(category.name());
    }

    public interface Netflix {
        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Netflix.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(Netflix.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(NetflixDETContract.BASE_URI, PATH);

        /**
         * Content uri to read data from the table by category.
         */
        Uri CONTENT_URI_WITH_CATEGORY = Uri.withAppendedPath(CONTENT_URI, "category");

        //
        // Columns
        //
        /**
         * The response what we receive after using a GraphQl query.
         * <P>Type: String</P>
         */
        String CATEGORY = "category";

        /**
         * The used UI language.
         * If the UI language is changed we have to refresh the Netflix stripe.
         * <P>Type: String</P>
         */
        String UI_LANGUAGE = "ui_language";

        /**
         * The response what we receive after using a GraphQl query.
         * <P>Type: String</P>
         */
        String RESPONSE_DATA = "response_data";

        /**
         * The minimum time what we should wait before handling a refresh.
         * In millis.
         * <P>Type: String</P>
         */
        String MIN_REFRESH_AT = "min_refresh_at";

        /**
         * The time stamp when the downloaded data is expired.
         * In millis
         * <P>Type: String</P>
         */
        String EXPIRE_AT = "expire_at";

        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;
    }
}
