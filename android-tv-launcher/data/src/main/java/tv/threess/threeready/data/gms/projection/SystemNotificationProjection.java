package tv.threess.threeready.data.gms.projection;

import android.database.Cursor;

import java.util.List;

import tv.threess.threeready.data.generic.IProjection;
import tv.threess.threeready.data.gms.GmsContract;
import tv.threess.threeready.api.notification.model.NotificationItem;
import tv.threess.threeready.data.utils.SqlUtils;

/**
 * Projection to get system notification.
 *
 * @author Eugen Guzyk
 * @since 2018.08.24
 */
public enum SystemNotificationProjection implements IProjection {

    SBN_KEY(GmsContract.SystemNotification.COLUMN_SBN_KEY),
    PACKAGE_NAME(GmsContract.SystemNotification.COLUMN_PACKAGE_NAME),
    NOTIF_TITLE(GmsContract.SystemNotification.COLUMN_NOTIF_TITLE),
    NOTIF_TEXT(GmsContract.SystemNotification.COLUMN_NOTIF_TEXT),
    AUTODISMISS(GmsContract.SystemNotification.COLUMN_AUTODISMISS),
    DISMISSIBLE(GmsContract.SystemNotification.COLUMN_DISMISSIBLE),
    ONGOING(GmsContract.SystemNotification.COLUMN_ONGOING),
    SMALL_ICON(GmsContract.SystemNotification.COLUMN_SMALL_ICON),
    IMPORTANCE(GmsContract.SystemNotification.COLUMN_IMPORTANCE),
    PROGRESS(GmsContract.SystemNotification.COLUMN_PROGRESS),
    PROGRESS_MAX(GmsContract.SystemNotification.COLUMN_PROGRESS_MAX),
    FLAGS(GmsContract.SystemNotification.COLUMN_FLAGS),
    HAS_CONTENT_INTENT(GmsContract.SystemNotification.COLUMN_HAS_CONTENT_INTENT),
    BIG_PICTURE(GmsContract.SystemNotification.COLUMN_BIG_PICTURE),
    CONTENT_BUTTON_LABEL(GmsContract.SystemNotification.COLUMN_CONTENT_BUTTON_LABEL),
    DISMISS_BUTTON_LABEL(GmsContract.SystemNotification.COLUMN_DISMISS_BUTTON_LABEL),
    TAG(GmsContract.SystemNotification.COLUMN_TAG);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    SystemNotificationProjection(String column) {
        this.mColumn = column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    public static List<NotificationItem> readAll(Cursor cursor) {
        return SqlUtils.readAll(cursor, SystemNotificationProjection::readRow);
    }

    public static NotificationItem readRow(Cursor cursor) {
        return new NotificationItem.Builder()
                .setSbnKey(cursor.getString(SBN_KEY.ordinal()))
                .setPackageName(cursor.getString(PACKAGE_NAME.ordinal()))
                .setNotifTitle(cursor.getString(NOTIF_TITLE.ordinal()))
                .setNotifText(cursor.getString(NOTIF_TEXT.ordinal()))
                .setAutoDismiss(cursor.getInt(AUTODISMISS.ordinal()))
                .setDismissible(cursor.getInt(DISMISSIBLE.ordinal()))
                .setOngoing(cursor.getInt(ONGOING.ordinal()))
                .setSmallIcon(cursor.getBlob(SMALL_ICON.ordinal()))
                .setImportance(cursor.getInt(IMPORTANCE.ordinal()))
                .setProgress(cursor.getInt(PROGRESS.ordinal()))
                .setProgressMax(cursor.getInt(PROGRESS_MAX.ordinal()))
                .setFlags(cursor.getInt(FLAGS.ordinal()))
                .setHasContentIntent(cursor.getInt(HAS_CONTENT_INTENT.ordinal()))
                .setBigPicture(cursor.getBlob(BIG_PICTURE.ordinal()))
                .setContentButtonLabel(cursor.getString(CONTENT_BUTTON_LABEL.ordinal()))
                .setDismissButtonLabel(cursor.getString(DISMISS_BUTTON_LABEL.ordinal()))
                .setTag(cursor.getString(TAG.ordinal()))
                .build();
    }
}
