package tv.threess.threeready.data.tv.observable;

import android.content.Context;

import java.util.List;
import java.util.Map;

import io.reactivex.ObservableEmitter;
import tv.threess.threeready.api.tv.TvChannelCacheType;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.api.tv.model.TvChannel;


/**
 * Returns both favorite and not favorite channels but in two separate lists.
 * Favorites are order by favorite position and not favorites by channel position.
 * In this observable we are register the observable itself for the favourites database table changes
 * as well.
 * <p>
 * Created by Bartalus Csaba - Zsolt since 18/10/2021.
 */
public class EpgGridChannelsObservable extends TvEPGChannelsObservable {

    public EpgGridChannelsObservable(Context context) {
        super(context);
    }

    @Override
    public void subscribe(ObservableEmitter<Map<TvChannelCacheType, List<TvChannel>>> emitter) throws Exception {
        super.subscribe(emitter);

        registerObserver(TvContract.Favorites.CONTENT_URI);
    }
}
