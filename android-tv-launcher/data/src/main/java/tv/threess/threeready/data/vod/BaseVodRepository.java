package tv.threess.threeready.data.vod;

import android.app.Application;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.model.CacheSettings;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.data.utils.PeriodicCacheUpdater;

/**
 * Base implementation of the Proximus specific VOD methods.
 *
 * @author Barabas Attila
 * @since 2018.04.30
 */
public abstract class BaseVodRepository {
    protected Application mApp;

    public BaseVodRepository(Application app) {
        mApp = app;
    }

    protected static final PeriodicCacheUpdater rentalUpdater = new PeriodicCacheUpdater(Settings.lastRentedVodSyncTime,
            Components.get(CacheSettings.class)::getVodPurchaseUpdatePeriod);

    protected static final PeriodicCacheUpdater categoryUpdater = new PeriodicCacheUpdater(Settings.lastCategoryContentSyncTime,
            Components.get(CacheSettings.class)::getVodCacheTime) {
        @Override
        public boolean isCacheValid() {
            return Components.get(VodServiceRepository.class)
                    .isDeviceProfileValid() && super.isCacheValid();
        }
    };
}
