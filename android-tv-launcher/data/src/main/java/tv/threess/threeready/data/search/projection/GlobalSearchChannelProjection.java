/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.search.projection;

import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.media.tv.TvContract;

import java.util.Arrays;
import java.util.List;

import tv.threess.threeready.data.generic.IProjection;
import tv.threess.threeready.data.search.GlobalSearchContract;
import tv.threess.threeready.api.search.model.GlobalSearchType;
import tv.threess.threeready.api.tv.model.TvChannel;

/**
 * Projection to display channels in global search.
 *
 * @author Barabas Attila
 * @since 2017.07.19
 */
public enum GlobalSearchChannelProjection implements IProjection {
    ID(GlobalSearchContract.Channel.ID),
    TITLE(GlobalSearchContract.Channel.TITLE),
    IS_LIVE(GlobalSearchContract.Channel.IS_LIVE),
    CARD_IMAGE(GlobalSearchContract.Channel.CARD_IMAGE),
    INTENT_ACTION(GlobalSearchContract.Channel.INTENT_ACTION),
    INTENT_DATA(GlobalSearchContract.Channel.INTENT_DATA),
    INTENT_DATA_ID(GlobalSearchContract.Channel.INTENT_DATA_ID),
    CONTENT_TYPE(GlobalSearchContract.Channel.CONTENT_TYPE);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    GlobalSearchChannelProjection(String column) {
        mColumn = column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    public static Cursor buildCursor(TvChannel... resultArray){
        return buildCursor(Arrays.asList(resultArray));
    }

    public static Cursor buildCursor(List<TvChannel> resultList) {
        MatrixCursor cursor = new MatrixCursor(PROJECTION);

        for (TvChannel result : resultList) {
            Object[] row = new Object[PROJECTION.length];
            row[ID.ordinal()] = result.getId();
            row[TITLE.ordinal()] = result.getName();
            row[IS_LIVE.ordinal()] = true;
            row[INTENT_ACTION.ordinal()] = Intent.ACTION_VIEW;
            row[INTENT_DATA.ordinal()] = TvContract.buildChannelUri(0); // Dummy TIF channel id.
            row[INTENT_DATA_ID.ordinal()] = result.getId()+ "/-1/" + GlobalSearchType.Channel.name();
            row[CARD_IMAGE.ordinal()] = GlobalSearchContract.Channel.CONTENT_URI + "/" + result.getId();
            row[CONTENT_TYPE.ordinal()] = TvContract.Channels.CONTENT_ITEM_TYPE;
            cursor.addRow(row);
        }

        return cursor;
    }
}
