/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.pvr.observable;

import android.content.Context;
import android.net.Uri;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.data.home.observable.BaseCategoryContentObservable;
import tv.threess.threeready.data.pvr.PvrCache;
import tv.threess.threeready.data.pvr.PvrContract;

/**
 * Rx observable class to return content from the recorded category based on the module config.
 * Automatically reloads and emits the new data when the recording changes.
 *
 * @author Barabas Attila
 * @since 2018.11.12
 */
public class ScheduledCategoryContentObservable extends BaseCategoryContentObservable<IBaseContentItem> {
    private final PvrCache mPvrCache = Components.get(PvrCache.class);

    public ScheduledCategoryContentObservable(Context context,
                                              ModuleConfig moduleConfig, int start, int count) {
        super(context, moduleConfig, start, count);
    }

    @Override
    public void subscribe(ObservableEmitter<ModuleData<IBaseContentItem>> emitter) throws Exception {
        super.subscribe(emitter);
        registerObserver(PvrContract.Recording.CONTENT_URI);
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<ModuleData<IBaseContentItem>> emitter) {
        loadItemList(emitter);
    }

    @Override
    protected void loadItemList(ObservableEmitter<ModuleData<IBaseContentItem>> emitter) {
        mCategoryData = new ModuleData<>(mModuleConfig, mPvrCache.getScheduledData(mStart, mCount));
        onItemListLoaded(emitter);
    }
}
