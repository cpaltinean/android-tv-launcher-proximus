package tv.threess.threeready.data.home.model.module;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.data.account.adapter.ModuleDataSourceFilterJsonAdapter3Ready;
import tv.threess.threeready.data.home.model.generic.ModuleFilter3Ready;

/**
 * Menu selector options for different collection pages, like LiveTv page.
 *
 * @author David Bondor
 * @since 18.05.2022
 */
public class ModuleDataSourceSelector3Ready implements SelectorOption {

    @SerializedName("title")
    private String mTitle;

    @SerializedName("filter")
    @JsonAdapter(ModuleDataSourceFilterJsonAdapter3Ready.class)
    private ModuleFilter3Ready mFilter;

    @SerializedName("isDefault")
    private boolean mIsDefault;

    @SerializedName("empty_page_message")
    private String mEmptyPageMessage;

    private int mItemCount;

    @Override
    public String getName() {
        return mTitle;
    }

    @Override
    public boolean isDefault() {
        return mIsDefault;
    }

    @Override
    public String getReportName() {
        return mTitle;
    }

    @Override
    public String getReportReference() {
        return getReportName();
    }

    @Override
    public int getItemCount() {
        return mItemCount;
    }

    @Override
    public String getEmptyPageMessage() {
        return mEmptyPageMessage;
    }

    public String getFilter(String key) {
        return mFilter == null ? null : mFilter.getFilter(key, String.class);
    }

    public List<String> getFilterList(String key) {
        return mFilter == null ? null: mFilter.getFilterListAsString(key, String.class);
    }

    public void setItemCount(int itemCount) {
        this.mItemCount = itemCount;
    }
}