/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.tv.projections;

import static tv.threess.threeready.data.tv.TvContract.Advertisement;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.data.tv.model.TvAdvertisement;
import tv.threess.threeready.data.generic.IProjection;
import tv.threess.threeready.data.tv.model.TvAdvertisementDBModel;
import tv.threess.threeready.data.utils.SqlUtils;

/**
 * Projection used to query data from database
 */
public enum AdvertisementProjection implements IProjection {
    CREATIVE_ID(Advertisement.CREATIVE_ID),
    NAME(Advertisement.NAME),
    BROADCASTER(Advertisement.BROADCASTER),
    FILENAME(Advertisement.FILENAME),
    SIZE(Advertisement.SIZE),
    DURATION(Advertisement.DURATION),
    EXPIRES(Advertisement.EXPIRES),
    ON_MARKER(Advertisement.ON_MARKER),
    ATTEMPT_COUNT(Advertisement.ATTEMPT_COUNT),
    LAST_UPDATED(Advertisement.LAST_UPDATED);

    public static final String TAG = Log.tag(AdvertisementProjection.class);

    public static final String[] PROJECTION = IProjection.build(values());
    public static final String[] PROJECTION_ATTEMPTS = IProjection.build(CREATIVE_ID, ATTEMPT_COUNT);

    private final String mColumn;

    AdvertisementProjection(String column) {
        this(Advertisement.TABLE_NAME, column);
    }

    AdvertisementProjection(String tableName, String column) {
        mColumn = tableName + "." + column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    public static TvAdvertisementDBModel readRow(Cursor cursor) {
        return new TvAdvertisementDBModel.Builder()
                .setCreativeId(cursor.getString(CREATIVE_ID.ordinal()))
                .setName(cursor.getString(NAME.ordinal()))
                .setBroadcaster(cursor.getString(BROADCASTER.ordinal()))
                .setFilename(cursor.getString(FILENAME.ordinal()))
                .setSize(cursor.getLong(SIZE.ordinal()))
                .setDuration(cursor.getLong(DURATION.ordinal()))
                .setExpires(cursor.getLong(EXPIRES.ordinal()))
                .setOnMarker(cursor.getInt(ON_MARKER.ordinal()) != 0)
                .setRetryCount(cursor.getInt(ATTEMPT_COUNT.ordinal()))
                .build();
    }

    public static ContentValues toContentValue(TvAdvertisement ad, long now, boolean onMarker) {
        return toContentValue(ad, now, onMarker, 0);
    }

    public static ContentValues toContentValue(TvAdvertisement ad, long now, boolean onMarker, int attemptCount) {
        ContentValues value = new ContentValues(10);
        value.put(TvContract.Advertisement.CREATIVE_ID, ad.getCreativeId());
        value.put(TvContract.Advertisement.NAME, ad.getName());
        value.put(TvContract.Advertisement.BROADCASTER, ad.getBroadcaster());
        value.put(TvContract.Advertisement.FILENAME, ad.getFilename());
        value.put(TvContract.Advertisement.SIZE, ad.getSize());
        value.put(TvContract.Advertisement.DURATION, ad.getDuration(TimeUnit.MILLISECONDS));
        value.put(TvContract.Advertisement.EXPIRES, ad.getExpires());
        value.put(TvContract.Advertisement.ON_MARKER, onMarker ? SqlUtils.TRUE : SqlUtils.FALSE);
        value.put(TvContract.Advertisement.ATTEMPT_COUNT, attemptCount);
        value.put(TvContract.Advertisement.LAST_UPDATED, now);
        return value;
    }
}
