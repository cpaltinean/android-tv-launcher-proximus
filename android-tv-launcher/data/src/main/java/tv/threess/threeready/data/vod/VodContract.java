/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.vod;

import android.content.ContentResolver;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.BuildConfig;
import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.vod.model.IBaseVodItem;

/**
 * Contract class to access data in vod database.
 *
 * @author Barabas Attila
 * @since 2017.09.04
 */
public class VodContract {

    public static final String AUTHORITY = BuildConfig.VOD_PROVIDER;

    static final Uri BASE_URI = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT).authority(AUTHORITY).build();

    /**
     * Build purchase uri for a give variant id.
     */
    public static Uri buildPurchaseUriForVariant(String variantId) {
        return RentalExpiration.CONTENT_URI.buildUpon()
                .appendEncodedPath(RentalExpiration.VARIANT_ID)
                .appendEncodedPath(variantId)
                .build();
    }

    /**
     * Build purchase uri for each variant in the vod.
     */
    public static List<Uri> buildPurchaseUrisForVod(IBaseVodItem vod) {
        List<Uri> uris = new ArrayList<>();
        for (String variantId : vod.getVariantIds()) {
            uris.add(RentalExpiration.CONTENT_URI.buildUpon()
                    .appendEncodedPath(RentalExpiration.VARIANT_ID)
                    .appendEncodedPath(variantId)
                    .build());
        }
        return uris;
    }

    /**
     * Get the id of the variant for a purchase URI.
     * @param uri The content URI of the purchase.
     * @return The id of the purchased variant or null if it's not a purchase URI.
     */
    public static String getVariantIdFromPurchaseUri(Uri uri) {
        if (uri.getEncodedPath() != null && VodContract.RentalExpiration.CONTENT_URI.getEncodedPath() != null
                && uri.getEncodedPath().startsWith(VodContract.RentalExpiration.CONTENT_URI.getEncodedPath())) {
            return uri.getLastPathSegment();
        }
        return null;
    }


    public interface Vod {

        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(VodContract.Vod.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(VodContract.Vod.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(VodContract.BASE_URI, PATH);

        /**
         * Unique identifier of the vod.
         */
        String VOD_ID = "vod_id";

        /**
         * Unique identifier of the vod.
         */
        String SERIES_ID = "vod_series_id";

        /**
         * Unique identifier of the default vod variant.
         * <P>Type: String</P>
         */
        String MAIN_VARIANT_ID = "main_variant_id";

        /**
         * Id of the variants belonging to the VOD, separated with ','.
         * <P>Type: String</P>
         */
        String VARIANT_IDS = "variant_ids";

        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;
    }

    public interface VodVariant {

        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(VodContract.VodVariant.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(VodContract.VodVariant.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(VodContract.BASE_URI, PATH);

        /**
         * Unique identifier of the vod variant.
         * <P>Type: String</P>
         */
        String VARIANT_ID = "vod_id";

        /**
         * Unique identifier of the series.
         * Only available for episodes.
         */
        String SERIES_ID = "series_id";

        /**
         * Program group title.
         * <P>Type: String</P>
         */
        String GROUP_TITLE = "group_title";


        /**
         * Vod group id.
         * <P>Type: String</P>
         */
        String GROUP_ID = "group_id";

        /**
         * Program item title.
         * <P>Type: String</P>
         */
        String ITEM_TITLE = "title";

        /**
         * Program description.
         * <P>Type: String</P>
         */
        String DESCRIPTION = "description";

        /**
         * Genres, separated with ','.
         * <P>Type: String </>
         */
        String GENRES = "genres";

        /**
         * Release year of the vod.
         * <P>Type: String</>
         */
        String RELEASE_YEAR = "release_year";

        /**
         * The duration of the vod variant in millis.
         * <P>Type: Numeric</>
         */
        String DURATION = "duration";

        /**
         * Season.
         * <P>Type: String</P>
         */
        String SEASON = "season";

        /**
         * Episode.
         * <P>Type: String</P>
         */
        String EPISODE = "episode";

        /**
         * A specific title for this program's episode.
         * <P>Type: String</P>
         */
        String EPISODE_TITLE = "episode_title";

        /**
         * Actor list joined by ','.
         * <P>Type: String</P>
         */
        String ACTORS = "actors";

        /**
         * Director list joined by ','.
         * <P>Type: String</P>
         */
        String DIRECTORS = "director";

        /**
         * Parental rating value of the broadcast.
         * <P>Type: String</></>
         */
        String PARENTAL_RATING = "parental_rating";

        /**
         * The image urls of the broadcast joined by ','.
         * <P>Type: String</P>
         */
        String IMAGE_URL = "image_url";

        /**
         * The type of each image url joined by ','.
         * E.g TCover_W320_H240,E.g TBackground_W120_H240
         *
         * The type consist of 3 part :
         * T - The name of the image source type. {@link IImageSource.Type}
         * W - The width of the image in pixel when defined.
         * H - The height of the image in pixel when defined.
         *
         *  <P>Type: String</P>
         */
        String IMAGE_TYPE = "image_type";

        /**
         * Marks if the variant has trailer.
         * <P>Type: Number</P>
         */
        String HAS_TRAILER = "has_trailer";

        /**
         * Audio language list joined by ','.
         * <P>Type: String</P>
         */
        String AUDIO_LANGUAGES = "audio_languages";

        /**
         * Subtitle language list joined by ','
         * <P>Type: String</>
         */
        String SUBTITLE_LANGUAGES = "subtitle_languages";

        /**
         * Mark if it's a transactional VOD variant.
         * <P>Type: Numeric</P>
         */
        String IS_TVOD = "is_tvod";

        /**
         * Mark if the VOD is part of a subscription.
         * <P>Type: Numeric</P>
         */
        String IS_SVOD = "is_svod";

        /**
         * Mark if the VOD is new in store.
         * <P>Type: Numeric</P>
         */
        String IS_NEW = "is_new";

        /**
         * Mark if the user is subscribe on the VOD variant.
         * <P>Type: Numeric</P>
         */
        String IS_SUBSCRIBED = "is_subscribed";

        /**
         * Mark if the variant currently available for rent.
         * <P>Type: Numeric</P>
         */
        String IS_RENTABLE = "is_rentable";

        /**
         * Mark if the highest available video quality of the VOD variant.
         * <P>Type: String</P>
         */
        String VIDEO_QUALITY = "video_quality";

        /**
         * The time when the VOD variant availability expires.
         * <P>Type: Numeric</>
         */
        String LICENCE_PERIOD_END = "licence_period_end";

        /**
         * True if the variant is free, false otherwise.
         * <P>Type: Numeric</P>
         */
        String IS_FREE = "is_free";

        /**
         * The parent genre of the VOD variant. (e.g. Movies, Series)
         * <P>Type: Numeric</P>
         */
        String SUPER_GENRE = "super_genre";

        /**
         * Identifier of the VOD variant used for internal processes.
         * <P>Type: Numeric</P>
         */
        String INTERNAL_ID = "internal_id";

        /**
         * Marks if the variant has HD playback.
         * <P>Type: Number</P>
         */
        String IS_HD = "is_hd";

        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;

        /**
         * True if the variant has descriptive audio, otherwise false.
         * <P>Type: Numeric</P>
         */
        String HAS_DESCRIPTIVE_AUDIO = "has_descriptive_audio";

        /**
         * True if the variant has descriptive subtitles, otherwise false.
         * <P>Type: Numeric</P>
         */
        String HAS_DESCRIPTIVE_SUBTITLE = "has_descriptive_subtitle";
    }

    public interface ContinueWatchingVod extends Vod {
        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(VodContract.ContinueWatchingVod.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(VodContract.ContinueWatchingVod.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(VodContract.BASE_URI, PATH);
    }

    public interface ContinueWatchingVodVariant extends VodVariant {
        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(VodContract.ContinueWatchingVodVariant.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(VodContract.ContinueWatchingVodVariant.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(VodContract.BASE_URI, PATH);
    }

    public interface RentalExpiration {
        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(RentalExpiration.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(VodContract.BASE_URI, PATH);

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(RentalExpiration.class);

        /**
         * Rented vod expiration time.
         */
        String EXPIRATION = "expiration";

        /**
         * The rental time of the rented vod.
         */
        String RENTAL_TIME = "rental_time";

        /**
         * Movie reference for rented vod.
         */
        String VARIANT_ID = "variant_id";

        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;
    }
}
