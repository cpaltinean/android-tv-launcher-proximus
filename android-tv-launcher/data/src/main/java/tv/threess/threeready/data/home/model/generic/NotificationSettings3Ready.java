package tv.threess.threeready.data.home.model.generic;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import tv.threess.threeready.api.home.model.generic.NotificationSettings;

/**
 * Model for parsing the notification settings from config.
 *
 * @author Lukacs Andor
 * @since 2017.07.26
 */
public class NotificationSettings3Ready implements Serializable, NotificationSettings {
    @SerializedName("low_priority_duration")
    private int mLowDuration;

    @SerializedName("normal_priority_duration")
    private int mNormalDuration;

    @SerializedName("high_priority_duration")
    private int mHighDuration;

    @SerializedName("urgent_priority_duration")
    private int mUrgentDuration;

    @SerializedName("minimum_display_duration")
    private int mMinimumDisplayDuration;

    @SerializedName("display_icon_when_empty")
    private boolean mDisplayIconWhenEmpty;

    @Override
    public int getLowDuration() {
        return mLowDuration;
    }

    @Override
    public int getNormalDuration() {
        return mNormalDuration;
    }

    @Override
    public int getHighDuration() {
        return mHighDuration;
    }

    @Override
    public int getUrgentDuration() {
        return mUrgentDuration;
    }

    @Override
    public int getMinimumDisplayDuration() {
        return mMinimumDisplayDuration;
    }

}
