package tv.threess.threeready.data.account.adapter;


import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import tv.threess.threeready.api.generic.helper.ColorUtils;

/**
 * Adapter to transform a String from config Json to Integer
 *
 * @author Solyom Zsolt
 * @since 2020.02.19
 */

public class ColorTypeAdapter3Ready extends TypeAdapter<Integer> {

    @Override
    public void write(JsonWriter out, Integer value) throws IOException {
        out.value(String.format("#%08X", value));
    }

    @Override
    public Integer read(JsonReader reader) throws IOException {
        return ColorUtils.parseColor(reader.nextString());
    }
}
