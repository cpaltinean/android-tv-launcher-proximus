/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.home.observable;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountCache;
import tv.threess.threeready.api.generic.model.ContinueWatchingConfig;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IRentalExpiration;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.pvr.PvrCache;
import tv.threess.threeready.data.pvr.PvrContract;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.vod.VodCache;
import tv.threess.threeready.data.vod.model.BaseVodItemDbModel;

/**
 * Rx observable class to return content from a category based on the module config.
 * Automatically reloads and emits the new data when the recording change or bookmark changes.
 *
 * @author Barabas Attila
 * @since 2018.11.15
 */
public class ContinueWatchingCategoryContentObservable extends BaseCategoryContentObservable<IBaseContentItem> {

    private final PvrCache mPvrCache = Components.get(PvrCache.class);
    private final TvCache mTvCache = Components.get(TvCache.class);
    private final VodCache mVodCache = Components.get(VodCache.class);
    private final AccountCache.Bookmarks mAccountCache = Components.get(AccountCache.Bookmarks.class);

    public ContinueWatchingCategoryContentObservable(Context context,
                                                     ModuleConfig moduleConfig, int start, int count) {
        super(context, moduleConfig, start, count);
    }


    @Override
    public void subscribe(ObservableEmitter<ModuleData<IBaseContentItem>> emitter) throws Exception {
        super.subscribe(emitter);
        registerObserver(ConfigContract.Bookmarks.CONTENT_URI);
        registerObserver(PvrContract.Recording.CONTENT_URI);
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<ModuleData<IBaseContentItem>> emitter) {
        loadItemList(emitter);
    }

    @Override
    protected void loadItemList(ObservableEmitter<ModuleData<IBaseContentItem>> emitter) {
        mCategoryData = getContinueWatchingDataSource();
        onItemListLoaded(emitter);
    }

    private ModuleData<IBaseContentItem> getContinueWatchingDataSource() {
        Set<String> seriesIdSet = new HashSet<>();
        Set<String> uniqueId = new HashSet<>();
        List<IBaseContentItem> contentItems = new ArrayList<>();
        Map<String, IRentalExpiration> rentalExpirationMap = null;

        // Get the latest bookmarks for continue watching.
        List<IBookmark> bookmarks = mAccountCache.getContinueWatchingBookmarks();

        ContinueWatchingConfig cwConfig = Components.get(ContinueWatchingConfig.class);
        // Create content item list based on bookmarks.
        for (IBookmark bookmark : bookmarks) {
            IContentItem contentItem = null;

            switch (bookmark.getType()) {
                case Replay:
                    if (bookmark.isNearEnd(cwConfig.getReplayWatchedPercent())) {
                        continue;
                    }

                    TvChannel channel = null;
                    IBroadcast broadcast = mTvCache.getContinueWatchingReplay(bookmark.getContentId());
                    if (broadcast != null) {
                        channel = mTvCache.getTvChannelById(broadcast.getChannelId());
                    }

                    // Check if can still replay.
                    if (channel != null && broadcast.canReplay(channel)) {
                        contentItem = broadcast;
                    }
                    break;

                case Recording:
                    if (bookmark.isNearEnd(cwConfig.getRecordingWatchedPercent())) {
                        continue;
                    }

                    IRecording recording = mPvrCache.getContinueWatchingRecording(bookmark.getContentId());

                    // Check if the recording is still playable.
                    if (recording != null && recording.canWatchRecording()) {
                        contentItem = recording;
                    }
                    break;

                case VodVariant:
                    if (bookmark.isNearEnd(cwConfig.getVodWatchedMaxPercent())) {
                        continue;
                    }

                    IBaseVodItem vod = mVodCache.getContinueWatchingVod(bookmark.getContentId());
                    if (vod != null) {
                        // Get rentals from cache.
                        if (rentalExpirationMap == null) {
                            rentalExpirationMap = mVodCache.getRentalExpiration();
                        }

                        // Update vod with rental information.
                        if (vod instanceof BaseVodItemDbModel) {
                            ((BaseVodItemDbModel) vod).setRentalExpirationInfo(rentalExpirationMap);
                        }

                        // Check if still playable.
                        if (vod.isSubscribed() || vod.isRented()) {
                            contentItem = vod;
                        }
                    }
                    break;
            }

            if (contentItem != null) {
                // Filter adult content
                if (contentItem.getParentalRating() == ParentalRating.Rated18) {
                    continue;
                }

                // Show only one episode per series.
                if (!TextUtils.isEmpty(contentItem.getSeriesId())) {
                    if (seriesIdSet.contains(contentItem.getSeriesId())) {
                        // Skip already added one episode for series
                        continue;
                    }
                    // Add a mark
                    seriesIdSet.add(contentItem.getSeriesId());
                }

                // Filter item with the same Id
                if (uniqueId.contains(contentItem.getId())) {
                    continue;
                }
                uniqueId.add(contentItem.getId());


                contentItems.add(contentItem);
            }
        }

        return new ModuleData<>(mModuleConfig, contentItems);
    }
}
