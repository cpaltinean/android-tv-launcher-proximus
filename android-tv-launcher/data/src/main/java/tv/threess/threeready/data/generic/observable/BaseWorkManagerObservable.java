package tv.threess.threeready.data.generic.observable;

import android.content.Context;

import android.content.Intent;
import android.content.IntentFilter;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.disposables.Disposable;
import tv.threess.threeready.api.generic.receivers.BaseLocalBroadcastReceiver;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.generic.JobIntent;
import tv.threess.threeready.ui.generic.utils.RxUtils;

import java.util.UUID;

/**
 * RX observable to start and wait for a WorkManager job to finish.
 *
 * @author Barabas Attila
 * @since 2018.02.14
 */
public abstract class BaseWorkManagerObservable<T> implements ObservableOnSubscribe<T> {

    protected final Context mContext;

    protected final JobIntent mJob;

    protected volatile Receiver mReceiver;

    protected BaseWorkManagerObservable(Context context, JobIntent intent) {
        mContext = context;
        mJob = intent;
    }

    @Override
    public void subscribe(ObservableEmitter<T> emitter) throws Exception {
        RxUtils.disposeSilently(mReceiver);
        mReceiver = new Receiver(emitter);
        emitter.setDisposable(mReceiver);
        mReceiver.registerReceiver(mContext);
        BaseWorkManager.start(mContext, mJob.withReceiver(mReceiver.mIntentAction));
    }

    /**
     * Called when the service finished the give action.
     */
    protected void onJobFinished(ObservableEmitter<T> emitter, boolean success) {
        emitter.onComplete();
    }

    private class Receiver extends BaseLocalBroadcastReceiver implements Disposable {

        private final String mIntentAction = "intent.action.BaseLocalBroadcastReceiver." + UUID.randomUUID();

        private volatile ObservableEmitter<T> mEmitter;

        private Receiver(ObservableEmitter<T> emitter) {
            mEmitter = emitter;
        }

        @Override
        public void dispose() {
            unregisterReceiver(mContext);
            mEmitter = null;
        }

        @Override
        public boolean isDisposed() {
            return mEmitter == null;
        }

        @Override
        public IntentFilter getIntentFilter() {
            return new IntentFilter(mIntentAction);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            onJobFinished(mEmitter, !intent.hasExtra(BaseWorkManager.KEY_INTENT_EXTRA_ERROR));
        }
    }
}
