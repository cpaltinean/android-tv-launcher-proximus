package tv.threess.threeready.data.generic;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.receivers.ScanReceiver;

public class MwScanReceiver extends ScanReceiver implements Component {

    private volatile boolean mInProgress = false;

    @Override
    public boolean isInProgress() {
        return mInProgress;
    }

    public void setInProgress(boolean inProgress) {
        this.mInProgress = inProgress;
        notifyListeners(inProgress);
    }
}
