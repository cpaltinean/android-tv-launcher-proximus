/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.home.model.module;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;
import tv.threess.threeready.data.account.adapter.ModuleDataSourceMethodTypeAdapter;

/**
 * Define the end point and the method for a data source request
 *
 * Created by Barabas Attila on 4/14/2017.
 */
public class ModuleDataSourceRequest3Ready implements Serializable {

    @JsonAdapter(ModuleDataSourceMethodTypeAdapter.class)
    @SerializedName("method")
    private final ModuleDataSourceMethod mMethod;

    public ModuleDataSourceRequest3Ready(ModuleDataSourceMethod method) {
        mMethod = method;
    }

    public ModuleDataSourceMethod getMethod() {
        return mMethod;
    }

    @Override
    public String toString() {
        return "ModuleDataSourceRequest{" +
                "mMethod='" + mMethod + '\'' +
                '}';
    }
}
