package tv.threess.threeready.data.account.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import tv.threess.threeready.api.home.model.module.ModuleType;

/**
 * Json type adapter to read the module type as enum.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public class ModuleTypeAdapter3Ready extends TypeAdapter<ModuleType> {

    @Override
    public void write(JsonWriter out, ModuleType value) throws IOException {
        out.value(value.name().toLowerCase());
    }

    @Override
    public ModuleType read(JsonReader in) throws IOException {
        String value = in.nextString();
        for (ModuleType type : ModuleType.values()) {
            if (type.name().equalsIgnoreCase(value)) {
                return type;
            }
        }

        return ModuleType.UNDEFINED;
    }
}
