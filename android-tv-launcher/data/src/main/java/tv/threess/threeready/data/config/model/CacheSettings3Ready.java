package tv.threess.threeready.data.config.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.account.model.CacheSettings;

/**
 * Class representing the cache settings from the local configuration json.
 *
 * @author Barabas Attila
 * @since 2018.02.08
 */
public class CacheSettings3Ready implements Serializable, CacheSettings {

    @SerializedName("epg_cache_windows_length_min")
    private int mEpgCacheWindowLengthMin;

    @SerializedName("channels_update_min")
    private int mChannelsUpdateMin;

    @SerializedName("epg_update_min")
    private int mEpgUpdateMin;

    @SerializedName("recording_update_min")
    private int mRecordingUpdateMin;

    @SerializedName("vod_purchase_update_min")
    private int mVodPurchaseUpdateMin;

    @SerializedName("vod_update_min")
    private int mVodUpdateMin;

    @SerializedName("global_install_update_min")
    private int mGlobalInstallUpdateMin;

    @SerializedName("boot_config_update_min")
    private int mBootConfigUpdateMin;

    @SerializedName("translations_update_min")
    private int mTranslationUpdateMin;

    @SerializedName("config_update_min")
    private int mConfigUpdateMin;

    @SerializedName("account_info_update_min")
    private int mAccountInfoUpdateMin;

    @SerializedName("watchlist_update_min")
    private int mWatchlistUpdateMin;

    @SerializedName("consent_update_limit_days")
    private int mConsentUpdateLimitDays;

    @Override
    public long getEpgCacheWindowLength(TimeUnit unit) {
        return unit.convert(mEpgCacheWindowLengthMin, TimeUnit.MINUTES);
    }

    @Override
    public long getChannelsUpdatePeriod(TimeUnit unit) {
        return unit.convert(mChannelsUpdateMin, TimeUnit.MINUTES);
    }

    @Override
    public long getEpgUpdatePeriod(TimeUnit unit) {
        return unit.convert(mEpgUpdateMin, TimeUnit.MINUTES);
    }

    @Override
    public long getRecordingUpdatePeriod(TimeUnit unit) {
        return unit.convert(mRecordingUpdateMin, TimeUnit.MINUTES);
    }

    @Override
    public long getVodPurchaseUpdatePeriod(TimeUnit unit) {
        return unit.convert(mVodPurchaseUpdateMin, TimeUnit.MINUTES);
    }

    @Override
    public long getVodCacheTime(TimeUnit unit) {
        return unit.convert(mVodUpdateMin, TimeUnit.MINUTES);
    }

    @Override
    public long getBootConfigUpdatePeriod(TimeUnit unit) {
        return unit.convert(mBootConfigUpdateMin, TimeUnit.MINUTES);
    }

    @Override
    public long getGlobalInstallUpdatePeriod(TimeUnit unit) {
        return unit.convert(mGlobalInstallUpdateMin, TimeUnit.MINUTES);
    }

    @Override
    public long getAccountInfoUpdatePeriod(TimeUnit unit) {
        return unit.convert(mAccountInfoUpdateMin, TimeUnit.MINUTES);
    }

    @Override
    public long getWatchlistUpdatePeriod(TimeUnit unit) {
        return unit.convert(mWatchlistUpdateMin, TimeUnit.MINUTES);
    }

    @Override
    public long getConsentUpdateLimit(TimeUnit unit){
        return unit.convert(mConsentUpdateLimitDays, TimeUnit.DAYS);
    }

    @Override
    public long geTranslationUpdatePeriod(TimeUnit unit) {
        return unit.convert(mTranslationUpdateMin, TimeUnit.MINUTES);
    }

    @Override
    public long getConfigUpdatePeriod(TimeUnit unit) {
        return unit.convert(mConfigUpdateMin, TimeUnit.MINUTES);
    }
}
