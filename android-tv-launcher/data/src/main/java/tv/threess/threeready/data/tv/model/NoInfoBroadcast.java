/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.tv.model;

import java.util.Collections;
import java.util.List;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.tv.model.IBroadcast;

/**
 * Dummy program to display on the UI when there is no data available from the backend.
 *
 * @author Barabas Attila
 * @since 2019.09.04
 */
public class NoInfoBroadcast implements IBroadcast {

    private final String mChannelId;
    private final String mId;
    private final String mTitle;
    private final ParentalRating mParentalRating;
    private final long mStartTime;
    private final long mEndTime;
    private final boolean mIsLoaded;

    public NoInfoBroadcast(String channelId, String title,
                           ParentalRating parentalRating,
                           long startTime, long endTime) {
        this(channelId, title, parentalRating, startTime, endTime, false);
    }

    public NoInfoBroadcast(String channelId, String title,
                           ParentalRating parentalRating,
                           long startTime, long endTime, boolean isLoaded) {
        mChannelId = channelId;
        mId = channelId + "." + startTime;
        mParentalRating = parentalRating;
        mStartTime = startTime;
        mEndTime = endTime;
        mTitle = title;
        mIsLoaded = isLoaded;
    }

    @Override
    public String getProgramId() {
        return "";
    }

    @Override
    public String getChannelId() {
        return mChannelId;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public long getStart() {
        return mStartTime;
    }

    @Override
    public long getEnd() {
        return mEndTime;
    }

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public boolean isBlackListed() {
        return true;
    }

    @Override
    public boolean isNextToLive() {
        return false;
    }

    @Override
    public boolean isNPVREnabled() {
        return false;
    }

    @Override
    public String getAssetIdentifier() {
        return "";
    }

    @Override
    public boolean isGenerated() {
        return true;
    }

    @Override
    public List<IImageSource> getImageSources() {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public Integer getSeasonNumber() {
        return null;
    }

    @Override
    public Integer getEpisodeNumber() {
        return null;
    }

    @Override
    public String getEpisodeTitle() {
        return "";
    }

    @Override
    public String getReleaseYear() {
        return "";
    }

    @Override
    public String getSeriesId() {
        return "";
    }

    @Override
    public List<String> getActors() {
        return Collections.emptyList();
    }

    @Override
    public List<String> getDirectors() {
        return Collections.emptyList();
    }

    @Override
    public List<String> getGenres() {
        return Collections.emptyList();
    }

    @Override
    public ParentalRating getParentalRating() {
        return mParentalRating;
    }

    @Override
    public boolean hasDescriptiveAudio() {
        return false;
    }

    @Override
    public boolean hasDescriptiveSubtitle() {
        return false;
    }

    @Override
    public boolean isLoaded() {
        return mIsLoaded;
    }

    @Override
    public String toString() {
        return "NoInfoBroadcast{Id='" + getId() + '\'' +
                ", Start='" + Log.timestamp(getStart()) + '\'' +
                ", End='" + Log.timestamp(getEnd()) + '\'' +
                ", Title='" + getTitle() + "'}";
    }
}
