/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.search;

import android.app.SearchManager;
import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Binder;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.ImageRepository;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.generic.helper.UriMatcher;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.UILog;
import tv.threess.threeready.api.log.UILog.SearchType;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.search.SearchRepository;
import tv.threess.threeready.api.search.model.SearchTerm;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.R;
import tv.threess.threeready.data.generic.BaseProvider;
import tv.threess.threeready.data.search.projection.GlobalSearchChannelProjection;
import tv.threess.threeready.data.search.projection.GlobalSearchMovieProjection;
import tv.threess.threeready.data.search.projection.GlobalSearchProgramProjection;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.data.tv.projections.ChannelProjection;
import tv.threess.threeready.data.utils.SqlUtils;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Content provider to inject results in global search.
 *
 * @author Barabas Attila
 * @since 2017.07.11
 */
public class GlobalSearchProvider extends BaseProvider {

    enum UriIDs {
        Movie,
        Program,
        Channel
    }

    interface SearchAction {
        int ACTION_TYPE_AMBIGUOUS = 1;
        int ACTION_TYPE_SWITCH_CHANNEL = 2;
    }

    private static final int DEFAULT_SEARCH_LIMIT = 20;
    private static final String SUGGEST_PARAMETER_ACTION = "action";
    private static final String DUMMY_SEARCH = "dummy";
    private static final int DEFAULT_SEARCH_ACTION = SearchAction.ACTION_TYPE_AMBIGUOUS;

    // Prepare the uri matcher.
    private static final UriMatcher<UriIDs> matcher = new UriMatcher<>(GlobalSearchContract.AUTHORITY, UriIDs.class)
            .add(UriIDs.Channel, GlobalSearchContract.Channel.PATH)
            .add(UriIDs.Channel, GlobalSearchContract.Channel.PATH, "*")
            .add(UriIDs.Movie, GlobalSearchContract.Movie.PATH)
            .add(UriIDs.Movie, GlobalSearchContract.Movie.PATH, "*")
            .add(UriIDs.Program, GlobalSearchContract.Program.PATH)
            .add(UriIDs.Program, GlobalSearchContract.Program.PATH, "*");

    private int mChannelCoverWidth;
    private int mChannelCoverHeight;
    private int mChannelCoverVerticalPadding;
    private int mChannelCoverHorizontalPadding;

    private SearchRepository mSearchRepository;
    private ParentalControlManager mParentalControlManager;
    private ImageRepository mImageRepository;

    @Override
    public boolean onCreate() {
        super.onCreate();
        mChannelCoverWidth = getContext().getResources().getDimensionPixelOffset(R.dimen.global_search_channel_cover_width);
        mChannelCoverHeight = getContext().getResources().getDimensionPixelOffset(R.dimen.global_search_channel_cover_height);
        mChannelCoverHorizontalPadding = getContext().getResources().getDimensionPixelOffset(R.dimen.global_search_channel_cover_horizontal_padding);
        mChannelCoverVerticalPadding = getContext().getResources().getDimensionPixelOffset(R.dimen.global_search_channel_cover_vertical_padding);

        mSearchRepository = Components.get(SearchRepository.class);
        mParentalControlManager = Components.get(ParentalControlManager.class);
        mImageRepository = Components.get(ImageRepository.class);

        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        long id = Binder.clearCallingIdentity();

        try {
            GlobalSearchProvider.UriIDs match = matcher.matchEnum(uri);

            final List<String> segments = uri.getPathSegments();
            if (segments == null || segments.isEmpty()) {
                throw new IllegalArgumentException("Search term is not defined.");
            }

            int start = 0;
            int limit = DEFAULT_SEARCH_LIMIT;
            int action = DEFAULT_SEARCH_ACTION;
            try {
                limit = Integer.parseInt(uri.getQueryParameter(SearchManager.SUGGEST_PARAMETER_LIMIT));
                action = Integer.parseInt(uri.getQueryParameter(SUGGEST_PARAMETER_ACTION));
            } catch (NumberFormatException | UnsupportedOperationException e) {
                // Ignore the exceptions
            }

            String searchTerm = segments.get(segments.size() - 1);
            if (isGoogleAssistantSearch(searchTerm)) {
                Log.d(TAG, "Search term : " + searchTerm + ", action type :" + action + ", limit : " + limit);
                Cursor cursor = null;
                switch (match) {
                    case Channel:
                        if (action == SearchAction.ACTION_TYPE_AMBIGUOUS) {
                            UILog.logSearchTrigger(SearchType.GA, UILog.SearchTermType.CHANNEL, searchTerm);

                            cursor = GlobalSearchChannelProjection.buildCursor(
                                    mSearchRepository.channelVoiceSearch(new SearchTerm(searchTerm), start, limit));

                        } else if (action == SearchAction.ACTION_TYPE_SWITCH_CHANNEL) { // Handle channel switch commands.
                            cursor = GlobalSearchChannelProjection.buildCursor(
                                    mSearchRepository.channelVoicePlaySearch(new SearchTerm(searchTerm)));
                        }

                        return cursor;
                    case Movie:
                        if (action == SearchAction.ACTION_TYPE_AMBIGUOUS) {
                            UILog.logSearchTrigger(SearchType.GA, UILog.SearchTermType.MOVIE, searchTerm);

                            // Trigger VOD play or result list search.
                            if (limit > 1) {
                                cursor = GlobalSearchMovieProjection.buildCursor(
                                        mSearchRepository.vodVoiceSearch(new SearchTerm(searchTerm), start, limit),
                                        mParentalControlManager, Components.get(Translator.class));
                            } else {
                                cursor = GlobalSearchMovieProjection.buildCursor(
                                        mParentalControlManager, Components.get(Translator.class),
                                        mSearchRepository.contentVoicePlaySearch(new SearchTerm(searchTerm)));
                            }
                        }
                        return cursor;

                    case Program:
                        if (action == SearchAction.ACTION_TYPE_AMBIGUOUS) {
                            UILog.logSearchTrigger(SearchType.GA, UILog.SearchTermType.SERIES, searchTerm);

                            // Trigger broadcast play or result list search.
                            if (limit > 1) {
                                cursor = GlobalSearchProgramProjection.buildCursor(
                                        mSearchRepository.broadcastVoiceSearch(new SearchTerm(searchTerm), start, limit),
                                        mParentalControlManager, Components.get(Translator.class));
                            } else {
                                cursor = GlobalSearchProgramProjection.buildCursor(
                                        mParentalControlManager, Components.get(Translator.class),
                                        mSearchRepository.contentVoicePlaySearch(new SearchTerm(searchTerm)));
                            }
                        }
                        return cursor;

                    default:
                        throw new IllegalArgumentException("Unknown Uri: " + uri);
                }
            }
        } catch (Exception e) {
            // Catch all exception, don't forward them.
            Log.e(TAG, "Could not provide search results.", e);
        } finally {
            Binder.restoreCallingIdentity(id);
        }

        return null;
    }

    /**
     * Generate a 16:9 cover for channels with the channel logo and a background color.
     */
    public File generateChannelCover(String channelId) throws IOException,
            ExecutionException, InterruptedException {

        Rect cardRect = new Rect(0, 0, mChannelCoverWidth, mChannelCoverHeight);
        Rect logoRect = new Rect(mChannelCoverHorizontalPadding, mChannelCoverVerticalPadding,
                mChannelCoverWidth - mChannelCoverHorizontalPadding,
                mChannelCoverHeight - mChannelCoverVerticalPadding);

        Bitmap bitmap = Bitmap.createBitmap(cardRect.width(), cardRect.height(), Bitmap.Config.ARGB_8888);

        TvChannel channel = SqlUtils.queryFirst(getContext(),
                TvContract.Channel.CONTENT_URI,
                ChannelProjection.PROJECTION,
                TvContract.Channel.ID + " = ?",
                new String[]{channelId},
                ChannelProjection::readRow
        );

        // Download channel logo.
        Bitmap logoBitmap = Glide.with(getContext())
                .asBitmap()
                .fitCenter()
                .load(channel)
                .submit(logoRect.width(), logoRect.height())
                .get();

        // Create padded channel cover.
        Paint paint = new Paint();

        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(Components.get(LayoutConfig.class).getPlaceHolderColor());
        canvas.drawBitmap(logoBitmap,
                (cardRect.width() - logoBitmap.getWidth()) / 2,
                (cardRect.height() - logoBitmap.getHeight()) / 2, paint);

        // Save padded channel cover.
        FileOutputStream fos = null;
        File file = new File(getContext().getCacheDir() + "/" + channelId);
        try {
            fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            bitmap.recycle();
        } finally {
            FileUtils.closeSafe(fos);
        }

        return file;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        GlobalSearchProvider.UriIDs match = matcher.matchEnum(uri);
        switch (match) {
            case Program:
                return GlobalSearchContract.Program.MIME_TYPE;

            case Movie:
                return GlobalSearchContract.Movie.MIME_TYPE;

            case Channel:
                return GlobalSearchContract.Channel.MIME_TYPE;

            default:
                return null;
        }
    }

    @Nullable
    @Override
    public ParcelFileDescriptor openFile(@NonNull Uri uri, @NonNull String mode) throws FileNotFoundException {
        long id = Binder.clearCallingIdentity();

        try {
            UriIDs match = matcher.matchEnum(uri);

            final List<String> segments = uri.getPathSegments();
            if (segments == null || segments.isEmpty()) {
                throw new IllegalArgumentException("Channel id is not defined.");
            }

            switch (match) {
                case Channel: {
                    try {
                        String channelId = segments.get(segments.size() - 1);
                        if (TextUtils.isEmpty(channelId)) {
                            throw new IllegalArgumentException("Channel id is not defined.");
                        }
                        File file = generateChannelCover(channelId);
                        return ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
                    } catch (Exception e) {
                        Log.e(TAG, "Could not provide channel logo.", e);
                        return null;
                    }
                }
                case Program:
                case Movie: {
                    try {
                        String imageName = segments.get(segments.size() - 1);
                        if (TextUtils.isEmpty(imageName)) {
                            throw new IllegalArgumentException("Image name is not defined.");
                        }

                        String imageUrl;
                        boolean isVod = match.equals(UriIDs.Movie);
                        imageUrl = mImageRepository.getCoverUrlForVoiceSearch(
                                imageName,
                                isVod ? getContext().getResources().getDimensionPixelOffset(R.dimen.global_search_portrait_card_width) : getContext().getResources().getDimensionPixelSize(R.dimen.global_search_landscape_card_width),
                                isVod ? getContext().getResources().getDimensionPixelOffset(R.dimen.global_search_portrait_card_height) : getContext().getResources().getDimensionPixelSize(R.dimen.global_search_landscape_card_height),
                                isVod);

                        File file = Glide
                                .with(getContext())
                                .load(imageUrl)
                                .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                                .get();
                        return ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
                    } catch (Exception e) {
                        Log.e(TAG, "Could not provide program/vod logo.", e);
                        return null;
                    }
                }
                default:
                    throw new FileNotFoundException("No files supported by provider at " + uri);
            }
        } finally {
            Binder.restoreCallingIdentity(id);
        }
    }

    private boolean isGoogleAssistantSearch(String searchTerm) {
        return !searchTerm.equalsIgnoreCase(DUMMY_SEARCH);
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        throw new IllegalStateException("Insert action is not available for global search provider");
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        throw new IllegalStateException("Delete action is not available for global search provider");
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values,
                      @Nullable String selection, @Nullable String[] selectionArgs) {
        throw new IllegalStateException("Update action is not available for global search provider");
    }
}
