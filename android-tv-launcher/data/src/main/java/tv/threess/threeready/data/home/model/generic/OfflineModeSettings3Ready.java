/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.home.model.generic;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.home.model.generic.OfflineModeSettings;

/**
 * Class representing the local configuration values for offline mode.
 *
 * @author Barabas Attila
 * @since 2017.08.24
 */
public class OfflineModeSettings3Ready implements Serializable, OfflineModeSettings {

    @SerializedName("notification_redisplay_period")
    private int mNotificationRedisplayPeriod;

    @SerializedName("backend_check_period_min_min")
    private int mBackendCheckPeriodMin;

    @SerializedName("backend_check_period_max_min")
    private int mBackendCheckPeriodMax;

    @SerializedName("internet_check_period_sec")
    private int mInternetCheckPeriod;

    @Override
    public int getNotificationRedisplayPeriod() {
        return mNotificationRedisplayPeriod;
    }

    @Override
    public long getBackendCheckPeriodMin(TimeUnit unit) {
        return unit.convert(mBackendCheckPeriodMin, TimeUnit.MINUTES);
    }

    @Override
    public long getBackendCheckPeriodMax(TimeUnit unit) {
        return unit.convert(mBackendCheckPeriodMax, TimeUnit.MINUTES);
    }

    @Override
    public long getInternetCheckPeriod(TimeUnit unit) {
        return unit.convert(mInternetCheckPeriod, TimeUnit.SECONDS);
    }
}
