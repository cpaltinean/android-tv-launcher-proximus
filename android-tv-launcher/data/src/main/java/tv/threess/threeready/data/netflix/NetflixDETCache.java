package tv.threess.threeready.data.netflix;

import android.app.Application;
import android.content.ContentResolver;
import android.content.ContentValues;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.helper.TimerLog;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.netflix.INetflixDETResponse;
import tv.threess.threeready.api.netflix.model.NetflixDETCategory;
import tv.threess.threeready.data.netflix.model.NetflixDETResponse;
import tv.threess.threeready.data.netflix.projection.NetflixDETProjection;
import tv.threess.threeready.data.utils.SqlUtils;

/**
 * Cache implementation used for handle the Netflix DET response related database methods.
 *
 * @author David
 * @since 25.02.2022
 */
public class NetflixDETCache implements Component {

    private final String TAG = Log.tag(NetflixDETCache.class);

    private final Application mApp;

    public NetflixDETCache(Application application) {
        mApp = application;
    }

    /**
     * Use this method for get the Netflix DET response from the data base.
     * This response will be used for preparing the Netflix cards for a new stripe.
     *
     * @param category is used for identifying the needed content for the given stripe.
     */
    @Nullable
    public NetflixDETResponse getNetflixStripe(@NonNull NetflixDETCategory category) {
        return SqlUtils.queryFirst(mApp,
                NetflixDETContract.buildForCategory(category.name()),
                NetflixDETProjection.PROJECTION,
                null,
                null,
                NetflixDETProjection::readRow
        );
    }

    /**
     * Method used for update the cached Netflix DET response.
     * If we already have a response with the same category, we will overwrite it and use the new one.
     */
    public void updateNetflixStripe(INetflixDETResponse tileResponse) {
        TimerLog timerLog = new TimerLog(TAG);

        long now = System.currentTimeMillis();
        timerLog.d("Update Netflix tile.");

        ContentValues values = new ContentValues();
        values.put(NetflixDETContract.Netflix.CATEGORY, tileResponse.getCategory().name());
        values.put(NetflixDETContract.Netflix.UI_LANGUAGE, tileResponse.getUILanguage());
        values.put(NetflixDETContract.Netflix.RESPONSE_DATA, tileResponse.getResponseData());
        values.put(NetflixDETContract.Netflix.MIN_REFRESH_AT, tileResponse.getMinRefreshAt());
        values.put(NetflixDETContract.Netflix.EXPIRE_AT, tileResponse.getExpireAt());
        values.put(NetflixDETContract.Netflix.LAST_UPDATED, now);

        ContentResolver cr = mApp.getContentResolver();
        SqlUtils.bulkReplace(cr, NetflixDETContract.buildForCategory(tileResponse.getCategory()), now, values);
        timerLog.d("Netflix tile updated.");
    }

    /**
     * When the DET token is changed we have to reset the response, for this we will reset the expiration time.
     * If somehow the new response is not received we keep the DET response until it`s expired {@see tv.threess.threeready.api.netflix.INetflixDETResponse#getExpireAt()}
     */
    public void resetNetflixDETExpirationTime() {
        String selection = NetflixDETContract.Netflix.MIN_REFRESH_AT + " > 0";

        ContentValues values = new ContentValues();
        values.put(NetflixDETContract.Netflix.MIN_REFRESH_AT, 0);

        ContentResolver cr = mApp.getContentResolver();
        cr.update(NetflixDETContract.Netflix.CONTENT_URI, values, selection, null);
    }
}