package tv.threess.threeready.data.gms;

import android.app.Application;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.notification.model.NotificationItem;
import tv.threess.threeready.data.gms.projection.AppProjection;
import tv.threess.threeready.data.gms.projection.StoredNotificationProjection;
import tv.threess.threeready.data.utils.SqlUtils;

/**
 * Cache implementation for reading/writing GMS related data from/in the locale data base.
 *
 * @author David
 * @since 16.11.2021
 */
public class GmsCache implements Component {
    private final static String TAG = Log.tag(GmsCache.class);

    private final Application mApp;

    public GmsCache(Application application) {
        mApp = application;
    }

    // ----------------- Apps

    /**
     * Update the last opened time of the given application
     *
     * @param packageName    used to identify the app
     * @param lastOpenedTime used for updating the correct field
     */
    public void updateAppLastOpenedTime(String packageName, long lastOpenedTime) {
        ContentValues c = new ContentValues();
        ContentResolver cr = mApp.getContentResolver();
        c.put(GmsContract.Apps.PACKAGE_NAME, packageName);
        c.put(GmsContract.Apps.LAST_OPENED, lastOpenedTime);
        cr.insert(GmsContract.Apps.CONTENT_URI, c);
    }

    /**
     * Return the package name and the last opened timed map from database.
     */
    public Map<String, Long> getAppsLastOpenedTimeMap() {
        return ArrayUtils.notNull(SqlUtils.queryFirst(mApp,
                GmsContract.Apps.CONTENT_URI,
                AppProjection.PROJECTION,
                AppProjection::mapFromCursor
        ));
    }

    // ----------------- StoredNotification

    /**
     * Method used for saving the given notifications.
     *
     * @param notificationsList list of notifications we want to save
     * @throws OperationApplicationException
     * @throws RemoteException
     */
    public void saveNotifications(List<NotificationItem> notificationsList) throws OperationApplicationException, RemoteException {
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();
        ContentProviderOperation deleteOperation = ContentProviderOperation.newDelete(GmsContract.StoredNotification.CONTENT_URI).build();
        operations.add(deleteOperation);

        for (NotificationItem notificationItem : notificationsList) {
            ContentProviderOperation insertOperation = ContentProviderOperation.newInsert(GmsContract.StoredNotification.CONTENT_URI)
                    .withValues(StoredNotificationProjection.getContentValues(notificationItem))
                    .build();
            operations.add(insertOperation);
        }

        mApp.getContentResolver().applyBatch(GmsContract.AUTHORITY, operations);
        Log.d(TAG, "All notifications deleted from DB.");
    }

    /**
     * @return The list of stored Notification items.
     */
    public List<NotificationItem> loadStoredNotifications() {
        return SqlUtils.queryAll(mApp,
                GmsContract.StoredNotification.CONTENT_URI,
                StoredNotificationProjection.PROJECTION,
                StoredNotificationProjection::readRow
        );
    }
}
