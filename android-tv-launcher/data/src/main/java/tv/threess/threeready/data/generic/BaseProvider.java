/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.generic;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.ComponentsInitializer;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.log.Log;

/**
 * Base class for all the content providers in the application.
 * It contains common database operation related for more convenient use
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.06.09
 */
public abstract class BaseProvider extends ContentProvider {
    protected final String TAG = Log.tag(getClass());

    public static final String DATABASE_VERSION_CHECK = "check.database.version";

    public boolean getNotify(Uri uri) {
        // Ignore notify during batch operations.
        return !uri.getBooleanQueryParameter(BaseContract.QUERY_BATCH, false);
    }

    @Override
    public boolean onCreate() {
        try {
            Context application = this.getContext();
            if (application == null) {
                // provider was not successfully loaded
                return false;
            }

            application = application.getApplicationContext();
            if (!(application instanceof ComponentsInitializer)) {
                // provider was not successfully loaded
                return false;
            }

            ((ComponentsInitializer) application).initialize();
            return true;
        } catch (Exception e) {
            Log.e(TAG, "failed to initialize components", e);
        }
        return false;
    }

    /**
     * Perform a query by combining all current settings and the
     * information passed into this method.
     *
     * @param uri           The content provider URI to query. This will be the full URI sent by the client.
     * @param db            the database to query on
     * @param table         the table from which query
     * @param projection    A list of which columns to return. Passing
     *                      null will return all columns, which is discouraged to prevent
     *                      reading data from storage that isn't going to be used.
     * @param selection     A filter declaring which rows to return,
     *                      formatted as an SQL WHERE clause (excluding the WHERE
     *                      itself). Passing null will return all rows for the given URL.
     * @param selectionArgs You may include ?s in selection, which
     *                      will be replaced by the values from selectionArgs, in order
     *                      that they appear in the selection. The values will be bound
     *                      as Strings.
     * @param sortOrder     How to order the rows, formatted as an SQL
     *                      ORDER BY clause (excluding the ORDER BY itself). Passing null
     *                      will use the default sort order, which may be unordered.
     * @return a cursor over the result set
     * @see android.content.ContentResolver#query(android.net.Uri, String[],
     * String, String[], String)
     */
    protected Cursor query(@NonNull Uri uri, @NonNull SQLiteDatabase db, @NonNull String table, String[] projection,
                           String selection, String[] selectionArgs, String sortOrder) {
        String limit = uri.getQueryParameter(BaseContract.QUERY_LIMIT);
        if (limit != null && !limit.isEmpty()) {
            if (sortOrder != null) {
                sortOrder += " " + BaseContract.QUERY_LIMIT + " " + limit;
            } else {
                sortOrder = BaseContract.QUERY_LIMIT + " " + limit;
            }
        }
        String group = uri.getQueryParameter(BaseContract.GROUP_BY);

        Cursor cursor = db.query(table, projection, selection, selectionArgs, group, null, sortOrder);

        Context context = getContext();
        if (context != null && cursor != null && getNotify(uri)) {
            Uri baseUri = uri.buildUpon().clearQuery().build();
            cursor.setNotificationUri(context.getContentResolver(), baseUri);
        }

        return cursor;
    }

    /**
     * Convenience method for inserting a row into the database.
     *
     * @param uri               The content provider URI of the insertion request.
     * @param db                the database to insert in
     * @param table             the table to insert the row into
     * @param values            this map contains the initial column values for the
     *                          row. The keys should be the column names and the values the
     *                          column values
     * @param conflictAlgorithm for insert conflict resolver
     * @param onBeforeNotify    Runnable executed before change is notified.
     * @return the row ID of the newly inserted row OR <code>-1</code> if either the
     * input parameter <code>conflictAlgorithm</code> = {@link SQLiteDatabase#CONFLICT_NONE }
     * or an error occurred.
     */
    protected long insert(@NonNull Uri uri, @NonNull SQLiteDatabase db,
                          @NonNull String table, ContentValues values,
                          int conflictAlgorithm, Runnable onBeforeNotify) {
        long rowID = db.insertWithOnConflict(table, null, values, conflictAlgorithm);
        if (onBeforeNotify != null) {
            onBeforeNotify.run();
        }
        if (rowID >= 0) {
            notifyChange(uri);
        }
        return rowID;
    }

    /**
     * General method for inserting a row into the database.
     *
     * @param uri               The content provider URI of the insertion request.
     * @param db                the database to insert in
     * @param table             the table to insert the row into
     * @param values            this map contains the initial column values for the
     *                          row. The keys should be the column names and the values the
     *                          column values
     * @param conflictAlgorithm for insert conflict resolver
     * @return the row ID of the newly inserted row OR <code>-1</code> if either the
     * input parameter <code>conflictAlgorithm</code> = {@link SQLiteDatabase#CONFLICT_NONE }
     * or an error occurred.
     */
    protected long insert(@NonNull Uri uri, @NonNull SQLiteDatabase db,
                          @NonNull String table, ContentValues values,
                          int conflictAlgorithm) {
        return insert(uri, db, table, values, conflictAlgorithm, null);
    }

    /**
     * Convenience method for inserting a row into the database.
     *
     * @param uri    The content provider URI of the insertion request.
     * @param db     the database to insert in
     * @param table  the table to insert the row into
     * @param values this map contains the initial column values for the
     *               row. The keys should be the column names and the values the
     *               column values
     * @return the row ID of the newly inserted row, or -1 if an error occurred
     */
    protected long insert(@NonNull Uri uri, @NonNull SQLiteDatabase db,
                          @NonNull String table, ContentValues values) {
        return insert(uri, db, table, values, SQLiteDatabase.CONFLICT_ABORT, null);
    }

    /**
     * Convenience method for deleting rows in the database.
     *
     * @param uri             The content provider URI of the deletion request.
     * @param db              the database to delete from
     * @param table           the table to delete from
     * @param deleteWhereCase the optional WHERE clause to apply when deleting.
     *                        Passing null will delete all rows.
     * @param selectionArgs   You may include ?s in the where clause, which
     *                        will be replaced by the values from whereArgs. The values
     *                        will be bound as Strings.
     * @param onBeforeNotify  Runnable executed before change is notified.
     * @return the number of rows affected if a whereClause is passed in, 0
     * otherwise. To remove all rows and get a count pass "1" as the
     * whereClause.
     */
    protected int delete(@NonNull Uri uri, @NonNull SQLiteDatabase db, @NonNull String table,
                         @Nullable String deleteWhereCase, @Nullable String[] selectionArgs,
                         Runnable onBeforeNotify) {
        int affected = db.delete(table, deleteWhereCase, selectionArgs);
        if (onBeforeNotify != null) {
            onBeforeNotify.run();
        }
        if (affected > 0) {
            notifyChange(uri);
        }
        return affected;
    }

    /**
     * Convenience method for deleting rows in the database.
     *
     * @param uri             The content provider URI of the deletion request.
     * @param db              the database to delete from
     * @param table           the table to delete from
     * @param deleteWhereCase the optional WHERE clause to apply when deleting.
     *                        Passing null will delete all rows.
     * @param selectionArgs   You may include ?s in the where clause, which
     *                        will be replaced by the values from whereArgs. The values
     *                        will be bound as Strings.
     * @return the number of rows affected if a whereClause is passed in, 0
     * otherwise. To remove all rows and get a count pass "1" as the
     * whereClause.
     */
    protected int delete(@NonNull Uri uri, @NonNull SQLiteDatabase db, @NonNull String table,
                         @Nullable String deleteWhereCase, @Nullable String[] selectionArgs) {
        return delete(uri, db, table, deleteWhereCase, selectionArgs, null);
    }

    /**
     * Convenience method for updating rows in the database.
     *
     * @param uri            The content provider URI of the update request.
     * @param db             the database to update
     * @param table          the table to update
     * @param values         a map from column names to new column values. null is a
     *                       valid value that will be translated to NULL.
     * @param selection      the optional WHERE clause to apply when updating.
     *                       Passing null will update all rows.
     * @param selectionArgs  You may include ?s in the where clause, which
     *                       will be replaced by the values from whereArgs. The values
     *                       will be bound as Strings.
     * @param onBeforeNotify Runnable executed before change is notified.
     * @return the number of rows affected
     */
    protected int update(@NonNull Uri uri, @NonNull SQLiteDatabase db, @NonNull String table,
                         ContentValues values, String selection, String[] selectionArgs,
                         Runnable onBeforeNotify) {
        int affected = db.update(table, values, selection, selectionArgs);
        if (onBeforeNotify != null) {
            onBeforeNotify.run();
        }
        if (affected > 0) {
            notifyChange(uri);
        }
        return affected;
    }

    /**
     * Convenience method for updating rows in the database.
     *
     * @param uri           The content provider URI of the update request.
     * @param db            the database to update
     * @param table         the table to update
     * @param values        a map from column names to new column values. null is a
     *                      valid value that will be translated to NULL.
     * @param selection     the optional WHERE clause to apply when updating.
     *                      Passing null will update all rows.
     * @param selectionArgs You may include ?s in the where clause, which
     *                      will be replaced by the values from whereArgs. The values
     *                      will be bound as Strings.
     * @return the number of rows affected
     */
    protected int update(@NonNull Uri uri, @NonNull SQLiteDatabase db, @NonNull String table,
                         ContentValues values, String selection, String[] selectionArgs) {
        return update(uri, db, table, values, selection, selectionArgs, null);
    }

    /**
     * Convenience method for updating rows in the database.
     *
     * @param uri    The content provider URI of the update request.
     * @param db     the database to update
     * @param table  the table to update
     * @param values a map from column names to new column values. null is a
     *               valid value that will be translated to NULL.
     * @return the number of rows affected.
     */
    protected int bulkInsertReplace(@NonNull Uri uri, @NonNull SQLiteDatabase db,
                                    @NonNull String table, ContentValues[] values) {
        return bulkInsertReplacePurge(uri, db, table, null, null, values, null);
    }

    /**
     * Convenience method for updating rows in the database.
     *
     * @param uri              The content provider URI of the update request.
     * @param db               the database to update
     * @param table            the table to update
     * @param values           a map from column names to new column values. null is a
     *                         valid value that will be translated to NULL.
     * @param onTransactionEnd Runnable executed at the end of the transaction.
     * @return the number of rows affected.
     */
    protected int bulkInsertReplace(Uri uri, SQLiteDatabase db, String table, ContentValues[] values, Runnable onTransactionEnd) {
        return bulkInsertReplacePurge(uri, db, table, null, null, values, onTransactionEnd);
    }

    /**
     * Convenience method for updating rows in the database
     * and delete some other rows within a transaction.
     *
     * @param uri             The content provider URI of the update request.
     * @param db              the database to update
     * @param table           the table to update
     * @param values          a map from column names to new column values. null is a
     *                        valid value that will be translated to NULL.
     * @param deleteWhereCase the optional WHERE clause to apply when deleting.
     *                        Passing null or empty will delete all the previously inserted rows.
     * @return the number of rows affected, combine the update and deletion
     */
    protected int bulkInsertReplacePurge(@NonNull Uri uri, @NonNull SQLiteDatabase db,
                                         @NonNull String table, String deleteWhereCase, ContentValues[] values) {
        if (!TextUtils.isEmpty(deleteWhereCase)) {
            deleteWhereCase = BaseContract.LAST_UPDATED + "<" + deleteWhereCase;
        }
        return bulkInsertReplacePurge(uri, db, table, deleteWhereCase, null, values);
    }

    /**
     * Convenience method for updating rows in the database
     * and delete some other rows within a transaction.
     *
     * @param uri              The content provider URI of the update request.
     * @param db               the database to update
     * @param table            the table to update
     * @param values           a map from column names to new column values. null is a
     *                         valid value that will be translated to NULL.
     * @param deleteWhereCase  the optional WHERE clause to apply when deleting.
     *                         Passing null will delete all rows.
     * @param deleteWhereArgs  You may include ?s in the where clause, which
     *                         will be replaced by the values from whereArgs. The values
     *                         will be bound as Strings.
     * @param onTransactionEnd Runnable executed
     * @return the number of rows affected, combine the update and deletion
     */
    protected int bulkInsertReplacePurge(@NonNull Uri uri, @NonNull SQLiteDatabase db,
                                         @NonNull String table, String deleteWhereCase,
                                         String[] deleteWhereArgs, ContentValues[] values,
                                         Runnable onTransactionEnd) {
        int inserted, deleted = 0;

        // insert all or nothing
        db.beginTransaction();
        try {
            inserted = bulkInsertWithOnConflict(db, table, values, SQLiteDatabase.CONFLICT_REPLACE);

            if (deleteWhereCase != null && !deleteWhereCase.isEmpty()) {
                deleted += db.delete(table, deleteWhereCase, deleteWhereArgs);
            }

            Log.d(TAG, "Inserted " + values.length
                    + " [" + inserted + " - " + deleted + "] rows into: " + table);

            if (onTransactionEnd != null) {
                onTransactionEnd.run();
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        int affected = inserted + deleted;
        if (affected > 0) {
            notifyChange(uri);
        }

        return affected;
    }

    /**
     * Convenience method for updating rows in the database
     * and delete some other rows within a transaction.
     *
     * @param uri             The content provider URI of the update request.
     * @param db              the database to update
     * @param table           the table to update
     * @param values          a map from column names to new column values. null is a
     *                        valid value that will be translated to NULL.
     * @param deleteWhereCase the optional WHERE clause to apply when deleting.
     *                        Passing null will delete all rows.
     * @param deleteWhereArgs You may include ?s in the where clause, which
     *                        will be replaced by the values from whereArgs. The values
     *                        will be bound as Strings.
     * @return the number of rows affected, combine the update and deletion
     */
    protected int bulkInsertReplacePurge(@NonNull Uri uri, @NonNull SQLiteDatabase db,
                                         @NonNull String table, String deleteWhereCase,
                                         String[] deleteWhereArgs, ContentValues[] values) {
        return bulkInsertReplacePurge(uri, db, table, deleteWhereCase, deleteWhereArgs, values, null);
    }

    /**
     * General method for inserting a row into the database.
     *
     * @param table             the table to insert the row into
     * @param contentValues     this map contains the initial column values for the
     *                          row. The keys should be the column names and the values the
     *                          column values
     * @param conflictAlgorithm for insert conflict resolver
     * @return the row ID of the newly inserted row OR <code>-1</code> if either the
     * input parameter <code>conflictAlgorithm</code> = {@link SQLiteDatabase#CONFLICT_IGNORE}
     * or an error occurred.
     */
    private int bulkInsertWithOnConflict(SQLiteDatabase db, String table, ContentValues[] contentValues, int conflictAlgorithm) {

        if (contentValues == null || contentValues.length == 0) {
            // Nothing to insert.
            return 0;
        }

        ContentValues initValue = contentValues[0];

        SQLiteStatement statement = null;
        db.acquireReference();
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT");
            sql.append(CONFLICT_VALUES[conflictAlgorithm]);
            sql.append(" INTO ");
            sql.append(table);
            sql.append('(');

            String[] keys = initValue.keySet().toArray(new String[0]);
            for (String key : keys) {
                sql.append(key);
                sql.append(",");
            }
            sql.deleteCharAt(sql.length() - 1);

            sql.append(')');
            sql.append(" VALUES (");
            for (int i = 0; i < keys.length; i++) {
                sql.append("?,");
            }
            sql.deleteCharAt(sql.length() - 1);

            sql.append(')');
            statement = db.compileStatement(sql.toString());

            int inserted = 0;
            for (ContentValues contentValue : contentValues) {
                for (int index = 1; index <= keys.length; ++index) {
                    Object value = contentValue.get(keys[index - 1]);
                    if (value == null) {
                        statement.bindNull(index);
                    } else if (value instanceof Float || value instanceof Double) {
                        statement.bindDouble(index, ((Number) value).doubleValue());
                    } else if (value instanceof Number) {
                        statement.bindLong(index, ((Number) value).longValue());
                    } else if (value instanceof byte[]) {
                        statement.bindBlob(index, (byte[]) value);
                    } else if (value instanceof Boolean) {
                        statement.bindLong(index, (Boolean) value ? 1L : 0L);
                    } else {
                        statement.bindString(index, value.toString());
                    }
                }

                statement.executeInsert();
                inserted++;
            }

            return inserted;
        } finally {
            FileUtils.closeSafe(statement);
            db.releaseReference();
        }
    }

    /**
     * Execute multiple database related operations (insert, update, delete) within a transaction.
     *
     * @param db         the database on which the changes needs to be applied
     * @param operations the operations which needs to be applied
     * @return URIs and the affected rows of each operation.
     * @throws OperationApplicationException thrown if any operation fails.
     */
    public ContentProviderResult[] applyBatch(@NonNull SQLiteDatabase db,
                                              @NonNull ArrayList<ContentProviderOperation> operations) throws OperationApplicationException {
        Context context = getContext();
        db.beginTransaction();
        try {
            ContentProviderResult[] results = super.applyBatch(operations);
            db.setTransactionSuccessful();

            // Notify all URI from operations.
            Set<Uri> notifiedUriSet = new HashSet<>();
            for (ContentProviderOperation operation : operations) {
                Uri baseUri = operation.getUri().buildUpon().clearQuery().build();
                if (context != null && operation.isWriteOperation() && !notifiedUriSet.contains(baseUri)) {
                    context.getContentResolver().notifyChange(baseUri, null);
                    notifiedUriSet.add(baseUri);
                }
            }

            return results;
        } finally {
            db.endTransaction();
        }
    }

    protected void notifyChange(@NonNull Uri uri) {
        Context context = getContext();
        if (context != null && getNotify(uri)) {
            Uri baseUri = uri.buildUpon().clearQuery().build();
            context.getContentResolver().notifyChange(baseUri, null);
        }
    }

    private static final String[] CONFLICT_VALUES = {
            "",
            " OR ROLLBACK ",
            " OR ABORT ",
            " OR FAIL ",
            " OR IGNORE ",
            " OR REPLACE "
    };
}