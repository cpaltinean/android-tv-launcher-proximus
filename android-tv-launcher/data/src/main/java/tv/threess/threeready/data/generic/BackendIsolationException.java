package tv.threess.threeready.data.generic;

import tv.threess.threeready.api.generic.exception.BackendException;

public class BackendIsolationException extends BackendException {
    public BackendIsolationException() {
        super("BEP-less Isolated State");
    }
}
