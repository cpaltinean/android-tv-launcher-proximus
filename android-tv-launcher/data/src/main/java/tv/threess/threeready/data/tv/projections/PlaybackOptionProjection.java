/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.tv.projections;

import android.database.Cursor;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.api.tv.model.PlaybackOption;
import tv.threess.threeready.data.generic.IProjection;

/**
 * Created by Boldijar Paul on 4/25/17.
 */
public enum PlaybackOptionProjection implements IProjection {
    CHANNEL_ID(TvContract.PlaybackOption.CHANNEL_ID),
    PLAYBACK_TYPE(TvContract.PlaybackOption.PLAYBACK_TYPE),
    PLAYBACK_DATA(TvContract.PlaybackOption.PLAYBACK_DATA),
    PLAYBACK_EXTRA(TvContract.PlaybackOption.PLAYBACK_EXTRA),
    TIF_CHANNEL_ID(TvContract.PlaybackOption.TIF_CHANNEL_ID),
    TIF_INPUT_ID(TvContract.PlaybackOption.TIF_INPUT_ID);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    PlaybackOptionProjection(String column) {
        mColumn = TvContract.PlaybackOption.TABLE_NAME + "." + column;
    }

    public String getColumn() {
        return mColumn;
    }

    public static PlaybackOption readRow(Cursor cursor) {
        return new PlaybackOption(
                cursor.getString(CHANNEL_ID.ordinal()),
                cursor.getLong(TIF_CHANNEL_ID.ordinal()),
                cursor.getString(TIF_INPUT_ID.ordinal()),
                cursor.getString(PLAYBACK_TYPE.ordinal()),
                cursor.getString(PLAYBACK_DATA.ordinal()),
                cursor.getString(PLAYBACK_EXTRA.ordinal())
        );
    }
}
