/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.home.model.module;

import android.text.TextUtils;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.home.model.generic.GradientStyle;
import tv.threess.threeready.api.home.model.module.EditorialItemStyle;
import tv.threess.threeready.api.tv.model.ImageSource;
import tv.threess.threeready.data.account.adapter.ColorTypeAdapter3Ready;
import tv.threess.threeready.data.home.model.generic.GradientStyle3Ready;

/**
 * POJO class which holds the style configuration options for a module item.
 * Can be set directly for one item or for the whole module.
 *
 * @author Barabas Attila
 * @since 2018.08.23
 */
public class EditorialItemStyle3Ready implements EditorialItemStyle {

    @SerializedName("width")
    private Float mWidth;

    @SerializedName("height")
    private Float mHeight;

    @SerializedName("padding")
    private Float mPadding;

    @SerializedName("title_color")
    @JsonAdapter(ColorTypeAdapter3Ready.class)
    private Integer mTitleColor;

    @SerializedName("gradient")
    private GradientStyle3Ready mGradientStyle;

    @SerializedName("background_image")
    private String mBackgroundImage;

    @SerializedName("icon")
    private String mIcon;

    @SerializedName("font_size")
    private Float mTitleFontSize;

    @SerializedName("focus_scale_size")
    private Float mFocusScaleFactor;

    private FixedSize mWidthSize;
    private FixedSize mHeightSize;
    private FixedSize mFocusScaleSize;

    private IImageSource mBackgroundSource;
    private IImageSource mIconSource;

    @Override
    public Float getTitleFontSize() {
        return mTitleFontSize;
    }

    @Override
    public Size getWidth() {
        if (mWidth != null && mWidth > 0) {
            if (mWidthSize == null) {
                mWidthSize = new FixedSize(mWidth);
            }
            return mWidthSize;
        }
        return null;
    }

    @Override
    public Size getHeight() {
        if (mHeight != null && mHeight > 0) {
            if (mHeightSize == null) {
                mHeightSize = new FixedSize(mHeight);
            }
            return mHeightSize;
        }
        return null;
    }

    @Override
    public Float getPadding() {
        return mPadding;
    }

    @Override
    public Integer getTitleColor() {
        return ColorUtils.getColor(mTitleColor, null);
    }

    @Override
    public GradientStyle getGradient() {
        return mGradientStyle;
    }

    @Override
    public IImageSource getBackgroundImage() {
        if (mBackgroundSource == null && !TextUtils.isEmpty(mBackgroundImage)) {
            mBackgroundSource = new ImageSource(mBackgroundImage, IImageSource.Type.URL);
        }
        return mBackgroundSource;
    }

    @Override
    public IImageSource getIcon() {
        if (mIconSource == null && !TextUtils.isEmpty(mIcon)) {
            mIconSource = new ImageSource(mIcon, IImageSource.Type.URL);
        }
        return mIconSource;
    }

    @Override
    public Size getFocusScaleSize() {
        if (mFocusScaleFactor != null && mFocusScaleFactor > 0) {
            if (mFocusScaleSize == null) {
                mFocusScaleSize = new FixedSize(mFocusScaleFactor);
            }
            return mFocusScaleSize;
        }
        return null;
    }
}
