/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.account.observable;

import android.content.Context;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.home.model.generic.FontStyle;
import tv.threess.threeready.api.home.model.local.FontConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.home.model.locale.FontConfig3Ready;
import tv.threess.threeready.data.utils.GsonUtils;

/**
 * RX observable to load the font configs.
 *
 * @author Barabas Attila
 * @since 2018.07.10
 */
public class FontConfigObservable implements ObservableOnSubscribe<FontConfig[]> {
    private static final String TAG = Log.tag(FontConfigObservable.class);


    private final FontStyle mFontStyle = Components.get(FontStyle.class);

    private final Context mContext;

    public FontConfigObservable(Context context) {
        mContext = context;
    }

    @Override
    public void subscribe(ObservableEmitter<FontConfig[]> emitter) throws Exception {
        Log.d(TAG, "Load from config from : " + mFontStyle.fileName);
        FontConfig[] fontConfigs = GsonUtils.readJsonFromAssets(
                mContext, mFontStyle.fileName, FontConfig3Ready[].class);
        emitter.onNext(fontConfigs);
        emitter.onComplete();
    }
}
