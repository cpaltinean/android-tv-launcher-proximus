/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.tv.observable;

import android.content.Context;

import java.util.List;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.ApiConfig;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.tv.model.IBroadcast;

/**
 * This class will handle the database query and fetching from server if needed for upcoming
 * data for specific id
 *
 * @author Hunor Madaras
 * @since 2019.05.07
 */
public class UpcomingBroadcastsObservable extends BroadcastsBetweenObservable<ModuleData<IBroadcast>> {

    private final ModuleConfig mConfig;
    private final boolean mHasPlayableFilter;
    private int mLimit;

    public UpcomingBroadcastsObservable(Context context, String channelId, long from, long to, boolean isAdultChannel, boolean hasPlayableFilter, ModuleConfig config) {
        super(context, channelId, from, to, isAdultChannel);
        if (from > to) {
            throw new IllegalArgumentException("From (" + from + ") cannot be bigger or equal than to (" + to + ")");
        }
        mConfig = config;
        mHasPlayableFilter = hasPlayableFilter;
        mLimit = Components.get(ApiConfig.class).getDefaultPageSize();

        if (config.getDataSource() != null && config.getDataSource().getParams() != null) {
            mLimit = config.getDataSource().getParams().getSize()[0];
        }
    }

    @Override
    ModuleData<IBroadcast> getPublishValue(List<IBroadcast> broadcasts) {
        return new ModuleData<>(mConfig, broadcasts);
    }

    @Override
    List<IBroadcast> getBroadcastsListFromDatabase() {
        return mTvCache.getUpcomingBroadcasts(mChannelId, mFrom, mTo, mHasPlayableFilter, mLimit);
    }
}