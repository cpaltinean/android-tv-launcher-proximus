/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.mw;

import android.content.Context;
import android.os.PowerManager;

import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.receivers.StandByStateChangeReceiver;
import tv.threess.threeready.api.log.Log;

/**
 * Acquire wakelock when the box goes into active stand by
 * to prevent network standby before the configured timeout.
 *
 *
 * @author Barabas Attila
 * @since 2019.10.18
 */
public class WakeLockHandler implements StandByStateChangeReceiver.Listener {
    private static final String TAG = Log.tag(WakeLockHandler.class);

    private final AppConfig mAppConfig = Components.get(AppConfig.class);
    private final Context mContext;

    private PowerManager.WakeLock mWakeLock;

    public WakeLockHandler(Context context) {
        super();
        mContext = context;

        PowerManager powerManager = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        if (powerManager != null) {
            mWakeLock = powerManager.newWakeLock(
                    PowerManager.PARTIAL_WAKE_LOCK, WakeLockHandler.class.getName());
        } else {
            Log.w(TAG, "Couldn't create wakelock.");
        }
    }

    @Override
    public void onStateChanged(boolean screenOn) {
        if (mWakeLock == null) {
            return;
        }

        try {
            if (screenOn) {
                Log.d(TAG, "Release wakelock.");
                if (mWakeLock.isHeld()) {
                    mWakeLock.release();
                }
            } else {
                if (!mWakeLock.isHeld()) {
                    // use timeout value from application settings
                    long duration = mAppConfig.getActiveStandbyTimeout(TimeUnit.MINUTES);

                    // use override from database
                    duration = Settings.activeStandbyDuration.get(duration);

                    Log.d(TAG, "Acquire wakelock for " + duration + " min.");
                    mWakeLock.acquire(TimeUnit.MINUTES.toMillis(duration));
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Couldn't change wakelock.", e);
        }
    }
}
