/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.generic.observable;

import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;

import androidx.annotation.CallSuper;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Cancellable;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * This class can be used to easily get notified when something was updated in the database
 *
 * @author Paul
 * @since 2017.05.08
 */
public abstract class BaseContentObservable<T> implements ObservableOnSubscribe<T> {
    private static final String TAG = Log.tag(BaseContentObservable.class);

    protected static Handler sWorkerHandler = new Handler(new HandlerThread(":ContentObservable") {{
        this.start();
    }}.getLooper());

    protected final Context mContext;
    protected ObservableEmitter<T> mEmitter;

    private final Set<Uri> mRegisteredUris = new HashSet<>();

    protected final ContentObserver mContentObserver = new ContentObserver(sWorkerHandler) {
        @Override
        public void onChange(boolean selfChange) {
            onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, final Uri uri) {
            if (RxUtils.isDisposed(mEmitter)) {
                Log.e(TAG, "Could not handle content change. Emitter already disposed. Uri : " + uri);
                return;
            }

            try {
                BaseContentObservable.this.onChange(uri, mEmitter);
            } catch (Exception e) {
                Log.e(TAG, "Failed to handle data change.", e);
                mEmitter.onError(e);
            }
        }
    };

    public BaseContentObservable(Context context) {
        mContext = context;
    }

    /**
     * Called when the content observer gets notified on a previously registered content URI.
     * @param uri The uri which changed and got notified.
     * @param observableEmitter The RX emitter to dispatch the changed data to the subscriber
     */
    protected void onChange(Uri uri, ObservableEmitter<T> observableEmitter) throws Exception { }

    /**
     * Register a content observer to get notify when one of the given content uri changes.

     * @param uris The array of URI the get notified on.
     *            The URI to watch for changes. This can be a specific row URI,
     *            or a base URI for a whole class of content.
     * @param notifyForDescendants When false, the observer will be notified
     *            whenever a change occurs to the exact URI specified by
     *            <code>uri</code> or to one of the URI's ancestors in the path
     *            hierarchy. When true, the observer will also be notified
     *            whenever a change occurs to the URI's descendants in the path
     *            hierarchy.
     */
    protected void registerObserver(boolean notifyForDescendants,  Uri... uris) {
        synchronized (this) {
            if (RxUtils.isDisposed(mEmitter)) {
                Log.w(TAG, "Can't register observer. Emitter already disposed.");
                return;
            }

            for (Uri uri : uris) {
                if (mRegisteredUris.contains(uri)) {
                    // Already registered.
                    continue;
                }

                mContext.getContentResolver().registerContentObserver(uri, notifyForDescendants, mContentObserver);
                mRegisteredUris.add(uri);
            }
        }
    }

    protected void registerObserver(Uri... uris) {
        registerObserver(true, uris);
    }

    protected void registerObserver(Collection<Uri> uris) {
        registerObserver(uris.toArray(new Uri[0]));
    }

    /**
     * Unregister all the previously registered content observers.
     */
    @CallSuper
    protected void unregisterObserver() {
        synchronized (this) {
            mContext.getContentResolver().unregisterContentObserver(mContentObserver);
            mRegisteredUris.clear();
        }
    }

    @Override
    @CallSuper
    public void subscribe(ObservableEmitter<T> e) throws Exception {
        mEmitter = e;
        mEmitter.setCancellable(mCancellable);
    }

    // Unregister content observable on cancel.
    private final Cancellable mCancellable = this::unregisterObserver;
}
