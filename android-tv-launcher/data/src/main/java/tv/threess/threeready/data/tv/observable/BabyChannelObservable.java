package tv.threess.threeready.data.tv.observable;

import android.content.Context;
import android.net.Uri;

import java.util.List;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.home.observable.TvChannelObservable;
import tv.threess.threeready.data.tv.TvCache;

/**
 * Rx Observable to get the list of baby channels from local databse.
 *
 * @author Barabas Attila
 * @since 2018.10.01
 */
public class BabyChannelObservable extends BaseContentObservable<List<TvChannel>> {
    public static final String TAG = Log.tag(TvChannelObservable.class);

    private final TvCache mTvCache = Components.get(TvCache.class);

    public BabyChannelObservable(Context context) {
        super(context);
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<List<TvChannel>> observableEmitter) {
        if (uri.equals(TvContract.Channel.CONTENT_URI)) {
            emitData(observableEmitter);
        }
    }

    @Override
    public void subscribe(ObservableEmitter<List<TvChannel>> emitter) throws Exception {
        super.subscribe(emitter);

        registerObserver(TvContract.Channel.CONTENT_URI);
        emitData(emitter);
    }

    /**
     * Emit the data through the given {@link ObservableEmitter} so the subscribed user can react on it.
     *
     * @param emitter The emitter used to emit the list of {@link TvChannel}
     */
    private void emitData(ObservableEmitter<List<TvChannel>> emitter) {
        emitter.onNext(mTvCache.getBabyChannels());
    }
}
