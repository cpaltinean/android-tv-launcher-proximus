package tv.threess.threeready.data.generic;

public interface IProjection {

    String getColumn();

    @SafeVarargs
    static<E extends Enum<E> & IProjection> String[] build(E... values) {
        String[] result = new String[values.length];
        for (int i = 0; i < values.length; i++) {
            result[i] = values[i].getColumn();
        }
        return result;
    }
}
