package tv.threess.threeready.data.pvr.model;

import tv.threess.threeready.api.pvr.model.ISeriesRecordingSchedule;

/**
 * Database model for scheduled series recording entities.
 *
 * Created by Noemi Dali on 17.04.2020.
 */
public class SeriesRecordingScheduleDBModel implements ISeriesRecordingSchedule {

    private String mSeriesId;
    private String mUserSeriesId;
    private String mChannelId;
    private String mTitle;

    @Override
    public String getSeriesRefNo() {
        return mSeriesId;
    }

    @Override
    public String getUserSeriesId() {
        return mUserSeriesId;
    }

    @Override
    public String getChannelId() {
        return mChannelId;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    public static class Builder {
        SeriesRecordingScheduleDBModel seriesDbModel = new SeriesRecordingScheduleDBModel();

        public static Builder builder() {
            return new Builder();
        }

        public Builder setSeriesId(String seriesId) {
            seriesDbModel.mSeriesId = seriesId;
            return this;
        }

        public Builder setUserSeriesId(String userSeriesId) {
            seriesDbModel.mUserSeriesId = userSeriesId;
            return this;
        }

        public Builder setChannelId(String channelId) {
            seriesDbModel.mChannelId = channelId;
            return this;
        }

        public Builder setSeriesTitle(String title) {
            seriesDbModel.mTitle = title;
            return this;
        }

        public SeriesRecordingScheduleDBModel build() {
            return seriesDbModel;
        }
    }
}
