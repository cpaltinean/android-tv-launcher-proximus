package tv.threess.threeready.data.vod.model;

import java.util.Map;

import tv.threess.threeready.api.vod.model.IRentalExpiration;
import tv.threess.threeready.api.vod.model.IBaseVodItem;

/**
 * Base class used for VOD item details.
 *
 * @author David
 * @since 23.03.2022
 */
public abstract class BaseVodItem<T extends BaseVodVariantModel> implements IBaseVodItem {

    protected T mMainVariant;

    protected long mRentExpiration;

    public BaseVodItem() {

    }

    public BaseVodItem(T mainVariant) {
        mMainVariant = mainVariant;
    }

    public void setRentalExpirationInfo(Map<String, IRentalExpiration> rentalExpirationMap) {
        // Calculate latest expiration for VOD.
        mRentExpiration = 0;
        for (String variantId : getVariantIds()) {
            IRentalExpiration rentalExpiration = rentalExpirationMap.get(variantId);
            if (rentalExpiration != null) {
                mRentExpiration = Math.max(mRentExpiration, rentalExpiration.getExpireTime());
            }
        }

        // Dispatch rental expiration for variant.
        IRentalExpiration rentalExpiration = rentalExpirationMap.get(mMainVariant.getId());
        if (rentalExpiration != null) {
            mMainVariant.setRentalExpirationInfo(rentalExpiration);
        }
    }
}
