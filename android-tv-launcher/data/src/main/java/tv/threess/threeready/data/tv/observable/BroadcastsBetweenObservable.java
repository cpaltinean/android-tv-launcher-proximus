/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.tv.observable;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.ObservableEmitter;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.EpgFetchObservable;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.tv.TvProxy;
import tv.threess.threeready.data.tv.model.NoInfoBroadcast;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * It is a base class for getting broadcasts between times, with an id for channel, example @{@link UpcomingBroadcastsObservable}
 * or @{@link EpgBroadcastsObservable}
 *
 * @author Hunor Madaras
 * @since 2019.05.07
 */

public abstract class BroadcastsBetweenObservable<TValue> extends BaseContentObservable<TValue> implements EpgFetchObservable<TValue> {
    private static final String TAG = Log.tag(BroadcastsBetweenObservable.class);

    protected final TvCache mTvCache = Components.get(TvCache.class);
    protected final TvProxy mTvProxy = Components.get(TvProxy.class);

    protected final String mChannelId;
    protected volatile long mFrom;
    protected volatile long mTo;
    protected String mDummyLoadingProgramTitle;
    protected String mDummyNoInfoProgramTitle;
    protected boolean mIsAdultChannel;
    private Disposable mProgramDisposable;

    public BroadcastsBetweenObservable(Context context, String channelId, long from, long to, boolean isAdultChannel) {
        super(context);
        if (from > to) {
            throw new IllegalArgumentException("From (" + from + ") cannot be bigger or equal than to (" + to + ")");
        }
        if (TextUtils.isEmpty(channelId)) {
            throw new IllegalArgumentException("Channel ID list cannot be empty!");
        }

        mChannelId = channelId;
        mFrom = from;
        mTo = to;

        Translator translator = Components.get(Translator.class);
        mDummyLoadingProgramTitle = translator.get(TranslationKey.MODULE_CARD_LOADING);
        mDummyNoInfoProgramTitle = translator.get(TranslationKey.EPG_NO_INFO);
        mIsAdultChannel = isAdultChannel;
    }

    @Override
    protected void onChange(final Uri uri, ObservableEmitter<TValue> emitter) {
        List<IBroadcast> broadcasts = getBroadcastsListFromDatabase();
        boolean hasMissingBroadcast = hasMissingBroadcast(broadcasts);

        Log.d(TAG, "onChange(). Uri : " + uri + ", hasMissingBroadcast : " + hasMissingBroadcast);
        if (TvContract.Broadcast.CONTENT_URI.equals(uri) && hasMissingBroadcast) {
            downloadAndPublishPrograms();
            return;
        }

        // publish the broadcasts from database or from backend
        publishPrograms(emitter, getPublishValue(broadcasts), true);
    }

    @Override
    public void subscribe(ObservableEmitter<TValue> emitter) throws Exception {
        super.subscribe(emitter);
        registerObserver(TvContract.Broadcast.CONTENT_URI);
        registerObserver(TvContract.buildBroadcastUriForChannel(mChannelId));

        // First return data available in local database, or 'loading ...' broadcasts
        List<IBroadcast> broadcasts = getBroadcastsListFromDatabase();
        publishPrograms(emitter, getPublishValue(broadcasts), false);
        downloadAndPublishPrograms();
    }

    /**
     * @param broadcasts The list of broadcast for the cache.
     * @return True if there are broadcasts missing from the interval, false otherwise.
     */
    private boolean hasMissingBroadcast(List<IBroadcast> broadcasts) {
        if (broadcasts == null || broadcasts.isEmpty()) {
            return true;
        }

        IBroadcast firstBroadcast = broadcasts.get(0);
        IBroadcast lastBroadcast = broadcasts.get(broadcasts.size() - 1);
        return mFrom <= firstBroadcast.getStart() || mTo >= lastBroadcast.getEnd();
    }

    /**
     * Download the broadcast list from backend for the channel with {@code mChannelId} in the requested time range,
     * and publish them trough the emitter to the subscriber.
     */
    private void downloadAndPublishPrograms() {
        // Fetch broadcasts from the backend and emit them when they are downloaded
        try {
            publishPrograms(mEmitter, getPublishValue(getBroadcastsListFromBackend()), false);
        } catch (Exception e) {
            Log.e(TAG, "Failed to broadcast list.", e);
        }
    }

    /**
     * Publish the list of program to the subscriber.
     *
     * @param emitter    The emitter to emit the data to the subscriber.
     * @param programs   The new list of program which needs to be sent to the subscriber.
     * @param fromNotify True if the publish was triggered after a change in the database.
     */
    protected void publishPrograms(ObservableEmitter<TValue> emitter, TValue programs, boolean fromNotify) {
        if (programs != null) {
            emitter.onNext(programs);
        }
    }

    abstract List<IBroadcast> getBroadcastsListFromDatabase();

    protected List<IBroadcast> getBroadcastsListFromBackend() throws IOException {
        return new ArrayList<>(mTvProxy.getBroadcasts(mChannelId, mFrom, mTo).getBroadcasts());
    }

    /**
     * Convert the broadcast list to the editable value.
     *
     * @param broadcasts The broadcast list from the cache.
     * @return The value which can be emitted through the RX observable.
     */
    abstract TValue getPublishValue(List<IBroadcast> broadcasts);

    /**
     * Create a live of dummy programs in the give interval.
     *
     * @param fromNotify true if we want to display no info over loading
     */
    protected List<IBroadcast> createDummyProgramsForInterval(long intervalStart, long intervalEnd, boolean fromNotify) {
        List<IBroadcast> dummyPrograms = new ArrayList<>();

        long currentTime = intervalStart;
        while (currentTime < intervalEnd) {
            long nextTime = currentTime + TimeUnit.HOURS.toMillis(1);
            if (nextTime > intervalEnd) {
                nextTime = intervalEnd;
            }

            String dummyTitle = fromNotify && currentTime >= mFrom && currentTime < mTo
                    ? mDummyNoInfoProgramTitle : mDummyLoadingProgramTitle;
            dummyPrograms.add(createDummyProgram(currentTime, nextTime, dummyTitle));
            currentTime = nextTime;
        }

        return dummyPrograms;
    }

    /**
     * Create a dummy program for the given start and end.
     */
    protected IBroadcast createDummyProgram(long start, long end, String title) {
        return new NoInfoBroadcast(mChannelId, title,
                mIsAdultChannel ? ParentalRating.Rated18 : ParentalRating.Undefined,
                start, end);
    }

    /**
     * Called when the timeline changes in the EPG grid and broadcasts needs to be loaded.
     * The timeline includes the extra prefetch time for the broadcasts.
     *
     * @param from The timestamp from which the broadcast needs to be loaded.
     * @param to   The timestamp until the broadcasts needs to be loaded.
     */
    @Override
    public boolean prefetchPrograms(String channelId, long from, long to) {
        if (!channelId.equals(mChannelId)) {
            return false;
        }

        if (mFrom == from && mTo == to) {
            // No change.
            return true;
        }

        mFrom = from;
        mTo = to;

        if (RxUtils.isDisposed(mEmitter)) {
            return false;
        }

        // Fetch broadcasts.
        RxUtils.disposeSilently(mProgramDisposable);
        mProgramDisposable = Schedulers.io().scheduleDirect(() -> {
            if (RxUtils.isDisposed(mEmitter)) {
                // Already disposed.
                return;
            }

            downloadAndPublishPrograms();
        });
        return true;
    }
}
