package tv.threess.threeready.data.account.adapter;

import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.home.model.generic.BackgroundPlayerType;

/**
 * Json type adapter the read the value as background player type enum.
 *
 * @author Barabas Attila
 * @since 5/14/21
 */
public class BackgroundPlayerTypeAdapter3Ready extends NameTypeMappingTypeAdapter<BackgroundPlayerType> {
    @Override
    protected Map<String, BackgroundPlayerType> createNameTypeMap() {
        return new HashMap<String, BackgroundPlayerType>() {
            {
                put("full_player", BackgroundPlayerType.FULL_PLAYER);
                put("mini_player", BackgroundPlayerType.MINI_PLAYER);
                put("none", BackgroundPlayerType.NONE);
            }
        };
    }

    @Nullable
    @Override
    public BackgroundPlayerType getDefaultValue() {
        return BackgroundPlayerType.FULL_PLAYER;
    }
}
