/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.tv.model;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * Entity class which holds the basic advertisement information.
 */
public interface TvAdvertisement extends Serializable {

    /**
     * @return the external identifier of the advertisement.
     */
    String getCreativeId();

    /**
     * @return the name of the advertisement, usually the filename without the extension.
     */
    String getName();

    /**
     * @return the name of the broadcaster(PXS, SBS, RMB, etc.).
     */
    String getBroadcaster();

    /**
     * @return the name of the advertisement file.
     */
    String getFilename();

    /**
     * @return the size of the advertisement file.
     */
    long getSize();

    /**
     * @return the duration or length of the advertisement video clip.
     */
    long getDuration(TimeUnit unit);

    /**
     * @return the timestamp when the ad expires.
     */
    long getExpires();

    /**
     * @return true if the advertisement was on demand downloaded, false if it was pre-downloaded.
     */
    boolean isOnMarker();

    /**
     * @return how many times the ad was started to be downloaded.
     */
    int getAttemptCount();

    /**
     * @return true if the ad is expired.
     */
    default boolean isExpired(long now) {
        return now > getExpires();
    }
}
