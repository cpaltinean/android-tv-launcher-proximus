package tv.threess.threeready.data.utils;

import android.content.Context;

import com.bumptech.glide.Glide;

import tv.threess.threeready.api.log.Log;

/**
 * Contains cache related utility methods.
 *
 * Created by Szilard on 2/16/2018.
 */

public class CacheUtils {
    private static final String TAG = Log.tag(CacheUtils.class);

    public static void clearImageCache(Context context) {
        try {
            Glide.get(context).clearDiskCache();
        } catch (Exception e) {
            Log.w(TAG, "Clear image cache failed", e);
        }
    }
}
