package tv.threess.threeready.data.tv.projections;

import android.database.Cursor;
import android.text.TextUtils;

import java.util.Arrays;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.data.generic.IProjection;
import tv.threess.threeready.data.generic.helper.ImageProjectionHelper;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.data.tv.model.TvBroadcastDBModel;

/**
 * Projection to get continue_watching_broadcast with additional channel information (channel image url)
 *
 * @author Daniela Toma
 * @since 2022.07.07
 */
public enum ContinueWatchingBroadcastProjection implements IProjection {
    EVENT_ID(TvContract.ContinueWatchingBroadcast.EVENT_ID),
    CHANNEL_ID(TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.ID),
    START(TvContract.ContinueWatchingBroadcast.START),
    END(TvContract.ContinueWatchingBroadcast.END),
    TITLE(TvContract.ContinueWatchingBroadcast.TITLE),
    DESCRIPTION(TvContract.ContinueWatchingBroadcast.DESCRIPTION),
    GENRES(TvContract.ContinueWatchingBroadcast.GENRES),
    SUB_GENRES(TvContract.ContinueWatchingBroadcast.SUB_GENRES),
    RELEASE_YEAR(TvContract.ContinueWatchingBroadcast.RELEASE_YEAR),
    SEASON(TvContract.ContinueWatchingBroadcast.SEASON),
    EPISODE(TvContract.ContinueWatchingBroadcast.EPISODE),
    EPISODE_TITLE(TvContract.ContinueWatchingBroadcast.EPISODE_TITLE),
    PROGRAM_ID(TvContract.ContinueWatchingBroadcast.PROGRAM_ID),
    SERIES_ID(TvContract.ContinueWatchingBroadcast.SERIES_PROGRAM_ID),
    UNSKIPABLE_ASSET_ID(TvContract.ContinueWatchingBroadcast.UNSKIPABLE_ASSET_ID),
    ACTORS(TvContract.ContinueWatchingBroadcast.ACTORS),
    DIRECTORS(TvContract.ContinueWatchingBroadcast.DIRECTORS),
    PARENTAL_RATING(TvContract.ContinueWatchingBroadcast.PARENTAL_RATING),
    IS_BLACKLISTED(TvContract.ContinueWatchingBroadcast.IS_BLACKLISTED),
    IMAGE_URL(TvContract.ContinueWatchingBroadcast.IMAGE_URL),
    IMAGE_TYPE(TvContract.ContinueWatchingBroadcast.IMAGE_TYPE),
    HAS_DESCRIPTIVE_AUDIO(TvContract.ContinueWatchingBroadcast.HAS_DESCRIPTIVE_AUDIO),
    HAS_DESCRIPTIVE_SUBTITLE(TvContract.ContinueWatchingBroadcast.HAS_DESCRIPTIVE_SUBTITLE),
    IS_NPVR_ENABLED(TvContract.ContinueWatchingBroadcast.TABLE_NAME, TvContract.ContinueWatchingBroadcast.IS_NPVR_ENABLE);

    private static final String TAG = Log.tag(ContinueWatchingBroadcastProjection.class);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    ContinueWatchingBroadcastProjection(String column) {
        this.mColumn = column;
    }

    ContinueWatchingBroadcastProjection(String tableName, String column) {
        mColumn = tableName + "." + column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    /**
     * Read a broadcast from cursor row.
     *
     * @param cursor Cursor with program details.
     * @return Model for the specified program.
     */
    public static IBroadcast readRow(Cursor cursor) {
        return readRow(cursor, false);
    }

    /**
     * Read a broadcast from cursor row.
     *
     * @param cursor         Cursor with program details.
     * @param isPreviousLive Flag used to know if the previous program was live.
     * @return Model for the specified program.
     */
    public static IBroadcast readRow(Cursor cursor, boolean isPreviousLive) {
        return TvBroadcastDBModel.Builder.builder()
                .setId(cursor.getString(EVENT_ID.ordinal()))
                .setProgramID(cursor.getString(PROGRAM_ID.ordinal()))
                .setChannelID(cursor.getString(CHANNEL_ID.ordinal()))
                .setStart(cursor.getLong(START.ordinal()))
                .setEnd(cursor.getLong(END.ordinal()))
                .setTitle(cursor.getString(TITLE.ordinal()))
                .setDescription(cursor.getString(DESCRIPTION.ordinal()))
                .setSeason(cursor.isNull(SEASON.ordinal())
                        ? null : cursor.getInt(SEASON.ordinal()))
                .setEpisode(cursor.isNull(EPISODE.ordinal())
                        ? null : cursor.getInt(EPISODE.ordinal()))
                .setEpisodeTitle(cursor.getString(EPISODE_TITLE.ordinal()))
                .setSeriesProgramID(cursor.getString(SERIES_ID.ordinal()))
                .setUnskipableAssetID(cursor.getString(UNSKIPABLE_ASSET_ID.ordinal()))
                .setActors(Arrays.asList(TextUtils.split(cursor.getString(ACTORS.ordinal()), ",")))
                .setDirectors(Arrays.asList(TextUtils.split(cursor.getString(DIRECTORS.ordinal()), ",")))
                .setParentalRating(ParentalRating.valueOf(ParentalRating.class, cursor.getString(PARENTAL_RATING.ordinal())))
                .setReleaseYear(cursor.getString(RELEASE_YEAR.ordinal()))
                .setGenres(Arrays.asList(TextUtils.split(cursor.getString(GENRES.ordinal()), ",")))
                .setSubGenres(Arrays.asList(TextUtils.split(cursor.getString(SUB_GENRES.ordinal()), ",")))
                .setIsBlackListed(cursor.getInt(IS_BLACKLISTED.ordinal()) > 0)
                .setImageSources(ImageProjectionHelper.imagesFromCursorRow(
                        cursor, IMAGE_URL.ordinal(), IMAGE_TYPE.ordinal()))
                .setNPVREnabled(cursor.getInt(IS_NPVR_ENABLED.ordinal()) > 0)
                .setNextToLive(isPreviousLive)
                .setHasDescriptiveAudio(cursor.getInt(HAS_DESCRIPTIVE_AUDIO.ordinal()) > 0)
                .setHasDescriptiveSubtitle(cursor.getInt(HAS_DESCRIPTIVE_SUBTITLE.ordinal()) > 0)
                .build();
    }
}
