package tv.threess.threeready.data.netflix.receiver;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.api.netflix.model.NetflixDETCategory;

/**
 * Used for sending the DET request with (for the retry mechanism) or without delay.
 *
 * @author David Bondor
 * @since 6.5.2022
 */
public class NetflixDiscoveryRunnable extends BaseNetflixDETRunnable {

    private static final String DISCOVERY_CATEGORY_KEY = "%EXTRA_DISCOVERY_CATEGORY%";
    private static final String COUNT_KEY = "%EXTRA_COUNT%";
    private static final String INTERACTION_ID_KEY = "%INTERACTION_ID_KEY%";
    private static final String EXTRA_DET_CATEGORY = "category:" + DISCOVERY_CATEGORY_KEY + ",";
    private static final String EXTRA_DET_INTERACTION_ID = ",interactionID:\\\"" + INTERACTION_ID_KEY + "\\\"";
    private static final String sDiscoveryPayload = "{\"query\":\"query Discovery($groupLimit:Int!,$tileLimit:Int!){discovery(" + EXTRA_DET_CATEGORY + "display:ROW" + EXTRA_DET_INTERACTION_ID + "){groups(limit:$groupLimit){title tiles(limit:$tileLimit){... on VideoTile{title pvid deepLink(type:TV) images{tile(width:417,height:234){url width height}}}... on ProfileTile{title deepLink(type:TV) images{tile(height:234,width:234){height url width}}}}}}}\",\"variables\":{\"groupLimit\":10,\"tileLimit\":" + COUNT_KEY + "}, \"version\":\"" + EXTRA_DET_APP_VERSION + "\"}";

    // Payload used for testing.
    // We can add different values to the "overrides" section to test different scenarios.
    private static final String sTestDiscoveryPayload = "{\"query\":\"query Discovery($groupLimit:Int!,$tileLimit:Int!){discovery(" + EXTRA_DET_CATEGORY + "display:ROW" + EXTRA_DET_INTERACTION_ID + "){groups(limit:$groupLimit){title tiles(limit:$tileLimit){... on VideoTile{title pvid deepLink(type:TV) images{tile(width:417,height:234){url width height}}}... on ProfileTile{title deepLink(type:TV) images{tile(height:234,width:234){height url width}}}}}}}\",\"variables\":{\"groupLimit\":10,\"tileLimit\":" + COUNT_KEY + "}, \"version\":\"" + EXTRA_DET_APP_VERSION + "\", \"overrides\":{\"forceExpiration\":60}}";

    private final AppConfig mAppConfig = Components.get(AppConfig.class);
    private final NetflixDETCategory mCategory;

    private String mPayload;

    private int mRetryCount = 0;

    public NetflixDiscoveryRunnable(Context context, NetflixDETCategory category, String iid, int count) {
        super(context);

        mCategory = category;
        mPayload = sDiscoveryPayload
                .replace(COUNT_KEY, Integer.toString(count));

        // Set the app version
        mPayload = mPayload.replace(EXTRA_DET_APP_VERSION, mwRepository.getAppVersion());

        // Set interaction ID if it`s available.
        if (TextUtils.isEmpty(iid)) {
            mPayload = mPayload.replace(EXTRA_DET_INTERACTION_ID, "");
        } else {
            mPayload = mPayload.replace(INTERACTION_ID_KEY, iid);
        }

        // Set Category if it`s available
        if (mCategory == NetflixDETCategory.DEFAULT) {
            mPayload = mPayload.replace(EXTRA_DET_CATEGORY, "");
        } else {
            mPayload = mPayload.replace(DISCOVERY_CATEGORY_KEY, mCategory.name());
        }
    }

    @Override
    public void run() {
        Log.d(TAG, "Send Netflix discovery request with category: " + mCategory);
        super.run();
    }

    @Override
    protected String getPayload() {
        return mPayload;
    }

    @Override
    protected void addIntentExtras(Intent intent) {
        intent.putExtra(BaseNetflixDETReceiver.EXTRA_DET_GRAPHQL_PAYLOAD, mPayload);
        intent.putExtra(EXTRA_DET_QUERY_COMMAND, BaseNetflixDETReceiver.EXTRA_DET_DISCOVERY_QUERY_COMMAND);
        intent.putExtra(EXTRA_DET_REQUEST_PARTNER_ID, mAppConfig.getNetflixPartnerId());
    }

    public NetflixDETCategory getCategory() {
        return mCategory;
    }

    public void increaseRetryCount() {
        this.mRetryCount++;
    }

    public int getRetryCount() {
        return mRetryCount;
    }

    public void resetRetryCount() {
        mRetryCount = 0;
    }
}
