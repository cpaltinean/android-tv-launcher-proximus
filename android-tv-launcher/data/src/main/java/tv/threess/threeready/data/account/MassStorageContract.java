package tv.threess.threeready.data.account;

import android.net.Uri;

/**
 * Contract class to access data in Technicolor's content provider.
 *
 * @author Zsolt Bokor_
 * @since 2020.09.08
 */
interface MassStorageContract {
    /**
     * Content uri to read data from the table.
     */
    Uri CONTENT_URI = Uri.parse("content://technicolor/mass_storage");

    /**
     * Methods to read/write data in Technicolor's content provider.
     */
    String CALL_GET_DATA = "get_mass_storage_data";
    String CALL_PUT_DATA = "put_mass_storage_data";
    String CALL_DELETE_DATA = "delete_mass_storage_data";

    /**
     * Key of the mass storage.
     */
    String KEY = "_id";

    /**
     * Name of the mass storage.
     */
    String NAME = "name";

    /**
     * State of the mass storage.
     */
    String STATE = "state";
}
