package tv.threess.threeready.data.log;

import android.content.Context;

import androidx.work.Data;
import androidx.work.WorkerParameters;
import tv.threess.lib.di.Components;
import tv.threess.threeready.data.generic.BaseWorker;
import tv.threess.threeready.data.generic.JobIntent;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.generic.BaseWorkManager;

public class ReportService extends BaseWorkManager {

    public static JobIntent buildSendUsageStats() {
        return new JobIntent(SendUsageStats.class, Data.EMPTY);
    }

    public static class SendUsageStats extends BaseWorker {

        public SendUsageStats(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            Components.get(MwProxy.class).reportUsageEvents();
        }
    }
}
