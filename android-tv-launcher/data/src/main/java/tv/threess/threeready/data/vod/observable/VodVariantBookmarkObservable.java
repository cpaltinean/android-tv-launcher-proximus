package tv.threess.threeready.data.vod.observable;

import android.content.Context;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountCache;
import tv.threess.threeready.api.generic.model.BookmarkType;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.generic.observable.BaseBookmarkObservable;

/**
 * RX observable that reacts when the {@link ConfigContract.Bookmarks} content's uri is notified
 * in order to query for a VOD variant bookmark that holds its duration & lastViewedPosition
 *
 * @author Barabas Attila
 * @since 08/14/2019
 */
public class VodVariantBookmarkObservable extends BaseBookmarkObservable<IVodVariant> {

    private final AccountCache.Bookmarks mAccountCache = Components.get(AccountCache.Bookmarks.class);

    public VodVariantBookmarkObservable(Context context, IVodVariant variant) {
        super(context, variant);
    }

    @Override
    protected IBookmark getBookmark() {
        return mAccountCache.getBookmark(mContentItem.getId(), BookmarkType.VodVariant);
    }
}