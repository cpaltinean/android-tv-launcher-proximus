/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.tv.observable;

import android.content.Context;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.TvChannel;

/**
 * This class will handle the database query and fetching from server if needed for the broadcasts
 *
 * @author Boldijar Paul
 * @since 2017.04.26
 */

public class EpgBroadcastsObservable extends BroadcastsBetweenObservable<List<IBroadcast>> {
    private static final String TAG = Log.tag(EpgBroadcastsObservable.class);

    private final TvChannel mChannel;
    private final long mWindowStartLength;
    private final long mWindowEndLength;

    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);

    public EpgBroadcastsObservable(Context context, TvChannel channel, long from, long to, boolean isAdultChannel,
                                   long windowStart, long windowEnd, String dummyLoadingProgramTitle, String dummyNoInfoProgramTitle) {
        super(context, channel.getId(), from, to, isAdultChannel);

        if (from > to) {
            throw new IllegalArgumentException("From (" + from + ") cannot be bigger or equal than to (" + to + ")");
        }

        mChannel = channel;
        mWindowStartLength = windowStart;
        mWindowEndLength = windowEnd;

        mDummyLoadingProgramTitle = dummyLoadingProgramTitle;
        mDummyNoInfoProgramTitle = dummyNoInfoProgramTitle;
    }

    @Override
    protected void publishPrograms(ObservableEmitter<List<IBroadcast>> emitter, List<IBroadcast> programs, boolean fromNotify) {
        fillWithDummyPrograms(programs, fromNotify);
        super.publishPrograms(emitter, programs, fromNotify);
    }

    @Override
    List<IBroadcast> getBroadcastsListFromDatabase() {
        return mTvCache.getEpgBroadcasts(mChannelId, mFrom, mTo);
    }

    @Override
    List<IBroadcast> getPublishValue(List<IBroadcast> broadcasts) {
        return broadcasts;
    }

    private void fillWithDummyPrograms(List<IBroadcast> programs, boolean fromNotify) {
        // Add dummy loading programs
        if (mInternetChecker.isInternetAvailable()) {
            // Add dummy current program if there's no program list
            if (programs.isEmpty()) {
                IBroadcast dummyCurrent = createDummyProgram(System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(30),
                        System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(30), mDummyNoInfoProgramTitle);
                programs.add(dummyCurrent);
            }

            if (mWindowStartLength == 0 && mWindowEndLength == 0) {
                // Window not defined. Skip fill.
                return;
            }

            // Add dummy programs to the front of the list
            IBroadcast firstProgram = programs.get(0);
            long windowStart = TimeBuilder.utc()
                    .set(System.currentTimeMillis() - mWindowStartLength)
                    .ceil(TimeUnit.HOURS).get();
            long intervalStart = Math.max(System.currentTimeMillis() - mWindowStartLength, windowStart);
            programs.addAll(0, createDummyProgramsForInterval(intervalStart, firstProgram.getStart(), fromNotify));

            // Add dummy programs to the end of the list
            IBroadcast lastProgram = programs.get(programs.size() - 1);
            long windowEnd = TimeBuilder.utc()
                    .set(System.currentTimeMillis() + mWindowEndLength)
                    .ceil(TimeUnit.HOURS).get();
            long intervalEnd = Math.min(windowEnd, lastProgram.getEnd() + mWindowEndLength);
            programs.addAll(createDummyProgramsForInterval(lastProgram.getEnd(), intervalEnd, fromNotify));

            Log.d(TAG, "Program list loaded. Channel : " + mChannel.getName()
                    + " from : " + Log.timestamp(firstProgram.getStart()) + ", to : " + Log.timestamp(lastProgram.getEnd())
                    + ", WindowStart : " + Log.timestamp(windowStart) + ", WindowEnd : " + Log.timestamp(windowEnd));

            // Add dummy no info program
        } else {
            if (programs.isEmpty()) {
                IBroadcast dummyCurrent = createDummyProgram(System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(30),
                        System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(30), mDummyNoInfoProgramTitle);
                programs.add(dummyCurrent);
            }
        }
    }
}
