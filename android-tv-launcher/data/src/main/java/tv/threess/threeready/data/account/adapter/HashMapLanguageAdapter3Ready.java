package tv.threess.threeready.data.account.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * Adapter to transform a JsonObj to Map
 * Json array must have this structure :
 * {
 *   "LANGUAGE_DESCRIPTIVE_DUTCH": ["bnl"], //LANGUAGE_DESCRIPTIVE_DUTCH, LANGUAGE_DUTCH are keys used in POEditor.
 *   "LANGUAGE_DUTCH": ["nl", "nld", "du", "dut", "west-vlaams"]
 * }
 * And will convert to a map like
 * "bnl"->"LANGUAGE_DESCRIPTIVE_DUTCH",
 * "nl"->"LANGUAGE_DUTCH",
 * "nld"->"LANGUAGE_DUTCH",
 * "du"->"LANGUAGE_DUTCH",
 * "dut"->"LANGUAGE_DUTCH",
 * "west-vlaams"->"LANGUAGE_DUTCH"
 *
 * @author Daniela Toma
 * @since 2019.09.10
 */
public class HashMapLanguageAdapter3Ready extends TypeAdapter<Map<String, String>> {

    @Override
    public void write(JsonWriter out, Map<String, String> value) throws IOException {
        Map<String, List<String>> translationCodeMap = invertCodeTranslationMap(value);

        out.beginObject();
        for (Map.Entry<String, List<String>> entry : translationCodeMap.entrySet()) {
            out.name(entry.getKey());
            out.beginArray();
            for (String code : entry.getValue()) {
                out.value(code);
            }
            out.endArray();
        }
        out.endObject();
    }

    private Map<String, List<String>> invertCodeTranslationMap(Map<String, String> codeTranslationMap) {
        Map<String, List<String>> translationCodeMap = new HashMap<>();
        for (Map.Entry<String, String> entry : codeTranslationMap.entrySet()) {
            List<String> codes = translationCodeMap.get(entry.getKey());
            if (codes == null) {
                codes = new ArrayList<>();
                translationCodeMap.put(entry.getValue(), codes);
            }
            codes.add(entry.getKey());
        }
        return translationCodeMap;
    }

    @Override
    public Map<String, String> read(JsonReader reader) throws IOException {
        if (reader.peek() == JsonToken.NULL) {
            reader.nextNull();
            return null;
        }

        Map<String, String> map = new HashMap<>();

        reader.beginObject();

        while (reader.hasNext()) {
            String value = reader.nextName();
            reader.beginArray();
            while (reader.hasNext()) {
                map.put(reader.nextString(), value);
            }
            reader.endArray();
        }
        reader.endObject();
        return map;
    }
}
