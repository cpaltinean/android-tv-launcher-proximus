package tv.threess.threeready.data.account.observable;

import android.content.Context;
import android.net.Uri;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountCache;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Hot observable for watchlist status for a specific item.
 *
 * @author Andor Lukacs
 * @since 09/11/2020
 */
public class InWatchlistObservable extends BaseContentObservable<Boolean> {
    private static final String TAG = Log.tag(InWatchlistObservable.class);

    private final String mContentId;
    private Boolean mLastStatus;
    private final AccountCache.WatchList mAccountCache = Components.get(AccountCache.WatchList.class);

    public InWatchlistObservable(Context context, String contentId) {
        super(context);
        mContentId = contentId;
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<Boolean> observableEmitter) {
        publishWatchlistStatus();
    }


    @Override
    public void subscribe(ObservableEmitter<Boolean> emitter) throws Exception {
        super.subscribe(emitter);

        // Observe status changes.
        registerObserver(ConfigContract.buildUriForWatchlist(mContentId));
        publishWatchlistStatus();
    }

    private void publishWatchlistStatus() {
        if (RxUtils.isDisposed(mEmitter)) {
            // Already canceled.
            return;
        }

        Boolean currentStatus = mAccountCache.isInWatchlist(mContentId);
        if (currentStatus != null && currentStatus != mLastStatus) {
            Log.d(TAG, "Publish watchlist status for item "
                    + mContentId + " " + currentStatus);

            mLastStatus = currentStatus;
            mEmitter.onNext(currentStatus);
        }
    }
}
