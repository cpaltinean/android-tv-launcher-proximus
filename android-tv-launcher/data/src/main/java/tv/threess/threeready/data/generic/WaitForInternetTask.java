package tv.threess.threeready.data.generic;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.ConnectivityStateChangeReceiver;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.log.Log;

/**
 * Task to wait until the internet becomes available.
 *
 * @author Barabas Attila
 * @since 1/6/21
 */
public class WaitForInternetTask {
    private static final String TAG = Log.tag(WaitForInternetTask.class);

    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);

    private final FutureTask<Void> mFutureTask = new FutureTask<>(() -> {
        Log.d(TAG, "System update task finished.");
        return null;
    });

    public WaitForInternetTask() {
    }

    /**
     * @param timeoutMillis The maximum time in millis to wait for the internet connectivity.
     */
    public void waitForInternet(long timeoutMillis) throws InterruptedException, ExecutionException, TimeoutException {
        InternetChecker.InternetCheckEnabler checkEnabler = () -> true;
        mInternetChecker.addEnabler(checkEnabler);

        try {
            boolean available = mInternetChecker.checkInternetAvailability();
            if (available) {
                return;
            }

            mInternetChecker.addStateChangedListener(mOnStateChangedListener);
            mFutureTask.get(timeoutMillis, TimeUnit.MILLISECONDS);
        } finally {
            mInternetChecker.removeEnabler(checkEnabler);
            mInternetChecker.removeStateChangedListener(mOnStateChangedListener);
        }
    }

    private final ConnectivityStateChangeReceiver.OnStateChangedListener mOnStateChangedListener = state -> {
        if (state.isInternetAvailable()) {
            mFutureTask.run();
        }
    };
}
