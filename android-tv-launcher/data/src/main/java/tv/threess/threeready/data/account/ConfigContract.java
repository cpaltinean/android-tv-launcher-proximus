/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.account;

import android.content.ContentResolver;
import android.net.Uri;

import tv.threess.threeready.api.BuildConfig;
import tv.threess.threeready.api.generic.BaseContract;

/**
 * Contract class to access data in user database.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.26
 */
public class ConfigContract {
    public static final String AUTHORITY = BuildConfig.CONFIG_PROVIDER;

    private static final Uri BASE_URI = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT).authority(AUTHORITY).build();

    /**
     * Content uri for updating the module config based on ID.
     */
    public static final Uri CONTENT_URI_WITH_MODULE_ID = Uri.withAppendedPath(BASE_URI, "module_id");

    /**
     * Build uri for the bookmark of a specific content.
     */
    public static Uri buildUriForBookmark(String contentId) {
        return ConfigContract.Bookmarks.CONTENT_URI.buildUpon()
                .appendEncodedPath(Bookmarks.CONTENT_ID)
                .appendEncodedPath(contentId)
                .build();
    }

    public static Uri buildUriForWatchlist(String contentId) {
        return Watchlist.CONTENT_URI.buildUpon()
                .appendEncodedPath(Watchlist.CONTENT_ID)
                .appendEncodedPath(contentId)
                .build();
    }

    /**
     * Build uri for the given subscription swimlane based on the config ID.
     */
    public static Uri buildUriForSubscriptionModuleId(String moduleId) {
        return Uri.withAppendedPath(CONTENT_URI_WITH_MODULE_ID, moduleId);
    }

    /**
     * Contract class for shared settings.
     */
    public interface Settings {

        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Settings.class);

        String CALL_PUT_CACHE_DATA = "put_data";
        String CALL_GET_CACHE_DATA = "get_data";
        String CALL_GET_SETTINGS_DATA = "get_settings_data";
        String CALL_INC_SETTINGS_DATA = "increment_settings_data";

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(Settings.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(ConfigContract.BASE_URI, PATH);

        //
        // Columns
        //

        /**
         * Key of the setting.
         * <P>Type: String</P>
         */
        String KEY = BaseContract._ID;

        /**
         * Value for the given {@link #KEY}
         * <P>Type: String</P>
         */
        String VALUE = "value";


        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;

        String[] PROJECTION_VALUE = { VALUE };
        String SELECTION_KEY = KEY + "=?";

        /**
         * Public view name.
         */
        String PUBLIC_VIEW = "public_settings";
    }

    public static Uri buildUriForKey(Uri uri, String key) {
        return Uri.withAppendedPath(uri, key);
    }

    /**
     * Builds a URI that points the specific key from the Settings
     *
     * @param key specific field key
     * @return Uri with specific key
     */
    public static Uri buildSettingsUriForKey(String key) {
        return buildUriForKey(Settings.CONTENT_URI, key);
    }

    /**
     * Builds a URI that points the specific key from the Session
     *
     * @param key specific field key
     * @return Uri with specific key
     */
    public static Uri buildSessionUriForKey(String key) {
        return buildUriForKey(Session.CONTENT_URI, key);
    }

    /**
     * Contract class for session information.
     */
    public interface Session {

        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Session.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(Session.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(ConfigContract.BASE_URI, PATH);

        String CALL_GET_SESSION_DATA = "get_session_data";

        //
        // Columns
        //

        /**
         * Key of the setting.
         * <P>Type: String</P>
         */
        String KEY = BaseContract._ID;

        /**
         * Value for the given {@link #KEY}
         * <P>Type: String</P>
         */
        String VALUE = "value";

        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;
    }

    /**
     * Contract class for the user bookmarks
     */
    public interface Bookmarks {

        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Bookmarks.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(Bookmarks.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(ConfigContract.BASE_URI, PATH);

        //
        // Columns
        //

        /**
         * Unique identifier for the content.
         * <P>Type: Number</P>
         */
        String CONTENT_ID = "content_id";

        /**
         * Unique identifier for a channel.
         * <P>Type: String</P>
         */
        String TYPE = "type";

        /**
         * Program start time UTC.
         * <P>Type: Number</P>
         */
        String POSITION = "position";

        /**
         * Program duration time..
         * <P>Type: Number</P>
         */
        String DURATION = "duration";

        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;
    }

    /**
     * Contract class for user watchlist
     */
    public interface Watchlist {
        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Watchlist.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(Watchlist.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(ConfigContract.BASE_URI, PATH);

        //
        // Columns
        //

        /**
         * Unique identifier for the watchlist item.
         * <P>Type: String</P>
         */
        String WATCHLIST_ID = "watchlist_id";

        /**
         * Unique identifier for the content.
         * <P>Type: Number</P>
         */
        String CONTENT_ID = "content_id";

        /**
         * Unique identifier for a channel.
         * <P>Type: String</P>
         */
        String TYPE = "type";

        /**
         * The time when the watchlist item was added.
         * <P>Type: Number</>
         */
        String ADDED_AT = "added_at";

        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;
    }

    @Deprecated
    public interface Hints {
        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Hints.class);
    }

    public interface Packages {
        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Packages.class);

        /**
         * Path used in content uri
         */
        String PATH = TABLE_NAME;

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(ConfigContract.BASE_URI, PATH);

        /**
         * Package id.
         * <P>Type: String</P>
         */
        String PACKAGE_ID = "package_id";

        /**
         * External package id.
         * <P>Type: String</P>
         */
        String EXTERNAL_ID = "external_id";

        /**
         * Package start date.
         * <P>Type: Long</P>
         */
        String PACKAGE_START = "package_start_date";

        /**
         * Package end date.
         * <P>Type: Long</P>
         */
        String PACKAGE_END = "package_end_date";

        /**
         * Package is locked.
         * <P>Type: Boolean</P>
         */
        String IS_LOCKED = "is_locked";

        /**
         * Package types - delimited with ","
         * <P>Type: String</P>
         */
        String PACKAGE_TYPES = "package_types";

        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;
    }

    public interface PosterServers {
        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(PosterServers.class);

        /**
         * Path used in content uri
         */
        String PATH = TABLE_NAME;

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(ConfigContract.BASE_URI, PATH);

        /**
         * Server name.
         * <P>Type: String</P>
         */
        String NAME = "name";

        /**
         * Allowed image width.
         * <P>Type: Float</P>
         */
        String WIDTH = "width";

        /**
         * Allowed image height.
         * <P>Type: Float</P>
         */
        String HEIGHT = "height";

        /**
         * Allowed image url.
         * <P>Type: String</P>
         */
        String URL = "url";

        /**
         * True if server allows resizable images.
         * <P>Type: Integer</P>
         */
        String IS_RESIZABLE = "is_resizable";

        /**
         * @see BaseContract#LAST_UPDATED
         */
        String LAST_UPDATED = BaseContract.LAST_UPDATED;

    }
}
