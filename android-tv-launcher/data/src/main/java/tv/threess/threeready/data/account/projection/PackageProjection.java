package tv.threess.threeready.data.account.projection;

import android.database.Cursor;

import tv.threess.threeready.api.account.model.account.IPackage;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.account.model.PackageDBModel;
import tv.threess.threeready.data.generic.IProjection;

/**
 * Projection to get package related information.
 *
 * Created by Noemi Dali on 27.03.2020.
 */
public enum PackageProjection implements IProjection {
    PACKAGE_ID(ConfigContract.Packages.TABLE_NAME, ConfigContract.Packages.PACKAGE_ID),
    EXTERNAL_ID(ConfigContract.Packages.TABLE_NAME, ConfigContract.Packages.EXTERNAL_ID),
    PACKAGE_START_DATE(ConfigContract.Packages.TABLE_NAME, ConfigContract.Packages.PACKAGE_START),
    PACKAGE_END_DATE(ConfigContract.Packages.TABLE_NAME, ConfigContract.Packages.PACKAGE_END),
    PACKAGE_IS_LOCKED(ConfigContract.Packages.TABLE_NAME, ConfigContract.Packages.IS_LOCKED),
    PACKAGE_TYPES(ConfigContract.Packages.TABLE_NAME, ConfigContract.Packages.PACKAGE_TYPES);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    PackageProjection(String tableName, String column) {
        mColumn = tableName + "." + column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    public static IPackage readRow(Cursor cursor) {
        return new PackageDBModel.Builder()
                .setPackageId(cursor.getString(PACKAGE_ID.ordinal()))
                .setExternalId(cursor.getString(EXTERNAL_ID.ordinal()))
                .setPackageStartDate(cursor.getLong(PACKAGE_START_DATE.ordinal()))
                .setPackageEndDate(cursor.getLong(PACKAGE_END_DATE.ordinal()))
                .setIsLocked(cursor.getInt(PACKAGE_IS_LOCKED.ordinal()) > 0)
                .setPackageType(cursor.getString(PACKAGE_TYPES.ordinal()))
                .build();
    }
}
