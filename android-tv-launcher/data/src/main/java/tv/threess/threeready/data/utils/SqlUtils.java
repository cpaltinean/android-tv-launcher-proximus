/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.utils;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.helper.TimerLog;
import tv.threess.threeready.api.log.Log;

/**
 * Utility functions.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.14
 */
public class SqlUtils {
    private static final String TAG = Log.tag(SqlUtils.class);

    public static final int TRUE = 1;
    public static final int FALSE = 0;

    /**
     * Build an SQL parameter binding string for where clauses.
     * the result should look something like: `title IN(?,?,?)`
     *
     * @param column    column name
     * @param operation one of: IN, NOT IN, VALUES, ...
     * @param count     number of binding variables
     * @return the string representing the binding.
     */
    public static String buildBinding(String column, String operation, int count) {
        StringBuilder where = new StringBuilder(column);
        where.append(' ').append(operation).append('(');
        for (int i = 0; i < count; i += 1) {
            if (i > 0) {
                where.append(',');
            }
            where.append('?');
        }
        where.append(')');
        return where.toString();
    }

    /**
     * Build an SQL string for where clauses.
     * the result should look something like: `_id IN(1,2,3)`
     * for string values use parameters: `title IN(?,?,?)`
     *
     * @param column    column name
     * @param operation one of: IN, NOT IN, VALUES, ...
     * @return the string representing the binding.
     */
    public static String buildSelection(String column, String operation, long... ids) {
        StringBuilder where = new StringBuilder(column);
        where.append(' ').append(operation).append('(');
        for (int i = 0; i < ids.length; i += 1) {
            if (i > 0) {
                where.append(',');
            }
            where.append(ids[i]);
        }
        where.append(')');
        return where.toString();
    }

    public static int bulkInsert(ContentResolver cr, Uri uri, Collection<ContentValues> values) {
        TimerLog log = new TimerLog(TAG + "/bulkInsert");
        int inserted = cr.bulkInsert(uri, values.toArray(new ContentValues[values.size()]));
        log.d("Inserted[" + inserted + "] rows: " + uri);
        return inserted;
    }

    public static int bulkReplace(ContentResolver cr, Uri uri, ContentValues... values) {
        TimerLog log = new TimerLog(TAG + "/bulkUpdate");
        int replaced = cr.bulkInsert(BaseContract.bulkReplace(uri), values);
        log.v("Updated[" + replaced + "] rows: " + uri);
        return replaced;
    }

    public static int bulkReplace(ContentResolver cr, Uri uri, Collection<ContentValues> values) {
        return bulkReplace(cr, uri, values.toArray(new ContentValues[values.size()]));
    }

    public static int bulkReplace(ContentResolver cr, Uri uri, long timestamp, ContentValues... values) {
        TimerLog log = new TimerLog(TAG + "/bulkReplace");
        int replaced = cr.bulkInsert(BaseContract.bulkReplace(uri, timestamp), values);
        log.v("Replaced[" + replaced + "] rows: " + uri);
        return replaced;
    }

    public static int bulkReplace(ContentResolver cr, Uri uri, long timestamp, Collection<ContentValues> values) {
        return bulkReplace(cr, uri, timestamp, values.toArray(new ContentValues[values.size()]));
    }

    public static long queryLong(Context context, Uri uri, String column, String selection, String[] selectionArgs, String sortOrder) {
        try (Cursor cursor = context.getContentResolver().query(uri, new String[]{column}, selection, selectionArgs, sortOrder)) {
            if (cursor != null) {
                cursor.moveToFirst();
                return cursor.getLong(0);
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to handle the query.", e);
        }

        return 0;
    }

    public static <E> List<E> readAll(Cursor cursor, Function<Cursor, E> readRow) {
        ArrayList<E> result = new ArrayList<>();
        if (cursor == null || !cursor.moveToFirst()) {
            return result;
        }

        do {
            result.add(readRow.apply(cursor));
        } while (cursor.moveToNext());
        return result;
    }

    public static <E> List<E> queryAll(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder, Function<Cursor, E> readRow) {
        try (Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, sortOrder)) {
            return readAll(cursor, readRow);
        }
    }

    public static <E> List<E> queryAll(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, Function<Cursor, E> readRow) {
        return queryAll(context, uri, projection, selection, selectionArgs, null, readRow);
    }

    public static <E> List<E> queryAll(Context context, Uri uri, String[] projection, String selection, Function<Cursor, E> readRow) {
        return queryAll(context, uri, projection, selection, null, null, readRow);
    }

    public static <E> List<E> queryAll(Context context, Uri uri, String[] projection, Function<Cursor, E> readRow) {
        return queryAll(context, uri, projection, null, null, null, readRow);
    }

    @Nullable
    public static <E> E queryFirst(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder, Function<Cursor, E> readRow) {
        try (Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, sortOrder)) {
            if (cursor != null && cursor.moveToFirst()) {
                return readRow.apply(cursor);
            }
            return null;
        }
    }

    @Nullable
    public static <E> E queryFirst(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, Function<Cursor, E> readRow) {
        return queryFirst(context, uri, projection, selection, selectionArgs, null, readRow);
    }

    @Nullable
    public static <E> E queryFirst(Context context, Uri uri, String[] projection, Function<Cursor, E> readRow) {
        return queryFirst(context, uri, projection, null, null, null, readRow);
    }

    @Nullable
    public static <E> E queryFirst(Context context, Uri uri, Function<Cursor, E> readRow) {
        return queryFirst(context, uri, null, null, null, null, readRow);
    }

    @Nullable
    public static <E> E queryLast(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder, Function<Cursor, E> readRow) {
        try (Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, sortOrder)) {
            if (cursor != null && cursor.moveToLast()) {
                return readRow.apply(cursor);
            }
            return null;
        }
    }

    @Nullable
    public static <E> E queryLast(Context context, Uri uri, Function<Cursor, E> readRow) {
        return queryLast(context, uri, null, null, null, null, readRow);
    }

    public static void forEach(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, Consumer<Cursor> readRow) {
        try (Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null)) {
            if (cursor == null || !cursor.moveToFirst()) {
                return;
            }

            do {
                readRow.accept(cursor);
            } while (cursor.moveToNext());
        }
    }

    public static void forEach(Context context, Uri uri, Consumer<Cursor> readRow) {
        forEach(context, uri, null, null, null, readRow);
    }
}
