package tv.threess.threeready.data.home.model.generic;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import tv.threess.threeready.api.home.model.generic.ChannelApp;

/**
 * POJO class which stores the information for app channels. E.g. netflix/nina.
 *
 * @author Zsolt Bokor_
 * @since 2020.07.09
 */
public class ChannelApp3Ready implements Serializable, ChannelApp {
    @SerializedName("call_letter")
    private String mCallLetter;

    @SerializedName("package_name")
    private String mPackageName;

    @SerializedName("subscription_package")
    private String mSubscriptionPackage;

    @SerializedName("tvo_link")
    private String mTvoLink;

    @Override
    public String getCallLetter() {
        return mCallLetter;
    }

    @Override
    public String getPackageName() {
        return mPackageName;
    }

    @Override
    public String getSubscriptionPackage() {
        return mSubscriptionPackage;
    }

    @Override
    public String getTvoLink() {
        return mTvoLink;
    }

    @Override
    public boolean isSubscriptionNeeded() {
        return !TextUtils.isEmpty(mSubscriptionPackage);
    }
}
