package tv.threess.threeready.data.gms;

import android.app.Application;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.gms.GmsRepository;
import tv.threess.threeready.api.gms.model.InstalledAppInfo;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.api.notification.NotificationPriority;
import tv.threess.threeready.api.notification.model.NotificationItem;
import tv.threess.threeready.data.gms.observable.AppsObservable;
import tv.threess.threeready.data.gms.observable.SystemNotificationsObservable;
import tv.threess.threeready.data.gms.observable.UpdatedNotificationsObservable;

/**
 * Implementation for the GMS Repository.
 *
 * @author Barabas Attila
 * @since 2018.04.25
 */
public class GmsRepositoryImpl implements GmsRepository {

    private final GmsProxy mGmsProxy = Components.get(GmsProxy.class);
    private final GmsCache mGmsCache = Components.get(GmsCache.class);
    private final Application mApp;

    public GmsRepositoryImpl(Application app) {
        mApp = app;
    }

    @Override
    public Observable<ModuleData<InstalledAppInfo>> getApps(final ModuleConfig config) {
        return Observable.create(new AppsObservable(mApp) {
            @Override
            public ModuleData<InstalledAppInfo> onLoadApps() {
                return new ModuleData<>(config, mGmsProxy.getApps(config.getDataSource().getParams()));
            }
        });
    }

    @Override
    public Observable<ModuleData<NotificationItem>> getSystemNotifications(final ModuleConfig config) {
        if (config.getDataSource() == null || config.getDataSource().getParams() == null) {
            return Observable.create(new SystemNotificationsObservable(mApp))
                    .map(notificationItems -> new ModuleData<>(config, notificationItems));
        }

        NotificationPriority priority = NotificationPriority.fromString(
                config.getDataSource().getParams().getFilter(ModuleFilterOption.Notification.Priority.NAME));
        return Observable.create(new SystemNotificationsObservable(mApp, priority))
                .map(notificationItems -> new ModuleData<>(config, notificationItems));
    }

    @Override
    public Observable<List<NotificationItem>> getSystemNotifications(NotificationPriority priority) {
        return Observable.create(new SystemNotificationsObservable(mApp, priority));
    }

    @Override
    public Observable<List<NotificationItem>> getUpdatedNotifications() {
        return Observable.create(new UpdatedNotificationsObservable(mApp));
    }

    @Override
    public Completable saveNotifications(List<NotificationItem> notificationsList) {
        return Completable.fromAction(() -> mGmsCache.saveNotifications(notificationsList));
    }

    @Override
    public Completable updateAppLastOpenedTime(final String packageName, final long lastOpenedTime) {
        return Completable.fromAction(() -> mGmsCache.updateAppLastOpenedTime(packageName, lastOpenedTime));
    }
}
