package tv.threess.threeready.data.netflix.model;

import com.google.gson.annotations.SerializedName;

import tv.threess.threeready.api.generic.model.IImageSource;

/**
 * Configuration for a netflix image tile response.
 *
 * @author David
 * @since 11.03.2022
 */
public class TileImages {

    @SerializedName("hero")
    TileImage mHero;

    @SerializedName("promotion")
    TileImage mPromotion;

    @SerializedName("tile")
    TileImage mTile;

    public TileImage getHero() {
        return mHero;
    }

    public TileImage getPromotion() {
        return mPromotion;
    }

    public TileImage getTile() {
        return mTile;
    }
}

class TileImage implements IImageSource {

    @SerializedName("height")
    int mHeight;

    @SerializedName("width")
    int mWidth;

    @SerializedName("url")
    String mUrl;

    @Override
    public int getHeight() {
        return mHeight;
    }

    @Override
    public int getWidth() {
        return mWidth;
    }

    @Override
    public Type getType() {
        return Type.URL;
    }

    @Override
    public String getUrl() {
        return mUrl;
    }
}
