/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.home.model.generic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.home.model.generic.Translation;

/**
 * The proximus backend response for the translations call.
 *
 * @author Paul
 * @since 2017.10.19
 */

public class TranslationsResponse3Ready extends ArrayList<Translation3Ready>  {

    private final Map<String, String> mTranslationMap = new HashMap<>();

    public Map<String, String> getTranslations() {
        if (mTranslationMap.isEmpty()) {
            for (Translation translation : this) {
                mTranslationMap.put(
                        translation.getTerm().toLowerCase(), translation.getDefinition());
            }
        }

        return mTranslationMap;
    }
}
