package tv.threess.threeready.data.account.model;

import tv.threess.threeready.api.generic.model.IWatchlistItem;
import tv.threess.threeready.api.generic.model.WatchlistType;

/**
 * Model for watchlist items in the database
 *
 * @author Andor Lukacs
 * @since 04/11/2020
 */
public class WatchlistItemDBModel implements IWatchlistItem {

    private String mId;

    private String mContentId;

    private WatchlistType mType;

    private long nAddedAt;

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public String getContentId() {
        return mContentId;
    }

    @Override
    public WatchlistType getType() {
        return mType;
    }

    @Override
    public long getTimestamp() {
        return nAddedAt;
    }

    @Override
    public String toString() {
        return "WatchlistItemDBModel{" +
                "mContentId='" + mContentId + '\'' +
                ", mType=" + mType +
                ", mAddedAt=" + nAddedAt +
                '}';
    }

    public static class Builder {
        WatchlistItemDBModel mWatchlistItem = new WatchlistItemDBModel();

        public static WatchlistItemDBModel.Builder builder() {
            return new WatchlistItemDBModel.Builder();
        }

        public WatchlistItemDBModel.Builder setId(String id) {
            mWatchlistItem.mId = id;
            return this;
        }

        public WatchlistItemDBModel.Builder setContentId(String contentId) {
            mWatchlistItem.mContentId = contentId;
            return this;
        }

        public WatchlistItemDBModel.Builder setType(WatchlistType type) {
            mWatchlistItem.mType = type;
            return this;
        }

        public WatchlistItemDBModel.Builder setAddedAt(long lastUpdatedDate) {
            mWatchlistItem.nAddedAt = lastUpdatedDate;
            return this;
        }

        public IWatchlistItem build() {
            return mWatchlistItem;
        }
    }
}
