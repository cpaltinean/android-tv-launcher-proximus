package tv.threess.threeready.data.pvr.observable;

import android.content.Context;
import android.net.Uri;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.pvr.PvrContract;
import tv.threess.threeready.api.pvr.model.IBaseRecordingSeries;
import tv.threess.threeready.api.pvr.model.SeriesRecordingStatus;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.pvr.PvrCache;

/**
 * RX observable which returns the recording status of a series.
 * Registers content observables and emits the new status when it changes.
 *
 * @author Barabas Attila
 * @since 2018.12.05
 */
public class SeriesRecordingStatusObservable extends BaseContentObservable<SeriesRecordingStatus> {
    private static final String TAG = Log.tag(SeriesRecordingStatusObservable.class);

    private final IBaseRecordingSeries mSeries;
    private SeriesRecordingStatus mLastStatus;

    private final PvrCache mPvrCache = Components.get(PvrCache.class);

    public SeriesRecordingStatusObservable(Context context, IBaseRecordingSeries series) {
        super(context);
        mSeries = series;
    }

    @Override
    public void subscribe(ObservableEmitter<SeriesRecordingStatus> emitter) throws Exception {
        super.subscribe(emitter);

        // Observe status changes.
        registerObserver(false,
                PvrContract.buildRecordingUriForSeries(mSeries.getId()));

        publishRecordingStatus();
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<SeriesRecordingStatus> emitter) {
        publishRecordingStatus();
    }

    private void publishRecordingStatus() {
        if (RxUtils.isDisposed(mEmitter)) {
            // Already canceled.
            return;
        }

        SeriesRecordingStatus currentStatus = mPvrCache.getSeriesStatusById(mSeries.getId());
        if (currentStatus != mLastStatus) {
            Log.d(TAG, "Publish recording status for series "
                    + mSeries.getId()  + " " + currentStatus);

            mLastStatus = currentStatus;
            mEmitter.onNext(currentStatus);
        }
    }
}
