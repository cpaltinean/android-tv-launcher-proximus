package tv.threess.threeready.data.pvr.observable;

import android.content.Context;
import android.net.Uri;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.pvr.PvrContract;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.pvr.PvrCache;

/**
 * Rx observable to return recordings for a series.
 * Emits new data every time when recording status of the series or the episode changes.
 *
 * @author Barabas Attila
 * @since 2018.12.13
 */
public class SeriesRecordingObservable extends BaseContentObservable<IRecordingSeries> {
    private static final String TAG = Log.tag(SeriesRecordingObservable.class);

    private final PvrCache mPvrCache = Components.get(PvrCache.class);

    private final String mSeriesId;

    public SeriesRecordingObservable(Context context, String seriesId) {
        super(context);
        mSeriesId = seriesId;
    }

    @Override
    public void subscribe(ObservableEmitter<IRecordingSeries> series) throws Exception {
        super.subscribe(series);

        // Observe status changes.
        registerObserver(true,
                PvrContract.buildRecordingUriForSeries(mSeriesId));

        publishSeriesRecording(mSeriesId);
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<IRecordingSeries> observableEmitter) {
        publishSeriesRecording(mSeriesId);
    }

    private void publishSeriesRecording(String seriesId) {
        if (RxUtils.isDisposed(mEmitter)) {
            // Already canceled.
            return;
        }

        Log.d(TAG, "Load series recording : " + seriesId);
        IRecordingSeries seriesRecording = mPvrCache.getSeriesRecording(seriesId);
        mEmitter.onNext(seriesRecording);
    }
}
