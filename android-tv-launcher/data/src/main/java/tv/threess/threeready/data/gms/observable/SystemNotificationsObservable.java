package tv.threess.threeready.data.gms.observable;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.ConnectivityStateChangeReceiver;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.notification.NotificationPriority;
import tv.threess.threeready.api.notification.model.NotificationItem;
import tv.threess.threeready.api.notification.model.NotificationType;
import tv.threess.threeready.data.gms.projection.SystemNotificationProjection;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Observe on android tv notifications data changes.
 *
 * @author Eugen Guzyk
 * @since 2018.08.23
 */
public class SystemNotificationsObservable extends BaseNotificationObservable implements ConnectivityStateChangeReceiver.OnStateChangedListener {
    public static final String TAG = Log.tag(SystemNotificationsObservable.class);

    private final NotificationPriority mPriority;
    private final Translator mTranslator = Components.get(Translator.class);
    private final InternetChecker mInternetChecker = Components.get(InternetChecker.class);

    public SystemNotificationsObservable(Context context) {
        this(context, null);
    }

    public SystemNotificationsObservable(Context context, NotificationPriority priority) {
        super(context);
        mPriority = priority;
    }

    @Override
    public void subscribe(ObservableEmitter<List<NotificationItem>> emitter) throws Exception {
        super.subscribe(emitter);
        mInternetChecker.addStateChangedListener(this);
    }

    @Override
    protected void unregisterObserver() {
        mInternetChecker.removeStateChangedListener(this);
        super.unregisterObserver();
    }

    @Override
    protected void resolveData(ObservableEmitter<List<NotificationItem>> emitter) {
        List<NotificationItem> notifications = appNotifications();
        try {
            if (mCursor != null && mCursor.moveToFirst()) {
                notifications.addAll(applyFilter(SystemNotificationProjection.readAll(mCursor)));
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to get system notifications.", e);
        }
        emitter.onNext(notifications);
    }

    private List<NotificationItem> applyFilter(List<NotificationItem> notificationList) {
        if (mPriority == null) {
            return notificationList;
        }

        List<NotificationItem> filteredNotifications = new ArrayList<>();
        for (NotificationItem item : notificationList) {
            if (item.getImportance() >= mPriority.ordinal()) {
                filteredNotifications.add(item);
            }
        }
        return filteredNotifications;
    }

    private List<NotificationItem> appNotifications() {
        List<NotificationItem> notifications = new ArrayList<>();
        if (mInternetChecker.isBackendIsolated()) {
            notifications.add(new NotificationItem.Builder()
                    .setNotifTitle(mTranslator.get(TranslationKey.NOTIFICATION_BE_UNAVAILABLE_TITLE))
                    .setNotifText(mTranslator.get(TranslationKey.NOTIFICATION_BE_UNAVAILABLE_BODY))
                    .setHasContentIntent(1)
                    .setContentButtonLabel(mTranslator.get(TranslationKey.NOTIFICATION_BUTTON_MORE_INFO))
                    .setType(NotificationType.APP_NOTIFICATION)
                    .setTag(TAG)
                    .build()
            );
        }
        return notifications;
    }

    @Override
    public void onStateChanged(State state) {
        sWorkerHandler.post(() -> mContentObserver.onChange(false));
    }
}
