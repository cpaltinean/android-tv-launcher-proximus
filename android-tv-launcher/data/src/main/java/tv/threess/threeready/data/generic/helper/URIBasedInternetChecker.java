/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.generic.helper;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;

import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

import java.net.UnknownHostException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.exception.MissingBaseUrlException;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.generic.receivers.NetworkChangeReceiver;
import tv.threess.threeready.api.generic.receivers.StandByStateChangeReceiver;
import tv.threess.threeready.api.home.model.generic.OfflineModeSettings;
import tv.threess.threeready.api.log.Log;

/**
 * Class for checking the internet in the application.
 * This is to be used in any location where we need internet status based on URI.
 *
 * @author Albert Antal
 * @since 2018.08.06
 */
public abstract class URIBasedInternetChecker extends InternetChecker {
    private static final String TAG = Log.tag(URIBasedInternetChecker.class);

    private static final String HANDLER_THREAD_NAME = "Internet Checker";

    private final Handler mHandler;
    private static final Random random = new Random();

    private volatile long mLastCheckTime;
    private volatile boolean mCheckInProgress = false;
    private volatile boolean mCableConnected = true;

    private volatile OnStateChangedListener.State mState = new OnStateChangedListener.State.Builder()
            .internetAvailable(true, true)
            .backendAvailable(null, null)
            .build();

    private final NetworkChangeReceiver mConnectivityChangeReceiver = Components.get(NetworkChangeReceiver.class);
    private final StandByStateChangeReceiver mStandByStateChangeReceiver = Components.get(StandByStateChangeReceiver.class);

    public URIBasedInternetChecker(Context context) {
        super(context);

        // Initialize the internet check handler thread and the handler for inserting in the database.
        HandlerThread handlerThread = new HandlerThread(HANDLER_THREAD_NAME);
        handlerThread.start();
        mHandler = new Handler(handlerThread.getLooper());

        triggerInternetCheck();
    }

     /**
      * Check the backend connectivity and return the status.
      * @return Null if the backend is available the exception for the error when not available.
     */
     @Nullable
     @WorkerThread
     protected abstract Exception checkBackendAvailability();

    /**
     * Checks the given error should trigger the internet checker or not.
     */
    protected abstract boolean shouldTriggerInternetCheck(Exception error);

    @Override
    public void triggerInternetCheck() {
        cancelInternetCheck();
        postInternetCheck(false);
    }

    /**
     * @return The timestamp when the internet availability was last verified.
     */
    @Override
    public long getLastCheckedTime() {
        return mLastCheckTime;
    }

    @Override
    public boolean isCableConnected() {
        return mCableConnected;
    }

    /**
     * @return The current state of the internet availability.
     */
    @Override
    public boolean isInternetAvailable() {
        return mState.isInternetAvailable();
    }

    @Override
    public boolean isBackendAvailable() {
        return mState.isBackendAvailable();
    }

    /**
     * Called when a request successfully connected to backend.
     */
    @Override
    public void onRequestSuccessful() {
        if (!isInternetAvailable()) {
            Log.d(TAG, "onRequestSuccessful, trigger internet check immediately.");
            postInternetCheck(false);
        }
    }

    /**
     * Called when a request failed to connect to the backend.
     */
    @Override
    public void onRequestFailed(Exception error) {
        if (!shouldTriggerInternetCheck(error)) {
            Log.d(TAG, "RequestFailed, no need to check internet", error);
            return;
        }

        if (mCheckInProgress || !isBackendAvailable()) {
            Log.d(TAG, "RequestFailed, internet check in progress[" + mCheckInProgress
                    + "], internet[" + isInternetAvailable() + "], backend[" + isBackendAvailable() + "]", error);
            return;
        }

        Log.d(TAG, "RequestFailed, we need to start the internet check", error);
        postInternetCheck(false);
    }

    /**
     * Cancels any ongoing internet checks.
     */
    private void cancelInternetCheck() {
        mHandler.removeCallbacks(mInternetCheckRunnable);
        mCheckInProgress = false;
    }

    /**
     * Trigger internet check immediately or with a configurable delay.
     */
    private void postInternetCheck(boolean delayed) {
        mHandler.removeCallbacks(mInternetCheckRunnable);
        if (!delayed) {
            mHandler.post(mInternetCheckRunnable);
            return;
        }

        OfflineModeSettings settings = Components.get(OfflineModeSettings.class);
        long timeout = settings.getInternetCheckPeriod(TimeUnit.MILLISECONDS);

        // Longer retry period when the backend can be resolved but not accessible.
        if (isInternetAvailable() && !isBackendAvailable() &&
                !(mState.getBackendError() instanceof UnknownHostException)) {
            long min = settings.getBackendCheckPeriodMin(TimeUnit.MILLISECONDS);
            long max = settings.getBackendCheckPeriodMax(TimeUnit.MILLISECONDS);
            if (min > max) {
                long t = min;
                min = max;
                max = t;
            }
            if (min < max) {
                timeout = min + Math.abs(random.nextLong()) % (max - min);
            } else {
                timeout = min;
            }
        }

        Log.d(TAG, "next internet check is delayed with: " + Log.formatDuration(timeout));
        mHandler.postDelayed(mInternetCheckRunnable, timeout);
    }

    @Override
    public void registerReceiver() {
        super.registerReceiver();
        if (mConnectivityChangeReceiver != null) {
            mConnectivityChangeReceiver.addConnectivityChangeListener(mConnectivityChangeListener);
        }
    }

    @Override
    public void unRegisterReceiver() {
        super.unRegisterReceiver();
        if (mConnectivityChangeReceiver != null) {
            mConnectivityChangeReceiver.removeConnectivityChangeListener(mConnectivityChangeListener);
        }
    }

    private final Runnable mInternetCheckRunnable = () -> {
        mCheckInProgress = true;
        try {
            boolean internetCheckEnabled =  isInternetCheckEnabled();
            Log.d(TAG, "Check internet. Enabled : " + internetCheckEnabled);
            if (!internetCheckEnabled) {
                return;
            }

            boolean internetAvailable = checkInternetAvailability();
            Exception backendError = checkBackendAvailability();

            // Boot not finished yet. Keep the old state.
            if (backendError instanceof MissingBaseUrlException) {
                backendError = mState.getBackendError();
            }

            OnStateChangedListener.State state = new OnStateChangedListener.State.Builder()
                    .cableConnected(mCableConnected)
                    .backendAvailable(backendError, mState.getBackendError())
                    .internetAvailable(internetAvailable, mState.isInternetAvailable())
                    .build();

            mState = state;
            mLastCheckTime = System.currentTimeMillis();

            super.onCheckFinished(state);
        } finally {
            if (isInternetAvailable() && isBackendAvailable()) {
                // The internet is available.. Cancel any ongoing check.
                cancelInternetCheck();
            } else {
                // Offline. Reschedule check after the configured period.
                postInternetCheck(true);
            }
            mCheckInProgress = false;
        }
    };

    private final NetworkChangeReceiver.NetworkChangeListener mConnectivityChangeListener = (connected) -> {
        mCableConnected = connected;
        Log.d(TAG, "Connectivity state changed. Internet check in progress : " + mCheckInProgress);
        if (!mCheckInProgress) {
            postInternetCheck(false);
        }
    };
}
