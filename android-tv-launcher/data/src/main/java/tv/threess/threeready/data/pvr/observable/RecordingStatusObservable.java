/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.pvr.observable;

import android.content.Context;
import android.net.Uri;

import java.util.concurrent.TimeUnit;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.pvr.PvrContract;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.RecordingStatus;
import tv.threess.threeready.api.pvr.model.SeriesRecordingStatus;
import tv.threess.threeready.api.pvr.model.SingleRecordingStatus;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.pvr.PvrCache;

/**
 * RX observable which returns the recording status of a broadcast.
 * Registers content observables and emits the new status when it changes.
 *
 * @author Barabas Attila
 * @since 2018.10.23
 */
public class RecordingStatusObservable extends BaseContentObservable<RecordingStatus> {
    private static final String TAG = Log.tag(RecordingStatusObservable.class);

    private final IBroadcast mBroadcast;
    private RecordingStatus mLastStatus;

    private final PvrCache mPvrCache = Components.get(PvrCache.class);

    public RecordingStatusObservable(Context context, IBroadcast broadcast) {
        super(context);
        mBroadcast = broadcast;
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<RecordingStatus> emitter) {
        publishRecordingStatus();
    }

    @Override
    public void subscribe(ObservableEmitter<RecordingStatus> emitter) throws Exception {
        super.subscribe(emitter);

        // Observe status changes.
        registerObserver(PvrContract.buildRecordingUriForBroadcast(mBroadcast));

        publishRecordingStatus();
    }

    private void publishRecordingStatus() {
        if (RxUtils.isDisposed(mEmitter)) {
            // Already canceled.
            return;
        }

        RecordingStatus currentStatus = getRecordingStatus(mBroadcast);

        if (!currentStatus.equals(mLastStatus)) {
            Log.d(TAG, "Publish recording status for airing "
                    + mBroadcast.getId() + " " + currentStatus);

            mLastStatus = currentStatus;
            mEmitter.onNext(currentStatus);

            if (currentStatus == RecordingStatus.DEFAULT) {
                return;
            }

            IRecording recording = currentStatus.getRecording();

            if (recording != null) {
                if (recording.isLiveRecording()) {
                    rescheduleRecordingStatusUpdate(recording.getRecordingEnd() - System.currentTimeMillis());
                    return;
                }

                if (recording.isFuture()) {
                    rescheduleRecordingStatusUpdate(mBroadcast.getStart() - System.currentTimeMillis());
                }
            }
        }
    }

    private RecordingStatus getRecordingStatus(IBroadcast broadcast) {
        SingleRecordingStatus singleRecordingStatus = SingleRecordingStatus.NONE;

        // Single recording status.
        IRecording recordingItem = mPvrCache.getRecording(broadcast.getId());
        if (recordingItem != null) {
            singleRecordingStatus = recordingItem.getRecordedStatus();
        }

        // Series recording status.
        SeriesRecordingStatus seriesRecordingStatus = SeriesRecordingStatus.NONE;
        if (broadcast.isEpisode()) {
            seriesRecordingStatus = mPvrCache.getSeriesStatusById(broadcast.getSeriesId());
        }

        return new RecordingStatus(recordingItem, singleRecordingStatus, seriesRecordingStatus);
    }

    private void rescheduleRecordingStatusUpdate(long delay) {
        Log.d(TAG, "Update recording status after." + TimeUnit.MILLISECONDS.toMinutes(delay) + " min.");

        if (delay > 0 && !mEmitter.isDisposed()) {
            sWorkerHandler.removeCallbacks(mRecordingStatRunnable);
            sWorkerHandler.postDelayed(mRecordingStatRunnable, delay);
        }
    }

    private final Runnable mRecordingStatRunnable = this::publishRecordingStatus;

    @Override
    protected void unregisterObserver() {
        super.unregisterObserver();
        sWorkerHandler.removeCallbacks(mRecordingStatRunnable);
    }
}

