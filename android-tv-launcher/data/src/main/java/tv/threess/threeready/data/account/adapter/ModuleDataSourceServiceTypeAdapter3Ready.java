package tv.threess.threeready.data.account.adapter;

import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.home.model.module.ModuleDataSourceService;


/**
 * JSON type adapter to read the module data source service as enum.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public class ModuleDataSourceServiceTypeAdapter3Ready extends NameTypeMappingTypeAdapter<ModuleDataSourceService> {
    @Override
    protected Map<String, ModuleDataSourceService> createNameTypeMap() {
        return new HashMap<String, ModuleDataSourceService>() {
            {
                put("Tv", ModuleDataSourceService.TV);
                put("Vod", ModuleDataSourceService.VOD);
                put("Pvr", ModuleDataSourceService.PVR);
                put("AndroidGms", ModuleDataSourceService.GMS);
                put("Netflix", ModuleDataSourceService.NETFLIX);
                put("Editorial", ModuleDataSourceService.EDITORIAL);
            }
        };
    }

    @Nullable
    @Override
    public ModuleDataSourceService getDefaultValue() {
        return ModuleDataSourceService.UNDEFINED;
    }
}
