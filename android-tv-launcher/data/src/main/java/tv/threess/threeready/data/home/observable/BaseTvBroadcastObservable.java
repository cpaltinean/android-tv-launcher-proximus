package tv.threess.threeready.data.home.observable;

import android.content.Context;
import android.net.Uri;

import io.reactivex.ObservableEmitter;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.tv.TvContract;

/**
 * Base observable for TV Broadcast content that is coming from the {@link tv.threess.threeready.data.tv.TvProvider}.
 * This class handles filtering added in the module config.
 *
 * @author Andor Lukacs
 * @since 12/17/18
 */
public abstract class BaseTvBroadcastObservable<T> extends BaseContentObservable<T> {
    public final String TAG = Log.tag(getClass());

    protected ModuleConfig mModuleConfig;

    protected int mCount;

    public BaseTvBroadcastObservable(Context context, ModuleConfig moduleConfig, int count) {
        super(context);
        mModuleConfig = moduleConfig;
        mCount = count;
    }

    @Override
    public void subscribe(ObservableEmitter<T> emitter) throws Exception {
        super.subscribe(emitter);

        // Listen to broadcast table changes
        registerObserver(TvContract.Broadcast.CONTENT_URI);

        publishBroadcasts(emitter);
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<T> observableEmitter) {
        // Publish new broadcast when the tabled changed.
        publishBroadcasts(observableEmitter);
    }

    protected abstract void publishBroadcasts(ObservableEmitter<T> emitter);
}
