package tv.threess.threeready.data.config.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.config.model.PlayerConfig;

/**
 * Player related config options.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public class PlayerConfig3Ready implements Serializable, PlayerConfig {

    @SerializedName("seek_to_end_seconds")
    private float mSeekToEndSeconds;

    @SerializedName("start_over_appearing_delay_seconds")
    private int mStartOverAppearingDelaySeconds;

    @SerializedName("max_playlist_extension_retries")
    private int mMaxPlaylistExtensionRetries;

    @SerializedName("playlist_update_retry_margin_seconds")
    private int mPlaylistUpdateRetryMarginSeconds;

    @Override
    public long getSeekToEndDuration(TimeUnit unit) {
        return unit.convert((long) mSeekToEndSeconds, TimeUnit.SECONDS);
    }

    @Override
    public long getStartOverAppearingDelay(TimeUnit unit) {
        return unit.convert(mStartOverAppearingDelaySeconds, TimeUnit.SECONDS);
    }

    @Override
    public int getMaxPlaylistExtensionRetries() {
        return mMaxPlaylistExtensionRetries;
    }

    @Override
    public long getPlaylistUpdateRetryMargin(TimeUnit unit) {
        return unit.convert(mPlaylistUpdateRetryMarginSeconds, TimeUnit.SECONDS);
    }
}
