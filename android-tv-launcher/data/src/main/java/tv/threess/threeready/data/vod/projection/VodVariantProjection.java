package tv.threess.threeready.data.vod.projection;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.List;

import tv.threess.threeready.api.generic.model.VideoQuality;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.data.vod.VodContract;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.data.generic.IProjection;
import tv.threess.threeready.data.generic.helper.ImageProjectionHelper;
import tv.threess.threeready.data.vod.model.VodVariantDbModel;

/**
 * Projection to read vod variant detail from database.
 *
 * @author Barabas Attila
 * @since 2020.05.15
 */
public enum VodVariantProjection implements IProjection {
    VARIANT_ID(VodContract.VodVariant.VARIANT_ID),
    SERIES_ID(VodContract.VodVariant.SERIES_ID),
    GROUP_TITLE(VodContract.VodVariant.GROUP_TITLE),
    GROUP_ID(VodContract.VodVariant.GROUP_ID),
    ITEM_TITLE(VodContract.VodVariant.ITEM_TITLE),
    DESCRIPTION(VodContract.VodVariant.DESCRIPTION),
    GENRES(VodContract.VodVariant.GENRES),
    RELEASE_YEAR(VodContract.VodVariant.RELEASE_YEAR),
    DURATION(VodContract.VodVariant.DURATION),
    SEASON(VodContract.VodVariant.SEASON),
    EPISODE(VodContract.VodVariant.EPISODE),
    EPISODE_TITLE(VodContract.VodVariant.EPISODE_TITLE),
    ACTORS(VodContract.VodVariant.ACTORS),
    DIRECTORS(VodContract.VodVariant.DIRECTORS),
    PARENTAL_RATING(VodContract.VodVariant.PARENTAL_RATING),
    IMAGE_URL(VodContract.VodVariant.IMAGE_URL),
    IMAGE_TYPE(VodContract.VodVariant.IMAGE_TYPE),
    HAS_TRAILER(VodContract.VodVariant.HAS_TRAILER),
    AUDIO_LANGUAGES(VodContract.VodVariant.AUDIO_LANGUAGES),
    SUBTITLE_LANGUAGES(VodContract.VodVariant.SUBTITLE_LANGUAGES),
    IS_NEW(VodContract.VodVariant.IS_NEW),
    IS_TVOD(VodContract.VodVariant.IS_TVOD),
    IS_SVOD(VodContract.VodVariant.IS_SVOD),
    IS_SUBSCRIBED(VodContract.VodVariant.IS_SUBSCRIBED),
    IS_RENTABLE(VodContract.VodVariant.IS_RENTABLE),
    VIDEO_QUALITY(VodContract.VodVariant.VIDEO_QUALITY),
    LICENCE_PERIOD_END(VodContract.VodVariant.LICENCE_PERIOD_END),
    IS_FREE(VodContract.VodVariant.IS_FREE),
    SUPER_GENRE(VodContract.VodVariant.SUPER_GENRE),
    INTERNAL_ID(VodContract.VodVariant.INTERNAL_ID),
    IS_HD(VodContract.VodVariant.IS_HD),
    HAS_DESCRIPTIVE_AUDIO(VodContract.VodVariant.HAS_DESCRIPTIVE_AUDIO),
    HAS_DESCRIPTIVE_SUBTITLE(VodContract.VodVariant.HAS_DESCRIPTIVE_SUBTITLE);

    private static final String TAG = Log.tag(VodVariantProjection.class);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    VodVariantProjection(String column) {
        this.mColumn = column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    /**
     * Create content values for database injection from the VOD variants.
     * @param variantList The variants to create content values for.
     * @return The content values for the variants.
     */
    public static ContentValues[] toContentValues(List<IVodVariant> variantList) {
        long now = System.currentTimeMillis();
        ContentValues[] contentValues = new ContentValues[variantList.size()];

        for (int i = 0, variantListSize = variantList.size(); i < variantListSize; i++) {
            IVodVariant variant = variantList.get(i);
            ContentValues values = new ContentValues();
            values.put(VodContract.VodVariant.VARIANT_ID, variant.getId());
            values.put(VodContract.VodVariant.SERIES_ID, variant.getSeriesId());
            values.put(VodContract.VodVariant.ITEM_TITLE, variant.getItemTitle());
            values.put(VodContract.VodVariant.GROUP_TITLE, variant.getGroupTitle());
            values.put(VodContract.VodVariant.DESCRIPTION, variant.getDescription());
            values.put(VodContract.VodVariant.GENRES, TextUtils.join(",", variant.getGenres()));
            values.put(VodContract.VodVariant.RELEASE_YEAR, variant.getReleaseYear());
            values.put(VodContract.VodVariant.DURATION, variant.getDuration());
            values.put(VodContract.VodVariant.SEASON, variant.getSeasonNumber());
            values.put(VodContract.VodVariant.EPISODE, variant.getEpisodeNumber());
            values.put(VodContract.VodVariant.EPISODE_TITLE, variant.getEpisodeTitle());
            values.put(VodContract.VodVariant.ACTORS, TextUtils.join(",", variant.getActors()));
            values.put(VodContract.VodVariant.DIRECTORS, TextUtils.join(",", variant.getDirectors()));
            values.put(VodContract.VodVariant.PARENTAL_RATING, variant.getParentalRating().name());

            ImageProjectionHelper.intoContentValues(variant.getImageSources(), values,
                    VodContract.VodVariant.IMAGE_URL, VodContract.VodVariant.IMAGE_TYPE);

            values.put(VodContract.VodVariant.HAS_TRAILER, variant.hasTrailer());
            values.put(VodContract.VodVariant.AUDIO_LANGUAGES, TextUtils.join(",", variant.getAudioLanguages()));
            values.put(VodContract.VodVariant.SUBTITLE_LANGUAGES, TextUtils.join(",", variant.getSubtitleLanguages()));
            values.put(VodContract.VodVariant.IS_NEW, variant.isNew());
            values.put(VodContract.VodVariant.IS_TVOD, variant.isTVod());
            values.put(VodContract.VodVariant.IS_SVOD, variant.isSVod());
            values.put(VodContract.VodVariant.IS_SUBSCRIBED, variant.isSubscribed());
            values.put(VodContract.VodVariant.IS_RENTABLE, variant.isRentable());
            values.put(VodContract.VodVariant.VIDEO_QUALITY, variant.getHighestQuality().name());
            values.put(VodContract.VodVariant.LICENCE_PERIOD_END, variant.getLicensePeriodEnd());
            values.put(VodContract.VodVariant.IS_FREE, variant.isFree());
            values.put(VodContract.VodVariant.SUPER_GENRE, variant.getSuperGenre());
            values.put(VodContract.VodVariant.INTERNAL_ID, variant.getInternalId());
            values.put(VodContract.VodVariant.IS_HD, variant.isHD());
            values.put(VodContract.VodVariant.LAST_UPDATED, now);
            values.put(VodContract.VodVariant.HAS_DESCRIPTIVE_AUDIO, variant.hasDescriptiveAudio());
            values.put(VodContract.VodVariant.HAS_DESCRIPTIVE_SUBTITLE, variant.hasDescriptiveSubtitle());

            contentValues[i] = values;
        }

        Log.d(TAG, "variant cv size = " + contentValues.length);
        return contentValues;
    }

    /**
     * Read a VOD variant from cursor row.
     *
     * @param cursor Cursor with vod variant details.
     * @return Model for the specified VOD variant.
     */
    public static VodVariantDbModel readRow(Cursor cursor) {
        return VodVariantDbModel.Builder.builder()
                .id(cursor.getString(VARIANT_ID.ordinal()))
                .seriesId(cursor.isNull(SERIES_ID.ordinal()) ? null : cursor.getString(SERIES_ID.ordinal()))
                .itemTitle(cursor.getString(ITEM_TITLE.ordinal()))
                .groupTitle(cursor.getString(GROUP_TITLE.ordinal()))
                .groupId(cursor.getString(GROUP_ID.ordinal()))
                .description(cursor.getString(DESCRIPTION.ordinal()))
                .genres(Arrays.asList(TextUtils.split(cursor.getString(GENRES.ordinal()), ",")))
                .releaseYear(cursor.getString(RELEASE_YEAR.ordinal()))
                .duration(cursor.getLong(DURATION.ordinal()))
                .seasonNumber(cursor.isNull(SEASON.ordinal()) ? null : cursor.getInt(SEASON.ordinal()))
                .episodeNumber(cursor.isNull(EPISODE.ordinal()) ? null : cursor.getInt(EPISODE.ordinal()))
                .episodeTitle(cursor.getString(EPISODE_TITLE.ordinal()))
                .actors(Arrays.asList(TextUtils.split(cursor.getString(ACTORS.ordinal()), ",")))
                .directors(Arrays.asList(TextUtils.split(cursor.getString(DIRECTORS.ordinal()), ",")))
                .parentalRating(ParentalRating.valueOf(cursor.getString(PARENTAL_RATING.ordinal())))
                .imageSources(ImageProjectionHelper.imagesFromCursorRow(cursor, IMAGE_URL.ordinal(), IMAGE_TYPE.ordinal()))
                .hasTrailer(cursor.getInt(HAS_TRAILER.ordinal()) > 0)
                .audioLanguages(Arrays.asList(TextUtils.split(cursor.getString(AUDIO_LANGUAGES.ordinal()), ",")))
                .subtitleLanguages(Arrays.asList(TextUtils.split(cursor.getString(SUBTITLE_LANGUAGES.ordinal()), ",")))
                .isTVod(cursor.getInt(IS_TVOD.ordinal()) > 0)
                .isSVod(cursor.getInt(IS_SVOD.ordinal()) > 0)
                .isSubscribed(cursor.getInt(IS_SUBSCRIBED.ordinal()) > 0)
                .isRentable(cursor.getInt(IS_RENTABLE.ordinal()) > 0)
                .quality(VideoQuality.valueOf(cursor.getString(VIDEO_QUALITY.ordinal())))
                .licencePeriodEnd(cursor.getInt(LICENCE_PERIOD_END.ordinal()))
                .isFree(cursor.getInt(IS_FREE.ordinal()) > 0)
                .superGenre(cursor.getString(SUPER_GENRE.ordinal()))
                .internalId(cursor.getInt(INTERNAL_ID.ordinal()))
                .isHd(cursor.getInt(IS_HD.ordinal()) > 0)
                .hasDescriptiveAudio(cursor.getInt(HAS_DESCRIPTIVE_AUDIO.ordinal()) > 0)
                .hasDescriptiveSubtitles(cursor.getInt(HAS_DESCRIPTIVE_SUBTITLE.ordinal()) > 0)
                .build();
    }

}
