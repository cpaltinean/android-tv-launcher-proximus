/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.home.model.generic;

import com.google.gson.annotations.SerializedName;

import tv.threess.threeready.api.home.model.generic.Translation;

/**
 * Response entity from backend.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.05.02
 */
public class Translation3Ready implements Translation {

    @SerializedName("term")
    private String mTerm;

    @SerializedName("definition")
    private String mDefinition;

    @Override
    public String getTerm() {
        return mTerm;
    }

    @Override
    public String getDefinition() {
        return mDefinition == null ? "" : mDefinition;
    }
}
