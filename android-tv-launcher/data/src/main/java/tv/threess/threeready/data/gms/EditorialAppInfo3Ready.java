package tv.threess.threeready.data.gms;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.gms.model.EditorialAppInfo;

/**
 * 3Ready specific implementation of an editorial app info.
 *
 * @author Barabas Attila
 * @since 6/25/21
 */
public class EditorialAppInfo3Ready implements EditorialAppInfo {

    @SerializedName("name")
    private String mName;

    @SerializedName("package")
    private String mPackage;

    @SerializedName("banner")
    private String mBanner;

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public String getPackageName() {
        return mPackage;
    }

    @Nullable
    @Override
    public IImageSource getBanner() {
        return mImageSource;
    }

    private final IImageSource mImageSource = new IImageSource() {
        @Override
        public String getUrl() {
            return mBanner;
        }

        @Override
        public Type getType() {
            return Type.APP;
        }
    };
}
