package tv.threess.threeready.data.generic;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.os.SystemClock;
import androidx.work.Data;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.log.Log;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

/**
 * Base class for the {@link IntentService}
 * all the common service action should be handled here
 *
 * @author Barabas Attila
 * @since 2018.02.14
 */
public abstract class BaseWorkManager {
    private static final String TAG = Log.tag(BaseWorkManager.class);

    public static final String KEY_INTENT_EXTRA_RECEIVER = "intent.extra.LOCAL_BROADCAST_RESULT_RECEIVER";

    public static final String KEY_INTENT_EXTRA_ERROR = "intent.extra.LOCAL_BROADCAST_RESULT_ERROR";

    public static void start(Context context, JobIntent job) {
        WorkManager manager = WorkManager.getInstance(context);
        long period = job.getSchedulePeriod();
        if (period <= 0) {
            Log.d(TAG, "Enqueuing OneTimeWorkRequest with action: " + job.getWorkName());
            OneTimeWorkRequest request = new OneTimeWorkRequest.Builder(job.getWorker())
                    .setInputData(job.getExtras())
                    .build();
            manager.enqueueUniqueWork(job.getWorkName(), ExistingWorkPolicy.REPLACE, request);
            return;
        }

        Log.d(TAG, "Enqueuing PeriodicWorkRequest with action: " + job.getWorkName() + ", period: " + Log.formatDuration(period));
        PeriodicWorkRequest request = new PeriodicWorkRequest.Builder(job.getWorker(), period, TimeUnit.MILLISECONDS)
                .setInputData(job.getExtras())
                .build();
        manager.enqueueUniquePeriodicWork(job.getWorkName(), ExistingPeriodicWorkPolicy.REPLACE, request);
    }

    /**
     * Schedule an alarm which triggers the intent at the given time.
     * @param context The context of the application.
     * @param job The intent which needs to be sent.
     * @param when The time when the intent needs to be sent.
     */
    public static void startAt(Context context, JobIntent job, boolean uniqueWork, long when) {
        OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(job.getWorker())
                .setInitialDelay(Duration.ofMillis(when - System.currentTimeMillis()))
                .setInputData(job.getExtras())
                .build();

        Log.d(TAG, "Scheduling work with action: " + job.getWorkName() + ", when: " + Log.timestamp(when));
        if (uniqueWork) {
            WorkManager.getInstance(context).enqueueUniqueWork(job.getWorkName(), ExistingWorkPolicy.KEEP, work);
        } else {
            WorkManager.getInstance(context).enqueue(work);
        }
    }

    /**
     * Schedule an alarm which wakes up the device from standby
     * and triggers the intent at the given time.
     * @param context The context of the application.
     * @param job The job which needs to be scheduled.
     * @param when The time when the intent needs to be sent.
     */
    public static void scheduleWakeupAlarm(Context context, JobIntent job, long when) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager == null) {
            Log.e(TAG, "Couldn't access the alarm manager.");
            return;
        }

        Intent intent = new Intent(job.getWorkName(), null, context, Scheduler.class);
        intent.putExtra(Scheduler.WORKER_DATA, job.getExtras().toByteArray());

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, when, pendingIntent);
        Log.d(TAG, "Schedule wakeup alarm, action: " + intent.getAction() + ", when: " + Log.timestamp(when));
    }

    /**
     * Cancel a previously scheduled alarm and the potentially running job.
     * @param context The application context.
     * @param job The job which needs to be canceled.
     */
    public static void cancelAlarm(Context context, JobIntent job) {
        WorkManager.getInstance(context).cancelUniqueWork(job.getWorkName());

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager == null) {
            Log.e(TAG, "Couldn't access the alarm manager.");
            return;
        }

        Intent intent = new Intent(job.getWorkName(), null, context, Scheduler.class);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Log.d(TAG, "Canceled alarm, action: " + intent.getAction());
        alarmManager.cancel(pendingIntent);
        pendingIntent.cancel();
    }

    /**
     * Calculate the last next time when a cache update needs to be scheduled.
     * @param lastUpdateTime The last time when the cache was update.
     * @param period The cache update period in millis.
     */
    public static long getNextUpdateScheduleTime(long lastUpdateTime, long period) {
        long scheduleTime = lastUpdateTime + period;

        // Schedule already expired. Reschedule from current time.
        if (scheduleTime < System.currentTimeMillis()) {
            scheduleTime = System.currentTimeMillis() + period;
        }

        return scheduleTime;
    }

    public static class Scheduler extends BroadcastReceiver {
        public static final String WORKER_DATA = "job.worker.data";

        @Override @SuppressWarnings("unchecked")
        public void onReceive(Context context, Intent intent) {
            try {
                Log.d(TAG, "Broadcast received at: " + Log.timestamp(System.currentTimeMillis()) +
                        ", uptime: " + Log.formatDuration(SystemClock.uptimeMillis()) +
                        ", action: " + intent.getAction());
                Class<? extends BaseWorker> worker = (Class<? extends BaseWorker>) Class.forName(intent.getAction());
                Data data = Data.fromByteArray(intent.getByteArrayExtra(WORKER_DATA));
                start(context, new JobIntent(worker, data));
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }
}
