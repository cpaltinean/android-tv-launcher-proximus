package tv.threess.threeready.data.utils;

import tv.threess.threeready.api.generic.helper.TimerLog;
import tv.threess.threeready.api.log.Log;

/**
 * Class to conditionally execute caching steps and schedule automatic background update.
 *
 * @author Barabas Attila
 * @since 4/21/22
 */
public abstract class CacheUpdater implements CacheValidator {
    private final String TAG = Log.tag(CacheUpdater.class);

    /**
     * Called when the cache update finished.
     */
    protected abstract void validateCache();

    /**
     * @return Updater to execute multiple caching steps during an update.
     */
    public BatchUpdater batchUpdate() {
        return new BatchUpdater();
    }

    /**
     * Update the cache when expired.
     * @param context The application context.
     * @param cacheStep The logic on how to update the cache.
     * @throws Exception In case the cache couldn't be updated
     * and there is no previous cache available.
     */
    public void updateIfExpired(CacheStep cacheStep) throws Exception {
        new BatchUpdater().runIfExpired(cacheStep).done();
    }

    public class BatchUpdater {
        private final TimerLog mTimerLog = new TimerLog(TAG);

        private BatchUpdater() {
        }

        /**
         * Execute the caching step only when the cache is expired.
         * @param cacheStep The logic on how to update the cache.
         * @return The batch update execute other caching logic on it.
         * @throws Exception In case the cache couldn't be updated
         * and there is no previous cache available.
         */
        public BatchUpdater runIfExpired(CacheStep cacheStep) throws Exception {
            if (!isCacheValid()) {
                run(cacheStep);
            }
            return this;
        }

        /**
         * Execute the caching step even if the current cache is still valid.
         * @param cacheStep The logic on how to update the cache.
         * @return The batch update execute other caching logic on it.
         * @throws Exception In case the cache couldn't be updated
         * and there is no previous cache available.
         */
        public BatchUpdater run(CacheStep cacheStep) throws Exception{
            try {
                cacheStep.run();
            } catch (Exception e) {
                if (hasCache()) {
                    Log.e(TAG, "Step failed in cache update for : "
                            + getName() + ". Use old cache : ", e);
                } else {
                    throw e;
                }
            }
            return this;
        }

        /**
         * Finish validating the cache.
         */
        public void done() {
            mTimerLog.d("Cache update finished for : " + getName());
            if (!isCacheValid()) {
                validateCache();
            }
        }
    }

    /**
     * Interface to provide cache update logic.
     */
    public interface CacheStep {

        void run() throws Exception;
    }

}
