package tv.threess.threeready.data.generic.observable;

import android.content.Context;
import android.net.Uri;

import io.reactivex.ObservableEmitter;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.utils.CacheValidator;

/**
 * Rx observable to wait until the cache is validated.
 *
 * @author Barabas Attila
 * @since 11/25/21
 */
public class WaitForUpdateObservable extends BaseContentObservable<Void> {
    private static final String TAG = Log.tag(WaitForUpdateObservable.class);

    private final CacheValidator[] mValidators;

    public WaitForUpdateObservable(Context context, CacheValidator... validator) {
        super(context);
        mValidators = validator;
    }

    @Override
    public void subscribe(ObservableEmitter<Void> e) throws Exception {
        super.subscribe(e);

        if (isCacheValid()) {
            e.onComplete();
        } else {
            registerObservers();
        }
    }

    private void registerObservers( ) {
        for (CacheValidator  validator : mValidators) {
            registerObserver(validator.getUpdateUri());
        }
    }

    /**
     * @return True if all cache are valid.
     */
    private boolean isCacheValid() {
        for (CacheValidator validator : mValidators) {
            boolean valid = validator.isCacheValid();
            Log.d(TAG, validator.getName() + " is valid : " + valid);
            if (!valid) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<Void> e) {
        if (isCacheValid()) {
            unregisterObserver();
            e.onComplete();
        }
    }
}
