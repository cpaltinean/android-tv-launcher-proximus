/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.home.model.generic;


import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import tv.threess.threeready.api.home.model.generic.ContentMarkerDetails;
import tv.threess.threeready.api.home.model.generic.ContentMarkers;
import tv.threess.threeready.api.home.model.module.MarkerType;

/**
 * Configuration for content markers (color, background, transparency and priority).
 *
 * @author Szende Pal
 * @since 2017.07.25
 */
public class ContentMarkers3Ready implements Serializable, ContentMarkers {
    @SerializedName("_DEFAULT")
    ContentMarkerDetails3Ready mDefault;

    @SerializedName("CONTENT_MARKER_NOW_PLAYING")
    ContentMarkerDetails3Ready mNowPlaying;

    @SerializedName("CONTENT_MARKER_NOW")
    ContentMarkerDetails3Ready mNow;

    @SerializedName("CONTENT_MARKER_LATER")
    ContentMarkerDetails3Ready mLater;

    @SerializedName("CONTENT_MARKER_NEXT")
    ContentMarkerDetails3Ready mNext;

    @SerializedName("CONTENT_MARKER_FREE")
    ContentMarkerDetails3Ready mFree;

    @SerializedName("CONTENT_MARKER_RENTED")
    ContentMarkerDetails3Ready mRented;

    @SerializedName("CONTENT_MARKER_PLANNED")
    ContentMarkerDetails3Ready mPlanned;

    @SerializedName("CONTENT_MARKER_PRICE")
    ContentMarkerDetails3Ready mPrice;

    @SerializedName("CONTENT_MARKER_RADIO")
    ContentMarkerDetails3Ready mRadio;

    @SerializedName("CONTENT_MARKER_INCLUDED")
    ContentMarkerDetails3Ready mIncluded;

    @SerializedName("CONTENT_MARKER_SUBSCRIBE")
    ContentMarkerDetails3Ready mSubscribe;

    @SerializedName("CONTENT_MARKER_DELETE_SOON")
    ContentMarkerDetails3Ready mDeleteSoon;

    @SerializedName("CONTENT_MARKER_LAST_CHANCE")
    ContentMarkerDetails3Ready mLastChance;

    @SerializedName("CONTENT_MARKER_NEW")
    ContentMarkerDetails3Ready mMarkerNew;

    @Override
    @Nullable
    public ContentMarkerDetails getContentMarkerDetails(MarkerType markerType) {
        ContentMarkerDetails3Ready markerDetails;
        switch (markerType) {
            case NOW:
                markerDetails = mNow;
                break;
            case LATER:
                markerDetails = mLater;
                break;
            case NEXT:
                markerDetails = mNext;
                break;
            case NOW_PLAYING:
                markerDetails = mNowPlaying;
                break;
            case FREE:
                markerDetails = mFree;
                break;
            case PRICE:
                markerDetails = mPrice;
                break;
            case RENTED:
                markerDetails = mRented;
                break;
            case PLANNED:
                markerDetails = mPlanned;
                break;
            case RADIO:
                markerDetails = mRadio;
                break;
            case INCLUDED:
                markerDetails = mIncluded;
                break;
            case SUBSCRIBE:
                markerDetails = mSubscribe;
                break;
            case DELETE_SOON:
                markerDetails = mDeleteSoon;
                break;
            case LAST_CHANCE:
                markerDetails = mLastChance;
                break;
            case NEW:
                markerDetails = mMarkerNew;
                break;
            case DEFAULT:
            default:
                markerDetails = mDefault;
                break;
        }

        if (markerDetails != null) {
            markerDetails.setMarkerType(markerType);
        }

        return markerDetails;
    }
}
