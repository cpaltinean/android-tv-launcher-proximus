/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.home.model.generic;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.home.model.generic.GradientStyle;
import tv.threess.threeready.data.account.adapter.ColorTypeAdapter3Ready;

/**
 * Content marker gradient color configuration.
 *
 * @author Barabas Attila
 * @since 2018.07.12
 */
public class GradientStyle3Ready implements Serializable, GradientStyle {

    @SerializedName("from")
    @JsonAdapter(ColorTypeAdapter3Ready.class)
    private Integer mFrom;

    @SerializedName("to")
    @JsonAdapter(ColorTypeAdapter3Ready.class)
    private Integer mTo;

    @SerializedName("orientation")
    private String mOrientation;

    @Override
    public Integer getFromColor() {
        return ColorUtils.getColor(mFrom, Color.BLACK);
    }

    @Override
    public Integer getToColor() {
        return ColorUtils.getColor(mTo, Color.BLACK);
    }

    @Override
    public GradientDrawable.Orientation getOrientation() {
        GradientDrawable.Orientation orientation = GradientDrawable.Orientation.TOP_BOTTOM;
        try {
            orientation = GradientDrawable.Orientation.valueOf(mOrientation);
        } catch (Exception e) {
            //no need to do something here
        }
        return orientation;
    }

    @Override
    public boolean hasBackgroundColor() {
        return mFrom != null || mTo != null;
    }

}
