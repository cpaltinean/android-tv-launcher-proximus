package tv.threess.threeready.data.config.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.config.model.PlaybackSettings;

/**
 * Class representing the playback settings from the local configuration json.
 *
 * @author Barabas Attila
 * @since 2018.02.08
 */

public class PlaybackSettings3Ready implements Serializable, PlaybackSettings {

    private static final int BUFFER_SIZE_BYTES = 20971520; // 20 MB
    private static final int MIN_PLAYBACK_START_BUFFER = 1;

    private static final int MIN_DURATION_FOR_QUALITY_INCREASE = 7;
    private static final int MAX_DURATION_FOR_QUALITY_DECREASE = 40;
    private static final int MIN_DURATION_TO_RETAIN_AFTER_DISCARD = 15;
    private static final int LOW_MEDIA_TIME_TRICKPLAY = 35;
    private static final int HIGH_MEDIA_TIME_TRICKPLAY = 35;
    private static final int LOW_MEDIA_TIME = 10;
    private static final int HIGH_MEDIA_TIME = 25;
    private static final int MAX_BITRATE_LIMITED = 4_000_000;
    private static final int BANDWIDTH_MIN = 13330;
    private static final boolean DRAIN_WHILE_CHARGING = false;

    private static final int MIN_DEGRADATION_SAMPLES = 20;

    private static final int MANIFEST_RETRY_COUNT = 3;
    private static final int SEGMENT_RETRY_COUNT = 2;
    private static final int MAXIMUM_SEGMENT_RETRY_DELAY_MS = 4000;
    private static final int MAXIMUM_MANIFEST_RETRY_DELAY_MS = 4000;

    private static final int MANIFEST_CONNECTION_TIMEOUT = 3000;
    private static final int MANIFEST_READ_TIMEOUT = 2000;

    private static final int SEGMENT_CONNECTION_TIMEOUT = 3000;
    private static final int SEGMENT_READ_TIMEOUT = 7000;

    private static final int DRM_CONNECTION_TIMEOUT = 7000;
    private static final int DRM_READ_TIMEOUT = 7000;
    private static final int DRM_ACQUISITION_TIMEOUT = 10000;
    private static final int OTT_LIVE_EDGE_LATENCY_MS = 16000;

    @SerializedName("iptv_playback_timeout_seconds")
    private int mIpTvPlaybackTimeoutSeconds;

    @SerializedName("ott_playback_timeout_seconds")
    private int mOttPlaybackTimeoutSeconds;

    @SerializedName("playback_fallback_timeout_seconds")
    private long mPlaybackFallbackTimeoutSeconds;

    @SerializedName("prime_time")
    private PrimeTime primeTime;

    @SerializedName("ott_live_edge_latency_ms")
    private Integer mOttLiveEdgeLatencyMs;

    @SerializedName("minimumPlaybackStartSeconds")
    private Integer mMinimumPlaybackStartSeconds;

    @SerializedName("bufferSize")
    private Integer mBufferSize;

    @SerializedName("lowestInitTrack")
    private Boolean mLowestInitTrack;

    @SerializedName("abrOn")
    private Boolean mAbrOn;

    @SerializedName("minDurationQualityIncrease")
    private Long mMinDurationQualityIncrease;

    @SerializedName("maxDurationQualityDecrease")
    private Long mMaxDurationQualityDecrease;

    @SerializedName("minDurationRetainAfterDiscard")
    private Long mMinDurationRetainAfterDiscard;

    @SerializedName("lowMediaTimeTrickplay")
    private Integer mLowMediaTimeTrickplay;

    @SerializedName("highMediaTimeTrickplay")
    private Integer mHighMediaTimeTrickplay;

    @SerializedName("drainWhileCharging")
    private Boolean mDrainWhileCharging;

    @SerializedName("lowMediaTime")
    private Integer mLowMediaTime;

    @SerializedName("highMediaTime")
    private Integer mHighMediaTime;

    @SerializedName("maximumLimitedBitrate")
    private Integer mMaxLimitedBitrate;

    @SerializedName("bandwidthControlMinimum")
    private Integer bandwidthControlMin;

    @SerializedName("minDegradationSamples")
    private Integer mMinDegradationSamples;

    @SerializedName("manifestRetryCount")
    private Integer mManifestRetryCount;

    @SerializedName("manifestRetryDelay")
    private Long mManifestRetryDelay;

    @SerializedName("segmentRetryCount")
    private Integer mSegmentRetryCount;

    @SerializedName("segmentRetryDelay")
    private Long mSegmentRetryDelay;

    @SerializedName("manifestConnectTimeout")
    private Integer mManifestConnectTimeout;

    @SerializedName("manifestReadTimeout")
    private Integer mManifestReadTimeout;

    @SerializedName("segmentConnectTimeout")
    private Integer mSegmentConnectTimeout;

    @SerializedName("segmentReadTimeout")
    private Integer mSegmentReadTimeout;

    @SerializedName("drmConnectionTimeout")
    private Integer mDrmConnectionTimeout;

    @SerializedName("drmReadTimeout")
    private Integer mDrmReadTimeout;

    @SerializedName("drmAcquisitionTimeout")
    private Integer mDrmAcquisitionTimeout;

    @SerializedName("playbackBufferingEventDelay")
    private long mPlaybackBufferingEventDelay;

    @SerializedName("weakSignalEventDelay")
    private long mWeakSignalEventDelay;

    @SerializedName("bingeWatchingDisplayTime")
    private long mBingeWatchingDisplayTime;

    @SerializedName("minimum_seeking_speed_seconds")
    private int mMinimumSeekingSpeedSeconds;

    @SerializedName("maximum_seeking_speed_seconds")
    private int mMaximumSeekingSpeedSeconds;

    @SerializedName("maximum_seeking_speed_rewind_seconds")
    private int mMaximumSeekingSpeedRewindSeconds;

    @SerializedName("single_press_jump_factor_percentage")
    private int mSinglePressJumpFactorPercentage;

    @SerializedName("single_skip_back_seconds")
    private int mSkipBackSeconds;

    @SerializedName("single_skip_forward_seconds")
    private int mSkipForwardSeconds;

    @SerializedName("long_press_skip_factor")
    private int mLongPressSkipFactor;

    @SerializedName("spinner_delay_seconds")
    private int mSpinnerDelay;

    @Override
    public long getIpTvPlaybackTimeout(TimeUnit unit) {
        return unit.convert(mIpTvPlaybackTimeoutSeconds, TimeUnit.SECONDS);
    }

    @Override
    public long getOttPlaybackTimeout(TimeUnit unit) {
        return unit.convert(mOttPlaybackTimeoutSeconds, TimeUnit.SECONDS);
    }

    @Override
    public long getPlaybackBufferingEventDelay(TimeUnit unit) {
        return unit.convert(mPlaybackBufferingEventDelay, TimeUnit.SECONDS);
    }

    @Override
    public long getWeakSignalEventDelay(TimeUnit unit) {
        return unit.convert(mWeakSignalEventDelay, TimeUnit.SECONDS);
    }

    @Override
    public long getBingeWatchingDisplayTime(TimeUnit unit) {
        return unit.convert(mBingeWatchingDisplayTime, TimeUnit.SECONDS);
    }

    @Override
    public int getMinimumPlaybackStartSeconds() {
        if(mMinimumPlaybackStartSeconds == null){
            return MIN_PLAYBACK_START_BUFFER;
        }
        return mMinimumPlaybackStartSeconds;
    }

    @Override
    public int getBufferSize() {
        if(mBufferSize == null){
            return BUFFER_SIZE_BYTES;
        }
        return mBufferSize;
    }

    @Override
    public boolean isLowestInitTrack() {
        if(mLowestInitTrack == null){
            return true;
        }
        return mLowestInitTrack;
    }

    @Override
    public boolean isAbrOn() {
        if(mAbrOn == null){
            return true;
        }
        return mAbrOn;
    }

    @Override
    public long getMinDurationQualityIncrease() {
        if(mMinDurationQualityIncrease == null){
            return MIN_DURATION_FOR_QUALITY_INCREASE;
        }
        return mMinDurationQualityIncrease;
    }

    @Override
    public long getMaxDurationQualityDecrease() {
        if(mMaxDurationQualityDecrease == null){
            return MAX_DURATION_FOR_QUALITY_DECREASE;
        }
        return mMaxDurationQualityDecrease;
    }

    @Override
    public long getMinDurationRetainAfterDiscard() {
        if(mMinDurationRetainAfterDiscard == null){
            return MIN_DURATION_TO_RETAIN_AFTER_DISCARD;
        }
        return mMinDurationRetainAfterDiscard;
    }

    @Override
    public int getLowMediaTimeTrickPlay() {
        if (mLowMediaTimeTrickplay == null) {
            return LOW_MEDIA_TIME_TRICKPLAY;
        }
        return mLowMediaTimeTrickplay;
    }

    @Override
    public int getHighMediaTimeTrickPlay() {
        if (mHighMediaTimeTrickplay == null) {
            return HIGH_MEDIA_TIME_TRICKPLAY;
        }
        return mHighMediaTimeTrickplay;
    }

    @Override
    public int getLowMediaTime() {
        if (mLowMediaTime == null) {
            return LOW_MEDIA_TIME;
        }
        return mLowMediaTime;
    }

    @Override
    public int getHighMediaTime() {
        if (mHighMediaTime == null) {
            return HIGH_MEDIA_TIME;
        }
        return mHighMediaTime;
    }

    @Override
    public int getMaximumLimitedBitrate() {
        if (mMaxLimitedBitrate == null) {
            return MAX_BITRATE_LIMITED;
        }
        return mMaxLimitedBitrate;
    }

    @Override
    public int getControlMinimum() {
        if (bandwidthControlMin == null) {
            return BANDWIDTH_MIN;
        }
        return bandwidthControlMin;
    }

    @Override
    public boolean isDrainWhileCharging() {
        if (mDrainWhileCharging == null) {
            return DRAIN_WHILE_CHARGING;
        }
        return mDrainWhileCharging;
    }

    @Override
    public int getMinDegradationSamples() {
        if(mMinDegradationSamples == null){
            return MIN_DEGRADATION_SAMPLES;
        }
        return mMinDegradationSamples;
    }

    @Override
    public int getManifestRetryCount() {
        if(mManifestRetryCount == null){
            return MANIFEST_RETRY_COUNT;
        }
        return mManifestRetryCount;
    }

    @Override
    public long getManifestRetryDelay() {
        if(mManifestRetryDelay == null){
            return MAXIMUM_MANIFEST_RETRY_DELAY_MS;
        }
        return mManifestRetryDelay;
    }

    @Override
    public int getSegmentRetryCount() {
        if(mSegmentRetryCount == null){
            return SEGMENT_RETRY_COUNT;
        }
        return mSegmentRetryCount;
    }

    @Override
    public long getSegmentRetryDelay() {
        if(mSegmentRetryDelay == null){
            return MAXIMUM_SEGMENT_RETRY_DELAY_MS;
        }
        return mSegmentRetryDelay;
    }

    @Override
    public int getManifestConnectTimeout() {
        if(mManifestConnectTimeout == null){
            return MANIFEST_CONNECTION_TIMEOUT;
        }
        return mManifestConnectTimeout;
    }

    @Override
    public int getManifestReadTimeout() {
        if(mManifestReadTimeout == null){
            return MANIFEST_READ_TIMEOUT;
        }
        return mManifestReadTimeout;
    }

    @Override
    public int getSegmentConnectTimeout() {
        if(mSegmentConnectTimeout == null){
            return SEGMENT_CONNECTION_TIMEOUT;
        }
        return mSegmentConnectTimeout;
    }

    @Override
    public int getSegmentReadTimeout() {
        if(mSegmentReadTimeout == null){
            return SEGMENT_READ_TIMEOUT;
        }
        return mSegmentReadTimeout;
    }

    @Override
    public int getDrmConnectionTimeout() {
        if(mDrmConnectionTimeout == null){
            return DRM_CONNECTION_TIMEOUT;
        }
        return mDrmConnectionTimeout;
    }

    @Override
    public int getDrmReadTimeout() {
        if(mDrmReadTimeout == null){
            return DRM_READ_TIMEOUT;
        }
        return mDrmReadTimeout;
    }

    @Override
    public int getDrmAcquisitionTimeout() {
        if(mDrmAcquisitionTimeout == null){
            return DRM_ACQUISITION_TIMEOUT;
        }
        return mDrmAcquisitionTimeout;
    }

    @Override
    public PrimeTime getPrimeTime() {
        return primeTime;
    }

    @Override
    public int getLiveEdgeLatencyMs() {
        if(mOttLiveEdgeLatencyMs == null){
            return OTT_LIVE_EDGE_LATENCY_MS;
        }
        return mOttLiveEdgeLatencyMs;
    }

    public static class PrimeTime implements  PlaybackSettings.PrimeTime {

        @SerializedName("hour")
        private final int hour;

        @SerializedName("minutes")
        private final int minutes;

        public PrimeTime(int hour, int minutes) {
            this.hour = hour;
            this.minutes = minutes;
        }

        public int getHour() {
            return hour;
        }

        public int getMinutes() {
            return minutes;
        }
    }

    @Override
    public long getMinimumSeekingSpeed(TimeUnit unit) {
        return unit.convert(mMinimumSeekingSpeedSeconds, TimeUnit.SECONDS);
    }

    @Override
    public long getMaximumSeekingSpeed(TimeUnit unit) {
        return unit.convert(mMaximumSeekingSpeedSeconds, TimeUnit.SECONDS);
    }

    @Override
    public long getMaximumSeekingSpeedRewind(TimeUnit unit) {
        return unit.convert(mMaximumSeekingSpeedRewindSeconds, TimeUnit.SECONDS);
    }

    @Override
    public int getSinglePressJumpFactorPercentage() {
        return mSinglePressJumpFactorPercentage;
    }

    @Override
    public long getSkipBack(TimeUnit unit) {
        return unit.convert(mSkipBackSeconds, TimeUnit.SECONDS);
    }

    @Override
    public long getSkipForward(TimeUnit unit) {
        return unit.convert(mSkipForwardSeconds, TimeUnit.SECONDS);
    }

    @Override
    public int getLongPressSkipFactor() {
        return mLongPressSkipFactor;
    }

    @Override
    public long getSpinnerDelayTimeout(TimeUnit unit) {
        return unit.convert(mSpinnerDelay, TimeUnit.SECONDS);
    }
}
