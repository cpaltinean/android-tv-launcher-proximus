package tv.threess.threeready.data.vod.projection;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.vod.VodContract;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.data.generic.IProjection;
import tv.threess.threeready.data.tv.projections.BroadcastProjection;
import tv.threess.threeready.data.vod.model.BaseVodItemDbModel;

/**
 * Database projection to read a VOD base item from database.
 *
 * @author Barabas Attila
 * @since 2020.05.15
 */
public enum VodProjection implements IProjection {
    VOD_ID(VodContract.Vod.VOD_ID),
    SERIES_ID(VodContract.Vod.SERIES_ID),
    VARIANT_IDS(VodContract.Vod.VARIANT_IDS),
    MAIN_VARIANT_ID(VodContract.Vod.MAIN_VARIANT_ID);

    private static final String TAG = Log.tag(BroadcastProjection.class);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    VodProjection(String column) {
        this.mColumn = column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    /**
     * Create content values for database insertion for the VOD items.
     * @param vodList The vod items to create content values for.
     * @return The content values for the vod items.
     */
    public static ContentValues[] toContentValues(List<IBaseVodItem> vodList) {
        long now = System.currentTimeMillis();
        ContentValues[] contentValues = new ContentValues[vodList.size()];

        for (int i = 0, vodListSize = vodList.size(); i < vodListSize; i++) {
            IBaseVodItem vod = vodList.get(i);
            ContentValues values = new ContentValues();
            values.put(VodContract.Vod.VOD_ID, vod.getId());
            values.put(VodContract.Vod.SERIES_ID, vod.getSeriesId());
            values.put(VodContract.Vod.MAIN_VARIANT_ID, vod.getMainVariant().getId());
            values.put(VodContract.Vod.VARIANT_IDS, TextUtils.join(",", vod.getVariantIds()));
            values.put(VodContract.Vod.LAST_UPDATED, now);
            contentValues[i] = values;
        }

        Log.d(TAG, "vod cv size = " + contentValues.length);
        return contentValues;
    }

    /**
     * Read a list of VOD variant from the cursor.
     * @param cursor The cursor to read from.
     * @return The list of VODs or empty list.
     */
    @NonNull
    public static List<BaseVodItemDbModel> readAll(Cursor cursor) {
        ArrayList<BaseVodItemDbModel> variants = new ArrayList<>();
        BaseVodItemDbModel vodItems;
        if (!cursor.moveToFirst()) {
            return variants;
        }

        do {
            vodItems = readRow(cursor);
            variants.add(vodItems);

        } while (cursor.moveToNext());
        return variants;
    }

    /**
     * Read a VOD variant from cursor row.
     *
     * @param cursor Cursor with vod details.
     * @return Model for the specified VOD variant.
     */
    public static BaseVodItemDbModel readRow(Cursor cursor) {
        return BaseVodItemDbModel.Builder.builder()
                .id(cursor.getString(VOD_ID.ordinal()))
                .mainVariantId(cursor.getString(MAIN_VARIANT_ID.ordinal()))
                .variantIds(Arrays.asList(TextUtils.split(
                        cursor.getString(VARIANT_IDS.ordinal()), ",")))
                .build();
    }

}
