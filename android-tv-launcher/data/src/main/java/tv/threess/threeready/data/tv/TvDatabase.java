/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.tv;

import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.tv.model.PlaybackOption;
import tv.threess.threeready.api.tv.model.StreamType;
import tv.threess.threeready.data.generic.helper.BaseSQLiteOpenHelper;
import tv.threess.threeready.data.tv.projections.PlaybackOptionProjection;
import tv.threess.threeready.data.utils.SqlUtils;

/**
 * Helper class to manage database creation and version management.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.13
 */
public class TvDatabase extends BaseSQLiteOpenHelper {

    // database file name
    private static final String NAME = "tv.db";

    // database version number
    private static final int VERSION = 58;

    public TvDatabase(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TvContract.Channel.TABLE_NAME + " ("
                + TvContract.Channel.ID + " TEXT NOT NULL UNIQUE,"
                + TvContract.Channel.INTERNAL_ID + " TEXT NOT NULL,"
                + TvContract.Channel.CALL_LETTER + " TEXT NOT NULL,"
                + TvContract.Channel.POSITION + " INTEGER NOT NULL,"
                + TvContract.Channel.NUMBER + " INTEGER,"
                + TvContract.Channel.NAME + " TEXT,"
                + TvContract.Channel.LANGUAGE + " TEXT,"
                + TvContract.Channel.IMAGE_URL + " TEXT,"
                + TvContract.Channel.TYPE + " INTEGER,"
                + TvContract.Channel.IS_BABY_CONTENT_WARNING_REQUIRED + " INTEGER NOT NULL DEFAULT 0,"
                + TvContract.Channel.IS_USER_TV_SUBSCRIBED_CHANNEL + " INTEGER NOT NULL DEFAULT 0,"
                + TvContract.Channel.IS_HD_ENABLED + " INTEGER,"
                + TvContract.Channel.IS_NPVR_ENABLED + " INTEGER,"
                + TvContract.Channel.IS_REPLAY_ENABLED + " INTEGER,"
                + TvContract.Channel.IS_LIVE_TAD_ENABLED + " INTEGER NOT NULL DEFAULT 0,"
                + TvContract.Channel.IS_TIME_SHIFT_TAD_ENABLED + " INTEGER NOT NULL DEFAULT 0,"
                + TvContract.Channel.TARGET_AD_INACTIVITY + " INTEGER NOT NULL DEFAULT 0,"
                + TvContract.Channel.REPLAY_WINDOW + " INTEGER,"
                + TvContract.Channel.LAST_UPDATED + " INTEGER NOT NULL)"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS " + TvContract.PlaybackOption.TABLE_NAME + " ("
                + TvContract.PlaybackOption.OPTION_ID + " INTEGER PRIMARY KEY NOT NULL,"
                + TvContract.PlaybackOption.CHANNEL_ID + " TEXT NOT NULL,"
                + TvContract.PlaybackOption.PLAYBACK_TYPE + " INTEGER,"
                + TvContract.PlaybackOption.PLAYBACK_ORDER + " INTEGER,"
                + TvContract.PlaybackOption.PLAYBACK_DATA + " TEXT,"
                + TvContract.PlaybackOption.PLAYBACK_EXTRA + " TEXT,"
                + TvContract.PlaybackOption.TIF_INPUT_ID + " TEXT,"
                + TvContract.PlaybackOption.TIF_CHANNEL_ID + " INTEGER,"
                + TvContract.PlaybackOption.LAST_UPDATED + " INTEGER NOT NULL)"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS " + TvContract.Broadcast.TABLE_NAME + " ("
                + TvContract.Broadcast.EVENT_ID + " TEXT PRIMARY KEY NOT NULL,"
                + TvContract.Broadcast.CHANNEL_ID + " TEXT NOT NULL,"
                + TvContract.Broadcast.START + " INTEGER,"
                + TvContract.Broadcast.END + " INTEGER,"
                + TvContract.Broadcast.TITLE + " TEXT,"
                + TvContract.Broadcast.DESCRIPTION + " TEXT,"
                + TvContract.Broadcast.GENRES + " TEXT,"
                + TvContract.Broadcast.SUB_GENRES + " TEXT,"
                + TvContract.Broadcast.RELEASE_YEAR + " TEXT,"
                + TvContract.Broadcast.SEASON + " TEXT,"
                + TvContract.Broadcast.EPISODE + " TEXT,"
                + TvContract.Broadcast.EPISODE_TITLE + " TEXT,"
                + TvContract.Broadcast.PROGRAM_ID + " TEXT,"
                + TvContract.Broadcast.SERIES_PROGRAM_ID + " TEXT,"
                + TvContract.Broadcast.UNSKIPABLE_ASSET_ID + " TEXT,"
                + TvContract.Broadcast.ACTORS + " TEXT,"
                + TvContract.Broadcast.DIRECTORS + " TEXT,"
                + TvContract.Broadcast.PARENTAL_RATING + " TEXT,"
                + TvContract.Broadcast.LAST_UPDATED + " INTEGER NOT NULL,"
                + TvContract.Broadcast.IS_BLACKLISTED + " INTEGER,"
                + TvContract.Broadcast.IMAGE_URL + " TEXT,"
                + TvContract.Broadcast.IMAGE_TYPE + " TEXT,"
                + TvContract.Broadcast.HAS_DESCRIPTIVE_AUDIO + " INTEGER,"
                + TvContract.Broadcast.HAS_DESCRIPTIVE_SUBTITLE + " INTEGER,"
                + TvContract.Broadcast.IS_NPVR_ENABLE + " INTEGER)"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS " + TvContract.ContinueWatchingBroadcast.TABLE_NAME + " ("
                + TvContract.ContinueWatchingBroadcast.EVENT_ID + " TEXT PRIMARY KEY NOT NULL,"
                + TvContract.ContinueWatchingBroadcast.CHANNEL_ID + " TEXT NOT NULL,"
                + TvContract.ContinueWatchingBroadcast.START + " INTEGER,"
                + TvContract.ContinueWatchingBroadcast.END + " INTEGER,"
                + TvContract.ContinueWatchingBroadcast.TITLE + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.DESCRIPTION + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.GENRES + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.SUB_GENRES + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.RELEASE_YEAR + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.SEASON + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.EPISODE + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.EPISODE_TITLE + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.PROGRAM_ID + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.SERIES_PROGRAM_ID + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.UNSKIPABLE_ASSET_ID + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.ACTORS + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.DIRECTORS + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.PARENTAL_RATING + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.LAST_UPDATED + " INTEGER NOT NULL,"
                + TvContract.ContinueWatchingBroadcast.IS_BLACKLISTED + " INTEGER,"
                + TvContract.ContinueWatchingBroadcast.IMAGE_URL + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.IMAGE_TYPE + " TEXT,"
                + TvContract.ContinueWatchingBroadcast.HAS_DESCRIPTIVE_AUDIO + " INTEGER,"
                + TvContract.ContinueWatchingBroadcast.HAS_DESCRIPTIVE_SUBTITLE + " INTEGER,"
                + TvContract.ContinueWatchingBroadcast.IS_NPVR_ENABLE + " INTEGER)"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS " + TvContract.Favorites.TABLE_NAME + " ("
                + TvContract.Favorites.CHANNEL_ID + " TEXT PRIMARY KEY NOT NULL, "
                + TvContract.Favorites.POSITION + " INTEGER NOT NULL, "
                + TvContract.Favorites.LAST_UPDATED + " INTEGER NOT NULL DEFAULT 0)"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS " + TvContract.Advertisement.TABLE_NAME + " ("
                + TvContract.Advertisement.CREATIVE_ID + " TEXT PRIMARY KEY NOT NULL, "
                + TvContract.Advertisement.NAME + " TEXT, "
                + TvContract.Advertisement.BROADCASTER + " TEXT, "
                + TvContract.Advertisement.FILENAME + " TEXT, "
                + TvContract.Advertisement.SIZE + " TEXT, "
                + TvContract.Advertisement.DURATION + " INTEGER NOT NULL, "
                + TvContract.Advertisement.EXPIRES + " INTEGER NOT NULL, "
                + TvContract.Advertisement.ON_MARKER + " INTEGER NOT NULL, "
                + TvContract.Advertisement.ATTEMPT_COUNT + " INTEGER NOT NULL DEFAULT 0, "
                + TvContract.Advertisement.LAST_UPDATED + " INTEGER NOT NULL DEFAULT 0)"
        );
    }

    @Override
    public boolean onUpgrade(SQLiteDatabase db, int toVersion) {
        switch (toVersion) {
            default:
                onDowngrade(db, toVersion, toVersion);
                return true;

            case 41:
            case 42:
                db.execSQL("DROP TABLE IF EXISTS " + TvContract.Channel.TABLE_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + TvContract.Broadcast.TABLE_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + TvContract.PlaybackOption.TABLE_NAME);

                clearEpgRelatedSettings();
                onCreate(db);
                return false;

            case 43:
            case 49:
                String columns = TextUtils.join(",",
                        new String[]{TvContract.Favorites.CHANNEL_ID, TvContract.Favorites.POSITION});

                //Rename
                db.execSQL("ALTER TABLE " + TvContract.Favorites.TABLE_NAME
                        + " RENAME TO " + TvContract.Favorites.TABLE_NAME + "_old;");

                //Create a new favorite table
                db.execSQL("CREATE TABLE IF NOT EXISTS " + TvContract.Favorites.TABLE_NAME + " ("
                        + TvContract.Favorites.CHANNEL_ID + " TEXT PRIMARY KEY NOT NULL, "
                        + TvContract.Favorites.POSITION + " INTEGER NOT NULL, "
                        + TvContract.Favorites.LAST_UPDATED + " INTEGER NOT NULL DEFAULT 0)");

                //Fill with data the new favorite table
                db.execSQL("INSERT INTO " + TvContract.Favorites.TABLE_NAME + "(" + columns + ") SELECT "
                        + columns + " FROM " + TvContract.Favorites.TABLE_NAME + "_old;");

                //Delete the old favorite table
                db.execSQL("DROP TABLE " + TvContract.Favorites.TABLE_NAME + "_old;");
                return false;

            case 44:
                // table `channel` changed, drop it and recreate it, and force the sync
                db.execSQL("DROP TABLE IF EXISTS " + TvContract.Channel.TABLE_NAME);
                Settings.lastChannelsSyncTime.edit().remove();
                onCreate(db);
                return false;

            case 45:
                // table `playback_option` changed, drop it and recreate it, and force the sync
                db.execSQL("DROP TABLE IF EXISTS " + TvContract.PlaybackOption.TABLE_NAME);
                Settings.lastChannelsSyncTime.edit().remove();
                onCreate(db);
                return false;

            case 46: // Channel filtering added.
            case 51: // Targeted ad enabled flags.
            case 54: // Targeted ad inactivity flag.
                db.execSQL("DROP TABLE IF EXISTS " + TvContract.Channel.TABLE_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + TvContract.PlaybackOption.TABLE_NAME);
                Settings.lastChannelsSyncTime.edit().remove();
                onCreate(db);
                return false;

            case 47:
                db.execSQL("DROP TABLE IF EXISTS " + TvContract.Broadcast.TABLE_NAME);
                Settings.lastBroadcastSyncTime.edit().remove();
                onCreate(db);
                return false;

            case 48:
                // Table `broadcast` changed, recreate and force update the broadcasts
                db.execSQL("ALTER TABLE " + TvContract.ContinueWatchingBroadcast.TABLE_NAME
                        + " ADD COLUMN " + TvContract.ContinueWatchingBroadcast.SUB_GENRES + " TEXT DEFAULT '';");
                return false;

            case 50:
                //reload the EPG
                Settings.lastBroadcastSyncTime.edit().remove();
                return false;

            case 52: // new table: advertisement
                onCreate(db);
                return false;

            case 53: // recreate table: advertisement
            case 55: // Targeted ad attempt counter
                db.execSQL("DROP TABLE IF EXISTS " + TvContract.Advertisement.TABLE_NAME);
                onCreate(db);
                return false;

            case 56:
                db.execSQL("DROP TABLE IF EXISTS " + TvContract.Broadcast.TABLE_NAME);
                db.execSQL("ALTER TABLE " + TvContract.ContinueWatchingBroadcast.TABLE_NAME
                        + " ADD COLUMN " + TvContract.ContinueWatchingBroadcast.HAS_DESCRIPTIVE_AUDIO
                        + " INTEGER");
                db.execSQL("ALTER TABLE " + TvContract.ContinueWatchingBroadcast.TABLE_NAME
                        + " ADD COLUMN " + TvContract.ContinueWatchingBroadcast.HAS_DESCRIPTIVE_SUBTITLE
                        + " INTEGER");
                clearEpgRelatedSettings();
                onCreate(db);
                return false;

            case 57:
                db.execSQL("DROP TABLE IF EXISTS " + TvContract.Broadcast.TABLE_NAME);
                db.execSQL("ALTER TABLE " + TvContract.ContinueWatchingBroadcast.TABLE_NAME
                        + " ADD COLUMN " + TvContract.ContinueWatchingBroadcast.UNSKIPABLE_ASSET_ID
                        + " TEXT");
                clearEpgRelatedSettings();
                onCreate(db);
                return false;

            case 58:
                db.execSQL("DROP TABLE IF EXISTS " + TvContract.Broadcast.TABLE_NAME);
                db.execSQL("ALTER TABLE " + TvContract.ContinueWatchingBroadcast.TABLE_NAME
                        + " ADD COLUMN " + TvContract.ContinueWatchingBroadcast.IS_NPVR_ENABLE
                        + " INTEGER DEFAULT 1");
                clearEpgRelatedSettings();
                onCreate(db);
                return false;
        }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop all tables and recreate them.
        db.execSQL("DROP TABLE IF EXISTS " + TvContract.Channel.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TvContract.PlaybackOption.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TvContract.Broadcast.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TvContract.Favorites.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TvContract.Advertisement.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TvContract.ContinueWatchingBroadcast.TABLE_NAME);
        clearEpgRelatedSettings();
        onCreate(db);
    }

    /**
     * Remove only the EPG/Channel related settings (has dependencies on the TV database)
     */
    private void clearEpgRelatedSettings() {
        Settings.batchEdit()
                .remove(Settings.lastBroadcastSyncTime)
                .remove(Settings.lastBaseBroadcastSyncTime)
                .remove(Settings.lastChannelsSyncTime)
                .persistNow();
    }

    /**
     * Return playback options
     *
     * @param onlyAvailable if true exclude unavailable playback options (e.g. if no internet excludes OTT and IPTV options)
     */
    public static List<PlaybackOption> queryPlaybackOptions(Context context, String channelId,
                                                            boolean hasInternet, boolean hasCableConnected,
                                                            boolean onlyChannels, boolean onlyAvailable) {
        String selection = TvContract.PlaybackOption.CHANNEL_ID + "=?";
        ArrayList<String> arguments = new ArrayList<>();
        arguments.add(0, channelId);

        if (onlyAvailable) {
            int count = arguments.size();
            // exclude unavailable playback options
            if (!hasCableConnected && !hasInternet) {
                arguments.add(StreamType.Ott.name());
                arguments.add(StreamType.IpTv.name());
                arguments.add(StreamType.DvbC.name());
                arguments.add(StreamType.DvbT.name());
            } else if (!hasInternet) {
                arguments.add(StreamType.Ott.name());
                arguments.add(StreamType.IpTv.name());
            } else if (!hasCableConnected) {
                arguments.add(StreamType.DvbC.name());
                arguments.add(StreamType.DvbT.name());
            }
            count = arguments.size() - count;
            String available = SqlUtils.buildBinding(TvContract.PlaybackOption.PLAYBACK_TYPE, "NOT IN", count);
            selection = DatabaseUtils.concatenateWhere(selection, available);
        }

        if (onlyChannels) {
            arguments.add(StreamType.Ott.name());
            selection = DatabaseUtils.concatenateWhere(selection, TvContract.PlaybackOption.TIF_INPUT_ID + " IS NOT NULL" +
                    " OR " + TvContract.PlaybackOption.PLAYBACK_TYPE + "=?"
            );
        } else {
            arguments.add(StreamType.Ott.name());
            arguments.add(StreamType.App.name());
            selection = DatabaseUtils.concatenateWhere(selection, TvContract.PlaybackOption.TIF_INPUT_ID + " IS NOT NULL" +
                    " OR " + TvContract.PlaybackOption.PLAYBACK_TYPE + "=?" +
                    " OR " + TvContract.PlaybackOption.PLAYBACK_TYPE + "=?"
            );
        }

        return SqlUtils.queryAll(context,
                TvContract.PlaybackOption.CONTENT_URI,
                PlaybackOptionProjection.PROJECTION,
                selection,
                arguments.toArray(new String[0]),
                TvContract.PlaybackOption.PLAYBACK_ORDER,
                PlaybackOptionProjection::readRow
        );
    }
}
