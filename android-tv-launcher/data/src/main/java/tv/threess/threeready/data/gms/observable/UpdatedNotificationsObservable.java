package tv.threess.threeready.data.gms.observable;

import android.content.Context;

import java.util.Collections;
import java.util.List;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.notification.model.NotificationItem;
import tv.threess.threeready.data.gms.GmsCache;
import tv.threess.threeready.data.gms.projection.SystemNotificationProjection;

/**
 * Observe on stored notifications data changes.
 *
 * @author Eugen Guzyk
 * @since 2018.12.19
 */

public class UpdatedNotificationsObservable extends BaseNotificationObservable {
    private static final String TAG = Log.tag(UpdatedNotificationsObservable.class);

    private final GmsCache mGmsCache = Components.get(GmsCache.class);

    public UpdatedNotificationsObservable(Context context) {
        super(context);
    }

    @Override
    protected void resolveData(ObservableEmitter<List<NotificationItem>> emitter) {

        List<NotificationItem> systemNotificationsList = loadSystemNotifications();

        if (systemNotificationsList.isEmpty()) {
            emitItems(emitter, systemNotificationsList);
            return;
        }

        List<NotificationItem> storedNotificationsList = mGmsCache.loadStoredNotifications();

        for (NotificationItem systemNotification : systemNotificationsList) {
            for (NotificationItem storedNotification : storedNotificationsList) {
                if (storedNotification.equals(systemNotification) && storedNotification.isRead()) {
                    systemNotification.setRead();
                }
            }
        }

        emitItems(emitter, systemNotificationsList);

    }

    private List<NotificationItem> loadSystemNotifications() {
        try {
            if (mCursor != null && mCursor.moveToFirst()) {
                return SystemNotificationProjection.readAll(mCursor);
            }

        } catch (Exception e) {
            Log.e(TAG, "Failed to get system notifications.", e);
        }
        return Collections.emptyList();
    }

    private void emitItems(ObservableEmitter<List<NotificationItem>> emitter, List<NotificationItem> notificationsList) {
        if (emitter != null) {
            emitter.onNext(notificationsList);
        }
    }

}
