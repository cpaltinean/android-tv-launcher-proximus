/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.tv;

import java.io.IOException;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.tv.model.ITvBroadcastResponse;
import tv.threess.threeready.api.tv.model.ITvChannelResponse;
import tv.threess.threeready.api.tv.model.TvAdReplacement;

/**
 * Proxy (pass-through) interface to access tv related backend data.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.13
 */
public interface TvProxy extends Component {

    String TAD_MOUNT_DIR = "/tad";
    String TAD_DOWNLOAD_DIR = TAD_MOUNT_DIR + "/pre";

    /**
     * @return The backend channel response, which holds the channels list and scan parameters.
     * @throws IOException in case of errors.
     */
    ITvChannelResponse<?> getChannels() throws IOException;

    /**
     * @return The currently running events on all the available channels.
     * @throws IOException in case of errors.
     */
    ITvBroadcastResponse<?> getCurrentEvents() throws IOException;

    /**
     * Get the events for the given time period on all the available channels.
     *
     * @param from The start of the time period in millis.
     * @param to   The end of the time period in millis.
     * @return The response which contains the requested events.
     * @throws IOException In case of errors.
     */
    ITvBroadcastResponse<?> getBroadcasts(long from, long to) throws IOException;

    /**
     * Get the events for the given time period on a given channel.
     *
     * @param from      The start of the time period in millis.
     * @param to        The end of the time period in millis.
     * @param channelId The unique id of the channel to request events from.
     * @return The response which contains the requested events.
     * @throws IOException In case of errors.
     */
    ITvBroadcastResponse<?> getBroadcasts(String channelId, long from, long to) throws IOException;

    /**
     * update the list of advertisements to be downloaded.
     */
    TvAdReplacement getAdReplacement(String channelId, double seconds, String segmentDesc) throws IOException;

    /**
     * execute the vast callback when the given event occur.
     */
    void trackAdReplacement(TvAdReplacement replacement, TvAdReplacement.Event event);

    /**
     * execute the vast callback when the given error occur.
     */
    void trackAdReplacement(TvAdReplacement replacement, TvAdReplacement.Error error);
}
