package tv.threess.threeready.data.netflix.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import tv.threess.threeready.api.netflix.INetflixGroup;
import tv.threess.threeready.api.netflix.INetflixTile;

/**
 * Describe the request and parameters for a netflix DET response.
 *
 * @author David
 * @since 25.02.2022
 */
public class NetflixDETQueryResponse implements Serializable {

    @SerializedName("data")
    private Data mData;

    public Data getData() {
        return mData;
    }

    public static class Data {

        @SerializedName("discovery")
        Discovery mDiscovery;

        public Discovery getDiscovery() {
            return mDiscovery;
        }
    }

    public static class Discovery {

        @SerializedName("groups")
        List<DiscoveryGroup> mGroups;

        public List<DiscoveryGroup> getGroups() {
            return mGroups;
        }
    }

    public static class DiscoveryGroup implements INetflixGroup {

        @SerializedName("title")
        String mTitle;

        @SerializedName("tiles")
        List<NetflixTile> mTiles;

        @Override
        public List<INetflixTile> getItems() {
            return (List) mTiles;
        }

        @Override
        public String getTitle() {
            return mTitle;
        }
    }
}
