/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.config.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import tv.threess.threeready.api.config.model.ApiConfig;

/**
 * API related client configuration options.
 *
 * @author Barabas Attila
 * @since 5/12/21
 */
public class ApiConfig3Ready implements Serializable, ApiConfig {

    @SerializedName("default_page_size")
    protected int mDefaultPageSize;

    public int getDefaultPageSize() {
        return mDefaultPageSize;
    }
}
