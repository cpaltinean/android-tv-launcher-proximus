package tv.threess.threeready.data.pvr.model;

import android.annotation.SuppressLint;

import java.util.List;
import java.util.Objects;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.generic.model.ISeason;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;

/**
 * Complete recording series from database.
 *
 * @author Barabas Attila
 * @since 2020.04.20
 */
public class RecordingSeriesDbModel extends BaseRecordingSeriesDbModel implements IRecordingSeries {
    private final List<ISeason<IRecording>> mSeasons;

    @SuppressLint("UseSparseArrays")
    public RecordingSeriesDbModel(List<IRecording> episodes) {
        mSeasons = groupEpisodes(episodes);
    }

    @Override
    public List<ISeason<IRecording>> getSeasons() {
        return mSeasons;
    }

    @Override
    public boolean isCompleted() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return Objects.equals(((RecordingSeriesDbModel) o).getId(), getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public List<IImageSource> getImageSources() {
        if (getFirstEpisode() != null) {
            return getFirstEpisode().getImageSources();
        }
        return null;
    }

    @Override
    public String getId() {
        if (getFirstEpisode() != null) {
            return getFirstEpisode().getSeriesId();
        }
        return null;
    }

    @Override
    public String getTitle() {
        if (getFirstEpisode() != null) {
            return getFirstEpisode().getTitle();
        }
        return null;
    }

    @Override
    public int getNumberOfSeasons() {
        return mSeasons.size();
    }

    @Override
    public String getChannelId() {
        if (getFirstEpisode() != null) {
            return getFirstEpisode().getChannelId();
        }
        return null;
    }
}
