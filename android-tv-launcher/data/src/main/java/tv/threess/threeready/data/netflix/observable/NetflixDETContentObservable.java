package tv.threess.threeready.data.netflix.observable;

import android.content.Context;
import android.net.Uri;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.netflix.INetflixGroup;
import tv.threess.threeready.api.netflix.model.NetflixDETCategory;
import tv.threess.threeready.data.home.observable.BaseCategoryContentObservable;
import tv.threess.threeready.data.netflix.NetflixDETCache;
import tv.threess.threeready.data.netflix.NetflixDETContract;
import tv.threess.threeready.data.netflix.model.NetflixDETResponse;
import tv.threess.threeready.data.netflix.receiver.NetflixDETResponseReceiver;

/**
 * Content Observable for triggering and handling the response from netflix ninja.
 *
 * @author David
 * @since 25.02.2022
 */
public class NetflixDETContentObservable extends BaseCategoryContentObservable<INetflixGroup> {

    private final static String TAG = Log.tag(NetflixDETContentObservable.class);

    private final NetflixDETResponseReceiver mReceiver = Components.get(NetflixDETResponseReceiver.class);
    private final NetflixDETCache mCache = Components.get(NetflixDETCache.class);

    private final NetflixDETCategory mCategory;
    private final String mIid;

    public NetflixDETContentObservable(Context context, ModuleConfig moduleConfig,
                                       NetflixDETCategory category, String iid, int count) {
        super(context, moduleConfig, -1, count);

        mCategory = category;
        mIid = iid;
    }

    @Override
    public void subscribe(ObservableEmitter<ModuleData<INetflixGroup>> emitter) throws Exception {
        super.subscribe(emitter);
        registerObserver(NetflixDETContract.buildForCategory(mCategory));
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<ModuleData<INetflixGroup>> emitter) {
        Log.d(TAG, "onChange: " + uri);
        loadItemList(emitter);
    }

    @Override
    protected void loadItemList(ObservableEmitter<ModuleData<INetflixGroup>> emitter) {
        Log.d(TAG, "Load Netflix DET Items.");
        NetflixDETResponse response = mCache.getNetflixStripe(mCategory);

        long now = System.currentTimeMillis();
        if (response == null || response.getMinRefreshAt() < now) {
            scheduleNetflixDETUpdate(now);

            if (mCategoryData == null) {
                // Emit an empty list until the content loads.
                onItemListLoaded(emitter, new ArrayList<>());
            }
            return;
        }

        if (isCacheExpired(response.getExpireAt())) {
            emitter.onError(new IOException("The cache is expired!"));
            return;
        }

        scheduleNetflixDETUpdate(response.getMinRefreshAt());
        onItemListLoaded(emitter, response.getGroups());
    }

    private void onItemListLoaded(ObservableEmitter<ModuleData<INetflixGroup>> emitter, List<INetflixGroup> groups) {
        mCategoryData = new ModuleData<>(mModuleConfig, groups);
        onItemListLoaded(emitter);
    }

    @Override
    protected void unregisterObserver() {
        super.unregisterObserver();
        sWorkerHandler.removeCallbacks(mNetflixDETUpdateRunnable);
    }

    private void sendNetflixDiscoveryRequest() {
        mReceiver.sendNetflixDiscoveryRequest(mContext, mCategory, mIid, mCount);
    }

    private boolean isCacheExpired(long expireAt) {
        Log.d(TAG, "Cache expires at: " + Log.timestamp(expireAt));
        return System.currentTimeMillis() >= expireAt;
    }

    /**
     * @param refreshAt the delay in millis
     */
    private void scheduleNetflixDETUpdate(long refreshAt) {
        Log.d(TAG, "Refresh at: " + Log.timestamp(refreshAt));
        if (!mEmitter.isDisposed()) {
            sWorkerHandler.removeCallbacks(mNetflixDETUpdateRunnable);
            long now = System.currentTimeMillis();
            if (refreshAt <= now) {
                sWorkerHandler.post(mNetflixDETUpdateRunnable);
            } else {
                sWorkerHandler.postDelayed(mNetflixDETUpdateRunnable, (refreshAt - now));
            }
        }
    }

    private final Runnable mNetflixDETUpdateRunnable = () -> {
        Log.d(TAG, "Update Netflix DET.");
        sendNetflixDiscoveryRequest();
    };
}
