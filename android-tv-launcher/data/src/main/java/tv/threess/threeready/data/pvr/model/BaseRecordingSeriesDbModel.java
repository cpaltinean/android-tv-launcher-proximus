/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.pvr.model;

import java.util.List;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.api.pvr.model.IBaseRecordingSeries;

/**
 * Proximus implementation of grouped recording episodes for a series.
 *
 * @author Barabas Attila
 * @since 2018.12.04
 */
public class BaseRecordingSeriesDbModel implements IBaseRecordingSeries {

    private String mId;

    private String mChannelId;

    private String mTitle;

    private int mNumberOfEpisodes;

    private ParentalRating mParentalRating;

    private List<IImageSource> mImageSources;

    private List<String> mGenres;

    @Override
    public String getId() {
        return mId;
    }

    @Override
    public String getChannelId() {
        return mChannelId;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public int getNumberOfRecordings() {
        return mNumberOfEpisodes;
    }

    @Override
    public ParentalRating getParentalRating() {
        return mParentalRating;
    }

    @Override
    public List<IImageSource> getImageSources() {
        return mImageSources;
    }

    @Override
    public List<String> getGenres() {
        return mGenres;
    }

    public static class Builder {
        private final BaseRecordingSeriesDbModel recordingSeries =
                new BaseRecordingSeriesDbModel();

        public Builder id(String id) {
            recordingSeries.mId = id;
            return this;
        }

        public Builder channelId(String channelId) {
            recordingSeries.mChannelId = channelId;
            return this;
        }

        public Builder title(String title) {
            recordingSeries.mTitle = title;
            return this;
        }

        public Builder numberOfEpisodes(int numberOfEpisodes) {
            recordingSeries.mNumberOfEpisodes = numberOfEpisodes;
            return this;
        }

        public Builder imageSources(List<IImageSource> imageSources) {
            recordingSeries.mImageSources = imageSources;
            return this;
        }

        public Builder parentalRating(ParentalRating parentalRating) {
            recordingSeries.mParentalRating = parentalRating;
            return this;
        }

        public Builder genres(List<String> genres) {
            recordingSeries.mGenres = genres;
            return this;
        }

        public BaseRecordingSeriesDbModel build() {
            return recordingSeries;
        }
    }
}
