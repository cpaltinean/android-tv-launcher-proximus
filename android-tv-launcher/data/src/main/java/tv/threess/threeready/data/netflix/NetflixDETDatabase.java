package tv.threess.threeready.data.netflix;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import tv.threess.threeready.data.generic.helper.BaseSQLiteOpenHelper;

/**
 * Helper class to manage database creation and version management.
 *
 * @author David
 * @since 25.02.2022
 */
public class NetflixDETDatabase extends BaseSQLiteOpenHelper {

    // database file name
    private static final String NAME = "netflix.db";

    // database version number
    private static final int VERSION = 1;

    public NetflixDETDatabase(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public boolean onUpgrade(SQLiteDatabase db, int toVersion) {
        return true;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + NetflixDETContract.Netflix.TABLE_NAME + " ("
                + NetflixDETContract.Netflix.CATEGORY + " TEXT NOT NULL,"
                + NetflixDETContract.Netflix.UI_LANGUAGE + " TEXT NOT NULL,"
                + NetflixDETContract.Netflix.RESPONSE_DATA + " TEXT,"
                + NetflixDETContract.Netflix.MIN_REFRESH_AT + " INTEGER NOT NULL,"
                + NetflixDETContract.Netflix.EXPIRE_AT + " INTEGER NOT NULL,"
                + NetflixDETContract.Netflix.LAST_UPDATED + " INTEGER NOT NULL)"
        );
    }
}
