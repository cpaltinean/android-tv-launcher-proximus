 /*
  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
  */
package tv.threess.threeready.data.home.model.generic;

 import com.google.gson.annotations.JsonAdapter;
 import com.google.gson.annotations.SerializedName;

 import java.io.Serializable;

 import tv.threess.threeready.api.home.model.generic.ButtonStyle;
 import tv.threess.threeready.api.home.model.generic.ContentMarkerStyle;
 import tv.threess.threeready.api.home.model.generic.FontStyle;
 import tv.threess.threeready.api.home.model.generic.StyleSettings;

 /**
 * POJO class which stores the style configuration of the UI components, like the button and font style.
 *
 * @author Barabas Attila
 * @since 2018.07.06
 */
public class StyleSettings3Ready implements Serializable, StyleSettings {

    @JsonAdapter(ButtonStyleTypeAdapter3Ready.class)
    @SerializedName("button_style")
    ButtonStyle mButtonStyle;

    @JsonAdapter(FontStyleTypeAdapter3Ready.class)
    @SerializedName("font_style")
    FontStyle mFontStyle;

    @JsonAdapter(ContentMarkerStyleTypeAdapter3Ready.class)
    @SerializedName("content_marker_style")
    ContentMarkerStyle mContentMarkerStyle;

    @Override
    public ButtonStyle getButtonStyle() {
        if (mButtonStyle == null) {
            return ButtonStyle.DEFAULT;
        }

        return mButtonStyle;
    }

    @Override
    public FontStyle getFontStyle() {
        if (mFontStyle == null) {
            return FontStyle.DEFAULT;
        }

        return mFontStyle;
    }

    @Override
    public ContentMarkerStyle getContentMarkerStyle() {
        if (mContentMarkerStyle == null) {
            mContentMarkerStyle = ContentMarkerStyle.DEFAULT;
        }

        return mContentMarkerStyle;
    }
}
