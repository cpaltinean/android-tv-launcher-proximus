package tv.threess.threeready.data.home.model.generic;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.generic.model.ContinueWatchingConfig;

/**
 * Continue watching related config settings.
 *
 * @author Barabas Attila
 * @since 2020.05.14
 */
public class ContinueWatchingConfig3Ready implements Serializable, ContinueWatchingConfig {
    @SerializedName("cw_size")
    private int mCwMaxSize;

    @SerializedName("cutv_watched_min_secs")
    private int mCatchupMinSecs;

    @SerializedName("pvr_watched_min_secs")
    private int mPvrMinSecs;

    @SerializedName("vod_watched_min_secs")
    private int mVodMinSecs;

    @SerializedName("cutv_max_pct_watched")
    private double mReplayWatchedPercent;

    @SerializedName("pvr_max_pct_watched")
    private double mPvrWatchedPercent;

    @SerializedName("vod_max_pct_watched")
    private double mVodWatchedPercent;

    @SerializedName("cw_age_limit_mins")
    private long mCwAgeLimitMins;

    /**
     * @return The maximum number of items to be displayed in the continue watching stripe.
     */
    @Override
    public int getContinueWatchingStripeMaxSize() {
        return mCwMaxSize;
    }

    /**
     * @return The minimum watched time after which replay bookmark needs to be added.
     * @param unit The time unit to return value from.
     */
    @Override
    public long getReplayWatchedMinTime(TimeUnit unit) {
        return unit.convert(mCatchupMinSecs, TimeUnit.SECONDS);
    }

    /**
     * @return The minimum watched time after which recording bookmark needs to be added.
     * @param unit The time unit to return value from.
     */
    @Override
    public long getRecordingWatchedMinTime(TimeUnit unit) {
        return unit.convert(mPvrMinSecs, TimeUnit.SECONDS);
    }

    /**
     * @return The minimum watched time after which VOD bookmark needs to be added.
     * @param unit The time unit to return value from.
     */
    @Override
    public long getVodWatchedMinTime(TimeUnit unit) {
        return unit.convert(mVodMinSecs, TimeUnit.SECONDS);
    }

    /**
     * @return The maximum watched percentage
     * after which the bookmark for the replay needs to be removed.
     */
    @Override
    public double getReplayWatchedPercent() {
        return mReplayWatchedPercent;
    }

    /**
     * @return The maximum watched percentage
     * after which the bookmark for the recording needs to be removed.
     */
    @Override
    public double getRecordingWatchedPercent() {
        return mPvrWatchedPercent;
    }

    /**
     * @return The maximum watched percentage
     * after which the bookmark for the VOD needs to be removed.
     */
    @Override
    public double getVodWatchedMaxPercent() {
        return mVodWatchedPercent;
    }

    /**
     * @return The maximum time to keep the bookmarks.
     * @param unit The time unit to return value from.
     */
    @Override
    public long geAgeLimit(TimeUnit unit) {
        return unit.convert(mCwAgeLimitMins, TimeUnit.MINUTES);
    }
}
