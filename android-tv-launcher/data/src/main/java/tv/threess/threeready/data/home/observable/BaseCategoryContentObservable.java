/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.home.observable;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.ObservableEmitter;
import io.reactivex.disposables.Disposable;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.vod.VodContract;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Base Rx observable class to return content from a category based on the module config.
 *
 * Created by Szilard on 3/1/2018.
 */
public abstract class BaseCategoryContentObservable<TItem> extends BaseContentObservable<ModuleData<TItem>> {
    public static final String TAG = Log.tag(BaseCategoryContentObservable.class);

    protected ModuleConfig mModuleConfig;
    protected int mStart;
    protected int mCount;
    private Disposable mDisposable;
    protected List<TItem> mItemList = new ArrayList<>();
    protected ModuleData<TItem> mCategoryData;

    public BaseCategoryContentObservable(Context context, ModuleConfig moduleConfig, int start, int count) {
        super(context);

        mModuleConfig = moduleConfig;
        mStart = start;
        mCount = count;
    }

    @Override
    public void subscribe(ObservableEmitter<ModuleData<TItem>> emitter) throws Exception {
        super.subscribe(emitter);
        loadItemList(emitter);
    }

    protected abstract void loadItemList(ObservableEmitter<ModuleData<TItem>> emitter) throws Exception;

    protected void onItemListLoaded(final ObservableEmitter<ModuleData<TItem>> emitter) {
        mItemList = new ArrayList<>(mCategoryData.getItems());

        emitter.onNext(mCategoryData);

        // Subscribe to vod purchase changes.
        for (TItem item : mCategoryData.getItems()) {
            if (item instanceof IBaseVodItem) {
                registerObserver(VodContract.buildPurchaseUrisForVod((IBaseVodItem) item));
            }
        }
    }

    /**
     * Cancel reschedule after observable cancelled.
     */
    @Override
    protected void unregisterObserver() {
        super.unregisterObserver();
        RxUtils.disposeSilently(mDisposable);
        mDisposable = null;
    }
}
