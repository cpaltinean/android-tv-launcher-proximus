/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.search;

import android.app.SearchManager;
import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

import tv.threess.threeready.api.BuildConfig;
import tv.threess.threeready.api.generic.BaseContract;

/**
 * Contract class to access global search result data.
 *
 * @author Barabas Attila
 * @since 2017.07.11
 */
public class GlobalSearchContract {
    public static final String AUTHORITY = BuildConfig.GLOBAL_SEARCH_PROVIDER;

    private static final Uri BASE_URI = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT).authority(AUTHORITY).build();

    public interface Movie {

        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Movie.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME + "/" + SearchManager.SUGGEST_URI_PATH_QUERY;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(GlobalSearchContract.Movie.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(GlobalSearchContract.BASE_URI, PATH);

        String ID = BaseColumns._ID;

        String TITLE = SearchManager.SUGGEST_COLUMN_TEXT_1;

        String DESCRIPTION = SearchManager.SUGGEST_COLUMN_TEXT_2;

        String CARD_IMAGE = SearchManager.SUGGEST_COLUMN_RESULT_CARD_IMAGE;

        String INTENT_DATA = SearchManager.SUGGEST_COLUMN_INTENT_DATA;

        String DURATION = SearchManager.SUGGEST_COLUMN_DURATION;

        String RENTAL_PRICE = SearchManager.SUGGEST_COLUMN_RENTAL_PRICE;

        String PURCHASE_PRICE = SearchManager.SUGGEST_COLUMN_PURCHASE_PRICE;

        String RELEASE_YEAR = SearchManager.SUGGEST_COLUMN_PRODUCTION_YEAR;

        String INTENT_DATA_ID = SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID;
    }


    public interface Program {

        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Program.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME + "/" + SearchManager.SUGGEST_URI_PATH_QUERY;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(Program.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(GlobalSearchContract.BASE_URI, PATH);

        String ID = BaseColumns._ID;

        String TITLE = SearchManager.SUGGEST_COLUMN_TEXT_1;

        String DESCRIPTION = SearchManager.SUGGEST_COLUMN_TEXT_2;

        String CARD_IMAGE = SearchManager.SUGGEST_COLUMN_RESULT_CARD_IMAGE;

        String INTENT_DATA = SearchManager.SUGGEST_COLUMN_INTENT_DATA;

        String DURATION = SearchManager.SUGGEST_COLUMN_DURATION;

        String RELEASE_YEAR = SearchManager.SUGGEST_COLUMN_PRODUCTION_YEAR;

        String INTENT_DATA_ID = SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID;

    }


    public interface Channel {

        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Channel.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME + "/" + SearchManager.SUGGEST_URI_PATH_QUERY;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(GlobalSearchContract.Channel.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(GlobalSearchContract.BASE_URI, PATH);

        String ID = BaseColumns._ID;

        String TITLE = SearchManager.SUGGEST_COLUMN_TEXT_1;

        String IS_LIVE = SearchManager.SUGGEST_COLUMN_IS_LIVE;

        String CARD_IMAGE = SearchManager.SUGGEST_COLUMN_RESULT_CARD_IMAGE;

        String INTENT_ACTION = SearchManager.SUGGEST_COLUMN_INTENT_ACTION;

        String INTENT_DATA = SearchManager.SUGGEST_COLUMN_INTENT_DATA;

        String INTENT_DATA_ID = SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID;

        String CONTENT_TYPE = SearchManager.SUGGEST_COLUMN_CONTENT_TYPE;
    }

}
