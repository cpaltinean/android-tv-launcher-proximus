package tv.threess.threeready.data.account.adapter;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.data.home.model.generic.ModuleFilter3Ready;
import tv.threess.threeready.data.home.model.module.ModuleDataSourceParams3Ready;

/**
 * adapter to manage filters from simple array to map at @{@link ModuleDataSourceParams3Ready#mFilter}
 * @author hunormadaras
 * @since 13/05/2019
 */
public class ModuleDataSourceFilterJsonAdapter3Ready implements JsonSerializer<ModuleFilter3Ready>, JsonDeserializer<ModuleFilter3Ready> {

    @Override
    public JsonElement serialize(ModuleFilter3Ready src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray jsonArray = new JsonArray();
        if (src == null) {
            return jsonArray;
        }
        for (Map.Entry<String, Serializable> entry : ArrayUtils.notNull(src.getFilter().entrySet())) {
            jsonArray.add(entry.getKey() + "[" + entry.getValue() + "]");
        }
        return jsonArray;
    }

    @Override
    public ModuleFilter3Ready deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonArray jsonArray = json.getAsJsonArray();
        Map<String, Serializable> map = new HashMap<>();
        if (jsonArray == null || jsonArray.size() == 0) {
            return null;
        }
        for (JsonElement data : jsonArray) {
            String item = data.toString();
            if (item.contains("[")) {
                int indexStart = item.indexOf('[');
                int indexEnd = item.indexOf(']');
                if (indexEnd == -1 || indexStart == -1) {
                    continue;
                }
                String key = item.substring(0, indexStart).replace("\"","");
                String value = item.substring(indexStart + 1, indexEnd).replace("\"","");
                map.put(key, value);
            }
        }
        return new ModuleFilter3Ready(map);
    }
}
