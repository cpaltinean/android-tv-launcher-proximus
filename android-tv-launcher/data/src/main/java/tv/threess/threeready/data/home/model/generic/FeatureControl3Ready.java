 /*
  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
  */
 package tv.threess.threeready.data.home.model.generic;

 import com.google.gson.annotations.SerializedName;

 import java.io.Serializable;

 import tv.threess.threeready.api.config.model.FeatureControl;

 /**
  * Feature behavior related config options.
  *
  * @author Barabas Attila
  * @since 5/14/21
  */
 public class FeatureControl3Ready implements Serializable, FeatureControl {

     @SerializedName("offline")
     private boolean mOffline;

     @SerializedName("epg_all_channels")
     private boolean mAllChannels;

     @SerializedName("search")
     private boolean mSearch;

     @SerializedName("extended_page_config")
     private boolean mExtendedPageConfig;

     @SerializedName("pvr")
     private boolean mPVR;

     @SerializedName("trickplay_enabled")
     private boolean mTrickplayEnabled;

     @SerializedName("open_home_after_standby")
     private boolean mShouldOpenHomeAfterStandBy;

     @Override
     public boolean displayAllChannels() {
         return mAllChannels;
     }

     @Override
     public boolean isPVREnabled() {
         return mPVR;
     }

     @Override
     public boolean hasSearch() {
         return mSearch;
     }

     @Override
     public boolean extendedPageConfig() {
         return mExtendedPageConfig;
     }

     @Override
     public boolean isOffline() {
         return mOffline;
     }

     @Override
     public boolean shouldOpenHomeAfterStandBy() {
         return mShouldOpenHomeAfterStandBy;
     }

     @Override
     public boolean isTrickplayEnabled() {
         return mTrickplayEnabled;
     }
 }
