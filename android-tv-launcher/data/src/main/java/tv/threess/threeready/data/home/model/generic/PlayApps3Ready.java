 /*
  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
  */
package tv.threess.threeready.data.home.model.generic;

 import com.google.gson.annotations.SerializedName;

 import java.io.Serializable;
 import java.util.List;

 import tv.threess.threeready.api.gms.model.EditorialAppInfo;
 import tv.threess.threeready.api.home.model.generic.PlayApps;
 import tv.threess.threeready.data.gms.EditorialAppInfo3Ready;

 /**
 * POJO class which stores the featured and play store apps.
 *
 * @author Barabas Attila
 * @since 2017.12.14
 */

public class PlayApps3Ready implements Serializable, PlayApps {

    @SerializedName("featured")
    List<EditorialAppInfo3Ready> mFeatured;

    @SerializedName("store")
    List<EditorialAppInfo3Ready> mStore;

    @Override
    public List<EditorialAppInfo> getFeatured() {
        return (List) mFeatured;
    }

    @Override
    public List<EditorialAppInfo> getStore() {
        return (List) mStore;
    }
}
