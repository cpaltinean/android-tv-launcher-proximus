/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.home.model.module;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import tv.threess.threeready.api.home.model.module.ModuleDataSource;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceMethod;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceService;
import tv.threess.threeready.data.account.adapter.ModuleDataSourceServiceTypeAdapter3Ready;

/**
 * Describe the request and parameters for a module data source.
 *
 * Created by Barabas Attila on 4/14/2017.
 */
public class ModuleDataSource3Ready implements ModuleDataSource {

    @JsonAdapter(ModuleDataSourceServiceTypeAdapter3Ready.class)
    @SerializedName("Service")
    private final ModuleDataSourceService mService;

    @SerializedName("request")
    private final ModuleDataSourceRequest3Ready mRequest;

    @SerializedName("params")
    private ModuleDataSourceParams3Ready mParams;

    public ModuleDataSource3Ready(ModuleDataSourceService service,
                                  ModuleDataSourceRequest3Ready request, ModuleDataSourceParams3Ready params) {
        mService = service;
        mRequest = request;
        mParams = params;
    }

    @Override
    public ModuleDataSourceService getService() {
        if (mService == null) {
            return ModuleDataSourceService.UNDEFINED;
        }
        return mService;
    }

    @Override
    public ModuleDataSourceMethod getMethod() {
        if (mRequest == null) {
            return ModuleDataSourceMethod.UNKNOWN;
        }
        return mRequest.getMethod();
    }

    @Override
    public ModuleDataSourceParams3Ready getParams() {
        return mParams;
    }

    public void setParams(ModuleDataSourceParams3Ready params) {
        this.mParams = params;
    }

    @Override
    public String toString() {
        return "ModuleDataSource{" +
                "mService=" + mService +
                ", mRequest=" + mRequest +
                ", mParams=" + mParams +
                '}';
    }
}
