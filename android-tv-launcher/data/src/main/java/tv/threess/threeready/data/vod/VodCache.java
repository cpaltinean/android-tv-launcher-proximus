package tv.threess.threeready.data.vod;

import android.app.Application;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.vod.model.IBaseVodItem;
import tv.threess.threeready.api.vod.model.IRentalExpiration;
import tv.threess.threeready.api.vod.model.IVodItem;
import tv.threess.threeready.api.vod.model.IVodVariant;
import tv.threess.threeready.data.utils.SqlUtils;
import tv.threess.threeready.data.vod.model.BaseVodItemDbModel;
import tv.threess.threeready.data.vod.model.VodVariantDbModel;
import tv.threess.threeready.data.vod.projection.VodProjection;
import tv.threess.threeready.data.vod.projection.VodVariantProjection;
import tv.threess.threeready.data.vod.projections.RentalExpirationProjection;

/**
 * Cache implementation for reading/writing VOD domain related data from/in the locale data base.
 *
 * @author David
 * @since 16.11.2021
 */
public class VodCache implements Component {
    private final String TAG = Log.tag(VodCache.class);

    private final Application mApp;

    public VodCache(Application application) {
        mApp = application;
    }

    // ----------------- Vod

    /**
     * @param vodVariantId The unique identifier of the VodVariant item.
     * @return The continue watching vod with the given id.
     */
    @Nullable
    public IBaseVodItem getContinueWatchingVod(String vodVariantId) {
        BaseVodItemDbModel vod = SqlUtils.queryFirst(mApp,
                VodContract.ContinueWatchingVod.CONTENT_URI,
                VodProjection.PROJECTION,
                cursor -> {
                    List<BaseVodItemDbModel> allContinueWatchingVods = VodProjection.readAll(cursor);
                    for (BaseVodItemDbModel temp : allContinueWatchingVods) {
                        if (temp.getVariantIds().contains(vodVariantId)) {
                            return temp;
                        }
                    }
                    return null;
                }
        );

        if (vod != null) {
            // Get required variant of the VOD.
            VodVariantDbModel vodVariant = getContinueWatchingVodVariant(vodVariantId);
            if (vodVariant == null) {
                return null;
            }

            vod.setMainVariant(vodVariant);
        }

        return vod;
    }

    /**
     * @param seriesId The unique identifier of the VOD series.
     * @return All the episodes in the series with bookmark.
     */
    public List<IBaseVodItem> getContinueWatchingEpisodes(String seriesId) {
        return SqlUtils.queryFirst(mApp,
                VodContract.ContinueWatchingVod.CONTENT_URI,
                VodProjection.PROJECTION,
                VodContract.ContinueWatchingVod.SERIES_ID + " = ?",
                new String[]{seriesId},
                cursor -> {
                    List<BaseVodItemDbModel> items = VodProjection.readAll(cursor);
                    List<IBaseVodItem> retItems = new ArrayList<>();

                    for (BaseVodItemDbModel item : items) {
                        for (String variantId : item.getVariantIds()) {
                            // Get required variant of the VOD.
                            VodVariantDbModel vodVariant = getContinueWatchingVodVariant(variantId);
                            if (vodVariant != null) {
                                item.setMainVariant(vodVariant);
                                retItems.add(item);
                                break;
                            }
                        }
                    }

                    return retItems;
                }

        );
    }

    /**
     * Add vod item to continue watching list.
     *
     * @param vod     The vod item which needs to be added as continue watching.
     * @param variant The vod variant which needs to be added as continue watching.
     */
    public void addVodContinueWatching(IVodItem vod, IVodVariant variant) {
        ContentValues[] vodValues = VodProjection.toContentValues(Collections.singletonList(vod));
        SqlUtils.bulkReplace(mApp.getContentResolver(), VodContract.ContinueWatchingVod.CONTENT_URI, vodValues);

        ContentValues[] variantValues = VodVariantProjection.toContentValues(Collections.singletonList(variant));
        SqlUtils.bulkReplace(mApp.getContentResolver(),
                VodContract.ContinueWatchingVodVariant.CONTENT_URI, variantValues);
    }

    /**
     * Delete previously saved VOD continue watching item.
     *
     * @param vodItem The vod which needs to be removed.
     * @param variant The vod variant which needs to be removed.
     */
    public void deleteVodContinueWatching(IVodItem vodItem, IVodVariant variant) {
        String selection = VodContract.Vod.VOD_ID + " = ? ";
        mApp.getContentResolver().delete(VodContract.ContinueWatchingVod.CONTENT_URI,
                selection, new String[]{vodItem.getId()});

        selection = VodContract.ContinueWatchingVodVariant.VARIANT_ID + " = ? ";
        mApp.getContentResolver().delete(VodContract.ContinueWatchingVodVariant.CONTENT_URI,
                selection, new String[]{variant.getId()});
    }

    /**
     * Delete all previously saved VOD which is older then {@param olderThan}.
     */
    public void deleteOlderContinueWatchingVod(long olderThan) {
        String selection = BaseContract.LAST_UPDATED + " <= ? ";

        mApp.getContentResolver().delete(BaseContract.withBatchUpdate(
                VodContract.ContinueWatchingVod.CONTENT_URI), selection,
                new String[]{String.valueOf(olderThan)});
    }

    // ----------------- VodVariant

    /**
     * Delete all previously saved continue watching VOD which is older then {@param olderThan}.
     */
    public void deleteOlderContinueWatchingVodVariant(long olderThan) {
        String selection = BaseContract.LAST_UPDATED + " <= ? ";

        mApp.getContentResolver().delete(BaseContract.withBatchUpdate(
                VodContract.ContinueWatchingVodVariant.CONTENT_URI), selection,
                new String[]{String.valueOf(olderThan)});
    }

    // ----------------- RentalExpiration

    /**
     * @return Map, where the key
     */
    public Map<String, IRentalExpiration> getRentalExpiration() {
        return ArrayUtils.notNull(SqlUtils.queryFirst(mApp,
                VodContract.RentalExpiration.CONTENT_URI,
                RentalExpirationProjection.PROJECTION,
                RentalExpirationProjection::mapFromCursor
        ));
    }

    /**
     * Method used for rent a VOD item.
     */
    public void rentVodItem(String variantId, long expirationTime, long rentalTime) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(VodContract.RentalExpiration.VARIANT_ID, variantId);
        contentValues.put(VodContract.RentalExpiration.EXPIRATION, expirationTime);
        contentValues.put(VodContract.RentalExpiration.RENTAL_TIME, rentalTime);
        contentValues.put(VodContract.RentalExpiration.LAST_UPDATED, System.currentTimeMillis());

        mApp.getContentResolver().insert(BaseContract.withBatchUpdate(VodContract.RentalExpiration.CONTENT_URI), contentValues);

        // Notify observers about the vod purchase status change.
        mApp.getContentResolver().notifyChange(
                VodContract.buildPurchaseUriForVariant(variantId), null);
    }

    /**
     * Update the rented vods with the given list of vod items.
     */
    public void updateRentedVods(List<? extends IRentalExpiration> rentedVods, long now) {
        Map<String, IRentalExpiration> localRentalMap = getRentalExpiration();

        // Update cache with new rentals.
        List<ContentValues> rentedValues = new ArrayList<>();
        for (IRentalExpiration rentedVod : rentedVods) {
            ContentValues values = new ContentValues();
            values.put(VodContract.RentalExpiration.VARIANT_ID, rentedVod.getVariantId());
            values.put(VodContract.RentalExpiration.EXPIRATION, rentedVod.getExpireTime());
            values.put(VodContract.RentalExpiration.RENTAL_TIME, rentedVod.getRentalTime());
            values.put(VodContract.RentalExpiration.LAST_UPDATED, now);
            rentedValues.add(values);
        }
        ContentResolver cr = mApp.getContentResolver();
        SqlUtils.bulkReplace(cr, BaseContract.withBatchUpdate(VodContract.RentalExpiration.CONTENT_URI), now, rentedValues);

        // Notify rental changes.
        List<IRentalExpiration> expiredRentals = new ArrayList<>(localRentalMap.values());
        for (IRentalExpiration backendRental : rentedVods) {
            IRentalExpiration localRental = localRentalMap.get(backendRental.getVariantId());

            Uri purchaseNotifyUri = VodContract.buildPurchaseUriForVariant(backendRental.getVariantId());

            // Notify new rental.
            if (localRental == null || localRental.getExpireTime() != backendRental.getExpireTime()) {
                Log.d(TAG, "Notify new rental. Variant : " + backendRental.getVariantId());
                cr.notifyChange(purchaseNotifyUri, null);
                // Notify rental change.
            } else if (localRental.getExpireTime() != backendRental.getExpireTime()) {
                Log.d(TAG, "Notify rental change. Variant : " + backendRental.getVariantId());
                cr.notifyChange(purchaseNotifyUri, null);
            }

            expiredRentals.removeIf(rental -> Objects.equals(rental.getVariantId(), backendRental.getVariantId()));
        }

        // Notify all the rentals which expired.
        for (IRentalExpiration expiredRental : expiredRentals) {
            Log.d(TAG, "Notify rental expired. Variant : " + expiredRental.getVariantId());
            cr.notifyChange(VodContract.buildPurchaseUriForVariant(expiredRental.getVariantId()), null);
        }
    }

    // private methods

    /**
     * @param variantId The unique identifier of the VOD variant.
     * @return The continue watching VOD variant with the given id,
     * or null if there is no such variant.
     */
    @Nullable
    private VodVariantDbModel getContinueWatchingVodVariant(String variantId) {
        String selection = VodContract.ContinueWatchingVodVariant.VARIANT_ID + " =?";

        return SqlUtils.queryFirst(mApp,
                VodContract.ContinueWatchingVodVariant.CONTENT_URI,
                VodVariantProjection.PROJECTION,
                selection,
                new String[]{variantId},
                VodVariantProjection::readRow
        );
    }
}
