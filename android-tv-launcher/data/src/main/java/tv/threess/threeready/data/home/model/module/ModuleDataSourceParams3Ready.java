/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.home.model.module;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import tv.threess.threeready.api.home.SelectorOption;
import tv.threess.threeready.api.home.model.module.ModuleDataSourcePaginationParams;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceParams;
import tv.threess.threeready.data.account.adapter.ModuleDataSourceFilterJsonAdapter3Ready;
import tv.threess.threeready.data.home.model.generic.ModuleFilter3Ready;


/**
 * Filter and ordering parameters for a data source request.
 *
 * Created by Barabas Attila on 4/14/2017.
 */
public class ModuleDataSourceParams3Ready implements ModuleDataSourceParams {

    @SerializedName("filter")
    @JsonAdapter(ModuleDataSourceFilterJsonAdapter3Ready.class)
    private ModuleFilter3Ready mFilter;

    @SerializedName("sort")
    private String[] mSort;

    @SerializedName("size")
    private int[] mSize;

    @SerializedName("page")
    private ModuleDataSourcePaginationParams3Ready mPagination;

    @Override
    public int[] getSize() {
        return mSize;
    }

    @Override
    public String[] getSort() {
        return mSort;
    }

    ModuleDataSourcePaginationParams getPagination() {
        return mPagination;
    }

    @Override
    public boolean hasFilter(String key) {
        return mFilter == null ? false : mFilter.hasFilter(key);
    }

    @Override
    public String getFilter(String key) {
        return mFilter == null ? null : String.valueOf(mFilter.getFilter(key, String.class));
    }

    @Override
    public String getSort(String key) {
        if (mSort == null || mSort.length == 0) {
            return null;
        }
        for (String filter : mSort) {
            if (filter.startsWith(key + "[")) {
                int indexStart = filter.indexOf('[');
                int indexEnd = filter.indexOf(']');
                if (indexEnd == -1 || indexStart == -1) {
                    continue;
                }
                return filter.substring(indexStart + 1, indexEnd);
            }
        }
        return null;
    }

    @Override
    public void addFilter(String key, Serializable value) {
        if (mFilter != null) {
            mFilter.addFilter(key, value);
        }
    }

    @Nullable
    @Override
    public <T extends Serializable> T getFilter(String key, Class<T> clazz) {
        return mFilter == null ? null : mFilter.getFilter(key, clazz);
    }

    @Override
    public List<String> getFilterList(String key) {
        return mFilter == null ? null : mFilter.getFilterListAsString(key, String.class);
    }

    @Override
    public List<String> getSortList(String key) {
        String sort = getSort(key);
        if (TextUtils.isEmpty(sort)) {
            return null;
        }

        String[] sortArray = sort.split(",");
        return Arrays.asList(sortArray);
    }

    @Override
    public String toString() {
        return "ModuleDataSourceParams{" +
                "mFilter=" + mFilter +
                ", mSort=" + Arrays.toString(mSort) +
                ", mSize=" + Arrays.toString(mSize) +
                '}';
    }

}
