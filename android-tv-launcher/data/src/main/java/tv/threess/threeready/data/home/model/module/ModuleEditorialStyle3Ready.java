/*
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.home.model.module;

import android.graphics.Color;
import android.text.TextUtils;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.home.model.module.EditorialItemStyle;
import tv.threess.threeready.api.home.model.module.ModuleEditorialStyle;
import tv.threess.threeready.data.account.adapter.ColorTypeAdapter3Ready;

/**
 * POJO class which holds the item style options for a module.
 *
 * @author Barabas Attila
 * @since 2018.08.23
 */
public class ModuleEditorialStyle3Ready implements ModuleEditorialStyle {

    @SerializedName("items")
    private EditorialItemStyle3Ready mItemStyle;

    @SerializedName("background_color")
    @JsonAdapter(ColorTypeAdapter3Ready.class)
    private Integer mBackgroundColor;

    @SerializedName("placeholder_color")
    @JsonAdapter(ColorTypeAdapter3Ready.class)
    private Integer mPlaceholderColor;

    @SerializedName("title_color")
    @JsonAdapter(ColorTypeAdapter3Ready.class)
    private Integer mTitleColor;

    @SerializedName("text_color")
    @JsonAdapter(ColorTypeAdapter3Ready.class)
    private Integer mTextColor;

    @SerializedName("background_image")
    private String mBackgroundImage;

    @Override
    public EditorialItemStyle getItemStyle() {
        return mItemStyle;
    }

    @Override
    public boolean hasTitleColor() {
        return mTitleColor != null;
    }

    @Override
    public int getTitleColor() {
        return mTitleColor;
    }

    @Override
    public int getBackgroundColor() {
        return ColorUtils.getColor(mBackgroundColor, Color.BLACK);
    }

    @Override
    public String getBackgroundImage() {
        return mBackgroundImage;
    }

    @Override
    public boolean hasBackgroundImage() {
        return !TextUtils.isEmpty(mBackgroundImage);
    }
}
