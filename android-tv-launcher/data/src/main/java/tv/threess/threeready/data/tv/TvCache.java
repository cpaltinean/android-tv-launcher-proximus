package tv.threess.threeready.data.tv;

import android.app.Application;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Pair;
import android.util.Range;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.TimerLog;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.api.home.model.module.ModuleSortOption;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.model.TifChannel;
import tv.threess.threeready.api.middleware.model.TifChannelList;
import tv.threess.threeready.api.search.model.SearchTerm;
import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.TvChannelCacheType;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.ITvChannelResponse;
import tv.threess.threeready.api.tv.model.StreamType;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.tv.model.TvAdvertisement;
import tv.threess.threeready.data.tv.model.TvAdvertisementDBModel;
import tv.threess.threeready.data.tv.projections.AdvertisementProjection;
import tv.threess.threeready.data.tv.projections.BroadcastProjection;
import tv.threess.threeready.data.tv.projections.ChannelProjection;
import tv.threess.threeready.data.tv.projections.ContinueWatchingBroadcastProjection;
import tv.threess.threeready.data.utils.SqlUtils;

/**
 * Cache implementation for reading/writing Tv domain related data from/in the locale data base.
 *
 * @author David
 * @since 16.11.2021
 */
public class TvCache implements Component {
    private static final String TAG = Log.tag(TvCache.class);

    private final Application mApp;

    private static final char ESCAPE_CHR = '\\';
    private static final String ESCAPE_STR = String.valueOf(ESCAPE_CHR);

    public TvCache(Application application) {
        mApp = application;
    }

    // ----------------- Tv Channels

    /**
     * @return list of channels filtered by the given parameters.
     */
    public List<TvChannel> getTvChannels(String typeFilter, String languageFilter, String idFilter, String[] sortOptions, int count) {
        Uri uri = BaseContract.withLimit(TvContract.Channel.CONTENT_URI, count);

        // Apply filters.
        List<String> selectionList = new ArrayList<>();
        List<String> selectionArgs = new ArrayList<>();

        // Apply channel type filter.
        if (!TextUtils.isEmpty(typeFilter)) {
            selectionList.add(applyTypeFilter(typeFilter));
        }

        // Apply language filter.
        if (!TextUtils.isEmpty(languageFilter)) {
            selectionList.add(TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.LANGUAGE + "=?");
            selectionArgs.add(languageFilter);
        }

        // Apply id filter.
        String[] filterIds = ArrayUtils.split(idFilter, ",");
        if (!ArrayUtils.isEmpty(filterIds)) {
            selectionList.add(SqlUtils.buildBinding(TvContract.Channel.QUALIFIED_ID, "IN", filterIds.length));
            selectionArgs.addAll(Arrays.asList(filterIds));
        }

        //apply user subscribed filter
        selectionList.add(TvContract.Channel.IS_USER_TV_SUBSCRIBED_CHANNEL + " = 1");

        // Build query selection.
        selectionList.removeIf(String::isEmpty);
        String selection = TextUtils.join(" AND ", selectionList);
        selection = TextUtils.isEmpty(selection) ? null : selection;

        // Apply sorting.
        List<String> sortOrderList = new ArrayList<>();
        if (sortOptions == null) {
            sortOrderList.add(applySorting("")); // Default order
        } else {
            for (String sortOption : sortOptions) {
                sortOrderList.add(applySorting(sortOption));
            }
        }

        // Build query sort order.
        sortOrderList.removeIf(String::isEmpty);
        String sortOrder = TextUtils.join(",", sortOrderList);
        sortOrder = TextUtils.isEmpty(sortOrder) ? null : sortOrder;

        return SqlUtils.queryAll(mApp,
                uri,
                ChannelProjection.PROJECTION,
                selection,
                ArrayUtils.toArray(selectionArgs),
                sortOrder,
                ChannelProjection::readRow
        );
    }

    /**
     * @param channelIds for filtering the channels we need
     * @return list of channels filtered by the given parameters.
     */
    public List<TvChannel> getChannels(List<String> channelIds) {
        if (channelIds == null || channelIds.isEmpty()) {
            return new ArrayList<>();
        }

        String selection = TvContract.Channel.QUALIFIED_ID + " IN ("
                + TextUtils.join(",", Collections.nCopies(channelIds.size(), "?")) + ")";

        String[] selectionArgs = channelIds.toArray(new String[0]);

        List<TvChannel> channelList = getTvChannelsByQueryParams(TvContract.Channel.CONTENT_URI,
                selection, selectionArgs, null);

        if (!ArrayUtils.isEmpty(channelList)) {
            channelList.sort(Comparator.comparingInt(o -> channelIds.indexOf(o.getId())));
            return channelList;
        }

        return new ArrayList<>();
    }

    /**
     * @param channelType {@see tv.threess.threeready.api.tv.TvChannelCacheType}
     * @return list of replay enabled channels filtered by the given parameters.
     */
    public List<TvChannel> getReplayChannels(TvChannelCacheType channelType, int start, int count) {
        String selection;
        String sortOrder = TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.POSITION + " ASC";
        Uri uri = BaseContract.withLimit(TvContract.Channel.CONTENT_URI, count, start);

        if (channelType == TvChannelCacheType.FAVORITE) {
            selection = TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.IS_REPLAY_ENABLED + " > 0" +
                    " AND " + TvContract.Favorites.CHANNEL_ID + " = " + TvContract.Channel.QUALIFIED_ID;
        } else {
            selection = TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.IS_REPLAY_ENABLED + " > 0";
        }

        return getTvChannelsByQueryParams(uri, selection, null, sortOrder);
    }

    /**
     * @return channel by channel number.
     */
    @Nullable
    public TvChannel getChannelByNumber(int number) {
        String selection = TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.NUMBER + " >= ?";
        String[] selectionArgs = {String.valueOf(number)};
        String sortOrder = TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.NUMBER;

        return getTvChannelByQueryParams(selection, selectionArgs, sortOrder);
    }

    /**
     * @return first favorite channel from the recent channel list.
     */
    @Nullable
    public TvChannel getFirstFavoriteChannel() {
        String selection = TvContract.Favorites.TABLE_NAME + "." + TvContract.Favorites.POSITION + " IS NOT NULL";
        String sortOrder = TvContract.Favorites.TABLE_NAME + "." + TvContract.Favorites.POSITION + " DESC";
        Uri uri = BaseContract.withLimit(TvContract.Channel.CONTENT_URI, Integer.MAX_VALUE);

        List<TvChannel> channels = getTvChannelsByQueryParams(uri, selection, null, sortOrder);

        if (!channels.isEmpty()) {
            //If channelList size is greater then 1 and the first favorite channel in the list is a
            // netflix channel, then this function returns channels.get(1). It's because we need to
            // skip the netflix channel(App) if we have another channels in the favoriteList.
            return channels.get((channels.size() > 1 && channels.get(0).getId().equalsIgnoreCase(TvContract.Channel.NETFLIX_CHANNEL_NAME)) ? 1 : 0);
        }

        return null;
    }

    /**
     * @return first channel from the recent channel list.
     */
    @Nullable
    public TvChannel getFirstChannel() {
        String sortOrder = TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.NUMBER;
        return getTvChannelByQueryParams(null, null, sortOrder);
    }

    /**
     * @return Ids of favorite channels.
     */
    public List<String> getFavoriteChannelIds() {
        String selection = TvContract.Favorites.TABLE_NAME + "." + TvContract.Favorites.CHANNEL_ID + " IS NOT NULL";
        String sortOrder = TvContract.Favorites.TABLE_NAME + "." + TvContract.Favorites.POSITION + " ASC";

        return SqlUtils.queryAll(mApp,
                TvContract.Channel.CONTENT_URI,
                ChannelProjection.COLUMN_CHANNEL_ID,
                selection,
                null,
                sortOrder,
                cursor -> cursor.getString(ChannelProjection.CHANNEL_ID.ordinal())
        );
    }

    /**
     * @return channel by id.
     */
    @Nullable
    public TvChannel getTvChannelById(String channelId) {
        return getTvChannelByQueryParams(TvContract.Channel.ID + " = ?", new String[]{channelId}, null);
    }

    /**
     * @return list of Baby channels.
     */
    public List<TvChannel> getBabyChannels() {
        String selection = TvContract.Channel.IS_BABY_CONTENT_WARNING_REQUIRED + " = 1";
        return getTvChannelsByQueryParams(TvContract.Channel.CONTENT_URI, selection, null, null);
    }

    /**
     * Update the cached channels.
     */
    public <Channel extends TvChannel> void updateChannels(ITvChannelResponse<Channel> response, TifChannelList tifChannelList) {
        long now = System.currentTimeMillis();

        // perform scan, add channels to TIF.
        List<ContentValues> channelCV = new ArrayList<>();
        List<ContentValues> playbackOptionCV = new ArrayList<>();
        List<Channel> tvChannels = response.getChannels();

        for (int position = 0; position < tvChannels.size(); position++) {
            Channel channel = tvChannels.get(position);

            // Add channel details.
            ContentValues channelContentValues = new ContentValues();
            channelContentValues.put(TvContract.Channel.ID, channel.getId());
            channelContentValues.put(TvContract.Channel.INTERNAL_ID, channel.getInternalId());
            channelContentValues.put(TvContract.Channel.CALL_LETTER, channel.getCallLetter());
            channelContentValues.put(TvContract.Channel.POSITION, position);
            channelContentValues.put(TvContract.Channel.NUMBER, channel.getNumber());
            channelContentValues.put(TvContract.Channel.NAME, channel.getName());
            channelContentValues.put(TvContract.Channel.LANGUAGE, channel.getLanguage());
            channelContentValues.put(TvContract.Channel.IMAGE_URL, channel.getChannelLogoUrl());
            channelContentValues.put(TvContract.Channel.TYPE, channel.getType().ordinal());
            channelContentValues.put(TvContract.Channel.IS_USER_TV_SUBSCRIBED_CHANNEL, channel.isUserTvSubscribedChannel());
            channelContentValues.put(TvContract.Channel.IS_BABY_CONTENT_WARNING_REQUIRED, channel.isBabyContentWarningRequired());
            channelContentValues.put(TvContract.Channel.IS_NPVR_ENABLED, channel.isNPVREnabled());
            channelContentValues.put(TvContract.Channel.IS_REPLAY_ENABLED, channel.isReplayEnabled());
            channelContentValues.put(TvContract.Channel.IS_HD_ENABLED, channel.isHDEnabled());
            channelContentValues.put(TvContract.Channel.IS_LIVE_TAD_ENABLED, channel.isLiveTadEnabled());
            channelContentValues.put(TvContract.Channel.IS_TIME_SHIFT_TAD_ENABLED, channel.isTimeShiftTadEnabled());
            channelContentValues.put(TvContract.Channel.REPLAY_WINDOW, channel.getReplayWindow());
            channelContentValues.put(TvContract.Channel.TARGET_AD_INACTIVITY, channel.getTargetAdInactivity(TimeUnit.MINUTES));
            channelContentValues.put(TvContract.Channel.LAST_UPDATED, now);

            int order = 0;
            for (ITvChannelResponse.Option option : response.getOptions(channel)) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(TvContract.PlaybackOption.CHANNEL_ID, channel.getId());
                contentValues.put(TvContract.PlaybackOption.PLAYBACK_TYPE, option.getPlaybackType().name());
                contentValues.put(TvContract.PlaybackOption.PLAYBACK_DATA, option.getPlaybackData());
                contentValues.put(TvContract.PlaybackOption.PLAYBACK_EXTRA, option.getPlaybackExtra());
                contentValues.put(TvContract.PlaybackOption.PLAYBACK_ORDER, order);
                contentValues.put(TvContract.PlaybackOption.LAST_UPDATED, now);
                TifChannel tifChannel = tifChannelList.get(option);
                if (tifChannel != null) {
                    contentValues.put(TvContract.PlaybackOption.TIF_INPUT_ID, tifChannel.getInputId());
                    contentValues.put(TvContract.PlaybackOption.TIF_CHANNEL_ID, tifChannel.getChannelId());
                }
                playbackOptionCV.add(contentValues);
                order += 1;
            }

            // Check if channel has any available playback option.
            channelCV.add(channelContentValues);
        }

        Log.d(TAG, "Update channels - performing update, content values prepared");
        ContentResolver cr = mApp.getContentResolver();

        // Insert playback options in database.
        long options = SqlUtils.bulkReplace(cr, TvContract.PlaybackOption.CONTENT_URI, now, playbackOptionCV);

        // Insert channel details in database.
        long channels = SqlUtils.bulkReplace(cr, TvContract.Channel.CONTENT_URI, now, channelCV);

        Log.i(TAG, "Update channels - updated " + channels + " channels with " + options + " playback options");
    }

    /**
     * @param channelType      {@see tv.threess.threeready.api.tv.TvChannelCacheType}
     * @param orderWithNetflix flag used for identifying if we need some extra filters
     * @return list of channels filtered by the given parameters.
     */
    public List<TvChannel> getChannels(TvChannelCacheType channelType, boolean orderWithNetflix) {
        String selection = null;
        StringBuilder order = new StringBuilder();

        switch (channelType) {
            case FAVORITE:
                selection = TvContract.Favorites.TABLE_NAME + "." + TvContract.Favorites.POSITION + " IS NOT NULL";
                if (orderWithNetflix) {
                    order.append(TvContract.Channel.TABLE_NAME).append(".").append(TvContract.Channel.NUMBER);
                    DatabaseUtils.appendEscapedSQLString(order.append("="), TvContract.Channel.NETFLIX_CHANNEL_NAME);
                    order.append("DESC,").append(TvContract.Favorites.TABLE_NAME).append(".").append(TvContract.Favorites.POSITION);
                } else {
                    order.append(TvContract.Favorites.TABLE_NAME).append(".").append(TvContract.Favorites.POSITION);
                }
                break;
            case NOT_FAVORITE:
                selection = TvContract.Favorites.TABLE_NAME + "." + TvContract.Favorites.POSITION + " IS NULL AND "
                        + TvContract.Channel.IS_USER_TV_SUBSCRIBED_CHANNEL + " = 1";
                order.append(TvContract.Channel.TABLE_NAME).append(".").append(TvContract.Channel.POSITION);
                break;
            case ALL:
                selection = TvContract.Channel.IS_USER_TV_SUBSCRIBED_CHANNEL + " = 1 ";
                order.append(TvContract.Channel.TABLE_NAME).append(".").append(TvContract.Channel.POSITION);
                break;
        }

        return getTvChannelsByQueryParams(TvContract.Channel.CONTENT_URI, selection, null, order.toString());
    }

    /**
     * @return channels results for local search.
     */
    public List<TvChannel> getChannelForSearch(SearchTerm searchTerm, int start, int count) {
        Log.d(TAG, "channelSearch() searching for: " + searchTerm.getTerm());

        String selection = TvContract.Channel.TABLE_NAME + "." +
                TvContract.Channel.NAME +
                " LIKE ? COLLATE NOCASE ESCAPE ?" +
                " AND " + TvContract.Channel.TABLE_NAME +
                "." + TvContract.Channel.IS_USER_TV_SUBSCRIBED_CHANNEL + " = 1";

        String[] selectionArgs = {escapeSearchTerm(searchTerm.getTerm()), ESCAPE_STR};
        Uri uri = BaseContract.withLimit(TvContract.Channel.CONTENT_URI, count, start);
        String sortOrder = TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.NUMBER;

        return getTvChannelsByQueryParams(uri, selection, selectionArgs, sortOrder);
    }

    /**
     * @return channel results for voice search.
     */
    @Nullable
    public TvChannel getChannelForVoicePlaySearch(SearchTerm searchTerm) {
        // Read channel results from local database.
        String where;
        String[] args;

        if (TextUtils.isDigitsOnly(searchTerm.getTerm())) {
            where = TvContract.Channel.NUMBER + " = ?";
            args = new String[]{searchTerm.getTerm()};
        } else {
            where = TvContract.Channel.NAME + " LIKE ? ESCAPE ?";
            args = new String[]{escapeSearchTerm(searchTerm.getTerm()), ESCAPE_STR};
        }

        where += " AND " + TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.IS_USER_TV_SUBSCRIBED_CHANNEL + " = 1";
        String sortOrder = TvContract.Channel.IS_HD_ENABLED + " DESC";

        return getTvChannelByQueryParams(where, args, sortOrder);
    }

    /**
     * The STB will analyse whether target ad features are applicable to him based on:
     * - Availability of a subscription to at least one channel that is target ad enabled
     * - Opt-in for target advertisement is set
     * <p>
     * If the STB is not enabled for target advertisement, it should not execute Target ad download / replacement
     */
    public boolean isChannelWithEnabledTAD() {
        Uri uri = TvContract.Channel.CONTENT_URI;
        String projection = BaseContract.COUNT_ALL;
        String selection = TvContract.Channel.IS_LIVE_TAD_ENABLED + "!=0 OR " +
                TvContract.Channel.IS_TIME_SHIFT_TAD_ENABLED + "!=0";
        return SqlUtils.queryLong(mApp, uri, projection, selection, null, null) > 0;
    }


    // ----------------- PlaybackOption


    /**
     * Delete all playback options.
     */
    public void deletePlaybackOptions() {
        mApp.getContentResolver().delete(TvContract.PlaybackOption.CONTENT_URI, null, null);
    }

    /**
     * @return app channel package id found by the given {@param channelId}.
     */
    public String getAppChannelPackageId(String channelId) {
        Uri uri = TvContract.PlaybackOption.CONTENT_URI;
        String selection = TvContract.PlaybackOption.TABLE_NAME + "." + TvContract.PlaybackOption.PLAYBACK_TYPE + "=? " +
                "AND " + TvContract.PlaybackOption.TABLE_NAME + "." + TvContract.PlaybackOption.CHANNEL_ID + "=?";
        String[] selectionArgs = new String[]{StreamType.App.name(), channelId};
        String[] project = new String[]{TvContract.PlaybackOption.PLAYBACK_DATA};

        return SqlUtils.queryFirst(mApp,
                uri,
                project,
                selection,
                selectionArgs,
                cursor -> cursor.getString(0)
        );
    }

    /**
     * Check if we have TIF channels in the Data Base.
     *
     * @throws Exception if there are no scanned channels.
     */
    public void checkTIFPlaybackOptions() throws Exception {
        String select = BaseContract.COUNT_ALL;
        Uri uri = TvContract.PlaybackOption.CONTENT_URI;
        String where = TvContract.PlaybackOption.TIF_CHANNEL_ID + " is not null";
        if (SqlUtils.queryLong(mApp, uri, select, where, null, null) == 0) {
            throw new Exception("Channel scan failed, there are no playback options");
        }
    }

    // ----------------- Broadcast


    /**
     * @return the currently running program list.
     */
    public List<IBroadcast> getAllNowBroadcasts() {
        Uri uri = TvContract.Broadcast.CONTENT_URI;
        long now = System.currentTimeMillis();
        String selection = "( " + TvContract.Channel.TYPE + " = " + ChannelType.TV.ordinal() + " OR " +
                TvContract.Channel.TYPE + " = " + ChannelType.RADIO.ordinal() + " ) AND " +
                TvContract.Broadcast.END + " >= " + now + " AND " + TvContract.Broadcast.START + " < " + now;

        return getBroadcastsByQueryParams(uri, selection, null, null);
    }

    /**
     * @return Currently running broadcasts filtered by the data source parameters.
     */
    public List<IBroadcast> getNowBroadcasts(List<String> genreList, String favorite, int count) {
        String selection = TvContract.Broadcast.START + " < " + System.currentTimeMillis()
                + " AND " + TvContract.Broadcast.END + " > " + System.currentTimeMillis()
                + " AND " + TvContract.Channel.TYPE + " <> " + ChannelType.APP.ordinal()
                + " AND " + TvContract.Channel.TYPE + " <> " + ChannelType.REGIONAL.ordinal()
                + " AND " + TvContract.Channel.IS_USER_TV_SUBSCRIBED_CHANNEL + " = 1"
                + " GROUP BY " + TvContract.Broadcast.QUALIFIED_CHANNEL_ID;
        String sortOrder = TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.POSITION + " ASC LIMIT " + count;

        return getBroadcastsByGenre(selection, sortOrder, genreList, favorite);
    }

    /**
     * Returns a list of now playing broadcasts, with a limit of count,
     * there is to type of sort oder, on with basic position of channel and one with selecting
     * last watched channel and positioning the according now playing broadcast to the start of
     * the list.
     *
     * @return list of now playing broadcasts
     */
    public List<IBroadcast> getNowOnTvBroadcasts(List<String> genreList, String favorite, String lastPlayedChannelId, int count) {
        String selection = TvContract.Broadcast.START + " <= " + System.currentTimeMillis()
                + " AND " + TvContract.Broadcast.END + " > " + System.currentTimeMillis()
                + " AND " + TvContract.Channel.TYPE + " <> " + ChannelType.APP.ordinal()
                + " AND " + TvContract.Channel.IS_USER_TV_SUBSCRIBED_CHANNEL + " = 1"
                + " GROUP BY " + TvContract.Broadcast.QUALIFIED_CHANNEL_ID;
        String sortOrder = "CASE WHEN " + TvContract.Broadcast.QUALIFIED_CHANNEL_ID + " LIKE \"" + lastPlayedChannelId + "\" THEN " + TvContract.Broadcast.QUALIFIED_CHANNEL_ID + " END DESC, "
                + TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.POSITION + " ASC LIMIT " + count;

        return getBroadcastsByGenre(selection, sortOrder, genreList, favorite);
    }

    /**
     * @return Next broadcasts filtered by the favorite arguments.
     */
    public List<IBroadcast> getNextBroadcasts(String favorite, int count) {
        String selection = TvContract.Broadcast.START + " > " + System.currentTimeMillis()
                + " AND " + TvContract.Channel.TYPE + " <> " + ChannelType.APP.ordinal()
                + " AND " + TvContract.Channel.IS_USER_TV_SUBSCRIBED_CHANNEL + " = 1"
                + " GROUP BY " + TvContract.Broadcast.QUALIFIED_CHANNEL_ID
                + " HAVING " + TvContract.Broadcast.START + " = MIN(" + TvContract.Broadcast.START + ")";
        String sortOrder = TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.POSITION + " ASC LIMIT " + count;

        if (favorite != null && !favorite.isEmpty()) {
            selection = applyFavoriteFilterOnBroadcasts(Boolean.parseBoolean(favorite)) + selection;
        }

        return getBroadcastsByQueryParams(TvContract.Broadcast.CONTENT_URI, selection, null, sortOrder);
    }

    /**
     * @return Upcoming broadcasts filtered by the data source parameters between the {@param from} and {@param to} frame.
     */
    public List<IBroadcast> getUpcomingBroadcasts(String channelId, long from, long to, boolean hasPlayableFilter, int limit) {
        Uri uri = !TextUtils.isEmpty(channelId) ? TvContract.buildBroadcastUriForChannel(channelId) : TvContract.Broadcast.CONTENT_URI;

        StringBuilder whereStringBuilder = new StringBuilder();
        appendWhereStatement(whereStringBuilder,
                TvContract.Broadcast.END + " >= " + from + " AND " + TvContract.Broadcast.START + " < " + to);

        long currentTime = System.currentTimeMillis();
        if (hasPlayableFilter) {
            appendWhereStatement(whereStringBuilder,
                    TvContract.Channel.IS_REPLAY_ENABLED + " > 0 OR ("
                            + TvContract.Broadcast.START + " <= " + currentTime
                            + " AND " + TvContract.Broadcast.END + " >= " + currentTime + ")");
        }

        String sortOrder = TvContract.Broadcast.START + " asc " + (limit > 0 ? " limit " + limit : "");

        return getBroadcastsByQueryParams(uri, whereStringBuilder.toString(), null, sortOrder);
    }

    /**
     * @return Past broadcasts in the channel with {@param channelId} until {@param to}.
     */
    public List<IBroadcast> getPreviousBroadcasts(String channelId, long to) {
        Uri uri = TvContract.buildBroadcastUriForChannel(channelId);
        String selection = TvContract.Broadcast.END + " <= " + to;
        String sortOrder = TvContract.Broadcast.START + " desc limit 1";

        return getBroadcastsByQueryParams(uri, selection, null, sortOrder);
    }

    /**
     * @return Next broadcasts filtered by the favorite arguments.
     */
    public List<IBroadcast> getNextBroadcasts(String channelId, long from) {
        Uri uri = TvContract.buildBroadcastUriForChannel(channelId);
        String selection = TvContract.Broadcast.START + " >= " + from;
        String sortOrder = TvContract.Broadcast.START + " asc limit 1";

        return getBroadcastsByQueryParams(uri, selection, null, sortOrder);
    }

    /**
     * @return List of broadcast between the given time frame.
     */
    public List<IBroadcast> getEpgBroadcasts(String channelId, long from, long to) {
        Uri uri = TvContract.buildBroadcastUriForChannel(channelId);
        String selection = TvContract.Broadcast.END + " >= " + from + " and " + TvContract.Broadcast.START + " <= " + to;
        String sortOrder = TvContract.Broadcast.START + " asc";

        return getBroadcastsByQueryParams(uri, selection, null, sortOrder);
    }

    /**
     * Quarry the currently running broadcast on a given channel.
     *
     * @param channelId The id of the channel from which the broadcast should be returned
     * @return The currently running broadcast on the channel.
     */
    @Nullable
    public IBroadcast getNowBroadcastByChannelId(String channelId) {
        long now = System.currentTimeMillis();
        String selection = "( "
                + TvContract.Channel.TYPE + " = " + ChannelType.TV.ordinal() + " OR "
                + TvContract.Channel.TYPE + " = " + ChannelType.RADIO.ordinal() + " ) AND "
                + TvContract.Broadcast.END + " >= " + now + " AND "
                + TvContract.Broadcast.START + " < " + now + " AND "
                + TvContract.Broadcast.QUALIFIED_CHANNEL_ID + "=?";
        String sort = TvContract.Broadcast.START + " asc limit 1";

        Log.d(TAG, "Selection now event: " + selection);

        return SqlUtils.queryFirst(mApp,
                TvContract.Broadcast.CONTENT_URI,
                BroadcastProjection.PROJECTION,
                selection,
                new String[]{channelId},
                sort,
                BroadcastProjection::readRow
        );
    }

    /**
     * @return a pair of long values, first = the start of the first broadcast, second is the
     * end time of the last broadcast, returns null if table is empty
     */
    @Nullable
    public Range<Long> getCachedBroadcastInterval() {
        String[] projection = new String[]{
                "min(" + TvContract.Broadcast.START + ") ",
                "max(" + TvContract.Broadcast.END + ") "
        };

        Uri uri = BaseContract.withGroupBy(TvContract.Broadcast.CONTENT_URI, TvContract.Broadcast.QUALIFIED_CHANNEL_ID);
        return SqlUtils.queryFirst(mApp, uri, projection, cursor -> {
            long finalStart = cursor.getLong(0);
            long finalEnd = cursor.getLong(1);

            while (cursor.moveToNext()) {
                long start = cursor.getLong(0);
                long end = cursor.getLong(1);

                if (start > finalStart) {
                    finalStart = start;
                }

                if (end < finalEnd) {
                    finalEnd = end;
                }
            }

            return Range.create(finalStart, finalEnd);
        });
    }

    /**
     * Update the EPG cache with the given broadcast
     */
    public void updateBroadcasts(List<? extends IBroadcast> broadcasts, long start, long end) {
        TimerLog timerLog = new TimerLog(TAG);

        ContentValues[] values = BroadcastProjection.toContentValues(broadcasts);
        timerLog.d("UpdateBroadcasts - content values prepared. " + values.length);

        // Insert or replace new values and do not purge old ones
        ContentResolver cr = mApp.getContentResolver();
        SqlUtils.bulkReplace(cr, TvContract.Broadcast.CONTENT_URI, values);

        // Purge the broadcasts which are not contained in the epg window
        String where = TvContract.Broadcast.END + "<" + start
                + " OR " + TvContract.Broadcast.START + ">" + end;
        cr.delete(TvContract.Broadcast.CONTENT_URI, where, null);

        timerLog.d("UpdateBroadcasts - update finished");
    }

    /**
     * Update the EPG cache totally, the old data is deleted
     *
     * @param broadcasts data from BE
     * @param updateTime time of update,
     *                   so old data will be deleted
     */
    public void updateBroadcastsTotally(List<? extends IBroadcast> broadcasts, long updateTime) {
        TimerLog timerLog = new TimerLog(TAG);
        ContentValues[] values = BroadcastProjection.toContentValues(broadcasts);
        timerLog.d("Update broadcasts totally - content values prepared.");

        ContentResolver cr = mApp.getContentResolver();

        // Insert or replace new values and do not purge old ones
        SqlUtils.bulkReplace(cr, TvContract.Broadcast.CONTENT_URI, updateTime, values);

        timerLog.d("Update broadcasts totally - update finished");
    }


    // ----------------- ContinueWatchingBroadcast


    /**
     * @param broadcastId The unique identifier of the replay broadcast.
     * @return The continue watching broadcast, or null if there is no such replay.
     */
    @Nullable
    public IBroadcast getContinueWatchingReplay(String broadcastId) {
        String selection = TvContract.ContinueWatchingBroadcast.EVENT_ID + " =? ";

        return SqlUtils.queryFirst(mApp,
                TvContract.ContinueWatchingBroadcast.CONTENT_URI,
                ContinueWatchingBroadcastProjection.PROJECTION,
                selection,
                new String[]{broadcastId},
                BroadcastProjection::readRow
        );
    }

    /**
     * Add replay to the continue watching list.
     *
     * @param broadcast The replay broadcast which needs to be added.
     */
    public void addReplayContinueWatching(IBroadcast broadcast) {
        ContentValues[] values = BroadcastProjection.toContentValues(Collections.singletonList(broadcast));
        SqlUtils.bulkReplace(mApp.getContentResolver(), TvContract.ContinueWatchingBroadcast.CONTENT_URI, values);
    }

    /**
     * Delete previously saved replay continue watching item.
     *
     * @param broadcast The replay broadcast which needs to be removed.
     */
    public void deleteReplayContinueWatching(IBroadcast broadcast) {
        String selection = TvContract.ContinueWatchingBroadcast.EVENT_ID + " = ? ";
        mApp.getContentResolver().delete(TvContract.ContinueWatchingBroadcast.CONTENT_URI,
                selection, new String[]{broadcast.getId()});
    }

    /**
     * Deletes the continue watching items older than the configured value (e.g 10 days)
     */
    public void deleteOlderContinueWatchingReplay(long olderThan) {
        String selection = BaseContract.LAST_UPDATED + " <= ? ";
        mApp.getContentResolver().delete(BaseContract.withBatchUpdate(
                TvContract.ContinueWatchingBroadcast.CONTENT_URI), selection,
                new String[]{String.valueOf(olderThan)});
    }


    // ----------------- Favorite

    /**
     * This method will synchronise the favorite channels with the local db and with the the BEP.
     *
     * @throws OperationApplicationException, RemoteException in case of error
     */
    public void synchronizeFavoriteChannels(Set<String> addChannel, Set<String> removeList) throws OperationApplicationException, RemoteException {
        ArrayList<ContentProviderOperation> operationList = new ArrayList<>();

        if (removeList != null && !removeList.isEmpty()) {
            String selection = SqlUtils.buildBinding(TvContract.Favorites.CHANNEL_ID, "IN", removeList.size());
            String[] selectionArgs = ArrayUtils.toArray(removeList);

            ContentProviderOperation contentProviderOperation = ContentProviderOperation.newDelete(
                    BaseContract.withBatchUpdate(TvContract.Favorites.CONTENT_URI))
                    .withSelection(selection, selectionArgs)
                    .build();
            operationList.add(contentProviderOperation);

            removeList.clear();

            mApp.getContentResolver().delete(TvContract.Favorites.CONTENT_URI, selection, selectionArgs);
        }

        if (addChannel != null && !addChannel.isEmpty()) {
            operationList.addAll(createInsertOperations(addChannel));
            addChannel.clear();
        }

        mApp.getContentResolver().applyBatch(TvContract.AUTHORITY, operationList);
    }

    /**
     * Update Favorite Channels Ids with the given new channel ids.
     */
    public void updateFavoriteChannels(List<String> channelIds) {
        long timestamp = System.currentTimeMillis();
        List<ContentValues> channelContentList = new ArrayList<>();

        for (String channelId : channelIds) {
            ContentValues values = new ContentValues();
            values.put(TvContract.Favorites.CHANNEL_ID, channelId);
            values.put(TvContract.Favorites.POSITION, channelContentList.size());
            values.put(TvContract.Favorites.LAST_UPDATED, timestamp);
            channelContentList.add(values);
        }

        SqlUtils.bulkReplace(mApp.getContentResolver(), TvContract.Favorites.CONTENT_URI, timestamp, channelContentList);
    }

    // ----------------- Advertisement

    public List<TvAdvertisement> getAllAdvertisements() {
        return SqlUtils.queryAll(mApp,
                TvContract.Advertisement.CONTENT_URI,
                AdvertisementProjection.PROJECTION,
                AdvertisementProjection::readRow
        );
    }

    public List<TvAdvertisement> getAdvertisements(boolean onMarker) {
        return SqlUtils.queryAll(mApp,
                TvContract.Advertisement.CONTENT_URI,
                AdvertisementProjection.PROJECTION,
                TvContract.Advertisement.ON_MARKER + (onMarker ? "!=0" : "=0"),
                AdvertisementProjection::readRow
        );
    }

    @Nullable
    public TvAdvertisement getAdvertisement(String id) {
        List<TvAdvertisementDBModel> result = SqlUtils.queryAll(mApp,
                TvContract.Advertisement.CONTENT_URI,
                AdvertisementProjection.PROJECTION,
                TvContract.Advertisement.CREATIVE_ID + "=?",
                new String[]{id},
                AdvertisementProjection::readRow
        );
        if (result.size() != 1) {
            return null;
        }
        return result.get(0);
    }

    public long getLastUpdateTime(long defValue) {
        try {
            return SqlUtils.queryLong(mApp,
                    TvContract.Advertisement.CONTENT_URI,
                    "MIN(" + TvContract.Advertisement.LAST_UPDATED + ")",
                    null, null, null
            );
        } catch (Exception e) {
            Log.d(TAG, "Failed to query last updated field", e);
        }
        return defValue;
    }

    public void updateAdvertisements(Collection<? extends TvAdvertisement> advertisements) {
        long now = System.currentTimeMillis();
        ArrayList<ContentValues> values = new ArrayList<>(advertisements.size());
        Set<String> preDownloadedIds = new HashSet<>();
        Map<String, Integer> attemptMap = getAdvertisementAttempts();

        for (TvAdvertisement ad : advertisements) {
            values.add(AdvertisementProjection.toContentValue(ad, now, false, attemptMap.getOrDefault(ad.getCreativeId(), 0)));
            preDownloadedIds.add(ad.getCreativeId());
        }

        // keep downloaded on marker ad downloads
        File path = new File(TvProxy.TAD_DOWNLOAD_DIR);
        for (TvAdvertisement ad : getAdvertisements(true)) {
            if (preDownloadedIds.contains(ad.getCreativeId())) {
                // on marker ad is also a pre download ad, keep it as it is
                continue;
            }

            File file = new File(path, ad.getFilename());
            if (!file.exists()) {
                // on marker ad was deleted, remove from database
                continue;
            }
            values.add(AdvertisementProjection.toContentValue(ad, now, true));
        }

        ContentResolver resolver = mApp.getContentResolver();
        int rows = SqlUtils.bulkReplace(resolver, TvContract.Advertisement.CONTENT_URI, now, values);
        Log.d(TAG, "Update advertisements: modified " + rows + " rows.");
    }

    public void insertAdvertisements(List<? extends TvAdvertisement> ads) {
        long now = System.currentTimeMillis();
        ArrayList<ContentValues> values = new ArrayList<>(ads.size());
        for (TvAdvertisement ad : ArrayUtils.notNull(ads)) {
            values.add(AdvertisementProjection.toContentValue(ad, now, true));
        }
        ContentResolver resolver = mApp.getContentResolver();
        int rows = SqlUtils.bulkInsert(resolver, TvContract.Advertisement.CONTENT_URI, values);
        Log.d(TAG, "Update advertisements: modified " + rows + " rows.");
    }


    // ----------------- private methods


    // Tv - Channels

    private TvChannel getTvChannelByQueryParams(String selection, String[] selectionArgs, String order) {
        return SqlUtils.queryFirst(mApp,
                TvContract.Channel.CONTENT_URI,
                ChannelProjection.PROJECTION,
                selection,
                selectionArgs,
                order,
                ChannelProjection::readRow);
    }

    private List<TvChannel> getTvChannelsByQueryParams(@NonNull Uri uri, String selection, String[] selectionArgs, String order) {
        return SqlUtils.queryAll(mApp,
                uri,
                ChannelProjection.PROJECTION,
                selection,
                selectionArgs,
                order,
                ChannelProjection::readRow
        );
    }

    private String applyTypeFilter(@NonNull String typeFilter) {
        String selection = "";

        switch (typeFilter) {
            case ModuleFilterOption.Channel.Type.Value.FAVORITE:
                selection = TvContract.Favorites.TABLE_NAME + "." + TvContract.Favorites.POSITION + " IS NOT NULL";
                break;
            case ModuleFilterOption.Channel.Type.Value.RADIO:
                selection = TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.TYPE + "=" + ChannelType.RADIO.ordinal();
                break;
            case ModuleFilterOption.Channel.Type.Value.APP:
                selection = TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.TYPE + "=" + ChannelType.APP.ordinal();
                break;
            default:
                break;
        }

        return selection;
    }

    private String applySorting(String sorting) {
        StringBuilder sortOrder = new StringBuilder();
        switch (sorting) {

            case ModuleSortOption.Channel.FAVORITE:
                sortOrder.append(TvContract.Channel.TABLE_NAME).append(".").append(TvContract.Channel.NAME);
                DatabaseUtils.appendEscapedSQLString(sortOrder.append('='), TvContract.Channel.NETFLIX_CHANNEL_NAME);
                sortOrder.append("DESC,").append(TvContract.Favorites.TABLE_NAME).append(".").append(TvContract.Favorites.POSITION);
                break;

            case ModuleSortOption.Channel.RANDOM:
                sortOrder.append("RANDOM()");
                break;

            default:
                sortOrder.append(TvContract.Channel.TABLE_NAME).append(".").append(TvContract.Channel.POSITION);
                break;
        }
        return sortOrder.toString();
    }

    /**
     * Method used for escape the search term in a form what we can use for the selection arguments.
     */
    private String escapeSearchTerm(String s) {
        StringBuilder result = new StringBuilder(s.length());
        result.append('%');
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (c == '%' || c == '_' || c == ESCAPE_CHR) {
                result.append(ESCAPE_CHR);
            }
            result.append(c);
        }
        result.append('%');
        return result.toString();
    }

    // Broadcasts

    private List<IBroadcast> getBroadcastsByQueryParams(Uri uri, String selection, String[] selectionArgs, String sortOrder) {
        List<IBroadcast> tvBroadcasts = new ArrayList<>();
        ContentResolver contentResolver = mApp.getContentResolver();
        try (Cursor cursor = contentResolver.query(
                uri, BroadcastProjection.PROJECTION, selection, selectionArgs, sortOrder)) {
            if (cursor != null && cursor.moveToFirst()) {
                IBroadcast broadcast = null;
                boolean isPreviousLive = false;
                do {
                    if (broadcast != null) {
                        isPreviousLive = broadcast.isLive();
                    }
                    broadcast = BroadcastProjection.readRow(cursor, isPreviousLive);
                    tvBroadcasts.add(broadcast);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to get the broadcasts.", e);
        }

        return tvBroadcasts;
    }

    private List<IBroadcast> getBroadcastsByGenre(String selection, String sortOrder, List<String> genreList, String favorite) {
        String[] selectionArgs = new String[]{};

        if (genreList != null && !genreList.isEmpty()) {
            Pair<String, String[]> genreSelectionPair = applyGenreFilterOnBroadcasts(genreList);

            selection = genreSelectionPair.first + selection;
            selectionArgs = genreSelectionPair.second;
        }

        if (favorite != null && !favorite.isEmpty()) {
            String favoriteSelection;

            favoriteSelection = applyFavoriteFilterOnBroadcasts(Boolean.parseBoolean(favorite));
            selection = favoriteSelection + selection;
        }

        return getBroadcastsByQueryParams(TvContract.Broadcast.CONTENT_URI, selection, selectionArgs, sortOrder);
    }

    private void appendWhereStatement(StringBuilder whereStringBuilder, String statement) {
        whereStringBuilder.append(whereStringBuilder.toString().length() > 0 ? " and " : "").append(statement);
    }

    /**
     * Get the genre filter selection and selection arguments based on the given genre list.
     * The selection is built the list of arguments that are going to be replaced with the selection arguments.
     *
     * @param genreList List of genres to check
     * @return A pair of selection string and selection arguments list.
     */
    private Pair<String, String[]> applyGenreFilterOnBroadcasts(@NonNull List<String> genreList) {
        String genreSelection = SqlUtils.buildBinding(TvContract.Broadcast.GENRES, "IN", genreList.size()) + " COLLATE NOCASE AND ";
        return new Pair<>(genreSelection, genreList.toArray(new String[0]));
    }

    /**
     * Apply the favorite filtering based on the favorite filter.
     * If the filter has a value true or false, it will add the favorite filter to the selection.
     *
     * @param favorite boolean value of favorite flag
     * @return The selection for the favorite filter.
     */
    private String applyFavoriteFilterOnBroadcasts(boolean favorite) {
        String favoriteSelection;

        if (favorite) {
            favoriteSelection = TvContract.Favorites.TABLE_NAME + "." + TvContract.Favorites.POSITION + " IS NOT NULL AND ";
        } else {
            favoriteSelection = TvContract.Favorites.TABLE_NAME + "." + TvContract.Favorites.POSITION + " IS NULL AND ";
        }

        return favoriteSelection;
    }

    // Favorite

    private List<ContentProviderOperation> createInsertOperations(Set<String> channelSet) {
        String[] projection = new String[]{"max(" + TvContract.Favorites.TABLE_NAME + "." + TvContract.Favorites.POSITION + ") "};
        Log.d(TAG, "Insert: " + Arrays.toString(projection));

        int favPos = Optional.ofNullable(SqlUtils.queryFirst(mApp,
                TvContract.Favorites.CONTENT_URI,
                projection,
                cursor -> cursor.getInt(0) + 1
        )).orElse(0);

        List<ContentProviderOperation> contentProviderOperations = new ArrayList<>();
        long time = System.currentTimeMillis();
        for (String channelId : channelSet) {
            ContentValues values = new ContentValues();
            values.put(TvContract.Favorites.CHANNEL_ID, channelId);
            values.put(TvContract.Favorites.POSITION, favPos);
            values.put(TvContract.Favorites.LAST_UPDATED, time);
            favPos++;
            ContentProviderOperation operation = ContentProviderOperation.newInsert(BaseContract.withBatchUpdate(TvContract.Favorites.CONTENT_URI))
                    .withValues(values).build();
            contentProviderOperations.add(operation);
        }
        return contentProviderOperations;
    }

    // TAD

    private Map<String, Integer> getAdvertisementAttempts() {
        Map<String, Integer> attempts = SqlUtils.queryFirst(mApp,
                TvContract.Advertisement.CONTENT_URI,
                AdvertisementProjection.PROJECTION_ATTEMPTS,
                cursor -> {
                    HashMap<String, Integer> attemptMap = new HashMap<>();
                    do {
                        String key = cursor.getString(0);
                        Integer count = cursor.getInt(1);
                        attemptMap.put(key, count);
                    } while (cursor.moveToNext());
                    return attemptMap;
                }
        );
        return ArrayUtils.notNull(attempts);
    }
}
