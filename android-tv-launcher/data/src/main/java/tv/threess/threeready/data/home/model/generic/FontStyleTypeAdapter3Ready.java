package tv.threess.threeready.data.home.model.generic;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import tv.threess.threeready.api.home.model.generic.FontStyle;

/**
 * Json type adapter to read value as font style enum.
 *
 * @author Barabas Attila
 * @since 5/13/21
 */
public class FontStyleTypeAdapter3Ready extends TypeAdapter<FontStyle> {
    private static final String DEFAULT = "default";
    private static final String PROXIMUS = "proximus";

    @Override
    public void write(JsonWriter out, FontStyle value) throws IOException {
        if (value == FontStyle.PROXIMUS) {
            out.value(PROXIMUS);
        } else {
            out.value(DEFAULT);
        }
    }

    @Override
    public FontStyle read(JsonReader in) throws IOException {
        String value = in.nextString();
        if (PROXIMUS.equals(value)) {
            return FontStyle.PROXIMUS;
        }
        return FontStyle.DEFAULT;
    }
}