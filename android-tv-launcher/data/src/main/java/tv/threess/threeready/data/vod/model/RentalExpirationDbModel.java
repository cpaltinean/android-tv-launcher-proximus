package tv.threess.threeready.data.vod.model;

import tv.threess.threeready.api.vod.model.IRentalExpiration;

/**
 * Proximus db model for rental expiration info.
 *
 * Created by Noemi Dali on 27.04.2020.
 */
public class RentalExpirationDbModel implements IRentalExpiration {

    private String mVariantId;
    private long mExpirationTime;
    private long mRentalTime;

    @Override
    public long getExpireTime() {
        return mExpirationTime;
    }

    @Override
    public long getRentalTime() {
        return mRentalTime;
    }

    @Override
    public String getVariantId() {
        return mVariantId;
    }

    public static class Builder {

        RentalExpirationDbModel mRentalExpiration = new RentalExpirationDbModel();

        public static Builder builder() {
            return new Builder();
        }

        public Builder setVariantId(String variantId) {
            mRentalExpiration.mVariantId = variantId;
            return this;
        }

        public Builder setExpirationTime(long expirationTime) {
            mRentalExpiration.mExpirationTime = expirationTime;
            return this;
        }

        public Builder setRentalTime(long rentalTime) {
            mRentalExpiration.mRentalTime = rentalTime;
            return this;
        }

        public RentalExpirationDbModel build() {
            return mRentalExpiration;
        }
    }
}
