package tv.threess.threeready.data.netflix.receiver;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import java.util.Objects;

import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.log.Log;

/**
 * Receiver class used for get the Netflix DET Token.
 * If the Token is changed (exp: netflix user changed) we have to update the Netflix content stripe(s).
 *
 * @author Barabas Attila
 * @since 2/1/22
 */
public class NetflixDETTokenReceiver extends BaseNetflixDETReceiver {
    private static final String TAG = Log.tag(NetflixDETTokenReceiver.class);

    public static final String ACTION_DET_TOKEN_REQUEST = "com.netflix.ninja.intent.action.DET_TOKEN_REQUEST";
    public static final String EXTRA_DET_RESPONSE_TOKEN = "token";

    public static void requestDETToken(Context context) {
        Intent intent = new Intent();
        intent.setAction(ACTION_DET_TOKEN_REQUEST);
        intent.setPackage(PackageUtils.PACKAGE_NAME_NETFLIX);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        context.sendBroadcast(intent, NETFLIX_DET_PERMISSION);

        Log.d(TAG, "Send Netflix DET toke request. " + intent);
    }

    @Override
    public void handleIntent(Context context, Intent intent) {
        String receivedDETToken = intent.getStringExtra(EXTRA_DET_RESPONSE_TOKEN);

        if (TextUtils.isEmpty(receivedDETToken)) {
            return;
        }

        Log.d(TAG, "Netflix DET token response received: " + receivedDETToken);

        String settingsDETToken = Settings.netflixDETToken.get(null);

        if (Objects.equals(receivedDETToken, settingsDETToken)) {
            return;
        }

        sWorkerHandler.post(() -> {
            Settings.netflixDETToken.edit().put(receivedDETToken);
            mCache.resetNetflixDETExpirationTime();
        });
    }
}
