package tv.threess.threeready.data.tv.observable;

import android.content.Context;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.ObservableEmitter;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountCache;
import tv.threess.threeready.api.generic.model.BookmarkType;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.api.tv.model.LiveBookmark;
import tv.threess.threeready.data.generic.observable.BaseBookmarkObservable;
import tv.threess.threeready.data.pvr.PvrContract;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * RX observable that reacts for the last viewed position of a {@link IBroadcast}
 *
 * @author Denisa Trif
 * @since 2019.01.10
 */
public class BroadcastBookmarkObservable extends BaseBookmarkObservable<IBroadcast> {

    private final AccountCache.Bookmarks mAccountCache = Components.get(AccountCache.Bookmarks.class);
    private final boolean mContinueWatch;

    private Disposable mRescheduleDisposable;

    public BroadcastBookmarkObservable(Context context, IBroadcast broadcast, boolean continueWatch) {
        super(context, broadcast);
        mContinueWatch = continueWatch;
    }

    @Override
    public void subscribe(ObservableEmitter<IBookmark> e) throws Exception {
        super.subscribe(e);

        registerObserver(PvrContract.buildRecordingUriForBroadcast(mContentItem));
    }

    @Override
    protected void publishBookmark() {
        super.publishBookmark();

        if (mContentItem.isPast()) {
            return;
        }

        long scheduleDate = mContentItem.isLive() ? mContentItem.getEnd() : mContentItem.getStart();
        long delay = scheduleDate - System.currentTimeMillis();

        if (delay > 0 && !mEmitter.isDisposed()) {
            RxUtils.disposeSilently(mRescheduleDisposable);

            mRescheduleDisposable = Completable.fromAction(this::publishBookmark)
                    .delaySubscription(delay, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe();

        }
    }

    @Override
    protected IBookmark getBookmark() {
        List<IBookmark> bookmarks = new ArrayList<>();
        IBookmark recordingBookmark = getRecordingBookmark();
        IBookmark liveBookmark = getLiveBookmark();
        IBookmark replayBookmark = getReplayBookmark();

        if(recordingBookmark != null) {
            bookmarks.add(recordingBookmark);
        }
        if(replayBookmark != null) {
            bookmarks.add(replayBookmark);
        }
        if(!mContinueWatch && liveBookmark != null){
            bookmarks.add(liveBookmark);
        }

        return getLatestBookmark(bookmarks);
    }

    @Nullable
    private IBookmark getReplayBookmark() {
        return mAccountCache.getBookmark(mContentItem.getId(), BookmarkType.Replay);
    }

    @Nullable
    private IBookmark getLiveBookmark() {
        if (mContentItem == null) {
            return null;
        }

        if(!mContentItem.isLive()){
            return null;
        }

        return new LiveBookmark(mContentItem);
    }

    @Nullable
    private IBookmark getRecordingBookmark() {
        return mAccountCache.getBookmark(mContentItem.getId(), BookmarkType.Recording);
    }

    /**
     * Decides which bookmark to publish based on the timestamp
     * @param bookmarks List of bookmarks.
     */
    @Nullable
    private IBookmark getLatestBookmark(List<IBookmark> bookmarks) {
        IBookmark latestBookmark = null;
        for (IBookmark bookmark : bookmarks) {
            if (bookmark != null) {
                if (latestBookmark == null
                        || bookmark.getTimestamp() >= latestBookmark.getTimestamp()) {
                    latestBookmark = bookmark;
                }
            }
        }
        return latestBookmark;
    }

    @Override
    protected void unregisterObserver() {
        super.unregisterObserver();
        RxUtils.disposeSilently(mRescheduleDisposable);
    }
}
