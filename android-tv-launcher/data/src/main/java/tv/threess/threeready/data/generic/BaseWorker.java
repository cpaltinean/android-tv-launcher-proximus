package tv.threess.threeready.data.generic;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.work.ForegroundInfo;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.R;

/**
 * Base class of the jobs that needs to be executed in the background.
 */
public abstract class BaseWorker extends Worker {

    private static final String TAG = Log.tag(BaseWorker.class);

    private final int mForeground;

    public BaseWorker(Context context, int foreground, WorkerParameters workerParams) {
        super(context, workerParams);
        mForeground = foreground;
    }

    public BaseWorker(Context context, WorkerParameters workerParams) {
        this(context, Integer.MIN_VALUE, workerParams);
    }

    protected abstract void executeJob() throws Exception;

    @Override
    public final Result doWork() {
        String action = getClass().getName();
        String receiver = getStringInputData(BaseWorkManager.KEY_INTENT_EXTRA_RECEIVER);
        Log.v(TAG, "Service started with action: " + action + ", receiver: " + receiver);
        try {
            if (mForeground != Integer.MIN_VALUE) {
                setForeground(mForeground);
            }
            executeJob();
            Log.v(TAG, "Service finished action: " + action);
            if (receiver != null) {
                Intent result = new Intent(receiver);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(result);
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to perform action: " + action, e);
            if (receiver != null) {
                Intent result = new Intent(receiver);
                result.putExtra(BaseWorkManager.KEY_INTENT_EXTRA_ERROR, e.getMessage());
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(result);
            }
        }
        return Result.success();
    }

    private void setForeground(int notificationId) {
        Context context = getApplicationContext();
        String id = context.getString(R.string.notification_channel_id);
        String title = context.getString(R.string.notification_title);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelName = context.getString(R.string.notification_channel_name);
            NotificationChannel notificationChannel = new NotificationChannel(id, channelName, NotificationManager.IMPORTANCE_MIN);
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        setForegroundAsync(new ForegroundInfo(notificationId, new NotificationCompat.Builder(context, id)
                .setOngoing(true)
                .setContentTitle(title)
                .setSmallIcon(android.R.color.transparent)
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build()
        ));
    }

    protected String getStringInputData(String key) {
        return getInputData().getString(key);
    }

    protected long getLongInputData(String key, long defaultValue) {
        return getInputData().getLong(key, defaultValue);
    }
}
