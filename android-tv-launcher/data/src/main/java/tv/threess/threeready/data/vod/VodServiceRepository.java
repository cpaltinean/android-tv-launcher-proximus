/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.vod;

import tv.threess.lib.di.Component;

/**
 * Extension methods that are invoked from the vod service to take project specific actions.
 *
 * @author Barabas Attila
 * @since 2018.04.30
 */
public interface VodServiceRepository extends Component {

    /**
     * Update the VOD rental cache.
     * @throws Exception In case of errors.
     */
    void updatePurchases() throws Exception;

    /**
     * Update the VOD category cache.
     * @throws Exception In case of errors.
     */
    void updateVodCategories() throws Exception;

    /**
     * Check if device profile was not changed.
     */
    boolean isDeviceProfileValid();
}
