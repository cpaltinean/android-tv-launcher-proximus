package tv.threess.threeready.data.account;

import android.app.Application;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.account.AccountCache;
import tv.threess.threeready.api.account.setting.SettingProperty;
import tv.threess.threeready.api.generic.JsonConverter;
import tv.threess.threeready.api.generic.helper.BundleBuilder;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.utils.GsonUtils;
import tv.threess.threeready.data.utils.SqlUtils;

/**
 * Access proxy to read and write settings related values.
 */
public class SettingsAccessProxy implements AccountCache.Settings, AccountCache.MemoryCache, AccountCache.MassStorage, JsonConverter, Component {
    private static final String TAG = Log.tag(SettingsAccessProxy.class);

    private static final String COLUMN_KEY = "_id";
    private static final String COLUMN_VALUE = "value";

    private final Application mApp;

    public SettingsAccessProxy(Application mApp) {
        this.mApp = mApp;
    }

    @Override
    public <T extends Enum<T> & SettingProperty> String getSetting(T key) {
        return getSetting(ConfigContract.Settings.CONTENT_URI, ConfigContract.Settings.CALL_GET_SETTINGS_DATA, key);
    }

    @Override
    public <T extends Enum<T> & SettingProperty> int putSettings(Map<T, String> values) {
        long now = System.currentTimeMillis();
        ArrayList<ContentValues> contentValues = new ArrayList<>(values.size());
        for (Map.Entry<T, String> val : values.entrySet()) {
            String name = val.getKey().getName();
            String value = val.getValue();

            ContentValues row = new ContentValues(3);
            row.put(ConfigContract.Settings.KEY, name);
            if (value != null) {
                row.put(ConfigContract.Settings.VALUE, value);
            } else {
                row.putNull(ConfigContract.Settings.VALUE);
            }
            row.put(ConfigContract.Settings.LAST_UPDATED, now);
            contentValues.add(row);
        }
        return SqlUtils.bulkReplace(mApp.getContentResolver(), ConfigContract.Settings.CONTENT_URI, contentValues);
    }

    /**
     * increment and return the value of a given setting (used to increment eventCounter)
     */
    public <T extends Enum<T> & SettingProperty> long incSetting(T key) {
        ContentResolver cr = mApp.getContentResolver();
        Bundle result = cr.call(ConfigContract.Settings.CONTENT_URI, ConfigContract.Settings.CALL_INC_SETTINGS_DATA, key.getName(), new BundleBuilder()
                .put(ConfigContract.Settings.LAST_UPDATED, System.currentTimeMillis())
                .put(key.getName(), 1L)
                .build()
        );
        return result != null ? result.getLong(key.getName()) : 0;
    }


    /**
     * @return the value for the the given session variable
     */
    public <T extends Enum<T> & SettingProperty> String getSession(T key) {
        return getSetting(ConfigContract.Session.CONTENT_URI, ConfigContract.Session.CALL_GET_SESSION_DATA, key);
    }

    /**
     * persist the value for the given session variable
     */
    public <T extends Enum<T> & SettingProperty> void putSession(Map<T, String> values) {
        long now = System.currentTimeMillis();
        ArrayList<ContentValues> contentValues = new ArrayList<>(values.size());
        for (Map.Entry<T, String> val : values.entrySet()) {
            String name = val.getKey().getName();
            String value = val.getValue();

            ContentValues row = new ContentValues(3);
            row.put(ConfigContract.Session.KEY, name);
            if (value != null) {
                row.put(ConfigContract.Session.VALUE, value);
            } else {
                row.putNull(ConfigContract.Session.VALUE);
            }
            row.put(ConfigContract.Session.LAST_UPDATED, now);
            contentValues.add(row);
        }
        SqlUtils.bulkReplace(mApp.getContentResolver(), ConfigContract.Session.CONTENT_URI, contentValues);
    }

    @Override
    public <T extends Enum<T> & SettingProperty> String getMemoryCache(T key) {
        return getSetting(ConfigContract.Settings.CONTENT_URI, ConfigContract.Settings.CALL_GET_CACHE_DATA, key);
    }

    @Override
    public <T extends Enum<T> & SettingProperty> void putMemoryCache(T key, String value) {
        mApp.getContentResolver().call(
                ConfigContract.Settings.CONTENT_URI, ConfigContract.Settings.CALL_PUT_CACHE_DATA,
                key.getName(), new BundleBuilder().put(key.getName(), value).build()
        );
    }

    @Override
    public <T extends Enum<T> & SettingProperty> String getMassStorage(T key) {
        return getSetting(ConfigContract.Settings.CONTENT_URI, MassStorageContract.CALL_GET_DATA, key);
    }

    @Override
    public <T extends Enum<T> & SettingProperty> void putMassStorage(T key, String value) {
        if (value == null) {
            mApp.getContentResolver().call(ConfigContract.Settings.CONTENT_URI,
                    MassStorageContract.CALL_DELETE_DATA, key.getName(), Bundle.EMPTY
            );
            return;
        }

        // insert or update
        mApp.getContentResolver().call(ConfigContract.Settings.CONTENT_URI, MassStorageContract.CALL_PUT_DATA, "",
                new BundleBuilder().put(key.getName(), value).build()
        );
    }

    /**
     * @return value read from technicolor provided settings
     */
    public <T extends Enum<T> & SettingProperty> String getTechnicolorSettings(Uri uri, T key) {
        String selection = COLUMN_KEY + "=" + DatabaseUtils.sqlEscapeString(key.getName());
        //TODO: change this to selectionArgs implementation after TCH fixes PROX-7113
        return SqlUtils.queryFirst(mApp, uri, new String[]{COLUMN_VALUE}, selection, null,
                cursor -> cursor.getString(0)
        );
    }

    /**
     * persist the value for the given technicolor provided setting
     */
    public <T extends Enum<T> & SettingProperty> int putTechnicolorSettings(Uri uri, T key, String value) {
        ContentValues contentValues = new ContentValues();
        ContentResolver contentResolver = mApp.getContentResolver();
        contentValues.put(COLUMN_KEY, key.getName());
        contentValues.put(COLUMN_VALUE, value);

        String where = COLUMN_KEY + "=" + DatabaseUtils.sqlEscapeString(key.getName());
        int result = contentResolver.update(uri, contentValues, where, null);
        if (result == 0) {
            result = contentResolver.insert(uri, contentValues) != null ? 1 : 0;
        }
        Log.d(TAG, "Update technicolor settings: " + uri + " " + key + " = " + value);
        return result;
    }

    // json utility functions

    @Override
    public String toJson(Object value) {
        return GsonUtils.getGson().toJson(value);
    }

    @Override
    public <T> T fromJson(String value, Class<T> type) {
        return GsonUtils.getGson().fromJson(value, type);
    }

    @Override
    public List<String> toStringList(String value) {
        return GsonUtils.getGson().fromJson(value, GsonUtils.TYPE_LIST_STRING);
    }

    @Override
    public Map<String, String> toStringMap(String value) {
        return GsonUtils.getGson().fromJson(value, GsonUtils.TYPE_MAP_STRING_2_STRING);
    }

    private String getSetting(Uri uri, String method, SettingProperty key) {
        Bundle result = mApp.getContentResolver().call(uri, method, key.getName(), null);
        if (result != null && result.containsKey(key.getName())) {
            return result.getString(key.getName());
        }
        return null;
    }
}
