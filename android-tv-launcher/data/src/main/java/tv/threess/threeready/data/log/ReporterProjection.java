package tv.threess.threeready.data.log;

import android.database.Cursor;

import androidx.annotation.Nullable;

import tv.threess.threeready.api.log.ReportContract;
import tv.threess.threeready.api.log.Reporter;
import tv.threess.threeready.api.log.model.ReporterInfo;
import tv.threess.threeready.data.generic.IProjection;

/**
 * Reporter info projection enum.
 *
 * @author Zsolt Bokor_
 * @since 2020.05.29
 */
public enum ReporterProjection implements IProjection {
    ID(ReportContract.Reporter.ID),
    ENDPOINT(ReportContract.Reporter.ENDPOINT),
    HEADERS(ReportContract.Reporter.HEADERS),
    MAX_KEEP_PERIOD(ReportContract.Reporter.MAX_KEEP_PERIOD),
    MAX_KEEP_SIZE(ReportContract.Reporter.MAX_KEEP_SIZE),
    MAX_BATCH_SIZE(ReportContract.Reporter.MAX_BATCH_SIZE),
    BATCH_SIZE(ReportContract.Reporter.BATCH_SIZE),
    BATCH_PERIOD(ReportContract.Reporter.BATCH_PERIOD),
    RANDOM_PERIOD(ReportContract.Reporter.RANDOM_PERIOD),
    TIMEOUT_PERIOD(ReportContract.Reporter.TIMEOUT_PERIOD),
    SENDING_DELAY(ReportContract.Reporter.SENDING_DELAY);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    ReporterProjection(String column) {
        this.mColumn = ReportContract.Reporter.TABLE_NAME + "." + column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    /**
     * Return a reporter info from the given cursor.
     *
     * @param cursor Cursor that contains the reporter info.
     * @return Reporter info for a reporter.
     */
    @Nullable
    public static ReporterInfo readRow(Cursor cursor) throws ClassNotFoundException {
        if (cursor == null) {
            return null;
        }

        if (!cursor.moveToFirst()) {
            return null;
        }

        String id = cursor.getString(ID.ordinal());
        return new ReporterInfo.Builder()
                .setReporter((Class<? extends Reporter>) Class.forName(id))
                .setEndpoint(cursor.getString(ENDPOINT.ordinal()))
                .setHeaders(cursor.getString(HEADERS.ordinal()))
                .setMaxKeepPeriod(cursor.getLong(MAX_KEEP_PERIOD.ordinal()))
                .setMaxKeepSize(cursor.getInt(MAX_KEEP_SIZE.ordinal()))
                .setMaxBatchSize(cursor.getInt(MAX_BATCH_SIZE.ordinal()))
                .setBatchSize(cursor.getInt(BATCH_SIZE.ordinal()))
                .setBatchPeriod(cursor.getLong(BATCH_PERIOD.ordinal()))
                .setRandomPeriod(cursor.getLong(RANDOM_PERIOD.ordinal()))
                .setTimeoutPeriod(cursor.getLong(TIMEOUT_PERIOD.ordinal()))
                .setSendingDelay(cursor.getLong(SENDING_DELAY.ordinal()))
                .build();
    }
}
