package tv.threess.threeready.data.account.projection;

import android.database.Cursor;

import tv.threess.threeready.api.account.model.server.IPosterServerInfoField;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.account.model.PosterServerFieldDBModel;
import tv.threess.threeready.data.generic.IProjection;

/**
 * Projection to get allowed image sizes from database.
 *
 * Created by Noemi Dali on 02.04.2020.
 */
public enum PosterServerInfoFieldProjection implements IProjection {
    WIDTH(ConfigContract.PosterServers.WIDTH),
    HEIGHT(ConfigContract.PosterServers.HEIGHT),
    URI(ConfigContract.PosterServers.URL),
    IS_RESIZABLE(ConfigContract.PosterServers.IS_RESIZABLE);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    PosterServerInfoFieldProjection(String column) {
        mColumn = column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    public static IPosterServerInfoField readRow(Cursor cursor) {
        return new PosterServerFieldDBModel.Builder()
                .setWidth(cursor.getFloat(WIDTH.ordinal()))
                .setHeight(cursor.getFloat(HEIGHT.ordinal()))
                .setUri(cursor.getString(URI.ordinal()))
                .setIsResizable(cursor.getInt(IS_RESIZABLE.ordinal()) > 0)
                .build();
    }
}
