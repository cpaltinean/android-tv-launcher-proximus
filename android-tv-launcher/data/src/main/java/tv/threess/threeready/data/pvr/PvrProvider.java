package tv.threess.threeready.data.pvr;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.helper.UriMatcher;
import tv.threess.threeready.data.pvr.PvrContract.SeriesRecording;
import tv.threess.threeready.data.generic.BaseProvider;

/**
 * Content provider to access pvr persisted data.
 *
 * @author Denisa Trif
 * @since 2018.09.14
 */
public class PvrProvider extends BaseProvider {

    enum UriIDs {
        Recordings,
        SeriesRecording
    }

    // prepare the uri matcher
    private static final UriMatcher<PvrProvider.UriIDs> matcher = new UriMatcher<>(PvrContract.AUTHORITY, PvrProvider.UriIDs.class)
        .add(UriIDs.SeriesRecording, SeriesRecording.CONTENT_URI)
        .add(UriIDs.Recordings, PvrContract.Recording.CONTENT_URI);

    private PvrDatabase mDatabase;

    @Override
    public boolean onCreate() {
        super.onCreate();
        // prepare the database helper
        mDatabase = new PvrDatabase(this.getContext());

        return true;
    }


    @Override
    public Bundle call(String method, String arg, Bundle extras) {
        if (DATABASE_VERSION_CHECK.equals(method)) {
            mDatabase.getReadableDatabase();
        }
        return super.call(method, arg, extras);
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs,
        @Nullable String sortOrder) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);

            case SeriesRecording:
                table = PvrContract.SeriesRecording.TABLE_NAME;
                break;
            case Recordings:
                table = PvrContract.Recording.TABLE_NAME;
                break;
        }

        SQLiteDatabase db = mDatabase.getReadableDatabase();
        return super.query(uri, db, table, projection, selection, selectionArgs, sortOrder);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        UriIDs match = matcher.matchEnum(uri);
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);
            case SeriesRecording:
                return SeriesRecording.MIME_TYPE;
            case Recordings:
                return PvrContract.Recording.MIME_TYPE;
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
       UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);
            case SeriesRecording:
                table = SeriesRecording.TABLE_NAME;
                break;
            case Recordings:
                table = PvrContract.Recording.TABLE_NAME;
                break;
        }
        long rowId = super.insert(uri, mDatabase.getWritableDatabase(), table, values);
        return rowId < 0 ? null : uri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);
            case SeriesRecording:
                table = PvrContract.SeriesRecording.TABLE_NAME;
                break;
            case Recordings:
                table = PvrContract.Recording.TABLE_NAME;
                break;
        }

        return super.delete(uri,
            mDatabase.getWritableDatabase(), table, selection, selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);

            case SeriesRecording:
                table = PvrContract.SeriesRecording.TABLE_NAME;
                break;
            case Recordings:
                table = PvrContract.Recording.TABLE_NAME;
                break;

        }

        return super.update(uri, mDatabase.getWritableDatabase(), table, values, selection, selectionArgs);
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);

            case SeriesRecording:
                table = PvrContract.SeriesRecording.TABLE_NAME;
                break;
            case Recordings:
                table = PvrContract.Recording.TABLE_NAME;
                break;
        }

        String replace = uri.getQueryParameter(BaseContract.BULK_REPLACE);
        return bulkInsertReplacePurge(uri,
            mDatabase.getWritableDatabase(), table, replace, values);
    }

    @NonNull
    @Override
    public ContentProviderResult[] applyBatch(@NonNull ArrayList<ContentProviderOperation> operations) throws OperationApplicationException {
        return super.applyBatch(mDatabase.getWritableDatabase(), operations);
    }

}
