/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.search.projection;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.media.tv.TvContract;

import java.util.Arrays;
import java.util.List;

import tv.threess.threeready.data.generic.IProjection;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.data.search.GlobalSearchContract.Program;
import tv.threess.threeready.api.search.model.GlobalSearchType;
import tv.threess.threeready.data.R;
import tv.threess.threeready.ui.generic.utils.TranslationKey;
import tv.threess.threeready.ui.generic.utils.Translator;

/**
 * Projection to display shows in global search.
 *
 * @author Barabas Attila
 * @since 2017.07.13
 */
public enum GlobalSearchProgramProjection implements IProjection {
    ID(Program.ID),
    TITLE(Program.TITLE),
    DESCRIPTION(Program.DESCRIPTION),
    CARD_IMAGE(Program.CARD_IMAGE),
    INTENT_DATA(Program.INTENT_DATA),
    DURATION(Program.DURATION),
    RELEASE_YEAR(Program.RELEASE_YEAR),
    INTENT_DATA_ID(Program.INTENT_DATA_ID);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    GlobalSearchProgramProjection(String column) {
        mColumn = column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    public static Cursor buildCursor(ParentalControlManager parentalControlManager,
                                     Translator translator,
                                     IContentItem... resultArray) {
        return buildCursor(Arrays.asList(resultArray), parentalControlManager, translator);
    }

    public static Cursor buildCursor(List<IContentItem> resultList,
                                     ParentalControlManager parentalControlManager,
                                     Translator translator) {
        MatrixCursor cursor = new MatrixCursor(PROJECTION);

        for (IContentItem result : resultList) {
            boolean contentLocked = parentalControlManager.isRestricted(result);
            Object[] row = new Object[PROJECTION.length];
            row[ID.ordinal()] = result.getId();
            row[TITLE.ordinal()] = contentLocked ? translator.get(TranslationKey.CARDS_TITLE_CONTENT_LOCKED) : result.getTitle();
            row[DESCRIPTION.ordinal()] = contentLocked ? null : result.getReleaseYear();
            row[INTENT_DATA.ordinal()] = result.getId();
            row[DURATION.ordinal()] = contentLocked ? null : result.getDuration();
            row[RELEASE_YEAR.ordinal()] = contentLocked ? null : result.getReleaseYear();
            row[INTENT_DATA.ordinal()] = TvContract.buildChannelUri(0); // Dummy TIF channel id.
            row[INTENT_DATA_ID.ordinal()] = result.getId()+ "/" + result.getParentalRating().getMinimumAge() + "/" + GlobalSearchType.Program.name();
            if (contentLocked) {
                row[CARD_IMAGE.ordinal()] = R.drawable.locked_cover_landscape;
            } else {
                if (result.getImageSources() != null && !result.getImageSources().isEmpty()) {
                    row[CARD_IMAGE.ordinal()] = Program.CONTENT_URI + "/" + result.getImageSources().get(0).getUrl();
                }
            }

            cursor.addRow(row);
        }

        return cursor;
    }

}
