package tv.threess.threeready.data.account.adapter;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.home.model.error.ErrorConfig.DisplayMode;

/**
 * Json type adapter to read error display mode as enum.
 *
 * @author Barabas Attila
 * @since 5/16/21
 */
public class ErrorDisplayModeTypeAdapter extends NameTypeMappingTypeAdapter<DisplayMode> {
    @Override
    protected Map<String, DisplayMode> createNameTypeMap() {
        return new HashMap<String, DisplayMode>() {
            {
                put("alert", DisplayMode.ALERT);
                put("notification", DisplayMode.NOTIFICATION);
                put("playback_error", DisplayMode.PLAYBACK_ERROR);
            }
        };
    }

}
