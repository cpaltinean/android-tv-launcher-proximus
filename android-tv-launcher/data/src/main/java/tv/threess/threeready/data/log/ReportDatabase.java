/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.log;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import tv.threess.threeready.api.log.ReportContract;
import tv.threess.threeready.data.generic.helper.BaseSQLiteOpenHelper;

/**
 * Helper class to manage event database creation
 *
 * @author Arnold Szabo
 * @since 2017.07.19
 */
public class ReportDatabase extends BaseSQLiteOpenHelper {
    // database file name
    private static final String NAME = "event.db";

    // database version number
    private static final int VERSION = 9;

    public ReportDatabase(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + ReportContract.Event.TABLE_NAME + " ("
                + ReportContract.Event.ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + ReportContract.Event.REPORTER_ID + " TEXT NOT NULL,"
                + ReportContract.Event.NAME + " TEXT,"
                + ReportContract.Event.DOMAIN + " TEXT,"
                + ReportContract.Event.TIMESTAMP + " INTEGER,"
                + ReportContract.Event.VALUE + " TEXT,"
                + ReportContract.Event.INCIDENT_COUNT + " INTEGER,"
                + ReportContract.Event.LAST_INCIDENT_TIME + " INTEGER,"
                + ReportContract.Event.LAST_UPDATED + " INTEGER NOT NULL )"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS " + ReportContract.Reporter.TABLE_NAME + " ("
                + ReportContract.Reporter.ID + " TEXT PRIMARY KEY NOT NULL,"
                + ReportContract.Reporter.ENDPOINT + " TEXT,"
                + ReportContract.Reporter.HEADERS + " TEXT,"
                + ReportContract.Reporter.MAX_KEEP_PERIOD + " INTEGER,"
                + ReportContract.Reporter.MAX_KEEP_SIZE + " INTEGER,"
                + ReportContract.Reporter.MAX_BATCH_SIZE + " INTEGER,"
                + ReportContract.Reporter.BATCH_SIZE + " INTEGER,"
                + ReportContract.Reporter.BATCH_PERIOD + " INTEGER,"
                + ReportContract.Reporter.RANDOM_PERIOD + " INTEGER,"
                + ReportContract.Reporter.TIMEOUT_PERIOD + " INTEGER,"
                + ReportContract.Reporter.SENDING_DELAY + " INTEGER )"
        );
    }

    @Override
    public boolean onUpgrade(SQLiteDatabase db, int toVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ReportContract.Event.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ReportContract.Reporter.TABLE_NAME);
        onCreate(db);
        return true;
    }
}
