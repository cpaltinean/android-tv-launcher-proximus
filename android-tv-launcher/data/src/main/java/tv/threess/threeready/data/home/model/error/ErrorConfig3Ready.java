/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.home.model.error;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import tv.threess.threeready.api.home.model.error.ErrorConfig;
import tv.threess.threeready.data.account.adapter.ErrorDisplayModeTypeAdapter;

/**
 * Configuration options to display a generic error message in app.
 *
 * @author Barabas Attila
 * @since 2017.08.29
 */
public class ErrorConfig3Ready implements ErrorConfig {

    @SerializedName("type")
    private String mType;

    @JsonAdapter(ErrorDisplayModeTypeAdapter.class)
    @SerializedName("display")
    private DisplayMode mDisplayMode;

    @SerializedName("priority")
    private int mPriority;

    @Override
    public String getType() {
        return mType;
    }


    @Override
    public DisplayMode getDisplayMode() {
        return mDisplayMode;
    }

    @Override
    public int getPriority() {
        return mPriority;
    }

}
