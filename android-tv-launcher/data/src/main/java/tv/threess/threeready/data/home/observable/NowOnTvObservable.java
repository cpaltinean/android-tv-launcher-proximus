/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.home.observable;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.ObservableEmitter;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * Rx observable class to return the current programs based on the module config.
 * Automatically refresh and emits a new program list when a program expires.
 *
 * @author Barabas Attila
 * @since 2018.10.15
 */
public class NowOnTvObservable extends BaseTvBroadcastObservable<ModuleData<IBroadcast>> {

    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);
    private final TvCache mTvCache = Components.get(TvCache.class);

    private Disposable mRescheduleDisposable;

    public NowOnTvObservable(Context context, ModuleConfig moduleConfig, int count) {
        super(context, moduleConfig, count);
    }

    /**
     * Needed to be overridden for listen at last watched channel changed.
     *
     * @param emitter for emit list
     * @throws Exception in case of error, exception
     */
    @Override
    public void subscribe(ObservableEmitter<ModuleData<IBroadcast>> emitter) throws Exception {
        super.subscribe(emitter);
        registerObserver(TvContract.Favorites.CONTENT_URI, TvContract.Channel.CHANNEL_MEMORY_CACHE_CONTENT_URI,
                ConfigContract.buildSettingsUriForKey(Settings.lastPlayedChannelNumber.name()));
    }

    @Override
    protected void publishBroadcasts(ObservableEmitter<ModuleData<IBroadcast>> emitter) {
        List<IBroadcast> nowBroadcasts;

        List<String> genreList = new ArrayList<>();
        String favorite = "";
        if (mModuleConfig.getDataSource() != null && mModuleConfig.getDataSource().getParams() != null) {
            genreList = mModuleConfig.getDataSource().getParams().getFilterList(ModuleFilterOption.Mixed.Genre.NAME);
            favorite = mModuleConfig.getDataSource().getParams().getFilter(ModuleFilterOption.Mixed.Favorite.NAME);
        }

        nowBroadcasts = mTvCache.getNowOnTvBroadcasts(genreList, favorite, mNowOnTvCache.getLastPlayedChannelId(), mCount);
        emitter.onNext(new ModuleData<>(mModuleConfig, nowBroadcasts));

        // Calculate the remaining time until a show expires.
        long minEndDate = Long.MAX_VALUE;
        for (IBroadcast now : nowBroadcasts) {
            if (now.getEnd() > System.currentTimeMillis()
                    && now.getEnd() < minEndDate) {
                minEndDate = now.getEnd();
            }
        }

        // Calculate the remaining time until the first show expires.
        final long delay = minEndDate - System.currentTimeMillis()
                // Add a few seconds threshold in case there are small gaps between programs.
                + TimeUnit.SECONDS.toMillis(3);

        if (delay > 0 && !RxUtils.isDisposed(emitter)) {
            Log.d(TAG, "Reschedule now next update after." + TimeUnit.MILLISECONDS.toSeconds(delay) + " sec.");
            RxUtils.disposeSilently(mRescheduleDisposable);
            mRescheduleDisposable = Completable.fromAction(() -> publishBroadcasts(emitter))
                    .delaySubscription(delay, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe();
        }
    }

    /**
     * Cancel reschedule after observable cancelled.
     */
    @Override
    protected void unregisterObserver() {
        super.unregisterObserver();
        RxUtils.disposeSilently(mRescheduleDisposable);
    }
}
