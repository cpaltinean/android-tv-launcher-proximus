package tv.threess.threeready.data.account.model;

import tv.threess.threeready.api.account.model.server.IPosterServerInfoField;

/**
 * PosterServerAllowedImage entity created from database.
 *
 * Created by Noemi Dali on 02.04.2020.
 */
public class PosterServerFieldDBModel implements IPosterServerInfoField {
    private Float mWidth;
    private Float mHeight;
    private String mUri;
    private boolean mIsImageResizable;

    @Override
    public Float getWidth() {
        return mWidth;
    }

    @Override
    public Float getHeight() {
        return mHeight;
    }

    @Override
    public String getUri() {
        return mUri;
    }

    @Override
    public boolean isResizable() {
        return mIsImageResizable;
    }

    public static class Builder {
        PosterServerFieldDBModel dbModel;

        public Builder() {
            dbModel = new PosterServerFieldDBModel();
        }

        public Builder setWidth(Float width) {
            dbModel.mWidth = width;
            return this;
        }

        public Builder setHeight(Float height) {
            dbModel.mHeight = height;
            return this;
        }

        public Builder setUri(String uri) {
            dbModel.mUri = uri;
            return this;
        }

        public Builder setIsResizable(boolean resizable) {
            dbModel.mIsImageResizable = resizable;
            return this;
        }

        public PosterServerFieldDBModel build() {
            return dbModel;
        }
    }
}
