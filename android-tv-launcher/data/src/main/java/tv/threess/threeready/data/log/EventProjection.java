/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.log;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.log.ReportContract;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.data.generic.IProjection;

/**
 * Event projection enum
 *
 * @author Arnold Szabo
 * @since 2017.08.09
 */
public enum EventProjection implements IProjection {
    ID(ReportContract.Event.ID),
    NAME(ReportContract.Event.NAME),
    DOMAIN(ReportContract.Event.DOMAIN),
    TIMESTAMP(ReportContract.Event.TIMESTAMP),
    VALUE(ReportContract.Event.VALUE),
    LAST_UPDATED(ReportContract.Event.LAST_UPDATED);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    EventProjection(String column) {
        this.mColumn = ReportContract.Event.TABLE_NAME + "." + column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }
    /**
     * Return a list of event logs from the given cursor
     *
     * @param cursor Cursor that has the events
     * @return List of event logs
     */
    public static List<Event<?, ?>> readAll(Cursor cursor) {
        List<Event<?, ?>> eventList = new ArrayList<>();

        if (cursor == null) return eventList;

        while (cursor.moveToNext()) {
            long id = cursor.getLong(ID.ordinal());
            String name = cursor.getString(NAME.ordinal());
            String domain = cursor.getString(DOMAIN.ordinal());
            long timestamp = cursor.getLong(TIMESTAMP.ordinal());
            String value = cursor.getString(VALUE.ordinal());
            eventList.add(new Event(id, name, domain, timestamp, value));
        }

        return eventList;
    }
}
