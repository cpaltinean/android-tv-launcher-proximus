package tv.threess.threeready.data.tv.observable;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.tv.TvCache;

/**
 * Rx observable class to return a channel by its number in the DB.
 *
 * @author Denisa Trif
 * @since 2018.08.14
 */
public class ChannelByNumberObservable implements ObservableOnSubscribe<TvChannel> {

    private final AppConfig mAppConfig = Components.get(AppConfig.class);
    private final TvCache mTvCache = Components.get(TvCache.class);

    private final int mNumber;

    public ChannelByNumberObservable(int number) {
        mNumber = number;
    }

    @Override
    public void subscribe(ObservableEmitter<TvChannel> e) throws Exception {
        e.onNext(mTvCache.getChannelByNumber(mNumber > mAppConfig.getMaxChannelNumber() ? 0 : mNumber));
        e.onComplete();
    }
}
