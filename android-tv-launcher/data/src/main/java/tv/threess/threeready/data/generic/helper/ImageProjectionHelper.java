package tv.threess.threeready.data.generic.helper;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.model.ImageSource;

/**
 * Class to put image sources to content values and read from cursor.
 *
 * @author Barabas Attila
 * @since 2020.05.18
 */
public class ImageProjectionHelper {
    private static final String TAG = Log.tag(ImageProjectionHelper.class);

    /**
     * Constants to build the image type and url list.
     */
    private static final String IMAGE_SEPARATOR = System.lineSeparator();
    private static final String IMAGE_TYPE_PART_SEPARATOR = "_";
    private static final String IMAGE_TYPE_PREFIX = "T";
    private static final String IMAGE_WIDTH_PREFIX = "W";
    private static final String IMAGE_HEIGHT_PREFIX = "H";

    /**
     * Append the content values with the image sources.
     * @param images The image sources which needs to be put in the content values.
     * @param values The content values which needs to be appended.
     * @param urlField The name of the field in the content values where the urls should be saved.
     * @param typeField The name of the field in the content values where the image types should be saved.
     */
    public static void intoContentValues(List<IImageSource> images,
                                         ContentValues values, String urlField, String typeField) {

        StringBuilder urlValueBuilder = new StringBuilder();
        StringBuilder typeValueBuilder = new StringBuilder();
        for (IImageSource is : images) {
            // Build the type format. E.g. TCover_W320_H240
            typeValueBuilder.append(IMAGE_TYPE_PREFIX);
            typeValueBuilder.append(is.getType().name());
            typeValueBuilder.append(IMAGE_TYPE_PART_SEPARATOR);

            if (is.getWidth() > 0) {
                typeValueBuilder.append(IMAGE_WIDTH_PREFIX);
                typeValueBuilder.append(is.getWidth());
                typeValueBuilder.append(IMAGE_TYPE_PART_SEPARATOR);
            }

            if (is.getHeight() > 0) {
                typeValueBuilder.append(IMAGE_HEIGHT_PREFIX);
                typeValueBuilder.append(is.getHeight());
                typeValueBuilder.append(IMAGE_TYPE_PART_SEPARATOR);
            }

            typeValueBuilder.deleteCharAt(typeValueBuilder.length() - 1);
            typeValueBuilder.append(IMAGE_SEPARATOR);

            urlValueBuilder.append(is.getUrl());
            urlValueBuilder.append(IMAGE_SEPARATOR);
        }

        if (typeValueBuilder.length() > 0) {
            typeValueBuilder.deleteCharAt(typeValueBuilder.length() - 1);
        }

        if (urlValueBuilder.length() > 0) {
            urlValueBuilder.deleteCharAt(urlValueBuilder.length() - 1);
        }

        values.put(urlField, urlValueBuilder.toString() );
        values.put(typeField, typeValueBuilder.toString());
    }
    /**
     * Read image sources from a cursor row.
     * @param cursor The cursor to read from.
     * @return The list of image sources from the cursor.
     */
    public static List<IImageSource> imagesFromCursorRow(Cursor cursor, int urlIndex, int typeIndex) {
        String urlValue = cursor.getString(urlIndex);
        String[] imageUrls = urlValue.split(IMAGE_SEPARATOR);

        String typeValue = cursor.getString(typeIndex);
        String[] types = typeValue.split(IMAGE_SEPARATOR);

        List<IImageSource> imageSources = new ArrayList<>(imageUrls.length);
        for (int i = 0; i < imageUrls.length; ++i) {
            int width = 0;
            int height = 0;
            IImageSource.Type type = IImageSource.Type.COVER;

            try {
                // Split the type format. E.g TCover_W320_H240
                if (!TextUtils.isEmpty(types[i])) {
                    String[] typeParts = types[i].split(IMAGE_TYPE_PART_SEPARATOR);
                    for (String part : typeParts) {
                        String partValue = part.substring(1);
                        if (part.startsWith(IMAGE_TYPE_PREFIX)) {
                            type = IImageSource.Type.valueOf(partValue);
                        } else if (part.startsWith(IMAGE_WIDTH_PREFIX)) {
                            width = Integer.parseInt(partValue);
                        } else if (part.startsWith(IMAGE_HEIGHT_PREFIX)) {
                            height = Integer.parseInt(partValue);
                        }
                    }
                }

                imageSources.add(new ImageSource(imageUrls[i], type, width, height));
            } catch (Exception e) {
                Log.e(TAG, "Couldn't read image from cursor.", e);
            }
        }

        return imageSources;
    }
}
