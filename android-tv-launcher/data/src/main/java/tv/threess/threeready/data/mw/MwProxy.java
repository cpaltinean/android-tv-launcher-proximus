/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.mw;

import android.content.ComponentName;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;

import androidx.annotation.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.generic.receivers.WaitTask;
import tv.threess.threeready.api.middleware.TvAdObserver;
import tv.threess.threeready.api.middleware.model.BaseInfo;
import tv.threess.threeready.api.middleware.model.HdmiInfo;
import tv.threess.threeready.api.middleware.model.TifChannelList;
import tv.threess.threeready.api.tv.model.TifScanParameters;

/**
 * Generic interface for scan actions.
 *
 * @author Lukacs Andor
 * @since 2017.06.06
 */
public interface MwProxy extends Component {

    String REBOOT_REASON_QUIESCENT = "quiescent";

    /**
     * Timeout for tif channel scanning.
     */
    long SCAN_TIMEOUT = TimeUnit.SECONDS.toMillis(20);

    /**
     * Update the configurations for the VQE client dynamically without the scan process.
     * The update checks if the hash of the old config is different from the hash of the new one and does the update only then
     */
    void updateVqeConfig(Object info) throws IOException;

    /**
     * Update the re joining config.
     */
    void updateRejoinConfig(String rejoinConfig);

    /**
     * Start scanning process.
     *
     * @param params parameter to be used for tif scanning at scanning.
     * @return the scan task which was started.
     */
    WaitTask<Boolean> startScan(TifScanParameters params) throws Exception;

    /**
     * register and return the content observer for tad marker and stitching
     * @param handler where to process the callbacks
     * @param observer invoked when scte marker or stitching state changes
     */
    ContentObserver registerScteObserver(Handler handler, TvAdObserver observer);

    /**
     * Get the HDMI connection status and EDID data.
     *
     * @return HDMI connection information or null if there is no such.
     */
    @Nullable
    HdmiInfo getHdmiInfo();

    /**
     * @return The unique chipset id of the device.
     */
    String getSocId();

    /**
     * @return The ip address of the device from the currently connected network.
     * Null if there is no active network connection.
     */
    @Nullable
    String getIpAddress();

    /**
     * @param propertyName The name of the system property.
     * @return The value of the given system property or null if there is no such.
     */
    String getSystemProperties(String propertyName);

    /**
     * @return The currently installed TV App version
     */
    String getAppVersion();

    /**
     * @return The software version of the middleware.
     */
    String getMWVersion();

    /**
     * Get the content of the boot_id file, This file provides a UUID that changes on each boot of the machine.
     *
     * @return content of the boot_id file.
     */
    String getBootId();

    /**
     * Reboot the device with the given reason.
     *
     * @param reason The reason of the reboot. E.g recovery, quiescent etc.
     */
    void reboot(String reason);

    /**
     * @return True if the box is in active state, false if the box is in stand by state.
     */
    boolean isScreenOn();

    /**
     * @return True if talkback accessibility feature is on.
     */
    boolean isTalkBackOn();

    /**
     * @return True if the application is the default launcher on the device.
     */
    boolean isDefaultLauncher();

    /**
     * @return The width of the screen in pixels.
     */
    int getWindowWidth();

    /**
     * @return The height of the screen in pixels.
     */
    int getWindowHeight();

    /**
     * Returns the MAC address of the network interface where the hardware address is set.
     *
     * @return The MAC of the network interface or null in case of errors.
     */
    @Nullable
    String getEthernetMacAddress();

    /**
     * @return The hardware serial number of the device.
     */
    String getHardwareSerialNumber();

    /**
     * @return The android serial number of the device.
     */
    String getAndroidSerialNumber();

    /**
     * @return The hardware version of the device.
     */
    String getHardwareVersion();

    /**
     * @param componentName The name of the component to check the process for.
     * @return The name of the process in which the given components running.
     */
    @Nullable
    String getProcessName(ComponentName componentName);

    /**
     * Read information like memory, CPU usage from a given process.
     *
     * @param packageName The package name of the application to read process info from.
     * @param info        The type of the process information.
     * @return The model which holds information about the process.
     */
    <T extends BaseInfo<?>> T readProcessInfo(String packageName, T info);

    /**
     * @return True if a DVB cable is connected.
     */
    boolean isDVBCableConnected();

    /**
     * Check for system update on the server and start the download when available.
     *
     * @param reset         True if the download should be restarted in case it was already download or the download is in progress.
     * @param rollOutBypass True to bypass server daily update roll out quota.
     * @param manualCheck   True if the update check was initiated by the user.
     */
    void checkSystemUpdates(boolean reset, boolean rollOutBypass, boolean manualCheck);

    /**
     * Reboot the device and apply the previously downloaded system image.
     */
    void applySystemUpdate();

    /**
     * @return Read accessible channels from the TV input framework.
     */
    TifChannelList getTifChannels();

    /**
     * @return The version of the operating system the application running on.
     */
    String getOperationSystemVersion();

    /**
     * @return The type of the device the application running on.
     */
    String getHardwareType();

    /**
     * Query the `ro.boot.bootreason` system property, defined in
     * https://source.android.com/devices/bootloader/boot-reason
     */
    String getBootReason();

    /**
     * Read the content of a file from system partition.
     *
     * @param fileUri The URI of the file to read from.
     * @return The content of the given file as string.
     */
    String readFileContent(Uri fileUri);

    /**
     * used when boot is completed, add boot finish actions here
     */
    void bootCompletedAction();

    /**
     * @return A task which waits for the DTV service setup to finish.
     */
    WaitTask<Void> waitForDtvSetup();

    /**
     * @return VCAS plugin version name.
     */
    String getVCASPluginVersion();

    /**
     * @return VCAS client version name.
     */
    String getVCASClientVersion();

    /**
     * read the application usage stats from the system and report them as start stop events
     */
    void reportUsageEvents();

    /**
     * Method used for save the heartBeat event.
     */
    void saveHeartBeat();

    static int exec(ByteArrayOutputStream out, String... args) throws IOException, InterruptedException {
        Process process = Runtime.getRuntime().exec(args);
        FileUtils.copy(out, process.getInputStream());
        if (out.size() == 0) {
            FileUtils.copy(out, process.getErrorStream());
        }
        return process.waitFor();
    }

    static void exec(String... args) throws IOException, InterruptedException {
        Process process = Runtime.getRuntime().exec(args);
        int exitCode = process.waitFor();
        if (exitCode == 0) {
            return;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        FileUtils.copy(out, process.getInputStream());
        if (out.size() == 0) {
            FileUtils.copy(out, process.getErrorStream());
        }
        throw new IOException(out.toString().trim());
    }
}
