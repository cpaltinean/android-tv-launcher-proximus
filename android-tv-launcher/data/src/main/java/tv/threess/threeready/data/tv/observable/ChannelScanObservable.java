package tv.threess.threeready.data.tv.observable;

import android.content.Context;
import io.reactivex.ObservableEmitter;
import tv.threess.threeready.data.generic.observable.BaseWorkManagerObservable;
import tv.threess.threeready.data.tv.TvService;

public class ChannelScanObservable extends BaseWorkManagerObservable<Boolean> {

    public ChannelScanObservable(Context context) {
        super(context, TvService.buildChannelScanIntent());
    }

    @Override
    protected void onJobFinished(ObservableEmitter<Boolean> emitter, boolean success) {
        emitter.onNext(success);
        emitter.onComplete();
    }
}
