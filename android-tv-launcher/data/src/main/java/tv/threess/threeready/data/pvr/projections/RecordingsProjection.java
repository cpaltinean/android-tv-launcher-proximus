package tv.threess.threeready.data.pvr.projections;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.data.pvr.PvrContract;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.SingleRecordingStatus;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.data.generic.IProjection;
import tv.threess.threeready.data.generic.helper.ImageProjectionHelper;
import tv.threess.threeready.data.pvr.model.RecordingDBModel;

/**
 * Projection to get recording entity.
 * <p>
 * Created by Noemi Dali on 17.04.2020.
 */
public enum RecordingsProjection implements IProjection {
    ID(PvrContract.Recording.BROADCAST_ID),
    RECORDING_ID(PvrContract.Recording.RECORDING_ID),
    BROADCAST_ID(PvrContract.Recording.BROADCAST_ID),
    SYSTEM_RECORD_ID(PvrContract.Recording.SYSTEM_RECORD_ID),
    CHANNEL_ID(PvrContract.Recording.CHANNEL_ID),
    RECORDING_START(PvrContract.Recording.RECORDING_START),
    RECORDING_END(PvrContract.Recording.RECORDING_END),
    PROGRAM_START(PvrContract.Recording.BROADCAST_START),
    PROGRAM_END(PvrContract.Recording.BROADCAST_END),
    TITLE(PvrContract.Recording.TITLE),
    DESCRIPTION(PvrContract.Recording.DESCRIPTION),
    GENRES(PvrContract.Recording.GENRES),
    RELEASE_YEAR(PvrContract.Recording.RELEASE_YEAR),
    SEASON(PvrContract.Recording.SEASON),
    EPISODE(PvrContract.Recording.EPISODE),
    EPISODE_TITLE(PvrContract.Recording.EPISODE_TITLE),
    PROGRAM_ID(PvrContract.Recording.PROGRAM_ID),
    SERIES_ID(PvrContract.Recording.SERIES_ID),
    UNSKIPABLE_ASSET_ID(PvrContract.Recording.UNSKIPABLE_ASSET_ID),
    ACTORS(PvrContract.Recording.ACTORS),
    DIRECTORS(PvrContract.Recording.DIRECTOR),
    PARENTAL_RATING(PvrContract.Recording.PARENTAL_RATING),
    IS_BLACKLISTED(PvrContract.Recording.IS_BLACKLISTED),
    IS_HD(PvrContract.Recording.IS_HD),
    IS_NPVR_ENABLED(PvrContract.Recording.IS_NPVR_ENABLED),
    IMAGE_URL(PvrContract.Recording.IMAGE_URL),
    IMAGE_TYPE(PvrContract.Recording.IMAGE_TYPE),
    FAILED_RECORDING(PvrContract.Recording.FAILED_RECORDING),
    HAS_DESCRIPTIVE_AUDIO(PvrContract.Recording.HAS_DESCRIPTIVE_AUDIO),
    HAS_DESCRIPTIVE_SUBTITLE(PvrContract.Recording.HAS_DESCRIPTIVE_SUBTITLE);

    private static final String TAG = Log.tag(RecordingsProjection.class);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    RecordingsProjection(String column) {
        mColumn = column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    public static List<String> getRecIdsFromCursor(Cursor cursor) {
        ArrayList<String> recordingsIds = new ArrayList<>();
        String recordingId;
        if (!cursor.moveToFirst()) {
            return recordingsIds;
        }
        do {
            recordingId = cursor.getString(0);
            recordingsIds.add(recordingId);
        } while (cursor.moveToNext());
        return recordingsIds;
    }

    /**
     * Read a recording from cursor row.
     *
     * @param cursor Cursor with program details.
     * @return Model for the specified recording.
     */
    public static IRecording readRow(Cursor cursor) {
        RecordingDBModel.Builder builder = RecordingDBModel.Builder.builder()
                .setId(cursor.getString(ID.ordinal()))
                .setRecordingId(cursor.getString(RECORDING_ID.ordinal()))
                .setSystemRecordId(cursor.getString(SYSTEM_RECORD_ID.ordinal()))
                .setProgramID(cursor.getString(PROGRAM_ID.ordinal()))
                .setChannelID(cursor.getString(CHANNEL_ID.ordinal()))
                .setRecordingStart(cursor.getLong(RECORDING_START.ordinal()))
                .setRecordingEnd(cursor.getLong(RECORDING_END.ordinal()))
                .setStart(cursor.getLong(PROGRAM_START.ordinal()))
                .setEnd(cursor.getLong(PROGRAM_END.ordinal()))
                .setTitle(cursor.getString(TITLE.ordinal()))
                .setDescription(cursor.getString(DESCRIPTION.ordinal()))
                .setSeason(cursor.isNull(SEASON.ordinal())
                        ? null : cursor.getInt(SEASON.ordinal()))
                .setEpisode(cursor.isNull(EPISODE.ordinal())
                        ? null : cursor.getInt(EPISODE.ordinal()))
                .setEpisodeTitle(cursor.getString(EPISODE_TITLE.ordinal()))
                .setSeriesID(cursor.getString(SERIES_ID.ordinal()))
                .setUnskipableAssetID(cursor.getString(UNSKIPABLE_ASSET_ID.ordinal()))
                .setActors(Arrays.asList(TextUtils.split(cursor.getString(ACTORS.ordinal()), ",")))
                .setDirectors(Arrays.asList(TextUtils.split(cursor.getString(DIRECTORS.ordinal()), ",")))
                .setParentalRating(ParentalRating.valueOf(ParentalRating.class, cursor.getString(PARENTAL_RATING.ordinal())))
                .setReleaseYear(cursor.getString(RELEASE_YEAR.ordinal()))
                .setGenres(Arrays.asList(TextUtils.split(cursor.getString(GENRES.ordinal()), ",")))
                .setIsBlackListed(cursor.getInt(IS_BLACKLISTED.ordinal()) > 0)
                .setImageSources(ImageProjectionHelper.imagesFromCursorRow(cursor, IMAGE_URL.ordinal(), IMAGE_TYPE.ordinal()))
                .setNPVREnabled(cursor.getInt(IS_NPVR_ENABLED.ordinal()) > 0)
                .setFailedRecording(cursor.getInt(FAILED_RECORDING.ordinal()) > 0)
                .setHasDescriptiveAudio(cursor.getInt(HAS_DESCRIPTIVE_AUDIO.ordinal()) > 0)
                .setHasDescriptiveSubtitle(cursor.getInt(HAS_DESCRIPTIVE_SUBTITLE.ordinal()) > 0);

        return builder.build();
    }


    /**
     * Adds recording values to content values.
     *
     * @param recording recording item
     * @return Content values with recording values.
     */
    public static ContentValues toContentValues(IRecording recording) {
        return toContentValues(recording, null);
    }

    /**
     * Adds recording and broadcast values to content values.
     *
     * @param recording recording item
     * @param broadcast broadcast item
     * @return content values
     */
    public static ContentValues toContentValues(IRecording recording, IBroadcast broadcast) {
        long now = System.currentTimeMillis();
        ContentValues values = new ContentValues();

        values.put(PvrContract.Recording.RECORDING_ID, recording.getRecordingId());
        values.put(PvrContract.Recording.BROADCAST_ID, broadcast != null ? broadcast.getId() : recording.getId());
        values.put(PvrContract.Recording.SYSTEM_RECORD_ID, recording.getSystemRecordId());
        values.put(PvrContract.Recording.CHANNEL_ID, broadcast != null ? broadcast.getChannelId() : recording.getChannelId());
        values.put(PvrContract.Recording.RECORDING_START, recording.getRecordingStart());
        values.put(PvrContract.Recording.RECORDING_END, recording.getRecordingEnd());
        values.put(PvrContract.Recording.BROADCAST_START, broadcast != null ? broadcast.getStart() : recording.getStart());
        values.put(PvrContract.Recording.BROADCAST_END, broadcast != null ? broadcast.getEnd() : recording.getEnd());
        values.put(PvrContract.Recording.TITLE, broadcast != null ? broadcast.getTitle() : recording.getTitle());
        values.put(PvrContract.Recording.DESCRIPTION, broadcast != null ? broadcast.getDescription() : recording.getDescription());
        values.put(PvrContract.Recording.GENRES, TextUtils.join(",", broadcast != null ? broadcast.getGenres() : recording.getGenres()));
        values.put(PvrContract.Recording.RELEASE_YEAR, broadcast != null ? broadcast.getReleaseYear() : recording.getReleaseYear());
        values.put(PvrContract.Recording.SEASON, broadcast != null ? broadcast.getSeasonNumber() : recording.getSeasonNumber());
        values.put(PvrContract.Recording.EPISODE, broadcast != null ? broadcast.getEpisodeNumber() : recording.getEpisodeNumber());
        values.put(PvrContract.Recording.EPISODE_TITLE, broadcast != null ? broadcast.getEpisodeTitle() : recording.getEpisodeTitle());
        values.put(PvrContract.Recording.PROGRAM_ID, broadcast != null ? broadcast.getProgramId() : recording.getProgramId());
        values.put(PvrContract.Recording.SERIES_ID, broadcast != null ? broadcast.getSeriesId() : recording.getSeriesId());
        values.put(PvrContract.Recording.UNSKIPABLE_ASSET_ID, broadcast != null ? broadcast.getAssetIdentifier() : recording.getAssetIdentifier());
        values.put(PvrContract.Recording.ACTORS, TextUtils.join(",", broadcast != null ? broadcast.getActors() : recording.getActors()));
        values.put(PvrContract.Recording.DIRECTOR, TextUtils.join(",", broadcast != null ? broadcast.getDirectors() : recording.getDirectors()));
        values.put(PvrContract.Recording.PARENTAL_RATING, broadcast != null ? broadcast.getParentalRating().name() : recording.getParentalRating().name());
        values.put(PvrContract.Recording.IS_BLACKLISTED, broadcast != null ? broadcast.isBlackListed() : recording.isBlackListed());
        values.put(PvrContract.Recording.IS_NPVR_ENABLED, broadcast != null ? broadcast.isNPVREnabled() : recording.isNPVREnabled());
        values.put(PvrContract.Recording.HAS_DESCRIPTIVE_AUDIO, broadcast != null ? broadcast.hasDescriptiveAudio() : recording.hasDescriptiveAudio());
        values.put(PvrContract.Recording.HAS_DESCRIPTIVE_SUBTITLE, broadcast != null ? broadcast.hasDescriptiveSubtitle() : recording.hasDescriptiveSubtitle());
        values.put(PvrContract.Recording.FAILED_RECORDING, recording.isFailedRecording());
        values.put(PvrContract.Recording.LAST_UPDATED, now);

        List<IImageSource> imageSources = broadcast != null ? broadcast.getImageSources() : recording.getImageSources();
        ImageProjectionHelper.intoContentValues(imageSources, values,
                TvContract.Broadcast.IMAGE_URL, TvContract.Broadcast.IMAGE_TYPE);

        return values;
    }

    private static SingleRecordingStatus toSingleRecordingStatus(String value) {
        if (value == null) {
            return null;
        }
        try {
            return SingleRecordingStatus.valueOf(value);
        } catch (Exception e) {
            Log.w(TAG, "Failed to lookup recording status:" + value, e);
        }
        return SingleRecordingStatus.NONE;
    }
}
