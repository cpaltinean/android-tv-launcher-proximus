/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.home.model.module;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.data.generic.RuntimeTypeAdapterFactory;

/**
 * POJO class which holds the action configuration options of a module item.
 *
 * @author Barabas Attila
 * @since 2018.08.23
 */
public abstract class EditorialItemAction3Ready implements EditorialItemAction  {

    /**
     * @return Type adapter factory to read editorial actions based on type.
     */
    public static RuntimeTypeAdapterFactory<EditorialItemAction3Ready> createActionItemTypeAdapterFactory() {
        return RuntimeTypeAdapterFactory.of(EditorialItemAction3Ready.class, "type")
                .registerSubtype(PageEditorialItemAction3Ready.class, "PAGE")
                .registerSubtype(ModuleEditorialItemAction3Ready.class, "MODULE")
                .registerSubtype(AppEditorialItemAction3Ready.class, "APPLICATION")
                .registerSubtype(WebPageEditorialItemAction3Ready.class, "DEEP_LINK")
                .registerSubtype(IntentEditorialItemAction3Ready.class, "INTENT")
                .registerSubtype(CategoryEditorialItemAction3Ready.class, "CATEGORY");
    }

    public static class PageEditorialItemAction3Ready
            extends EditorialItemAction3Ready implements PageByIdEditorialItemAction {

        @SerializedName("page_id")
        String mPageId;

        @Override
        public String getPageId() {
            return mPageId;
        }
    }

    public static class CategoryEditorialItemAction3Ready
            extends EditorialItemAction3Ready implements CategoryEditorialItemAction {

        @SerializedName("category_id")
        private String mCategoryId;

        @SerializedName("empty_page_message")
        private String mEmptyPageMessage;

        @Override
        public String getCategoryId() {
            return mCategoryId;
        }

        @Override
        public String getEmptyPageMessage() {
            return mEmptyPageMessage;
        }
    }

    public static class AppEditorialItemAction3Ready
            extends EditorialItemAction3Ready implements AppEditorialItemAction {

        @SerializedName("package")
        String mPackageName;

        @Override
        public String getPackageName() {
            return mPackageName;
        }
    }

    public static class IntentEditorialItemAction3Ready
            extends EditorialItemAction3Ready implements IntentEditorialItemAction {

        @SerializedName("action")
        String mIntentAction;

        @SerializedName("package")
        String mPackageName;

        @SerializedName("uri")
        String mUri;

        @SerializedName("extras")
        HashMap<String, String> mExtras;

        @Override
        public String getIntentAction() {
            return mIntentAction;
        }

        @Override
        public String getPackageName() {
            return mPackageName;
        }

        @Nullable
        @Override
        public String getUri() {
            return mUri;
        }

        @Nullable
        @Override
        public Map<String, String> getExtras() {
            return mExtras;
        }
    }

    public static class WebPageEditorialItemAction3Ready
            extends EditorialItemAction3Ready implements WebPageEditorialItemAction {

        @SerializedName("deep_link")
        String mDeepLink;

        @Override
        public String getHtmlUrl() {
            return mDeepLink;
        }
    }

    public static class ModuleEditorialItemAction3Ready
            extends EditorialItemAction3Ready implements ModuleEditorialItemAction {

        @SerializedName("module")
        ModuleConfig3Ready mModuleConfig;

        @SerializedName("empty_page_message")
        private String mEmptyPageMessage;

        @Override
        public ModuleConfig getModuleConfig() {
            return mModuleConfig;
        }

        @Override
        public String getEmptyPageMessage() {
            return mEmptyPageMessage;
        }
    }
}
