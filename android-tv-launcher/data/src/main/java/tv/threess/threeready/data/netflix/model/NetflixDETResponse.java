package tv.threess.threeready.data.netflix.model;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.netflix.INetflixDETResponse;
import tv.threess.threeready.api.netflix.INetflixGroup;
import tv.threess.threeready.api.netflix.model.NetflixDETCategory;
import tv.threess.threeready.data.utils.GsonUtils;

/**
 * Item class used for the returned DET response.
 *
 * @author David
 * @since 25.02.2022
 */
public class NetflixDETResponse implements INetflixDETResponse {

    final static String TAG = Log.tag(NetflixDETResponse.class);

    public final static String DEFAULT_CATEGORY = "DEFAULT";

    private final NetflixDETCategory mCategory;
    private final String mUILanguage;
    private final String mResponseData;
    private final long mMinRefreshAt;
    private final long mExpireAt;
    private final List<INetflixGroup> mGroups = new ArrayList<>();

    public NetflixDETResponse(@NonNull NetflixDETCategory category, String uiLanguage, String responseData, long minRefreshAt, long expireAt) {
        this.mCategory = category;
        this.mUILanguage = uiLanguage;
        this.mResponseData = responseData;
        this.mMinRefreshAt = minRefreshAt;
        this.mExpireAt = expireAt;
    }

    @Override
    @NonNull
    public NetflixDETCategory getCategory() {
        return mCategory;
    }

    @Override
    public String getUILanguage() {
        return mUILanguage;
    }

    @Override
    public String getResponseData() {
        return mResponseData;
    }

    /**
     * @return The time stamp when the refresh should be done.
     */
    @Override
    public long getMinRefreshAt() {
        return mMinRefreshAt;
    }

    /**
     * @return The expiration date in millis.
     */
    @Override
    public long getExpireAt() {
        return mExpireAt;
    }

    /**
     * @return The list of Netflix DET groups. Each group contains multiple tiles.
     */
    @NonNull
    public List<INetflixGroup> getGroups() {
        if (ArrayUtils.isEmpty(mGroups)) {
            prepareGroupList();
        }
        return mGroups;
    }

    /**
     * Method used for prepare the Netflix DET group which contains the Netflix Tiles.
     */
    private void prepareGroupList() {
        try {
            NetflixDETQueryResponse queryResponse = GsonUtils.readJsonFromString(getResponseData(), NetflixDETQueryResponse.class);
            if (queryResponse != null && queryResponse.getData() != null
                    && queryResponse.getData().getDiscovery() != null
                    && !ArrayUtils.isEmpty(queryResponse.getData().getDiscovery().getGroups())) {
                mGroups.addAll(queryResponse.getData().getDiscovery().getGroups());
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to parse the Netflix DET response.", e);
        }
    }
}
