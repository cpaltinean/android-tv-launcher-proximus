/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.vod;

import android.content.Context;

import androidx.work.Data;
import androidx.work.WorkerParameters;
import tv.threess.lib.di.Components;
import tv.threess.threeready.data.generic.BaseWorker;
import tv.threess.threeready.data.generic.JobIntent;

/**
 * Download vod related data and persist it in database.
 *
 * @author Barabas Attila
 * @since 2017.09.04
 */
public class VodService {
    public static JobIntent buildUpdatePurchasesIntent() {
        return new JobIntent(UpdatePurchases.class, BaseVodRepository.rentalUpdater, Data.EMPTY);
    }

    public static JobIntent buildUpdateVodCategoriesIntent() {
        return new JobIntent(UpdateVodCategories.class, BaseVodRepository.categoryUpdater, Data.EMPTY);
    }

    public static class UpdatePurchases extends BaseWorker {

        public UpdatePurchases(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            Components.get(VodServiceRepository.class).updatePurchases();
        }
    }

    public static class UpdateVodCategories extends BaseWorker {

        public UpdateVodCategories(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            Components.get(VodServiceRepository.class).updateVodCategories();
        }
    }
}
