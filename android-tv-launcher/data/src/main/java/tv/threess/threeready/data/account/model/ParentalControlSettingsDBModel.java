package tv.threess.threeready.data.account.model;

import tv.threess.threeready.api.generic.model.IParentalControlSettings;
import tv.threess.threeready.api.pc.model.ParentalRating;

/**
 * Parental control settings coming from the local database.
 *
 * @author Barabas Attila
 * @since 12/7/20
 */
public class ParentalControlSettingsDBModel implements IParentalControlSettings {
    private final boolean mEnableParentalControl;
    private final ParentalRating mParentalRating;

    public ParentalControlSettingsDBModel(boolean enableParentalControl, ParentalRating parentalRating) {
        mEnableParentalControl = enableParentalControl;
        mParentalRating = parentalRating;
    }

    @Override
    public boolean isEnabled() {
        return mEnableParentalControl;
    }

    @Override
    public ParentalRating getParentalRating() {
        return mParentalRating;
    }
}
