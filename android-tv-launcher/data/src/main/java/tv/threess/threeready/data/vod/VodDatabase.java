/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.vod;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.data.generic.helper.BaseSQLiteOpenHelper;

/**
 * Helper class to manage VOD database creation and version management.
 *
 * @author Barabas Attila
 * @since 2017.09.04
 */
public class VodDatabase extends BaseSQLiteOpenHelper {

    // database file name
    private static final String NAME = "vod.db";

    // database version number
    private static final int VERSION = 15;

    public VodDatabase(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
       db.execSQL("CREATE TABLE IF NOT EXISTS " + VodContract.RentalExpiration.TABLE_NAME + " ("
               + VodContract.RentalExpiration.VARIANT_ID + " TEXT PRIMARY KEY NOT NULL,"
               + VodContract.RentalExpiration.EXPIRATION + " INTEGER,"
               + VodContract.RentalExpiration.RENTAL_TIME + " INTEGER,"
               + VodContract.RentalExpiration.LAST_UPDATED + " INTEGER NOT NULL)"
       );

        db.execSQL("CREATE TABLE IF NOT EXISTS " + VodContract.ContinueWatchingVod.TABLE_NAME + " ( "
                + VodContract.ContinueWatchingVod.VOD_ID + " TEXT PRIMARY KEY NOT NULL,"
                + VodContract.ContinueWatchingVod.SERIES_ID + " TEXT,"
                + VodContract.ContinueWatchingVod.MAIN_VARIANT_ID + " TEXT,"
                + VodContract.ContinueWatchingVod.VARIANT_IDS + " TEXT,"
                + VodContract.ContinueWatchingVod.LAST_UPDATED + " INTEGER NOT NULL)"
        );

       db.execSQL("CREATE TABLE IF NOT EXISTS " + VodContract.ContinueWatchingVodVariant.TABLE_NAME + " ( "
               + VodContract.ContinueWatchingVodVariant.VARIANT_ID + " TEXT PRIMARY KEY NOT NULL,"
               + VodContract.ContinueWatchingVodVariant.SERIES_ID + " TEXT,"
               + VodContract.ContinueWatchingVodVariant.ITEM_TITLE + " TEXT,"
               + VodContract.ContinueWatchingVodVariant.GROUP_TITLE + " TEXT,"
               + VodContract.ContinueWatchingVodVariant.GROUP_ID + " TEXT,"
               + VodContract.ContinueWatchingVodVariant.EPISODE_TITLE + " TEXT,"
               + VodContract.ContinueWatchingVodVariant.DESCRIPTION + " TEXT, "
               + VodContract.ContinueWatchingVodVariant.GENRES + " TEXT,"
               + VodContract.ContinueWatchingVodVariant.RELEASE_YEAR + " TEXT, "
               + VodContract.ContinueWatchingVodVariant.DURATION + " INTEGER, "
               + VodContract.ContinueWatchingVodVariant.SEASON + " TEXT, "
               + VodContract.ContinueWatchingVodVariant.EPISODE + " TEXT, "
               + VodContract.ContinueWatchingVodVariant.ACTORS + " TEXT, "
               + VodContract.ContinueWatchingVodVariant.DIRECTORS + " TEXT,"
               + VodContract.ContinueWatchingVodVariant.PARENTAL_RATING + " TEXT,"
               + VodContract.ContinueWatchingVodVariant.IMAGE_URL + " TEXT,"
               + VodContract.ContinueWatchingVodVariant.IMAGE_TYPE + " TEXT,"
               + VodContract.ContinueWatchingVodVariant.HAS_TRAILER + " INTEGER,"
               + VodContract.ContinueWatchingVodVariant.AUDIO_LANGUAGES + " TEXT,"
               + VodContract.ContinueWatchingVodVariant.SUBTITLE_LANGUAGES + " TEXT,"
               + VodContract.ContinueWatchingVodVariant.IS_NEW + " INTEGER,"
               + VodContract.ContinueWatchingVodVariant.IS_TVOD + " INTEGER,"
               + VodContract.ContinueWatchingVodVariant.IS_SVOD + " INTEGER,"
               + VodContract.ContinueWatchingVodVariant.IS_SUBSCRIBED + " INTEGER,"
               + VodContract.ContinueWatchingVodVariant.IS_RENTABLE + " INTEGER,"
               + VodContract.ContinueWatchingVodVariant.VIDEO_QUALITY + " TEXT,"
               + VodContract.ContinueWatchingVodVariant.LICENCE_PERIOD_END + " INTEGER,"
               + VodContract.ContinueWatchingVodVariant.IS_FREE + " INTEGER,"
               + VodContract.ContinueWatchingVodVariant.SUPER_GENRE + " TEXT,"
               + VodContract.ContinueWatchingVodVariant.INTERNAL_ID + " INTEGER,"
               + VodContract.ContinueWatchingVodVariant.IS_HD + " INTEGER,"
               + VodContract.ContinueWatchingVodVariant.HAS_DESCRIPTIVE_AUDIO + " INTEGER,"
               + VodContract.ContinueWatchingVodVariant.HAS_DESCRIPTIVE_SUBTITLE + " INTEGER,"
               + VodContract.ContinueWatchingVodVariant.LAST_UPDATED + " INTEGER NOT NULL)"
       );
    }

    @Override
    public boolean onUpgrade(SQLiteDatabase db, int toVersion) {
        switch (toVersion) {
            default:
                db.execSQL("DROP TABLE IF EXISTS " + VodContract.RentalExpiration.TABLE_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + VodContract.ContinueWatchingVod.TABLE_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + VodContract.ContinueWatchingVodVariant.TABLE_NAME);
                Settings.lastRentedVodSyncTime.edit().remove();
                onCreate(db);
                return true;

            case 10:
                //insert a new group_title column
                db.execSQL("ALTER TABLE " + VodContract.ContinueWatchingVodVariant.TABLE_NAME
                        + " ADD COLUMN " + VodContract.ContinueWatchingVodVariant.GROUP_TITLE
                        + " TEXT DEFAULT null");
                return false;

            case 11:
                // Insert the is_free column to the ContinueWatchingVodVariant table
                db.execSQL("ALTER TABLE " + VodContract.ContinueWatchingVodVariant.TABLE_NAME
                        + " ADD COLUMN " + VodContract.ContinueWatchingVodVariant.IS_FREE
                        + " INTEGER");
                return false;

            case 12:
            case 13:
                db.execSQL("DROP TABLE IF EXISTS " + VodContract.RentalExpiration.TABLE_NAME);
                Settings.lastRentedVodSyncTime.edit().remove();
                onCreate(db);
                return false;

            case 14:
                db.execSQL("ALTER TABLE " + VodContract.ContinueWatchingVodVariant.TABLE_NAME
                        + " ADD COLUMN " + VodContract.ContinueWatchingVodVariant.GROUP_ID
                        + " TEXT");
                return false;

            case 15:
                db.execSQL("ALTER TABLE " + VodContract.ContinueWatchingVodVariant.TABLE_NAME
                        + " ADD COLUMN " + VodContract.ContinueWatchingVodVariant.HAS_DESCRIPTIVE_AUDIO
                        + " INTEGER");

                db.execSQL("ALTER TABLE " + VodContract.ContinueWatchingVodVariant.TABLE_NAME
                        + " ADD COLUMN " + VodContract.ContinueWatchingVodVariant.HAS_DESCRIPTIVE_SUBTITLE
                        + " INTEGER");
                return false;
        }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + VodContract.RentalExpiration.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + VodContract.ContinueWatchingVod.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + VodContract.ContinueWatchingVodVariant.TABLE_NAME);
        onCreate(db);
    }
}
