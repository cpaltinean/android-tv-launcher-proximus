/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.home.model.generic;

import android.graphics.Color;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.home.model.generic.BackgroundPlayerType;
import tv.threess.threeready.api.home.model.generic.LayoutConfig;
import tv.threess.threeready.data.account.adapter.BackgroundPlayerTypeAdapter3Ready;
import tv.threess.threeready.data.account.adapter.ColorTypeAdapter3Ready;

/**
 * Configuration class for layouts.
 * Contains the colors, backgrounds which will be used in user interface.
 *
 * Created by Szilard on 4/18/2017.
 */

public class LayoutConfig3Ready implements Serializable, LayoutConfig {


    @SerializedName("welcome_logo")
    String mWelcomeLogo;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("welcome_color")
    private Integer mWelcomeColor;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("switch_thumb_checked_color")
    private Integer mSwitchThumbCheckedColor;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("switch_track_checked_color")
    private Integer mSwitchTrackCheckedColor;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("switch_thumb_unchecked_color")
    private Integer mSwitchThumbUncheckedColor;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("switch_track_unchecked_color")
    private Integer mSwitchTrackUncheckedColor;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("radio_button_checked_color")
    private Integer mRadioButtonCheckedColor;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("radio_button_unchecked_color")
    private Integer mRadioButtonUncheckedColor;

    @SerializedName("logo")
    String mLogo;

    @SerializedName("secondary")
    String mSecondaryColor;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("placeholder")
    Integer mPlaceHolder;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("primary_start")
    Integer mPrimaryColorStart;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("primary_end")
    Integer mPrimaryColorEnd;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("font")
    Integer mFont;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("placeholder_font")
    Integer mPlaceholderFont;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("focused_background")
    Integer mFocusedBackground;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("settings_submenu_color")
    Integer mSettingsSubmenuColor;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("button_active_start")
    Integer mButtonActiveStart;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("button_active_end")
    Integer mButtonActiveEnd;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("button_pressed_start")
    Integer mButtonPressedStart;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("button_pressed_end")
    Integer mButtonPressedEnd;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("button_focused_start")
    Integer mButtonFocusedStart;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("button_focused_end")
    Integer mButtonFocusedEnd;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("button_no_state_start")
    Integer mButtonNoStateStart;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("button_no_state_end")
    Integer mButtonNoStateEnd;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("loading_button_color")
    Integer mLoadingButtonColor;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("input_cursor_color")
    private Integer mInputCursorColor;

    @SerializedName(value = "error")
    @JsonAdapter(ColorTypeAdapter3Ready.class)
    private Integer mError;

    @SerializedName("menu_selector_focused")
    @JsonAdapter(ColorTypeAdapter3Ready.class)
    private Integer mMenuSelectorFocused;

    @SerializedName("menu_selector_active")
    @JsonAdapter(ColorTypeAdapter3Ready.class)
    private Integer mMenuSelectorActive;

    @SerializedName("menu_selector_no_state")
    @JsonAdapter(ColorTypeAdapter3Ready.class)
    private Integer mMenuSelectorNoState;

    @SerializedName(value = "button_focused_font", alternate = "focused_font")
    @JsonAdapter(ColorTypeAdapter3Ready.class)
    Integer mFocusedButtonFont;

    @Deprecated
    @SerializedName("content_markers_color")
    String mContentMarkerFont;

    @Deprecated
    @SerializedName("content_markers_background")
    String mContentMarkerBackground;

    @Deprecated
    @SerializedName("content_markers_transparency")
    Float mContentMarkerTransparency;

    @Deprecated
    @SerializedName("content_markers_priority")
    Integer mContentMarkerPriority;

    @SerializedName("play_icon")
    String mPlayIcon;

    @SerializedName("rewind_icon")
    String mRewindIcon;

    @SerializedName("forward_icon")
    String mForwardIcon;

    @SerializedName("pause_icon")
    String mPauseIcon;

    @SerializedName("play_animated")
    Boolean mPlayAnimated;

    @JsonAdapter(BackgroundPlayerTypeAdapter3Ready.class)
    @SerializedName("background_player_type")
    BackgroundPlayerType mBackgroundPlayerType;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("background")
    Integer mBackground;

    @JsonAdapter(ColorTypeAdapter3Ready.class)
    @SerializedName("notification_background")
    private Integer mNotificationBackground;

    @SerializedName("background_image")
    String mBackgroundImage;

    @SerializedName("mini_player_x")
    Float mMiniPlayerPosX;

    @SerializedName("mini_player_y")
    Float mMiniPlayerPosY;

    @SerializedName("mini_player_width")
    Float mMiniPlayerWidth;

    @SerializedName("mini_player_height")
    Float mMiniPlayerHeight;

    @SerializedName("player_transition")
    Boolean mPlayerTransition;

    @SerializedName("empty_page_message")
    private String mEmptyPageMessage;

    @SerializedName("isolation_mode_message")
    private String mIsolationModeMessage;

    @Override
    public String getLogo() {
        return mLogo;
    }

    @Override
    public int getPrimaryColorStart() {
        return ColorUtils.getColor(mPrimaryColorStart, Color.BLACK);
    }

    @Override
    public int getPrimaryColorEnd() {
        return ColorUtils.getColor(mPrimaryColorEnd, Color.BLACK);
    }

    @Override
    public int getPlaceHolderColor() {
        return ColorUtils.getColor(mPlaceHolder, Color.BLACK);
    }

    @Override
    public int getFontColor() {
        return ColorUtils.getColor(mFont, Color.BLACK);
    }

    @Override
    public int getPlaceholderFontColor() {
        return ColorUtils.getColor(mPlaceholderFont, Color.BLACK);
    }

    @Override
    public int getSettingsSubmenuColor() {
        return ColorUtils.getColor(mSettingsSubmenuColor, Color.BLACK);
    }

    @Override
    public int getFocusedBackground() {
        return ColorUtils.getColor(mFocusedBackground, Color.BLACK);
    }

    @Override
    public int getPlaceholderTransparentFontColor() {
        return ColorUtils.applyTransparencyToColor(getPlaceholderFontColor(), ALPHA_PLACEHOLDER_FONT);
    }

    @Override
    public int getButtonPressedColorStart() {
        // Fallback to focus state.
        return mButtonPressedStart == null ? getButtonFocusedColorStart() :
                ColorUtils.applyTransparencyToColor(mButtonPressedStart, 0.5f);
    }

    @Override
    public int getButtonPressedColorEnd() {
        // Fallback to focus state.
        return mButtonPressedEnd == null ? getButtonFocusedColorEnd() :
                ColorUtils.applyTransparencyToColor(mButtonPressedEnd, 0.5f);
    }

    @Override
    public int getButtonActiveColorStart() {
        return ColorUtils.getColor(mButtonActiveStart, Color.BLACK);
    }

    @Override
    public int getButtonActiveColorEnd() {
        return ColorUtils.getColor(mButtonActiveEnd, Color.BLACK);
    }

    @Override
    public int getButtonFocusedColorStart() {
        return ColorUtils.getColor(mButtonFocusedStart, Color.BLACK);
    }

    @Override
    public int getButtonFocusedColorEnd() {
        return ColorUtils.getColor(mButtonFocusedEnd, Color.BLACK);
    }

    @Override
    public int getButtonNoStateColorStart() {
        return ColorUtils.getColor(mButtonNoStateStart, Color.BLACK);
    }

    @Override
    public int getButtonNoStateColorEnd() {
        return ColorUtils.getColor(mButtonNoStateEnd, Color.BLACK);
    }

    @Override
    public int getLoadingButtonColor() {
        return ColorUtils.getColor(mLoadingButtonColor, Color.BLACK);
    }

    @Override
    public int getButtonFocusedFontColor() {
        return ColorUtils.getColor(mFocusedButtonFont, Color.BLACK);
    }

    @Override
    public int getInputCursorColor() {
        return ColorUtils.getColor(mInputCursorColor, Color.BLACK);
    }

    @Override
    public int getMenuSelectorFocused() {
        return ColorUtils.getColor(mMenuSelectorFocused, Color.BLACK);
    }

    @Override
    public int getMenuSelectorActive() {
        return ColorUtils.getColor(mMenuSelectorActive, Color.BLACK);
    }

    @Override
    public int getMenuSelectorNoState() {
        return ColorUtils.getColor(mMenuSelectorNoState, Color.BLACK);
    }

    @Override
    public int getErrorColor() {
        return ColorUtils.getColor(mError, Color.BLACK);
    }

    @Override
    public String getPlayIcon() {
        return mPlayIcon;
    }

    @Override
    public String getPauseIcon() {
        return mPauseIcon;
    }

    @Override
    public String getRewindIcon() {
        return mRewindIcon;
    }

    @Override
    public String getForwardIcon() {
        return mForwardIcon;
    }

    @Override
    public String getWelcomeLogo() {
        return mWelcomeLogo;
    }

    @Override
    public int getWelcomeColor() {
        return ColorUtils.getColor(mWelcomeColor, Color.BLACK);
    }

    @Override
    public int getSwitchThumbCheckedColor() {
        return ColorUtils.getColor(mSwitchThumbCheckedColor, Color.BLACK);
    }

    @Override
    public int getSwitchTrackCheckedColor() {
        return ColorUtils.getColor(mSwitchTrackCheckedColor, Color.BLACK);
    }

    @Override
    public int getSwitchThumbUncheckedColor() {
        return ColorUtils.getColor(mSwitchThumbUncheckedColor, Color.BLACK);
    }

    @Override
    public int getSwitchTrackUncheckedColor() {
        return ColorUtils.getColor(mSwitchTrackUncheckedColor, Color.BLACK);
    }

    @Override
    public int getRadioButtonCheckedColor() {
        return ColorUtils.getColor(mRadioButtonCheckedColor, Color.BLACK);
    }

    @Override
    public int getRadioButtonUncheckedColor() {
        return ColorUtils.getColor(mRadioButtonUncheckedColor, Color.BLACK);
    }

    @Override
    public int getBackgroundColor() {
        return ColorUtils.getColor(mBackground, Color.BLACK);
    }

    @Override
    public int getNotificationBackgroundColor() {
        return ColorUtils.getColor(mNotificationBackground, Color.BLACK);
    }

    @Override
    public String getBackgroundImage() {
        return mBackgroundImage;
    }

    @Override
    public String getEmptyPageMessage() {
        return mEmptyPageMessage;
    }

    @Override
    public String getIsolationModeMessage() {
        return mIsolationModeMessage;
    }

}
