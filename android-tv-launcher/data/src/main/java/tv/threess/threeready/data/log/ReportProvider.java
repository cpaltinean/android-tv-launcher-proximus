/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.log;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Process;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.generic.helper.UriMatcher;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.ReportContract;
import tv.threess.threeready.api.log.Reporter;
import tv.threess.threeready.api.log.ReporterProvider;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.ReporterInfo;
import tv.threess.threeready.data.generic.BaseProvider;
import tv.threess.threeready.data.utils.SqlUtils;

/**
 * Content provider to access error/event log data
 *
 * @author Arnold Szabo
 * @since 2017.07.19
 */
public class ReportProvider extends BaseProvider {
    private static final String TAG = Log.tag(ReportProvider.class);

    private static final String WORKER_THREAD_NAME = "ReportProvider";

    enum UriIDs {
        Event,
        EventRegister,
        Reporter
    }

    // prepare the uri matcher
    private static final UriMatcher<UriIDs> matcher = new UriMatcher<>(ReportContract.AUTHORITY, UriIDs.class)
            .add(UriIDs.Event, ReportContract.Event.CONTENT_URI)
            .add(UriIDs.Reporter, ReportContract.Reporter.CONTENT_URI);

    private ReportDatabase mDatabase;
    private final Map<String, Reporter<?,?>> mReporters = new ConcurrentHashMap<>();
    private Handler mHandler;

    @Override
    public boolean onCreate() {
        mDatabase = new ReportDatabase(getContext());
        HandlerThread handlerThread = new HandlerThread(WORKER_THREAD_NAME,
                Process.THREAD_PRIORITY_BACKGROUND);
        handlerThread.start();
        mHandler = new Handler(handlerThread.getLooper());

        mHandler.post(() -> {
            android.util.Log.d(TAG, "Initialize reporters.");
            ReporterProvider reporterProvider = Components.get(ReporterProvider.class);
            for (Class<?> reporter : reporterProvider.getReporters()) {
                updateReporter(reporter.getCanonicalName(), ReporterInfo.EMPTY);
            }
        });
        return super.onCreate();
    }

    @Override
    public boolean getNotify(Uri uri) {
        return false;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (matcher.matchEnum(uri)) {
            case Event:
                return ReportContract.Event.MIME_TYPE;
            case Reporter:
                return ReportContract.Reporter.MIME_TYPE;
        }
        throw new UnsupportedOperationException("Unimplemented match: " + matcher.matchEnum(uri));
    }

    @Nullable
    @Override
    public Bundle call(@NonNull String method, @Nullable String arg, @Nullable Bundle extras) {
        try {
            switch (method) {
                case DATABASE_VERSION_CHECK:
                    mDatabase.getReadableDatabase();
                    return super.call(method, arg, extras);

                case ReportContract.CALL_UPDATE_REPORTER:
                    ReporterInfo reporterInfo = extras != null ?
                            (ReporterInfo) extras.getParcelable(ReportContract.EXTRA_REPORTER_INFO) : null;
                    mHandler.post(() -> updateReporter(arg, reporterInfo));
                    return null;

                case ReportContract.CALL_SEND_EVENT:
                    mHandler.post(new SendEvent(extras));
                    return null;

                case ReportContract.CALL_SEND_EVENTS:
                    Reporter<?, ?> reporter = mReporters.get(arg);
                    if (reporter == null) {
                        return null;
                    }
                    mHandler.postDelayed(new SendEvents(Collections.singletonList(reporter), true),
                        reporter.getSendingDelay());
                    return null;
            }
        } catch (Exception e) {
            throw new IllegalStateException(e.getMessage());
        }
        return super.call(method, arg, extras);
    }

    /**
     * Update the reporter with the given reporter info.
     * @param cls Id of the Reporter which will be updated.
     * @param reporterInfo Reporter info which will be used to update the reporter.
     */
    private void updateReporter(String cls, ReporterInfo reporterInfo) {
        Reporter<?, ?> reporter = mReporters.get(cls);
        if (reporterInfo == null) {
            android.util.Log.d(TAG, "Could not update the reporter. Reporter info is null.");
            return;
        }

        if (reporter != null) {
            // Update the existing reporter
            reporter.update(reporterInfo);
            saveReporter(reporter);
            return;
        }

        // Initialize and register reporters
        try {
            ReporterInfo reporterFromDatabase = getReporterFromDatabase(cls);
            if (reporterFromDatabase != null) {
                reporter = (Reporter<?, ?>) Class.forName(cls).getConstructor(Context.class, ReporterInfo.class)
                        .newInstance(this.getContext().getApplicationContext(), reporterFromDatabase);
            } else {
                reporter = (Reporter<?, ?>) Class.forName(cls).getConstructor(Context.class, ReporterInfo.class)
                        .newInstance(this.getContext().getApplicationContext(), reporterInfo);
                saveReporter(reporter);
            }
            mReporters.put(cls, reporter);
            Scheduler.startSchedule(getContext(), reporter);
        } catch (Exception ex) {
            android.util.Log.e(TAG, "Failed to initialize reporter: " + cls, ex);
        }
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        ReportProvider.UriIDs match = matcher.matchEnum(uri);
        final String table;

        switch (match) {
            case Event:
                table = ReportContract.Event.TABLE_NAME;
                break;
            case Reporter:
                table = ReportContract.Reporter.TABLE_NAME;
                break;
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);
        }

        SQLiteDatabase db = mDatabase.getReadableDatabase();
        return super.query(uri, db, table, projection, selection, selectionArgs, sortOrder);
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;

        switch (match) {
            case Event:
                table = ReportContract.Event.TABLE_NAME;
                long id = super.insert(uri, mDatabase.getWritableDatabase(), table, contentValues);

                // return the new id in order to use transaction with withValueBackReference when invoking applyBatch
                return (id != -1) ? Uri.withAppendedPath(ReportContract.Event.CONTENT_URI, String.valueOf(id)) : uri;
            case Reporter:
                table = ReportContract.Reporter.TABLE_NAME;
                super.insert(uri, mDatabase.getWritableDatabase(), table, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
                return uri;
        }

        throw new UnsupportedOperationException("Unimplemented match: " + matcher.matchEnum(uri));
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        ReportProvider.UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);
            case Event:
                table = ReportContract.Event.TABLE_NAME;
                break;
            case Reporter:
                table = ReportContract.Reporter.TABLE_NAME;
                break;
        }

        String replace = uri.getQueryParameter(BaseContract.BULK_REPLACE);
        return bulkInsertReplacePurge(uri,
                mDatabase.getWritableDatabase(), table, replace, values);
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String selection, @Nullable String[] selectionArgs) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;

        switch (match) {
            case Event:
                table = ReportContract.Event.TABLE_NAME;
                break;
            case Reporter:
                table = ReportContract.Reporter.TABLE_NAME;
                break;
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + matcher.matchEnum(uri));
        }

        return super.update(uri, mDatabase.getWritableDatabase(), table, contentValues, selection, selectionArgs);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;

        switch (match) {
            case Event:
                table = ReportContract.Event.TABLE_NAME;
                break;
            case Reporter:
                table = ReportContract.Reporter.TABLE_NAME;
                break;
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + matcher.matchEnum(uri));
        }

        return super.delete(uri, mDatabase.getWritableDatabase(), table, selection, selectionArgs);
    }

    @NonNull
    @Override
    public ContentProviderResult[] applyBatch(@NonNull ArrayList<ContentProviderOperation> operations) throws OperationApplicationException {
        return super.applyBatch(mDatabase.getWritableDatabase(), operations);
    }

    /**
     * @return Reporter info for the given reporter from database.
     */
    private ReporterInfo getReporterFromDatabase(String reporter) {
        SQLiteDatabase db = mDatabase.getReadableDatabase();
        Cursor reporterInfoCursor = null;
        ReporterInfo reporterInfo = null;
        try {
            String selection = ReportContract.Reporter.ID + "=?";
            String[] selectionArgs = {reporter};
            reporterInfoCursor = db.query(ReportContract.Reporter.TABLE_NAME, ReporterProjection.PROJECTION,
                    selection, selectionArgs, null, null, null);
            reporterInfo = ReporterProjection.readRow(reporterInfoCursor);
        } catch (Exception ex) {
            android.util.Log.e(TAG, "Exception during reading reporter from database.", ex);
        } finally {
            FileUtils.closeSafe(reporterInfoCursor);
        }
        return reporterInfo;
    }

    /**
     * Saves a reporter to database.
     * @param reporter Reporter which will be saved to database.
     */
    private void saveReporter(Reporter<?, ?> reporter) {
        SQLiteDatabase db = mDatabase.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(ReportContract.Reporter.ID, reporter.getId());
        contentValues.put(ReportContract.Reporter.ENDPOINT, reporter.getEndpoint());
        contentValues.put(ReportContract.Reporter.HEADERS, reporter.getHeaders());
        contentValues.put(ReportContract.Reporter.MAX_KEEP_PERIOD, reporter.getMaxKeepPeriod());
        contentValues.put(ReportContract.Reporter.MAX_KEEP_SIZE, reporter.getMaxKeepSize());
        contentValues.put(ReportContract.Reporter.MAX_BATCH_SIZE, reporter.getMaxBatchSize());
        contentValues.put(ReportContract.Reporter.BATCH_SIZE, reporter.getBatchSize());
        contentValues.put(ReportContract.Reporter.BATCH_PERIOD, reporter.getBatchPeriod());
        contentValues.put(ReportContract.Reporter.RANDOM_PERIOD, reporter.getRandomPeriod());
        contentValues.put(ReportContract.Reporter.TIMEOUT_PERIOD, reporter.getTimeoutPeriod());
        contentValues.put(ReportContract.Reporter.SENDING_DELAY, reporter.getSendingDelay());

        try {
            db.insertWithOnConflict(ReportContract.Reporter.TABLE_NAME, null, contentValues,
                    SQLiteDatabase.CONFLICT_REPLACE);
            android.util.Log.d(TAG, "Reporter: " + reporter + " saved to database.");
        } catch (Exception e) {
            android.util.Log.e(TAG, "Exception during saving reporter to database.", e);
        }
    }

    /**
     * @param reporter Reporter whose events will be returned.
     * @return The first maxBatchSize elements for a reporter from the database.
     */
    private List<Event<?, ?>> readEvents(Reporter<?, ?> reporter) {
        SQLiteDatabase db = mDatabase.getReadableDatabase();
        List<Event<?, ?>> events = new ArrayList<>();
        Cursor eventCursor = null;
        try {
            String selection = ReportContract.Event.REPORTER_ID + "=?";
            String[] selectionArgs = {reporter.getId()};
            eventCursor = db.query(ReportContract.Event.TABLE_NAME, EventProjection.PROJECTION,
                    selection, selectionArgs, null, null, null, String.valueOf(reporter.getMaxBatchSize()));
            events = EventProjection.readAll(eventCursor);
        } catch (Exception ex) {
            android.util.Log.e(TAG, "Exception during reading events from database.", ex);
        } finally {
            FileUtils.closeSafe(eventCursor);
        }
        return events;
    }

    /**
     * Deletes the obsolete events for a reporter from the database.
     * @return Number of deleted rows.
     */
    private int deleteObsoleteEvents(Reporter reporter) {
        SQLiteDatabase db = mDatabase.getWritableDatabase();
        try {
            long threshold = System.currentTimeMillis() - reporter.getMaxKeepPeriod();
            return db.delete(ReportContract.Event.TABLE_NAME,
                    ReportContract.Event.REPORTER_ID + "=?" +
                            " AND " + ReportContract.Event.ID + " NOT IN (" +
                            " SELECT " + ReportContract.Event.ID +
                            " FROM " + ReportContract.Event.TABLE_NAME +
                            " WHERE " + ReportContract.Event.REPORTER_ID + "=?" +
                            " AND " + ReportContract.Event.TIMESTAMP + " >  " + threshold +
                            " ORDER BY " + ReportContract.Event.TIMESTAMP + " DESC " +
                            " LIMIT " + reporter.getMaxKeepSize() + " )",
                    new String[]{reporter.getId(), reporter.getId()});
        } catch (Exception ex) {
            android.util.Log.e(TAG, "Exception during deleting outdated events from database.", ex);
        }
        return 0;
    }

    /**
     * Deletes the list of events from the database.
     * @param events List of events to be deleted.
     * @return Number of deleted rows.
     */
    private int deleteEvents(List<Event<?, ?>> events) {
        SQLiteDatabase db = mDatabase.getWritableDatabase();
        try {
            long[] ids = new long[events.size()];
            for (int i = 0; i < ids.length; i++) {
                ids[i] = events.get(i).getId();
            }
            return db.delete(ReportContract.Event.TABLE_NAME, SqlUtils.buildSelection(
                    ReportContract.Event.ID, "IN", ids), null);
        } catch (Exception ex) {
            android.util.Log.e(TAG, "Exception during deleting events from database.", ex);
        }
        return 0;
    }

    /**
     * Saves the event to the database.
     * @param batchReporters List of batch reporters.
     * @param event Event which will be saved to database.
     */
    private void saveEvent(HashSet<Reporter<?, ?>> batchReporters, Event event) {
        List<ContentValues> contentValuesList = new ArrayList<>();
        for (Reporter<?, ?> reporter : batchReporters) {
            android.util.Log.d(TAG, "save event: " + event.getDomain() + " " + event.getName() +
                    " for reporter: " + reporter.getId());
            String serializedEvent = reporter.serializeEvent(event);
            if (serializedEvent == null) {
                continue;
            }

            ContentValues contentValues = new ContentValues();
            contentValues.put(ReportContract.Event.REPORTER_ID, reporter.getId());
            contentValues.put(ReportContract.Event.NAME, event.getName());
            contentValues.put(ReportContract.Event.DOMAIN, event.getDomain());
            contentValues.put(ReportContract.Event.TIMESTAMP, System.currentTimeMillis());
            contentValues.put(ReportContract.Event.VALUE, serializedEvent);
            contentValues.put(ReportContract.Event.INCIDENT_COUNT, 0);
            contentValues.put(ReportContract.Event.LAST_INCIDENT_TIME, System.currentTimeMillis());
            contentValues.put(ReportContract.Event.LAST_UPDATED, System.currentTimeMillis());
            contentValuesList.add(contentValues);
        }

        getContext().getContentResolver().bulkInsert(ReportContract.Event.CONTENT_URI,
                contentValuesList.toArray(new ContentValues[0]));
    }

    private class SendEvent implements Runnable {

        private final Bundle mBundle;

        private SendEvent(Bundle bundle) {
            mBundle = bundle;
        }

        @Override
        public void run() {
            try {
                boolean ignored = true;
                String name = mBundle.getString(ReportContract.Event.NAME);
                String domain = mBundle.getString(ReportContract.Event.DOMAIN);
                long timestamp = mBundle.getLong(ReportContract.Event.TIMESTAMP);
                Bundle details = mBundle.getBundle(ReportContract.Event.VALUE);
                Event event = new Event<>(name, domain, timestamp, details);

                final HashSet<Reporter<?, ?>> batchReporters = new HashSet<>();
                for (Reporter<?, ?> reporter : mReporters.values()) {
                    if (reporter == null || TextUtils.isEmpty(reporter.getId()) ||
                            (reporter.isInstant() && reporter.isBatch())) {
                        android.util.Log.d(TAG, "Invalid event reporter: " + reporter);
                        // reporter is disabled.
                        continue;
                    }

                    if (!reporter.canSend(event)) {
                        continue;
                    }

                    if (reporter.isBatch()) {
                        batchReporters.add(reporter);
                        continue;
                    }

                    try {
                        reporter.sendEvent(event);
                        ignored = false;
                    } catch (Exception e) {
                        if (reporter.isInstant()) {
                            android.util.Log.e(TAG, "Failed to send event, discarding instant reporter", e);
                            continue;
                        }
                        android.util.Log.e(TAG, "Failed to send event, persisting event and reporter ", e);
                        batchReporters.add(reporter);
                    }
                }
                if (batchReporters.size() > 0) {
                    ignored = false;
                    saveEvent(batchReporters, event);
                    for (Reporter<?, ?> batchReporter : batchReporters) {
                        android.util.Log.d(TAG, "Sending events in batch for reporter:  " + batchReporter + " with delay: " +
                                batchReporter.getSendingDelay());
                        mHandler.postDelayed(new SendEvents(Collections.singletonList(batchReporter), false),
                                batchReporter.getSendingDelay());
                    }
                }
                android.util.Log.v(TAG, (ignored ? "Ignoring: " : "Sending ") + event);
            } catch (Exception e) {
                android.util.Log.e(TAG, "Failed to save event", e);
            }
        }
    }

    private class SendEvents implements Runnable {

        private final Collection<Reporter<?, ?>> mReporters;
        private final boolean mForceSend;

        private SendEvents(Collection<Reporter<?, ?>> reporters, boolean forceSend) {
            mReporters = reporters;
            mForceSend = forceSend;
        }

        @Override
        public void run() {
            try {
                for (Reporter reporter : mReporters) {
                    if (reporter == null || TextUtils.isEmpty(reporter.getId())) {
                        android.util.Log.d(TAG, "Invalid event reporter: " + reporter);
                        // reporter is disabled.
                        continue;
                    }

                    // Delete the obsolete events from database.
                    int deletedObsoleteEvents = deleteObsoleteEvents(reporter);
                    android.util.Log.d(TAG, "Deleted obsolete events: " + deletedObsoleteEvents +
                            " for reporter: " + reporter.getId());

                    try {
                        List<Event<?, ?>> events = readEvents(reporter);
                        if (events == null || (events.size() < reporter.getBatchSize() && !mForceSend)) {
                            // nothing or no need to send events
                            continue;
                        }

                        android.util.Log.d(TAG, "Sending events for reporter: " + reporter.getId());
                        reporter.sendEvents(events);
                        int deletedEvents = deleteEvents(events);
                        android.util.Log.d(TAG, "Deleted sent events: " + deletedEvents +
                                " for reporter: " + reporter.getId());

                        if (events.size() != 0 && events.size() == reporter.getMaxBatchSize()) {
                            mHandler.post(new SendEvents(Collections.singletonList(reporter), false));
                        }
                    } catch (Exception e) {
                        android.util.Log.e(TAG, "Failed to send events", e);
                    }
                }
            } catch (Exception e) {
                android.util.Log.e(TAG, "Failed to send events", e);
            }
        }
    }

    public static class Scheduler extends BroadcastReceiver {

        static final String INTENT_EXTRA_EVENT_REPORTER_ID = "intent.extra.EVENT_REPORTER";

        static final String INTENT_EXTRA_EVENT_REPORT_RATE = "intent.extra.REPORT_RATE";

        private static void startSchedule(Context context, Reporter<?, ?> reporter) {
            String action = "intent.action." + reporter.getId();
            Uri uri = ReportContract.BASE_URI;

            Intent intent = new Intent(action, uri, context, ReportProvider.Scheduler.class);
            intent.putExtra(INTENT_EXTRA_EVENT_REPORT_RATE, reporter.getBatchPeriod());
            intent.putExtra(INTENT_EXTRA_EVENT_REPORTER_ID, reporter.getId());
            startSchedule(context, intent);
        }

        private static void startSchedule(Context context, Intent intent) {
            long interval = intent.getLongExtra(INTENT_EXTRA_EVENT_REPORT_RATE, 0L);
            if (interval <= 0) {
                return;
            }
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            alarmManager.setExact(AlarmManager.RTC, System.currentTimeMillis() + interval, pendingIntent);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            startSchedule(context, intent);
            String reporterId = intent.getStringExtra(INTENT_EXTRA_EVENT_REPORTER_ID);
            context.getContentResolver().call(ReportContract.BASE_URI, ReportContract.CALL_SEND_EVENTS, reporterId, intent.getExtras());
        }
    }
}
