/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.tv;


import android.app.Application;

import java.util.concurrent.TimeUnit;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.model.CacheSettings;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.utils.CacheUpdater;
import tv.threess.threeready.data.utils.PeriodicCacheUpdater;

/**
 * Base common implementation class for the tv handler.
 * All generic tv related action should be implemented here.
 *
 * @author Barabas Attila
 * @since 2018.02.08
 */
public class BaseTvRepository {

    protected final Application mApp;

    public BaseTvRepository(Application application) {
        mApp = application;
    }

    protected static final PeriodicCacheUpdater channelUpdater = new PeriodicCacheUpdater(Settings.lastChannelsSyncTime,
            Components.get(CacheSettings.class)::getChannelsUpdatePeriod);

    protected static final CacheUpdater baseBroadcastUpdater = new PeriodicCacheUpdater(Settings.lastBaseBroadcastSyncTime,
            (timeUnit) -> Components.get(CacheSettings.class).getEpgCacheWindowLength(timeUnit) / 2);

    protected static final PeriodicCacheUpdater broadcastUpdater = new PeriodicCacheUpdater(Settings.lastBroadcastSyncTime,
            Components.get(CacheSettings.class)::getEpgUpdatePeriod) {

        @Override
        public boolean isCacheValid() {
            return super.isCacheValid() && baseBroadcastUpdater.isCacheValid();
        }

        @Override
        protected long getNextScheduleTime(long period) {
            long periodicScheduleTime = super.getNextScheduleTime(period);

            long windowEndScheduleTime = BaseWorkManager.getNextUpdateScheduleTime(
                    Settings.lastBaseBroadcastSyncTime.get(0L),
                    Components.get(CacheSettings.class).getEpgCacheWindowLength(TimeUnit.MILLISECONDS) / 2);

            // Schedule next update before the cache window ends.
            return Math.min(periodicScheduleTime, windowEndScheduleTime);
        }
    };
}
