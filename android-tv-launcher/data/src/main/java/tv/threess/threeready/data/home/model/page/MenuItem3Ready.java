/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.home.model.page;

import com.google.gson.annotations.SerializedName;

import tv.threess.threeready.api.home.model.menu.MenuItem;

/**
 * POJO class to configure a main or submenu item on the UI.
 *
 * Created by Szilard on 4/13/2017.
 */
public class MenuItem3Ready implements MenuItem {

    @SerializedName("id")
    private final String mId;

    @SerializedName("title")
    private final String mTitle;

    MenuItem3Ready(String id, String title) {
        mId = id;
        mTitle = title;
    }

    @Override
    public String getReportName() {
        return mTitle;
    }

    @Override
    public String getReportReference() {
        return mId;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getId() {
        return mId;
    }
}