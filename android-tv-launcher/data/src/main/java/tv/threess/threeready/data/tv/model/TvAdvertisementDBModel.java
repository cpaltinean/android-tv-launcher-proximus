package tv.threess.threeready.data.tv.model;

import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.log.Log;

public class TvAdvertisementDBModel implements TvAdvertisement {
    private String mCreativeId;

    private String mName;

    private String mBroadcaster;

    private String mFilename;

    private long mSize;

    private long mDuration;

    private long mExpires;

    private boolean mOnMarker;

    private int mRetryCount;

    @Override
    public String getCreativeId() {
        return mCreativeId;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public String getBroadcaster() {
        return mBroadcaster;
    }

    @Override
    public String getFilename() {
        return mFilename;
    }

    @Override
    public long getSize() {
        return mSize;
    }

    @Override
    public long getDuration(TimeUnit unit) {
        return unit.convert(mDuration, TimeUnit.MILLISECONDS);
    }

    @Override
    public long getExpires() {
        return mExpires;
    }

    @Override
    public boolean isOnMarker() {
        return mOnMarker;
    }

    public int getAttemptCount() {
        return mRetryCount;
    }

    public static class Builder {

        TvAdvertisementDBModel mInstance;

        public Builder() {
            mInstance = new TvAdvertisementDBModel();
        }

        public TvAdvertisementDBModel build() {
            return mInstance;
        }

        public Builder setBroadcaster(String value) {
            mInstance.mBroadcaster = value;
            return this;
        }

        public Builder setCreativeId(String value) {
            mInstance.mCreativeId = value;
            return this;
        }

        public Builder setDuration(long value) {
            mInstance.mDuration = value;
            return this;
        }

        public Builder setExpires(long value) {
            mInstance.mExpires = value;
            return this;
        }

        public Builder setOnMarker(boolean value) {
            mInstance.mOnMarker = value;
            return this;
        }

        public Builder setFilename(String value) {
            mInstance.mFilename = value;
            return this;
        }

        public Builder setName(String value) {
            mInstance.mName = value;
            return this;
        }

        public Builder setSize(long value) {
            mInstance.mSize = value;
            return this;
        }

        public Builder setRetryCount(int value) {
            mInstance.mRetryCount = value;
            return this;
        }
    }

    @Override
    public String toString() {
        return "TvAdvertisement{" +
                "Expires: " + Log.timestamp(mExpires) +
                ", OnMarker: " + mOnMarker +
                ", file: " + mFilename +
                "}";
    }
}
