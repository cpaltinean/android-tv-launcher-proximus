package tv.threess.threeready.data.tv.observable;

import android.content.Context;
import android.net.Uri;

import java.util.Objects;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.NowOnTvCache;
import tv.threess.threeready.data.account.ConfigContract;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.home.observable.TvChannelObservable;
import tv.threess.threeready.data.tv.TvContract;

/**
 * Rx Observable which emits in case the channel lineup changed and we need to re-tune because the
 * channel behind the last played channel number changed also.
 *
 * @author Istvan Nagy
 * @since 2020.08.25
 */
public class LastPlayedChannelIdChangeObservable extends BaseContentObservable<String> {
    public static final String TAG = Log.tag(TvChannelObservable.class);

    private final NowOnTvCache mNowOnTvCache = Components.get(NowOnTvCache.class);

    private String mLastPlayedChannelId;

    public LastPlayedChannelIdChangeObservable(Context context) {
        super(context);
        mLastPlayedChannelId = mNowOnTvCache.getLastPlayedChannelId();
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<String> observableEmitter) {
        emitData(observableEmitter);
    }

    @Override
    public void subscribe(ObservableEmitter<String> emitter) throws Exception {
        super.subscribe(emitter);
        registerObserver(TvContract.Channel.CHANNEL_MEMORY_CACHE_CONTENT_URI);
        registerObserver(ConfigContract.buildSettingsUriForKey(Settings.lastPlayedChannelNumber.name()));
        emitData(emitter);
    }

    /**
     * Emit the data through the given {@link ObservableEmitter} so the subscribed user can react on it.
     *
     * @param emitter The emitter used to emit the updated channel id
     */
    private void emitData(ObservableEmitter<String> emitter) {
        String newLastPlayedChannelId = mNowOnTvCache.getLastPlayedChannelId();
        if (!Objects.equals(mLastPlayedChannelId, newLastPlayedChannelId)) {
            Log.d(TAG, "Last played channel changed. Old channel : "
                    + mLastPlayedChannelId + ", new channel : " + newLastPlayedChannelId);
            mLastPlayedChannelId = newLastPlayedChannelId;
            emitter.onNext(mLastPlayedChannelId);
        }
    }
}
