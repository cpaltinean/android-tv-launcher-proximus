/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.home.model.module;

import com.google.gson.annotations.SerializedName;

import tv.threess.threeready.api.home.model.module.ModuleDataSourcePaginationParams;

/**
 * Data source configuration options for pagination.
 * Contains the page size and the threshold after which the next page should be requested.
 *
 * @author Barabas Attila
 * @since 2019.11.08
 */
public class ModuleDataSourcePaginationParams3Ready implements ModuleDataSourcePaginationParams {

    @SerializedName("size")
    private final int mSize;

    @SerializedName("threshold")
    private final int mThreshold;

    public ModuleDataSourcePaginationParams3Ready(int mSize, int mThreshold) {
        this.mSize = mSize;
        this.mThreshold = mThreshold;
    }


    @Override
    public int getSize() {
        return mSize;
    }

    @Override
    public int getThreshold() {
        return mThreshold;
    }
}
