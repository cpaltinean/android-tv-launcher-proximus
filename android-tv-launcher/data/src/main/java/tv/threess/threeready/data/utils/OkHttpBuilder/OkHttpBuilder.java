package tv.threess.threeready.data.utils.OkHttpBuilder;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.EnumMap;
import java.util.concurrent.Callable;

import javax.net.ssl.X509TrustManager;

import android.content.Context;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.internal.platform.Platform;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.BuildConfig;

/**
 * Util class for OkHTTP client builder.
 *
 * @author Andor Lukacs
 * @since 06/01/2021
 */
public class OkHttpBuilder {
    private static final String TAG = OkHttpBuilder.class.getSimpleName();

    /**
     * Know Dscp values, for more info see:
     * <a href="http://www.netcontractor.pl/download/QoS%20Values%20Calculator%20v3.pdf">QoS Values Calculator v3</a>
     * <a href="https://wiki.innovaphone.com/index.php?title=Howto:Calculate_Values_for_Type_of_Service_%28ToS%29_from_DiffServ_or_DSCP_Values">Calculate Values for Type of Service (ToS) from DiffServ or DSCP Values</a>
     */
    public enum Dscp {
        CS0(0),       // CS0
        CS1(8),       // CS1
        AF11(10),     // AF11 low drop
        AF12(12),     // AF12 medium drop
        AF13(14),     // AF13 high drop
        CS2(16),      // CS2
        AF21(18),     // AF21 low drop
        AF22(20),     // AF22 medium drop
        AF23(22),     // AF23 high drop
        CS3(24),      // CS3
        AF31(26),     // AF31 low drop
        AF32(28),     // AF32 medium drop
        AF33(30),     // AF33 high drop
        CS4(32),      // CS4
        AF41(34),     // AF41 low drop
        AF42(36),     // AF42 medium drop
        AF43(38),     // AF43 high drop
        CS5(40),      // CS5
        VA(44),       // Voice Admit
        EF(46),       // Expedited Forwarding
        CS6(48),      // CS6
        CS7(56);      // CS7

        private final int tos;

        Dscp(int dscp) {
            this.tos = dscp << 2;
        }
    }

    private static final javax.net.ssl.SSLSocketFactory defaultSslSocketFactory = (javax.net.ssl.SSLSocketFactory) javax.net.ssl.SSLSocketFactory.getDefault();
    private static final javax.net.SocketFactory defaultSocketFactory = javax.net.SocketFactory.getDefault();

    private static final EnumMap<Dscp, javax.net.ssl.SSLSocketFactory> sslSocketFactoryCache = new EnumMap<>(Dscp.class);

    private static final EnumMap<Dscp, javax.net.SocketFactory> socketFactoryCache = new EnumMap<>(Dscp.class);

    /**
     * Initialize Stetho to allow debugging of network request
     */
    public static void initializeStetho(Context context) {
        if (!BuildConfig.ENABLE_STETHO) {
            return;
        }

        Stetho.initialize(Stetho.newInitializerBuilder(context)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(context))
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(context))
                .build()
        );
    }

    /**
     * Create an OkHTTP client with specific set DSCP TOS priority bits.
     *
     * @param value TOS bit in hexadecimal
     * @return OkHTTP builder.
     */
    public static OkHttpClient.Builder createClient(Dscp value) {
        X509TrustManager trustManager = Platform.get().platformTrustManager();
        OkHttpClient.Builder mTosOkHttpClient = new OkHttpClient().newBuilder();

        mTosOkHttpClient.sslSocketFactory(getSSLSocketFactory(value), trustManager);
        mTosOkHttpClient.socketFactory(getSocketFactory(value));
        if (BuildConfig.ENABLE_STETHO) {
            mTosOkHttpClient.addNetworkInterceptor(new StethoInterceptor());
        }

        return mTosOkHttpClient;
    }

    /**
     * Get the secure SSL socket factory for the OkHttp Clients that sets up specific DSCP priority bit (TCP TOS bit)
     * ** Used for HTTPS calls **
     *
     * @param value - Used to identify the factory
     */
    public static javax.net.ssl.SSLSocketFactory getSSLSocketFactory(Dscp value) {
        synchronized (sslSocketFactoryCache) {
            javax.net.ssl.SSLSocketFactory factory = sslSocketFactoryCache.get(value);
            if (factory == null) {
                factory = new SSLSocketFactory(value);
                sslSocketFactoryCache.put(value, factory);
            }
            return factory;
        }
    }

    /**
     * Get the socket factory for the OkHttp Clients that sets up specific DSCP priority bit (TCP TOS bit)
     * ** Used for HTTP calls **
     */
    private static javax.net.SocketFactory getSocketFactory(Dscp value) {
        synchronized (socketFactoryCache) {
            javax.net.SocketFactory factory = socketFactoryCache.get(value);
            if (factory == null) {
                factory = new SocketFactory(value);
                socketFactoryCache.put(value, factory);
            }
            return factory;
        }
    }

    private static class SSLSocketFactory extends javax.net.ssl.SSLSocketFactory {
        private final Dscp mDscp;

        SSLSocketFactory(Dscp value) {
            mDscp = value;
        }

        @Override
        public String[] getDefaultCipherSuites() {
            return defaultSslSocketFactory.getDefaultCipherSuites();
        }

        @Override
        public String[] getSupportedCipherSuites() {
            return defaultSslSocketFactory.getSupportedCipherSuites();
        }

        @Override
        public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
            return applyTrafficBits(() -> defaultSslSocketFactory.createSocket(s, host, port, autoClose), mDscp);
        }

        @Override
        public Socket createSocket(String host, int port) throws IOException {
            return applyTrafficBits(() -> defaultSslSocketFactory.createSocket(host, port), mDscp);
        }

        @Override
        public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException {
            return applyTrafficBits(() -> defaultSslSocketFactory.createSocket(host, port, localHost, localPort), mDscp);
        }

        @Override
        public Socket createSocket(InetAddress host, int port) throws IOException {
            return applyTrafficBits(() -> defaultSslSocketFactory.createSocket(host, port), mDscp);
        }

        @Override
        public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
            return applyTrafficBits(() -> defaultSslSocketFactory.createSocket(address, port, localAddress, localPort), mDscp);
        }
    }

    private static class SocketFactory extends javax.net.SocketFactory {
        private final Dscp mDscp;

        SocketFactory(Dscp value) {
            mDscp = value;
        }

        @Override
        public Socket createSocket() throws IOException {
            return applyTrafficBits(defaultSocketFactory::createSocket, mDscp);
        }

        @Override
        public Socket createSocket(String host, int port) throws IOException {
            return applyTrafficBits(() -> defaultSocketFactory.createSocket(host, port), mDscp);
        }

        @Override
        public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException {
            return applyTrafficBits(() -> defaultSocketFactory.createSocket(host, port, localHost, localPort), mDscp);
        }

        @Override
        public Socket createSocket(InetAddress host, int port) throws IOException {
            return applyTrafficBits(() -> defaultSocketFactory.createSocket(host, port), mDscp);
        }

        @Override
        public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
            return applyTrafficBits(() -> defaultSocketFactory.createSocket(address, port, localAddress, localPort), mDscp);
        }
    }

    /**
     * @return the created socket.
     * @throws IOException when some problems occurs in creation or in set traffic class.
     */
    private static Socket applyTrafficBits(Callable<Socket> creator, Dscp dscp) throws IOException {
        Socket socket = null;
        try {
            socket = creator.call();
            socket.setTrafficClass(dscp.tos);
        } catch (Exception e) {
            Log.e(TAG, "Socket creation failed.", e);
            FileUtils.closeSafe(socket);
            throw new IOException(e);
        }
        return socket;
    }
}
