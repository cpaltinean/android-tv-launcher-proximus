package tv.threess.threeready.data.utils;

import android.net.Uri;

import androidx.annotation.Nullable;

/**
 * Interface used for Tv, PVR and VOD cache validation.
 *
 * @author David
 * @since 23.03.2022
 */
public interface CacheValidator {

    /**
     * @return True if the cache is up to date.
     */
    boolean isCacheValid();

    /**
     * @return True if there is some cache data available, even if it's expired.
     */
    default boolean hasCache() {
        return false;
    }

    /**
     * @return The Uri to subscribed on to detect changes.
     */
    @Nullable
    default Uri getUpdateUri() {
        return null;
    }

    /**
     * @return The name of the cache validator in logs.
     */
    default String getName() {
        return "";
    }
}
