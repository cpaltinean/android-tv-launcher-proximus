/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.mw;

import android.content.Context;
import android.os.PowerManager;

import java.util.concurrent.TimeUnit;

import androidx.work.Data;
import androidx.work.WorkerParameters;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.api.middleware.MwServiceRepository;
import tv.threess.threeready.api.tv.TvServiceRepository;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.generic.BaseWorker;
import tv.threess.threeready.data.generic.JobIntent;
import tv.threess.threeready.data.generic.WaitForInternetTask;

/**
 * Performs the scheduling of quiescent reboot around midnight
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.07.27
 */
public class MwService {
    private static final String TAG = Log.tag(MwService.class);

    private static final long START_HOUR_MILLIS = TimeUnit.HOURS.toMillis(2);
    private static final long END_HOUR_MILLIS = TimeUnit.HOURS.toMillis(6);


    public static void scheduleRusWindowAlarm(Context context) {
        int start = (int) Settings.remoteUpdatePollStartTime.get(START_HOUR_MILLIS);
        int end = (int) Settings.remoteUpdatePollEndTime.get(END_HOUR_MILLIS);

        // distribute randomly between start and end in the future, allowing a q-reboot also the same day
        // protecting the system and application by the low memory killer app
        TimeBuilder triggerTime = TimeBuilder.local().floor(TimeUnit.DAYS)
                .distribute(TimeUnit.MILLISECONDS, start, end);

        if (triggerTime.get() < System.currentTimeMillis()) {
            // make sure it is in the future
            triggerTime.add(TimeUnit.DAYS, 1);
        }
        BaseWorkManager.scheduleWakeupAlarm(context, buildRusWindowIntent(), triggerTime.get());
    }

    public static void reScheduleRusWindowAlarm(Context context) {
        int start = (int) Settings.remoteUpdatePollStartTime.get(START_HOUR_MILLIS);
        int end = (int) Settings.remoteUpdatePollEndTime.get(END_HOUR_MILLIS);

        // distribute randomly between start and end the next day, restricting only one alarm to be scheduled a day
        TimeBuilder triggerTime = TimeBuilder.local().ceil(TimeUnit.DAYS)
                .distribute(TimeUnit.MILLISECONDS, start, end);

        BaseWorkManager.scheduleWakeupAlarm(context, buildRusWindowIntent(), triggerTime.get());
    }

    public static JobIntent buildRusWindowIntent() {
        return new JobIntent(RusWindow.class, Data.EMPTY);
    }

    public static JobIntent buildApplySystemUpdateIntent() {
        return new JobIntent(ApplySystemUpdate.class, Data.EMPTY);
    }

    public static JobIntent buildCheckSystemUpdateIntent() {
        return new JobIntent(CheckSystemUpdate.class, Data.EMPTY);
    }

    private static long getRusWindowEndAt(long timestamp) {
        int end = (int) Settings.remoteUpdatePollEndTime.get(END_HOUR_MILLIS);
        TimeBuilder rusWindowEnd = TimeBuilder.local(timestamp).floor(TimeUnit.DAYS).add(end);
        if (rusWindowEnd.get() < System.currentTimeMillis()) {
            // make sure it is in the future
            rusWindowEnd.add(TimeUnit.DAYS, 1);
        }
        return rusWindowEnd.get();
    }

    public static class RusWindow extends BaseWorker {

        public RusWindow(Context context, WorkerParameters workerParams) {
            super(context, 0, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            reScheduleRusWindowAlarm(getApplicationContext());

            AppConfig mAppConfig = Components.get(AppConfig.class);
            MwServiceRepository mMWServiceRepository = Components.get(MwServiceRepository.class);
            TvServiceRepository mTvServiceRepository = Components.get(TvServiceRepository.class);

            boolean updateDownloaded = false;
            PowerManager.WakeLock mWakeLock = mMWServiceRepository.getWakeLock();
            try {
                // Acquire wakelock before doing anything so the STB doesn't go back to sleep
                long rusWindowTimeout = mAppConfig.getRusWindowTimeout(TimeUnit.MILLISECONDS);
                if (mWakeLock != null) {
                    mWakeLock.acquire(rusWindowTimeout);
                }
                // Wait for internet.
                WaitForInternetTask waitForInternetTask = new WaitForInternetTask();
                waitForInternetTask.waitForInternet(mAppConfig.getInternetCheckTimeout(TimeUnit.MILLISECONDS));

                try {
                    // Download targeted advertisement files in the rus window
                    mTvServiceRepository.downloadAdvertisements(System.currentTimeMillis(), null);
                } catch (Exception e) {
                    Log.e(TAG, "Advertisement update failed.", e);
                }

                // Check if update check needed in the current RUS window.
                long rusWindowEnd = getRusWindowEndAt(System.currentTimeMillis());

                // Check and download system update.
                updateDownloaded = mMWServiceRepository.checkSystemUpdate(rusWindowEnd, true);
            } catch (Exception e) {
                Log.e(TAG, "System update check failed.", e);
            } finally {
                // Release wakelock.
                if (mWakeLock != null) {
                    mWakeLock.release();
                }
            }

            // Trigger only if the reboot was not initiated by the update.
            if (!updateDownloaded) {
                mMWServiceRepository.quiescentReboot();
            }
        }
    }

    public static class CheckSystemUpdate extends BaseWorker {

        public CheckSystemUpdate(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            long atTime = getRusWindowEndAt(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1));
            Components.get(MwServiceRepository.class).checkSystemUpdate(atTime, false);
        }
    }

    public static class ApplySystemUpdate extends BaseWorker {

        public ApplySystemUpdate(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            Components.get(MwRepository.class).applySystemUpdate();
        }
    }
}
