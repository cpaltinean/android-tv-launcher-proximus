package tv.threess.threeready.data.pvr.observable;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.AccountCache;
import tv.threess.threeready.api.generic.model.BookmarkType;
import tv.threess.threeready.api.generic.model.IBookmark;
import tv.threess.threeready.api.generic.model.ISeason;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pvr.model.IRecording;
import tv.threess.threeready.api.pvr.model.IRecordingSeries;
import tv.threess.threeready.api.pvr.model.SeriesRecordingDetailModel;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.pvr.PvrCache;
import tv.threess.threeready.data.pvr.PvrContract;

/**
 * Observable to get the detail model for a series recoridng detail page.
 * It contains the full series information and the last played recording episode.
 *
 * @author Andor Lukacs
 * @since 2020-06-05
 */
public class SeriesDetailModelObservable extends BaseContentObservable<SeriesRecordingDetailModel> {
    private static final String TAG = Log.tag(SeriesDetailModelObservable.class);

    private final PvrCache mPvrCache = Components.get(PvrCache.class);
    private final AccountCache.Bookmarks mAccountCache = Components.get(AccountCache.Bookmarks.class);

    private final String mSeriesId;

    public SeriesDetailModelObservable(Context context, String seriesId) {
        super(context);
        mSeriesId = seriesId;
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<SeriesRecordingDetailModel> observableEmitter) {
        publishModel(observableEmitter);
    }

    @Override
    public void subscribe(ObservableEmitter<SeriesRecordingDetailModel> emitter) throws Exception {
        super.subscribe(emitter);

        // Observe status changes.
        registerObserver(true,
                PvrContract.buildRecordingUriForSeries(mSeriesId));

        publishModel(emitter);
    }

    private void publishModel(ObservableEmitter<SeriesRecordingDetailModel> emitter) {
        if (emitter == null) {
            // Already canceled.
            return;
        }

        if (TextUtils.isEmpty(mSeriesId)) {
            emitter.onError(new Exception("No recording episode available."));
        }

        IRecordingSeries seriesRecording;
        Log.d(TAG, "Load series recording : " + mSeriesId);
        seriesRecording = mPvrCache.getSeriesRecording(mSeriesId);

        IRecording lastEpisode = getLastPlayedRecordingInSeries(seriesRecording);
        emitter.onNext(new SeriesRecordingDetailModel(seriesRecording, lastEpisode));
    }

    /**
     * @param seriesRecording The series recording to search episode form.
     * @return The last played {@link IRecording} in the {@link IRecordingSeries}
     * or null if there is no such episode.
     */
    @Nullable
    private IRecording getLastPlayedRecordingInSeries(IRecordingSeries seriesRecording) {
        if (seriesRecording == null) {
            return null;
        }

        IRecording lastPlayedRecording = null;
        long lastUpdated = 0;

        for (ISeason<IRecording> season : seriesRecording.getSeasons()) {
            if (season != null) {
                for (IRecording episode : season.getEpisodes()) {
                    IBookmark bookmark = mAccountCache.getBookmark(episode.getId(), BookmarkType.Recording);
                    if (bookmark == null) {
                        continue;
                    }

                    long episodeBookmarkStartPosition = bookmark.getTimestamp();
                    if (!bookmark.isEmpty() && episodeBookmarkStartPosition > lastUpdated) {
                        lastPlayedRecording = episode;
                        lastUpdated = episodeBookmarkStartPosition;
                    }
                }
            }
        }

        return lastPlayedRecording;
    }
}
