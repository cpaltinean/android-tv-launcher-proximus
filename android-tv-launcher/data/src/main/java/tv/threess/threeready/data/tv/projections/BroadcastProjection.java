/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.tv.projections;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.List;

import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.api.tv.model.IBroadcast;
import tv.threess.threeready.data.generic.IProjection;
import tv.threess.threeready.data.generic.helper.ImageProjectionHelper;
import tv.threess.threeready.data.tv.model.TvBroadcastDBModel;

/**
 * Projection to get broadcast with additional channel information (channel image url)
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.28
 */
public enum BroadcastProjection implements IProjection {
    EVENT_ID(TvContract.Broadcast.EVENT_ID),
    CHANNEL_ID(TvContract.Channel.TABLE_NAME + "." + TvContract.Channel.ID),
    START(TvContract.Broadcast.START),
    END(TvContract.Broadcast.END),
    TITLE(TvContract.Broadcast.TITLE),
    DESCRIPTION(TvContract.Broadcast.DESCRIPTION),
    GENRES(TvContract.Broadcast.GENRES),
    SUB_GENRES(TvContract.Broadcast.SUB_GENRES),
    RELEASE_YEAR(TvContract.Broadcast.RELEASE_YEAR),
    SEASON(TvContract.Broadcast.SEASON),
    EPISODE(TvContract.Broadcast.EPISODE),
    EPISODE_TITLE(TvContract.Broadcast.EPISODE_TITLE),
    PROGRAM_ID(TvContract.Broadcast.PROGRAM_ID),
    SERIES_ID(TvContract.Broadcast.SERIES_PROGRAM_ID),
    UNSKIPABLE_ASSET_ID(TvContract.Broadcast.UNSKIPABLE_ASSET_ID),
    ACTORS(TvContract.Broadcast.ACTORS),
    DIRECTORS(TvContract.Broadcast.DIRECTORS),
    PARENTAL_RATING(TvContract.Broadcast.PARENTAL_RATING),
    IS_BLACKLISTED(TvContract.Broadcast.IS_BLACKLISTED),
    IMAGE_URL(TvContract.Broadcast.IMAGE_URL),
    IMAGE_TYPE(TvContract.Broadcast.IMAGE_TYPE),
    HAS_DESCRIPTIVE_AUDIO(TvContract.Broadcast.HAS_DESCRIPTIVE_AUDIO),
    HAS_DESCRIPTIVE_SUBTITLE(TvContract.Broadcast.HAS_DESCRIPTIVE_SUBTITLE),
    IS_NPVR_ENABLED(TvContract.Broadcast.TABLE_NAME, TvContract.Broadcast.IS_NPVR_ENABLE);

    private static final String TAG = Log.tag(BroadcastProjection.class);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    BroadcastProjection(String column) {
        this.mColumn = column;
    }

    BroadcastProjection(String tableName, String column) {
        mColumn = tableName + "." + column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    /**
     * Read a broadcast from cursor row.
     *
     * @param cursor Cursor with program details.
     * @return Model for the specified program.
     */
    public static IBroadcast readRow(Cursor cursor) {
        return readRow(cursor, false);
    }

    /**
     * Read a broadcast from cursor row.
     *
     * @param cursor         Cursor with program details.
     * @param isPreviousLive Flag used to know if the previous program was live.
     * @return Model for the specified program.
     */
    public static IBroadcast readRow(Cursor cursor, boolean isPreviousLive) {
        return TvBroadcastDBModel.Builder.builder()
                .setId(cursor.getString(EVENT_ID.ordinal()))
                .setProgramID(cursor.getString(PROGRAM_ID.ordinal()))
                .setChannelID(cursor.getString(CHANNEL_ID.ordinal()))
                .setStart(cursor.getLong(START.ordinal()))
                .setEnd(cursor.getLong(END.ordinal()))
                .setTitle(cursor.getString(TITLE.ordinal()))
                .setDescription(cursor.getString(DESCRIPTION.ordinal()))
                .setSeason(cursor.isNull(SEASON.ordinal())
                        ? null : cursor.getInt(SEASON.ordinal()))
                .setEpisode(cursor.isNull(EPISODE.ordinal())
                        ? null : cursor.getInt(EPISODE.ordinal()))
                .setEpisodeTitle(cursor.getString(EPISODE_TITLE.ordinal()))
                .setSeriesProgramID(cursor.getString(SERIES_ID.ordinal()))
                .setUnskipableAssetID(cursor.getString(UNSKIPABLE_ASSET_ID.ordinal()))
                .setActors(Arrays.asList(TextUtils.split(cursor.getString(ACTORS.ordinal()), ",")))
                .setDirectors(Arrays.asList(TextUtils.split(cursor.getString(DIRECTORS.ordinal()), ",")))
                .setParentalRating(ParentalRating.valueOf(ParentalRating.class, cursor.getString(PARENTAL_RATING.ordinal())))
                .setReleaseYear(cursor.getString(RELEASE_YEAR.ordinal()))
                .setGenres(Arrays.asList(TextUtils.split(cursor.getString(GENRES.ordinal()), ",")))
                .setSubGenres(Arrays.asList(TextUtils.split(cursor.getString(SUB_GENRES.ordinal()), ",")))
                .setIsBlackListed(cursor.getInt(IS_BLACKLISTED.ordinal()) > 0)
                .setImageSources(ImageProjectionHelper.imagesFromCursorRow(
                        cursor, IMAGE_URL.ordinal(), IMAGE_TYPE.ordinal()))
                .setNPVREnabled(cursor.getInt(IS_NPVR_ENABLED.ordinal()) > 0)
                .setNextToLive(isPreviousLive)
                .setHasDescriptiveAudio(cursor.getInt(HAS_DESCRIPTIVE_AUDIO.ordinal()) > 0)
                .setHasDescriptiveSubtitle(cursor.getInt(HAS_DESCRIPTIVE_SUBTITLE.ordinal()) > 0)
                .build();
    }

    /**
     * Convert the give broadcast list to database content values.
     * @param broadcasts The broadcast list which needs to be converted.
     * @return The content values from the broadcast list.
     */
    public static ContentValues[] toContentValues(List<? extends IBroadcast> broadcasts) {
        long now = System.currentTimeMillis();
        ContentValues[] contentValues = new ContentValues[broadcasts.size()];

        for (int i = 0, tvBroadcastsSize = broadcasts.size(); i < tvBroadcastsSize; i++) {
            IBroadcast broadcast = broadcasts.get(i);
            ContentValues values = new ContentValues();
            try {
                values.put(TvContract.Broadcast.EVENT_ID, broadcast.getId());
                values.put(TvContract.Broadcast.CHANNEL_ID, broadcast.getChannelId());
                values.put(TvContract.Broadcast.START, broadcast.getStart());
                values.put(TvContract.Broadcast.END, broadcast.getEnd());
                values.put(TvContract.Broadcast.TITLE, broadcast.getTitle());
                values.put(TvContract.Broadcast.DESCRIPTION, broadcast.getDescription());
                values.put(TvContract.Broadcast.GENRES, TextUtils.join(",", broadcast.getGenres()));
                values.put(TvContract.Broadcast.SUB_GENRES, TextUtils.join(",", broadcast.getSubGenres()));
                values.put(TvContract.Broadcast.RELEASE_YEAR, broadcast.getReleaseYear());
                values.put(TvContract.Broadcast.SEASON, broadcast.getSeasonNumber());
                values.put(TvContract.Broadcast.EPISODE, broadcast.getEpisodeNumber());
                values.put(TvContract.Broadcast.EPISODE_TITLE, broadcast.getEpisodeTitle());
                values.put(TvContract.Broadcast.PROGRAM_ID, broadcast.getProgramId());
                values.put(TvContract.Broadcast.SERIES_PROGRAM_ID, broadcast.getSeriesId());
                values.put(TvContract.Broadcast.UNSKIPABLE_ASSET_ID, broadcast.getAssetIdentifier());
                values.put(TvContract.Broadcast.ACTORS, TextUtils.join(",", broadcast.getActors()));
                values.put(TvContract.Broadcast.DIRECTORS, TextUtils.join(",", broadcast.getDirectors()));
                values.put(TvContract.Broadcast.PARENTAL_RATING, broadcast.getParentalRating().name());
                values.put(TvContract.Broadcast.LAST_UPDATED, now);
                values.put(TvContract.Broadcast.IS_BLACKLISTED, broadcast.isBlackListed());
                values.put(TvContract.Broadcast.HAS_DESCRIPTIVE_AUDIO, broadcast.hasDescriptiveAudio());
                values.put(TvContract.Broadcast.HAS_DESCRIPTIVE_SUBTITLE, broadcast.hasDescriptiveSubtitle());
                values.put(TvContract.Broadcast.IS_NPVR_ENABLE, broadcast.isNPVREnabled());

                ImageProjectionHelper.intoContentValues(broadcast.getImageSources(), values,
                        TvContract.Broadcast.IMAGE_URL, TvContract.Broadcast.IMAGE_TYPE);

                contentValues[i] = values;
            } catch (NullPointerException e) {
                Log.e(TAG, "Failed to insert in DB Broadcast event with id: " + broadcast.getId(), e);
            }
        }

        Log.d(TAG, "broadcast cv size = " + contentValues.length);
        return contentValues;
    }
}
