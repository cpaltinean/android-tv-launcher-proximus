package tv.threess.threeready.data.generic;

import androidx.work.Data;
import tv.threess.threeready.data.utils.PeriodicCacheUpdater;

/**
 * Class representing the intent of a job to be performed,
 * containing also the extra data needed for the processing.
 */
public class JobIntent {

    private final Class<? extends BaseWorker> mWorker;

    private final Data mExtras;

    private final PeriodicCacheUpdater mRepeatPeriod;

    public JobIntent(Class<? extends BaseWorker> worker, Data extras) {
        mWorker = worker;
        mExtras = extras;
        mRepeatPeriod = null;
    }

    public JobIntent(Class<? extends BaseWorker> worker, PeriodicCacheUpdater repeatPeriod, Data extras) {
        mWorker = worker;
        mExtras = extras;
        mRepeatPeriod = repeatPeriod;
    }

    /**
     * @return the name of the worker, this should be unique for each worker.
     */
    public String getWorkName() {
        if (mWorker == null) {
            return null;
        }
        return mWorker.getName();
    }

    /**
     * @return a new intent of the job that is capable of receiving when
     * the job if finished, containing also the status (success or fail)
     */
    public JobIntent withReceiver(String intentAction) {
        return new JobIntent(mWorker, new Data.Builder()
                .putAll(mExtras)
                .putString(BaseWorkManager.KEY_INTENT_EXTRA_RECEIVER, intentAction)
                .build()
        );
    }

    Class<? extends BaseWorker> getWorker() {
        return mWorker;
    }

    Data getExtras() {
        return mExtras;
    }

    long getSchedulePeriod() {
        if (mRepeatPeriod == null) {
            return 0;
        }
        return mRepeatPeriod.getSchedulePeriod();
    }
}
