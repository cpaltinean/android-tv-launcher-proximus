/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.config.model;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import tv.threess.threeready.api.account.model.CacheSettings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.config.model.PlaybackSettings;
import tv.threess.threeready.api.config.model.PlayerConfig;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.model.ContinueWatchingConfig;
import tv.threess.threeready.api.home.model.generic.ChannelApp;
import tv.threess.threeready.api.home.model.generic.ContentMarkers;
import tv.threess.threeready.api.home.model.generic.LocaleSettings;
import tv.threess.threeready.api.home.model.generic.NotificationSettings;
import tv.threess.threeready.api.home.model.generic.OfflineModeSettings;
import tv.threess.threeready.api.home.model.generic.PlayApps;
import tv.threess.threeready.api.home.model.generic.StyleSettings;
import tv.threess.threeready.data.account.adapter.ChannelAppsMapAdapter3Ready;
import tv.threess.threeready.data.account.adapter.HashMapLanguageAdapter3Ready;
import tv.threess.threeready.data.home.model.generic.ContentMarkers3Ready;
import tv.threess.threeready.data.home.model.generic.ContinueWatchingConfig3Ready;
import tv.threess.threeready.data.home.model.generic.LocaleSettings3Ready;
import tv.threess.threeready.data.home.model.generic.NotificationSettings3Ready;
import tv.threess.threeready.data.home.model.generic.OfflineModeSettings3Ready;
import tv.threess.threeready.data.home.model.generic.PlayApps3Ready;
import tv.threess.threeready.data.home.model.generic.StyleSettings3Ready;

/**
 * POJO class which stores the application related configurations.
 *
 * Created by Barabas Attila on 4/20/2017.
 */
public class AppConfig3Ready implements Serializable, AppConfig {
    private static final int DEFAULT_PAGE_INIT_MODULE_COUNT = 3;
    private static final int DEFAULT_PAGE_PREFETCH_MODULE_COUNT  = 3;
    private static final int DEFAULT_CHANNEL_SCAN_TIMEOUT_SECONDS = 30;

    @SerializedName("player_config")
    private PlayerConfig3Ready mPlayerConfig;

    @SerializedName("style_settings")
    private StyleSettings3Ready mStyleSettings;

    @SerializedName("locale_settings")
    private LocaleSettings3Ready mLocaleSettings;

    @SerializedName("play_apps")
    private PlayApps3Ready mPlayApps;

    @SerializedName("search_threshold")
    private int mSearchThreshold;

    @SerializedName("forced_reboot_delay_seconds")
    private int mForcedRebootDelaySeconds;

    @SerializedName("channel_apps")
    @JsonAdapter(ChannelAppsMapAdapter3Ready.class)
    private Map<String, ChannelApp> mChannelApps;

    @SerializedName("replay_plus_link")
    private String mReplayPlusLink;

    @SerializedName("regional_channel_link")
    private String mRegionalChannelLink;

    @SerializedName("channel_list_renumbering_link")
    private String mChannelListRenumberingLink;

    @SerializedName("content_markers")
    private ContentMarkers3Ready mContentMarkers;

    @SerializedName("channel_scan_retries")
    private int mChannelScanRetries;

    @SerializedName("home_timeout_seconds")
    private int mHomeTimeoutSeconds;

    @SerializedName("guide_timeout_seconds")
    private int mGuideTimeoutSeconds;

    @SerializedName("channel_scan_timeout_seconds")
    private Long mChannelScanTimeoutSeconds;

    @SerializedName("page_init_module_count")
    private int mPageInitModuleCount;

    @SerializedName("page_prefetch_module_count")
    private int mPagePrefetchModuleCount;

    @SerializedName("player_ui_timeout_seconds")
    private int mPlayerUiTimeoutSeconds;

    @SerializedName("zapper_timeout_seconds")
    private int mZapperTimeoutSeconds;

    @SerializedName("guide_page_size_hours")
    private int mProgramGuidePageSizeHours;

    @SerializedName("query_timeout_seconds")
    private int mQueryTimeout;

    @SerializedName("startup_timeout_seconds")
    private int mStartupTimeoutSeconds;

    @SerializedName("module_data_source_timeout")
    private int mModuleDataSourceTimeoutSeconds;

    @SerializedName("standby_update_apply_delay_seconds")
    private int mStandbySystemUpdateApplyDelaySeconds;

    @SerializedName("system_update_timeout_minutes")
    private int mSystemUpdateTimeoutMinutes;

    @SerializedName("rus_window_timeout_minutes")
    private int mRusWindowTimeoutMinutes;

    @SerializedName("quiescent_startup_enabled")
    private boolean mQuiescentStartupEnabled;

    @SerializedName("remote_names")
    private List<String> mRemoteNames;

    @SerializedName("baby_channel_warning_delay_seconds")
    private int mBabyChannelWarningDelay;

    @SerializedName("max_number_of_channels")
    private int maxNumberOfChannels;

    @SerializedName("internet_check_timeout_seconds")
    private int mInternetCheckTimeoutSeconds;

    @SerializedName("hdmi_check_timeout_seconds")
    private int mHDMICheckTimeoutSeconds;

    @SerializedName("hints_minimum_display_time_seconds")
    private int mHintsDisplayTimeSeconds;

    @SerializedName("parental_control_temporal_unblock_seconds")
    private int mParentalControlTemporalUnblockSeconds;

    @SerializedName("active_standby_timeout_minutes")
    private int mActiveStandbyTimeoutMinutes;

    @SerializedName("audio_subtitle_language_map")
    @JsonAdapter(HashMapLanguageAdapter3Ready.class)
    private Map<String, String> mAudioSubtitleLanguageMap;

    @SerializedName("playback_settings")
    private PlaybackSettings3Ready mPlaybackSettings;

    @SerializedName("cache_settings")
    private CacheSettings3Ready mCacheSettings;

    @SerializedName("notification_settings")
    private NotificationSettings3Ready mNotificationSettings;

    @SerializedName("offline_mode_settings")
    private OfflineModeSettings3Ready mOfflineModeSettings;

    @SerializedName("initial_app_order")
    private List<String> mInitialAppOrder;

    @SerializedName("continue_watching")
    private ContinueWatchingConfig3Ready mContinueWatchingConfig;

    @SerializedName("netflix_partner_id")
    protected String mNetflixPartnerId;

    @Override
    public PlayerConfig getPlayerConfig() {
        return mPlayerConfig;
    }

    @Override
    public StyleSettings getStyleSettings() {
        if (mStyleSettings == null) {
            // Create default style settings.
            mStyleSettings = new StyleSettings3Ready();
        }

        return mStyleSettings;
    }

    @Override
    public PlayApps getPlayApps() {
        return mPlayApps;
    }

    /**
     * @return Continue watching related client config options.
     */
    @Override
    public ContinueWatchingConfig getContinueWatchingConfig() {
        return mContinueWatchingConfig;
    }

    @Override
    public LocaleSettings getLocaleSettings() {
        return mLocaleSettings;
    }


    @Override
    public int getSearchThreshold() {
        return mSearchThreshold;
    }

    @Override
    public Map<String, ChannelApp> getChannelApps() {
        return mChannelApps != null ? mChannelApps : Collections.emptyMap();
    }

    @Override
    public String getReplayPlusLink() {
        return mReplayPlusLink;
    }

    @Override
    public String getRegionalChannelLink() {
        return mRegionalChannelLink;
    }

    @Override
    public ContentMarkers getContentMarkers() {
        return mContentMarkers;
    }

    @Override
    public long getHomeScreenHideTimeout() {
        return TimeUnit.SECONDS.toMillis(mHomeTimeoutSeconds);
    }

    @Override
    public long getGuideHideTimeout() {
        return TimeUnit.SECONDS.toMillis(mGuideTimeoutSeconds);
    }

    @Override
    public long getPlayerUiHideTimeout() {
        return TimeUnit.SECONDS.toMillis(mPlayerUiTimeoutSeconds);
    }

    @Override
    public long getZapperUiHideTimeout() {
        return TimeUnit.SECONDS.toMillis(mZapperTimeoutSeconds);
    }

    @Override
    public long getProgramGuidePageSize(TimeUnit unit) {
        return unit.convert(mProgramGuidePageSizeHours, TimeUnit.HOURS);
    }

    @Override
    public int getQueryTimeout() {
        return mQueryTimeout;
    }

    @Override
    public long getStartupTimeout(TimeUnit unit) {
        return unit.convert(mStartupTimeoutSeconds, TimeUnit.SECONDS);
    }

    @Override
    public long getModuleDataSourceTimeout(TimeUnit unit) {
        return unit.convert(mModuleDataSourceTimeoutSeconds, TimeUnit.SECONDS);
    }

    @Override
    public Collection<String> getRemoteNames() {
        return ArrayUtils.notNull(mRemoteNames);
    }

    @Override
    public long getBabyChannelWarningDelay(TimeUnit unit) {
        return unit.convert(mBabyChannelWarningDelay, TimeUnit.SECONDS);
    }

    @Override
    public int getMaxChannelNumber() {
        return maxNumberOfChannels;
    }

    @Override
    public long getInternetCheckTimeout(TimeUnit unit) {
        return unit.convert(mInternetCheckTimeoutSeconds, TimeUnit.SECONDS);
    }

    @Override
    public long getHDMICheckTimeout(TimeUnit unit) {
        return unit.convert(mHDMICheckTimeoutSeconds, TimeUnit.SECONDS);
    }

    @Override
    public long getHintsDisplayTime(TimeUnit unit) {
        return unit.convert(mHintsDisplayTimeSeconds, TimeUnit.SECONDS);
    }

    /**
     * @param unit The time unit in which the result should be returned.
     * @return The time for which the parental control is temporally unblocked.
     */
    @Override
    public long getParentalControlTemporalUnblockSeconds(TimeUnit unit) {
        return unit.convert(mParentalControlTemporalUnblockSeconds, TimeUnit.SECONDS);
    }

    /**
     * @param unit The expected unit of the returned time.
     * @return Time to wait in active standby before the box goes into network standby.
     */
    @Override
    public long getActiveStandbyTimeout(TimeUnit unit) {
        return unit.convert(mActiveStandbyTimeoutMinutes, TimeUnit.MINUTES);
    }

    /**
     * Gets a map with language(PoEditor key) as key and possibles name received  for a language as value.
     */
    @Override
    public Map<String, String> getAudioSubtitleLanguageMap() {
        return mAudioSubtitleLanguageMap;
    }

    @Override
    public PlaybackSettings getPlaybackSettings() {
        return mPlaybackSettings;
    }

    @Override
    public String getChannelRenumberingLink() {
        return mChannelListRenumberingLink;
    }

    @Override
    public CacheSettings getCacheSettings() {
        return mCacheSettings;
    }

    @Override
    public NotificationSettings getNotificationSettings() {
        return mNotificationSettings;
    }

    @Override
    public OfflineModeSettings getOfflineModeSettings() {
        return mOfflineModeSettings;
    }

    @Override
    public List<String> getInitialAppOrder() {
        return mInitialAppOrder;
    }

    @Override
    public int getChannelScanRetries() {
        return mChannelScanRetries;
    }

    /**
     * @param unit The time unit to get the delay in.
     * @return The time to wait in after standby before applying the update.
     */
    @Override
    public long getStandbySystemUpdateApplyDelay(TimeUnit unit) {
        return unit.convert(mStandbySystemUpdateApplyDelaySeconds, TimeUnit.SECONDS);
    }

    /**
     * @param unit The time unit to get the timeout in.
     * @return The maximum time to wait for system update image download in the rus window.
     */
    @Override
    public long getSystemUpdateTimeout(TimeUnit unit) {
        return unit.convert(mSystemUpdateTimeoutMinutes, TimeUnit.MINUTES);
    }

    @Override
    public long getRusWindowTimeout(TimeUnit unit) {
        return unit.convert(mRusWindowTimeoutMinutes, TimeUnit.MINUTES);
    }

    @Override
    public boolean isQuiescentStartupEnabled() {
        return mQuiescentStartupEnabled;
    }

    @Override
    public long getChannelScanTimeOut(TimeUnit unit) {
        if (mChannelScanTimeoutSeconds == null) {
            return unit.convert(DEFAULT_CHANNEL_SCAN_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        }

        return unit.convert(mChannelScanTimeoutSeconds, TimeUnit.SECONDS);
    }

    /**
     * @return The number of non empty module which needs to be loaded when a modular page opens.
     */
    @Override
    public int getPageInitModuleCount() {
        if (mPageInitModuleCount == 0) {
            return DEFAULT_PAGE_INIT_MODULE_COUNT;
        }

        return mPageInitModuleCount;
    }

    /**
     * @return The number of non empty module which needs to be pre-loaded after the last visible module.
     */
    @Override
    public int getPagePrefetchModuleCount() {
        if (mPagePrefetchModuleCount == 0) {
            return DEFAULT_PAGE_PREFETCH_MODULE_COUNT;
        }

        return mPagePrefetchModuleCount;
    }

    @Override
    public long getForcedRebootDelay(TimeUnit unit) {
        return unit.convert(mForcedRebootDelaySeconds, TimeUnit.SECONDS);
    }

    @Override
    public String getNetflixPartnerId() {
        return mNetflixPartnerId;
    }
}
