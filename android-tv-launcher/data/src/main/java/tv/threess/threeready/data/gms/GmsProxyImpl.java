/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.gms;

import android.app.Application;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.generic.helper.PackageUtils;
import tv.threess.threeready.api.gms.model.EditorialAppInfo;
import tv.threess.threeready.api.gms.model.InstalledAppInfo;
import tv.threess.threeready.api.home.model.generic.PlayApps;
import tv.threess.threeready.api.home.model.module.ModuleDataSourceParams;
import tv.threess.threeready.api.home.model.module.ModuleFilterOption;
import tv.threess.threeready.api.home.model.module.ModuleSortOption;
import tv.threess.threeready.api.log.Log;

/**
 * Com-hem specific Gms Proxy implementation.
 *
 * @author Barabas Attila
 * @since 2017.05.17
 */
public class GmsProxyImpl implements GmsProxy {
    private static final String TAG = Log.tag(GmsProxyImpl.class);

    private final GmsCache mGmsCache = Components.get(GmsCache.class);

    private final Application mContext;

    public GmsProxyImpl(Application application) {
        mContext = application;
    }

    public Application getApplication() {
        return mContext;
    }

    /**
     * Return a list of installed application.
     * Filtered and sorted by the given data source parameters.
     */
    @Override
    public List<InstalledAppInfo> getApps(final ModuleDataSourceParams dataSourceParams) {
        final PackageManager pm = mContext.getPackageManager();

        PlayApps playApps = Components.get(AppConfig.class).getPlayApps();
        List<String> featuredApps = ArrayUtils.notNull(playApps.getFeatured()).stream()
                .map(EditorialAppInfo::getPackageName).collect(Collectors.toList());

        List<String> storeApps = ArrayUtils.notNull(playApps.getStore()).stream()
                .map(EditorialAppInfo::getPackageName).collect(Collectors.toList());

        // Query start intents for installed apps and games.
        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LEANBACK_LAUNCHER);
        final List<ResolveInfo> resolveInfoList = pm.queryIntentActivities(mainIntent, 0);
        if (resolveInfoList == null || resolveInfoList.isEmpty()) {
            return Collections.emptyList();
        }

        List<InstalledAppInfo> appList = new ArrayList<>();

        // Query apps last opened time.
        Map<String, Long> lastOpenedTimeMap = mGmsCache.getAppsLastOpenedTimeMap();

        for (ResolveInfo resolveInfo : resolveInfoList) {
            final String packageName = resolveInfo.activityInfo.packageName;

            Log.d(TAG, "Package name : " + packageName);

            // Exclude own package
            if (mContext.getApplicationInfo().packageName.equals(packageName)) {
                continue;
            }

            // Get launch intent
            Intent intent = pm.getLeanbackLaunchIntentForPackage(resolveInfo.activityInfo.packageName);
            if (intent == null) {
                Log.i(TAG, "No launcher Activity for package [" + resolveInfo.activityInfo.packageName + "]. We ignore it.");
                continue;
            }

            String appName = PackageUtils.getAppNameByPackageName(mContext, packageName);

            long lastOpenedTime = lastOpenedTimeMap.containsKey(packageName)
                    ? lastOpenedTimeMap.get(packageName) : 0;

            InstalledAppInfo appInfo = new InstalledAppInfo(packageName, appName, lastOpenedTime, featuredApps.indexOf(packageName),
                    storeApps.indexOf(packageName), resolveInfo.activityInfo.name);

            // Apply data source filtering.
            if (applyFilters(appInfo, dataSourceParams)) {
                appList.add(appInfo);
            }
        }

        // Sort list by given sort options.
        Comparator<InstalledAppInfo> sorter = buildAppSorter(dataSourceParams);
        if (sorter != null) {
            appList.sort(sorter);
        }

        return appList;
    }

    @Nullable
    private Comparator<InstalledAppInfo> buildAppSorter(ModuleDataSourceParams params) {
        String[] sort;
        if (params == null) {
            sort = DEFAULT_SORT_ORDER;
        } else {
            sort = params.getSort();
            if (sort == null || sort.length == 0) {
                sort = DEFAULT_SORT_ORDER;
            }
        }

        List<AppIndexer> indexers = new ArrayList<>();
        for (String sortOption : sort) {
            if (ModuleSortOption.App.FEATURED.equalsIgnoreCase(sortOption)) {
                indexers.add(new FeaturedAppIndexer());
            } else if (ModuleSortOption.App.STORE.equalsIgnoreCase(sortOption)) {
                indexers.add(new StoreAppIndexer());
            } else if (ModuleSortOption.App.RECENCY.equalsIgnoreCase(sortOption)) {
                indexers.add(new RecencyAppIndexer());
            } else if (sortOption != null && sortOption.startsWith(ModuleSortOption.App.PACKAGE)) {
                final List<String> packages = params.getSortList(ModuleSortOption.App.PACKAGE);
                if (packages != null) {
                    indexers.add(new PackageAppIndexer(packages));
                }
            } else {
                Log.w(TAG, "Ignoring sort option [" + sortOption + "]");
            }
        }

        return new IndexedAppComparator(indexers);
    }

    /**
     * Resolves an index for an app to be used when sorting. Smaller values come first.
     */
    private interface AppIndexer {

        /**
         * @return A relevant index for the given app or Long.MAX_VALUE if the app doesn't have an index in this indexer.
         */
        long getIndex(InstalledAppInfo appInfo);
    }

    /**
     * App indexer that sorts in the order specified in configured featured app package names.
     */
    private static class FeaturedAppIndexer implements AppIndexer {

        public FeaturedAppIndexer() {
        }

        @Override
        public long getIndex(InstalledAppInfo appInfo) {
            // Smaller first, same order as in config, those not configured at the end
            return appInfo.getFeaturedIndex() < 0 ? Long.MAX_VALUE : appInfo.getFeaturedIndex();
        }
    }

    /**
     * App indexer that sorts in the order specified in configured store app package names.
     */
    private static class StoreAppIndexer implements AppIndexer {

        public StoreAppIndexer() {
        }

        @Override
        public long getIndex(InstalledAppInfo appInfo) {
            // Smaller first, same order as in config, those not configured at the end
            return appInfo.getStoreIndex() < 0 ? Long.MAX_VALUE : appInfo.getStoreIndex();
        }
    }

    /**
     * App indexer that sorts in the order specified in configured app package names.
     */
    private static class PackageAppIndexer implements AppIndexer {

        private final List<String> packages;

        public PackageAppIndexer(List<String> packages) {
            this.packages = packages;
        }

        @Override
        public long getIndex(InstalledAppInfo appInfo) {
            // Smaller first, same order as in config, those not configured at the end
            int idx = packages.indexOf(appInfo.getPackageName());
            return idx < 0 ? Long.MAX_VALUE : idx;
        }
    }

    /**
     * App indexer that sorts recently opened apps first.
     */
    private static class RecencyAppIndexer implements AppIndexer {
        private final List<String> mInitAppOrder;

        public RecencyAppIndexer() {
            mInitAppOrder = Components.get(AppConfig.class).getInitialAppOrder();
        }

        @Override
        public long getIndex(InstalledAppInfo appInfo) {
            // Bigger first, so we need to inverse
            return Long.MAX_VALUE - getLastOpenedTime(appInfo, mInitAppOrder);
        }
    }

    /**
     * App Comparator that applies several indexers in sequence. The first Indexer having at least one relevant index
     * is applied.
     */
    private static class IndexedAppComparator implements Comparator<InstalledAppInfo> {

        @NonNull
        private final List<AppIndexer> indexers;

        public IndexedAppComparator(@NonNull List<AppIndexer> indexers) {
            this.indexers = indexers;
        }

        @Override
        public int compare(InstalledAppInfo o1, InstalledAppInfo o2) {
            long idx1, idx2;
            for (AppIndexer i : indexers) {
                idx1 = i.getIndex(o1);
                idx2 = i.getIndex(o2);
                // If at least one of the objects has an index, apply the sorting
                // If none of the object have an index, move to the next indexer
                if (idx1 < Long.MAX_VALUE || idx2 < Long.MAX_VALUE) {
                    return Long.compare(idx1, idx2);
                }
            }
            return 0;
        }
    }

    /**
     * Returns true if the app has to be included.
     */
    private boolean applyFilters(InstalledAppInfo appInfo, ModuleDataSourceParams dataSourceParams) {
        if (dataSourceParams == null) {
            // No filter. Show all apps.
            return true;
        }

        //  Apply filter type filter on apps.
        List<String> typeFilters = dataSourceParams.getFilterList(ModuleFilterOption.App.Type.NAME);
        for (String filter : ArrayUtils.notNull(typeFilters)) {
            switch (filter) {
                case ModuleFilterOption.App.Type.Value.FEATURED:
                    if (appInfo.isFeatured()) {
                        return true;
                    }
                    break;

                case ModuleFilterOption.App.Type.Value.INSTALLED:
                    if (!appInfo.isFeatured()) {
                        return true;
                    }
                    break;

                case ModuleFilterOption.App.Type.Value.STORE:
                    if (appInfo.isStore()) {
                        return true;
                    }
                    break;
            }
        }

        // Apply package based filtering.
        List<String> packageFilters = dataSourceParams.getFilterList(ModuleFilterOption.App.Package.NAME);
        if (packageFilters != null && !packageFilters.isEmpty()) {
            for (String filter : packageFilters) {
                if (filter != null && filter.equals(appInfo.getPackageName())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Get the app last opened time
     *
     * @param app             The app that will be checked
     * @param initialAppOrder The list of package names of apps from {@link AppConfig#getInitialAppOrder()}
     * @return if {@link InstalledAppInfo#getLastOpened == 0}  and {@link AppConfig#getInitialAppOrder()} not contains the app return 0
     * or if contains then return the {@see AppConfig#getInitialAppOrder().size()} - {@see AppConfig#getInitialAppOrder().indexOf(AppInfo)}.
     * if {@link InstalledAppInfo#getLastOpened() != 0} return the last opened time in long
     * if {@link InstalledAppInfo#isStore()} return {@value Long#MAX_VALUE}
     */
    private static long getLastOpenedTime(InstalledAppInfo app, List<String> initialAppOrder) {
        if (app.isStore()) {
            return Long.MAX_VALUE;
        }

        if (app.getLastOpened() == 0) {
            int appIndex = initialAppOrder.indexOf(app.getPackageName());
            if (appIndex == -1) {
                return 0L;
            }
            return initialAppOrder.size() - appIndex;
        }
        return app.getLastOpened();
    }


    private static final String[] DEFAULT_SORT_ORDER =
            new String[]{ModuleSortOption.App.STORE, ModuleSortOption.App.RECENCY};
}
