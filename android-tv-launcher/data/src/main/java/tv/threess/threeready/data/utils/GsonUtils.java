package tv.threess.threeready.data.utils;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import tv.threess.threeready.data.home.model.module.EditorialItemAction3Ready;

/**
 * Utility method for gson parser.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public class GsonUtils {
    private static final ThreadLocal<Gson> GSON_THREAD_LOCAL = ThreadLocal.withInitial(() ->
            new GsonBuilder()
                    .registerTypeAdapterFactory(EditorialItemAction3Ready.createActionItemTypeAdapterFactory())
                    .setLenient()
                    .create());

    public static Type TYPE_MAP_STRING_2_STRING = new TypeToken<Map<String, String>>() {}.getType();
    public static Type TYPE_LIST_STRING = new TypeToken<List<String>>() {}.getType();

    public static Gson getGson() {
        return GSON_THREAD_LOCAL.get();
    }

    public static <T> T readFromFile(Context context, String fileName, Class<T> type) throws IOException {
        try (InputStream stream = context.openFileInput(fileName)) {
            return getGson().fromJson(new InputStreamReader(stream), type);
        }
    }

    /**
     * @param context we need it in order to get the assets content
     * @param path    the file path of the file in the assets folder
     * @param type    the return type that we need, will be used by Gson
     * @return the contents of the file, decoded into the wanted type.
     * @throws Exception in case of IO or parsing issues
     */
    public static <T> T readJsonFromAssets(Context context, String path, Class<T> type) throws Exception {
        try (InputStream stream = context.getAssets().open(path)) {
            return getGson().fromJson(new InputStreamReader(stream), type);
        }
    }

    /**
     * @param type the return type that we need, will be used by Gson
     * @return the contents of the file, decoded into the wanted type.
     * @throws Exception in case of IO or parsing issues
     */
    public static <T> T readJsonFromString(String jsonString, Class<T> type) throws Exception {
        return getGson().fromJson(jsonString, type);
    }
}
