package tv.threess.threeready.data.tv.model;

import tv.threess.threeready.api.tv.ChannelType;
import tv.threess.threeready.api.tv.model.TvChannel;

import java.util.concurrent.TimeUnit;

/**
 * Channel entity created from database.
 *
 * @author Barabas Attila
 * @since 2017.12.20
 */

public class ChannelDBModel extends TvChannel {

    private String mChannelId;
    private String mInternalId;
    private String mCallLetter;
    private int mNumber;
    private String mName;
    private String mLanguage;
    private String mChannelLogoUrl;
    private ChannelType mType;
    private boolean mIsUserTvSubscribedChannel;
    private boolean mIsBabyContentWarningRequired;
    private boolean mIsReplayEnabled;
    private boolean mIsNPVREnabled;
    private boolean mIsHDEnabled;
    private boolean mLiveTadEnabled;
    private boolean mTimeShiftTadEnabled;
    private long mTargetAdInactivity;
    private long mReplayWindow;

    @Override
    public String getId() {
        return mChannelId;
    }

    @Override
    public String getInternalId() {
        return mInternalId;
    }

    @Override
    public String getCallLetter() {
        return mCallLetter;
    }

    @Override
    public int getNumber() {
        return mNumber;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public String getLanguage() {
        return mLanguage;
    }

    @Override
    public String getChannelLogoUrl() {
        return mChannelLogoUrl;
    }

    @Override
    public ChannelType getType() {
        return mType;
    }

    @Override
    public boolean isUserTvSubscribedChannel() {
        return mIsUserTvSubscribedChannel;
    }

    /**
     * Gets whether a warning is required regarding baby content.
     */
    @Override
    public boolean isBabyContentWarningRequired() {
        return mIsBabyContentWarningRequired;
    }

    @Override
    public boolean isReplayEnabled() {
        return mIsReplayEnabled;
    }

    @Override
    public boolean isHDEnabled() {
        return mIsHDEnabled;
    }

    @Override
    public boolean isLiveTadEnabled() {
        return mLiveTadEnabled;
    }

    @Override
    public boolean isTimeShiftTadEnabled() {
        return mTimeShiftTadEnabled;
    }

    @Override
    public long getTargetAdInactivity(TimeUnit unit) {
        return unit.convert(mTargetAdInactivity, TimeUnit.MINUTES);
    }

    /**
     * @return true if the channel is recording capable.
     */
    @Override
    public boolean isNPVREnabled() {
        return mIsNPVREnabled;
    }

    @Override
    public long getReplayWindow() {
        return mReplayWindow;
    }


    public static class Builder {

        ChannelDBModel mChannel;

        public Builder() {
            mChannel = new ChannelDBModel();
        }

        public Builder setId(String id) {
            mChannel.mChannelId = id;
            return this;
        }

        public Builder setInternalId(String value) {
            mChannel.mInternalId = value;
            return this;
        }

        public Builder setCallLetter(String callLetter) {
            mChannel.mCallLetter = callLetter;
            return this;
        }

        public Builder setNumber(int number) {
            mChannel.mNumber = number;
            return this;
        }

        public Builder setName(String name) {
            mChannel.mName = name;
            return this;
        }

        public Builder setLanguage(String language) {
            mChannel.mLanguage = language;
            return this;
        }

        public Builder setImageUrl(String url) {
            mChannel.mChannelLogoUrl = url;
            return this;
        }

        public Builder setType(ChannelType channelType) {
            mChannel.mType = channelType;
            return this;
        }

        public Builder setFavorite(boolean isFavorite) {
            mChannel.mIsFavorite = isFavorite;
            return this;
        }

        public Builder setIsUserTvSubscribedChannel(boolean isUserTvSubscribedChannel) {
            mChannel.mIsUserTvSubscribedChannel = isUserTvSubscribedChannel;
            return this;
        }

        public Builder setIsBabyContentWarningRequired(boolean babyContentWarningRequired) {
            mChannel.mIsBabyContentWarningRequired = babyContentWarningRequired;
            return this;
        }

        public Builder setReplayEnabled(boolean isReplayEnabled) {
            mChannel.mIsReplayEnabled = isReplayEnabled;
            return this;
        }

        public Builder setHdEnabled(boolean enabled) {
            mChannel.mIsHDEnabled = enabled;
            return this;
        }

        public Builder setNPVREnabled(boolean enabled) {
            mChannel.mIsNPVREnabled = enabled;
            return this;
        }

        public Builder setReplayWindow(long replayWindow) {
            mChannel.mReplayWindow = replayWindow;
            return this;
        }

        public Builder setLiveTadEnabled(boolean value) {
            mChannel.mLiveTadEnabled = value;
            return this;
        }

        public Builder setTimeShiftTadEnabled(boolean value) {
            mChannel.mTimeShiftTadEnabled = value;
            return this;
        }

        public Builder setTargetAdInactivity(long value) {
            mChannel.mTargetAdInactivity = value;
            return this;
        }

        public ChannelDBModel build() {
            return mChannel;
        }
    }
}
