/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.home.model.locale;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import tv.threess.threeready.api.home.model.local.FontConfig;
import tv.threess.threeready.api.home.model.local.FontType;
import tv.threess.threeready.data.account.adapter.FontTypeAdapter3Ready;

/**
 * Configure class to a give font type.
 * Contains the exact font family and type,
 * also some optional scale factor parameters for design fine tune.
 *
 * @author Barabas Attila
 * @since 2018.07.10
 */
public class FontConfig3Ready implements FontConfig {

    @JsonAdapter(FontTypeAdapter3Ready.class)
    @SerializedName("type")
    FontType mFontType;

    @SerializedName("name")
    String mName;

    @SerializedName("textSizeFactor")
    float mTextSizeFactor;

    @SerializedName("lineSpacingExtraFactor")
    float mLineSpacingExtraFactor;

    @SerializedName("letterSpacingFactor")
    float mLetterSpacingFactor;

    @Override
    public FontType getFontType() {
        return mFontType;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public float getTextSizeFactor() {
        return mTextSizeFactor == 0 ? 1f : mTextSizeFactor;
    }

    @Override
    public float getLineSpacingExtraFactor() {
        return mLineSpacingExtraFactor == 0 ? 1f : mLineSpacingExtraFactor;
    }

    @Override
    public float getLetterSpacingFactor() {
        return mLetterSpacingFactor == 0 ? 1f : mLetterSpacingFactor;
    }
}
