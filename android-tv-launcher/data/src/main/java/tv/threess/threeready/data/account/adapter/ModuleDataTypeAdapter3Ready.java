package tv.threess.threeready.data.account.adapter;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.home.model.module.ModuleDataType;

/**
 * Json type adapter to read module type adapter as enum.
 *
 * @author Barabas Attila
 * @since 5/15/21
 */
public class ModuleDataTypeAdapter3Ready extends NameTypeMappingTypeAdapter<ModuleDataType> {
    @Override
    protected Map<String, ModuleDataType> createNameTypeMap() {
        return new HashMap<String, ModuleDataType>() {
            {
                put("tv_program", ModuleDataType.TV_PROGRAM);
                put("tv_channel", ModuleDataType.TV_CHANNEL);
                put("tv_now_next", ModuleDataType.TV_NOW_NEXT);
                put("android_tv_apps", ModuleDataType.APPS);
                put("mixed", ModuleDataType.MIXED);
                put("netflix", ModuleDataType.NETFLIX);
                put("content_category", ModuleDataType.CONTENT_CATEGORY);
                put("start_watching", ModuleDataType.START_WATCHING);
                put("editorial", ModuleDataType.EDITORIAL);
                put("android_tv_system_notifications", ModuleDataType.SYSTEM_NOTIFICATIONS);
                put("gallery", ModuleDataType.GALLERY);
            }
        };
    }
}
