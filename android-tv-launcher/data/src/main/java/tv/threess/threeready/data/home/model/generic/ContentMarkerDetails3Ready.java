/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.home.model.generic;

import android.graphics.Color;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import tv.threess.threeready.api.generic.helper.ColorUtils;
import tv.threess.threeready.api.home.model.generic.ContentMarkerDetails;
import tv.threess.threeready.api.home.model.generic.GradientStyle;
import tv.threess.threeready.api.home.model.module.MarkerType;
import tv.threess.threeready.data.account.adapter.ColorTypeAdapter3Ready;

/**
 * Configuration for content markers (color, background, transparency, priority)
 *
 * @author Szende Pal
 * @since 2017.07.26
 */
public class ContentMarkerDetails3Ready implements Serializable, ContentMarkerDetails {

    @SerializedName("color")
    @JsonAdapter(ColorTypeAdapter3Ready.class)
    protected Integer mColor;

    @SerializedName("background")
    @JsonAdapter(ColorTypeAdapter3Ready.class)
    protected Integer mBackground;

    @SerializedName("transparency")
    protected Float mTransparency;

    @SerializedName("priority")
    protected Integer mPriority;

    @SerializedName("gradient")
    protected GradientStyle3Ready mGradient;

    protected MarkerType mMarkerType;

    @Override
    public int getColor() {
        return mColor == null ? Color.WHITE : mColor;
    }

    @Override
    public int getBackground() {
        return ColorUtils.getColor(mBackground, Color.BLACK);
    }

    @Override
    public float getTransparency() {
        return (mTransparency == null) ? 1 : mTransparency;
    }

    @Override
    public int getPriority() {
        return (mPriority == null) ? 0 : mPriority;
    }

    @Override
    public MarkerType getMarkerType() {
        return mMarkerType;
    }

    public void setPriority(int priority) {
        mPriority = priority;
    }

    public void setMarkerType(MarkerType markerType) {
        mMarkerType = markerType;
    }

    @Override
    public boolean isEmpty() {
        return mColor == null || mBackground == null;
    }

    @Override
    public boolean hasGradient() {
        return mGradient != null && mGradient.getToColor() != null && mGradient.getFromColor() != null;
    }

    @Override
    public GradientStyle getGradient() {
        return mGradient;
    }
}
