package tv.threess.threeready.data.gms;

import android.content.ContentResolver;
import android.net.Uri;

import tv.threess.threeready.api.BuildConfig;
import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.notification.model.NotificationItem;

/**
 * Contract class to access GMS data.
 *
 * @author Eugen Guzyk
 * @since 2018.08.29
 */
public class GmsContract {

    public static final String AUTHORITY = BuildConfig.GMS_PROVIDER;

    private static final Uri BASE_URI = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT).authority(AUTHORITY).build();

    /**
     * Contract class for 3rd party application information
     */

    public interface Apps {
        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(Apps.class);

        /**
         * Path used in content uri
         */
        String PATH = TABLE_NAME;

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(GmsContract.BASE_URI, PATH);

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(Apps.class);

        /**
         * Package name
         * <P>Type: String</P>
         */
        String PACKAGE_NAME = "package_name";

        /**
         * Last opened time UTC
         * <P>Type: Number</P>
         */
        String LAST_OPENED = "last_opened";
    }

    public interface Notification {
        /**
         * Notification key as stored in the system (TEXT)
         */
        String COLUMN_SBN_KEY = "sbn_key";

        /**
         * Package name of the app posting the notification (TEXT)
         */
        String COLUMN_PACKAGE_NAME = "package_name";

        /**
         * Title of notification (TEXT)
         */
        String COLUMN_NOTIF_TITLE = "title";

        /**
         * The second line of text (TEXT)
         */
        String COLUMN_NOTIF_TEXT = "text";

        /**
         * Indicates of the notification should be dismissed when clicked by the user.
         * (INTEGER, 1 for true , 0 for false)
         */
        String COLUMN_AUTODISMISS = "is_auto_dismiss";

        /**
         * Indicates if the notification can be dismissed by the user.
         * (INTEGER, 1 for true , 0 for false)
         */
        String COLUMN_DISMISSIBLE = "dismissible";

        /**
         * Indicates if the notification is ongoing (INTEGER, 1 for true , 0 for false).
         */
        String COLUMN_ONGOING = "ongoing";

        /**
         * Small icon representing this notification (BLOB)
         */
        String COLUMN_SMALL_ICON = "small_icon";

        /**
         * Importance level of notification (INTEGER , 1 - 5)
         */
        String COLUMN_IMPORTANCE = "channel";

        /**
         * Progress of an ongoing action the notification represents, for example: when
         * playing content (INTEGER)
         */
        String COLUMN_PROGRESS = "progress";

        /**
         * The max progress value the "progress" value can be (INTEGER)
         */
        String COLUMN_PROGRESS_MAX = "progress_max";

        /**
         * All flags of the notification as defined in:
         * https://developer.android.com/reference/android/app/Notification.html (INTEGER)
         */
        String COLUMN_FLAGS = "flags";

        /**
         * Indicates that the notification has an intent that can be sent when the user
         * clicks on the notification (INTEGER, 1 for true , 0 for false).
         */
        String COLUMN_HAS_CONTENT_INTENT = "has_content_intent";

        /**
         * A bigger picture than can be shown (BLOB)
         */
        String COLUMN_BIG_PICTURE = "big_picture";

        /**
         * Label to be shown on the open (notification handling) button for a notification (TEXT)
         */
        String COLUMN_CONTENT_BUTTON_LABEL  = "content_button_label";

        /**
         * Label to be shown on the dismiss button for a notification (TEXT)
         */
        String COLUMN_DISMISS_BUTTON_LABEL = "dismiss_button_label";

        /**
         * The tag that was supplied to the system when the notification was posted (TEXT)
         */
        String COLUMN_TAG = "tag";
    }


    public interface SystemNotification extends Notification {

        /**
         * Base uri to read data from the notifications content provider.
         */
        Uri BASE_URI = Uri.parse("content://com.android.tv.notifications.NotificationContentProvider");

        /**
         * Uri to read notifications data from the notifications content provider.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(BASE_URI, "notifications");
    }

    public interface StoredNotification extends Notification {

        /**
         * Table name.
         */
        String TABLE_NAME = BaseContract.tableName(GmsContract.StoredNotification.class);

        /**
         * Path used in content uri to identify this table.
         */
        String PATH = TABLE_NAME;

        /**
         * Content type of the data returned from this table.
         */
        String MIME_TYPE = BaseContract.mimeType(NotificationItem.class);

        /**
         * Content uri to read data from the table.
         */
        Uri CONTENT_URI = Uri.withAppendedPath(BASE_URI, PATH);

        //
        // Columns
        //

        /**
         * The unique ID for a row.
         * <P>Type: Number</P>
         */
        String ID = BaseContract._ID;

        /**
         * Status of the notification.
         * <P>Type: Boolean</P>
         */
        String COLUMN_READ = "read";
    }

}
