/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.search;

import java.io.IOException;
import java.util.List;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.generic.model.IContentItem;
import tv.threess.threeready.api.home.ModuleData;
import tv.threess.threeready.api.home.model.module.ModuleConfig;
import tv.threess.threeready.api.search.model.SearchSuggestion;
import tv.threess.threeready.api.search.model.SearchTerm;

/**
 * Proxy (pass-through) interface to access search related backend data.
 *
 * @author Barabas Attila
 * @since 2017.07.12
 */
public interface SearchProxy extends Component {
    /**
     * Get a list of suggested terms to be used to perform a search.
     *
     * @param searchTerm The text to get the suggestions for.
     * @return A list of suggested search terms.
     * @throws IOException In case of errors.
     */
    List<SearchSuggestion> getSuggestions(String searchTerm) throws IOException;

    /**
     * Perform a text based search for a 3ready module.
     *
     * @param moduleConfig The config of the module which contains
     *                     the search term, filter options and sort option of the results.
     * @param start        The position of the first item from where the search results should be returned.
     * @param limit        The maximum number of search result returned.
     * @return A list mixed broadcast and vod search results based on the data source.
     * @throws IOException In case of errors.
     */
    ModuleData<IBaseContentItem> contentSearch(ModuleConfig moduleConfig, int start, int limit) throws IOException;

    /**
     * Perform broadcast search with text recognized from spoken language.
     *
     * @param searchTerm The recognized text for which the search should be triggered.
     * @param start      The position of the first item from where the search results should be returned.
     * @param limit      The maximum number of search result returned.
     * @return A list of broadcast search results for the given term.
     * @throws IOException In case of errors.
     */
    List<IContentItem> broadcastVoiceSearch(SearchTerm searchTerm, int start, int limit) throws IOException;

    /**
     * Perform vod search with text recognized from spoken language.
     *
     * @param searchTerm The recognized text for which the search should be triggered.
     * @param start      The position of the first item from where the search results should be returned.
     * @param limit      The maximum number of search result returned.
     * @return VOD search results for the given term.
     * @throws IOException In case of errors.
     */
    List<IContentItem> vodVoiceSearch(SearchTerm searchTerm, int start, int limit) throws IOException;

    /**
     * Search for content which should be opened for 'play' voice commands
     *
     * @param searchTerm The recognized text for which the search should be triggered.
     * @return The content item which should be opened.
     * @throws IOException In case of errors.
     */
    IContentItem contentVoicePlaySearch(SearchTerm searchTerm) throws IOException;
}
