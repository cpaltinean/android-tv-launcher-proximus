package tv.threess.threeready.data.generic.observable;

import android.content.ContentProviderClient;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.CancellationSignal;

import io.reactivex.ObservableEmitter;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.ui.generic.utils.RxUtils;

/**
 * base for observables, where you are registering for a cursor
 *
 * @author hunormadaras
 * @since 28/02/2019
 */
public abstract class BaseCursorObservable<T> extends BaseContentObservable<T> {
    private final static String TAG = Log.tag(BaseCursorObservable.class);

    private CancellationSignal mCancellationSignal;
    protected Cursor mCursor;

    /**
     * gets result data and manages it in the corresponding way
     *
     * @param emitter emitter
     */
    protected abstract void resolveData(ObservableEmitter<T> emitter);

    /**
     * uri to cursor
     *
     * @return proper uri
     */
    protected abstract Uri getUri();

    /**
     * projection for cursor, according to {@link #getUri()}
     *
     * @return proper projection
     */
    protected abstract String[] getProjection();

    public BaseCursorObservable(Context context) {
        super(context);
    }

    /**
     * @param emitter emitter
     * @throws Exception what kind of exception
     */
    @Override
    public void subscribe(ObservableEmitter<T> emitter) throws Exception {
        super.subscribe(emitter);
        subscribeAction(emitter);
    }

    /**
     * always call it in the {@link #subscribe(ObservableEmitter)} method
     *
     * @param emitter emitter
     */
    private void subscribeAction(ObservableEmitter<T> emitter) {
        registerObservable();
        resolveData(emitter);
    }

    /**
     * firs register the observable to get the cursor and after that pushed the result,
     * otherwise you will get old data
     *
     * @param uri     what we watch
     * @param emitter emitter
     */
    @Override
    protected void onChange(Uri uri, ObservableEmitter<T> emitter) {
        registerObservable();
        resolveData(emitter);
    }

    @Override
    protected void unregisterObserver() {
        super.unregisterObserver();
        FileUtils.closeSafe(mCursor);
        FileUtils.cancelSafe(mCancellationSignal);
    }

    /**
     * registers observable for cursor, makes sure that cursor window is filled and the previous is closed, before register
     */
    private void registerObservable() {
        FileUtils.closeSafe(mCursor);
        FileUtils.cancelSafe(mCancellationSignal);

        try (ContentProviderClient contentProviderClient =
                     mContext.getContentResolver().acquireUnstableContentProviderClient(getUri())) {
            if (contentProviderClient == null) {
                Log.d(TAG, "The content provider client is null -> ignore this registration");
                return;
            }

            mCancellationSignal = new CancellationSignal();
            mCursor = contentProviderClient.query(getUri(),
                    getProjection(), null, null, null, mCancellationSignal);

            if (mCursor != null && !RxUtils.isDisposed(mEmitter)) {
                mCursor.getCount();  // Hack: getCount will force the cursor to fill its window
                Log.d(TAG, "registerObservable -> " + getUri());
                mCursor.registerContentObserver(mContentObserver);
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception exists.", ex);
            FileUtils.closeSafe(mCursor);
        }
    }
}
