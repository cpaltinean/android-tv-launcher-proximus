/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.account;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.helper.BundleBuilder;
import tv.threess.threeready.api.generic.helper.UriMatcher;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.generic.BaseProvider;
import tv.threess.threeready.data.utils.SqlUtils;

/**
 * Content provider to access persisted data.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.27
 */
public class ConfigProvider extends BaseProvider {

    enum UriIDs {
        PublicSettings(ConfigContract.Settings.PUBLIC_VIEW),
        Settings(ConfigContract.Settings.TABLE_NAME),
        Session(ConfigContract.Session.TABLE_NAME),
        Packages(ConfigContract.Packages.TABLE_NAME),
        PosterServers(ConfigContract.PosterServers.TABLE_NAME),
        Bookmarks(ConfigContract.Bookmarks.TABLE_NAME),
        Watchlist(ConfigContract.Watchlist.TABLE_NAME);

        UriIDs(String table) {
            mTable = table;
        }

        final String mTable;
    }

    // prepare the uri matcher
    private static final UriMatcher<UriIDs> matcher = new UriMatcher<>(ConfigContract.AUTHORITY, UriIDs.class)
            .add(UriIDs.PublicSettings, ConfigContract.Settings.CONTENT_URI, ConfigContract.Settings.PUBLIC_VIEW)
            .add(UriIDs.Settings, ConfigContract.Settings.CONTENT_URI)
            .add(UriIDs.Session, ConfigContract.Session.CONTENT_URI)
            .add(UriIDs.Packages, ConfigContract.Packages.CONTENT_URI)
            .add(UriIDs.PosterServers, ConfigContract.PosterServers.CONTENT_URI)
            .add(UriIDs.Bookmarks, ConfigContract.Bookmarks.CONTENT_URI)
            .add(UriIDs.Watchlist, ConfigContract.Watchlist.CONTENT_URI);

    private ConfigDatabase mDatabase;
    private final Bundle mMemoryCacheMap = new Bundle();
    private Bundle mSettingsMap = new Bundle();
    private Bundle mSessionMap = new Bundle();
    private final Bundle mMassStorageMap = new Bundle();

    @Override
    public boolean getNotify(Uri uri) {
        switch (matcher.matchEnum(uri)) {
            case PublicSettings:
            case Settings:
            case Session:
                return false;
            default:
                return super.getNotify(uri);
        }
    }

    @Override
    public Bundle call(@NonNull String method, String arg, Bundle extras) {
        if (arg == null) {
            return null;
        }
        switch (method) {
            case DATABASE_VERSION_CHECK:
                mDatabase.getReadableDatabase();
                return super.call(method, arg, extras);

            case ConfigContract.Settings.CALL_PUT_CACHE_DATA:
                if (extras == null) {
                    throw new IllegalStateException("Extras can't be null!");
                }

                if (extras.containsKey(arg)) {
                    mMemoryCacheMap.putString(arg, extras.getString(arg));
                }
                return extras;

            case ConfigContract.Settings.CALL_GET_CACHE_DATA:
                if (mMemoryCacheMap.containsKey(arg)) {
                    Bundle bundle = new Bundle();
                    bundle.putString(arg, mMemoryCacheMap.getString(arg));
                    return bundle;
                }
                return null;

            case ConfigContract.Settings.CALL_GET_SETTINGS_DATA:
                if (mSettingsMap.containsKey(arg)) {
                    Bundle bundle = new Bundle();
                    bundle.putString(arg, mSettingsMap.getString(arg));
                    return bundle;
                }
                return null;

            case ConfigContract.Session.CALL_GET_SESSION_DATA:
                if (mSessionMap.containsKey(arg)) {
                    Bundle bundle = new Bundle();
                    bundle.putString(arg, mSessionMap.getString(arg));
                    return bundle;
                }
                return null;

            case ConfigContract.Settings.CALL_INC_SETTINGS_DATA:
                if (extras != null) {
                    return incrementSettingsValue(extras);
                }
                return null;

            case MassStorageContract.CALL_GET_DATA:
                if (mMassStorageMap.containsKey(arg)) {
                    return new BundleBuilder()
                            .put(arg, mMassStorageMap.getString(arg))
                            .build();
                }
                return null;

            case MassStorageContract.CALL_PUT_DATA:
                putDataToMassStorage(extras);
                return null;

            case MassStorageContract.CALL_DELETE_DATA:
                if (mMassStorageMap.containsKey(arg)) {
                    deleteFromMassStorage(arg);
                }
                return null;
        }
        return super.call(method, arg, extras);
    }

    private Bundle incrementSettingsValue(Bundle extras) {
        SQLiteDatabase db = mDatabase.getWritableDatabase();
        db.beginTransaction();

        try {
            long now = extras.getLong(ConfigContract.Settings.LAST_UPDATED);
            for (String key : extras.keySet()) {

                if (ConfigContract.Settings.LAST_UPDATED.equals(key)) {
                    continue;
                }

                String[] args = {key};
                try (Cursor cursor = db.query(ConfigContract.Settings.TABLE_NAME,
                        ConfigContract.Settings.PROJECTION_VALUE,
                        ConfigContract.Settings.SELECTION_KEY, args,
                        null, null, null)) {
                    long value = extras.getLong(key, 1L);
                    if (cursor.moveToFirst()) {
                        value += cursor.getLong(0);
                    }

                    ContentValues values = new ContentValues(3);
                    values.put(ConfigContract.Settings.KEY, key);
                    values.put(ConfigContract.Settings.VALUE, value);
                    values.put(ConfigContract.Settings.LAST_UPDATED, now);
                    super.insert(ConfigContract.Settings.CONTENT_URI,
                            db, ConfigContract.Settings.TABLE_NAME,
                            values, SQLiteDatabase.CONFLICT_REPLACE);
                    extras.putLong(key, value);
                    notifyChange(ConfigContract.Settings.CONTENT_URI, key);
                } catch (Exception e) {
                    Log.e(TAG, "Failed to read the settings.", e);
                }
            }
            updateSettings();
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        return extras;
    }

    @Override
    public boolean onCreate() {
        super.onCreate();
        // prepare the database helper
        Context context = this.getContext();
        if (context != null) {
            mDatabase = ConfigDatabase.getInstance(context);
            updateSession();
            updateSettings();
            updateMassStorage();
        }

        return true;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        switch (matcher.matchEnum(uri)) {
            case PublicSettings:
            case Settings:
                return ConfigContract.Settings.MIME_TYPE;
            case Session:
                return ConfigContract.Session.MIME_TYPE;
            case Bookmarks:
                return ConfigContract.Bookmarks.MIME_TYPE;
            case Watchlist:
                return ConfigContract.Watchlist.MIME_TYPE;
        }
        throw new UnsupportedOperationException("Unimplemented call: " + matcher.matchEnum(uri));
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        UriIDs match = matcher.matchEnum(uri);
        SQLiteDatabase db = mDatabase.getReadableDatabase();
        return super.query(uri, db, match.mTable, projection, selection, selectionArgs, sortOrder);
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        UriIDs match = matcher.matchEnum(uri);

        long rowId;

        switch (match) {
            default:
                rowId = super.insert(uri, mDatabase.getWritableDatabase(),
                        match.mTable, values, SQLiteDatabase.CONFLICT_REPLACE);
                break;

            case Settings:
            case Session:
                rowId = super.insert(uri, mDatabase.getWritableDatabase(), match.mTable, values,
                        SQLiteDatabase.CONFLICT_REPLACE, () -> updateBundle(match));

                Uri baseUri = uri.buildUpon().clearQuery().build();
                notifyChange(baseUri, values.getAsString(BaseContract._ID));
                break;
        }

        return rowId < 0 ? null : uri;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        UriIDs match = matcher.matchEnum(uri);

        switch (match) {
            default:
                return super.update(uri, mDatabase.getWritableDatabase(), match.mTable, values, selection, selectionArgs);

            case Session:
            case Settings:
                int result = super.update(uri, mDatabase.getWritableDatabase(), match.mTable, values, selection, selectionArgs,
                        () -> updateBundle(match));
                Uri baseUri = uri.buildUpon().clearQuery().build();
                notifyChange(baseUri, values.getAsString(BaseContract._ID));
                return result;

        }
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        UriIDs match = matcher.matchEnum(uri);

        switch (match) {
            default:
                return super.delete(uri,
                        mDatabase.getWritableDatabase(), match.mTable, selection, selectionArgs);

            case Settings:
            case Session:
                int result = super.delete(uri, mDatabase.getWritableDatabase(), match.mTable, selection, selectionArgs,
                        () -> updateBundle(match));

                Uri baseUri = uri.buildUpon().clearQuery().build();
                notifyChange(baseUri, selectionArgs[0]);
                return result;
        }
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        UriIDs match = matcher.matchEnum(uri);

        switch (match) {
            default:
                String replace = uri.getQueryParameter(BaseContract.BULK_REPLACE);
                return bulkInsertReplacePurge(uri,
                        mDatabase.getWritableDatabase(), match.mTable, replace, values);
            case Session:
            case Settings:
                int result = super.bulkInsertReplace(uri, mDatabase.getWritableDatabase(), match.mTable, values,
                        () -> updateBundle(match));

                //notify the key changes
                Uri baseUri = uri.buildUpon().clearQuery().build();
                for (ContentValues contentValue : values) {
                    notifyChange(baseUri, contentValue.getAsString(BaseContract._ID));
                }

                return result;
        }
    }

    private void updateBundle(UriIDs match) {
        switch (match) {
            case Settings:
                updateSettings();
                break;
            case Session:
                updateSession();
                break;
        }
    }

    /**
     * Update settings memory cache
     */
    private void updateSettings() {
        String[] projection = {ConfigContract.Settings.KEY, ConfigContract.Settings.VALUE};

        try (Cursor cursor = query(ConfigContract.Settings.CONTENT_URI, projection, null,
                null, null)) {
            mSettingsMap = updateMemoryCache(cursor);
        } catch (Exception e) {
            Log.e(TAG, "Fail to get settings data", e);
        }
    }

    /**
     * Update session memory cache
     */
    private void updateSession() {
        String[] projection = {ConfigContract.Session.KEY, ConfigContract.Session.VALUE};

        try (Cursor cursor = query(ConfigContract.Session.CONTENT_URI, projection, null,
                null, null)) {
            mSessionMap = updateMemoryCache(cursor);
        } catch (Exception e) {
            Log.e(TAG, "Fail to get session data", e);
        }
    }

    /**
     * Update the memory cache
     *
     * @param cursor contains the whole data from database
     */
    private Bundle updateMemoryCache(Cursor cursor) {
        Bundle cache = new Bundle();

        if (cursor == null || !cursor.moveToFirst()) {
            return cache;
        }

        do {
            cache.putString(cursor.getString(0), cursor.getString(1));
        } while (cursor.moveToNext());

        return cache;
    }

    /**
     * Sends change update.
     *
     * @param key exact key for witch column was changed
     */
    private void notifyChange(Uri uri, String key) {
        Context context = getContext();
        if (context == null) {
            Log.d(TAG, "failed to notifyChange(uri: " + uri + ", key: " + key + "): no context");
            return;
        }

        ContentResolver resolver = context.getContentResolver();
        if (resolver == null) {
            Log.d(TAG, "failed to notifyChange(uri: " + uri + ", key: " + key + "): no content resolver");
            return;
        }


        Log.v(TAG, "Notify change : " + ConfigContract.buildUriForKey(uri, key));
        resolver.notifyChange(ConfigContract.buildUriForKey(uri, key), null);
    }

    /**
     * Update or insert data to Technicolor's mass storage.
     *
     * @param extras Key-value pairs to be inserted in Technicolor's content provider.
     */
    private void putDataToMassStorage(Bundle extras) {
        Context context = getContext();
        if (context == null || extras == null) {
            Log.e(TAG, "Could not update or insert data to Technicolor's content provider.");
            return;
        }
        ContentResolver contentResolver = context.getContentResolver();

        List<ContentValues> contentValuesList = new ArrayList<>();

        for (String key : extras.keySet()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(MassStorageContract.KEY, key);
            contentValues.put(MassStorageContract.NAME, key);
            contentValues.put(MassStorageContract.STATE, extras.getString(key));
            contentValuesList.add(contentValues);
        }

        for (ContentValues contentValues : contentValuesList) {
            try {
                String selection = MassStorageContract.NAME + "=?";
                String[] selectionArgs = {contentValues.getAsString(MassStorageContract.KEY)};
                int updatedCount = contentResolver.update(MassStorageContract.CONTENT_URI, contentValues,
                        selection, selectionArgs);
                if (updatedCount == 0) {
                    contentResolver.insert(MassStorageContract.CONTENT_URI, contentValues);
                }
            } catch (Exception ex) {
                Log.e("Failed to update or insert to Technicolor's content provider", ex);
            }
        }
        updateMassStorage();
    }

    /**
     * Delete data from Technicolor's content provider
     */
    private void deleteFromMassStorage(String arg) {
        Context context = getContext();
        if (context == null || TextUtils.isEmpty(arg)) {
            Log.e(TAG, "Could not delete from Technicolor's content provider");
            return;
        }

        try {
            String selection = MassStorageContract.NAME + "=?";
            String[] selectionArgs = new String[]{arg};
            int deletedRows = context.getContentResolver().delete(MassStorageContract.CONTENT_URI, selection, selectionArgs);
            mMassStorageMap.remove(arg);
            Log.d(TAG, "Rows deleted from Technicolor's content provider: " + deletedRows);
        } catch (Exception ex) {
            Log.e(TAG, "Failed to delete arg: " + arg + " from Technicolor's content provider", ex);
        }
        updateMassStorage();
    }

    /**
     * Update mass storage memory cache.
     */
    private void updateMassStorage() {
        Context context = getContext();
        if (context == null) {
            Log.e(TAG, "Could not update the memory cache for mass storage");
            return;
        }

        SqlUtils.forEach(context, MassStorageContract.CONTENT_URI, cursor -> {
            String key = cursor.getString(cursor.getColumnIndex(MassStorageContract.KEY));
            String state = cursor.getString(cursor.getColumnIndex(MassStorageContract.STATE));
            mMassStorageMap.putString(key, state);
        });
    }
}
