/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.tv;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import tv.threess.threeready.api.generic.BaseContract;
import tv.threess.threeready.api.generic.helper.FileUtils;
import tv.threess.threeready.api.generic.helper.UriMatcher;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.generic.BaseProvider;

/**
 * Content provider to access persisted data.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.13
 */
public class TvProvider extends BaseProvider {
    private static final String TAG = Log.tag(TvProvider.class);

    enum UriIDs {
        Channel,
        PlaybackOptions,
        Broadcast,
        BroadcastContinueWatching,
        BroadcastForChannel,
        PlaybackOptionsChannel,
        Favorites,
        Advertisement
    }

    // prepare the uri matcher
    private static final UriMatcher<UriIDs> matcher = new UriMatcher<>(TvContract.AUTHORITY, UriIDs.class)
            .add(UriIDs.Channel, TvContract.Channel.CONTENT_URI)
            .add(UriIDs.PlaybackOptions, TvContract.PlaybackOption.CONTENT_URI)
            .add(UriIDs.Broadcast, TvContract.Broadcast.CONTENT_URI)
            .add(UriIDs.BroadcastContinueWatching, TvContract.ContinueWatchingBroadcast.CONTENT_URI)
            .add(UriIDs.BroadcastForChannel, TvContract.Broadcast.CONTENT_URI_WITH_CHANNEL, "*")
            .add(UriIDs.PlaybackOptionsChannel, TvContract.PlaybackOption.CONTENT_URI_WITH_CHANNEL)
            .add(UriIDs.Favorites, TvContract.Favorites.CONTENT_URI)
            .add(UriIDs.Advertisement, TvContract.Advertisement.CONTENT_URI);

    private TvDatabase mDatabase;

    @Override
    public boolean onCreate() {
        super.onCreate();
        // prepare the database helper
        mDatabase = new TvDatabase(this.getContext());
        return true;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        UriIDs match = matcher.matchEnum(uri);
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);

            case Channel:
                return TvContract.Channel.MIME_TYPE;

            case PlaybackOptions:
            case PlaybackOptionsChannel:
                return TvContract.PlaybackOption.MIME_TYPE;

            case Broadcast:
            case BroadcastForChannel:
                return TvContract.Broadcast.MIME_TYPE;

            case Favorites:
                return TvContract.Favorites.MIME_TYPE;
            case Advertisement:
                return TvContract.Advertisement.MIME_TYPE;

            case BroadcastContinueWatching:
                return TvContract.ContinueWatchingBroadcast.MIME_TYPE;
        }
    }

    @Override
    public Bundle call(String method, String arg, Bundle extras) {
        switch (method) {
            case DATABASE_VERSION_CHECK:
                mDatabase.getReadableDatabase();
                break;
            case TvContract.Advertisement.CALL_INCREMENT_ATTEMPTS:
                String query = "UPDATE " + TvContract.Advertisement.TABLE_NAME +
                        " SET " + TvContract.Advertisement.ATTEMPT_COUNT + "=" + TvContract.Advertisement.ATTEMPT_COUNT + "+1, "
                        + TvContract.Advertisement.LAST_UPDATED + "=" + System.currentTimeMillis()
                        + " WHERE " + TvContract.Advertisement.CREATIVE_ID + "='" + arg + "'";
                SQLiteDatabase db = mDatabase.getWritableDatabase();
                db.beginTransaction();
                try {
                    db.execSQL(query);
                    db.setTransactionSuccessful();
                } catch (Exception e) {
                    Log.e(TAG, "Exception while incrementing attempt counter: ", e);
                } finally {
                    db.endTransaction();
                    FileUtils.closeSafe(db);
                }
                return extras;
        }
        return super.call(method, arg, extras);
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);

            case Channel:
                table = TvContract.Channel.TABLE_NAME +
                        " LEFT JOIN " + TvContract.Favorites.TABLE_NAME +
                        " ON (" + TvContract.Favorites.TABLE_NAME + "." + TvContract.Favorites.CHANNEL_ID
                        + " = " + TvContract.Channel.QUALIFIED_ID + ")";
                break;

            case Broadcast:
                table = TvContract.Broadcast.TABLE_NAME +
                        " INNER JOIN " + TvContract.Channel.TABLE_NAME +
                        " ON " + TvContract.Broadcast.QUALIFIED_CHANNEL_ID
                        + " = " + TvContract.Channel.QUALIFIED_ID +
                        " LEFT JOIN " + TvContract.Favorites.TABLE_NAME +
                        " ON (" + TvContract.Favorites.QUALIFIED_CHANNEL_ID
                        + " = " + TvContract.Broadcast.QUALIFIED_CHANNEL_ID + ")";
                break;

            case Favorites:
                table = TvContract.Favorites.TABLE_NAME;
                break;

            case Advertisement:
                table = TvContract.Advertisement.TABLE_NAME;
                break;

            case BroadcastForChannel:
                table = TvContract.Broadcast.TABLE_NAME +
                        " INNER JOIN " + TvContract.Channel.TABLE_NAME +
                        " ON " + TvContract.Broadcast.CHANNEL_ID + " = " + TvContract.Channel.QUALIFIED_ID;

                // Filter for the given channel.
                final String channelId = uri.getLastPathSegment();
                selection = DatabaseUtils.concatenateWhere(selection, TvContract.Broadcast.CHANNEL_ID + " = ?");
                selectionArgs = DatabaseUtils.appendSelectionArgs(selectionArgs, new String[]{channelId});
                break;

            case PlaybackOptions:
                table = TvContract.PlaybackOption.TABLE_NAME;
                break;

            case PlaybackOptionsChannel:
                table = TvContract.PlaybackOption.TABLE_NAME +
                        " INNER JOIN " + TvContract.Channel.TABLE_NAME +
                        " ON " + TvContract.PlaybackOption.CHANNEL_ID + " = " + TvContract.Channel.QUALIFIED_ID;
                break;

            case BroadcastContinueWatching:
                table = TvContract.ContinueWatchingBroadcast.TABLE_NAME +
                        " INNER JOIN " + TvContract.Channel.TABLE_NAME +
                        " ON " + TvContract.ContinueWatchingBroadcast.QUALIFIED_CHANNEL_ID
                        + " = " + TvContract.Channel.QUALIFIED_ID;
                break;
        }

        SQLiteDatabase db = mDatabase.getReadableDatabase();
        return super.query(uri, db, table, projection, selection, selectionArgs, sortOrder);
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);

            case Channel:
                table = TvContract.Channel.TABLE_NAME;
                break;

            case Favorites:
                table = TvContract.Favorites.TABLE_NAME;
                break;

            case Advertisement:
                table = TvContract.Advertisement.TABLE_NAME;
                break;

            case Broadcast:
                table = TvContract.Broadcast.TABLE_NAME;
                break;

            case BroadcastForChannel:
                table = TvContract.Broadcast.TABLE_NAME;
                values.put(TvContract.Broadcast.CHANNEL_ID, uri.getLastPathSegment());
                break;

            case BroadcastContinueWatching:
                table = TvContract.ContinueWatchingBroadcast.TABLE_NAME;
                break;
        }

        long rowId = super.insert(uri, mDatabase.getWritableDatabase(), table, values);
        return rowId < 0 ? null : uri;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);

            case Channel:
                table = TvContract.Channel.TABLE_NAME;
                break;

            case PlaybackOptions:
                table = TvContract.PlaybackOption.TABLE_NAME;
                break;

            case Broadcast:
                table = TvContract.Broadcast.TABLE_NAME;
                break;

            case BroadcastForChannel:
                table = TvContract.Broadcast.TABLE_NAME;

                // Filter for the given channel.
                final String channelId = uri.getLastPathSegment();
                selection = DatabaseUtils.concatenateWhere(selection, TvContract.Broadcast.CHANNEL_ID + " = ?");
                selectionArgs = DatabaseUtils.appendSelectionArgs(selectionArgs, new String[]{channelId});
                break;

            case Favorites:
                table = TvContract.Favorites.TABLE_NAME;
                break;

            case Advertisement:
                table = TvContract.Advertisement.TABLE_NAME;
                break;

            case BroadcastContinueWatching:
                table = TvContract.ContinueWatchingBroadcast.TABLE_NAME;
                break;
        }

        return super.delete(uri,
                mDatabase.getWritableDatabase(), table, selection, selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);

            case Channel:
                table = TvContract.Channel.TABLE_NAME;
                break;

            case PlaybackOptions:
                table = TvContract.PlaybackOption.TABLE_NAME;
                break;

            case Broadcast:
                table = TvContract.Broadcast.TABLE_NAME;
                break;

            case BroadcastForChannel:
                table = TvContract.Broadcast.TABLE_NAME;

                // Filter for the given channel.
                final String channelId = uri.getLastPathSegment();
                selection = DatabaseUtils.concatenateWhere(selection, TvContract.Broadcast.CHANNEL_ID + " = ?");
                selectionArgs = DatabaseUtils.appendSelectionArgs(selectionArgs, new String[]{channelId});
                values.put(TvContract.Broadcast.CHANNEL_ID, channelId);
                break;

            case Advertisement:
                table = TvContract.Advertisement.TABLE_NAME;
                break;

            case BroadcastContinueWatching:
                table = TvContract.ContinueWatchingBroadcast.TABLE_NAME;
                break;
        }

        return super.update(uri, mDatabase.getWritableDatabase(), table, values, selection, selectionArgs);
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        UriIDs match = matcher.matchEnum(uri);
        final String table;
        switch (match) {
            default:
                throw new UnsupportedOperationException("Unimplemented match: " + match);

            case Favorites:
                table = TvContract.Favorites.TABLE_NAME;
                break;

            case Advertisement:
                table = TvContract.Advertisement.TABLE_NAME;
                break;

            case Channel:
                table = TvContract.Channel.TABLE_NAME;
                break;

            case PlaybackOptions:
                table = TvContract.PlaybackOption.TABLE_NAME;
                break;

            case Broadcast:
                table = TvContract.Broadcast.TABLE_NAME;
                break;

            case BroadcastForChannel:
                SQLiteDatabase database = mDatabase.getWritableDatabase();

                String replaceTimeStamp = uri.getQueryParameter(BaseContract.BULK_REPLACE);
                String replaceWhereCase = null;
                String[] replaceWhereArgs = null;

                if (!TextUtils.isEmpty(replaceTimeStamp)) {
                    replaceWhereCase = BaseContract.LAST_UPDATED + "<" + replaceTimeStamp
                            + " AND " + TvContract.Broadcast.CHANNEL_ID + " =? ";
                    replaceWhereArgs = new String[]{uri.getLastPathSegment()};
                }

                int bulkInsertedBroadcastsChannel = bulkInsertReplacePurge(uri,
                        database, TvContract.Broadcast.TABLE_NAME, replaceWhereCase, replaceWhereArgs, values);

                Log.d(TAG, "Inserted broadcasts: " + bulkInsertedBroadcastsChannel);
                return bulkInsertedBroadcastsChannel;

            case BroadcastContinueWatching:
                table = TvContract.ContinueWatchingBroadcast.TABLE_NAME;
                break;
        }

        String replace = uri.getQueryParameter(BaseContract.BULK_REPLACE);
        return bulkInsertReplacePurge(uri,
                mDatabase.getWritableDatabase(), table, replace, values);
    }


    @NonNull
    @Override
    public ContentProviderResult[] applyBatch(@NonNull ArrayList<ContentProviderOperation> operations) throws OperationApplicationException {
        return super.applyBatch(mDatabase.getWritableDatabase(), operations);
    }
}
