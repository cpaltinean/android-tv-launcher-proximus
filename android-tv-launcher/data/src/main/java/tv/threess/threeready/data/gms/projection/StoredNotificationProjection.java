package tv.threess.threeready.data.gms.projection;

import android.content.ContentValues;
import android.database.Cursor;

import tv.threess.threeready.api.notification.model.NotificationItem;
import tv.threess.threeready.data.generic.IProjection;
import tv.threess.threeready.data.gms.GmsContract;

/**
 * Projection to get system notification.
 *
 * @author Eugen Guzyk
 * @since 2018.08.24
 */
public enum StoredNotificationProjection implements IProjection {

    SBN_KEY(GmsContract.StoredNotification.COLUMN_SBN_KEY),
    PACKAGE_NAME(GmsContract.StoredNotification.COLUMN_PACKAGE_NAME),
    NOTIF_TITLE(GmsContract.StoredNotification.COLUMN_NOTIF_TITLE),
    NOTIF_TEXT(GmsContract.StoredNotification.COLUMN_NOTIF_TEXT),
    AUTODISMISS(GmsContract.StoredNotification.COLUMN_AUTODISMISS),
    DISMISSIBLE(GmsContract.StoredNotification.COLUMN_DISMISSIBLE),
    ONGOING(GmsContract.StoredNotification.COLUMN_ONGOING),
    SMALL_ICON(GmsContract.StoredNotification.COLUMN_SMALL_ICON),
    IMPORTANCE(GmsContract.StoredNotification.COLUMN_IMPORTANCE),
    PROGRESS(GmsContract.StoredNotification.COLUMN_PROGRESS),
    PROGRESS_MAX(GmsContract.StoredNotification.COLUMN_PROGRESS_MAX),
    FLAGS(GmsContract.StoredNotification.COLUMN_FLAGS),
    HAS_CONTENT_INTENT(GmsContract.StoredNotification.COLUMN_HAS_CONTENT_INTENT),
    BIG_PICTURE(GmsContract.StoredNotification.COLUMN_BIG_PICTURE),
    CONTENT_BUTTON_LABEL(GmsContract.StoredNotification.COLUMN_CONTENT_BUTTON_LABEL),
    DISMISS_BUTTON_LABEL(GmsContract.StoredNotification.COLUMN_DISMISS_BUTTON_LABEL),
    TAG(GmsContract.StoredNotification.COLUMN_TAG),
    READ(GmsContract.StoredNotification.COLUMN_READ);

    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    StoredNotificationProjection(String column) {
        this.mColumn = column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    public static NotificationItem readRow(Cursor cursor) {
        return new NotificationItem.Builder()
                .setSbnKey(cursor.getString(SBN_KEY.ordinal()))
                .setPackageName(cursor.getString(PACKAGE_NAME.ordinal()))
                .setNotifTitle(cursor.getString(NOTIF_TITLE.ordinal()))
                .setNotifText(cursor.getString(NOTIF_TEXT.ordinal()))
                .setAutoDismiss(cursor.getInt(AUTODISMISS.ordinal()))
                .setDismissible(cursor.getInt(DISMISSIBLE.ordinal()))
                .setOngoing(cursor.getInt(ONGOING.ordinal()))
                .setSmallIcon(cursor.getBlob(SMALL_ICON.ordinal()))
                .setImportance(cursor.getInt(IMPORTANCE.ordinal()))
                .setProgress(cursor.getInt(PROGRESS.ordinal()))
                .setProgressMax(cursor.getInt(PROGRESS_MAX.ordinal()))
                .setFlags(cursor.getInt(FLAGS.ordinal()))
                .setHasContentIntent(cursor.getInt(HAS_CONTENT_INTENT.ordinal()))
                .setBigPicture(cursor.getBlob(BIG_PICTURE.ordinal()))
                .setContentButtonLabel(cursor.getString(CONTENT_BUTTON_LABEL.ordinal()))
                .setDismissButtonLabel(cursor.getString(DISMISS_BUTTON_LABEL.ordinal()))
                .setTag(cursor.getString(TAG.ordinal()))
                .setRead(cursor.getInt(READ.ordinal()) >= NotificationItem.TRUE)
                .build();
    }

    public static ContentValues getContentValues(NotificationItem notification) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(GmsContract.StoredNotification.COLUMN_SBN_KEY, notification.getSbnKey());
        contentValues.put(GmsContract.StoredNotification.COLUMN_PACKAGE_NAME, notification.getPackageName());
        contentValues.put(GmsContract.StoredNotification.COLUMN_NOTIF_TITLE, notification.getNotifTitle());
        contentValues.put(GmsContract.StoredNotification.COLUMN_NOTIF_TEXT, notification.getNotifText());
        contentValues.put(GmsContract.StoredNotification.COLUMN_AUTODISMISS, notification.isAutoDismiss());
        contentValues.put(GmsContract.StoredNotification.COLUMN_DISMISSIBLE, notification.isDismissible());
        contentValues.put(GmsContract.StoredNotification.COLUMN_ONGOING, notification.isOngoing());
        contentValues.put(GmsContract.StoredNotification.COLUMN_SMALL_ICON, notification.getSmallIcon());
        contentValues.put(GmsContract.StoredNotification.COLUMN_IMPORTANCE, notification.getImportance());
        contentValues.put(GmsContract.StoredNotification.COLUMN_PROGRESS, notification.getProgress());
        contentValues.put(GmsContract.StoredNotification.COLUMN_PROGRESS_MAX, notification.getProgressMax());
        contentValues.put(GmsContract.StoredNotification.COLUMN_FLAGS, notification.getFlags());
        contentValues.put(GmsContract.StoredNotification.COLUMN_HAS_CONTENT_INTENT, notification.hasContentIntent());
        contentValues.put(GmsContract.StoredNotification.COLUMN_BIG_PICTURE, notification.getBigPicture());
        contentValues.put(GmsContract.StoredNotification.COLUMN_CONTENT_BUTTON_LABEL, notification.getContentButtonLabel());
        contentValues.put(GmsContract.StoredNotification.COLUMN_DISMISS_BUTTON_LABEL, notification.getDismissButtonLabel());
        contentValues.put(GmsContract.StoredNotification.COLUMN_TAG, notification.getTag());
        contentValues.put(GmsContract.StoredNotification.COLUMN_READ, notification.isRead());

        return contentValues;
    }
}
