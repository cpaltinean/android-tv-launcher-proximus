/*
 *
 *  * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.home.model.generic;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import tv.threess.threeready.api.generic.helper.ArrayUtils;
import tv.threess.threeready.api.home.model.generic.ContentConfig;
import tv.threess.threeready.api.home.model.menu.MenuItem;
import tv.threess.threeready.api.home.model.page.PageConfig;
import tv.threess.threeready.data.home.model.page.MenuItem3Ready;
import tv.threess.threeready.data.home.model.page.PageConfig3Ready;

/**
 * Main menu and page configuration of the app.

 * Created by Szilard on 4/13/2017.
 */
public class ContentConfig3Ready implements ContentConfig {

    @SerializedName("main_menu")
    private final List<MenuItem3Ready> mMainMenuItemList = new ArrayList<>();

    @SerializedName("configs")
    private final List<PageConfig3Ready> mPageConfigs = new ArrayList<>();

    private Map<String, PageConfig> mConfigMap;

    @Override
    public List<MenuItem> getMainMenuItemList() {
        // Filter menu items without page definition
        return mMainMenuItemList.stream()
                .filter(menuItem -> getPageConfig(menuItem.getId()) != null).collect(Collectors.toList());
    }

    @Override
    public List<PageConfig> getPageConfigs() {
        return (List) mPageConfigs;
    }

    @Override
    public PageConfig getPageConfig(String pageID) {
        if (mConfigMap == null) {
            populateConfigMap();
        }

        if (mConfigMap.containsKey(pageID)) {
            return mConfigMap.get(pageID);
        }

        return null;
    }

    @Nullable
    @Override
    public MenuItem getMenuItem(String menuId) {
        for (MenuItem menuItem : getMainMenuItemList()) {
            if (Objects.equals(menuItem.getId(), menuId)) {
                return menuItem;
            }
        }
        return null;
    }

    private void populateConfigMap() {
        mConfigMap = new HashMap<>();

        for (PageConfig config : ArrayUtils.notNull(mPageConfigs)) {
            mConfigMap.put(config.getId(), config);
        }
    }
}
