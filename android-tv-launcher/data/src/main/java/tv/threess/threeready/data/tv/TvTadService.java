/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 *
 */

package tv.threess.threeready.data.tv;

import android.content.Context;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import androidx.work.Data;
import androidx.work.WorkerParameters;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.helper.TimeBuilder;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.TvServiceRepository;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.generic.BaseWorker;
import tv.threess.threeready.data.generic.JobIntent;

/**
 * Download data from the backend and persist it in the database.
 *
 * @author Karetka Mezei Zoltan
 * @since 2017.04.13
 */
public class TvTadService extends BaseWorkManager {
    private static final String TAG = Log.tag(TvTadService.class);

    public static final String INTENT_EXTRA_TAD_FILE_NAME = "intent.extra.TAD_FILE_NAME";
    public static final String INTENT_EXTRA_TRIGGER_TIMESTAMP = "intent.extra.TRIGGER_TIMESTAMP";

    private static final Map<String, Long> sOnMarkerDownloadAlarms = new ConcurrentHashMap<>();

    /**
     * Pre-download all ad files
     */
    public static JobIntent buildAdvertisementDownloadIntent() {
        return new JobIntent(DownloadAdvertisements.class, new Data.Builder()
                .putLong(INTENT_EXTRA_TRIGGER_TIMESTAMP, System.currentTimeMillis())
                .build()
        );
    }

    /**
     * On-marker download
     */
    private static JobIntent buildAdvertisementDownloadIntent(String filename) {
        return new JobIntent(DownloadAdvertisements.class, new Data.Builder()
                .putLong(INTENT_EXTRA_TRIGGER_TIMESTAMP, System.currentTimeMillis())
                .putString(INTENT_EXTRA_TAD_FILE_NAME, filename)
                .build()
        );
    }

    /**
     * Schedule the download of an on-marker advertisement video file download.
     */
    public static void scheduleAdvertisementDownload(Context context, String filename) {
        Long alarm = sOnMarkerDownloadAlarms.get(filename);
        if (alarm != null && alarm > System.currentTimeMillis()) {
            Log.d(TAG, "alarm is already scheduled");
            return;
        }
        long when = TimeBuilder.utc()
                .distribute(TimeUnit.SECONDS, Settings.tasRequestRandomInterval.get(180))
                .get();

        BaseWorkManager.startAt(context, buildAdvertisementDownloadIntent(filename), false, when);
        sOnMarkerDownloadAlarms.put(filename, when);
    }

    public static class DownloadAdvertisements extends BaseWorker {
        public DownloadAdvertisements(Context context, WorkerParameters workerParams) {
            super(context, workerParams);
        }

        @Override
        protected void executeJob() throws Exception {
            sOnMarkerDownloadAlarms.values().removeIf(alarm -> alarm < System.currentTimeMillis());
            long triggerTime = getLongInputData(INTENT_EXTRA_TRIGGER_TIMESTAMP, 0);
            String filename = getStringInputData(INTENT_EXTRA_TAD_FILE_NAME);
            if (filename != null) {
                sOnMarkerDownloadAlarms.keySet().removeIf(key -> key.equals(filename));
            }
            Components.get(TvServiceRepository.class).downloadAdvertisements(triggerTime, filename);
        }
    }
}
