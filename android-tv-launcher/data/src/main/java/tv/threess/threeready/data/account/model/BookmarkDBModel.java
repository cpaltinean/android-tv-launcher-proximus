package tv.threess.threeready.data.account.model;

import androidx.annotation.FloatRange;

import tv.threess.threeready.api.generic.model.BookmarkType;
import tv.threess.threeready.api.generic.model.IBookmark;

/**
 * Bookmark entity created from database.
 *
 * @author Daniela Toma
 * @since 2020.04.28
 */
public class BookmarkDBModel implements IBookmark {

    private String mContentId;
    private long mDuration;
    private long mPosition = -1;
    private BookmarkType mType;
    private long mLastUpdatedDate;

    public BookmarkDBModel() {
        super();
    }


    @Override
    public String getContentId() {
        return mContentId;
    }

    @Override
    public long getDuration() {
        return mDuration;
    }

    @Override
    public long getPosition() {
        return mPosition;
    }

    @Override
    public BookmarkType getType() {
        return mType;
    }

    @Override
    public long getTimestamp() {
        return mLastUpdatedDate;
    }

    @Override
    public boolean isNearEnd(double maxWatchPercentage) {
        return getWatchedPercent(mPosition, mDuration) >= maxWatchPercentage;
    }

    /**
     * @return The percentage of the bookmark position compared to the duration.
     */
    @FloatRange(from = 0, to = 100)
    private double getWatchedPercent(long position, long duration) {
        return position * 100d / duration;
    }

    @Override
    public String toString() {
        return "BookmarkDBModel{" +
                "mContentId='" + mContentId + '\'' +
                ", mDuration=" + mDuration +
                ", mPosition=" + mPosition +
                ", mType=" + mType +
                ", mLastUpdatedDate=" + mLastUpdatedDate +
                '}';
    }

    public static class Builder {

        BookmarkDBModel mBookmark = new BookmarkDBModel();

        public static Builder builder() {
            return new Builder();
        }

        public Builder setContentId(String contentId) {
            mBookmark.mContentId = contentId;
            return this;
        }

        public Builder setDuration(long duration) {
            mBookmark.mDuration = duration;
            return this;
        }

        public Builder setPosition(long position) {
            mBookmark.mPosition = position;
            return this;
        }

        public Builder setType(BookmarkType type) {
            mBookmark.mType = type;
            return this;
        }

        public Builder setLastUpdatedDate(long lastUpdatedDate) {
            mBookmark.mLastUpdatedDate = lastUpdatedDate;
            return this;
        }

        public IBookmark build() {
            return mBookmark;
        }
    }
}
