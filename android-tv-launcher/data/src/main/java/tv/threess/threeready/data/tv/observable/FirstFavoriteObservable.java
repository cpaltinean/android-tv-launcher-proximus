package tv.threess.threeready.data.tv.observable;

import android.content.Context;
import android.net.Uri;

import io.reactivex.ObservableEmitter;
import tv.threess.lib.di.Components;
import tv.threess.threeready.data.tv.TvContract;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.generic.observable.BaseContentObservable;
import tv.threess.threeready.data.tv.TvCache;

/**
 * Rx observable class to return the first favorite object in the database.
 *
 * @author Denisa Trif
 * @since 2018.08.23
 */
public class FirstFavoriteObservable extends BaseContentObservable<TvChannel> {

    private final TvCache mTvCache = Components.get(TvCache.class);

    public FirstFavoriteObservable(Context context) {
        super(context);
    }

    @Override
    public void subscribe(ObservableEmitter<TvChannel> e) throws Exception {
        registerObserver(TvContract.Channel.CONTENT_URI);
        TvChannel tvChannel = mTvCache.getFirstFavoriteChannel();
        if (tvChannel != null) {
            e.onNext(tvChannel);
        } else {
            e.onNext(mTvCache.getFirstChannel());
        }
        super.subscribe(e);
    }

    @Override
    protected void onChange(Uri uri, ObservableEmitter<TvChannel> observableEmitter) {
        observableEmitter.onNext(mTvCache.getFirstFavoriteChannel());
    }
}
