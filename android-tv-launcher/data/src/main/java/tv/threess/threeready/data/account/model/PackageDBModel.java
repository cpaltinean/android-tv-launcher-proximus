package tv.threess.threeready.data.account.model;

import android.text.TextUtils;

import java.util.Arrays;
import java.util.List;

import tv.threess.threeready.api.account.model.account.IPackage;

/**
 * Package entity created from database.
 *
 * Created by Noemi Dali on 27.03.2020.
 */
public class PackageDBModel implements IPackage {

    private String packageId;
    private String externalId;
    private long packageStartDate;
    private long packageEndDate;
    private boolean isLocked;
    private List<String> packageType;

    @Override
    public String getPackageId() {
        return packageId;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    @Override
    public long getPackageStartDate() {
        return packageStartDate;
    }

    @Override
    public long getPackageEndDate() {
        return packageEndDate;
    }

    @Override
    public boolean isLocked() {
        return isLocked;
    }

    @Override
    public List<String> getPackageTypes() {
        return packageType;
    }

    public static class Builder {

        PackageDBModel mPackageModel;

        public Builder() {
            mPackageModel = new PackageDBModel();
        }

        public Builder setPackageId(String id) {
            mPackageModel.packageId = id;
            return this;
        }

        public Builder setExternalId(String externalId) {
            mPackageModel.externalId = externalId;
            return this;
        }

        public Builder setPackageStartDate(long startDate) {
            mPackageModel.packageStartDate = startDate;
            return this;
        }

        public Builder setPackageEndDate(long endDate) {
            mPackageModel.packageEndDate = endDate;
            return this;
        }

        public Builder setIsLocked(boolean isLocked) {
            mPackageModel.isLocked = isLocked;
            return this;
        }

        public Builder setPackageType(String types) {
            mPackageModel.packageType = Arrays.asList(TextUtils.split(types, ","));
            return this;
        }

        public PackageDBModel build() {
            return mPackageModel;
        }
    }
}
