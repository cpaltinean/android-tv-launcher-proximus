/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.data.home.model.module;

import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;

import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.home.model.module.EditorialItem;
import tv.threess.threeready.api.home.model.module.EditorialItemAction;
import tv.threess.threeready.api.home.model.module.EditorialItemStyle;
import tv.threess.threeready.api.home.model.module.ModuleCondition;
import tv.threess.threeready.api.pc.model.ParentalRating;

/**
 * POJO class represents one item in the editorial stripe.
 *
 * @author Barabas Attila
 * @since 2018.08.23
 */
public class EditorialItem3Ready implements EditorialItem {

    @SerializedName("id")
    String mId;

    @SerializedName("title")
    String mTitle;

    @SerializedName("content_description")
    String mContentDescription;

    @SerializedName("fallback_title")
    String mFallbackTitle;

    @SerializedName("position")
    Integer mPosition;

    @SerializedName("style")
    EditorialItemStyle3Ready mStyle;

    @SerializedName("action")
    EditorialItemAction3Ready mAction;

    @SerializedName("conditions")
    ModuleCondition3Ready[] mConditions;

    public String getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getContentDescription() {
        return mContentDescription;
    }

    @Override
    public ParentalRating getParentalRating() {
        return ParentalRating.Undefined;
    }

    @Override
    public List<IImageSource> getImageSources() {
        return Collections.emptyList();
    }

    @Override
    public String getFallbackTitle() {
        return mFallbackTitle;
    }

    @Override
    public EditorialItemStyle getStyle() {
        return mStyle;
    }

    @Override
    public EditorialItemAction getAction() {
        return mAction;
    }

    @Override
    public Integer getPosition() {
        return mPosition;
    }

    @Override
    public ModuleCondition[] getConditions() {
        return mConditions;
    }

    /**
     * Class to build a module item.
     */
    public static class Builder {
        EditorialItem3Ready mModuleItem;

        public Builder() {
            mModuleItem = new EditorialItem3Ready();
        }

        public Builder setTitle(String title) {
            mModuleItem.mTitle = title;
            return this;
        }

        public Builder setFallbackTitle(String fallbackTitle) {
            mModuleItem.mFallbackTitle = fallbackTitle;
            return this;
        }

        public Builder setStyle(EditorialItemStyle3Ready itemStyle) {
            mModuleItem.mStyle = itemStyle;
            return this;
        }

        public Builder setAction(EditorialItemAction3Ready action) {
            mModuleItem.mAction = action;
            return this;
        }

        public EditorialItem3Ready build() {
            return mModuleItem;
        }
    }
}
