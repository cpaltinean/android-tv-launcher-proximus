package tv.threess.threeready.data.home.model.generic;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * POJO class which stores the module filter.
 *
 * @author David Bondor
 * @since 18.05.2022
 */
public class ModuleFilter3Ready implements Serializable {

    private final Map<String, Serializable> mFilter;

    public ModuleFilter3Ready(Map<String, Serializable> filter) {
        mFilter = filter;
    }

    public boolean hasFilter(String key) {
        return mFilter.containsKey(key);
    }

    public void addFilter(String key, Serializable value) {
        mFilter.put(key, value);
    }

    public Map<String, Serializable> getFilter() {
        return mFilter;
    }

    public <T extends Serializable> T getFilter(String key, Class<T> clazz) {
        try {
            Serializable value = mFilter.get(key);
            return (T) value;
        } catch (Exception ignored) {
        }
        return null;
    }

    public <T extends Serializable> List<String> getFilterListAsString(String key, Class<T> clazz) {
        T filter = getFilter(key, clazz);
        if (filter == null) {
            return null;
        }

        String[] filterArray = String.valueOf(filter).split(",");
        return Arrays.asList(filterArray);
    }
}
