package tv.threess.threeready.data.vod;

import androidx.annotation.Nullable;

import tv.threess.lib.di.Component;
import tv.threess.threeready.api.vod.model.IBaseVodSeries;

/**
 * Interface used for memory cache of VOD categories.
 *
 * @author David
 * @since 22.03.2022
 */
public interface VodCategoryCache extends Component {

    /**
     * @return The VOD category series found by Id.
     */
    @Nullable
    IBaseVodSeries getVodCategorySeries(String seriesId);

    boolean isCategoryTreeInitialized();
}
