package tv.threess.threeready.data.account.adapter;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.api.home.model.generic.ChannelApp;
import tv.threess.threeready.data.home.model.generic.ChannelApp3Ready;
import tv.threess.threeready.data.utils.GsonUtils;

/**
 * Type adapter to convert channel apps from config to a map.
 *
 * @author Zsolt Bokor_
 * @since 2020.07.16
 */

public class ChannelAppsMapAdapter3Ready extends TypeAdapter<Map<String, ChannelApp>> {

    @Override
    public void write(JsonWriter writer, Map<String, ChannelApp> channelApps) throws IOException {
        if (channelApps == null) {
            return;
        }
        writer.beginArray();
        for (String key : channelApps.keySet()) {
            ChannelApp channelApp = channelApps.get(key);
            GsonUtils.getGson().toJson(channelApp, ChannelApp3Ready.class, writer);
        }
        writer.endArray();
    }

    @Override
    public Map<String, ChannelApp> read(JsonReader reader) throws IOException {
        Map<String, ChannelApp> channelApps = new HashMap<>();
        Gson gson = GsonUtils.getGson();
        reader.beginArray();
        while (reader.hasNext()) {
            ChannelApp channelApp = gson.fromJson(reader, ChannelApp3Ready.class);
            channelApps.put(channelApp.getCallLetter(), channelApp);
        }
        reader.endArray();
        return channelApps;
    }
}
