package tv.threess.threeready.data.vod.model;

import tv.threess.threeready.api.vod.model.IRentalExpiration;
import tv.threess.threeready.api.vod.model.IVodVariant;

/**
 * Base VOD variant model.
 *
 * @author David
 * @since 23.03.2022
 */
public abstract class BaseVodVariantModel implements IVodVariant {

    private long mRentExpiration;
    private long mRentalTime;

    @Override
    public boolean isRented() {
        return mRentExpiration > System.currentTimeMillis();
    }

    @Override
    public long getRentalPeriodEnd() {
        return mRentExpiration;
    }

    @Override
    public long getRentalPeriodStart() {
        return mRentalTime;
    }

    public void setRentalExpirationInfo(IRentalExpiration rentalExpiration) {
        mRentExpiration = rentalExpiration.getExpireTime();
        mRentalTime = rentalExpiration.getRentalTime();
    }
}
