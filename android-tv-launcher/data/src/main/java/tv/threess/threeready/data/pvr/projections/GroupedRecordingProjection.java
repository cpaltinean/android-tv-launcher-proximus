package tv.threess.threeready.data.pvr.projections;

import android.database.Cursor;
import android.text.TextUtils;

import java.util.Arrays;

import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.pc.model.ParentalRating;
import tv.threess.threeready.data.pvr.PvrContract;
import tv.threess.threeready.data.generic.helper.ImageProjectionHelper;
import tv.threess.threeready.data.pvr.model.BaseRecordingSeriesDbModel;

/**
 * Database projection to query recordings grouped by series id.
 *
 * @author Barabas Attila
 * @since 2020.04.20
 */
public enum GroupedRecordingProjection {

    NUMBER_OF_EPISODES("count(DISTINCT " + PvrContract.Recording.BROADCAST_ID + ")", "NUMBER_OF_EPISODES"),
    MAX_RECORDING_START("max(" + PvrContract.Recording.RECORDING_START + ")", "MAX_RECORDING_START"),
    MIN_RECORDING_START("min(" + PvrContract.Recording.RECORDING_START + ")", "MIN_RECORDING_START");

    public static final String[] PROJECTION = new String[
            RecordingsProjection.values().length + values().length];

    private final String mColumn;
    private final String mAlias;

    GroupedRecordingProjection(String column, String alias) {
        mColumn = column;
        mAlias = alias;
    }

    static {
        // Add single recording projection as base.
        RecordingsProjection[] recordingValues = RecordingsProjection.values();
        for (int i = 0; i < recordingValues.length; i++) {
            PROJECTION[i] = recordingValues[i].getColumn();
        }

        // Extend projection with group fields.
        GroupedRecordingProjection[] values = values();
        for (int i = 0; i < values.length; i++) {
            PROJECTION[recordingValues.length + i] = values[i].mColumn
                    + (TextUtils.isEmpty(values[i].mAlias) ? "" : " as " + values[i].mAlias);
        }
    }

    public String getAlias() {
        return mAlias;
    }

    /**
     * Read a grouped recordings by series id from cursor row.
     *
     * @param cursor Cursor with program details.
     * @return The single or grouped recording.
     */
    public static IBaseContentItem readRow(Cursor cursor) {
        int numberOfEpisodes = cursor.getInt(
                RecordingsProjection.values().length + NUMBER_OF_EPISODES.ordinal());

        if (numberOfEpisodes > 1) {
            return new BaseRecordingSeriesDbModel.Builder()
                    .id(cursor.getString(RecordingsProjection.SERIES_ID.ordinal()))
                    .title(cursor.getString(RecordingsProjection.TITLE.ordinal()))
                    .channelId(cursor.getString(RecordingsProjection.CHANNEL_ID.ordinal()))
                    .numberOfEpisodes(numberOfEpisodes)
                    .imageSources(ImageProjectionHelper.imagesFromCursorRow(cursor,
                            RecordingsProjection.IMAGE_URL.ordinal(),
                            RecordingsProjection.IMAGE_TYPE.ordinal()))
                    .genres(Arrays.asList(TextUtils.split(cursor.getString(
                            RecordingsProjection.GENRES.ordinal()), ",")))
                    .parentalRating(ParentalRating.valueOf(ParentalRating.class,
                            cursor.getString(RecordingsProjection.PARENTAL_RATING.ordinal())))
                    .build();
        }

        return RecordingsProjection.readRow(cursor);
    }

}
