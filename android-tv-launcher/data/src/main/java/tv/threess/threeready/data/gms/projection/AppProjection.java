/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.data.gms.projection;

import android.database.Cursor;

import java.util.HashMap;
import java.util.Map;

import tv.threess.threeready.data.gms.GmsContract;
import tv.threess.threeready.data.generic.IProjection;

/**
 * Projection to get the all application related info from database (package name, last opened time).
 *
 * @author Barabas Attila
 * @since 2017.05.23
 */
public enum AppProjection implements IProjection {
    PACKAGE_NAME(GmsContract.Apps.PACKAGE_NAME),
    LAST_OPENED(GmsContract.Apps.LAST_OPENED);

    // Projection used to query.
    public static final String[] PROJECTION = IProjection.build(values());

    private final String mColumn;

    AppProjection(String column) {
        mColumn = GmsContract.Apps.TABLE_NAME + "." + column;
    }

    @Override
    public String getColumn() {
        return mColumn;
    }

    /**
     * Creates a package name last opened time map from cursor.
     */
    public static Map<String, Long> mapFromCursor(Cursor cursor) {
        Map<String, Long> result = new HashMap<>();
        while (cursor != null && cursor.moveToNext()) {
            String packageName = cursor.getString(AppProjection.PACKAGE_NAME.ordinal());
            long value = cursor.getLong(AppProjection.LAST_OPENED.ordinal());
            result.put(packageName, value);
        }
        return result;
    }
}
