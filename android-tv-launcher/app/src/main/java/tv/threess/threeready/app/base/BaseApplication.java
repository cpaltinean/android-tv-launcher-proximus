/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */
package tv.threess.threeready.app.base;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.text.TextUtils;

import androidx.annotation.WorkerThread;

import com.bumptech.glide.Glide;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.BatchSettingEditor;
import tv.threess.threeready.api.account.setting.MassStorage;
import tv.threess.threeready.api.account.setting.MemoryCache;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.config.model.Config;
import tv.threess.threeready.api.generic.ComponentsInitializer;
import tv.threess.threeready.api.generic.receivers.BaseBroadcastReceiver;
import tv.threess.threeready.api.generic.receivers.HdmiConnectivityReceiver;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.generic.receivers.NetworkChangeReceiver;
import tv.threess.threeready.api.generic.receivers.StandByStateChangeReceiver;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.helper.ErrorType;
import tv.threess.threeready.api.log.model.Error;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.api.middleware.BluetoothUtils;
import tv.threess.threeready.api.middleware.MwRepository;
import tv.threess.threeready.api.middleware.SystemUpdateReceiver;
import tv.threess.threeready.api.middleware.UpdateUrgency;
import tv.threess.threeready.app.BuildConfig;
import tv.threess.threeready.app.debug.DebugReceiver;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.generic.JobIntent;
import tv.threess.threeready.data.mw.MwService;
import tv.threess.threeready.data.mw.WakeLockHandler;
import tv.threess.threeready.data.utils.OkHttpBuilder.OkHttpBuilder;
import tv.threess.threeready.ui.generic.receiver.AppReceiver;
import tv.threess.threeready.ui.generic.utils.RxUtils;
import tv.threess.threeready.ui.utils.SystemUtils;

/**
 * Base class for maintaining global application state.
 *
 * @author Andor Lukacs
 * @since 6/18/18
 */

public abstract class BaseApplication extends Application implements ComponentsInitializer {
    protected final String TAG = Log.tag(getClass());

    protected boolean mInitialized = false;

    private final AppReceiver mPackageStateReceiver = new AppReceiver();

    private BaseBroadcastReceiver mDebugReceiver = null;

    // Keep a strong reference to avoid cleanup.
    private WakeLockHandler mWakeLockHandler;

    protected StandByStateChangeReceiver mStandByStateChangeReceiver;
    protected InternetChecker mInternetChecker;
    protected HdmiConnectivityReceiver mHdmiReceiver;
    protected NetworkChangeReceiver mNetworkChangeReceiver;
    protected SystemUpdateReceiver mSystemUpdateReceiver;
    protected AppConfig mAppConfig;
    protected Handler mForcedUpdateHandler;

    private final StandByStateChangeReceiver.Listener mStandByStateChangeListener = screenOn -> {
        if (screenOn) {
            Settings.outOfStandbyTime.asyncEdit().put(System.currentTimeMillis());
        } else {
            Settings.intoStandbyTime.asyncEdit().put(System.currentTimeMillis());
        }

        boolean isRemoteConnected = BluetoothUtils.isRemoteConnected(getApplicationContext());
        Log.event(new Event<>(UILogEvent.Standby)
                .addDetail(UILogEvent.Detail.ActiveState, screenOn)
                .addDetail(UILogEvent.Detail.RemoteConnected, isRemoteConnected)
        );

        // Wakeup from standby. Cancel any pending system update apply alarms.
        if (screenOn) {
            BaseWorkManager.cancelAlarm(this, MwService.buildApplySystemUpdateIntent());

            // Box goes to standby. Reboot to apply system update after a short delay.
        } else if (Settings.systemUpdateDownloadedTime.hasValue()){
            long applyDelay = mAppConfig.getStandbySystemUpdateApplyDelay(TimeUnit.MILLISECONDS);
            Log.d(TAG, "Apply system update after " + TimeUnit.MILLISECONDS.toSeconds(applyDelay) + " seconds.");
            BaseWorkManager.scheduleWakeupAlarm(this, MwService.buildApplySystemUpdateIntent(),
                    System.currentTimeMillis() + applyDelay);
        }
    };

    private final InternetChecker.OnStateChangedListener mInternetStateChangeListener = (state) -> {
        if (mStandByStateChangeReceiver.isScreenOn()) {
            Log.event(new Error.Builder(state.isInternetAvailable() ?
                    ErrorType.INTERNET_AVAILABLE : ErrorType.INTERNET_NOT_AVAILABLE).build());
        }
    };

    private final NetworkChangeReceiver.NetworkChangeListener mNetworkChangeListener = connected -> {
        if (mStandByStateChangeReceiver.isScreenOn()) {
            Log.event(new Error.Builder(connected ?
                    ErrorType.INTERNET_CABLE_CONNECTED : ErrorType.INTERNET_CABLE_DISCONNECTED).build());
        }
    };

    private final SystemUpdateReceiver.Listener mSystemUpdateListener = new SystemUpdateReceiver.Listener() {
        @Override
        public void onUpdateDownload(UpdateUrgency urgency, boolean manualCheck) {
            Settings.systemUpdateDownloadedTime.edit().put(System.currentTimeMillis());
            MwRepository mwRepository = Components.get(MwRepository.class);

            // Reboot immediately if the screen is not on or received an automatic update.
            JobIntent systemUpdateIntent = MwService.buildApplySystemUpdateIntent();
            long delay = 0;
            if (mwRepository.isScreenOn() && urgency == UpdateUrgency.Automatic) {
                delay = mAppConfig.getForcedRebootDelay(TimeUnit.MILLISECONDS);
            }

            if (!mwRepository.isScreenOn() || urgency == UpdateUrgency.Automatic) {
                try {
                    mForcedUpdateHandler.postDelayed(() -> BaseWorkManager.start(BaseApplication.this, systemUpdateIntent), delay);
                } catch (Exception e) {
                    Log.d(TAG, "Failed to start the service: " + e);
                }
            }
        }
    };

    // Disable internet check while the box is in standby.
    private final InternetChecker.InternetCheckEnabler mInternetCheckEnabler =
            new InternetChecker.InternetCheckEnabler() {
        @Override
        public boolean checkEnabled() {
            return mStandByStateChangeReceiver.isScreenOn();
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize stetho
        OkHttpBuilder.initializeStetho(this);

        // Initialize logger
        Log.init(this, BuildConfig.LOGCAT_LOG_LEVEL, BuildConfig.EVENT_LOG_LEVEL);

        RxJavaPlugins.setErrorHandler(throwable -> {
            if (throwable instanceof Exception) {
                Log.e(TAG, "Rx Error Handler", throwable);
            } else {
                throw new Exception(throwable);
            }
        });

        RxUtils.initSchedulers();

        initialize();

        MwRepository mwRepository = Components.get(MwRepository.class);
        if (SystemUtils.isInMainProcess(this)) {
            MwService.scheduleRusWindowAlarm(this);

            if (BuildConfig.DEBUG) {
                mDebugReceiver = new DebugReceiver();
                mDebugReceiver.registerReceiver(this);
                StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                        .detectAll()
                        .penaltyLog()
                        .build());

                StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                        .detectLeakedSqlLiteObjects()
                        .detectLeakedClosableObjects()
                        .penaltyLog()
                        .build());
            } else {
                //add Crashlytics mac address key
                String mac = mwRepository.getEthernetMacAddress();
                Log.d(TAG ,"Add mac to firebase. Mac : " + mac);
                FirebaseCrashlytics.getInstance().setCustomKey(
                        getString(tv.threess.threeready.app.R.string.crashlytics_mac_param), mac);
            }

            // Detect reboots.
            String prevBootId = Settings.bootId.get("");
            String newBootId = mwRepository.getBootId();
            if (!TextUtils.equals(prevBootId, newBootId)) {
                onBoot(newBootId);
                Completable.fromAction(() -> updateBootId(newBootId))
                        .subscribeOn(Schedulers.io())
                        .subscribe();
            }
        }
    }

    /**
     * Called when the application starts after a device reboot.
     * @param bootId The unique identifier of the boot.
     */
    private void onBoot(String bootId) {
        boolean quiescentReboot = Components.get(MwRepository.class).isInQuiescentReboot();
        Log.d(TAG, "OnBoot - boot id : " + bootId + ", Quiescent Reboot : " + quiescentReboot);

        BatchSettingEditor<Settings> settingsEditor = Settings.batchEdit()
                .put(Settings.bootId, bootId)
                // Netflix ESN might change after a FW upgrade.
                .remove(Settings.netflixESN)

                // System update
                .remove(Settings.systemUpdateDownloadedTime)
                .remove(Settings.systemUpdateUrgency)

                // DRM registration update.
                .remove(Settings.lastDRMClientSyncTime);

        if (!quiescentReboot) {
            // Set the last sync to 1 to force a cache refresh after a reboot.
            settingsEditor
                    .put(Settings.lastBootConfigSyncTime, 1)
                    .put(Settings.lastConfigSyncTime, 1)
                    .put(Settings.lastTranslationSyncTime, 1)
                    .put(Settings.lastAccountInfoSyncTime, 1)
                    .put(Settings.lastChannelsSyncTime, 1)
                    .put(Settings.lastRecordingSyncTime, 1)
                    .put(Settings.lastRentedVodSyncTime, 1)
                    .put(Settings.consentLastUpdateTime, 1)
                    .remove(Settings.startupFlowFinished);
        }

        settingsEditor.persist();
    }

    @Override
    public void initialize() {
        if (mInitialized) {
            return;
        }
        mInitialized = true;

        getComponentInitializer().initialize();

        mStandByStateChangeReceiver = Components.get(StandByStateChangeReceiver.class);
        mInternetChecker = Components.get(InternetChecker.class);
        mHdmiReceiver = Components.get(HdmiConnectivityReceiver.class);
        mNetworkChangeReceiver = Components.get(NetworkChangeReceiver.class);
        mSystemUpdateReceiver = Components.get(SystemUpdateReceiver.class);
        mAppConfig = Components.get(AppConfig.class);
        mForcedUpdateHandler = new Handler(Looper.getMainLooper());

        if (SystemUtils.isInMainProcess(this)) {
            Log.d(TAG, "We're in the main process, init receivers.");
            mHdmiReceiver.registerReceiver(this);
            mPackageStateReceiver.registerReceiver(this);
            mStandByStateChangeReceiver.registerReceiver(this);
            mNetworkChangeReceiver.registerReceiver(this);
            mSystemUpdateReceiver.registerReceiver();
            mInternetChecker.registerReceiver();
            mInternetChecker.addEnabler(mInternetCheckEnabler);

            // Log network cable connectivity changes.
            mNetworkChangeReceiver.addConnectivityChangeListener(mNetworkChangeListener);

            // Log internet changes.
            mInternetChecker.addStateChangedListener(mInternetStateChangeListener);

            // Log standby change events.
            mStandByStateChangeReceiver.addStateChangedListener(mStandByStateChangeListener);

            // Prevent box to go into network standby before timeout.
            mWakeLockHandler = new WakeLockHandler(this);
            mStandByStateChangeReceiver.addStateChangedListener(mWakeLockHandler);

            // Persist system update download time.
            mSystemUpdateReceiver.addStateChangedListener(mSystemUpdateListener);
        }
    }

    @Override
    public void initConfigComponents(Config config) {
        getComponentInitializer().initConfigComponents(config);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);

        if (SystemUtils.getCurrentProcessType(this) == SystemUtils.ProcessType.MAIN) {
            Log.d(TAG, "Glide - onTrimMemory : " + level);
            Glide.get(this).onTrimMemory(level);
        }
    }

    /**
     * @return A project specific instance of component initializer.
     */
    protected abstract ComponentsInitializer getComponentInitializer();

    @WorkerThread
    private void updateBootId(String bootId) {
        if (bootId.equals(MassStorage.previousBootId.get(null))) {
            Log.d(TAG, "Application start is caused by restart");
            MemoryCache.StartedAfterBoot.edit().put(false);
            return;
        }

        Log.d(TAG, "Application start is caused by reboot");
        MemoryCache.StartedAfterBoot.edit().put(true);
        MassStorage.previousBootId.edit().put(bootId);
    }
}
