/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.app.base;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import com.bumptech.glide.load.model.ResourceLoader;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;

import java.io.InputStream;
import java.io.Serializable;

import okhttp3.OkHttpClient;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.model.IBaseContentItem;
import tv.threess.threeready.api.generic.model.IImageSource;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.tv.model.TvChannel;
import tv.threess.threeready.data.proximus.generic.RetrofitServicesProximus;
import tv.threess.threeready.ui.utils.glide.ChannelGlideLoaderProximus;
import tv.threess.threeready.ui.utils.glide.ContentItemGlideModelLoader;
import tv.threess.threeready.ui.utils.glide.DefaultGlideModelLoader;
import tv.threess.threeready.ui.utils.glide.GlideInternetConnectivityMonitor;
import tv.threess.threeready.ui.utils.glide.ImageSourceGlideModelLoader;

/**
 * Glide default config module
 *
 * @author Ionut Oltean
 * @since 2018.01.17
 */
@GlideModule
public class GlideConfigModule extends AppGlideModule {

    private static final String TAG = GlideConfigModule.class.getName();
    /**
     * The max memory size of cash in bytes for Glide, 20 MB
     */
    private static final int MEMORY_CASH_SIZE_IN_BYTES = 1024 * 1024 * 20;

    public GlideConfigModule() {
        super();
    }

    @Override
    public void registerComponents(Context context, Glide glide, Registry registry) {
        OkHttpClient client = Components.get(RetrofitServicesProximus.class).getGlideOkHttpClient();
        registry.replace(GlideUrl.class, InputStream.class, new OkHttpUrlLoader.Factory(client));

        registry.prepend(TvChannel.class, InputStream.class, (ContentItemLoaderFactory<TvChannel>) ChannelGlideLoaderProximus::new)
                .append(IImageSource.class, InputStream.class, new ImageSourceLoaderFactory(context))
                .append(IBaseContentItem.class, InputStream.class, (ContentItemLoaderFactory<IBaseContentItem>) ContentItemGlideModelLoader::new)
                .append(IBaseContentItem.class, InputStream.class, (ContentItemLoaderFactory<IBaseContentItem>) DefaultGlideModelLoader::new);
    }

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        Log.d(TAG, "applyOptions");

        builder.setDefaultRequestOptions(
                new RequestOptions()
                        .fitCenter().format(DecodeFormat.PREFER_RGB_565))
                .setMemoryCache(new LruResourceCache(MEMORY_CASH_SIZE_IN_BYTES))
                .setConnectivityMonitorFactory((contextInt, listener)
                        -> new GlideInternetConnectivityMonitor(listener));
    }

    /**
     * Glide content item loads to select the best image for the content.
     */
    private interface ContentItemLoaderFactory<TContentItem extends Serializable>
            extends ModelLoaderFactory<TContentItem, InputStream> {

        @NonNull
        @Override
        default ModelLoader<TContentItem, InputStream> build(@NonNull MultiModelLoaderFactory multiFactory) {
            return build(multiFactory.build(GlideUrl.class, InputStream.class));
        }

        ModelLoader<TContentItem, InputStream> build(ModelLoader<GlideUrl, InputStream> concreteLoader);

        @Override
        default void teardown() {}
    }


    /**
     * Glide loader factory to load images for editorial module items.
     */
    private static class ImageSourceLoaderFactory implements ModelLoaderFactory<IImageSource, InputStream> {
        private final Context mContext;

        public ImageSourceLoaderFactory(Context context) {
            mContext = context;
        }

        @NonNull
        @Override
        public ModelLoader<IImageSource, InputStream> build(@NonNull MultiModelLoaderFactory multiFactory) {
            return new ImageSourceGlideModelLoader(mContext,
                    multiFactory.build(GlideUrl.class, InputStream.class),
                    new ResourceLoader<>(mContext.getResources(),
                            multiFactory.build(Uri.class, InputStream.class)));
        }

        @Override
        public void teardown() { }
    }
}
