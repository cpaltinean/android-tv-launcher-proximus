/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.app.debug;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Debug;

import io.reactivex.schedulers.Schedulers;

import java.io.ByteArrayOutputStream;
import java.util.Collections;

import tv.threess.lib.di.Components;

import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.generic.receivers.BaseBroadcastReceiver;
import tv.threess.threeready.api.generic.receivers.InternetChecker;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.data.account.AccountService;
import tv.threess.threeready.data.account.SettingsAccessProxy;
import tv.threess.threeready.data.generic.BaseWorkManager;
import tv.threess.threeready.data.mw.MwProxy;
import tv.threess.threeready.data.mw.MwService;
import tv.threess.threeready.data.proximus.account.SessionProximus;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.tv.TvService;
import tv.threess.threeready.data.tv.TvTadService;
import tv.threess.threeready.data.utils.SqlUtils;


/**
 * Receive and react to debug commands like:
 * adb shell am broadcast -a tv.data.updateChannels --ez force-sync false --ez force-scan false
 * adb shell am broadcast -a fs.exec --esa cmd 'cp','-R','/data/data/be.px.stbtvclient','/sdcard/Download/'
 * adb shell am broadcast -a px.deleteConfig --es session "iamAccessToken"
 * adb shell am broadcast -a fs.exec --esa cmd 'ls','-lAtruc','/tad/pre'
 * adb shell am broadcast -a fs.exec --esa cmd 'rm','-Rf','/tad/pre'
 *
 * @author Karetka Mezei Zoltan.
 * @since 2019.10.07
 */
public class DebugReceiver extends BaseBroadcastReceiver {
    private static final String TAG = Log.tag(DebugReceiver.class);

    private static final String ACTION_DUMP_REFERENCE_TABLE = "android.os.debug.dumpReferenceTables";
    private static final String ACTION_PERFORM_CACHING = "tv.data.performCaching";
    private static final String ACTION_UPDATE_CHANNELS = "tv.data.updateChannels";
    private static final String ACTION_UPDATE_BROADCASTS = "tv.data.updateBroadcasts";
    private static final String ACTION_DOWNLOAD_ADVERTISEMENT = "tv.data.downloadAds";
    private static final String ACTION_DELETE_PLAYBACKS = "tv.data.deletePlaybackOptions";
    private static final String ACTION_ACCOUNT_INFO = "tv.data.updateAccountInfo";
    private static final String ACTION_ACCOUNT_PACKAGE_INFO = "tv.data.updateAccountPackageInfo";
    private static final String ACTION_CHECK_INTERNET = "px.checkInternet";
    private static final String ACTION_DELETE_CONFIG = "px.deleteConfig";
    private static final String ACTION_QUERY_PROVIDER = "px.query";
    private static final String ACTION_FILE_SYSTEM_EXEC = "fs.exec";
    private static final String ACTION_RUS_WINDOW_CHECK = "mw.data.rusWindowCheck";
    private static final String ACTION_RUS_OTA_CHECK = "mw.data.rusOTACheck";

    private static final String EXTRA_FORCE_SYNC = "force-sync";
    private static final String EXTRA_FORCE_SCAN = "force-scan";
    private static final String EXTRA_SETTINGS = "settings";
    private static final String EXTRA_SESSION = "session";
    private static final String EXTRA_CMD = "cmd";
    private static final String EXTRA_URI = "uri";
    private static final String EXTRA_ORDER = "order";

    private final TvCache mTvCache = Components.get(TvCache.class);
    private final SettingsAccessProxy sSettingsAccess = Components.get(SettingsAccessProxy.class);

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null || intent.getAction() == null) {
            return;
        }
        Schedulers.io().scheduleDirect(() -> {
            try {
                switch (intent.getAction()) {
                    case ACTION_DUMP_REFERENCE_TABLE:
                        Debug.class.getDeclaredMethod("dumpReferenceTables").invoke(null);
                        break;

                    case ACTION_PERFORM_CACHING:
                        Settings.lastBootConfigSyncTime.edit().remove();
                        BaseWorkManager.start(context, AccountService.buildPerformCachingIntent());
                        break;

                    case ACTION_UPDATE_CHANNELS:
                        if (intent.getBooleanExtra(EXTRA_FORCE_SYNC, true)) {
                            Settings.lastChannelsSyncTime.edit().remove();
                        }
                        if (intent.getBooleanExtra(EXTRA_FORCE_SCAN, true)) {
                            Settings.iptvPlaybackOptionsHash.edit().remove();
                        }
                        BaseWorkManager.start(context, TvService.buildChannelUpdateIntent());
                        break;

                    case ACTION_DELETE_PLAYBACKS:
                        mTvCache.deletePlaybackOptions();
                        break;

                    case ACTION_UPDATE_BROADCASTS:
                        if (intent.getBooleanExtra(EXTRA_FORCE_SYNC, true)) {
                            Settings.batchEdit()
                                    .remove(Settings.lastBroadcastSyncTime)
                                    .remove(Settings.lastBaseBroadcastSyncTime)
                                    .persistNow();
                        }
                        BaseWorkManager.start(context, TvService.buildBroadcastUpdateIntent());
                        break;

                    case ACTION_DOWNLOAD_ADVERTISEMENT:
                        BaseWorkManager.start(context, TvTadService.buildAdvertisementDownloadIntent());
                        break;

                    case ACTION_QUERY_PROVIDER:
                        Uri uri = Uri.parse(intent.getStringExtra(EXTRA_URI));
                        String order = intent.getStringExtra(EXTRA_ORDER);
                        SqlUtils.queryFirst(context, uri, null, null, null, order, cursor -> {
                            Log.d(TAG, cursor, 1000, null);
                            return null;
                        });
                        break;

                    case ACTION_FILE_SYSTEM_EXEC:
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        String[] cmd = intent.getStringArrayExtra(EXTRA_CMD);
                        int exitCode = MwProxy.exec(out, cmd);
                        Log.d(TAG, "exec [" + String.join(" ", cmd) + "] exited: " + exitCode + "\n" + out);
                        break;

                    case ACTION_ACCOUNT_INFO:
                        Settings.lastAccountInfoSyncTime.edit().remove();
                        BaseWorkManager.start(context, AccountService.buildUpdateAccountInfoIntent());
                        break;

                    case ACTION_ACCOUNT_PACKAGE_INFO:
                        Settings.batchEdit()
                                .remove(Settings.lastAccountInfoSyncTime)
                                .remove(Settings.lastChannelsSyncTime)
                                .persistNow();
                        BaseWorkManager.start(context, AccountService.buildUpdateAccountPackagesInfo());
                        break;

                    case ACTION_CHECK_INTERNET:
                        Components.get(InternetChecker.class).triggerInternetCheck();
                        break;

                    case ACTION_DELETE_CONFIG:
                        if (intent.hasExtra(EXTRA_SESSION)) {
                            SessionProximus value = SessionProximus.valueOf(intent.getStringExtra(EXTRA_SESSION));
                            sSettingsAccess.putSession(Collections.singletonMap(value, null));
                            break;
                        }
                        if (intent.hasExtra(EXTRA_SETTINGS)) {
                            try {
                                SettingsProximus value = SettingsProximus.valueOf(intent.getStringExtra(EXTRA_SETTINGS));
                                sSettingsAccess.putSettings(Collections.singletonMap(value, null));
                            } catch (Exception e) {
                                Settings value = Settings.valueOf(intent.getStringExtra(EXTRA_SETTINGS));
                                sSettingsAccess.putSettings(Collections.singletonMap(value, null));
                            }
                            break;
                        }
                        throw new Exception("delete allowed from session or settings");

                    case ACTION_RUS_WINDOW_CHECK:
                        BaseWorkManager.start(context, MwService.buildRusWindowIntent());
                        break;

                    case ACTION_RUS_OTA_CHECK:
                        Settings.systemUpdateNextCheckInTime.edit().put(1L);
                        BaseWorkManager.start(context, MwService.buildRusWindowIntent());
                        break;
                }
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        });
    }

    @Override
    public IntentFilter getIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_DUMP_REFERENCE_TABLE);
        intentFilter.addAction(ACTION_QUERY_PROVIDER);
        intentFilter.addAction(ACTION_FILE_SYSTEM_EXEC);
        intentFilter.addAction(ACTION_PERFORM_CACHING);
        intentFilter.addAction(ACTION_UPDATE_CHANNELS);
        intentFilter.addAction(ACTION_DELETE_PLAYBACKS);
        intentFilter.addAction(ACTION_UPDATE_BROADCASTS);
        intentFilter.addAction(ACTION_DOWNLOAD_ADVERTISEMENT);
        intentFilter.addAction(ACTION_ACCOUNT_INFO);
        intentFilter.addAction(ACTION_RUS_WINDOW_CHECK);
        intentFilter.addAction(ACTION_RUS_OTA_CHECK);
        intentFilter.addAction(ACTION_ACCOUNT_PACKAGE_INFO);
        intentFilter.addAction(ACTION_CHECK_INTERNET);
        intentFilter.addAction(ACTION_DELETE_CONFIG);
        return intentFilter;
    }
}
