package tv.threess.threeready.app;

import android.app.Application;
import android.content.ComponentName;

import com.threess.threeready.playerCastlabs.wrapper.CastlabsSurfacePlayerWrapper;
import com.threess.threeready.playerCastlabs.wrapper.CastlabsTifPlayerWrapper;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import tv.threess.lib.di.ComponentProvider;
import tv.threess.lib.di.Components;
import tv.threess.threeready.api.account.setting.Settings;
import tv.threess.threeready.api.config.model.AppConfig;
import tv.threess.threeready.api.config.model.Config;
import tv.threess.threeready.api.generic.ComponentsInitializer;
import tv.threess.threeready.api.generic.helper.LocaleUtils;
import tv.threess.threeready.api.generic.model.InputServiceInfo;
import tv.threess.threeready.api.generic.receivers.HdmiConnectivityReceiver;
import tv.threess.threeready.api.generic.receivers.NetworkChangeReceiver;
import tv.threess.threeready.api.generic.receivers.StandByStateChangeReceiver;
import tv.threess.threeready.api.home.model.error.ErrorConfig;
import tv.threess.threeready.api.home.model.generic.StyleSettings;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.middleware.CasUpdateReceiver;
import tv.threess.threeready.api.pc.ParentalControlManager;
import tv.threess.threeready.api.startup.IStepCreator;
import tv.threess.threeready.data.account.AccountLocalCache;
import tv.threess.threeready.data.account.SettingsAccessProxy;
import tv.threess.threeready.data.generic.MwScanReceiver;
import tv.threess.threeready.data.gms.GmsCache;
import tv.threess.threeready.data.gms.GmsProxyImpl;
import tv.threess.threeready.data.gms.GmsRepositoryImpl;
import tv.threess.threeready.data.home.BaseHomeServiceRepository;
import tv.threess.threeready.data.netflix.NetflixDETCache;
import tv.threess.threeready.data.netflix.receiver.NetflixDETResponseReceiver;
import tv.threess.threeready.data.proximus.account.AccountProxyProximus;
import tv.threess.threeready.data.proximus.account.AccountRepositoryProximus;
import tv.threess.threeready.data.proximus.account.AccountServiceRepositoryProximus;
import tv.threess.threeready.data.proximus.account.BookmarkRepositoryProximus;
import tv.threess.threeready.data.proximus.account.SettingsProximus;
import tv.threess.threeready.data.proximus.generic.ImageRepositoryProximus;
import tv.threess.threeready.data.proximus.generic.InternetCheckerProximus;
import tv.threess.threeready.data.proximus.generic.RetrofitServicesProximus;
import tv.threess.threeready.data.proximus.home.HomeProxyProximus;
import tv.threess.threeready.data.proximus.home.HomeRepositoryProximus;
import tv.threess.threeready.data.proximus.home.HomeServiceRepositoryProximus;
import tv.threess.threeready.data.proximus.log.ReporterProviderProximus;
import tv.threess.threeready.data.proximus.mw.MwProxyProximus;
import tv.threess.threeready.data.proximus.mw.MwRepositoryProximus;
import tv.threess.threeready.data.proximus.mw.MwServiceRepositoryProximus;
import tv.threess.threeready.data.proximus.mw.SystemUpdateReceiverProximus;
import tv.threess.threeready.data.proximus.playback.StreamingSessionProxyProximus;
import tv.threess.threeready.data.proximus.playback.StreamingSessionRepositoryProximus;
import tv.threess.threeready.data.proximus.pvr.PvrProxyProximus;
import tv.threess.threeready.data.proximus.pvr.PvrRepositoryProximus;
import tv.threess.threeready.data.proximus.pvr.PvrServiceRepositoryProximus;
import tv.threess.threeready.data.proximus.search.SearchProxyProximus;
import tv.threess.threeready.data.proximus.search.SearchRepositoryProximus;
import tv.threess.threeready.data.proximus.tv.NowOnTvCacheProximus;
import tv.threess.threeready.data.proximus.tv.TvProxyProximus;
import tv.threess.threeready.data.proximus.tv.TvRepositoryProximus;
import tv.threess.threeready.data.proximus.tv.TvServiceRepositoryProximus;
import tv.threess.threeready.data.proximus.vod.VodCategoryCacheProximus;
import tv.threess.threeready.data.proximus.vod.VodProxyProximus;
import tv.threess.threeready.data.proximus.vod.VodRepositoryProximus;
import tv.threess.threeready.data.proximus.vod.VodServiceRepositoryProximus;
import tv.threess.threeready.data.pvr.PvrCache;
import tv.threess.threeready.data.tv.TvCache;
import tv.threess.threeready.data.vod.VodCache;
import tv.threess.threeready.player.CommandFactory;
import tv.threess.threeready.player.CommandFactoryImpl;
import tv.threess.threeready.player.ControlFactoryProximus;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.PlayerStateManager;
import tv.threess.threeready.player.TrackManager;
import tv.threess.threeready.player.control.ControlFactory;
import tv.threess.threeready.player.inputservice.OttInputService;
import tv.threess.threeready.ui.generic.hint.HintManager;
import tv.threess.threeready.ui.generic.navigation.Navigator;
import tv.threess.threeready.ui.generic.notification.NotificationManager;
import tv.threess.threeready.ui.generic.player.plugin.IpTvPlayerPlugin;
import tv.threess.threeready.ui.generic.player.plugin.IpTvScanRetryPlugin;
import tv.threess.threeready.ui.generic.player.plugin.ReportPlugin;
import tv.threess.threeready.ui.generic.resolver.ConditionResolver;
import tv.threess.threeready.ui.generic.resolver.DataSourceResolver;
import tv.threess.threeready.ui.generic.utils.ErrorHelper;
import tv.threess.threeready.ui.generic.utils.Translator;
import tv.threess.threeready.ui.pvr.RecordingActionHelper;
import tv.threess.threeready.ui.startup.StartupFlow;
import tv.threess.threeready.ui.startup.StepCreator;
import tv.threess.threeready.ui.tv.helper.FavoriteChannelsCache;

/**
 * Initialize the proximus project injectable components.
 *
 * @author Barabas Attila
 * @since 9/22/20
 */
public class ComponentInitializerProximus implements ComponentsInitializer {
    private static final String TAG = Log.tag(ComponentInitializerProximus.class);

    private final Application mApp;

    public ComponentInitializerProximus(Application app) {
        mApp = app;
    }

    @Override
    public void initialize() {
        Components.registerSingletonProvider(SettingsAccessProxy.class, () -> new SettingsAccessProxy(mApp));
        initConfigComponents(BaseHomeServiceRepository.loadConfig(mApp,
                Settings.authenticated.get(false),
                Settings.forceLocalConfig.get(false)));
        initSystemComponents();
        initReceiverComponents();
        initRepositoryComponents();
        initProxyComponents();
        initCacheComponents();
        initTranslator();
        initServiceRepositoryComponents();
        initPlaybackComponents();
        initUIComponents();
    }

    /**
     * Initialize the system related components here. E.g internet checker, standby receiver etc.
     */
    private void initSystemComponents() {
        Components.registerSingletonProvider(NetworkChangeReceiver.class, NetworkChangeReceiver::new);
        Components.registerSingletonProvider(MwScanReceiver.class, MwScanReceiver::new);
        Components.registerSingletonProvider(InternetCheckerProximus.class, () -> new InternetCheckerProximus(mApp));
        Components.registerSingletonProvider(StandByStateChangeReceiver.class, StandByStateChangeReceiver::new);
        Components.registerSingletonProvider(CasUpdateReceiver.class, CasUpdateReceiver::new);
        Components.registerSingletonProvider(HdmiConnectivityReceiver.class, HdmiConnectivityReceiver::new);
        Components.registerSingletonProvider(SystemUpdateReceiverProximus.class, () -> new SystemUpdateReceiverProximus(mApp));
    }

    /**
     * Initialize the broadcast receiver components here.
     */
    private void initReceiverComponents() {
        Components.registerSingletonProvider(NetflixDETResponseReceiver.class, NetflixDETResponseReceiver::new);
    }

    /**
     * Initialize the project specific access data components here.
     */
    private void initRepositoryComponents() {
        Components.registerSingletonProvider(RetrofitServicesProximus.class,
                () -> new RetrofitServicesProximus(mApp));
        Components.registerSingletonProvider(VodCategoryCacheProximus.class, () ->
                new VodCategoryCacheProximus(
                        () -> {
                            try {
                                return Components.get(AccountProxyProximus.class)
                                        .getGlobalData().getCategories();
                            } catch (Exception e) {
                                Log.e(TAG, "Failed to get categories.", e);
                                return Collections.emptyList();
                            }
                        },
                        SettingsProximus.leafCategoryIds::getSet));
        Components.registerSingletonProvider(StreamingSessionRepositoryProximus.class,
                StreamingSessionRepositoryProximus::new);
        Components.registerSingletonProvider(MwRepositoryProximus.class,
                () -> new MwRepositoryProximus(mApp));
        Components.registerSingletonProvider(ImageRepositoryProximus.class,
                ImageRepositoryProximus::new);
        Components.registerSingletonProvider(GmsRepositoryImpl.class,
                () -> new GmsRepositoryImpl(mApp));
        Components.registerSingletonProvider(AccountRepositoryProximus.class,
                () -> new AccountRepositoryProximus(mApp));
        Components.registerSingletonProvider(SearchRepositoryProximus.class,
                SearchRepositoryProximus::new);
        Components.registerSingletonProvider(TvRepositoryProximus.class,
                () -> new TvRepositoryProximus(mApp));
        Components.registerSingletonProvider(VodRepositoryProximus.class,
                () -> new VodRepositoryProximus(mApp));
        Components.registerSingletonProvider(PvrRepositoryProximus.class,
                () -> new PvrRepositoryProximus(mApp));
        Components.registerSingletonProvider(HomeRepositoryProximus.class,
                () -> new HomeRepositoryProximus(mApp));
        Components.registerSingletonProvider(BookmarkRepositoryProximus.class,
                BookmarkRepositoryProximus::new);
    }

    /**
     * Initialize the project specific proxy components here.
     */
    private void initProxyComponents() {
        Components.registerSingletonProvider(AccountProxyProximus.class,
                () -> new AccountProxyProximus(mApp));
        Components.registerSingletonProvider(GmsProxyImpl.class,
                () -> new GmsProxyImpl(mApp));
        Components.registerSingletonProvider(HomeProxyProximus.class,
                () -> new HomeProxyProximus(mApp));
        Components.registerSingletonProvider(MwProxyProximus.class,
                () -> new MwProxyProximus(mApp));
        Components.registerSingletonProvider(PvrProxyProximus.class,
                () -> new PvrProxyProximus(mApp));
        Components.registerSingletonProvider(SearchProxyProximus.class,
                () -> new SearchProxyProximus(mApp));
        Components.registerSingletonProvider(StreamingSessionProxyProximus.class,
                StreamingSessionProxyProximus::new);
        Components.registerSingletonProvider(TvProxyProximus.class,
                () -> new TvProxyProximus(mApp));
        Components.registerSingletonProvider(VodProxyProximus.class,
                () -> new VodProxyProximus(mApp));
    }

    /**
     * Initialize the project specific cache components here.
     */
    private void initCacheComponents() {
        Components.registerSingletonProvider(AccountLocalCache.class,
                () -> new AccountLocalCache(mApp));
        Components.registerSingletonProvider(GmsCache.class,
                () -> new GmsCache(mApp));
        Components.registerSingletonProvider(PvrCache.class,
                () -> new PvrCache(mApp));
        Components.registerSingletonProvider(TvCache.class,
                () -> new TvCache(mApp));
        Components.registerSingletonProvider(VodCache.class,
                () -> new VodCache(mApp));
        Components.registerSingletonProvider(NowOnTvCacheProximus.class,
                () -> new NowOnTvCacheProximus(mApp));
        Components.registerSingletonProvider(FavoriteChannelsCache.class, FavoriteChannelsCache::new);
        Components.registerSingletonProvider(NetflixDETCache.class,
                () -> new NetflixDETCache(mApp));
    }

    /**
     * Initialize the config related components here.
     *
     * @param config The config instance from which the components needs to be registered.
     */
    @Override
    public void initConfigComponents(Config config) {
        Components.registerInstance(config);
        Components.registerInstance(config.getApiConfig());
        Components.registerInstance(config.getContentConfig());
        Components.registerInstance(config.getLayoutConfig());
        Components.registerInstance(config.getFeatureControl());

        AppConfig appConfig = config.getAppConfig();
        if (appConfig != null) {
            Components.registerInstance(appConfig);
            Components.registerInstance(appConfig.getContentMarkers());
            Components.registerInstance(appConfig.getLocaleSettings());
            Components.registerInstance(appConfig.getPlayerConfig());
            Components.registerInstance(appConfig.getContinueWatchingConfig());

            StyleSettings styleSettings = appConfig.getStyleSettings();
            if (styleSettings != null) {
                Components.registerInstance(styleSettings.getButtonStyle());
                Components.registerInstance(styleSettings.getFontStyle());
                Components.registerInstance(styleSettings.getContentMarkerStyle());
            }

            if (appConfig.getPlaybackSettings() != null) {
                Components.registerInstance(appConfig.getPlaybackSettings());
            }

            if (appConfig.getCacheSettings() != null) {
                Components.registerInstance(appConfig.getCacheSettings());
            }

            if (appConfig.getNotificationSettings() != null) {
                Components.registerInstance(appConfig.getNotificationSettings());
            }

            if (appConfig.getOfflineModeSettings() != null) {
                Components.registerInstance(appConfig.getOfflineModeSettings());
            }
        }
        Components.registerSingletonProvider(ReporterProviderProximus.class, ReporterProviderProximus::new);
    }

    /**
     * Initialize the translations and locale related components here.
     */
    private void initTranslator() {
        Components.registerProvider(Translator.class, new ComponentProvider<Translator>() {
            private final Translator mTranslator = new Translator();
            private String mLoadedLanguage = "";

            @Override
            public Translator provide() {
                String currentLanguage = LocaleUtils.getApplicationLanguage();

                // Check if language changed.
                if (!Objects.equals(mLoadedLanguage, currentLanguage)) {
                    mTranslator.setTranslations(HomeServiceRepositoryProximus.loadTranslations(mApp));
                    mLoadedLanguage = currentLanguage;
                }

                return mTranslator;
            }
        });
    }

    /**
     * Initialize the project specific service access data components here.
     */
    private void initServiceRepositoryComponents() {
        Components.registerSingletonProvider(HomeServiceRepositoryProximus.class,
                () -> new HomeServiceRepositoryProximus(mApp, this));
        Components.registerSingletonProvider(AccountServiceRepositoryProximus.class,
                () -> new AccountServiceRepositoryProximus(mApp));
        Components.registerSingletonProvider(TvServiceRepositoryProximus.class,
                () -> new TvServiceRepositoryProximus(mApp));
        Components.registerSingletonProvider(VodServiceRepositoryProximus.class,
                () -> new VodServiceRepositoryProximus(mApp));
        Components.registerSingletonProvider(PvrServiceRepositoryProximus.class,
                () -> new PvrServiceRepositoryProximus(mApp));
        Components.registerSingletonProvider(MwServiceRepositoryProximus.class,
                () -> new MwServiceRepositoryProximus(mApp));
    }

    /**
     * Initialize the playback related components here.
     */
    private void initPlaybackComponents() {
        Components.registerSingletonProvider(TrackManager.class, TrackManager::new);
        Components.registerSingletonProvider(InputServiceInfo.class,
                () -> new InputServiceInfo(new ComponentName(mApp, OttInputService.class)));
        Components.registerSingletonProvider(CastlabsTifPlayerWrapper.class, () -> new CastlabsTifPlayerWrapper(mApp));
        Components.registerSingletonProvider(CastlabsSurfacePlayerWrapper.class, () -> new CastlabsSurfacePlayerWrapper(mApp));
        ControlFactory controlFactory = new ControlFactoryProximus();
        Components.registerInstance(controlFactory);
        Components.registerSingletonProvider(PlaybackDetailsManager.class, () -> {
            CommandFactory commandFactory = new CommandFactoryImpl();
            PlaybackDetailsManager playbackDetailsManager = new PlaybackDetailsManager(
                    new WeakReference<>(mApp), controlFactory, commandFactory);

            playbackDetailsManager.registerPlugin(new IpTvPlayerPlugin(playbackDetailsManager));
            playbackDetailsManager.registerPlugin(new IpTvScanRetryPlugin(playbackDetailsManager));

            ReportPlugin reportPlugin = new ReportPlugin(playbackDetailsManager);
            playbackDetailsManager.registerCallback(reportPlugin);
            playbackDetailsManager.registerPlugin(reportPlugin);

            return playbackDetailsManager;
        });
    }

    /**
     * Initialize the UI related components here. E.g navigator, hint manager, notification manager etc.
     */
    private void initUIComponents() {
        Components.registerSingletonProvider(ErrorHelper.class, () -> {
            List<ErrorConfig> errorConfigs = BaseHomeServiceRepository.getErrorHandlers(mApp);
            Translator translator = Components.get(Translator.class);
            return new ErrorHelper(translator, errorConfigs);
        });

        Components.registerSingletonProvider(Navigator.class, Navigator::new);
        Components.registerSingletonProvider(NotificationManager.class, NotificationManager::new);
        Components.registerSingletonProvider(HintManager.class, () -> new HintManager(mApp));
        Components.registerSingletonProvider(ParentalControlManager.class, ParentalControlManager::new);
        Components.registerSingletonProvider(StartupFlow.class, () -> new StartupFlow(mApp));
        Components.registerSingletonProvider(RecordingActionHelper.class, RecordingActionHelper::new);
        Components.registerSingletonProvider(IStepCreator.class, StepCreator::new);
        Components.registerSingletonProvider(PlayerStateManager.class, PlayerStateManager::new);
        Components.registerSingletonProvider(DataSourceResolver.class, DataSourceResolver::new);
        Components.registerSingletonProvider(ConditionResolver.class, ConditionResolver::new);
    }

}
