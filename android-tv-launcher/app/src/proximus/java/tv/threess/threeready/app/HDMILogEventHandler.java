package tv.threess.threeready.app;

import android.os.Handler;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import tv.threess.lib.di.Components;
import tv.threess.threeready.api.generic.receivers.HdmiConnectivityReceiver;
import tv.threess.threeready.api.generic.receivers.StandByStateChangeReceiver;
import tv.threess.threeready.api.log.Log;
import tv.threess.threeready.api.log.model.Event;
import tv.threess.threeready.api.log.model.UILogEvent;
import tv.threess.threeready.player.PlaybackDetailsManager;
import tv.threess.threeready.player.contract.PlaybackState;

/**
 * Log event handler for data collection to report HDMI connectivity change events.
 *
 * @author Andor Lukacs
 * @since 2019-11-29
 */
public class HDMILogEventHandler implements HdmiConnectivityReceiver.Listener {
    private static final long EVENT_SEND_TIMEOUT = TimeUnit.SECONDS.toMillis(2);

    private final AtomicInteger mConnectCount = new AtomicInteger(0);
    private final AtomicInteger mDisconnectCount = new AtomicInteger(0);
    private final AtomicLong mConnectTimestamp = new AtomicLong(0);
    private final AtomicLong mDisconnectTimestamp = new AtomicLong(0);

    private final Handler mHdmiEventHandler = new Handler();

    private final StandByStateChangeReceiver mStandByStateChangeReceiver = Components.get(StandByStateChangeReceiver.class);
    private final HdmiConnectivityReceiver mHdmiReceiver = Components.get(HdmiConnectivityReceiver.class);
    private final PlaybackDetailsManager mPlaybackManager = Components.get(PlaybackDetailsManager.class);

    @Override
    public void onConnectionChanged(boolean isConnected) {
        mHdmiEventHandler.removeCallbacksAndMessages(null);
        long now = System.currentTimeMillis();
        if (!isConnected) {
            mDisconnectTimestamp.set(now);
            mDisconnectCount.incrementAndGet();
        } else {
            mConnectTimestamp.set(now);
            mConnectCount.incrementAndGet();
        }

        // This workaround is used for avoid multiple viewing identifiers
        if (mDisconnectTimestamp.get() == mConnectTimestamp.get()) {
            if (isConnected) {
                mDisconnectTimestamp.set(0);
                mDisconnectCount.set(0);
            } else {
                mConnectTimestamp.set(0);
                mConnectCount.set(0);
            }
        }

        if (!mStandByStateChangeReceiver.isScreenOn()) {
            // Ignore HDMI status changes. Box is in standby.
            return;
        }

        mHdmiEventHandler.postDelayed(this::hdmiEventTask, EVENT_SEND_TIMEOUT);
    }

    private void hdmiEventTask() {
        long disconnectTimestamp = mDisconnectTimestamp.getAndSet(0);
        long connectTimestamp = mConnectTimestamp.getAndSet(0);
        int disconnectCount = mDisconnectCount.getAndSet(0);
        int connectCount = mConnectCount.getAndSet(0);

        if (disconnectTimestamp > 0 && connectTimestamp > 0) {
            if (disconnectTimestamp < connectTimestamp) {
                reportDisconnect(disconnectTimestamp, disconnectCount);
                reportConnect(connectTimestamp, connectCount);
            } else {
                reportConnect(connectTimestamp, connectCount);
                reportDisconnect(disconnectTimestamp, disconnectCount);
            }
        }
        else if (disconnectTimestamp > 0) {
            reportDisconnect(disconnectTimestamp, disconnectCount);
        }
        else if (connectTimestamp > 0) {
            reportConnect(connectTimestamp, connectCount);
        }
    }

    private void reportConnect(long timestamp, int count) {
        boolean connected = mHdmiReceiver.isConnected();
        Log.event(new Event<>(UILogEvent.Display, timestamp)
                .addDetail(UILogEvent.Detail.ActiveState, true)                     // START event
                .addDetail(UILogEvent.Detail.HDMIPlugState, connected)
                .addDetail(UILogEvent.Detail.HDMIPlugCount, count)
        );
    }

    private void reportDisconnect(long timestamp, int count) {
        boolean playing = mPlaybackManager.inPlaybackState(PlaybackState.Started);
        boolean connected = mHdmiReceiver.isConnected();
        if (!connected && playing) {
            // attended viewing session stopped
            Log.event(new Event<>(UILogEvent.Display, timestamp)
                    .addDetail(UILogEvent.Detail.ActiveState, false)
                    .addDetail(UILogEvent.Detail.HDMIPlugState, true)
                    .addDetail(UILogEvent.Detail.HDMIPlugCount, count)
            );
            // unattended viewing session is started
            Log.event(new Event<>(UILogEvent.Display, timestamp)
                    .addDetail(UILogEvent.Detail.ActiveState, true)
                    .addDetail(UILogEvent.Detail.HDMIPlugState, false)
                    .addDetail(UILogEvent.Detail.HDMIPlugCount, count)
            );
        } else {
            Log.event(new Event<>(UILogEvent.Display, timestamp)
                    .addDetail(UILogEvent.Detail.ActiveState, false)                     // STOP event
                    .addDetail(UILogEvent.Detail.HDMIPlugState, connected)
                    .addDetail(UILogEvent.Detail.HDMIPlugCount, count)
            );
        }
    }
}
