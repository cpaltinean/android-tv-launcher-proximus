/*
 * Copyright ( c ) 2017, 3ScreenSolutions GmbH & Co. KG, All Rights Reserved.
 */

package tv.threess.threeready.app;

import tv.threess.threeready.api.generic.ComponentsInitializer;
import tv.threess.threeready.app.base.BaseApplication;
import tv.threess.threeready.ui.utils.SystemUtils;

/**
 * Application class for Proximus specific implementation.
 * <p>
 * Created by Barabas Attila on 4/11/2017.
 */

public class App extends BaseApplication {
    private HDMILogEventHandler mHDMILogEventHandler;
    private final ComponentInitializerProximus mComponentInitializer = new ComponentInitializerProximus(this);

    @Override
    public void onCreate() {
        super.onCreate();

        switch (SystemUtils.getCurrentProcessType(this)) {
            case MAIN:
                mHDMILogEventHandler = new HDMILogEventHandler();
                mHdmiReceiver.addStateChangedListener(mHDMILogEventHandler);
                break;
            default:
                break;
        }
    }

    @Override
    protected ComponentsInitializer getComponentInitializer() {
        return mComponentInitializer;
    }
}
